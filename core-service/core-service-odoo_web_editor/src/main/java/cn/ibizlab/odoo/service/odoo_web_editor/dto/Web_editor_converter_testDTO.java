package cn.ibizlab.odoo.service.odoo_web_editor.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.core.odoo_web_editor.valuerule.anno.web_editor_converter_test.*;
import cn.ibizlab.odoo.core.odoo_web_editor.domain.Web_editor_converter_test;
import org.springframework.cglib.beans.BeanCopier;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 服务DTO对象[Web_editor_converter_testDTO]
 */
public class Web_editor_converter_testDTO implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [TEXT]
     *
     */
    @Web_editor_converter_testTextDefault(info = "默认规则")
    private String text;

    @JsonIgnore
    private boolean textDirtyFlag;

    /**
     * 属性 [CREATE_DATE]
     *
     */
    @Web_editor_converter_testCreate_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp create_date;

    @JsonIgnore
    private boolean create_dateDirtyFlag;

    /**
     * 属性 [SELECTION_STR]
     *
     */
    @Web_editor_converter_testSelection_strDefault(info = "默认规则")
    private String selection_str;

    @JsonIgnore
    private boolean selection_strDirtyFlag;

    /**
     * 属性 [NUMERIC]
     *
     */
    @Web_editor_converter_testNumericDefault(info = "默认规则")
    private Double numeric;

    @JsonIgnore
    private boolean numericDirtyFlag;

    /**
     * 属性 [INTEGER]
     *
     */
    @Web_editor_converter_testIntegerDefault(info = "默认规则")
    private Integer integer;

    @JsonIgnore
    private boolean integerDirtyFlag;

    /**
     * 属性 [BINARY]
     *
     */
    @Web_editor_converter_testBinaryDefault(info = "默认规则")
    private byte[] binary;

    @JsonIgnore
    private boolean binaryDirtyFlag;

    /**
     * 属性 [DATE]
     *
     */
    @Web_editor_converter_testDateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    private Timestamp date;

    @JsonIgnore
    private boolean dateDirtyFlag;

    /**
     * 属性 [IBIZFLOAT]
     *
     */
    @Web_editor_converter_testIbizfloatDefault(info = "默认规则")
    private Double ibizfloat;

    @JsonIgnore
    private boolean ibizfloatDirtyFlag;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @Web_editor_converter_test__last_updateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp __last_update;

    @JsonIgnore
    private boolean __last_updateDirtyFlag;

    /**
     * 属性 [DATETIME]
     *
     */
    @Web_editor_converter_testDatetimeDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp datetime;

    @JsonIgnore
    private boolean datetimeDirtyFlag;

    /**
     * 属性 [ID]
     *
     */
    @Web_editor_converter_testIdDefault(info = "默认规则")
    private Integer id;

    @JsonIgnore
    private boolean idDirtyFlag;

    /**
     * 属性 [WRITE_DATE]
     *
     */
    @Web_editor_converter_testWrite_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp write_date;

    @JsonIgnore
    private boolean write_dateDirtyFlag;

    /**
     * 属性 [HTML]
     *
     */
    @Web_editor_converter_testHtmlDefault(info = "默认规则")
    private String html;

    @JsonIgnore
    private boolean htmlDirtyFlag;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @Web_editor_converter_testDisplay_nameDefault(info = "默认规则")
    private String display_name;

    @JsonIgnore
    private boolean display_nameDirtyFlag;

    /**
     * 属性 [IBIZCHAR]
     *
     */
    @Web_editor_converter_testIbizcharDefault(info = "默认规则")
    private String ibizchar;

    @JsonIgnore
    private boolean ibizcharDirtyFlag;

    /**
     * 属性 [SELECTION]
     *
     */
    @Web_editor_converter_testSelectionDefault(info = "默认规则")
    private String selection;

    @JsonIgnore
    private boolean selectionDirtyFlag;

    /**
     * 属性 [CREATE_UID_TEXT]
     *
     */
    @Web_editor_converter_testCreate_uid_textDefault(info = "默认规则")
    private String create_uid_text;

    @JsonIgnore
    private boolean create_uid_textDirtyFlag;

    /**
     * 属性 [WRITE_UID_TEXT]
     *
     */
    @Web_editor_converter_testWrite_uid_textDefault(info = "默认规则")
    private String write_uid_text;

    @JsonIgnore
    private boolean write_uid_textDirtyFlag;

    /**
     * 属性 [MANY2ONE_TEXT]
     *
     */
    @Web_editor_converter_testMany2one_textDefault(info = "默认规则")
    private String many2one_text;

    @JsonIgnore
    private boolean many2one_textDirtyFlag;

    /**
     * 属性 [WRITE_UID]
     *
     */
    @Web_editor_converter_testWrite_uidDefault(info = "默认规则")
    private Integer write_uid;

    @JsonIgnore
    private boolean write_uidDirtyFlag;

    /**
     * 属性 [MANY2ONE]
     *
     */
    @Web_editor_converter_testMany2oneDefault(info = "默认规则")
    private Integer many2one;

    @JsonIgnore
    private boolean many2oneDirtyFlag;

    /**
     * 属性 [CREATE_UID]
     *
     */
    @Web_editor_converter_testCreate_uidDefault(info = "默认规则")
    private Integer create_uid;

    @JsonIgnore
    private boolean create_uidDirtyFlag;


    /**
     * 获取 [TEXT]
     */
    @JsonProperty("text")
    public String getText(){
        return text ;
    }

    /**
     * 设置 [TEXT]
     */
    @JsonProperty("text")
    public void setText(String  text){
        this.text = text ;
        this.textDirtyFlag = true ;
    }

    /**
     * 获取 [TEXT]脏标记
     */
    @JsonIgnore
    public boolean getTextDirtyFlag(){
        return textDirtyFlag ;
    }

    /**
     * 获取 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return create_date ;
    }

    /**
     * 设置 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return create_dateDirtyFlag ;
    }

    /**
     * 获取 [SELECTION_STR]
     */
    @JsonProperty("selection_str")
    public String getSelection_str(){
        return selection_str ;
    }

    /**
     * 设置 [SELECTION_STR]
     */
    @JsonProperty("selection_str")
    public void setSelection_str(String  selection_str){
        this.selection_str = selection_str ;
        this.selection_strDirtyFlag = true ;
    }

    /**
     * 获取 [SELECTION_STR]脏标记
     */
    @JsonIgnore
    public boolean getSelection_strDirtyFlag(){
        return selection_strDirtyFlag ;
    }

    /**
     * 获取 [NUMERIC]
     */
    @JsonProperty("numeric")
    public Double getNumeric(){
        return numeric ;
    }

    /**
     * 设置 [NUMERIC]
     */
    @JsonProperty("numeric")
    public void setNumeric(Double  numeric){
        this.numeric = numeric ;
        this.numericDirtyFlag = true ;
    }

    /**
     * 获取 [NUMERIC]脏标记
     */
    @JsonIgnore
    public boolean getNumericDirtyFlag(){
        return numericDirtyFlag ;
    }

    /**
     * 获取 [INTEGER]
     */
    @JsonProperty("integer")
    public Integer getInteger(){
        return integer ;
    }

    /**
     * 设置 [INTEGER]
     */
    @JsonProperty("integer")
    public void setInteger(Integer  integer){
        this.integer = integer ;
        this.integerDirtyFlag = true ;
    }

    /**
     * 获取 [INTEGER]脏标记
     */
    @JsonIgnore
    public boolean getIntegerDirtyFlag(){
        return integerDirtyFlag ;
    }

    /**
     * 获取 [BINARY]
     */
    @JsonProperty("binary")
    public byte[] getBinary(){
        return binary ;
    }

    /**
     * 设置 [BINARY]
     */
    @JsonProperty("binary")
    public void setBinary(byte[]  binary){
        this.binary = binary ;
        this.binaryDirtyFlag = true ;
    }

    /**
     * 获取 [BINARY]脏标记
     */
    @JsonIgnore
    public boolean getBinaryDirtyFlag(){
        return binaryDirtyFlag ;
    }

    /**
     * 获取 [DATE]
     */
    @JsonProperty("date")
    public Timestamp getDate(){
        return date ;
    }

    /**
     * 设置 [DATE]
     */
    @JsonProperty("date")
    public void setDate(Timestamp  date){
        this.date = date ;
        this.dateDirtyFlag = true ;
    }

    /**
     * 获取 [DATE]脏标记
     */
    @JsonIgnore
    public boolean getDateDirtyFlag(){
        return dateDirtyFlag ;
    }

    /**
     * 获取 [IBIZFLOAT]
     */
    @JsonProperty("ibizfloat")
    public Double getIbizfloat(){
        return ibizfloat ;
    }

    /**
     * 设置 [IBIZFLOAT]
     */
    @JsonProperty("ibizfloat")
    public void setIbizfloat(Double  ibizfloat){
        this.ibizfloat = ibizfloat ;
        this.ibizfloatDirtyFlag = true ;
    }

    /**
     * 获取 [IBIZFLOAT]脏标记
     */
    @JsonIgnore
    public boolean getIbizfloatDirtyFlag(){
        return ibizfloatDirtyFlag ;
    }

    /**
     * 获取 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return __last_update ;
    }

    /**
     * 设置 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

    /**
     * 获取 [__LAST_UPDATE]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return __last_updateDirtyFlag ;
    }

    /**
     * 获取 [DATETIME]
     */
    @JsonProperty("datetime")
    public Timestamp getDatetime(){
        return datetime ;
    }

    /**
     * 设置 [DATETIME]
     */
    @JsonProperty("datetime")
    public void setDatetime(Timestamp  datetime){
        this.datetime = datetime ;
        this.datetimeDirtyFlag = true ;
    }

    /**
     * 获取 [DATETIME]脏标记
     */
    @JsonIgnore
    public boolean getDatetimeDirtyFlag(){
        return datetimeDirtyFlag ;
    }

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

    /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return idDirtyFlag ;
    }

    /**
     * 获取 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return write_date ;
    }

    /**
     * 设置 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return write_dateDirtyFlag ;
    }

    /**
     * 获取 [HTML]
     */
    @JsonProperty("html")
    public String getHtml(){
        return html ;
    }

    /**
     * 设置 [HTML]
     */
    @JsonProperty("html")
    public void setHtml(String  html){
        this.html = html ;
        this.htmlDirtyFlag = true ;
    }

    /**
     * 获取 [HTML]脏标记
     */
    @JsonIgnore
    public boolean getHtmlDirtyFlag(){
        return htmlDirtyFlag ;
    }

    /**
     * 获取 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return display_name ;
    }

    /**
     * 设置 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

    /**
     * 获取 [DISPLAY_NAME]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return display_nameDirtyFlag ;
    }

    /**
     * 获取 [IBIZCHAR]
     */
    @JsonProperty("ibizchar")
    public String getIbizchar(){
        return ibizchar ;
    }

    /**
     * 设置 [IBIZCHAR]
     */
    @JsonProperty("ibizchar")
    public void setIbizchar(String  ibizchar){
        this.ibizchar = ibizchar ;
        this.ibizcharDirtyFlag = true ;
    }

    /**
     * 获取 [IBIZCHAR]脏标记
     */
    @JsonIgnore
    public boolean getIbizcharDirtyFlag(){
        return ibizcharDirtyFlag ;
    }

    /**
     * 获取 [SELECTION]
     */
    @JsonProperty("selection")
    public String getSelection(){
        return selection ;
    }

    /**
     * 设置 [SELECTION]
     */
    @JsonProperty("selection")
    public void setSelection(String  selection){
        this.selection = selection ;
        this.selectionDirtyFlag = true ;
    }

    /**
     * 获取 [SELECTION]脏标记
     */
    @JsonIgnore
    public boolean getSelectionDirtyFlag(){
        return selectionDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return create_uid_text ;
    }

    /**
     * 设置 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return create_uid_textDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return write_uid_text ;
    }

    /**
     * 设置 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return write_uid_textDirtyFlag ;
    }

    /**
     * 获取 [MANY2ONE_TEXT]
     */
    @JsonProperty("many2one_text")
    public String getMany2one_text(){
        return many2one_text ;
    }

    /**
     * 设置 [MANY2ONE_TEXT]
     */
    @JsonProperty("many2one_text")
    public void setMany2one_text(String  many2one_text){
        this.many2one_text = many2one_text ;
        this.many2one_textDirtyFlag = true ;
    }

    /**
     * 获取 [MANY2ONE_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getMany2one_textDirtyFlag(){
        return many2one_textDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return write_uid ;
    }

    /**
     * 设置 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return write_uidDirtyFlag ;
    }

    /**
     * 获取 [MANY2ONE]
     */
    @JsonProperty("many2one")
    public Integer getMany2one(){
        return many2one ;
    }

    /**
     * 设置 [MANY2ONE]
     */
    @JsonProperty("many2one")
    public void setMany2one(Integer  many2one){
        this.many2one = many2one ;
        this.many2oneDirtyFlag = true ;
    }

    /**
     * 获取 [MANY2ONE]脏标记
     */
    @JsonIgnore
    public boolean getMany2oneDirtyFlag(){
        return many2oneDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return create_uid ;
    }

    /**
     * 设置 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return create_uidDirtyFlag ;
    }



    public Web_editor_converter_test toDO() {
        Web_editor_converter_test srfdomain = new Web_editor_converter_test();
        if(getTextDirtyFlag())
            srfdomain.setText(text);
        if(getCreate_dateDirtyFlag())
            srfdomain.setCreate_date(create_date);
        if(getSelection_strDirtyFlag())
            srfdomain.setSelection_str(selection_str);
        if(getNumericDirtyFlag())
            srfdomain.setNumeric(numeric);
        if(getIntegerDirtyFlag())
            srfdomain.setInteger(integer);
        if(getBinaryDirtyFlag())
            srfdomain.setBinary(binary);
        if(getDateDirtyFlag())
            srfdomain.setDate(date);
        if(getIbizfloatDirtyFlag())
            srfdomain.setIbizfloat(ibizfloat);
        if(get__last_updateDirtyFlag())
            srfdomain.set__last_update(__last_update);
        if(getDatetimeDirtyFlag())
            srfdomain.setDatetime(datetime);
        if(getIdDirtyFlag())
            srfdomain.setId(id);
        if(getWrite_dateDirtyFlag())
            srfdomain.setWrite_date(write_date);
        if(getHtmlDirtyFlag())
            srfdomain.setHtml(html);
        if(getDisplay_nameDirtyFlag())
            srfdomain.setDisplay_name(display_name);
        if(getIbizcharDirtyFlag())
            srfdomain.setIbizchar(ibizchar);
        if(getSelectionDirtyFlag())
            srfdomain.setSelection(selection);
        if(getCreate_uid_textDirtyFlag())
            srfdomain.setCreate_uid_text(create_uid_text);
        if(getWrite_uid_textDirtyFlag())
            srfdomain.setWrite_uid_text(write_uid_text);
        if(getMany2one_textDirtyFlag())
            srfdomain.setMany2one_text(many2one_text);
        if(getWrite_uidDirtyFlag())
            srfdomain.setWrite_uid(write_uid);
        if(getMany2oneDirtyFlag())
            srfdomain.setMany2one(many2one);
        if(getCreate_uidDirtyFlag())
            srfdomain.setCreate_uid(create_uid);

        return srfdomain;
    }

    public void fromDO(Web_editor_converter_test srfdomain) {
        if(srfdomain == null )
          return ;
        if(srfdomain.getTextDirtyFlag())
            this.setText(srfdomain.getText());
        if(srfdomain.getCreate_dateDirtyFlag())
            this.setCreate_date(srfdomain.getCreate_date());
        if(srfdomain.getSelection_strDirtyFlag())
            this.setSelection_str(srfdomain.getSelection_str());
        if(srfdomain.getNumericDirtyFlag())
            this.setNumeric(srfdomain.getNumeric());
        if(srfdomain.getIntegerDirtyFlag())
            this.setInteger(srfdomain.getInteger());
        if(srfdomain.getBinaryDirtyFlag())
            this.setBinary(srfdomain.getBinary());
        if(srfdomain.getDateDirtyFlag())
            this.setDate(srfdomain.getDate());
        if(srfdomain.getIbizfloatDirtyFlag())
            this.setIbizfloat(srfdomain.getIbizfloat());
        if(srfdomain.get__last_updateDirtyFlag())
            this.set__last_update(srfdomain.get__last_update());
        if(srfdomain.getDatetimeDirtyFlag())
            this.setDatetime(srfdomain.getDatetime());
        if(srfdomain.getIdDirtyFlag())
            this.setId(srfdomain.getId());
        if(srfdomain.getWrite_dateDirtyFlag())
            this.setWrite_date(srfdomain.getWrite_date());
        if(srfdomain.getHtmlDirtyFlag())
            this.setHtml(srfdomain.getHtml());
        if(srfdomain.getDisplay_nameDirtyFlag())
            this.setDisplay_name(srfdomain.getDisplay_name());
        if(srfdomain.getIbizcharDirtyFlag())
            this.setIbizchar(srfdomain.getIbizchar());
        if(srfdomain.getSelectionDirtyFlag())
            this.setSelection(srfdomain.getSelection());
        if(srfdomain.getCreate_uid_textDirtyFlag())
            this.setCreate_uid_text(srfdomain.getCreate_uid_text());
        if(srfdomain.getWrite_uid_textDirtyFlag())
            this.setWrite_uid_text(srfdomain.getWrite_uid_text());
        if(srfdomain.getMany2one_textDirtyFlag())
            this.setMany2one_text(srfdomain.getMany2one_text());
        if(srfdomain.getWrite_uidDirtyFlag())
            this.setWrite_uid(srfdomain.getWrite_uid());
        if(srfdomain.getMany2oneDirtyFlag())
            this.setMany2one(srfdomain.getMany2one());
        if(srfdomain.getCreate_uidDirtyFlag())
            this.setCreate_uid(srfdomain.getCreate_uid());

    }

    public List<Web_editor_converter_testDTO> fromDOPage(List<Web_editor_converter_test> poPage)   {
        if(poPage == null)
            return null;
        List<Web_editor_converter_testDTO> dtos=new ArrayList<Web_editor_converter_testDTO>();
        for(Web_editor_converter_test domain : poPage) {
            Web_editor_converter_testDTO dto = new Web_editor_converter_testDTO();
            dto.fromDO(domain);
            dtos.add(dto);
        }
        return dtos;
    }
}

