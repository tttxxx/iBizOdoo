package cn.ibizlab.odoo.service.odoo_fleet.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.core.odoo_fleet.valuerule.anno.fleet_vehicle_assignation_log.*;
import cn.ibizlab.odoo.core.odoo_fleet.domain.Fleet_vehicle_assignation_log;
import org.springframework.cglib.beans.BeanCopier;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 服务DTO对象[Fleet_vehicle_assignation_logDTO]
 */
public class Fleet_vehicle_assignation_logDTO implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [ID]
     *
     */
    @Fleet_vehicle_assignation_logIdDefault(info = "默认规则")
    private Integer id;

    @JsonIgnore
    private boolean idDirtyFlag;

    /**
     * 属性 [DATE_START]
     *
     */
    @Fleet_vehicle_assignation_logDate_startDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    private Timestamp date_start;

    @JsonIgnore
    private boolean date_startDirtyFlag;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @Fleet_vehicle_assignation_log__last_updateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp __last_update;

    @JsonIgnore
    private boolean __last_updateDirtyFlag;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @Fleet_vehicle_assignation_logDisplay_nameDefault(info = "默认规则")
    private String display_name;

    @JsonIgnore
    private boolean display_nameDirtyFlag;

    /**
     * 属性 [CREATE_DATE]
     *
     */
    @Fleet_vehicle_assignation_logCreate_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp create_date;

    @JsonIgnore
    private boolean create_dateDirtyFlag;

    /**
     * 属性 [DATE_END]
     *
     */
    @Fleet_vehicle_assignation_logDate_endDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    private Timestamp date_end;

    @JsonIgnore
    private boolean date_endDirtyFlag;

    /**
     * 属性 [WRITE_DATE]
     *
     */
    @Fleet_vehicle_assignation_logWrite_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp write_date;

    @JsonIgnore
    private boolean write_dateDirtyFlag;

    /**
     * 属性 [VEHICLE_ID_TEXT]
     *
     */
    @Fleet_vehicle_assignation_logVehicle_id_textDefault(info = "默认规则")
    private String vehicle_id_text;

    @JsonIgnore
    private boolean vehicle_id_textDirtyFlag;

    /**
     * 属性 [DRIVER_ID_TEXT]
     *
     */
    @Fleet_vehicle_assignation_logDriver_id_textDefault(info = "默认规则")
    private String driver_id_text;

    @JsonIgnore
    private boolean driver_id_textDirtyFlag;

    /**
     * 属性 [CREATE_UID_TEXT]
     *
     */
    @Fleet_vehicle_assignation_logCreate_uid_textDefault(info = "默认规则")
    private String create_uid_text;

    @JsonIgnore
    private boolean create_uid_textDirtyFlag;

    /**
     * 属性 [WRITE_UID_TEXT]
     *
     */
    @Fleet_vehicle_assignation_logWrite_uid_textDefault(info = "默认规则")
    private String write_uid_text;

    @JsonIgnore
    private boolean write_uid_textDirtyFlag;

    /**
     * 属性 [CREATE_UID]
     *
     */
    @Fleet_vehicle_assignation_logCreate_uidDefault(info = "默认规则")
    private Integer create_uid;

    @JsonIgnore
    private boolean create_uidDirtyFlag;

    /**
     * 属性 [VEHICLE_ID]
     *
     */
    @Fleet_vehicle_assignation_logVehicle_idDefault(info = "默认规则")
    private Integer vehicle_id;

    @JsonIgnore
    private boolean vehicle_idDirtyFlag;

    /**
     * 属性 [WRITE_UID]
     *
     */
    @Fleet_vehicle_assignation_logWrite_uidDefault(info = "默认规则")
    private Integer write_uid;

    @JsonIgnore
    private boolean write_uidDirtyFlag;

    /**
     * 属性 [DRIVER_ID]
     *
     */
    @Fleet_vehicle_assignation_logDriver_idDefault(info = "默认规则")
    private Integer driver_id;

    @JsonIgnore
    private boolean driver_idDirtyFlag;


    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

    /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return idDirtyFlag ;
    }

    /**
     * 获取 [DATE_START]
     */
    @JsonProperty("date_start")
    public Timestamp getDate_start(){
        return date_start ;
    }

    /**
     * 设置 [DATE_START]
     */
    @JsonProperty("date_start")
    public void setDate_start(Timestamp  date_start){
        this.date_start = date_start ;
        this.date_startDirtyFlag = true ;
    }

    /**
     * 获取 [DATE_START]脏标记
     */
    @JsonIgnore
    public boolean getDate_startDirtyFlag(){
        return date_startDirtyFlag ;
    }

    /**
     * 获取 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return __last_update ;
    }

    /**
     * 设置 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

    /**
     * 获取 [__LAST_UPDATE]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return __last_updateDirtyFlag ;
    }

    /**
     * 获取 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return display_name ;
    }

    /**
     * 设置 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

    /**
     * 获取 [DISPLAY_NAME]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return display_nameDirtyFlag ;
    }

    /**
     * 获取 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return create_date ;
    }

    /**
     * 设置 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return create_dateDirtyFlag ;
    }

    /**
     * 获取 [DATE_END]
     */
    @JsonProperty("date_end")
    public Timestamp getDate_end(){
        return date_end ;
    }

    /**
     * 设置 [DATE_END]
     */
    @JsonProperty("date_end")
    public void setDate_end(Timestamp  date_end){
        this.date_end = date_end ;
        this.date_endDirtyFlag = true ;
    }

    /**
     * 获取 [DATE_END]脏标记
     */
    @JsonIgnore
    public boolean getDate_endDirtyFlag(){
        return date_endDirtyFlag ;
    }

    /**
     * 获取 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return write_date ;
    }

    /**
     * 设置 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return write_dateDirtyFlag ;
    }

    /**
     * 获取 [VEHICLE_ID_TEXT]
     */
    @JsonProperty("vehicle_id_text")
    public String getVehicle_id_text(){
        return vehicle_id_text ;
    }

    /**
     * 设置 [VEHICLE_ID_TEXT]
     */
    @JsonProperty("vehicle_id_text")
    public void setVehicle_id_text(String  vehicle_id_text){
        this.vehicle_id_text = vehicle_id_text ;
        this.vehicle_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [VEHICLE_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getVehicle_id_textDirtyFlag(){
        return vehicle_id_textDirtyFlag ;
    }

    /**
     * 获取 [DRIVER_ID_TEXT]
     */
    @JsonProperty("driver_id_text")
    public String getDriver_id_text(){
        return driver_id_text ;
    }

    /**
     * 设置 [DRIVER_ID_TEXT]
     */
    @JsonProperty("driver_id_text")
    public void setDriver_id_text(String  driver_id_text){
        this.driver_id_text = driver_id_text ;
        this.driver_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [DRIVER_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getDriver_id_textDirtyFlag(){
        return driver_id_textDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return create_uid_text ;
    }

    /**
     * 设置 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return create_uid_textDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return write_uid_text ;
    }

    /**
     * 设置 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return write_uid_textDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return create_uid ;
    }

    /**
     * 设置 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return create_uidDirtyFlag ;
    }

    /**
     * 获取 [VEHICLE_ID]
     */
    @JsonProperty("vehicle_id")
    public Integer getVehicle_id(){
        return vehicle_id ;
    }

    /**
     * 设置 [VEHICLE_ID]
     */
    @JsonProperty("vehicle_id")
    public void setVehicle_id(Integer  vehicle_id){
        this.vehicle_id = vehicle_id ;
        this.vehicle_idDirtyFlag = true ;
    }

    /**
     * 获取 [VEHICLE_ID]脏标记
     */
    @JsonIgnore
    public boolean getVehicle_idDirtyFlag(){
        return vehicle_idDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return write_uid ;
    }

    /**
     * 设置 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return write_uidDirtyFlag ;
    }

    /**
     * 获取 [DRIVER_ID]
     */
    @JsonProperty("driver_id")
    public Integer getDriver_id(){
        return driver_id ;
    }

    /**
     * 设置 [DRIVER_ID]
     */
    @JsonProperty("driver_id")
    public void setDriver_id(Integer  driver_id){
        this.driver_id = driver_id ;
        this.driver_idDirtyFlag = true ;
    }

    /**
     * 获取 [DRIVER_ID]脏标记
     */
    @JsonIgnore
    public boolean getDriver_idDirtyFlag(){
        return driver_idDirtyFlag ;
    }



    public Fleet_vehicle_assignation_log toDO() {
        Fleet_vehicle_assignation_log srfdomain = new Fleet_vehicle_assignation_log();
        if(getIdDirtyFlag())
            srfdomain.setId(id);
        if(getDate_startDirtyFlag())
            srfdomain.setDate_start(date_start);
        if(get__last_updateDirtyFlag())
            srfdomain.set__last_update(__last_update);
        if(getDisplay_nameDirtyFlag())
            srfdomain.setDisplay_name(display_name);
        if(getCreate_dateDirtyFlag())
            srfdomain.setCreate_date(create_date);
        if(getDate_endDirtyFlag())
            srfdomain.setDate_end(date_end);
        if(getWrite_dateDirtyFlag())
            srfdomain.setWrite_date(write_date);
        if(getVehicle_id_textDirtyFlag())
            srfdomain.setVehicle_id_text(vehicle_id_text);
        if(getDriver_id_textDirtyFlag())
            srfdomain.setDriver_id_text(driver_id_text);
        if(getCreate_uid_textDirtyFlag())
            srfdomain.setCreate_uid_text(create_uid_text);
        if(getWrite_uid_textDirtyFlag())
            srfdomain.setWrite_uid_text(write_uid_text);
        if(getCreate_uidDirtyFlag())
            srfdomain.setCreate_uid(create_uid);
        if(getVehicle_idDirtyFlag())
            srfdomain.setVehicle_id(vehicle_id);
        if(getWrite_uidDirtyFlag())
            srfdomain.setWrite_uid(write_uid);
        if(getDriver_idDirtyFlag())
            srfdomain.setDriver_id(driver_id);

        return srfdomain;
    }

    public void fromDO(Fleet_vehicle_assignation_log srfdomain) {
        if(srfdomain == null )
          return ;
        if(srfdomain.getIdDirtyFlag())
            this.setId(srfdomain.getId());
        if(srfdomain.getDate_startDirtyFlag())
            this.setDate_start(srfdomain.getDate_start());
        if(srfdomain.get__last_updateDirtyFlag())
            this.set__last_update(srfdomain.get__last_update());
        if(srfdomain.getDisplay_nameDirtyFlag())
            this.setDisplay_name(srfdomain.getDisplay_name());
        if(srfdomain.getCreate_dateDirtyFlag())
            this.setCreate_date(srfdomain.getCreate_date());
        if(srfdomain.getDate_endDirtyFlag())
            this.setDate_end(srfdomain.getDate_end());
        if(srfdomain.getWrite_dateDirtyFlag())
            this.setWrite_date(srfdomain.getWrite_date());
        if(srfdomain.getVehicle_id_textDirtyFlag())
            this.setVehicle_id_text(srfdomain.getVehicle_id_text());
        if(srfdomain.getDriver_id_textDirtyFlag())
            this.setDriver_id_text(srfdomain.getDriver_id_text());
        if(srfdomain.getCreate_uid_textDirtyFlag())
            this.setCreate_uid_text(srfdomain.getCreate_uid_text());
        if(srfdomain.getWrite_uid_textDirtyFlag())
            this.setWrite_uid_text(srfdomain.getWrite_uid_text());
        if(srfdomain.getCreate_uidDirtyFlag())
            this.setCreate_uid(srfdomain.getCreate_uid());
        if(srfdomain.getVehicle_idDirtyFlag())
            this.setVehicle_id(srfdomain.getVehicle_id());
        if(srfdomain.getWrite_uidDirtyFlag())
            this.setWrite_uid(srfdomain.getWrite_uid());
        if(srfdomain.getDriver_idDirtyFlag())
            this.setDriver_id(srfdomain.getDriver_id());

    }

    public List<Fleet_vehicle_assignation_logDTO> fromDOPage(List<Fleet_vehicle_assignation_log> poPage)   {
        if(poPage == null)
            return null;
        List<Fleet_vehicle_assignation_logDTO> dtos=new ArrayList<Fleet_vehicle_assignation_logDTO>();
        for(Fleet_vehicle_assignation_log domain : poPage) {
            Fleet_vehicle_assignation_logDTO dto = new Fleet_vehicle_assignation_logDTO();
            dto.fromDO(domain);
            dtos.add(dto);
        }
        return dtos;
    }
}

