package cn.ibizlab.odoo.service.odoo_fleet.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_fleet.dto.Fleet_vehicle_costDTO;
import cn.ibizlab.odoo.core.odoo_fleet.domain.Fleet_vehicle_cost;
import cn.ibizlab.odoo.core.odoo_fleet.service.IFleet_vehicle_costService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_fleet.filter.Fleet_vehicle_costSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Fleet_vehicle_cost" })
@RestController
@RequestMapping("")
public class Fleet_vehicle_costResource {

    @Autowired
    private IFleet_vehicle_costService fleet_vehicle_costService;

    public IFleet_vehicle_costService getFleet_vehicle_costService() {
        return this.fleet_vehicle_costService;
    }

    @ApiOperation(value = "获取数据", tags = {"Fleet_vehicle_cost" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_fleet/fleet_vehicle_costs/{fleet_vehicle_cost_id}")
    public ResponseEntity<Fleet_vehicle_costDTO> get(@PathVariable("fleet_vehicle_cost_id") Integer fleet_vehicle_cost_id) {
        Fleet_vehicle_costDTO dto = new Fleet_vehicle_costDTO();
        Fleet_vehicle_cost domain = fleet_vehicle_costService.get(fleet_vehicle_cost_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "建立数据", tags = {"Fleet_vehicle_cost" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_fleet/fleet_vehicle_costs")

    public ResponseEntity<Fleet_vehicle_costDTO> create(@RequestBody Fleet_vehicle_costDTO fleet_vehicle_costdto) {
        Fleet_vehicle_costDTO dto = new Fleet_vehicle_costDTO();
        Fleet_vehicle_cost domain = fleet_vehicle_costdto.toDO();
		fleet_vehicle_costService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批更新数据", tags = {"Fleet_vehicle_cost" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_fleet/fleet_vehicle_costs/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Fleet_vehicle_costDTO> fleet_vehicle_costdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "删除数据", tags = {"Fleet_vehicle_cost" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_fleet/fleet_vehicle_costs/{fleet_vehicle_cost_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("fleet_vehicle_cost_id") Integer fleet_vehicle_cost_id) {
        Fleet_vehicle_costDTO fleet_vehicle_costdto = new Fleet_vehicle_costDTO();
		Fleet_vehicle_cost domain = new Fleet_vehicle_cost();
		fleet_vehicle_costdto.setId(fleet_vehicle_cost_id);
		domain.setId(fleet_vehicle_cost_id);
        Boolean rst = fleet_vehicle_costService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "批删除数据", tags = {"Fleet_vehicle_cost" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_fleet/fleet_vehicle_costs/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Fleet_vehicle_costDTO> fleet_vehicle_costdtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "更新数据", tags = {"Fleet_vehicle_cost" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_fleet/fleet_vehicle_costs/{fleet_vehicle_cost_id}")

    public ResponseEntity<Fleet_vehicle_costDTO> update(@PathVariable("fleet_vehicle_cost_id") Integer fleet_vehicle_cost_id, @RequestBody Fleet_vehicle_costDTO fleet_vehicle_costdto) {
		Fleet_vehicle_cost domain = fleet_vehicle_costdto.toDO();
        domain.setId(fleet_vehicle_cost_id);
		fleet_vehicle_costService.update(domain);
		Fleet_vehicle_costDTO dto = new Fleet_vehicle_costDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批建立数据", tags = {"Fleet_vehicle_cost" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_fleet/fleet_vehicle_costs/createBatch")
    public ResponseEntity<Boolean> createBatchFleet_vehicle_cost(@RequestBody List<Fleet_vehicle_costDTO> fleet_vehicle_costdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Fleet_vehicle_cost" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_fleet/fleet_vehicle_costs/fetchdefault")
	public ResponseEntity<Page<Fleet_vehicle_costDTO>> fetchDefault(Fleet_vehicle_costSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Fleet_vehicle_costDTO> list = new ArrayList<Fleet_vehicle_costDTO>();
        
        Page<Fleet_vehicle_cost> domains = fleet_vehicle_costService.searchDefault(context) ;
        for(Fleet_vehicle_cost fleet_vehicle_cost : domains.getContent()){
            Fleet_vehicle_costDTO dto = new Fleet_vehicle_costDTO();
            dto.fromDO(fleet_vehicle_cost);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
