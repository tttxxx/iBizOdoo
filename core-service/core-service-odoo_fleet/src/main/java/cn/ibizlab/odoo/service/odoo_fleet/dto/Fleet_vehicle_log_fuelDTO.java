package cn.ibizlab.odoo.service.odoo_fleet.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.core.odoo_fleet.valuerule.anno.fleet_vehicle_log_fuel.*;
import cn.ibizlab.odoo.core.odoo_fleet.domain.Fleet_vehicle_log_fuel;
import org.springframework.cglib.beans.BeanCopier;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 服务DTO对象[Fleet_vehicle_log_fuelDTO]
 */
public class Fleet_vehicle_log_fuelDTO implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [ID]
     *
     */
    @Fleet_vehicle_log_fuelIdDefault(info = "默认规则")
    private Integer id;

    @JsonIgnore
    private boolean idDirtyFlag;

    /**
     * 属性 [PRICE_PER_LITER]
     *
     */
    @Fleet_vehicle_log_fuelPrice_per_literDefault(info = "默认规则")
    private Double price_per_liter;

    @JsonIgnore
    private boolean price_per_literDirtyFlag;

    /**
     * 属性 [COST_IDS]
     *
     */
    @Fleet_vehicle_log_fuelCost_idsDefault(info = "默认规则")
    private String cost_ids;

    @JsonIgnore
    private boolean cost_idsDirtyFlag;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @Fleet_vehicle_log_fuelDisplay_nameDefault(info = "默认规则")
    private String display_name;

    @JsonIgnore
    private boolean display_nameDirtyFlag;

    /**
     * 属性 [WRITE_DATE]
     *
     */
    @Fleet_vehicle_log_fuelWrite_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp write_date;

    @JsonIgnore
    private boolean write_dateDirtyFlag;

    /**
     * 属性 [LITER]
     *
     */
    @Fleet_vehicle_log_fuelLiterDefault(info = "默认规则")
    private Double liter;

    @JsonIgnore
    private boolean literDirtyFlag;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @Fleet_vehicle_log_fuel__last_updateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp __last_update;

    @JsonIgnore
    private boolean __last_updateDirtyFlag;

    /**
     * 属性 [NOTES]
     *
     */
    @Fleet_vehicle_log_fuelNotesDefault(info = "默认规则")
    private String notes;

    @JsonIgnore
    private boolean notesDirtyFlag;

    /**
     * 属性 [CREATE_DATE]
     *
     */
    @Fleet_vehicle_log_fuelCreate_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp create_date;

    @JsonIgnore
    private boolean create_dateDirtyFlag;

    /**
     * 属性 [INV_REF]
     *
     */
    @Fleet_vehicle_log_fuelInv_refDefault(info = "默认规则")
    private String inv_ref;

    @JsonIgnore
    private boolean inv_refDirtyFlag;

    /**
     * 属性 [COST_SUBTYPE_ID]
     *
     */
    @Fleet_vehicle_log_fuelCost_subtype_idDefault(info = "默认规则")
    private Integer cost_subtype_id;

    @JsonIgnore
    private boolean cost_subtype_idDirtyFlag;

    /**
     * 属性 [CREATE_UID_TEXT]
     *
     */
    @Fleet_vehicle_log_fuelCreate_uid_textDefault(info = "默认规则")
    private String create_uid_text;

    @JsonIgnore
    private boolean create_uid_textDirtyFlag;

    /**
     * 属性 [ODOMETER_ID]
     *
     */
    @Fleet_vehicle_log_fuelOdometer_idDefault(info = "默认规则")
    private Integer odometer_id;

    @JsonIgnore
    private boolean odometer_idDirtyFlag;

    /**
     * 属性 [COST_TYPE]
     *
     */
    @Fleet_vehicle_log_fuelCost_typeDefault(info = "默认规则")
    private String cost_type;

    @JsonIgnore
    private boolean cost_typeDirtyFlag;

    /**
     * 属性 [AMOUNT]
     *
     */
    @Fleet_vehicle_log_fuelAmountDefault(info = "默认规则")
    private Double amount;

    @JsonIgnore
    private boolean amountDirtyFlag;

    /**
     * 属性 [PURCHASER_ID_TEXT]
     *
     */
    @Fleet_vehicle_log_fuelPurchaser_id_textDefault(info = "默认规则")
    private String purchaser_id_text;

    @JsonIgnore
    private boolean purchaser_id_textDirtyFlag;

    /**
     * 属性 [WRITE_UID_TEXT]
     *
     */
    @Fleet_vehicle_log_fuelWrite_uid_textDefault(info = "默认规则")
    private String write_uid_text;

    @JsonIgnore
    private boolean write_uid_textDirtyFlag;

    /**
     * 属性 [COST_AMOUNT]
     *
     */
    @Fleet_vehicle_log_fuelCost_amountDefault(info = "默认规则")
    private Double cost_amount;

    @JsonIgnore
    private boolean cost_amountDirtyFlag;

    /**
     * 属性 [PARENT_ID]
     *
     */
    @Fleet_vehicle_log_fuelParent_idDefault(info = "默认规则")
    private Integer parent_id;

    @JsonIgnore
    private boolean parent_idDirtyFlag;

    /**
     * 属性 [VEHICLE_ID]
     *
     */
    @Fleet_vehicle_log_fuelVehicle_idDefault(info = "默认规则")
    private Integer vehicle_id;

    @JsonIgnore
    private boolean vehicle_idDirtyFlag;

    /**
     * 属性 [AUTO_GENERATED]
     *
     */
    @Fleet_vehicle_log_fuelAuto_generatedDefault(info = "默认规则")
    private String auto_generated;

    @JsonIgnore
    private boolean auto_generatedDirtyFlag;

    /**
     * 属性 [CONTRACT_ID]
     *
     */
    @Fleet_vehicle_log_fuelContract_idDefault(info = "默认规则")
    private Integer contract_id;

    @JsonIgnore
    private boolean contract_idDirtyFlag;

    /**
     * 属性 [DESCRIPTION]
     *
     */
    @Fleet_vehicle_log_fuelDescriptionDefault(info = "默认规则")
    private String description;

    @JsonIgnore
    private boolean descriptionDirtyFlag;

    /**
     * 属性 [NAME]
     *
     */
    @Fleet_vehicle_log_fuelNameDefault(info = "默认规则")
    private String name;

    @JsonIgnore
    private boolean nameDirtyFlag;

    /**
     * 属性 [VENDOR_ID_TEXT]
     *
     */
    @Fleet_vehicle_log_fuelVendor_id_textDefault(info = "默认规则")
    private String vendor_id_text;

    @JsonIgnore
    private boolean vendor_id_textDirtyFlag;

    /**
     * 属性 [ODOMETER]
     *
     */
    @Fleet_vehicle_log_fuelOdometerDefault(info = "默认规则")
    private Double odometer;

    @JsonIgnore
    private boolean odometerDirtyFlag;

    /**
     * 属性 [DATE]
     *
     */
    @Fleet_vehicle_log_fuelDateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    private Timestamp date;

    @JsonIgnore
    private boolean dateDirtyFlag;

    /**
     * 属性 [ODOMETER_UNIT]
     *
     */
    @Fleet_vehicle_log_fuelOdometer_unitDefault(info = "默认规则")
    private String odometer_unit;

    @JsonIgnore
    private boolean odometer_unitDirtyFlag;

    /**
     * 属性 [WRITE_UID]
     *
     */
    @Fleet_vehicle_log_fuelWrite_uidDefault(info = "默认规则")
    private Integer write_uid;

    @JsonIgnore
    private boolean write_uidDirtyFlag;

    /**
     * 属性 [COST_ID]
     *
     */
    @Fleet_vehicle_log_fuelCost_idDefault(info = "默认规则")
    private Integer cost_id;

    @JsonIgnore
    private boolean cost_idDirtyFlag;

    /**
     * 属性 [CREATE_UID]
     *
     */
    @Fleet_vehicle_log_fuelCreate_uidDefault(info = "默认规则")
    private Integer create_uid;

    @JsonIgnore
    private boolean create_uidDirtyFlag;

    /**
     * 属性 [VENDOR_ID]
     *
     */
    @Fleet_vehicle_log_fuelVendor_idDefault(info = "默认规则")
    private Integer vendor_id;

    @JsonIgnore
    private boolean vendor_idDirtyFlag;

    /**
     * 属性 [PURCHASER_ID]
     *
     */
    @Fleet_vehicle_log_fuelPurchaser_idDefault(info = "默认规则")
    private Integer purchaser_id;

    @JsonIgnore
    private boolean purchaser_idDirtyFlag;


    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

    /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return idDirtyFlag ;
    }

    /**
     * 获取 [PRICE_PER_LITER]
     */
    @JsonProperty("price_per_liter")
    public Double getPrice_per_liter(){
        return price_per_liter ;
    }

    /**
     * 设置 [PRICE_PER_LITER]
     */
    @JsonProperty("price_per_liter")
    public void setPrice_per_liter(Double  price_per_liter){
        this.price_per_liter = price_per_liter ;
        this.price_per_literDirtyFlag = true ;
    }

    /**
     * 获取 [PRICE_PER_LITER]脏标记
     */
    @JsonIgnore
    public boolean getPrice_per_literDirtyFlag(){
        return price_per_literDirtyFlag ;
    }

    /**
     * 获取 [COST_IDS]
     */
    @JsonProperty("cost_ids")
    public String getCost_ids(){
        return cost_ids ;
    }

    /**
     * 设置 [COST_IDS]
     */
    @JsonProperty("cost_ids")
    public void setCost_ids(String  cost_ids){
        this.cost_ids = cost_ids ;
        this.cost_idsDirtyFlag = true ;
    }

    /**
     * 获取 [COST_IDS]脏标记
     */
    @JsonIgnore
    public boolean getCost_idsDirtyFlag(){
        return cost_idsDirtyFlag ;
    }

    /**
     * 获取 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return display_name ;
    }

    /**
     * 设置 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

    /**
     * 获取 [DISPLAY_NAME]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return display_nameDirtyFlag ;
    }

    /**
     * 获取 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return write_date ;
    }

    /**
     * 设置 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return write_dateDirtyFlag ;
    }

    /**
     * 获取 [LITER]
     */
    @JsonProperty("liter")
    public Double getLiter(){
        return liter ;
    }

    /**
     * 设置 [LITER]
     */
    @JsonProperty("liter")
    public void setLiter(Double  liter){
        this.liter = liter ;
        this.literDirtyFlag = true ;
    }

    /**
     * 获取 [LITER]脏标记
     */
    @JsonIgnore
    public boolean getLiterDirtyFlag(){
        return literDirtyFlag ;
    }

    /**
     * 获取 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return __last_update ;
    }

    /**
     * 设置 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

    /**
     * 获取 [__LAST_UPDATE]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return __last_updateDirtyFlag ;
    }

    /**
     * 获取 [NOTES]
     */
    @JsonProperty("notes")
    public String getNotes(){
        return notes ;
    }

    /**
     * 设置 [NOTES]
     */
    @JsonProperty("notes")
    public void setNotes(String  notes){
        this.notes = notes ;
        this.notesDirtyFlag = true ;
    }

    /**
     * 获取 [NOTES]脏标记
     */
    @JsonIgnore
    public boolean getNotesDirtyFlag(){
        return notesDirtyFlag ;
    }

    /**
     * 获取 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return create_date ;
    }

    /**
     * 设置 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return create_dateDirtyFlag ;
    }

    /**
     * 获取 [INV_REF]
     */
    @JsonProperty("inv_ref")
    public String getInv_ref(){
        return inv_ref ;
    }

    /**
     * 设置 [INV_REF]
     */
    @JsonProperty("inv_ref")
    public void setInv_ref(String  inv_ref){
        this.inv_ref = inv_ref ;
        this.inv_refDirtyFlag = true ;
    }

    /**
     * 获取 [INV_REF]脏标记
     */
    @JsonIgnore
    public boolean getInv_refDirtyFlag(){
        return inv_refDirtyFlag ;
    }

    /**
     * 获取 [COST_SUBTYPE_ID]
     */
    @JsonProperty("cost_subtype_id")
    public Integer getCost_subtype_id(){
        return cost_subtype_id ;
    }

    /**
     * 设置 [COST_SUBTYPE_ID]
     */
    @JsonProperty("cost_subtype_id")
    public void setCost_subtype_id(Integer  cost_subtype_id){
        this.cost_subtype_id = cost_subtype_id ;
        this.cost_subtype_idDirtyFlag = true ;
    }

    /**
     * 获取 [COST_SUBTYPE_ID]脏标记
     */
    @JsonIgnore
    public boolean getCost_subtype_idDirtyFlag(){
        return cost_subtype_idDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return create_uid_text ;
    }

    /**
     * 设置 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return create_uid_textDirtyFlag ;
    }

    /**
     * 获取 [ODOMETER_ID]
     */
    @JsonProperty("odometer_id")
    public Integer getOdometer_id(){
        return odometer_id ;
    }

    /**
     * 设置 [ODOMETER_ID]
     */
    @JsonProperty("odometer_id")
    public void setOdometer_id(Integer  odometer_id){
        this.odometer_id = odometer_id ;
        this.odometer_idDirtyFlag = true ;
    }

    /**
     * 获取 [ODOMETER_ID]脏标记
     */
    @JsonIgnore
    public boolean getOdometer_idDirtyFlag(){
        return odometer_idDirtyFlag ;
    }

    /**
     * 获取 [COST_TYPE]
     */
    @JsonProperty("cost_type")
    public String getCost_type(){
        return cost_type ;
    }

    /**
     * 设置 [COST_TYPE]
     */
    @JsonProperty("cost_type")
    public void setCost_type(String  cost_type){
        this.cost_type = cost_type ;
        this.cost_typeDirtyFlag = true ;
    }

    /**
     * 获取 [COST_TYPE]脏标记
     */
    @JsonIgnore
    public boolean getCost_typeDirtyFlag(){
        return cost_typeDirtyFlag ;
    }

    /**
     * 获取 [AMOUNT]
     */
    @JsonProperty("amount")
    public Double getAmount(){
        return amount ;
    }

    /**
     * 设置 [AMOUNT]
     */
    @JsonProperty("amount")
    public void setAmount(Double  amount){
        this.amount = amount ;
        this.amountDirtyFlag = true ;
    }

    /**
     * 获取 [AMOUNT]脏标记
     */
    @JsonIgnore
    public boolean getAmountDirtyFlag(){
        return amountDirtyFlag ;
    }

    /**
     * 获取 [PURCHASER_ID_TEXT]
     */
    @JsonProperty("purchaser_id_text")
    public String getPurchaser_id_text(){
        return purchaser_id_text ;
    }

    /**
     * 设置 [PURCHASER_ID_TEXT]
     */
    @JsonProperty("purchaser_id_text")
    public void setPurchaser_id_text(String  purchaser_id_text){
        this.purchaser_id_text = purchaser_id_text ;
        this.purchaser_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [PURCHASER_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getPurchaser_id_textDirtyFlag(){
        return purchaser_id_textDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return write_uid_text ;
    }

    /**
     * 设置 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return write_uid_textDirtyFlag ;
    }

    /**
     * 获取 [COST_AMOUNT]
     */
    @JsonProperty("cost_amount")
    public Double getCost_amount(){
        return cost_amount ;
    }

    /**
     * 设置 [COST_AMOUNT]
     */
    @JsonProperty("cost_amount")
    public void setCost_amount(Double  cost_amount){
        this.cost_amount = cost_amount ;
        this.cost_amountDirtyFlag = true ;
    }

    /**
     * 获取 [COST_AMOUNT]脏标记
     */
    @JsonIgnore
    public boolean getCost_amountDirtyFlag(){
        return cost_amountDirtyFlag ;
    }

    /**
     * 获取 [PARENT_ID]
     */
    @JsonProperty("parent_id")
    public Integer getParent_id(){
        return parent_id ;
    }

    /**
     * 设置 [PARENT_ID]
     */
    @JsonProperty("parent_id")
    public void setParent_id(Integer  parent_id){
        this.parent_id = parent_id ;
        this.parent_idDirtyFlag = true ;
    }

    /**
     * 获取 [PARENT_ID]脏标记
     */
    @JsonIgnore
    public boolean getParent_idDirtyFlag(){
        return parent_idDirtyFlag ;
    }

    /**
     * 获取 [VEHICLE_ID]
     */
    @JsonProperty("vehicle_id")
    public Integer getVehicle_id(){
        return vehicle_id ;
    }

    /**
     * 设置 [VEHICLE_ID]
     */
    @JsonProperty("vehicle_id")
    public void setVehicle_id(Integer  vehicle_id){
        this.vehicle_id = vehicle_id ;
        this.vehicle_idDirtyFlag = true ;
    }

    /**
     * 获取 [VEHICLE_ID]脏标记
     */
    @JsonIgnore
    public boolean getVehicle_idDirtyFlag(){
        return vehicle_idDirtyFlag ;
    }

    /**
     * 获取 [AUTO_GENERATED]
     */
    @JsonProperty("auto_generated")
    public String getAuto_generated(){
        return auto_generated ;
    }

    /**
     * 设置 [AUTO_GENERATED]
     */
    @JsonProperty("auto_generated")
    public void setAuto_generated(String  auto_generated){
        this.auto_generated = auto_generated ;
        this.auto_generatedDirtyFlag = true ;
    }

    /**
     * 获取 [AUTO_GENERATED]脏标记
     */
    @JsonIgnore
    public boolean getAuto_generatedDirtyFlag(){
        return auto_generatedDirtyFlag ;
    }

    /**
     * 获取 [CONTRACT_ID]
     */
    @JsonProperty("contract_id")
    public Integer getContract_id(){
        return contract_id ;
    }

    /**
     * 设置 [CONTRACT_ID]
     */
    @JsonProperty("contract_id")
    public void setContract_id(Integer  contract_id){
        this.contract_id = contract_id ;
        this.contract_idDirtyFlag = true ;
    }

    /**
     * 获取 [CONTRACT_ID]脏标记
     */
    @JsonIgnore
    public boolean getContract_idDirtyFlag(){
        return contract_idDirtyFlag ;
    }

    /**
     * 获取 [DESCRIPTION]
     */
    @JsonProperty("description")
    public String getDescription(){
        return description ;
    }

    /**
     * 设置 [DESCRIPTION]
     */
    @JsonProperty("description")
    public void setDescription(String  description){
        this.description = description ;
        this.descriptionDirtyFlag = true ;
    }

    /**
     * 获取 [DESCRIPTION]脏标记
     */
    @JsonIgnore
    public boolean getDescriptionDirtyFlag(){
        return descriptionDirtyFlag ;
    }

    /**
     * 获取 [NAME]
     */
    @JsonProperty("name")
    public String getName(){
        return name ;
    }

    /**
     * 设置 [NAME]
     */
    @JsonProperty("name")
    public void setName(String  name){
        this.name = name ;
        this.nameDirtyFlag = true ;
    }

    /**
     * 获取 [NAME]脏标记
     */
    @JsonIgnore
    public boolean getNameDirtyFlag(){
        return nameDirtyFlag ;
    }

    /**
     * 获取 [VENDOR_ID_TEXT]
     */
    @JsonProperty("vendor_id_text")
    public String getVendor_id_text(){
        return vendor_id_text ;
    }

    /**
     * 设置 [VENDOR_ID_TEXT]
     */
    @JsonProperty("vendor_id_text")
    public void setVendor_id_text(String  vendor_id_text){
        this.vendor_id_text = vendor_id_text ;
        this.vendor_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [VENDOR_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getVendor_id_textDirtyFlag(){
        return vendor_id_textDirtyFlag ;
    }

    /**
     * 获取 [ODOMETER]
     */
    @JsonProperty("odometer")
    public Double getOdometer(){
        return odometer ;
    }

    /**
     * 设置 [ODOMETER]
     */
    @JsonProperty("odometer")
    public void setOdometer(Double  odometer){
        this.odometer = odometer ;
        this.odometerDirtyFlag = true ;
    }

    /**
     * 获取 [ODOMETER]脏标记
     */
    @JsonIgnore
    public boolean getOdometerDirtyFlag(){
        return odometerDirtyFlag ;
    }

    /**
     * 获取 [DATE]
     */
    @JsonProperty("date")
    public Timestamp getDate(){
        return date ;
    }

    /**
     * 设置 [DATE]
     */
    @JsonProperty("date")
    public void setDate(Timestamp  date){
        this.date = date ;
        this.dateDirtyFlag = true ;
    }

    /**
     * 获取 [DATE]脏标记
     */
    @JsonIgnore
    public boolean getDateDirtyFlag(){
        return dateDirtyFlag ;
    }

    /**
     * 获取 [ODOMETER_UNIT]
     */
    @JsonProperty("odometer_unit")
    public String getOdometer_unit(){
        return odometer_unit ;
    }

    /**
     * 设置 [ODOMETER_UNIT]
     */
    @JsonProperty("odometer_unit")
    public void setOdometer_unit(String  odometer_unit){
        this.odometer_unit = odometer_unit ;
        this.odometer_unitDirtyFlag = true ;
    }

    /**
     * 获取 [ODOMETER_UNIT]脏标记
     */
    @JsonIgnore
    public boolean getOdometer_unitDirtyFlag(){
        return odometer_unitDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return write_uid ;
    }

    /**
     * 设置 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return write_uidDirtyFlag ;
    }

    /**
     * 获取 [COST_ID]
     */
    @JsonProperty("cost_id")
    public Integer getCost_id(){
        return cost_id ;
    }

    /**
     * 设置 [COST_ID]
     */
    @JsonProperty("cost_id")
    public void setCost_id(Integer  cost_id){
        this.cost_id = cost_id ;
        this.cost_idDirtyFlag = true ;
    }

    /**
     * 获取 [COST_ID]脏标记
     */
    @JsonIgnore
    public boolean getCost_idDirtyFlag(){
        return cost_idDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return create_uid ;
    }

    /**
     * 设置 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return create_uidDirtyFlag ;
    }

    /**
     * 获取 [VENDOR_ID]
     */
    @JsonProperty("vendor_id")
    public Integer getVendor_id(){
        return vendor_id ;
    }

    /**
     * 设置 [VENDOR_ID]
     */
    @JsonProperty("vendor_id")
    public void setVendor_id(Integer  vendor_id){
        this.vendor_id = vendor_id ;
        this.vendor_idDirtyFlag = true ;
    }

    /**
     * 获取 [VENDOR_ID]脏标记
     */
    @JsonIgnore
    public boolean getVendor_idDirtyFlag(){
        return vendor_idDirtyFlag ;
    }

    /**
     * 获取 [PURCHASER_ID]
     */
    @JsonProperty("purchaser_id")
    public Integer getPurchaser_id(){
        return purchaser_id ;
    }

    /**
     * 设置 [PURCHASER_ID]
     */
    @JsonProperty("purchaser_id")
    public void setPurchaser_id(Integer  purchaser_id){
        this.purchaser_id = purchaser_id ;
        this.purchaser_idDirtyFlag = true ;
    }

    /**
     * 获取 [PURCHASER_ID]脏标记
     */
    @JsonIgnore
    public boolean getPurchaser_idDirtyFlag(){
        return purchaser_idDirtyFlag ;
    }



    public Fleet_vehicle_log_fuel toDO() {
        Fleet_vehicle_log_fuel srfdomain = new Fleet_vehicle_log_fuel();
        if(getIdDirtyFlag())
            srfdomain.setId(id);
        if(getPrice_per_literDirtyFlag())
            srfdomain.setPrice_per_liter(price_per_liter);
        if(getCost_idsDirtyFlag())
            srfdomain.setCost_ids(cost_ids);
        if(getDisplay_nameDirtyFlag())
            srfdomain.setDisplay_name(display_name);
        if(getWrite_dateDirtyFlag())
            srfdomain.setWrite_date(write_date);
        if(getLiterDirtyFlag())
            srfdomain.setLiter(liter);
        if(get__last_updateDirtyFlag())
            srfdomain.set__last_update(__last_update);
        if(getNotesDirtyFlag())
            srfdomain.setNotes(notes);
        if(getCreate_dateDirtyFlag())
            srfdomain.setCreate_date(create_date);
        if(getInv_refDirtyFlag())
            srfdomain.setInv_ref(inv_ref);
        if(getCost_subtype_idDirtyFlag())
            srfdomain.setCost_subtype_id(cost_subtype_id);
        if(getCreate_uid_textDirtyFlag())
            srfdomain.setCreate_uid_text(create_uid_text);
        if(getOdometer_idDirtyFlag())
            srfdomain.setOdometer_id(odometer_id);
        if(getCost_typeDirtyFlag())
            srfdomain.setCost_type(cost_type);
        if(getAmountDirtyFlag())
            srfdomain.setAmount(amount);
        if(getPurchaser_id_textDirtyFlag())
            srfdomain.setPurchaser_id_text(purchaser_id_text);
        if(getWrite_uid_textDirtyFlag())
            srfdomain.setWrite_uid_text(write_uid_text);
        if(getCost_amountDirtyFlag())
            srfdomain.setCost_amount(cost_amount);
        if(getParent_idDirtyFlag())
            srfdomain.setParent_id(parent_id);
        if(getVehicle_idDirtyFlag())
            srfdomain.setVehicle_id(vehicle_id);
        if(getAuto_generatedDirtyFlag())
            srfdomain.setAuto_generated(auto_generated);
        if(getContract_idDirtyFlag())
            srfdomain.setContract_id(contract_id);
        if(getDescriptionDirtyFlag())
            srfdomain.setDescription(description);
        if(getNameDirtyFlag())
            srfdomain.setName(name);
        if(getVendor_id_textDirtyFlag())
            srfdomain.setVendor_id_text(vendor_id_text);
        if(getOdometerDirtyFlag())
            srfdomain.setOdometer(odometer);
        if(getDateDirtyFlag())
            srfdomain.setDate(date);
        if(getOdometer_unitDirtyFlag())
            srfdomain.setOdometer_unit(odometer_unit);
        if(getWrite_uidDirtyFlag())
            srfdomain.setWrite_uid(write_uid);
        if(getCost_idDirtyFlag())
            srfdomain.setCost_id(cost_id);
        if(getCreate_uidDirtyFlag())
            srfdomain.setCreate_uid(create_uid);
        if(getVendor_idDirtyFlag())
            srfdomain.setVendor_id(vendor_id);
        if(getPurchaser_idDirtyFlag())
            srfdomain.setPurchaser_id(purchaser_id);

        return srfdomain;
    }

    public void fromDO(Fleet_vehicle_log_fuel srfdomain) {
        if(srfdomain == null )
          return ;
        if(srfdomain.getIdDirtyFlag())
            this.setId(srfdomain.getId());
        if(srfdomain.getPrice_per_literDirtyFlag())
            this.setPrice_per_liter(srfdomain.getPrice_per_liter());
        if(srfdomain.getCost_idsDirtyFlag())
            this.setCost_ids(srfdomain.getCost_ids());
        if(srfdomain.getDisplay_nameDirtyFlag())
            this.setDisplay_name(srfdomain.getDisplay_name());
        if(srfdomain.getWrite_dateDirtyFlag())
            this.setWrite_date(srfdomain.getWrite_date());
        if(srfdomain.getLiterDirtyFlag())
            this.setLiter(srfdomain.getLiter());
        if(srfdomain.get__last_updateDirtyFlag())
            this.set__last_update(srfdomain.get__last_update());
        if(srfdomain.getNotesDirtyFlag())
            this.setNotes(srfdomain.getNotes());
        if(srfdomain.getCreate_dateDirtyFlag())
            this.setCreate_date(srfdomain.getCreate_date());
        if(srfdomain.getInv_refDirtyFlag())
            this.setInv_ref(srfdomain.getInv_ref());
        if(srfdomain.getCost_subtype_idDirtyFlag())
            this.setCost_subtype_id(srfdomain.getCost_subtype_id());
        if(srfdomain.getCreate_uid_textDirtyFlag())
            this.setCreate_uid_text(srfdomain.getCreate_uid_text());
        if(srfdomain.getOdometer_idDirtyFlag())
            this.setOdometer_id(srfdomain.getOdometer_id());
        if(srfdomain.getCost_typeDirtyFlag())
            this.setCost_type(srfdomain.getCost_type());
        if(srfdomain.getAmountDirtyFlag())
            this.setAmount(srfdomain.getAmount());
        if(srfdomain.getPurchaser_id_textDirtyFlag())
            this.setPurchaser_id_text(srfdomain.getPurchaser_id_text());
        if(srfdomain.getWrite_uid_textDirtyFlag())
            this.setWrite_uid_text(srfdomain.getWrite_uid_text());
        if(srfdomain.getCost_amountDirtyFlag())
            this.setCost_amount(srfdomain.getCost_amount());
        if(srfdomain.getParent_idDirtyFlag())
            this.setParent_id(srfdomain.getParent_id());
        if(srfdomain.getVehicle_idDirtyFlag())
            this.setVehicle_id(srfdomain.getVehicle_id());
        if(srfdomain.getAuto_generatedDirtyFlag())
            this.setAuto_generated(srfdomain.getAuto_generated());
        if(srfdomain.getContract_idDirtyFlag())
            this.setContract_id(srfdomain.getContract_id());
        if(srfdomain.getDescriptionDirtyFlag())
            this.setDescription(srfdomain.getDescription());
        if(srfdomain.getNameDirtyFlag())
            this.setName(srfdomain.getName());
        if(srfdomain.getVendor_id_textDirtyFlag())
            this.setVendor_id_text(srfdomain.getVendor_id_text());
        if(srfdomain.getOdometerDirtyFlag())
            this.setOdometer(srfdomain.getOdometer());
        if(srfdomain.getDateDirtyFlag())
            this.setDate(srfdomain.getDate());
        if(srfdomain.getOdometer_unitDirtyFlag())
            this.setOdometer_unit(srfdomain.getOdometer_unit());
        if(srfdomain.getWrite_uidDirtyFlag())
            this.setWrite_uid(srfdomain.getWrite_uid());
        if(srfdomain.getCost_idDirtyFlag())
            this.setCost_id(srfdomain.getCost_id());
        if(srfdomain.getCreate_uidDirtyFlag())
            this.setCreate_uid(srfdomain.getCreate_uid());
        if(srfdomain.getVendor_idDirtyFlag())
            this.setVendor_id(srfdomain.getVendor_id());
        if(srfdomain.getPurchaser_idDirtyFlag())
            this.setPurchaser_id(srfdomain.getPurchaser_id());

    }

    public List<Fleet_vehicle_log_fuelDTO> fromDOPage(List<Fleet_vehicle_log_fuel> poPage)   {
        if(poPage == null)
            return null;
        List<Fleet_vehicle_log_fuelDTO> dtos=new ArrayList<Fleet_vehicle_log_fuelDTO>();
        for(Fleet_vehicle_log_fuel domain : poPage) {
            Fleet_vehicle_log_fuelDTO dto = new Fleet_vehicle_log_fuelDTO();
            dto.fromDO(domain);
            dtos.add(dto);
        }
        return dtos;
    }
}

