package cn.ibizlab.odoo.service.odoo_fleet.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.core.odoo_fleet.valuerule.anno.fleet_vehicle.*;
import cn.ibizlab.odoo.core.odoo_fleet.domain.Fleet_vehicle;
import org.springframework.cglib.beans.BeanCopier;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 服务DTO对象[Fleet_vehicleDTO]
 */
public class Fleet_vehicleDTO implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [FUEL_TYPE]
     *
     */
    @Fleet_vehicleFuel_typeDefault(info = "默认规则")
    private String fuel_type;

    @JsonIgnore
    private boolean fuel_typeDirtyFlag;

    /**
     * 属性 [TAG_IDS]
     *
     */
    @Fleet_vehicleTag_idsDefault(info = "默认规则")
    private String tag_ids;

    @JsonIgnore
    private boolean tag_idsDirtyFlag;

    /**
     * 属性 [ACTIVITY_IDS]
     *
     */
    @Fleet_vehicleActivity_idsDefault(info = "默认规则")
    private String activity_ids;

    @JsonIgnore
    private boolean activity_idsDirtyFlag;

    /**
     * 属性 [MESSAGE_UNREAD]
     *
     */
    @Fleet_vehicleMessage_unreadDefault(info = "默认规则")
    private String message_unread;

    @JsonIgnore
    private boolean message_unreadDirtyFlag;

    /**
     * 属性 [WEBSITE_MESSAGE_IDS]
     *
     */
    @Fleet_vehicleWebsite_message_idsDefault(info = "默认规则")
    private String website_message_ids;

    @JsonIgnore
    private boolean website_message_idsDirtyFlag;

    /**
     * 属性 [POWER]
     *
     */
    @Fleet_vehiclePowerDefault(info = "默认规则")
    private Integer power;

    @JsonIgnore
    private boolean powerDirtyFlag;

    /**
     * 属性 [MESSAGE_NEEDACTION]
     *
     */
    @Fleet_vehicleMessage_needactionDefault(info = "默认规则")
    private String message_needaction;

    @JsonIgnore
    private boolean message_needactionDirtyFlag;

    /**
     * 属性 [RESIDUAL_VALUE]
     *
     */
    @Fleet_vehicleResidual_valueDefault(info = "默认规则")
    private Double residual_value;

    @JsonIgnore
    private boolean residual_valueDirtyFlag;

    /**
     * 属性 [ODOMETER]
     *
     */
    @Fleet_vehicleOdometerDefault(info = "默认规则")
    private Double odometer;

    @JsonIgnore
    private boolean odometerDirtyFlag;

    /**
     * 属性 [MESSAGE_CHANNEL_IDS]
     *
     */
    @Fleet_vehicleMessage_channel_idsDefault(info = "默认规则")
    private String message_channel_ids;

    @JsonIgnore
    private boolean message_channel_idsDirtyFlag;

    /**
     * 属性 [FUEL_LOGS_COUNT]
     *
     */
    @Fleet_vehicleFuel_logs_countDefault(info = "默认规则")
    private Integer fuel_logs_count;

    @JsonIgnore
    private boolean fuel_logs_countDirtyFlag;

    /**
     * 属性 [MESSAGE_FOLLOWER_IDS]
     *
     */
    @Fleet_vehicleMessage_follower_idsDefault(info = "默认规则")
    private String message_follower_ids;

    @JsonIgnore
    private boolean message_follower_idsDirtyFlag;

    /**
     * 属性 [MODEL_YEAR]
     *
     */
    @Fleet_vehicleModel_yearDefault(info = "默认规则")
    private String model_year;

    @JsonIgnore
    private boolean model_yearDirtyFlag;

    /**
     * 属性 [CAR_VALUE]
     *
     */
    @Fleet_vehicleCar_valueDefault(info = "默认规则")
    private Double car_value;

    @JsonIgnore
    private boolean car_valueDirtyFlag;

    /**
     * 属性 [LICENSE_PLATE]
     *
     */
    @Fleet_vehicleLicense_plateDefault(info = "默认规则")
    private String license_plate;

    @JsonIgnore
    private boolean license_plateDirtyFlag;

    /**
     * 属性 [CONTRACT_RENEWAL_OVERDUE]
     *
     */
    @Fleet_vehicleContract_renewal_overdueDefault(info = "默认规则")
    private String contract_renewal_overdue;

    @JsonIgnore
    private boolean contract_renewal_overdueDirtyFlag;

    /**
     * 属性 [MESSAGE_UNREAD_COUNTER]
     *
     */
    @Fleet_vehicleMessage_unread_counterDefault(info = "默认规则")
    private Integer message_unread_counter;

    @JsonIgnore
    private boolean message_unread_counterDirtyFlag;

    /**
     * 属性 [ACQUISITION_DATE]
     *
     */
    @Fleet_vehicleAcquisition_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    private Timestamp acquisition_date;

    @JsonIgnore
    private boolean acquisition_dateDirtyFlag;

    /**
     * 属性 [ACTIVITY_DATE_DEADLINE]
     *
     */
    @Fleet_vehicleActivity_date_deadlineDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    private Timestamp activity_date_deadline;

    @JsonIgnore
    private boolean activity_date_deadlineDirtyFlag;

    /**
     * 属性 [WRITE_DATE]
     *
     */
    @Fleet_vehicleWrite_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp write_date;

    @JsonIgnore
    private boolean write_dateDirtyFlag;

    /**
     * 属性 [ACTIVITY_USER_ID]
     *
     */
    @Fleet_vehicleActivity_user_idDefault(info = "默认规则")
    private Integer activity_user_id;

    @JsonIgnore
    private boolean activity_user_idDirtyFlag;

    /**
     * 属性 [ACTIVITY_TYPE_ID]
     *
     */
    @Fleet_vehicleActivity_type_idDefault(info = "默认规则")
    private Integer activity_type_id;

    @JsonIgnore
    private boolean activity_type_idDirtyFlag;

    /**
     * 属性 [HORSEPOWER_TAX]
     *
     */
    @Fleet_vehicleHorsepower_taxDefault(info = "默认规则")
    private Double horsepower_tax;

    @JsonIgnore
    private boolean horsepower_taxDirtyFlag;

    /**
     * 属性 [LOCATION]
     *
     */
    @Fleet_vehicleLocationDefault(info = "默认规则")
    private String location;

    @JsonIgnore
    private boolean locationDirtyFlag;

    /**
     * 属性 [MESSAGE_HAS_ERROR_COUNTER]
     *
     */
    @Fleet_vehicleMessage_has_error_counterDefault(info = "默认规则")
    private Integer message_has_error_counter;

    @JsonIgnore
    private boolean message_has_error_counterDirtyFlag;

    /**
     * 属性 [MESSAGE_IDS]
     *
     */
    @Fleet_vehicleMessage_idsDefault(info = "默认规则")
    private String message_ids;

    @JsonIgnore
    private boolean message_idsDirtyFlag;

    /**
     * 属性 [MESSAGE_IS_FOLLOWER]
     *
     */
    @Fleet_vehicleMessage_is_followerDefault(info = "默认规则")
    private String message_is_follower;

    @JsonIgnore
    private boolean message_is_followerDirtyFlag;

    /**
     * 属性 [ACTIVE]
     *
     */
    @Fleet_vehicleActiveDefault(info = "默认规则")
    private String active;

    @JsonIgnore
    private boolean activeDirtyFlag;

    /**
     * 属性 [HORSEPOWER]
     *
     */
    @Fleet_vehicleHorsepowerDefault(info = "默认规则")
    private Integer horsepower;

    @JsonIgnore
    private boolean horsepowerDirtyFlag;

    /**
     * 属性 [VIN_SN]
     *
     */
    @Fleet_vehicleVin_snDefault(info = "默认规则")
    private String vin_sn;

    @JsonIgnore
    private boolean vin_snDirtyFlag;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @Fleet_vehicleDisplay_nameDefault(info = "默认规则")
    private String display_name;

    @JsonIgnore
    private boolean display_nameDirtyFlag;

    /**
     * 属性 [MESSAGE_HAS_ERROR]
     *
     */
    @Fleet_vehicleMessage_has_errorDefault(info = "默认规则")
    private String message_has_error;

    @JsonIgnore
    private boolean message_has_errorDirtyFlag;

    /**
     * 属性 [ODOMETER_COUNT]
     *
     */
    @Fleet_vehicleOdometer_countDefault(info = "默认规则")
    private Integer odometer_count;

    @JsonIgnore
    private boolean odometer_countDirtyFlag;

    /**
     * 属性 [MESSAGE_PARTNER_IDS]
     *
     */
    @Fleet_vehicleMessage_partner_idsDefault(info = "默认规则")
    private String message_partner_ids;

    @JsonIgnore
    private boolean message_partner_idsDirtyFlag;

    /**
     * 属性 [MESSAGE_NEEDACTION_COUNTER]
     *
     */
    @Fleet_vehicleMessage_needaction_counterDefault(info = "默认规则")
    private Integer message_needaction_counter;

    @JsonIgnore
    private boolean message_needaction_counterDirtyFlag;

    /**
     * 属性 [CONTRACT_RENEWAL_NAME]
     *
     */
    @Fleet_vehicleContract_renewal_nameDefault(info = "默认规则")
    private String contract_renewal_name;

    @JsonIgnore
    private boolean contract_renewal_nameDirtyFlag;

    /**
     * 属性 [CREATE_DATE]
     *
     */
    @Fleet_vehicleCreate_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp create_date;

    @JsonIgnore
    private boolean create_dateDirtyFlag;

    /**
     * 属性 [ACTIVITY_SUMMARY]
     *
     */
    @Fleet_vehicleActivity_summaryDefault(info = "默认规则")
    private String activity_summary;

    @JsonIgnore
    private boolean activity_summaryDirtyFlag;

    /**
     * 属性 [SEATS]
     *
     */
    @Fleet_vehicleSeatsDefault(info = "默认规则")
    private Integer seats;

    @JsonIgnore
    private boolean seatsDirtyFlag;

    /**
     * 属性 [LOG_FUEL]
     *
     */
    @Fleet_vehicleLog_fuelDefault(info = "默认规则")
    private String log_fuel;

    @JsonIgnore
    private boolean log_fuelDirtyFlag;

    /**
     * 属性 [COST_COUNT]
     *
     */
    @Fleet_vehicleCost_countDefault(info = "默认规则")
    private Integer cost_count;

    @JsonIgnore
    private boolean cost_countDirtyFlag;

    /**
     * 属性 [LOG_DRIVERS]
     *
     */
    @Fleet_vehicleLog_driversDefault(info = "默认规则")
    private String log_drivers;

    @JsonIgnore
    private boolean log_driversDirtyFlag;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @Fleet_vehicle__last_updateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp __last_update;

    @JsonIgnore
    private boolean __last_updateDirtyFlag;

    /**
     * 属性 [CONTRACT_RENEWAL_DUE_SOON]
     *
     */
    @Fleet_vehicleContract_renewal_due_soonDefault(info = "默认规则")
    private String contract_renewal_due_soon;

    @JsonIgnore
    private boolean contract_renewal_due_soonDirtyFlag;

    /**
     * 属性 [ODOMETER_UNIT]
     *
     */
    @Fleet_vehicleOdometer_unitDefault(info = "默认规则")
    private String odometer_unit;

    @JsonIgnore
    private boolean odometer_unitDirtyFlag;

    /**
     * 属性 [CO2]
     *
     */
    @Fleet_vehicleCo2Default(info = "默认规则")
    private Double co2;

    @JsonIgnore
    private boolean co2DirtyFlag;

    /**
     * 属性 [MESSAGE_MAIN_ATTACHMENT_ID]
     *
     */
    @Fleet_vehicleMessage_main_attachment_idDefault(info = "默认规则")
    private Integer message_main_attachment_id;

    @JsonIgnore
    private boolean message_main_attachment_idDirtyFlag;

    /**
     * 属性 [LOG_CONTRACTS]
     *
     */
    @Fleet_vehicleLog_contractsDefault(info = "默认规则")
    private String log_contracts;

    @JsonIgnore
    private boolean log_contractsDirtyFlag;

    /**
     * 属性 [FIRST_CONTRACT_DATE]
     *
     */
    @Fleet_vehicleFirst_contract_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    private Timestamp first_contract_date;

    @JsonIgnore
    private boolean first_contract_dateDirtyFlag;

    /**
     * 属性 [ACTIVITY_STATE]
     *
     */
    @Fleet_vehicleActivity_stateDefault(info = "默认规则")
    private String activity_state;

    @JsonIgnore
    private boolean activity_stateDirtyFlag;

    /**
     * 属性 [COLOR]
     *
     */
    @Fleet_vehicleColorDefault(info = "默认规则")
    private String color;

    @JsonIgnore
    private boolean colorDirtyFlag;

    /**
     * 属性 [DOORS]
     *
     */
    @Fleet_vehicleDoorsDefault(info = "默认规则")
    private Integer doors;

    @JsonIgnore
    private boolean doorsDirtyFlag;

    /**
     * 属性 [ID]
     *
     */
    @Fleet_vehicleIdDefault(info = "默认规则")
    private Integer id;

    @JsonIgnore
    private boolean idDirtyFlag;

    /**
     * 属性 [MESSAGE_ATTACHMENT_COUNT]
     *
     */
    @Fleet_vehicleMessage_attachment_countDefault(info = "默认规则")
    private Integer message_attachment_count;

    @JsonIgnore
    private boolean message_attachment_countDirtyFlag;

    /**
     * 属性 [NAME]
     *
     */
    @Fleet_vehicleNameDefault(info = "默认规则")
    private String name;

    @JsonIgnore
    private boolean nameDirtyFlag;

    /**
     * 属性 [LOG_SERVICES]
     *
     */
    @Fleet_vehicleLog_servicesDefault(info = "默认规则")
    private String log_services;

    @JsonIgnore
    private boolean log_servicesDirtyFlag;

    /**
     * 属性 [CONTRACT_COUNT]
     *
     */
    @Fleet_vehicleContract_countDefault(info = "默认规则")
    private Integer contract_count;

    @JsonIgnore
    private boolean contract_countDirtyFlag;

    /**
     * 属性 [TRANSMISSION]
     *
     */
    @Fleet_vehicleTransmissionDefault(info = "默认规则")
    private String transmission;

    @JsonIgnore
    private boolean transmissionDirtyFlag;

    /**
     * 属性 [SERVICE_COUNT]
     *
     */
    @Fleet_vehicleService_countDefault(info = "默认规则")
    private Integer service_count;

    @JsonIgnore
    private boolean service_countDirtyFlag;

    /**
     * 属性 [CONTRACT_RENEWAL_TOTAL]
     *
     */
    @Fleet_vehicleContract_renewal_totalDefault(info = "默认规则")
    private String contract_renewal_total;

    @JsonIgnore
    private boolean contract_renewal_totalDirtyFlag;

    /**
     * 属性 [COMPANY_ID_TEXT]
     *
     */
    @Fleet_vehicleCompany_id_textDefault(info = "默认规则")
    private String company_id_text;

    @JsonIgnore
    private boolean company_id_textDirtyFlag;

    /**
     * 属性 [WRITE_UID_TEXT]
     *
     */
    @Fleet_vehicleWrite_uid_textDefault(info = "默认规则")
    private String write_uid_text;

    @JsonIgnore
    private boolean write_uid_textDirtyFlag;

    /**
     * 属性 [DRIVER_ID_TEXT]
     *
     */
    @Fleet_vehicleDriver_id_textDefault(info = "默认规则")
    private String driver_id_text;

    @JsonIgnore
    private boolean driver_id_textDirtyFlag;

    /**
     * 属性 [CREATE_UID_TEXT]
     *
     */
    @Fleet_vehicleCreate_uid_textDefault(info = "默认规则")
    private String create_uid_text;

    @JsonIgnore
    private boolean create_uid_textDirtyFlag;

    /**
     * 属性 [IMAGE_MEDIUM]
     *
     */
    @Fleet_vehicleImage_mediumDefault(info = "默认规则")
    private byte[] image_medium;

    @JsonIgnore
    private boolean image_mediumDirtyFlag;

    /**
     * 属性 [IMAGE]
     *
     */
    @Fleet_vehicleImageDefault(info = "默认规则")
    private byte[] image;

    @JsonIgnore
    private boolean imageDirtyFlag;

    /**
     * 属性 [MODEL_ID_TEXT]
     *
     */
    @Fleet_vehicleModel_id_textDefault(info = "默认规则")
    private String model_id_text;

    @JsonIgnore
    private boolean model_id_textDirtyFlag;

    /**
     * 属性 [IMAGE_SMALL]
     *
     */
    @Fleet_vehicleImage_smallDefault(info = "默认规则")
    private byte[] image_small;

    @JsonIgnore
    private boolean image_smallDirtyFlag;

    /**
     * 属性 [BRAND_ID_TEXT]
     *
     */
    @Fleet_vehicleBrand_id_textDefault(info = "默认规则")
    private String brand_id_text;

    @JsonIgnore
    private boolean brand_id_textDirtyFlag;

    /**
     * 属性 [STATE_ID_TEXT]
     *
     */
    @Fleet_vehicleState_id_textDefault(info = "默认规则")
    private String state_id_text;

    @JsonIgnore
    private boolean state_id_textDirtyFlag;

    /**
     * 属性 [CREATE_UID]
     *
     */
    @Fleet_vehicleCreate_uidDefault(info = "默认规则")
    private Integer create_uid;

    @JsonIgnore
    private boolean create_uidDirtyFlag;

    /**
     * 属性 [MODEL_ID]
     *
     */
    @Fleet_vehicleModel_idDefault(info = "默认规则")
    private Integer model_id;

    @JsonIgnore
    private boolean model_idDirtyFlag;

    /**
     * 属性 [BRAND_ID]
     *
     */
    @Fleet_vehicleBrand_idDefault(info = "默认规则")
    private Integer brand_id;

    @JsonIgnore
    private boolean brand_idDirtyFlag;

    /**
     * 属性 [COMPANY_ID]
     *
     */
    @Fleet_vehicleCompany_idDefault(info = "默认规则")
    private Integer company_id;

    @JsonIgnore
    private boolean company_idDirtyFlag;

    /**
     * 属性 [STATE_ID]
     *
     */
    @Fleet_vehicleState_idDefault(info = "默认规则")
    private Integer state_id;

    @JsonIgnore
    private boolean state_idDirtyFlag;

    /**
     * 属性 [DRIVER_ID]
     *
     */
    @Fleet_vehicleDriver_idDefault(info = "默认规则")
    private Integer driver_id;

    @JsonIgnore
    private boolean driver_idDirtyFlag;

    /**
     * 属性 [WRITE_UID]
     *
     */
    @Fleet_vehicleWrite_uidDefault(info = "默认规则")
    private Integer write_uid;

    @JsonIgnore
    private boolean write_uidDirtyFlag;


    /**
     * 获取 [FUEL_TYPE]
     */
    @JsonProperty("fuel_type")
    public String getFuel_type(){
        return fuel_type ;
    }

    /**
     * 设置 [FUEL_TYPE]
     */
    @JsonProperty("fuel_type")
    public void setFuel_type(String  fuel_type){
        this.fuel_type = fuel_type ;
        this.fuel_typeDirtyFlag = true ;
    }

    /**
     * 获取 [FUEL_TYPE]脏标记
     */
    @JsonIgnore
    public boolean getFuel_typeDirtyFlag(){
        return fuel_typeDirtyFlag ;
    }

    /**
     * 获取 [TAG_IDS]
     */
    @JsonProperty("tag_ids")
    public String getTag_ids(){
        return tag_ids ;
    }

    /**
     * 设置 [TAG_IDS]
     */
    @JsonProperty("tag_ids")
    public void setTag_ids(String  tag_ids){
        this.tag_ids = tag_ids ;
        this.tag_idsDirtyFlag = true ;
    }

    /**
     * 获取 [TAG_IDS]脏标记
     */
    @JsonIgnore
    public boolean getTag_idsDirtyFlag(){
        return tag_idsDirtyFlag ;
    }

    /**
     * 获取 [ACTIVITY_IDS]
     */
    @JsonProperty("activity_ids")
    public String getActivity_ids(){
        return activity_ids ;
    }

    /**
     * 设置 [ACTIVITY_IDS]
     */
    @JsonProperty("activity_ids")
    public void setActivity_ids(String  activity_ids){
        this.activity_ids = activity_ids ;
        this.activity_idsDirtyFlag = true ;
    }

    /**
     * 获取 [ACTIVITY_IDS]脏标记
     */
    @JsonIgnore
    public boolean getActivity_idsDirtyFlag(){
        return activity_idsDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_UNREAD]
     */
    @JsonProperty("message_unread")
    public String getMessage_unread(){
        return message_unread ;
    }

    /**
     * 设置 [MESSAGE_UNREAD]
     */
    @JsonProperty("message_unread")
    public void setMessage_unread(String  message_unread){
        this.message_unread = message_unread ;
        this.message_unreadDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_UNREAD]脏标记
     */
    @JsonIgnore
    public boolean getMessage_unreadDirtyFlag(){
        return message_unreadDirtyFlag ;
    }

    /**
     * 获取 [WEBSITE_MESSAGE_IDS]
     */
    @JsonProperty("website_message_ids")
    public String getWebsite_message_ids(){
        return website_message_ids ;
    }

    /**
     * 设置 [WEBSITE_MESSAGE_IDS]
     */
    @JsonProperty("website_message_ids")
    public void setWebsite_message_ids(String  website_message_ids){
        this.website_message_ids = website_message_ids ;
        this.website_message_idsDirtyFlag = true ;
    }

    /**
     * 获取 [WEBSITE_MESSAGE_IDS]脏标记
     */
    @JsonIgnore
    public boolean getWebsite_message_idsDirtyFlag(){
        return website_message_idsDirtyFlag ;
    }

    /**
     * 获取 [POWER]
     */
    @JsonProperty("power")
    public Integer getPower(){
        return power ;
    }

    /**
     * 设置 [POWER]
     */
    @JsonProperty("power")
    public void setPower(Integer  power){
        this.power = power ;
        this.powerDirtyFlag = true ;
    }

    /**
     * 获取 [POWER]脏标记
     */
    @JsonIgnore
    public boolean getPowerDirtyFlag(){
        return powerDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_NEEDACTION]
     */
    @JsonProperty("message_needaction")
    public String getMessage_needaction(){
        return message_needaction ;
    }

    /**
     * 设置 [MESSAGE_NEEDACTION]
     */
    @JsonProperty("message_needaction")
    public void setMessage_needaction(String  message_needaction){
        this.message_needaction = message_needaction ;
        this.message_needactionDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_NEEDACTION]脏标记
     */
    @JsonIgnore
    public boolean getMessage_needactionDirtyFlag(){
        return message_needactionDirtyFlag ;
    }

    /**
     * 获取 [RESIDUAL_VALUE]
     */
    @JsonProperty("residual_value")
    public Double getResidual_value(){
        return residual_value ;
    }

    /**
     * 设置 [RESIDUAL_VALUE]
     */
    @JsonProperty("residual_value")
    public void setResidual_value(Double  residual_value){
        this.residual_value = residual_value ;
        this.residual_valueDirtyFlag = true ;
    }

    /**
     * 获取 [RESIDUAL_VALUE]脏标记
     */
    @JsonIgnore
    public boolean getResidual_valueDirtyFlag(){
        return residual_valueDirtyFlag ;
    }

    /**
     * 获取 [ODOMETER]
     */
    @JsonProperty("odometer")
    public Double getOdometer(){
        return odometer ;
    }

    /**
     * 设置 [ODOMETER]
     */
    @JsonProperty("odometer")
    public void setOdometer(Double  odometer){
        this.odometer = odometer ;
        this.odometerDirtyFlag = true ;
    }

    /**
     * 获取 [ODOMETER]脏标记
     */
    @JsonIgnore
    public boolean getOdometerDirtyFlag(){
        return odometerDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_CHANNEL_IDS]
     */
    @JsonProperty("message_channel_ids")
    public String getMessage_channel_ids(){
        return message_channel_ids ;
    }

    /**
     * 设置 [MESSAGE_CHANNEL_IDS]
     */
    @JsonProperty("message_channel_ids")
    public void setMessage_channel_ids(String  message_channel_ids){
        this.message_channel_ids = message_channel_ids ;
        this.message_channel_idsDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_CHANNEL_IDS]脏标记
     */
    @JsonIgnore
    public boolean getMessage_channel_idsDirtyFlag(){
        return message_channel_idsDirtyFlag ;
    }

    /**
     * 获取 [FUEL_LOGS_COUNT]
     */
    @JsonProperty("fuel_logs_count")
    public Integer getFuel_logs_count(){
        return fuel_logs_count ;
    }

    /**
     * 设置 [FUEL_LOGS_COUNT]
     */
    @JsonProperty("fuel_logs_count")
    public void setFuel_logs_count(Integer  fuel_logs_count){
        this.fuel_logs_count = fuel_logs_count ;
        this.fuel_logs_countDirtyFlag = true ;
    }

    /**
     * 获取 [FUEL_LOGS_COUNT]脏标记
     */
    @JsonIgnore
    public boolean getFuel_logs_countDirtyFlag(){
        return fuel_logs_countDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_FOLLOWER_IDS]
     */
    @JsonProperty("message_follower_ids")
    public String getMessage_follower_ids(){
        return message_follower_ids ;
    }

    /**
     * 设置 [MESSAGE_FOLLOWER_IDS]
     */
    @JsonProperty("message_follower_ids")
    public void setMessage_follower_ids(String  message_follower_ids){
        this.message_follower_ids = message_follower_ids ;
        this.message_follower_idsDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_FOLLOWER_IDS]脏标记
     */
    @JsonIgnore
    public boolean getMessage_follower_idsDirtyFlag(){
        return message_follower_idsDirtyFlag ;
    }

    /**
     * 获取 [MODEL_YEAR]
     */
    @JsonProperty("model_year")
    public String getModel_year(){
        return model_year ;
    }

    /**
     * 设置 [MODEL_YEAR]
     */
    @JsonProperty("model_year")
    public void setModel_year(String  model_year){
        this.model_year = model_year ;
        this.model_yearDirtyFlag = true ;
    }

    /**
     * 获取 [MODEL_YEAR]脏标记
     */
    @JsonIgnore
    public boolean getModel_yearDirtyFlag(){
        return model_yearDirtyFlag ;
    }

    /**
     * 获取 [CAR_VALUE]
     */
    @JsonProperty("car_value")
    public Double getCar_value(){
        return car_value ;
    }

    /**
     * 设置 [CAR_VALUE]
     */
    @JsonProperty("car_value")
    public void setCar_value(Double  car_value){
        this.car_value = car_value ;
        this.car_valueDirtyFlag = true ;
    }

    /**
     * 获取 [CAR_VALUE]脏标记
     */
    @JsonIgnore
    public boolean getCar_valueDirtyFlag(){
        return car_valueDirtyFlag ;
    }

    /**
     * 获取 [LICENSE_PLATE]
     */
    @JsonProperty("license_plate")
    public String getLicense_plate(){
        return license_plate ;
    }

    /**
     * 设置 [LICENSE_PLATE]
     */
    @JsonProperty("license_plate")
    public void setLicense_plate(String  license_plate){
        this.license_plate = license_plate ;
        this.license_plateDirtyFlag = true ;
    }

    /**
     * 获取 [LICENSE_PLATE]脏标记
     */
    @JsonIgnore
    public boolean getLicense_plateDirtyFlag(){
        return license_plateDirtyFlag ;
    }

    /**
     * 获取 [CONTRACT_RENEWAL_OVERDUE]
     */
    @JsonProperty("contract_renewal_overdue")
    public String getContract_renewal_overdue(){
        return contract_renewal_overdue ;
    }

    /**
     * 设置 [CONTRACT_RENEWAL_OVERDUE]
     */
    @JsonProperty("contract_renewal_overdue")
    public void setContract_renewal_overdue(String  contract_renewal_overdue){
        this.contract_renewal_overdue = contract_renewal_overdue ;
        this.contract_renewal_overdueDirtyFlag = true ;
    }

    /**
     * 获取 [CONTRACT_RENEWAL_OVERDUE]脏标记
     */
    @JsonIgnore
    public boolean getContract_renewal_overdueDirtyFlag(){
        return contract_renewal_overdueDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_UNREAD_COUNTER]
     */
    @JsonProperty("message_unread_counter")
    public Integer getMessage_unread_counter(){
        return message_unread_counter ;
    }

    /**
     * 设置 [MESSAGE_UNREAD_COUNTER]
     */
    @JsonProperty("message_unread_counter")
    public void setMessage_unread_counter(Integer  message_unread_counter){
        this.message_unread_counter = message_unread_counter ;
        this.message_unread_counterDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_UNREAD_COUNTER]脏标记
     */
    @JsonIgnore
    public boolean getMessage_unread_counterDirtyFlag(){
        return message_unread_counterDirtyFlag ;
    }

    /**
     * 获取 [ACQUISITION_DATE]
     */
    @JsonProperty("acquisition_date")
    public Timestamp getAcquisition_date(){
        return acquisition_date ;
    }

    /**
     * 设置 [ACQUISITION_DATE]
     */
    @JsonProperty("acquisition_date")
    public void setAcquisition_date(Timestamp  acquisition_date){
        this.acquisition_date = acquisition_date ;
        this.acquisition_dateDirtyFlag = true ;
    }

    /**
     * 获取 [ACQUISITION_DATE]脏标记
     */
    @JsonIgnore
    public boolean getAcquisition_dateDirtyFlag(){
        return acquisition_dateDirtyFlag ;
    }

    /**
     * 获取 [ACTIVITY_DATE_DEADLINE]
     */
    @JsonProperty("activity_date_deadline")
    public Timestamp getActivity_date_deadline(){
        return activity_date_deadline ;
    }

    /**
     * 设置 [ACTIVITY_DATE_DEADLINE]
     */
    @JsonProperty("activity_date_deadline")
    public void setActivity_date_deadline(Timestamp  activity_date_deadline){
        this.activity_date_deadline = activity_date_deadline ;
        this.activity_date_deadlineDirtyFlag = true ;
    }

    /**
     * 获取 [ACTIVITY_DATE_DEADLINE]脏标记
     */
    @JsonIgnore
    public boolean getActivity_date_deadlineDirtyFlag(){
        return activity_date_deadlineDirtyFlag ;
    }

    /**
     * 获取 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return write_date ;
    }

    /**
     * 设置 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return write_dateDirtyFlag ;
    }

    /**
     * 获取 [ACTIVITY_USER_ID]
     */
    @JsonProperty("activity_user_id")
    public Integer getActivity_user_id(){
        return activity_user_id ;
    }

    /**
     * 设置 [ACTIVITY_USER_ID]
     */
    @JsonProperty("activity_user_id")
    public void setActivity_user_id(Integer  activity_user_id){
        this.activity_user_id = activity_user_id ;
        this.activity_user_idDirtyFlag = true ;
    }

    /**
     * 获取 [ACTIVITY_USER_ID]脏标记
     */
    @JsonIgnore
    public boolean getActivity_user_idDirtyFlag(){
        return activity_user_idDirtyFlag ;
    }

    /**
     * 获取 [ACTIVITY_TYPE_ID]
     */
    @JsonProperty("activity_type_id")
    public Integer getActivity_type_id(){
        return activity_type_id ;
    }

    /**
     * 设置 [ACTIVITY_TYPE_ID]
     */
    @JsonProperty("activity_type_id")
    public void setActivity_type_id(Integer  activity_type_id){
        this.activity_type_id = activity_type_id ;
        this.activity_type_idDirtyFlag = true ;
    }

    /**
     * 获取 [ACTIVITY_TYPE_ID]脏标记
     */
    @JsonIgnore
    public boolean getActivity_type_idDirtyFlag(){
        return activity_type_idDirtyFlag ;
    }

    /**
     * 获取 [HORSEPOWER_TAX]
     */
    @JsonProperty("horsepower_tax")
    public Double getHorsepower_tax(){
        return horsepower_tax ;
    }

    /**
     * 设置 [HORSEPOWER_TAX]
     */
    @JsonProperty("horsepower_tax")
    public void setHorsepower_tax(Double  horsepower_tax){
        this.horsepower_tax = horsepower_tax ;
        this.horsepower_taxDirtyFlag = true ;
    }

    /**
     * 获取 [HORSEPOWER_TAX]脏标记
     */
    @JsonIgnore
    public boolean getHorsepower_taxDirtyFlag(){
        return horsepower_taxDirtyFlag ;
    }

    /**
     * 获取 [LOCATION]
     */
    @JsonProperty("location")
    public String getLocation(){
        return location ;
    }

    /**
     * 设置 [LOCATION]
     */
    @JsonProperty("location")
    public void setLocation(String  location){
        this.location = location ;
        this.locationDirtyFlag = true ;
    }

    /**
     * 获取 [LOCATION]脏标记
     */
    @JsonIgnore
    public boolean getLocationDirtyFlag(){
        return locationDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_HAS_ERROR_COUNTER]
     */
    @JsonProperty("message_has_error_counter")
    public Integer getMessage_has_error_counter(){
        return message_has_error_counter ;
    }

    /**
     * 设置 [MESSAGE_HAS_ERROR_COUNTER]
     */
    @JsonProperty("message_has_error_counter")
    public void setMessage_has_error_counter(Integer  message_has_error_counter){
        this.message_has_error_counter = message_has_error_counter ;
        this.message_has_error_counterDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_HAS_ERROR_COUNTER]脏标记
     */
    @JsonIgnore
    public boolean getMessage_has_error_counterDirtyFlag(){
        return message_has_error_counterDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_IDS]
     */
    @JsonProperty("message_ids")
    public String getMessage_ids(){
        return message_ids ;
    }

    /**
     * 设置 [MESSAGE_IDS]
     */
    @JsonProperty("message_ids")
    public void setMessage_ids(String  message_ids){
        this.message_ids = message_ids ;
        this.message_idsDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_IDS]脏标记
     */
    @JsonIgnore
    public boolean getMessage_idsDirtyFlag(){
        return message_idsDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_IS_FOLLOWER]
     */
    @JsonProperty("message_is_follower")
    public String getMessage_is_follower(){
        return message_is_follower ;
    }

    /**
     * 设置 [MESSAGE_IS_FOLLOWER]
     */
    @JsonProperty("message_is_follower")
    public void setMessage_is_follower(String  message_is_follower){
        this.message_is_follower = message_is_follower ;
        this.message_is_followerDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_IS_FOLLOWER]脏标记
     */
    @JsonIgnore
    public boolean getMessage_is_followerDirtyFlag(){
        return message_is_followerDirtyFlag ;
    }

    /**
     * 获取 [ACTIVE]
     */
    @JsonProperty("active")
    public String getActive(){
        return active ;
    }

    /**
     * 设置 [ACTIVE]
     */
    @JsonProperty("active")
    public void setActive(String  active){
        this.active = active ;
        this.activeDirtyFlag = true ;
    }

    /**
     * 获取 [ACTIVE]脏标记
     */
    @JsonIgnore
    public boolean getActiveDirtyFlag(){
        return activeDirtyFlag ;
    }

    /**
     * 获取 [HORSEPOWER]
     */
    @JsonProperty("horsepower")
    public Integer getHorsepower(){
        return horsepower ;
    }

    /**
     * 设置 [HORSEPOWER]
     */
    @JsonProperty("horsepower")
    public void setHorsepower(Integer  horsepower){
        this.horsepower = horsepower ;
        this.horsepowerDirtyFlag = true ;
    }

    /**
     * 获取 [HORSEPOWER]脏标记
     */
    @JsonIgnore
    public boolean getHorsepowerDirtyFlag(){
        return horsepowerDirtyFlag ;
    }

    /**
     * 获取 [VIN_SN]
     */
    @JsonProperty("vin_sn")
    public String getVin_sn(){
        return vin_sn ;
    }

    /**
     * 设置 [VIN_SN]
     */
    @JsonProperty("vin_sn")
    public void setVin_sn(String  vin_sn){
        this.vin_sn = vin_sn ;
        this.vin_snDirtyFlag = true ;
    }

    /**
     * 获取 [VIN_SN]脏标记
     */
    @JsonIgnore
    public boolean getVin_snDirtyFlag(){
        return vin_snDirtyFlag ;
    }

    /**
     * 获取 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return display_name ;
    }

    /**
     * 设置 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

    /**
     * 获取 [DISPLAY_NAME]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return display_nameDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_HAS_ERROR]
     */
    @JsonProperty("message_has_error")
    public String getMessage_has_error(){
        return message_has_error ;
    }

    /**
     * 设置 [MESSAGE_HAS_ERROR]
     */
    @JsonProperty("message_has_error")
    public void setMessage_has_error(String  message_has_error){
        this.message_has_error = message_has_error ;
        this.message_has_errorDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_HAS_ERROR]脏标记
     */
    @JsonIgnore
    public boolean getMessage_has_errorDirtyFlag(){
        return message_has_errorDirtyFlag ;
    }

    /**
     * 获取 [ODOMETER_COUNT]
     */
    @JsonProperty("odometer_count")
    public Integer getOdometer_count(){
        return odometer_count ;
    }

    /**
     * 设置 [ODOMETER_COUNT]
     */
    @JsonProperty("odometer_count")
    public void setOdometer_count(Integer  odometer_count){
        this.odometer_count = odometer_count ;
        this.odometer_countDirtyFlag = true ;
    }

    /**
     * 获取 [ODOMETER_COUNT]脏标记
     */
    @JsonIgnore
    public boolean getOdometer_countDirtyFlag(){
        return odometer_countDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_PARTNER_IDS]
     */
    @JsonProperty("message_partner_ids")
    public String getMessage_partner_ids(){
        return message_partner_ids ;
    }

    /**
     * 设置 [MESSAGE_PARTNER_IDS]
     */
    @JsonProperty("message_partner_ids")
    public void setMessage_partner_ids(String  message_partner_ids){
        this.message_partner_ids = message_partner_ids ;
        this.message_partner_idsDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_PARTNER_IDS]脏标记
     */
    @JsonIgnore
    public boolean getMessage_partner_idsDirtyFlag(){
        return message_partner_idsDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_NEEDACTION_COUNTER]
     */
    @JsonProperty("message_needaction_counter")
    public Integer getMessage_needaction_counter(){
        return message_needaction_counter ;
    }

    /**
     * 设置 [MESSAGE_NEEDACTION_COUNTER]
     */
    @JsonProperty("message_needaction_counter")
    public void setMessage_needaction_counter(Integer  message_needaction_counter){
        this.message_needaction_counter = message_needaction_counter ;
        this.message_needaction_counterDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_NEEDACTION_COUNTER]脏标记
     */
    @JsonIgnore
    public boolean getMessage_needaction_counterDirtyFlag(){
        return message_needaction_counterDirtyFlag ;
    }

    /**
     * 获取 [CONTRACT_RENEWAL_NAME]
     */
    @JsonProperty("contract_renewal_name")
    public String getContract_renewal_name(){
        return contract_renewal_name ;
    }

    /**
     * 设置 [CONTRACT_RENEWAL_NAME]
     */
    @JsonProperty("contract_renewal_name")
    public void setContract_renewal_name(String  contract_renewal_name){
        this.contract_renewal_name = contract_renewal_name ;
        this.contract_renewal_nameDirtyFlag = true ;
    }

    /**
     * 获取 [CONTRACT_RENEWAL_NAME]脏标记
     */
    @JsonIgnore
    public boolean getContract_renewal_nameDirtyFlag(){
        return contract_renewal_nameDirtyFlag ;
    }

    /**
     * 获取 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return create_date ;
    }

    /**
     * 设置 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return create_dateDirtyFlag ;
    }

    /**
     * 获取 [ACTIVITY_SUMMARY]
     */
    @JsonProperty("activity_summary")
    public String getActivity_summary(){
        return activity_summary ;
    }

    /**
     * 设置 [ACTIVITY_SUMMARY]
     */
    @JsonProperty("activity_summary")
    public void setActivity_summary(String  activity_summary){
        this.activity_summary = activity_summary ;
        this.activity_summaryDirtyFlag = true ;
    }

    /**
     * 获取 [ACTIVITY_SUMMARY]脏标记
     */
    @JsonIgnore
    public boolean getActivity_summaryDirtyFlag(){
        return activity_summaryDirtyFlag ;
    }

    /**
     * 获取 [SEATS]
     */
    @JsonProperty("seats")
    public Integer getSeats(){
        return seats ;
    }

    /**
     * 设置 [SEATS]
     */
    @JsonProperty("seats")
    public void setSeats(Integer  seats){
        this.seats = seats ;
        this.seatsDirtyFlag = true ;
    }

    /**
     * 获取 [SEATS]脏标记
     */
    @JsonIgnore
    public boolean getSeatsDirtyFlag(){
        return seatsDirtyFlag ;
    }

    /**
     * 获取 [LOG_FUEL]
     */
    @JsonProperty("log_fuel")
    public String getLog_fuel(){
        return log_fuel ;
    }

    /**
     * 设置 [LOG_FUEL]
     */
    @JsonProperty("log_fuel")
    public void setLog_fuel(String  log_fuel){
        this.log_fuel = log_fuel ;
        this.log_fuelDirtyFlag = true ;
    }

    /**
     * 获取 [LOG_FUEL]脏标记
     */
    @JsonIgnore
    public boolean getLog_fuelDirtyFlag(){
        return log_fuelDirtyFlag ;
    }

    /**
     * 获取 [COST_COUNT]
     */
    @JsonProperty("cost_count")
    public Integer getCost_count(){
        return cost_count ;
    }

    /**
     * 设置 [COST_COUNT]
     */
    @JsonProperty("cost_count")
    public void setCost_count(Integer  cost_count){
        this.cost_count = cost_count ;
        this.cost_countDirtyFlag = true ;
    }

    /**
     * 获取 [COST_COUNT]脏标记
     */
    @JsonIgnore
    public boolean getCost_countDirtyFlag(){
        return cost_countDirtyFlag ;
    }

    /**
     * 获取 [LOG_DRIVERS]
     */
    @JsonProperty("log_drivers")
    public String getLog_drivers(){
        return log_drivers ;
    }

    /**
     * 设置 [LOG_DRIVERS]
     */
    @JsonProperty("log_drivers")
    public void setLog_drivers(String  log_drivers){
        this.log_drivers = log_drivers ;
        this.log_driversDirtyFlag = true ;
    }

    /**
     * 获取 [LOG_DRIVERS]脏标记
     */
    @JsonIgnore
    public boolean getLog_driversDirtyFlag(){
        return log_driversDirtyFlag ;
    }

    /**
     * 获取 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return __last_update ;
    }

    /**
     * 设置 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

    /**
     * 获取 [__LAST_UPDATE]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return __last_updateDirtyFlag ;
    }

    /**
     * 获取 [CONTRACT_RENEWAL_DUE_SOON]
     */
    @JsonProperty("contract_renewal_due_soon")
    public String getContract_renewal_due_soon(){
        return contract_renewal_due_soon ;
    }

    /**
     * 设置 [CONTRACT_RENEWAL_DUE_SOON]
     */
    @JsonProperty("contract_renewal_due_soon")
    public void setContract_renewal_due_soon(String  contract_renewal_due_soon){
        this.contract_renewal_due_soon = contract_renewal_due_soon ;
        this.contract_renewal_due_soonDirtyFlag = true ;
    }

    /**
     * 获取 [CONTRACT_RENEWAL_DUE_SOON]脏标记
     */
    @JsonIgnore
    public boolean getContract_renewal_due_soonDirtyFlag(){
        return contract_renewal_due_soonDirtyFlag ;
    }

    /**
     * 获取 [ODOMETER_UNIT]
     */
    @JsonProperty("odometer_unit")
    public String getOdometer_unit(){
        return odometer_unit ;
    }

    /**
     * 设置 [ODOMETER_UNIT]
     */
    @JsonProperty("odometer_unit")
    public void setOdometer_unit(String  odometer_unit){
        this.odometer_unit = odometer_unit ;
        this.odometer_unitDirtyFlag = true ;
    }

    /**
     * 获取 [ODOMETER_UNIT]脏标记
     */
    @JsonIgnore
    public boolean getOdometer_unitDirtyFlag(){
        return odometer_unitDirtyFlag ;
    }

    /**
     * 获取 [CO2]
     */
    @JsonProperty("co2")
    public Double getCo2(){
        return co2 ;
    }

    /**
     * 设置 [CO2]
     */
    @JsonProperty("co2")
    public void setCo2(Double  co2){
        this.co2 = co2 ;
        this.co2DirtyFlag = true ;
    }

    /**
     * 获取 [CO2]脏标记
     */
    @JsonIgnore
    public boolean getCo2DirtyFlag(){
        return co2DirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_MAIN_ATTACHMENT_ID]
     */
    @JsonProperty("message_main_attachment_id")
    public Integer getMessage_main_attachment_id(){
        return message_main_attachment_id ;
    }

    /**
     * 设置 [MESSAGE_MAIN_ATTACHMENT_ID]
     */
    @JsonProperty("message_main_attachment_id")
    public void setMessage_main_attachment_id(Integer  message_main_attachment_id){
        this.message_main_attachment_id = message_main_attachment_id ;
        this.message_main_attachment_idDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_MAIN_ATTACHMENT_ID]脏标记
     */
    @JsonIgnore
    public boolean getMessage_main_attachment_idDirtyFlag(){
        return message_main_attachment_idDirtyFlag ;
    }

    /**
     * 获取 [LOG_CONTRACTS]
     */
    @JsonProperty("log_contracts")
    public String getLog_contracts(){
        return log_contracts ;
    }

    /**
     * 设置 [LOG_CONTRACTS]
     */
    @JsonProperty("log_contracts")
    public void setLog_contracts(String  log_contracts){
        this.log_contracts = log_contracts ;
        this.log_contractsDirtyFlag = true ;
    }

    /**
     * 获取 [LOG_CONTRACTS]脏标记
     */
    @JsonIgnore
    public boolean getLog_contractsDirtyFlag(){
        return log_contractsDirtyFlag ;
    }

    /**
     * 获取 [FIRST_CONTRACT_DATE]
     */
    @JsonProperty("first_contract_date")
    public Timestamp getFirst_contract_date(){
        return first_contract_date ;
    }

    /**
     * 设置 [FIRST_CONTRACT_DATE]
     */
    @JsonProperty("first_contract_date")
    public void setFirst_contract_date(Timestamp  first_contract_date){
        this.first_contract_date = first_contract_date ;
        this.first_contract_dateDirtyFlag = true ;
    }

    /**
     * 获取 [FIRST_CONTRACT_DATE]脏标记
     */
    @JsonIgnore
    public boolean getFirst_contract_dateDirtyFlag(){
        return first_contract_dateDirtyFlag ;
    }

    /**
     * 获取 [ACTIVITY_STATE]
     */
    @JsonProperty("activity_state")
    public String getActivity_state(){
        return activity_state ;
    }

    /**
     * 设置 [ACTIVITY_STATE]
     */
    @JsonProperty("activity_state")
    public void setActivity_state(String  activity_state){
        this.activity_state = activity_state ;
        this.activity_stateDirtyFlag = true ;
    }

    /**
     * 获取 [ACTIVITY_STATE]脏标记
     */
    @JsonIgnore
    public boolean getActivity_stateDirtyFlag(){
        return activity_stateDirtyFlag ;
    }

    /**
     * 获取 [COLOR]
     */
    @JsonProperty("color")
    public String getColor(){
        return color ;
    }

    /**
     * 设置 [COLOR]
     */
    @JsonProperty("color")
    public void setColor(String  color){
        this.color = color ;
        this.colorDirtyFlag = true ;
    }

    /**
     * 获取 [COLOR]脏标记
     */
    @JsonIgnore
    public boolean getColorDirtyFlag(){
        return colorDirtyFlag ;
    }

    /**
     * 获取 [DOORS]
     */
    @JsonProperty("doors")
    public Integer getDoors(){
        return doors ;
    }

    /**
     * 设置 [DOORS]
     */
    @JsonProperty("doors")
    public void setDoors(Integer  doors){
        this.doors = doors ;
        this.doorsDirtyFlag = true ;
    }

    /**
     * 获取 [DOORS]脏标记
     */
    @JsonIgnore
    public boolean getDoorsDirtyFlag(){
        return doorsDirtyFlag ;
    }

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

    /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return idDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_ATTACHMENT_COUNT]
     */
    @JsonProperty("message_attachment_count")
    public Integer getMessage_attachment_count(){
        return message_attachment_count ;
    }

    /**
     * 设置 [MESSAGE_ATTACHMENT_COUNT]
     */
    @JsonProperty("message_attachment_count")
    public void setMessage_attachment_count(Integer  message_attachment_count){
        this.message_attachment_count = message_attachment_count ;
        this.message_attachment_countDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_ATTACHMENT_COUNT]脏标记
     */
    @JsonIgnore
    public boolean getMessage_attachment_countDirtyFlag(){
        return message_attachment_countDirtyFlag ;
    }

    /**
     * 获取 [NAME]
     */
    @JsonProperty("name")
    public String getName(){
        return name ;
    }

    /**
     * 设置 [NAME]
     */
    @JsonProperty("name")
    public void setName(String  name){
        this.name = name ;
        this.nameDirtyFlag = true ;
    }

    /**
     * 获取 [NAME]脏标记
     */
    @JsonIgnore
    public boolean getNameDirtyFlag(){
        return nameDirtyFlag ;
    }

    /**
     * 获取 [LOG_SERVICES]
     */
    @JsonProperty("log_services")
    public String getLog_services(){
        return log_services ;
    }

    /**
     * 设置 [LOG_SERVICES]
     */
    @JsonProperty("log_services")
    public void setLog_services(String  log_services){
        this.log_services = log_services ;
        this.log_servicesDirtyFlag = true ;
    }

    /**
     * 获取 [LOG_SERVICES]脏标记
     */
    @JsonIgnore
    public boolean getLog_servicesDirtyFlag(){
        return log_servicesDirtyFlag ;
    }

    /**
     * 获取 [CONTRACT_COUNT]
     */
    @JsonProperty("contract_count")
    public Integer getContract_count(){
        return contract_count ;
    }

    /**
     * 设置 [CONTRACT_COUNT]
     */
    @JsonProperty("contract_count")
    public void setContract_count(Integer  contract_count){
        this.contract_count = contract_count ;
        this.contract_countDirtyFlag = true ;
    }

    /**
     * 获取 [CONTRACT_COUNT]脏标记
     */
    @JsonIgnore
    public boolean getContract_countDirtyFlag(){
        return contract_countDirtyFlag ;
    }

    /**
     * 获取 [TRANSMISSION]
     */
    @JsonProperty("transmission")
    public String getTransmission(){
        return transmission ;
    }

    /**
     * 设置 [TRANSMISSION]
     */
    @JsonProperty("transmission")
    public void setTransmission(String  transmission){
        this.transmission = transmission ;
        this.transmissionDirtyFlag = true ;
    }

    /**
     * 获取 [TRANSMISSION]脏标记
     */
    @JsonIgnore
    public boolean getTransmissionDirtyFlag(){
        return transmissionDirtyFlag ;
    }

    /**
     * 获取 [SERVICE_COUNT]
     */
    @JsonProperty("service_count")
    public Integer getService_count(){
        return service_count ;
    }

    /**
     * 设置 [SERVICE_COUNT]
     */
    @JsonProperty("service_count")
    public void setService_count(Integer  service_count){
        this.service_count = service_count ;
        this.service_countDirtyFlag = true ;
    }

    /**
     * 获取 [SERVICE_COUNT]脏标记
     */
    @JsonIgnore
    public boolean getService_countDirtyFlag(){
        return service_countDirtyFlag ;
    }

    /**
     * 获取 [CONTRACT_RENEWAL_TOTAL]
     */
    @JsonProperty("contract_renewal_total")
    public String getContract_renewal_total(){
        return contract_renewal_total ;
    }

    /**
     * 设置 [CONTRACT_RENEWAL_TOTAL]
     */
    @JsonProperty("contract_renewal_total")
    public void setContract_renewal_total(String  contract_renewal_total){
        this.contract_renewal_total = contract_renewal_total ;
        this.contract_renewal_totalDirtyFlag = true ;
    }

    /**
     * 获取 [CONTRACT_RENEWAL_TOTAL]脏标记
     */
    @JsonIgnore
    public boolean getContract_renewal_totalDirtyFlag(){
        return contract_renewal_totalDirtyFlag ;
    }

    /**
     * 获取 [COMPANY_ID_TEXT]
     */
    @JsonProperty("company_id_text")
    public String getCompany_id_text(){
        return company_id_text ;
    }

    /**
     * 设置 [COMPANY_ID_TEXT]
     */
    @JsonProperty("company_id_text")
    public void setCompany_id_text(String  company_id_text){
        this.company_id_text = company_id_text ;
        this.company_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [COMPANY_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getCompany_id_textDirtyFlag(){
        return company_id_textDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return write_uid_text ;
    }

    /**
     * 设置 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return write_uid_textDirtyFlag ;
    }

    /**
     * 获取 [DRIVER_ID_TEXT]
     */
    @JsonProperty("driver_id_text")
    public String getDriver_id_text(){
        return driver_id_text ;
    }

    /**
     * 设置 [DRIVER_ID_TEXT]
     */
    @JsonProperty("driver_id_text")
    public void setDriver_id_text(String  driver_id_text){
        this.driver_id_text = driver_id_text ;
        this.driver_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [DRIVER_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getDriver_id_textDirtyFlag(){
        return driver_id_textDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return create_uid_text ;
    }

    /**
     * 设置 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return create_uid_textDirtyFlag ;
    }

    /**
     * 获取 [IMAGE_MEDIUM]
     */
    @JsonProperty("image_medium")
    public byte[] getImage_medium(){
        return image_medium ;
    }

    /**
     * 设置 [IMAGE_MEDIUM]
     */
    @JsonProperty("image_medium")
    public void setImage_medium(byte[]  image_medium){
        this.image_medium = image_medium ;
        this.image_mediumDirtyFlag = true ;
    }

    /**
     * 获取 [IMAGE_MEDIUM]脏标记
     */
    @JsonIgnore
    public boolean getImage_mediumDirtyFlag(){
        return image_mediumDirtyFlag ;
    }

    /**
     * 获取 [IMAGE]
     */
    @JsonProperty("image")
    public byte[] getImage(){
        return image ;
    }

    /**
     * 设置 [IMAGE]
     */
    @JsonProperty("image")
    public void setImage(byte[]  image){
        this.image = image ;
        this.imageDirtyFlag = true ;
    }

    /**
     * 获取 [IMAGE]脏标记
     */
    @JsonIgnore
    public boolean getImageDirtyFlag(){
        return imageDirtyFlag ;
    }

    /**
     * 获取 [MODEL_ID_TEXT]
     */
    @JsonProperty("model_id_text")
    public String getModel_id_text(){
        return model_id_text ;
    }

    /**
     * 设置 [MODEL_ID_TEXT]
     */
    @JsonProperty("model_id_text")
    public void setModel_id_text(String  model_id_text){
        this.model_id_text = model_id_text ;
        this.model_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [MODEL_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getModel_id_textDirtyFlag(){
        return model_id_textDirtyFlag ;
    }

    /**
     * 获取 [IMAGE_SMALL]
     */
    @JsonProperty("image_small")
    public byte[] getImage_small(){
        return image_small ;
    }

    /**
     * 设置 [IMAGE_SMALL]
     */
    @JsonProperty("image_small")
    public void setImage_small(byte[]  image_small){
        this.image_small = image_small ;
        this.image_smallDirtyFlag = true ;
    }

    /**
     * 获取 [IMAGE_SMALL]脏标记
     */
    @JsonIgnore
    public boolean getImage_smallDirtyFlag(){
        return image_smallDirtyFlag ;
    }

    /**
     * 获取 [BRAND_ID_TEXT]
     */
    @JsonProperty("brand_id_text")
    public String getBrand_id_text(){
        return brand_id_text ;
    }

    /**
     * 设置 [BRAND_ID_TEXT]
     */
    @JsonProperty("brand_id_text")
    public void setBrand_id_text(String  brand_id_text){
        this.brand_id_text = brand_id_text ;
        this.brand_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [BRAND_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getBrand_id_textDirtyFlag(){
        return brand_id_textDirtyFlag ;
    }

    /**
     * 获取 [STATE_ID_TEXT]
     */
    @JsonProperty("state_id_text")
    public String getState_id_text(){
        return state_id_text ;
    }

    /**
     * 设置 [STATE_ID_TEXT]
     */
    @JsonProperty("state_id_text")
    public void setState_id_text(String  state_id_text){
        this.state_id_text = state_id_text ;
        this.state_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [STATE_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getState_id_textDirtyFlag(){
        return state_id_textDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return create_uid ;
    }

    /**
     * 设置 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return create_uidDirtyFlag ;
    }

    /**
     * 获取 [MODEL_ID]
     */
    @JsonProperty("model_id")
    public Integer getModel_id(){
        return model_id ;
    }

    /**
     * 设置 [MODEL_ID]
     */
    @JsonProperty("model_id")
    public void setModel_id(Integer  model_id){
        this.model_id = model_id ;
        this.model_idDirtyFlag = true ;
    }

    /**
     * 获取 [MODEL_ID]脏标记
     */
    @JsonIgnore
    public boolean getModel_idDirtyFlag(){
        return model_idDirtyFlag ;
    }

    /**
     * 获取 [BRAND_ID]
     */
    @JsonProperty("brand_id")
    public Integer getBrand_id(){
        return brand_id ;
    }

    /**
     * 设置 [BRAND_ID]
     */
    @JsonProperty("brand_id")
    public void setBrand_id(Integer  brand_id){
        this.brand_id = brand_id ;
        this.brand_idDirtyFlag = true ;
    }

    /**
     * 获取 [BRAND_ID]脏标记
     */
    @JsonIgnore
    public boolean getBrand_idDirtyFlag(){
        return brand_idDirtyFlag ;
    }

    /**
     * 获取 [COMPANY_ID]
     */
    @JsonProperty("company_id")
    public Integer getCompany_id(){
        return company_id ;
    }

    /**
     * 设置 [COMPANY_ID]
     */
    @JsonProperty("company_id")
    public void setCompany_id(Integer  company_id){
        this.company_id = company_id ;
        this.company_idDirtyFlag = true ;
    }

    /**
     * 获取 [COMPANY_ID]脏标记
     */
    @JsonIgnore
    public boolean getCompany_idDirtyFlag(){
        return company_idDirtyFlag ;
    }

    /**
     * 获取 [STATE_ID]
     */
    @JsonProperty("state_id")
    public Integer getState_id(){
        return state_id ;
    }

    /**
     * 设置 [STATE_ID]
     */
    @JsonProperty("state_id")
    public void setState_id(Integer  state_id){
        this.state_id = state_id ;
        this.state_idDirtyFlag = true ;
    }

    /**
     * 获取 [STATE_ID]脏标记
     */
    @JsonIgnore
    public boolean getState_idDirtyFlag(){
        return state_idDirtyFlag ;
    }

    /**
     * 获取 [DRIVER_ID]
     */
    @JsonProperty("driver_id")
    public Integer getDriver_id(){
        return driver_id ;
    }

    /**
     * 设置 [DRIVER_ID]
     */
    @JsonProperty("driver_id")
    public void setDriver_id(Integer  driver_id){
        this.driver_id = driver_id ;
        this.driver_idDirtyFlag = true ;
    }

    /**
     * 获取 [DRIVER_ID]脏标记
     */
    @JsonIgnore
    public boolean getDriver_idDirtyFlag(){
        return driver_idDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return write_uid ;
    }

    /**
     * 设置 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return write_uidDirtyFlag ;
    }



    public Fleet_vehicle toDO() {
        Fleet_vehicle srfdomain = new Fleet_vehicle();
        if(getFuel_typeDirtyFlag())
            srfdomain.setFuel_type(fuel_type);
        if(getTag_idsDirtyFlag())
            srfdomain.setTag_ids(tag_ids);
        if(getActivity_idsDirtyFlag())
            srfdomain.setActivity_ids(activity_ids);
        if(getMessage_unreadDirtyFlag())
            srfdomain.setMessage_unread(message_unread);
        if(getWebsite_message_idsDirtyFlag())
            srfdomain.setWebsite_message_ids(website_message_ids);
        if(getPowerDirtyFlag())
            srfdomain.setPower(power);
        if(getMessage_needactionDirtyFlag())
            srfdomain.setMessage_needaction(message_needaction);
        if(getResidual_valueDirtyFlag())
            srfdomain.setResidual_value(residual_value);
        if(getOdometerDirtyFlag())
            srfdomain.setOdometer(odometer);
        if(getMessage_channel_idsDirtyFlag())
            srfdomain.setMessage_channel_ids(message_channel_ids);
        if(getFuel_logs_countDirtyFlag())
            srfdomain.setFuel_logs_count(fuel_logs_count);
        if(getMessage_follower_idsDirtyFlag())
            srfdomain.setMessage_follower_ids(message_follower_ids);
        if(getModel_yearDirtyFlag())
            srfdomain.setModel_year(model_year);
        if(getCar_valueDirtyFlag())
            srfdomain.setCar_value(car_value);
        if(getLicense_plateDirtyFlag())
            srfdomain.setLicense_plate(license_plate);
        if(getContract_renewal_overdueDirtyFlag())
            srfdomain.setContract_renewal_overdue(contract_renewal_overdue);
        if(getMessage_unread_counterDirtyFlag())
            srfdomain.setMessage_unread_counter(message_unread_counter);
        if(getAcquisition_dateDirtyFlag())
            srfdomain.setAcquisition_date(acquisition_date);
        if(getActivity_date_deadlineDirtyFlag())
            srfdomain.setActivity_date_deadline(activity_date_deadline);
        if(getWrite_dateDirtyFlag())
            srfdomain.setWrite_date(write_date);
        if(getActivity_user_idDirtyFlag())
            srfdomain.setActivity_user_id(activity_user_id);
        if(getActivity_type_idDirtyFlag())
            srfdomain.setActivity_type_id(activity_type_id);
        if(getHorsepower_taxDirtyFlag())
            srfdomain.setHorsepower_tax(horsepower_tax);
        if(getLocationDirtyFlag())
            srfdomain.setLocation(location);
        if(getMessage_has_error_counterDirtyFlag())
            srfdomain.setMessage_has_error_counter(message_has_error_counter);
        if(getMessage_idsDirtyFlag())
            srfdomain.setMessage_ids(message_ids);
        if(getMessage_is_followerDirtyFlag())
            srfdomain.setMessage_is_follower(message_is_follower);
        if(getActiveDirtyFlag())
            srfdomain.setActive(active);
        if(getHorsepowerDirtyFlag())
            srfdomain.setHorsepower(horsepower);
        if(getVin_snDirtyFlag())
            srfdomain.setVin_sn(vin_sn);
        if(getDisplay_nameDirtyFlag())
            srfdomain.setDisplay_name(display_name);
        if(getMessage_has_errorDirtyFlag())
            srfdomain.setMessage_has_error(message_has_error);
        if(getOdometer_countDirtyFlag())
            srfdomain.setOdometer_count(odometer_count);
        if(getMessage_partner_idsDirtyFlag())
            srfdomain.setMessage_partner_ids(message_partner_ids);
        if(getMessage_needaction_counterDirtyFlag())
            srfdomain.setMessage_needaction_counter(message_needaction_counter);
        if(getContract_renewal_nameDirtyFlag())
            srfdomain.setContract_renewal_name(contract_renewal_name);
        if(getCreate_dateDirtyFlag())
            srfdomain.setCreate_date(create_date);
        if(getActivity_summaryDirtyFlag())
            srfdomain.setActivity_summary(activity_summary);
        if(getSeatsDirtyFlag())
            srfdomain.setSeats(seats);
        if(getLog_fuelDirtyFlag())
            srfdomain.setLog_fuel(log_fuel);
        if(getCost_countDirtyFlag())
            srfdomain.setCost_count(cost_count);
        if(getLog_driversDirtyFlag())
            srfdomain.setLog_drivers(log_drivers);
        if(get__last_updateDirtyFlag())
            srfdomain.set__last_update(__last_update);
        if(getContract_renewal_due_soonDirtyFlag())
            srfdomain.setContract_renewal_due_soon(contract_renewal_due_soon);
        if(getOdometer_unitDirtyFlag())
            srfdomain.setOdometer_unit(odometer_unit);
        if(getCo2DirtyFlag())
            srfdomain.setCo2(co2);
        if(getMessage_main_attachment_idDirtyFlag())
            srfdomain.setMessage_main_attachment_id(message_main_attachment_id);
        if(getLog_contractsDirtyFlag())
            srfdomain.setLog_contracts(log_contracts);
        if(getFirst_contract_dateDirtyFlag())
            srfdomain.setFirst_contract_date(first_contract_date);
        if(getActivity_stateDirtyFlag())
            srfdomain.setActivity_state(activity_state);
        if(getColorDirtyFlag())
            srfdomain.setColor(color);
        if(getDoorsDirtyFlag())
            srfdomain.setDoors(doors);
        if(getIdDirtyFlag())
            srfdomain.setId(id);
        if(getMessage_attachment_countDirtyFlag())
            srfdomain.setMessage_attachment_count(message_attachment_count);
        if(getNameDirtyFlag())
            srfdomain.setName(name);
        if(getLog_servicesDirtyFlag())
            srfdomain.setLog_services(log_services);
        if(getContract_countDirtyFlag())
            srfdomain.setContract_count(contract_count);
        if(getTransmissionDirtyFlag())
            srfdomain.setTransmission(transmission);
        if(getService_countDirtyFlag())
            srfdomain.setService_count(service_count);
        if(getContract_renewal_totalDirtyFlag())
            srfdomain.setContract_renewal_total(contract_renewal_total);
        if(getCompany_id_textDirtyFlag())
            srfdomain.setCompany_id_text(company_id_text);
        if(getWrite_uid_textDirtyFlag())
            srfdomain.setWrite_uid_text(write_uid_text);
        if(getDriver_id_textDirtyFlag())
            srfdomain.setDriver_id_text(driver_id_text);
        if(getCreate_uid_textDirtyFlag())
            srfdomain.setCreate_uid_text(create_uid_text);
        if(getImage_mediumDirtyFlag())
            srfdomain.setImage_medium(image_medium);
        if(getImageDirtyFlag())
            srfdomain.setImage(image);
        if(getModel_id_textDirtyFlag())
            srfdomain.setModel_id_text(model_id_text);
        if(getImage_smallDirtyFlag())
            srfdomain.setImage_small(image_small);
        if(getBrand_id_textDirtyFlag())
            srfdomain.setBrand_id_text(brand_id_text);
        if(getState_id_textDirtyFlag())
            srfdomain.setState_id_text(state_id_text);
        if(getCreate_uidDirtyFlag())
            srfdomain.setCreate_uid(create_uid);
        if(getModel_idDirtyFlag())
            srfdomain.setModel_id(model_id);
        if(getBrand_idDirtyFlag())
            srfdomain.setBrand_id(brand_id);
        if(getCompany_idDirtyFlag())
            srfdomain.setCompany_id(company_id);
        if(getState_idDirtyFlag())
            srfdomain.setState_id(state_id);
        if(getDriver_idDirtyFlag())
            srfdomain.setDriver_id(driver_id);
        if(getWrite_uidDirtyFlag())
            srfdomain.setWrite_uid(write_uid);

        return srfdomain;
    }

    public void fromDO(Fleet_vehicle srfdomain) {
        if(srfdomain == null )
          return ;
        if(srfdomain.getFuel_typeDirtyFlag())
            this.setFuel_type(srfdomain.getFuel_type());
        if(srfdomain.getTag_idsDirtyFlag())
            this.setTag_ids(srfdomain.getTag_ids());
        if(srfdomain.getActivity_idsDirtyFlag())
            this.setActivity_ids(srfdomain.getActivity_ids());
        if(srfdomain.getMessage_unreadDirtyFlag())
            this.setMessage_unread(srfdomain.getMessage_unread());
        if(srfdomain.getWebsite_message_idsDirtyFlag())
            this.setWebsite_message_ids(srfdomain.getWebsite_message_ids());
        if(srfdomain.getPowerDirtyFlag())
            this.setPower(srfdomain.getPower());
        if(srfdomain.getMessage_needactionDirtyFlag())
            this.setMessage_needaction(srfdomain.getMessage_needaction());
        if(srfdomain.getResidual_valueDirtyFlag())
            this.setResidual_value(srfdomain.getResidual_value());
        if(srfdomain.getOdometerDirtyFlag())
            this.setOdometer(srfdomain.getOdometer());
        if(srfdomain.getMessage_channel_idsDirtyFlag())
            this.setMessage_channel_ids(srfdomain.getMessage_channel_ids());
        if(srfdomain.getFuel_logs_countDirtyFlag())
            this.setFuel_logs_count(srfdomain.getFuel_logs_count());
        if(srfdomain.getMessage_follower_idsDirtyFlag())
            this.setMessage_follower_ids(srfdomain.getMessage_follower_ids());
        if(srfdomain.getModel_yearDirtyFlag())
            this.setModel_year(srfdomain.getModel_year());
        if(srfdomain.getCar_valueDirtyFlag())
            this.setCar_value(srfdomain.getCar_value());
        if(srfdomain.getLicense_plateDirtyFlag())
            this.setLicense_plate(srfdomain.getLicense_plate());
        if(srfdomain.getContract_renewal_overdueDirtyFlag())
            this.setContract_renewal_overdue(srfdomain.getContract_renewal_overdue());
        if(srfdomain.getMessage_unread_counterDirtyFlag())
            this.setMessage_unread_counter(srfdomain.getMessage_unread_counter());
        if(srfdomain.getAcquisition_dateDirtyFlag())
            this.setAcquisition_date(srfdomain.getAcquisition_date());
        if(srfdomain.getActivity_date_deadlineDirtyFlag())
            this.setActivity_date_deadline(srfdomain.getActivity_date_deadline());
        if(srfdomain.getWrite_dateDirtyFlag())
            this.setWrite_date(srfdomain.getWrite_date());
        if(srfdomain.getActivity_user_idDirtyFlag())
            this.setActivity_user_id(srfdomain.getActivity_user_id());
        if(srfdomain.getActivity_type_idDirtyFlag())
            this.setActivity_type_id(srfdomain.getActivity_type_id());
        if(srfdomain.getHorsepower_taxDirtyFlag())
            this.setHorsepower_tax(srfdomain.getHorsepower_tax());
        if(srfdomain.getLocationDirtyFlag())
            this.setLocation(srfdomain.getLocation());
        if(srfdomain.getMessage_has_error_counterDirtyFlag())
            this.setMessage_has_error_counter(srfdomain.getMessage_has_error_counter());
        if(srfdomain.getMessage_idsDirtyFlag())
            this.setMessage_ids(srfdomain.getMessage_ids());
        if(srfdomain.getMessage_is_followerDirtyFlag())
            this.setMessage_is_follower(srfdomain.getMessage_is_follower());
        if(srfdomain.getActiveDirtyFlag())
            this.setActive(srfdomain.getActive());
        if(srfdomain.getHorsepowerDirtyFlag())
            this.setHorsepower(srfdomain.getHorsepower());
        if(srfdomain.getVin_snDirtyFlag())
            this.setVin_sn(srfdomain.getVin_sn());
        if(srfdomain.getDisplay_nameDirtyFlag())
            this.setDisplay_name(srfdomain.getDisplay_name());
        if(srfdomain.getMessage_has_errorDirtyFlag())
            this.setMessage_has_error(srfdomain.getMessage_has_error());
        if(srfdomain.getOdometer_countDirtyFlag())
            this.setOdometer_count(srfdomain.getOdometer_count());
        if(srfdomain.getMessage_partner_idsDirtyFlag())
            this.setMessage_partner_ids(srfdomain.getMessage_partner_ids());
        if(srfdomain.getMessage_needaction_counterDirtyFlag())
            this.setMessage_needaction_counter(srfdomain.getMessage_needaction_counter());
        if(srfdomain.getContract_renewal_nameDirtyFlag())
            this.setContract_renewal_name(srfdomain.getContract_renewal_name());
        if(srfdomain.getCreate_dateDirtyFlag())
            this.setCreate_date(srfdomain.getCreate_date());
        if(srfdomain.getActivity_summaryDirtyFlag())
            this.setActivity_summary(srfdomain.getActivity_summary());
        if(srfdomain.getSeatsDirtyFlag())
            this.setSeats(srfdomain.getSeats());
        if(srfdomain.getLog_fuelDirtyFlag())
            this.setLog_fuel(srfdomain.getLog_fuel());
        if(srfdomain.getCost_countDirtyFlag())
            this.setCost_count(srfdomain.getCost_count());
        if(srfdomain.getLog_driversDirtyFlag())
            this.setLog_drivers(srfdomain.getLog_drivers());
        if(srfdomain.get__last_updateDirtyFlag())
            this.set__last_update(srfdomain.get__last_update());
        if(srfdomain.getContract_renewal_due_soonDirtyFlag())
            this.setContract_renewal_due_soon(srfdomain.getContract_renewal_due_soon());
        if(srfdomain.getOdometer_unitDirtyFlag())
            this.setOdometer_unit(srfdomain.getOdometer_unit());
        if(srfdomain.getCo2DirtyFlag())
            this.setCo2(srfdomain.getCo2());
        if(srfdomain.getMessage_main_attachment_idDirtyFlag())
            this.setMessage_main_attachment_id(srfdomain.getMessage_main_attachment_id());
        if(srfdomain.getLog_contractsDirtyFlag())
            this.setLog_contracts(srfdomain.getLog_contracts());
        if(srfdomain.getFirst_contract_dateDirtyFlag())
            this.setFirst_contract_date(srfdomain.getFirst_contract_date());
        if(srfdomain.getActivity_stateDirtyFlag())
            this.setActivity_state(srfdomain.getActivity_state());
        if(srfdomain.getColorDirtyFlag())
            this.setColor(srfdomain.getColor());
        if(srfdomain.getDoorsDirtyFlag())
            this.setDoors(srfdomain.getDoors());
        if(srfdomain.getIdDirtyFlag())
            this.setId(srfdomain.getId());
        if(srfdomain.getMessage_attachment_countDirtyFlag())
            this.setMessage_attachment_count(srfdomain.getMessage_attachment_count());
        if(srfdomain.getNameDirtyFlag())
            this.setName(srfdomain.getName());
        if(srfdomain.getLog_servicesDirtyFlag())
            this.setLog_services(srfdomain.getLog_services());
        if(srfdomain.getContract_countDirtyFlag())
            this.setContract_count(srfdomain.getContract_count());
        if(srfdomain.getTransmissionDirtyFlag())
            this.setTransmission(srfdomain.getTransmission());
        if(srfdomain.getService_countDirtyFlag())
            this.setService_count(srfdomain.getService_count());
        if(srfdomain.getContract_renewal_totalDirtyFlag())
            this.setContract_renewal_total(srfdomain.getContract_renewal_total());
        if(srfdomain.getCompany_id_textDirtyFlag())
            this.setCompany_id_text(srfdomain.getCompany_id_text());
        if(srfdomain.getWrite_uid_textDirtyFlag())
            this.setWrite_uid_text(srfdomain.getWrite_uid_text());
        if(srfdomain.getDriver_id_textDirtyFlag())
            this.setDriver_id_text(srfdomain.getDriver_id_text());
        if(srfdomain.getCreate_uid_textDirtyFlag())
            this.setCreate_uid_text(srfdomain.getCreate_uid_text());
        if(srfdomain.getImage_mediumDirtyFlag())
            this.setImage_medium(srfdomain.getImage_medium());
        if(srfdomain.getImageDirtyFlag())
            this.setImage(srfdomain.getImage());
        if(srfdomain.getModel_id_textDirtyFlag())
            this.setModel_id_text(srfdomain.getModel_id_text());
        if(srfdomain.getImage_smallDirtyFlag())
            this.setImage_small(srfdomain.getImage_small());
        if(srfdomain.getBrand_id_textDirtyFlag())
            this.setBrand_id_text(srfdomain.getBrand_id_text());
        if(srfdomain.getState_id_textDirtyFlag())
            this.setState_id_text(srfdomain.getState_id_text());
        if(srfdomain.getCreate_uidDirtyFlag())
            this.setCreate_uid(srfdomain.getCreate_uid());
        if(srfdomain.getModel_idDirtyFlag())
            this.setModel_id(srfdomain.getModel_id());
        if(srfdomain.getBrand_idDirtyFlag())
            this.setBrand_id(srfdomain.getBrand_id());
        if(srfdomain.getCompany_idDirtyFlag())
            this.setCompany_id(srfdomain.getCompany_id());
        if(srfdomain.getState_idDirtyFlag())
            this.setState_id(srfdomain.getState_id());
        if(srfdomain.getDriver_idDirtyFlag())
            this.setDriver_id(srfdomain.getDriver_id());
        if(srfdomain.getWrite_uidDirtyFlag())
            this.setWrite_uid(srfdomain.getWrite_uid());

    }

    public List<Fleet_vehicleDTO> fromDOPage(List<Fleet_vehicle> poPage)   {
        if(poPage == null)
            return null;
        List<Fleet_vehicleDTO> dtos=new ArrayList<Fleet_vehicleDTO>();
        for(Fleet_vehicle domain : poPage) {
            Fleet_vehicleDTO dto = new Fleet_vehicleDTO();
            dto.fromDO(domain);
            dtos.add(dto);
        }
        return dtos;
    }
}

