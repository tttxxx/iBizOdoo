package cn.ibizlab.odoo.service.odoo_fleet.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_fleet.dto.Fleet_vehicle_stateDTO;
import cn.ibizlab.odoo.core.odoo_fleet.domain.Fleet_vehicle_state;
import cn.ibizlab.odoo.core.odoo_fleet.service.IFleet_vehicle_stateService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_fleet.filter.Fleet_vehicle_stateSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Fleet_vehicle_state" })
@RestController
@RequestMapping("")
public class Fleet_vehicle_stateResource {

    @Autowired
    private IFleet_vehicle_stateService fleet_vehicle_stateService;

    public IFleet_vehicle_stateService getFleet_vehicle_stateService() {
        return this.fleet_vehicle_stateService;
    }

    @ApiOperation(value = "批删除数据", tags = {"Fleet_vehicle_state" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_fleet/fleet_vehicle_states/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Fleet_vehicle_stateDTO> fleet_vehicle_statedtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "获取数据", tags = {"Fleet_vehicle_state" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_fleet/fleet_vehicle_states/{fleet_vehicle_state_id}")
    public ResponseEntity<Fleet_vehicle_stateDTO> get(@PathVariable("fleet_vehicle_state_id") Integer fleet_vehicle_state_id) {
        Fleet_vehicle_stateDTO dto = new Fleet_vehicle_stateDTO();
        Fleet_vehicle_state domain = fleet_vehicle_stateService.get(fleet_vehicle_state_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "建立数据", tags = {"Fleet_vehicle_state" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_fleet/fleet_vehicle_states")

    public ResponseEntity<Fleet_vehicle_stateDTO> create(@RequestBody Fleet_vehicle_stateDTO fleet_vehicle_statedto) {
        Fleet_vehicle_stateDTO dto = new Fleet_vehicle_stateDTO();
        Fleet_vehicle_state domain = fleet_vehicle_statedto.toDO();
		fleet_vehicle_stateService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "更新数据", tags = {"Fleet_vehicle_state" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_fleet/fleet_vehicle_states/{fleet_vehicle_state_id}")

    public ResponseEntity<Fleet_vehicle_stateDTO> update(@PathVariable("fleet_vehicle_state_id") Integer fleet_vehicle_state_id, @RequestBody Fleet_vehicle_stateDTO fleet_vehicle_statedto) {
		Fleet_vehicle_state domain = fleet_vehicle_statedto.toDO();
        domain.setId(fleet_vehicle_state_id);
		fleet_vehicle_stateService.update(domain);
		Fleet_vehicle_stateDTO dto = new Fleet_vehicle_stateDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批更新数据", tags = {"Fleet_vehicle_state" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_fleet/fleet_vehicle_states/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Fleet_vehicle_stateDTO> fleet_vehicle_statedtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "批建立数据", tags = {"Fleet_vehicle_state" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_fleet/fleet_vehicle_states/createBatch")
    public ResponseEntity<Boolean> createBatchFleet_vehicle_state(@RequestBody List<Fleet_vehicle_stateDTO> fleet_vehicle_statedtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "删除数据", tags = {"Fleet_vehicle_state" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_fleet/fleet_vehicle_states/{fleet_vehicle_state_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("fleet_vehicle_state_id") Integer fleet_vehicle_state_id) {
        Fleet_vehicle_stateDTO fleet_vehicle_statedto = new Fleet_vehicle_stateDTO();
		Fleet_vehicle_state domain = new Fleet_vehicle_state();
		fleet_vehicle_statedto.setId(fleet_vehicle_state_id);
		domain.setId(fleet_vehicle_state_id);
        Boolean rst = fleet_vehicle_stateService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

	@ApiOperation(value = "获取默认查询", tags = {"Fleet_vehicle_state" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_fleet/fleet_vehicle_states/fetchdefault")
	public ResponseEntity<Page<Fleet_vehicle_stateDTO>> fetchDefault(Fleet_vehicle_stateSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Fleet_vehicle_stateDTO> list = new ArrayList<Fleet_vehicle_stateDTO>();
        
        Page<Fleet_vehicle_state> domains = fleet_vehicle_stateService.searchDefault(context) ;
        for(Fleet_vehicle_state fleet_vehicle_state : domains.getContent()){
            Fleet_vehicle_stateDTO dto = new Fleet_vehicle_stateDTO();
            dto.fromDO(fleet_vehicle_state);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
