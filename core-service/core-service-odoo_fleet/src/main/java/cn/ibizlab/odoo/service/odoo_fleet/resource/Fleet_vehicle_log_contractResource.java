package cn.ibizlab.odoo.service.odoo_fleet.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_fleet.dto.Fleet_vehicle_log_contractDTO;
import cn.ibizlab.odoo.core.odoo_fleet.domain.Fleet_vehicle_log_contract;
import cn.ibizlab.odoo.core.odoo_fleet.service.IFleet_vehicle_log_contractService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_fleet.filter.Fleet_vehicle_log_contractSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Fleet_vehicle_log_contract" })
@RestController
@RequestMapping("")
public class Fleet_vehicle_log_contractResource {

    @Autowired
    private IFleet_vehicle_log_contractService fleet_vehicle_log_contractService;

    public IFleet_vehicle_log_contractService getFleet_vehicle_log_contractService() {
        return this.fleet_vehicle_log_contractService;
    }

    @ApiOperation(value = "获取数据", tags = {"Fleet_vehicle_log_contract" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_fleet/fleet_vehicle_log_contracts/{fleet_vehicle_log_contract_id}")
    public ResponseEntity<Fleet_vehicle_log_contractDTO> get(@PathVariable("fleet_vehicle_log_contract_id") Integer fleet_vehicle_log_contract_id) {
        Fleet_vehicle_log_contractDTO dto = new Fleet_vehicle_log_contractDTO();
        Fleet_vehicle_log_contract domain = fleet_vehicle_log_contractService.get(fleet_vehicle_log_contract_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批建立数据", tags = {"Fleet_vehicle_log_contract" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_fleet/fleet_vehicle_log_contracts/createBatch")
    public ResponseEntity<Boolean> createBatchFleet_vehicle_log_contract(@RequestBody List<Fleet_vehicle_log_contractDTO> fleet_vehicle_log_contractdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "更新数据", tags = {"Fleet_vehicle_log_contract" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_fleet/fleet_vehicle_log_contracts/{fleet_vehicle_log_contract_id}")

    public ResponseEntity<Fleet_vehicle_log_contractDTO> update(@PathVariable("fleet_vehicle_log_contract_id") Integer fleet_vehicle_log_contract_id, @RequestBody Fleet_vehicle_log_contractDTO fleet_vehicle_log_contractdto) {
		Fleet_vehicle_log_contract domain = fleet_vehicle_log_contractdto.toDO();
        domain.setId(fleet_vehicle_log_contract_id);
		fleet_vehicle_log_contractService.update(domain);
		Fleet_vehicle_log_contractDTO dto = new Fleet_vehicle_log_contractDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "删除数据", tags = {"Fleet_vehicle_log_contract" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_fleet/fleet_vehicle_log_contracts/{fleet_vehicle_log_contract_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("fleet_vehicle_log_contract_id") Integer fleet_vehicle_log_contract_id) {
        Fleet_vehicle_log_contractDTO fleet_vehicle_log_contractdto = new Fleet_vehicle_log_contractDTO();
		Fleet_vehicle_log_contract domain = new Fleet_vehicle_log_contract();
		fleet_vehicle_log_contractdto.setId(fleet_vehicle_log_contract_id);
		domain.setId(fleet_vehicle_log_contract_id);
        Boolean rst = fleet_vehicle_log_contractService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "批删除数据", tags = {"Fleet_vehicle_log_contract" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_fleet/fleet_vehicle_log_contracts/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Fleet_vehicle_log_contractDTO> fleet_vehicle_log_contractdtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "建立数据", tags = {"Fleet_vehicle_log_contract" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_fleet/fleet_vehicle_log_contracts")

    public ResponseEntity<Fleet_vehicle_log_contractDTO> create(@RequestBody Fleet_vehicle_log_contractDTO fleet_vehicle_log_contractdto) {
        Fleet_vehicle_log_contractDTO dto = new Fleet_vehicle_log_contractDTO();
        Fleet_vehicle_log_contract domain = fleet_vehicle_log_contractdto.toDO();
		fleet_vehicle_log_contractService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批更新数据", tags = {"Fleet_vehicle_log_contract" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_fleet/fleet_vehicle_log_contracts/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Fleet_vehicle_log_contractDTO> fleet_vehicle_log_contractdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Fleet_vehicle_log_contract" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_fleet/fleet_vehicle_log_contracts/fetchdefault")
	public ResponseEntity<Page<Fleet_vehicle_log_contractDTO>> fetchDefault(Fleet_vehicle_log_contractSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Fleet_vehicle_log_contractDTO> list = new ArrayList<Fleet_vehicle_log_contractDTO>();
        
        Page<Fleet_vehicle_log_contract> domains = fleet_vehicle_log_contractService.searchDefault(context) ;
        for(Fleet_vehicle_log_contract fleet_vehicle_log_contract : domains.getContent()){
            Fleet_vehicle_log_contractDTO dto = new Fleet_vehicle_log_contractDTO();
            dto.fromDO(fleet_vehicle_log_contract);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
