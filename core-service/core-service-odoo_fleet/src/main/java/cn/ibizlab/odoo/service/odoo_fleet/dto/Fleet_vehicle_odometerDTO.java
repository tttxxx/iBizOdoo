package cn.ibizlab.odoo.service.odoo_fleet.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.core.odoo_fleet.valuerule.anno.fleet_vehicle_odometer.*;
import cn.ibizlab.odoo.core.odoo_fleet.domain.Fleet_vehicle_odometer;
import org.springframework.cglib.beans.BeanCopier;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 服务DTO对象[Fleet_vehicle_odometerDTO]
 */
public class Fleet_vehicle_odometerDTO implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [NAME]
     *
     */
    @Fleet_vehicle_odometerNameDefault(info = "默认规则")
    private String name;

    @JsonIgnore
    private boolean nameDirtyFlag;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @Fleet_vehicle_odometer__last_updateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp __last_update;

    @JsonIgnore
    private boolean __last_updateDirtyFlag;

    /**
     * 属性 [DATE]
     *
     */
    @Fleet_vehicle_odometerDateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    private Timestamp date;

    @JsonIgnore
    private boolean dateDirtyFlag;

    /**
     * 属性 [ID]
     *
     */
    @Fleet_vehicle_odometerIdDefault(info = "默认规则")
    private Integer id;

    @JsonIgnore
    private boolean idDirtyFlag;

    /**
     * 属性 [WRITE_DATE]
     *
     */
    @Fleet_vehicle_odometerWrite_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp write_date;

    @JsonIgnore
    private boolean write_dateDirtyFlag;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @Fleet_vehicle_odometerDisplay_nameDefault(info = "默认规则")
    private String display_name;

    @JsonIgnore
    private boolean display_nameDirtyFlag;

    /**
     * 属性 [VALUE]
     *
     */
    @Fleet_vehicle_odometerValueDefault(info = "默认规则")
    private Double value;

    @JsonIgnore
    private boolean valueDirtyFlag;

    /**
     * 属性 [CREATE_DATE]
     *
     */
    @Fleet_vehicle_odometerCreate_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp create_date;

    @JsonIgnore
    private boolean create_dateDirtyFlag;

    /**
     * 属性 [VEHICLE_ID_TEXT]
     *
     */
    @Fleet_vehicle_odometerVehicle_id_textDefault(info = "默认规则")
    private String vehicle_id_text;

    @JsonIgnore
    private boolean vehicle_id_textDirtyFlag;

    /**
     * 属性 [CREATE_UID_TEXT]
     *
     */
    @Fleet_vehicle_odometerCreate_uid_textDefault(info = "默认规则")
    private String create_uid_text;

    @JsonIgnore
    private boolean create_uid_textDirtyFlag;

    /**
     * 属性 [UNIT]
     *
     */
    @Fleet_vehicle_odometerUnitDefault(info = "默认规则")
    private String unit;

    @JsonIgnore
    private boolean unitDirtyFlag;

    /**
     * 属性 [DRIVER_ID]
     *
     */
    @Fleet_vehicle_odometerDriver_idDefault(info = "默认规则")
    private Integer driver_id;

    @JsonIgnore
    private boolean driver_idDirtyFlag;

    /**
     * 属性 [WRITE_UID_TEXT]
     *
     */
    @Fleet_vehicle_odometerWrite_uid_textDefault(info = "默认规则")
    private String write_uid_text;

    @JsonIgnore
    private boolean write_uid_textDirtyFlag;

    /**
     * 属性 [VEHICLE_ID]
     *
     */
    @Fleet_vehicle_odometerVehicle_idDefault(info = "默认规则")
    private Integer vehicle_id;

    @JsonIgnore
    private boolean vehicle_idDirtyFlag;

    /**
     * 属性 [CREATE_UID]
     *
     */
    @Fleet_vehicle_odometerCreate_uidDefault(info = "默认规则")
    private Integer create_uid;

    @JsonIgnore
    private boolean create_uidDirtyFlag;

    /**
     * 属性 [WRITE_UID]
     *
     */
    @Fleet_vehicle_odometerWrite_uidDefault(info = "默认规则")
    private Integer write_uid;

    @JsonIgnore
    private boolean write_uidDirtyFlag;


    /**
     * 获取 [NAME]
     */
    @JsonProperty("name")
    public String getName(){
        return name ;
    }

    /**
     * 设置 [NAME]
     */
    @JsonProperty("name")
    public void setName(String  name){
        this.name = name ;
        this.nameDirtyFlag = true ;
    }

    /**
     * 获取 [NAME]脏标记
     */
    @JsonIgnore
    public boolean getNameDirtyFlag(){
        return nameDirtyFlag ;
    }

    /**
     * 获取 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return __last_update ;
    }

    /**
     * 设置 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

    /**
     * 获取 [__LAST_UPDATE]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return __last_updateDirtyFlag ;
    }

    /**
     * 获取 [DATE]
     */
    @JsonProperty("date")
    public Timestamp getDate(){
        return date ;
    }

    /**
     * 设置 [DATE]
     */
    @JsonProperty("date")
    public void setDate(Timestamp  date){
        this.date = date ;
        this.dateDirtyFlag = true ;
    }

    /**
     * 获取 [DATE]脏标记
     */
    @JsonIgnore
    public boolean getDateDirtyFlag(){
        return dateDirtyFlag ;
    }

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

    /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return idDirtyFlag ;
    }

    /**
     * 获取 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return write_date ;
    }

    /**
     * 设置 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return write_dateDirtyFlag ;
    }

    /**
     * 获取 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return display_name ;
    }

    /**
     * 设置 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

    /**
     * 获取 [DISPLAY_NAME]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return display_nameDirtyFlag ;
    }

    /**
     * 获取 [VALUE]
     */
    @JsonProperty("value")
    public Double getValue(){
        return value ;
    }

    /**
     * 设置 [VALUE]
     */
    @JsonProperty("value")
    public void setValue(Double  value){
        this.value = value ;
        this.valueDirtyFlag = true ;
    }

    /**
     * 获取 [VALUE]脏标记
     */
    @JsonIgnore
    public boolean getValueDirtyFlag(){
        return valueDirtyFlag ;
    }

    /**
     * 获取 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return create_date ;
    }

    /**
     * 设置 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return create_dateDirtyFlag ;
    }

    /**
     * 获取 [VEHICLE_ID_TEXT]
     */
    @JsonProperty("vehicle_id_text")
    public String getVehicle_id_text(){
        return vehicle_id_text ;
    }

    /**
     * 设置 [VEHICLE_ID_TEXT]
     */
    @JsonProperty("vehicle_id_text")
    public void setVehicle_id_text(String  vehicle_id_text){
        this.vehicle_id_text = vehicle_id_text ;
        this.vehicle_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [VEHICLE_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getVehicle_id_textDirtyFlag(){
        return vehicle_id_textDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return create_uid_text ;
    }

    /**
     * 设置 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return create_uid_textDirtyFlag ;
    }

    /**
     * 获取 [UNIT]
     */
    @JsonProperty("unit")
    public String getUnit(){
        return unit ;
    }

    /**
     * 设置 [UNIT]
     */
    @JsonProperty("unit")
    public void setUnit(String  unit){
        this.unit = unit ;
        this.unitDirtyFlag = true ;
    }

    /**
     * 获取 [UNIT]脏标记
     */
    @JsonIgnore
    public boolean getUnitDirtyFlag(){
        return unitDirtyFlag ;
    }

    /**
     * 获取 [DRIVER_ID]
     */
    @JsonProperty("driver_id")
    public Integer getDriver_id(){
        return driver_id ;
    }

    /**
     * 设置 [DRIVER_ID]
     */
    @JsonProperty("driver_id")
    public void setDriver_id(Integer  driver_id){
        this.driver_id = driver_id ;
        this.driver_idDirtyFlag = true ;
    }

    /**
     * 获取 [DRIVER_ID]脏标记
     */
    @JsonIgnore
    public boolean getDriver_idDirtyFlag(){
        return driver_idDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return write_uid_text ;
    }

    /**
     * 设置 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return write_uid_textDirtyFlag ;
    }

    /**
     * 获取 [VEHICLE_ID]
     */
    @JsonProperty("vehicle_id")
    public Integer getVehicle_id(){
        return vehicle_id ;
    }

    /**
     * 设置 [VEHICLE_ID]
     */
    @JsonProperty("vehicle_id")
    public void setVehicle_id(Integer  vehicle_id){
        this.vehicle_id = vehicle_id ;
        this.vehicle_idDirtyFlag = true ;
    }

    /**
     * 获取 [VEHICLE_ID]脏标记
     */
    @JsonIgnore
    public boolean getVehicle_idDirtyFlag(){
        return vehicle_idDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return create_uid ;
    }

    /**
     * 设置 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return create_uidDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return write_uid ;
    }

    /**
     * 设置 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return write_uidDirtyFlag ;
    }



    public Fleet_vehicle_odometer toDO() {
        Fleet_vehicle_odometer srfdomain = new Fleet_vehicle_odometer();
        if(getNameDirtyFlag())
            srfdomain.setName(name);
        if(get__last_updateDirtyFlag())
            srfdomain.set__last_update(__last_update);
        if(getDateDirtyFlag())
            srfdomain.setDate(date);
        if(getIdDirtyFlag())
            srfdomain.setId(id);
        if(getWrite_dateDirtyFlag())
            srfdomain.setWrite_date(write_date);
        if(getDisplay_nameDirtyFlag())
            srfdomain.setDisplay_name(display_name);
        if(getValueDirtyFlag())
            srfdomain.setValue(value);
        if(getCreate_dateDirtyFlag())
            srfdomain.setCreate_date(create_date);
        if(getVehicle_id_textDirtyFlag())
            srfdomain.setVehicle_id_text(vehicle_id_text);
        if(getCreate_uid_textDirtyFlag())
            srfdomain.setCreate_uid_text(create_uid_text);
        if(getUnitDirtyFlag())
            srfdomain.setUnit(unit);
        if(getDriver_idDirtyFlag())
            srfdomain.setDriver_id(driver_id);
        if(getWrite_uid_textDirtyFlag())
            srfdomain.setWrite_uid_text(write_uid_text);
        if(getVehicle_idDirtyFlag())
            srfdomain.setVehicle_id(vehicle_id);
        if(getCreate_uidDirtyFlag())
            srfdomain.setCreate_uid(create_uid);
        if(getWrite_uidDirtyFlag())
            srfdomain.setWrite_uid(write_uid);

        return srfdomain;
    }

    public void fromDO(Fleet_vehicle_odometer srfdomain) {
        if(srfdomain == null )
          return ;
        if(srfdomain.getNameDirtyFlag())
            this.setName(srfdomain.getName());
        if(srfdomain.get__last_updateDirtyFlag())
            this.set__last_update(srfdomain.get__last_update());
        if(srfdomain.getDateDirtyFlag())
            this.setDate(srfdomain.getDate());
        if(srfdomain.getIdDirtyFlag())
            this.setId(srfdomain.getId());
        if(srfdomain.getWrite_dateDirtyFlag())
            this.setWrite_date(srfdomain.getWrite_date());
        if(srfdomain.getDisplay_nameDirtyFlag())
            this.setDisplay_name(srfdomain.getDisplay_name());
        if(srfdomain.getValueDirtyFlag())
            this.setValue(srfdomain.getValue());
        if(srfdomain.getCreate_dateDirtyFlag())
            this.setCreate_date(srfdomain.getCreate_date());
        if(srfdomain.getVehicle_id_textDirtyFlag())
            this.setVehicle_id_text(srfdomain.getVehicle_id_text());
        if(srfdomain.getCreate_uid_textDirtyFlag())
            this.setCreate_uid_text(srfdomain.getCreate_uid_text());
        if(srfdomain.getUnitDirtyFlag())
            this.setUnit(srfdomain.getUnit());
        if(srfdomain.getDriver_idDirtyFlag())
            this.setDriver_id(srfdomain.getDriver_id());
        if(srfdomain.getWrite_uid_textDirtyFlag())
            this.setWrite_uid_text(srfdomain.getWrite_uid_text());
        if(srfdomain.getVehicle_idDirtyFlag())
            this.setVehicle_id(srfdomain.getVehicle_id());
        if(srfdomain.getCreate_uidDirtyFlag())
            this.setCreate_uid(srfdomain.getCreate_uid());
        if(srfdomain.getWrite_uidDirtyFlag())
            this.setWrite_uid(srfdomain.getWrite_uid());

    }

    public List<Fleet_vehicle_odometerDTO> fromDOPage(List<Fleet_vehicle_odometer> poPage)   {
        if(poPage == null)
            return null;
        List<Fleet_vehicle_odometerDTO> dtos=new ArrayList<Fleet_vehicle_odometerDTO>();
        for(Fleet_vehicle_odometer domain : poPage) {
            Fleet_vehicle_odometerDTO dto = new Fleet_vehicle_odometerDTO();
            dto.fromDO(domain);
            dtos.add(dto);
        }
        return dtos;
    }
}

