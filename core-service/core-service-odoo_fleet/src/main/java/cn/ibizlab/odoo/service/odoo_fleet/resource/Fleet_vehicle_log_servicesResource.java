package cn.ibizlab.odoo.service.odoo_fleet.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_fleet.dto.Fleet_vehicle_log_servicesDTO;
import cn.ibizlab.odoo.core.odoo_fleet.domain.Fleet_vehicle_log_services;
import cn.ibizlab.odoo.core.odoo_fleet.service.IFleet_vehicle_log_servicesService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_fleet.filter.Fleet_vehicle_log_servicesSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Fleet_vehicle_log_services" })
@RestController
@RequestMapping("")
public class Fleet_vehicle_log_servicesResource {

    @Autowired
    private IFleet_vehicle_log_servicesService fleet_vehicle_log_servicesService;

    public IFleet_vehicle_log_servicesService getFleet_vehicle_log_servicesService() {
        return this.fleet_vehicle_log_servicesService;
    }

    @ApiOperation(value = "批建立数据", tags = {"Fleet_vehicle_log_services" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_fleet/fleet_vehicle_log_services/createBatch")
    public ResponseEntity<Boolean> createBatchFleet_vehicle_log_services(@RequestBody List<Fleet_vehicle_log_servicesDTO> fleet_vehicle_log_servicesdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "批更新数据", tags = {"Fleet_vehicle_log_services" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_fleet/fleet_vehicle_log_services/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Fleet_vehicle_log_servicesDTO> fleet_vehicle_log_servicesdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "批删除数据", tags = {"Fleet_vehicle_log_services" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_fleet/fleet_vehicle_log_services/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Fleet_vehicle_log_servicesDTO> fleet_vehicle_log_servicesdtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "建立数据", tags = {"Fleet_vehicle_log_services" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_fleet/fleet_vehicle_log_services")

    public ResponseEntity<Fleet_vehicle_log_servicesDTO> create(@RequestBody Fleet_vehicle_log_servicesDTO fleet_vehicle_log_servicesdto) {
        Fleet_vehicle_log_servicesDTO dto = new Fleet_vehicle_log_servicesDTO();
        Fleet_vehicle_log_services domain = fleet_vehicle_log_servicesdto.toDO();
		fleet_vehicle_log_servicesService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "删除数据", tags = {"Fleet_vehicle_log_services" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_fleet/fleet_vehicle_log_services/{fleet_vehicle_log_services_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("fleet_vehicle_log_services_id") Integer fleet_vehicle_log_services_id) {
        Fleet_vehicle_log_servicesDTO fleet_vehicle_log_servicesdto = new Fleet_vehicle_log_servicesDTO();
		Fleet_vehicle_log_services domain = new Fleet_vehicle_log_services();
		fleet_vehicle_log_servicesdto.setId(fleet_vehicle_log_services_id);
		domain.setId(fleet_vehicle_log_services_id);
        Boolean rst = fleet_vehicle_log_servicesService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "更新数据", tags = {"Fleet_vehicle_log_services" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_fleet/fleet_vehicle_log_services/{fleet_vehicle_log_services_id}")

    public ResponseEntity<Fleet_vehicle_log_servicesDTO> update(@PathVariable("fleet_vehicle_log_services_id") Integer fleet_vehicle_log_services_id, @RequestBody Fleet_vehicle_log_servicesDTO fleet_vehicle_log_servicesdto) {
		Fleet_vehicle_log_services domain = fleet_vehicle_log_servicesdto.toDO();
        domain.setId(fleet_vehicle_log_services_id);
		fleet_vehicle_log_servicesService.update(domain);
		Fleet_vehicle_log_servicesDTO dto = new Fleet_vehicle_log_servicesDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取数据", tags = {"Fleet_vehicle_log_services" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_fleet/fleet_vehicle_log_services/{fleet_vehicle_log_services_id}")
    public ResponseEntity<Fleet_vehicle_log_servicesDTO> get(@PathVariable("fleet_vehicle_log_services_id") Integer fleet_vehicle_log_services_id) {
        Fleet_vehicle_log_servicesDTO dto = new Fleet_vehicle_log_servicesDTO();
        Fleet_vehicle_log_services domain = fleet_vehicle_log_servicesService.get(fleet_vehicle_log_services_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Fleet_vehicle_log_services" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_fleet/fleet_vehicle_log_services/fetchdefault")
	public ResponseEntity<Page<Fleet_vehicle_log_servicesDTO>> fetchDefault(Fleet_vehicle_log_servicesSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Fleet_vehicle_log_servicesDTO> list = new ArrayList<Fleet_vehicle_log_servicesDTO>();
        
        Page<Fleet_vehicle_log_services> domains = fleet_vehicle_log_servicesService.searchDefault(context) ;
        for(Fleet_vehicle_log_services fleet_vehicle_log_services : domains.getContent()){
            Fleet_vehicle_log_servicesDTO dto = new Fleet_vehicle_log_servicesDTO();
            dto.fromDO(fleet_vehicle_log_services);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
