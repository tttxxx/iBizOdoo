package cn.ibizlab.odoo.service.odoo_fleet.config;

import org.springframework.context.annotation.ComponentScan;

@ComponentScan("cn.ibizlab.odoo.service.odoo_fleet")
public class odoo_fleetRestConfiguration {

}
