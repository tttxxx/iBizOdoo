package cn.ibizlab.odoo.service.odoo_fleet.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_fleet.dto.Fleet_vehicle_assignation_logDTO;
import cn.ibizlab.odoo.core.odoo_fleet.domain.Fleet_vehicle_assignation_log;
import cn.ibizlab.odoo.core.odoo_fleet.service.IFleet_vehicle_assignation_logService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_fleet.filter.Fleet_vehicle_assignation_logSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Fleet_vehicle_assignation_log" })
@RestController
@RequestMapping("")
public class Fleet_vehicle_assignation_logResource {

    @Autowired
    private IFleet_vehicle_assignation_logService fleet_vehicle_assignation_logService;

    public IFleet_vehicle_assignation_logService getFleet_vehicle_assignation_logService() {
        return this.fleet_vehicle_assignation_logService;
    }

    @ApiOperation(value = "批删除数据", tags = {"Fleet_vehicle_assignation_log" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_fleet/fleet_vehicle_assignation_logs/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Fleet_vehicle_assignation_logDTO> fleet_vehicle_assignation_logdtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "批更新数据", tags = {"Fleet_vehicle_assignation_log" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_fleet/fleet_vehicle_assignation_logs/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Fleet_vehicle_assignation_logDTO> fleet_vehicle_assignation_logdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "获取数据", tags = {"Fleet_vehicle_assignation_log" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_fleet/fleet_vehicle_assignation_logs/{fleet_vehicle_assignation_log_id}")
    public ResponseEntity<Fleet_vehicle_assignation_logDTO> get(@PathVariable("fleet_vehicle_assignation_log_id") Integer fleet_vehicle_assignation_log_id) {
        Fleet_vehicle_assignation_logDTO dto = new Fleet_vehicle_assignation_logDTO();
        Fleet_vehicle_assignation_log domain = fleet_vehicle_assignation_logService.get(fleet_vehicle_assignation_log_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "建立数据", tags = {"Fleet_vehicle_assignation_log" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_fleet/fleet_vehicle_assignation_logs")

    public ResponseEntity<Fleet_vehicle_assignation_logDTO> create(@RequestBody Fleet_vehicle_assignation_logDTO fleet_vehicle_assignation_logdto) {
        Fleet_vehicle_assignation_logDTO dto = new Fleet_vehicle_assignation_logDTO();
        Fleet_vehicle_assignation_log domain = fleet_vehicle_assignation_logdto.toDO();
		fleet_vehicle_assignation_logService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "删除数据", tags = {"Fleet_vehicle_assignation_log" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_fleet/fleet_vehicle_assignation_logs/{fleet_vehicle_assignation_log_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("fleet_vehicle_assignation_log_id") Integer fleet_vehicle_assignation_log_id) {
        Fleet_vehicle_assignation_logDTO fleet_vehicle_assignation_logdto = new Fleet_vehicle_assignation_logDTO();
		Fleet_vehicle_assignation_log domain = new Fleet_vehicle_assignation_log();
		fleet_vehicle_assignation_logdto.setId(fleet_vehicle_assignation_log_id);
		domain.setId(fleet_vehicle_assignation_log_id);
        Boolean rst = fleet_vehicle_assignation_logService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "批建立数据", tags = {"Fleet_vehicle_assignation_log" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_fleet/fleet_vehicle_assignation_logs/createBatch")
    public ResponseEntity<Boolean> createBatchFleet_vehicle_assignation_log(@RequestBody List<Fleet_vehicle_assignation_logDTO> fleet_vehicle_assignation_logdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "更新数据", tags = {"Fleet_vehicle_assignation_log" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_fleet/fleet_vehicle_assignation_logs/{fleet_vehicle_assignation_log_id}")

    public ResponseEntity<Fleet_vehicle_assignation_logDTO> update(@PathVariable("fleet_vehicle_assignation_log_id") Integer fleet_vehicle_assignation_log_id, @RequestBody Fleet_vehicle_assignation_logDTO fleet_vehicle_assignation_logdto) {
		Fleet_vehicle_assignation_log domain = fleet_vehicle_assignation_logdto.toDO();
        domain.setId(fleet_vehicle_assignation_log_id);
		fleet_vehicle_assignation_logService.update(domain);
		Fleet_vehicle_assignation_logDTO dto = new Fleet_vehicle_assignation_logDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Fleet_vehicle_assignation_log" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_fleet/fleet_vehicle_assignation_logs/fetchdefault")
	public ResponseEntity<Page<Fleet_vehicle_assignation_logDTO>> fetchDefault(Fleet_vehicle_assignation_logSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Fleet_vehicle_assignation_logDTO> list = new ArrayList<Fleet_vehicle_assignation_logDTO>();
        
        Page<Fleet_vehicle_assignation_log> domains = fleet_vehicle_assignation_logService.searchDefault(context) ;
        for(Fleet_vehicle_assignation_log fleet_vehicle_assignation_log : domains.getContent()){
            Fleet_vehicle_assignation_logDTO dto = new Fleet_vehicle_assignation_logDTO();
            dto.fromDO(fleet_vehicle_assignation_log);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
