package cn.ibizlab.odoo.service.odoo_fleet.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.core.odoo_fleet.valuerule.anno.fleet_vehicle_cost.*;
import cn.ibizlab.odoo.core.odoo_fleet.domain.Fleet_vehicle_cost;
import org.springframework.cglib.beans.BeanCopier;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 服务DTO对象[Fleet_vehicle_costDTO]
 */
public class Fleet_vehicle_costDTO implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @Fleet_vehicle_cost__last_updateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp __last_update;

    @JsonIgnore
    private boolean __last_updateDirtyFlag;

    /**
     * 属性 [AMOUNT]
     *
     */
    @Fleet_vehicle_costAmountDefault(info = "默认规则")
    private Double amount;

    @JsonIgnore
    private boolean amountDirtyFlag;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @Fleet_vehicle_costDisplay_nameDefault(info = "默认规则")
    private String display_name;

    @JsonIgnore
    private boolean display_nameDirtyFlag;

    /**
     * 属性 [CREATE_DATE]
     *
     */
    @Fleet_vehicle_costCreate_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp create_date;

    @JsonIgnore
    private boolean create_dateDirtyFlag;

    /**
     * 属性 [ODOMETER]
     *
     */
    @Fleet_vehicle_costOdometerDefault(info = "默认规则")
    private Double odometer;

    @JsonIgnore
    private boolean odometerDirtyFlag;

    /**
     * 属性 [ID]
     *
     */
    @Fleet_vehicle_costIdDefault(info = "默认规则")
    private Integer id;

    @JsonIgnore
    private boolean idDirtyFlag;

    /**
     * 属性 [WRITE_DATE]
     *
     */
    @Fleet_vehicle_costWrite_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp write_date;

    @JsonIgnore
    private boolean write_dateDirtyFlag;

    /**
     * 属性 [COST_TYPE]
     *
     */
    @Fleet_vehicle_costCost_typeDefault(info = "默认规则")
    private String cost_type;

    @JsonIgnore
    private boolean cost_typeDirtyFlag;

    /**
     * 属性 [DESCRIPTION]
     *
     */
    @Fleet_vehicle_costDescriptionDefault(info = "默认规则")
    private String description;

    @JsonIgnore
    private boolean descriptionDirtyFlag;

    /**
     * 属性 [AUTO_GENERATED]
     *
     */
    @Fleet_vehicle_costAuto_generatedDefault(info = "默认规则")
    private String auto_generated;

    @JsonIgnore
    private boolean auto_generatedDirtyFlag;

    /**
     * 属性 [DATE]
     *
     */
    @Fleet_vehicle_costDateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    private Timestamp date;

    @JsonIgnore
    private boolean dateDirtyFlag;

    /**
     * 属性 [COST_IDS]
     *
     */
    @Fleet_vehicle_costCost_idsDefault(info = "默认规则")
    private String cost_ids;

    @JsonIgnore
    private boolean cost_idsDirtyFlag;

    /**
     * 属性 [CONTRACT_ID_TEXT]
     *
     */
    @Fleet_vehicle_costContract_id_textDefault(info = "默认规则")
    private String contract_id_text;

    @JsonIgnore
    private boolean contract_id_textDirtyFlag;

    /**
     * 属性 [NAME]
     *
     */
    @Fleet_vehicle_costNameDefault(info = "默认规则")
    private String name;

    @JsonIgnore
    private boolean nameDirtyFlag;

    /**
     * 属性 [PARENT_ID_TEXT]
     *
     */
    @Fleet_vehicle_costParent_id_textDefault(info = "默认规则")
    private String parent_id_text;

    @JsonIgnore
    private boolean parent_id_textDirtyFlag;

    /**
     * 属性 [COST_SUBTYPE_ID_TEXT]
     *
     */
    @Fleet_vehicle_costCost_subtype_id_textDefault(info = "默认规则")
    private String cost_subtype_id_text;

    @JsonIgnore
    private boolean cost_subtype_id_textDirtyFlag;

    /**
     * 属性 [CREATE_UID_TEXT]
     *
     */
    @Fleet_vehicle_costCreate_uid_textDefault(info = "默认规则")
    private String create_uid_text;

    @JsonIgnore
    private boolean create_uid_textDirtyFlag;

    /**
     * 属性 [ODOMETER_UNIT]
     *
     */
    @Fleet_vehicle_costOdometer_unitDefault(info = "默认规则")
    private String odometer_unit;

    @JsonIgnore
    private boolean odometer_unitDirtyFlag;

    /**
     * 属性 [ODOMETER_ID_TEXT]
     *
     */
    @Fleet_vehicle_costOdometer_id_textDefault(info = "默认规则")
    private String odometer_id_text;

    @JsonIgnore
    private boolean odometer_id_textDirtyFlag;

    /**
     * 属性 [WRITE_UID_TEXT]
     *
     */
    @Fleet_vehicle_costWrite_uid_textDefault(info = "默认规则")
    private String write_uid_text;

    @JsonIgnore
    private boolean write_uid_textDirtyFlag;

    /**
     * 属性 [COST_SUBTYPE_ID]
     *
     */
    @Fleet_vehicle_costCost_subtype_idDefault(info = "默认规则")
    private Integer cost_subtype_id;

    @JsonIgnore
    private boolean cost_subtype_idDirtyFlag;

    /**
     * 属性 [CONTRACT_ID]
     *
     */
    @Fleet_vehicle_costContract_idDefault(info = "默认规则")
    private Integer contract_id;

    @JsonIgnore
    private boolean contract_idDirtyFlag;

    /**
     * 属性 [WRITE_UID]
     *
     */
    @Fleet_vehicle_costWrite_uidDefault(info = "默认规则")
    private Integer write_uid;

    @JsonIgnore
    private boolean write_uidDirtyFlag;

    /**
     * 属性 [CREATE_UID]
     *
     */
    @Fleet_vehicle_costCreate_uidDefault(info = "默认规则")
    private Integer create_uid;

    @JsonIgnore
    private boolean create_uidDirtyFlag;

    /**
     * 属性 [PARENT_ID]
     *
     */
    @Fleet_vehicle_costParent_idDefault(info = "默认规则")
    private Integer parent_id;

    @JsonIgnore
    private boolean parent_idDirtyFlag;

    /**
     * 属性 [VEHICLE_ID]
     *
     */
    @Fleet_vehicle_costVehicle_idDefault(info = "默认规则")
    private Integer vehicle_id;

    @JsonIgnore
    private boolean vehicle_idDirtyFlag;

    /**
     * 属性 [ODOMETER_ID]
     *
     */
    @Fleet_vehicle_costOdometer_idDefault(info = "默认规则")
    private Integer odometer_id;

    @JsonIgnore
    private boolean odometer_idDirtyFlag;


    /**
     * 获取 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return __last_update ;
    }

    /**
     * 设置 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

    /**
     * 获取 [__LAST_UPDATE]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return __last_updateDirtyFlag ;
    }

    /**
     * 获取 [AMOUNT]
     */
    @JsonProperty("amount")
    public Double getAmount(){
        return amount ;
    }

    /**
     * 设置 [AMOUNT]
     */
    @JsonProperty("amount")
    public void setAmount(Double  amount){
        this.amount = amount ;
        this.amountDirtyFlag = true ;
    }

    /**
     * 获取 [AMOUNT]脏标记
     */
    @JsonIgnore
    public boolean getAmountDirtyFlag(){
        return amountDirtyFlag ;
    }

    /**
     * 获取 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return display_name ;
    }

    /**
     * 设置 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

    /**
     * 获取 [DISPLAY_NAME]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return display_nameDirtyFlag ;
    }

    /**
     * 获取 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return create_date ;
    }

    /**
     * 设置 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return create_dateDirtyFlag ;
    }

    /**
     * 获取 [ODOMETER]
     */
    @JsonProperty("odometer")
    public Double getOdometer(){
        return odometer ;
    }

    /**
     * 设置 [ODOMETER]
     */
    @JsonProperty("odometer")
    public void setOdometer(Double  odometer){
        this.odometer = odometer ;
        this.odometerDirtyFlag = true ;
    }

    /**
     * 获取 [ODOMETER]脏标记
     */
    @JsonIgnore
    public boolean getOdometerDirtyFlag(){
        return odometerDirtyFlag ;
    }

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

    /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return idDirtyFlag ;
    }

    /**
     * 获取 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return write_date ;
    }

    /**
     * 设置 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return write_dateDirtyFlag ;
    }

    /**
     * 获取 [COST_TYPE]
     */
    @JsonProperty("cost_type")
    public String getCost_type(){
        return cost_type ;
    }

    /**
     * 设置 [COST_TYPE]
     */
    @JsonProperty("cost_type")
    public void setCost_type(String  cost_type){
        this.cost_type = cost_type ;
        this.cost_typeDirtyFlag = true ;
    }

    /**
     * 获取 [COST_TYPE]脏标记
     */
    @JsonIgnore
    public boolean getCost_typeDirtyFlag(){
        return cost_typeDirtyFlag ;
    }

    /**
     * 获取 [DESCRIPTION]
     */
    @JsonProperty("description")
    public String getDescription(){
        return description ;
    }

    /**
     * 设置 [DESCRIPTION]
     */
    @JsonProperty("description")
    public void setDescription(String  description){
        this.description = description ;
        this.descriptionDirtyFlag = true ;
    }

    /**
     * 获取 [DESCRIPTION]脏标记
     */
    @JsonIgnore
    public boolean getDescriptionDirtyFlag(){
        return descriptionDirtyFlag ;
    }

    /**
     * 获取 [AUTO_GENERATED]
     */
    @JsonProperty("auto_generated")
    public String getAuto_generated(){
        return auto_generated ;
    }

    /**
     * 设置 [AUTO_GENERATED]
     */
    @JsonProperty("auto_generated")
    public void setAuto_generated(String  auto_generated){
        this.auto_generated = auto_generated ;
        this.auto_generatedDirtyFlag = true ;
    }

    /**
     * 获取 [AUTO_GENERATED]脏标记
     */
    @JsonIgnore
    public boolean getAuto_generatedDirtyFlag(){
        return auto_generatedDirtyFlag ;
    }

    /**
     * 获取 [DATE]
     */
    @JsonProperty("date")
    public Timestamp getDate(){
        return date ;
    }

    /**
     * 设置 [DATE]
     */
    @JsonProperty("date")
    public void setDate(Timestamp  date){
        this.date = date ;
        this.dateDirtyFlag = true ;
    }

    /**
     * 获取 [DATE]脏标记
     */
    @JsonIgnore
    public boolean getDateDirtyFlag(){
        return dateDirtyFlag ;
    }

    /**
     * 获取 [COST_IDS]
     */
    @JsonProperty("cost_ids")
    public String getCost_ids(){
        return cost_ids ;
    }

    /**
     * 设置 [COST_IDS]
     */
    @JsonProperty("cost_ids")
    public void setCost_ids(String  cost_ids){
        this.cost_ids = cost_ids ;
        this.cost_idsDirtyFlag = true ;
    }

    /**
     * 获取 [COST_IDS]脏标记
     */
    @JsonIgnore
    public boolean getCost_idsDirtyFlag(){
        return cost_idsDirtyFlag ;
    }

    /**
     * 获取 [CONTRACT_ID_TEXT]
     */
    @JsonProperty("contract_id_text")
    public String getContract_id_text(){
        return contract_id_text ;
    }

    /**
     * 设置 [CONTRACT_ID_TEXT]
     */
    @JsonProperty("contract_id_text")
    public void setContract_id_text(String  contract_id_text){
        this.contract_id_text = contract_id_text ;
        this.contract_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [CONTRACT_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getContract_id_textDirtyFlag(){
        return contract_id_textDirtyFlag ;
    }

    /**
     * 获取 [NAME]
     */
    @JsonProperty("name")
    public String getName(){
        return name ;
    }

    /**
     * 设置 [NAME]
     */
    @JsonProperty("name")
    public void setName(String  name){
        this.name = name ;
        this.nameDirtyFlag = true ;
    }

    /**
     * 获取 [NAME]脏标记
     */
    @JsonIgnore
    public boolean getNameDirtyFlag(){
        return nameDirtyFlag ;
    }

    /**
     * 获取 [PARENT_ID_TEXT]
     */
    @JsonProperty("parent_id_text")
    public String getParent_id_text(){
        return parent_id_text ;
    }

    /**
     * 设置 [PARENT_ID_TEXT]
     */
    @JsonProperty("parent_id_text")
    public void setParent_id_text(String  parent_id_text){
        this.parent_id_text = parent_id_text ;
        this.parent_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [PARENT_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getParent_id_textDirtyFlag(){
        return parent_id_textDirtyFlag ;
    }

    /**
     * 获取 [COST_SUBTYPE_ID_TEXT]
     */
    @JsonProperty("cost_subtype_id_text")
    public String getCost_subtype_id_text(){
        return cost_subtype_id_text ;
    }

    /**
     * 设置 [COST_SUBTYPE_ID_TEXT]
     */
    @JsonProperty("cost_subtype_id_text")
    public void setCost_subtype_id_text(String  cost_subtype_id_text){
        this.cost_subtype_id_text = cost_subtype_id_text ;
        this.cost_subtype_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [COST_SUBTYPE_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getCost_subtype_id_textDirtyFlag(){
        return cost_subtype_id_textDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return create_uid_text ;
    }

    /**
     * 设置 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return create_uid_textDirtyFlag ;
    }

    /**
     * 获取 [ODOMETER_UNIT]
     */
    @JsonProperty("odometer_unit")
    public String getOdometer_unit(){
        return odometer_unit ;
    }

    /**
     * 设置 [ODOMETER_UNIT]
     */
    @JsonProperty("odometer_unit")
    public void setOdometer_unit(String  odometer_unit){
        this.odometer_unit = odometer_unit ;
        this.odometer_unitDirtyFlag = true ;
    }

    /**
     * 获取 [ODOMETER_UNIT]脏标记
     */
    @JsonIgnore
    public boolean getOdometer_unitDirtyFlag(){
        return odometer_unitDirtyFlag ;
    }

    /**
     * 获取 [ODOMETER_ID_TEXT]
     */
    @JsonProperty("odometer_id_text")
    public String getOdometer_id_text(){
        return odometer_id_text ;
    }

    /**
     * 设置 [ODOMETER_ID_TEXT]
     */
    @JsonProperty("odometer_id_text")
    public void setOdometer_id_text(String  odometer_id_text){
        this.odometer_id_text = odometer_id_text ;
        this.odometer_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [ODOMETER_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getOdometer_id_textDirtyFlag(){
        return odometer_id_textDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return write_uid_text ;
    }

    /**
     * 设置 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return write_uid_textDirtyFlag ;
    }

    /**
     * 获取 [COST_SUBTYPE_ID]
     */
    @JsonProperty("cost_subtype_id")
    public Integer getCost_subtype_id(){
        return cost_subtype_id ;
    }

    /**
     * 设置 [COST_SUBTYPE_ID]
     */
    @JsonProperty("cost_subtype_id")
    public void setCost_subtype_id(Integer  cost_subtype_id){
        this.cost_subtype_id = cost_subtype_id ;
        this.cost_subtype_idDirtyFlag = true ;
    }

    /**
     * 获取 [COST_SUBTYPE_ID]脏标记
     */
    @JsonIgnore
    public boolean getCost_subtype_idDirtyFlag(){
        return cost_subtype_idDirtyFlag ;
    }

    /**
     * 获取 [CONTRACT_ID]
     */
    @JsonProperty("contract_id")
    public Integer getContract_id(){
        return contract_id ;
    }

    /**
     * 设置 [CONTRACT_ID]
     */
    @JsonProperty("contract_id")
    public void setContract_id(Integer  contract_id){
        this.contract_id = contract_id ;
        this.contract_idDirtyFlag = true ;
    }

    /**
     * 获取 [CONTRACT_ID]脏标记
     */
    @JsonIgnore
    public boolean getContract_idDirtyFlag(){
        return contract_idDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return write_uid ;
    }

    /**
     * 设置 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return write_uidDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return create_uid ;
    }

    /**
     * 设置 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return create_uidDirtyFlag ;
    }

    /**
     * 获取 [PARENT_ID]
     */
    @JsonProperty("parent_id")
    public Integer getParent_id(){
        return parent_id ;
    }

    /**
     * 设置 [PARENT_ID]
     */
    @JsonProperty("parent_id")
    public void setParent_id(Integer  parent_id){
        this.parent_id = parent_id ;
        this.parent_idDirtyFlag = true ;
    }

    /**
     * 获取 [PARENT_ID]脏标记
     */
    @JsonIgnore
    public boolean getParent_idDirtyFlag(){
        return parent_idDirtyFlag ;
    }

    /**
     * 获取 [VEHICLE_ID]
     */
    @JsonProperty("vehicle_id")
    public Integer getVehicle_id(){
        return vehicle_id ;
    }

    /**
     * 设置 [VEHICLE_ID]
     */
    @JsonProperty("vehicle_id")
    public void setVehicle_id(Integer  vehicle_id){
        this.vehicle_id = vehicle_id ;
        this.vehicle_idDirtyFlag = true ;
    }

    /**
     * 获取 [VEHICLE_ID]脏标记
     */
    @JsonIgnore
    public boolean getVehicle_idDirtyFlag(){
        return vehicle_idDirtyFlag ;
    }

    /**
     * 获取 [ODOMETER_ID]
     */
    @JsonProperty("odometer_id")
    public Integer getOdometer_id(){
        return odometer_id ;
    }

    /**
     * 设置 [ODOMETER_ID]
     */
    @JsonProperty("odometer_id")
    public void setOdometer_id(Integer  odometer_id){
        this.odometer_id = odometer_id ;
        this.odometer_idDirtyFlag = true ;
    }

    /**
     * 获取 [ODOMETER_ID]脏标记
     */
    @JsonIgnore
    public boolean getOdometer_idDirtyFlag(){
        return odometer_idDirtyFlag ;
    }



    public Fleet_vehicle_cost toDO() {
        Fleet_vehicle_cost srfdomain = new Fleet_vehicle_cost();
        if(get__last_updateDirtyFlag())
            srfdomain.set__last_update(__last_update);
        if(getAmountDirtyFlag())
            srfdomain.setAmount(amount);
        if(getDisplay_nameDirtyFlag())
            srfdomain.setDisplay_name(display_name);
        if(getCreate_dateDirtyFlag())
            srfdomain.setCreate_date(create_date);
        if(getOdometerDirtyFlag())
            srfdomain.setOdometer(odometer);
        if(getIdDirtyFlag())
            srfdomain.setId(id);
        if(getWrite_dateDirtyFlag())
            srfdomain.setWrite_date(write_date);
        if(getCost_typeDirtyFlag())
            srfdomain.setCost_type(cost_type);
        if(getDescriptionDirtyFlag())
            srfdomain.setDescription(description);
        if(getAuto_generatedDirtyFlag())
            srfdomain.setAuto_generated(auto_generated);
        if(getDateDirtyFlag())
            srfdomain.setDate(date);
        if(getCost_idsDirtyFlag())
            srfdomain.setCost_ids(cost_ids);
        if(getContract_id_textDirtyFlag())
            srfdomain.setContract_id_text(contract_id_text);
        if(getNameDirtyFlag())
            srfdomain.setName(name);
        if(getParent_id_textDirtyFlag())
            srfdomain.setParent_id_text(parent_id_text);
        if(getCost_subtype_id_textDirtyFlag())
            srfdomain.setCost_subtype_id_text(cost_subtype_id_text);
        if(getCreate_uid_textDirtyFlag())
            srfdomain.setCreate_uid_text(create_uid_text);
        if(getOdometer_unitDirtyFlag())
            srfdomain.setOdometer_unit(odometer_unit);
        if(getOdometer_id_textDirtyFlag())
            srfdomain.setOdometer_id_text(odometer_id_text);
        if(getWrite_uid_textDirtyFlag())
            srfdomain.setWrite_uid_text(write_uid_text);
        if(getCost_subtype_idDirtyFlag())
            srfdomain.setCost_subtype_id(cost_subtype_id);
        if(getContract_idDirtyFlag())
            srfdomain.setContract_id(contract_id);
        if(getWrite_uidDirtyFlag())
            srfdomain.setWrite_uid(write_uid);
        if(getCreate_uidDirtyFlag())
            srfdomain.setCreate_uid(create_uid);
        if(getParent_idDirtyFlag())
            srfdomain.setParent_id(parent_id);
        if(getVehicle_idDirtyFlag())
            srfdomain.setVehicle_id(vehicle_id);
        if(getOdometer_idDirtyFlag())
            srfdomain.setOdometer_id(odometer_id);

        return srfdomain;
    }

    public void fromDO(Fleet_vehicle_cost srfdomain) {
        if(srfdomain == null )
          return ;
        if(srfdomain.get__last_updateDirtyFlag())
            this.set__last_update(srfdomain.get__last_update());
        if(srfdomain.getAmountDirtyFlag())
            this.setAmount(srfdomain.getAmount());
        if(srfdomain.getDisplay_nameDirtyFlag())
            this.setDisplay_name(srfdomain.getDisplay_name());
        if(srfdomain.getCreate_dateDirtyFlag())
            this.setCreate_date(srfdomain.getCreate_date());
        if(srfdomain.getOdometerDirtyFlag())
            this.setOdometer(srfdomain.getOdometer());
        if(srfdomain.getIdDirtyFlag())
            this.setId(srfdomain.getId());
        if(srfdomain.getWrite_dateDirtyFlag())
            this.setWrite_date(srfdomain.getWrite_date());
        if(srfdomain.getCost_typeDirtyFlag())
            this.setCost_type(srfdomain.getCost_type());
        if(srfdomain.getDescriptionDirtyFlag())
            this.setDescription(srfdomain.getDescription());
        if(srfdomain.getAuto_generatedDirtyFlag())
            this.setAuto_generated(srfdomain.getAuto_generated());
        if(srfdomain.getDateDirtyFlag())
            this.setDate(srfdomain.getDate());
        if(srfdomain.getCost_idsDirtyFlag())
            this.setCost_ids(srfdomain.getCost_ids());
        if(srfdomain.getContract_id_textDirtyFlag())
            this.setContract_id_text(srfdomain.getContract_id_text());
        if(srfdomain.getNameDirtyFlag())
            this.setName(srfdomain.getName());
        if(srfdomain.getParent_id_textDirtyFlag())
            this.setParent_id_text(srfdomain.getParent_id_text());
        if(srfdomain.getCost_subtype_id_textDirtyFlag())
            this.setCost_subtype_id_text(srfdomain.getCost_subtype_id_text());
        if(srfdomain.getCreate_uid_textDirtyFlag())
            this.setCreate_uid_text(srfdomain.getCreate_uid_text());
        if(srfdomain.getOdometer_unitDirtyFlag())
            this.setOdometer_unit(srfdomain.getOdometer_unit());
        if(srfdomain.getOdometer_id_textDirtyFlag())
            this.setOdometer_id_text(srfdomain.getOdometer_id_text());
        if(srfdomain.getWrite_uid_textDirtyFlag())
            this.setWrite_uid_text(srfdomain.getWrite_uid_text());
        if(srfdomain.getCost_subtype_idDirtyFlag())
            this.setCost_subtype_id(srfdomain.getCost_subtype_id());
        if(srfdomain.getContract_idDirtyFlag())
            this.setContract_id(srfdomain.getContract_id());
        if(srfdomain.getWrite_uidDirtyFlag())
            this.setWrite_uid(srfdomain.getWrite_uid());
        if(srfdomain.getCreate_uidDirtyFlag())
            this.setCreate_uid(srfdomain.getCreate_uid());
        if(srfdomain.getParent_idDirtyFlag())
            this.setParent_id(srfdomain.getParent_id());
        if(srfdomain.getVehicle_idDirtyFlag())
            this.setVehicle_id(srfdomain.getVehicle_id());
        if(srfdomain.getOdometer_idDirtyFlag())
            this.setOdometer_id(srfdomain.getOdometer_id());

    }

    public List<Fleet_vehicle_costDTO> fromDOPage(List<Fleet_vehicle_cost> poPage)   {
        if(poPage == null)
            return null;
        List<Fleet_vehicle_costDTO> dtos=new ArrayList<Fleet_vehicle_costDTO>();
        for(Fleet_vehicle_cost domain : poPage) {
            Fleet_vehicle_costDTO dto = new Fleet_vehicle_costDTO();
            dto.fromDO(domain);
            dtos.add(dto);
        }
        return dtos;
    }
}

