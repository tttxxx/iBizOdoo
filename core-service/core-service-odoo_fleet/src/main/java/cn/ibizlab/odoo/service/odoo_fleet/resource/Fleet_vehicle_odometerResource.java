package cn.ibizlab.odoo.service.odoo_fleet.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_fleet.dto.Fleet_vehicle_odometerDTO;
import cn.ibizlab.odoo.core.odoo_fleet.domain.Fleet_vehicle_odometer;
import cn.ibizlab.odoo.core.odoo_fleet.service.IFleet_vehicle_odometerService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_fleet.filter.Fleet_vehicle_odometerSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Fleet_vehicle_odometer" })
@RestController
@RequestMapping("")
public class Fleet_vehicle_odometerResource {

    @Autowired
    private IFleet_vehicle_odometerService fleet_vehicle_odometerService;

    public IFleet_vehicle_odometerService getFleet_vehicle_odometerService() {
        return this.fleet_vehicle_odometerService;
    }

    @ApiOperation(value = "批建立数据", tags = {"Fleet_vehicle_odometer" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_fleet/fleet_vehicle_odometers/createBatch")
    public ResponseEntity<Boolean> createBatchFleet_vehicle_odometer(@RequestBody List<Fleet_vehicle_odometerDTO> fleet_vehicle_odometerdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "批删除数据", tags = {"Fleet_vehicle_odometer" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_fleet/fleet_vehicle_odometers/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Fleet_vehicle_odometerDTO> fleet_vehicle_odometerdtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "建立数据", tags = {"Fleet_vehicle_odometer" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_fleet/fleet_vehicle_odometers")

    public ResponseEntity<Fleet_vehicle_odometerDTO> create(@RequestBody Fleet_vehicle_odometerDTO fleet_vehicle_odometerdto) {
        Fleet_vehicle_odometerDTO dto = new Fleet_vehicle_odometerDTO();
        Fleet_vehicle_odometer domain = fleet_vehicle_odometerdto.toDO();
		fleet_vehicle_odometerService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批更新数据", tags = {"Fleet_vehicle_odometer" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_fleet/fleet_vehicle_odometers/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Fleet_vehicle_odometerDTO> fleet_vehicle_odometerdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "更新数据", tags = {"Fleet_vehicle_odometer" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_fleet/fleet_vehicle_odometers/{fleet_vehicle_odometer_id}")

    public ResponseEntity<Fleet_vehicle_odometerDTO> update(@PathVariable("fleet_vehicle_odometer_id") Integer fleet_vehicle_odometer_id, @RequestBody Fleet_vehicle_odometerDTO fleet_vehicle_odometerdto) {
		Fleet_vehicle_odometer domain = fleet_vehicle_odometerdto.toDO();
        domain.setId(fleet_vehicle_odometer_id);
		fleet_vehicle_odometerService.update(domain);
		Fleet_vehicle_odometerDTO dto = new Fleet_vehicle_odometerDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "删除数据", tags = {"Fleet_vehicle_odometer" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_fleet/fleet_vehicle_odometers/{fleet_vehicle_odometer_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("fleet_vehicle_odometer_id") Integer fleet_vehicle_odometer_id) {
        Fleet_vehicle_odometerDTO fleet_vehicle_odometerdto = new Fleet_vehicle_odometerDTO();
		Fleet_vehicle_odometer domain = new Fleet_vehicle_odometer();
		fleet_vehicle_odometerdto.setId(fleet_vehicle_odometer_id);
		domain.setId(fleet_vehicle_odometer_id);
        Boolean rst = fleet_vehicle_odometerService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "获取数据", tags = {"Fleet_vehicle_odometer" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_fleet/fleet_vehicle_odometers/{fleet_vehicle_odometer_id}")
    public ResponseEntity<Fleet_vehicle_odometerDTO> get(@PathVariable("fleet_vehicle_odometer_id") Integer fleet_vehicle_odometer_id) {
        Fleet_vehicle_odometerDTO dto = new Fleet_vehicle_odometerDTO();
        Fleet_vehicle_odometer domain = fleet_vehicle_odometerService.get(fleet_vehicle_odometer_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Fleet_vehicle_odometer" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_fleet/fleet_vehicle_odometers/fetchdefault")
	public ResponseEntity<Page<Fleet_vehicle_odometerDTO>> fetchDefault(Fleet_vehicle_odometerSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Fleet_vehicle_odometerDTO> list = new ArrayList<Fleet_vehicle_odometerDTO>();
        
        Page<Fleet_vehicle_odometer> domains = fleet_vehicle_odometerService.searchDefault(context) ;
        for(Fleet_vehicle_odometer fleet_vehicle_odometer : domains.getContent()){
            Fleet_vehicle_odometerDTO dto = new Fleet_vehicle_odometerDTO();
            dto.fromDO(fleet_vehicle_odometer);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
