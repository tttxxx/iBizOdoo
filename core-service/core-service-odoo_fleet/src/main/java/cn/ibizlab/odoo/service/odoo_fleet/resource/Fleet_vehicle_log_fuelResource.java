package cn.ibizlab.odoo.service.odoo_fleet.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_fleet.dto.Fleet_vehicle_log_fuelDTO;
import cn.ibizlab.odoo.core.odoo_fleet.domain.Fleet_vehicle_log_fuel;
import cn.ibizlab.odoo.core.odoo_fleet.service.IFleet_vehicle_log_fuelService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_fleet.filter.Fleet_vehicle_log_fuelSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Fleet_vehicle_log_fuel" })
@RestController
@RequestMapping("")
public class Fleet_vehicle_log_fuelResource {

    @Autowired
    private IFleet_vehicle_log_fuelService fleet_vehicle_log_fuelService;

    public IFleet_vehicle_log_fuelService getFleet_vehicle_log_fuelService() {
        return this.fleet_vehicle_log_fuelService;
    }

    @ApiOperation(value = "批更新数据", tags = {"Fleet_vehicle_log_fuel" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_fleet/fleet_vehicle_log_fuels/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Fleet_vehicle_log_fuelDTO> fleet_vehicle_log_fueldtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "删除数据", tags = {"Fleet_vehicle_log_fuel" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_fleet/fleet_vehicle_log_fuels/{fleet_vehicle_log_fuel_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("fleet_vehicle_log_fuel_id") Integer fleet_vehicle_log_fuel_id) {
        Fleet_vehicle_log_fuelDTO fleet_vehicle_log_fueldto = new Fleet_vehicle_log_fuelDTO();
		Fleet_vehicle_log_fuel domain = new Fleet_vehicle_log_fuel();
		fleet_vehicle_log_fueldto.setId(fleet_vehicle_log_fuel_id);
		domain.setId(fleet_vehicle_log_fuel_id);
        Boolean rst = fleet_vehicle_log_fuelService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "建立数据", tags = {"Fleet_vehicle_log_fuel" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_fleet/fleet_vehicle_log_fuels")

    public ResponseEntity<Fleet_vehicle_log_fuelDTO> create(@RequestBody Fleet_vehicle_log_fuelDTO fleet_vehicle_log_fueldto) {
        Fleet_vehicle_log_fuelDTO dto = new Fleet_vehicle_log_fuelDTO();
        Fleet_vehicle_log_fuel domain = fleet_vehicle_log_fueldto.toDO();
		fleet_vehicle_log_fuelService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "更新数据", tags = {"Fleet_vehicle_log_fuel" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_fleet/fleet_vehicle_log_fuels/{fleet_vehicle_log_fuel_id}")

    public ResponseEntity<Fleet_vehicle_log_fuelDTO> update(@PathVariable("fleet_vehicle_log_fuel_id") Integer fleet_vehicle_log_fuel_id, @RequestBody Fleet_vehicle_log_fuelDTO fleet_vehicle_log_fueldto) {
		Fleet_vehicle_log_fuel domain = fleet_vehicle_log_fueldto.toDO();
        domain.setId(fleet_vehicle_log_fuel_id);
		fleet_vehicle_log_fuelService.update(domain);
		Fleet_vehicle_log_fuelDTO dto = new Fleet_vehicle_log_fuelDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批删除数据", tags = {"Fleet_vehicle_log_fuel" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_fleet/fleet_vehicle_log_fuels/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Fleet_vehicle_log_fuelDTO> fleet_vehicle_log_fueldtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "获取数据", tags = {"Fleet_vehicle_log_fuel" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_fleet/fleet_vehicle_log_fuels/{fleet_vehicle_log_fuel_id}")
    public ResponseEntity<Fleet_vehicle_log_fuelDTO> get(@PathVariable("fleet_vehicle_log_fuel_id") Integer fleet_vehicle_log_fuel_id) {
        Fleet_vehicle_log_fuelDTO dto = new Fleet_vehicle_log_fuelDTO();
        Fleet_vehicle_log_fuel domain = fleet_vehicle_log_fuelService.get(fleet_vehicle_log_fuel_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批建立数据", tags = {"Fleet_vehicle_log_fuel" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_fleet/fleet_vehicle_log_fuels/createBatch")
    public ResponseEntity<Boolean> createBatchFleet_vehicle_log_fuel(@RequestBody List<Fleet_vehicle_log_fuelDTO> fleet_vehicle_log_fueldtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Fleet_vehicle_log_fuel" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_fleet/fleet_vehicle_log_fuels/fetchdefault")
	public ResponseEntity<Page<Fleet_vehicle_log_fuelDTO>> fetchDefault(Fleet_vehicle_log_fuelSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Fleet_vehicle_log_fuelDTO> list = new ArrayList<Fleet_vehicle_log_fuelDTO>();
        
        Page<Fleet_vehicle_log_fuel> domains = fleet_vehicle_log_fuelService.searchDefault(context) ;
        for(Fleet_vehicle_log_fuel fleet_vehicle_log_fuel : domains.getContent()){
            Fleet_vehicle_log_fuelDTO dto = new Fleet_vehicle_log_fuelDTO();
            dto.fromDO(fleet_vehicle_log_fuel);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
