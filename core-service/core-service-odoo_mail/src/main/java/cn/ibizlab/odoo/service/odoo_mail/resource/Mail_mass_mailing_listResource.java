package cn.ibizlab.odoo.service.odoo_mail.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_mail.dto.Mail_mass_mailing_listDTO;
import cn.ibizlab.odoo.core.odoo_mail.domain.Mail_mass_mailing_list;
import cn.ibizlab.odoo.core.odoo_mail.service.IMail_mass_mailing_listService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_mail.filter.Mail_mass_mailing_listSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Mail_mass_mailing_list" })
@RestController
@RequestMapping("")
public class Mail_mass_mailing_listResource {

    @Autowired
    private IMail_mass_mailing_listService mail_mass_mailing_listService;

    public IMail_mass_mailing_listService getMail_mass_mailing_listService() {
        return this.mail_mass_mailing_listService;
    }

    @ApiOperation(value = "更新数据", tags = {"Mail_mass_mailing_list" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_mail/mail_mass_mailing_lists/{mail_mass_mailing_list_id}")

    public ResponseEntity<Mail_mass_mailing_listDTO> update(@PathVariable("mail_mass_mailing_list_id") Integer mail_mass_mailing_list_id, @RequestBody Mail_mass_mailing_listDTO mail_mass_mailing_listdto) {
		Mail_mass_mailing_list domain = mail_mass_mailing_listdto.toDO();
        domain.setId(mail_mass_mailing_list_id);
		mail_mass_mailing_listService.update(domain);
		Mail_mass_mailing_listDTO dto = new Mail_mass_mailing_listDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "建立数据", tags = {"Mail_mass_mailing_list" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_mail/mail_mass_mailing_lists")

    public ResponseEntity<Mail_mass_mailing_listDTO> create(@RequestBody Mail_mass_mailing_listDTO mail_mass_mailing_listdto) {
        Mail_mass_mailing_listDTO dto = new Mail_mass_mailing_listDTO();
        Mail_mass_mailing_list domain = mail_mass_mailing_listdto.toDO();
		mail_mass_mailing_listService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取数据", tags = {"Mail_mass_mailing_list" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_mail/mail_mass_mailing_lists/{mail_mass_mailing_list_id}")
    public ResponseEntity<Mail_mass_mailing_listDTO> get(@PathVariable("mail_mass_mailing_list_id") Integer mail_mass_mailing_list_id) {
        Mail_mass_mailing_listDTO dto = new Mail_mass_mailing_listDTO();
        Mail_mass_mailing_list domain = mail_mass_mailing_listService.get(mail_mass_mailing_list_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批建立数据", tags = {"Mail_mass_mailing_list" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_mail/mail_mass_mailing_lists/createBatch")
    public ResponseEntity<Boolean> createBatchMail_mass_mailing_list(@RequestBody List<Mail_mass_mailing_listDTO> mail_mass_mailing_listdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "批删除数据", tags = {"Mail_mass_mailing_list" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_mail/mail_mass_mailing_lists/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Mail_mass_mailing_listDTO> mail_mass_mailing_listdtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "删除数据", tags = {"Mail_mass_mailing_list" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_mail/mail_mass_mailing_lists/{mail_mass_mailing_list_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("mail_mass_mailing_list_id") Integer mail_mass_mailing_list_id) {
        Mail_mass_mailing_listDTO mail_mass_mailing_listdto = new Mail_mass_mailing_listDTO();
		Mail_mass_mailing_list domain = new Mail_mass_mailing_list();
		mail_mass_mailing_listdto.setId(mail_mass_mailing_list_id);
		domain.setId(mail_mass_mailing_list_id);
        Boolean rst = mail_mass_mailing_listService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "批更新数据", tags = {"Mail_mass_mailing_list" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_mail/mail_mass_mailing_lists/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Mail_mass_mailing_listDTO> mail_mass_mailing_listdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Mail_mass_mailing_list" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_mail/mail_mass_mailing_lists/fetchdefault")
	public ResponseEntity<Page<Mail_mass_mailing_listDTO>> fetchDefault(Mail_mass_mailing_listSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Mail_mass_mailing_listDTO> list = new ArrayList<Mail_mass_mailing_listDTO>();
        
        Page<Mail_mass_mailing_list> domains = mail_mass_mailing_listService.searchDefault(context) ;
        for(Mail_mass_mailing_list mail_mass_mailing_list : domains.getContent()){
            Mail_mass_mailing_listDTO dto = new Mail_mass_mailing_listDTO();
            dto.fromDO(mail_mass_mailing_list);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
