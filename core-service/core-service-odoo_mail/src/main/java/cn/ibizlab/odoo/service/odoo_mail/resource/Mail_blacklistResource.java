package cn.ibizlab.odoo.service.odoo_mail.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_mail.dto.Mail_blacklistDTO;
import cn.ibizlab.odoo.core.odoo_mail.domain.Mail_blacklist;
import cn.ibizlab.odoo.core.odoo_mail.service.IMail_blacklistService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_mail.filter.Mail_blacklistSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Mail_blacklist" })
@RestController
@RequestMapping("")
public class Mail_blacklistResource {

    @Autowired
    private IMail_blacklistService mail_blacklistService;

    public IMail_blacklistService getMail_blacklistService() {
        return this.mail_blacklistService;
    }

    @ApiOperation(value = "删除数据", tags = {"Mail_blacklist" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_mail/mail_blacklists/{mail_blacklist_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("mail_blacklist_id") Integer mail_blacklist_id) {
        Mail_blacklistDTO mail_blacklistdto = new Mail_blacklistDTO();
		Mail_blacklist domain = new Mail_blacklist();
		mail_blacklistdto.setId(mail_blacklist_id);
		domain.setId(mail_blacklist_id);
        Boolean rst = mail_blacklistService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "获取数据", tags = {"Mail_blacklist" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_mail/mail_blacklists/{mail_blacklist_id}")
    public ResponseEntity<Mail_blacklistDTO> get(@PathVariable("mail_blacklist_id") Integer mail_blacklist_id) {
        Mail_blacklistDTO dto = new Mail_blacklistDTO();
        Mail_blacklist domain = mail_blacklistService.get(mail_blacklist_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "更新数据", tags = {"Mail_blacklist" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_mail/mail_blacklists/{mail_blacklist_id}")

    public ResponseEntity<Mail_blacklistDTO> update(@PathVariable("mail_blacklist_id") Integer mail_blacklist_id, @RequestBody Mail_blacklistDTO mail_blacklistdto) {
		Mail_blacklist domain = mail_blacklistdto.toDO();
        domain.setId(mail_blacklist_id);
		mail_blacklistService.update(domain);
		Mail_blacklistDTO dto = new Mail_blacklistDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批删除数据", tags = {"Mail_blacklist" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_mail/mail_blacklists/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Mail_blacklistDTO> mail_blacklistdtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "批更新数据", tags = {"Mail_blacklist" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_mail/mail_blacklists/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Mail_blacklistDTO> mail_blacklistdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "建立数据", tags = {"Mail_blacklist" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_mail/mail_blacklists")

    public ResponseEntity<Mail_blacklistDTO> create(@RequestBody Mail_blacklistDTO mail_blacklistdto) {
        Mail_blacklistDTO dto = new Mail_blacklistDTO();
        Mail_blacklist domain = mail_blacklistdto.toDO();
		mail_blacklistService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批建立数据", tags = {"Mail_blacklist" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_mail/mail_blacklists/createBatch")
    public ResponseEntity<Boolean> createBatchMail_blacklist(@RequestBody List<Mail_blacklistDTO> mail_blacklistdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Mail_blacklist" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_mail/mail_blacklists/fetchdefault")
	public ResponseEntity<Page<Mail_blacklistDTO>> fetchDefault(Mail_blacklistSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Mail_blacklistDTO> list = new ArrayList<Mail_blacklistDTO>();
        
        Page<Mail_blacklist> domains = mail_blacklistService.searchDefault(context) ;
        for(Mail_blacklist mail_blacklist : domains.getContent()){
            Mail_blacklistDTO dto = new Mail_blacklistDTO();
            dto.fromDO(mail_blacklist);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
