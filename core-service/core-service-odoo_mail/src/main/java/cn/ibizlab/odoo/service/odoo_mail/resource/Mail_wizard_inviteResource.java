package cn.ibizlab.odoo.service.odoo_mail.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_mail.dto.Mail_wizard_inviteDTO;
import cn.ibizlab.odoo.core.odoo_mail.domain.Mail_wizard_invite;
import cn.ibizlab.odoo.core.odoo_mail.service.IMail_wizard_inviteService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_mail.filter.Mail_wizard_inviteSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Mail_wizard_invite" })
@RestController
@RequestMapping("")
public class Mail_wizard_inviteResource {

    @Autowired
    private IMail_wizard_inviteService mail_wizard_inviteService;

    public IMail_wizard_inviteService getMail_wizard_inviteService() {
        return this.mail_wizard_inviteService;
    }

    @ApiOperation(value = "批删除数据", tags = {"Mail_wizard_invite" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_mail/mail_wizard_invites/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Mail_wizard_inviteDTO> mail_wizard_invitedtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "获取数据", tags = {"Mail_wizard_invite" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_mail/mail_wizard_invites/{mail_wizard_invite_id}")
    public ResponseEntity<Mail_wizard_inviteDTO> get(@PathVariable("mail_wizard_invite_id") Integer mail_wizard_invite_id) {
        Mail_wizard_inviteDTO dto = new Mail_wizard_inviteDTO();
        Mail_wizard_invite domain = mail_wizard_inviteService.get(mail_wizard_invite_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "建立数据", tags = {"Mail_wizard_invite" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_mail/mail_wizard_invites")

    public ResponseEntity<Mail_wizard_inviteDTO> create(@RequestBody Mail_wizard_inviteDTO mail_wizard_invitedto) {
        Mail_wizard_inviteDTO dto = new Mail_wizard_inviteDTO();
        Mail_wizard_invite domain = mail_wizard_invitedto.toDO();
		mail_wizard_inviteService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "更新数据", tags = {"Mail_wizard_invite" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_mail/mail_wizard_invites/{mail_wizard_invite_id}")

    public ResponseEntity<Mail_wizard_inviteDTO> update(@PathVariable("mail_wizard_invite_id") Integer mail_wizard_invite_id, @RequestBody Mail_wizard_inviteDTO mail_wizard_invitedto) {
		Mail_wizard_invite domain = mail_wizard_invitedto.toDO();
        domain.setId(mail_wizard_invite_id);
		mail_wizard_inviteService.update(domain);
		Mail_wizard_inviteDTO dto = new Mail_wizard_inviteDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "删除数据", tags = {"Mail_wizard_invite" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_mail/mail_wizard_invites/{mail_wizard_invite_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("mail_wizard_invite_id") Integer mail_wizard_invite_id) {
        Mail_wizard_inviteDTO mail_wizard_invitedto = new Mail_wizard_inviteDTO();
		Mail_wizard_invite domain = new Mail_wizard_invite();
		mail_wizard_invitedto.setId(mail_wizard_invite_id);
		domain.setId(mail_wizard_invite_id);
        Boolean rst = mail_wizard_inviteService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "批更新数据", tags = {"Mail_wizard_invite" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_mail/mail_wizard_invites/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Mail_wizard_inviteDTO> mail_wizard_invitedtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "批建立数据", tags = {"Mail_wizard_invite" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_mail/mail_wizard_invites/createBatch")
    public ResponseEntity<Boolean> createBatchMail_wizard_invite(@RequestBody List<Mail_wizard_inviteDTO> mail_wizard_invitedtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Mail_wizard_invite" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_mail/mail_wizard_invites/fetchdefault")
	public ResponseEntity<Page<Mail_wizard_inviteDTO>> fetchDefault(Mail_wizard_inviteSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Mail_wizard_inviteDTO> list = new ArrayList<Mail_wizard_inviteDTO>();
        
        Page<Mail_wizard_invite> domains = mail_wizard_inviteService.searchDefault(context) ;
        for(Mail_wizard_invite mail_wizard_invite : domains.getContent()){
            Mail_wizard_inviteDTO dto = new Mail_wizard_inviteDTO();
            dto.fromDO(mail_wizard_invite);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
