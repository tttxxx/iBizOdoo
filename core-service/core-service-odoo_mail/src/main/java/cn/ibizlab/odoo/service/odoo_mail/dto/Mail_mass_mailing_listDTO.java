package cn.ibizlab.odoo.service.odoo_mail.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.core.odoo_mail.valuerule.anno.mail_mass_mailing_list.*;
import cn.ibizlab.odoo.core.odoo_mail.domain.Mail_mass_mailing_list;
import org.springframework.cglib.beans.BeanCopier;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 服务DTO对象[Mail_mass_mailing_listDTO]
 */
public class Mail_mass_mailing_listDTO implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [WRITE_DATE]
     *
     */
    @Mail_mass_mailing_listWrite_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp write_date;

    @JsonIgnore
    private boolean write_dateDirtyFlag;

    /**
     * 属性 [ID]
     *
     */
    @Mail_mass_mailing_listIdDefault(info = "默认规则")
    private Integer id;

    @JsonIgnore
    private boolean idDirtyFlag;

    /**
     * 属性 [CONTACT_NBR]
     *
     */
    @Mail_mass_mailing_listContact_nbrDefault(info = "默认规则")
    private Integer contact_nbr;

    @JsonIgnore
    private boolean contact_nbrDirtyFlag;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @Mail_mass_mailing_list__last_updateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp __last_update;

    @JsonIgnore
    private boolean __last_updateDirtyFlag;

    /**
     * 属性 [CREATE_DATE]
     *
     */
    @Mail_mass_mailing_listCreate_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp create_date;

    @JsonIgnore
    private boolean create_dateDirtyFlag;

    /**
     * 属性 [SUBSCRIPTION_CONTACT_IDS]
     *
     */
    @Mail_mass_mailing_listSubscription_contact_idsDefault(info = "默认规则")
    private String subscription_contact_ids;

    @JsonIgnore
    private boolean subscription_contact_idsDirtyFlag;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @Mail_mass_mailing_listDisplay_nameDefault(info = "默认规则")
    private String display_name;

    @JsonIgnore
    private boolean display_nameDirtyFlag;

    /**
     * 属性 [CONTACT_IDS]
     *
     */
    @Mail_mass_mailing_listContact_idsDefault(info = "默认规则")
    private String contact_ids;

    @JsonIgnore
    private boolean contact_idsDirtyFlag;

    /**
     * 属性 [POPUP_CONTENT]
     *
     */
    @Mail_mass_mailing_listPopup_contentDefault(info = "默认规则")
    private String popup_content;

    @JsonIgnore
    private boolean popup_contentDirtyFlag;

    /**
     * 属性 [IS_PUBLIC]
     *
     */
    @Mail_mass_mailing_listIs_publicDefault(info = "默认规则")
    private String is_public;

    @JsonIgnore
    private boolean is_publicDirtyFlag;

    /**
     * 属性 [NAME]
     *
     */
    @Mail_mass_mailing_listNameDefault(info = "默认规则")
    private String name;

    @JsonIgnore
    private boolean nameDirtyFlag;

    /**
     * 属性 [POPUP_REDIRECT_URL]
     *
     */
    @Mail_mass_mailing_listPopup_redirect_urlDefault(info = "默认规则")
    private String popup_redirect_url;

    @JsonIgnore
    private boolean popup_redirect_urlDirtyFlag;

    /**
     * 属性 [ACTIVE]
     *
     */
    @Mail_mass_mailing_listActiveDefault(info = "默认规则")
    private String active;

    @JsonIgnore
    private boolean activeDirtyFlag;

    /**
     * 属性 [CREATE_UID_TEXT]
     *
     */
    @Mail_mass_mailing_listCreate_uid_textDefault(info = "默认规则")
    private String create_uid_text;

    @JsonIgnore
    private boolean create_uid_textDirtyFlag;

    /**
     * 属性 [WRITE_UID_TEXT]
     *
     */
    @Mail_mass_mailing_listWrite_uid_textDefault(info = "默认规则")
    private String write_uid_text;

    @JsonIgnore
    private boolean write_uid_textDirtyFlag;

    /**
     * 属性 [WRITE_UID]
     *
     */
    @Mail_mass_mailing_listWrite_uidDefault(info = "默认规则")
    private Integer write_uid;

    @JsonIgnore
    private boolean write_uidDirtyFlag;

    /**
     * 属性 [CREATE_UID]
     *
     */
    @Mail_mass_mailing_listCreate_uidDefault(info = "默认规则")
    private Integer create_uid;

    @JsonIgnore
    private boolean create_uidDirtyFlag;


    /**
     * 获取 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return write_date ;
    }

    /**
     * 设置 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return write_dateDirtyFlag ;
    }

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

    /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return idDirtyFlag ;
    }

    /**
     * 获取 [CONTACT_NBR]
     */
    @JsonProperty("contact_nbr")
    public Integer getContact_nbr(){
        return contact_nbr ;
    }

    /**
     * 设置 [CONTACT_NBR]
     */
    @JsonProperty("contact_nbr")
    public void setContact_nbr(Integer  contact_nbr){
        this.contact_nbr = contact_nbr ;
        this.contact_nbrDirtyFlag = true ;
    }

    /**
     * 获取 [CONTACT_NBR]脏标记
     */
    @JsonIgnore
    public boolean getContact_nbrDirtyFlag(){
        return contact_nbrDirtyFlag ;
    }

    /**
     * 获取 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return __last_update ;
    }

    /**
     * 设置 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

    /**
     * 获取 [__LAST_UPDATE]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return __last_updateDirtyFlag ;
    }

    /**
     * 获取 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return create_date ;
    }

    /**
     * 设置 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return create_dateDirtyFlag ;
    }

    /**
     * 获取 [SUBSCRIPTION_CONTACT_IDS]
     */
    @JsonProperty("subscription_contact_ids")
    public String getSubscription_contact_ids(){
        return subscription_contact_ids ;
    }

    /**
     * 设置 [SUBSCRIPTION_CONTACT_IDS]
     */
    @JsonProperty("subscription_contact_ids")
    public void setSubscription_contact_ids(String  subscription_contact_ids){
        this.subscription_contact_ids = subscription_contact_ids ;
        this.subscription_contact_idsDirtyFlag = true ;
    }

    /**
     * 获取 [SUBSCRIPTION_CONTACT_IDS]脏标记
     */
    @JsonIgnore
    public boolean getSubscription_contact_idsDirtyFlag(){
        return subscription_contact_idsDirtyFlag ;
    }

    /**
     * 获取 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return display_name ;
    }

    /**
     * 设置 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

    /**
     * 获取 [DISPLAY_NAME]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return display_nameDirtyFlag ;
    }

    /**
     * 获取 [CONTACT_IDS]
     */
    @JsonProperty("contact_ids")
    public String getContact_ids(){
        return contact_ids ;
    }

    /**
     * 设置 [CONTACT_IDS]
     */
    @JsonProperty("contact_ids")
    public void setContact_ids(String  contact_ids){
        this.contact_ids = contact_ids ;
        this.contact_idsDirtyFlag = true ;
    }

    /**
     * 获取 [CONTACT_IDS]脏标记
     */
    @JsonIgnore
    public boolean getContact_idsDirtyFlag(){
        return contact_idsDirtyFlag ;
    }

    /**
     * 获取 [POPUP_CONTENT]
     */
    @JsonProperty("popup_content")
    public String getPopup_content(){
        return popup_content ;
    }

    /**
     * 设置 [POPUP_CONTENT]
     */
    @JsonProperty("popup_content")
    public void setPopup_content(String  popup_content){
        this.popup_content = popup_content ;
        this.popup_contentDirtyFlag = true ;
    }

    /**
     * 获取 [POPUP_CONTENT]脏标记
     */
    @JsonIgnore
    public boolean getPopup_contentDirtyFlag(){
        return popup_contentDirtyFlag ;
    }

    /**
     * 获取 [IS_PUBLIC]
     */
    @JsonProperty("is_public")
    public String getIs_public(){
        return is_public ;
    }

    /**
     * 设置 [IS_PUBLIC]
     */
    @JsonProperty("is_public")
    public void setIs_public(String  is_public){
        this.is_public = is_public ;
        this.is_publicDirtyFlag = true ;
    }

    /**
     * 获取 [IS_PUBLIC]脏标记
     */
    @JsonIgnore
    public boolean getIs_publicDirtyFlag(){
        return is_publicDirtyFlag ;
    }

    /**
     * 获取 [NAME]
     */
    @JsonProperty("name")
    public String getName(){
        return name ;
    }

    /**
     * 设置 [NAME]
     */
    @JsonProperty("name")
    public void setName(String  name){
        this.name = name ;
        this.nameDirtyFlag = true ;
    }

    /**
     * 获取 [NAME]脏标记
     */
    @JsonIgnore
    public boolean getNameDirtyFlag(){
        return nameDirtyFlag ;
    }

    /**
     * 获取 [POPUP_REDIRECT_URL]
     */
    @JsonProperty("popup_redirect_url")
    public String getPopup_redirect_url(){
        return popup_redirect_url ;
    }

    /**
     * 设置 [POPUP_REDIRECT_URL]
     */
    @JsonProperty("popup_redirect_url")
    public void setPopup_redirect_url(String  popup_redirect_url){
        this.popup_redirect_url = popup_redirect_url ;
        this.popup_redirect_urlDirtyFlag = true ;
    }

    /**
     * 获取 [POPUP_REDIRECT_URL]脏标记
     */
    @JsonIgnore
    public boolean getPopup_redirect_urlDirtyFlag(){
        return popup_redirect_urlDirtyFlag ;
    }

    /**
     * 获取 [ACTIVE]
     */
    @JsonProperty("active")
    public String getActive(){
        return active ;
    }

    /**
     * 设置 [ACTIVE]
     */
    @JsonProperty("active")
    public void setActive(String  active){
        this.active = active ;
        this.activeDirtyFlag = true ;
    }

    /**
     * 获取 [ACTIVE]脏标记
     */
    @JsonIgnore
    public boolean getActiveDirtyFlag(){
        return activeDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return create_uid_text ;
    }

    /**
     * 设置 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return create_uid_textDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return write_uid_text ;
    }

    /**
     * 设置 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return write_uid_textDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return write_uid ;
    }

    /**
     * 设置 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return write_uidDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return create_uid ;
    }

    /**
     * 设置 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return create_uidDirtyFlag ;
    }



    public Mail_mass_mailing_list toDO() {
        Mail_mass_mailing_list srfdomain = new Mail_mass_mailing_list();
        if(getWrite_dateDirtyFlag())
            srfdomain.setWrite_date(write_date);
        if(getIdDirtyFlag())
            srfdomain.setId(id);
        if(getContact_nbrDirtyFlag())
            srfdomain.setContact_nbr(contact_nbr);
        if(get__last_updateDirtyFlag())
            srfdomain.set__last_update(__last_update);
        if(getCreate_dateDirtyFlag())
            srfdomain.setCreate_date(create_date);
        if(getSubscription_contact_idsDirtyFlag())
            srfdomain.setSubscription_contact_ids(subscription_contact_ids);
        if(getDisplay_nameDirtyFlag())
            srfdomain.setDisplay_name(display_name);
        if(getContact_idsDirtyFlag())
            srfdomain.setContact_ids(contact_ids);
        if(getPopup_contentDirtyFlag())
            srfdomain.setPopup_content(popup_content);
        if(getIs_publicDirtyFlag())
            srfdomain.setIs_public(is_public);
        if(getNameDirtyFlag())
            srfdomain.setName(name);
        if(getPopup_redirect_urlDirtyFlag())
            srfdomain.setPopup_redirect_url(popup_redirect_url);
        if(getActiveDirtyFlag())
            srfdomain.setActive(active);
        if(getCreate_uid_textDirtyFlag())
            srfdomain.setCreate_uid_text(create_uid_text);
        if(getWrite_uid_textDirtyFlag())
            srfdomain.setWrite_uid_text(write_uid_text);
        if(getWrite_uidDirtyFlag())
            srfdomain.setWrite_uid(write_uid);
        if(getCreate_uidDirtyFlag())
            srfdomain.setCreate_uid(create_uid);

        return srfdomain;
    }

    public void fromDO(Mail_mass_mailing_list srfdomain) {
        if(srfdomain == null )
          return ;
        if(srfdomain.getWrite_dateDirtyFlag())
            this.setWrite_date(srfdomain.getWrite_date());
        if(srfdomain.getIdDirtyFlag())
            this.setId(srfdomain.getId());
        if(srfdomain.getContact_nbrDirtyFlag())
            this.setContact_nbr(srfdomain.getContact_nbr());
        if(srfdomain.get__last_updateDirtyFlag())
            this.set__last_update(srfdomain.get__last_update());
        if(srfdomain.getCreate_dateDirtyFlag())
            this.setCreate_date(srfdomain.getCreate_date());
        if(srfdomain.getSubscription_contact_idsDirtyFlag())
            this.setSubscription_contact_ids(srfdomain.getSubscription_contact_ids());
        if(srfdomain.getDisplay_nameDirtyFlag())
            this.setDisplay_name(srfdomain.getDisplay_name());
        if(srfdomain.getContact_idsDirtyFlag())
            this.setContact_ids(srfdomain.getContact_ids());
        if(srfdomain.getPopup_contentDirtyFlag())
            this.setPopup_content(srfdomain.getPopup_content());
        if(srfdomain.getIs_publicDirtyFlag())
            this.setIs_public(srfdomain.getIs_public());
        if(srfdomain.getNameDirtyFlag())
            this.setName(srfdomain.getName());
        if(srfdomain.getPopup_redirect_urlDirtyFlag())
            this.setPopup_redirect_url(srfdomain.getPopup_redirect_url());
        if(srfdomain.getActiveDirtyFlag())
            this.setActive(srfdomain.getActive());
        if(srfdomain.getCreate_uid_textDirtyFlag())
            this.setCreate_uid_text(srfdomain.getCreate_uid_text());
        if(srfdomain.getWrite_uid_textDirtyFlag())
            this.setWrite_uid_text(srfdomain.getWrite_uid_text());
        if(srfdomain.getWrite_uidDirtyFlag())
            this.setWrite_uid(srfdomain.getWrite_uid());
        if(srfdomain.getCreate_uidDirtyFlag())
            this.setCreate_uid(srfdomain.getCreate_uid());

    }

    public List<Mail_mass_mailing_listDTO> fromDOPage(List<Mail_mass_mailing_list> poPage)   {
        if(poPage == null)
            return null;
        List<Mail_mass_mailing_listDTO> dtos=new ArrayList<Mail_mass_mailing_listDTO>();
        for(Mail_mass_mailing_list domain : poPage) {
            Mail_mass_mailing_listDTO dto = new Mail_mass_mailing_listDTO();
            dto.fromDO(domain);
            dtos.add(dto);
        }
        return dtos;
    }
}

