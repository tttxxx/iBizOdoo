package cn.ibizlab.odoo.service.odoo_mail.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_mail.dto.Mail_notificationDTO;
import cn.ibizlab.odoo.core.odoo_mail.domain.Mail_notification;
import cn.ibizlab.odoo.core.odoo_mail.service.IMail_notificationService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_mail.filter.Mail_notificationSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Mail_notification" })
@RestController
@RequestMapping("")
public class Mail_notificationResource {

    @Autowired
    private IMail_notificationService mail_notificationService;

    public IMail_notificationService getMail_notificationService() {
        return this.mail_notificationService;
    }

    @ApiOperation(value = "更新数据", tags = {"Mail_notification" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_mail/mail_notifications/{mail_notification_id}")

    public ResponseEntity<Mail_notificationDTO> update(@PathVariable("mail_notification_id") Integer mail_notification_id, @RequestBody Mail_notificationDTO mail_notificationdto) {
		Mail_notification domain = mail_notificationdto.toDO();
        domain.setId(mail_notification_id);
		mail_notificationService.update(domain);
		Mail_notificationDTO dto = new Mail_notificationDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批建立数据", tags = {"Mail_notification" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_mail/mail_notifications/createBatch")
    public ResponseEntity<Boolean> createBatchMail_notification(@RequestBody List<Mail_notificationDTO> mail_notificationdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "建立数据", tags = {"Mail_notification" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_mail/mail_notifications")

    public ResponseEntity<Mail_notificationDTO> create(@RequestBody Mail_notificationDTO mail_notificationdto) {
        Mail_notificationDTO dto = new Mail_notificationDTO();
        Mail_notification domain = mail_notificationdto.toDO();
		mail_notificationService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取数据", tags = {"Mail_notification" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_mail/mail_notifications/{mail_notification_id}")
    public ResponseEntity<Mail_notificationDTO> get(@PathVariable("mail_notification_id") Integer mail_notification_id) {
        Mail_notificationDTO dto = new Mail_notificationDTO();
        Mail_notification domain = mail_notificationService.get(mail_notification_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批更新数据", tags = {"Mail_notification" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_mail/mail_notifications/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Mail_notificationDTO> mail_notificationdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "删除数据", tags = {"Mail_notification" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_mail/mail_notifications/{mail_notification_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("mail_notification_id") Integer mail_notification_id) {
        Mail_notificationDTO mail_notificationdto = new Mail_notificationDTO();
		Mail_notification domain = new Mail_notification();
		mail_notificationdto.setId(mail_notification_id);
		domain.setId(mail_notification_id);
        Boolean rst = mail_notificationService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "批删除数据", tags = {"Mail_notification" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_mail/mail_notifications/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Mail_notificationDTO> mail_notificationdtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Mail_notification" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_mail/mail_notifications/fetchdefault")
	public ResponseEntity<Page<Mail_notificationDTO>> fetchDefault(Mail_notificationSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Mail_notificationDTO> list = new ArrayList<Mail_notificationDTO>();
        
        Page<Mail_notification> domains = mail_notificationService.searchDefault(context) ;
        for(Mail_notification mail_notification : domains.getContent()){
            Mail_notificationDTO dto = new Mail_notificationDTO();
            dto.fromDO(mail_notification);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
