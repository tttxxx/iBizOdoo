package cn.ibizlab.odoo.service.odoo_mail.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_mail.dto.Mail_mass_mailing_list_contact_relDTO;
import cn.ibizlab.odoo.core.odoo_mail.domain.Mail_mass_mailing_list_contact_rel;
import cn.ibizlab.odoo.core.odoo_mail.service.IMail_mass_mailing_list_contact_relService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_mail.filter.Mail_mass_mailing_list_contact_relSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Mail_mass_mailing_list_contact_rel" })
@RestController
@RequestMapping("")
public class Mail_mass_mailing_list_contact_relResource {

    @Autowired
    private IMail_mass_mailing_list_contact_relService mail_mass_mailing_list_contact_relService;

    public IMail_mass_mailing_list_contact_relService getMail_mass_mailing_list_contact_relService() {
        return this.mail_mass_mailing_list_contact_relService;
    }

    @ApiOperation(value = "删除数据", tags = {"Mail_mass_mailing_list_contact_rel" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_mail/mail_mass_mailing_list_contact_rels/{mail_mass_mailing_list_contact_rel_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("mail_mass_mailing_list_contact_rel_id") Integer mail_mass_mailing_list_contact_rel_id) {
        Mail_mass_mailing_list_contact_relDTO mail_mass_mailing_list_contact_reldto = new Mail_mass_mailing_list_contact_relDTO();
		Mail_mass_mailing_list_contact_rel domain = new Mail_mass_mailing_list_contact_rel();
		mail_mass_mailing_list_contact_reldto.setId(mail_mass_mailing_list_contact_rel_id);
		domain.setId(mail_mass_mailing_list_contact_rel_id);
        Boolean rst = mail_mass_mailing_list_contact_relService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "批建立数据", tags = {"Mail_mass_mailing_list_contact_rel" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_mail/mail_mass_mailing_list_contact_rels/createBatch")
    public ResponseEntity<Boolean> createBatchMail_mass_mailing_list_contact_rel(@RequestBody List<Mail_mass_mailing_list_contact_relDTO> mail_mass_mailing_list_contact_reldtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "建立数据", tags = {"Mail_mass_mailing_list_contact_rel" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_mail/mail_mass_mailing_list_contact_rels")

    public ResponseEntity<Mail_mass_mailing_list_contact_relDTO> create(@RequestBody Mail_mass_mailing_list_contact_relDTO mail_mass_mailing_list_contact_reldto) {
        Mail_mass_mailing_list_contact_relDTO dto = new Mail_mass_mailing_list_contact_relDTO();
        Mail_mass_mailing_list_contact_rel domain = mail_mass_mailing_list_contact_reldto.toDO();
		mail_mass_mailing_list_contact_relService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取数据", tags = {"Mail_mass_mailing_list_contact_rel" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_mail/mail_mass_mailing_list_contact_rels/{mail_mass_mailing_list_contact_rel_id}")
    public ResponseEntity<Mail_mass_mailing_list_contact_relDTO> get(@PathVariable("mail_mass_mailing_list_contact_rel_id") Integer mail_mass_mailing_list_contact_rel_id) {
        Mail_mass_mailing_list_contact_relDTO dto = new Mail_mass_mailing_list_contact_relDTO();
        Mail_mass_mailing_list_contact_rel domain = mail_mass_mailing_list_contact_relService.get(mail_mass_mailing_list_contact_rel_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "更新数据", tags = {"Mail_mass_mailing_list_contact_rel" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_mail/mail_mass_mailing_list_contact_rels/{mail_mass_mailing_list_contact_rel_id}")

    public ResponseEntity<Mail_mass_mailing_list_contact_relDTO> update(@PathVariable("mail_mass_mailing_list_contact_rel_id") Integer mail_mass_mailing_list_contact_rel_id, @RequestBody Mail_mass_mailing_list_contact_relDTO mail_mass_mailing_list_contact_reldto) {
		Mail_mass_mailing_list_contact_rel domain = mail_mass_mailing_list_contact_reldto.toDO();
        domain.setId(mail_mass_mailing_list_contact_rel_id);
		mail_mass_mailing_list_contact_relService.update(domain);
		Mail_mass_mailing_list_contact_relDTO dto = new Mail_mass_mailing_list_contact_relDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批更新数据", tags = {"Mail_mass_mailing_list_contact_rel" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_mail/mail_mass_mailing_list_contact_rels/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Mail_mass_mailing_list_contact_relDTO> mail_mass_mailing_list_contact_reldtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "批删除数据", tags = {"Mail_mass_mailing_list_contact_rel" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_mail/mail_mass_mailing_list_contact_rels/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Mail_mass_mailing_list_contact_relDTO> mail_mass_mailing_list_contact_reldtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Mail_mass_mailing_list_contact_rel" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_mail/mail_mass_mailing_list_contact_rels/fetchdefault")
	public ResponseEntity<Page<Mail_mass_mailing_list_contact_relDTO>> fetchDefault(Mail_mass_mailing_list_contact_relSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Mail_mass_mailing_list_contact_relDTO> list = new ArrayList<Mail_mass_mailing_list_contact_relDTO>();
        
        Page<Mail_mass_mailing_list_contact_rel> domains = mail_mass_mailing_list_contact_relService.searchDefault(context) ;
        for(Mail_mass_mailing_list_contact_rel mail_mass_mailing_list_contact_rel : domains.getContent()){
            Mail_mass_mailing_list_contact_relDTO dto = new Mail_mass_mailing_list_contact_relDTO();
            dto.fromDO(mail_mass_mailing_list_contact_rel);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
