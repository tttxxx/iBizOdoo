package cn.ibizlab.odoo.service.odoo_mail.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_mail.dto.Mail_blacklist_mixinDTO;
import cn.ibizlab.odoo.core.odoo_mail.domain.Mail_blacklist_mixin;
import cn.ibizlab.odoo.core.odoo_mail.service.IMail_blacklist_mixinService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_mail.filter.Mail_blacklist_mixinSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Mail_blacklist_mixin" })
@RestController
@RequestMapping("")
public class Mail_blacklist_mixinResource {

    @Autowired
    private IMail_blacklist_mixinService mail_blacklist_mixinService;

    public IMail_blacklist_mixinService getMail_blacklist_mixinService() {
        return this.mail_blacklist_mixinService;
    }

    @ApiOperation(value = "删除数据", tags = {"Mail_blacklist_mixin" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_mail/mail_blacklist_mixins/{mail_blacklist_mixin_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("mail_blacklist_mixin_id") Integer mail_blacklist_mixin_id) {
        Mail_blacklist_mixinDTO mail_blacklist_mixindto = new Mail_blacklist_mixinDTO();
		Mail_blacklist_mixin domain = new Mail_blacklist_mixin();
		mail_blacklist_mixindto.setId(mail_blacklist_mixin_id);
		domain.setId(mail_blacklist_mixin_id);
        Boolean rst = mail_blacklist_mixinService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "批建立数据", tags = {"Mail_blacklist_mixin" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_mail/mail_blacklist_mixins/createBatch")
    public ResponseEntity<Boolean> createBatchMail_blacklist_mixin(@RequestBody List<Mail_blacklist_mixinDTO> mail_blacklist_mixindtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "批删除数据", tags = {"Mail_blacklist_mixin" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_mail/mail_blacklist_mixins/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Mail_blacklist_mixinDTO> mail_blacklist_mixindtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "更新数据", tags = {"Mail_blacklist_mixin" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_mail/mail_blacklist_mixins/{mail_blacklist_mixin_id}")

    public ResponseEntity<Mail_blacklist_mixinDTO> update(@PathVariable("mail_blacklist_mixin_id") Integer mail_blacklist_mixin_id, @RequestBody Mail_blacklist_mixinDTO mail_blacklist_mixindto) {
		Mail_blacklist_mixin domain = mail_blacklist_mixindto.toDO();
        domain.setId(mail_blacklist_mixin_id);
		mail_blacklist_mixinService.update(domain);
		Mail_blacklist_mixinDTO dto = new Mail_blacklist_mixinDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批更新数据", tags = {"Mail_blacklist_mixin" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_mail/mail_blacklist_mixins/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Mail_blacklist_mixinDTO> mail_blacklist_mixindtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "获取数据", tags = {"Mail_blacklist_mixin" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_mail/mail_blacklist_mixins/{mail_blacklist_mixin_id}")
    public ResponseEntity<Mail_blacklist_mixinDTO> get(@PathVariable("mail_blacklist_mixin_id") Integer mail_blacklist_mixin_id) {
        Mail_blacklist_mixinDTO dto = new Mail_blacklist_mixinDTO();
        Mail_blacklist_mixin domain = mail_blacklist_mixinService.get(mail_blacklist_mixin_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "建立数据", tags = {"Mail_blacklist_mixin" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_mail/mail_blacklist_mixins")

    public ResponseEntity<Mail_blacklist_mixinDTO> create(@RequestBody Mail_blacklist_mixinDTO mail_blacklist_mixindto) {
        Mail_blacklist_mixinDTO dto = new Mail_blacklist_mixinDTO();
        Mail_blacklist_mixin domain = mail_blacklist_mixindto.toDO();
		mail_blacklist_mixinService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Mail_blacklist_mixin" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_mail/mail_blacklist_mixins/fetchdefault")
	public ResponseEntity<Page<Mail_blacklist_mixinDTO>> fetchDefault(Mail_blacklist_mixinSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Mail_blacklist_mixinDTO> list = new ArrayList<Mail_blacklist_mixinDTO>();
        
        Page<Mail_blacklist_mixin> domains = mail_blacklist_mixinService.searchDefault(context) ;
        for(Mail_blacklist_mixin mail_blacklist_mixin : domains.getContent()){
            Mail_blacklist_mixinDTO dto = new Mail_blacklist_mixinDTO();
            dto.fromDO(mail_blacklist_mixin);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
