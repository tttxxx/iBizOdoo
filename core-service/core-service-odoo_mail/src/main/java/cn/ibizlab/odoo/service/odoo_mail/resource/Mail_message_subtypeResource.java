package cn.ibizlab.odoo.service.odoo_mail.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_mail.dto.Mail_message_subtypeDTO;
import cn.ibizlab.odoo.core.odoo_mail.domain.Mail_message_subtype;
import cn.ibizlab.odoo.core.odoo_mail.service.IMail_message_subtypeService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_mail.filter.Mail_message_subtypeSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Mail_message_subtype" })
@RestController
@RequestMapping("")
public class Mail_message_subtypeResource {

    @Autowired
    private IMail_message_subtypeService mail_message_subtypeService;

    public IMail_message_subtypeService getMail_message_subtypeService() {
        return this.mail_message_subtypeService;
    }

    @ApiOperation(value = "批建立数据", tags = {"Mail_message_subtype" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_mail/mail_message_subtypes/createBatch")
    public ResponseEntity<Boolean> createBatchMail_message_subtype(@RequestBody List<Mail_message_subtypeDTO> mail_message_subtypedtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "批更新数据", tags = {"Mail_message_subtype" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_mail/mail_message_subtypes/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Mail_message_subtypeDTO> mail_message_subtypedtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "获取数据", tags = {"Mail_message_subtype" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_mail/mail_message_subtypes/{mail_message_subtype_id}")
    public ResponseEntity<Mail_message_subtypeDTO> get(@PathVariable("mail_message_subtype_id") Integer mail_message_subtype_id) {
        Mail_message_subtypeDTO dto = new Mail_message_subtypeDTO();
        Mail_message_subtype domain = mail_message_subtypeService.get(mail_message_subtype_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批删除数据", tags = {"Mail_message_subtype" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_mail/mail_message_subtypes/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Mail_message_subtypeDTO> mail_message_subtypedtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "删除数据", tags = {"Mail_message_subtype" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_mail/mail_message_subtypes/{mail_message_subtype_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("mail_message_subtype_id") Integer mail_message_subtype_id) {
        Mail_message_subtypeDTO mail_message_subtypedto = new Mail_message_subtypeDTO();
		Mail_message_subtype domain = new Mail_message_subtype();
		mail_message_subtypedto.setId(mail_message_subtype_id);
		domain.setId(mail_message_subtype_id);
        Boolean rst = mail_message_subtypeService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "建立数据", tags = {"Mail_message_subtype" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_mail/mail_message_subtypes")

    public ResponseEntity<Mail_message_subtypeDTO> create(@RequestBody Mail_message_subtypeDTO mail_message_subtypedto) {
        Mail_message_subtypeDTO dto = new Mail_message_subtypeDTO();
        Mail_message_subtype domain = mail_message_subtypedto.toDO();
		mail_message_subtypeService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "更新数据", tags = {"Mail_message_subtype" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_mail/mail_message_subtypes/{mail_message_subtype_id}")

    public ResponseEntity<Mail_message_subtypeDTO> update(@PathVariable("mail_message_subtype_id") Integer mail_message_subtype_id, @RequestBody Mail_message_subtypeDTO mail_message_subtypedto) {
		Mail_message_subtype domain = mail_message_subtypedto.toDO();
        domain.setId(mail_message_subtype_id);
		mail_message_subtypeService.update(domain);
		Mail_message_subtypeDTO dto = new Mail_message_subtypeDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Mail_message_subtype" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_mail/mail_message_subtypes/fetchdefault")
	public ResponseEntity<Page<Mail_message_subtypeDTO>> fetchDefault(Mail_message_subtypeSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Mail_message_subtypeDTO> list = new ArrayList<Mail_message_subtypeDTO>();
        
        Page<Mail_message_subtype> domains = mail_message_subtypeService.searchDefault(context) ;
        for(Mail_message_subtype mail_message_subtype : domains.getContent()){
            Mail_message_subtypeDTO dto = new Mail_message_subtypeDTO();
            dto.fromDO(mail_message_subtype);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
