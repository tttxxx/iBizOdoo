package cn.ibizlab.odoo.service.odoo_mail.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_mail.dto.Mail_mail_statisticsDTO;
import cn.ibizlab.odoo.core.odoo_mail.domain.Mail_mail_statistics;
import cn.ibizlab.odoo.core.odoo_mail.service.IMail_mail_statisticsService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_mail.filter.Mail_mail_statisticsSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Mail_mail_statistics" })
@RestController
@RequestMapping("")
public class Mail_mail_statisticsResource {

    @Autowired
    private IMail_mail_statisticsService mail_mail_statisticsService;

    public IMail_mail_statisticsService getMail_mail_statisticsService() {
        return this.mail_mail_statisticsService;
    }

    @ApiOperation(value = "更新数据", tags = {"Mail_mail_statistics" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_mail/mail_mail_statistics/{mail_mail_statistics_id}")

    public ResponseEntity<Mail_mail_statisticsDTO> update(@PathVariable("mail_mail_statistics_id") Integer mail_mail_statistics_id, @RequestBody Mail_mail_statisticsDTO mail_mail_statisticsdto) {
		Mail_mail_statistics domain = mail_mail_statisticsdto.toDO();
        domain.setId(mail_mail_statistics_id);
		mail_mail_statisticsService.update(domain);
		Mail_mail_statisticsDTO dto = new Mail_mail_statisticsDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取数据", tags = {"Mail_mail_statistics" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_mail/mail_mail_statistics/{mail_mail_statistics_id}")
    public ResponseEntity<Mail_mail_statisticsDTO> get(@PathVariable("mail_mail_statistics_id") Integer mail_mail_statistics_id) {
        Mail_mail_statisticsDTO dto = new Mail_mail_statisticsDTO();
        Mail_mail_statistics domain = mail_mail_statisticsService.get(mail_mail_statistics_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批更新数据", tags = {"Mail_mail_statistics" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_mail/mail_mail_statistics/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Mail_mail_statisticsDTO> mail_mail_statisticsdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "批删除数据", tags = {"Mail_mail_statistics" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_mail/mail_mail_statistics/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Mail_mail_statisticsDTO> mail_mail_statisticsdtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "批建立数据", tags = {"Mail_mail_statistics" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_mail/mail_mail_statistics/createBatch")
    public ResponseEntity<Boolean> createBatchMail_mail_statistics(@RequestBody List<Mail_mail_statisticsDTO> mail_mail_statisticsdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "删除数据", tags = {"Mail_mail_statistics" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_mail/mail_mail_statistics/{mail_mail_statistics_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("mail_mail_statistics_id") Integer mail_mail_statistics_id) {
        Mail_mail_statisticsDTO mail_mail_statisticsdto = new Mail_mail_statisticsDTO();
		Mail_mail_statistics domain = new Mail_mail_statistics();
		mail_mail_statisticsdto.setId(mail_mail_statistics_id);
		domain.setId(mail_mail_statistics_id);
        Boolean rst = mail_mail_statisticsService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "建立数据", tags = {"Mail_mail_statistics" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_mail/mail_mail_statistics")

    public ResponseEntity<Mail_mail_statisticsDTO> create(@RequestBody Mail_mail_statisticsDTO mail_mail_statisticsdto) {
        Mail_mail_statisticsDTO dto = new Mail_mail_statisticsDTO();
        Mail_mail_statistics domain = mail_mail_statisticsdto.toDO();
		mail_mail_statisticsService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Mail_mail_statistics" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_mail/mail_mail_statistics/fetchdefault")
	public ResponseEntity<Page<Mail_mail_statisticsDTO>> fetchDefault(Mail_mail_statisticsSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Mail_mail_statisticsDTO> list = new ArrayList<Mail_mail_statisticsDTO>();
        
        Page<Mail_mail_statistics> domains = mail_mail_statisticsService.searchDefault(context) ;
        for(Mail_mail_statistics mail_mail_statistics : domains.getContent()){
            Mail_mail_statisticsDTO dto = new Mail_mail_statisticsDTO();
            dto.fromDO(mail_mail_statistics);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
