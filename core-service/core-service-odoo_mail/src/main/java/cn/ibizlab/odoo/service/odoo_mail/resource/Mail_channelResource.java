package cn.ibizlab.odoo.service.odoo_mail.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_mail.dto.Mail_channelDTO;
import cn.ibizlab.odoo.core.odoo_mail.domain.Mail_channel;
import cn.ibizlab.odoo.core.odoo_mail.service.IMail_channelService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_mail.filter.Mail_channelSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Mail_channel" })
@RestController
@RequestMapping("")
public class Mail_channelResource {

    @Autowired
    private IMail_channelService mail_channelService;

    public IMail_channelService getMail_channelService() {
        return this.mail_channelService;
    }

    @ApiOperation(value = "批删除数据", tags = {"Mail_channel" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_mail/mail_channels/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Mail_channelDTO> mail_channeldtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "更新数据", tags = {"Mail_channel" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_mail/mail_channels/{mail_channel_id}")

    public ResponseEntity<Mail_channelDTO> update(@PathVariable("mail_channel_id") Integer mail_channel_id, @RequestBody Mail_channelDTO mail_channeldto) {
		Mail_channel domain = mail_channeldto.toDO();
        domain.setId(mail_channel_id);
		mail_channelService.update(domain);
		Mail_channelDTO dto = new Mail_channelDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "删除数据", tags = {"Mail_channel" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_mail/mail_channels/{mail_channel_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("mail_channel_id") Integer mail_channel_id) {
        Mail_channelDTO mail_channeldto = new Mail_channelDTO();
		Mail_channel domain = new Mail_channel();
		mail_channeldto.setId(mail_channel_id);
		domain.setId(mail_channel_id);
        Boolean rst = mail_channelService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "建立数据", tags = {"Mail_channel" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_mail/mail_channels")

    public ResponseEntity<Mail_channelDTO> create(@RequestBody Mail_channelDTO mail_channeldto) {
        Mail_channelDTO dto = new Mail_channelDTO();
        Mail_channel domain = mail_channeldto.toDO();
		mail_channelService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批建立数据", tags = {"Mail_channel" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_mail/mail_channels/createBatch")
    public ResponseEntity<Boolean> createBatchMail_channel(@RequestBody List<Mail_channelDTO> mail_channeldtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "获取数据", tags = {"Mail_channel" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_mail/mail_channels/{mail_channel_id}")
    public ResponseEntity<Mail_channelDTO> get(@PathVariable("mail_channel_id") Integer mail_channel_id) {
        Mail_channelDTO dto = new Mail_channelDTO();
        Mail_channel domain = mail_channelService.get(mail_channel_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批更新数据", tags = {"Mail_channel" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_mail/mail_channels/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Mail_channelDTO> mail_channeldtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Mail_channel" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_mail/mail_channels/fetchdefault")
	public ResponseEntity<Page<Mail_channelDTO>> fetchDefault(Mail_channelSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Mail_channelDTO> list = new ArrayList<Mail_channelDTO>();
        
        Page<Mail_channel> domains = mail_channelService.searchDefault(context) ;
        for(Mail_channel mail_channel : domains.getContent()){
            Mail_channelDTO dto = new Mail_channelDTO();
            dto.fromDO(mail_channel);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
