package cn.ibizlab.odoo.service.odoo_mail.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_mail.dto.Mail_channel_partnerDTO;
import cn.ibizlab.odoo.core.odoo_mail.domain.Mail_channel_partner;
import cn.ibizlab.odoo.core.odoo_mail.service.IMail_channel_partnerService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_mail.filter.Mail_channel_partnerSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Mail_channel_partner" })
@RestController
@RequestMapping("")
public class Mail_channel_partnerResource {

    @Autowired
    private IMail_channel_partnerService mail_channel_partnerService;

    public IMail_channel_partnerService getMail_channel_partnerService() {
        return this.mail_channel_partnerService;
    }

    @ApiOperation(value = "批删除数据", tags = {"Mail_channel_partner" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_mail/mail_channel_partners/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Mail_channel_partnerDTO> mail_channel_partnerdtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "建立数据", tags = {"Mail_channel_partner" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_mail/mail_channel_partners")

    public ResponseEntity<Mail_channel_partnerDTO> create(@RequestBody Mail_channel_partnerDTO mail_channel_partnerdto) {
        Mail_channel_partnerDTO dto = new Mail_channel_partnerDTO();
        Mail_channel_partner domain = mail_channel_partnerdto.toDO();
		mail_channel_partnerService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批更新数据", tags = {"Mail_channel_partner" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_mail/mail_channel_partners/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Mail_channel_partnerDTO> mail_channel_partnerdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "删除数据", tags = {"Mail_channel_partner" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_mail/mail_channel_partners/{mail_channel_partner_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("mail_channel_partner_id") Integer mail_channel_partner_id) {
        Mail_channel_partnerDTO mail_channel_partnerdto = new Mail_channel_partnerDTO();
		Mail_channel_partner domain = new Mail_channel_partner();
		mail_channel_partnerdto.setId(mail_channel_partner_id);
		domain.setId(mail_channel_partner_id);
        Boolean rst = mail_channel_partnerService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "获取数据", tags = {"Mail_channel_partner" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_mail/mail_channel_partners/{mail_channel_partner_id}")
    public ResponseEntity<Mail_channel_partnerDTO> get(@PathVariable("mail_channel_partner_id") Integer mail_channel_partner_id) {
        Mail_channel_partnerDTO dto = new Mail_channel_partnerDTO();
        Mail_channel_partner domain = mail_channel_partnerService.get(mail_channel_partner_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "更新数据", tags = {"Mail_channel_partner" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_mail/mail_channel_partners/{mail_channel_partner_id}")

    public ResponseEntity<Mail_channel_partnerDTO> update(@PathVariable("mail_channel_partner_id") Integer mail_channel_partner_id, @RequestBody Mail_channel_partnerDTO mail_channel_partnerdto) {
		Mail_channel_partner domain = mail_channel_partnerdto.toDO();
        domain.setId(mail_channel_partner_id);
		mail_channel_partnerService.update(domain);
		Mail_channel_partnerDTO dto = new Mail_channel_partnerDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批建立数据", tags = {"Mail_channel_partner" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_mail/mail_channel_partners/createBatch")
    public ResponseEntity<Boolean> createBatchMail_channel_partner(@RequestBody List<Mail_channel_partnerDTO> mail_channel_partnerdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Mail_channel_partner" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_mail/mail_channel_partners/fetchdefault")
	public ResponseEntity<Page<Mail_channel_partnerDTO>> fetchDefault(Mail_channel_partnerSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Mail_channel_partnerDTO> list = new ArrayList<Mail_channel_partnerDTO>();
        
        Page<Mail_channel_partner> domains = mail_channel_partnerService.searchDefault(context) ;
        for(Mail_channel_partner mail_channel_partner : domains.getContent()){
            Mail_channel_partnerDTO dto = new Mail_channel_partnerDTO();
            dto.fromDO(mail_channel_partner);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
