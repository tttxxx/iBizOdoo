package cn.ibizlab.odoo.service.odoo_mail.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.core.odoo_mail.valuerule.anno.mail_template.*;
import cn.ibizlab.odoo.core.odoo_mail.domain.Mail_template;
import org.springframework.cglib.beans.BeanCopier;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 服务DTO对象[Mail_templateDTO]
 */
public class Mail_templateDTO implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [ID]
     *
     */
    @Mail_templateIdDefault(info = "默认规则")
    private Integer id;

    @JsonIgnore
    private boolean idDirtyFlag;

    /**
     * 属性 [USE_DEFAULT_TO]
     *
     */
    @Mail_templateUse_default_toDefault(info = "默认规则")
    private String use_default_to;

    @JsonIgnore
    private boolean use_default_toDirtyFlag;

    /**
     * 属性 [SUBJECT]
     *
     */
    @Mail_templateSubjectDefault(info = "默认规则")
    private String subject;

    @JsonIgnore
    private boolean subjectDirtyFlag;

    /**
     * 属性 [ATTACHMENT_IDS]
     *
     */
    @Mail_templateAttachment_idsDefault(info = "默认规则")
    private String attachment_ids;

    @JsonIgnore
    private boolean attachment_idsDirtyFlag;

    /**
     * 属性 [WRITE_DATE]
     *
     */
    @Mail_templateWrite_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp write_date;

    @JsonIgnore
    private boolean write_dateDirtyFlag;

    /**
     * 属性 [REPLY_TO]
     *
     */
    @Mail_templateReply_toDefault(info = "默认规则")
    private String reply_to;

    @JsonIgnore
    private boolean reply_toDirtyFlag;

    /**
     * 属性 [SUB_OBJECT]
     *
     */
    @Mail_templateSub_objectDefault(info = "默认规则")
    private Integer sub_object;

    @JsonIgnore
    private boolean sub_objectDirtyFlag;

    /**
     * 属性 [SCHEDULED_DATE]
     *
     */
    @Mail_templateScheduled_dateDefault(info = "默认规则")
    private String scheduled_date;

    @JsonIgnore
    private boolean scheduled_dateDirtyFlag;

    /**
     * 属性 [MODEL_ID]
     *
     */
    @Mail_templateModel_idDefault(info = "默认规则")
    private Integer model_id;

    @JsonIgnore
    private boolean model_idDirtyFlag;

    /**
     * 属性 [MAIL_SERVER_ID]
     *
     */
    @Mail_templateMail_server_idDefault(info = "默认规则")
    private Integer mail_server_id;

    @JsonIgnore
    private boolean mail_server_idDirtyFlag;

    /**
     * 属性 [REPORT_TEMPLATE]
     *
     */
    @Mail_templateReport_templateDefault(info = "默认规则")
    private Integer report_template;

    @JsonIgnore
    private boolean report_templateDirtyFlag;

    /**
     * 属性 [BODY_HTML]
     *
     */
    @Mail_templateBody_htmlDefault(info = "默认规则")
    private String body_html;

    @JsonIgnore
    private boolean body_htmlDirtyFlag;

    /**
     * 属性 [REF_IR_ACT_WINDOW]
     *
     */
    @Mail_templateRef_ir_act_windowDefault(info = "默认规则")
    private Integer ref_ir_act_window;

    @JsonIgnore
    private boolean ref_ir_act_windowDirtyFlag;

    /**
     * 属性 [NAME]
     *
     */
    @Mail_templateNameDefault(info = "默认规则")
    private String name;

    @JsonIgnore
    private boolean nameDirtyFlag;

    /**
     * 属性 [PARTNER_TO]
     *
     */
    @Mail_templatePartner_toDefault(info = "默认规则")
    private String partner_to;

    @JsonIgnore
    private boolean partner_toDirtyFlag;

    /**
     * 属性 [MODEL]
     *
     */
    @Mail_templateModelDefault(info = "默认规则")
    private String model;

    @JsonIgnore
    private boolean modelDirtyFlag;

    /**
     * 属性 [EMAIL_FROM]
     *
     */
    @Mail_templateEmail_fromDefault(info = "默认规则")
    private String email_from;

    @JsonIgnore
    private boolean email_fromDirtyFlag;

    /**
     * 属性 [AUTO_DELETE]
     *
     */
    @Mail_templateAuto_deleteDefault(info = "默认规则")
    private String auto_delete;

    @JsonIgnore
    private boolean auto_deleteDirtyFlag;

    /**
     * 属性 [EMAIL_CC]
     *
     */
    @Mail_templateEmail_ccDefault(info = "默认规则")
    private String email_cc;

    @JsonIgnore
    private boolean email_ccDirtyFlag;

    /**
     * 属性 [REPORT_NAME]
     *
     */
    @Mail_templateReport_nameDefault(info = "默认规则")
    private String report_name;

    @JsonIgnore
    private boolean report_nameDirtyFlag;

    /**
     * 属性 [CREATE_DATE]
     *
     */
    @Mail_templateCreate_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp create_date;

    @JsonIgnore
    private boolean create_dateDirtyFlag;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @Mail_template__last_updateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp __last_update;

    @JsonIgnore
    private boolean __last_updateDirtyFlag;

    /**
     * 属性 [MODEL_OBJECT_FIELD]
     *
     */
    @Mail_templateModel_object_fieldDefault(info = "默认规则")
    private Integer model_object_field;

    @JsonIgnore
    private boolean model_object_fieldDirtyFlag;

    /**
     * 属性 [EMAIL_TO]
     *
     */
    @Mail_templateEmail_toDefault(info = "默认规则")
    private String email_to;

    @JsonIgnore
    private boolean email_toDirtyFlag;

    /**
     * 属性 [NULL_VALUE]
     *
     */
    @Mail_templateNull_valueDefault(info = "默认规则")
    private String null_value;

    @JsonIgnore
    private boolean null_valueDirtyFlag;

    /**
     * 属性 [COPYVALUE]
     *
     */
    @Mail_templateCopyvalueDefault(info = "默认规则")
    private String copyvalue;

    @JsonIgnore
    private boolean copyvalueDirtyFlag;

    /**
     * 属性 [USER_SIGNATURE]
     *
     */
    @Mail_templateUser_signatureDefault(info = "默认规则")
    private String user_signature;

    @JsonIgnore
    private boolean user_signatureDirtyFlag;

    /**
     * 属性 [SUB_MODEL_OBJECT_FIELD]
     *
     */
    @Mail_templateSub_model_object_fieldDefault(info = "默认规则")
    private Integer sub_model_object_field;

    @JsonIgnore
    private boolean sub_model_object_fieldDirtyFlag;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @Mail_templateDisplay_nameDefault(info = "默认规则")
    private String display_name;

    @JsonIgnore
    private boolean display_nameDirtyFlag;

    /**
     * 属性 [LANG]
     *
     */
    @Mail_templateLangDefault(info = "默认规则")
    private String lang;

    @JsonIgnore
    private boolean langDirtyFlag;

    /**
     * 属性 [CREATE_UID_TEXT]
     *
     */
    @Mail_templateCreate_uid_textDefault(info = "默认规则")
    private String create_uid_text;

    @JsonIgnore
    private boolean create_uid_textDirtyFlag;

    /**
     * 属性 [WRITE_UID_TEXT]
     *
     */
    @Mail_templateWrite_uid_textDefault(info = "默认规则")
    private String write_uid_text;

    @JsonIgnore
    private boolean write_uid_textDirtyFlag;

    /**
     * 属性 [CREATE_UID]
     *
     */
    @Mail_templateCreate_uidDefault(info = "默认规则")
    private Integer create_uid;

    @JsonIgnore
    private boolean create_uidDirtyFlag;

    /**
     * 属性 [WRITE_UID]
     *
     */
    @Mail_templateWrite_uidDefault(info = "默认规则")
    private Integer write_uid;

    @JsonIgnore
    private boolean write_uidDirtyFlag;


    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

    /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return idDirtyFlag ;
    }

    /**
     * 获取 [USE_DEFAULT_TO]
     */
    @JsonProperty("use_default_to")
    public String getUse_default_to(){
        return use_default_to ;
    }

    /**
     * 设置 [USE_DEFAULT_TO]
     */
    @JsonProperty("use_default_to")
    public void setUse_default_to(String  use_default_to){
        this.use_default_to = use_default_to ;
        this.use_default_toDirtyFlag = true ;
    }

    /**
     * 获取 [USE_DEFAULT_TO]脏标记
     */
    @JsonIgnore
    public boolean getUse_default_toDirtyFlag(){
        return use_default_toDirtyFlag ;
    }

    /**
     * 获取 [SUBJECT]
     */
    @JsonProperty("subject")
    public String getSubject(){
        return subject ;
    }

    /**
     * 设置 [SUBJECT]
     */
    @JsonProperty("subject")
    public void setSubject(String  subject){
        this.subject = subject ;
        this.subjectDirtyFlag = true ;
    }

    /**
     * 获取 [SUBJECT]脏标记
     */
    @JsonIgnore
    public boolean getSubjectDirtyFlag(){
        return subjectDirtyFlag ;
    }

    /**
     * 获取 [ATTACHMENT_IDS]
     */
    @JsonProperty("attachment_ids")
    public String getAttachment_ids(){
        return attachment_ids ;
    }

    /**
     * 设置 [ATTACHMENT_IDS]
     */
    @JsonProperty("attachment_ids")
    public void setAttachment_ids(String  attachment_ids){
        this.attachment_ids = attachment_ids ;
        this.attachment_idsDirtyFlag = true ;
    }

    /**
     * 获取 [ATTACHMENT_IDS]脏标记
     */
    @JsonIgnore
    public boolean getAttachment_idsDirtyFlag(){
        return attachment_idsDirtyFlag ;
    }

    /**
     * 获取 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return write_date ;
    }

    /**
     * 设置 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return write_dateDirtyFlag ;
    }

    /**
     * 获取 [REPLY_TO]
     */
    @JsonProperty("reply_to")
    public String getReply_to(){
        return reply_to ;
    }

    /**
     * 设置 [REPLY_TO]
     */
    @JsonProperty("reply_to")
    public void setReply_to(String  reply_to){
        this.reply_to = reply_to ;
        this.reply_toDirtyFlag = true ;
    }

    /**
     * 获取 [REPLY_TO]脏标记
     */
    @JsonIgnore
    public boolean getReply_toDirtyFlag(){
        return reply_toDirtyFlag ;
    }

    /**
     * 获取 [SUB_OBJECT]
     */
    @JsonProperty("sub_object")
    public Integer getSub_object(){
        return sub_object ;
    }

    /**
     * 设置 [SUB_OBJECT]
     */
    @JsonProperty("sub_object")
    public void setSub_object(Integer  sub_object){
        this.sub_object = sub_object ;
        this.sub_objectDirtyFlag = true ;
    }

    /**
     * 获取 [SUB_OBJECT]脏标记
     */
    @JsonIgnore
    public boolean getSub_objectDirtyFlag(){
        return sub_objectDirtyFlag ;
    }

    /**
     * 获取 [SCHEDULED_DATE]
     */
    @JsonProperty("scheduled_date")
    public String getScheduled_date(){
        return scheduled_date ;
    }

    /**
     * 设置 [SCHEDULED_DATE]
     */
    @JsonProperty("scheduled_date")
    public void setScheduled_date(String  scheduled_date){
        this.scheduled_date = scheduled_date ;
        this.scheduled_dateDirtyFlag = true ;
    }

    /**
     * 获取 [SCHEDULED_DATE]脏标记
     */
    @JsonIgnore
    public boolean getScheduled_dateDirtyFlag(){
        return scheduled_dateDirtyFlag ;
    }

    /**
     * 获取 [MODEL_ID]
     */
    @JsonProperty("model_id")
    public Integer getModel_id(){
        return model_id ;
    }

    /**
     * 设置 [MODEL_ID]
     */
    @JsonProperty("model_id")
    public void setModel_id(Integer  model_id){
        this.model_id = model_id ;
        this.model_idDirtyFlag = true ;
    }

    /**
     * 获取 [MODEL_ID]脏标记
     */
    @JsonIgnore
    public boolean getModel_idDirtyFlag(){
        return model_idDirtyFlag ;
    }

    /**
     * 获取 [MAIL_SERVER_ID]
     */
    @JsonProperty("mail_server_id")
    public Integer getMail_server_id(){
        return mail_server_id ;
    }

    /**
     * 设置 [MAIL_SERVER_ID]
     */
    @JsonProperty("mail_server_id")
    public void setMail_server_id(Integer  mail_server_id){
        this.mail_server_id = mail_server_id ;
        this.mail_server_idDirtyFlag = true ;
    }

    /**
     * 获取 [MAIL_SERVER_ID]脏标记
     */
    @JsonIgnore
    public boolean getMail_server_idDirtyFlag(){
        return mail_server_idDirtyFlag ;
    }

    /**
     * 获取 [REPORT_TEMPLATE]
     */
    @JsonProperty("report_template")
    public Integer getReport_template(){
        return report_template ;
    }

    /**
     * 设置 [REPORT_TEMPLATE]
     */
    @JsonProperty("report_template")
    public void setReport_template(Integer  report_template){
        this.report_template = report_template ;
        this.report_templateDirtyFlag = true ;
    }

    /**
     * 获取 [REPORT_TEMPLATE]脏标记
     */
    @JsonIgnore
    public boolean getReport_templateDirtyFlag(){
        return report_templateDirtyFlag ;
    }

    /**
     * 获取 [BODY_HTML]
     */
    @JsonProperty("body_html")
    public String getBody_html(){
        return body_html ;
    }

    /**
     * 设置 [BODY_HTML]
     */
    @JsonProperty("body_html")
    public void setBody_html(String  body_html){
        this.body_html = body_html ;
        this.body_htmlDirtyFlag = true ;
    }

    /**
     * 获取 [BODY_HTML]脏标记
     */
    @JsonIgnore
    public boolean getBody_htmlDirtyFlag(){
        return body_htmlDirtyFlag ;
    }

    /**
     * 获取 [REF_IR_ACT_WINDOW]
     */
    @JsonProperty("ref_ir_act_window")
    public Integer getRef_ir_act_window(){
        return ref_ir_act_window ;
    }

    /**
     * 设置 [REF_IR_ACT_WINDOW]
     */
    @JsonProperty("ref_ir_act_window")
    public void setRef_ir_act_window(Integer  ref_ir_act_window){
        this.ref_ir_act_window = ref_ir_act_window ;
        this.ref_ir_act_windowDirtyFlag = true ;
    }

    /**
     * 获取 [REF_IR_ACT_WINDOW]脏标记
     */
    @JsonIgnore
    public boolean getRef_ir_act_windowDirtyFlag(){
        return ref_ir_act_windowDirtyFlag ;
    }

    /**
     * 获取 [NAME]
     */
    @JsonProperty("name")
    public String getName(){
        return name ;
    }

    /**
     * 设置 [NAME]
     */
    @JsonProperty("name")
    public void setName(String  name){
        this.name = name ;
        this.nameDirtyFlag = true ;
    }

    /**
     * 获取 [NAME]脏标记
     */
    @JsonIgnore
    public boolean getNameDirtyFlag(){
        return nameDirtyFlag ;
    }

    /**
     * 获取 [PARTNER_TO]
     */
    @JsonProperty("partner_to")
    public String getPartner_to(){
        return partner_to ;
    }

    /**
     * 设置 [PARTNER_TO]
     */
    @JsonProperty("partner_to")
    public void setPartner_to(String  partner_to){
        this.partner_to = partner_to ;
        this.partner_toDirtyFlag = true ;
    }

    /**
     * 获取 [PARTNER_TO]脏标记
     */
    @JsonIgnore
    public boolean getPartner_toDirtyFlag(){
        return partner_toDirtyFlag ;
    }

    /**
     * 获取 [MODEL]
     */
    @JsonProperty("model")
    public String getModel(){
        return model ;
    }

    /**
     * 设置 [MODEL]
     */
    @JsonProperty("model")
    public void setModel(String  model){
        this.model = model ;
        this.modelDirtyFlag = true ;
    }

    /**
     * 获取 [MODEL]脏标记
     */
    @JsonIgnore
    public boolean getModelDirtyFlag(){
        return modelDirtyFlag ;
    }

    /**
     * 获取 [EMAIL_FROM]
     */
    @JsonProperty("email_from")
    public String getEmail_from(){
        return email_from ;
    }

    /**
     * 设置 [EMAIL_FROM]
     */
    @JsonProperty("email_from")
    public void setEmail_from(String  email_from){
        this.email_from = email_from ;
        this.email_fromDirtyFlag = true ;
    }

    /**
     * 获取 [EMAIL_FROM]脏标记
     */
    @JsonIgnore
    public boolean getEmail_fromDirtyFlag(){
        return email_fromDirtyFlag ;
    }

    /**
     * 获取 [AUTO_DELETE]
     */
    @JsonProperty("auto_delete")
    public String getAuto_delete(){
        return auto_delete ;
    }

    /**
     * 设置 [AUTO_DELETE]
     */
    @JsonProperty("auto_delete")
    public void setAuto_delete(String  auto_delete){
        this.auto_delete = auto_delete ;
        this.auto_deleteDirtyFlag = true ;
    }

    /**
     * 获取 [AUTO_DELETE]脏标记
     */
    @JsonIgnore
    public boolean getAuto_deleteDirtyFlag(){
        return auto_deleteDirtyFlag ;
    }

    /**
     * 获取 [EMAIL_CC]
     */
    @JsonProperty("email_cc")
    public String getEmail_cc(){
        return email_cc ;
    }

    /**
     * 设置 [EMAIL_CC]
     */
    @JsonProperty("email_cc")
    public void setEmail_cc(String  email_cc){
        this.email_cc = email_cc ;
        this.email_ccDirtyFlag = true ;
    }

    /**
     * 获取 [EMAIL_CC]脏标记
     */
    @JsonIgnore
    public boolean getEmail_ccDirtyFlag(){
        return email_ccDirtyFlag ;
    }

    /**
     * 获取 [REPORT_NAME]
     */
    @JsonProperty("report_name")
    public String getReport_name(){
        return report_name ;
    }

    /**
     * 设置 [REPORT_NAME]
     */
    @JsonProperty("report_name")
    public void setReport_name(String  report_name){
        this.report_name = report_name ;
        this.report_nameDirtyFlag = true ;
    }

    /**
     * 获取 [REPORT_NAME]脏标记
     */
    @JsonIgnore
    public boolean getReport_nameDirtyFlag(){
        return report_nameDirtyFlag ;
    }

    /**
     * 获取 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return create_date ;
    }

    /**
     * 设置 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return create_dateDirtyFlag ;
    }

    /**
     * 获取 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return __last_update ;
    }

    /**
     * 设置 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

    /**
     * 获取 [__LAST_UPDATE]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return __last_updateDirtyFlag ;
    }

    /**
     * 获取 [MODEL_OBJECT_FIELD]
     */
    @JsonProperty("model_object_field")
    public Integer getModel_object_field(){
        return model_object_field ;
    }

    /**
     * 设置 [MODEL_OBJECT_FIELD]
     */
    @JsonProperty("model_object_field")
    public void setModel_object_field(Integer  model_object_field){
        this.model_object_field = model_object_field ;
        this.model_object_fieldDirtyFlag = true ;
    }

    /**
     * 获取 [MODEL_OBJECT_FIELD]脏标记
     */
    @JsonIgnore
    public boolean getModel_object_fieldDirtyFlag(){
        return model_object_fieldDirtyFlag ;
    }

    /**
     * 获取 [EMAIL_TO]
     */
    @JsonProperty("email_to")
    public String getEmail_to(){
        return email_to ;
    }

    /**
     * 设置 [EMAIL_TO]
     */
    @JsonProperty("email_to")
    public void setEmail_to(String  email_to){
        this.email_to = email_to ;
        this.email_toDirtyFlag = true ;
    }

    /**
     * 获取 [EMAIL_TO]脏标记
     */
    @JsonIgnore
    public boolean getEmail_toDirtyFlag(){
        return email_toDirtyFlag ;
    }

    /**
     * 获取 [NULL_VALUE]
     */
    @JsonProperty("null_value")
    public String getNull_value(){
        return null_value ;
    }

    /**
     * 设置 [NULL_VALUE]
     */
    @JsonProperty("null_value")
    public void setNull_value(String  null_value){
        this.null_value = null_value ;
        this.null_valueDirtyFlag = true ;
    }

    /**
     * 获取 [NULL_VALUE]脏标记
     */
    @JsonIgnore
    public boolean getNull_valueDirtyFlag(){
        return null_valueDirtyFlag ;
    }

    /**
     * 获取 [COPYVALUE]
     */
    @JsonProperty("copyvalue")
    public String getCopyvalue(){
        return copyvalue ;
    }

    /**
     * 设置 [COPYVALUE]
     */
    @JsonProperty("copyvalue")
    public void setCopyvalue(String  copyvalue){
        this.copyvalue = copyvalue ;
        this.copyvalueDirtyFlag = true ;
    }

    /**
     * 获取 [COPYVALUE]脏标记
     */
    @JsonIgnore
    public boolean getCopyvalueDirtyFlag(){
        return copyvalueDirtyFlag ;
    }

    /**
     * 获取 [USER_SIGNATURE]
     */
    @JsonProperty("user_signature")
    public String getUser_signature(){
        return user_signature ;
    }

    /**
     * 设置 [USER_SIGNATURE]
     */
    @JsonProperty("user_signature")
    public void setUser_signature(String  user_signature){
        this.user_signature = user_signature ;
        this.user_signatureDirtyFlag = true ;
    }

    /**
     * 获取 [USER_SIGNATURE]脏标记
     */
    @JsonIgnore
    public boolean getUser_signatureDirtyFlag(){
        return user_signatureDirtyFlag ;
    }

    /**
     * 获取 [SUB_MODEL_OBJECT_FIELD]
     */
    @JsonProperty("sub_model_object_field")
    public Integer getSub_model_object_field(){
        return sub_model_object_field ;
    }

    /**
     * 设置 [SUB_MODEL_OBJECT_FIELD]
     */
    @JsonProperty("sub_model_object_field")
    public void setSub_model_object_field(Integer  sub_model_object_field){
        this.sub_model_object_field = sub_model_object_field ;
        this.sub_model_object_fieldDirtyFlag = true ;
    }

    /**
     * 获取 [SUB_MODEL_OBJECT_FIELD]脏标记
     */
    @JsonIgnore
    public boolean getSub_model_object_fieldDirtyFlag(){
        return sub_model_object_fieldDirtyFlag ;
    }

    /**
     * 获取 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return display_name ;
    }

    /**
     * 设置 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

    /**
     * 获取 [DISPLAY_NAME]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return display_nameDirtyFlag ;
    }

    /**
     * 获取 [LANG]
     */
    @JsonProperty("lang")
    public String getLang(){
        return lang ;
    }

    /**
     * 设置 [LANG]
     */
    @JsonProperty("lang")
    public void setLang(String  lang){
        this.lang = lang ;
        this.langDirtyFlag = true ;
    }

    /**
     * 获取 [LANG]脏标记
     */
    @JsonIgnore
    public boolean getLangDirtyFlag(){
        return langDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return create_uid_text ;
    }

    /**
     * 设置 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return create_uid_textDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return write_uid_text ;
    }

    /**
     * 设置 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return write_uid_textDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return create_uid ;
    }

    /**
     * 设置 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return create_uidDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return write_uid ;
    }

    /**
     * 设置 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return write_uidDirtyFlag ;
    }



    public Mail_template toDO() {
        Mail_template srfdomain = new Mail_template();
        if(getIdDirtyFlag())
            srfdomain.setId(id);
        if(getUse_default_toDirtyFlag())
            srfdomain.setUse_default_to(use_default_to);
        if(getSubjectDirtyFlag())
            srfdomain.setSubject(subject);
        if(getAttachment_idsDirtyFlag())
            srfdomain.setAttachment_ids(attachment_ids);
        if(getWrite_dateDirtyFlag())
            srfdomain.setWrite_date(write_date);
        if(getReply_toDirtyFlag())
            srfdomain.setReply_to(reply_to);
        if(getSub_objectDirtyFlag())
            srfdomain.setSub_object(sub_object);
        if(getScheduled_dateDirtyFlag())
            srfdomain.setScheduled_date(scheduled_date);
        if(getModel_idDirtyFlag())
            srfdomain.setModel_id(model_id);
        if(getMail_server_idDirtyFlag())
            srfdomain.setMail_server_id(mail_server_id);
        if(getReport_templateDirtyFlag())
            srfdomain.setReport_template(report_template);
        if(getBody_htmlDirtyFlag())
            srfdomain.setBody_html(body_html);
        if(getRef_ir_act_windowDirtyFlag())
            srfdomain.setRef_ir_act_window(ref_ir_act_window);
        if(getNameDirtyFlag())
            srfdomain.setName(name);
        if(getPartner_toDirtyFlag())
            srfdomain.setPartner_to(partner_to);
        if(getModelDirtyFlag())
            srfdomain.setModel(model);
        if(getEmail_fromDirtyFlag())
            srfdomain.setEmail_from(email_from);
        if(getAuto_deleteDirtyFlag())
            srfdomain.setAuto_delete(auto_delete);
        if(getEmail_ccDirtyFlag())
            srfdomain.setEmail_cc(email_cc);
        if(getReport_nameDirtyFlag())
            srfdomain.setReport_name(report_name);
        if(getCreate_dateDirtyFlag())
            srfdomain.setCreate_date(create_date);
        if(get__last_updateDirtyFlag())
            srfdomain.set__last_update(__last_update);
        if(getModel_object_fieldDirtyFlag())
            srfdomain.setModel_object_field(model_object_field);
        if(getEmail_toDirtyFlag())
            srfdomain.setEmail_to(email_to);
        if(getNull_valueDirtyFlag())
            srfdomain.setNull_value(null_value);
        if(getCopyvalueDirtyFlag())
            srfdomain.setCopyvalue(copyvalue);
        if(getUser_signatureDirtyFlag())
            srfdomain.setUser_signature(user_signature);
        if(getSub_model_object_fieldDirtyFlag())
            srfdomain.setSub_model_object_field(sub_model_object_field);
        if(getDisplay_nameDirtyFlag())
            srfdomain.setDisplay_name(display_name);
        if(getLangDirtyFlag())
            srfdomain.setLang(lang);
        if(getCreate_uid_textDirtyFlag())
            srfdomain.setCreate_uid_text(create_uid_text);
        if(getWrite_uid_textDirtyFlag())
            srfdomain.setWrite_uid_text(write_uid_text);
        if(getCreate_uidDirtyFlag())
            srfdomain.setCreate_uid(create_uid);
        if(getWrite_uidDirtyFlag())
            srfdomain.setWrite_uid(write_uid);

        return srfdomain;
    }

    public void fromDO(Mail_template srfdomain) {
        if(srfdomain == null )
          return ;
        if(srfdomain.getIdDirtyFlag())
            this.setId(srfdomain.getId());
        if(srfdomain.getUse_default_toDirtyFlag())
            this.setUse_default_to(srfdomain.getUse_default_to());
        if(srfdomain.getSubjectDirtyFlag())
            this.setSubject(srfdomain.getSubject());
        if(srfdomain.getAttachment_idsDirtyFlag())
            this.setAttachment_ids(srfdomain.getAttachment_ids());
        if(srfdomain.getWrite_dateDirtyFlag())
            this.setWrite_date(srfdomain.getWrite_date());
        if(srfdomain.getReply_toDirtyFlag())
            this.setReply_to(srfdomain.getReply_to());
        if(srfdomain.getSub_objectDirtyFlag())
            this.setSub_object(srfdomain.getSub_object());
        if(srfdomain.getScheduled_dateDirtyFlag())
            this.setScheduled_date(srfdomain.getScheduled_date());
        if(srfdomain.getModel_idDirtyFlag())
            this.setModel_id(srfdomain.getModel_id());
        if(srfdomain.getMail_server_idDirtyFlag())
            this.setMail_server_id(srfdomain.getMail_server_id());
        if(srfdomain.getReport_templateDirtyFlag())
            this.setReport_template(srfdomain.getReport_template());
        if(srfdomain.getBody_htmlDirtyFlag())
            this.setBody_html(srfdomain.getBody_html());
        if(srfdomain.getRef_ir_act_windowDirtyFlag())
            this.setRef_ir_act_window(srfdomain.getRef_ir_act_window());
        if(srfdomain.getNameDirtyFlag())
            this.setName(srfdomain.getName());
        if(srfdomain.getPartner_toDirtyFlag())
            this.setPartner_to(srfdomain.getPartner_to());
        if(srfdomain.getModelDirtyFlag())
            this.setModel(srfdomain.getModel());
        if(srfdomain.getEmail_fromDirtyFlag())
            this.setEmail_from(srfdomain.getEmail_from());
        if(srfdomain.getAuto_deleteDirtyFlag())
            this.setAuto_delete(srfdomain.getAuto_delete());
        if(srfdomain.getEmail_ccDirtyFlag())
            this.setEmail_cc(srfdomain.getEmail_cc());
        if(srfdomain.getReport_nameDirtyFlag())
            this.setReport_name(srfdomain.getReport_name());
        if(srfdomain.getCreate_dateDirtyFlag())
            this.setCreate_date(srfdomain.getCreate_date());
        if(srfdomain.get__last_updateDirtyFlag())
            this.set__last_update(srfdomain.get__last_update());
        if(srfdomain.getModel_object_fieldDirtyFlag())
            this.setModel_object_field(srfdomain.getModel_object_field());
        if(srfdomain.getEmail_toDirtyFlag())
            this.setEmail_to(srfdomain.getEmail_to());
        if(srfdomain.getNull_valueDirtyFlag())
            this.setNull_value(srfdomain.getNull_value());
        if(srfdomain.getCopyvalueDirtyFlag())
            this.setCopyvalue(srfdomain.getCopyvalue());
        if(srfdomain.getUser_signatureDirtyFlag())
            this.setUser_signature(srfdomain.getUser_signature());
        if(srfdomain.getSub_model_object_fieldDirtyFlag())
            this.setSub_model_object_field(srfdomain.getSub_model_object_field());
        if(srfdomain.getDisplay_nameDirtyFlag())
            this.setDisplay_name(srfdomain.getDisplay_name());
        if(srfdomain.getLangDirtyFlag())
            this.setLang(srfdomain.getLang());
        if(srfdomain.getCreate_uid_textDirtyFlag())
            this.setCreate_uid_text(srfdomain.getCreate_uid_text());
        if(srfdomain.getWrite_uid_textDirtyFlag())
            this.setWrite_uid_text(srfdomain.getWrite_uid_text());
        if(srfdomain.getCreate_uidDirtyFlag())
            this.setCreate_uid(srfdomain.getCreate_uid());
        if(srfdomain.getWrite_uidDirtyFlag())
            this.setWrite_uid(srfdomain.getWrite_uid());

    }

    public List<Mail_templateDTO> fromDOPage(List<Mail_template> poPage)   {
        if(poPage == null)
            return null;
        List<Mail_templateDTO> dtos=new ArrayList<Mail_templateDTO>();
        for(Mail_template domain : poPage) {
            Mail_templateDTO dto = new Mail_templateDTO();
            dto.fromDO(domain);
            dtos.add(dto);
        }
        return dtos;
    }
}

