package cn.ibizlab.odoo.service.odoo_mail.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_mail.dto.Mail_moderationDTO;
import cn.ibizlab.odoo.core.odoo_mail.domain.Mail_moderation;
import cn.ibizlab.odoo.core.odoo_mail.service.IMail_moderationService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_mail.filter.Mail_moderationSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Mail_moderation" })
@RestController
@RequestMapping("")
public class Mail_moderationResource {

    @Autowired
    private IMail_moderationService mail_moderationService;

    public IMail_moderationService getMail_moderationService() {
        return this.mail_moderationService;
    }

    @ApiOperation(value = "删除数据", tags = {"Mail_moderation" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_mail/mail_moderations/{mail_moderation_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("mail_moderation_id") Integer mail_moderation_id) {
        Mail_moderationDTO mail_moderationdto = new Mail_moderationDTO();
		Mail_moderation domain = new Mail_moderation();
		mail_moderationdto.setId(mail_moderation_id);
		domain.setId(mail_moderation_id);
        Boolean rst = mail_moderationService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "更新数据", tags = {"Mail_moderation" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_mail/mail_moderations/{mail_moderation_id}")

    public ResponseEntity<Mail_moderationDTO> update(@PathVariable("mail_moderation_id") Integer mail_moderation_id, @RequestBody Mail_moderationDTO mail_moderationdto) {
		Mail_moderation domain = mail_moderationdto.toDO();
        domain.setId(mail_moderation_id);
		mail_moderationService.update(domain);
		Mail_moderationDTO dto = new Mail_moderationDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批建立数据", tags = {"Mail_moderation" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_mail/mail_moderations/createBatch")
    public ResponseEntity<Boolean> createBatchMail_moderation(@RequestBody List<Mail_moderationDTO> mail_moderationdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "建立数据", tags = {"Mail_moderation" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_mail/mail_moderations")

    public ResponseEntity<Mail_moderationDTO> create(@RequestBody Mail_moderationDTO mail_moderationdto) {
        Mail_moderationDTO dto = new Mail_moderationDTO();
        Mail_moderation domain = mail_moderationdto.toDO();
		mail_moderationService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批更新数据", tags = {"Mail_moderation" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_mail/mail_moderations/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Mail_moderationDTO> mail_moderationdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "批删除数据", tags = {"Mail_moderation" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_mail/mail_moderations/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Mail_moderationDTO> mail_moderationdtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "获取数据", tags = {"Mail_moderation" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_mail/mail_moderations/{mail_moderation_id}")
    public ResponseEntity<Mail_moderationDTO> get(@PathVariable("mail_moderation_id") Integer mail_moderation_id) {
        Mail_moderationDTO dto = new Mail_moderationDTO();
        Mail_moderation domain = mail_moderationService.get(mail_moderation_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Mail_moderation" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_mail/mail_moderations/fetchdefault")
	public ResponseEntity<Page<Mail_moderationDTO>> fetchDefault(Mail_moderationSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Mail_moderationDTO> list = new ArrayList<Mail_moderationDTO>();
        
        Page<Mail_moderation> domains = mail_moderationService.searchDefault(context) ;
        for(Mail_moderation mail_moderation : domains.getContent()){
            Mail_moderationDTO dto = new Mail_moderationDTO();
            dto.fromDO(mail_moderation);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
