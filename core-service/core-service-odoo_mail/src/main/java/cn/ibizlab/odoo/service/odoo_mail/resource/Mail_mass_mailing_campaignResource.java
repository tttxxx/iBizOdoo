package cn.ibizlab.odoo.service.odoo_mail.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_mail.dto.Mail_mass_mailing_campaignDTO;
import cn.ibizlab.odoo.core.odoo_mail.domain.Mail_mass_mailing_campaign;
import cn.ibizlab.odoo.core.odoo_mail.service.IMail_mass_mailing_campaignService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_mail.filter.Mail_mass_mailing_campaignSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Mail_mass_mailing_campaign" })
@RestController
@RequestMapping("")
public class Mail_mass_mailing_campaignResource {

    @Autowired
    private IMail_mass_mailing_campaignService mail_mass_mailing_campaignService;

    public IMail_mass_mailing_campaignService getMail_mass_mailing_campaignService() {
        return this.mail_mass_mailing_campaignService;
    }

    @ApiOperation(value = "获取数据", tags = {"Mail_mass_mailing_campaign" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_mail/mail_mass_mailing_campaigns/{mail_mass_mailing_campaign_id}")
    public ResponseEntity<Mail_mass_mailing_campaignDTO> get(@PathVariable("mail_mass_mailing_campaign_id") Integer mail_mass_mailing_campaign_id) {
        Mail_mass_mailing_campaignDTO dto = new Mail_mass_mailing_campaignDTO();
        Mail_mass_mailing_campaign domain = mail_mass_mailing_campaignService.get(mail_mass_mailing_campaign_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批建立数据", tags = {"Mail_mass_mailing_campaign" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_mail/mail_mass_mailing_campaigns/createBatch")
    public ResponseEntity<Boolean> createBatchMail_mass_mailing_campaign(@RequestBody List<Mail_mass_mailing_campaignDTO> mail_mass_mailing_campaigndtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "批删除数据", tags = {"Mail_mass_mailing_campaign" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_mail/mail_mass_mailing_campaigns/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Mail_mass_mailing_campaignDTO> mail_mass_mailing_campaigndtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "批更新数据", tags = {"Mail_mass_mailing_campaign" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_mail/mail_mass_mailing_campaigns/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Mail_mass_mailing_campaignDTO> mail_mass_mailing_campaigndtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "建立数据", tags = {"Mail_mass_mailing_campaign" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_mail/mail_mass_mailing_campaigns")

    public ResponseEntity<Mail_mass_mailing_campaignDTO> create(@RequestBody Mail_mass_mailing_campaignDTO mail_mass_mailing_campaigndto) {
        Mail_mass_mailing_campaignDTO dto = new Mail_mass_mailing_campaignDTO();
        Mail_mass_mailing_campaign domain = mail_mass_mailing_campaigndto.toDO();
		mail_mass_mailing_campaignService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "更新数据", tags = {"Mail_mass_mailing_campaign" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_mail/mail_mass_mailing_campaigns/{mail_mass_mailing_campaign_id}")

    public ResponseEntity<Mail_mass_mailing_campaignDTO> update(@PathVariable("mail_mass_mailing_campaign_id") Integer mail_mass_mailing_campaign_id, @RequestBody Mail_mass_mailing_campaignDTO mail_mass_mailing_campaigndto) {
		Mail_mass_mailing_campaign domain = mail_mass_mailing_campaigndto.toDO();
        domain.setId(mail_mass_mailing_campaign_id);
		mail_mass_mailing_campaignService.update(domain);
		Mail_mass_mailing_campaignDTO dto = new Mail_mass_mailing_campaignDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "删除数据", tags = {"Mail_mass_mailing_campaign" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_mail/mail_mass_mailing_campaigns/{mail_mass_mailing_campaign_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("mail_mass_mailing_campaign_id") Integer mail_mass_mailing_campaign_id) {
        Mail_mass_mailing_campaignDTO mail_mass_mailing_campaigndto = new Mail_mass_mailing_campaignDTO();
		Mail_mass_mailing_campaign domain = new Mail_mass_mailing_campaign();
		mail_mass_mailing_campaigndto.setId(mail_mass_mailing_campaign_id);
		domain.setId(mail_mass_mailing_campaign_id);
        Boolean rst = mail_mass_mailing_campaignService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

	@ApiOperation(value = "获取默认查询", tags = {"Mail_mass_mailing_campaign" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_mail/mail_mass_mailing_campaigns/fetchdefault")
	public ResponseEntity<Page<Mail_mass_mailing_campaignDTO>> fetchDefault(Mail_mass_mailing_campaignSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Mail_mass_mailing_campaignDTO> list = new ArrayList<Mail_mass_mailing_campaignDTO>();
        
        Page<Mail_mass_mailing_campaign> domains = mail_mass_mailing_campaignService.searchDefault(context) ;
        for(Mail_mass_mailing_campaign mail_mass_mailing_campaign : domains.getContent()){
            Mail_mass_mailing_campaignDTO dto = new Mail_mass_mailing_campaignDTO();
            dto.fromDO(mail_mass_mailing_campaign);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
