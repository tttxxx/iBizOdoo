package cn.ibizlab.odoo.service.odoo_mail.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_mail.dto.Mail_mass_mailing_stageDTO;
import cn.ibizlab.odoo.core.odoo_mail.domain.Mail_mass_mailing_stage;
import cn.ibizlab.odoo.core.odoo_mail.service.IMail_mass_mailing_stageService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_mail.filter.Mail_mass_mailing_stageSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Mail_mass_mailing_stage" })
@RestController
@RequestMapping("")
public class Mail_mass_mailing_stageResource {

    @Autowired
    private IMail_mass_mailing_stageService mail_mass_mailing_stageService;

    public IMail_mass_mailing_stageService getMail_mass_mailing_stageService() {
        return this.mail_mass_mailing_stageService;
    }

    @ApiOperation(value = "更新数据", tags = {"Mail_mass_mailing_stage" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_mail/mail_mass_mailing_stages/{mail_mass_mailing_stage_id}")

    public ResponseEntity<Mail_mass_mailing_stageDTO> update(@PathVariable("mail_mass_mailing_stage_id") Integer mail_mass_mailing_stage_id, @RequestBody Mail_mass_mailing_stageDTO mail_mass_mailing_stagedto) {
		Mail_mass_mailing_stage domain = mail_mass_mailing_stagedto.toDO();
        domain.setId(mail_mass_mailing_stage_id);
		mail_mass_mailing_stageService.update(domain);
		Mail_mass_mailing_stageDTO dto = new Mail_mass_mailing_stageDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批建立数据", tags = {"Mail_mass_mailing_stage" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_mail/mail_mass_mailing_stages/createBatch")
    public ResponseEntity<Boolean> createBatchMail_mass_mailing_stage(@RequestBody List<Mail_mass_mailing_stageDTO> mail_mass_mailing_stagedtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "建立数据", tags = {"Mail_mass_mailing_stage" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_mail/mail_mass_mailing_stages")

    public ResponseEntity<Mail_mass_mailing_stageDTO> create(@RequestBody Mail_mass_mailing_stageDTO mail_mass_mailing_stagedto) {
        Mail_mass_mailing_stageDTO dto = new Mail_mass_mailing_stageDTO();
        Mail_mass_mailing_stage domain = mail_mass_mailing_stagedto.toDO();
		mail_mass_mailing_stageService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批更新数据", tags = {"Mail_mass_mailing_stage" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_mail/mail_mass_mailing_stages/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Mail_mass_mailing_stageDTO> mail_mass_mailing_stagedtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "获取数据", tags = {"Mail_mass_mailing_stage" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_mail/mail_mass_mailing_stages/{mail_mass_mailing_stage_id}")
    public ResponseEntity<Mail_mass_mailing_stageDTO> get(@PathVariable("mail_mass_mailing_stage_id") Integer mail_mass_mailing_stage_id) {
        Mail_mass_mailing_stageDTO dto = new Mail_mass_mailing_stageDTO();
        Mail_mass_mailing_stage domain = mail_mass_mailing_stageService.get(mail_mass_mailing_stage_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "删除数据", tags = {"Mail_mass_mailing_stage" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_mail/mail_mass_mailing_stages/{mail_mass_mailing_stage_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("mail_mass_mailing_stage_id") Integer mail_mass_mailing_stage_id) {
        Mail_mass_mailing_stageDTO mail_mass_mailing_stagedto = new Mail_mass_mailing_stageDTO();
		Mail_mass_mailing_stage domain = new Mail_mass_mailing_stage();
		mail_mass_mailing_stagedto.setId(mail_mass_mailing_stage_id);
		domain.setId(mail_mass_mailing_stage_id);
        Boolean rst = mail_mass_mailing_stageService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "批删除数据", tags = {"Mail_mass_mailing_stage" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_mail/mail_mass_mailing_stages/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Mail_mass_mailing_stageDTO> mail_mass_mailing_stagedtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Mail_mass_mailing_stage" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_mail/mail_mass_mailing_stages/fetchdefault")
	public ResponseEntity<Page<Mail_mass_mailing_stageDTO>> fetchDefault(Mail_mass_mailing_stageSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Mail_mass_mailing_stageDTO> list = new ArrayList<Mail_mass_mailing_stageDTO>();
        
        Page<Mail_mass_mailing_stage> domains = mail_mass_mailing_stageService.searchDefault(context) ;
        for(Mail_mass_mailing_stage mail_mass_mailing_stage : domains.getContent()){
            Mail_mass_mailing_stageDTO dto = new Mail_mass_mailing_stageDTO();
            dto.fromDO(mail_mass_mailing_stage);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
