package cn.ibizlab.odoo.service.odoo_mail.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_mail.dto.Mail_followersDTO;
import cn.ibizlab.odoo.core.odoo_mail.domain.Mail_followers;
import cn.ibizlab.odoo.core.odoo_mail.service.IMail_followersService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_mail.filter.Mail_followersSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Mail_followers" })
@RestController
@RequestMapping("")
public class Mail_followersResource {

    @Autowired
    private IMail_followersService mail_followersService;

    public IMail_followersService getMail_followersService() {
        return this.mail_followersService;
    }

    @ApiOperation(value = "获取数据", tags = {"Mail_followers" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_mail/mail_followers/{mail_followers_id}")
    public ResponseEntity<Mail_followersDTO> get(@PathVariable("mail_followers_id") Integer mail_followers_id) {
        Mail_followersDTO dto = new Mail_followersDTO();
        Mail_followers domain = mail_followersService.get(mail_followers_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "建立数据", tags = {"Mail_followers" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_mail/mail_followers")

    public ResponseEntity<Mail_followersDTO> create(@RequestBody Mail_followersDTO mail_followersdto) {
        Mail_followersDTO dto = new Mail_followersDTO();
        Mail_followers domain = mail_followersdto.toDO();
		mail_followersService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批删除数据", tags = {"Mail_followers" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_mail/mail_followers/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Mail_followersDTO> mail_followersdtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "批更新数据", tags = {"Mail_followers" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_mail/mail_followers/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Mail_followersDTO> mail_followersdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "更新数据", tags = {"Mail_followers" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_mail/mail_followers/{mail_followers_id}")

    public ResponseEntity<Mail_followersDTO> update(@PathVariable("mail_followers_id") Integer mail_followers_id, @RequestBody Mail_followersDTO mail_followersdto) {
		Mail_followers domain = mail_followersdto.toDO();
        domain.setId(mail_followers_id);
		mail_followersService.update(domain);
		Mail_followersDTO dto = new Mail_followersDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批建立数据", tags = {"Mail_followers" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_mail/mail_followers/createBatch")
    public ResponseEntity<Boolean> createBatchMail_followers(@RequestBody List<Mail_followersDTO> mail_followersdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "删除数据", tags = {"Mail_followers" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_mail/mail_followers/{mail_followers_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("mail_followers_id") Integer mail_followers_id) {
        Mail_followersDTO mail_followersdto = new Mail_followersDTO();
		Mail_followers domain = new Mail_followers();
		mail_followersdto.setId(mail_followers_id);
		domain.setId(mail_followers_id);
        Boolean rst = mail_followersService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

	@ApiOperation(value = "获取默认查询", tags = {"Mail_followers" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_mail/mail_followers/fetchdefault")
	public ResponseEntity<Page<Mail_followersDTO>> fetchDefault(Mail_followersSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Mail_followersDTO> list = new ArrayList<Mail_followersDTO>();
        
        Page<Mail_followers> domains = mail_followersService.searchDefault(context) ;
        for(Mail_followers mail_followers : domains.getContent()){
            Mail_followersDTO dto = new Mail_followersDTO();
            dto.fromDO(mail_followers);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
