package cn.ibizlab.odoo.service.odoo_mail.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_mail.dto.Mail_mass_mailingDTO;
import cn.ibizlab.odoo.core.odoo_mail.domain.Mail_mass_mailing;
import cn.ibizlab.odoo.core.odoo_mail.service.IMail_mass_mailingService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_mail.filter.Mail_mass_mailingSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Mail_mass_mailing" })
@RestController
@RequestMapping("")
public class Mail_mass_mailingResource {

    @Autowired
    private IMail_mass_mailingService mail_mass_mailingService;

    public IMail_mass_mailingService getMail_mass_mailingService() {
        return this.mail_mass_mailingService;
    }

    @ApiOperation(value = "更新数据", tags = {"Mail_mass_mailing" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_mail/mail_mass_mailings/{mail_mass_mailing_id}")

    public ResponseEntity<Mail_mass_mailingDTO> update(@PathVariable("mail_mass_mailing_id") Integer mail_mass_mailing_id, @RequestBody Mail_mass_mailingDTO mail_mass_mailingdto) {
		Mail_mass_mailing domain = mail_mass_mailingdto.toDO();
        domain.setId(mail_mass_mailing_id);
		mail_mass_mailingService.update(domain);
		Mail_mass_mailingDTO dto = new Mail_mass_mailingDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批建立数据", tags = {"Mail_mass_mailing" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_mail/mail_mass_mailings/createBatch")
    public ResponseEntity<Boolean> createBatchMail_mass_mailing(@RequestBody List<Mail_mass_mailingDTO> mail_mass_mailingdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "建立数据", tags = {"Mail_mass_mailing" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_mail/mail_mass_mailings")

    public ResponseEntity<Mail_mass_mailingDTO> create(@RequestBody Mail_mass_mailingDTO mail_mass_mailingdto) {
        Mail_mass_mailingDTO dto = new Mail_mass_mailingDTO();
        Mail_mass_mailing domain = mail_mass_mailingdto.toDO();
		mail_mass_mailingService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "删除数据", tags = {"Mail_mass_mailing" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_mail/mail_mass_mailings/{mail_mass_mailing_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("mail_mass_mailing_id") Integer mail_mass_mailing_id) {
        Mail_mass_mailingDTO mail_mass_mailingdto = new Mail_mass_mailingDTO();
		Mail_mass_mailing domain = new Mail_mass_mailing();
		mail_mass_mailingdto.setId(mail_mass_mailing_id);
		domain.setId(mail_mass_mailing_id);
        Boolean rst = mail_mass_mailingService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "获取数据", tags = {"Mail_mass_mailing" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_mail/mail_mass_mailings/{mail_mass_mailing_id}")
    public ResponseEntity<Mail_mass_mailingDTO> get(@PathVariable("mail_mass_mailing_id") Integer mail_mass_mailing_id) {
        Mail_mass_mailingDTO dto = new Mail_mass_mailingDTO();
        Mail_mass_mailing domain = mail_mass_mailingService.get(mail_mass_mailing_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批更新数据", tags = {"Mail_mass_mailing" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_mail/mail_mass_mailings/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Mail_mass_mailingDTO> mail_mass_mailingdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "批删除数据", tags = {"Mail_mass_mailing" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_mail/mail_mass_mailings/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Mail_mass_mailingDTO> mail_mass_mailingdtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Mail_mass_mailing" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_mail/mail_mass_mailings/fetchdefault")
	public ResponseEntity<Page<Mail_mass_mailingDTO>> fetchDefault(Mail_mass_mailingSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Mail_mass_mailingDTO> list = new ArrayList<Mail_mass_mailingDTO>();
        
        Page<Mail_mass_mailing> domains = mail_mass_mailingService.searchDefault(context) ;
        for(Mail_mass_mailing mail_mass_mailing : domains.getContent()){
            Mail_mass_mailingDTO dto = new Mail_mass_mailingDTO();
            dto.fromDO(mail_mass_mailing);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
