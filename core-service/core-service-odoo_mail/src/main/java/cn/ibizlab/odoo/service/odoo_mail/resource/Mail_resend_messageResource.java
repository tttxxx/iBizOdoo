package cn.ibizlab.odoo.service.odoo_mail.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_mail.dto.Mail_resend_messageDTO;
import cn.ibizlab.odoo.core.odoo_mail.domain.Mail_resend_message;
import cn.ibizlab.odoo.core.odoo_mail.service.IMail_resend_messageService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_mail.filter.Mail_resend_messageSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Mail_resend_message" })
@RestController
@RequestMapping("")
public class Mail_resend_messageResource {

    @Autowired
    private IMail_resend_messageService mail_resend_messageService;

    public IMail_resend_messageService getMail_resend_messageService() {
        return this.mail_resend_messageService;
    }

    @ApiOperation(value = "获取数据", tags = {"Mail_resend_message" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_mail/mail_resend_messages/{mail_resend_message_id}")
    public ResponseEntity<Mail_resend_messageDTO> get(@PathVariable("mail_resend_message_id") Integer mail_resend_message_id) {
        Mail_resend_messageDTO dto = new Mail_resend_messageDTO();
        Mail_resend_message domain = mail_resend_messageService.get(mail_resend_message_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批删除数据", tags = {"Mail_resend_message" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_mail/mail_resend_messages/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Mail_resend_messageDTO> mail_resend_messagedtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "删除数据", tags = {"Mail_resend_message" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_mail/mail_resend_messages/{mail_resend_message_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("mail_resend_message_id") Integer mail_resend_message_id) {
        Mail_resend_messageDTO mail_resend_messagedto = new Mail_resend_messageDTO();
		Mail_resend_message domain = new Mail_resend_message();
		mail_resend_messagedto.setId(mail_resend_message_id);
		domain.setId(mail_resend_message_id);
        Boolean rst = mail_resend_messageService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "批更新数据", tags = {"Mail_resend_message" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_mail/mail_resend_messages/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Mail_resend_messageDTO> mail_resend_messagedtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "建立数据", tags = {"Mail_resend_message" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_mail/mail_resend_messages")

    public ResponseEntity<Mail_resend_messageDTO> create(@RequestBody Mail_resend_messageDTO mail_resend_messagedto) {
        Mail_resend_messageDTO dto = new Mail_resend_messageDTO();
        Mail_resend_message domain = mail_resend_messagedto.toDO();
		mail_resend_messageService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批建立数据", tags = {"Mail_resend_message" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_mail/mail_resend_messages/createBatch")
    public ResponseEntity<Boolean> createBatchMail_resend_message(@RequestBody List<Mail_resend_messageDTO> mail_resend_messagedtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "更新数据", tags = {"Mail_resend_message" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_mail/mail_resend_messages/{mail_resend_message_id}")

    public ResponseEntity<Mail_resend_messageDTO> update(@PathVariable("mail_resend_message_id") Integer mail_resend_message_id, @RequestBody Mail_resend_messageDTO mail_resend_messagedto) {
		Mail_resend_message domain = mail_resend_messagedto.toDO();
        domain.setId(mail_resend_message_id);
		mail_resend_messageService.update(domain);
		Mail_resend_messageDTO dto = new Mail_resend_messageDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Mail_resend_message" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_mail/mail_resend_messages/fetchdefault")
	public ResponseEntity<Page<Mail_resend_messageDTO>> fetchDefault(Mail_resend_messageSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Mail_resend_messageDTO> list = new ArrayList<Mail_resend_messageDTO>();
        
        Page<Mail_resend_message> domains = mail_resend_messageService.searchDefault(context) ;
        for(Mail_resend_message mail_resend_message : domains.getContent()){
            Mail_resend_messageDTO dto = new Mail_resend_messageDTO();
            dto.fromDO(mail_resend_message);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
