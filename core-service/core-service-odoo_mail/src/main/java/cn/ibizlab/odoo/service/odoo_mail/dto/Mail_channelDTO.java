package cn.ibizlab.odoo.service.odoo_mail.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.core.odoo_mail.valuerule.anno.mail_channel.*;
import cn.ibizlab.odoo.core.odoo_mail.domain.Mail_channel;
import org.springframework.cglib.beans.BeanCopier;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 服务DTO对象[Mail_channelDTO]
 */
public class Mail_channelDTO implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [WEBSITE_MESSAGE_IDS]
     *
     */
    @Mail_channelWebsite_message_idsDefault(info = "默认规则")
    private String website_message_ids;

    @JsonIgnore
    private boolean website_message_idsDirtyFlag;

    /**
     * 属性 [IMAGE_MEDIUM]
     *
     */
    @Mail_channelImage_mediumDefault(info = "默认规则")
    private byte[] image_medium;

    @JsonIgnore
    private boolean image_mediumDirtyFlag;

    /**
     * 属性 [MODERATION_COUNT]
     *
     */
    @Mail_channelModeration_countDefault(info = "默认规则")
    private Integer moderation_count;

    @JsonIgnore
    private boolean moderation_countDirtyFlag;

    /**
     * 属性 [DESCRIPTION]
     *
     */
    @Mail_channelDescriptionDefault(info = "默认规则")
    private String description;

    @JsonIgnore
    private boolean descriptionDirtyFlag;

    /**
     * 属性 [MODERATION_NOTIFY]
     *
     */
    @Mail_channelModeration_notifyDefault(info = "默认规则")
    private String moderation_notify;

    @JsonIgnore
    private boolean moderation_notifyDirtyFlag;

    /**
     * 属性 [MESSAGE_MAIN_ATTACHMENT_ID]
     *
     */
    @Mail_channelMessage_main_attachment_idDefault(info = "默认规则")
    private Integer message_main_attachment_id;

    @JsonIgnore
    private boolean message_main_attachment_idDirtyFlag;

    /**
     * 属性 [MODERATION]
     *
     */
    @Mail_channelModerationDefault(info = "默认规则")
    private String moderation;

    @JsonIgnore
    private boolean moderationDirtyFlag;

    /**
     * 属性 [IS_MODERATOR]
     *
     */
    @Mail_channelIs_moderatorDefault(info = "默认规则")
    private String is_moderator;

    @JsonIgnore
    private boolean is_moderatorDirtyFlag;

    /**
     * 属性 [MODERATION_NOTIFY_MSG]
     *
     */
    @Mail_channelModeration_notify_msgDefault(info = "默认规则")
    private String moderation_notify_msg;

    @JsonIgnore
    private boolean moderation_notify_msgDirtyFlag;

    /**
     * 属性 [MODERATION_IDS]
     *
     */
    @Mail_channelModeration_idsDefault(info = "默认规则")
    private String moderation_ids;

    @JsonIgnore
    private boolean moderation_idsDirtyFlag;

    /**
     * 属性 [RATING_LAST_IMAGE]
     *
     */
    @Mail_channelRating_last_imageDefault(info = "默认规则")
    private byte[] rating_last_image;

    @JsonIgnore
    private boolean rating_last_imageDirtyFlag;

    /**
     * 属性 [MESSAGE_HAS_ERROR]
     *
     */
    @Mail_channelMessage_has_errorDefault(info = "默认规则")
    private String message_has_error;

    @JsonIgnore
    private boolean message_has_errorDirtyFlag;

    /**
     * 属性 [MODERATION_GUIDELINES]
     *
     */
    @Mail_channelModeration_guidelinesDefault(info = "默认规则")
    private String moderation_guidelines;

    @JsonIgnore
    private boolean moderation_guidelinesDirtyFlag;

    /**
     * 属性 [RATING_LAST_FEEDBACK]
     *
     */
    @Mail_channelRating_last_feedbackDefault(info = "默认规则")
    private String rating_last_feedback;

    @JsonIgnore
    private boolean rating_last_feedbackDirtyFlag;

    /**
     * 属性 [MESSAGE_IDS]
     *
     */
    @Mail_channelMessage_idsDefault(info = "默认规则")
    private String message_ids;

    @JsonIgnore
    private boolean message_idsDirtyFlag;

    /**
     * 属性 [SUBSCRIPTION_DEPARTMENT_IDS]
     *
     */
    @Mail_channelSubscription_department_idsDefault(info = "默认规则")
    private String subscription_department_ids;

    @JsonIgnore
    private boolean subscription_department_idsDirtyFlag;

    /**
     * 属性 [MESSAGE_UNREAD_COUNTER]
     *
     */
    @Mail_channelMessage_unread_counterDefault(info = "默认规则")
    private Integer message_unread_counter;

    @JsonIgnore
    private boolean message_unread_counterDirtyFlag;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @Mail_channel__last_updateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp __last_update;

    @JsonIgnore
    private boolean __last_updateDirtyFlag;

    /**
     * 属性 [IBIZPUBLIC]
     *
     */
    @Mail_channelIbizpublicDefault(info = "默认规则")
    private String ibizpublic;

    @JsonIgnore
    private boolean ibizpublicDirtyFlag;

    /**
     * 属性 [MESSAGE_FOLLOWER_IDS]
     *
     */
    @Mail_channelMessage_follower_idsDefault(info = "默认规则")
    private String message_follower_ids;

    @JsonIgnore
    private boolean message_follower_idsDirtyFlag;

    /**
     * 属性 [MESSAGE_CHANNEL_IDS]
     *
     */
    @Mail_channelMessage_channel_idsDefault(info = "默认规则")
    private String message_channel_ids;

    @JsonIgnore
    private boolean message_channel_idsDirtyFlag;

    /**
     * 属性 [RATING_IDS]
     *
     */
    @Mail_channelRating_idsDefault(info = "默认规则")
    private String rating_ids;

    @JsonIgnore
    private boolean rating_idsDirtyFlag;

    /**
     * 属性 [ANONYMOUS_NAME]
     *
     */
    @Mail_channelAnonymous_nameDefault(info = "默认规则")
    private String anonymous_name;

    @JsonIgnore
    private boolean anonymous_nameDirtyFlag;

    /**
     * 属性 [NAME]
     *
     */
    @Mail_channelNameDefault(info = "默认规则")
    private String name;

    @JsonIgnore
    private boolean nameDirtyFlag;

    /**
     * 属性 [IS_MEMBER]
     *
     */
    @Mail_channelIs_memberDefault(info = "默认规则")
    private String is_member;

    @JsonIgnore
    private boolean is_memberDirtyFlag;

    /**
     * 属性 [IS_SUBSCRIBED]
     *
     */
    @Mail_channelIs_subscribedDefault(info = "默认规则")
    private String is_subscribed;

    @JsonIgnore
    private boolean is_subscribedDirtyFlag;

    /**
     * 属性 [MODERATION_GUIDELINES_MSG]
     *
     */
    @Mail_channelModeration_guidelines_msgDefault(info = "默认规则")
    private String moderation_guidelines_msg;

    @JsonIgnore
    private boolean moderation_guidelines_msgDirtyFlag;

    /**
     * 属性 [MESSAGE_IS_FOLLOWER]
     *
     */
    @Mail_channelMessage_is_followerDefault(info = "默认规则")
    private String message_is_follower;

    @JsonIgnore
    private boolean message_is_followerDirtyFlag;

    /**
     * 属性 [ID]
     *
     */
    @Mail_channelIdDefault(info = "默认规则")
    private Integer id;

    @JsonIgnore
    private boolean idDirtyFlag;

    /**
     * 属性 [WRITE_DATE]
     *
     */
    @Mail_channelWrite_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp write_date;

    @JsonIgnore
    private boolean write_dateDirtyFlag;

    /**
     * 属性 [IMAGE]
     *
     */
    @Mail_channelImageDefault(info = "默认规则")
    private byte[] image;

    @JsonIgnore
    private boolean imageDirtyFlag;

    /**
     * 属性 [MESSAGE_ATTACHMENT_COUNT]
     *
     */
    @Mail_channelMessage_attachment_countDefault(info = "默认规则")
    private Integer message_attachment_count;

    @JsonIgnore
    private boolean message_attachment_countDirtyFlag;

    /**
     * 属性 [MESSAGE_UNREAD]
     *
     */
    @Mail_channelMessage_unreadDefault(info = "默认规则")
    private String message_unread;

    @JsonIgnore
    private boolean message_unreadDirtyFlag;

    /**
     * 属性 [CREATE_DATE]
     *
     */
    @Mail_channelCreate_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp create_date;

    @JsonIgnore
    private boolean create_dateDirtyFlag;

    /**
     * 属性 [IMAGE_SMALL]
     *
     */
    @Mail_channelImage_smallDefault(info = "默认规则")
    private byte[] image_small;

    @JsonIgnore
    private boolean image_smallDirtyFlag;

    /**
     * 属性 [MESSAGE_PARTNER_IDS]
     *
     */
    @Mail_channelMessage_partner_idsDefault(info = "默认规则")
    private String message_partner_ids;

    @JsonIgnore
    private boolean message_partner_idsDirtyFlag;

    /**
     * 属性 [MESSAGE_HAS_ERROR_COUNTER]
     *
     */
    @Mail_channelMessage_has_error_counterDefault(info = "默认规则")
    private Integer message_has_error_counter;

    @JsonIgnore
    private boolean message_has_error_counterDirtyFlag;

    /**
     * 属性 [CHANNEL_PARTNER_IDS]
     *
     */
    @Mail_channelChannel_partner_idsDefault(info = "默认规则")
    private String channel_partner_ids;

    @JsonIgnore
    private boolean channel_partner_idsDirtyFlag;

    /**
     * 属性 [MODERATOR_IDS]
     *
     */
    @Mail_channelModerator_idsDefault(info = "默认规则")
    private String moderator_ids;

    @JsonIgnore
    private boolean moderator_idsDirtyFlag;

    /**
     * 属性 [GROUP_IDS]
     *
     */
    @Mail_channelGroup_idsDefault(info = "默认规则")
    private String group_ids;

    @JsonIgnore
    private boolean group_idsDirtyFlag;

    /**
     * 属性 [EMAIL_SEND]
     *
     */
    @Mail_channelEmail_sendDefault(info = "默认规则")
    private String email_send;

    @JsonIgnore
    private boolean email_sendDirtyFlag;

    /**
     * 属性 [CHANNEL_LAST_SEEN_PARTNER_IDS]
     *
     */
    @Mail_channelChannel_last_seen_partner_idsDefault(info = "默认规则")
    private String channel_last_seen_partner_ids;

    @JsonIgnore
    private boolean channel_last_seen_partner_idsDirtyFlag;

    /**
     * 属性 [MESSAGE_NEEDACTION]
     *
     */
    @Mail_channelMessage_needactionDefault(info = "默认规则")
    private String message_needaction;

    @JsonIgnore
    private boolean message_needactionDirtyFlag;

    /**
     * 属性 [UUID]
     *
     */
    @Mail_channelUuidDefault(info = "默认规则")
    private String uuid;

    @JsonIgnore
    private boolean uuidDirtyFlag;

    /**
     * 属性 [RATING_COUNT]
     *
     */
    @Mail_channelRating_countDefault(info = "默认规则")
    private Integer rating_count;

    @JsonIgnore
    private boolean rating_countDirtyFlag;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @Mail_channelDisplay_nameDefault(info = "默认规则")
    private String display_name;

    @JsonIgnore
    private boolean display_nameDirtyFlag;

    /**
     * 属性 [MESSAGE_NEEDACTION_COUNTER]
     *
     */
    @Mail_channelMessage_needaction_counterDefault(info = "默认规则")
    private Integer message_needaction_counter;

    @JsonIgnore
    private boolean message_needaction_counterDirtyFlag;

    /**
     * 属性 [CHANNEL_MESSAGE_IDS]
     *
     */
    @Mail_channelChannel_message_idsDefault(info = "默认规则")
    private String channel_message_ids;

    @JsonIgnore
    private boolean channel_message_idsDirtyFlag;

    /**
     * 属性 [CHANNEL_TYPE]
     *
     */
    @Mail_channelChannel_typeDefault(info = "默认规则")
    private String channel_type;

    @JsonIgnore
    private boolean channel_typeDirtyFlag;

    /**
     * 属性 [IS_CHAT]
     *
     */
    @Mail_channelIs_chatDefault(info = "默认规则")
    private String is_chat;

    @JsonIgnore
    private boolean is_chatDirtyFlag;

    /**
     * 属性 [RATING_LAST_VALUE]
     *
     */
    @Mail_channelRating_last_valueDefault(info = "默认规则")
    private Double rating_last_value;

    @JsonIgnore
    private boolean rating_last_valueDirtyFlag;

    /**
     * 属性 [ALIAS_FORCE_THREAD_ID]
     *
     */
    @Mail_channelAlias_force_thread_idDefault(info = "默认规则")
    private Integer alias_force_thread_id;

    @JsonIgnore
    private boolean alias_force_thread_idDirtyFlag;

    /**
     * 属性 [ALIAS_DEFAULTS]
     *
     */
    @Mail_channelAlias_defaultsDefault(info = "默认规则")
    private String alias_defaults;

    @JsonIgnore
    private boolean alias_defaultsDirtyFlag;

    /**
     * 属性 [ALIAS_USER_ID]
     *
     */
    @Mail_channelAlias_user_idDefault(info = "默认规则")
    private Integer alias_user_id;

    @JsonIgnore
    private boolean alias_user_idDirtyFlag;

    /**
     * 属性 [ALIAS_PARENT_MODEL_ID]
     *
     */
    @Mail_channelAlias_parent_model_idDefault(info = "默认规则")
    private Integer alias_parent_model_id;

    @JsonIgnore
    private boolean alias_parent_model_idDirtyFlag;

    /**
     * 属性 [GROUP_PUBLIC_ID_TEXT]
     *
     */
    @Mail_channelGroup_public_id_textDefault(info = "默认规则")
    private String group_public_id_text;

    @JsonIgnore
    private boolean group_public_id_textDirtyFlag;

    /**
     * 属性 [WRITE_UID_TEXT]
     *
     */
    @Mail_channelWrite_uid_textDefault(info = "默认规则")
    private String write_uid_text;

    @JsonIgnore
    private boolean write_uid_textDirtyFlag;

    /**
     * 属性 [ALIAS_PARENT_THREAD_ID]
     *
     */
    @Mail_channelAlias_parent_thread_idDefault(info = "默认规则")
    private Integer alias_parent_thread_id;

    @JsonIgnore
    private boolean alias_parent_thread_idDirtyFlag;

    /**
     * 属性 [ALIAS_NAME]
     *
     */
    @Mail_channelAlias_nameDefault(info = "默认规则")
    private String alias_name;

    @JsonIgnore
    private boolean alias_nameDirtyFlag;

    /**
     * 属性 [ALIAS_MODEL_ID]
     *
     */
    @Mail_channelAlias_model_idDefault(info = "默认规则")
    private Integer alias_model_id;

    @JsonIgnore
    private boolean alias_model_idDirtyFlag;

    /**
     * 属性 [LIVECHAT_CHANNEL_ID_TEXT]
     *
     */
    @Mail_channelLivechat_channel_id_textDefault(info = "默认规则")
    private String livechat_channel_id_text;

    @JsonIgnore
    private boolean livechat_channel_id_textDirtyFlag;

    /**
     * 属性 [ALIAS_DOMAIN]
     *
     */
    @Mail_channelAlias_domainDefault(info = "默认规则")
    private String alias_domain;

    @JsonIgnore
    private boolean alias_domainDirtyFlag;

    /**
     * 属性 [CREATE_UID_TEXT]
     *
     */
    @Mail_channelCreate_uid_textDefault(info = "默认规则")
    private String create_uid_text;

    @JsonIgnore
    private boolean create_uid_textDirtyFlag;

    /**
     * 属性 [ALIAS_CONTACT]
     *
     */
    @Mail_channelAlias_contactDefault(info = "默认规则")
    private String alias_contact;

    @JsonIgnore
    private boolean alias_contactDirtyFlag;

    /**
     * 属性 [GROUP_PUBLIC_ID]
     *
     */
    @Mail_channelGroup_public_idDefault(info = "默认规则")
    private Integer group_public_id;

    @JsonIgnore
    private boolean group_public_idDirtyFlag;

    /**
     * 属性 [WRITE_UID]
     *
     */
    @Mail_channelWrite_uidDefault(info = "默认规则")
    private Integer write_uid;

    @JsonIgnore
    private boolean write_uidDirtyFlag;

    /**
     * 属性 [CREATE_UID]
     *
     */
    @Mail_channelCreate_uidDefault(info = "默认规则")
    private Integer create_uid;

    @JsonIgnore
    private boolean create_uidDirtyFlag;

    /**
     * 属性 [ALIAS_ID]
     *
     */
    @Mail_channelAlias_idDefault(info = "默认规则")
    private Integer alias_id;

    @JsonIgnore
    private boolean alias_idDirtyFlag;

    /**
     * 属性 [LIVECHAT_CHANNEL_ID]
     *
     */
    @Mail_channelLivechat_channel_idDefault(info = "默认规则")
    private Integer livechat_channel_id;

    @JsonIgnore
    private boolean livechat_channel_idDirtyFlag;


    /**
     * 获取 [WEBSITE_MESSAGE_IDS]
     */
    @JsonProperty("website_message_ids")
    public String getWebsite_message_ids(){
        return website_message_ids ;
    }

    /**
     * 设置 [WEBSITE_MESSAGE_IDS]
     */
    @JsonProperty("website_message_ids")
    public void setWebsite_message_ids(String  website_message_ids){
        this.website_message_ids = website_message_ids ;
        this.website_message_idsDirtyFlag = true ;
    }

    /**
     * 获取 [WEBSITE_MESSAGE_IDS]脏标记
     */
    @JsonIgnore
    public boolean getWebsite_message_idsDirtyFlag(){
        return website_message_idsDirtyFlag ;
    }

    /**
     * 获取 [IMAGE_MEDIUM]
     */
    @JsonProperty("image_medium")
    public byte[] getImage_medium(){
        return image_medium ;
    }

    /**
     * 设置 [IMAGE_MEDIUM]
     */
    @JsonProperty("image_medium")
    public void setImage_medium(byte[]  image_medium){
        this.image_medium = image_medium ;
        this.image_mediumDirtyFlag = true ;
    }

    /**
     * 获取 [IMAGE_MEDIUM]脏标记
     */
    @JsonIgnore
    public boolean getImage_mediumDirtyFlag(){
        return image_mediumDirtyFlag ;
    }

    /**
     * 获取 [MODERATION_COUNT]
     */
    @JsonProperty("moderation_count")
    public Integer getModeration_count(){
        return moderation_count ;
    }

    /**
     * 设置 [MODERATION_COUNT]
     */
    @JsonProperty("moderation_count")
    public void setModeration_count(Integer  moderation_count){
        this.moderation_count = moderation_count ;
        this.moderation_countDirtyFlag = true ;
    }

    /**
     * 获取 [MODERATION_COUNT]脏标记
     */
    @JsonIgnore
    public boolean getModeration_countDirtyFlag(){
        return moderation_countDirtyFlag ;
    }

    /**
     * 获取 [DESCRIPTION]
     */
    @JsonProperty("description")
    public String getDescription(){
        return description ;
    }

    /**
     * 设置 [DESCRIPTION]
     */
    @JsonProperty("description")
    public void setDescription(String  description){
        this.description = description ;
        this.descriptionDirtyFlag = true ;
    }

    /**
     * 获取 [DESCRIPTION]脏标记
     */
    @JsonIgnore
    public boolean getDescriptionDirtyFlag(){
        return descriptionDirtyFlag ;
    }

    /**
     * 获取 [MODERATION_NOTIFY]
     */
    @JsonProperty("moderation_notify")
    public String getModeration_notify(){
        return moderation_notify ;
    }

    /**
     * 设置 [MODERATION_NOTIFY]
     */
    @JsonProperty("moderation_notify")
    public void setModeration_notify(String  moderation_notify){
        this.moderation_notify = moderation_notify ;
        this.moderation_notifyDirtyFlag = true ;
    }

    /**
     * 获取 [MODERATION_NOTIFY]脏标记
     */
    @JsonIgnore
    public boolean getModeration_notifyDirtyFlag(){
        return moderation_notifyDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_MAIN_ATTACHMENT_ID]
     */
    @JsonProperty("message_main_attachment_id")
    public Integer getMessage_main_attachment_id(){
        return message_main_attachment_id ;
    }

    /**
     * 设置 [MESSAGE_MAIN_ATTACHMENT_ID]
     */
    @JsonProperty("message_main_attachment_id")
    public void setMessage_main_attachment_id(Integer  message_main_attachment_id){
        this.message_main_attachment_id = message_main_attachment_id ;
        this.message_main_attachment_idDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_MAIN_ATTACHMENT_ID]脏标记
     */
    @JsonIgnore
    public boolean getMessage_main_attachment_idDirtyFlag(){
        return message_main_attachment_idDirtyFlag ;
    }

    /**
     * 获取 [MODERATION]
     */
    @JsonProperty("moderation")
    public String getModeration(){
        return moderation ;
    }

    /**
     * 设置 [MODERATION]
     */
    @JsonProperty("moderation")
    public void setModeration(String  moderation){
        this.moderation = moderation ;
        this.moderationDirtyFlag = true ;
    }

    /**
     * 获取 [MODERATION]脏标记
     */
    @JsonIgnore
    public boolean getModerationDirtyFlag(){
        return moderationDirtyFlag ;
    }

    /**
     * 获取 [IS_MODERATOR]
     */
    @JsonProperty("is_moderator")
    public String getIs_moderator(){
        return is_moderator ;
    }

    /**
     * 设置 [IS_MODERATOR]
     */
    @JsonProperty("is_moderator")
    public void setIs_moderator(String  is_moderator){
        this.is_moderator = is_moderator ;
        this.is_moderatorDirtyFlag = true ;
    }

    /**
     * 获取 [IS_MODERATOR]脏标记
     */
    @JsonIgnore
    public boolean getIs_moderatorDirtyFlag(){
        return is_moderatorDirtyFlag ;
    }

    /**
     * 获取 [MODERATION_NOTIFY_MSG]
     */
    @JsonProperty("moderation_notify_msg")
    public String getModeration_notify_msg(){
        return moderation_notify_msg ;
    }

    /**
     * 设置 [MODERATION_NOTIFY_MSG]
     */
    @JsonProperty("moderation_notify_msg")
    public void setModeration_notify_msg(String  moderation_notify_msg){
        this.moderation_notify_msg = moderation_notify_msg ;
        this.moderation_notify_msgDirtyFlag = true ;
    }

    /**
     * 获取 [MODERATION_NOTIFY_MSG]脏标记
     */
    @JsonIgnore
    public boolean getModeration_notify_msgDirtyFlag(){
        return moderation_notify_msgDirtyFlag ;
    }

    /**
     * 获取 [MODERATION_IDS]
     */
    @JsonProperty("moderation_ids")
    public String getModeration_ids(){
        return moderation_ids ;
    }

    /**
     * 设置 [MODERATION_IDS]
     */
    @JsonProperty("moderation_ids")
    public void setModeration_ids(String  moderation_ids){
        this.moderation_ids = moderation_ids ;
        this.moderation_idsDirtyFlag = true ;
    }

    /**
     * 获取 [MODERATION_IDS]脏标记
     */
    @JsonIgnore
    public boolean getModeration_idsDirtyFlag(){
        return moderation_idsDirtyFlag ;
    }

    /**
     * 获取 [RATING_LAST_IMAGE]
     */
    @JsonProperty("rating_last_image")
    public byte[] getRating_last_image(){
        return rating_last_image ;
    }

    /**
     * 设置 [RATING_LAST_IMAGE]
     */
    @JsonProperty("rating_last_image")
    public void setRating_last_image(byte[]  rating_last_image){
        this.rating_last_image = rating_last_image ;
        this.rating_last_imageDirtyFlag = true ;
    }

    /**
     * 获取 [RATING_LAST_IMAGE]脏标记
     */
    @JsonIgnore
    public boolean getRating_last_imageDirtyFlag(){
        return rating_last_imageDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_HAS_ERROR]
     */
    @JsonProperty("message_has_error")
    public String getMessage_has_error(){
        return message_has_error ;
    }

    /**
     * 设置 [MESSAGE_HAS_ERROR]
     */
    @JsonProperty("message_has_error")
    public void setMessage_has_error(String  message_has_error){
        this.message_has_error = message_has_error ;
        this.message_has_errorDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_HAS_ERROR]脏标记
     */
    @JsonIgnore
    public boolean getMessage_has_errorDirtyFlag(){
        return message_has_errorDirtyFlag ;
    }

    /**
     * 获取 [MODERATION_GUIDELINES]
     */
    @JsonProperty("moderation_guidelines")
    public String getModeration_guidelines(){
        return moderation_guidelines ;
    }

    /**
     * 设置 [MODERATION_GUIDELINES]
     */
    @JsonProperty("moderation_guidelines")
    public void setModeration_guidelines(String  moderation_guidelines){
        this.moderation_guidelines = moderation_guidelines ;
        this.moderation_guidelinesDirtyFlag = true ;
    }

    /**
     * 获取 [MODERATION_GUIDELINES]脏标记
     */
    @JsonIgnore
    public boolean getModeration_guidelinesDirtyFlag(){
        return moderation_guidelinesDirtyFlag ;
    }

    /**
     * 获取 [RATING_LAST_FEEDBACK]
     */
    @JsonProperty("rating_last_feedback")
    public String getRating_last_feedback(){
        return rating_last_feedback ;
    }

    /**
     * 设置 [RATING_LAST_FEEDBACK]
     */
    @JsonProperty("rating_last_feedback")
    public void setRating_last_feedback(String  rating_last_feedback){
        this.rating_last_feedback = rating_last_feedback ;
        this.rating_last_feedbackDirtyFlag = true ;
    }

    /**
     * 获取 [RATING_LAST_FEEDBACK]脏标记
     */
    @JsonIgnore
    public boolean getRating_last_feedbackDirtyFlag(){
        return rating_last_feedbackDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_IDS]
     */
    @JsonProperty("message_ids")
    public String getMessage_ids(){
        return message_ids ;
    }

    /**
     * 设置 [MESSAGE_IDS]
     */
    @JsonProperty("message_ids")
    public void setMessage_ids(String  message_ids){
        this.message_ids = message_ids ;
        this.message_idsDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_IDS]脏标记
     */
    @JsonIgnore
    public boolean getMessage_idsDirtyFlag(){
        return message_idsDirtyFlag ;
    }

    /**
     * 获取 [SUBSCRIPTION_DEPARTMENT_IDS]
     */
    @JsonProperty("subscription_department_ids")
    public String getSubscription_department_ids(){
        return subscription_department_ids ;
    }

    /**
     * 设置 [SUBSCRIPTION_DEPARTMENT_IDS]
     */
    @JsonProperty("subscription_department_ids")
    public void setSubscription_department_ids(String  subscription_department_ids){
        this.subscription_department_ids = subscription_department_ids ;
        this.subscription_department_idsDirtyFlag = true ;
    }

    /**
     * 获取 [SUBSCRIPTION_DEPARTMENT_IDS]脏标记
     */
    @JsonIgnore
    public boolean getSubscription_department_idsDirtyFlag(){
        return subscription_department_idsDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_UNREAD_COUNTER]
     */
    @JsonProperty("message_unread_counter")
    public Integer getMessage_unread_counter(){
        return message_unread_counter ;
    }

    /**
     * 设置 [MESSAGE_UNREAD_COUNTER]
     */
    @JsonProperty("message_unread_counter")
    public void setMessage_unread_counter(Integer  message_unread_counter){
        this.message_unread_counter = message_unread_counter ;
        this.message_unread_counterDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_UNREAD_COUNTER]脏标记
     */
    @JsonIgnore
    public boolean getMessage_unread_counterDirtyFlag(){
        return message_unread_counterDirtyFlag ;
    }

    /**
     * 获取 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return __last_update ;
    }

    /**
     * 设置 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

    /**
     * 获取 [__LAST_UPDATE]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return __last_updateDirtyFlag ;
    }

    /**
     * 获取 [IBIZPUBLIC]
     */
    @JsonProperty("ibizpublic")
    public String getIbizpublic(){
        return ibizpublic ;
    }

    /**
     * 设置 [IBIZPUBLIC]
     */
    @JsonProperty("ibizpublic")
    public void setIbizpublic(String  ibizpublic){
        this.ibizpublic = ibizpublic ;
        this.ibizpublicDirtyFlag = true ;
    }

    /**
     * 获取 [IBIZPUBLIC]脏标记
     */
    @JsonIgnore
    public boolean getIbizpublicDirtyFlag(){
        return ibizpublicDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_FOLLOWER_IDS]
     */
    @JsonProperty("message_follower_ids")
    public String getMessage_follower_ids(){
        return message_follower_ids ;
    }

    /**
     * 设置 [MESSAGE_FOLLOWER_IDS]
     */
    @JsonProperty("message_follower_ids")
    public void setMessage_follower_ids(String  message_follower_ids){
        this.message_follower_ids = message_follower_ids ;
        this.message_follower_idsDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_FOLLOWER_IDS]脏标记
     */
    @JsonIgnore
    public boolean getMessage_follower_idsDirtyFlag(){
        return message_follower_idsDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_CHANNEL_IDS]
     */
    @JsonProperty("message_channel_ids")
    public String getMessage_channel_ids(){
        return message_channel_ids ;
    }

    /**
     * 设置 [MESSAGE_CHANNEL_IDS]
     */
    @JsonProperty("message_channel_ids")
    public void setMessage_channel_ids(String  message_channel_ids){
        this.message_channel_ids = message_channel_ids ;
        this.message_channel_idsDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_CHANNEL_IDS]脏标记
     */
    @JsonIgnore
    public boolean getMessage_channel_idsDirtyFlag(){
        return message_channel_idsDirtyFlag ;
    }

    /**
     * 获取 [RATING_IDS]
     */
    @JsonProperty("rating_ids")
    public String getRating_ids(){
        return rating_ids ;
    }

    /**
     * 设置 [RATING_IDS]
     */
    @JsonProperty("rating_ids")
    public void setRating_ids(String  rating_ids){
        this.rating_ids = rating_ids ;
        this.rating_idsDirtyFlag = true ;
    }

    /**
     * 获取 [RATING_IDS]脏标记
     */
    @JsonIgnore
    public boolean getRating_idsDirtyFlag(){
        return rating_idsDirtyFlag ;
    }

    /**
     * 获取 [ANONYMOUS_NAME]
     */
    @JsonProperty("anonymous_name")
    public String getAnonymous_name(){
        return anonymous_name ;
    }

    /**
     * 设置 [ANONYMOUS_NAME]
     */
    @JsonProperty("anonymous_name")
    public void setAnonymous_name(String  anonymous_name){
        this.anonymous_name = anonymous_name ;
        this.anonymous_nameDirtyFlag = true ;
    }

    /**
     * 获取 [ANONYMOUS_NAME]脏标记
     */
    @JsonIgnore
    public boolean getAnonymous_nameDirtyFlag(){
        return anonymous_nameDirtyFlag ;
    }

    /**
     * 获取 [NAME]
     */
    @JsonProperty("name")
    public String getName(){
        return name ;
    }

    /**
     * 设置 [NAME]
     */
    @JsonProperty("name")
    public void setName(String  name){
        this.name = name ;
        this.nameDirtyFlag = true ;
    }

    /**
     * 获取 [NAME]脏标记
     */
    @JsonIgnore
    public boolean getNameDirtyFlag(){
        return nameDirtyFlag ;
    }

    /**
     * 获取 [IS_MEMBER]
     */
    @JsonProperty("is_member")
    public String getIs_member(){
        return is_member ;
    }

    /**
     * 设置 [IS_MEMBER]
     */
    @JsonProperty("is_member")
    public void setIs_member(String  is_member){
        this.is_member = is_member ;
        this.is_memberDirtyFlag = true ;
    }

    /**
     * 获取 [IS_MEMBER]脏标记
     */
    @JsonIgnore
    public boolean getIs_memberDirtyFlag(){
        return is_memberDirtyFlag ;
    }

    /**
     * 获取 [IS_SUBSCRIBED]
     */
    @JsonProperty("is_subscribed")
    public String getIs_subscribed(){
        return is_subscribed ;
    }

    /**
     * 设置 [IS_SUBSCRIBED]
     */
    @JsonProperty("is_subscribed")
    public void setIs_subscribed(String  is_subscribed){
        this.is_subscribed = is_subscribed ;
        this.is_subscribedDirtyFlag = true ;
    }

    /**
     * 获取 [IS_SUBSCRIBED]脏标记
     */
    @JsonIgnore
    public boolean getIs_subscribedDirtyFlag(){
        return is_subscribedDirtyFlag ;
    }

    /**
     * 获取 [MODERATION_GUIDELINES_MSG]
     */
    @JsonProperty("moderation_guidelines_msg")
    public String getModeration_guidelines_msg(){
        return moderation_guidelines_msg ;
    }

    /**
     * 设置 [MODERATION_GUIDELINES_MSG]
     */
    @JsonProperty("moderation_guidelines_msg")
    public void setModeration_guidelines_msg(String  moderation_guidelines_msg){
        this.moderation_guidelines_msg = moderation_guidelines_msg ;
        this.moderation_guidelines_msgDirtyFlag = true ;
    }

    /**
     * 获取 [MODERATION_GUIDELINES_MSG]脏标记
     */
    @JsonIgnore
    public boolean getModeration_guidelines_msgDirtyFlag(){
        return moderation_guidelines_msgDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_IS_FOLLOWER]
     */
    @JsonProperty("message_is_follower")
    public String getMessage_is_follower(){
        return message_is_follower ;
    }

    /**
     * 设置 [MESSAGE_IS_FOLLOWER]
     */
    @JsonProperty("message_is_follower")
    public void setMessage_is_follower(String  message_is_follower){
        this.message_is_follower = message_is_follower ;
        this.message_is_followerDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_IS_FOLLOWER]脏标记
     */
    @JsonIgnore
    public boolean getMessage_is_followerDirtyFlag(){
        return message_is_followerDirtyFlag ;
    }

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

    /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return idDirtyFlag ;
    }

    /**
     * 获取 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return write_date ;
    }

    /**
     * 设置 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return write_dateDirtyFlag ;
    }

    /**
     * 获取 [IMAGE]
     */
    @JsonProperty("image")
    public byte[] getImage(){
        return image ;
    }

    /**
     * 设置 [IMAGE]
     */
    @JsonProperty("image")
    public void setImage(byte[]  image){
        this.image = image ;
        this.imageDirtyFlag = true ;
    }

    /**
     * 获取 [IMAGE]脏标记
     */
    @JsonIgnore
    public boolean getImageDirtyFlag(){
        return imageDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_ATTACHMENT_COUNT]
     */
    @JsonProperty("message_attachment_count")
    public Integer getMessage_attachment_count(){
        return message_attachment_count ;
    }

    /**
     * 设置 [MESSAGE_ATTACHMENT_COUNT]
     */
    @JsonProperty("message_attachment_count")
    public void setMessage_attachment_count(Integer  message_attachment_count){
        this.message_attachment_count = message_attachment_count ;
        this.message_attachment_countDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_ATTACHMENT_COUNT]脏标记
     */
    @JsonIgnore
    public boolean getMessage_attachment_countDirtyFlag(){
        return message_attachment_countDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_UNREAD]
     */
    @JsonProperty("message_unread")
    public String getMessage_unread(){
        return message_unread ;
    }

    /**
     * 设置 [MESSAGE_UNREAD]
     */
    @JsonProperty("message_unread")
    public void setMessage_unread(String  message_unread){
        this.message_unread = message_unread ;
        this.message_unreadDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_UNREAD]脏标记
     */
    @JsonIgnore
    public boolean getMessage_unreadDirtyFlag(){
        return message_unreadDirtyFlag ;
    }

    /**
     * 获取 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return create_date ;
    }

    /**
     * 设置 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return create_dateDirtyFlag ;
    }

    /**
     * 获取 [IMAGE_SMALL]
     */
    @JsonProperty("image_small")
    public byte[] getImage_small(){
        return image_small ;
    }

    /**
     * 设置 [IMAGE_SMALL]
     */
    @JsonProperty("image_small")
    public void setImage_small(byte[]  image_small){
        this.image_small = image_small ;
        this.image_smallDirtyFlag = true ;
    }

    /**
     * 获取 [IMAGE_SMALL]脏标记
     */
    @JsonIgnore
    public boolean getImage_smallDirtyFlag(){
        return image_smallDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_PARTNER_IDS]
     */
    @JsonProperty("message_partner_ids")
    public String getMessage_partner_ids(){
        return message_partner_ids ;
    }

    /**
     * 设置 [MESSAGE_PARTNER_IDS]
     */
    @JsonProperty("message_partner_ids")
    public void setMessage_partner_ids(String  message_partner_ids){
        this.message_partner_ids = message_partner_ids ;
        this.message_partner_idsDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_PARTNER_IDS]脏标记
     */
    @JsonIgnore
    public boolean getMessage_partner_idsDirtyFlag(){
        return message_partner_idsDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_HAS_ERROR_COUNTER]
     */
    @JsonProperty("message_has_error_counter")
    public Integer getMessage_has_error_counter(){
        return message_has_error_counter ;
    }

    /**
     * 设置 [MESSAGE_HAS_ERROR_COUNTER]
     */
    @JsonProperty("message_has_error_counter")
    public void setMessage_has_error_counter(Integer  message_has_error_counter){
        this.message_has_error_counter = message_has_error_counter ;
        this.message_has_error_counterDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_HAS_ERROR_COUNTER]脏标记
     */
    @JsonIgnore
    public boolean getMessage_has_error_counterDirtyFlag(){
        return message_has_error_counterDirtyFlag ;
    }

    /**
     * 获取 [CHANNEL_PARTNER_IDS]
     */
    @JsonProperty("channel_partner_ids")
    public String getChannel_partner_ids(){
        return channel_partner_ids ;
    }

    /**
     * 设置 [CHANNEL_PARTNER_IDS]
     */
    @JsonProperty("channel_partner_ids")
    public void setChannel_partner_ids(String  channel_partner_ids){
        this.channel_partner_ids = channel_partner_ids ;
        this.channel_partner_idsDirtyFlag = true ;
    }

    /**
     * 获取 [CHANNEL_PARTNER_IDS]脏标记
     */
    @JsonIgnore
    public boolean getChannel_partner_idsDirtyFlag(){
        return channel_partner_idsDirtyFlag ;
    }

    /**
     * 获取 [MODERATOR_IDS]
     */
    @JsonProperty("moderator_ids")
    public String getModerator_ids(){
        return moderator_ids ;
    }

    /**
     * 设置 [MODERATOR_IDS]
     */
    @JsonProperty("moderator_ids")
    public void setModerator_ids(String  moderator_ids){
        this.moderator_ids = moderator_ids ;
        this.moderator_idsDirtyFlag = true ;
    }

    /**
     * 获取 [MODERATOR_IDS]脏标记
     */
    @JsonIgnore
    public boolean getModerator_idsDirtyFlag(){
        return moderator_idsDirtyFlag ;
    }

    /**
     * 获取 [GROUP_IDS]
     */
    @JsonProperty("group_ids")
    public String getGroup_ids(){
        return group_ids ;
    }

    /**
     * 设置 [GROUP_IDS]
     */
    @JsonProperty("group_ids")
    public void setGroup_ids(String  group_ids){
        this.group_ids = group_ids ;
        this.group_idsDirtyFlag = true ;
    }

    /**
     * 获取 [GROUP_IDS]脏标记
     */
    @JsonIgnore
    public boolean getGroup_idsDirtyFlag(){
        return group_idsDirtyFlag ;
    }

    /**
     * 获取 [EMAIL_SEND]
     */
    @JsonProperty("email_send")
    public String getEmail_send(){
        return email_send ;
    }

    /**
     * 设置 [EMAIL_SEND]
     */
    @JsonProperty("email_send")
    public void setEmail_send(String  email_send){
        this.email_send = email_send ;
        this.email_sendDirtyFlag = true ;
    }

    /**
     * 获取 [EMAIL_SEND]脏标记
     */
    @JsonIgnore
    public boolean getEmail_sendDirtyFlag(){
        return email_sendDirtyFlag ;
    }

    /**
     * 获取 [CHANNEL_LAST_SEEN_PARTNER_IDS]
     */
    @JsonProperty("channel_last_seen_partner_ids")
    public String getChannel_last_seen_partner_ids(){
        return channel_last_seen_partner_ids ;
    }

    /**
     * 设置 [CHANNEL_LAST_SEEN_PARTNER_IDS]
     */
    @JsonProperty("channel_last_seen_partner_ids")
    public void setChannel_last_seen_partner_ids(String  channel_last_seen_partner_ids){
        this.channel_last_seen_partner_ids = channel_last_seen_partner_ids ;
        this.channel_last_seen_partner_idsDirtyFlag = true ;
    }

    /**
     * 获取 [CHANNEL_LAST_SEEN_PARTNER_IDS]脏标记
     */
    @JsonIgnore
    public boolean getChannel_last_seen_partner_idsDirtyFlag(){
        return channel_last_seen_partner_idsDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_NEEDACTION]
     */
    @JsonProperty("message_needaction")
    public String getMessage_needaction(){
        return message_needaction ;
    }

    /**
     * 设置 [MESSAGE_NEEDACTION]
     */
    @JsonProperty("message_needaction")
    public void setMessage_needaction(String  message_needaction){
        this.message_needaction = message_needaction ;
        this.message_needactionDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_NEEDACTION]脏标记
     */
    @JsonIgnore
    public boolean getMessage_needactionDirtyFlag(){
        return message_needactionDirtyFlag ;
    }

    /**
     * 获取 [UUID]
     */
    @JsonProperty("uuid")
    public String getUuid(){
        return uuid ;
    }

    /**
     * 设置 [UUID]
     */
    @JsonProperty("uuid")
    public void setUuid(String  uuid){
        this.uuid = uuid ;
        this.uuidDirtyFlag = true ;
    }

    /**
     * 获取 [UUID]脏标记
     */
    @JsonIgnore
    public boolean getUuidDirtyFlag(){
        return uuidDirtyFlag ;
    }

    /**
     * 获取 [RATING_COUNT]
     */
    @JsonProperty("rating_count")
    public Integer getRating_count(){
        return rating_count ;
    }

    /**
     * 设置 [RATING_COUNT]
     */
    @JsonProperty("rating_count")
    public void setRating_count(Integer  rating_count){
        this.rating_count = rating_count ;
        this.rating_countDirtyFlag = true ;
    }

    /**
     * 获取 [RATING_COUNT]脏标记
     */
    @JsonIgnore
    public boolean getRating_countDirtyFlag(){
        return rating_countDirtyFlag ;
    }

    /**
     * 获取 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return display_name ;
    }

    /**
     * 设置 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

    /**
     * 获取 [DISPLAY_NAME]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return display_nameDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_NEEDACTION_COUNTER]
     */
    @JsonProperty("message_needaction_counter")
    public Integer getMessage_needaction_counter(){
        return message_needaction_counter ;
    }

    /**
     * 设置 [MESSAGE_NEEDACTION_COUNTER]
     */
    @JsonProperty("message_needaction_counter")
    public void setMessage_needaction_counter(Integer  message_needaction_counter){
        this.message_needaction_counter = message_needaction_counter ;
        this.message_needaction_counterDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_NEEDACTION_COUNTER]脏标记
     */
    @JsonIgnore
    public boolean getMessage_needaction_counterDirtyFlag(){
        return message_needaction_counterDirtyFlag ;
    }

    /**
     * 获取 [CHANNEL_MESSAGE_IDS]
     */
    @JsonProperty("channel_message_ids")
    public String getChannel_message_ids(){
        return channel_message_ids ;
    }

    /**
     * 设置 [CHANNEL_MESSAGE_IDS]
     */
    @JsonProperty("channel_message_ids")
    public void setChannel_message_ids(String  channel_message_ids){
        this.channel_message_ids = channel_message_ids ;
        this.channel_message_idsDirtyFlag = true ;
    }

    /**
     * 获取 [CHANNEL_MESSAGE_IDS]脏标记
     */
    @JsonIgnore
    public boolean getChannel_message_idsDirtyFlag(){
        return channel_message_idsDirtyFlag ;
    }

    /**
     * 获取 [CHANNEL_TYPE]
     */
    @JsonProperty("channel_type")
    public String getChannel_type(){
        return channel_type ;
    }

    /**
     * 设置 [CHANNEL_TYPE]
     */
    @JsonProperty("channel_type")
    public void setChannel_type(String  channel_type){
        this.channel_type = channel_type ;
        this.channel_typeDirtyFlag = true ;
    }

    /**
     * 获取 [CHANNEL_TYPE]脏标记
     */
    @JsonIgnore
    public boolean getChannel_typeDirtyFlag(){
        return channel_typeDirtyFlag ;
    }

    /**
     * 获取 [IS_CHAT]
     */
    @JsonProperty("is_chat")
    public String getIs_chat(){
        return is_chat ;
    }

    /**
     * 设置 [IS_CHAT]
     */
    @JsonProperty("is_chat")
    public void setIs_chat(String  is_chat){
        this.is_chat = is_chat ;
        this.is_chatDirtyFlag = true ;
    }

    /**
     * 获取 [IS_CHAT]脏标记
     */
    @JsonIgnore
    public boolean getIs_chatDirtyFlag(){
        return is_chatDirtyFlag ;
    }

    /**
     * 获取 [RATING_LAST_VALUE]
     */
    @JsonProperty("rating_last_value")
    public Double getRating_last_value(){
        return rating_last_value ;
    }

    /**
     * 设置 [RATING_LAST_VALUE]
     */
    @JsonProperty("rating_last_value")
    public void setRating_last_value(Double  rating_last_value){
        this.rating_last_value = rating_last_value ;
        this.rating_last_valueDirtyFlag = true ;
    }

    /**
     * 获取 [RATING_LAST_VALUE]脏标记
     */
    @JsonIgnore
    public boolean getRating_last_valueDirtyFlag(){
        return rating_last_valueDirtyFlag ;
    }

    /**
     * 获取 [ALIAS_FORCE_THREAD_ID]
     */
    @JsonProperty("alias_force_thread_id")
    public Integer getAlias_force_thread_id(){
        return alias_force_thread_id ;
    }

    /**
     * 设置 [ALIAS_FORCE_THREAD_ID]
     */
    @JsonProperty("alias_force_thread_id")
    public void setAlias_force_thread_id(Integer  alias_force_thread_id){
        this.alias_force_thread_id = alias_force_thread_id ;
        this.alias_force_thread_idDirtyFlag = true ;
    }

    /**
     * 获取 [ALIAS_FORCE_THREAD_ID]脏标记
     */
    @JsonIgnore
    public boolean getAlias_force_thread_idDirtyFlag(){
        return alias_force_thread_idDirtyFlag ;
    }

    /**
     * 获取 [ALIAS_DEFAULTS]
     */
    @JsonProperty("alias_defaults")
    public String getAlias_defaults(){
        return alias_defaults ;
    }

    /**
     * 设置 [ALIAS_DEFAULTS]
     */
    @JsonProperty("alias_defaults")
    public void setAlias_defaults(String  alias_defaults){
        this.alias_defaults = alias_defaults ;
        this.alias_defaultsDirtyFlag = true ;
    }

    /**
     * 获取 [ALIAS_DEFAULTS]脏标记
     */
    @JsonIgnore
    public boolean getAlias_defaultsDirtyFlag(){
        return alias_defaultsDirtyFlag ;
    }

    /**
     * 获取 [ALIAS_USER_ID]
     */
    @JsonProperty("alias_user_id")
    public Integer getAlias_user_id(){
        return alias_user_id ;
    }

    /**
     * 设置 [ALIAS_USER_ID]
     */
    @JsonProperty("alias_user_id")
    public void setAlias_user_id(Integer  alias_user_id){
        this.alias_user_id = alias_user_id ;
        this.alias_user_idDirtyFlag = true ;
    }

    /**
     * 获取 [ALIAS_USER_ID]脏标记
     */
    @JsonIgnore
    public boolean getAlias_user_idDirtyFlag(){
        return alias_user_idDirtyFlag ;
    }

    /**
     * 获取 [ALIAS_PARENT_MODEL_ID]
     */
    @JsonProperty("alias_parent_model_id")
    public Integer getAlias_parent_model_id(){
        return alias_parent_model_id ;
    }

    /**
     * 设置 [ALIAS_PARENT_MODEL_ID]
     */
    @JsonProperty("alias_parent_model_id")
    public void setAlias_parent_model_id(Integer  alias_parent_model_id){
        this.alias_parent_model_id = alias_parent_model_id ;
        this.alias_parent_model_idDirtyFlag = true ;
    }

    /**
     * 获取 [ALIAS_PARENT_MODEL_ID]脏标记
     */
    @JsonIgnore
    public boolean getAlias_parent_model_idDirtyFlag(){
        return alias_parent_model_idDirtyFlag ;
    }

    /**
     * 获取 [GROUP_PUBLIC_ID_TEXT]
     */
    @JsonProperty("group_public_id_text")
    public String getGroup_public_id_text(){
        return group_public_id_text ;
    }

    /**
     * 设置 [GROUP_PUBLIC_ID_TEXT]
     */
    @JsonProperty("group_public_id_text")
    public void setGroup_public_id_text(String  group_public_id_text){
        this.group_public_id_text = group_public_id_text ;
        this.group_public_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [GROUP_PUBLIC_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getGroup_public_id_textDirtyFlag(){
        return group_public_id_textDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return write_uid_text ;
    }

    /**
     * 设置 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return write_uid_textDirtyFlag ;
    }

    /**
     * 获取 [ALIAS_PARENT_THREAD_ID]
     */
    @JsonProperty("alias_parent_thread_id")
    public Integer getAlias_parent_thread_id(){
        return alias_parent_thread_id ;
    }

    /**
     * 设置 [ALIAS_PARENT_THREAD_ID]
     */
    @JsonProperty("alias_parent_thread_id")
    public void setAlias_parent_thread_id(Integer  alias_parent_thread_id){
        this.alias_parent_thread_id = alias_parent_thread_id ;
        this.alias_parent_thread_idDirtyFlag = true ;
    }

    /**
     * 获取 [ALIAS_PARENT_THREAD_ID]脏标记
     */
    @JsonIgnore
    public boolean getAlias_parent_thread_idDirtyFlag(){
        return alias_parent_thread_idDirtyFlag ;
    }

    /**
     * 获取 [ALIAS_NAME]
     */
    @JsonProperty("alias_name")
    public String getAlias_name(){
        return alias_name ;
    }

    /**
     * 设置 [ALIAS_NAME]
     */
    @JsonProperty("alias_name")
    public void setAlias_name(String  alias_name){
        this.alias_name = alias_name ;
        this.alias_nameDirtyFlag = true ;
    }

    /**
     * 获取 [ALIAS_NAME]脏标记
     */
    @JsonIgnore
    public boolean getAlias_nameDirtyFlag(){
        return alias_nameDirtyFlag ;
    }

    /**
     * 获取 [ALIAS_MODEL_ID]
     */
    @JsonProperty("alias_model_id")
    public Integer getAlias_model_id(){
        return alias_model_id ;
    }

    /**
     * 设置 [ALIAS_MODEL_ID]
     */
    @JsonProperty("alias_model_id")
    public void setAlias_model_id(Integer  alias_model_id){
        this.alias_model_id = alias_model_id ;
        this.alias_model_idDirtyFlag = true ;
    }

    /**
     * 获取 [ALIAS_MODEL_ID]脏标记
     */
    @JsonIgnore
    public boolean getAlias_model_idDirtyFlag(){
        return alias_model_idDirtyFlag ;
    }

    /**
     * 获取 [LIVECHAT_CHANNEL_ID_TEXT]
     */
    @JsonProperty("livechat_channel_id_text")
    public String getLivechat_channel_id_text(){
        return livechat_channel_id_text ;
    }

    /**
     * 设置 [LIVECHAT_CHANNEL_ID_TEXT]
     */
    @JsonProperty("livechat_channel_id_text")
    public void setLivechat_channel_id_text(String  livechat_channel_id_text){
        this.livechat_channel_id_text = livechat_channel_id_text ;
        this.livechat_channel_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [LIVECHAT_CHANNEL_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getLivechat_channel_id_textDirtyFlag(){
        return livechat_channel_id_textDirtyFlag ;
    }

    /**
     * 获取 [ALIAS_DOMAIN]
     */
    @JsonProperty("alias_domain")
    public String getAlias_domain(){
        return alias_domain ;
    }

    /**
     * 设置 [ALIAS_DOMAIN]
     */
    @JsonProperty("alias_domain")
    public void setAlias_domain(String  alias_domain){
        this.alias_domain = alias_domain ;
        this.alias_domainDirtyFlag = true ;
    }

    /**
     * 获取 [ALIAS_DOMAIN]脏标记
     */
    @JsonIgnore
    public boolean getAlias_domainDirtyFlag(){
        return alias_domainDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return create_uid_text ;
    }

    /**
     * 设置 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return create_uid_textDirtyFlag ;
    }

    /**
     * 获取 [ALIAS_CONTACT]
     */
    @JsonProperty("alias_contact")
    public String getAlias_contact(){
        return alias_contact ;
    }

    /**
     * 设置 [ALIAS_CONTACT]
     */
    @JsonProperty("alias_contact")
    public void setAlias_contact(String  alias_contact){
        this.alias_contact = alias_contact ;
        this.alias_contactDirtyFlag = true ;
    }

    /**
     * 获取 [ALIAS_CONTACT]脏标记
     */
    @JsonIgnore
    public boolean getAlias_contactDirtyFlag(){
        return alias_contactDirtyFlag ;
    }

    /**
     * 获取 [GROUP_PUBLIC_ID]
     */
    @JsonProperty("group_public_id")
    public Integer getGroup_public_id(){
        return group_public_id ;
    }

    /**
     * 设置 [GROUP_PUBLIC_ID]
     */
    @JsonProperty("group_public_id")
    public void setGroup_public_id(Integer  group_public_id){
        this.group_public_id = group_public_id ;
        this.group_public_idDirtyFlag = true ;
    }

    /**
     * 获取 [GROUP_PUBLIC_ID]脏标记
     */
    @JsonIgnore
    public boolean getGroup_public_idDirtyFlag(){
        return group_public_idDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return write_uid ;
    }

    /**
     * 设置 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return write_uidDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return create_uid ;
    }

    /**
     * 设置 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return create_uidDirtyFlag ;
    }

    /**
     * 获取 [ALIAS_ID]
     */
    @JsonProperty("alias_id")
    public Integer getAlias_id(){
        return alias_id ;
    }

    /**
     * 设置 [ALIAS_ID]
     */
    @JsonProperty("alias_id")
    public void setAlias_id(Integer  alias_id){
        this.alias_id = alias_id ;
        this.alias_idDirtyFlag = true ;
    }

    /**
     * 获取 [ALIAS_ID]脏标记
     */
    @JsonIgnore
    public boolean getAlias_idDirtyFlag(){
        return alias_idDirtyFlag ;
    }

    /**
     * 获取 [LIVECHAT_CHANNEL_ID]
     */
    @JsonProperty("livechat_channel_id")
    public Integer getLivechat_channel_id(){
        return livechat_channel_id ;
    }

    /**
     * 设置 [LIVECHAT_CHANNEL_ID]
     */
    @JsonProperty("livechat_channel_id")
    public void setLivechat_channel_id(Integer  livechat_channel_id){
        this.livechat_channel_id = livechat_channel_id ;
        this.livechat_channel_idDirtyFlag = true ;
    }

    /**
     * 获取 [LIVECHAT_CHANNEL_ID]脏标记
     */
    @JsonIgnore
    public boolean getLivechat_channel_idDirtyFlag(){
        return livechat_channel_idDirtyFlag ;
    }



    public Mail_channel toDO() {
        Mail_channel srfdomain = new Mail_channel();
        if(getWebsite_message_idsDirtyFlag())
            srfdomain.setWebsite_message_ids(website_message_ids);
        if(getImage_mediumDirtyFlag())
            srfdomain.setImage_medium(image_medium);
        if(getModeration_countDirtyFlag())
            srfdomain.setModeration_count(moderation_count);
        if(getDescriptionDirtyFlag())
            srfdomain.setDescription(description);
        if(getModeration_notifyDirtyFlag())
            srfdomain.setModeration_notify(moderation_notify);
        if(getMessage_main_attachment_idDirtyFlag())
            srfdomain.setMessage_main_attachment_id(message_main_attachment_id);
        if(getModerationDirtyFlag())
            srfdomain.setModeration(moderation);
        if(getIs_moderatorDirtyFlag())
            srfdomain.setIs_moderator(is_moderator);
        if(getModeration_notify_msgDirtyFlag())
            srfdomain.setModeration_notify_msg(moderation_notify_msg);
        if(getModeration_idsDirtyFlag())
            srfdomain.setModeration_ids(moderation_ids);
        if(getRating_last_imageDirtyFlag())
            srfdomain.setRating_last_image(rating_last_image);
        if(getMessage_has_errorDirtyFlag())
            srfdomain.setMessage_has_error(message_has_error);
        if(getModeration_guidelinesDirtyFlag())
            srfdomain.setModeration_guidelines(moderation_guidelines);
        if(getRating_last_feedbackDirtyFlag())
            srfdomain.setRating_last_feedback(rating_last_feedback);
        if(getMessage_idsDirtyFlag())
            srfdomain.setMessage_ids(message_ids);
        if(getSubscription_department_idsDirtyFlag())
            srfdomain.setSubscription_department_ids(subscription_department_ids);
        if(getMessage_unread_counterDirtyFlag())
            srfdomain.setMessage_unread_counter(message_unread_counter);
        if(get__last_updateDirtyFlag())
            srfdomain.set__last_update(__last_update);
        if(getIbizpublicDirtyFlag())
            srfdomain.setIbizpublic(ibizpublic);
        if(getMessage_follower_idsDirtyFlag())
            srfdomain.setMessage_follower_ids(message_follower_ids);
        if(getMessage_channel_idsDirtyFlag())
            srfdomain.setMessage_channel_ids(message_channel_ids);
        if(getRating_idsDirtyFlag())
            srfdomain.setRating_ids(rating_ids);
        if(getAnonymous_nameDirtyFlag())
            srfdomain.setAnonymous_name(anonymous_name);
        if(getNameDirtyFlag())
            srfdomain.setName(name);
        if(getIs_memberDirtyFlag())
            srfdomain.setIs_member(is_member);
        if(getIs_subscribedDirtyFlag())
            srfdomain.setIs_subscribed(is_subscribed);
        if(getModeration_guidelines_msgDirtyFlag())
            srfdomain.setModeration_guidelines_msg(moderation_guidelines_msg);
        if(getMessage_is_followerDirtyFlag())
            srfdomain.setMessage_is_follower(message_is_follower);
        if(getIdDirtyFlag())
            srfdomain.setId(id);
        if(getWrite_dateDirtyFlag())
            srfdomain.setWrite_date(write_date);
        if(getImageDirtyFlag())
            srfdomain.setImage(image);
        if(getMessage_attachment_countDirtyFlag())
            srfdomain.setMessage_attachment_count(message_attachment_count);
        if(getMessage_unreadDirtyFlag())
            srfdomain.setMessage_unread(message_unread);
        if(getCreate_dateDirtyFlag())
            srfdomain.setCreate_date(create_date);
        if(getImage_smallDirtyFlag())
            srfdomain.setImage_small(image_small);
        if(getMessage_partner_idsDirtyFlag())
            srfdomain.setMessage_partner_ids(message_partner_ids);
        if(getMessage_has_error_counterDirtyFlag())
            srfdomain.setMessage_has_error_counter(message_has_error_counter);
        if(getChannel_partner_idsDirtyFlag())
            srfdomain.setChannel_partner_ids(channel_partner_ids);
        if(getModerator_idsDirtyFlag())
            srfdomain.setModerator_ids(moderator_ids);
        if(getGroup_idsDirtyFlag())
            srfdomain.setGroup_ids(group_ids);
        if(getEmail_sendDirtyFlag())
            srfdomain.setEmail_send(email_send);
        if(getChannel_last_seen_partner_idsDirtyFlag())
            srfdomain.setChannel_last_seen_partner_ids(channel_last_seen_partner_ids);
        if(getMessage_needactionDirtyFlag())
            srfdomain.setMessage_needaction(message_needaction);
        if(getUuidDirtyFlag())
            srfdomain.setUuid(uuid);
        if(getRating_countDirtyFlag())
            srfdomain.setRating_count(rating_count);
        if(getDisplay_nameDirtyFlag())
            srfdomain.setDisplay_name(display_name);
        if(getMessage_needaction_counterDirtyFlag())
            srfdomain.setMessage_needaction_counter(message_needaction_counter);
        if(getChannel_message_idsDirtyFlag())
            srfdomain.setChannel_message_ids(channel_message_ids);
        if(getChannel_typeDirtyFlag())
            srfdomain.setChannel_type(channel_type);
        if(getIs_chatDirtyFlag())
            srfdomain.setIs_chat(is_chat);
        if(getRating_last_valueDirtyFlag())
            srfdomain.setRating_last_value(rating_last_value);
        if(getAlias_force_thread_idDirtyFlag())
            srfdomain.setAlias_force_thread_id(alias_force_thread_id);
        if(getAlias_defaultsDirtyFlag())
            srfdomain.setAlias_defaults(alias_defaults);
        if(getAlias_user_idDirtyFlag())
            srfdomain.setAlias_user_id(alias_user_id);
        if(getAlias_parent_model_idDirtyFlag())
            srfdomain.setAlias_parent_model_id(alias_parent_model_id);
        if(getGroup_public_id_textDirtyFlag())
            srfdomain.setGroup_public_id_text(group_public_id_text);
        if(getWrite_uid_textDirtyFlag())
            srfdomain.setWrite_uid_text(write_uid_text);
        if(getAlias_parent_thread_idDirtyFlag())
            srfdomain.setAlias_parent_thread_id(alias_parent_thread_id);
        if(getAlias_nameDirtyFlag())
            srfdomain.setAlias_name(alias_name);
        if(getAlias_model_idDirtyFlag())
            srfdomain.setAlias_model_id(alias_model_id);
        if(getLivechat_channel_id_textDirtyFlag())
            srfdomain.setLivechat_channel_id_text(livechat_channel_id_text);
        if(getAlias_domainDirtyFlag())
            srfdomain.setAlias_domain(alias_domain);
        if(getCreate_uid_textDirtyFlag())
            srfdomain.setCreate_uid_text(create_uid_text);
        if(getAlias_contactDirtyFlag())
            srfdomain.setAlias_contact(alias_contact);
        if(getGroup_public_idDirtyFlag())
            srfdomain.setGroup_public_id(group_public_id);
        if(getWrite_uidDirtyFlag())
            srfdomain.setWrite_uid(write_uid);
        if(getCreate_uidDirtyFlag())
            srfdomain.setCreate_uid(create_uid);
        if(getAlias_idDirtyFlag())
            srfdomain.setAlias_id(alias_id);
        if(getLivechat_channel_idDirtyFlag())
            srfdomain.setLivechat_channel_id(livechat_channel_id);

        return srfdomain;
    }

    public void fromDO(Mail_channel srfdomain) {
        if(srfdomain == null )
          return ;
        if(srfdomain.getWebsite_message_idsDirtyFlag())
            this.setWebsite_message_ids(srfdomain.getWebsite_message_ids());
        if(srfdomain.getImage_mediumDirtyFlag())
            this.setImage_medium(srfdomain.getImage_medium());
        if(srfdomain.getModeration_countDirtyFlag())
            this.setModeration_count(srfdomain.getModeration_count());
        if(srfdomain.getDescriptionDirtyFlag())
            this.setDescription(srfdomain.getDescription());
        if(srfdomain.getModeration_notifyDirtyFlag())
            this.setModeration_notify(srfdomain.getModeration_notify());
        if(srfdomain.getMessage_main_attachment_idDirtyFlag())
            this.setMessage_main_attachment_id(srfdomain.getMessage_main_attachment_id());
        if(srfdomain.getModerationDirtyFlag())
            this.setModeration(srfdomain.getModeration());
        if(srfdomain.getIs_moderatorDirtyFlag())
            this.setIs_moderator(srfdomain.getIs_moderator());
        if(srfdomain.getModeration_notify_msgDirtyFlag())
            this.setModeration_notify_msg(srfdomain.getModeration_notify_msg());
        if(srfdomain.getModeration_idsDirtyFlag())
            this.setModeration_ids(srfdomain.getModeration_ids());
        if(srfdomain.getRating_last_imageDirtyFlag())
            this.setRating_last_image(srfdomain.getRating_last_image());
        if(srfdomain.getMessage_has_errorDirtyFlag())
            this.setMessage_has_error(srfdomain.getMessage_has_error());
        if(srfdomain.getModeration_guidelinesDirtyFlag())
            this.setModeration_guidelines(srfdomain.getModeration_guidelines());
        if(srfdomain.getRating_last_feedbackDirtyFlag())
            this.setRating_last_feedback(srfdomain.getRating_last_feedback());
        if(srfdomain.getMessage_idsDirtyFlag())
            this.setMessage_ids(srfdomain.getMessage_ids());
        if(srfdomain.getSubscription_department_idsDirtyFlag())
            this.setSubscription_department_ids(srfdomain.getSubscription_department_ids());
        if(srfdomain.getMessage_unread_counterDirtyFlag())
            this.setMessage_unread_counter(srfdomain.getMessage_unread_counter());
        if(srfdomain.get__last_updateDirtyFlag())
            this.set__last_update(srfdomain.get__last_update());
        if(srfdomain.getIbizpublicDirtyFlag())
            this.setIbizpublic(srfdomain.getIbizpublic());
        if(srfdomain.getMessage_follower_idsDirtyFlag())
            this.setMessage_follower_ids(srfdomain.getMessage_follower_ids());
        if(srfdomain.getMessage_channel_idsDirtyFlag())
            this.setMessage_channel_ids(srfdomain.getMessage_channel_ids());
        if(srfdomain.getRating_idsDirtyFlag())
            this.setRating_ids(srfdomain.getRating_ids());
        if(srfdomain.getAnonymous_nameDirtyFlag())
            this.setAnonymous_name(srfdomain.getAnonymous_name());
        if(srfdomain.getNameDirtyFlag())
            this.setName(srfdomain.getName());
        if(srfdomain.getIs_memberDirtyFlag())
            this.setIs_member(srfdomain.getIs_member());
        if(srfdomain.getIs_subscribedDirtyFlag())
            this.setIs_subscribed(srfdomain.getIs_subscribed());
        if(srfdomain.getModeration_guidelines_msgDirtyFlag())
            this.setModeration_guidelines_msg(srfdomain.getModeration_guidelines_msg());
        if(srfdomain.getMessage_is_followerDirtyFlag())
            this.setMessage_is_follower(srfdomain.getMessage_is_follower());
        if(srfdomain.getIdDirtyFlag())
            this.setId(srfdomain.getId());
        if(srfdomain.getWrite_dateDirtyFlag())
            this.setWrite_date(srfdomain.getWrite_date());
        if(srfdomain.getImageDirtyFlag())
            this.setImage(srfdomain.getImage());
        if(srfdomain.getMessage_attachment_countDirtyFlag())
            this.setMessage_attachment_count(srfdomain.getMessage_attachment_count());
        if(srfdomain.getMessage_unreadDirtyFlag())
            this.setMessage_unread(srfdomain.getMessage_unread());
        if(srfdomain.getCreate_dateDirtyFlag())
            this.setCreate_date(srfdomain.getCreate_date());
        if(srfdomain.getImage_smallDirtyFlag())
            this.setImage_small(srfdomain.getImage_small());
        if(srfdomain.getMessage_partner_idsDirtyFlag())
            this.setMessage_partner_ids(srfdomain.getMessage_partner_ids());
        if(srfdomain.getMessage_has_error_counterDirtyFlag())
            this.setMessage_has_error_counter(srfdomain.getMessage_has_error_counter());
        if(srfdomain.getChannel_partner_idsDirtyFlag())
            this.setChannel_partner_ids(srfdomain.getChannel_partner_ids());
        if(srfdomain.getModerator_idsDirtyFlag())
            this.setModerator_ids(srfdomain.getModerator_ids());
        if(srfdomain.getGroup_idsDirtyFlag())
            this.setGroup_ids(srfdomain.getGroup_ids());
        if(srfdomain.getEmail_sendDirtyFlag())
            this.setEmail_send(srfdomain.getEmail_send());
        if(srfdomain.getChannel_last_seen_partner_idsDirtyFlag())
            this.setChannel_last_seen_partner_ids(srfdomain.getChannel_last_seen_partner_ids());
        if(srfdomain.getMessage_needactionDirtyFlag())
            this.setMessage_needaction(srfdomain.getMessage_needaction());
        if(srfdomain.getUuidDirtyFlag())
            this.setUuid(srfdomain.getUuid());
        if(srfdomain.getRating_countDirtyFlag())
            this.setRating_count(srfdomain.getRating_count());
        if(srfdomain.getDisplay_nameDirtyFlag())
            this.setDisplay_name(srfdomain.getDisplay_name());
        if(srfdomain.getMessage_needaction_counterDirtyFlag())
            this.setMessage_needaction_counter(srfdomain.getMessage_needaction_counter());
        if(srfdomain.getChannel_message_idsDirtyFlag())
            this.setChannel_message_ids(srfdomain.getChannel_message_ids());
        if(srfdomain.getChannel_typeDirtyFlag())
            this.setChannel_type(srfdomain.getChannel_type());
        if(srfdomain.getIs_chatDirtyFlag())
            this.setIs_chat(srfdomain.getIs_chat());
        if(srfdomain.getRating_last_valueDirtyFlag())
            this.setRating_last_value(srfdomain.getRating_last_value());
        if(srfdomain.getAlias_force_thread_idDirtyFlag())
            this.setAlias_force_thread_id(srfdomain.getAlias_force_thread_id());
        if(srfdomain.getAlias_defaultsDirtyFlag())
            this.setAlias_defaults(srfdomain.getAlias_defaults());
        if(srfdomain.getAlias_user_idDirtyFlag())
            this.setAlias_user_id(srfdomain.getAlias_user_id());
        if(srfdomain.getAlias_parent_model_idDirtyFlag())
            this.setAlias_parent_model_id(srfdomain.getAlias_parent_model_id());
        if(srfdomain.getGroup_public_id_textDirtyFlag())
            this.setGroup_public_id_text(srfdomain.getGroup_public_id_text());
        if(srfdomain.getWrite_uid_textDirtyFlag())
            this.setWrite_uid_text(srfdomain.getWrite_uid_text());
        if(srfdomain.getAlias_parent_thread_idDirtyFlag())
            this.setAlias_parent_thread_id(srfdomain.getAlias_parent_thread_id());
        if(srfdomain.getAlias_nameDirtyFlag())
            this.setAlias_name(srfdomain.getAlias_name());
        if(srfdomain.getAlias_model_idDirtyFlag())
            this.setAlias_model_id(srfdomain.getAlias_model_id());
        if(srfdomain.getLivechat_channel_id_textDirtyFlag())
            this.setLivechat_channel_id_text(srfdomain.getLivechat_channel_id_text());
        if(srfdomain.getAlias_domainDirtyFlag())
            this.setAlias_domain(srfdomain.getAlias_domain());
        if(srfdomain.getCreate_uid_textDirtyFlag())
            this.setCreate_uid_text(srfdomain.getCreate_uid_text());
        if(srfdomain.getAlias_contactDirtyFlag())
            this.setAlias_contact(srfdomain.getAlias_contact());
        if(srfdomain.getGroup_public_idDirtyFlag())
            this.setGroup_public_id(srfdomain.getGroup_public_id());
        if(srfdomain.getWrite_uidDirtyFlag())
            this.setWrite_uid(srfdomain.getWrite_uid());
        if(srfdomain.getCreate_uidDirtyFlag())
            this.setCreate_uid(srfdomain.getCreate_uid());
        if(srfdomain.getAlias_idDirtyFlag())
            this.setAlias_id(srfdomain.getAlias_id());
        if(srfdomain.getLivechat_channel_idDirtyFlag())
            this.setLivechat_channel_id(srfdomain.getLivechat_channel_id());

    }

    public List<Mail_channelDTO> fromDOPage(List<Mail_channel> poPage)   {
        if(poPage == null)
            return null;
        List<Mail_channelDTO> dtos=new ArrayList<Mail_channelDTO>();
        for(Mail_channel domain : poPage) {
            Mail_channelDTO dto = new Mail_channelDTO();
            dto.fromDO(domain);
            dtos.add(dto);
        }
        return dtos;
    }
}

