package cn.ibizlab.odoo.service.odoo_mail.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_mail.dto.Mail_statistics_reportDTO;
import cn.ibizlab.odoo.core.odoo_mail.domain.Mail_statistics_report;
import cn.ibizlab.odoo.core.odoo_mail.service.IMail_statistics_reportService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_mail.filter.Mail_statistics_reportSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Mail_statistics_report" })
@RestController
@RequestMapping("")
public class Mail_statistics_reportResource {

    @Autowired
    private IMail_statistics_reportService mail_statistics_reportService;

    public IMail_statistics_reportService getMail_statistics_reportService() {
        return this.mail_statistics_reportService;
    }

    @ApiOperation(value = "更新数据", tags = {"Mail_statistics_report" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_mail/mail_statistics_reports/{mail_statistics_report_id}")

    public ResponseEntity<Mail_statistics_reportDTO> update(@PathVariable("mail_statistics_report_id") Integer mail_statistics_report_id, @RequestBody Mail_statistics_reportDTO mail_statistics_reportdto) {
		Mail_statistics_report domain = mail_statistics_reportdto.toDO();
        domain.setId(mail_statistics_report_id);
		mail_statistics_reportService.update(domain);
		Mail_statistics_reportDTO dto = new Mail_statistics_reportDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批删除数据", tags = {"Mail_statistics_report" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_mail/mail_statistics_reports/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Mail_statistics_reportDTO> mail_statistics_reportdtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "删除数据", tags = {"Mail_statistics_report" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_mail/mail_statistics_reports/{mail_statistics_report_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("mail_statistics_report_id") Integer mail_statistics_report_id) {
        Mail_statistics_reportDTO mail_statistics_reportdto = new Mail_statistics_reportDTO();
		Mail_statistics_report domain = new Mail_statistics_report();
		mail_statistics_reportdto.setId(mail_statistics_report_id);
		domain.setId(mail_statistics_report_id);
        Boolean rst = mail_statistics_reportService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "批建立数据", tags = {"Mail_statistics_report" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_mail/mail_statistics_reports/createBatch")
    public ResponseEntity<Boolean> createBatchMail_statistics_report(@RequestBody List<Mail_statistics_reportDTO> mail_statistics_reportdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "获取数据", tags = {"Mail_statistics_report" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_mail/mail_statistics_reports/{mail_statistics_report_id}")
    public ResponseEntity<Mail_statistics_reportDTO> get(@PathVariable("mail_statistics_report_id") Integer mail_statistics_report_id) {
        Mail_statistics_reportDTO dto = new Mail_statistics_reportDTO();
        Mail_statistics_report domain = mail_statistics_reportService.get(mail_statistics_report_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "建立数据", tags = {"Mail_statistics_report" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_mail/mail_statistics_reports")

    public ResponseEntity<Mail_statistics_reportDTO> create(@RequestBody Mail_statistics_reportDTO mail_statistics_reportdto) {
        Mail_statistics_reportDTO dto = new Mail_statistics_reportDTO();
        Mail_statistics_report domain = mail_statistics_reportdto.toDO();
		mail_statistics_reportService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批更新数据", tags = {"Mail_statistics_report" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_mail/mail_statistics_reports/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Mail_statistics_reportDTO> mail_statistics_reportdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Mail_statistics_report" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_mail/mail_statistics_reports/fetchdefault")
	public ResponseEntity<Page<Mail_statistics_reportDTO>> fetchDefault(Mail_statistics_reportSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Mail_statistics_reportDTO> list = new ArrayList<Mail_statistics_reportDTO>();
        
        Page<Mail_statistics_report> domains = mail_statistics_reportService.searchDefault(context) ;
        for(Mail_statistics_report mail_statistics_report : domains.getContent()){
            Mail_statistics_reportDTO dto = new Mail_statistics_reportDTO();
            dto.fromDO(mail_statistics_report);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
