package cn.ibizlab.odoo.service.odoo_mail.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_mail.dto.Mail_mass_mailing_contactDTO;
import cn.ibizlab.odoo.core.odoo_mail.domain.Mail_mass_mailing_contact;
import cn.ibizlab.odoo.core.odoo_mail.service.IMail_mass_mailing_contactService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_mail.filter.Mail_mass_mailing_contactSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Mail_mass_mailing_contact" })
@RestController
@RequestMapping("")
public class Mail_mass_mailing_contactResource {

    @Autowired
    private IMail_mass_mailing_contactService mail_mass_mailing_contactService;

    public IMail_mass_mailing_contactService getMail_mass_mailing_contactService() {
        return this.mail_mass_mailing_contactService;
    }

    @ApiOperation(value = "删除数据", tags = {"Mail_mass_mailing_contact" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_mail/mail_mass_mailing_contacts/{mail_mass_mailing_contact_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("mail_mass_mailing_contact_id") Integer mail_mass_mailing_contact_id) {
        Mail_mass_mailing_contactDTO mail_mass_mailing_contactdto = new Mail_mass_mailing_contactDTO();
		Mail_mass_mailing_contact domain = new Mail_mass_mailing_contact();
		mail_mass_mailing_contactdto.setId(mail_mass_mailing_contact_id);
		domain.setId(mail_mass_mailing_contact_id);
        Boolean rst = mail_mass_mailing_contactService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "获取数据", tags = {"Mail_mass_mailing_contact" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_mail/mail_mass_mailing_contacts/{mail_mass_mailing_contact_id}")
    public ResponseEntity<Mail_mass_mailing_contactDTO> get(@PathVariable("mail_mass_mailing_contact_id") Integer mail_mass_mailing_contact_id) {
        Mail_mass_mailing_contactDTO dto = new Mail_mass_mailing_contactDTO();
        Mail_mass_mailing_contact domain = mail_mass_mailing_contactService.get(mail_mass_mailing_contact_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "更新数据", tags = {"Mail_mass_mailing_contact" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_mail/mail_mass_mailing_contacts/{mail_mass_mailing_contact_id}")

    public ResponseEntity<Mail_mass_mailing_contactDTO> update(@PathVariable("mail_mass_mailing_contact_id") Integer mail_mass_mailing_contact_id, @RequestBody Mail_mass_mailing_contactDTO mail_mass_mailing_contactdto) {
		Mail_mass_mailing_contact domain = mail_mass_mailing_contactdto.toDO();
        domain.setId(mail_mass_mailing_contact_id);
		mail_mass_mailing_contactService.update(domain);
		Mail_mass_mailing_contactDTO dto = new Mail_mass_mailing_contactDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批删除数据", tags = {"Mail_mass_mailing_contact" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_mail/mail_mass_mailing_contacts/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Mail_mass_mailing_contactDTO> mail_mass_mailing_contactdtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "批建立数据", tags = {"Mail_mass_mailing_contact" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_mail/mail_mass_mailing_contacts/createBatch")
    public ResponseEntity<Boolean> createBatchMail_mass_mailing_contact(@RequestBody List<Mail_mass_mailing_contactDTO> mail_mass_mailing_contactdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "批更新数据", tags = {"Mail_mass_mailing_contact" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_mail/mail_mass_mailing_contacts/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Mail_mass_mailing_contactDTO> mail_mass_mailing_contactdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "建立数据", tags = {"Mail_mass_mailing_contact" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_mail/mail_mass_mailing_contacts")

    public ResponseEntity<Mail_mass_mailing_contactDTO> create(@RequestBody Mail_mass_mailing_contactDTO mail_mass_mailing_contactdto) {
        Mail_mass_mailing_contactDTO dto = new Mail_mass_mailing_contactDTO();
        Mail_mass_mailing_contact domain = mail_mass_mailing_contactdto.toDO();
		mail_mass_mailing_contactService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Mail_mass_mailing_contact" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_mail/mail_mass_mailing_contacts/fetchdefault")
	public ResponseEntity<Page<Mail_mass_mailing_contactDTO>> fetchDefault(Mail_mass_mailing_contactSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Mail_mass_mailing_contactDTO> list = new ArrayList<Mail_mass_mailing_contactDTO>();
        
        Page<Mail_mass_mailing_contact> domains = mail_mass_mailing_contactService.searchDefault(context) ;
        for(Mail_mass_mailing_contact mail_mass_mailing_contact : domains.getContent()){
            Mail_mass_mailing_contactDTO dto = new Mail_mass_mailing_contactDTO();
            dto.fromDO(mail_mass_mailing_contact);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
