package cn.ibizlab.odoo.service.odoo_maintenance.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_maintenance.dto.Maintenance_teamDTO;
import cn.ibizlab.odoo.core.odoo_maintenance.domain.Maintenance_team;
import cn.ibizlab.odoo.core.odoo_maintenance.service.IMaintenance_teamService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_maintenance.filter.Maintenance_teamSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Maintenance_team" })
@RestController
@RequestMapping("")
public class Maintenance_teamResource {

    @Autowired
    private IMaintenance_teamService maintenance_teamService;

    public IMaintenance_teamService getMaintenance_teamService() {
        return this.maintenance_teamService;
    }

    @ApiOperation(value = "批删除数据", tags = {"Maintenance_team" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_maintenance/maintenance_teams/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Maintenance_teamDTO> maintenance_teamdtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "批更新数据", tags = {"Maintenance_team" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_maintenance/maintenance_teams/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Maintenance_teamDTO> maintenance_teamdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "删除数据", tags = {"Maintenance_team" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_maintenance/maintenance_teams/{maintenance_team_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("maintenance_team_id") Integer maintenance_team_id) {
        Maintenance_teamDTO maintenance_teamdto = new Maintenance_teamDTO();
		Maintenance_team domain = new Maintenance_team();
		maintenance_teamdto.setId(maintenance_team_id);
		domain.setId(maintenance_team_id);
        Boolean rst = maintenance_teamService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "更新数据", tags = {"Maintenance_team" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_maintenance/maintenance_teams/{maintenance_team_id}")

    public ResponseEntity<Maintenance_teamDTO> update(@PathVariable("maintenance_team_id") Integer maintenance_team_id, @RequestBody Maintenance_teamDTO maintenance_teamdto) {
		Maintenance_team domain = maintenance_teamdto.toDO();
        domain.setId(maintenance_team_id);
		maintenance_teamService.update(domain);
		Maintenance_teamDTO dto = new Maintenance_teamDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批建立数据", tags = {"Maintenance_team" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_maintenance/maintenance_teams/createBatch")
    public ResponseEntity<Boolean> createBatchMaintenance_team(@RequestBody List<Maintenance_teamDTO> maintenance_teamdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "获取数据", tags = {"Maintenance_team" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_maintenance/maintenance_teams/{maintenance_team_id}")
    public ResponseEntity<Maintenance_teamDTO> get(@PathVariable("maintenance_team_id") Integer maintenance_team_id) {
        Maintenance_teamDTO dto = new Maintenance_teamDTO();
        Maintenance_team domain = maintenance_teamService.get(maintenance_team_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "建立数据", tags = {"Maintenance_team" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_maintenance/maintenance_teams")

    public ResponseEntity<Maintenance_teamDTO> create(@RequestBody Maintenance_teamDTO maintenance_teamdto) {
        Maintenance_teamDTO dto = new Maintenance_teamDTO();
        Maintenance_team domain = maintenance_teamdto.toDO();
		maintenance_teamService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Maintenance_team" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_maintenance/maintenance_teams/fetchdefault")
	public ResponseEntity<Page<Maintenance_teamDTO>> fetchDefault(Maintenance_teamSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Maintenance_teamDTO> list = new ArrayList<Maintenance_teamDTO>();
        
        Page<Maintenance_team> domains = maintenance_teamService.searchDefault(context) ;
        for(Maintenance_team maintenance_team : domains.getContent()){
            Maintenance_teamDTO dto = new Maintenance_teamDTO();
            dto.fromDO(maintenance_team);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
