package cn.ibizlab.odoo.service.odoo_maintenance.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.core.odoo_maintenance.valuerule.anno.maintenance_equipment.*;
import cn.ibizlab.odoo.core.odoo_maintenance.domain.Maintenance_equipment;
import org.springframework.cglib.beans.BeanCopier;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 服务DTO对象[Maintenance_equipmentDTO]
 */
public class Maintenance_equipmentDTO implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [ACTIVITY_SUMMARY]
     *
     */
    @Maintenance_equipmentActivity_summaryDefault(info = "默认规则")
    private String activity_summary;

    @JsonIgnore
    private boolean activity_summaryDirtyFlag;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @Maintenance_equipment__last_updateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp __last_update;

    @JsonIgnore
    private boolean __last_updateDirtyFlag;

    /**
     * 属性 [ACTIVITY_IDS]
     *
     */
    @Maintenance_equipmentActivity_idsDefault(info = "默认规则")
    private String activity_ids;

    @JsonIgnore
    private boolean activity_idsDirtyFlag;

    /**
     * 属性 [MESSAGE_NEEDACTION_COUNTER]
     *
     */
    @Maintenance_equipmentMessage_needaction_counterDefault(info = "默认规则")
    private Integer message_needaction_counter;

    @JsonIgnore
    private boolean message_needaction_counterDirtyFlag;

    /**
     * 属性 [MESSAGE_UNREAD]
     *
     */
    @Maintenance_equipmentMessage_unreadDefault(info = "默认规则")
    private String message_unread;

    @JsonIgnore
    private boolean message_unreadDirtyFlag;

    /**
     * 属性 [ID]
     *
     */
    @Maintenance_equipmentIdDefault(info = "默认规则")
    private Integer id;

    @JsonIgnore
    private boolean idDirtyFlag;

    /**
     * 属性 [EQUIPMENT_ASSIGN_TO]
     *
     */
    @Maintenance_equipmentEquipment_assign_toDefault(info = "默认规则")
    private String equipment_assign_to;

    @JsonIgnore
    private boolean equipment_assign_toDirtyFlag;

    /**
     * 属性 [LOCATION]
     *
     */
    @Maintenance_equipmentLocationDefault(info = "默认规则")
    private String location;

    @JsonIgnore
    private boolean locationDirtyFlag;

    /**
     * 属性 [WARRANTY_DATE]
     *
     */
    @Maintenance_equipmentWarranty_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    private Timestamp warranty_date;

    @JsonIgnore
    private boolean warranty_dateDirtyFlag;

    /**
     * 属性 [WEBSITE_MESSAGE_IDS]
     *
     */
    @Maintenance_equipmentWebsite_message_idsDefault(info = "默认规则")
    private String website_message_ids;

    @JsonIgnore
    private boolean website_message_idsDirtyFlag;

    /**
     * 属性 [NAME]
     *
     */
    @Maintenance_equipmentNameDefault(info = "默认规则")
    private String name;

    @JsonIgnore
    private boolean nameDirtyFlag;

    /**
     * 属性 [MAINTENANCE_DURATION]
     *
     */
    @Maintenance_equipmentMaintenance_durationDefault(info = "默认规则")
    private Double maintenance_duration;

    @JsonIgnore
    private boolean maintenance_durationDirtyFlag;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @Maintenance_equipmentDisplay_nameDefault(info = "默认规则")
    private String display_name;

    @JsonIgnore
    private boolean display_nameDirtyFlag;

    /**
     * 属性 [MODEL]
     *
     */
    @Maintenance_equipmentModelDefault(info = "默认规则")
    private String model;

    @JsonIgnore
    private boolean modelDirtyFlag;

    /**
     * 属性 [MAINTENANCE_OPEN_COUNT]
     *
     */
    @Maintenance_equipmentMaintenance_open_countDefault(info = "默认规则")
    private Integer maintenance_open_count;

    @JsonIgnore
    private boolean maintenance_open_countDirtyFlag;

    /**
     * 属性 [MESSAGE_IS_FOLLOWER]
     *
     */
    @Maintenance_equipmentMessage_is_followerDefault(info = "默认规则")
    private String message_is_follower;

    @JsonIgnore
    private boolean message_is_followerDirtyFlag;

    /**
     * 属性 [ACTIVITY_STATE]
     *
     */
    @Maintenance_equipmentActivity_stateDefault(info = "默认规则")
    private String activity_state;

    @JsonIgnore
    private boolean activity_stateDirtyFlag;

    /**
     * 属性 [ACTIVITY_DATE_DEADLINE]
     *
     */
    @Maintenance_equipmentActivity_date_deadlineDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    private Timestamp activity_date_deadline;

    @JsonIgnore
    private boolean activity_date_deadlineDirtyFlag;

    /**
     * 属性 [MAINTENANCE_COUNT]
     *
     */
    @Maintenance_equipmentMaintenance_countDefault(info = "默认规则")
    private Integer maintenance_count;

    @JsonIgnore
    private boolean maintenance_countDirtyFlag;

    /**
     * 属性 [WRITE_DATE]
     *
     */
    @Maintenance_equipmentWrite_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp write_date;

    @JsonIgnore
    private boolean write_dateDirtyFlag;

    /**
     * 属性 [MESSAGE_UNREAD_COUNTER]
     *
     */
    @Maintenance_equipmentMessage_unread_counterDefault(info = "默认规则")
    private Integer message_unread_counter;

    @JsonIgnore
    private boolean message_unread_counterDirtyFlag;

    /**
     * 属性 [NOTE]
     *
     */
    @Maintenance_equipmentNoteDefault(info = "默认规则")
    private String note;

    @JsonIgnore
    private boolean noteDirtyFlag;

    /**
     * 属性 [PARTNER_REF]
     *
     */
    @Maintenance_equipmentPartner_refDefault(info = "默认规则")
    private String partner_ref;

    @JsonIgnore
    private boolean partner_refDirtyFlag;

    /**
     * 属性 [MAINTENANCE_IDS]
     *
     */
    @Maintenance_equipmentMaintenance_idsDefault(info = "默认规则")
    private String maintenance_ids;

    @JsonIgnore
    private boolean maintenance_idsDirtyFlag;

    /**
     * 属性 [MESSAGE_HAS_ERROR_COUNTER]
     *
     */
    @Maintenance_equipmentMessage_has_error_counterDefault(info = "默认规则")
    private Integer message_has_error_counter;

    @JsonIgnore
    private boolean message_has_error_counterDirtyFlag;

    /**
     * 属性 [ACTIVITY_USER_ID]
     *
     */
    @Maintenance_equipmentActivity_user_idDefault(info = "默认规则")
    private Integer activity_user_id;

    @JsonIgnore
    private boolean activity_user_idDirtyFlag;

    /**
     * 属性 [MESSAGE_MAIN_ATTACHMENT_ID]
     *
     */
    @Maintenance_equipmentMessage_main_attachment_idDefault(info = "默认规则")
    private Integer message_main_attachment_id;

    @JsonIgnore
    private boolean message_main_attachment_idDirtyFlag;

    /**
     * 属性 [SERIAL_NO]
     *
     */
    @Maintenance_equipmentSerial_noDefault(info = "默认规则")
    private String serial_no;

    @JsonIgnore
    private boolean serial_noDirtyFlag;

    /**
     * 属性 [MESSAGE_ATTACHMENT_COUNT]
     *
     */
    @Maintenance_equipmentMessage_attachment_countDefault(info = "默认规则")
    private Integer message_attachment_count;

    @JsonIgnore
    private boolean message_attachment_countDirtyFlag;

    /**
     * 属性 [COST]
     *
     */
    @Maintenance_equipmentCostDefault(info = "默认规则")
    private Double cost;

    @JsonIgnore
    private boolean costDirtyFlag;

    /**
     * 属性 [NEXT_ACTION_DATE]
     *
     */
    @Maintenance_equipmentNext_action_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    private Timestamp next_action_date;

    @JsonIgnore
    private boolean next_action_dateDirtyFlag;

    /**
     * 属性 [ASSIGN_DATE]
     *
     */
    @Maintenance_equipmentAssign_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    private Timestamp assign_date;

    @JsonIgnore
    private boolean assign_dateDirtyFlag;

    /**
     * 属性 [EFFECTIVE_DATE]
     *
     */
    @Maintenance_equipmentEffective_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    private Timestamp effective_date;

    @JsonIgnore
    private boolean effective_dateDirtyFlag;

    /**
     * 属性 [MESSAGE_NEEDACTION]
     *
     */
    @Maintenance_equipmentMessage_needactionDefault(info = "默认规则")
    private String message_needaction;

    @JsonIgnore
    private boolean message_needactionDirtyFlag;

    /**
     * 属性 [MESSAGE_CHANNEL_IDS]
     *
     */
    @Maintenance_equipmentMessage_channel_idsDefault(info = "默认规则")
    private String message_channel_ids;

    @JsonIgnore
    private boolean message_channel_idsDirtyFlag;

    /**
     * 属性 [ACTIVE]
     *
     */
    @Maintenance_equipmentActiveDefault(info = "默认规则")
    private String active;

    @JsonIgnore
    private boolean activeDirtyFlag;

    /**
     * 属性 [MESSAGE_IDS]
     *
     */
    @Maintenance_equipmentMessage_idsDefault(info = "默认规则")
    private String message_ids;

    @JsonIgnore
    private boolean message_idsDirtyFlag;

    /**
     * 属性 [SCRAP_DATE]
     *
     */
    @Maintenance_equipmentScrap_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    private Timestamp scrap_date;

    @JsonIgnore
    private boolean scrap_dateDirtyFlag;

    /**
     * 属性 [CREATE_DATE]
     *
     */
    @Maintenance_equipmentCreate_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp create_date;

    @JsonIgnore
    private boolean create_dateDirtyFlag;

    /**
     * 属性 [MESSAGE_FOLLOWER_IDS]
     *
     */
    @Maintenance_equipmentMessage_follower_idsDefault(info = "默认规则")
    private String message_follower_ids;

    @JsonIgnore
    private boolean message_follower_idsDirtyFlag;

    /**
     * 属性 [PERIOD]
     *
     */
    @Maintenance_equipmentPeriodDefault(info = "默认规则")
    private Integer period;

    @JsonIgnore
    private boolean periodDirtyFlag;

    /**
     * 属性 [MESSAGE_HAS_ERROR]
     *
     */
    @Maintenance_equipmentMessage_has_errorDefault(info = "默认规则")
    private String message_has_error;

    @JsonIgnore
    private boolean message_has_errorDirtyFlag;

    /**
     * 属性 [ACTIVITY_TYPE_ID]
     *
     */
    @Maintenance_equipmentActivity_type_idDefault(info = "默认规则")
    private Integer activity_type_id;

    @JsonIgnore
    private boolean activity_type_idDirtyFlag;

    /**
     * 属性 [COLOR]
     *
     */
    @Maintenance_equipmentColorDefault(info = "默认规则")
    private Integer color;

    @JsonIgnore
    private boolean colorDirtyFlag;

    /**
     * 属性 [MESSAGE_PARTNER_IDS]
     *
     */
    @Maintenance_equipmentMessage_partner_idsDefault(info = "默认规则")
    private String message_partner_ids;

    @JsonIgnore
    private boolean message_partner_idsDirtyFlag;

    /**
     * 属性 [DEPARTMENT_ID_TEXT]
     *
     */
    @Maintenance_equipmentDepartment_id_textDefault(info = "默认规则")
    private String department_id_text;

    @JsonIgnore
    private boolean department_id_textDirtyFlag;

    /**
     * 属性 [EMPLOYEE_ID_TEXT]
     *
     */
    @Maintenance_equipmentEmployee_id_textDefault(info = "默认规则")
    private String employee_id_text;

    @JsonIgnore
    private boolean employee_id_textDirtyFlag;

    /**
     * 属性 [TECHNICIAN_USER_ID_TEXT]
     *
     */
    @Maintenance_equipmentTechnician_user_id_textDefault(info = "默认规则")
    private String technician_user_id_text;

    @JsonIgnore
    private boolean technician_user_id_textDirtyFlag;

    /**
     * 属性 [COMPANY_ID_TEXT]
     *
     */
    @Maintenance_equipmentCompany_id_textDefault(info = "默认规则")
    private String company_id_text;

    @JsonIgnore
    private boolean company_id_textDirtyFlag;

    /**
     * 属性 [CATEGORY_ID_TEXT]
     *
     */
    @Maintenance_equipmentCategory_id_textDefault(info = "默认规则")
    private String category_id_text;

    @JsonIgnore
    private boolean category_id_textDirtyFlag;

    /**
     * 属性 [CREATE_UID_TEXT]
     *
     */
    @Maintenance_equipmentCreate_uid_textDefault(info = "默认规则")
    private String create_uid_text;

    @JsonIgnore
    private boolean create_uid_textDirtyFlag;

    /**
     * 属性 [PARTNER_ID_TEXT]
     *
     */
    @Maintenance_equipmentPartner_id_textDefault(info = "默认规则")
    private String partner_id_text;

    @JsonIgnore
    private boolean partner_id_textDirtyFlag;

    /**
     * 属性 [WRITE_UID_TEXT]
     *
     */
    @Maintenance_equipmentWrite_uid_textDefault(info = "默认规则")
    private String write_uid_text;

    @JsonIgnore
    private boolean write_uid_textDirtyFlag;

    /**
     * 属性 [OWNER_USER_ID_TEXT]
     *
     */
    @Maintenance_equipmentOwner_user_id_textDefault(info = "默认规则")
    private String owner_user_id_text;

    @JsonIgnore
    private boolean owner_user_id_textDirtyFlag;

    /**
     * 属性 [MAINTENANCE_TEAM_ID_TEXT]
     *
     */
    @Maintenance_equipmentMaintenance_team_id_textDefault(info = "默认规则")
    private String maintenance_team_id_text;

    @JsonIgnore
    private boolean maintenance_team_id_textDirtyFlag;

    /**
     * 属性 [CREATE_UID]
     *
     */
    @Maintenance_equipmentCreate_uidDefault(info = "默认规则")
    private Integer create_uid;

    @JsonIgnore
    private boolean create_uidDirtyFlag;

    /**
     * 属性 [TECHNICIAN_USER_ID]
     *
     */
    @Maintenance_equipmentTechnician_user_idDefault(info = "默认规则")
    private Integer technician_user_id;

    @JsonIgnore
    private boolean technician_user_idDirtyFlag;

    /**
     * 属性 [PARTNER_ID]
     *
     */
    @Maintenance_equipmentPartner_idDefault(info = "默认规则")
    private Integer partner_id;

    @JsonIgnore
    private boolean partner_idDirtyFlag;

    /**
     * 属性 [CATEGORY_ID]
     *
     */
    @Maintenance_equipmentCategory_idDefault(info = "默认规则")
    private Integer category_id;

    @JsonIgnore
    private boolean category_idDirtyFlag;

    /**
     * 属性 [EMPLOYEE_ID]
     *
     */
    @Maintenance_equipmentEmployee_idDefault(info = "默认规则")
    private Integer employee_id;

    @JsonIgnore
    private boolean employee_idDirtyFlag;

    /**
     * 属性 [MAINTENANCE_TEAM_ID]
     *
     */
    @Maintenance_equipmentMaintenance_team_idDefault(info = "默认规则")
    private Integer maintenance_team_id;

    @JsonIgnore
    private boolean maintenance_team_idDirtyFlag;

    /**
     * 属性 [COMPANY_ID]
     *
     */
    @Maintenance_equipmentCompany_idDefault(info = "默认规则")
    private Integer company_id;

    @JsonIgnore
    private boolean company_idDirtyFlag;

    /**
     * 属性 [OWNER_USER_ID]
     *
     */
    @Maintenance_equipmentOwner_user_idDefault(info = "默认规则")
    private Integer owner_user_id;

    @JsonIgnore
    private boolean owner_user_idDirtyFlag;

    /**
     * 属性 [DEPARTMENT_ID]
     *
     */
    @Maintenance_equipmentDepartment_idDefault(info = "默认规则")
    private Integer department_id;

    @JsonIgnore
    private boolean department_idDirtyFlag;

    /**
     * 属性 [WRITE_UID]
     *
     */
    @Maintenance_equipmentWrite_uidDefault(info = "默认规则")
    private Integer write_uid;

    @JsonIgnore
    private boolean write_uidDirtyFlag;


    /**
     * 获取 [ACTIVITY_SUMMARY]
     */
    @JsonProperty("activity_summary")
    public String getActivity_summary(){
        return activity_summary ;
    }

    /**
     * 设置 [ACTIVITY_SUMMARY]
     */
    @JsonProperty("activity_summary")
    public void setActivity_summary(String  activity_summary){
        this.activity_summary = activity_summary ;
        this.activity_summaryDirtyFlag = true ;
    }

    /**
     * 获取 [ACTIVITY_SUMMARY]脏标记
     */
    @JsonIgnore
    public boolean getActivity_summaryDirtyFlag(){
        return activity_summaryDirtyFlag ;
    }

    /**
     * 获取 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return __last_update ;
    }

    /**
     * 设置 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

    /**
     * 获取 [__LAST_UPDATE]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return __last_updateDirtyFlag ;
    }

    /**
     * 获取 [ACTIVITY_IDS]
     */
    @JsonProperty("activity_ids")
    public String getActivity_ids(){
        return activity_ids ;
    }

    /**
     * 设置 [ACTIVITY_IDS]
     */
    @JsonProperty("activity_ids")
    public void setActivity_ids(String  activity_ids){
        this.activity_ids = activity_ids ;
        this.activity_idsDirtyFlag = true ;
    }

    /**
     * 获取 [ACTIVITY_IDS]脏标记
     */
    @JsonIgnore
    public boolean getActivity_idsDirtyFlag(){
        return activity_idsDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_NEEDACTION_COUNTER]
     */
    @JsonProperty("message_needaction_counter")
    public Integer getMessage_needaction_counter(){
        return message_needaction_counter ;
    }

    /**
     * 设置 [MESSAGE_NEEDACTION_COUNTER]
     */
    @JsonProperty("message_needaction_counter")
    public void setMessage_needaction_counter(Integer  message_needaction_counter){
        this.message_needaction_counter = message_needaction_counter ;
        this.message_needaction_counterDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_NEEDACTION_COUNTER]脏标记
     */
    @JsonIgnore
    public boolean getMessage_needaction_counterDirtyFlag(){
        return message_needaction_counterDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_UNREAD]
     */
    @JsonProperty("message_unread")
    public String getMessage_unread(){
        return message_unread ;
    }

    /**
     * 设置 [MESSAGE_UNREAD]
     */
    @JsonProperty("message_unread")
    public void setMessage_unread(String  message_unread){
        this.message_unread = message_unread ;
        this.message_unreadDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_UNREAD]脏标记
     */
    @JsonIgnore
    public boolean getMessage_unreadDirtyFlag(){
        return message_unreadDirtyFlag ;
    }

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

    /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return idDirtyFlag ;
    }

    /**
     * 获取 [EQUIPMENT_ASSIGN_TO]
     */
    @JsonProperty("equipment_assign_to")
    public String getEquipment_assign_to(){
        return equipment_assign_to ;
    }

    /**
     * 设置 [EQUIPMENT_ASSIGN_TO]
     */
    @JsonProperty("equipment_assign_to")
    public void setEquipment_assign_to(String  equipment_assign_to){
        this.equipment_assign_to = equipment_assign_to ;
        this.equipment_assign_toDirtyFlag = true ;
    }

    /**
     * 获取 [EQUIPMENT_ASSIGN_TO]脏标记
     */
    @JsonIgnore
    public boolean getEquipment_assign_toDirtyFlag(){
        return equipment_assign_toDirtyFlag ;
    }

    /**
     * 获取 [LOCATION]
     */
    @JsonProperty("location")
    public String getLocation(){
        return location ;
    }

    /**
     * 设置 [LOCATION]
     */
    @JsonProperty("location")
    public void setLocation(String  location){
        this.location = location ;
        this.locationDirtyFlag = true ;
    }

    /**
     * 获取 [LOCATION]脏标记
     */
    @JsonIgnore
    public boolean getLocationDirtyFlag(){
        return locationDirtyFlag ;
    }

    /**
     * 获取 [WARRANTY_DATE]
     */
    @JsonProperty("warranty_date")
    public Timestamp getWarranty_date(){
        return warranty_date ;
    }

    /**
     * 设置 [WARRANTY_DATE]
     */
    @JsonProperty("warranty_date")
    public void setWarranty_date(Timestamp  warranty_date){
        this.warranty_date = warranty_date ;
        this.warranty_dateDirtyFlag = true ;
    }

    /**
     * 获取 [WARRANTY_DATE]脏标记
     */
    @JsonIgnore
    public boolean getWarranty_dateDirtyFlag(){
        return warranty_dateDirtyFlag ;
    }

    /**
     * 获取 [WEBSITE_MESSAGE_IDS]
     */
    @JsonProperty("website_message_ids")
    public String getWebsite_message_ids(){
        return website_message_ids ;
    }

    /**
     * 设置 [WEBSITE_MESSAGE_IDS]
     */
    @JsonProperty("website_message_ids")
    public void setWebsite_message_ids(String  website_message_ids){
        this.website_message_ids = website_message_ids ;
        this.website_message_idsDirtyFlag = true ;
    }

    /**
     * 获取 [WEBSITE_MESSAGE_IDS]脏标记
     */
    @JsonIgnore
    public boolean getWebsite_message_idsDirtyFlag(){
        return website_message_idsDirtyFlag ;
    }

    /**
     * 获取 [NAME]
     */
    @JsonProperty("name")
    public String getName(){
        return name ;
    }

    /**
     * 设置 [NAME]
     */
    @JsonProperty("name")
    public void setName(String  name){
        this.name = name ;
        this.nameDirtyFlag = true ;
    }

    /**
     * 获取 [NAME]脏标记
     */
    @JsonIgnore
    public boolean getNameDirtyFlag(){
        return nameDirtyFlag ;
    }

    /**
     * 获取 [MAINTENANCE_DURATION]
     */
    @JsonProperty("maintenance_duration")
    public Double getMaintenance_duration(){
        return maintenance_duration ;
    }

    /**
     * 设置 [MAINTENANCE_DURATION]
     */
    @JsonProperty("maintenance_duration")
    public void setMaintenance_duration(Double  maintenance_duration){
        this.maintenance_duration = maintenance_duration ;
        this.maintenance_durationDirtyFlag = true ;
    }

    /**
     * 获取 [MAINTENANCE_DURATION]脏标记
     */
    @JsonIgnore
    public boolean getMaintenance_durationDirtyFlag(){
        return maintenance_durationDirtyFlag ;
    }

    /**
     * 获取 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return display_name ;
    }

    /**
     * 设置 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

    /**
     * 获取 [DISPLAY_NAME]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return display_nameDirtyFlag ;
    }

    /**
     * 获取 [MODEL]
     */
    @JsonProperty("model")
    public String getModel(){
        return model ;
    }

    /**
     * 设置 [MODEL]
     */
    @JsonProperty("model")
    public void setModel(String  model){
        this.model = model ;
        this.modelDirtyFlag = true ;
    }

    /**
     * 获取 [MODEL]脏标记
     */
    @JsonIgnore
    public boolean getModelDirtyFlag(){
        return modelDirtyFlag ;
    }

    /**
     * 获取 [MAINTENANCE_OPEN_COUNT]
     */
    @JsonProperty("maintenance_open_count")
    public Integer getMaintenance_open_count(){
        return maintenance_open_count ;
    }

    /**
     * 设置 [MAINTENANCE_OPEN_COUNT]
     */
    @JsonProperty("maintenance_open_count")
    public void setMaintenance_open_count(Integer  maintenance_open_count){
        this.maintenance_open_count = maintenance_open_count ;
        this.maintenance_open_countDirtyFlag = true ;
    }

    /**
     * 获取 [MAINTENANCE_OPEN_COUNT]脏标记
     */
    @JsonIgnore
    public boolean getMaintenance_open_countDirtyFlag(){
        return maintenance_open_countDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_IS_FOLLOWER]
     */
    @JsonProperty("message_is_follower")
    public String getMessage_is_follower(){
        return message_is_follower ;
    }

    /**
     * 设置 [MESSAGE_IS_FOLLOWER]
     */
    @JsonProperty("message_is_follower")
    public void setMessage_is_follower(String  message_is_follower){
        this.message_is_follower = message_is_follower ;
        this.message_is_followerDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_IS_FOLLOWER]脏标记
     */
    @JsonIgnore
    public boolean getMessage_is_followerDirtyFlag(){
        return message_is_followerDirtyFlag ;
    }

    /**
     * 获取 [ACTIVITY_STATE]
     */
    @JsonProperty("activity_state")
    public String getActivity_state(){
        return activity_state ;
    }

    /**
     * 设置 [ACTIVITY_STATE]
     */
    @JsonProperty("activity_state")
    public void setActivity_state(String  activity_state){
        this.activity_state = activity_state ;
        this.activity_stateDirtyFlag = true ;
    }

    /**
     * 获取 [ACTIVITY_STATE]脏标记
     */
    @JsonIgnore
    public boolean getActivity_stateDirtyFlag(){
        return activity_stateDirtyFlag ;
    }

    /**
     * 获取 [ACTIVITY_DATE_DEADLINE]
     */
    @JsonProperty("activity_date_deadline")
    public Timestamp getActivity_date_deadline(){
        return activity_date_deadline ;
    }

    /**
     * 设置 [ACTIVITY_DATE_DEADLINE]
     */
    @JsonProperty("activity_date_deadline")
    public void setActivity_date_deadline(Timestamp  activity_date_deadline){
        this.activity_date_deadline = activity_date_deadline ;
        this.activity_date_deadlineDirtyFlag = true ;
    }

    /**
     * 获取 [ACTIVITY_DATE_DEADLINE]脏标记
     */
    @JsonIgnore
    public boolean getActivity_date_deadlineDirtyFlag(){
        return activity_date_deadlineDirtyFlag ;
    }

    /**
     * 获取 [MAINTENANCE_COUNT]
     */
    @JsonProperty("maintenance_count")
    public Integer getMaintenance_count(){
        return maintenance_count ;
    }

    /**
     * 设置 [MAINTENANCE_COUNT]
     */
    @JsonProperty("maintenance_count")
    public void setMaintenance_count(Integer  maintenance_count){
        this.maintenance_count = maintenance_count ;
        this.maintenance_countDirtyFlag = true ;
    }

    /**
     * 获取 [MAINTENANCE_COUNT]脏标记
     */
    @JsonIgnore
    public boolean getMaintenance_countDirtyFlag(){
        return maintenance_countDirtyFlag ;
    }

    /**
     * 获取 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return write_date ;
    }

    /**
     * 设置 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return write_dateDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_UNREAD_COUNTER]
     */
    @JsonProperty("message_unread_counter")
    public Integer getMessage_unread_counter(){
        return message_unread_counter ;
    }

    /**
     * 设置 [MESSAGE_UNREAD_COUNTER]
     */
    @JsonProperty("message_unread_counter")
    public void setMessage_unread_counter(Integer  message_unread_counter){
        this.message_unread_counter = message_unread_counter ;
        this.message_unread_counterDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_UNREAD_COUNTER]脏标记
     */
    @JsonIgnore
    public boolean getMessage_unread_counterDirtyFlag(){
        return message_unread_counterDirtyFlag ;
    }

    /**
     * 获取 [NOTE]
     */
    @JsonProperty("note")
    public String getNote(){
        return note ;
    }

    /**
     * 设置 [NOTE]
     */
    @JsonProperty("note")
    public void setNote(String  note){
        this.note = note ;
        this.noteDirtyFlag = true ;
    }

    /**
     * 获取 [NOTE]脏标记
     */
    @JsonIgnore
    public boolean getNoteDirtyFlag(){
        return noteDirtyFlag ;
    }

    /**
     * 获取 [PARTNER_REF]
     */
    @JsonProperty("partner_ref")
    public String getPartner_ref(){
        return partner_ref ;
    }

    /**
     * 设置 [PARTNER_REF]
     */
    @JsonProperty("partner_ref")
    public void setPartner_ref(String  partner_ref){
        this.partner_ref = partner_ref ;
        this.partner_refDirtyFlag = true ;
    }

    /**
     * 获取 [PARTNER_REF]脏标记
     */
    @JsonIgnore
    public boolean getPartner_refDirtyFlag(){
        return partner_refDirtyFlag ;
    }

    /**
     * 获取 [MAINTENANCE_IDS]
     */
    @JsonProperty("maintenance_ids")
    public String getMaintenance_ids(){
        return maintenance_ids ;
    }

    /**
     * 设置 [MAINTENANCE_IDS]
     */
    @JsonProperty("maintenance_ids")
    public void setMaintenance_ids(String  maintenance_ids){
        this.maintenance_ids = maintenance_ids ;
        this.maintenance_idsDirtyFlag = true ;
    }

    /**
     * 获取 [MAINTENANCE_IDS]脏标记
     */
    @JsonIgnore
    public boolean getMaintenance_idsDirtyFlag(){
        return maintenance_idsDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_HAS_ERROR_COUNTER]
     */
    @JsonProperty("message_has_error_counter")
    public Integer getMessage_has_error_counter(){
        return message_has_error_counter ;
    }

    /**
     * 设置 [MESSAGE_HAS_ERROR_COUNTER]
     */
    @JsonProperty("message_has_error_counter")
    public void setMessage_has_error_counter(Integer  message_has_error_counter){
        this.message_has_error_counter = message_has_error_counter ;
        this.message_has_error_counterDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_HAS_ERROR_COUNTER]脏标记
     */
    @JsonIgnore
    public boolean getMessage_has_error_counterDirtyFlag(){
        return message_has_error_counterDirtyFlag ;
    }

    /**
     * 获取 [ACTIVITY_USER_ID]
     */
    @JsonProperty("activity_user_id")
    public Integer getActivity_user_id(){
        return activity_user_id ;
    }

    /**
     * 设置 [ACTIVITY_USER_ID]
     */
    @JsonProperty("activity_user_id")
    public void setActivity_user_id(Integer  activity_user_id){
        this.activity_user_id = activity_user_id ;
        this.activity_user_idDirtyFlag = true ;
    }

    /**
     * 获取 [ACTIVITY_USER_ID]脏标记
     */
    @JsonIgnore
    public boolean getActivity_user_idDirtyFlag(){
        return activity_user_idDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_MAIN_ATTACHMENT_ID]
     */
    @JsonProperty("message_main_attachment_id")
    public Integer getMessage_main_attachment_id(){
        return message_main_attachment_id ;
    }

    /**
     * 设置 [MESSAGE_MAIN_ATTACHMENT_ID]
     */
    @JsonProperty("message_main_attachment_id")
    public void setMessage_main_attachment_id(Integer  message_main_attachment_id){
        this.message_main_attachment_id = message_main_attachment_id ;
        this.message_main_attachment_idDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_MAIN_ATTACHMENT_ID]脏标记
     */
    @JsonIgnore
    public boolean getMessage_main_attachment_idDirtyFlag(){
        return message_main_attachment_idDirtyFlag ;
    }

    /**
     * 获取 [SERIAL_NO]
     */
    @JsonProperty("serial_no")
    public String getSerial_no(){
        return serial_no ;
    }

    /**
     * 设置 [SERIAL_NO]
     */
    @JsonProperty("serial_no")
    public void setSerial_no(String  serial_no){
        this.serial_no = serial_no ;
        this.serial_noDirtyFlag = true ;
    }

    /**
     * 获取 [SERIAL_NO]脏标记
     */
    @JsonIgnore
    public boolean getSerial_noDirtyFlag(){
        return serial_noDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_ATTACHMENT_COUNT]
     */
    @JsonProperty("message_attachment_count")
    public Integer getMessage_attachment_count(){
        return message_attachment_count ;
    }

    /**
     * 设置 [MESSAGE_ATTACHMENT_COUNT]
     */
    @JsonProperty("message_attachment_count")
    public void setMessage_attachment_count(Integer  message_attachment_count){
        this.message_attachment_count = message_attachment_count ;
        this.message_attachment_countDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_ATTACHMENT_COUNT]脏标记
     */
    @JsonIgnore
    public boolean getMessage_attachment_countDirtyFlag(){
        return message_attachment_countDirtyFlag ;
    }

    /**
     * 获取 [COST]
     */
    @JsonProperty("cost")
    public Double getCost(){
        return cost ;
    }

    /**
     * 设置 [COST]
     */
    @JsonProperty("cost")
    public void setCost(Double  cost){
        this.cost = cost ;
        this.costDirtyFlag = true ;
    }

    /**
     * 获取 [COST]脏标记
     */
    @JsonIgnore
    public boolean getCostDirtyFlag(){
        return costDirtyFlag ;
    }

    /**
     * 获取 [NEXT_ACTION_DATE]
     */
    @JsonProperty("next_action_date")
    public Timestamp getNext_action_date(){
        return next_action_date ;
    }

    /**
     * 设置 [NEXT_ACTION_DATE]
     */
    @JsonProperty("next_action_date")
    public void setNext_action_date(Timestamp  next_action_date){
        this.next_action_date = next_action_date ;
        this.next_action_dateDirtyFlag = true ;
    }

    /**
     * 获取 [NEXT_ACTION_DATE]脏标记
     */
    @JsonIgnore
    public boolean getNext_action_dateDirtyFlag(){
        return next_action_dateDirtyFlag ;
    }

    /**
     * 获取 [ASSIGN_DATE]
     */
    @JsonProperty("assign_date")
    public Timestamp getAssign_date(){
        return assign_date ;
    }

    /**
     * 设置 [ASSIGN_DATE]
     */
    @JsonProperty("assign_date")
    public void setAssign_date(Timestamp  assign_date){
        this.assign_date = assign_date ;
        this.assign_dateDirtyFlag = true ;
    }

    /**
     * 获取 [ASSIGN_DATE]脏标记
     */
    @JsonIgnore
    public boolean getAssign_dateDirtyFlag(){
        return assign_dateDirtyFlag ;
    }

    /**
     * 获取 [EFFECTIVE_DATE]
     */
    @JsonProperty("effective_date")
    public Timestamp getEffective_date(){
        return effective_date ;
    }

    /**
     * 设置 [EFFECTIVE_DATE]
     */
    @JsonProperty("effective_date")
    public void setEffective_date(Timestamp  effective_date){
        this.effective_date = effective_date ;
        this.effective_dateDirtyFlag = true ;
    }

    /**
     * 获取 [EFFECTIVE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getEffective_dateDirtyFlag(){
        return effective_dateDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_NEEDACTION]
     */
    @JsonProperty("message_needaction")
    public String getMessage_needaction(){
        return message_needaction ;
    }

    /**
     * 设置 [MESSAGE_NEEDACTION]
     */
    @JsonProperty("message_needaction")
    public void setMessage_needaction(String  message_needaction){
        this.message_needaction = message_needaction ;
        this.message_needactionDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_NEEDACTION]脏标记
     */
    @JsonIgnore
    public boolean getMessage_needactionDirtyFlag(){
        return message_needactionDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_CHANNEL_IDS]
     */
    @JsonProperty("message_channel_ids")
    public String getMessage_channel_ids(){
        return message_channel_ids ;
    }

    /**
     * 设置 [MESSAGE_CHANNEL_IDS]
     */
    @JsonProperty("message_channel_ids")
    public void setMessage_channel_ids(String  message_channel_ids){
        this.message_channel_ids = message_channel_ids ;
        this.message_channel_idsDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_CHANNEL_IDS]脏标记
     */
    @JsonIgnore
    public boolean getMessage_channel_idsDirtyFlag(){
        return message_channel_idsDirtyFlag ;
    }

    /**
     * 获取 [ACTIVE]
     */
    @JsonProperty("active")
    public String getActive(){
        return active ;
    }

    /**
     * 设置 [ACTIVE]
     */
    @JsonProperty("active")
    public void setActive(String  active){
        this.active = active ;
        this.activeDirtyFlag = true ;
    }

    /**
     * 获取 [ACTIVE]脏标记
     */
    @JsonIgnore
    public boolean getActiveDirtyFlag(){
        return activeDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_IDS]
     */
    @JsonProperty("message_ids")
    public String getMessage_ids(){
        return message_ids ;
    }

    /**
     * 设置 [MESSAGE_IDS]
     */
    @JsonProperty("message_ids")
    public void setMessage_ids(String  message_ids){
        this.message_ids = message_ids ;
        this.message_idsDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_IDS]脏标记
     */
    @JsonIgnore
    public boolean getMessage_idsDirtyFlag(){
        return message_idsDirtyFlag ;
    }

    /**
     * 获取 [SCRAP_DATE]
     */
    @JsonProperty("scrap_date")
    public Timestamp getScrap_date(){
        return scrap_date ;
    }

    /**
     * 设置 [SCRAP_DATE]
     */
    @JsonProperty("scrap_date")
    public void setScrap_date(Timestamp  scrap_date){
        this.scrap_date = scrap_date ;
        this.scrap_dateDirtyFlag = true ;
    }

    /**
     * 获取 [SCRAP_DATE]脏标记
     */
    @JsonIgnore
    public boolean getScrap_dateDirtyFlag(){
        return scrap_dateDirtyFlag ;
    }

    /**
     * 获取 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return create_date ;
    }

    /**
     * 设置 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return create_dateDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_FOLLOWER_IDS]
     */
    @JsonProperty("message_follower_ids")
    public String getMessage_follower_ids(){
        return message_follower_ids ;
    }

    /**
     * 设置 [MESSAGE_FOLLOWER_IDS]
     */
    @JsonProperty("message_follower_ids")
    public void setMessage_follower_ids(String  message_follower_ids){
        this.message_follower_ids = message_follower_ids ;
        this.message_follower_idsDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_FOLLOWER_IDS]脏标记
     */
    @JsonIgnore
    public boolean getMessage_follower_idsDirtyFlag(){
        return message_follower_idsDirtyFlag ;
    }

    /**
     * 获取 [PERIOD]
     */
    @JsonProperty("period")
    public Integer getPeriod(){
        return period ;
    }

    /**
     * 设置 [PERIOD]
     */
    @JsonProperty("period")
    public void setPeriod(Integer  period){
        this.period = period ;
        this.periodDirtyFlag = true ;
    }

    /**
     * 获取 [PERIOD]脏标记
     */
    @JsonIgnore
    public boolean getPeriodDirtyFlag(){
        return periodDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_HAS_ERROR]
     */
    @JsonProperty("message_has_error")
    public String getMessage_has_error(){
        return message_has_error ;
    }

    /**
     * 设置 [MESSAGE_HAS_ERROR]
     */
    @JsonProperty("message_has_error")
    public void setMessage_has_error(String  message_has_error){
        this.message_has_error = message_has_error ;
        this.message_has_errorDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_HAS_ERROR]脏标记
     */
    @JsonIgnore
    public boolean getMessage_has_errorDirtyFlag(){
        return message_has_errorDirtyFlag ;
    }

    /**
     * 获取 [ACTIVITY_TYPE_ID]
     */
    @JsonProperty("activity_type_id")
    public Integer getActivity_type_id(){
        return activity_type_id ;
    }

    /**
     * 设置 [ACTIVITY_TYPE_ID]
     */
    @JsonProperty("activity_type_id")
    public void setActivity_type_id(Integer  activity_type_id){
        this.activity_type_id = activity_type_id ;
        this.activity_type_idDirtyFlag = true ;
    }

    /**
     * 获取 [ACTIVITY_TYPE_ID]脏标记
     */
    @JsonIgnore
    public boolean getActivity_type_idDirtyFlag(){
        return activity_type_idDirtyFlag ;
    }

    /**
     * 获取 [COLOR]
     */
    @JsonProperty("color")
    public Integer getColor(){
        return color ;
    }

    /**
     * 设置 [COLOR]
     */
    @JsonProperty("color")
    public void setColor(Integer  color){
        this.color = color ;
        this.colorDirtyFlag = true ;
    }

    /**
     * 获取 [COLOR]脏标记
     */
    @JsonIgnore
    public boolean getColorDirtyFlag(){
        return colorDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_PARTNER_IDS]
     */
    @JsonProperty("message_partner_ids")
    public String getMessage_partner_ids(){
        return message_partner_ids ;
    }

    /**
     * 设置 [MESSAGE_PARTNER_IDS]
     */
    @JsonProperty("message_partner_ids")
    public void setMessage_partner_ids(String  message_partner_ids){
        this.message_partner_ids = message_partner_ids ;
        this.message_partner_idsDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_PARTNER_IDS]脏标记
     */
    @JsonIgnore
    public boolean getMessage_partner_idsDirtyFlag(){
        return message_partner_idsDirtyFlag ;
    }

    /**
     * 获取 [DEPARTMENT_ID_TEXT]
     */
    @JsonProperty("department_id_text")
    public String getDepartment_id_text(){
        return department_id_text ;
    }

    /**
     * 设置 [DEPARTMENT_ID_TEXT]
     */
    @JsonProperty("department_id_text")
    public void setDepartment_id_text(String  department_id_text){
        this.department_id_text = department_id_text ;
        this.department_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [DEPARTMENT_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getDepartment_id_textDirtyFlag(){
        return department_id_textDirtyFlag ;
    }

    /**
     * 获取 [EMPLOYEE_ID_TEXT]
     */
    @JsonProperty("employee_id_text")
    public String getEmployee_id_text(){
        return employee_id_text ;
    }

    /**
     * 设置 [EMPLOYEE_ID_TEXT]
     */
    @JsonProperty("employee_id_text")
    public void setEmployee_id_text(String  employee_id_text){
        this.employee_id_text = employee_id_text ;
        this.employee_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [EMPLOYEE_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getEmployee_id_textDirtyFlag(){
        return employee_id_textDirtyFlag ;
    }

    /**
     * 获取 [TECHNICIAN_USER_ID_TEXT]
     */
    @JsonProperty("technician_user_id_text")
    public String getTechnician_user_id_text(){
        return technician_user_id_text ;
    }

    /**
     * 设置 [TECHNICIAN_USER_ID_TEXT]
     */
    @JsonProperty("technician_user_id_text")
    public void setTechnician_user_id_text(String  technician_user_id_text){
        this.technician_user_id_text = technician_user_id_text ;
        this.technician_user_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [TECHNICIAN_USER_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getTechnician_user_id_textDirtyFlag(){
        return technician_user_id_textDirtyFlag ;
    }

    /**
     * 获取 [COMPANY_ID_TEXT]
     */
    @JsonProperty("company_id_text")
    public String getCompany_id_text(){
        return company_id_text ;
    }

    /**
     * 设置 [COMPANY_ID_TEXT]
     */
    @JsonProperty("company_id_text")
    public void setCompany_id_text(String  company_id_text){
        this.company_id_text = company_id_text ;
        this.company_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [COMPANY_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getCompany_id_textDirtyFlag(){
        return company_id_textDirtyFlag ;
    }

    /**
     * 获取 [CATEGORY_ID_TEXT]
     */
    @JsonProperty("category_id_text")
    public String getCategory_id_text(){
        return category_id_text ;
    }

    /**
     * 设置 [CATEGORY_ID_TEXT]
     */
    @JsonProperty("category_id_text")
    public void setCategory_id_text(String  category_id_text){
        this.category_id_text = category_id_text ;
        this.category_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [CATEGORY_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getCategory_id_textDirtyFlag(){
        return category_id_textDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return create_uid_text ;
    }

    /**
     * 设置 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return create_uid_textDirtyFlag ;
    }

    /**
     * 获取 [PARTNER_ID_TEXT]
     */
    @JsonProperty("partner_id_text")
    public String getPartner_id_text(){
        return partner_id_text ;
    }

    /**
     * 设置 [PARTNER_ID_TEXT]
     */
    @JsonProperty("partner_id_text")
    public void setPartner_id_text(String  partner_id_text){
        this.partner_id_text = partner_id_text ;
        this.partner_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [PARTNER_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getPartner_id_textDirtyFlag(){
        return partner_id_textDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return write_uid_text ;
    }

    /**
     * 设置 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return write_uid_textDirtyFlag ;
    }

    /**
     * 获取 [OWNER_USER_ID_TEXT]
     */
    @JsonProperty("owner_user_id_text")
    public String getOwner_user_id_text(){
        return owner_user_id_text ;
    }

    /**
     * 设置 [OWNER_USER_ID_TEXT]
     */
    @JsonProperty("owner_user_id_text")
    public void setOwner_user_id_text(String  owner_user_id_text){
        this.owner_user_id_text = owner_user_id_text ;
        this.owner_user_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [OWNER_USER_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getOwner_user_id_textDirtyFlag(){
        return owner_user_id_textDirtyFlag ;
    }

    /**
     * 获取 [MAINTENANCE_TEAM_ID_TEXT]
     */
    @JsonProperty("maintenance_team_id_text")
    public String getMaintenance_team_id_text(){
        return maintenance_team_id_text ;
    }

    /**
     * 设置 [MAINTENANCE_TEAM_ID_TEXT]
     */
    @JsonProperty("maintenance_team_id_text")
    public void setMaintenance_team_id_text(String  maintenance_team_id_text){
        this.maintenance_team_id_text = maintenance_team_id_text ;
        this.maintenance_team_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [MAINTENANCE_TEAM_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getMaintenance_team_id_textDirtyFlag(){
        return maintenance_team_id_textDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return create_uid ;
    }

    /**
     * 设置 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return create_uidDirtyFlag ;
    }

    /**
     * 获取 [TECHNICIAN_USER_ID]
     */
    @JsonProperty("technician_user_id")
    public Integer getTechnician_user_id(){
        return technician_user_id ;
    }

    /**
     * 设置 [TECHNICIAN_USER_ID]
     */
    @JsonProperty("technician_user_id")
    public void setTechnician_user_id(Integer  technician_user_id){
        this.technician_user_id = technician_user_id ;
        this.technician_user_idDirtyFlag = true ;
    }

    /**
     * 获取 [TECHNICIAN_USER_ID]脏标记
     */
    @JsonIgnore
    public boolean getTechnician_user_idDirtyFlag(){
        return technician_user_idDirtyFlag ;
    }

    /**
     * 获取 [PARTNER_ID]
     */
    @JsonProperty("partner_id")
    public Integer getPartner_id(){
        return partner_id ;
    }

    /**
     * 设置 [PARTNER_ID]
     */
    @JsonProperty("partner_id")
    public void setPartner_id(Integer  partner_id){
        this.partner_id = partner_id ;
        this.partner_idDirtyFlag = true ;
    }

    /**
     * 获取 [PARTNER_ID]脏标记
     */
    @JsonIgnore
    public boolean getPartner_idDirtyFlag(){
        return partner_idDirtyFlag ;
    }

    /**
     * 获取 [CATEGORY_ID]
     */
    @JsonProperty("category_id")
    public Integer getCategory_id(){
        return category_id ;
    }

    /**
     * 设置 [CATEGORY_ID]
     */
    @JsonProperty("category_id")
    public void setCategory_id(Integer  category_id){
        this.category_id = category_id ;
        this.category_idDirtyFlag = true ;
    }

    /**
     * 获取 [CATEGORY_ID]脏标记
     */
    @JsonIgnore
    public boolean getCategory_idDirtyFlag(){
        return category_idDirtyFlag ;
    }

    /**
     * 获取 [EMPLOYEE_ID]
     */
    @JsonProperty("employee_id")
    public Integer getEmployee_id(){
        return employee_id ;
    }

    /**
     * 设置 [EMPLOYEE_ID]
     */
    @JsonProperty("employee_id")
    public void setEmployee_id(Integer  employee_id){
        this.employee_id = employee_id ;
        this.employee_idDirtyFlag = true ;
    }

    /**
     * 获取 [EMPLOYEE_ID]脏标记
     */
    @JsonIgnore
    public boolean getEmployee_idDirtyFlag(){
        return employee_idDirtyFlag ;
    }

    /**
     * 获取 [MAINTENANCE_TEAM_ID]
     */
    @JsonProperty("maintenance_team_id")
    public Integer getMaintenance_team_id(){
        return maintenance_team_id ;
    }

    /**
     * 设置 [MAINTENANCE_TEAM_ID]
     */
    @JsonProperty("maintenance_team_id")
    public void setMaintenance_team_id(Integer  maintenance_team_id){
        this.maintenance_team_id = maintenance_team_id ;
        this.maintenance_team_idDirtyFlag = true ;
    }

    /**
     * 获取 [MAINTENANCE_TEAM_ID]脏标记
     */
    @JsonIgnore
    public boolean getMaintenance_team_idDirtyFlag(){
        return maintenance_team_idDirtyFlag ;
    }

    /**
     * 获取 [COMPANY_ID]
     */
    @JsonProperty("company_id")
    public Integer getCompany_id(){
        return company_id ;
    }

    /**
     * 设置 [COMPANY_ID]
     */
    @JsonProperty("company_id")
    public void setCompany_id(Integer  company_id){
        this.company_id = company_id ;
        this.company_idDirtyFlag = true ;
    }

    /**
     * 获取 [COMPANY_ID]脏标记
     */
    @JsonIgnore
    public boolean getCompany_idDirtyFlag(){
        return company_idDirtyFlag ;
    }

    /**
     * 获取 [OWNER_USER_ID]
     */
    @JsonProperty("owner_user_id")
    public Integer getOwner_user_id(){
        return owner_user_id ;
    }

    /**
     * 设置 [OWNER_USER_ID]
     */
    @JsonProperty("owner_user_id")
    public void setOwner_user_id(Integer  owner_user_id){
        this.owner_user_id = owner_user_id ;
        this.owner_user_idDirtyFlag = true ;
    }

    /**
     * 获取 [OWNER_USER_ID]脏标记
     */
    @JsonIgnore
    public boolean getOwner_user_idDirtyFlag(){
        return owner_user_idDirtyFlag ;
    }

    /**
     * 获取 [DEPARTMENT_ID]
     */
    @JsonProperty("department_id")
    public Integer getDepartment_id(){
        return department_id ;
    }

    /**
     * 设置 [DEPARTMENT_ID]
     */
    @JsonProperty("department_id")
    public void setDepartment_id(Integer  department_id){
        this.department_id = department_id ;
        this.department_idDirtyFlag = true ;
    }

    /**
     * 获取 [DEPARTMENT_ID]脏标记
     */
    @JsonIgnore
    public boolean getDepartment_idDirtyFlag(){
        return department_idDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return write_uid ;
    }

    /**
     * 设置 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return write_uidDirtyFlag ;
    }



    public Maintenance_equipment toDO() {
        Maintenance_equipment srfdomain = new Maintenance_equipment();
        if(getActivity_summaryDirtyFlag())
            srfdomain.setActivity_summary(activity_summary);
        if(get__last_updateDirtyFlag())
            srfdomain.set__last_update(__last_update);
        if(getActivity_idsDirtyFlag())
            srfdomain.setActivity_ids(activity_ids);
        if(getMessage_needaction_counterDirtyFlag())
            srfdomain.setMessage_needaction_counter(message_needaction_counter);
        if(getMessage_unreadDirtyFlag())
            srfdomain.setMessage_unread(message_unread);
        if(getIdDirtyFlag())
            srfdomain.setId(id);
        if(getEquipment_assign_toDirtyFlag())
            srfdomain.setEquipment_assign_to(equipment_assign_to);
        if(getLocationDirtyFlag())
            srfdomain.setLocation(location);
        if(getWarranty_dateDirtyFlag())
            srfdomain.setWarranty_date(warranty_date);
        if(getWebsite_message_idsDirtyFlag())
            srfdomain.setWebsite_message_ids(website_message_ids);
        if(getNameDirtyFlag())
            srfdomain.setName(name);
        if(getMaintenance_durationDirtyFlag())
            srfdomain.setMaintenance_duration(maintenance_duration);
        if(getDisplay_nameDirtyFlag())
            srfdomain.setDisplay_name(display_name);
        if(getModelDirtyFlag())
            srfdomain.setModel(model);
        if(getMaintenance_open_countDirtyFlag())
            srfdomain.setMaintenance_open_count(maintenance_open_count);
        if(getMessage_is_followerDirtyFlag())
            srfdomain.setMessage_is_follower(message_is_follower);
        if(getActivity_stateDirtyFlag())
            srfdomain.setActivity_state(activity_state);
        if(getActivity_date_deadlineDirtyFlag())
            srfdomain.setActivity_date_deadline(activity_date_deadline);
        if(getMaintenance_countDirtyFlag())
            srfdomain.setMaintenance_count(maintenance_count);
        if(getWrite_dateDirtyFlag())
            srfdomain.setWrite_date(write_date);
        if(getMessage_unread_counterDirtyFlag())
            srfdomain.setMessage_unread_counter(message_unread_counter);
        if(getNoteDirtyFlag())
            srfdomain.setNote(note);
        if(getPartner_refDirtyFlag())
            srfdomain.setPartner_ref(partner_ref);
        if(getMaintenance_idsDirtyFlag())
            srfdomain.setMaintenance_ids(maintenance_ids);
        if(getMessage_has_error_counterDirtyFlag())
            srfdomain.setMessage_has_error_counter(message_has_error_counter);
        if(getActivity_user_idDirtyFlag())
            srfdomain.setActivity_user_id(activity_user_id);
        if(getMessage_main_attachment_idDirtyFlag())
            srfdomain.setMessage_main_attachment_id(message_main_attachment_id);
        if(getSerial_noDirtyFlag())
            srfdomain.setSerial_no(serial_no);
        if(getMessage_attachment_countDirtyFlag())
            srfdomain.setMessage_attachment_count(message_attachment_count);
        if(getCostDirtyFlag())
            srfdomain.setCost(cost);
        if(getNext_action_dateDirtyFlag())
            srfdomain.setNext_action_date(next_action_date);
        if(getAssign_dateDirtyFlag())
            srfdomain.setAssign_date(assign_date);
        if(getEffective_dateDirtyFlag())
            srfdomain.setEffective_date(effective_date);
        if(getMessage_needactionDirtyFlag())
            srfdomain.setMessage_needaction(message_needaction);
        if(getMessage_channel_idsDirtyFlag())
            srfdomain.setMessage_channel_ids(message_channel_ids);
        if(getActiveDirtyFlag())
            srfdomain.setActive(active);
        if(getMessage_idsDirtyFlag())
            srfdomain.setMessage_ids(message_ids);
        if(getScrap_dateDirtyFlag())
            srfdomain.setScrap_date(scrap_date);
        if(getCreate_dateDirtyFlag())
            srfdomain.setCreate_date(create_date);
        if(getMessage_follower_idsDirtyFlag())
            srfdomain.setMessage_follower_ids(message_follower_ids);
        if(getPeriodDirtyFlag())
            srfdomain.setPeriod(period);
        if(getMessage_has_errorDirtyFlag())
            srfdomain.setMessage_has_error(message_has_error);
        if(getActivity_type_idDirtyFlag())
            srfdomain.setActivity_type_id(activity_type_id);
        if(getColorDirtyFlag())
            srfdomain.setColor(color);
        if(getMessage_partner_idsDirtyFlag())
            srfdomain.setMessage_partner_ids(message_partner_ids);
        if(getDepartment_id_textDirtyFlag())
            srfdomain.setDepartment_id_text(department_id_text);
        if(getEmployee_id_textDirtyFlag())
            srfdomain.setEmployee_id_text(employee_id_text);
        if(getTechnician_user_id_textDirtyFlag())
            srfdomain.setTechnician_user_id_text(technician_user_id_text);
        if(getCompany_id_textDirtyFlag())
            srfdomain.setCompany_id_text(company_id_text);
        if(getCategory_id_textDirtyFlag())
            srfdomain.setCategory_id_text(category_id_text);
        if(getCreate_uid_textDirtyFlag())
            srfdomain.setCreate_uid_text(create_uid_text);
        if(getPartner_id_textDirtyFlag())
            srfdomain.setPartner_id_text(partner_id_text);
        if(getWrite_uid_textDirtyFlag())
            srfdomain.setWrite_uid_text(write_uid_text);
        if(getOwner_user_id_textDirtyFlag())
            srfdomain.setOwner_user_id_text(owner_user_id_text);
        if(getMaintenance_team_id_textDirtyFlag())
            srfdomain.setMaintenance_team_id_text(maintenance_team_id_text);
        if(getCreate_uidDirtyFlag())
            srfdomain.setCreate_uid(create_uid);
        if(getTechnician_user_idDirtyFlag())
            srfdomain.setTechnician_user_id(technician_user_id);
        if(getPartner_idDirtyFlag())
            srfdomain.setPartner_id(partner_id);
        if(getCategory_idDirtyFlag())
            srfdomain.setCategory_id(category_id);
        if(getEmployee_idDirtyFlag())
            srfdomain.setEmployee_id(employee_id);
        if(getMaintenance_team_idDirtyFlag())
            srfdomain.setMaintenance_team_id(maintenance_team_id);
        if(getCompany_idDirtyFlag())
            srfdomain.setCompany_id(company_id);
        if(getOwner_user_idDirtyFlag())
            srfdomain.setOwner_user_id(owner_user_id);
        if(getDepartment_idDirtyFlag())
            srfdomain.setDepartment_id(department_id);
        if(getWrite_uidDirtyFlag())
            srfdomain.setWrite_uid(write_uid);

        return srfdomain;
    }

    public void fromDO(Maintenance_equipment srfdomain) {
        if(srfdomain == null )
          return ;
        if(srfdomain.getActivity_summaryDirtyFlag())
            this.setActivity_summary(srfdomain.getActivity_summary());
        if(srfdomain.get__last_updateDirtyFlag())
            this.set__last_update(srfdomain.get__last_update());
        if(srfdomain.getActivity_idsDirtyFlag())
            this.setActivity_ids(srfdomain.getActivity_ids());
        if(srfdomain.getMessage_needaction_counterDirtyFlag())
            this.setMessage_needaction_counter(srfdomain.getMessage_needaction_counter());
        if(srfdomain.getMessage_unreadDirtyFlag())
            this.setMessage_unread(srfdomain.getMessage_unread());
        if(srfdomain.getIdDirtyFlag())
            this.setId(srfdomain.getId());
        if(srfdomain.getEquipment_assign_toDirtyFlag())
            this.setEquipment_assign_to(srfdomain.getEquipment_assign_to());
        if(srfdomain.getLocationDirtyFlag())
            this.setLocation(srfdomain.getLocation());
        if(srfdomain.getWarranty_dateDirtyFlag())
            this.setWarranty_date(srfdomain.getWarranty_date());
        if(srfdomain.getWebsite_message_idsDirtyFlag())
            this.setWebsite_message_ids(srfdomain.getWebsite_message_ids());
        if(srfdomain.getNameDirtyFlag())
            this.setName(srfdomain.getName());
        if(srfdomain.getMaintenance_durationDirtyFlag())
            this.setMaintenance_duration(srfdomain.getMaintenance_duration());
        if(srfdomain.getDisplay_nameDirtyFlag())
            this.setDisplay_name(srfdomain.getDisplay_name());
        if(srfdomain.getModelDirtyFlag())
            this.setModel(srfdomain.getModel());
        if(srfdomain.getMaintenance_open_countDirtyFlag())
            this.setMaintenance_open_count(srfdomain.getMaintenance_open_count());
        if(srfdomain.getMessage_is_followerDirtyFlag())
            this.setMessage_is_follower(srfdomain.getMessage_is_follower());
        if(srfdomain.getActivity_stateDirtyFlag())
            this.setActivity_state(srfdomain.getActivity_state());
        if(srfdomain.getActivity_date_deadlineDirtyFlag())
            this.setActivity_date_deadline(srfdomain.getActivity_date_deadline());
        if(srfdomain.getMaintenance_countDirtyFlag())
            this.setMaintenance_count(srfdomain.getMaintenance_count());
        if(srfdomain.getWrite_dateDirtyFlag())
            this.setWrite_date(srfdomain.getWrite_date());
        if(srfdomain.getMessage_unread_counterDirtyFlag())
            this.setMessage_unread_counter(srfdomain.getMessage_unread_counter());
        if(srfdomain.getNoteDirtyFlag())
            this.setNote(srfdomain.getNote());
        if(srfdomain.getPartner_refDirtyFlag())
            this.setPartner_ref(srfdomain.getPartner_ref());
        if(srfdomain.getMaintenance_idsDirtyFlag())
            this.setMaintenance_ids(srfdomain.getMaintenance_ids());
        if(srfdomain.getMessage_has_error_counterDirtyFlag())
            this.setMessage_has_error_counter(srfdomain.getMessage_has_error_counter());
        if(srfdomain.getActivity_user_idDirtyFlag())
            this.setActivity_user_id(srfdomain.getActivity_user_id());
        if(srfdomain.getMessage_main_attachment_idDirtyFlag())
            this.setMessage_main_attachment_id(srfdomain.getMessage_main_attachment_id());
        if(srfdomain.getSerial_noDirtyFlag())
            this.setSerial_no(srfdomain.getSerial_no());
        if(srfdomain.getMessage_attachment_countDirtyFlag())
            this.setMessage_attachment_count(srfdomain.getMessage_attachment_count());
        if(srfdomain.getCostDirtyFlag())
            this.setCost(srfdomain.getCost());
        if(srfdomain.getNext_action_dateDirtyFlag())
            this.setNext_action_date(srfdomain.getNext_action_date());
        if(srfdomain.getAssign_dateDirtyFlag())
            this.setAssign_date(srfdomain.getAssign_date());
        if(srfdomain.getEffective_dateDirtyFlag())
            this.setEffective_date(srfdomain.getEffective_date());
        if(srfdomain.getMessage_needactionDirtyFlag())
            this.setMessage_needaction(srfdomain.getMessage_needaction());
        if(srfdomain.getMessage_channel_idsDirtyFlag())
            this.setMessage_channel_ids(srfdomain.getMessage_channel_ids());
        if(srfdomain.getActiveDirtyFlag())
            this.setActive(srfdomain.getActive());
        if(srfdomain.getMessage_idsDirtyFlag())
            this.setMessage_ids(srfdomain.getMessage_ids());
        if(srfdomain.getScrap_dateDirtyFlag())
            this.setScrap_date(srfdomain.getScrap_date());
        if(srfdomain.getCreate_dateDirtyFlag())
            this.setCreate_date(srfdomain.getCreate_date());
        if(srfdomain.getMessage_follower_idsDirtyFlag())
            this.setMessage_follower_ids(srfdomain.getMessage_follower_ids());
        if(srfdomain.getPeriodDirtyFlag())
            this.setPeriod(srfdomain.getPeriod());
        if(srfdomain.getMessage_has_errorDirtyFlag())
            this.setMessage_has_error(srfdomain.getMessage_has_error());
        if(srfdomain.getActivity_type_idDirtyFlag())
            this.setActivity_type_id(srfdomain.getActivity_type_id());
        if(srfdomain.getColorDirtyFlag())
            this.setColor(srfdomain.getColor());
        if(srfdomain.getMessage_partner_idsDirtyFlag())
            this.setMessage_partner_ids(srfdomain.getMessage_partner_ids());
        if(srfdomain.getDepartment_id_textDirtyFlag())
            this.setDepartment_id_text(srfdomain.getDepartment_id_text());
        if(srfdomain.getEmployee_id_textDirtyFlag())
            this.setEmployee_id_text(srfdomain.getEmployee_id_text());
        if(srfdomain.getTechnician_user_id_textDirtyFlag())
            this.setTechnician_user_id_text(srfdomain.getTechnician_user_id_text());
        if(srfdomain.getCompany_id_textDirtyFlag())
            this.setCompany_id_text(srfdomain.getCompany_id_text());
        if(srfdomain.getCategory_id_textDirtyFlag())
            this.setCategory_id_text(srfdomain.getCategory_id_text());
        if(srfdomain.getCreate_uid_textDirtyFlag())
            this.setCreate_uid_text(srfdomain.getCreate_uid_text());
        if(srfdomain.getPartner_id_textDirtyFlag())
            this.setPartner_id_text(srfdomain.getPartner_id_text());
        if(srfdomain.getWrite_uid_textDirtyFlag())
            this.setWrite_uid_text(srfdomain.getWrite_uid_text());
        if(srfdomain.getOwner_user_id_textDirtyFlag())
            this.setOwner_user_id_text(srfdomain.getOwner_user_id_text());
        if(srfdomain.getMaintenance_team_id_textDirtyFlag())
            this.setMaintenance_team_id_text(srfdomain.getMaintenance_team_id_text());
        if(srfdomain.getCreate_uidDirtyFlag())
            this.setCreate_uid(srfdomain.getCreate_uid());
        if(srfdomain.getTechnician_user_idDirtyFlag())
            this.setTechnician_user_id(srfdomain.getTechnician_user_id());
        if(srfdomain.getPartner_idDirtyFlag())
            this.setPartner_id(srfdomain.getPartner_id());
        if(srfdomain.getCategory_idDirtyFlag())
            this.setCategory_id(srfdomain.getCategory_id());
        if(srfdomain.getEmployee_idDirtyFlag())
            this.setEmployee_id(srfdomain.getEmployee_id());
        if(srfdomain.getMaintenance_team_idDirtyFlag())
            this.setMaintenance_team_id(srfdomain.getMaintenance_team_id());
        if(srfdomain.getCompany_idDirtyFlag())
            this.setCompany_id(srfdomain.getCompany_id());
        if(srfdomain.getOwner_user_idDirtyFlag())
            this.setOwner_user_id(srfdomain.getOwner_user_id());
        if(srfdomain.getDepartment_idDirtyFlag())
            this.setDepartment_id(srfdomain.getDepartment_id());
        if(srfdomain.getWrite_uidDirtyFlag())
            this.setWrite_uid(srfdomain.getWrite_uid());

    }

    public List<Maintenance_equipmentDTO> fromDOPage(List<Maintenance_equipment> poPage)   {
        if(poPage == null)
            return null;
        List<Maintenance_equipmentDTO> dtos=new ArrayList<Maintenance_equipmentDTO>();
        for(Maintenance_equipment domain : poPage) {
            Maintenance_equipmentDTO dto = new Maintenance_equipmentDTO();
            dto.fromDO(domain);
            dtos.add(dto);
        }
        return dtos;
    }
}

