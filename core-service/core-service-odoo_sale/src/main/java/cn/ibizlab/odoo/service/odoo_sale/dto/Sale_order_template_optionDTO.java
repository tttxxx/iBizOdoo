package cn.ibizlab.odoo.service.odoo_sale.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.core.odoo_sale.valuerule.anno.sale_order_template_option.*;
import cn.ibizlab.odoo.core.odoo_sale.domain.Sale_order_template_option;
import org.springframework.cglib.beans.BeanCopier;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 服务DTO对象[Sale_order_template_optionDTO]
 */
public class Sale_order_template_optionDTO implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [QUANTITY]
     *
     */
    @Sale_order_template_optionQuantityDefault(info = "默认规则")
    private Double quantity;

    @JsonIgnore
    private boolean quantityDirtyFlag;

    /**
     * 属性 [CREATE_DATE]
     *
     */
    @Sale_order_template_optionCreate_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp create_date;

    @JsonIgnore
    private boolean create_dateDirtyFlag;

    /**
     * 属性 [NAME]
     *
     */
    @Sale_order_template_optionNameDefault(info = "默认规则")
    private String name;

    @JsonIgnore
    private boolean nameDirtyFlag;

    /**
     * 属性 [WRITE_DATE]
     *
     */
    @Sale_order_template_optionWrite_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp write_date;

    @JsonIgnore
    private boolean write_dateDirtyFlag;

    /**
     * 属性 [ID]
     *
     */
    @Sale_order_template_optionIdDefault(info = "默认规则")
    private Integer id;

    @JsonIgnore
    private boolean idDirtyFlag;

    /**
     * 属性 [PRICE_UNIT]
     *
     */
    @Sale_order_template_optionPrice_unitDefault(info = "默认规则")
    private Double price_unit;

    @JsonIgnore
    private boolean price_unitDirtyFlag;

    /**
     * 属性 [DISCOUNT]
     *
     */
    @Sale_order_template_optionDiscountDefault(info = "默认规则")
    private Double discount;

    @JsonIgnore
    private boolean discountDirtyFlag;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @Sale_order_template_optionDisplay_nameDefault(info = "默认规则")
    private String display_name;

    @JsonIgnore
    private boolean display_nameDirtyFlag;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @Sale_order_template_option__last_updateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp __last_update;

    @JsonIgnore
    private boolean __last_updateDirtyFlag;

    /**
     * 属性 [CREATE_UID_TEXT]
     *
     */
    @Sale_order_template_optionCreate_uid_textDefault(info = "默认规则")
    private String create_uid_text;

    @JsonIgnore
    private boolean create_uid_textDirtyFlag;

    /**
     * 属性 [WRITE_UID_TEXT]
     *
     */
    @Sale_order_template_optionWrite_uid_textDefault(info = "默认规则")
    private String write_uid_text;

    @JsonIgnore
    private boolean write_uid_textDirtyFlag;

    /**
     * 属性 [SALE_ORDER_TEMPLATE_ID_TEXT]
     *
     */
    @Sale_order_template_optionSale_order_template_id_textDefault(info = "默认规则")
    private String sale_order_template_id_text;

    @JsonIgnore
    private boolean sale_order_template_id_textDirtyFlag;

    /**
     * 属性 [PRODUCT_ID_TEXT]
     *
     */
    @Sale_order_template_optionProduct_id_textDefault(info = "默认规则")
    private String product_id_text;

    @JsonIgnore
    private boolean product_id_textDirtyFlag;

    /**
     * 属性 [UOM_ID_TEXT]
     *
     */
    @Sale_order_template_optionUom_id_textDefault(info = "默认规则")
    private String uom_id_text;

    @JsonIgnore
    private boolean uom_id_textDirtyFlag;

    /**
     * 属性 [CREATE_UID]
     *
     */
    @Sale_order_template_optionCreate_uidDefault(info = "默认规则")
    private Integer create_uid;

    @JsonIgnore
    private boolean create_uidDirtyFlag;

    /**
     * 属性 [PRODUCT_ID]
     *
     */
    @Sale_order_template_optionProduct_idDefault(info = "默认规则")
    private Integer product_id;

    @JsonIgnore
    private boolean product_idDirtyFlag;

    /**
     * 属性 [SALE_ORDER_TEMPLATE_ID]
     *
     */
    @Sale_order_template_optionSale_order_template_idDefault(info = "默认规则")
    private Integer sale_order_template_id;

    @JsonIgnore
    private boolean sale_order_template_idDirtyFlag;

    /**
     * 属性 [UOM_ID]
     *
     */
    @Sale_order_template_optionUom_idDefault(info = "默认规则")
    private Integer uom_id;

    @JsonIgnore
    private boolean uom_idDirtyFlag;

    /**
     * 属性 [WRITE_UID]
     *
     */
    @Sale_order_template_optionWrite_uidDefault(info = "默认规则")
    private Integer write_uid;

    @JsonIgnore
    private boolean write_uidDirtyFlag;


    /**
     * 获取 [QUANTITY]
     */
    @JsonProperty("quantity")
    public Double getQuantity(){
        return quantity ;
    }

    /**
     * 设置 [QUANTITY]
     */
    @JsonProperty("quantity")
    public void setQuantity(Double  quantity){
        this.quantity = quantity ;
        this.quantityDirtyFlag = true ;
    }

    /**
     * 获取 [QUANTITY]脏标记
     */
    @JsonIgnore
    public boolean getQuantityDirtyFlag(){
        return quantityDirtyFlag ;
    }

    /**
     * 获取 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return create_date ;
    }

    /**
     * 设置 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return create_dateDirtyFlag ;
    }

    /**
     * 获取 [NAME]
     */
    @JsonProperty("name")
    public String getName(){
        return name ;
    }

    /**
     * 设置 [NAME]
     */
    @JsonProperty("name")
    public void setName(String  name){
        this.name = name ;
        this.nameDirtyFlag = true ;
    }

    /**
     * 获取 [NAME]脏标记
     */
    @JsonIgnore
    public boolean getNameDirtyFlag(){
        return nameDirtyFlag ;
    }

    /**
     * 获取 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return write_date ;
    }

    /**
     * 设置 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return write_dateDirtyFlag ;
    }

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

    /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return idDirtyFlag ;
    }

    /**
     * 获取 [PRICE_UNIT]
     */
    @JsonProperty("price_unit")
    public Double getPrice_unit(){
        return price_unit ;
    }

    /**
     * 设置 [PRICE_UNIT]
     */
    @JsonProperty("price_unit")
    public void setPrice_unit(Double  price_unit){
        this.price_unit = price_unit ;
        this.price_unitDirtyFlag = true ;
    }

    /**
     * 获取 [PRICE_UNIT]脏标记
     */
    @JsonIgnore
    public boolean getPrice_unitDirtyFlag(){
        return price_unitDirtyFlag ;
    }

    /**
     * 获取 [DISCOUNT]
     */
    @JsonProperty("discount")
    public Double getDiscount(){
        return discount ;
    }

    /**
     * 设置 [DISCOUNT]
     */
    @JsonProperty("discount")
    public void setDiscount(Double  discount){
        this.discount = discount ;
        this.discountDirtyFlag = true ;
    }

    /**
     * 获取 [DISCOUNT]脏标记
     */
    @JsonIgnore
    public boolean getDiscountDirtyFlag(){
        return discountDirtyFlag ;
    }

    /**
     * 获取 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return display_name ;
    }

    /**
     * 设置 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

    /**
     * 获取 [DISPLAY_NAME]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return display_nameDirtyFlag ;
    }

    /**
     * 获取 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return __last_update ;
    }

    /**
     * 设置 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

    /**
     * 获取 [__LAST_UPDATE]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return __last_updateDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return create_uid_text ;
    }

    /**
     * 设置 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return create_uid_textDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return write_uid_text ;
    }

    /**
     * 设置 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return write_uid_textDirtyFlag ;
    }

    /**
     * 获取 [SALE_ORDER_TEMPLATE_ID_TEXT]
     */
    @JsonProperty("sale_order_template_id_text")
    public String getSale_order_template_id_text(){
        return sale_order_template_id_text ;
    }

    /**
     * 设置 [SALE_ORDER_TEMPLATE_ID_TEXT]
     */
    @JsonProperty("sale_order_template_id_text")
    public void setSale_order_template_id_text(String  sale_order_template_id_text){
        this.sale_order_template_id_text = sale_order_template_id_text ;
        this.sale_order_template_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [SALE_ORDER_TEMPLATE_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getSale_order_template_id_textDirtyFlag(){
        return sale_order_template_id_textDirtyFlag ;
    }

    /**
     * 获取 [PRODUCT_ID_TEXT]
     */
    @JsonProperty("product_id_text")
    public String getProduct_id_text(){
        return product_id_text ;
    }

    /**
     * 设置 [PRODUCT_ID_TEXT]
     */
    @JsonProperty("product_id_text")
    public void setProduct_id_text(String  product_id_text){
        this.product_id_text = product_id_text ;
        this.product_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [PRODUCT_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getProduct_id_textDirtyFlag(){
        return product_id_textDirtyFlag ;
    }

    /**
     * 获取 [UOM_ID_TEXT]
     */
    @JsonProperty("uom_id_text")
    public String getUom_id_text(){
        return uom_id_text ;
    }

    /**
     * 设置 [UOM_ID_TEXT]
     */
    @JsonProperty("uom_id_text")
    public void setUom_id_text(String  uom_id_text){
        this.uom_id_text = uom_id_text ;
        this.uom_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [UOM_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getUom_id_textDirtyFlag(){
        return uom_id_textDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return create_uid ;
    }

    /**
     * 设置 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return create_uidDirtyFlag ;
    }

    /**
     * 获取 [PRODUCT_ID]
     */
    @JsonProperty("product_id")
    public Integer getProduct_id(){
        return product_id ;
    }

    /**
     * 设置 [PRODUCT_ID]
     */
    @JsonProperty("product_id")
    public void setProduct_id(Integer  product_id){
        this.product_id = product_id ;
        this.product_idDirtyFlag = true ;
    }

    /**
     * 获取 [PRODUCT_ID]脏标记
     */
    @JsonIgnore
    public boolean getProduct_idDirtyFlag(){
        return product_idDirtyFlag ;
    }

    /**
     * 获取 [SALE_ORDER_TEMPLATE_ID]
     */
    @JsonProperty("sale_order_template_id")
    public Integer getSale_order_template_id(){
        return sale_order_template_id ;
    }

    /**
     * 设置 [SALE_ORDER_TEMPLATE_ID]
     */
    @JsonProperty("sale_order_template_id")
    public void setSale_order_template_id(Integer  sale_order_template_id){
        this.sale_order_template_id = sale_order_template_id ;
        this.sale_order_template_idDirtyFlag = true ;
    }

    /**
     * 获取 [SALE_ORDER_TEMPLATE_ID]脏标记
     */
    @JsonIgnore
    public boolean getSale_order_template_idDirtyFlag(){
        return sale_order_template_idDirtyFlag ;
    }

    /**
     * 获取 [UOM_ID]
     */
    @JsonProperty("uom_id")
    public Integer getUom_id(){
        return uom_id ;
    }

    /**
     * 设置 [UOM_ID]
     */
    @JsonProperty("uom_id")
    public void setUom_id(Integer  uom_id){
        this.uom_id = uom_id ;
        this.uom_idDirtyFlag = true ;
    }

    /**
     * 获取 [UOM_ID]脏标记
     */
    @JsonIgnore
    public boolean getUom_idDirtyFlag(){
        return uom_idDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return write_uid ;
    }

    /**
     * 设置 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return write_uidDirtyFlag ;
    }



    public Sale_order_template_option toDO() {
        Sale_order_template_option srfdomain = new Sale_order_template_option();
        if(getQuantityDirtyFlag())
            srfdomain.setQuantity(quantity);
        if(getCreate_dateDirtyFlag())
            srfdomain.setCreate_date(create_date);
        if(getNameDirtyFlag())
            srfdomain.setName(name);
        if(getWrite_dateDirtyFlag())
            srfdomain.setWrite_date(write_date);
        if(getIdDirtyFlag())
            srfdomain.setId(id);
        if(getPrice_unitDirtyFlag())
            srfdomain.setPrice_unit(price_unit);
        if(getDiscountDirtyFlag())
            srfdomain.setDiscount(discount);
        if(getDisplay_nameDirtyFlag())
            srfdomain.setDisplay_name(display_name);
        if(get__last_updateDirtyFlag())
            srfdomain.set__last_update(__last_update);
        if(getCreate_uid_textDirtyFlag())
            srfdomain.setCreate_uid_text(create_uid_text);
        if(getWrite_uid_textDirtyFlag())
            srfdomain.setWrite_uid_text(write_uid_text);
        if(getSale_order_template_id_textDirtyFlag())
            srfdomain.setSale_order_template_id_text(sale_order_template_id_text);
        if(getProduct_id_textDirtyFlag())
            srfdomain.setProduct_id_text(product_id_text);
        if(getUom_id_textDirtyFlag())
            srfdomain.setUom_id_text(uom_id_text);
        if(getCreate_uidDirtyFlag())
            srfdomain.setCreate_uid(create_uid);
        if(getProduct_idDirtyFlag())
            srfdomain.setProduct_id(product_id);
        if(getSale_order_template_idDirtyFlag())
            srfdomain.setSale_order_template_id(sale_order_template_id);
        if(getUom_idDirtyFlag())
            srfdomain.setUom_id(uom_id);
        if(getWrite_uidDirtyFlag())
            srfdomain.setWrite_uid(write_uid);

        return srfdomain;
    }

    public void fromDO(Sale_order_template_option srfdomain) {
        if(srfdomain == null )
          return ;
        if(srfdomain.getQuantityDirtyFlag())
            this.setQuantity(srfdomain.getQuantity());
        if(srfdomain.getCreate_dateDirtyFlag())
            this.setCreate_date(srfdomain.getCreate_date());
        if(srfdomain.getNameDirtyFlag())
            this.setName(srfdomain.getName());
        if(srfdomain.getWrite_dateDirtyFlag())
            this.setWrite_date(srfdomain.getWrite_date());
        if(srfdomain.getIdDirtyFlag())
            this.setId(srfdomain.getId());
        if(srfdomain.getPrice_unitDirtyFlag())
            this.setPrice_unit(srfdomain.getPrice_unit());
        if(srfdomain.getDiscountDirtyFlag())
            this.setDiscount(srfdomain.getDiscount());
        if(srfdomain.getDisplay_nameDirtyFlag())
            this.setDisplay_name(srfdomain.getDisplay_name());
        if(srfdomain.get__last_updateDirtyFlag())
            this.set__last_update(srfdomain.get__last_update());
        if(srfdomain.getCreate_uid_textDirtyFlag())
            this.setCreate_uid_text(srfdomain.getCreate_uid_text());
        if(srfdomain.getWrite_uid_textDirtyFlag())
            this.setWrite_uid_text(srfdomain.getWrite_uid_text());
        if(srfdomain.getSale_order_template_id_textDirtyFlag())
            this.setSale_order_template_id_text(srfdomain.getSale_order_template_id_text());
        if(srfdomain.getProduct_id_textDirtyFlag())
            this.setProduct_id_text(srfdomain.getProduct_id_text());
        if(srfdomain.getUom_id_textDirtyFlag())
            this.setUom_id_text(srfdomain.getUom_id_text());
        if(srfdomain.getCreate_uidDirtyFlag())
            this.setCreate_uid(srfdomain.getCreate_uid());
        if(srfdomain.getProduct_idDirtyFlag())
            this.setProduct_id(srfdomain.getProduct_id());
        if(srfdomain.getSale_order_template_idDirtyFlag())
            this.setSale_order_template_id(srfdomain.getSale_order_template_id());
        if(srfdomain.getUom_idDirtyFlag())
            this.setUom_id(srfdomain.getUom_id());
        if(srfdomain.getWrite_uidDirtyFlag())
            this.setWrite_uid(srfdomain.getWrite_uid());

    }

    public List<Sale_order_template_optionDTO> fromDOPage(List<Sale_order_template_option> poPage)   {
        if(poPage == null)
            return null;
        List<Sale_order_template_optionDTO> dtos=new ArrayList<Sale_order_template_optionDTO>();
        for(Sale_order_template_option domain : poPage) {
            Sale_order_template_optionDTO dto = new Sale_order_template_optionDTO();
            dto.fromDO(domain);
            dtos.add(dto);
        }
        return dtos;
    }
}

