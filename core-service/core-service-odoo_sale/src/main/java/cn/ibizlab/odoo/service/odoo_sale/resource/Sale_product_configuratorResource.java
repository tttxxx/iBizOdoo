package cn.ibizlab.odoo.service.odoo_sale.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_sale.dto.Sale_product_configuratorDTO;
import cn.ibizlab.odoo.core.odoo_sale.domain.Sale_product_configurator;
import cn.ibizlab.odoo.core.odoo_sale.service.ISale_product_configuratorService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_sale.filter.Sale_product_configuratorSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Sale_product_configurator" })
@RestController
@RequestMapping("")
public class Sale_product_configuratorResource {

    @Autowired
    private ISale_product_configuratorService sale_product_configuratorService;

    public ISale_product_configuratorService getSale_product_configuratorService() {
        return this.sale_product_configuratorService;
    }

    @ApiOperation(value = "建立数据", tags = {"Sale_product_configurator" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_sale/sale_product_configurators")

    public ResponseEntity<Sale_product_configuratorDTO> create(@RequestBody Sale_product_configuratorDTO sale_product_configuratordto) {
        Sale_product_configuratorDTO dto = new Sale_product_configuratorDTO();
        Sale_product_configurator domain = sale_product_configuratordto.toDO();
		sale_product_configuratorService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "删除数据", tags = {"Sale_product_configurator" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_sale/sale_product_configurators/{sale_product_configurator_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("sale_product_configurator_id") Integer sale_product_configurator_id) {
        Sale_product_configuratorDTO sale_product_configuratordto = new Sale_product_configuratorDTO();
		Sale_product_configurator domain = new Sale_product_configurator();
		sale_product_configuratordto.setId(sale_product_configurator_id);
		domain.setId(sale_product_configurator_id);
        Boolean rst = sale_product_configuratorService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "获取数据", tags = {"Sale_product_configurator" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_sale/sale_product_configurators/{sale_product_configurator_id}")
    public ResponseEntity<Sale_product_configuratorDTO> get(@PathVariable("sale_product_configurator_id") Integer sale_product_configurator_id) {
        Sale_product_configuratorDTO dto = new Sale_product_configuratorDTO();
        Sale_product_configurator domain = sale_product_configuratorService.get(sale_product_configurator_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "更新数据", tags = {"Sale_product_configurator" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_sale/sale_product_configurators/{sale_product_configurator_id}")

    public ResponseEntity<Sale_product_configuratorDTO> update(@PathVariable("sale_product_configurator_id") Integer sale_product_configurator_id, @RequestBody Sale_product_configuratorDTO sale_product_configuratordto) {
		Sale_product_configurator domain = sale_product_configuratordto.toDO();
        domain.setId(sale_product_configurator_id);
		sale_product_configuratorService.update(domain);
		Sale_product_configuratorDTO dto = new Sale_product_configuratorDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批更新数据", tags = {"Sale_product_configurator" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_sale/sale_product_configurators/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Sale_product_configuratorDTO> sale_product_configuratordtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "批建立数据", tags = {"Sale_product_configurator" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_sale/sale_product_configurators/createBatch")
    public ResponseEntity<Boolean> createBatchSale_product_configurator(@RequestBody List<Sale_product_configuratorDTO> sale_product_configuratordtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "批删除数据", tags = {"Sale_product_configurator" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_sale/sale_product_configurators/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Sale_product_configuratorDTO> sale_product_configuratordtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Sale_product_configurator" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_sale/sale_product_configurators/fetchdefault")
	public ResponseEntity<Page<Sale_product_configuratorDTO>> fetchDefault(Sale_product_configuratorSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Sale_product_configuratorDTO> list = new ArrayList<Sale_product_configuratorDTO>();
        
        Page<Sale_product_configurator> domains = sale_product_configuratorService.searchDefault(context) ;
        for(Sale_product_configurator sale_product_configurator : domains.getContent()){
            Sale_product_configuratorDTO dto = new Sale_product_configuratorDTO();
            dto.fromDO(sale_product_configurator);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
