package cn.ibizlab.odoo.service.odoo_utm.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_utm.dto.Utm_sourceDTO;
import cn.ibizlab.odoo.core.odoo_utm.domain.Utm_source;
import cn.ibizlab.odoo.core.odoo_utm.service.IUtm_sourceService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_utm.filter.Utm_sourceSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Utm_source" })
@RestController
@RequestMapping("")
public class Utm_sourceResource {

    @Autowired
    private IUtm_sourceService utm_sourceService;

    public IUtm_sourceService getUtm_sourceService() {
        return this.utm_sourceService;
    }

    @ApiOperation(value = "批删除数据", tags = {"Utm_source" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_utm/utm_sources/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Utm_sourceDTO> utm_sourcedtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "批建立数据", tags = {"Utm_source" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_utm/utm_sources/createBatch")
    public ResponseEntity<Boolean> createBatchUtm_source(@RequestBody List<Utm_sourceDTO> utm_sourcedtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "批更新数据", tags = {"Utm_source" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_utm/utm_sources/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Utm_sourceDTO> utm_sourcedtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "获取数据", tags = {"Utm_source" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_utm/utm_sources/{utm_source_id}")
    public ResponseEntity<Utm_sourceDTO> get(@PathVariable("utm_source_id") Integer utm_source_id) {
        Utm_sourceDTO dto = new Utm_sourceDTO();
        Utm_source domain = utm_sourceService.get(utm_source_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "建立数据", tags = {"Utm_source" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_utm/utm_sources")

    public ResponseEntity<Utm_sourceDTO> create(@RequestBody Utm_sourceDTO utm_sourcedto) {
        Utm_sourceDTO dto = new Utm_sourceDTO();
        Utm_source domain = utm_sourcedto.toDO();
		utm_sourceService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "删除数据", tags = {"Utm_source" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_utm/utm_sources/{utm_source_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("utm_source_id") Integer utm_source_id) {
        Utm_sourceDTO utm_sourcedto = new Utm_sourceDTO();
		Utm_source domain = new Utm_source();
		utm_sourcedto.setId(utm_source_id);
		domain.setId(utm_source_id);
        Boolean rst = utm_sourceService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "更新数据", tags = {"Utm_source" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_utm/utm_sources/{utm_source_id}")

    public ResponseEntity<Utm_sourceDTO> update(@PathVariable("utm_source_id") Integer utm_source_id, @RequestBody Utm_sourceDTO utm_sourcedto) {
		Utm_source domain = utm_sourcedto.toDO();
        domain.setId(utm_source_id);
		utm_sourceService.update(domain);
		Utm_sourceDTO dto = new Utm_sourceDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Utm_source" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_utm/utm_sources/fetchdefault")
	public ResponseEntity<Page<Utm_sourceDTO>> fetchDefault(Utm_sourceSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Utm_sourceDTO> list = new ArrayList<Utm_sourceDTO>();
        
        Page<Utm_source> domains = utm_sourceService.searchDefault(context) ;
        for(Utm_source utm_source : domains.getContent()){
            Utm_sourceDTO dto = new Utm_sourceDTO();
            dto.fromDO(utm_source);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
