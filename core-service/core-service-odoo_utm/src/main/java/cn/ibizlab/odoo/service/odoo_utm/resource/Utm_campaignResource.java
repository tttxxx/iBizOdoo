package cn.ibizlab.odoo.service.odoo_utm.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_utm.dto.Utm_campaignDTO;
import cn.ibizlab.odoo.core.odoo_utm.domain.Utm_campaign;
import cn.ibizlab.odoo.core.odoo_utm.service.IUtm_campaignService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_utm.filter.Utm_campaignSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Utm_campaign" })
@RestController
@RequestMapping("")
public class Utm_campaignResource {

    @Autowired
    private IUtm_campaignService utm_campaignService;

    public IUtm_campaignService getUtm_campaignService() {
        return this.utm_campaignService;
    }

    @ApiOperation(value = "更新数据", tags = {"Utm_campaign" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_utm/utm_campaigns/{utm_campaign_id}")

    public ResponseEntity<Utm_campaignDTO> update(@PathVariable("utm_campaign_id") Integer utm_campaign_id, @RequestBody Utm_campaignDTO utm_campaigndto) {
		Utm_campaign domain = utm_campaigndto.toDO();
        domain.setId(utm_campaign_id);
		utm_campaignService.update(domain);
		Utm_campaignDTO dto = new Utm_campaignDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取数据", tags = {"Utm_campaign" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_utm/utm_campaigns/{utm_campaign_id}")
    public ResponseEntity<Utm_campaignDTO> get(@PathVariable("utm_campaign_id") Integer utm_campaign_id) {
        Utm_campaignDTO dto = new Utm_campaignDTO();
        Utm_campaign domain = utm_campaignService.get(utm_campaign_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "删除数据", tags = {"Utm_campaign" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_utm/utm_campaigns/{utm_campaign_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("utm_campaign_id") Integer utm_campaign_id) {
        Utm_campaignDTO utm_campaigndto = new Utm_campaignDTO();
		Utm_campaign domain = new Utm_campaign();
		utm_campaigndto.setId(utm_campaign_id);
		domain.setId(utm_campaign_id);
        Boolean rst = utm_campaignService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "建立数据", tags = {"Utm_campaign" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_utm/utm_campaigns")

    public ResponseEntity<Utm_campaignDTO> create(@RequestBody Utm_campaignDTO utm_campaigndto) {
        Utm_campaignDTO dto = new Utm_campaignDTO();
        Utm_campaign domain = utm_campaigndto.toDO();
		utm_campaignService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批删除数据", tags = {"Utm_campaign" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_utm/utm_campaigns/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Utm_campaignDTO> utm_campaigndtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "批建立数据", tags = {"Utm_campaign" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_utm/utm_campaigns/createBatch")
    public ResponseEntity<Boolean> createBatchUtm_campaign(@RequestBody List<Utm_campaignDTO> utm_campaigndtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "批更新数据", tags = {"Utm_campaign" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_utm/utm_campaigns/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Utm_campaignDTO> utm_campaigndtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Utm_campaign" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_utm/utm_campaigns/fetchdefault")
	public ResponseEntity<Page<Utm_campaignDTO>> fetchDefault(Utm_campaignSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Utm_campaignDTO> list = new ArrayList<Utm_campaignDTO>();
        
        Page<Utm_campaign> domains = utm_campaignService.searchDefault(context) ;
        for(Utm_campaign utm_campaign : domains.getContent()){
            Utm_campaignDTO dto = new Utm_campaignDTO();
            dto.fromDO(utm_campaign);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
