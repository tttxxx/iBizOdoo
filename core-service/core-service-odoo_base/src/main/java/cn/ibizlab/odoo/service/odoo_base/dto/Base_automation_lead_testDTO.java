package cn.ibizlab.odoo.service.odoo_base.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.core.odoo_base.valuerule.anno.base_automation_lead_test.*;
import cn.ibizlab.odoo.core.odoo_base.domain.Base_automation_lead_test;
import org.springframework.cglib.beans.BeanCopier;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 服务DTO对象[Base_automation_lead_testDTO]
 */
public class Base_automation_lead_testDTO implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [IS_ASSIGNED_TO_ADMIN]
     *
     */
    @Base_automation_lead_testIs_assigned_to_adminDefault(info = "默认规则")
    private String is_assigned_to_admin;

    @JsonIgnore
    private boolean is_assigned_to_adminDirtyFlag;

    /**
     * 属性 [LINE_IDS]
     *
     */
    @Base_automation_lead_testLine_idsDefault(info = "默认规则")
    private String line_ids;

    @JsonIgnore
    private boolean line_idsDirtyFlag;

    /**
     * 属性 [CREATE_DATE]
     *
     */
    @Base_automation_lead_testCreate_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp create_date;

    @JsonIgnore
    private boolean create_dateDirtyFlag;

    /**
     * 属性 [NAME]
     *
     */
    @Base_automation_lead_testNameDefault(info = "默认规则")
    private String name;

    @JsonIgnore
    private boolean nameDirtyFlag;

    /**
     * 属性 [DATE_ACTION_LAST]
     *
     */
    @Base_automation_lead_testDate_action_lastDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp date_action_last;

    @JsonIgnore
    private boolean date_action_lastDirtyFlag;

    /**
     * 属性 [WRITE_DATE]
     *
     */
    @Base_automation_lead_testWrite_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp write_date;

    @JsonIgnore
    private boolean write_dateDirtyFlag;

    /**
     * 属性 [ACTIVE]
     *
     */
    @Base_automation_lead_testActiveDefault(info = "默认规则")
    private String active;

    @JsonIgnore
    private boolean activeDirtyFlag;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @Base_automation_lead_test__last_updateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp __last_update;

    @JsonIgnore
    private boolean __last_updateDirtyFlag;

    /**
     * 属性 [STATE]
     *
     */
    @Base_automation_lead_testStateDefault(info = "默认规则")
    private String state;

    @JsonIgnore
    private boolean stateDirtyFlag;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @Base_automation_lead_testDisplay_nameDefault(info = "默认规则")
    private String display_name;

    @JsonIgnore
    private boolean display_nameDirtyFlag;

    /**
     * 属性 [DEADLINE]
     *
     */
    @Base_automation_lead_testDeadlineDefault(info = "默认规则")
    private String deadline;

    @JsonIgnore
    private boolean deadlineDirtyFlag;

    /**
     * 属性 [ID]
     *
     */
    @Base_automation_lead_testIdDefault(info = "默认规则")
    private Integer id;

    @JsonIgnore
    private boolean idDirtyFlag;

    /**
     * 属性 [PRIORITY]
     *
     */
    @Base_automation_lead_testPriorityDefault(info = "默认规则")
    private String priority;

    @JsonIgnore
    private boolean priorityDirtyFlag;

    /**
     * 属性 [PARTNER_ID_TEXT]
     *
     */
    @Base_automation_lead_testPartner_id_textDefault(info = "默认规则")
    private String partner_id_text;

    @JsonIgnore
    private boolean partner_id_textDirtyFlag;

    /**
     * 属性 [CREATE_UID_TEXT]
     *
     */
    @Base_automation_lead_testCreate_uid_textDefault(info = "默认规则")
    private String create_uid_text;

    @JsonIgnore
    private boolean create_uid_textDirtyFlag;

    /**
     * 属性 [CUSTOMER]
     *
     */
    @Base_automation_lead_testCustomerDefault(info = "默认规则")
    private String customer;

    @JsonIgnore
    private boolean customerDirtyFlag;

    /**
     * 属性 [WRITE_UID_TEXT]
     *
     */
    @Base_automation_lead_testWrite_uid_textDefault(info = "默认规则")
    private String write_uid_text;

    @JsonIgnore
    private boolean write_uid_textDirtyFlag;

    /**
     * 属性 [USER_ID_TEXT]
     *
     */
    @Base_automation_lead_testUser_id_textDefault(info = "默认规则")
    private String user_id_text;

    @JsonIgnore
    private boolean user_id_textDirtyFlag;

    /**
     * 属性 [CREATE_UID]
     *
     */
    @Base_automation_lead_testCreate_uidDefault(info = "默认规则")
    private Integer create_uid;

    @JsonIgnore
    private boolean create_uidDirtyFlag;

    /**
     * 属性 [PARTNER_ID]
     *
     */
    @Base_automation_lead_testPartner_idDefault(info = "默认规则")
    private Integer partner_id;

    @JsonIgnore
    private boolean partner_idDirtyFlag;

    /**
     * 属性 [WRITE_UID]
     *
     */
    @Base_automation_lead_testWrite_uidDefault(info = "默认规则")
    private Integer write_uid;

    @JsonIgnore
    private boolean write_uidDirtyFlag;

    /**
     * 属性 [USER_ID]
     *
     */
    @Base_automation_lead_testUser_idDefault(info = "默认规则")
    private Integer user_id;

    @JsonIgnore
    private boolean user_idDirtyFlag;


    /**
     * 获取 [IS_ASSIGNED_TO_ADMIN]
     */
    @JsonProperty("is_assigned_to_admin")
    public String getIs_assigned_to_admin(){
        return is_assigned_to_admin ;
    }

    /**
     * 设置 [IS_ASSIGNED_TO_ADMIN]
     */
    @JsonProperty("is_assigned_to_admin")
    public void setIs_assigned_to_admin(String  is_assigned_to_admin){
        this.is_assigned_to_admin = is_assigned_to_admin ;
        this.is_assigned_to_adminDirtyFlag = true ;
    }

    /**
     * 获取 [IS_ASSIGNED_TO_ADMIN]脏标记
     */
    @JsonIgnore
    public boolean getIs_assigned_to_adminDirtyFlag(){
        return is_assigned_to_adminDirtyFlag ;
    }

    /**
     * 获取 [LINE_IDS]
     */
    @JsonProperty("line_ids")
    public String getLine_ids(){
        return line_ids ;
    }

    /**
     * 设置 [LINE_IDS]
     */
    @JsonProperty("line_ids")
    public void setLine_ids(String  line_ids){
        this.line_ids = line_ids ;
        this.line_idsDirtyFlag = true ;
    }

    /**
     * 获取 [LINE_IDS]脏标记
     */
    @JsonIgnore
    public boolean getLine_idsDirtyFlag(){
        return line_idsDirtyFlag ;
    }

    /**
     * 获取 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return create_date ;
    }

    /**
     * 设置 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return create_dateDirtyFlag ;
    }

    /**
     * 获取 [NAME]
     */
    @JsonProperty("name")
    public String getName(){
        return name ;
    }

    /**
     * 设置 [NAME]
     */
    @JsonProperty("name")
    public void setName(String  name){
        this.name = name ;
        this.nameDirtyFlag = true ;
    }

    /**
     * 获取 [NAME]脏标记
     */
    @JsonIgnore
    public boolean getNameDirtyFlag(){
        return nameDirtyFlag ;
    }

    /**
     * 获取 [DATE_ACTION_LAST]
     */
    @JsonProperty("date_action_last")
    public Timestamp getDate_action_last(){
        return date_action_last ;
    }

    /**
     * 设置 [DATE_ACTION_LAST]
     */
    @JsonProperty("date_action_last")
    public void setDate_action_last(Timestamp  date_action_last){
        this.date_action_last = date_action_last ;
        this.date_action_lastDirtyFlag = true ;
    }

    /**
     * 获取 [DATE_ACTION_LAST]脏标记
     */
    @JsonIgnore
    public boolean getDate_action_lastDirtyFlag(){
        return date_action_lastDirtyFlag ;
    }

    /**
     * 获取 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return write_date ;
    }

    /**
     * 设置 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return write_dateDirtyFlag ;
    }

    /**
     * 获取 [ACTIVE]
     */
    @JsonProperty("active")
    public String getActive(){
        return active ;
    }

    /**
     * 设置 [ACTIVE]
     */
    @JsonProperty("active")
    public void setActive(String  active){
        this.active = active ;
        this.activeDirtyFlag = true ;
    }

    /**
     * 获取 [ACTIVE]脏标记
     */
    @JsonIgnore
    public boolean getActiveDirtyFlag(){
        return activeDirtyFlag ;
    }

    /**
     * 获取 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return __last_update ;
    }

    /**
     * 设置 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

    /**
     * 获取 [__LAST_UPDATE]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return __last_updateDirtyFlag ;
    }

    /**
     * 获取 [STATE]
     */
    @JsonProperty("state")
    public String getState(){
        return state ;
    }

    /**
     * 设置 [STATE]
     */
    @JsonProperty("state")
    public void setState(String  state){
        this.state = state ;
        this.stateDirtyFlag = true ;
    }

    /**
     * 获取 [STATE]脏标记
     */
    @JsonIgnore
    public boolean getStateDirtyFlag(){
        return stateDirtyFlag ;
    }

    /**
     * 获取 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return display_name ;
    }

    /**
     * 设置 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

    /**
     * 获取 [DISPLAY_NAME]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return display_nameDirtyFlag ;
    }

    /**
     * 获取 [DEADLINE]
     */
    @JsonProperty("deadline")
    public String getDeadline(){
        return deadline ;
    }

    /**
     * 设置 [DEADLINE]
     */
    @JsonProperty("deadline")
    public void setDeadline(String  deadline){
        this.deadline = deadline ;
        this.deadlineDirtyFlag = true ;
    }

    /**
     * 获取 [DEADLINE]脏标记
     */
    @JsonIgnore
    public boolean getDeadlineDirtyFlag(){
        return deadlineDirtyFlag ;
    }

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

    /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return idDirtyFlag ;
    }

    /**
     * 获取 [PRIORITY]
     */
    @JsonProperty("priority")
    public String getPriority(){
        return priority ;
    }

    /**
     * 设置 [PRIORITY]
     */
    @JsonProperty("priority")
    public void setPriority(String  priority){
        this.priority = priority ;
        this.priorityDirtyFlag = true ;
    }

    /**
     * 获取 [PRIORITY]脏标记
     */
    @JsonIgnore
    public boolean getPriorityDirtyFlag(){
        return priorityDirtyFlag ;
    }

    /**
     * 获取 [PARTNER_ID_TEXT]
     */
    @JsonProperty("partner_id_text")
    public String getPartner_id_text(){
        return partner_id_text ;
    }

    /**
     * 设置 [PARTNER_ID_TEXT]
     */
    @JsonProperty("partner_id_text")
    public void setPartner_id_text(String  partner_id_text){
        this.partner_id_text = partner_id_text ;
        this.partner_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [PARTNER_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getPartner_id_textDirtyFlag(){
        return partner_id_textDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return create_uid_text ;
    }

    /**
     * 设置 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return create_uid_textDirtyFlag ;
    }

    /**
     * 获取 [CUSTOMER]
     */
    @JsonProperty("customer")
    public String getCustomer(){
        return customer ;
    }

    /**
     * 设置 [CUSTOMER]
     */
    @JsonProperty("customer")
    public void setCustomer(String  customer){
        this.customer = customer ;
        this.customerDirtyFlag = true ;
    }

    /**
     * 获取 [CUSTOMER]脏标记
     */
    @JsonIgnore
    public boolean getCustomerDirtyFlag(){
        return customerDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return write_uid_text ;
    }

    /**
     * 设置 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return write_uid_textDirtyFlag ;
    }

    /**
     * 获取 [USER_ID_TEXT]
     */
    @JsonProperty("user_id_text")
    public String getUser_id_text(){
        return user_id_text ;
    }

    /**
     * 设置 [USER_ID_TEXT]
     */
    @JsonProperty("user_id_text")
    public void setUser_id_text(String  user_id_text){
        this.user_id_text = user_id_text ;
        this.user_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [USER_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getUser_id_textDirtyFlag(){
        return user_id_textDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return create_uid ;
    }

    /**
     * 设置 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return create_uidDirtyFlag ;
    }

    /**
     * 获取 [PARTNER_ID]
     */
    @JsonProperty("partner_id")
    public Integer getPartner_id(){
        return partner_id ;
    }

    /**
     * 设置 [PARTNER_ID]
     */
    @JsonProperty("partner_id")
    public void setPartner_id(Integer  partner_id){
        this.partner_id = partner_id ;
        this.partner_idDirtyFlag = true ;
    }

    /**
     * 获取 [PARTNER_ID]脏标记
     */
    @JsonIgnore
    public boolean getPartner_idDirtyFlag(){
        return partner_idDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return write_uid ;
    }

    /**
     * 设置 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return write_uidDirtyFlag ;
    }

    /**
     * 获取 [USER_ID]
     */
    @JsonProperty("user_id")
    public Integer getUser_id(){
        return user_id ;
    }

    /**
     * 设置 [USER_ID]
     */
    @JsonProperty("user_id")
    public void setUser_id(Integer  user_id){
        this.user_id = user_id ;
        this.user_idDirtyFlag = true ;
    }

    /**
     * 获取 [USER_ID]脏标记
     */
    @JsonIgnore
    public boolean getUser_idDirtyFlag(){
        return user_idDirtyFlag ;
    }



    public Base_automation_lead_test toDO() {
        Base_automation_lead_test srfdomain = new Base_automation_lead_test();
        if(getIs_assigned_to_adminDirtyFlag())
            srfdomain.setIs_assigned_to_admin(is_assigned_to_admin);
        if(getLine_idsDirtyFlag())
            srfdomain.setLine_ids(line_ids);
        if(getCreate_dateDirtyFlag())
            srfdomain.setCreate_date(create_date);
        if(getNameDirtyFlag())
            srfdomain.setName(name);
        if(getDate_action_lastDirtyFlag())
            srfdomain.setDate_action_last(date_action_last);
        if(getWrite_dateDirtyFlag())
            srfdomain.setWrite_date(write_date);
        if(getActiveDirtyFlag())
            srfdomain.setActive(active);
        if(get__last_updateDirtyFlag())
            srfdomain.set__last_update(__last_update);
        if(getStateDirtyFlag())
            srfdomain.setState(state);
        if(getDisplay_nameDirtyFlag())
            srfdomain.setDisplay_name(display_name);
        if(getDeadlineDirtyFlag())
            srfdomain.setDeadline(deadline);
        if(getIdDirtyFlag())
            srfdomain.setId(id);
        if(getPriorityDirtyFlag())
            srfdomain.setPriority(priority);
        if(getPartner_id_textDirtyFlag())
            srfdomain.setPartner_id_text(partner_id_text);
        if(getCreate_uid_textDirtyFlag())
            srfdomain.setCreate_uid_text(create_uid_text);
        if(getCustomerDirtyFlag())
            srfdomain.setCustomer(customer);
        if(getWrite_uid_textDirtyFlag())
            srfdomain.setWrite_uid_text(write_uid_text);
        if(getUser_id_textDirtyFlag())
            srfdomain.setUser_id_text(user_id_text);
        if(getCreate_uidDirtyFlag())
            srfdomain.setCreate_uid(create_uid);
        if(getPartner_idDirtyFlag())
            srfdomain.setPartner_id(partner_id);
        if(getWrite_uidDirtyFlag())
            srfdomain.setWrite_uid(write_uid);
        if(getUser_idDirtyFlag())
            srfdomain.setUser_id(user_id);

        return srfdomain;
    }

    public void fromDO(Base_automation_lead_test srfdomain) {
        if(srfdomain == null )
          return ;
        if(srfdomain.getIs_assigned_to_adminDirtyFlag())
            this.setIs_assigned_to_admin(srfdomain.getIs_assigned_to_admin());
        if(srfdomain.getLine_idsDirtyFlag())
            this.setLine_ids(srfdomain.getLine_ids());
        if(srfdomain.getCreate_dateDirtyFlag())
            this.setCreate_date(srfdomain.getCreate_date());
        if(srfdomain.getNameDirtyFlag())
            this.setName(srfdomain.getName());
        if(srfdomain.getDate_action_lastDirtyFlag())
            this.setDate_action_last(srfdomain.getDate_action_last());
        if(srfdomain.getWrite_dateDirtyFlag())
            this.setWrite_date(srfdomain.getWrite_date());
        if(srfdomain.getActiveDirtyFlag())
            this.setActive(srfdomain.getActive());
        if(srfdomain.get__last_updateDirtyFlag())
            this.set__last_update(srfdomain.get__last_update());
        if(srfdomain.getStateDirtyFlag())
            this.setState(srfdomain.getState());
        if(srfdomain.getDisplay_nameDirtyFlag())
            this.setDisplay_name(srfdomain.getDisplay_name());
        if(srfdomain.getDeadlineDirtyFlag())
            this.setDeadline(srfdomain.getDeadline());
        if(srfdomain.getIdDirtyFlag())
            this.setId(srfdomain.getId());
        if(srfdomain.getPriorityDirtyFlag())
            this.setPriority(srfdomain.getPriority());
        if(srfdomain.getPartner_id_textDirtyFlag())
            this.setPartner_id_text(srfdomain.getPartner_id_text());
        if(srfdomain.getCreate_uid_textDirtyFlag())
            this.setCreate_uid_text(srfdomain.getCreate_uid_text());
        if(srfdomain.getCustomerDirtyFlag())
            this.setCustomer(srfdomain.getCustomer());
        if(srfdomain.getWrite_uid_textDirtyFlag())
            this.setWrite_uid_text(srfdomain.getWrite_uid_text());
        if(srfdomain.getUser_id_textDirtyFlag())
            this.setUser_id_text(srfdomain.getUser_id_text());
        if(srfdomain.getCreate_uidDirtyFlag())
            this.setCreate_uid(srfdomain.getCreate_uid());
        if(srfdomain.getPartner_idDirtyFlag())
            this.setPartner_id(srfdomain.getPartner_id());
        if(srfdomain.getWrite_uidDirtyFlag())
            this.setWrite_uid(srfdomain.getWrite_uid());
        if(srfdomain.getUser_idDirtyFlag())
            this.setUser_id(srfdomain.getUser_id());

    }

    public List<Base_automation_lead_testDTO> fromDOPage(List<Base_automation_lead_test> poPage)   {
        if(poPage == null)
            return null;
        List<Base_automation_lead_testDTO> dtos=new ArrayList<Base_automation_lead_testDTO>();
        for(Base_automation_lead_test domain : poPage) {
            Base_automation_lead_testDTO dto = new Base_automation_lead_testDTO();
            dto.fromDO(domain);
            dtos.add(dto);
        }
        return dtos;
    }
}

