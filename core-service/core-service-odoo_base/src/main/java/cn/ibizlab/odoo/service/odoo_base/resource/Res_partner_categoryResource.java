package cn.ibizlab.odoo.service.odoo_base.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_base.dto.Res_partner_categoryDTO;
import cn.ibizlab.odoo.core.odoo_base.domain.Res_partner_category;
import cn.ibizlab.odoo.core.odoo_base.service.IRes_partner_categoryService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_base.filter.Res_partner_categorySearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Res_partner_category" })
@RestController
@RequestMapping("")
public class Res_partner_categoryResource {

    @Autowired
    private IRes_partner_categoryService res_partner_categoryService;

    public IRes_partner_categoryService getRes_partner_categoryService() {
        return this.res_partner_categoryService;
    }

    @ApiOperation(value = "获取数据", tags = {"Res_partner_category" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_base/res_partner_categories/{res_partner_category_id}")
    public ResponseEntity<Res_partner_categoryDTO> get(@PathVariable("res_partner_category_id") Integer res_partner_category_id) {
        Res_partner_categoryDTO dto = new Res_partner_categoryDTO();
        Res_partner_category domain = res_partner_categoryService.get(res_partner_category_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批删除数据", tags = {"Res_partner_category" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_base/res_partner_categories/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Res_partner_categoryDTO> res_partner_categorydtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "删除数据", tags = {"Res_partner_category" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_base/res_partner_categories/{res_partner_category_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("res_partner_category_id") Integer res_partner_category_id) {
        Res_partner_categoryDTO res_partner_categorydto = new Res_partner_categoryDTO();
		Res_partner_category domain = new Res_partner_category();
		res_partner_categorydto.setId(res_partner_category_id);
		domain.setId(res_partner_category_id);
        Boolean rst = res_partner_categoryService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "建立数据", tags = {"Res_partner_category" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_base/res_partner_categories")

    public ResponseEntity<Res_partner_categoryDTO> create(@RequestBody Res_partner_categoryDTO res_partner_categorydto) {
        Res_partner_categoryDTO dto = new Res_partner_categoryDTO();
        Res_partner_category domain = res_partner_categorydto.toDO();
		res_partner_categoryService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "更新数据", tags = {"Res_partner_category" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_base/res_partner_categories/{res_partner_category_id}")

    public ResponseEntity<Res_partner_categoryDTO> update(@PathVariable("res_partner_category_id") Integer res_partner_category_id, @RequestBody Res_partner_categoryDTO res_partner_categorydto) {
		Res_partner_category domain = res_partner_categorydto.toDO();
        domain.setId(res_partner_category_id);
		res_partner_categoryService.update(domain);
		Res_partner_categoryDTO dto = new Res_partner_categoryDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批更新数据", tags = {"Res_partner_category" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_base/res_partner_categories/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Res_partner_categoryDTO> res_partner_categorydtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "批建立数据", tags = {"Res_partner_category" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_base/res_partner_categories/createBatch")
    public ResponseEntity<Boolean> createBatchRes_partner_category(@RequestBody List<Res_partner_categoryDTO> res_partner_categorydtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Res_partner_category" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_base/res_partner_categories/fetchdefault")
	public ResponseEntity<Page<Res_partner_categoryDTO>> fetchDefault(Res_partner_categorySearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Res_partner_categoryDTO> list = new ArrayList<Res_partner_categoryDTO>();
        
        Page<Res_partner_category> domains = res_partner_categoryService.searchDefault(context) ;
        for(Res_partner_category res_partner_category : domains.getContent()){
            Res_partner_categoryDTO dto = new Res_partner_categoryDTO();
            dto.fromDO(res_partner_category);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
