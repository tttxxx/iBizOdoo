package cn.ibizlab.odoo.service.odoo_base.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_base.dto.Base_language_importDTO;
import cn.ibizlab.odoo.core.odoo_base.domain.Base_language_import;
import cn.ibizlab.odoo.core.odoo_base.service.IBase_language_importService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_base.filter.Base_language_importSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Base_language_import" })
@RestController
@RequestMapping("")
public class Base_language_importResource {

    @Autowired
    private IBase_language_importService base_language_importService;

    public IBase_language_importService getBase_language_importService() {
        return this.base_language_importService;
    }

    @ApiOperation(value = "建立数据", tags = {"Base_language_import" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_base/base_language_imports")

    public ResponseEntity<Base_language_importDTO> create(@RequestBody Base_language_importDTO base_language_importdto) {
        Base_language_importDTO dto = new Base_language_importDTO();
        Base_language_import domain = base_language_importdto.toDO();
		base_language_importService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批更新数据", tags = {"Base_language_import" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_base/base_language_imports/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Base_language_importDTO> base_language_importdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "删除数据", tags = {"Base_language_import" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_base/base_language_imports/{base_language_import_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("base_language_import_id") Integer base_language_import_id) {
        Base_language_importDTO base_language_importdto = new Base_language_importDTO();
		Base_language_import domain = new Base_language_import();
		base_language_importdto.setId(base_language_import_id);
		domain.setId(base_language_import_id);
        Boolean rst = base_language_importService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "更新数据", tags = {"Base_language_import" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_base/base_language_imports/{base_language_import_id}")

    public ResponseEntity<Base_language_importDTO> update(@PathVariable("base_language_import_id") Integer base_language_import_id, @RequestBody Base_language_importDTO base_language_importdto) {
		Base_language_import domain = base_language_importdto.toDO();
        domain.setId(base_language_import_id);
		base_language_importService.update(domain);
		Base_language_importDTO dto = new Base_language_importDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批建立数据", tags = {"Base_language_import" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_base/base_language_imports/createBatch")
    public ResponseEntity<Boolean> createBatchBase_language_import(@RequestBody List<Base_language_importDTO> base_language_importdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "获取数据", tags = {"Base_language_import" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_base/base_language_imports/{base_language_import_id}")
    public ResponseEntity<Base_language_importDTO> get(@PathVariable("base_language_import_id") Integer base_language_import_id) {
        Base_language_importDTO dto = new Base_language_importDTO();
        Base_language_import domain = base_language_importService.get(base_language_import_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批删除数据", tags = {"Base_language_import" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_base/base_language_imports/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Base_language_importDTO> base_language_importdtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Base_language_import" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_base/base_language_imports/fetchdefault")
	public ResponseEntity<Page<Base_language_importDTO>> fetchDefault(Base_language_importSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Base_language_importDTO> list = new ArrayList<Base_language_importDTO>();
        
        Page<Base_language_import> domains = base_language_importService.searchDefault(context) ;
        for(Base_language_import base_language_import : domains.getContent()){
            Base_language_importDTO dto = new Base_language_importDTO();
            dto.fromDO(base_language_import);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
