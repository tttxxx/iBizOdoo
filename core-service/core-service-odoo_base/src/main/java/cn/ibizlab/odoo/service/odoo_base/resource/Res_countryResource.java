package cn.ibizlab.odoo.service.odoo_base.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_base.dto.Res_countryDTO;
import cn.ibizlab.odoo.core.odoo_base.domain.Res_country;
import cn.ibizlab.odoo.core.odoo_base.service.IRes_countryService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_base.filter.Res_countrySearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Res_country" })
@RestController
@RequestMapping("")
public class Res_countryResource {

    @Autowired
    private IRes_countryService res_countryService;

    public IRes_countryService getRes_countryService() {
        return this.res_countryService;
    }

    @ApiOperation(value = "更新数据", tags = {"Res_country" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_base/res_countries/{res_country_id}")

    public ResponseEntity<Res_countryDTO> update(@PathVariable("res_country_id") Integer res_country_id, @RequestBody Res_countryDTO res_countrydto) {
		Res_country domain = res_countrydto.toDO();
        domain.setId(res_country_id);
		res_countryService.update(domain);
		Res_countryDTO dto = new Res_countryDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取数据", tags = {"Res_country" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_base/res_countries/{res_country_id}")
    public ResponseEntity<Res_countryDTO> get(@PathVariable("res_country_id") Integer res_country_id) {
        Res_countryDTO dto = new Res_countryDTO();
        Res_country domain = res_countryService.get(res_country_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "建立数据", tags = {"Res_country" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_base/res_countries")

    public ResponseEntity<Res_countryDTO> create(@RequestBody Res_countryDTO res_countrydto) {
        Res_countryDTO dto = new Res_countryDTO();
        Res_country domain = res_countrydto.toDO();
		res_countryService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批更新数据", tags = {"Res_country" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_base/res_countries/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Res_countryDTO> res_countrydtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "删除数据", tags = {"Res_country" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_base/res_countries/{res_country_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("res_country_id") Integer res_country_id) {
        Res_countryDTO res_countrydto = new Res_countryDTO();
		Res_country domain = new Res_country();
		res_countrydto.setId(res_country_id);
		domain.setId(res_country_id);
        Boolean rst = res_countryService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "批建立数据", tags = {"Res_country" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_base/res_countries/createBatch")
    public ResponseEntity<Boolean> createBatchRes_country(@RequestBody List<Res_countryDTO> res_countrydtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "批删除数据", tags = {"Res_country" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_base/res_countries/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Res_countryDTO> res_countrydtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Res_country" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_base/res_countries/fetchdefault")
	public ResponseEntity<Page<Res_countryDTO>> fetchDefault(Res_countrySearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Res_countryDTO> list = new ArrayList<Res_countryDTO>();
        
        Page<Res_country> domains = res_countryService.searchDefault(context) ;
        for(Res_country res_country : domains.getContent()){
            Res_countryDTO dto = new Res_countryDTO();
            dto.fromDO(res_country);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
