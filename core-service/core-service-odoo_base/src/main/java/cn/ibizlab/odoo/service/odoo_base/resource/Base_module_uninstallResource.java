package cn.ibizlab.odoo.service.odoo_base.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_base.dto.Base_module_uninstallDTO;
import cn.ibizlab.odoo.core.odoo_base.domain.Base_module_uninstall;
import cn.ibizlab.odoo.core.odoo_base.service.IBase_module_uninstallService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_base.filter.Base_module_uninstallSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Base_module_uninstall" })
@RestController
@RequestMapping("")
public class Base_module_uninstallResource {

    @Autowired
    private IBase_module_uninstallService base_module_uninstallService;

    public IBase_module_uninstallService getBase_module_uninstallService() {
        return this.base_module_uninstallService;
    }

    @ApiOperation(value = "更新数据", tags = {"Base_module_uninstall" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_base/base_module_uninstalls/{base_module_uninstall_id}")

    public ResponseEntity<Base_module_uninstallDTO> update(@PathVariable("base_module_uninstall_id") Integer base_module_uninstall_id, @RequestBody Base_module_uninstallDTO base_module_uninstalldto) {
		Base_module_uninstall domain = base_module_uninstalldto.toDO();
        domain.setId(base_module_uninstall_id);
		base_module_uninstallService.update(domain);
		Base_module_uninstallDTO dto = new Base_module_uninstallDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取数据", tags = {"Base_module_uninstall" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_base/base_module_uninstalls/{base_module_uninstall_id}")
    public ResponseEntity<Base_module_uninstallDTO> get(@PathVariable("base_module_uninstall_id") Integer base_module_uninstall_id) {
        Base_module_uninstallDTO dto = new Base_module_uninstallDTO();
        Base_module_uninstall domain = base_module_uninstallService.get(base_module_uninstall_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批删除数据", tags = {"Base_module_uninstall" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_base/base_module_uninstalls/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Base_module_uninstallDTO> base_module_uninstalldtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "删除数据", tags = {"Base_module_uninstall" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_base/base_module_uninstalls/{base_module_uninstall_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("base_module_uninstall_id") Integer base_module_uninstall_id) {
        Base_module_uninstallDTO base_module_uninstalldto = new Base_module_uninstallDTO();
		Base_module_uninstall domain = new Base_module_uninstall();
		base_module_uninstalldto.setId(base_module_uninstall_id);
		domain.setId(base_module_uninstall_id);
        Boolean rst = base_module_uninstallService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "批更新数据", tags = {"Base_module_uninstall" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_base/base_module_uninstalls/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Base_module_uninstallDTO> base_module_uninstalldtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "批建立数据", tags = {"Base_module_uninstall" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_base/base_module_uninstalls/createBatch")
    public ResponseEntity<Boolean> createBatchBase_module_uninstall(@RequestBody List<Base_module_uninstallDTO> base_module_uninstalldtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "建立数据", tags = {"Base_module_uninstall" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_base/base_module_uninstalls")

    public ResponseEntity<Base_module_uninstallDTO> create(@RequestBody Base_module_uninstallDTO base_module_uninstalldto) {
        Base_module_uninstallDTO dto = new Base_module_uninstallDTO();
        Base_module_uninstall domain = base_module_uninstalldto.toDO();
		base_module_uninstallService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Base_module_uninstall" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_base/base_module_uninstalls/fetchdefault")
	public ResponseEntity<Page<Base_module_uninstallDTO>> fetchDefault(Base_module_uninstallSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Base_module_uninstallDTO> list = new ArrayList<Base_module_uninstallDTO>();
        
        Page<Base_module_uninstall> domains = base_module_uninstallService.searchDefault(context) ;
        for(Base_module_uninstall base_module_uninstall : domains.getContent()){
            Base_module_uninstallDTO dto = new Base_module_uninstallDTO();
            dto.fromDO(base_module_uninstall);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
