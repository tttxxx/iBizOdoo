package cn.ibizlab.odoo.service.odoo_base.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_base.dto.Base_automation_line_testDTO;
import cn.ibizlab.odoo.core.odoo_base.domain.Base_automation_line_test;
import cn.ibizlab.odoo.core.odoo_base.service.IBase_automation_line_testService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_base.filter.Base_automation_line_testSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Base_automation_line_test" })
@RestController
@RequestMapping("")
public class Base_automation_line_testResource {

    @Autowired
    private IBase_automation_line_testService base_automation_line_testService;

    public IBase_automation_line_testService getBase_automation_line_testService() {
        return this.base_automation_line_testService;
    }

    @ApiOperation(value = "删除数据", tags = {"Base_automation_line_test" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_base/base_automation_line_tests/{base_automation_line_test_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("base_automation_line_test_id") Integer base_automation_line_test_id) {
        Base_automation_line_testDTO base_automation_line_testdto = new Base_automation_line_testDTO();
		Base_automation_line_test domain = new Base_automation_line_test();
		base_automation_line_testdto.setId(base_automation_line_test_id);
		domain.setId(base_automation_line_test_id);
        Boolean rst = base_automation_line_testService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "更新数据", tags = {"Base_automation_line_test" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_base/base_automation_line_tests/{base_automation_line_test_id}")

    public ResponseEntity<Base_automation_line_testDTO> update(@PathVariable("base_automation_line_test_id") Integer base_automation_line_test_id, @RequestBody Base_automation_line_testDTO base_automation_line_testdto) {
		Base_automation_line_test domain = base_automation_line_testdto.toDO();
        domain.setId(base_automation_line_test_id);
		base_automation_line_testService.update(domain);
		Base_automation_line_testDTO dto = new Base_automation_line_testDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批删除数据", tags = {"Base_automation_line_test" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_base/base_automation_line_tests/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Base_automation_line_testDTO> base_automation_line_testdtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "批建立数据", tags = {"Base_automation_line_test" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_base/base_automation_line_tests/createBatch")
    public ResponseEntity<Boolean> createBatchBase_automation_line_test(@RequestBody List<Base_automation_line_testDTO> base_automation_line_testdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "获取数据", tags = {"Base_automation_line_test" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_base/base_automation_line_tests/{base_automation_line_test_id}")
    public ResponseEntity<Base_automation_line_testDTO> get(@PathVariable("base_automation_line_test_id") Integer base_automation_line_test_id) {
        Base_automation_line_testDTO dto = new Base_automation_line_testDTO();
        Base_automation_line_test domain = base_automation_line_testService.get(base_automation_line_test_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批更新数据", tags = {"Base_automation_line_test" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_base/base_automation_line_tests/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Base_automation_line_testDTO> base_automation_line_testdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "建立数据", tags = {"Base_automation_line_test" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_base/base_automation_line_tests")

    public ResponseEntity<Base_automation_line_testDTO> create(@RequestBody Base_automation_line_testDTO base_automation_line_testdto) {
        Base_automation_line_testDTO dto = new Base_automation_line_testDTO();
        Base_automation_line_test domain = base_automation_line_testdto.toDO();
		base_automation_line_testService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Base_automation_line_test" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_base/base_automation_line_tests/fetchdefault")
	public ResponseEntity<Page<Base_automation_line_testDTO>> fetchDefault(Base_automation_line_testSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Base_automation_line_testDTO> list = new ArrayList<Base_automation_line_testDTO>();
        
        Page<Base_automation_line_test> domains = base_automation_line_testService.searchDefault(context) ;
        for(Base_automation_line_test base_automation_line_test : domains.getContent()){
            Base_automation_line_testDTO dto = new Base_automation_line_testDTO();
            dto.fromDO(base_automation_line_test);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
