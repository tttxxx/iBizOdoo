package cn.ibizlab.odoo.service.odoo_base.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.core.odoo_base.valuerule.anno.res_partner_bank.*;
import cn.ibizlab.odoo.core.odoo_base.domain.Res_partner_bank;
import org.springframework.cglib.beans.BeanCopier;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 服务DTO对象[Res_partner_bankDTO]
 */
public class Res_partner_bankDTO implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [SANITIZED_ACC_NUMBER]
     *
     */
    @Res_partner_bankSanitized_acc_numberDefault(info = "默认规则")
    private String sanitized_acc_number;

    @JsonIgnore
    private boolean sanitized_acc_numberDirtyFlag;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @Res_partner_bankDisplay_nameDefault(info = "默认规则")
    private String display_name;

    @JsonIgnore
    private boolean display_nameDirtyFlag;

    /**
     * 属性 [SEQUENCE]
     *
     */
    @Res_partner_bankSequenceDefault(info = "默认规则")
    private Integer sequence;

    @JsonIgnore
    private boolean sequenceDirtyFlag;

    /**
     * 属性 [JOURNAL_ID]
     *
     */
    @Res_partner_bankJournal_idDefault(info = "默认规则")
    private String journal_id;

    @JsonIgnore
    private boolean journal_idDirtyFlag;

    /**
     * 属性 [ID]
     *
     */
    @Res_partner_bankIdDefault(info = "默认规则")
    private Integer id;

    @JsonIgnore
    private boolean idDirtyFlag;

    /**
     * 属性 [CREATE_DATE]
     *
     */
    @Res_partner_bankCreate_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp create_date;

    @JsonIgnore
    private boolean create_dateDirtyFlag;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @Res_partner_bank__last_updateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp __last_update;

    @JsonIgnore
    private boolean __last_updateDirtyFlag;

    /**
     * 属性 [ACC_NUMBER]
     *
     */
    @Res_partner_bankAcc_numberDefault(info = "默认规则")
    private String acc_number;

    @JsonIgnore
    private boolean acc_numberDirtyFlag;

    /**
     * 属性 [ACC_TYPE]
     *
     */
    @Res_partner_bankAcc_typeDefault(info = "默认规则")
    private String acc_type;

    @JsonIgnore
    private boolean acc_typeDirtyFlag;

    /**
     * 属性 [QR_CODE_VALID]
     *
     */
    @Res_partner_bankQr_code_validDefault(info = "默认规则")
    private String qr_code_valid;

    @JsonIgnore
    private boolean qr_code_validDirtyFlag;

    /**
     * 属性 [ACC_HOLDER_NAME]
     *
     */
    @Res_partner_bankAcc_holder_nameDefault(info = "默认规则")
    private String acc_holder_name;

    @JsonIgnore
    private boolean acc_holder_nameDirtyFlag;

    /**
     * 属性 [WRITE_DATE]
     *
     */
    @Res_partner_bankWrite_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp write_date;

    @JsonIgnore
    private boolean write_dateDirtyFlag;

    /**
     * 属性 [BANK_NAME]
     *
     */
    @Res_partner_bankBank_nameDefault(info = "默认规则")
    private String bank_name;

    @JsonIgnore
    private boolean bank_nameDirtyFlag;

    /**
     * 属性 [CREATE_UID_TEXT]
     *
     */
    @Res_partner_bankCreate_uid_textDefault(info = "默认规则")
    private String create_uid_text;

    @JsonIgnore
    private boolean create_uid_textDirtyFlag;

    /**
     * 属性 [PARTNER_ID_TEXT]
     *
     */
    @Res_partner_bankPartner_id_textDefault(info = "默认规则")
    private String partner_id_text;

    @JsonIgnore
    private boolean partner_id_textDirtyFlag;

    /**
     * 属性 [CURRENCY_ID_TEXT]
     *
     */
    @Res_partner_bankCurrency_id_textDefault(info = "默认规则")
    private String currency_id_text;

    @JsonIgnore
    private boolean currency_id_textDirtyFlag;

    /**
     * 属性 [BANK_BIC]
     *
     */
    @Res_partner_bankBank_bicDefault(info = "默认规则")
    private String bank_bic;

    @JsonIgnore
    private boolean bank_bicDirtyFlag;

    /**
     * 属性 [WRITE_UID_TEXT]
     *
     */
    @Res_partner_bankWrite_uid_textDefault(info = "默认规则")
    private String write_uid_text;

    @JsonIgnore
    private boolean write_uid_textDirtyFlag;

    /**
     * 属性 [COMPANY_ID_TEXT]
     *
     */
    @Res_partner_bankCompany_id_textDefault(info = "默认规则")
    private String company_id_text;

    @JsonIgnore
    private boolean company_id_textDirtyFlag;

    /**
     * 属性 [WRITE_UID]
     *
     */
    @Res_partner_bankWrite_uidDefault(info = "默认规则")
    private Integer write_uid;

    @JsonIgnore
    private boolean write_uidDirtyFlag;

    /**
     * 属性 [BANK_ID]
     *
     */
    @Res_partner_bankBank_idDefault(info = "默认规则")
    private Integer bank_id;

    @JsonIgnore
    private boolean bank_idDirtyFlag;

    /**
     * 属性 [COMPANY_ID]
     *
     */
    @Res_partner_bankCompany_idDefault(info = "默认规则")
    private Integer company_id;

    @JsonIgnore
    private boolean company_idDirtyFlag;

    /**
     * 属性 [PARTNER_ID]
     *
     */
    @Res_partner_bankPartner_idDefault(info = "默认规则")
    private Integer partner_id;

    @JsonIgnore
    private boolean partner_idDirtyFlag;

    /**
     * 属性 [CREATE_UID]
     *
     */
    @Res_partner_bankCreate_uidDefault(info = "默认规则")
    private Integer create_uid;

    @JsonIgnore
    private boolean create_uidDirtyFlag;

    /**
     * 属性 [CURRENCY_ID]
     *
     */
    @Res_partner_bankCurrency_idDefault(info = "默认规则")
    private Integer currency_id;

    @JsonIgnore
    private boolean currency_idDirtyFlag;


    /**
     * 获取 [SANITIZED_ACC_NUMBER]
     */
    @JsonProperty("sanitized_acc_number")
    public String getSanitized_acc_number(){
        return sanitized_acc_number ;
    }

    /**
     * 设置 [SANITIZED_ACC_NUMBER]
     */
    @JsonProperty("sanitized_acc_number")
    public void setSanitized_acc_number(String  sanitized_acc_number){
        this.sanitized_acc_number = sanitized_acc_number ;
        this.sanitized_acc_numberDirtyFlag = true ;
    }

    /**
     * 获取 [SANITIZED_ACC_NUMBER]脏标记
     */
    @JsonIgnore
    public boolean getSanitized_acc_numberDirtyFlag(){
        return sanitized_acc_numberDirtyFlag ;
    }

    /**
     * 获取 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return display_name ;
    }

    /**
     * 设置 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

    /**
     * 获取 [DISPLAY_NAME]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return display_nameDirtyFlag ;
    }

    /**
     * 获取 [SEQUENCE]
     */
    @JsonProperty("sequence")
    public Integer getSequence(){
        return sequence ;
    }

    /**
     * 设置 [SEQUENCE]
     */
    @JsonProperty("sequence")
    public void setSequence(Integer  sequence){
        this.sequence = sequence ;
        this.sequenceDirtyFlag = true ;
    }

    /**
     * 获取 [SEQUENCE]脏标记
     */
    @JsonIgnore
    public boolean getSequenceDirtyFlag(){
        return sequenceDirtyFlag ;
    }

    /**
     * 获取 [JOURNAL_ID]
     */
    @JsonProperty("journal_id")
    public String getJournal_id(){
        return journal_id ;
    }

    /**
     * 设置 [JOURNAL_ID]
     */
    @JsonProperty("journal_id")
    public void setJournal_id(String  journal_id){
        this.journal_id = journal_id ;
        this.journal_idDirtyFlag = true ;
    }

    /**
     * 获取 [JOURNAL_ID]脏标记
     */
    @JsonIgnore
    public boolean getJournal_idDirtyFlag(){
        return journal_idDirtyFlag ;
    }

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

    /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return idDirtyFlag ;
    }

    /**
     * 获取 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return create_date ;
    }

    /**
     * 设置 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return create_dateDirtyFlag ;
    }

    /**
     * 获取 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return __last_update ;
    }

    /**
     * 设置 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

    /**
     * 获取 [__LAST_UPDATE]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return __last_updateDirtyFlag ;
    }

    /**
     * 获取 [ACC_NUMBER]
     */
    @JsonProperty("acc_number")
    public String getAcc_number(){
        return acc_number ;
    }

    /**
     * 设置 [ACC_NUMBER]
     */
    @JsonProperty("acc_number")
    public void setAcc_number(String  acc_number){
        this.acc_number = acc_number ;
        this.acc_numberDirtyFlag = true ;
    }

    /**
     * 获取 [ACC_NUMBER]脏标记
     */
    @JsonIgnore
    public boolean getAcc_numberDirtyFlag(){
        return acc_numberDirtyFlag ;
    }

    /**
     * 获取 [ACC_TYPE]
     */
    @JsonProperty("acc_type")
    public String getAcc_type(){
        return acc_type ;
    }

    /**
     * 设置 [ACC_TYPE]
     */
    @JsonProperty("acc_type")
    public void setAcc_type(String  acc_type){
        this.acc_type = acc_type ;
        this.acc_typeDirtyFlag = true ;
    }

    /**
     * 获取 [ACC_TYPE]脏标记
     */
    @JsonIgnore
    public boolean getAcc_typeDirtyFlag(){
        return acc_typeDirtyFlag ;
    }

    /**
     * 获取 [QR_CODE_VALID]
     */
    @JsonProperty("qr_code_valid")
    public String getQr_code_valid(){
        return qr_code_valid ;
    }

    /**
     * 设置 [QR_CODE_VALID]
     */
    @JsonProperty("qr_code_valid")
    public void setQr_code_valid(String  qr_code_valid){
        this.qr_code_valid = qr_code_valid ;
        this.qr_code_validDirtyFlag = true ;
    }

    /**
     * 获取 [QR_CODE_VALID]脏标记
     */
    @JsonIgnore
    public boolean getQr_code_validDirtyFlag(){
        return qr_code_validDirtyFlag ;
    }

    /**
     * 获取 [ACC_HOLDER_NAME]
     */
    @JsonProperty("acc_holder_name")
    public String getAcc_holder_name(){
        return acc_holder_name ;
    }

    /**
     * 设置 [ACC_HOLDER_NAME]
     */
    @JsonProperty("acc_holder_name")
    public void setAcc_holder_name(String  acc_holder_name){
        this.acc_holder_name = acc_holder_name ;
        this.acc_holder_nameDirtyFlag = true ;
    }

    /**
     * 获取 [ACC_HOLDER_NAME]脏标记
     */
    @JsonIgnore
    public boolean getAcc_holder_nameDirtyFlag(){
        return acc_holder_nameDirtyFlag ;
    }

    /**
     * 获取 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return write_date ;
    }

    /**
     * 设置 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return write_dateDirtyFlag ;
    }

    /**
     * 获取 [BANK_NAME]
     */
    @JsonProperty("bank_name")
    public String getBank_name(){
        return bank_name ;
    }

    /**
     * 设置 [BANK_NAME]
     */
    @JsonProperty("bank_name")
    public void setBank_name(String  bank_name){
        this.bank_name = bank_name ;
        this.bank_nameDirtyFlag = true ;
    }

    /**
     * 获取 [BANK_NAME]脏标记
     */
    @JsonIgnore
    public boolean getBank_nameDirtyFlag(){
        return bank_nameDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return create_uid_text ;
    }

    /**
     * 设置 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return create_uid_textDirtyFlag ;
    }

    /**
     * 获取 [PARTNER_ID_TEXT]
     */
    @JsonProperty("partner_id_text")
    public String getPartner_id_text(){
        return partner_id_text ;
    }

    /**
     * 设置 [PARTNER_ID_TEXT]
     */
    @JsonProperty("partner_id_text")
    public void setPartner_id_text(String  partner_id_text){
        this.partner_id_text = partner_id_text ;
        this.partner_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [PARTNER_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getPartner_id_textDirtyFlag(){
        return partner_id_textDirtyFlag ;
    }

    /**
     * 获取 [CURRENCY_ID_TEXT]
     */
    @JsonProperty("currency_id_text")
    public String getCurrency_id_text(){
        return currency_id_text ;
    }

    /**
     * 设置 [CURRENCY_ID_TEXT]
     */
    @JsonProperty("currency_id_text")
    public void setCurrency_id_text(String  currency_id_text){
        this.currency_id_text = currency_id_text ;
        this.currency_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [CURRENCY_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getCurrency_id_textDirtyFlag(){
        return currency_id_textDirtyFlag ;
    }

    /**
     * 获取 [BANK_BIC]
     */
    @JsonProperty("bank_bic")
    public String getBank_bic(){
        return bank_bic ;
    }

    /**
     * 设置 [BANK_BIC]
     */
    @JsonProperty("bank_bic")
    public void setBank_bic(String  bank_bic){
        this.bank_bic = bank_bic ;
        this.bank_bicDirtyFlag = true ;
    }

    /**
     * 获取 [BANK_BIC]脏标记
     */
    @JsonIgnore
    public boolean getBank_bicDirtyFlag(){
        return bank_bicDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return write_uid_text ;
    }

    /**
     * 设置 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return write_uid_textDirtyFlag ;
    }

    /**
     * 获取 [COMPANY_ID_TEXT]
     */
    @JsonProperty("company_id_text")
    public String getCompany_id_text(){
        return company_id_text ;
    }

    /**
     * 设置 [COMPANY_ID_TEXT]
     */
    @JsonProperty("company_id_text")
    public void setCompany_id_text(String  company_id_text){
        this.company_id_text = company_id_text ;
        this.company_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [COMPANY_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getCompany_id_textDirtyFlag(){
        return company_id_textDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return write_uid ;
    }

    /**
     * 设置 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return write_uidDirtyFlag ;
    }

    /**
     * 获取 [BANK_ID]
     */
    @JsonProperty("bank_id")
    public Integer getBank_id(){
        return bank_id ;
    }

    /**
     * 设置 [BANK_ID]
     */
    @JsonProperty("bank_id")
    public void setBank_id(Integer  bank_id){
        this.bank_id = bank_id ;
        this.bank_idDirtyFlag = true ;
    }

    /**
     * 获取 [BANK_ID]脏标记
     */
    @JsonIgnore
    public boolean getBank_idDirtyFlag(){
        return bank_idDirtyFlag ;
    }

    /**
     * 获取 [COMPANY_ID]
     */
    @JsonProperty("company_id")
    public Integer getCompany_id(){
        return company_id ;
    }

    /**
     * 设置 [COMPANY_ID]
     */
    @JsonProperty("company_id")
    public void setCompany_id(Integer  company_id){
        this.company_id = company_id ;
        this.company_idDirtyFlag = true ;
    }

    /**
     * 获取 [COMPANY_ID]脏标记
     */
    @JsonIgnore
    public boolean getCompany_idDirtyFlag(){
        return company_idDirtyFlag ;
    }

    /**
     * 获取 [PARTNER_ID]
     */
    @JsonProperty("partner_id")
    public Integer getPartner_id(){
        return partner_id ;
    }

    /**
     * 设置 [PARTNER_ID]
     */
    @JsonProperty("partner_id")
    public void setPartner_id(Integer  partner_id){
        this.partner_id = partner_id ;
        this.partner_idDirtyFlag = true ;
    }

    /**
     * 获取 [PARTNER_ID]脏标记
     */
    @JsonIgnore
    public boolean getPartner_idDirtyFlag(){
        return partner_idDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return create_uid ;
    }

    /**
     * 设置 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return create_uidDirtyFlag ;
    }

    /**
     * 获取 [CURRENCY_ID]
     */
    @JsonProperty("currency_id")
    public Integer getCurrency_id(){
        return currency_id ;
    }

    /**
     * 设置 [CURRENCY_ID]
     */
    @JsonProperty("currency_id")
    public void setCurrency_id(Integer  currency_id){
        this.currency_id = currency_id ;
        this.currency_idDirtyFlag = true ;
    }

    /**
     * 获取 [CURRENCY_ID]脏标记
     */
    @JsonIgnore
    public boolean getCurrency_idDirtyFlag(){
        return currency_idDirtyFlag ;
    }



    public Res_partner_bank toDO() {
        Res_partner_bank srfdomain = new Res_partner_bank();
        if(getSanitized_acc_numberDirtyFlag())
            srfdomain.setSanitized_acc_number(sanitized_acc_number);
        if(getDisplay_nameDirtyFlag())
            srfdomain.setDisplay_name(display_name);
        if(getSequenceDirtyFlag())
            srfdomain.setSequence(sequence);
        if(getJournal_idDirtyFlag())
            srfdomain.setJournal_id(journal_id);
        if(getIdDirtyFlag())
            srfdomain.setId(id);
        if(getCreate_dateDirtyFlag())
            srfdomain.setCreate_date(create_date);
        if(get__last_updateDirtyFlag())
            srfdomain.set__last_update(__last_update);
        if(getAcc_numberDirtyFlag())
            srfdomain.setAcc_number(acc_number);
        if(getAcc_typeDirtyFlag())
            srfdomain.setAcc_type(acc_type);
        if(getQr_code_validDirtyFlag())
            srfdomain.setQr_code_valid(qr_code_valid);
        if(getAcc_holder_nameDirtyFlag())
            srfdomain.setAcc_holder_name(acc_holder_name);
        if(getWrite_dateDirtyFlag())
            srfdomain.setWrite_date(write_date);
        if(getBank_nameDirtyFlag())
            srfdomain.setBank_name(bank_name);
        if(getCreate_uid_textDirtyFlag())
            srfdomain.setCreate_uid_text(create_uid_text);
        if(getPartner_id_textDirtyFlag())
            srfdomain.setPartner_id_text(partner_id_text);
        if(getCurrency_id_textDirtyFlag())
            srfdomain.setCurrency_id_text(currency_id_text);
        if(getBank_bicDirtyFlag())
            srfdomain.setBank_bic(bank_bic);
        if(getWrite_uid_textDirtyFlag())
            srfdomain.setWrite_uid_text(write_uid_text);
        if(getCompany_id_textDirtyFlag())
            srfdomain.setCompany_id_text(company_id_text);
        if(getWrite_uidDirtyFlag())
            srfdomain.setWrite_uid(write_uid);
        if(getBank_idDirtyFlag())
            srfdomain.setBank_id(bank_id);
        if(getCompany_idDirtyFlag())
            srfdomain.setCompany_id(company_id);
        if(getPartner_idDirtyFlag())
            srfdomain.setPartner_id(partner_id);
        if(getCreate_uidDirtyFlag())
            srfdomain.setCreate_uid(create_uid);
        if(getCurrency_idDirtyFlag())
            srfdomain.setCurrency_id(currency_id);

        return srfdomain;
    }

    public void fromDO(Res_partner_bank srfdomain) {
        if(srfdomain == null )
          return ;
        if(srfdomain.getSanitized_acc_numberDirtyFlag())
            this.setSanitized_acc_number(srfdomain.getSanitized_acc_number());
        if(srfdomain.getDisplay_nameDirtyFlag())
            this.setDisplay_name(srfdomain.getDisplay_name());
        if(srfdomain.getSequenceDirtyFlag())
            this.setSequence(srfdomain.getSequence());
        if(srfdomain.getJournal_idDirtyFlag())
            this.setJournal_id(srfdomain.getJournal_id());
        if(srfdomain.getIdDirtyFlag())
            this.setId(srfdomain.getId());
        if(srfdomain.getCreate_dateDirtyFlag())
            this.setCreate_date(srfdomain.getCreate_date());
        if(srfdomain.get__last_updateDirtyFlag())
            this.set__last_update(srfdomain.get__last_update());
        if(srfdomain.getAcc_numberDirtyFlag())
            this.setAcc_number(srfdomain.getAcc_number());
        if(srfdomain.getAcc_typeDirtyFlag())
            this.setAcc_type(srfdomain.getAcc_type());
        if(srfdomain.getQr_code_validDirtyFlag())
            this.setQr_code_valid(srfdomain.getQr_code_valid());
        if(srfdomain.getAcc_holder_nameDirtyFlag())
            this.setAcc_holder_name(srfdomain.getAcc_holder_name());
        if(srfdomain.getWrite_dateDirtyFlag())
            this.setWrite_date(srfdomain.getWrite_date());
        if(srfdomain.getBank_nameDirtyFlag())
            this.setBank_name(srfdomain.getBank_name());
        if(srfdomain.getCreate_uid_textDirtyFlag())
            this.setCreate_uid_text(srfdomain.getCreate_uid_text());
        if(srfdomain.getPartner_id_textDirtyFlag())
            this.setPartner_id_text(srfdomain.getPartner_id_text());
        if(srfdomain.getCurrency_id_textDirtyFlag())
            this.setCurrency_id_text(srfdomain.getCurrency_id_text());
        if(srfdomain.getBank_bicDirtyFlag())
            this.setBank_bic(srfdomain.getBank_bic());
        if(srfdomain.getWrite_uid_textDirtyFlag())
            this.setWrite_uid_text(srfdomain.getWrite_uid_text());
        if(srfdomain.getCompany_id_textDirtyFlag())
            this.setCompany_id_text(srfdomain.getCompany_id_text());
        if(srfdomain.getWrite_uidDirtyFlag())
            this.setWrite_uid(srfdomain.getWrite_uid());
        if(srfdomain.getBank_idDirtyFlag())
            this.setBank_id(srfdomain.getBank_id());
        if(srfdomain.getCompany_idDirtyFlag())
            this.setCompany_id(srfdomain.getCompany_id());
        if(srfdomain.getPartner_idDirtyFlag())
            this.setPartner_id(srfdomain.getPartner_id());
        if(srfdomain.getCreate_uidDirtyFlag())
            this.setCreate_uid(srfdomain.getCreate_uid());
        if(srfdomain.getCurrency_idDirtyFlag())
            this.setCurrency_id(srfdomain.getCurrency_id());

    }

    public List<Res_partner_bankDTO> fromDOPage(List<Res_partner_bank> poPage)   {
        if(poPage == null)
            return null;
        List<Res_partner_bankDTO> dtos=new ArrayList<Res_partner_bankDTO>();
        for(Res_partner_bank domain : poPage) {
            Res_partner_bankDTO dto = new Res_partner_bankDTO();
            dto.fromDO(domain);
            dtos.add(dto);
        }
        return dtos;
    }
}

