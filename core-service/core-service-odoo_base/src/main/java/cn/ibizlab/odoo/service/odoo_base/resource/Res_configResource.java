package cn.ibizlab.odoo.service.odoo_base.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_base.dto.Res_configDTO;
import cn.ibizlab.odoo.core.odoo_base.domain.Res_config;
import cn.ibizlab.odoo.core.odoo_base.service.IRes_configService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_base.filter.Res_configSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Res_config" })
@RestController
@RequestMapping("")
public class Res_configResource {

    @Autowired
    private IRes_configService res_configService;

    public IRes_configService getRes_configService() {
        return this.res_configService;
    }

    @ApiOperation(value = "获取数据", tags = {"Res_config" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_base/res_configs/{res_config_id}")
    public ResponseEntity<Res_configDTO> get(@PathVariable("res_config_id") Integer res_config_id) {
        Res_configDTO dto = new Res_configDTO();
        Res_config domain = res_configService.get(res_config_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "删除数据", tags = {"Res_config" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_base/res_configs/{res_config_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("res_config_id") Integer res_config_id) {
        Res_configDTO res_configdto = new Res_configDTO();
		Res_config domain = new Res_config();
		res_configdto.setId(res_config_id);
		domain.setId(res_config_id);
        Boolean rst = res_configService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "批更新数据", tags = {"Res_config" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_base/res_configs/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Res_configDTO> res_configdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "更新数据", tags = {"Res_config" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_base/res_configs/{res_config_id}")

    public ResponseEntity<Res_configDTO> update(@PathVariable("res_config_id") Integer res_config_id, @RequestBody Res_configDTO res_configdto) {
		Res_config domain = res_configdto.toDO();
        domain.setId(res_config_id);
		res_configService.update(domain);
		Res_configDTO dto = new Res_configDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批删除数据", tags = {"Res_config" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_base/res_configs/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Res_configDTO> res_configdtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "建立数据", tags = {"Res_config" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_base/res_configs")

    public ResponseEntity<Res_configDTO> create(@RequestBody Res_configDTO res_configdto) {
        Res_configDTO dto = new Res_configDTO();
        Res_config domain = res_configdto.toDO();
		res_configService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批建立数据", tags = {"Res_config" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_base/res_configs/createBatch")
    public ResponseEntity<Boolean> createBatchRes_config(@RequestBody List<Res_configDTO> res_configdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Res_config" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_base/res_configs/fetchdefault")
	public ResponseEntity<Page<Res_configDTO>> fetchDefault(Res_configSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Res_configDTO> list = new ArrayList<Res_configDTO>();
        
        Page<Res_config> domains = res_configService.searchDefault(context) ;
        for(Res_config res_config : domains.getContent()){
            Res_configDTO dto = new Res_configDTO();
            dto.fromDO(res_config);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
