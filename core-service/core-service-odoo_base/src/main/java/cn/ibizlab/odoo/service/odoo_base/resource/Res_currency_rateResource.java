package cn.ibizlab.odoo.service.odoo_base.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_base.dto.Res_currency_rateDTO;
import cn.ibizlab.odoo.core.odoo_base.domain.Res_currency_rate;
import cn.ibizlab.odoo.core.odoo_base.service.IRes_currency_rateService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_base.filter.Res_currency_rateSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Res_currency_rate" })
@RestController
@RequestMapping("")
public class Res_currency_rateResource {

    @Autowired
    private IRes_currency_rateService res_currency_rateService;

    public IRes_currency_rateService getRes_currency_rateService() {
        return this.res_currency_rateService;
    }

    @ApiOperation(value = "批删除数据", tags = {"Res_currency_rate" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_base/res_currency_rates/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Res_currency_rateDTO> res_currency_ratedtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "批更新数据", tags = {"Res_currency_rate" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_base/res_currency_rates/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Res_currency_rateDTO> res_currency_ratedtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "建立数据", tags = {"Res_currency_rate" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_base/res_currency_rates")

    public ResponseEntity<Res_currency_rateDTO> create(@RequestBody Res_currency_rateDTO res_currency_ratedto) {
        Res_currency_rateDTO dto = new Res_currency_rateDTO();
        Res_currency_rate domain = res_currency_ratedto.toDO();
		res_currency_rateService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取数据", tags = {"Res_currency_rate" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_base/res_currency_rates/{res_currency_rate_id}")
    public ResponseEntity<Res_currency_rateDTO> get(@PathVariable("res_currency_rate_id") Integer res_currency_rate_id) {
        Res_currency_rateDTO dto = new Res_currency_rateDTO();
        Res_currency_rate domain = res_currency_rateService.get(res_currency_rate_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批建立数据", tags = {"Res_currency_rate" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_base/res_currency_rates/createBatch")
    public ResponseEntity<Boolean> createBatchRes_currency_rate(@RequestBody List<Res_currency_rateDTO> res_currency_ratedtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "删除数据", tags = {"Res_currency_rate" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_base/res_currency_rates/{res_currency_rate_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("res_currency_rate_id") Integer res_currency_rate_id) {
        Res_currency_rateDTO res_currency_ratedto = new Res_currency_rateDTO();
		Res_currency_rate domain = new Res_currency_rate();
		res_currency_ratedto.setId(res_currency_rate_id);
		domain.setId(res_currency_rate_id);
        Boolean rst = res_currency_rateService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "更新数据", tags = {"Res_currency_rate" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_base/res_currency_rates/{res_currency_rate_id}")

    public ResponseEntity<Res_currency_rateDTO> update(@PathVariable("res_currency_rate_id") Integer res_currency_rate_id, @RequestBody Res_currency_rateDTO res_currency_ratedto) {
		Res_currency_rate domain = res_currency_ratedto.toDO();
        domain.setId(res_currency_rate_id);
		res_currency_rateService.update(domain);
		Res_currency_rateDTO dto = new Res_currency_rateDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Res_currency_rate" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_base/res_currency_rates/fetchdefault")
	public ResponseEntity<Page<Res_currency_rateDTO>> fetchDefault(Res_currency_rateSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Res_currency_rateDTO> list = new ArrayList<Res_currency_rateDTO>();
        
        Page<Res_currency_rate> domains = res_currency_rateService.searchDefault(context) ;
        for(Res_currency_rate res_currency_rate : domains.getContent()){
            Res_currency_rateDTO dto = new Res_currency_rateDTO();
            dto.fromDO(res_currency_rate);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
