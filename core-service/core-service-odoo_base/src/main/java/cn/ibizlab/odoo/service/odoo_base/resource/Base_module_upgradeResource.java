package cn.ibizlab.odoo.service.odoo_base.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_base.dto.Base_module_upgradeDTO;
import cn.ibizlab.odoo.core.odoo_base.domain.Base_module_upgrade;
import cn.ibizlab.odoo.core.odoo_base.service.IBase_module_upgradeService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_base.filter.Base_module_upgradeSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Base_module_upgrade" })
@RestController
@RequestMapping("")
public class Base_module_upgradeResource {

    @Autowired
    private IBase_module_upgradeService base_module_upgradeService;

    public IBase_module_upgradeService getBase_module_upgradeService() {
        return this.base_module_upgradeService;
    }

    @ApiOperation(value = "获取数据", tags = {"Base_module_upgrade" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_base/base_module_upgrades/{base_module_upgrade_id}")
    public ResponseEntity<Base_module_upgradeDTO> get(@PathVariable("base_module_upgrade_id") Integer base_module_upgrade_id) {
        Base_module_upgradeDTO dto = new Base_module_upgradeDTO();
        Base_module_upgrade domain = base_module_upgradeService.get(base_module_upgrade_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "更新数据", tags = {"Base_module_upgrade" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_base/base_module_upgrades/{base_module_upgrade_id}")

    public ResponseEntity<Base_module_upgradeDTO> update(@PathVariable("base_module_upgrade_id") Integer base_module_upgrade_id, @RequestBody Base_module_upgradeDTO base_module_upgradedto) {
		Base_module_upgrade domain = base_module_upgradedto.toDO();
        domain.setId(base_module_upgrade_id);
		base_module_upgradeService.update(domain);
		Base_module_upgradeDTO dto = new Base_module_upgradeDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "建立数据", tags = {"Base_module_upgrade" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_base/base_module_upgrades")

    public ResponseEntity<Base_module_upgradeDTO> create(@RequestBody Base_module_upgradeDTO base_module_upgradedto) {
        Base_module_upgradeDTO dto = new Base_module_upgradeDTO();
        Base_module_upgrade domain = base_module_upgradedto.toDO();
		base_module_upgradeService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批更新数据", tags = {"Base_module_upgrade" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_base/base_module_upgrades/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Base_module_upgradeDTO> base_module_upgradedtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "批删除数据", tags = {"Base_module_upgrade" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_base/base_module_upgrades/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Base_module_upgradeDTO> base_module_upgradedtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "删除数据", tags = {"Base_module_upgrade" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_base/base_module_upgrades/{base_module_upgrade_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("base_module_upgrade_id") Integer base_module_upgrade_id) {
        Base_module_upgradeDTO base_module_upgradedto = new Base_module_upgradeDTO();
		Base_module_upgrade domain = new Base_module_upgrade();
		base_module_upgradedto.setId(base_module_upgrade_id);
		domain.setId(base_module_upgrade_id);
        Boolean rst = base_module_upgradeService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "批建立数据", tags = {"Base_module_upgrade" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_base/base_module_upgrades/createBatch")
    public ResponseEntity<Boolean> createBatchBase_module_upgrade(@RequestBody List<Base_module_upgradeDTO> base_module_upgradedtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Base_module_upgrade" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_base/base_module_upgrades/fetchdefault")
	public ResponseEntity<Page<Base_module_upgradeDTO>> fetchDefault(Base_module_upgradeSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Base_module_upgradeDTO> list = new ArrayList<Base_module_upgradeDTO>();
        
        Page<Base_module_upgrade> domains = base_module_upgradeService.searchDefault(context) ;
        for(Base_module_upgrade base_module_upgrade : domains.getContent()){
            Base_module_upgradeDTO dto = new Base_module_upgradeDTO();
            dto.fromDO(base_module_upgrade);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
