package cn.ibizlab.odoo.service.odoo_base.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.core.odoo_base.valuerule.anno.res_country.*;
import cn.ibizlab.odoo.core.odoo_base.domain.Res_country;
import org.springframework.cglib.beans.BeanCopier;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 服务DTO对象[Res_countryDTO]
 */
public class Res_countryDTO implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [ADDRESS_VIEW_ID]
     *
     */
    @Res_countryAddress_view_idDefault(info = "默认规则")
    private Integer address_view_id;

    @JsonIgnore
    private boolean address_view_idDirtyFlag;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @Res_countryDisplay_nameDefault(info = "默认规则")
    private String display_name;

    @JsonIgnore
    private boolean display_nameDirtyFlag;

    /**
     * 属性 [IMAGE]
     *
     */
    @Res_countryImageDefault(info = "默认规则")
    private byte[] image;

    @JsonIgnore
    private boolean imageDirtyFlag;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @Res_country__last_updateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp __last_update;

    @JsonIgnore
    private boolean __last_updateDirtyFlag;

    /**
     * 属性 [COUNTRY_GROUP_IDS]
     *
     */
    @Res_countryCountry_group_idsDefault(info = "默认规则")
    private String country_group_ids;

    @JsonIgnore
    private boolean country_group_idsDirtyFlag;

    /**
     * 属性 [WRITE_DATE]
     *
     */
    @Res_countryWrite_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp write_date;

    @JsonIgnore
    private boolean write_dateDirtyFlag;

    /**
     * 属性 [STATE_IDS]
     *
     */
    @Res_countryState_idsDefault(info = "默认规则")
    private String state_ids;

    @JsonIgnore
    private boolean state_idsDirtyFlag;

    /**
     * 属性 [NAME_POSITION]
     *
     */
    @Res_countryName_positionDefault(info = "默认规则")
    private String name_position;

    @JsonIgnore
    private boolean name_positionDirtyFlag;

    /**
     * 属性 [CREATE_DATE]
     *
     */
    @Res_countryCreate_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp create_date;

    @JsonIgnore
    private boolean create_dateDirtyFlag;

    /**
     * 属性 [PHONE_CODE]
     *
     */
    @Res_countryPhone_codeDefault(info = "默认规则")
    private Integer phone_code;

    @JsonIgnore
    private boolean phone_codeDirtyFlag;

    /**
     * 属性 [ID]
     *
     */
    @Res_countryIdDefault(info = "默认规则")
    private Integer id;

    @JsonIgnore
    private boolean idDirtyFlag;

    /**
     * 属性 [CODE]
     *
     */
    @Res_countryCodeDefault(info = "默认规则")
    private String code;

    @JsonIgnore
    private boolean codeDirtyFlag;

    /**
     * 属性 [VAT_LABEL]
     *
     */
    @Res_countryVat_labelDefault(info = "默认规则")
    private String vat_label;

    @JsonIgnore
    private boolean vat_labelDirtyFlag;

    /**
     * 属性 [NAME]
     *
     */
    @Res_countryNameDefault(info = "默认规则")
    private String name;

    @JsonIgnore
    private boolean nameDirtyFlag;

    /**
     * 属性 [ADDRESS_FORMAT]
     *
     */
    @Res_countryAddress_formatDefault(info = "默认规则")
    private String address_format;

    @JsonIgnore
    private boolean address_formatDirtyFlag;

    /**
     * 属性 [CREATE_UID_TEXT]
     *
     */
    @Res_countryCreate_uid_textDefault(info = "默认规则")
    private String create_uid_text;

    @JsonIgnore
    private boolean create_uid_textDirtyFlag;

    /**
     * 属性 [WRITE_UID_TEXT]
     *
     */
    @Res_countryWrite_uid_textDefault(info = "默认规则")
    private String write_uid_text;

    @JsonIgnore
    private boolean write_uid_textDirtyFlag;

    /**
     * 属性 [CURRENCY_ID_TEXT]
     *
     */
    @Res_countryCurrency_id_textDefault(info = "默认规则")
    private String currency_id_text;

    @JsonIgnore
    private boolean currency_id_textDirtyFlag;

    /**
     * 属性 [WRITE_UID]
     *
     */
    @Res_countryWrite_uidDefault(info = "默认规则")
    private Integer write_uid;

    @JsonIgnore
    private boolean write_uidDirtyFlag;

    /**
     * 属性 [CREATE_UID]
     *
     */
    @Res_countryCreate_uidDefault(info = "默认规则")
    private Integer create_uid;

    @JsonIgnore
    private boolean create_uidDirtyFlag;

    /**
     * 属性 [CURRENCY_ID]
     *
     */
    @Res_countryCurrency_idDefault(info = "默认规则")
    private Integer currency_id;

    @JsonIgnore
    private boolean currency_idDirtyFlag;


    /**
     * 获取 [ADDRESS_VIEW_ID]
     */
    @JsonProperty("address_view_id")
    public Integer getAddress_view_id(){
        return address_view_id ;
    }

    /**
     * 设置 [ADDRESS_VIEW_ID]
     */
    @JsonProperty("address_view_id")
    public void setAddress_view_id(Integer  address_view_id){
        this.address_view_id = address_view_id ;
        this.address_view_idDirtyFlag = true ;
    }

    /**
     * 获取 [ADDRESS_VIEW_ID]脏标记
     */
    @JsonIgnore
    public boolean getAddress_view_idDirtyFlag(){
        return address_view_idDirtyFlag ;
    }

    /**
     * 获取 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return display_name ;
    }

    /**
     * 设置 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

    /**
     * 获取 [DISPLAY_NAME]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return display_nameDirtyFlag ;
    }

    /**
     * 获取 [IMAGE]
     */
    @JsonProperty("image")
    public byte[] getImage(){
        return image ;
    }

    /**
     * 设置 [IMAGE]
     */
    @JsonProperty("image")
    public void setImage(byte[]  image){
        this.image = image ;
        this.imageDirtyFlag = true ;
    }

    /**
     * 获取 [IMAGE]脏标记
     */
    @JsonIgnore
    public boolean getImageDirtyFlag(){
        return imageDirtyFlag ;
    }

    /**
     * 获取 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return __last_update ;
    }

    /**
     * 设置 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

    /**
     * 获取 [__LAST_UPDATE]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return __last_updateDirtyFlag ;
    }

    /**
     * 获取 [COUNTRY_GROUP_IDS]
     */
    @JsonProperty("country_group_ids")
    public String getCountry_group_ids(){
        return country_group_ids ;
    }

    /**
     * 设置 [COUNTRY_GROUP_IDS]
     */
    @JsonProperty("country_group_ids")
    public void setCountry_group_ids(String  country_group_ids){
        this.country_group_ids = country_group_ids ;
        this.country_group_idsDirtyFlag = true ;
    }

    /**
     * 获取 [COUNTRY_GROUP_IDS]脏标记
     */
    @JsonIgnore
    public boolean getCountry_group_idsDirtyFlag(){
        return country_group_idsDirtyFlag ;
    }

    /**
     * 获取 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return write_date ;
    }

    /**
     * 设置 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return write_dateDirtyFlag ;
    }

    /**
     * 获取 [STATE_IDS]
     */
    @JsonProperty("state_ids")
    public String getState_ids(){
        return state_ids ;
    }

    /**
     * 设置 [STATE_IDS]
     */
    @JsonProperty("state_ids")
    public void setState_ids(String  state_ids){
        this.state_ids = state_ids ;
        this.state_idsDirtyFlag = true ;
    }

    /**
     * 获取 [STATE_IDS]脏标记
     */
    @JsonIgnore
    public boolean getState_idsDirtyFlag(){
        return state_idsDirtyFlag ;
    }

    /**
     * 获取 [NAME_POSITION]
     */
    @JsonProperty("name_position")
    public String getName_position(){
        return name_position ;
    }

    /**
     * 设置 [NAME_POSITION]
     */
    @JsonProperty("name_position")
    public void setName_position(String  name_position){
        this.name_position = name_position ;
        this.name_positionDirtyFlag = true ;
    }

    /**
     * 获取 [NAME_POSITION]脏标记
     */
    @JsonIgnore
    public boolean getName_positionDirtyFlag(){
        return name_positionDirtyFlag ;
    }

    /**
     * 获取 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return create_date ;
    }

    /**
     * 设置 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return create_dateDirtyFlag ;
    }

    /**
     * 获取 [PHONE_CODE]
     */
    @JsonProperty("phone_code")
    public Integer getPhone_code(){
        return phone_code ;
    }

    /**
     * 设置 [PHONE_CODE]
     */
    @JsonProperty("phone_code")
    public void setPhone_code(Integer  phone_code){
        this.phone_code = phone_code ;
        this.phone_codeDirtyFlag = true ;
    }

    /**
     * 获取 [PHONE_CODE]脏标记
     */
    @JsonIgnore
    public boolean getPhone_codeDirtyFlag(){
        return phone_codeDirtyFlag ;
    }

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

    /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return idDirtyFlag ;
    }

    /**
     * 获取 [CODE]
     */
    @JsonProperty("code")
    public String getCode(){
        return code ;
    }

    /**
     * 设置 [CODE]
     */
    @JsonProperty("code")
    public void setCode(String  code){
        this.code = code ;
        this.codeDirtyFlag = true ;
    }

    /**
     * 获取 [CODE]脏标记
     */
    @JsonIgnore
    public boolean getCodeDirtyFlag(){
        return codeDirtyFlag ;
    }

    /**
     * 获取 [VAT_LABEL]
     */
    @JsonProperty("vat_label")
    public String getVat_label(){
        return vat_label ;
    }

    /**
     * 设置 [VAT_LABEL]
     */
    @JsonProperty("vat_label")
    public void setVat_label(String  vat_label){
        this.vat_label = vat_label ;
        this.vat_labelDirtyFlag = true ;
    }

    /**
     * 获取 [VAT_LABEL]脏标记
     */
    @JsonIgnore
    public boolean getVat_labelDirtyFlag(){
        return vat_labelDirtyFlag ;
    }

    /**
     * 获取 [NAME]
     */
    @JsonProperty("name")
    public String getName(){
        return name ;
    }

    /**
     * 设置 [NAME]
     */
    @JsonProperty("name")
    public void setName(String  name){
        this.name = name ;
        this.nameDirtyFlag = true ;
    }

    /**
     * 获取 [NAME]脏标记
     */
    @JsonIgnore
    public boolean getNameDirtyFlag(){
        return nameDirtyFlag ;
    }

    /**
     * 获取 [ADDRESS_FORMAT]
     */
    @JsonProperty("address_format")
    public String getAddress_format(){
        return address_format ;
    }

    /**
     * 设置 [ADDRESS_FORMAT]
     */
    @JsonProperty("address_format")
    public void setAddress_format(String  address_format){
        this.address_format = address_format ;
        this.address_formatDirtyFlag = true ;
    }

    /**
     * 获取 [ADDRESS_FORMAT]脏标记
     */
    @JsonIgnore
    public boolean getAddress_formatDirtyFlag(){
        return address_formatDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return create_uid_text ;
    }

    /**
     * 设置 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return create_uid_textDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return write_uid_text ;
    }

    /**
     * 设置 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return write_uid_textDirtyFlag ;
    }

    /**
     * 获取 [CURRENCY_ID_TEXT]
     */
    @JsonProperty("currency_id_text")
    public String getCurrency_id_text(){
        return currency_id_text ;
    }

    /**
     * 设置 [CURRENCY_ID_TEXT]
     */
    @JsonProperty("currency_id_text")
    public void setCurrency_id_text(String  currency_id_text){
        this.currency_id_text = currency_id_text ;
        this.currency_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [CURRENCY_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getCurrency_id_textDirtyFlag(){
        return currency_id_textDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return write_uid ;
    }

    /**
     * 设置 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return write_uidDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return create_uid ;
    }

    /**
     * 设置 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return create_uidDirtyFlag ;
    }

    /**
     * 获取 [CURRENCY_ID]
     */
    @JsonProperty("currency_id")
    public Integer getCurrency_id(){
        return currency_id ;
    }

    /**
     * 设置 [CURRENCY_ID]
     */
    @JsonProperty("currency_id")
    public void setCurrency_id(Integer  currency_id){
        this.currency_id = currency_id ;
        this.currency_idDirtyFlag = true ;
    }

    /**
     * 获取 [CURRENCY_ID]脏标记
     */
    @JsonIgnore
    public boolean getCurrency_idDirtyFlag(){
        return currency_idDirtyFlag ;
    }



    public Res_country toDO() {
        Res_country srfdomain = new Res_country();
        if(getAddress_view_idDirtyFlag())
            srfdomain.setAddress_view_id(address_view_id);
        if(getDisplay_nameDirtyFlag())
            srfdomain.setDisplay_name(display_name);
        if(getImageDirtyFlag())
            srfdomain.setImage(image);
        if(get__last_updateDirtyFlag())
            srfdomain.set__last_update(__last_update);
        if(getCountry_group_idsDirtyFlag())
            srfdomain.setCountry_group_ids(country_group_ids);
        if(getWrite_dateDirtyFlag())
            srfdomain.setWrite_date(write_date);
        if(getState_idsDirtyFlag())
            srfdomain.setState_ids(state_ids);
        if(getName_positionDirtyFlag())
            srfdomain.setName_position(name_position);
        if(getCreate_dateDirtyFlag())
            srfdomain.setCreate_date(create_date);
        if(getPhone_codeDirtyFlag())
            srfdomain.setPhone_code(phone_code);
        if(getIdDirtyFlag())
            srfdomain.setId(id);
        if(getCodeDirtyFlag())
            srfdomain.setCode(code);
        if(getVat_labelDirtyFlag())
            srfdomain.setVat_label(vat_label);
        if(getNameDirtyFlag())
            srfdomain.setName(name);
        if(getAddress_formatDirtyFlag())
            srfdomain.setAddress_format(address_format);
        if(getCreate_uid_textDirtyFlag())
            srfdomain.setCreate_uid_text(create_uid_text);
        if(getWrite_uid_textDirtyFlag())
            srfdomain.setWrite_uid_text(write_uid_text);
        if(getCurrency_id_textDirtyFlag())
            srfdomain.setCurrency_id_text(currency_id_text);
        if(getWrite_uidDirtyFlag())
            srfdomain.setWrite_uid(write_uid);
        if(getCreate_uidDirtyFlag())
            srfdomain.setCreate_uid(create_uid);
        if(getCurrency_idDirtyFlag())
            srfdomain.setCurrency_id(currency_id);

        return srfdomain;
    }

    public void fromDO(Res_country srfdomain) {
        if(srfdomain == null )
          return ;
        if(srfdomain.getAddress_view_idDirtyFlag())
            this.setAddress_view_id(srfdomain.getAddress_view_id());
        if(srfdomain.getDisplay_nameDirtyFlag())
            this.setDisplay_name(srfdomain.getDisplay_name());
        if(srfdomain.getImageDirtyFlag())
            this.setImage(srfdomain.getImage());
        if(srfdomain.get__last_updateDirtyFlag())
            this.set__last_update(srfdomain.get__last_update());
        if(srfdomain.getCountry_group_idsDirtyFlag())
            this.setCountry_group_ids(srfdomain.getCountry_group_ids());
        if(srfdomain.getWrite_dateDirtyFlag())
            this.setWrite_date(srfdomain.getWrite_date());
        if(srfdomain.getState_idsDirtyFlag())
            this.setState_ids(srfdomain.getState_ids());
        if(srfdomain.getName_positionDirtyFlag())
            this.setName_position(srfdomain.getName_position());
        if(srfdomain.getCreate_dateDirtyFlag())
            this.setCreate_date(srfdomain.getCreate_date());
        if(srfdomain.getPhone_codeDirtyFlag())
            this.setPhone_code(srfdomain.getPhone_code());
        if(srfdomain.getIdDirtyFlag())
            this.setId(srfdomain.getId());
        if(srfdomain.getCodeDirtyFlag())
            this.setCode(srfdomain.getCode());
        if(srfdomain.getVat_labelDirtyFlag())
            this.setVat_label(srfdomain.getVat_label());
        if(srfdomain.getNameDirtyFlag())
            this.setName(srfdomain.getName());
        if(srfdomain.getAddress_formatDirtyFlag())
            this.setAddress_format(srfdomain.getAddress_format());
        if(srfdomain.getCreate_uid_textDirtyFlag())
            this.setCreate_uid_text(srfdomain.getCreate_uid_text());
        if(srfdomain.getWrite_uid_textDirtyFlag())
            this.setWrite_uid_text(srfdomain.getWrite_uid_text());
        if(srfdomain.getCurrency_id_textDirtyFlag())
            this.setCurrency_id_text(srfdomain.getCurrency_id_text());
        if(srfdomain.getWrite_uidDirtyFlag())
            this.setWrite_uid(srfdomain.getWrite_uid());
        if(srfdomain.getCreate_uidDirtyFlag())
            this.setCreate_uid(srfdomain.getCreate_uid());
        if(srfdomain.getCurrency_idDirtyFlag())
            this.setCurrency_id(srfdomain.getCurrency_id());

    }

    public List<Res_countryDTO> fromDOPage(List<Res_country> poPage)   {
        if(poPage == null)
            return null;
        List<Res_countryDTO> dtos=new ArrayList<Res_countryDTO>();
        for(Res_country domain : poPage) {
            Res_countryDTO dto = new Res_countryDTO();
            dto.fromDO(domain);
            dtos.add(dto);
        }
        return dtos;
    }
}

