package cn.ibizlab.odoo.service.odoo_base.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_base.dto.Base_partner_merge_lineDTO;
import cn.ibizlab.odoo.core.odoo_base.domain.Base_partner_merge_line;
import cn.ibizlab.odoo.core.odoo_base.service.IBase_partner_merge_lineService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_base.filter.Base_partner_merge_lineSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Base_partner_merge_line" })
@RestController
@RequestMapping("")
public class Base_partner_merge_lineResource {

    @Autowired
    private IBase_partner_merge_lineService base_partner_merge_lineService;

    public IBase_partner_merge_lineService getBase_partner_merge_lineService() {
        return this.base_partner_merge_lineService;
    }

    @ApiOperation(value = "获取数据", tags = {"Base_partner_merge_line" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_base/base_partner_merge_lines/{base_partner_merge_line_id}")
    public ResponseEntity<Base_partner_merge_lineDTO> get(@PathVariable("base_partner_merge_line_id") Integer base_partner_merge_line_id) {
        Base_partner_merge_lineDTO dto = new Base_partner_merge_lineDTO();
        Base_partner_merge_line domain = base_partner_merge_lineService.get(base_partner_merge_line_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "更新数据", tags = {"Base_partner_merge_line" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_base/base_partner_merge_lines/{base_partner_merge_line_id}")

    public ResponseEntity<Base_partner_merge_lineDTO> update(@PathVariable("base_partner_merge_line_id") Integer base_partner_merge_line_id, @RequestBody Base_partner_merge_lineDTO base_partner_merge_linedto) {
		Base_partner_merge_line domain = base_partner_merge_linedto.toDO();
        domain.setId(base_partner_merge_line_id);
		base_partner_merge_lineService.update(domain);
		Base_partner_merge_lineDTO dto = new Base_partner_merge_lineDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "建立数据", tags = {"Base_partner_merge_line" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_base/base_partner_merge_lines")

    public ResponseEntity<Base_partner_merge_lineDTO> create(@RequestBody Base_partner_merge_lineDTO base_partner_merge_linedto) {
        Base_partner_merge_lineDTO dto = new Base_partner_merge_lineDTO();
        Base_partner_merge_line domain = base_partner_merge_linedto.toDO();
		base_partner_merge_lineService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批更新数据", tags = {"Base_partner_merge_line" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_base/base_partner_merge_lines/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Base_partner_merge_lineDTO> base_partner_merge_linedtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "批删除数据", tags = {"Base_partner_merge_line" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_base/base_partner_merge_lines/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Base_partner_merge_lineDTO> base_partner_merge_linedtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "批建立数据", tags = {"Base_partner_merge_line" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_base/base_partner_merge_lines/createBatch")
    public ResponseEntity<Boolean> createBatchBase_partner_merge_line(@RequestBody List<Base_partner_merge_lineDTO> base_partner_merge_linedtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "删除数据", tags = {"Base_partner_merge_line" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_base/base_partner_merge_lines/{base_partner_merge_line_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("base_partner_merge_line_id") Integer base_partner_merge_line_id) {
        Base_partner_merge_lineDTO base_partner_merge_linedto = new Base_partner_merge_lineDTO();
		Base_partner_merge_line domain = new Base_partner_merge_line();
		base_partner_merge_linedto.setId(base_partner_merge_line_id);
		domain.setId(base_partner_merge_line_id);
        Boolean rst = base_partner_merge_lineService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

	@ApiOperation(value = "获取默认查询", tags = {"Base_partner_merge_line" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_base/base_partner_merge_lines/fetchdefault")
	public ResponseEntity<Page<Base_partner_merge_lineDTO>> fetchDefault(Base_partner_merge_lineSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Base_partner_merge_lineDTO> list = new ArrayList<Base_partner_merge_lineDTO>();
        
        Page<Base_partner_merge_line> domains = base_partner_merge_lineService.searchDefault(context) ;
        for(Base_partner_merge_line base_partner_merge_line : domains.getContent()){
            Base_partner_merge_lineDTO dto = new Base_partner_merge_lineDTO();
            dto.fromDO(base_partner_merge_line);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
