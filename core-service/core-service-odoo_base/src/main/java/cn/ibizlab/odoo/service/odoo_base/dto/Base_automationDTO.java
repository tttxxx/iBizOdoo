package cn.ibizlab.odoo.service.odoo_base.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.core.odoo_base.valuerule.anno.base_automation.*;
import cn.ibizlab.odoo.core.odoo_base.domain.Base_automation;
import org.springframework.cglib.beans.BeanCopier;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 服务DTO对象[Base_automationDTO]
 */
public class Base_automationDTO implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [BINDING_MODEL_ID]
     *
     */
    @Base_automationBinding_model_idDefault(info = "默认规则")
    private Integer binding_model_id;

    @JsonIgnore
    private boolean binding_model_idDirtyFlag;

    /**
     * 属性 [SEQUENCE]
     *
     */
    @Base_automationSequenceDefault(info = "默认规则")
    private Integer sequence;

    @JsonIgnore
    private boolean sequenceDirtyFlag;

    /**
     * 属性 [FILTER_PRE_DOMAIN]
     *
     */
    @Base_automationFilter_pre_domainDefault(info = "默认规则")
    private String filter_pre_domain;

    @JsonIgnore
    private boolean filter_pre_domainDirtyFlag;

    /**
     * 属性 [ON_CHANGE_FIELDS]
     *
     */
    @Base_automationOn_change_fieldsDefault(info = "默认规则")
    private String on_change_fields;

    @JsonIgnore
    private boolean on_change_fieldsDirtyFlag;

    /**
     * 属性 [ACTIVITY_SUMMARY]
     *
     */
    @Base_automationActivity_summaryDefault(info = "默认规则")
    private String activity_summary;

    @JsonIgnore
    private boolean activity_summaryDirtyFlag;

    /**
     * 属性 [ACTIVITY_USER_FIELD_NAME]
     *
     */
    @Base_automationActivity_user_field_nameDefault(info = "默认规则")
    private String activity_user_field_name;

    @JsonIgnore
    private boolean activity_user_field_nameDirtyFlag;

    /**
     * 属性 [WEBSITE_PUBLISHED]
     *
     */
    @Base_automationWebsite_publishedDefault(info = "默认规则")
    private String website_published;

    @JsonIgnore
    private boolean website_publishedDirtyFlag;

    /**
     * 属性 [USAGE]
     *
     */
    @Base_automationUsageDefault(info = "默认规则")
    private String usage;

    @JsonIgnore
    private boolean usageDirtyFlag;

    /**
     * 属性 [CRUD_MODEL_NAME]
     *
     */
    @Base_automationCrud_model_nameDefault(info = "默认规则")
    private String crud_model_name;

    @JsonIgnore
    private boolean crud_model_nameDirtyFlag;

    /**
     * 属性 [TRG_DATE_RANGE_TYPE]
     *
     */
    @Base_automationTrg_date_range_typeDefault(info = "默认规则")
    private String trg_date_range_type;

    @JsonIgnore
    private boolean trg_date_range_typeDirtyFlag;

    /**
     * 属性 [ID]
     *
     */
    @Base_automationIdDefault(info = "默认规则")
    private Integer id;

    @JsonIgnore
    private boolean idDirtyFlag;

    /**
     * 属性 [FIELDS_LINES]
     *
     */
    @Base_automationFields_linesDefault(info = "默认规则")
    private String fields_lines;

    @JsonIgnore
    private boolean fields_linesDirtyFlag;

    /**
     * 属性 [ACTIVITY_NOTE]
     *
     */
    @Base_automationActivity_noteDefault(info = "默认规则")
    private String activity_note;

    @JsonIgnore
    private boolean activity_noteDirtyFlag;

    /**
     * 属性 [ACTIVE]
     *
     */
    @Base_automationActiveDefault(info = "默认规则")
    private String active;

    @JsonIgnore
    private boolean activeDirtyFlag;

    /**
     * 属性 [ACTIVITY_TYPE_ID]
     *
     */
    @Base_automationActivity_type_idDefault(info = "默认规则")
    private Integer activity_type_id;

    @JsonIgnore
    private boolean activity_type_idDirtyFlag;

    /**
     * 属性 [CODE]
     *
     */
    @Base_automationCodeDefault(info = "默认规则")
    private String code;

    @JsonIgnore
    private boolean codeDirtyFlag;

    /**
     * 属性 [ACTIVITY_USER_TYPE]
     *
     */
    @Base_automationActivity_user_typeDefault(info = "默认规则")
    private String activity_user_type;

    @JsonIgnore
    private boolean activity_user_typeDirtyFlag;

    /**
     * 属性 [WEBSITE_PATH]
     *
     */
    @Base_automationWebsite_pathDefault(info = "默认规则")
    private String website_path;

    @JsonIgnore
    private boolean website_pathDirtyFlag;

    /**
     * 属性 [PARTNER_IDS]
     *
     */
    @Base_automationPartner_idsDefault(info = "默认规则")
    private String partner_ids;

    @JsonIgnore
    private boolean partner_idsDirtyFlag;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @Base_automation__last_updateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp __last_update;

    @JsonIgnore
    private boolean __last_updateDirtyFlag;

    /**
     * 属性 [TRG_DATE_ID]
     *
     */
    @Base_automationTrg_date_idDefault(info = "默认规则")
    private Integer trg_date_id;

    @JsonIgnore
    private boolean trg_date_idDirtyFlag;

    /**
     * 属性 [XML_ID]
     *
     */
    @Base_automationXml_idDefault(info = "默认规则")
    private String xml_id;

    @JsonIgnore
    private boolean xml_idDirtyFlag;

    /**
     * 属性 [LAST_RUN]
     *
     */
    @Base_automationLast_runDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp last_run;

    @JsonIgnore
    private boolean last_runDirtyFlag;

    /**
     * 属性 [TEMPLATE_ID]
     *
     */
    @Base_automationTemplate_idDefault(info = "默认规则")
    private Integer template_id;

    @JsonIgnore
    private boolean template_idDirtyFlag;

    /**
     * 属性 [BINDING_TYPE]
     *
     */
    @Base_automationBinding_typeDefault(info = "默认规则")
    private String binding_type;

    @JsonIgnore
    private boolean binding_typeDirtyFlag;

    /**
     * 属性 [NAME]
     *
     */
    @Base_automationNameDefault(info = "默认规则")
    private String name;

    @JsonIgnore
    private boolean nameDirtyFlag;

    /**
     * 属性 [MODEL_NAME]
     *
     */
    @Base_automationModel_nameDefault(info = "默认规则")
    private String model_name;

    @JsonIgnore
    private boolean model_nameDirtyFlag;

    /**
     * 属性 [STATE]
     *
     */
    @Base_automationStateDefault(info = "默认规则")
    private String state;

    @JsonIgnore
    private boolean stateDirtyFlag;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @Base_automationDisplay_nameDefault(info = "默认规则")
    private String display_name;

    @JsonIgnore
    private boolean display_nameDirtyFlag;

    /**
     * 属性 [CHANNEL_IDS]
     *
     */
    @Base_automationChannel_idsDefault(info = "默认规则")
    private String channel_ids;

    @JsonIgnore
    private boolean channel_idsDirtyFlag;

    /**
     * 属性 [MODEL_ID]
     *
     */
    @Base_automationModel_idDefault(info = "默认规则")
    private Integer model_id;

    @JsonIgnore
    private boolean model_idDirtyFlag;

    /**
     * 属性 [HELP]
     *
     */
    @Base_automationHelpDefault(info = "默认规则")
    private String help;

    @JsonIgnore
    private boolean helpDirtyFlag;

    /**
     * 属性 [WEBSITE_URL]
     *
     */
    @Base_automationWebsite_urlDefault(info = "默认规则")
    private String website_url;

    @JsonIgnore
    private boolean website_urlDirtyFlag;

    /**
     * 属性 [ACTIVITY_USER_ID]
     *
     */
    @Base_automationActivity_user_idDefault(info = "默认规则")
    private Integer activity_user_id;

    @JsonIgnore
    private boolean activity_user_idDirtyFlag;

    /**
     * 属性 [ACTIVITY_DATE_DEADLINE_RANGE]
     *
     */
    @Base_automationActivity_date_deadline_rangeDefault(info = "默认规则")
    private Integer activity_date_deadline_range;

    @JsonIgnore
    private boolean activity_date_deadline_rangeDirtyFlag;

    /**
     * 属性 [TRG_DATE_RANGE]
     *
     */
    @Base_automationTrg_date_rangeDefault(info = "默认规则")
    private Integer trg_date_range;

    @JsonIgnore
    private boolean trg_date_rangeDirtyFlag;

    /**
     * 属性 [TYPE]
     *
     */
    @Base_automationTypeDefault(info = "默认规则")
    private String type;

    @JsonIgnore
    private boolean typeDirtyFlag;

    /**
     * 属性 [FILTER_DOMAIN]
     *
     */
    @Base_automationFilter_domainDefault(info = "默认规则")
    private String filter_domain;

    @JsonIgnore
    private boolean filter_domainDirtyFlag;

    /**
     * 属性 [ACTIVITY_DATE_DEADLINE_RANGE_TYPE]
     *
     */
    @Base_automationActivity_date_deadline_range_typeDefault(info = "默认规则")
    private String activity_date_deadline_range_type;

    @JsonIgnore
    private boolean activity_date_deadline_range_typeDirtyFlag;

    /**
     * 属性 [WRITE_DATE]
     *
     */
    @Base_automationWrite_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp write_date;

    @JsonIgnore
    private boolean write_dateDirtyFlag;

    /**
     * 属性 [CHILD_IDS]
     *
     */
    @Base_automationChild_idsDefault(info = "默认规则")
    private String child_ids;

    @JsonIgnore
    private boolean child_idsDirtyFlag;

    /**
     * 属性 [LINK_FIELD_ID]
     *
     */
    @Base_automationLink_field_idDefault(info = "默认规则")
    private Integer link_field_id;

    @JsonIgnore
    private boolean link_field_idDirtyFlag;

    /**
     * 属性 [CREATE_DATE]
     *
     */
    @Base_automationCreate_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp create_date;

    @JsonIgnore
    private boolean create_dateDirtyFlag;

    /**
     * 属性 [TRIGGER]
     *
     */
    @Base_automationTriggerDefault(info = "默认规则")
    private String trigger;

    @JsonIgnore
    private boolean triggerDirtyFlag;

    /**
     * 属性 [ACTION_SERVER_ID]
     *
     */
    @Base_automationAction_server_idDefault(info = "默认规则")
    private Integer action_server_id;

    @JsonIgnore
    private boolean action_server_idDirtyFlag;

    /**
     * 属性 [CRUD_MODEL_ID]
     *
     */
    @Base_automationCrud_model_idDefault(info = "默认规则")
    private Integer crud_model_id;

    @JsonIgnore
    private boolean crud_model_idDirtyFlag;

    /**
     * 属性 [CREATE_UID_TEXT]
     *
     */
    @Base_automationCreate_uid_textDefault(info = "默认规则")
    private String create_uid_text;

    @JsonIgnore
    private boolean create_uid_textDirtyFlag;

    /**
     * 属性 [WRITE_UID_TEXT]
     *
     */
    @Base_automationWrite_uid_textDefault(info = "默认规则")
    private String write_uid_text;

    @JsonIgnore
    private boolean write_uid_textDirtyFlag;

    /**
     * 属性 [TRG_DATE_CALENDAR_ID_TEXT]
     *
     */
    @Base_automationTrg_date_calendar_id_textDefault(info = "默认规则")
    private String trg_date_calendar_id_text;

    @JsonIgnore
    private boolean trg_date_calendar_id_textDirtyFlag;

    /**
     * 属性 [WRITE_UID]
     *
     */
    @Base_automationWrite_uidDefault(info = "默认规则")
    private Integer write_uid;

    @JsonIgnore
    private boolean write_uidDirtyFlag;

    /**
     * 属性 [TRG_DATE_CALENDAR_ID]
     *
     */
    @Base_automationTrg_date_calendar_idDefault(info = "默认规则")
    private Integer trg_date_calendar_id;

    @JsonIgnore
    private boolean trg_date_calendar_idDirtyFlag;

    /**
     * 属性 [CREATE_UID]
     *
     */
    @Base_automationCreate_uidDefault(info = "默认规则")
    private Integer create_uid;

    @JsonIgnore
    private boolean create_uidDirtyFlag;


    /**
     * 获取 [BINDING_MODEL_ID]
     */
    @JsonProperty("binding_model_id")
    public Integer getBinding_model_id(){
        return binding_model_id ;
    }

    /**
     * 设置 [BINDING_MODEL_ID]
     */
    @JsonProperty("binding_model_id")
    public void setBinding_model_id(Integer  binding_model_id){
        this.binding_model_id = binding_model_id ;
        this.binding_model_idDirtyFlag = true ;
    }

    /**
     * 获取 [BINDING_MODEL_ID]脏标记
     */
    @JsonIgnore
    public boolean getBinding_model_idDirtyFlag(){
        return binding_model_idDirtyFlag ;
    }

    /**
     * 获取 [SEQUENCE]
     */
    @JsonProperty("sequence")
    public Integer getSequence(){
        return sequence ;
    }

    /**
     * 设置 [SEQUENCE]
     */
    @JsonProperty("sequence")
    public void setSequence(Integer  sequence){
        this.sequence = sequence ;
        this.sequenceDirtyFlag = true ;
    }

    /**
     * 获取 [SEQUENCE]脏标记
     */
    @JsonIgnore
    public boolean getSequenceDirtyFlag(){
        return sequenceDirtyFlag ;
    }

    /**
     * 获取 [FILTER_PRE_DOMAIN]
     */
    @JsonProperty("filter_pre_domain")
    public String getFilter_pre_domain(){
        return filter_pre_domain ;
    }

    /**
     * 设置 [FILTER_PRE_DOMAIN]
     */
    @JsonProperty("filter_pre_domain")
    public void setFilter_pre_domain(String  filter_pre_domain){
        this.filter_pre_domain = filter_pre_domain ;
        this.filter_pre_domainDirtyFlag = true ;
    }

    /**
     * 获取 [FILTER_PRE_DOMAIN]脏标记
     */
    @JsonIgnore
    public boolean getFilter_pre_domainDirtyFlag(){
        return filter_pre_domainDirtyFlag ;
    }

    /**
     * 获取 [ON_CHANGE_FIELDS]
     */
    @JsonProperty("on_change_fields")
    public String getOn_change_fields(){
        return on_change_fields ;
    }

    /**
     * 设置 [ON_CHANGE_FIELDS]
     */
    @JsonProperty("on_change_fields")
    public void setOn_change_fields(String  on_change_fields){
        this.on_change_fields = on_change_fields ;
        this.on_change_fieldsDirtyFlag = true ;
    }

    /**
     * 获取 [ON_CHANGE_FIELDS]脏标记
     */
    @JsonIgnore
    public boolean getOn_change_fieldsDirtyFlag(){
        return on_change_fieldsDirtyFlag ;
    }

    /**
     * 获取 [ACTIVITY_SUMMARY]
     */
    @JsonProperty("activity_summary")
    public String getActivity_summary(){
        return activity_summary ;
    }

    /**
     * 设置 [ACTIVITY_SUMMARY]
     */
    @JsonProperty("activity_summary")
    public void setActivity_summary(String  activity_summary){
        this.activity_summary = activity_summary ;
        this.activity_summaryDirtyFlag = true ;
    }

    /**
     * 获取 [ACTIVITY_SUMMARY]脏标记
     */
    @JsonIgnore
    public boolean getActivity_summaryDirtyFlag(){
        return activity_summaryDirtyFlag ;
    }

    /**
     * 获取 [ACTIVITY_USER_FIELD_NAME]
     */
    @JsonProperty("activity_user_field_name")
    public String getActivity_user_field_name(){
        return activity_user_field_name ;
    }

    /**
     * 设置 [ACTIVITY_USER_FIELD_NAME]
     */
    @JsonProperty("activity_user_field_name")
    public void setActivity_user_field_name(String  activity_user_field_name){
        this.activity_user_field_name = activity_user_field_name ;
        this.activity_user_field_nameDirtyFlag = true ;
    }

    /**
     * 获取 [ACTIVITY_USER_FIELD_NAME]脏标记
     */
    @JsonIgnore
    public boolean getActivity_user_field_nameDirtyFlag(){
        return activity_user_field_nameDirtyFlag ;
    }

    /**
     * 获取 [WEBSITE_PUBLISHED]
     */
    @JsonProperty("website_published")
    public String getWebsite_published(){
        return website_published ;
    }

    /**
     * 设置 [WEBSITE_PUBLISHED]
     */
    @JsonProperty("website_published")
    public void setWebsite_published(String  website_published){
        this.website_published = website_published ;
        this.website_publishedDirtyFlag = true ;
    }

    /**
     * 获取 [WEBSITE_PUBLISHED]脏标记
     */
    @JsonIgnore
    public boolean getWebsite_publishedDirtyFlag(){
        return website_publishedDirtyFlag ;
    }

    /**
     * 获取 [USAGE]
     */
    @JsonProperty("usage")
    public String getUsage(){
        return usage ;
    }

    /**
     * 设置 [USAGE]
     */
    @JsonProperty("usage")
    public void setUsage(String  usage){
        this.usage = usage ;
        this.usageDirtyFlag = true ;
    }

    /**
     * 获取 [USAGE]脏标记
     */
    @JsonIgnore
    public boolean getUsageDirtyFlag(){
        return usageDirtyFlag ;
    }

    /**
     * 获取 [CRUD_MODEL_NAME]
     */
    @JsonProperty("crud_model_name")
    public String getCrud_model_name(){
        return crud_model_name ;
    }

    /**
     * 设置 [CRUD_MODEL_NAME]
     */
    @JsonProperty("crud_model_name")
    public void setCrud_model_name(String  crud_model_name){
        this.crud_model_name = crud_model_name ;
        this.crud_model_nameDirtyFlag = true ;
    }

    /**
     * 获取 [CRUD_MODEL_NAME]脏标记
     */
    @JsonIgnore
    public boolean getCrud_model_nameDirtyFlag(){
        return crud_model_nameDirtyFlag ;
    }

    /**
     * 获取 [TRG_DATE_RANGE_TYPE]
     */
    @JsonProperty("trg_date_range_type")
    public String getTrg_date_range_type(){
        return trg_date_range_type ;
    }

    /**
     * 设置 [TRG_DATE_RANGE_TYPE]
     */
    @JsonProperty("trg_date_range_type")
    public void setTrg_date_range_type(String  trg_date_range_type){
        this.trg_date_range_type = trg_date_range_type ;
        this.trg_date_range_typeDirtyFlag = true ;
    }

    /**
     * 获取 [TRG_DATE_RANGE_TYPE]脏标记
     */
    @JsonIgnore
    public boolean getTrg_date_range_typeDirtyFlag(){
        return trg_date_range_typeDirtyFlag ;
    }

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

    /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return idDirtyFlag ;
    }

    /**
     * 获取 [FIELDS_LINES]
     */
    @JsonProperty("fields_lines")
    public String getFields_lines(){
        return fields_lines ;
    }

    /**
     * 设置 [FIELDS_LINES]
     */
    @JsonProperty("fields_lines")
    public void setFields_lines(String  fields_lines){
        this.fields_lines = fields_lines ;
        this.fields_linesDirtyFlag = true ;
    }

    /**
     * 获取 [FIELDS_LINES]脏标记
     */
    @JsonIgnore
    public boolean getFields_linesDirtyFlag(){
        return fields_linesDirtyFlag ;
    }

    /**
     * 获取 [ACTIVITY_NOTE]
     */
    @JsonProperty("activity_note")
    public String getActivity_note(){
        return activity_note ;
    }

    /**
     * 设置 [ACTIVITY_NOTE]
     */
    @JsonProperty("activity_note")
    public void setActivity_note(String  activity_note){
        this.activity_note = activity_note ;
        this.activity_noteDirtyFlag = true ;
    }

    /**
     * 获取 [ACTIVITY_NOTE]脏标记
     */
    @JsonIgnore
    public boolean getActivity_noteDirtyFlag(){
        return activity_noteDirtyFlag ;
    }

    /**
     * 获取 [ACTIVE]
     */
    @JsonProperty("active")
    public String getActive(){
        return active ;
    }

    /**
     * 设置 [ACTIVE]
     */
    @JsonProperty("active")
    public void setActive(String  active){
        this.active = active ;
        this.activeDirtyFlag = true ;
    }

    /**
     * 获取 [ACTIVE]脏标记
     */
    @JsonIgnore
    public boolean getActiveDirtyFlag(){
        return activeDirtyFlag ;
    }

    /**
     * 获取 [ACTIVITY_TYPE_ID]
     */
    @JsonProperty("activity_type_id")
    public Integer getActivity_type_id(){
        return activity_type_id ;
    }

    /**
     * 设置 [ACTIVITY_TYPE_ID]
     */
    @JsonProperty("activity_type_id")
    public void setActivity_type_id(Integer  activity_type_id){
        this.activity_type_id = activity_type_id ;
        this.activity_type_idDirtyFlag = true ;
    }

    /**
     * 获取 [ACTIVITY_TYPE_ID]脏标记
     */
    @JsonIgnore
    public boolean getActivity_type_idDirtyFlag(){
        return activity_type_idDirtyFlag ;
    }

    /**
     * 获取 [CODE]
     */
    @JsonProperty("code")
    public String getCode(){
        return code ;
    }

    /**
     * 设置 [CODE]
     */
    @JsonProperty("code")
    public void setCode(String  code){
        this.code = code ;
        this.codeDirtyFlag = true ;
    }

    /**
     * 获取 [CODE]脏标记
     */
    @JsonIgnore
    public boolean getCodeDirtyFlag(){
        return codeDirtyFlag ;
    }

    /**
     * 获取 [ACTIVITY_USER_TYPE]
     */
    @JsonProperty("activity_user_type")
    public String getActivity_user_type(){
        return activity_user_type ;
    }

    /**
     * 设置 [ACTIVITY_USER_TYPE]
     */
    @JsonProperty("activity_user_type")
    public void setActivity_user_type(String  activity_user_type){
        this.activity_user_type = activity_user_type ;
        this.activity_user_typeDirtyFlag = true ;
    }

    /**
     * 获取 [ACTIVITY_USER_TYPE]脏标记
     */
    @JsonIgnore
    public boolean getActivity_user_typeDirtyFlag(){
        return activity_user_typeDirtyFlag ;
    }

    /**
     * 获取 [WEBSITE_PATH]
     */
    @JsonProperty("website_path")
    public String getWebsite_path(){
        return website_path ;
    }

    /**
     * 设置 [WEBSITE_PATH]
     */
    @JsonProperty("website_path")
    public void setWebsite_path(String  website_path){
        this.website_path = website_path ;
        this.website_pathDirtyFlag = true ;
    }

    /**
     * 获取 [WEBSITE_PATH]脏标记
     */
    @JsonIgnore
    public boolean getWebsite_pathDirtyFlag(){
        return website_pathDirtyFlag ;
    }

    /**
     * 获取 [PARTNER_IDS]
     */
    @JsonProperty("partner_ids")
    public String getPartner_ids(){
        return partner_ids ;
    }

    /**
     * 设置 [PARTNER_IDS]
     */
    @JsonProperty("partner_ids")
    public void setPartner_ids(String  partner_ids){
        this.partner_ids = partner_ids ;
        this.partner_idsDirtyFlag = true ;
    }

    /**
     * 获取 [PARTNER_IDS]脏标记
     */
    @JsonIgnore
    public boolean getPartner_idsDirtyFlag(){
        return partner_idsDirtyFlag ;
    }

    /**
     * 获取 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return __last_update ;
    }

    /**
     * 设置 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

    /**
     * 获取 [__LAST_UPDATE]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return __last_updateDirtyFlag ;
    }

    /**
     * 获取 [TRG_DATE_ID]
     */
    @JsonProperty("trg_date_id")
    public Integer getTrg_date_id(){
        return trg_date_id ;
    }

    /**
     * 设置 [TRG_DATE_ID]
     */
    @JsonProperty("trg_date_id")
    public void setTrg_date_id(Integer  trg_date_id){
        this.trg_date_id = trg_date_id ;
        this.trg_date_idDirtyFlag = true ;
    }

    /**
     * 获取 [TRG_DATE_ID]脏标记
     */
    @JsonIgnore
    public boolean getTrg_date_idDirtyFlag(){
        return trg_date_idDirtyFlag ;
    }

    /**
     * 获取 [XML_ID]
     */
    @JsonProperty("xml_id")
    public String getXml_id(){
        return xml_id ;
    }

    /**
     * 设置 [XML_ID]
     */
    @JsonProperty("xml_id")
    public void setXml_id(String  xml_id){
        this.xml_id = xml_id ;
        this.xml_idDirtyFlag = true ;
    }

    /**
     * 获取 [XML_ID]脏标记
     */
    @JsonIgnore
    public boolean getXml_idDirtyFlag(){
        return xml_idDirtyFlag ;
    }

    /**
     * 获取 [LAST_RUN]
     */
    @JsonProperty("last_run")
    public Timestamp getLast_run(){
        return last_run ;
    }

    /**
     * 设置 [LAST_RUN]
     */
    @JsonProperty("last_run")
    public void setLast_run(Timestamp  last_run){
        this.last_run = last_run ;
        this.last_runDirtyFlag = true ;
    }

    /**
     * 获取 [LAST_RUN]脏标记
     */
    @JsonIgnore
    public boolean getLast_runDirtyFlag(){
        return last_runDirtyFlag ;
    }

    /**
     * 获取 [TEMPLATE_ID]
     */
    @JsonProperty("template_id")
    public Integer getTemplate_id(){
        return template_id ;
    }

    /**
     * 设置 [TEMPLATE_ID]
     */
    @JsonProperty("template_id")
    public void setTemplate_id(Integer  template_id){
        this.template_id = template_id ;
        this.template_idDirtyFlag = true ;
    }

    /**
     * 获取 [TEMPLATE_ID]脏标记
     */
    @JsonIgnore
    public boolean getTemplate_idDirtyFlag(){
        return template_idDirtyFlag ;
    }

    /**
     * 获取 [BINDING_TYPE]
     */
    @JsonProperty("binding_type")
    public String getBinding_type(){
        return binding_type ;
    }

    /**
     * 设置 [BINDING_TYPE]
     */
    @JsonProperty("binding_type")
    public void setBinding_type(String  binding_type){
        this.binding_type = binding_type ;
        this.binding_typeDirtyFlag = true ;
    }

    /**
     * 获取 [BINDING_TYPE]脏标记
     */
    @JsonIgnore
    public boolean getBinding_typeDirtyFlag(){
        return binding_typeDirtyFlag ;
    }

    /**
     * 获取 [NAME]
     */
    @JsonProperty("name")
    public String getName(){
        return name ;
    }

    /**
     * 设置 [NAME]
     */
    @JsonProperty("name")
    public void setName(String  name){
        this.name = name ;
        this.nameDirtyFlag = true ;
    }

    /**
     * 获取 [NAME]脏标记
     */
    @JsonIgnore
    public boolean getNameDirtyFlag(){
        return nameDirtyFlag ;
    }

    /**
     * 获取 [MODEL_NAME]
     */
    @JsonProperty("model_name")
    public String getModel_name(){
        return model_name ;
    }

    /**
     * 设置 [MODEL_NAME]
     */
    @JsonProperty("model_name")
    public void setModel_name(String  model_name){
        this.model_name = model_name ;
        this.model_nameDirtyFlag = true ;
    }

    /**
     * 获取 [MODEL_NAME]脏标记
     */
    @JsonIgnore
    public boolean getModel_nameDirtyFlag(){
        return model_nameDirtyFlag ;
    }

    /**
     * 获取 [STATE]
     */
    @JsonProperty("state")
    public String getState(){
        return state ;
    }

    /**
     * 设置 [STATE]
     */
    @JsonProperty("state")
    public void setState(String  state){
        this.state = state ;
        this.stateDirtyFlag = true ;
    }

    /**
     * 获取 [STATE]脏标记
     */
    @JsonIgnore
    public boolean getStateDirtyFlag(){
        return stateDirtyFlag ;
    }

    /**
     * 获取 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return display_name ;
    }

    /**
     * 设置 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

    /**
     * 获取 [DISPLAY_NAME]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return display_nameDirtyFlag ;
    }

    /**
     * 获取 [CHANNEL_IDS]
     */
    @JsonProperty("channel_ids")
    public String getChannel_ids(){
        return channel_ids ;
    }

    /**
     * 设置 [CHANNEL_IDS]
     */
    @JsonProperty("channel_ids")
    public void setChannel_ids(String  channel_ids){
        this.channel_ids = channel_ids ;
        this.channel_idsDirtyFlag = true ;
    }

    /**
     * 获取 [CHANNEL_IDS]脏标记
     */
    @JsonIgnore
    public boolean getChannel_idsDirtyFlag(){
        return channel_idsDirtyFlag ;
    }

    /**
     * 获取 [MODEL_ID]
     */
    @JsonProperty("model_id")
    public Integer getModel_id(){
        return model_id ;
    }

    /**
     * 设置 [MODEL_ID]
     */
    @JsonProperty("model_id")
    public void setModel_id(Integer  model_id){
        this.model_id = model_id ;
        this.model_idDirtyFlag = true ;
    }

    /**
     * 获取 [MODEL_ID]脏标记
     */
    @JsonIgnore
    public boolean getModel_idDirtyFlag(){
        return model_idDirtyFlag ;
    }

    /**
     * 获取 [HELP]
     */
    @JsonProperty("help")
    public String getHelp(){
        return help ;
    }

    /**
     * 设置 [HELP]
     */
    @JsonProperty("help")
    public void setHelp(String  help){
        this.help = help ;
        this.helpDirtyFlag = true ;
    }

    /**
     * 获取 [HELP]脏标记
     */
    @JsonIgnore
    public boolean getHelpDirtyFlag(){
        return helpDirtyFlag ;
    }

    /**
     * 获取 [WEBSITE_URL]
     */
    @JsonProperty("website_url")
    public String getWebsite_url(){
        return website_url ;
    }

    /**
     * 设置 [WEBSITE_URL]
     */
    @JsonProperty("website_url")
    public void setWebsite_url(String  website_url){
        this.website_url = website_url ;
        this.website_urlDirtyFlag = true ;
    }

    /**
     * 获取 [WEBSITE_URL]脏标记
     */
    @JsonIgnore
    public boolean getWebsite_urlDirtyFlag(){
        return website_urlDirtyFlag ;
    }

    /**
     * 获取 [ACTIVITY_USER_ID]
     */
    @JsonProperty("activity_user_id")
    public Integer getActivity_user_id(){
        return activity_user_id ;
    }

    /**
     * 设置 [ACTIVITY_USER_ID]
     */
    @JsonProperty("activity_user_id")
    public void setActivity_user_id(Integer  activity_user_id){
        this.activity_user_id = activity_user_id ;
        this.activity_user_idDirtyFlag = true ;
    }

    /**
     * 获取 [ACTIVITY_USER_ID]脏标记
     */
    @JsonIgnore
    public boolean getActivity_user_idDirtyFlag(){
        return activity_user_idDirtyFlag ;
    }

    /**
     * 获取 [ACTIVITY_DATE_DEADLINE_RANGE]
     */
    @JsonProperty("activity_date_deadline_range")
    public Integer getActivity_date_deadline_range(){
        return activity_date_deadline_range ;
    }

    /**
     * 设置 [ACTIVITY_DATE_DEADLINE_RANGE]
     */
    @JsonProperty("activity_date_deadline_range")
    public void setActivity_date_deadline_range(Integer  activity_date_deadline_range){
        this.activity_date_deadline_range = activity_date_deadline_range ;
        this.activity_date_deadline_rangeDirtyFlag = true ;
    }

    /**
     * 获取 [ACTIVITY_DATE_DEADLINE_RANGE]脏标记
     */
    @JsonIgnore
    public boolean getActivity_date_deadline_rangeDirtyFlag(){
        return activity_date_deadline_rangeDirtyFlag ;
    }

    /**
     * 获取 [TRG_DATE_RANGE]
     */
    @JsonProperty("trg_date_range")
    public Integer getTrg_date_range(){
        return trg_date_range ;
    }

    /**
     * 设置 [TRG_DATE_RANGE]
     */
    @JsonProperty("trg_date_range")
    public void setTrg_date_range(Integer  trg_date_range){
        this.trg_date_range = trg_date_range ;
        this.trg_date_rangeDirtyFlag = true ;
    }

    /**
     * 获取 [TRG_DATE_RANGE]脏标记
     */
    @JsonIgnore
    public boolean getTrg_date_rangeDirtyFlag(){
        return trg_date_rangeDirtyFlag ;
    }

    /**
     * 获取 [TYPE]
     */
    @JsonProperty("type")
    public String getType(){
        return type ;
    }

    /**
     * 设置 [TYPE]
     */
    @JsonProperty("type")
    public void setType(String  type){
        this.type = type ;
        this.typeDirtyFlag = true ;
    }

    /**
     * 获取 [TYPE]脏标记
     */
    @JsonIgnore
    public boolean getTypeDirtyFlag(){
        return typeDirtyFlag ;
    }

    /**
     * 获取 [FILTER_DOMAIN]
     */
    @JsonProperty("filter_domain")
    public String getFilter_domain(){
        return filter_domain ;
    }

    /**
     * 设置 [FILTER_DOMAIN]
     */
    @JsonProperty("filter_domain")
    public void setFilter_domain(String  filter_domain){
        this.filter_domain = filter_domain ;
        this.filter_domainDirtyFlag = true ;
    }

    /**
     * 获取 [FILTER_DOMAIN]脏标记
     */
    @JsonIgnore
    public boolean getFilter_domainDirtyFlag(){
        return filter_domainDirtyFlag ;
    }

    /**
     * 获取 [ACTIVITY_DATE_DEADLINE_RANGE_TYPE]
     */
    @JsonProperty("activity_date_deadline_range_type")
    public String getActivity_date_deadline_range_type(){
        return activity_date_deadline_range_type ;
    }

    /**
     * 设置 [ACTIVITY_DATE_DEADLINE_RANGE_TYPE]
     */
    @JsonProperty("activity_date_deadline_range_type")
    public void setActivity_date_deadline_range_type(String  activity_date_deadline_range_type){
        this.activity_date_deadline_range_type = activity_date_deadline_range_type ;
        this.activity_date_deadline_range_typeDirtyFlag = true ;
    }

    /**
     * 获取 [ACTIVITY_DATE_DEADLINE_RANGE_TYPE]脏标记
     */
    @JsonIgnore
    public boolean getActivity_date_deadline_range_typeDirtyFlag(){
        return activity_date_deadline_range_typeDirtyFlag ;
    }

    /**
     * 获取 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return write_date ;
    }

    /**
     * 设置 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return write_dateDirtyFlag ;
    }

    /**
     * 获取 [CHILD_IDS]
     */
    @JsonProperty("child_ids")
    public String getChild_ids(){
        return child_ids ;
    }

    /**
     * 设置 [CHILD_IDS]
     */
    @JsonProperty("child_ids")
    public void setChild_ids(String  child_ids){
        this.child_ids = child_ids ;
        this.child_idsDirtyFlag = true ;
    }

    /**
     * 获取 [CHILD_IDS]脏标记
     */
    @JsonIgnore
    public boolean getChild_idsDirtyFlag(){
        return child_idsDirtyFlag ;
    }

    /**
     * 获取 [LINK_FIELD_ID]
     */
    @JsonProperty("link_field_id")
    public Integer getLink_field_id(){
        return link_field_id ;
    }

    /**
     * 设置 [LINK_FIELD_ID]
     */
    @JsonProperty("link_field_id")
    public void setLink_field_id(Integer  link_field_id){
        this.link_field_id = link_field_id ;
        this.link_field_idDirtyFlag = true ;
    }

    /**
     * 获取 [LINK_FIELD_ID]脏标记
     */
    @JsonIgnore
    public boolean getLink_field_idDirtyFlag(){
        return link_field_idDirtyFlag ;
    }

    /**
     * 获取 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return create_date ;
    }

    /**
     * 设置 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return create_dateDirtyFlag ;
    }

    /**
     * 获取 [TRIGGER]
     */
    @JsonProperty("trigger")
    public String getTrigger(){
        return trigger ;
    }

    /**
     * 设置 [TRIGGER]
     */
    @JsonProperty("trigger")
    public void setTrigger(String  trigger){
        this.trigger = trigger ;
        this.triggerDirtyFlag = true ;
    }

    /**
     * 获取 [TRIGGER]脏标记
     */
    @JsonIgnore
    public boolean getTriggerDirtyFlag(){
        return triggerDirtyFlag ;
    }

    /**
     * 获取 [ACTION_SERVER_ID]
     */
    @JsonProperty("action_server_id")
    public Integer getAction_server_id(){
        return action_server_id ;
    }

    /**
     * 设置 [ACTION_SERVER_ID]
     */
    @JsonProperty("action_server_id")
    public void setAction_server_id(Integer  action_server_id){
        this.action_server_id = action_server_id ;
        this.action_server_idDirtyFlag = true ;
    }

    /**
     * 获取 [ACTION_SERVER_ID]脏标记
     */
    @JsonIgnore
    public boolean getAction_server_idDirtyFlag(){
        return action_server_idDirtyFlag ;
    }

    /**
     * 获取 [CRUD_MODEL_ID]
     */
    @JsonProperty("crud_model_id")
    public Integer getCrud_model_id(){
        return crud_model_id ;
    }

    /**
     * 设置 [CRUD_MODEL_ID]
     */
    @JsonProperty("crud_model_id")
    public void setCrud_model_id(Integer  crud_model_id){
        this.crud_model_id = crud_model_id ;
        this.crud_model_idDirtyFlag = true ;
    }

    /**
     * 获取 [CRUD_MODEL_ID]脏标记
     */
    @JsonIgnore
    public boolean getCrud_model_idDirtyFlag(){
        return crud_model_idDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return create_uid_text ;
    }

    /**
     * 设置 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return create_uid_textDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return write_uid_text ;
    }

    /**
     * 设置 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return write_uid_textDirtyFlag ;
    }

    /**
     * 获取 [TRG_DATE_CALENDAR_ID_TEXT]
     */
    @JsonProperty("trg_date_calendar_id_text")
    public String getTrg_date_calendar_id_text(){
        return trg_date_calendar_id_text ;
    }

    /**
     * 设置 [TRG_DATE_CALENDAR_ID_TEXT]
     */
    @JsonProperty("trg_date_calendar_id_text")
    public void setTrg_date_calendar_id_text(String  trg_date_calendar_id_text){
        this.trg_date_calendar_id_text = trg_date_calendar_id_text ;
        this.trg_date_calendar_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [TRG_DATE_CALENDAR_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getTrg_date_calendar_id_textDirtyFlag(){
        return trg_date_calendar_id_textDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return write_uid ;
    }

    /**
     * 设置 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return write_uidDirtyFlag ;
    }

    /**
     * 获取 [TRG_DATE_CALENDAR_ID]
     */
    @JsonProperty("trg_date_calendar_id")
    public Integer getTrg_date_calendar_id(){
        return trg_date_calendar_id ;
    }

    /**
     * 设置 [TRG_DATE_CALENDAR_ID]
     */
    @JsonProperty("trg_date_calendar_id")
    public void setTrg_date_calendar_id(Integer  trg_date_calendar_id){
        this.trg_date_calendar_id = trg_date_calendar_id ;
        this.trg_date_calendar_idDirtyFlag = true ;
    }

    /**
     * 获取 [TRG_DATE_CALENDAR_ID]脏标记
     */
    @JsonIgnore
    public boolean getTrg_date_calendar_idDirtyFlag(){
        return trg_date_calendar_idDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return create_uid ;
    }

    /**
     * 设置 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return create_uidDirtyFlag ;
    }



    public Base_automation toDO() {
        Base_automation srfdomain = new Base_automation();
        if(getBinding_model_idDirtyFlag())
            srfdomain.setBinding_model_id(binding_model_id);
        if(getSequenceDirtyFlag())
            srfdomain.setSequence(sequence);
        if(getFilter_pre_domainDirtyFlag())
            srfdomain.setFilter_pre_domain(filter_pre_domain);
        if(getOn_change_fieldsDirtyFlag())
            srfdomain.setOn_change_fields(on_change_fields);
        if(getActivity_summaryDirtyFlag())
            srfdomain.setActivity_summary(activity_summary);
        if(getActivity_user_field_nameDirtyFlag())
            srfdomain.setActivity_user_field_name(activity_user_field_name);
        if(getWebsite_publishedDirtyFlag())
            srfdomain.setWebsite_published(website_published);
        if(getUsageDirtyFlag())
            srfdomain.setUsage(usage);
        if(getCrud_model_nameDirtyFlag())
            srfdomain.setCrud_model_name(crud_model_name);
        if(getTrg_date_range_typeDirtyFlag())
            srfdomain.setTrg_date_range_type(trg_date_range_type);
        if(getIdDirtyFlag())
            srfdomain.setId(id);
        if(getFields_linesDirtyFlag())
            srfdomain.setFields_lines(fields_lines);
        if(getActivity_noteDirtyFlag())
            srfdomain.setActivity_note(activity_note);
        if(getActiveDirtyFlag())
            srfdomain.setActive(active);
        if(getActivity_type_idDirtyFlag())
            srfdomain.setActivity_type_id(activity_type_id);
        if(getCodeDirtyFlag())
            srfdomain.setCode(code);
        if(getActivity_user_typeDirtyFlag())
            srfdomain.setActivity_user_type(activity_user_type);
        if(getWebsite_pathDirtyFlag())
            srfdomain.setWebsite_path(website_path);
        if(getPartner_idsDirtyFlag())
            srfdomain.setPartner_ids(partner_ids);
        if(get__last_updateDirtyFlag())
            srfdomain.set__last_update(__last_update);
        if(getTrg_date_idDirtyFlag())
            srfdomain.setTrg_date_id(trg_date_id);
        if(getXml_idDirtyFlag())
            srfdomain.setXml_id(xml_id);
        if(getLast_runDirtyFlag())
            srfdomain.setLast_run(last_run);
        if(getTemplate_idDirtyFlag())
            srfdomain.setTemplate_id(template_id);
        if(getBinding_typeDirtyFlag())
            srfdomain.setBinding_type(binding_type);
        if(getNameDirtyFlag())
            srfdomain.setName(name);
        if(getModel_nameDirtyFlag())
            srfdomain.setModel_name(model_name);
        if(getStateDirtyFlag())
            srfdomain.setState(state);
        if(getDisplay_nameDirtyFlag())
            srfdomain.setDisplay_name(display_name);
        if(getChannel_idsDirtyFlag())
            srfdomain.setChannel_ids(channel_ids);
        if(getModel_idDirtyFlag())
            srfdomain.setModel_id(model_id);
        if(getHelpDirtyFlag())
            srfdomain.setHelp(help);
        if(getWebsite_urlDirtyFlag())
            srfdomain.setWebsite_url(website_url);
        if(getActivity_user_idDirtyFlag())
            srfdomain.setActivity_user_id(activity_user_id);
        if(getActivity_date_deadline_rangeDirtyFlag())
            srfdomain.setActivity_date_deadline_range(activity_date_deadline_range);
        if(getTrg_date_rangeDirtyFlag())
            srfdomain.setTrg_date_range(trg_date_range);
        if(getTypeDirtyFlag())
            srfdomain.setType(type);
        if(getFilter_domainDirtyFlag())
            srfdomain.setFilter_domain(filter_domain);
        if(getActivity_date_deadline_range_typeDirtyFlag())
            srfdomain.setActivity_date_deadline_range_type(activity_date_deadline_range_type);
        if(getWrite_dateDirtyFlag())
            srfdomain.setWrite_date(write_date);
        if(getChild_idsDirtyFlag())
            srfdomain.setChild_ids(child_ids);
        if(getLink_field_idDirtyFlag())
            srfdomain.setLink_field_id(link_field_id);
        if(getCreate_dateDirtyFlag())
            srfdomain.setCreate_date(create_date);
        if(getTriggerDirtyFlag())
            srfdomain.setTrigger(trigger);
        if(getAction_server_idDirtyFlag())
            srfdomain.setAction_server_id(action_server_id);
        if(getCrud_model_idDirtyFlag())
            srfdomain.setCrud_model_id(crud_model_id);
        if(getCreate_uid_textDirtyFlag())
            srfdomain.setCreate_uid_text(create_uid_text);
        if(getWrite_uid_textDirtyFlag())
            srfdomain.setWrite_uid_text(write_uid_text);
        if(getTrg_date_calendar_id_textDirtyFlag())
            srfdomain.setTrg_date_calendar_id_text(trg_date_calendar_id_text);
        if(getWrite_uidDirtyFlag())
            srfdomain.setWrite_uid(write_uid);
        if(getTrg_date_calendar_idDirtyFlag())
            srfdomain.setTrg_date_calendar_id(trg_date_calendar_id);
        if(getCreate_uidDirtyFlag())
            srfdomain.setCreate_uid(create_uid);

        return srfdomain;
    }

    public void fromDO(Base_automation srfdomain) {
        if(srfdomain == null )
          return ;
        if(srfdomain.getBinding_model_idDirtyFlag())
            this.setBinding_model_id(srfdomain.getBinding_model_id());
        if(srfdomain.getSequenceDirtyFlag())
            this.setSequence(srfdomain.getSequence());
        if(srfdomain.getFilter_pre_domainDirtyFlag())
            this.setFilter_pre_domain(srfdomain.getFilter_pre_domain());
        if(srfdomain.getOn_change_fieldsDirtyFlag())
            this.setOn_change_fields(srfdomain.getOn_change_fields());
        if(srfdomain.getActivity_summaryDirtyFlag())
            this.setActivity_summary(srfdomain.getActivity_summary());
        if(srfdomain.getActivity_user_field_nameDirtyFlag())
            this.setActivity_user_field_name(srfdomain.getActivity_user_field_name());
        if(srfdomain.getWebsite_publishedDirtyFlag())
            this.setWebsite_published(srfdomain.getWebsite_published());
        if(srfdomain.getUsageDirtyFlag())
            this.setUsage(srfdomain.getUsage());
        if(srfdomain.getCrud_model_nameDirtyFlag())
            this.setCrud_model_name(srfdomain.getCrud_model_name());
        if(srfdomain.getTrg_date_range_typeDirtyFlag())
            this.setTrg_date_range_type(srfdomain.getTrg_date_range_type());
        if(srfdomain.getIdDirtyFlag())
            this.setId(srfdomain.getId());
        if(srfdomain.getFields_linesDirtyFlag())
            this.setFields_lines(srfdomain.getFields_lines());
        if(srfdomain.getActivity_noteDirtyFlag())
            this.setActivity_note(srfdomain.getActivity_note());
        if(srfdomain.getActiveDirtyFlag())
            this.setActive(srfdomain.getActive());
        if(srfdomain.getActivity_type_idDirtyFlag())
            this.setActivity_type_id(srfdomain.getActivity_type_id());
        if(srfdomain.getCodeDirtyFlag())
            this.setCode(srfdomain.getCode());
        if(srfdomain.getActivity_user_typeDirtyFlag())
            this.setActivity_user_type(srfdomain.getActivity_user_type());
        if(srfdomain.getWebsite_pathDirtyFlag())
            this.setWebsite_path(srfdomain.getWebsite_path());
        if(srfdomain.getPartner_idsDirtyFlag())
            this.setPartner_ids(srfdomain.getPartner_ids());
        if(srfdomain.get__last_updateDirtyFlag())
            this.set__last_update(srfdomain.get__last_update());
        if(srfdomain.getTrg_date_idDirtyFlag())
            this.setTrg_date_id(srfdomain.getTrg_date_id());
        if(srfdomain.getXml_idDirtyFlag())
            this.setXml_id(srfdomain.getXml_id());
        if(srfdomain.getLast_runDirtyFlag())
            this.setLast_run(srfdomain.getLast_run());
        if(srfdomain.getTemplate_idDirtyFlag())
            this.setTemplate_id(srfdomain.getTemplate_id());
        if(srfdomain.getBinding_typeDirtyFlag())
            this.setBinding_type(srfdomain.getBinding_type());
        if(srfdomain.getNameDirtyFlag())
            this.setName(srfdomain.getName());
        if(srfdomain.getModel_nameDirtyFlag())
            this.setModel_name(srfdomain.getModel_name());
        if(srfdomain.getStateDirtyFlag())
            this.setState(srfdomain.getState());
        if(srfdomain.getDisplay_nameDirtyFlag())
            this.setDisplay_name(srfdomain.getDisplay_name());
        if(srfdomain.getChannel_idsDirtyFlag())
            this.setChannel_ids(srfdomain.getChannel_ids());
        if(srfdomain.getModel_idDirtyFlag())
            this.setModel_id(srfdomain.getModel_id());
        if(srfdomain.getHelpDirtyFlag())
            this.setHelp(srfdomain.getHelp());
        if(srfdomain.getWebsite_urlDirtyFlag())
            this.setWebsite_url(srfdomain.getWebsite_url());
        if(srfdomain.getActivity_user_idDirtyFlag())
            this.setActivity_user_id(srfdomain.getActivity_user_id());
        if(srfdomain.getActivity_date_deadline_rangeDirtyFlag())
            this.setActivity_date_deadline_range(srfdomain.getActivity_date_deadline_range());
        if(srfdomain.getTrg_date_rangeDirtyFlag())
            this.setTrg_date_range(srfdomain.getTrg_date_range());
        if(srfdomain.getTypeDirtyFlag())
            this.setType(srfdomain.getType());
        if(srfdomain.getFilter_domainDirtyFlag())
            this.setFilter_domain(srfdomain.getFilter_domain());
        if(srfdomain.getActivity_date_deadline_range_typeDirtyFlag())
            this.setActivity_date_deadline_range_type(srfdomain.getActivity_date_deadline_range_type());
        if(srfdomain.getWrite_dateDirtyFlag())
            this.setWrite_date(srfdomain.getWrite_date());
        if(srfdomain.getChild_idsDirtyFlag())
            this.setChild_ids(srfdomain.getChild_ids());
        if(srfdomain.getLink_field_idDirtyFlag())
            this.setLink_field_id(srfdomain.getLink_field_id());
        if(srfdomain.getCreate_dateDirtyFlag())
            this.setCreate_date(srfdomain.getCreate_date());
        if(srfdomain.getTriggerDirtyFlag())
            this.setTrigger(srfdomain.getTrigger());
        if(srfdomain.getAction_server_idDirtyFlag())
            this.setAction_server_id(srfdomain.getAction_server_id());
        if(srfdomain.getCrud_model_idDirtyFlag())
            this.setCrud_model_id(srfdomain.getCrud_model_id());
        if(srfdomain.getCreate_uid_textDirtyFlag())
            this.setCreate_uid_text(srfdomain.getCreate_uid_text());
        if(srfdomain.getWrite_uid_textDirtyFlag())
            this.setWrite_uid_text(srfdomain.getWrite_uid_text());
        if(srfdomain.getTrg_date_calendar_id_textDirtyFlag())
            this.setTrg_date_calendar_id_text(srfdomain.getTrg_date_calendar_id_text());
        if(srfdomain.getWrite_uidDirtyFlag())
            this.setWrite_uid(srfdomain.getWrite_uid());
        if(srfdomain.getTrg_date_calendar_idDirtyFlag())
            this.setTrg_date_calendar_id(srfdomain.getTrg_date_calendar_id());
        if(srfdomain.getCreate_uidDirtyFlag())
            this.setCreate_uid(srfdomain.getCreate_uid());

    }

    public List<Base_automationDTO> fromDOPage(List<Base_automation> poPage)   {
        if(poPage == null)
            return null;
        List<Base_automationDTO> dtos=new ArrayList<Base_automationDTO>();
        for(Base_automation domain : poPage) {
            Base_automationDTO dto = new Base_automationDTO();
            dto.fromDO(domain);
            dtos.add(dto);
        }
        return dtos;
    }
}

