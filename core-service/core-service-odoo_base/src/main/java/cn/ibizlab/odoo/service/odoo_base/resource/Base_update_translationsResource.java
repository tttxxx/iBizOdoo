package cn.ibizlab.odoo.service.odoo_base.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_base.dto.Base_update_translationsDTO;
import cn.ibizlab.odoo.core.odoo_base.domain.Base_update_translations;
import cn.ibizlab.odoo.core.odoo_base.service.IBase_update_translationsService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_base.filter.Base_update_translationsSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Base_update_translations" })
@RestController
@RequestMapping("")
public class Base_update_translationsResource {

    @Autowired
    private IBase_update_translationsService base_update_translationsService;

    public IBase_update_translationsService getBase_update_translationsService() {
        return this.base_update_translationsService;
    }

    @ApiOperation(value = "更新数据", tags = {"Base_update_translations" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_base/base_update_translations/{base_update_translations_id}")

    public ResponseEntity<Base_update_translationsDTO> update(@PathVariable("base_update_translations_id") Integer base_update_translations_id, @RequestBody Base_update_translationsDTO base_update_translationsdto) {
		Base_update_translations domain = base_update_translationsdto.toDO();
        domain.setId(base_update_translations_id);
		base_update_translationsService.update(domain);
		Base_update_translationsDTO dto = new Base_update_translationsDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取数据", tags = {"Base_update_translations" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_base/base_update_translations/{base_update_translations_id}")
    public ResponseEntity<Base_update_translationsDTO> get(@PathVariable("base_update_translations_id") Integer base_update_translations_id) {
        Base_update_translationsDTO dto = new Base_update_translationsDTO();
        Base_update_translations domain = base_update_translationsService.get(base_update_translations_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批建立数据", tags = {"Base_update_translations" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_base/base_update_translations/createBatch")
    public ResponseEntity<Boolean> createBatchBase_update_translations(@RequestBody List<Base_update_translationsDTO> base_update_translationsdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "建立数据", tags = {"Base_update_translations" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_base/base_update_translations")

    public ResponseEntity<Base_update_translationsDTO> create(@RequestBody Base_update_translationsDTO base_update_translationsdto) {
        Base_update_translationsDTO dto = new Base_update_translationsDTO();
        Base_update_translations domain = base_update_translationsdto.toDO();
		base_update_translationsService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批删除数据", tags = {"Base_update_translations" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_base/base_update_translations/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Base_update_translationsDTO> base_update_translationsdtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "删除数据", tags = {"Base_update_translations" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_base/base_update_translations/{base_update_translations_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("base_update_translations_id") Integer base_update_translations_id) {
        Base_update_translationsDTO base_update_translationsdto = new Base_update_translationsDTO();
		Base_update_translations domain = new Base_update_translations();
		base_update_translationsdto.setId(base_update_translations_id);
		domain.setId(base_update_translations_id);
        Boolean rst = base_update_translationsService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "批更新数据", tags = {"Base_update_translations" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_base/base_update_translations/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Base_update_translationsDTO> base_update_translationsdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Base_update_translations" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_base/base_update_translations/fetchdefault")
	public ResponseEntity<Page<Base_update_translationsDTO>> fetchDefault(Base_update_translationsSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Base_update_translationsDTO> list = new ArrayList<Base_update_translationsDTO>();
        
        Page<Base_update_translations> domains = base_update_translationsService.searchDefault(context) ;
        for(Base_update_translations base_update_translations : domains.getContent()){
            Base_update_translationsDTO dto = new Base_update_translationsDTO();
            dto.fromDO(base_update_translations);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
