package cn.ibizlab.odoo.service.odoo_base.config;

import org.springframework.context.annotation.ComponentScan;

@ComponentScan("cn.ibizlab.odoo.service.odoo_base")
public class odoo_baseRestConfiguration {

}
