package cn.ibizlab.odoo.service.odoo_base.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_base.dto.Res_partner_bankDTO;
import cn.ibizlab.odoo.core.odoo_base.domain.Res_partner_bank;
import cn.ibizlab.odoo.core.odoo_base.service.IRes_partner_bankService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_base.filter.Res_partner_bankSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Res_partner_bank" })
@RestController
@RequestMapping("")
public class Res_partner_bankResource {

    @Autowired
    private IRes_partner_bankService res_partner_bankService;

    public IRes_partner_bankService getRes_partner_bankService() {
        return this.res_partner_bankService;
    }

    @ApiOperation(value = "更新数据", tags = {"Res_partner_bank" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_base/res_partner_banks/{res_partner_bank_id}")

    public ResponseEntity<Res_partner_bankDTO> update(@PathVariable("res_partner_bank_id") Integer res_partner_bank_id, @RequestBody Res_partner_bankDTO res_partner_bankdto) {
		Res_partner_bank domain = res_partner_bankdto.toDO();
        domain.setId(res_partner_bank_id);
		res_partner_bankService.update(domain);
		Res_partner_bankDTO dto = new Res_partner_bankDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批更新数据", tags = {"Res_partner_bank" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_base/res_partner_banks/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Res_partner_bankDTO> res_partner_bankdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "删除数据", tags = {"Res_partner_bank" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_base/res_partner_banks/{res_partner_bank_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("res_partner_bank_id") Integer res_partner_bank_id) {
        Res_partner_bankDTO res_partner_bankdto = new Res_partner_bankDTO();
		Res_partner_bank domain = new Res_partner_bank();
		res_partner_bankdto.setId(res_partner_bank_id);
		domain.setId(res_partner_bank_id);
        Boolean rst = res_partner_bankService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "建立数据", tags = {"Res_partner_bank" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_base/res_partner_banks")

    public ResponseEntity<Res_partner_bankDTO> create(@RequestBody Res_partner_bankDTO res_partner_bankdto) {
        Res_partner_bankDTO dto = new Res_partner_bankDTO();
        Res_partner_bank domain = res_partner_bankdto.toDO();
		res_partner_bankService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批建立数据", tags = {"Res_partner_bank" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_base/res_partner_banks/createBatch")
    public ResponseEntity<Boolean> createBatchRes_partner_bank(@RequestBody List<Res_partner_bankDTO> res_partner_bankdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "批删除数据", tags = {"Res_partner_bank" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_base/res_partner_banks/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Res_partner_bankDTO> res_partner_bankdtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "获取数据", tags = {"Res_partner_bank" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_base/res_partner_banks/{res_partner_bank_id}")
    public ResponseEntity<Res_partner_bankDTO> get(@PathVariable("res_partner_bank_id") Integer res_partner_bank_id) {
        Res_partner_bankDTO dto = new Res_partner_bankDTO();
        Res_partner_bank domain = res_partner_bankService.get(res_partner_bank_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Res_partner_bank" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_base/res_partner_banks/fetchdefault")
	public ResponseEntity<Page<Res_partner_bankDTO>> fetchDefault(Res_partner_bankSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Res_partner_bankDTO> list = new ArrayList<Res_partner_bankDTO>();
        
        Page<Res_partner_bank> domains = res_partner_bankService.searchDefault(context) ;
        for(Res_partner_bank res_partner_bank : domains.getContent()){
            Res_partner_bankDTO dto = new Res_partner_bankDTO();
            dto.fromDO(res_partner_bank);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
