package cn.ibizlab.odoo.service.odoo_base.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_base.dto.Base_language_installDTO;
import cn.ibizlab.odoo.core.odoo_base.domain.Base_language_install;
import cn.ibizlab.odoo.core.odoo_base.service.IBase_language_installService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_base.filter.Base_language_installSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Base_language_install" })
@RestController
@RequestMapping("")
public class Base_language_installResource {

    @Autowired
    private IBase_language_installService base_language_installService;

    public IBase_language_installService getBase_language_installService() {
        return this.base_language_installService;
    }

    @ApiOperation(value = "建立数据", tags = {"Base_language_install" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_base/base_language_installs")

    public ResponseEntity<Base_language_installDTO> create(@RequestBody Base_language_installDTO base_language_installdto) {
        Base_language_installDTO dto = new Base_language_installDTO();
        Base_language_install domain = base_language_installdto.toDO();
		base_language_installService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批删除数据", tags = {"Base_language_install" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_base/base_language_installs/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Base_language_installDTO> base_language_installdtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "更新数据", tags = {"Base_language_install" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_base/base_language_installs/{base_language_install_id}")

    public ResponseEntity<Base_language_installDTO> update(@PathVariable("base_language_install_id") Integer base_language_install_id, @RequestBody Base_language_installDTO base_language_installdto) {
		Base_language_install domain = base_language_installdto.toDO();
        domain.setId(base_language_install_id);
		base_language_installService.update(domain);
		Base_language_installDTO dto = new Base_language_installDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批更新数据", tags = {"Base_language_install" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_base/base_language_installs/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Base_language_installDTO> base_language_installdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "批建立数据", tags = {"Base_language_install" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_base/base_language_installs/createBatch")
    public ResponseEntity<Boolean> createBatchBase_language_install(@RequestBody List<Base_language_installDTO> base_language_installdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "获取数据", tags = {"Base_language_install" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_base/base_language_installs/{base_language_install_id}")
    public ResponseEntity<Base_language_installDTO> get(@PathVariable("base_language_install_id") Integer base_language_install_id) {
        Base_language_installDTO dto = new Base_language_installDTO();
        Base_language_install domain = base_language_installService.get(base_language_install_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "删除数据", tags = {"Base_language_install" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_base/base_language_installs/{base_language_install_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("base_language_install_id") Integer base_language_install_id) {
        Base_language_installDTO base_language_installdto = new Base_language_installDTO();
		Base_language_install domain = new Base_language_install();
		base_language_installdto.setId(base_language_install_id);
		domain.setId(base_language_install_id);
        Boolean rst = base_language_installService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

	@ApiOperation(value = "获取默认查询", tags = {"Base_language_install" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_base/base_language_installs/fetchdefault")
	public ResponseEntity<Page<Base_language_installDTO>> fetchDefault(Base_language_installSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Base_language_installDTO> list = new ArrayList<Base_language_installDTO>();
        
        Page<Base_language_install> domains = base_language_installService.searchDefault(context) ;
        for(Base_language_install base_language_install : domains.getContent()){
            Base_language_installDTO dto = new Base_language_installDTO();
            dto.fromDO(base_language_install);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
