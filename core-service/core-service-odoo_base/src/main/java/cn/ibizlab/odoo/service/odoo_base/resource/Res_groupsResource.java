package cn.ibizlab.odoo.service.odoo_base.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_base.dto.Res_groupsDTO;
import cn.ibizlab.odoo.core.odoo_base.domain.Res_groups;
import cn.ibizlab.odoo.core.odoo_base.service.IRes_groupsService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_base.filter.Res_groupsSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Res_groups" })
@RestController
@RequestMapping("")
public class Res_groupsResource {

    @Autowired
    private IRes_groupsService res_groupsService;

    public IRes_groupsService getRes_groupsService() {
        return this.res_groupsService;
    }

    @ApiOperation(value = "获取数据", tags = {"Res_groups" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_base/res_groups/{res_groups_id}")
    public ResponseEntity<Res_groupsDTO> get(@PathVariable("res_groups_id") Integer res_groups_id) {
        Res_groupsDTO dto = new Res_groupsDTO();
        Res_groups domain = res_groupsService.get(res_groups_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批更新数据", tags = {"Res_groups" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_base/res_groups/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Res_groupsDTO> res_groupsdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "更新数据", tags = {"Res_groups" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_base/res_groups/{res_groups_id}")

    public ResponseEntity<Res_groupsDTO> update(@PathVariable("res_groups_id") Integer res_groups_id, @RequestBody Res_groupsDTO res_groupsdto) {
		Res_groups domain = res_groupsdto.toDO();
        domain.setId(res_groups_id);
		res_groupsService.update(domain);
		Res_groupsDTO dto = new Res_groupsDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批建立数据", tags = {"Res_groups" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_base/res_groups/createBatch")
    public ResponseEntity<Boolean> createBatchRes_groups(@RequestBody List<Res_groupsDTO> res_groupsdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "删除数据", tags = {"Res_groups" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_base/res_groups/{res_groups_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("res_groups_id") Integer res_groups_id) {
        Res_groupsDTO res_groupsdto = new Res_groupsDTO();
		Res_groups domain = new Res_groups();
		res_groupsdto.setId(res_groups_id);
		domain.setId(res_groups_id);
        Boolean rst = res_groupsService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "批删除数据", tags = {"Res_groups" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_base/res_groups/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Res_groupsDTO> res_groupsdtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "建立数据", tags = {"Res_groups" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_base/res_groups")

    public ResponseEntity<Res_groupsDTO> create(@RequestBody Res_groupsDTO res_groupsdto) {
        Res_groupsDTO dto = new Res_groupsDTO();
        Res_groups domain = res_groupsdto.toDO();
		res_groupsService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Res_groups" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_base/res_groups/fetchdefault")
	public ResponseEntity<Page<Res_groupsDTO>> fetchDefault(Res_groupsSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Res_groupsDTO> list = new ArrayList<Res_groupsDTO>();
        
        Page<Res_groups> domains = res_groupsService.searchDefault(context) ;
        for(Res_groups res_groups : domains.getContent()){
            Res_groupsDTO dto = new Res_groupsDTO();
            dto.fromDO(res_groups);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
