package cn.ibizlab.odoo.service.odoo_base.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_base.dto.Res_currencyDTO;
import cn.ibizlab.odoo.core.odoo_base.domain.Res_currency;
import cn.ibizlab.odoo.core.odoo_base.service.IRes_currencyService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_base.filter.Res_currencySearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Res_currency" })
@RestController
@RequestMapping("")
public class Res_currencyResource {

    @Autowired
    private IRes_currencyService res_currencyService;

    public IRes_currencyService getRes_currencyService() {
        return this.res_currencyService;
    }

    @ApiOperation(value = "获取数据", tags = {"Res_currency" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_base/res_currencies/{res_currency_id}")
    public ResponseEntity<Res_currencyDTO> get(@PathVariable("res_currency_id") Integer res_currency_id) {
        Res_currencyDTO dto = new Res_currencyDTO();
        Res_currency domain = res_currencyService.get(res_currency_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "更新数据", tags = {"Res_currency" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_base/res_currencies/{res_currency_id}")

    public ResponseEntity<Res_currencyDTO> update(@PathVariable("res_currency_id") Integer res_currency_id, @RequestBody Res_currencyDTO res_currencydto) {
		Res_currency domain = res_currencydto.toDO();
        domain.setId(res_currency_id);
		res_currencyService.update(domain);
		Res_currencyDTO dto = new Res_currencyDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "删除数据", tags = {"Res_currency" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_base/res_currencies/{res_currency_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("res_currency_id") Integer res_currency_id) {
        Res_currencyDTO res_currencydto = new Res_currencyDTO();
		Res_currency domain = new Res_currency();
		res_currencydto.setId(res_currency_id);
		domain.setId(res_currency_id);
        Boolean rst = res_currencyService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "批建立数据", tags = {"Res_currency" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_base/res_currencies/createBatch")
    public ResponseEntity<Boolean> createBatchRes_currency(@RequestBody List<Res_currencyDTO> res_currencydtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "批删除数据", tags = {"Res_currency" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_base/res_currencies/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Res_currencyDTO> res_currencydtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "建立数据", tags = {"Res_currency" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_base/res_currencies")

    public ResponseEntity<Res_currencyDTO> create(@RequestBody Res_currencyDTO res_currencydto) {
        Res_currencyDTO dto = new Res_currencyDTO();
        Res_currency domain = res_currencydto.toDO();
		res_currencyService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批更新数据", tags = {"Res_currency" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_base/res_currencies/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Res_currencyDTO> res_currencydtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Res_currency" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_base/res_currencies/fetchdefault")
	public ResponseEntity<Page<Res_currencyDTO>> fetchDefault(Res_currencySearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Res_currencyDTO> list = new ArrayList<Res_currencyDTO>();
        
        Page<Res_currency> domains = res_currencyService.searchDefault(context) ;
        for(Res_currency res_currency : domains.getContent()){
            Res_currencyDTO dto = new Res_currencyDTO();
            dto.fromDO(res_currency);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
