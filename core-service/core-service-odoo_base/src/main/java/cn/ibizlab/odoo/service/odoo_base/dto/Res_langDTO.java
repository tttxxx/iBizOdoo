package cn.ibizlab.odoo.service.odoo_base.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.core.odoo_base.valuerule.anno.res_lang.*;
import cn.ibizlab.odoo.core.odoo_base.domain.Res_lang;
import org.springframework.cglib.beans.BeanCopier;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 服务DTO对象[Res_langDTO]
 */
public class Res_langDTO implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [TRANSLATABLE]
     *
     */
    @Res_langTranslatableDefault(info = "默认规则")
    private String translatable;

    @JsonIgnore
    private boolean translatableDirtyFlag;

    /**
     * 属性 [THOUSANDS_SEP]
     *
     */
    @Res_langThousands_sepDefault(info = "默认规则")
    private String thousands_sep;

    @JsonIgnore
    private boolean thousands_sepDirtyFlag;

    /**
     * 属性 [ISO_CODE]
     *
     */
    @Res_langIso_codeDefault(info = "默认规则")
    private String iso_code;

    @JsonIgnore
    private boolean iso_codeDirtyFlag;

    /**
     * 属性 [DECIMAL_POINT]
     *
     */
    @Res_langDecimal_pointDefault(info = "默认规则")
    private String decimal_point;

    @JsonIgnore
    private boolean decimal_pointDirtyFlag;

    /**
     * 属性 [GROUPING]
     *
     */
    @Res_langGroupingDefault(info = "默认规则")
    private String grouping;

    @JsonIgnore
    private boolean groupingDirtyFlag;

    /**
     * 属性 [CREATE_DATE]
     *
     */
    @Res_langCreate_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp create_date;

    @JsonIgnore
    private boolean create_dateDirtyFlag;

    /**
     * 属性 [WRITE_DATE]
     *
     */
    @Res_langWrite_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp write_date;

    @JsonIgnore
    private boolean write_dateDirtyFlag;

    /**
     * 属性 [DIRECTION]
     *
     */
    @Res_langDirectionDefault(info = "默认规则")
    private String direction;

    @JsonIgnore
    private boolean directionDirtyFlag;

    /**
     * 属性 [DATE_FORMAT]
     *
     */
    @Res_langDate_formatDefault(info = "默认规则")
    private String date_format;

    @JsonIgnore
    private boolean date_formatDirtyFlag;

    /**
     * 属性 [TIME_FORMAT]
     *
     */
    @Res_langTime_formatDefault(info = "默认规则")
    private String time_format;

    @JsonIgnore
    private boolean time_formatDirtyFlag;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @Res_langDisplay_nameDefault(info = "默认规则")
    private String display_name;

    @JsonIgnore
    private boolean display_nameDirtyFlag;

    /**
     * 属性 [ACTIVE]
     *
     */
    @Res_langActiveDefault(info = "默认规则")
    private String active;

    @JsonIgnore
    private boolean activeDirtyFlag;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @Res_lang__last_updateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp __last_update;

    @JsonIgnore
    private boolean __last_updateDirtyFlag;

    /**
     * 属性 [CODE]
     *
     */
    @Res_langCodeDefault(info = "默认规则")
    private String code;

    @JsonIgnore
    private boolean codeDirtyFlag;

    /**
     * 属性 [ID]
     *
     */
    @Res_langIdDefault(info = "默认规则")
    private Integer id;

    @JsonIgnore
    private boolean idDirtyFlag;

    /**
     * 属性 [WEEK_START]
     *
     */
    @Res_langWeek_startDefault(info = "默认规则")
    private String week_start;

    @JsonIgnore
    private boolean week_startDirtyFlag;

    /**
     * 属性 [NAME]
     *
     */
    @Res_langNameDefault(info = "默认规则")
    private String name;

    @JsonIgnore
    private boolean nameDirtyFlag;

    /**
     * 属性 [CREATE_UID_TEXT]
     *
     */
    @Res_langCreate_uid_textDefault(info = "默认规则")
    private String create_uid_text;

    @JsonIgnore
    private boolean create_uid_textDirtyFlag;

    /**
     * 属性 [WRITE_UID_TEXT]
     *
     */
    @Res_langWrite_uid_textDefault(info = "默认规则")
    private String write_uid_text;

    @JsonIgnore
    private boolean write_uid_textDirtyFlag;

    /**
     * 属性 [WRITE_UID]
     *
     */
    @Res_langWrite_uidDefault(info = "默认规则")
    private Integer write_uid;

    @JsonIgnore
    private boolean write_uidDirtyFlag;

    /**
     * 属性 [CREATE_UID]
     *
     */
    @Res_langCreate_uidDefault(info = "默认规则")
    private Integer create_uid;

    @JsonIgnore
    private boolean create_uidDirtyFlag;


    /**
     * 获取 [TRANSLATABLE]
     */
    @JsonProperty("translatable")
    public String getTranslatable(){
        return translatable ;
    }

    /**
     * 设置 [TRANSLATABLE]
     */
    @JsonProperty("translatable")
    public void setTranslatable(String  translatable){
        this.translatable = translatable ;
        this.translatableDirtyFlag = true ;
    }

    /**
     * 获取 [TRANSLATABLE]脏标记
     */
    @JsonIgnore
    public boolean getTranslatableDirtyFlag(){
        return translatableDirtyFlag ;
    }

    /**
     * 获取 [THOUSANDS_SEP]
     */
    @JsonProperty("thousands_sep")
    public String getThousands_sep(){
        return thousands_sep ;
    }

    /**
     * 设置 [THOUSANDS_SEP]
     */
    @JsonProperty("thousands_sep")
    public void setThousands_sep(String  thousands_sep){
        this.thousands_sep = thousands_sep ;
        this.thousands_sepDirtyFlag = true ;
    }

    /**
     * 获取 [THOUSANDS_SEP]脏标记
     */
    @JsonIgnore
    public boolean getThousands_sepDirtyFlag(){
        return thousands_sepDirtyFlag ;
    }

    /**
     * 获取 [ISO_CODE]
     */
    @JsonProperty("iso_code")
    public String getIso_code(){
        return iso_code ;
    }

    /**
     * 设置 [ISO_CODE]
     */
    @JsonProperty("iso_code")
    public void setIso_code(String  iso_code){
        this.iso_code = iso_code ;
        this.iso_codeDirtyFlag = true ;
    }

    /**
     * 获取 [ISO_CODE]脏标记
     */
    @JsonIgnore
    public boolean getIso_codeDirtyFlag(){
        return iso_codeDirtyFlag ;
    }

    /**
     * 获取 [DECIMAL_POINT]
     */
    @JsonProperty("decimal_point")
    public String getDecimal_point(){
        return decimal_point ;
    }

    /**
     * 设置 [DECIMAL_POINT]
     */
    @JsonProperty("decimal_point")
    public void setDecimal_point(String  decimal_point){
        this.decimal_point = decimal_point ;
        this.decimal_pointDirtyFlag = true ;
    }

    /**
     * 获取 [DECIMAL_POINT]脏标记
     */
    @JsonIgnore
    public boolean getDecimal_pointDirtyFlag(){
        return decimal_pointDirtyFlag ;
    }

    /**
     * 获取 [GROUPING]
     */
    @JsonProperty("grouping")
    public String getGrouping(){
        return grouping ;
    }

    /**
     * 设置 [GROUPING]
     */
    @JsonProperty("grouping")
    public void setGrouping(String  grouping){
        this.grouping = grouping ;
        this.groupingDirtyFlag = true ;
    }

    /**
     * 获取 [GROUPING]脏标记
     */
    @JsonIgnore
    public boolean getGroupingDirtyFlag(){
        return groupingDirtyFlag ;
    }

    /**
     * 获取 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return create_date ;
    }

    /**
     * 设置 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return create_dateDirtyFlag ;
    }

    /**
     * 获取 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return write_date ;
    }

    /**
     * 设置 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return write_dateDirtyFlag ;
    }

    /**
     * 获取 [DIRECTION]
     */
    @JsonProperty("direction")
    public String getDirection(){
        return direction ;
    }

    /**
     * 设置 [DIRECTION]
     */
    @JsonProperty("direction")
    public void setDirection(String  direction){
        this.direction = direction ;
        this.directionDirtyFlag = true ;
    }

    /**
     * 获取 [DIRECTION]脏标记
     */
    @JsonIgnore
    public boolean getDirectionDirtyFlag(){
        return directionDirtyFlag ;
    }

    /**
     * 获取 [DATE_FORMAT]
     */
    @JsonProperty("date_format")
    public String getDate_format(){
        return date_format ;
    }

    /**
     * 设置 [DATE_FORMAT]
     */
    @JsonProperty("date_format")
    public void setDate_format(String  date_format){
        this.date_format = date_format ;
        this.date_formatDirtyFlag = true ;
    }

    /**
     * 获取 [DATE_FORMAT]脏标记
     */
    @JsonIgnore
    public boolean getDate_formatDirtyFlag(){
        return date_formatDirtyFlag ;
    }

    /**
     * 获取 [TIME_FORMAT]
     */
    @JsonProperty("time_format")
    public String getTime_format(){
        return time_format ;
    }

    /**
     * 设置 [TIME_FORMAT]
     */
    @JsonProperty("time_format")
    public void setTime_format(String  time_format){
        this.time_format = time_format ;
        this.time_formatDirtyFlag = true ;
    }

    /**
     * 获取 [TIME_FORMAT]脏标记
     */
    @JsonIgnore
    public boolean getTime_formatDirtyFlag(){
        return time_formatDirtyFlag ;
    }

    /**
     * 获取 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return display_name ;
    }

    /**
     * 设置 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

    /**
     * 获取 [DISPLAY_NAME]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return display_nameDirtyFlag ;
    }

    /**
     * 获取 [ACTIVE]
     */
    @JsonProperty("active")
    public String getActive(){
        return active ;
    }

    /**
     * 设置 [ACTIVE]
     */
    @JsonProperty("active")
    public void setActive(String  active){
        this.active = active ;
        this.activeDirtyFlag = true ;
    }

    /**
     * 获取 [ACTIVE]脏标记
     */
    @JsonIgnore
    public boolean getActiveDirtyFlag(){
        return activeDirtyFlag ;
    }

    /**
     * 获取 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return __last_update ;
    }

    /**
     * 设置 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

    /**
     * 获取 [__LAST_UPDATE]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return __last_updateDirtyFlag ;
    }

    /**
     * 获取 [CODE]
     */
    @JsonProperty("code")
    public String getCode(){
        return code ;
    }

    /**
     * 设置 [CODE]
     */
    @JsonProperty("code")
    public void setCode(String  code){
        this.code = code ;
        this.codeDirtyFlag = true ;
    }

    /**
     * 获取 [CODE]脏标记
     */
    @JsonIgnore
    public boolean getCodeDirtyFlag(){
        return codeDirtyFlag ;
    }

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

    /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return idDirtyFlag ;
    }

    /**
     * 获取 [WEEK_START]
     */
    @JsonProperty("week_start")
    public String getWeek_start(){
        return week_start ;
    }

    /**
     * 设置 [WEEK_START]
     */
    @JsonProperty("week_start")
    public void setWeek_start(String  week_start){
        this.week_start = week_start ;
        this.week_startDirtyFlag = true ;
    }

    /**
     * 获取 [WEEK_START]脏标记
     */
    @JsonIgnore
    public boolean getWeek_startDirtyFlag(){
        return week_startDirtyFlag ;
    }

    /**
     * 获取 [NAME]
     */
    @JsonProperty("name")
    public String getName(){
        return name ;
    }

    /**
     * 设置 [NAME]
     */
    @JsonProperty("name")
    public void setName(String  name){
        this.name = name ;
        this.nameDirtyFlag = true ;
    }

    /**
     * 获取 [NAME]脏标记
     */
    @JsonIgnore
    public boolean getNameDirtyFlag(){
        return nameDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return create_uid_text ;
    }

    /**
     * 设置 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return create_uid_textDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return write_uid_text ;
    }

    /**
     * 设置 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return write_uid_textDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return write_uid ;
    }

    /**
     * 设置 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return write_uidDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return create_uid ;
    }

    /**
     * 设置 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return create_uidDirtyFlag ;
    }



    public Res_lang toDO() {
        Res_lang srfdomain = new Res_lang();
        if(getTranslatableDirtyFlag())
            srfdomain.setTranslatable(translatable);
        if(getThousands_sepDirtyFlag())
            srfdomain.setThousands_sep(thousands_sep);
        if(getIso_codeDirtyFlag())
            srfdomain.setIso_code(iso_code);
        if(getDecimal_pointDirtyFlag())
            srfdomain.setDecimal_point(decimal_point);
        if(getGroupingDirtyFlag())
            srfdomain.setGrouping(grouping);
        if(getCreate_dateDirtyFlag())
            srfdomain.setCreate_date(create_date);
        if(getWrite_dateDirtyFlag())
            srfdomain.setWrite_date(write_date);
        if(getDirectionDirtyFlag())
            srfdomain.setDirection(direction);
        if(getDate_formatDirtyFlag())
            srfdomain.setDate_format(date_format);
        if(getTime_formatDirtyFlag())
            srfdomain.setTime_format(time_format);
        if(getDisplay_nameDirtyFlag())
            srfdomain.setDisplay_name(display_name);
        if(getActiveDirtyFlag())
            srfdomain.setActive(active);
        if(get__last_updateDirtyFlag())
            srfdomain.set__last_update(__last_update);
        if(getCodeDirtyFlag())
            srfdomain.setCode(code);
        if(getIdDirtyFlag())
            srfdomain.setId(id);
        if(getWeek_startDirtyFlag())
            srfdomain.setWeek_start(week_start);
        if(getNameDirtyFlag())
            srfdomain.setName(name);
        if(getCreate_uid_textDirtyFlag())
            srfdomain.setCreate_uid_text(create_uid_text);
        if(getWrite_uid_textDirtyFlag())
            srfdomain.setWrite_uid_text(write_uid_text);
        if(getWrite_uidDirtyFlag())
            srfdomain.setWrite_uid(write_uid);
        if(getCreate_uidDirtyFlag())
            srfdomain.setCreate_uid(create_uid);

        return srfdomain;
    }

    public void fromDO(Res_lang srfdomain) {
        if(srfdomain == null )
          return ;
        if(srfdomain.getTranslatableDirtyFlag())
            this.setTranslatable(srfdomain.getTranslatable());
        if(srfdomain.getThousands_sepDirtyFlag())
            this.setThousands_sep(srfdomain.getThousands_sep());
        if(srfdomain.getIso_codeDirtyFlag())
            this.setIso_code(srfdomain.getIso_code());
        if(srfdomain.getDecimal_pointDirtyFlag())
            this.setDecimal_point(srfdomain.getDecimal_point());
        if(srfdomain.getGroupingDirtyFlag())
            this.setGrouping(srfdomain.getGrouping());
        if(srfdomain.getCreate_dateDirtyFlag())
            this.setCreate_date(srfdomain.getCreate_date());
        if(srfdomain.getWrite_dateDirtyFlag())
            this.setWrite_date(srfdomain.getWrite_date());
        if(srfdomain.getDirectionDirtyFlag())
            this.setDirection(srfdomain.getDirection());
        if(srfdomain.getDate_formatDirtyFlag())
            this.setDate_format(srfdomain.getDate_format());
        if(srfdomain.getTime_formatDirtyFlag())
            this.setTime_format(srfdomain.getTime_format());
        if(srfdomain.getDisplay_nameDirtyFlag())
            this.setDisplay_name(srfdomain.getDisplay_name());
        if(srfdomain.getActiveDirtyFlag())
            this.setActive(srfdomain.getActive());
        if(srfdomain.get__last_updateDirtyFlag())
            this.set__last_update(srfdomain.get__last_update());
        if(srfdomain.getCodeDirtyFlag())
            this.setCode(srfdomain.getCode());
        if(srfdomain.getIdDirtyFlag())
            this.setId(srfdomain.getId());
        if(srfdomain.getWeek_startDirtyFlag())
            this.setWeek_start(srfdomain.getWeek_start());
        if(srfdomain.getNameDirtyFlag())
            this.setName(srfdomain.getName());
        if(srfdomain.getCreate_uid_textDirtyFlag())
            this.setCreate_uid_text(srfdomain.getCreate_uid_text());
        if(srfdomain.getWrite_uid_textDirtyFlag())
            this.setWrite_uid_text(srfdomain.getWrite_uid_text());
        if(srfdomain.getWrite_uidDirtyFlag())
            this.setWrite_uid(srfdomain.getWrite_uid());
        if(srfdomain.getCreate_uidDirtyFlag())
            this.setCreate_uid(srfdomain.getCreate_uid());

    }

    public List<Res_langDTO> fromDOPage(List<Res_lang> poPage)   {
        if(poPage == null)
            return null;
        List<Res_langDTO> dtos=new ArrayList<Res_langDTO>();
        for(Res_lang domain : poPage) {
            Res_langDTO dto = new Res_langDTO();
            dto.fromDO(domain);
            dtos.add(dto);
        }
        return dtos;
    }
}

