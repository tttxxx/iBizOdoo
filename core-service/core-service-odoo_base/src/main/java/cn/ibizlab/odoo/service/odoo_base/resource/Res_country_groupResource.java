package cn.ibizlab.odoo.service.odoo_base.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_base.dto.Res_country_groupDTO;
import cn.ibizlab.odoo.core.odoo_base.domain.Res_country_group;
import cn.ibizlab.odoo.core.odoo_base.service.IRes_country_groupService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_base.filter.Res_country_groupSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Res_country_group" })
@RestController
@RequestMapping("")
public class Res_country_groupResource {

    @Autowired
    private IRes_country_groupService res_country_groupService;

    public IRes_country_groupService getRes_country_groupService() {
        return this.res_country_groupService;
    }

    @ApiOperation(value = "批删除数据", tags = {"Res_country_group" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_base/res_country_groups/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Res_country_groupDTO> res_country_groupdtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "批建立数据", tags = {"Res_country_group" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_base/res_country_groups/createBatch")
    public ResponseEntity<Boolean> createBatchRes_country_group(@RequestBody List<Res_country_groupDTO> res_country_groupdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "批更新数据", tags = {"Res_country_group" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_base/res_country_groups/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Res_country_groupDTO> res_country_groupdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "获取数据", tags = {"Res_country_group" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_base/res_country_groups/{res_country_group_id}")
    public ResponseEntity<Res_country_groupDTO> get(@PathVariable("res_country_group_id") Integer res_country_group_id) {
        Res_country_groupDTO dto = new Res_country_groupDTO();
        Res_country_group domain = res_country_groupService.get(res_country_group_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "删除数据", tags = {"Res_country_group" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_base/res_country_groups/{res_country_group_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("res_country_group_id") Integer res_country_group_id) {
        Res_country_groupDTO res_country_groupdto = new Res_country_groupDTO();
		Res_country_group domain = new Res_country_group();
		res_country_groupdto.setId(res_country_group_id);
		domain.setId(res_country_group_id);
        Boolean rst = res_country_groupService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "更新数据", tags = {"Res_country_group" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_base/res_country_groups/{res_country_group_id}")

    public ResponseEntity<Res_country_groupDTO> update(@PathVariable("res_country_group_id") Integer res_country_group_id, @RequestBody Res_country_groupDTO res_country_groupdto) {
		Res_country_group domain = res_country_groupdto.toDO();
        domain.setId(res_country_group_id);
		res_country_groupService.update(domain);
		Res_country_groupDTO dto = new Res_country_groupDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "建立数据", tags = {"Res_country_group" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_base/res_country_groups")

    public ResponseEntity<Res_country_groupDTO> create(@RequestBody Res_country_groupDTO res_country_groupdto) {
        Res_country_groupDTO dto = new Res_country_groupDTO();
        Res_country_group domain = res_country_groupdto.toDO();
		res_country_groupService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Res_country_group" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_base/res_country_groups/fetchdefault")
	public ResponseEntity<Page<Res_country_groupDTO>> fetchDefault(Res_country_groupSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Res_country_groupDTO> list = new ArrayList<Res_country_groupDTO>();
        
        Page<Res_country_group> domains = res_country_groupService.searchDefault(context) ;
        for(Res_country_group res_country_group : domains.getContent()){
            Res_country_groupDTO dto = new Res_country_groupDTO();
            dto.fromDO(res_country_group);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
