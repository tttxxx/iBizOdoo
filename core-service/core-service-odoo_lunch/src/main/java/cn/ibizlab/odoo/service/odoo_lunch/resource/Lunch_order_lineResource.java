package cn.ibizlab.odoo.service.odoo_lunch.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_lunch.dto.Lunch_order_lineDTO;
import cn.ibizlab.odoo.core.odoo_lunch.domain.Lunch_order_line;
import cn.ibizlab.odoo.core.odoo_lunch.service.ILunch_order_lineService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_lunch.filter.Lunch_order_lineSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Lunch_order_line" })
@RestController
@RequestMapping("")
public class Lunch_order_lineResource {

    @Autowired
    private ILunch_order_lineService lunch_order_lineService;

    public ILunch_order_lineService getLunch_order_lineService() {
        return this.lunch_order_lineService;
    }

    @ApiOperation(value = "更新数据", tags = {"Lunch_order_line" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_lunch/lunch_order_lines/{lunch_order_line_id}")

    public ResponseEntity<Lunch_order_lineDTO> update(@PathVariable("lunch_order_line_id") Integer lunch_order_line_id, @RequestBody Lunch_order_lineDTO lunch_order_linedto) {
		Lunch_order_line domain = lunch_order_linedto.toDO();
        domain.setId(lunch_order_line_id);
		lunch_order_lineService.update(domain);
		Lunch_order_lineDTO dto = new Lunch_order_lineDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批更新数据", tags = {"Lunch_order_line" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_lunch/lunch_order_lines/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Lunch_order_lineDTO> lunch_order_linedtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "获取数据", tags = {"Lunch_order_line" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_lunch/lunch_order_lines/{lunch_order_line_id}")
    public ResponseEntity<Lunch_order_lineDTO> get(@PathVariable("lunch_order_line_id") Integer lunch_order_line_id) {
        Lunch_order_lineDTO dto = new Lunch_order_lineDTO();
        Lunch_order_line domain = lunch_order_lineService.get(lunch_order_line_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批建立数据", tags = {"Lunch_order_line" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_lunch/lunch_order_lines/createBatch")
    public ResponseEntity<Boolean> createBatchLunch_order_line(@RequestBody List<Lunch_order_lineDTO> lunch_order_linedtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "删除数据", tags = {"Lunch_order_line" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_lunch/lunch_order_lines/{lunch_order_line_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("lunch_order_line_id") Integer lunch_order_line_id) {
        Lunch_order_lineDTO lunch_order_linedto = new Lunch_order_lineDTO();
		Lunch_order_line domain = new Lunch_order_line();
		lunch_order_linedto.setId(lunch_order_line_id);
		domain.setId(lunch_order_line_id);
        Boolean rst = lunch_order_lineService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "批删除数据", tags = {"Lunch_order_line" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_lunch/lunch_order_lines/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Lunch_order_lineDTO> lunch_order_linedtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "建立数据", tags = {"Lunch_order_line" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_lunch/lunch_order_lines")

    public ResponseEntity<Lunch_order_lineDTO> create(@RequestBody Lunch_order_lineDTO lunch_order_linedto) {
        Lunch_order_lineDTO dto = new Lunch_order_lineDTO();
        Lunch_order_line domain = lunch_order_linedto.toDO();
		lunch_order_lineService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Lunch_order_line" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_lunch/lunch_order_lines/fetchdefault")
	public ResponseEntity<Page<Lunch_order_lineDTO>> fetchDefault(Lunch_order_lineSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Lunch_order_lineDTO> list = new ArrayList<Lunch_order_lineDTO>();
        
        Page<Lunch_order_line> domains = lunch_order_lineService.searchDefault(context) ;
        for(Lunch_order_line lunch_order_line : domains.getContent()){
            Lunch_order_lineDTO dto = new Lunch_order_lineDTO();
            dto.fromDO(lunch_order_line);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
