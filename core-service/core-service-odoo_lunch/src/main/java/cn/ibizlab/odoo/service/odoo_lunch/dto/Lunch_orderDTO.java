package cn.ibizlab.odoo.service.odoo_lunch.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.core.odoo_lunch.valuerule.anno.lunch_order.*;
import cn.ibizlab.odoo.core.odoo_lunch.domain.Lunch_order;
import org.springframework.cglib.beans.BeanCopier;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 服务DTO对象[Lunch_orderDTO]
 */
public class Lunch_orderDTO implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @Lunch_order__last_updateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp __last_update;

    @JsonIgnore
    private boolean __last_updateDirtyFlag;

    /**
     * 属性 [PREVIOUS_ORDER_WIDGET]
     *
     */
    @Lunch_orderPrevious_order_widgetDefault(info = "默认规则")
    private String previous_order_widget;

    @JsonIgnore
    private boolean previous_order_widgetDirtyFlag;

    /**
     * 属性 [ID]
     *
     */
    @Lunch_orderIdDefault(info = "默认规则")
    private Integer id;

    @JsonIgnore
    private boolean idDirtyFlag;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @Lunch_orderDisplay_nameDefault(info = "默认规则")
    private String display_name;

    @JsonIgnore
    private boolean display_nameDirtyFlag;

    /**
     * 属性 [ORDER_LINE_IDS]
     *
     */
    @Lunch_orderOrder_line_idsDefault(info = "默认规则")
    private String order_line_ids;

    @JsonIgnore
    private boolean order_line_idsDirtyFlag;

    /**
     * 属性 [STATE]
     *
     */
    @Lunch_orderStateDefault(info = "默认规则")
    private String state;

    @JsonIgnore
    private boolean stateDirtyFlag;

    /**
     * 属性 [BALANCE_VISIBLE]
     *
     */
    @Lunch_orderBalance_visibleDefault(info = "默认规则")
    private String balance_visible;

    @JsonIgnore
    private boolean balance_visibleDirtyFlag;

    /**
     * 属性 [DATE]
     *
     */
    @Lunch_orderDateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    private Timestamp date;

    @JsonIgnore
    private boolean dateDirtyFlag;

    /**
     * 属性 [ALERTS]
     *
     */
    @Lunch_orderAlertsDefault(info = "默认规则")
    private String alerts;

    @JsonIgnore
    private boolean alertsDirtyFlag;

    /**
     * 属性 [CREATE_DATE]
     *
     */
    @Lunch_orderCreate_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp create_date;

    @JsonIgnore
    private boolean create_dateDirtyFlag;

    /**
     * 属性 [CASH_MOVE_BALANCE]
     *
     */
    @Lunch_orderCash_move_balanceDefault(info = "默认规则")
    private Double cash_move_balance;

    @JsonIgnore
    private boolean cash_move_balanceDirtyFlag;

    /**
     * 属性 [PREVIOUS_ORDER_IDS]
     *
     */
    @Lunch_orderPrevious_order_idsDefault(info = "默认规则")
    private String previous_order_ids;

    @JsonIgnore
    private boolean previous_order_idsDirtyFlag;

    /**
     * 属性 [TOTAL]
     *
     */
    @Lunch_orderTotalDefault(info = "默认规则")
    private Double total;

    @JsonIgnore
    private boolean totalDirtyFlag;

    /**
     * 属性 [WRITE_DATE]
     *
     */
    @Lunch_orderWrite_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp write_date;

    @JsonIgnore
    private boolean write_dateDirtyFlag;

    /**
     * 属性 [COMPANY_ID_TEXT]
     *
     */
    @Lunch_orderCompany_id_textDefault(info = "默认规则")
    private String company_id_text;

    @JsonIgnore
    private boolean company_id_textDirtyFlag;

    /**
     * 属性 [WRITE_UID_TEXT]
     *
     */
    @Lunch_orderWrite_uid_textDefault(info = "默认规则")
    private String write_uid_text;

    @JsonIgnore
    private boolean write_uid_textDirtyFlag;

    /**
     * 属性 [USER_ID_TEXT]
     *
     */
    @Lunch_orderUser_id_textDefault(info = "默认规则")
    private String user_id_text;

    @JsonIgnore
    private boolean user_id_textDirtyFlag;

    /**
     * 属性 [CREATE_UID_TEXT]
     *
     */
    @Lunch_orderCreate_uid_textDefault(info = "默认规则")
    private String create_uid_text;

    @JsonIgnore
    private boolean create_uid_textDirtyFlag;

    /**
     * 属性 [CURRENCY_ID_TEXT]
     *
     */
    @Lunch_orderCurrency_id_textDefault(info = "默认规则")
    private String currency_id_text;

    @JsonIgnore
    private boolean currency_id_textDirtyFlag;

    /**
     * 属性 [CURRENCY_ID]
     *
     */
    @Lunch_orderCurrency_idDefault(info = "默认规则")
    private Integer currency_id;

    @JsonIgnore
    private boolean currency_idDirtyFlag;

    /**
     * 属性 [COMPANY_ID]
     *
     */
    @Lunch_orderCompany_idDefault(info = "默认规则")
    private Integer company_id;

    @JsonIgnore
    private boolean company_idDirtyFlag;

    /**
     * 属性 [CREATE_UID]
     *
     */
    @Lunch_orderCreate_uidDefault(info = "默认规则")
    private Integer create_uid;

    @JsonIgnore
    private boolean create_uidDirtyFlag;

    /**
     * 属性 [USER_ID]
     *
     */
    @Lunch_orderUser_idDefault(info = "默认规则")
    private Integer user_id;

    @JsonIgnore
    private boolean user_idDirtyFlag;

    /**
     * 属性 [WRITE_UID]
     *
     */
    @Lunch_orderWrite_uidDefault(info = "默认规则")
    private Integer write_uid;

    @JsonIgnore
    private boolean write_uidDirtyFlag;


    /**
     * 获取 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return __last_update ;
    }

    /**
     * 设置 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

    /**
     * 获取 [__LAST_UPDATE]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return __last_updateDirtyFlag ;
    }

    /**
     * 获取 [PREVIOUS_ORDER_WIDGET]
     */
    @JsonProperty("previous_order_widget")
    public String getPrevious_order_widget(){
        return previous_order_widget ;
    }

    /**
     * 设置 [PREVIOUS_ORDER_WIDGET]
     */
    @JsonProperty("previous_order_widget")
    public void setPrevious_order_widget(String  previous_order_widget){
        this.previous_order_widget = previous_order_widget ;
        this.previous_order_widgetDirtyFlag = true ;
    }

    /**
     * 获取 [PREVIOUS_ORDER_WIDGET]脏标记
     */
    @JsonIgnore
    public boolean getPrevious_order_widgetDirtyFlag(){
        return previous_order_widgetDirtyFlag ;
    }

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

    /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return idDirtyFlag ;
    }

    /**
     * 获取 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return display_name ;
    }

    /**
     * 设置 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

    /**
     * 获取 [DISPLAY_NAME]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return display_nameDirtyFlag ;
    }

    /**
     * 获取 [ORDER_LINE_IDS]
     */
    @JsonProperty("order_line_ids")
    public String getOrder_line_ids(){
        return order_line_ids ;
    }

    /**
     * 设置 [ORDER_LINE_IDS]
     */
    @JsonProperty("order_line_ids")
    public void setOrder_line_ids(String  order_line_ids){
        this.order_line_ids = order_line_ids ;
        this.order_line_idsDirtyFlag = true ;
    }

    /**
     * 获取 [ORDER_LINE_IDS]脏标记
     */
    @JsonIgnore
    public boolean getOrder_line_idsDirtyFlag(){
        return order_line_idsDirtyFlag ;
    }

    /**
     * 获取 [STATE]
     */
    @JsonProperty("state")
    public String getState(){
        return state ;
    }

    /**
     * 设置 [STATE]
     */
    @JsonProperty("state")
    public void setState(String  state){
        this.state = state ;
        this.stateDirtyFlag = true ;
    }

    /**
     * 获取 [STATE]脏标记
     */
    @JsonIgnore
    public boolean getStateDirtyFlag(){
        return stateDirtyFlag ;
    }

    /**
     * 获取 [BALANCE_VISIBLE]
     */
    @JsonProperty("balance_visible")
    public String getBalance_visible(){
        return balance_visible ;
    }

    /**
     * 设置 [BALANCE_VISIBLE]
     */
    @JsonProperty("balance_visible")
    public void setBalance_visible(String  balance_visible){
        this.balance_visible = balance_visible ;
        this.balance_visibleDirtyFlag = true ;
    }

    /**
     * 获取 [BALANCE_VISIBLE]脏标记
     */
    @JsonIgnore
    public boolean getBalance_visibleDirtyFlag(){
        return balance_visibleDirtyFlag ;
    }

    /**
     * 获取 [DATE]
     */
    @JsonProperty("date")
    public Timestamp getDate(){
        return date ;
    }

    /**
     * 设置 [DATE]
     */
    @JsonProperty("date")
    public void setDate(Timestamp  date){
        this.date = date ;
        this.dateDirtyFlag = true ;
    }

    /**
     * 获取 [DATE]脏标记
     */
    @JsonIgnore
    public boolean getDateDirtyFlag(){
        return dateDirtyFlag ;
    }

    /**
     * 获取 [ALERTS]
     */
    @JsonProperty("alerts")
    public String getAlerts(){
        return alerts ;
    }

    /**
     * 设置 [ALERTS]
     */
    @JsonProperty("alerts")
    public void setAlerts(String  alerts){
        this.alerts = alerts ;
        this.alertsDirtyFlag = true ;
    }

    /**
     * 获取 [ALERTS]脏标记
     */
    @JsonIgnore
    public boolean getAlertsDirtyFlag(){
        return alertsDirtyFlag ;
    }

    /**
     * 获取 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return create_date ;
    }

    /**
     * 设置 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return create_dateDirtyFlag ;
    }

    /**
     * 获取 [CASH_MOVE_BALANCE]
     */
    @JsonProperty("cash_move_balance")
    public Double getCash_move_balance(){
        return cash_move_balance ;
    }

    /**
     * 设置 [CASH_MOVE_BALANCE]
     */
    @JsonProperty("cash_move_balance")
    public void setCash_move_balance(Double  cash_move_balance){
        this.cash_move_balance = cash_move_balance ;
        this.cash_move_balanceDirtyFlag = true ;
    }

    /**
     * 获取 [CASH_MOVE_BALANCE]脏标记
     */
    @JsonIgnore
    public boolean getCash_move_balanceDirtyFlag(){
        return cash_move_balanceDirtyFlag ;
    }

    /**
     * 获取 [PREVIOUS_ORDER_IDS]
     */
    @JsonProperty("previous_order_ids")
    public String getPrevious_order_ids(){
        return previous_order_ids ;
    }

    /**
     * 设置 [PREVIOUS_ORDER_IDS]
     */
    @JsonProperty("previous_order_ids")
    public void setPrevious_order_ids(String  previous_order_ids){
        this.previous_order_ids = previous_order_ids ;
        this.previous_order_idsDirtyFlag = true ;
    }

    /**
     * 获取 [PREVIOUS_ORDER_IDS]脏标记
     */
    @JsonIgnore
    public boolean getPrevious_order_idsDirtyFlag(){
        return previous_order_idsDirtyFlag ;
    }

    /**
     * 获取 [TOTAL]
     */
    @JsonProperty("total")
    public Double getTotal(){
        return total ;
    }

    /**
     * 设置 [TOTAL]
     */
    @JsonProperty("total")
    public void setTotal(Double  total){
        this.total = total ;
        this.totalDirtyFlag = true ;
    }

    /**
     * 获取 [TOTAL]脏标记
     */
    @JsonIgnore
    public boolean getTotalDirtyFlag(){
        return totalDirtyFlag ;
    }

    /**
     * 获取 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return write_date ;
    }

    /**
     * 设置 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return write_dateDirtyFlag ;
    }

    /**
     * 获取 [COMPANY_ID_TEXT]
     */
    @JsonProperty("company_id_text")
    public String getCompany_id_text(){
        return company_id_text ;
    }

    /**
     * 设置 [COMPANY_ID_TEXT]
     */
    @JsonProperty("company_id_text")
    public void setCompany_id_text(String  company_id_text){
        this.company_id_text = company_id_text ;
        this.company_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [COMPANY_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getCompany_id_textDirtyFlag(){
        return company_id_textDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return write_uid_text ;
    }

    /**
     * 设置 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return write_uid_textDirtyFlag ;
    }

    /**
     * 获取 [USER_ID_TEXT]
     */
    @JsonProperty("user_id_text")
    public String getUser_id_text(){
        return user_id_text ;
    }

    /**
     * 设置 [USER_ID_TEXT]
     */
    @JsonProperty("user_id_text")
    public void setUser_id_text(String  user_id_text){
        this.user_id_text = user_id_text ;
        this.user_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [USER_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getUser_id_textDirtyFlag(){
        return user_id_textDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return create_uid_text ;
    }

    /**
     * 设置 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return create_uid_textDirtyFlag ;
    }

    /**
     * 获取 [CURRENCY_ID_TEXT]
     */
    @JsonProperty("currency_id_text")
    public String getCurrency_id_text(){
        return currency_id_text ;
    }

    /**
     * 设置 [CURRENCY_ID_TEXT]
     */
    @JsonProperty("currency_id_text")
    public void setCurrency_id_text(String  currency_id_text){
        this.currency_id_text = currency_id_text ;
        this.currency_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [CURRENCY_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getCurrency_id_textDirtyFlag(){
        return currency_id_textDirtyFlag ;
    }

    /**
     * 获取 [CURRENCY_ID]
     */
    @JsonProperty("currency_id")
    public Integer getCurrency_id(){
        return currency_id ;
    }

    /**
     * 设置 [CURRENCY_ID]
     */
    @JsonProperty("currency_id")
    public void setCurrency_id(Integer  currency_id){
        this.currency_id = currency_id ;
        this.currency_idDirtyFlag = true ;
    }

    /**
     * 获取 [CURRENCY_ID]脏标记
     */
    @JsonIgnore
    public boolean getCurrency_idDirtyFlag(){
        return currency_idDirtyFlag ;
    }

    /**
     * 获取 [COMPANY_ID]
     */
    @JsonProperty("company_id")
    public Integer getCompany_id(){
        return company_id ;
    }

    /**
     * 设置 [COMPANY_ID]
     */
    @JsonProperty("company_id")
    public void setCompany_id(Integer  company_id){
        this.company_id = company_id ;
        this.company_idDirtyFlag = true ;
    }

    /**
     * 获取 [COMPANY_ID]脏标记
     */
    @JsonIgnore
    public boolean getCompany_idDirtyFlag(){
        return company_idDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return create_uid ;
    }

    /**
     * 设置 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return create_uidDirtyFlag ;
    }

    /**
     * 获取 [USER_ID]
     */
    @JsonProperty("user_id")
    public Integer getUser_id(){
        return user_id ;
    }

    /**
     * 设置 [USER_ID]
     */
    @JsonProperty("user_id")
    public void setUser_id(Integer  user_id){
        this.user_id = user_id ;
        this.user_idDirtyFlag = true ;
    }

    /**
     * 获取 [USER_ID]脏标记
     */
    @JsonIgnore
    public boolean getUser_idDirtyFlag(){
        return user_idDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return write_uid ;
    }

    /**
     * 设置 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return write_uidDirtyFlag ;
    }



    public Lunch_order toDO() {
        Lunch_order srfdomain = new Lunch_order();
        if(get__last_updateDirtyFlag())
            srfdomain.set__last_update(__last_update);
        if(getPrevious_order_widgetDirtyFlag())
            srfdomain.setPrevious_order_widget(previous_order_widget);
        if(getIdDirtyFlag())
            srfdomain.setId(id);
        if(getDisplay_nameDirtyFlag())
            srfdomain.setDisplay_name(display_name);
        if(getOrder_line_idsDirtyFlag())
            srfdomain.setOrder_line_ids(order_line_ids);
        if(getStateDirtyFlag())
            srfdomain.setState(state);
        if(getBalance_visibleDirtyFlag())
            srfdomain.setBalance_visible(balance_visible);
        if(getDateDirtyFlag())
            srfdomain.setDate(date);
        if(getAlertsDirtyFlag())
            srfdomain.setAlerts(alerts);
        if(getCreate_dateDirtyFlag())
            srfdomain.setCreate_date(create_date);
        if(getCash_move_balanceDirtyFlag())
            srfdomain.setCash_move_balance(cash_move_balance);
        if(getPrevious_order_idsDirtyFlag())
            srfdomain.setPrevious_order_ids(previous_order_ids);
        if(getTotalDirtyFlag())
            srfdomain.setTotal(total);
        if(getWrite_dateDirtyFlag())
            srfdomain.setWrite_date(write_date);
        if(getCompany_id_textDirtyFlag())
            srfdomain.setCompany_id_text(company_id_text);
        if(getWrite_uid_textDirtyFlag())
            srfdomain.setWrite_uid_text(write_uid_text);
        if(getUser_id_textDirtyFlag())
            srfdomain.setUser_id_text(user_id_text);
        if(getCreate_uid_textDirtyFlag())
            srfdomain.setCreate_uid_text(create_uid_text);
        if(getCurrency_id_textDirtyFlag())
            srfdomain.setCurrency_id_text(currency_id_text);
        if(getCurrency_idDirtyFlag())
            srfdomain.setCurrency_id(currency_id);
        if(getCompany_idDirtyFlag())
            srfdomain.setCompany_id(company_id);
        if(getCreate_uidDirtyFlag())
            srfdomain.setCreate_uid(create_uid);
        if(getUser_idDirtyFlag())
            srfdomain.setUser_id(user_id);
        if(getWrite_uidDirtyFlag())
            srfdomain.setWrite_uid(write_uid);

        return srfdomain;
    }

    public void fromDO(Lunch_order srfdomain) {
        if(srfdomain == null )
          return ;
        if(srfdomain.get__last_updateDirtyFlag())
            this.set__last_update(srfdomain.get__last_update());
        if(srfdomain.getPrevious_order_widgetDirtyFlag())
            this.setPrevious_order_widget(srfdomain.getPrevious_order_widget());
        if(srfdomain.getIdDirtyFlag())
            this.setId(srfdomain.getId());
        if(srfdomain.getDisplay_nameDirtyFlag())
            this.setDisplay_name(srfdomain.getDisplay_name());
        if(srfdomain.getOrder_line_idsDirtyFlag())
            this.setOrder_line_ids(srfdomain.getOrder_line_ids());
        if(srfdomain.getStateDirtyFlag())
            this.setState(srfdomain.getState());
        if(srfdomain.getBalance_visibleDirtyFlag())
            this.setBalance_visible(srfdomain.getBalance_visible());
        if(srfdomain.getDateDirtyFlag())
            this.setDate(srfdomain.getDate());
        if(srfdomain.getAlertsDirtyFlag())
            this.setAlerts(srfdomain.getAlerts());
        if(srfdomain.getCreate_dateDirtyFlag())
            this.setCreate_date(srfdomain.getCreate_date());
        if(srfdomain.getCash_move_balanceDirtyFlag())
            this.setCash_move_balance(srfdomain.getCash_move_balance());
        if(srfdomain.getPrevious_order_idsDirtyFlag())
            this.setPrevious_order_ids(srfdomain.getPrevious_order_ids());
        if(srfdomain.getTotalDirtyFlag())
            this.setTotal(srfdomain.getTotal());
        if(srfdomain.getWrite_dateDirtyFlag())
            this.setWrite_date(srfdomain.getWrite_date());
        if(srfdomain.getCompany_id_textDirtyFlag())
            this.setCompany_id_text(srfdomain.getCompany_id_text());
        if(srfdomain.getWrite_uid_textDirtyFlag())
            this.setWrite_uid_text(srfdomain.getWrite_uid_text());
        if(srfdomain.getUser_id_textDirtyFlag())
            this.setUser_id_text(srfdomain.getUser_id_text());
        if(srfdomain.getCreate_uid_textDirtyFlag())
            this.setCreate_uid_text(srfdomain.getCreate_uid_text());
        if(srfdomain.getCurrency_id_textDirtyFlag())
            this.setCurrency_id_text(srfdomain.getCurrency_id_text());
        if(srfdomain.getCurrency_idDirtyFlag())
            this.setCurrency_id(srfdomain.getCurrency_id());
        if(srfdomain.getCompany_idDirtyFlag())
            this.setCompany_id(srfdomain.getCompany_id());
        if(srfdomain.getCreate_uidDirtyFlag())
            this.setCreate_uid(srfdomain.getCreate_uid());
        if(srfdomain.getUser_idDirtyFlag())
            this.setUser_id(srfdomain.getUser_id());
        if(srfdomain.getWrite_uidDirtyFlag())
            this.setWrite_uid(srfdomain.getWrite_uid());

    }

    public List<Lunch_orderDTO> fromDOPage(List<Lunch_order> poPage)   {
        if(poPage == null)
            return null;
        List<Lunch_orderDTO> dtos=new ArrayList<Lunch_orderDTO>();
        for(Lunch_order domain : poPage) {
            Lunch_orderDTO dto = new Lunch_orderDTO();
            dto.fromDO(domain);
            dtos.add(dto);
        }
        return dtos;
    }
}

