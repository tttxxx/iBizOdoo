package cn.ibizlab.odoo.service.odoo_lunch.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_lunch.dto.Lunch_productDTO;
import cn.ibizlab.odoo.core.odoo_lunch.domain.Lunch_product;
import cn.ibizlab.odoo.core.odoo_lunch.service.ILunch_productService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_lunch.filter.Lunch_productSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Lunch_product" })
@RestController
@RequestMapping("")
public class Lunch_productResource {

    @Autowired
    private ILunch_productService lunch_productService;

    public ILunch_productService getLunch_productService() {
        return this.lunch_productService;
    }

    @ApiOperation(value = "获取数据", tags = {"Lunch_product" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_lunch/lunch_products/{lunch_product_id}")
    public ResponseEntity<Lunch_productDTO> get(@PathVariable("lunch_product_id") Integer lunch_product_id) {
        Lunch_productDTO dto = new Lunch_productDTO();
        Lunch_product domain = lunch_productService.get(lunch_product_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批删除数据", tags = {"Lunch_product" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_lunch/lunch_products/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Lunch_productDTO> lunch_productdtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "更新数据", tags = {"Lunch_product" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_lunch/lunch_products/{lunch_product_id}")

    public ResponseEntity<Lunch_productDTO> update(@PathVariable("lunch_product_id") Integer lunch_product_id, @RequestBody Lunch_productDTO lunch_productdto) {
		Lunch_product domain = lunch_productdto.toDO();
        domain.setId(lunch_product_id);
		lunch_productService.update(domain);
		Lunch_productDTO dto = new Lunch_productDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批更新数据", tags = {"Lunch_product" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_lunch/lunch_products/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Lunch_productDTO> lunch_productdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "建立数据", tags = {"Lunch_product" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_lunch/lunch_products")

    public ResponseEntity<Lunch_productDTO> create(@RequestBody Lunch_productDTO lunch_productdto) {
        Lunch_productDTO dto = new Lunch_productDTO();
        Lunch_product domain = lunch_productdto.toDO();
		lunch_productService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "删除数据", tags = {"Lunch_product" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_lunch/lunch_products/{lunch_product_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("lunch_product_id") Integer lunch_product_id) {
        Lunch_productDTO lunch_productdto = new Lunch_productDTO();
		Lunch_product domain = new Lunch_product();
		lunch_productdto.setId(lunch_product_id);
		domain.setId(lunch_product_id);
        Boolean rst = lunch_productService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "批建立数据", tags = {"Lunch_product" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_lunch/lunch_products/createBatch")
    public ResponseEntity<Boolean> createBatchLunch_product(@RequestBody List<Lunch_productDTO> lunch_productdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Lunch_product" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_lunch/lunch_products/fetchdefault")
	public ResponseEntity<Page<Lunch_productDTO>> fetchDefault(Lunch_productSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Lunch_productDTO> list = new ArrayList<Lunch_productDTO>();
        
        Page<Lunch_product> domains = lunch_productService.searchDefault(context) ;
        for(Lunch_product lunch_product : domains.getContent()){
            Lunch_productDTO dto = new Lunch_productDTO();
            dto.fromDO(lunch_product);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
