package cn.ibizlab.odoo.service.odoo_lunch.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_lunch.dto.Lunch_product_categoryDTO;
import cn.ibizlab.odoo.core.odoo_lunch.domain.Lunch_product_category;
import cn.ibizlab.odoo.core.odoo_lunch.service.ILunch_product_categoryService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_lunch.filter.Lunch_product_categorySearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Lunch_product_category" })
@RestController
@RequestMapping("")
public class Lunch_product_categoryResource {

    @Autowired
    private ILunch_product_categoryService lunch_product_categoryService;

    public ILunch_product_categoryService getLunch_product_categoryService() {
        return this.lunch_product_categoryService;
    }

    @ApiOperation(value = "获取数据", tags = {"Lunch_product_category" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_lunch/lunch_product_categories/{lunch_product_category_id}")
    public ResponseEntity<Lunch_product_categoryDTO> get(@PathVariable("lunch_product_category_id") Integer lunch_product_category_id) {
        Lunch_product_categoryDTO dto = new Lunch_product_categoryDTO();
        Lunch_product_category domain = lunch_product_categoryService.get(lunch_product_category_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批删除数据", tags = {"Lunch_product_category" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_lunch/lunch_product_categories/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Lunch_product_categoryDTO> lunch_product_categorydtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "批建立数据", tags = {"Lunch_product_category" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_lunch/lunch_product_categories/createBatch")
    public ResponseEntity<Boolean> createBatchLunch_product_category(@RequestBody List<Lunch_product_categoryDTO> lunch_product_categorydtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "更新数据", tags = {"Lunch_product_category" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_lunch/lunch_product_categories/{lunch_product_category_id}")

    public ResponseEntity<Lunch_product_categoryDTO> update(@PathVariable("lunch_product_category_id") Integer lunch_product_category_id, @RequestBody Lunch_product_categoryDTO lunch_product_categorydto) {
		Lunch_product_category domain = lunch_product_categorydto.toDO();
        domain.setId(lunch_product_category_id);
		lunch_product_categoryService.update(domain);
		Lunch_product_categoryDTO dto = new Lunch_product_categoryDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "删除数据", tags = {"Lunch_product_category" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_lunch/lunch_product_categories/{lunch_product_category_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("lunch_product_category_id") Integer lunch_product_category_id) {
        Lunch_product_categoryDTO lunch_product_categorydto = new Lunch_product_categoryDTO();
		Lunch_product_category domain = new Lunch_product_category();
		lunch_product_categorydto.setId(lunch_product_category_id);
		domain.setId(lunch_product_category_id);
        Boolean rst = lunch_product_categoryService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "建立数据", tags = {"Lunch_product_category" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_lunch/lunch_product_categories")

    public ResponseEntity<Lunch_product_categoryDTO> create(@RequestBody Lunch_product_categoryDTO lunch_product_categorydto) {
        Lunch_product_categoryDTO dto = new Lunch_product_categoryDTO();
        Lunch_product_category domain = lunch_product_categorydto.toDO();
		lunch_product_categoryService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批更新数据", tags = {"Lunch_product_category" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_lunch/lunch_product_categories/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Lunch_product_categoryDTO> lunch_product_categorydtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Lunch_product_category" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_lunch/lunch_product_categories/fetchdefault")
	public ResponseEntity<Page<Lunch_product_categoryDTO>> fetchDefault(Lunch_product_categorySearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Lunch_product_categoryDTO> list = new ArrayList<Lunch_product_categoryDTO>();
        
        Page<Lunch_product_category> domains = lunch_product_categoryService.searchDefault(context) ;
        for(Lunch_product_category lunch_product_category : domains.getContent()){
            Lunch_product_categoryDTO dto = new Lunch_product_categoryDTO();
            dto.fromDO(lunch_product_category);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
