package cn.ibizlab.odoo.service.odoo_lunch.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.core.odoo_lunch.valuerule.anno.lunch_alert.*;
import cn.ibizlab.odoo.core.odoo_lunch.domain.Lunch_alert;
import org.springframework.cglib.beans.BeanCopier;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 服务DTO对象[Lunch_alertDTO]
 */
public class Lunch_alertDTO implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [SPECIFIC_DAY]
     *
     */
    @Lunch_alertSpecific_dayDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    private Timestamp specific_day;

    @JsonIgnore
    private boolean specific_dayDirtyFlag;

    /**
     * 属性 [DISPLAY]
     *
     */
    @Lunch_alertDisplayDefault(info = "默认规则")
    private String display;

    @JsonIgnore
    private boolean displayDirtyFlag;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @Lunch_alertDisplay_nameDefault(info = "默认规则")
    private String display_name;

    @JsonIgnore
    private boolean display_nameDirtyFlag;

    /**
     * 属性 [THURSDAY]
     *
     */
    @Lunch_alertThursdayDefault(info = "默认规则")
    private String thursday;

    @JsonIgnore
    private boolean thursdayDirtyFlag;

    /**
     * 属性 [CREATE_DATE]
     *
     */
    @Lunch_alertCreate_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp create_date;

    @JsonIgnore
    private boolean create_dateDirtyFlag;

    /**
     * 属性 [ID]
     *
     */
    @Lunch_alertIdDefault(info = "默认规则")
    private Integer id;

    @JsonIgnore
    private boolean idDirtyFlag;

    /**
     * 属性 [WRITE_DATE]
     *
     */
    @Lunch_alertWrite_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp write_date;

    @JsonIgnore
    private boolean write_dateDirtyFlag;

    /**
     * 属性 [WEDNESDAY]
     *
     */
    @Lunch_alertWednesdayDefault(info = "默认规则")
    private String wednesday;

    @JsonIgnore
    private boolean wednesdayDirtyFlag;

    /**
     * 属性 [TUESDAY]
     *
     */
    @Lunch_alertTuesdayDefault(info = "默认规则")
    private String tuesday;

    @JsonIgnore
    private boolean tuesdayDirtyFlag;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @Lunch_alert__last_updateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp __last_update;

    @JsonIgnore
    private boolean __last_updateDirtyFlag;

    /**
     * 属性 [MESSAGE]
     *
     */
    @Lunch_alertMessageDefault(info = "默认规则")
    private String message;

    @JsonIgnore
    private boolean messageDirtyFlag;

    /**
     * 属性 [ALERT_TYPE]
     *
     */
    @Lunch_alertAlert_typeDefault(info = "默认规则")
    private String alert_type;

    @JsonIgnore
    private boolean alert_typeDirtyFlag;

    /**
     * 属性 [ACTIVE]
     *
     */
    @Lunch_alertActiveDefault(info = "默认规则")
    private String active;

    @JsonIgnore
    private boolean activeDirtyFlag;

    /**
     * 属性 [SATURDAY]
     *
     */
    @Lunch_alertSaturdayDefault(info = "默认规则")
    private String saturday;

    @JsonIgnore
    private boolean saturdayDirtyFlag;

    /**
     * 属性 [START_HOUR]
     *
     */
    @Lunch_alertStart_hourDefault(info = "默认规则")
    private Double start_hour;

    @JsonIgnore
    private boolean start_hourDirtyFlag;

    /**
     * 属性 [MONDAY]
     *
     */
    @Lunch_alertMondayDefault(info = "默认规则")
    private String monday;

    @JsonIgnore
    private boolean mondayDirtyFlag;

    /**
     * 属性 [FRIDAY]
     *
     */
    @Lunch_alertFridayDefault(info = "默认规则")
    private String friday;

    @JsonIgnore
    private boolean fridayDirtyFlag;

    /**
     * 属性 [END_HOUR]
     *
     */
    @Lunch_alertEnd_hourDefault(info = "默认规则")
    private Double end_hour;

    @JsonIgnore
    private boolean end_hourDirtyFlag;

    /**
     * 属性 [SUNDAY]
     *
     */
    @Lunch_alertSundayDefault(info = "默认规则")
    private String sunday;

    @JsonIgnore
    private boolean sundayDirtyFlag;

    /**
     * 属性 [CREATE_UID_TEXT]
     *
     */
    @Lunch_alertCreate_uid_textDefault(info = "默认规则")
    private String create_uid_text;

    @JsonIgnore
    private boolean create_uid_textDirtyFlag;

    /**
     * 属性 [WRITE_UID_TEXT]
     *
     */
    @Lunch_alertWrite_uid_textDefault(info = "默认规则")
    private String write_uid_text;

    @JsonIgnore
    private boolean write_uid_textDirtyFlag;

    /**
     * 属性 [PARTNER_ID_TEXT]
     *
     */
    @Lunch_alertPartner_id_textDefault(info = "默认规则")
    private String partner_id_text;

    @JsonIgnore
    private boolean partner_id_textDirtyFlag;

    /**
     * 属性 [CREATE_UID]
     *
     */
    @Lunch_alertCreate_uidDefault(info = "默认规则")
    private Integer create_uid;

    @JsonIgnore
    private boolean create_uidDirtyFlag;

    /**
     * 属性 [PARTNER_ID]
     *
     */
    @Lunch_alertPartner_idDefault(info = "默认规则")
    private Integer partner_id;

    @JsonIgnore
    private boolean partner_idDirtyFlag;

    /**
     * 属性 [WRITE_UID]
     *
     */
    @Lunch_alertWrite_uidDefault(info = "默认规则")
    private Integer write_uid;

    @JsonIgnore
    private boolean write_uidDirtyFlag;


    /**
     * 获取 [SPECIFIC_DAY]
     */
    @JsonProperty("specific_day")
    public Timestamp getSpecific_day(){
        return specific_day ;
    }

    /**
     * 设置 [SPECIFIC_DAY]
     */
    @JsonProperty("specific_day")
    public void setSpecific_day(Timestamp  specific_day){
        this.specific_day = specific_day ;
        this.specific_dayDirtyFlag = true ;
    }

    /**
     * 获取 [SPECIFIC_DAY]脏标记
     */
    @JsonIgnore
    public boolean getSpecific_dayDirtyFlag(){
        return specific_dayDirtyFlag ;
    }

    /**
     * 获取 [DISPLAY]
     */
    @JsonProperty("display")
    public String getDisplay(){
        return display ;
    }

    /**
     * 设置 [DISPLAY]
     */
    @JsonProperty("display")
    public void setDisplay(String  display){
        this.display = display ;
        this.displayDirtyFlag = true ;
    }

    /**
     * 获取 [DISPLAY]脏标记
     */
    @JsonIgnore
    public boolean getDisplayDirtyFlag(){
        return displayDirtyFlag ;
    }

    /**
     * 获取 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return display_name ;
    }

    /**
     * 设置 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

    /**
     * 获取 [DISPLAY_NAME]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return display_nameDirtyFlag ;
    }

    /**
     * 获取 [THURSDAY]
     */
    @JsonProperty("thursday")
    public String getThursday(){
        return thursday ;
    }

    /**
     * 设置 [THURSDAY]
     */
    @JsonProperty("thursday")
    public void setThursday(String  thursday){
        this.thursday = thursday ;
        this.thursdayDirtyFlag = true ;
    }

    /**
     * 获取 [THURSDAY]脏标记
     */
    @JsonIgnore
    public boolean getThursdayDirtyFlag(){
        return thursdayDirtyFlag ;
    }

    /**
     * 获取 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return create_date ;
    }

    /**
     * 设置 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return create_dateDirtyFlag ;
    }

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

    /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return idDirtyFlag ;
    }

    /**
     * 获取 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return write_date ;
    }

    /**
     * 设置 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return write_dateDirtyFlag ;
    }

    /**
     * 获取 [WEDNESDAY]
     */
    @JsonProperty("wednesday")
    public String getWednesday(){
        return wednesday ;
    }

    /**
     * 设置 [WEDNESDAY]
     */
    @JsonProperty("wednesday")
    public void setWednesday(String  wednesday){
        this.wednesday = wednesday ;
        this.wednesdayDirtyFlag = true ;
    }

    /**
     * 获取 [WEDNESDAY]脏标记
     */
    @JsonIgnore
    public boolean getWednesdayDirtyFlag(){
        return wednesdayDirtyFlag ;
    }

    /**
     * 获取 [TUESDAY]
     */
    @JsonProperty("tuesday")
    public String getTuesday(){
        return tuesday ;
    }

    /**
     * 设置 [TUESDAY]
     */
    @JsonProperty("tuesday")
    public void setTuesday(String  tuesday){
        this.tuesday = tuesday ;
        this.tuesdayDirtyFlag = true ;
    }

    /**
     * 获取 [TUESDAY]脏标记
     */
    @JsonIgnore
    public boolean getTuesdayDirtyFlag(){
        return tuesdayDirtyFlag ;
    }

    /**
     * 获取 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return __last_update ;
    }

    /**
     * 设置 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

    /**
     * 获取 [__LAST_UPDATE]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return __last_updateDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE]
     */
    @JsonProperty("message")
    public String getMessage(){
        return message ;
    }

    /**
     * 设置 [MESSAGE]
     */
    @JsonProperty("message")
    public void setMessage(String  message){
        this.message = message ;
        this.messageDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE]脏标记
     */
    @JsonIgnore
    public boolean getMessageDirtyFlag(){
        return messageDirtyFlag ;
    }

    /**
     * 获取 [ALERT_TYPE]
     */
    @JsonProperty("alert_type")
    public String getAlert_type(){
        return alert_type ;
    }

    /**
     * 设置 [ALERT_TYPE]
     */
    @JsonProperty("alert_type")
    public void setAlert_type(String  alert_type){
        this.alert_type = alert_type ;
        this.alert_typeDirtyFlag = true ;
    }

    /**
     * 获取 [ALERT_TYPE]脏标记
     */
    @JsonIgnore
    public boolean getAlert_typeDirtyFlag(){
        return alert_typeDirtyFlag ;
    }

    /**
     * 获取 [ACTIVE]
     */
    @JsonProperty("active")
    public String getActive(){
        return active ;
    }

    /**
     * 设置 [ACTIVE]
     */
    @JsonProperty("active")
    public void setActive(String  active){
        this.active = active ;
        this.activeDirtyFlag = true ;
    }

    /**
     * 获取 [ACTIVE]脏标记
     */
    @JsonIgnore
    public boolean getActiveDirtyFlag(){
        return activeDirtyFlag ;
    }

    /**
     * 获取 [SATURDAY]
     */
    @JsonProperty("saturday")
    public String getSaturday(){
        return saturday ;
    }

    /**
     * 设置 [SATURDAY]
     */
    @JsonProperty("saturday")
    public void setSaturday(String  saturday){
        this.saturday = saturday ;
        this.saturdayDirtyFlag = true ;
    }

    /**
     * 获取 [SATURDAY]脏标记
     */
    @JsonIgnore
    public boolean getSaturdayDirtyFlag(){
        return saturdayDirtyFlag ;
    }

    /**
     * 获取 [START_HOUR]
     */
    @JsonProperty("start_hour")
    public Double getStart_hour(){
        return start_hour ;
    }

    /**
     * 设置 [START_HOUR]
     */
    @JsonProperty("start_hour")
    public void setStart_hour(Double  start_hour){
        this.start_hour = start_hour ;
        this.start_hourDirtyFlag = true ;
    }

    /**
     * 获取 [START_HOUR]脏标记
     */
    @JsonIgnore
    public boolean getStart_hourDirtyFlag(){
        return start_hourDirtyFlag ;
    }

    /**
     * 获取 [MONDAY]
     */
    @JsonProperty("monday")
    public String getMonday(){
        return monday ;
    }

    /**
     * 设置 [MONDAY]
     */
    @JsonProperty("monday")
    public void setMonday(String  monday){
        this.monday = monday ;
        this.mondayDirtyFlag = true ;
    }

    /**
     * 获取 [MONDAY]脏标记
     */
    @JsonIgnore
    public boolean getMondayDirtyFlag(){
        return mondayDirtyFlag ;
    }

    /**
     * 获取 [FRIDAY]
     */
    @JsonProperty("friday")
    public String getFriday(){
        return friday ;
    }

    /**
     * 设置 [FRIDAY]
     */
    @JsonProperty("friday")
    public void setFriday(String  friday){
        this.friday = friday ;
        this.fridayDirtyFlag = true ;
    }

    /**
     * 获取 [FRIDAY]脏标记
     */
    @JsonIgnore
    public boolean getFridayDirtyFlag(){
        return fridayDirtyFlag ;
    }

    /**
     * 获取 [END_HOUR]
     */
    @JsonProperty("end_hour")
    public Double getEnd_hour(){
        return end_hour ;
    }

    /**
     * 设置 [END_HOUR]
     */
    @JsonProperty("end_hour")
    public void setEnd_hour(Double  end_hour){
        this.end_hour = end_hour ;
        this.end_hourDirtyFlag = true ;
    }

    /**
     * 获取 [END_HOUR]脏标记
     */
    @JsonIgnore
    public boolean getEnd_hourDirtyFlag(){
        return end_hourDirtyFlag ;
    }

    /**
     * 获取 [SUNDAY]
     */
    @JsonProperty("sunday")
    public String getSunday(){
        return sunday ;
    }

    /**
     * 设置 [SUNDAY]
     */
    @JsonProperty("sunday")
    public void setSunday(String  sunday){
        this.sunday = sunday ;
        this.sundayDirtyFlag = true ;
    }

    /**
     * 获取 [SUNDAY]脏标记
     */
    @JsonIgnore
    public boolean getSundayDirtyFlag(){
        return sundayDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return create_uid_text ;
    }

    /**
     * 设置 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return create_uid_textDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return write_uid_text ;
    }

    /**
     * 设置 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return write_uid_textDirtyFlag ;
    }

    /**
     * 获取 [PARTNER_ID_TEXT]
     */
    @JsonProperty("partner_id_text")
    public String getPartner_id_text(){
        return partner_id_text ;
    }

    /**
     * 设置 [PARTNER_ID_TEXT]
     */
    @JsonProperty("partner_id_text")
    public void setPartner_id_text(String  partner_id_text){
        this.partner_id_text = partner_id_text ;
        this.partner_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [PARTNER_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getPartner_id_textDirtyFlag(){
        return partner_id_textDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return create_uid ;
    }

    /**
     * 设置 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return create_uidDirtyFlag ;
    }

    /**
     * 获取 [PARTNER_ID]
     */
    @JsonProperty("partner_id")
    public Integer getPartner_id(){
        return partner_id ;
    }

    /**
     * 设置 [PARTNER_ID]
     */
    @JsonProperty("partner_id")
    public void setPartner_id(Integer  partner_id){
        this.partner_id = partner_id ;
        this.partner_idDirtyFlag = true ;
    }

    /**
     * 获取 [PARTNER_ID]脏标记
     */
    @JsonIgnore
    public boolean getPartner_idDirtyFlag(){
        return partner_idDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return write_uid ;
    }

    /**
     * 设置 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return write_uidDirtyFlag ;
    }



    public Lunch_alert toDO() {
        Lunch_alert srfdomain = new Lunch_alert();
        if(getSpecific_dayDirtyFlag())
            srfdomain.setSpecific_day(specific_day);
        if(getDisplayDirtyFlag())
            srfdomain.setDisplay(display);
        if(getDisplay_nameDirtyFlag())
            srfdomain.setDisplay_name(display_name);
        if(getThursdayDirtyFlag())
            srfdomain.setThursday(thursday);
        if(getCreate_dateDirtyFlag())
            srfdomain.setCreate_date(create_date);
        if(getIdDirtyFlag())
            srfdomain.setId(id);
        if(getWrite_dateDirtyFlag())
            srfdomain.setWrite_date(write_date);
        if(getWednesdayDirtyFlag())
            srfdomain.setWednesday(wednesday);
        if(getTuesdayDirtyFlag())
            srfdomain.setTuesday(tuesday);
        if(get__last_updateDirtyFlag())
            srfdomain.set__last_update(__last_update);
        if(getMessageDirtyFlag())
            srfdomain.setMessage(message);
        if(getAlert_typeDirtyFlag())
            srfdomain.setAlert_type(alert_type);
        if(getActiveDirtyFlag())
            srfdomain.setActive(active);
        if(getSaturdayDirtyFlag())
            srfdomain.setSaturday(saturday);
        if(getStart_hourDirtyFlag())
            srfdomain.setStart_hour(start_hour);
        if(getMondayDirtyFlag())
            srfdomain.setMonday(monday);
        if(getFridayDirtyFlag())
            srfdomain.setFriday(friday);
        if(getEnd_hourDirtyFlag())
            srfdomain.setEnd_hour(end_hour);
        if(getSundayDirtyFlag())
            srfdomain.setSunday(sunday);
        if(getCreate_uid_textDirtyFlag())
            srfdomain.setCreate_uid_text(create_uid_text);
        if(getWrite_uid_textDirtyFlag())
            srfdomain.setWrite_uid_text(write_uid_text);
        if(getPartner_id_textDirtyFlag())
            srfdomain.setPartner_id_text(partner_id_text);
        if(getCreate_uidDirtyFlag())
            srfdomain.setCreate_uid(create_uid);
        if(getPartner_idDirtyFlag())
            srfdomain.setPartner_id(partner_id);
        if(getWrite_uidDirtyFlag())
            srfdomain.setWrite_uid(write_uid);

        return srfdomain;
    }

    public void fromDO(Lunch_alert srfdomain) {
        if(srfdomain == null )
          return ;
        if(srfdomain.getSpecific_dayDirtyFlag())
            this.setSpecific_day(srfdomain.getSpecific_day());
        if(srfdomain.getDisplayDirtyFlag())
            this.setDisplay(srfdomain.getDisplay());
        if(srfdomain.getDisplay_nameDirtyFlag())
            this.setDisplay_name(srfdomain.getDisplay_name());
        if(srfdomain.getThursdayDirtyFlag())
            this.setThursday(srfdomain.getThursday());
        if(srfdomain.getCreate_dateDirtyFlag())
            this.setCreate_date(srfdomain.getCreate_date());
        if(srfdomain.getIdDirtyFlag())
            this.setId(srfdomain.getId());
        if(srfdomain.getWrite_dateDirtyFlag())
            this.setWrite_date(srfdomain.getWrite_date());
        if(srfdomain.getWednesdayDirtyFlag())
            this.setWednesday(srfdomain.getWednesday());
        if(srfdomain.getTuesdayDirtyFlag())
            this.setTuesday(srfdomain.getTuesday());
        if(srfdomain.get__last_updateDirtyFlag())
            this.set__last_update(srfdomain.get__last_update());
        if(srfdomain.getMessageDirtyFlag())
            this.setMessage(srfdomain.getMessage());
        if(srfdomain.getAlert_typeDirtyFlag())
            this.setAlert_type(srfdomain.getAlert_type());
        if(srfdomain.getActiveDirtyFlag())
            this.setActive(srfdomain.getActive());
        if(srfdomain.getSaturdayDirtyFlag())
            this.setSaturday(srfdomain.getSaturday());
        if(srfdomain.getStart_hourDirtyFlag())
            this.setStart_hour(srfdomain.getStart_hour());
        if(srfdomain.getMondayDirtyFlag())
            this.setMonday(srfdomain.getMonday());
        if(srfdomain.getFridayDirtyFlag())
            this.setFriday(srfdomain.getFriday());
        if(srfdomain.getEnd_hourDirtyFlag())
            this.setEnd_hour(srfdomain.getEnd_hour());
        if(srfdomain.getSundayDirtyFlag())
            this.setSunday(srfdomain.getSunday());
        if(srfdomain.getCreate_uid_textDirtyFlag())
            this.setCreate_uid_text(srfdomain.getCreate_uid_text());
        if(srfdomain.getWrite_uid_textDirtyFlag())
            this.setWrite_uid_text(srfdomain.getWrite_uid_text());
        if(srfdomain.getPartner_id_textDirtyFlag())
            this.setPartner_id_text(srfdomain.getPartner_id_text());
        if(srfdomain.getCreate_uidDirtyFlag())
            this.setCreate_uid(srfdomain.getCreate_uid());
        if(srfdomain.getPartner_idDirtyFlag())
            this.setPartner_id(srfdomain.getPartner_id());
        if(srfdomain.getWrite_uidDirtyFlag())
            this.setWrite_uid(srfdomain.getWrite_uid());

    }

    public List<Lunch_alertDTO> fromDOPage(List<Lunch_alert> poPage)   {
        if(poPage == null)
            return null;
        List<Lunch_alertDTO> dtos=new ArrayList<Lunch_alertDTO>();
        for(Lunch_alert domain : poPage) {
            Lunch_alertDTO dto = new Lunch_alertDTO();
            dto.fromDO(domain);
            dtos.add(dto);
        }
        return dtos;
    }
}

