package cn.ibizlab.odoo.service.odoo_lunch.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_lunch.dto.Lunch_cashmoveDTO;
import cn.ibizlab.odoo.core.odoo_lunch.domain.Lunch_cashmove;
import cn.ibizlab.odoo.core.odoo_lunch.service.ILunch_cashmoveService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_lunch.filter.Lunch_cashmoveSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Lunch_cashmove" })
@RestController
@RequestMapping("")
public class Lunch_cashmoveResource {

    @Autowired
    private ILunch_cashmoveService lunch_cashmoveService;

    public ILunch_cashmoveService getLunch_cashmoveService() {
        return this.lunch_cashmoveService;
    }

    @ApiOperation(value = "删除数据", tags = {"Lunch_cashmove" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_lunch/lunch_cashmoves/{lunch_cashmove_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("lunch_cashmove_id") Integer lunch_cashmove_id) {
        Lunch_cashmoveDTO lunch_cashmovedto = new Lunch_cashmoveDTO();
		Lunch_cashmove domain = new Lunch_cashmove();
		lunch_cashmovedto.setId(lunch_cashmove_id);
		domain.setId(lunch_cashmove_id);
        Boolean rst = lunch_cashmoveService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "批更新数据", tags = {"Lunch_cashmove" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_lunch/lunch_cashmoves/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Lunch_cashmoveDTO> lunch_cashmovedtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "批删除数据", tags = {"Lunch_cashmove" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_lunch/lunch_cashmoves/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Lunch_cashmoveDTO> lunch_cashmovedtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "更新数据", tags = {"Lunch_cashmove" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_lunch/lunch_cashmoves/{lunch_cashmove_id}")

    public ResponseEntity<Lunch_cashmoveDTO> update(@PathVariable("lunch_cashmove_id") Integer lunch_cashmove_id, @RequestBody Lunch_cashmoveDTO lunch_cashmovedto) {
		Lunch_cashmove domain = lunch_cashmovedto.toDO();
        domain.setId(lunch_cashmove_id);
		lunch_cashmoveService.update(domain);
		Lunch_cashmoveDTO dto = new Lunch_cashmoveDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批建立数据", tags = {"Lunch_cashmove" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_lunch/lunch_cashmoves/createBatch")
    public ResponseEntity<Boolean> createBatchLunch_cashmove(@RequestBody List<Lunch_cashmoveDTO> lunch_cashmovedtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "建立数据", tags = {"Lunch_cashmove" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_lunch/lunch_cashmoves")

    public ResponseEntity<Lunch_cashmoveDTO> create(@RequestBody Lunch_cashmoveDTO lunch_cashmovedto) {
        Lunch_cashmoveDTO dto = new Lunch_cashmoveDTO();
        Lunch_cashmove domain = lunch_cashmovedto.toDO();
		lunch_cashmoveService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取数据", tags = {"Lunch_cashmove" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_lunch/lunch_cashmoves/{lunch_cashmove_id}")
    public ResponseEntity<Lunch_cashmoveDTO> get(@PathVariable("lunch_cashmove_id") Integer lunch_cashmove_id) {
        Lunch_cashmoveDTO dto = new Lunch_cashmoveDTO();
        Lunch_cashmove domain = lunch_cashmoveService.get(lunch_cashmove_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Lunch_cashmove" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_lunch/lunch_cashmoves/fetchdefault")
	public ResponseEntity<Page<Lunch_cashmoveDTO>> fetchDefault(Lunch_cashmoveSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Lunch_cashmoveDTO> list = new ArrayList<Lunch_cashmoveDTO>();
        
        Page<Lunch_cashmove> domains = lunch_cashmoveService.searchDefault(context) ;
        for(Lunch_cashmove lunch_cashmove : domains.getContent()){
            Lunch_cashmoveDTO dto = new Lunch_cashmoveDTO();
            dto.fromDO(lunch_cashmove);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
