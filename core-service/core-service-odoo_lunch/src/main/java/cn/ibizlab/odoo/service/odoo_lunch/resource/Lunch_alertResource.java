package cn.ibizlab.odoo.service.odoo_lunch.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_lunch.dto.Lunch_alertDTO;
import cn.ibizlab.odoo.core.odoo_lunch.domain.Lunch_alert;
import cn.ibizlab.odoo.core.odoo_lunch.service.ILunch_alertService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_lunch.filter.Lunch_alertSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Lunch_alert" })
@RestController
@RequestMapping("")
public class Lunch_alertResource {

    @Autowired
    private ILunch_alertService lunch_alertService;

    public ILunch_alertService getLunch_alertService() {
        return this.lunch_alertService;
    }

    @ApiOperation(value = "删除数据", tags = {"Lunch_alert" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_lunch/lunch_alerts/{lunch_alert_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("lunch_alert_id") Integer lunch_alert_id) {
        Lunch_alertDTO lunch_alertdto = new Lunch_alertDTO();
		Lunch_alert domain = new Lunch_alert();
		lunch_alertdto.setId(lunch_alert_id);
		domain.setId(lunch_alert_id);
        Boolean rst = lunch_alertService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "更新数据", tags = {"Lunch_alert" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_lunch/lunch_alerts/{lunch_alert_id}")

    public ResponseEntity<Lunch_alertDTO> update(@PathVariable("lunch_alert_id") Integer lunch_alert_id, @RequestBody Lunch_alertDTO lunch_alertdto) {
		Lunch_alert domain = lunch_alertdto.toDO();
        domain.setId(lunch_alert_id);
		lunch_alertService.update(domain);
		Lunch_alertDTO dto = new Lunch_alertDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批建立数据", tags = {"Lunch_alert" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_lunch/lunch_alerts/createBatch")
    public ResponseEntity<Boolean> createBatchLunch_alert(@RequestBody List<Lunch_alertDTO> lunch_alertdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "获取数据", tags = {"Lunch_alert" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_lunch/lunch_alerts/{lunch_alert_id}")
    public ResponseEntity<Lunch_alertDTO> get(@PathVariable("lunch_alert_id") Integer lunch_alert_id) {
        Lunch_alertDTO dto = new Lunch_alertDTO();
        Lunch_alert domain = lunch_alertService.get(lunch_alert_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批删除数据", tags = {"Lunch_alert" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_lunch/lunch_alerts/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Lunch_alertDTO> lunch_alertdtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "建立数据", tags = {"Lunch_alert" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_lunch/lunch_alerts")

    public ResponseEntity<Lunch_alertDTO> create(@RequestBody Lunch_alertDTO lunch_alertdto) {
        Lunch_alertDTO dto = new Lunch_alertDTO();
        Lunch_alert domain = lunch_alertdto.toDO();
		lunch_alertService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批更新数据", tags = {"Lunch_alert" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_lunch/lunch_alerts/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Lunch_alertDTO> lunch_alertdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Lunch_alert" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_lunch/lunch_alerts/fetchdefault")
	public ResponseEntity<Page<Lunch_alertDTO>> fetchDefault(Lunch_alertSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Lunch_alertDTO> list = new ArrayList<Lunch_alertDTO>();
        
        Page<Lunch_alert> domains = lunch_alertService.searchDefault(context) ;
        for(Lunch_alert lunch_alert : domains.getContent()){
            Lunch_alertDTO dto = new Lunch_alertDTO();
            dto.fromDO(lunch_alert);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
