package cn.ibizlab.odoo.service.odoo_lunch.config;

import org.springframework.context.annotation.ComponentScan;

@ComponentScan("cn.ibizlab.odoo.service.odoo_lunch")
public class odoo_lunchRestConfiguration {

}
