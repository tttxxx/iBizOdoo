package cn.ibizlab.odoo.service.odoo_lunch.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_lunch.dto.Lunch_order_line_luckyDTO;
import cn.ibizlab.odoo.core.odoo_lunch.domain.Lunch_order_line_lucky;
import cn.ibizlab.odoo.core.odoo_lunch.service.ILunch_order_line_luckyService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_lunch.filter.Lunch_order_line_luckySearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Lunch_order_line_lucky" })
@RestController
@RequestMapping("")
public class Lunch_order_line_luckyResource {

    @Autowired
    private ILunch_order_line_luckyService lunch_order_line_luckyService;

    public ILunch_order_line_luckyService getLunch_order_line_luckyService() {
        return this.lunch_order_line_luckyService;
    }

    @ApiOperation(value = "批更新数据", tags = {"Lunch_order_line_lucky" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_lunch/lunch_order_line_luckies/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Lunch_order_line_luckyDTO> lunch_order_line_luckydtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "获取数据", tags = {"Lunch_order_line_lucky" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_lunch/lunch_order_line_luckies/{lunch_order_line_lucky_id}")
    public ResponseEntity<Lunch_order_line_luckyDTO> get(@PathVariable("lunch_order_line_lucky_id") Integer lunch_order_line_lucky_id) {
        Lunch_order_line_luckyDTO dto = new Lunch_order_line_luckyDTO();
        Lunch_order_line_lucky domain = lunch_order_line_luckyService.get(lunch_order_line_lucky_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "建立数据", tags = {"Lunch_order_line_lucky" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_lunch/lunch_order_line_luckies")

    public ResponseEntity<Lunch_order_line_luckyDTO> create(@RequestBody Lunch_order_line_luckyDTO lunch_order_line_luckydto) {
        Lunch_order_line_luckyDTO dto = new Lunch_order_line_luckyDTO();
        Lunch_order_line_lucky domain = lunch_order_line_luckydto.toDO();
		lunch_order_line_luckyService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批建立数据", tags = {"Lunch_order_line_lucky" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_lunch/lunch_order_line_luckies/createBatch")
    public ResponseEntity<Boolean> createBatchLunch_order_line_lucky(@RequestBody List<Lunch_order_line_luckyDTO> lunch_order_line_luckydtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "批删除数据", tags = {"Lunch_order_line_lucky" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_lunch/lunch_order_line_luckies/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Lunch_order_line_luckyDTO> lunch_order_line_luckydtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "更新数据", tags = {"Lunch_order_line_lucky" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_lunch/lunch_order_line_luckies/{lunch_order_line_lucky_id}")

    public ResponseEntity<Lunch_order_line_luckyDTO> update(@PathVariable("lunch_order_line_lucky_id") Integer lunch_order_line_lucky_id, @RequestBody Lunch_order_line_luckyDTO lunch_order_line_luckydto) {
		Lunch_order_line_lucky domain = lunch_order_line_luckydto.toDO();
        domain.setId(lunch_order_line_lucky_id);
		lunch_order_line_luckyService.update(domain);
		Lunch_order_line_luckyDTO dto = new Lunch_order_line_luckyDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "删除数据", tags = {"Lunch_order_line_lucky" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_lunch/lunch_order_line_luckies/{lunch_order_line_lucky_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("lunch_order_line_lucky_id") Integer lunch_order_line_lucky_id) {
        Lunch_order_line_luckyDTO lunch_order_line_luckydto = new Lunch_order_line_luckyDTO();
		Lunch_order_line_lucky domain = new Lunch_order_line_lucky();
		lunch_order_line_luckydto.setId(lunch_order_line_lucky_id);
		domain.setId(lunch_order_line_lucky_id);
        Boolean rst = lunch_order_line_luckyService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

	@ApiOperation(value = "获取默认查询", tags = {"Lunch_order_line_lucky" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_lunch/lunch_order_line_luckies/fetchdefault")
	public ResponseEntity<Page<Lunch_order_line_luckyDTO>> fetchDefault(Lunch_order_line_luckySearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Lunch_order_line_luckyDTO> list = new ArrayList<Lunch_order_line_luckyDTO>();
        
        Page<Lunch_order_line_lucky> domains = lunch_order_line_luckyService.searchDefault(context) ;
        for(Lunch_order_line_lucky lunch_order_line_lucky : domains.getContent()){
            Lunch_order_line_luckyDTO dto = new Lunch_order_line_luckyDTO();
            dto.fromDO(lunch_order_line_lucky);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
