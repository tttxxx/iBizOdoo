package cn.ibizlab.odoo.service.odoo_bus.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import lombok.Data;

@ConfigurationProperties(prefix = "service.odoo.bus")
@Data
public class odoo_busServiceProperties {

	private boolean enabled;

	private boolean auth;


}