package cn.ibizlab.odoo.service.odoo_bus.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_bus.dto.Bus_busDTO;
import cn.ibizlab.odoo.core.odoo_bus.domain.Bus_bus;
import cn.ibizlab.odoo.core.odoo_bus.service.IBus_busService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_bus.filter.Bus_busSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Bus_bus" })
@RestController
@RequestMapping("")
public class Bus_busResource {

    @Autowired
    private IBus_busService bus_busService;

    public IBus_busService getBus_busService() {
        return this.bus_busService;
    }

    @ApiOperation(value = "获取数据", tags = {"Bus_bus" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_bus/bus_buses/{bus_bus_id}")
    public ResponseEntity<Bus_busDTO> get(@PathVariable("bus_bus_id") Integer bus_bus_id) {
        Bus_busDTO dto = new Bus_busDTO();
        Bus_bus domain = bus_busService.get(bus_bus_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "更新数据", tags = {"Bus_bus" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_bus/bus_buses/{bus_bus_id}")

    public ResponseEntity<Bus_busDTO> update(@PathVariable("bus_bus_id") Integer bus_bus_id, @RequestBody Bus_busDTO bus_busdto) {
		Bus_bus domain = bus_busdto.toDO();
        domain.setId(bus_bus_id);
		bus_busService.update(domain);
		Bus_busDTO dto = new Bus_busDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批更新数据", tags = {"Bus_bus" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_bus/bus_buses/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Bus_busDTO> bus_busdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "删除数据", tags = {"Bus_bus" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_bus/bus_buses/{bus_bus_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("bus_bus_id") Integer bus_bus_id) {
        Bus_busDTO bus_busdto = new Bus_busDTO();
		Bus_bus domain = new Bus_bus();
		bus_busdto.setId(bus_bus_id);
		domain.setId(bus_bus_id);
        Boolean rst = bus_busService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "批建立数据", tags = {"Bus_bus" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_bus/bus_buses/createBatch")
    public ResponseEntity<Boolean> createBatchBus_bus(@RequestBody List<Bus_busDTO> bus_busdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "建立数据", tags = {"Bus_bus" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_bus/bus_buses")

    public ResponseEntity<Bus_busDTO> create(@RequestBody Bus_busDTO bus_busdto) {
        Bus_busDTO dto = new Bus_busDTO();
        Bus_bus domain = bus_busdto.toDO();
		bus_busService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批删除数据", tags = {"Bus_bus" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_bus/bus_buses/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Bus_busDTO> bus_busdtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Bus_bus" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_bus/bus_buses/fetchdefault")
	public ResponseEntity<Page<Bus_busDTO>> fetchDefault(Bus_busSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Bus_busDTO> list = new ArrayList<Bus_busDTO>();
        
        Page<Bus_bus> domains = bus_busService.searchDefault(context) ;
        for(Bus_bus bus_bus : domains.getContent()){
            Bus_busDTO dto = new Bus_busDTO();
            dto.fromDO(bus_bus);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
