package cn.ibizlab.odoo.service.odoo_sms.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_sms.dto.Sms_apiDTO;
import cn.ibizlab.odoo.core.odoo_sms.domain.Sms_api;
import cn.ibizlab.odoo.core.odoo_sms.service.ISms_apiService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_sms.filter.Sms_apiSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Sms_api" })
@RestController
@RequestMapping("")
public class Sms_apiResource {

    @Autowired
    private ISms_apiService sms_apiService;

    public ISms_apiService getSms_apiService() {
        return this.sms_apiService;
    }

    @ApiOperation(value = "获取数据", tags = {"Sms_api" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_sms/sms_apis/{sms_api_id}")
    public ResponseEntity<Sms_apiDTO> get(@PathVariable("sms_api_id") Integer sms_api_id) {
        Sms_apiDTO dto = new Sms_apiDTO();
        Sms_api domain = sms_apiService.get(sms_api_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "删除数据", tags = {"Sms_api" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_sms/sms_apis/{sms_api_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("sms_api_id") Integer sms_api_id) {
        Sms_apiDTO sms_apidto = new Sms_apiDTO();
		Sms_api domain = new Sms_api();
		sms_apidto.setId(sms_api_id);
		domain.setId(sms_api_id);
        Boolean rst = sms_apiService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "批更新数据", tags = {"Sms_api" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_sms/sms_apis/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Sms_apiDTO> sms_apidtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "建立数据", tags = {"Sms_api" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_sms/sms_apis")

    public ResponseEntity<Sms_apiDTO> create(@RequestBody Sms_apiDTO sms_apidto) {
        Sms_apiDTO dto = new Sms_apiDTO();
        Sms_api domain = sms_apidto.toDO();
		sms_apiService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批删除数据", tags = {"Sms_api" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_sms/sms_apis/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Sms_apiDTO> sms_apidtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "更新数据", tags = {"Sms_api" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_sms/sms_apis/{sms_api_id}")

    public ResponseEntity<Sms_apiDTO> update(@PathVariable("sms_api_id") Integer sms_api_id, @RequestBody Sms_apiDTO sms_apidto) {
		Sms_api domain = sms_apidto.toDO();
        domain.setId(sms_api_id);
		sms_apiService.update(domain);
		Sms_apiDTO dto = new Sms_apiDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批建立数据", tags = {"Sms_api" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_sms/sms_apis/createBatch")
    public ResponseEntity<Boolean> createBatchSms_api(@RequestBody List<Sms_apiDTO> sms_apidtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Sms_api" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_sms/sms_apis/fetchdefault")
	public ResponseEntity<Page<Sms_apiDTO>> fetchDefault(Sms_apiSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Sms_apiDTO> list = new ArrayList<Sms_apiDTO>();
        
        Page<Sms_api> domains = sms_apiService.searchDefault(context) ;
        for(Sms_api sms_api : domains.getContent()){
            Sms_apiDTO dto = new Sms_apiDTO();
            dto.fromDO(sms_api);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
