package cn.ibizlab.odoo.service.odoo_product.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.core.odoo_product.valuerule.anno.product_replenish.*;
import cn.ibizlab.odoo.core.odoo_product.domain.Product_replenish;
import org.springframework.cglib.beans.BeanCopier;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 服务DTO对象[Product_replenishDTO]
 */
public class Product_replenishDTO implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @Product_replenish__last_updateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp __last_update;

    @JsonIgnore
    private boolean __last_updateDirtyFlag;

    /**
     * 属性 [PRODUCT_HAS_VARIANTS]
     *
     */
    @Product_replenishProduct_has_variantsDefault(info = "默认规则")
    private String product_has_variants;

    @JsonIgnore
    private boolean product_has_variantsDirtyFlag;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @Product_replenishDisplay_nameDefault(info = "默认规则")
    private String display_name;

    @JsonIgnore
    private boolean display_nameDirtyFlag;

    /**
     * 属性 [QUANTITY]
     *
     */
    @Product_replenishQuantityDefault(info = "默认规则")
    private Double quantity;

    @JsonIgnore
    private boolean quantityDirtyFlag;

    /**
     * 属性 [WRITE_DATE]
     *
     */
    @Product_replenishWrite_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp write_date;

    @JsonIgnore
    private boolean write_dateDirtyFlag;

    /**
     * 属性 [ID]
     *
     */
    @Product_replenishIdDefault(info = "默认规则")
    private Integer id;

    @JsonIgnore
    private boolean idDirtyFlag;

    /**
     * 属性 [PRODUCT_UOM_CATEGORY_ID]
     *
     */
    @Product_replenishProduct_uom_category_idDefault(info = "默认规则")
    private Integer product_uom_category_id;

    @JsonIgnore
    private boolean product_uom_category_idDirtyFlag;

    /**
     * 属性 [DATE_PLANNED]
     *
     */
    @Product_replenishDate_plannedDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp date_planned;

    @JsonIgnore
    private boolean date_plannedDirtyFlag;

    /**
     * 属性 [CREATE_DATE]
     *
     */
    @Product_replenishCreate_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp create_date;

    @JsonIgnore
    private boolean create_dateDirtyFlag;

    /**
     * 属性 [ROUTE_IDS]
     *
     */
    @Product_replenishRoute_idsDefault(info = "默认规则")
    private String route_ids;

    @JsonIgnore
    private boolean route_idsDirtyFlag;

    /**
     * 属性 [CREATE_UID_TEXT]
     *
     */
    @Product_replenishCreate_uid_textDefault(info = "默认规则")
    private String create_uid_text;

    @JsonIgnore
    private boolean create_uid_textDirtyFlag;

    /**
     * 属性 [PRODUCT_UOM_ID_TEXT]
     *
     */
    @Product_replenishProduct_uom_id_textDefault(info = "默认规则")
    private String product_uom_id_text;

    @JsonIgnore
    private boolean product_uom_id_textDirtyFlag;

    /**
     * 属性 [PRODUCT_TMPL_ID_TEXT]
     *
     */
    @Product_replenishProduct_tmpl_id_textDefault(info = "默认规则")
    private String product_tmpl_id_text;

    @JsonIgnore
    private boolean product_tmpl_id_textDirtyFlag;

    /**
     * 属性 [WRITE_UID_TEXT]
     *
     */
    @Product_replenishWrite_uid_textDefault(info = "默认规则")
    private String write_uid_text;

    @JsonIgnore
    private boolean write_uid_textDirtyFlag;

    /**
     * 属性 [WAREHOUSE_ID_TEXT]
     *
     */
    @Product_replenishWarehouse_id_textDefault(info = "默认规则")
    private String warehouse_id_text;

    @JsonIgnore
    private boolean warehouse_id_textDirtyFlag;

    /**
     * 属性 [PRODUCT_ID_TEXT]
     *
     */
    @Product_replenishProduct_id_textDefault(info = "默认规则")
    private String product_id_text;

    @JsonIgnore
    private boolean product_id_textDirtyFlag;

    /**
     * 属性 [WRITE_UID]
     *
     */
    @Product_replenishWrite_uidDefault(info = "默认规则")
    private Integer write_uid;

    @JsonIgnore
    private boolean write_uidDirtyFlag;

    /**
     * 属性 [PRODUCT_TMPL_ID]
     *
     */
    @Product_replenishProduct_tmpl_idDefault(info = "默认规则")
    private Integer product_tmpl_id;

    @JsonIgnore
    private boolean product_tmpl_idDirtyFlag;

    /**
     * 属性 [CREATE_UID]
     *
     */
    @Product_replenishCreate_uidDefault(info = "默认规则")
    private Integer create_uid;

    @JsonIgnore
    private boolean create_uidDirtyFlag;

    /**
     * 属性 [PRODUCT_UOM_ID]
     *
     */
    @Product_replenishProduct_uom_idDefault(info = "默认规则")
    private Integer product_uom_id;

    @JsonIgnore
    private boolean product_uom_idDirtyFlag;

    /**
     * 属性 [PRODUCT_ID]
     *
     */
    @Product_replenishProduct_idDefault(info = "默认规则")
    private Integer product_id;

    @JsonIgnore
    private boolean product_idDirtyFlag;

    /**
     * 属性 [WAREHOUSE_ID]
     *
     */
    @Product_replenishWarehouse_idDefault(info = "默认规则")
    private Integer warehouse_id;

    @JsonIgnore
    private boolean warehouse_idDirtyFlag;


    /**
     * 获取 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return __last_update ;
    }

    /**
     * 设置 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

    /**
     * 获取 [__LAST_UPDATE]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return __last_updateDirtyFlag ;
    }

    /**
     * 获取 [PRODUCT_HAS_VARIANTS]
     */
    @JsonProperty("product_has_variants")
    public String getProduct_has_variants(){
        return product_has_variants ;
    }

    /**
     * 设置 [PRODUCT_HAS_VARIANTS]
     */
    @JsonProperty("product_has_variants")
    public void setProduct_has_variants(String  product_has_variants){
        this.product_has_variants = product_has_variants ;
        this.product_has_variantsDirtyFlag = true ;
    }

    /**
     * 获取 [PRODUCT_HAS_VARIANTS]脏标记
     */
    @JsonIgnore
    public boolean getProduct_has_variantsDirtyFlag(){
        return product_has_variantsDirtyFlag ;
    }

    /**
     * 获取 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return display_name ;
    }

    /**
     * 设置 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

    /**
     * 获取 [DISPLAY_NAME]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return display_nameDirtyFlag ;
    }

    /**
     * 获取 [QUANTITY]
     */
    @JsonProperty("quantity")
    public Double getQuantity(){
        return quantity ;
    }

    /**
     * 设置 [QUANTITY]
     */
    @JsonProperty("quantity")
    public void setQuantity(Double  quantity){
        this.quantity = quantity ;
        this.quantityDirtyFlag = true ;
    }

    /**
     * 获取 [QUANTITY]脏标记
     */
    @JsonIgnore
    public boolean getQuantityDirtyFlag(){
        return quantityDirtyFlag ;
    }

    /**
     * 获取 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return write_date ;
    }

    /**
     * 设置 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return write_dateDirtyFlag ;
    }

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

    /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return idDirtyFlag ;
    }

    /**
     * 获取 [PRODUCT_UOM_CATEGORY_ID]
     */
    @JsonProperty("product_uom_category_id")
    public Integer getProduct_uom_category_id(){
        return product_uom_category_id ;
    }

    /**
     * 设置 [PRODUCT_UOM_CATEGORY_ID]
     */
    @JsonProperty("product_uom_category_id")
    public void setProduct_uom_category_id(Integer  product_uom_category_id){
        this.product_uom_category_id = product_uom_category_id ;
        this.product_uom_category_idDirtyFlag = true ;
    }

    /**
     * 获取 [PRODUCT_UOM_CATEGORY_ID]脏标记
     */
    @JsonIgnore
    public boolean getProduct_uom_category_idDirtyFlag(){
        return product_uom_category_idDirtyFlag ;
    }

    /**
     * 获取 [DATE_PLANNED]
     */
    @JsonProperty("date_planned")
    public Timestamp getDate_planned(){
        return date_planned ;
    }

    /**
     * 设置 [DATE_PLANNED]
     */
    @JsonProperty("date_planned")
    public void setDate_planned(Timestamp  date_planned){
        this.date_planned = date_planned ;
        this.date_plannedDirtyFlag = true ;
    }

    /**
     * 获取 [DATE_PLANNED]脏标记
     */
    @JsonIgnore
    public boolean getDate_plannedDirtyFlag(){
        return date_plannedDirtyFlag ;
    }

    /**
     * 获取 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return create_date ;
    }

    /**
     * 设置 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return create_dateDirtyFlag ;
    }

    /**
     * 获取 [ROUTE_IDS]
     */
    @JsonProperty("route_ids")
    public String getRoute_ids(){
        return route_ids ;
    }

    /**
     * 设置 [ROUTE_IDS]
     */
    @JsonProperty("route_ids")
    public void setRoute_ids(String  route_ids){
        this.route_ids = route_ids ;
        this.route_idsDirtyFlag = true ;
    }

    /**
     * 获取 [ROUTE_IDS]脏标记
     */
    @JsonIgnore
    public boolean getRoute_idsDirtyFlag(){
        return route_idsDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return create_uid_text ;
    }

    /**
     * 设置 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return create_uid_textDirtyFlag ;
    }

    /**
     * 获取 [PRODUCT_UOM_ID_TEXT]
     */
    @JsonProperty("product_uom_id_text")
    public String getProduct_uom_id_text(){
        return product_uom_id_text ;
    }

    /**
     * 设置 [PRODUCT_UOM_ID_TEXT]
     */
    @JsonProperty("product_uom_id_text")
    public void setProduct_uom_id_text(String  product_uom_id_text){
        this.product_uom_id_text = product_uom_id_text ;
        this.product_uom_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [PRODUCT_UOM_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getProduct_uom_id_textDirtyFlag(){
        return product_uom_id_textDirtyFlag ;
    }

    /**
     * 获取 [PRODUCT_TMPL_ID_TEXT]
     */
    @JsonProperty("product_tmpl_id_text")
    public String getProduct_tmpl_id_text(){
        return product_tmpl_id_text ;
    }

    /**
     * 设置 [PRODUCT_TMPL_ID_TEXT]
     */
    @JsonProperty("product_tmpl_id_text")
    public void setProduct_tmpl_id_text(String  product_tmpl_id_text){
        this.product_tmpl_id_text = product_tmpl_id_text ;
        this.product_tmpl_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [PRODUCT_TMPL_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getProduct_tmpl_id_textDirtyFlag(){
        return product_tmpl_id_textDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return write_uid_text ;
    }

    /**
     * 设置 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return write_uid_textDirtyFlag ;
    }

    /**
     * 获取 [WAREHOUSE_ID_TEXT]
     */
    @JsonProperty("warehouse_id_text")
    public String getWarehouse_id_text(){
        return warehouse_id_text ;
    }

    /**
     * 设置 [WAREHOUSE_ID_TEXT]
     */
    @JsonProperty("warehouse_id_text")
    public void setWarehouse_id_text(String  warehouse_id_text){
        this.warehouse_id_text = warehouse_id_text ;
        this.warehouse_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [WAREHOUSE_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getWarehouse_id_textDirtyFlag(){
        return warehouse_id_textDirtyFlag ;
    }

    /**
     * 获取 [PRODUCT_ID_TEXT]
     */
    @JsonProperty("product_id_text")
    public String getProduct_id_text(){
        return product_id_text ;
    }

    /**
     * 设置 [PRODUCT_ID_TEXT]
     */
    @JsonProperty("product_id_text")
    public void setProduct_id_text(String  product_id_text){
        this.product_id_text = product_id_text ;
        this.product_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [PRODUCT_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getProduct_id_textDirtyFlag(){
        return product_id_textDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return write_uid ;
    }

    /**
     * 设置 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return write_uidDirtyFlag ;
    }

    /**
     * 获取 [PRODUCT_TMPL_ID]
     */
    @JsonProperty("product_tmpl_id")
    public Integer getProduct_tmpl_id(){
        return product_tmpl_id ;
    }

    /**
     * 设置 [PRODUCT_TMPL_ID]
     */
    @JsonProperty("product_tmpl_id")
    public void setProduct_tmpl_id(Integer  product_tmpl_id){
        this.product_tmpl_id = product_tmpl_id ;
        this.product_tmpl_idDirtyFlag = true ;
    }

    /**
     * 获取 [PRODUCT_TMPL_ID]脏标记
     */
    @JsonIgnore
    public boolean getProduct_tmpl_idDirtyFlag(){
        return product_tmpl_idDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return create_uid ;
    }

    /**
     * 设置 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return create_uidDirtyFlag ;
    }

    /**
     * 获取 [PRODUCT_UOM_ID]
     */
    @JsonProperty("product_uom_id")
    public Integer getProduct_uom_id(){
        return product_uom_id ;
    }

    /**
     * 设置 [PRODUCT_UOM_ID]
     */
    @JsonProperty("product_uom_id")
    public void setProduct_uom_id(Integer  product_uom_id){
        this.product_uom_id = product_uom_id ;
        this.product_uom_idDirtyFlag = true ;
    }

    /**
     * 获取 [PRODUCT_UOM_ID]脏标记
     */
    @JsonIgnore
    public boolean getProduct_uom_idDirtyFlag(){
        return product_uom_idDirtyFlag ;
    }

    /**
     * 获取 [PRODUCT_ID]
     */
    @JsonProperty("product_id")
    public Integer getProduct_id(){
        return product_id ;
    }

    /**
     * 设置 [PRODUCT_ID]
     */
    @JsonProperty("product_id")
    public void setProduct_id(Integer  product_id){
        this.product_id = product_id ;
        this.product_idDirtyFlag = true ;
    }

    /**
     * 获取 [PRODUCT_ID]脏标记
     */
    @JsonIgnore
    public boolean getProduct_idDirtyFlag(){
        return product_idDirtyFlag ;
    }

    /**
     * 获取 [WAREHOUSE_ID]
     */
    @JsonProperty("warehouse_id")
    public Integer getWarehouse_id(){
        return warehouse_id ;
    }

    /**
     * 设置 [WAREHOUSE_ID]
     */
    @JsonProperty("warehouse_id")
    public void setWarehouse_id(Integer  warehouse_id){
        this.warehouse_id = warehouse_id ;
        this.warehouse_idDirtyFlag = true ;
    }

    /**
     * 获取 [WAREHOUSE_ID]脏标记
     */
    @JsonIgnore
    public boolean getWarehouse_idDirtyFlag(){
        return warehouse_idDirtyFlag ;
    }



    public Product_replenish toDO() {
        Product_replenish srfdomain = new Product_replenish();
        if(get__last_updateDirtyFlag())
            srfdomain.set__last_update(__last_update);
        if(getProduct_has_variantsDirtyFlag())
            srfdomain.setProduct_has_variants(product_has_variants);
        if(getDisplay_nameDirtyFlag())
            srfdomain.setDisplay_name(display_name);
        if(getQuantityDirtyFlag())
            srfdomain.setQuantity(quantity);
        if(getWrite_dateDirtyFlag())
            srfdomain.setWrite_date(write_date);
        if(getIdDirtyFlag())
            srfdomain.setId(id);
        if(getProduct_uom_category_idDirtyFlag())
            srfdomain.setProduct_uom_category_id(product_uom_category_id);
        if(getDate_plannedDirtyFlag())
            srfdomain.setDate_planned(date_planned);
        if(getCreate_dateDirtyFlag())
            srfdomain.setCreate_date(create_date);
        if(getRoute_idsDirtyFlag())
            srfdomain.setRoute_ids(route_ids);
        if(getCreate_uid_textDirtyFlag())
            srfdomain.setCreate_uid_text(create_uid_text);
        if(getProduct_uom_id_textDirtyFlag())
            srfdomain.setProduct_uom_id_text(product_uom_id_text);
        if(getProduct_tmpl_id_textDirtyFlag())
            srfdomain.setProduct_tmpl_id_text(product_tmpl_id_text);
        if(getWrite_uid_textDirtyFlag())
            srfdomain.setWrite_uid_text(write_uid_text);
        if(getWarehouse_id_textDirtyFlag())
            srfdomain.setWarehouse_id_text(warehouse_id_text);
        if(getProduct_id_textDirtyFlag())
            srfdomain.setProduct_id_text(product_id_text);
        if(getWrite_uidDirtyFlag())
            srfdomain.setWrite_uid(write_uid);
        if(getProduct_tmpl_idDirtyFlag())
            srfdomain.setProduct_tmpl_id(product_tmpl_id);
        if(getCreate_uidDirtyFlag())
            srfdomain.setCreate_uid(create_uid);
        if(getProduct_uom_idDirtyFlag())
            srfdomain.setProduct_uom_id(product_uom_id);
        if(getProduct_idDirtyFlag())
            srfdomain.setProduct_id(product_id);
        if(getWarehouse_idDirtyFlag())
            srfdomain.setWarehouse_id(warehouse_id);

        return srfdomain;
    }

    public void fromDO(Product_replenish srfdomain) {
        if(srfdomain == null )
          return ;
        if(srfdomain.get__last_updateDirtyFlag())
            this.set__last_update(srfdomain.get__last_update());
        if(srfdomain.getProduct_has_variantsDirtyFlag())
            this.setProduct_has_variants(srfdomain.getProduct_has_variants());
        if(srfdomain.getDisplay_nameDirtyFlag())
            this.setDisplay_name(srfdomain.getDisplay_name());
        if(srfdomain.getQuantityDirtyFlag())
            this.setQuantity(srfdomain.getQuantity());
        if(srfdomain.getWrite_dateDirtyFlag())
            this.setWrite_date(srfdomain.getWrite_date());
        if(srfdomain.getIdDirtyFlag())
            this.setId(srfdomain.getId());
        if(srfdomain.getProduct_uom_category_idDirtyFlag())
            this.setProduct_uom_category_id(srfdomain.getProduct_uom_category_id());
        if(srfdomain.getDate_plannedDirtyFlag())
            this.setDate_planned(srfdomain.getDate_planned());
        if(srfdomain.getCreate_dateDirtyFlag())
            this.setCreate_date(srfdomain.getCreate_date());
        if(srfdomain.getRoute_idsDirtyFlag())
            this.setRoute_ids(srfdomain.getRoute_ids());
        if(srfdomain.getCreate_uid_textDirtyFlag())
            this.setCreate_uid_text(srfdomain.getCreate_uid_text());
        if(srfdomain.getProduct_uom_id_textDirtyFlag())
            this.setProduct_uom_id_text(srfdomain.getProduct_uom_id_text());
        if(srfdomain.getProduct_tmpl_id_textDirtyFlag())
            this.setProduct_tmpl_id_text(srfdomain.getProduct_tmpl_id_text());
        if(srfdomain.getWrite_uid_textDirtyFlag())
            this.setWrite_uid_text(srfdomain.getWrite_uid_text());
        if(srfdomain.getWarehouse_id_textDirtyFlag())
            this.setWarehouse_id_text(srfdomain.getWarehouse_id_text());
        if(srfdomain.getProduct_id_textDirtyFlag())
            this.setProduct_id_text(srfdomain.getProduct_id_text());
        if(srfdomain.getWrite_uidDirtyFlag())
            this.setWrite_uid(srfdomain.getWrite_uid());
        if(srfdomain.getProduct_tmpl_idDirtyFlag())
            this.setProduct_tmpl_id(srfdomain.getProduct_tmpl_id());
        if(srfdomain.getCreate_uidDirtyFlag())
            this.setCreate_uid(srfdomain.getCreate_uid());
        if(srfdomain.getProduct_uom_idDirtyFlag())
            this.setProduct_uom_id(srfdomain.getProduct_uom_id());
        if(srfdomain.getProduct_idDirtyFlag())
            this.setProduct_id(srfdomain.getProduct_id());
        if(srfdomain.getWarehouse_idDirtyFlag())
            this.setWarehouse_id(srfdomain.getWarehouse_id());

    }

    public List<Product_replenishDTO> fromDOPage(List<Product_replenish> poPage)   {
        if(poPage == null)
            return null;
        List<Product_replenishDTO> dtos=new ArrayList<Product_replenishDTO>();
        for(Product_replenish domain : poPage) {
            Product_replenishDTO dto = new Product_replenishDTO();
            dto.fromDO(domain);
            dtos.add(dto);
        }
        return dtos;
    }
}

