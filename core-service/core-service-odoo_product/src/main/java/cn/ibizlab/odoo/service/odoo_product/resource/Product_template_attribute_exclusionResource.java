package cn.ibizlab.odoo.service.odoo_product.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_product.dto.Product_template_attribute_exclusionDTO;
import cn.ibizlab.odoo.core.odoo_product.domain.Product_template_attribute_exclusion;
import cn.ibizlab.odoo.core.odoo_product.service.IProduct_template_attribute_exclusionService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_product.filter.Product_template_attribute_exclusionSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Product_template_attribute_exclusion" })
@RestController
@RequestMapping("")
public class Product_template_attribute_exclusionResource {

    @Autowired
    private IProduct_template_attribute_exclusionService product_template_attribute_exclusionService;

    public IProduct_template_attribute_exclusionService getProduct_template_attribute_exclusionService() {
        return this.product_template_attribute_exclusionService;
    }

    @ApiOperation(value = "建立数据", tags = {"Product_template_attribute_exclusion" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_product/product_template_attribute_exclusions")

    public ResponseEntity<Product_template_attribute_exclusionDTO> create(@RequestBody Product_template_attribute_exclusionDTO product_template_attribute_exclusiondto) {
        Product_template_attribute_exclusionDTO dto = new Product_template_attribute_exclusionDTO();
        Product_template_attribute_exclusion domain = product_template_attribute_exclusiondto.toDO();
		product_template_attribute_exclusionService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批更新数据", tags = {"Product_template_attribute_exclusion" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_product/product_template_attribute_exclusions/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Product_template_attribute_exclusionDTO> product_template_attribute_exclusiondtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "获取数据", tags = {"Product_template_attribute_exclusion" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_product/product_template_attribute_exclusions/{product_template_attribute_exclusion_id}")
    public ResponseEntity<Product_template_attribute_exclusionDTO> get(@PathVariable("product_template_attribute_exclusion_id") Integer product_template_attribute_exclusion_id) {
        Product_template_attribute_exclusionDTO dto = new Product_template_attribute_exclusionDTO();
        Product_template_attribute_exclusion domain = product_template_attribute_exclusionService.get(product_template_attribute_exclusion_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "更新数据", tags = {"Product_template_attribute_exclusion" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_product/product_template_attribute_exclusions/{product_template_attribute_exclusion_id}")

    public ResponseEntity<Product_template_attribute_exclusionDTO> update(@PathVariable("product_template_attribute_exclusion_id") Integer product_template_attribute_exclusion_id, @RequestBody Product_template_attribute_exclusionDTO product_template_attribute_exclusiondto) {
		Product_template_attribute_exclusion domain = product_template_attribute_exclusiondto.toDO();
        domain.setId(product_template_attribute_exclusion_id);
		product_template_attribute_exclusionService.update(domain);
		Product_template_attribute_exclusionDTO dto = new Product_template_attribute_exclusionDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批建立数据", tags = {"Product_template_attribute_exclusion" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_product/product_template_attribute_exclusions/createBatch")
    public ResponseEntity<Boolean> createBatchProduct_template_attribute_exclusion(@RequestBody List<Product_template_attribute_exclusionDTO> product_template_attribute_exclusiondtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "批删除数据", tags = {"Product_template_attribute_exclusion" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_product/product_template_attribute_exclusions/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Product_template_attribute_exclusionDTO> product_template_attribute_exclusiondtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "删除数据", tags = {"Product_template_attribute_exclusion" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_product/product_template_attribute_exclusions/{product_template_attribute_exclusion_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("product_template_attribute_exclusion_id") Integer product_template_attribute_exclusion_id) {
        Product_template_attribute_exclusionDTO product_template_attribute_exclusiondto = new Product_template_attribute_exclusionDTO();
		Product_template_attribute_exclusion domain = new Product_template_attribute_exclusion();
		product_template_attribute_exclusiondto.setId(product_template_attribute_exclusion_id);
		domain.setId(product_template_attribute_exclusion_id);
        Boolean rst = product_template_attribute_exclusionService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

	@ApiOperation(value = "获取默认查询", tags = {"Product_template_attribute_exclusion" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_product/product_template_attribute_exclusions/fetchdefault")
	public ResponseEntity<Page<Product_template_attribute_exclusionDTO>> fetchDefault(Product_template_attribute_exclusionSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Product_template_attribute_exclusionDTO> list = new ArrayList<Product_template_attribute_exclusionDTO>();
        
        Page<Product_template_attribute_exclusion> domains = product_template_attribute_exclusionService.searchDefault(context) ;
        for(Product_template_attribute_exclusion product_template_attribute_exclusion : domains.getContent()){
            Product_template_attribute_exclusionDTO dto = new Product_template_attribute_exclusionDTO();
            dto.fromDO(product_template_attribute_exclusion);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
