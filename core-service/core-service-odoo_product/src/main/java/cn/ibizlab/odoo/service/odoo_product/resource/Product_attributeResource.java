package cn.ibizlab.odoo.service.odoo_product.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_product.dto.Product_attributeDTO;
import cn.ibizlab.odoo.core.odoo_product.domain.Product_attribute;
import cn.ibizlab.odoo.core.odoo_product.service.IProduct_attributeService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_product.filter.Product_attributeSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Product_attribute" })
@RestController
@RequestMapping("")
public class Product_attributeResource {

    @Autowired
    private IProduct_attributeService product_attributeService;

    public IProduct_attributeService getProduct_attributeService() {
        return this.product_attributeService;
    }

    @ApiOperation(value = "建立数据", tags = {"Product_attribute" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_product/product_attributes")

    public ResponseEntity<Product_attributeDTO> create(@RequestBody Product_attributeDTO product_attributedto) {
        Product_attributeDTO dto = new Product_attributeDTO();
        Product_attribute domain = product_attributedto.toDO();
		product_attributeService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "更新数据", tags = {"Product_attribute" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_product/product_attributes/{product_attribute_id}")

    public ResponseEntity<Product_attributeDTO> update(@PathVariable("product_attribute_id") Integer product_attribute_id, @RequestBody Product_attributeDTO product_attributedto) {
		Product_attribute domain = product_attributedto.toDO();
        domain.setId(product_attribute_id);
		product_attributeService.update(domain);
		Product_attributeDTO dto = new Product_attributeDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "删除数据", tags = {"Product_attribute" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_product/product_attributes/{product_attribute_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("product_attribute_id") Integer product_attribute_id) {
        Product_attributeDTO product_attributedto = new Product_attributeDTO();
		Product_attribute domain = new Product_attribute();
		product_attributedto.setId(product_attribute_id);
		domain.setId(product_attribute_id);
        Boolean rst = product_attributeService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "批删除数据", tags = {"Product_attribute" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_product/product_attributes/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Product_attributeDTO> product_attributedtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "批建立数据", tags = {"Product_attribute" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_product/product_attributes/createBatch")
    public ResponseEntity<Boolean> createBatchProduct_attribute(@RequestBody List<Product_attributeDTO> product_attributedtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "批更新数据", tags = {"Product_attribute" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_product/product_attributes/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Product_attributeDTO> product_attributedtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "获取数据", tags = {"Product_attribute" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_product/product_attributes/{product_attribute_id}")
    public ResponseEntity<Product_attributeDTO> get(@PathVariable("product_attribute_id") Integer product_attribute_id) {
        Product_attributeDTO dto = new Product_attributeDTO();
        Product_attribute domain = product_attributeService.get(product_attribute_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Product_attribute" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_product/product_attributes/fetchdefault")
	public ResponseEntity<Page<Product_attributeDTO>> fetchDefault(Product_attributeSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Product_attributeDTO> list = new ArrayList<Product_attributeDTO>();
        
        Page<Product_attribute> domains = product_attributeService.searchDefault(context) ;
        for(Product_attribute product_attribute : domains.getContent()){
            Product_attributeDTO dto = new Product_attributeDTO();
            dto.fromDO(product_attribute);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
