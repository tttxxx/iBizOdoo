package cn.ibizlab.odoo.service.odoo_product.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_product.dto.Product_attribute_custom_valueDTO;
import cn.ibizlab.odoo.core.odoo_product.domain.Product_attribute_custom_value;
import cn.ibizlab.odoo.core.odoo_product.service.IProduct_attribute_custom_valueService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_product.filter.Product_attribute_custom_valueSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Product_attribute_custom_value" })
@RestController
@RequestMapping("")
public class Product_attribute_custom_valueResource {

    @Autowired
    private IProduct_attribute_custom_valueService product_attribute_custom_valueService;

    public IProduct_attribute_custom_valueService getProduct_attribute_custom_valueService() {
        return this.product_attribute_custom_valueService;
    }

    @ApiOperation(value = "批删除数据", tags = {"Product_attribute_custom_value" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_product/product_attribute_custom_values/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Product_attribute_custom_valueDTO> product_attribute_custom_valuedtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "建立数据", tags = {"Product_attribute_custom_value" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_product/product_attribute_custom_values")

    public ResponseEntity<Product_attribute_custom_valueDTO> create(@RequestBody Product_attribute_custom_valueDTO product_attribute_custom_valuedto) {
        Product_attribute_custom_valueDTO dto = new Product_attribute_custom_valueDTO();
        Product_attribute_custom_value domain = product_attribute_custom_valuedto.toDO();
		product_attribute_custom_valueService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "删除数据", tags = {"Product_attribute_custom_value" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_product/product_attribute_custom_values/{product_attribute_custom_value_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("product_attribute_custom_value_id") Integer product_attribute_custom_value_id) {
        Product_attribute_custom_valueDTO product_attribute_custom_valuedto = new Product_attribute_custom_valueDTO();
		Product_attribute_custom_value domain = new Product_attribute_custom_value();
		product_attribute_custom_valuedto.setId(product_attribute_custom_value_id);
		domain.setId(product_attribute_custom_value_id);
        Boolean rst = product_attribute_custom_valueService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "更新数据", tags = {"Product_attribute_custom_value" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_product/product_attribute_custom_values/{product_attribute_custom_value_id}")

    public ResponseEntity<Product_attribute_custom_valueDTO> update(@PathVariable("product_attribute_custom_value_id") Integer product_attribute_custom_value_id, @RequestBody Product_attribute_custom_valueDTO product_attribute_custom_valuedto) {
		Product_attribute_custom_value domain = product_attribute_custom_valuedto.toDO();
        domain.setId(product_attribute_custom_value_id);
		product_attribute_custom_valueService.update(domain);
		Product_attribute_custom_valueDTO dto = new Product_attribute_custom_valueDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批更新数据", tags = {"Product_attribute_custom_value" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_product/product_attribute_custom_values/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Product_attribute_custom_valueDTO> product_attribute_custom_valuedtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "获取数据", tags = {"Product_attribute_custom_value" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_product/product_attribute_custom_values/{product_attribute_custom_value_id}")
    public ResponseEntity<Product_attribute_custom_valueDTO> get(@PathVariable("product_attribute_custom_value_id") Integer product_attribute_custom_value_id) {
        Product_attribute_custom_valueDTO dto = new Product_attribute_custom_valueDTO();
        Product_attribute_custom_value domain = product_attribute_custom_valueService.get(product_attribute_custom_value_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批建立数据", tags = {"Product_attribute_custom_value" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_product/product_attribute_custom_values/createBatch")
    public ResponseEntity<Boolean> createBatchProduct_attribute_custom_value(@RequestBody List<Product_attribute_custom_valueDTO> product_attribute_custom_valuedtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Product_attribute_custom_value" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_product/product_attribute_custom_values/fetchdefault")
	public ResponseEntity<Page<Product_attribute_custom_valueDTO>> fetchDefault(Product_attribute_custom_valueSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Product_attribute_custom_valueDTO> list = new ArrayList<Product_attribute_custom_valueDTO>();
        
        Page<Product_attribute_custom_value> domains = product_attribute_custom_valueService.searchDefault(context) ;
        for(Product_attribute_custom_value product_attribute_custom_value : domains.getContent()){
            Product_attribute_custom_valueDTO dto = new Product_attribute_custom_valueDTO();
            dto.fromDO(product_attribute_custom_value);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
