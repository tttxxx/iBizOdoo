package cn.ibizlab.odoo.service.odoo_product.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_product.dto.Product_template_attribute_lineDTO;
import cn.ibizlab.odoo.core.odoo_product.domain.Product_template_attribute_line;
import cn.ibizlab.odoo.core.odoo_product.service.IProduct_template_attribute_lineService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_product.filter.Product_template_attribute_lineSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Product_template_attribute_line" })
@RestController
@RequestMapping("")
public class Product_template_attribute_lineResource {

    @Autowired
    private IProduct_template_attribute_lineService product_template_attribute_lineService;

    public IProduct_template_attribute_lineService getProduct_template_attribute_lineService() {
        return this.product_template_attribute_lineService;
    }

    @ApiOperation(value = "获取数据", tags = {"Product_template_attribute_line" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_product/product_template_attribute_lines/{product_template_attribute_line_id}")
    public ResponseEntity<Product_template_attribute_lineDTO> get(@PathVariable("product_template_attribute_line_id") Integer product_template_attribute_line_id) {
        Product_template_attribute_lineDTO dto = new Product_template_attribute_lineDTO();
        Product_template_attribute_line domain = product_template_attribute_lineService.get(product_template_attribute_line_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "删除数据", tags = {"Product_template_attribute_line" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_product/product_template_attribute_lines/{product_template_attribute_line_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("product_template_attribute_line_id") Integer product_template_attribute_line_id) {
        Product_template_attribute_lineDTO product_template_attribute_linedto = new Product_template_attribute_lineDTO();
		Product_template_attribute_line domain = new Product_template_attribute_line();
		product_template_attribute_linedto.setId(product_template_attribute_line_id);
		domain.setId(product_template_attribute_line_id);
        Boolean rst = product_template_attribute_lineService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "更新数据", tags = {"Product_template_attribute_line" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_product/product_template_attribute_lines/{product_template_attribute_line_id}")

    public ResponseEntity<Product_template_attribute_lineDTO> update(@PathVariable("product_template_attribute_line_id") Integer product_template_attribute_line_id, @RequestBody Product_template_attribute_lineDTO product_template_attribute_linedto) {
		Product_template_attribute_line domain = product_template_attribute_linedto.toDO();
        domain.setId(product_template_attribute_line_id);
		product_template_attribute_lineService.update(domain);
		Product_template_attribute_lineDTO dto = new Product_template_attribute_lineDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批删除数据", tags = {"Product_template_attribute_line" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_product/product_template_attribute_lines/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Product_template_attribute_lineDTO> product_template_attribute_linedtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "批建立数据", tags = {"Product_template_attribute_line" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_product/product_template_attribute_lines/createBatch")
    public ResponseEntity<Boolean> createBatchProduct_template_attribute_line(@RequestBody List<Product_template_attribute_lineDTO> product_template_attribute_linedtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "批更新数据", tags = {"Product_template_attribute_line" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_product/product_template_attribute_lines/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Product_template_attribute_lineDTO> product_template_attribute_linedtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "建立数据", tags = {"Product_template_attribute_line" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_product/product_template_attribute_lines")

    public ResponseEntity<Product_template_attribute_lineDTO> create(@RequestBody Product_template_attribute_lineDTO product_template_attribute_linedto) {
        Product_template_attribute_lineDTO dto = new Product_template_attribute_lineDTO();
        Product_template_attribute_line domain = product_template_attribute_linedto.toDO();
		product_template_attribute_lineService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Product_template_attribute_line" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_product/product_template_attribute_lines/fetchdefault")
	public ResponseEntity<Page<Product_template_attribute_lineDTO>> fetchDefault(Product_template_attribute_lineSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Product_template_attribute_lineDTO> list = new ArrayList<Product_template_attribute_lineDTO>();
        
        Page<Product_template_attribute_line> domains = product_template_attribute_lineService.searchDefault(context) ;
        for(Product_template_attribute_line product_template_attribute_line : domains.getContent()){
            Product_template_attribute_lineDTO dto = new Product_template_attribute_lineDTO();
            dto.fromDO(product_template_attribute_line);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
