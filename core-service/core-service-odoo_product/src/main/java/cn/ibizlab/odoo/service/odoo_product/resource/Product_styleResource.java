package cn.ibizlab.odoo.service.odoo_product.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_product.dto.Product_styleDTO;
import cn.ibizlab.odoo.core.odoo_product.domain.Product_style;
import cn.ibizlab.odoo.core.odoo_product.service.IProduct_styleService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_product.filter.Product_styleSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Product_style" })
@RestController
@RequestMapping("")
public class Product_styleResource {

    @Autowired
    private IProduct_styleService product_styleService;

    public IProduct_styleService getProduct_styleService() {
        return this.product_styleService;
    }

    @ApiOperation(value = "删除数据", tags = {"Product_style" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_product/product_styles/{product_style_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("product_style_id") Integer product_style_id) {
        Product_styleDTO product_styledto = new Product_styleDTO();
		Product_style domain = new Product_style();
		product_styledto.setId(product_style_id);
		domain.setId(product_style_id);
        Boolean rst = product_styleService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "获取数据", tags = {"Product_style" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_product/product_styles/{product_style_id}")
    public ResponseEntity<Product_styleDTO> get(@PathVariable("product_style_id") Integer product_style_id) {
        Product_styleDTO dto = new Product_styleDTO();
        Product_style domain = product_styleService.get(product_style_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "建立数据", tags = {"Product_style" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_product/product_styles")

    public ResponseEntity<Product_styleDTO> create(@RequestBody Product_styleDTO product_styledto) {
        Product_styleDTO dto = new Product_styleDTO();
        Product_style domain = product_styledto.toDO();
		product_styleService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批更新数据", tags = {"Product_style" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_product/product_styles/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Product_styleDTO> product_styledtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "批建立数据", tags = {"Product_style" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_product/product_styles/createBatch")
    public ResponseEntity<Boolean> createBatchProduct_style(@RequestBody List<Product_styleDTO> product_styledtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "更新数据", tags = {"Product_style" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_product/product_styles/{product_style_id}")

    public ResponseEntity<Product_styleDTO> update(@PathVariable("product_style_id") Integer product_style_id, @RequestBody Product_styleDTO product_styledto) {
		Product_style domain = product_styledto.toDO();
        domain.setId(product_style_id);
		product_styleService.update(domain);
		Product_styleDTO dto = new Product_styleDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批删除数据", tags = {"Product_style" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_product/product_styles/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Product_styleDTO> product_styledtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Product_style" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_product/product_styles/fetchdefault")
	public ResponseEntity<Page<Product_styleDTO>> fetchDefault(Product_styleSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Product_styleDTO> list = new ArrayList<Product_styleDTO>();
        
        Page<Product_style> domains = product_styleService.searchDefault(context) ;
        for(Product_style product_style : domains.getContent()){
            Product_styleDTO dto = new Product_styleDTO();
            dto.fromDO(product_style);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
