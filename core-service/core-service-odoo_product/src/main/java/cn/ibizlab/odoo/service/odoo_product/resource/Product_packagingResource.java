package cn.ibizlab.odoo.service.odoo_product.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_product.dto.Product_packagingDTO;
import cn.ibizlab.odoo.core.odoo_product.domain.Product_packaging;
import cn.ibizlab.odoo.core.odoo_product.service.IProduct_packagingService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_product.filter.Product_packagingSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Product_packaging" })
@RestController
@RequestMapping("")
public class Product_packagingResource {

    @Autowired
    private IProduct_packagingService product_packagingService;

    public IProduct_packagingService getProduct_packagingService() {
        return this.product_packagingService;
    }

    @ApiOperation(value = "批更新数据", tags = {"Product_packaging" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_product/product_packagings/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Product_packagingDTO> product_packagingdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "删除数据", tags = {"Product_packaging" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_product/product_packagings/{product_packaging_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("product_packaging_id") Integer product_packaging_id) {
        Product_packagingDTO product_packagingdto = new Product_packagingDTO();
		Product_packaging domain = new Product_packaging();
		product_packagingdto.setId(product_packaging_id);
		domain.setId(product_packaging_id);
        Boolean rst = product_packagingService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "批建立数据", tags = {"Product_packaging" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_product/product_packagings/createBatch")
    public ResponseEntity<Boolean> createBatchProduct_packaging(@RequestBody List<Product_packagingDTO> product_packagingdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "批删除数据", tags = {"Product_packaging" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_product/product_packagings/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Product_packagingDTO> product_packagingdtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "建立数据", tags = {"Product_packaging" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_product/product_packagings")

    public ResponseEntity<Product_packagingDTO> create(@RequestBody Product_packagingDTO product_packagingdto) {
        Product_packagingDTO dto = new Product_packagingDTO();
        Product_packaging domain = product_packagingdto.toDO();
		product_packagingService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取数据", tags = {"Product_packaging" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_product/product_packagings/{product_packaging_id}")
    public ResponseEntity<Product_packagingDTO> get(@PathVariable("product_packaging_id") Integer product_packaging_id) {
        Product_packagingDTO dto = new Product_packagingDTO();
        Product_packaging domain = product_packagingService.get(product_packaging_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "更新数据", tags = {"Product_packaging" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_product/product_packagings/{product_packaging_id}")

    public ResponseEntity<Product_packagingDTO> update(@PathVariable("product_packaging_id") Integer product_packaging_id, @RequestBody Product_packagingDTO product_packagingdto) {
		Product_packaging domain = product_packagingdto.toDO();
        domain.setId(product_packaging_id);
		product_packagingService.update(domain);
		Product_packagingDTO dto = new Product_packagingDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Product_packaging" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_product/product_packagings/fetchdefault")
	public ResponseEntity<Page<Product_packagingDTO>> fetchDefault(Product_packagingSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Product_packagingDTO> list = new ArrayList<Product_packagingDTO>();
        
        Page<Product_packaging> domains = product_packagingService.searchDefault(context) ;
        for(Product_packaging product_packaging : domains.getContent()){
            Product_packagingDTO dto = new Product_packagingDTO();
            dto.fromDO(product_packaging);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
