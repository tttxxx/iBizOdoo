package cn.ibizlab.odoo.service.odoo_product.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_product.dto.Product_pricelistDTO;
import cn.ibizlab.odoo.core.odoo_product.domain.Product_pricelist;
import cn.ibizlab.odoo.core.odoo_product.service.IProduct_pricelistService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_product.filter.Product_pricelistSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Product_pricelist" })
@RestController
@RequestMapping("")
public class Product_pricelistResource {

    @Autowired
    private IProduct_pricelistService product_pricelistService;

    public IProduct_pricelistService getProduct_pricelistService() {
        return this.product_pricelistService;
    }

    @ApiOperation(value = "获取数据", tags = {"Product_pricelist" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_product/product_pricelists/{product_pricelist_id}")
    public ResponseEntity<Product_pricelistDTO> get(@PathVariable("product_pricelist_id") Integer product_pricelist_id) {
        Product_pricelistDTO dto = new Product_pricelistDTO();
        Product_pricelist domain = product_pricelistService.get(product_pricelist_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批删除数据", tags = {"Product_pricelist" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_product/product_pricelists/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Product_pricelistDTO> product_pricelistdtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "更新数据", tags = {"Product_pricelist" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_product/product_pricelists/{product_pricelist_id}")

    public ResponseEntity<Product_pricelistDTO> update(@PathVariable("product_pricelist_id") Integer product_pricelist_id, @RequestBody Product_pricelistDTO product_pricelistdto) {
		Product_pricelist domain = product_pricelistdto.toDO();
        domain.setId(product_pricelist_id);
		product_pricelistService.update(domain);
		Product_pricelistDTO dto = new Product_pricelistDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "删除数据", tags = {"Product_pricelist" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_product/product_pricelists/{product_pricelist_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("product_pricelist_id") Integer product_pricelist_id) {
        Product_pricelistDTO product_pricelistdto = new Product_pricelistDTO();
		Product_pricelist domain = new Product_pricelist();
		product_pricelistdto.setId(product_pricelist_id);
		domain.setId(product_pricelist_id);
        Boolean rst = product_pricelistService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "批更新数据", tags = {"Product_pricelist" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_product/product_pricelists/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Product_pricelistDTO> product_pricelistdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "批建立数据", tags = {"Product_pricelist" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_product/product_pricelists/createBatch")
    public ResponseEntity<Boolean> createBatchProduct_pricelist(@RequestBody List<Product_pricelistDTO> product_pricelistdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "建立数据", tags = {"Product_pricelist" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_product/product_pricelists")

    public ResponseEntity<Product_pricelistDTO> create(@RequestBody Product_pricelistDTO product_pricelistdto) {
        Product_pricelistDTO dto = new Product_pricelistDTO();
        Product_pricelist domain = product_pricelistdto.toDO();
		product_pricelistService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Product_pricelist" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_product/product_pricelists/fetchdefault")
	public ResponseEntity<Page<Product_pricelistDTO>> fetchDefault(Product_pricelistSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Product_pricelistDTO> list = new ArrayList<Product_pricelistDTO>();
        
        Page<Product_pricelist> domains = product_pricelistService.searchDefault(context) ;
        for(Product_pricelist product_pricelist : domains.getContent()){
            Product_pricelistDTO dto = new Product_pricelistDTO();
            dto.fromDO(product_pricelist);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
