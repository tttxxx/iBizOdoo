package cn.ibizlab.odoo.service.odoo_product.config;

import org.springframework.context.annotation.ComponentScan;

@ComponentScan("cn.ibizlab.odoo.service.odoo_product")
public class odoo_productRestConfiguration {

}
