package cn.ibizlab.odoo.service.odoo_product.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import lombok.Data;

@ConfigurationProperties(prefix = "service.odoo.product")
@Data
public class odoo_productServiceProperties {

	private boolean enabled;

	private boolean auth;


}