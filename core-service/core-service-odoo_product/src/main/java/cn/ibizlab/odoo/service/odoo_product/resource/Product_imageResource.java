package cn.ibizlab.odoo.service.odoo_product.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_product.dto.Product_imageDTO;
import cn.ibizlab.odoo.core.odoo_product.domain.Product_image;
import cn.ibizlab.odoo.core.odoo_product.service.IProduct_imageService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_product.filter.Product_imageSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Product_image" })
@RestController
@RequestMapping("")
public class Product_imageResource {

    @Autowired
    private IProduct_imageService product_imageService;

    public IProduct_imageService getProduct_imageService() {
        return this.product_imageService;
    }

    @ApiOperation(value = "更新数据", tags = {"Product_image" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_product/product_images/{product_image_id}")

    public ResponseEntity<Product_imageDTO> update(@PathVariable("product_image_id") Integer product_image_id, @RequestBody Product_imageDTO product_imagedto) {
		Product_image domain = product_imagedto.toDO();
        domain.setId(product_image_id);
		product_imageService.update(domain);
		Product_imageDTO dto = new Product_imageDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "删除数据", tags = {"Product_image" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_product/product_images/{product_image_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("product_image_id") Integer product_image_id) {
        Product_imageDTO product_imagedto = new Product_imageDTO();
		Product_image domain = new Product_image();
		product_imagedto.setId(product_image_id);
		domain.setId(product_image_id);
        Boolean rst = product_imageService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "批删除数据", tags = {"Product_image" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_product/product_images/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Product_imageDTO> product_imagedtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "获取数据", tags = {"Product_image" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_product/product_images/{product_image_id}")
    public ResponseEntity<Product_imageDTO> get(@PathVariable("product_image_id") Integer product_image_id) {
        Product_imageDTO dto = new Product_imageDTO();
        Product_image domain = product_imageService.get(product_image_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "建立数据", tags = {"Product_image" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_product/product_images")

    public ResponseEntity<Product_imageDTO> create(@RequestBody Product_imageDTO product_imagedto) {
        Product_imageDTO dto = new Product_imageDTO();
        Product_image domain = product_imagedto.toDO();
		product_imageService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批建立数据", tags = {"Product_image" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_product/product_images/createBatch")
    public ResponseEntity<Boolean> createBatchProduct_image(@RequestBody List<Product_imageDTO> product_imagedtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "批更新数据", tags = {"Product_image" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_product/product_images/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Product_imageDTO> product_imagedtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Product_image" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_product/product_images/fetchdefault")
	public ResponseEntity<Page<Product_imageDTO>> fetchDefault(Product_imageSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Product_imageDTO> list = new ArrayList<Product_imageDTO>();
        
        Page<Product_image> domains = product_imageService.searchDefault(context) ;
        for(Product_image product_image : domains.getContent()){
            Product_imageDTO dto = new Product_imageDTO();
            dto.fromDO(product_image);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
