package cn.ibizlab.odoo.service.odoo_product.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_product.dto.Product_price_historyDTO;
import cn.ibizlab.odoo.core.odoo_product.domain.Product_price_history;
import cn.ibizlab.odoo.core.odoo_product.service.IProduct_price_historyService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_product.filter.Product_price_historySearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Product_price_history" })
@RestController
@RequestMapping("")
public class Product_price_historyResource {

    @Autowired
    private IProduct_price_historyService product_price_historyService;

    public IProduct_price_historyService getProduct_price_historyService() {
        return this.product_price_historyService;
    }

    @ApiOperation(value = "建立数据", tags = {"Product_price_history" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_product/product_price_histories")

    public ResponseEntity<Product_price_historyDTO> create(@RequestBody Product_price_historyDTO product_price_historydto) {
        Product_price_historyDTO dto = new Product_price_historyDTO();
        Product_price_history domain = product_price_historydto.toDO();
		product_price_historyService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批建立数据", tags = {"Product_price_history" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_product/product_price_histories/createBatch")
    public ResponseEntity<Boolean> createBatchProduct_price_history(@RequestBody List<Product_price_historyDTO> product_price_historydtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "删除数据", tags = {"Product_price_history" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_product/product_price_histories/{product_price_history_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("product_price_history_id") Integer product_price_history_id) {
        Product_price_historyDTO product_price_historydto = new Product_price_historyDTO();
		Product_price_history domain = new Product_price_history();
		product_price_historydto.setId(product_price_history_id);
		domain.setId(product_price_history_id);
        Boolean rst = product_price_historyService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "批删除数据", tags = {"Product_price_history" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_product/product_price_histories/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Product_price_historyDTO> product_price_historydtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "获取数据", tags = {"Product_price_history" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_product/product_price_histories/{product_price_history_id}")
    public ResponseEntity<Product_price_historyDTO> get(@PathVariable("product_price_history_id") Integer product_price_history_id) {
        Product_price_historyDTO dto = new Product_price_historyDTO();
        Product_price_history domain = product_price_historyService.get(product_price_history_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "更新数据", tags = {"Product_price_history" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_product/product_price_histories/{product_price_history_id}")

    public ResponseEntity<Product_price_historyDTO> update(@PathVariable("product_price_history_id") Integer product_price_history_id, @RequestBody Product_price_historyDTO product_price_historydto) {
		Product_price_history domain = product_price_historydto.toDO();
        domain.setId(product_price_history_id);
		product_price_historyService.update(domain);
		Product_price_historyDTO dto = new Product_price_historyDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批更新数据", tags = {"Product_price_history" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_product/product_price_histories/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Product_price_historyDTO> product_price_historydtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Product_price_history" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_product/product_price_histories/fetchdefault")
	public ResponseEntity<Page<Product_price_historyDTO>> fetchDefault(Product_price_historySearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Product_price_historyDTO> list = new ArrayList<Product_price_historyDTO>();
        
        Page<Product_price_history> domains = product_price_historyService.searchDefault(context) ;
        for(Product_price_history product_price_history : domains.getContent()){
            Product_price_historyDTO dto = new Product_price_historyDTO();
            dto.fromDO(product_price_history);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
