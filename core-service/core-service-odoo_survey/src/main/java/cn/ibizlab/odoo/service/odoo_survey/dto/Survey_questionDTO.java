package cn.ibizlab.odoo.service.odoo_survey.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.core.odoo_survey.valuerule.anno.survey_question.*;
import cn.ibizlab.odoo.core.odoo_survey.domain.Survey_question;
import org.springframework.cglib.beans.BeanCopier;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 服务DTO对象[Survey_questionDTO]
 */
public class Survey_questionDTO implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [MATRIX_SUBTYPE]
     *
     */
    @Survey_questionMatrix_subtypeDefault(info = "默认规则")
    private String matrix_subtype;

    @JsonIgnore
    private boolean matrix_subtypeDirtyFlag;

    /**
     * 属性 [VALIDATION_ERROR_MSG]
     *
     */
    @Survey_questionValidation_error_msgDefault(info = "默认规则")
    private String validation_error_msg;

    @JsonIgnore
    private boolean validation_error_msgDirtyFlag;

    /**
     * 属性 [CREATE_DATE]
     *
     */
    @Survey_questionCreate_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp create_date;

    @JsonIgnore
    private boolean create_dateDirtyFlag;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @Survey_question__last_updateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp __last_update;

    @JsonIgnore
    private boolean __last_updateDirtyFlag;

    /**
     * 属性 [TYPE]
     *
     */
    @Survey_questionTypeDefault(info = "默认规则")
    private String type;

    @JsonIgnore
    private boolean typeDirtyFlag;

    /**
     * 属性 [COMMENTS_ALLOWED]
     *
     */
    @Survey_questionComments_allowedDefault(info = "默认规则")
    private String comments_allowed;

    @JsonIgnore
    private boolean comments_allowedDirtyFlag;

    /**
     * 属性 [VALIDATION_MAX_DATE]
     *
     */
    @Survey_questionValidation_max_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    private Timestamp validation_max_date;

    @JsonIgnore
    private boolean validation_max_dateDirtyFlag;

    /**
     * 属性 [LABELS_IDS_2]
     *
     */
    @Survey_questionLabels_ids_2Default(info = "默认规则")
    private String labels_ids_2;

    @JsonIgnore
    private boolean labels_ids_2DirtyFlag;

    /**
     * 属性 [ID]
     *
     */
    @Survey_questionIdDefault(info = "默认规则")
    private Integer id;

    @JsonIgnore
    private boolean idDirtyFlag;

    /**
     * 属性 [VALIDATION_MAX_FLOAT_VALUE]
     *
     */
    @Survey_questionValidation_max_float_valueDefault(info = "默认规则")
    private Double validation_max_float_value;

    @JsonIgnore
    private boolean validation_max_float_valueDirtyFlag;

    /**
     * 属性 [QUESTION]
     *
     */
    @Survey_questionQuestionDefault(info = "默认规则")
    private String question;

    @JsonIgnore
    private boolean questionDirtyFlag;

    /**
     * 属性 [LABELS_IDS]
     *
     */
    @Survey_questionLabels_idsDefault(info = "默认规则")
    private String labels_ids;

    @JsonIgnore
    private boolean labels_idsDirtyFlag;

    /**
     * 属性 [VALIDATION_MIN_FLOAT_VALUE]
     *
     */
    @Survey_questionValidation_min_float_valueDefault(info = "默认规则")
    private Double validation_min_float_value;

    @JsonIgnore
    private boolean validation_min_float_valueDirtyFlag;

    /**
     * 属性 [SEQUENCE]
     *
     */
    @Survey_questionSequenceDefault(info = "默认规则")
    private Integer sequence;

    @JsonIgnore
    private boolean sequenceDirtyFlag;

    /**
     * 属性 [WRITE_DATE]
     *
     */
    @Survey_questionWrite_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp write_date;

    @JsonIgnore
    private boolean write_dateDirtyFlag;

    /**
     * 属性 [CONSTR_MANDATORY]
     *
     */
    @Survey_questionConstr_mandatoryDefault(info = "默认规则")
    private String constr_mandatory;

    @JsonIgnore
    private boolean constr_mandatoryDirtyFlag;

    /**
     * 属性 [USER_INPUT_LINE_IDS]
     *
     */
    @Survey_questionUser_input_line_idsDefault(info = "默认规则")
    private String user_input_line_ids;

    @JsonIgnore
    private boolean user_input_line_idsDirtyFlag;

    /**
     * 属性 [COMMENTS_MESSAGE]
     *
     */
    @Survey_questionComments_messageDefault(info = "默认规则")
    private String comments_message;

    @JsonIgnore
    private boolean comments_messageDirtyFlag;

    /**
     * 属性 [COMMENT_COUNT_AS_ANSWER]
     *
     */
    @Survey_questionComment_count_as_answerDefault(info = "默认规则")
    private String comment_count_as_answer;

    @JsonIgnore
    private boolean comment_count_as_answerDirtyFlag;

    /**
     * 属性 [VALIDATION_REQUIRED]
     *
     */
    @Survey_questionValidation_requiredDefault(info = "默认规则")
    private String validation_required;

    @JsonIgnore
    private boolean validation_requiredDirtyFlag;

    /**
     * 属性 [VALIDATION_EMAIL]
     *
     */
    @Survey_questionValidation_emailDefault(info = "默认规则")
    private String validation_email;

    @JsonIgnore
    private boolean validation_emailDirtyFlag;

    /**
     * 属性 [VALIDATION_MIN_DATE]
     *
     */
    @Survey_questionValidation_min_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    private Timestamp validation_min_date;

    @JsonIgnore
    private boolean validation_min_dateDirtyFlag;

    /**
     * 属性 [CONSTR_ERROR_MSG]
     *
     */
    @Survey_questionConstr_error_msgDefault(info = "默认规则")
    private String constr_error_msg;

    @JsonIgnore
    private boolean constr_error_msgDirtyFlag;

    /**
     * 属性 [VALIDATION_LENGTH_MAX]
     *
     */
    @Survey_questionValidation_length_maxDefault(info = "默认规则")
    private Integer validation_length_max;

    @JsonIgnore
    private boolean validation_length_maxDirtyFlag;

    /**
     * 属性 [COLUMN_NB]
     *
     */
    @Survey_questionColumn_nbDefault(info = "默认规则")
    private String column_nb;

    @JsonIgnore
    private boolean column_nbDirtyFlag;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @Survey_questionDisplay_nameDefault(info = "默认规则")
    private String display_name;

    @JsonIgnore
    private boolean display_nameDirtyFlag;

    /**
     * 属性 [DISPLAY_MODE]
     *
     */
    @Survey_questionDisplay_modeDefault(info = "默认规则")
    private String display_mode;

    @JsonIgnore
    private boolean display_modeDirtyFlag;

    /**
     * 属性 [VALIDATION_LENGTH_MIN]
     *
     */
    @Survey_questionValidation_length_minDefault(info = "默认规则")
    private Integer validation_length_min;

    @JsonIgnore
    private boolean validation_length_minDirtyFlag;

    /**
     * 属性 [DESCRIPTION]
     *
     */
    @Survey_questionDescriptionDefault(info = "默认规则")
    private String description;

    @JsonIgnore
    private boolean descriptionDirtyFlag;

    /**
     * 属性 [WRITE_UID_TEXT]
     *
     */
    @Survey_questionWrite_uid_textDefault(info = "默认规则")
    private String write_uid_text;

    @JsonIgnore
    private boolean write_uid_textDirtyFlag;

    /**
     * 属性 [CREATE_UID_TEXT]
     *
     */
    @Survey_questionCreate_uid_textDefault(info = "默认规则")
    private String create_uid_text;

    @JsonIgnore
    private boolean create_uid_textDirtyFlag;

    /**
     * 属性 [SURVEY_ID]
     *
     */
    @Survey_questionSurvey_idDefault(info = "默认规则")
    private Integer survey_id;

    @JsonIgnore
    private boolean survey_idDirtyFlag;

    /**
     * 属性 [WRITE_UID]
     *
     */
    @Survey_questionWrite_uidDefault(info = "默认规则")
    private Integer write_uid;

    @JsonIgnore
    private boolean write_uidDirtyFlag;

    /**
     * 属性 [CREATE_UID]
     *
     */
    @Survey_questionCreate_uidDefault(info = "默认规则")
    private Integer create_uid;

    @JsonIgnore
    private boolean create_uidDirtyFlag;

    /**
     * 属性 [PAGE_ID]
     *
     */
    @Survey_questionPage_idDefault(info = "默认规则")
    private Integer page_id;

    @JsonIgnore
    private boolean page_idDirtyFlag;


    /**
     * 获取 [MATRIX_SUBTYPE]
     */
    @JsonProperty("matrix_subtype")
    public String getMatrix_subtype(){
        return matrix_subtype ;
    }

    /**
     * 设置 [MATRIX_SUBTYPE]
     */
    @JsonProperty("matrix_subtype")
    public void setMatrix_subtype(String  matrix_subtype){
        this.matrix_subtype = matrix_subtype ;
        this.matrix_subtypeDirtyFlag = true ;
    }

    /**
     * 获取 [MATRIX_SUBTYPE]脏标记
     */
    @JsonIgnore
    public boolean getMatrix_subtypeDirtyFlag(){
        return matrix_subtypeDirtyFlag ;
    }

    /**
     * 获取 [VALIDATION_ERROR_MSG]
     */
    @JsonProperty("validation_error_msg")
    public String getValidation_error_msg(){
        return validation_error_msg ;
    }

    /**
     * 设置 [VALIDATION_ERROR_MSG]
     */
    @JsonProperty("validation_error_msg")
    public void setValidation_error_msg(String  validation_error_msg){
        this.validation_error_msg = validation_error_msg ;
        this.validation_error_msgDirtyFlag = true ;
    }

    /**
     * 获取 [VALIDATION_ERROR_MSG]脏标记
     */
    @JsonIgnore
    public boolean getValidation_error_msgDirtyFlag(){
        return validation_error_msgDirtyFlag ;
    }

    /**
     * 获取 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return create_date ;
    }

    /**
     * 设置 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return create_dateDirtyFlag ;
    }

    /**
     * 获取 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return __last_update ;
    }

    /**
     * 设置 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

    /**
     * 获取 [__LAST_UPDATE]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return __last_updateDirtyFlag ;
    }

    /**
     * 获取 [TYPE]
     */
    @JsonProperty("type")
    public String getType(){
        return type ;
    }

    /**
     * 设置 [TYPE]
     */
    @JsonProperty("type")
    public void setType(String  type){
        this.type = type ;
        this.typeDirtyFlag = true ;
    }

    /**
     * 获取 [TYPE]脏标记
     */
    @JsonIgnore
    public boolean getTypeDirtyFlag(){
        return typeDirtyFlag ;
    }

    /**
     * 获取 [COMMENTS_ALLOWED]
     */
    @JsonProperty("comments_allowed")
    public String getComments_allowed(){
        return comments_allowed ;
    }

    /**
     * 设置 [COMMENTS_ALLOWED]
     */
    @JsonProperty("comments_allowed")
    public void setComments_allowed(String  comments_allowed){
        this.comments_allowed = comments_allowed ;
        this.comments_allowedDirtyFlag = true ;
    }

    /**
     * 获取 [COMMENTS_ALLOWED]脏标记
     */
    @JsonIgnore
    public boolean getComments_allowedDirtyFlag(){
        return comments_allowedDirtyFlag ;
    }

    /**
     * 获取 [VALIDATION_MAX_DATE]
     */
    @JsonProperty("validation_max_date")
    public Timestamp getValidation_max_date(){
        return validation_max_date ;
    }

    /**
     * 设置 [VALIDATION_MAX_DATE]
     */
    @JsonProperty("validation_max_date")
    public void setValidation_max_date(Timestamp  validation_max_date){
        this.validation_max_date = validation_max_date ;
        this.validation_max_dateDirtyFlag = true ;
    }

    /**
     * 获取 [VALIDATION_MAX_DATE]脏标记
     */
    @JsonIgnore
    public boolean getValidation_max_dateDirtyFlag(){
        return validation_max_dateDirtyFlag ;
    }

    /**
     * 获取 [LABELS_IDS_2]
     */
    @JsonProperty("labels_ids_2")
    public String getLabels_ids_2(){
        return labels_ids_2 ;
    }

    /**
     * 设置 [LABELS_IDS_2]
     */
    @JsonProperty("labels_ids_2")
    public void setLabels_ids_2(String  labels_ids_2){
        this.labels_ids_2 = labels_ids_2 ;
        this.labels_ids_2DirtyFlag = true ;
    }

    /**
     * 获取 [LABELS_IDS_2]脏标记
     */
    @JsonIgnore
    public boolean getLabels_ids_2DirtyFlag(){
        return labels_ids_2DirtyFlag ;
    }

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

    /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return idDirtyFlag ;
    }

    /**
     * 获取 [VALIDATION_MAX_FLOAT_VALUE]
     */
    @JsonProperty("validation_max_float_value")
    public Double getValidation_max_float_value(){
        return validation_max_float_value ;
    }

    /**
     * 设置 [VALIDATION_MAX_FLOAT_VALUE]
     */
    @JsonProperty("validation_max_float_value")
    public void setValidation_max_float_value(Double  validation_max_float_value){
        this.validation_max_float_value = validation_max_float_value ;
        this.validation_max_float_valueDirtyFlag = true ;
    }

    /**
     * 获取 [VALIDATION_MAX_FLOAT_VALUE]脏标记
     */
    @JsonIgnore
    public boolean getValidation_max_float_valueDirtyFlag(){
        return validation_max_float_valueDirtyFlag ;
    }

    /**
     * 获取 [QUESTION]
     */
    @JsonProperty("question")
    public String getQuestion(){
        return question ;
    }

    /**
     * 设置 [QUESTION]
     */
    @JsonProperty("question")
    public void setQuestion(String  question){
        this.question = question ;
        this.questionDirtyFlag = true ;
    }

    /**
     * 获取 [QUESTION]脏标记
     */
    @JsonIgnore
    public boolean getQuestionDirtyFlag(){
        return questionDirtyFlag ;
    }

    /**
     * 获取 [LABELS_IDS]
     */
    @JsonProperty("labels_ids")
    public String getLabels_ids(){
        return labels_ids ;
    }

    /**
     * 设置 [LABELS_IDS]
     */
    @JsonProperty("labels_ids")
    public void setLabels_ids(String  labels_ids){
        this.labels_ids = labels_ids ;
        this.labels_idsDirtyFlag = true ;
    }

    /**
     * 获取 [LABELS_IDS]脏标记
     */
    @JsonIgnore
    public boolean getLabels_idsDirtyFlag(){
        return labels_idsDirtyFlag ;
    }

    /**
     * 获取 [VALIDATION_MIN_FLOAT_VALUE]
     */
    @JsonProperty("validation_min_float_value")
    public Double getValidation_min_float_value(){
        return validation_min_float_value ;
    }

    /**
     * 设置 [VALIDATION_MIN_FLOAT_VALUE]
     */
    @JsonProperty("validation_min_float_value")
    public void setValidation_min_float_value(Double  validation_min_float_value){
        this.validation_min_float_value = validation_min_float_value ;
        this.validation_min_float_valueDirtyFlag = true ;
    }

    /**
     * 获取 [VALIDATION_MIN_FLOAT_VALUE]脏标记
     */
    @JsonIgnore
    public boolean getValidation_min_float_valueDirtyFlag(){
        return validation_min_float_valueDirtyFlag ;
    }

    /**
     * 获取 [SEQUENCE]
     */
    @JsonProperty("sequence")
    public Integer getSequence(){
        return sequence ;
    }

    /**
     * 设置 [SEQUENCE]
     */
    @JsonProperty("sequence")
    public void setSequence(Integer  sequence){
        this.sequence = sequence ;
        this.sequenceDirtyFlag = true ;
    }

    /**
     * 获取 [SEQUENCE]脏标记
     */
    @JsonIgnore
    public boolean getSequenceDirtyFlag(){
        return sequenceDirtyFlag ;
    }

    /**
     * 获取 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return write_date ;
    }

    /**
     * 设置 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return write_dateDirtyFlag ;
    }

    /**
     * 获取 [CONSTR_MANDATORY]
     */
    @JsonProperty("constr_mandatory")
    public String getConstr_mandatory(){
        return constr_mandatory ;
    }

    /**
     * 设置 [CONSTR_MANDATORY]
     */
    @JsonProperty("constr_mandatory")
    public void setConstr_mandatory(String  constr_mandatory){
        this.constr_mandatory = constr_mandatory ;
        this.constr_mandatoryDirtyFlag = true ;
    }

    /**
     * 获取 [CONSTR_MANDATORY]脏标记
     */
    @JsonIgnore
    public boolean getConstr_mandatoryDirtyFlag(){
        return constr_mandatoryDirtyFlag ;
    }

    /**
     * 获取 [USER_INPUT_LINE_IDS]
     */
    @JsonProperty("user_input_line_ids")
    public String getUser_input_line_ids(){
        return user_input_line_ids ;
    }

    /**
     * 设置 [USER_INPUT_LINE_IDS]
     */
    @JsonProperty("user_input_line_ids")
    public void setUser_input_line_ids(String  user_input_line_ids){
        this.user_input_line_ids = user_input_line_ids ;
        this.user_input_line_idsDirtyFlag = true ;
    }

    /**
     * 获取 [USER_INPUT_LINE_IDS]脏标记
     */
    @JsonIgnore
    public boolean getUser_input_line_idsDirtyFlag(){
        return user_input_line_idsDirtyFlag ;
    }

    /**
     * 获取 [COMMENTS_MESSAGE]
     */
    @JsonProperty("comments_message")
    public String getComments_message(){
        return comments_message ;
    }

    /**
     * 设置 [COMMENTS_MESSAGE]
     */
    @JsonProperty("comments_message")
    public void setComments_message(String  comments_message){
        this.comments_message = comments_message ;
        this.comments_messageDirtyFlag = true ;
    }

    /**
     * 获取 [COMMENTS_MESSAGE]脏标记
     */
    @JsonIgnore
    public boolean getComments_messageDirtyFlag(){
        return comments_messageDirtyFlag ;
    }

    /**
     * 获取 [COMMENT_COUNT_AS_ANSWER]
     */
    @JsonProperty("comment_count_as_answer")
    public String getComment_count_as_answer(){
        return comment_count_as_answer ;
    }

    /**
     * 设置 [COMMENT_COUNT_AS_ANSWER]
     */
    @JsonProperty("comment_count_as_answer")
    public void setComment_count_as_answer(String  comment_count_as_answer){
        this.comment_count_as_answer = comment_count_as_answer ;
        this.comment_count_as_answerDirtyFlag = true ;
    }

    /**
     * 获取 [COMMENT_COUNT_AS_ANSWER]脏标记
     */
    @JsonIgnore
    public boolean getComment_count_as_answerDirtyFlag(){
        return comment_count_as_answerDirtyFlag ;
    }

    /**
     * 获取 [VALIDATION_REQUIRED]
     */
    @JsonProperty("validation_required")
    public String getValidation_required(){
        return validation_required ;
    }

    /**
     * 设置 [VALIDATION_REQUIRED]
     */
    @JsonProperty("validation_required")
    public void setValidation_required(String  validation_required){
        this.validation_required = validation_required ;
        this.validation_requiredDirtyFlag = true ;
    }

    /**
     * 获取 [VALIDATION_REQUIRED]脏标记
     */
    @JsonIgnore
    public boolean getValidation_requiredDirtyFlag(){
        return validation_requiredDirtyFlag ;
    }

    /**
     * 获取 [VALIDATION_EMAIL]
     */
    @JsonProperty("validation_email")
    public String getValidation_email(){
        return validation_email ;
    }

    /**
     * 设置 [VALIDATION_EMAIL]
     */
    @JsonProperty("validation_email")
    public void setValidation_email(String  validation_email){
        this.validation_email = validation_email ;
        this.validation_emailDirtyFlag = true ;
    }

    /**
     * 获取 [VALIDATION_EMAIL]脏标记
     */
    @JsonIgnore
    public boolean getValidation_emailDirtyFlag(){
        return validation_emailDirtyFlag ;
    }

    /**
     * 获取 [VALIDATION_MIN_DATE]
     */
    @JsonProperty("validation_min_date")
    public Timestamp getValidation_min_date(){
        return validation_min_date ;
    }

    /**
     * 设置 [VALIDATION_MIN_DATE]
     */
    @JsonProperty("validation_min_date")
    public void setValidation_min_date(Timestamp  validation_min_date){
        this.validation_min_date = validation_min_date ;
        this.validation_min_dateDirtyFlag = true ;
    }

    /**
     * 获取 [VALIDATION_MIN_DATE]脏标记
     */
    @JsonIgnore
    public boolean getValidation_min_dateDirtyFlag(){
        return validation_min_dateDirtyFlag ;
    }

    /**
     * 获取 [CONSTR_ERROR_MSG]
     */
    @JsonProperty("constr_error_msg")
    public String getConstr_error_msg(){
        return constr_error_msg ;
    }

    /**
     * 设置 [CONSTR_ERROR_MSG]
     */
    @JsonProperty("constr_error_msg")
    public void setConstr_error_msg(String  constr_error_msg){
        this.constr_error_msg = constr_error_msg ;
        this.constr_error_msgDirtyFlag = true ;
    }

    /**
     * 获取 [CONSTR_ERROR_MSG]脏标记
     */
    @JsonIgnore
    public boolean getConstr_error_msgDirtyFlag(){
        return constr_error_msgDirtyFlag ;
    }

    /**
     * 获取 [VALIDATION_LENGTH_MAX]
     */
    @JsonProperty("validation_length_max")
    public Integer getValidation_length_max(){
        return validation_length_max ;
    }

    /**
     * 设置 [VALIDATION_LENGTH_MAX]
     */
    @JsonProperty("validation_length_max")
    public void setValidation_length_max(Integer  validation_length_max){
        this.validation_length_max = validation_length_max ;
        this.validation_length_maxDirtyFlag = true ;
    }

    /**
     * 获取 [VALIDATION_LENGTH_MAX]脏标记
     */
    @JsonIgnore
    public boolean getValidation_length_maxDirtyFlag(){
        return validation_length_maxDirtyFlag ;
    }

    /**
     * 获取 [COLUMN_NB]
     */
    @JsonProperty("column_nb")
    public String getColumn_nb(){
        return column_nb ;
    }

    /**
     * 设置 [COLUMN_NB]
     */
    @JsonProperty("column_nb")
    public void setColumn_nb(String  column_nb){
        this.column_nb = column_nb ;
        this.column_nbDirtyFlag = true ;
    }

    /**
     * 获取 [COLUMN_NB]脏标记
     */
    @JsonIgnore
    public boolean getColumn_nbDirtyFlag(){
        return column_nbDirtyFlag ;
    }

    /**
     * 获取 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return display_name ;
    }

    /**
     * 设置 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

    /**
     * 获取 [DISPLAY_NAME]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return display_nameDirtyFlag ;
    }

    /**
     * 获取 [DISPLAY_MODE]
     */
    @JsonProperty("display_mode")
    public String getDisplay_mode(){
        return display_mode ;
    }

    /**
     * 设置 [DISPLAY_MODE]
     */
    @JsonProperty("display_mode")
    public void setDisplay_mode(String  display_mode){
        this.display_mode = display_mode ;
        this.display_modeDirtyFlag = true ;
    }

    /**
     * 获取 [DISPLAY_MODE]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_modeDirtyFlag(){
        return display_modeDirtyFlag ;
    }

    /**
     * 获取 [VALIDATION_LENGTH_MIN]
     */
    @JsonProperty("validation_length_min")
    public Integer getValidation_length_min(){
        return validation_length_min ;
    }

    /**
     * 设置 [VALIDATION_LENGTH_MIN]
     */
    @JsonProperty("validation_length_min")
    public void setValidation_length_min(Integer  validation_length_min){
        this.validation_length_min = validation_length_min ;
        this.validation_length_minDirtyFlag = true ;
    }

    /**
     * 获取 [VALIDATION_LENGTH_MIN]脏标记
     */
    @JsonIgnore
    public boolean getValidation_length_minDirtyFlag(){
        return validation_length_minDirtyFlag ;
    }

    /**
     * 获取 [DESCRIPTION]
     */
    @JsonProperty("description")
    public String getDescription(){
        return description ;
    }

    /**
     * 设置 [DESCRIPTION]
     */
    @JsonProperty("description")
    public void setDescription(String  description){
        this.description = description ;
        this.descriptionDirtyFlag = true ;
    }

    /**
     * 获取 [DESCRIPTION]脏标记
     */
    @JsonIgnore
    public boolean getDescriptionDirtyFlag(){
        return descriptionDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return write_uid_text ;
    }

    /**
     * 设置 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return write_uid_textDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return create_uid_text ;
    }

    /**
     * 设置 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return create_uid_textDirtyFlag ;
    }

    /**
     * 获取 [SURVEY_ID]
     */
    @JsonProperty("survey_id")
    public Integer getSurvey_id(){
        return survey_id ;
    }

    /**
     * 设置 [SURVEY_ID]
     */
    @JsonProperty("survey_id")
    public void setSurvey_id(Integer  survey_id){
        this.survey_id = survey_id ;
        this.survey_idDirtyFlag = true ;
    }

    /**
     * 获取 [SURVEY_ID]脏标记
     */
    @JsonIgnore
    public boolean getSurvey_idDirtyFlag(){
        return survey_idDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return write_uid ;
    }

    /**
     * 设置 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return write_uidDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return create_uid ;
    }

    /**
     * 设置 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return create_uidDirtyFlag ;
    }

    /**
     * 获取 [PAGE_ID]
     */
    @JsonProperty("page_id")
    public Integer getPage_id(){
        return page_id ;
    }

    /**
     * 设置 [PAGE_ID]
     */
    @JsonProperty("page_id")
    public void setPage_id(Integer  page_id){
        this.page_id = page_id ;
        this.page_idDirtyFlag = true ;
    }

    /**
     * 获取 [PAGE_ID]脏标记
     */
    @JsonIgnore
    public boolean getPage_idDirtyFlag(){
        return page_idDirtyFlag ;
    }



    public Survey_question toDO() {
        Survey_question srfdomain = new Survey_question();
        if(getMatrix_subtypeDirtyFlag())
            srfdomain.setMatrix_subtype(matrix_subtype);
        if(getValidation_error_msgDirtyFlag())
            srfdomain.setValidation_error_msg(validation_error_msg);
        if(getCreate_dateDirtyFlag())
            srfdomain.setCreate_date(create_date);
        if(get__last_updateDirtyFlag())
            srfdomain.set__last_update(__last_update);
        if(getTypeDirtyFlag())
            srfdomain.setType(type);
        if(getComments_allowedDirtyFlag())
            srfdomain.setComments_allowed(comments_allowed);
        if(getValidation_max_dateDirtyFlag())
            srfdomain.setValidation_max_date(validation_max_date);
        if(getLabels_ids_2DirtyFlag())
            srfdomain.setLabels_ids_2(labels_ids_2);
        if(getIdDirtyFlag())
            srfdomain.setId(id);
        if(getValidation_max_float_valueDirtyFlag())
            srfdomain.setValidation_max_float_value(validation_max_float_value);
        if(getQuestionDirtyFlag())
            srfdomain.setQuestion(question);
        if(getLabels_idsDirtyFlag())
            srfdomain.setLabels_ids(labels_ids);
        if(getValidation_min_float_valueDirtyFlag())
            srfdomain.setValidation_min_float_value(validation_min_float_value);
        if(getSequenceDirtyFlag())
            srfdomain.setSequence(sequence);
        if(getWrite_dateDirtyFlag())
            srfdomain.setWrite_date(write_date);
        if(getConstr_mandatoryDirtyFlag())
            srfdomain.setConstr_mandatory(constr_mandatory);
        if(getUser_input_line_idsDirtyFlag())
            srfdomain.setUser_input_line_ids(user_input_line_ids);
        if(getComments_messageDirtyFlag())
            srfdomain.setComments_message(comments_message);
        if(getComment_count_as_answerDirtyFlag())
            srfdomain.setComment_count_as_answer(comment_count_as_answer);
        if(getValidation_requiredDirtyFlag())
            srfdomain.setValidation_required(validation_required);
        if(getValidation_emailDirtyFlag())
            srfdomain.setValidation_email(validation_email);
        if(getValidation_min_dateDirtyFlag())
            srfdomain.setValidation_min_date(validation_min_date);
        if(getConstr_error_msgDirtyFlag())
            srfdomain.setConstr_error_msg(constr_error_msg);
        if(getValidation_length_maxDirtyFlag())
            srfdomain.setValidation_length_max(validation_length_max);
        if(getColumn_nbDirtyFlag())
            srfdomain.setColumn_nb(column_nb);
        if(getDisplay_nameDirtyFlag())
            srfdomain.setDisplay_name(display_name);
        if(getDisplay_modeDirtyFlag())
            srfdomain.setDisplay_mode(display_mode);
        if(getValidation_length_minDirtyFlag())
            srfdomain.setValidation_length_min(validation_length_min);
        if(getDescriptionDirtyFlag())
            srfdomain.setDescription(description);
        if(getWrite_uid_textDirtyFlag())
            srfdomain.setWrite_uid_text(write_uid_text);
        if(getCreate_uid_textDirtyFlag())
            srfdomain.setCreate_uid_text(create_uid_text);
        if(getSurvey_idDirtyFlag())
            srfdomain.setSurvey_id(survey_id);
        if(getWrite_uidDirtyFlag())
            srfdomain.setWrite_uid(write_uid);
        if(getCreate_uidDirtyFlag())
            srfdomain.setCreate_uid(create_uid);
        if(getPage_idDirtyFlag())
            srfdomain.setPage_id(page_id);

        return srfdomain;
    }

    public void fromDO(Survey_question srfdomain) {
        if(srfdomain == null )
          return ;
        if(srfdomain.getMatrix_subtypeDirtyFlag())
            this.setMatrix_subtype(srfdomain.getMatrix_subtype());
        if(srfdomain.getValidation_error_msgDirtyFlag())
            this.setValidation_error_msg(srfdomain.getValidation_error_msg());
        if(srfdomain.getCreate_dateDirtyFlag())
            this.setCreate_date(srfdomain.getCreate_date());
        if(srfdomain.get__last_updateDirtyFlag())
            this.set__last_update(srfdomain.get__last_update());
        if(srfdomain.getTypeDirtyFlag())
            this.setType(srfdomain.getType());
        if(srfdomain.getComments_allowedDirtyFlag())
            this.setComments_allowed(srfdomain.getComments_allowed());
        if(srfdomain.getValidation_max_dateDirtyFlag())
            this.setValidation_max_date(srfdomain.getValidation_max_date());
        if(srfdomain.getLabels_ids_2DirtyFlag())
            this.setLabels_ids_2(srfdomain.getLabels_ids_2());
        if(srfdomain.getIdDirtyFlag())
            this.setId(srfdomain.getId());
        if(srfdomain.getValidation_max_float_valueDirtyFlag())
            this.setValidation_max_float_value(srfdomain.getValidation_max_float_value());
        if(srfdomain.getQuestionDirtyFlag())
            this.setQuestion(srfdomain.getQuestion());
        if(srfdomain.getLabels_idsDirtyFlag())
            this.setLabels_ids(srfdomain.getLabels_ids());
        if(srfdomain.getValidation_min_float_valueDirtyFlag())
            this.setValidation_min_float_value(srfdomain.getValidation_min_float_value());
        if(srfdomain.getSequenceDirtyFlag())
            this.setSequence(srfdomain.getSequence());
        if(srfdomain.getWrite_dateDirtyFlag())
            this.setWrite_date(srfdomain.getWrite_date());
        if(srfdomain.getConstr_mandatoryDirtyFlag())
            this.setConstr_mandatory(srfdomain.getConstr_mandatory());
        if(srfdomain.getUser_input_line_idsDirtyFlag())
            this.setUser_input_line_ids(srfdomain.getUser_input_line_ids());
        if(srfdomain.getComments_messageDirtyFlag())
            this.setComments_message(srfdomain.getComments_message());
        if(srfdomain.getComment_count_as_answerDirtyFlag())
            this.setComment_count_as_answer(srfdomain.getComment_count_as_answer());
        if(srfdomain.getValidation_requiredDirtyFlag())
            this.setValidation_required(srfdomain.getValidation_required());
        if(srfdomain.getValidation_emailDirtyFlag())
            this.setValidation_email(srfdomain.getValidation_email());
        if(srfdomain.getValidation_min_dateDirtyFlag())
            this.setValidation_min_date(srfdomain.getValidation_min_date());
        if(srfdomain.getConstr_error_msgDirtyFlag())
            this.setConstr_error_msg(srfdomain.getConstr_error_msg());
        if(srfdomain.getValidation_length_maxDirtyFlag())
            this.setValidation_length_max(srfdomain.getValidation_length_max());
        if(srfdomain.getColumn_nbDirtyFlag())
            this.setColumn_nb(srfdomain.getColumn_nb());
        if(srfdomain.getDisplay_nameDirtyFlag())
            this.setDisplay_name(srfdomain.getDisplay_name());
        if(srfdomain.getDisplay_modeDirtyFlag())
            this.setDisplay_mode(srfdomain.getDisplay_mode());
        if(srfdomain.getValidation_length_minDirtyFlag())
            this.setValidation_length_min(srfdomain.getValidation_length_min());
        if(srfdomain.getDescriptionDirtyFlag())
            this.setDescription(srfdomain.getDescription());
        if(srfdomain.getWrite_uid_textDirtyFlag())
            this.setWrite_uid_text(srfdomain.getWrite_uid_text());
        if(srfdomain.getCreate_uid_textDirtyFlag())
            this.setCreate_uid_text(srfdomain.getCreate_uid_text());
        if(srfdomain.getSurvey_idDirtyFlag())
            this.setSurvey_id(srfdomain.getSurvey_id());
        if(srfdomain.getWrite_uidDirtyFlag())
            this.setWrite_uid(srfdomain.getWrite_uid());
        if(srfdomain.getCreate_uidDirtyFlag())
            this.setCreate_uid(srfdomain.getCreate_uid());
        if(srfdomain.getPage_idDirtyFlag())
            this.setPage_id(srfdomain.getPage_id());

    }

    public List<Survey_questionDTO> fromDOPage(List<Survey_question> poPage)   {
        if(poPage == null)
            return null;
        List<Survey_questionDTO> dtos=new ArrayList<Survey_questionDTO>();
        for(Survey_question domain : poPage) {
            Survey_questionDTO dto = new Survey_questionDTO();
            dto.fromDO(domain);
            dtos.add(dto);
        }
        return dtos;
    }
}

