package cn.ibizlab.odoo.service.odoo_survey.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_survey.dto.Survey_labelDTO;
import cn.ibizlab.odoo.core.odoo_survey.domain.Survey_label;
import cn.ibizlab.odoo.core.odoo_survey.service.ISurvey_labelService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_survey.filter.Survey_labelSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Survey_label" })
@RestController
@RequestMapping("")
public class Survey_labelResource {

    @Autowired
    private ISurvey_labelService survey_labelService;

    public ISurvey_labelService getSurvey_labelService() {
        return this.survey_labelService;
    }

    @ApiOperation(value = "建立数据", tags = {"Survey_label" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_survey/survey_labels")

    public ResponseEntity<Survey_labelDTO> create(@RequestBody Survey_labelDTO survey_labeldto) {
        Survey_labelDTO dto = new Survey_labelDTO();
        Survey_label domain = survey_labeldto.toDO();
		survey_labelService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批更新数据", tags = {"Survey_label" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_survey/survey_labels/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Survey_labelDTO> survey_labeldtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "更新数据", tags = {"Survey_label" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_survey/survey_labels/{survey_label_id}")

    public ResponseEntity<Survey_labelDTO> update(@PathVariable("survey_label_id") Integer survey_label_id, @RequestBody Survey_labelDTO survey_labeldto) {
		Survey_label domain = survey_labeldto.toDO();
        domain.setId(survey_label_id);
		survey_labelService.update(domain);
		Survey_labelDTO dto = new Survey_labelDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批建立数据", tags = {"Survey_label" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_survey/survey_labels/createBatch")
    public ResponseEntity<Boolean> createBatchSurvey_label(@RequestBody List<Survey_labelDTO> survey_labeldtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "获取数据", tags = {"Survey_label" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_survey/survey_labels/{survey_label_id}")
    public ResponseEntity<Survey_labelDTO> get(@PathVariable("survey_label_id") Integer survey_label_id) {
        Survey_labelDTO dto = new Survey_labelDTO();
        Survey_label domain = survey_labelService.get(survey_label_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批删除数据", tags = {"Survey_label" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_survey/survey_labels/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Survey_labelDTO> survey_labeldtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "删除数据", tags = {"Survey_label" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_survey/survey_labels/{survey_label_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("survey_label_id") Integer survey_label_id) {
        Survey_labelDTO survey_labeldto = new Survey_labelDTO();
		Survey_label domain = new Survey_label();
		survey_labeldto.setId(survey_label_id);
		domain.setId(survey_label_id);
        Boolean rst = survey_labelService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

	@ApiOperation(value = "获取默认查询", tags = {"Survey_label" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_survey/survey_labels/fetchdefault")
	public ResponseEntity<Page<Survey_labelDTO>> fetchDefault(Survey_labelSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Survey_labelDTO> list = new ArrayList<Survey_labelDTO>();
        
        Page<Survey_label> domains = survey_labelService.searchDefault(context) ;
        for(Survey_label survey_label : domains.getContent()){
            Survey_labelDTO dto = new Survey_labelDTO();
            dto.fromDO(survey_label);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
