package cn.ibizlab.odoo.service.odoo_survey.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_survey.dto.Survey_questionDTO;
import cn.ibizlab.odoo.core.odoo_survey.domain.Survey_question;
import cn.ibizlab.odoo.core.odoo_survey.service.ISurvey_questionService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_survey.filter.Survey_questionSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Survey_question" })
@RestController
@RequestMapping("")
public class Survey_questionResource {

    @Autowired
    private ISurvey_questionService survey_questionService;

    public ISurvey_questionService getSurvey_questionService() {
        return this.survey_questionService;
    }

    @ApiOperation(value = "建立数据", tags = {"Survey_question" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_survey/survey_questions")

    public ResponseEntity<Survey_questionDTO> create(@RequestBody Survey_questionDTO survey_questiondto) {
        Survey_questionDTO dto = new Survey_questionDTO();
        Survey_question domain = survey_questiondto.toDO();
		survey_questionService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "更新数据", tags = {"Survey_question" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_survey/survey_questions/{survey_question_id}")

    public ResponseEntity<Survey_questionDTO> update(@PathVariable("survey_question_id") Integer survey_question_id, @RequestBody Survey_questionDTO survey_questiondto) {
		Survey_question domain = survey_questiondto.toDO();
        domain.setId(survey_question_id);
		survey_questionService.update(domain);
		Survey_questionDTO dto = new Survey_questionDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批更新数据", tags = {"Survey_question" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_survey/survey_questions/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Survey_questionDTO> survey_questiondtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "批建立数据", tags = {"Survey_question" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_survey/survey_questions/createBatch")
    public ResponseEntity<Boolean> createBatchSurvey_question(@RequestBody List<Survey_questionDTO> survey_questiondtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "删除数据", tags = {"Survey_question" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_survey/survey_questions/{survey_question_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("survey_question_id") Integer survey_question_id) {
        Survey_questionDTO survey_questiondto = new Survey_questionDTO();
		Survey_question domain = new Survey_question();
		survey_questiondto.setId(survey_question_id);
		domain.setId(survey_question_id);
        Boolean rst = survey_questionService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "获取数据", tags = {"Survey_question" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_survey/survey_questions/{survey_question_id}")
    public ResponseEntity<Survey_questionDTO> get(@PathVariable("survey_question_id") Integer survey_question_id) {
        Survey_questionDTO dto = new Survey_questionDTO();
        Survey_question domain = survey_questionService.get(survey_question_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批删除数据", tags = {"Survey_question" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_survey/survey_questions/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Survey_questionDTO> survey_questiondtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Survey_question" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_survey/survey_questions/fetchdefault")
	public ResponseEntity<Page<Survey_questionDTO>> fetchDefault(Survey_questionSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Survey_questionDTO> list = new ArrayList<Survey_questionDTO>();
        
        Page<Survey_question> domains = survey_questionService.searchDefault(context) ;
        for(Survey_question survey_question : domains.getContent()){
            Survey_questionDTO dto = new Survey_questionDTO();
            dto.fromDO(survey_question);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
