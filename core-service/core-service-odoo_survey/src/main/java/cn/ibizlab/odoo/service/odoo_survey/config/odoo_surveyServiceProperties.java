package cn.ibizlab.odoo.service.odoo_survey.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import lombok.Data;

@ConfigurationProperties(prefix = "service.odoo.survey")
@Data
public class odoo_surveyServiceProperties {

	private boolean enabled;

	private boolean auth;


}