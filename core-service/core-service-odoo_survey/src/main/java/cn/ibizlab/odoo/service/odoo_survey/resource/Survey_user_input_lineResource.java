package cn.ibizlab.odoo.service.odoo_survey.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_survey.dto.Survey_user_input_lineDTO;
import cn.ibizlab.odoo.core.odoo_survey.domain.Survey_user_input_line;
import cn.ibizlab.odoo.core.odoo_survey.service.ISurvey_user_input_lineService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_survey.filter.Survey_user_input_lineSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Survey_user_input_line" })
@RestController
@RequestMapping("")
public class Survey_user_input_lineResource {

    @Autowired
    private ISurvey_user_input_lineService survey_user_input_lineService;

    public ISurvey_user_input_lineService getSurvey_user_input_lineService() {
        return this.survey_user_input_lineService;
    }

    @ApiOperation(value = "批建立数据", tags = {"Survey_user_input_line" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_survey/survey_user_input_lines/createBatch")
    public ResponseEntity<Boolean> createBatchSurvey_user_input_line(@RequestBody List<Survey_user_input_lineDTO> survey_user_input_linedtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "更新数据", tags = {"Survey_user_input_line" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_survey/survey_user_input_lines/{survey_user_input_line_id}")

    public ResponseEntity<Survey_user_input_lineDTO> update(@PathVariable("survey_user_input_line_id") Integer survey_user_input_line_id, @RequestBody Survey_user_input_lineDTO survey_user_input_linedto) {
		Survey_user_input_line domain = survey_user_input_linedto.toDO();
        domain.setId(survey_user_input_line_id);
		survey_user_input_lineService.update(domain);
		Survey_user_input_lineDTO dto = new Survey_user_input_lineDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批删除数据", tags = {"Survey_user_input_line" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_survey/survey_user_input_lines/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Survey_user_input_lineDTO> survey_user_input_linedtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "批更新数据", tags = {"Survey_user_input_line" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_survey/survey_user_input_lines/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Survey_user_input_lineDTO> survey_user_input_linedtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "获取数据", tags = {"Survey_user_input_line" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_survey/survey_user_input_lines/{survey_user_input_line_id}")
    public ResponseEntity<Survey_user_input_lineDTO> get(@PathVariable("survey_user_input_line_id") Integer survey_user_input_line_id) {
        Survey_user_input_lineDTO dto = new Survey_user_input_lineDTO();
        Survey_user_input_line domain = survey_user_input_lineService.get(survey_user_input_line_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "建立数据", tags = {"Survey_user_input_line" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_survey/survey_user_input_lines")

    public ResponseEntity<Survey_user_input_lineDTO> create(@RequestBody Survey_user_input_lineDTO survey_user_input_linedto) {
        Survey_user_input_lineDTO dto = new Survey_user_input_lineDTO();
        Survey_user_input_line domain = survey_user_input_linedto.toDO();
		survey_user_input_lineService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "删除数据", tags = {"Survey_user_input_line" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_survey/survey_user_input_lines/{survey_user_input_line_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("survey_user_input_line_id") Integer survey_user_input_line_id) {
        Survey_user_input_lineDTO survey_user_input_linedto = new Survey_user_input_lineDTO();
		Survey_user_input_line domain = new Survey_user_input_line();
		survey_user_input_linedto.setId(survey_user_input_line_id);
		domain.setId(survey_user_input_line_id);
        Boolean rst = survey_user_input_lineService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

	@ApiOperation(value = "获取默认查询", tags = {"Survey_user_input_line" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_survey/survey_user_input_lines/fetchdefault")
	public ResponseEntity<Page<Survey_user_input_lineDTO>> fetchDefault(Survey_user_input_lineSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Survey_user_input_lineDTO> list = new ArrayList<Survey_user_input_lineDTO>();
        
        Page<Survey_user_input_line> domains = survey_user_input_lineService.searchDefault(context) ;
        for(Survey_user_input_line survey_user_input_line : domains.getContent()){
            Survey_user_input_lineDTO dto = new Survey_user_input_lineDTO();
            dto.fromDO(survey_user_input_line);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
