package cn.ibizlab.odoo.service.odoo_survey.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_survey.dto.Survey_pageDTO;
import cn.ibizlab.odoo.core.odoo_survey.domain.Survey_page;
import cn.ibizlab.odoo.core.odoo_survey.service.ISurvey_pageService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_survey.filter.Survey_pageSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Survey_page" })
@RestController
@RequestMapping("")
public class Survey_pageResource {

    @Autowired
    private ISurvey_pageService survey_pageService;

    public ISurvey_pageService getSurvey_pageService() {
        return this.survey_pageService;
    }

    @ApiOperation(value = "更新数据", tags = {"Survey_page" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_survey/survey_pages/{survey_page_id}")

    public ResponseEntity<Survey_pageDTO> update(@PathVariable("survey_page_id") Integer survey_page_id, @RequestBody Survey_pageDTO survey_pagedto) {
		Survey_page domain = survey_pagedto.toDO();
        domain.setId(survey_page_id);
		survey_pageService.update(domain);
		Survey_pageDTO dto = new Survey_pageDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取数据", tags = {"Survey_page" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_survey/survey_pages/{survey_page_id}")
    public ResponseEntity<Survey_pageDTO> get(@PathVariable("survey_page_id") Integer survey_page_id) {
        Survey_pageDTO dto = new Survey_pageDTO();
        Survey_page domain = survey_pageService.get(survey_page_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "删除数据", tags = {"Survey_page" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_survey/survey_pages/{survey_page_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("survey_page_id") Integer survey_page_id) {
        Survey_pageDTO survey_pagedto = new Survey_pageDTO();
		Survey_page domain = new Survey_page();
		survey_pagedto.setId(survey_page_id);
		domain.setId(survey_page_id);
        Boolean rst = survey_pageService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "批删除数据", tags = {"Survey_page" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_survey/survey_pages/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Survey_pageDTO> survey_pagedtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "批建立数据", tags = {"Survey_page" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_survey/survey_pages/createBatch")
    public ResponseEntity<Boolean> createBatchSurvey_page(@RequestBody List<Survey_pageDTO> survey_pagedtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "建立数据", tags = {"Survey_page" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_survey/survey_pages")

    public ResponseEntity<Survey_pageDTO> create(@RequestBody Survey_pageDTO survey_pagedto) {
        Survey_pageDTO dto = new Survey_pageDTO();
        Survey_page domain = survey_pagedto.toDO();
		survey_pageService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批更新数据", tags = {"Survey_page" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_survey/survey_pages/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Survey_pageDTO> survey_pagedtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Survey_page" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_survey/survey_pages/fetchdefault")
	public ResponseEntity<Page<Survey_pageDTO>> fetchDefault(Survey_pageSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Survey_pageDTO> list = new ArrayList<Survey_pageDTO>();
        
        Page<Survey_page> domains = survey_pageService.searchDefault(context) ;
        for(Survey_page survey_page : domains.getContent()){
            Survey_pageDTO dto = new Survey_pageDTO();
            dto.fromDO(survey_page);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
