package cn.ibizlab.odoo.service.odoo_survey.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.core.odoo_survey.valuerule.anno.survey_user_input_line.*;
import cn.ibizlab.odoo.core.odoo_survey.domain.Survey_user_input_line;
import org.springframework.cglib.beans.BeanCopier;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 服务DTO对象[Survey_user_input_lineDTO]
 */
public class Survey_user_input_lineDTO implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [VALUE_DATE]
     *
     */
    @Survey_user_input_lineValue_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    private Timestamp value_date;

    @JsonIgnore
    private boolean value_dateDirtyFlag;

    /**
     * 属性 [VALUE_FREE_TEXT]
     *
     */
    @Survey_user_input_lineValue_free_textDefault(info = "默认规则")
    private String value_free_text;

    @JsonIgnore
    private boolean value_free_textDirtyFlag;

    /**
     * 属性 [ID]
     *
     */
    @Survey_user_input_lineIdDefault(info = "默认规则")
    private Integer id;

    @JsonIgnore
    private boolean idDirtyFlag;

    /**
     * 属性 [DATE_CREATE]
     *
     */
    @Survey_user_input_lineDate_createDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp date_create;

    @JsonIgnore
    private boolean date_createDirtyFlag;

    /**
     * 属性 [SKIPPED]
     *
     */
    @Survey_user_input_lineSkippedDefault(info = "默认规则")
    private String skipped;

    @JsonIgnore
    private boolean skippedDirtyFlag;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @Survey_user_input_line__last_updateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp __last_update;

    @JsonIgnore
    private boolean __last_updateDirtyFlag;

    /**
     * 属性 [QUIZZ_MARK]
     *
     */
    @Survey_user_input_lineQuizz_markDefault(info = "默认规则")
    private Double quizz_mark;

    @JsonIgnore
    private boolean quizz_markDirtyFlag;

    /**
     * 属性 [CREATE_DATE]
     *
     */
    @Survey_user_input_lineCreate_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp create_date;

    @JsonIgnore
    private boolean create_dateDirtyFlag;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @Survey_user_input_lineDisplay_nameDefault(info = "默认规则")
    private String display_name;

    @JsonIgnore
    private boolean display_nameDirtyFlag;

    /**
     * 属性 [ANSWER_TYPE]
     *
     */
    @Survey_user_input_lineAnswer_typeDefault(info = "默认规则")
    private String answer_type;

    @JsonIgnore
    private boolean answer_typeDirtyFlag;

    /**
     * 属性 [VALUE_TEXT]
     *
     */
    @Survey_user_input_lineValue_textDefault(info = "默认规则")
    private String value_text;

    @JsonIgnore
    private boolean value_textDirtyFlag;

    /**
     * 属性 [VALUE_NUMBER]
     *
     */
    @Survey_user_input_lineValue_numberDefault(info = "默认规则")
    private Double value_number;

    @JsonIgnore
    private boolean value_numberDirtyFlag;

    /**
     * 属性 [WRITE_DATE]
     *
     */
    @Survey_user_input_lineWrite_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp write_date;

    @JsonIgnore
    private boolean write_dateDirtyFlag;

    /**
     * 属性 [WRITE_UID_TEXT]
     *
     */
    @Survey_user_input_lineWrite_uid_textDefault(info = "默认规则")
    private String write_uid_text;

    @JsonIgnore
    private boolean write_uid_textDirtyFlag;

    /**
     * 属性 [CREATE_UID_TEXT]
     *
     */
    @Survey_user_input_lineCreate_uid_textDefault(info = "默认规则")
    private String create_uid_text;

    @JsonIgnore
    private boolean create_uid_textDirtyFlag;

    /**
     * 属性 [PAGE_ID]
     *
     */
    @Survey_user_input_linePage_idDefault(info = "默认规则")
    private Integer page_id;

    @JsonIgnore
    private boolean page_idDirtyFlag;

    /**
     * 属性 [USER_INPUT_ID]
     *
     */
    @Survey_user_input_lineUser_input_idDefault(info = "默认规则")
    private Integer user_input_id;

    @JsonIgnore
    private boolean user_input_idDirtyFlag;

    /**
     * 属性 [VALUE_SUGGESTED]
     *
     */
    @Survey_user_input_lineValue_suggestedDefault(info = "默认规则")
    private Integer value_suggested;

    @JsonIgnore
    private boolean value_suggestedDirtyFlag;

    /**
     * 属性 [CREATE_UID]
     *
     */
    @Survey_user_input_lineCreate_uidDefault(info = "默认规则")
    private Integer create_uid;

    @JsonIgnore
    private boolean create_uidDirtyFlag;

    /**
     * 属性 [VALUE_SUGGESTED_ROW]
     *
     */
    @Survey_user_input_lineValue_suggested_rowDefault(info = "默认规则")
    private Integer value_suggested_row;

    @JsonIgnore
    private boolean value_suggested_rowDirtyFlag;

    /**
     * 属性 [WRITE_UID]
     *
     */
    @Survey_user_input_lineWrite_uidDefault(info = "默认规则")
    private Integer write_uid;

    @JsonIgnore
    private boolean write_uidDirtyFlag;

    /**
     * 属性 [QUESTION_ID]
     *
     */
    @Survey_user_input_lineQuestion_idDefault(info = "默认规则")
    private Integer question_id;

    @JsonIgnore
    private boolean question_idDirtyFlag;

    /**
     * 属性 [SURVEY_ID]
     *
     */
    @Survey_user_input_lineSurvey_idDefault(info = "默认规则")
    private Integer survey_id;

    @JsonIgnore
    private boolean survey_idDirtyFlag;


    /**
     * 获取 [VALUE_DATE]
     */
    @JsonProperty("value_date")
    public Timestamp getValue_date(){
        return value_date ;
    }

    /**
     * 设置 [VALUE_DATE]
     */
    @JsonProperty("value_date")
    public void setValue_date(Timestamp  value_date){
        this.value_date = value_date ;
        this.value_dateDirtyFlag = true ;
    }

    /**
     * 获取 [VALUE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getValue_dateDirtyFlag(){
        return value_dateDirtyFlag ;
    }

    /**
     * 获取 [VALUE_FREE_TEXT]
     */
    @JsonProperty("value_free_text")
    public String getValue_free_text(){
        return value_free_text ;
    }

    /**
     * 设置 [VALUE_FREE_TEXT]
     */
    @JsonProperty("value_free_text")
    public void setValue_free_text(String  value_free_text){
        this.value_free_text = value_free_text ;
        this.value_free_textDirtyFlag = true ;
    }

    /**
     * 获取 [VALUE_FREE_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getValue_free_textDirtyFlag(){
        return value_free_textDirtyFlag ;
    }

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

    /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return idDirtyFlag ;
    }

    /**
     * 获取 [DATE_CREATE]
     */
    @JsonProperty("date_create")
    public Timestamp getDate_create(){
        return date_create ;
    }

    /**
     * 设置 [DATE_CREATE]
     */
    @JsonProperty("date_create")
    public void setDate_create(Timestamp  date_create){
        this.date_create = date_create ;
        this.date_createDirtyFlag = true ;
    }

    /**
     * 获取 [DATE_CREATE]脏标记
     */
    @JsonIgnore
    public boolean getDate_createDirtyFlag(){
        return date_createDirtyFlag ;
    }

    /**
     * 获取 [SKIPPED]
     */
    @JsonProperty("skipped")
    public String getSkipped(){
        return skipped ;
    }

    /**
     * 设置 [SKIPPED]
     */
    @JsonProperty("skipped")
    public void setSkipped(String  skipped){
        this.skipped = skipped ;
        this.skippedDirtyFlag = true ;
    }

    /**
     * 获取 [SKIPPED]脏标记
     */
    @JsonIgnore
    public boolean getSkippedDirtyFlag(){
        return skippedDirtyFlag ;
    }

    /**
     * 获取 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return __last_update ;
    }

    /**
     * 设置 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

    /**
     * 获取 [__LAST_UPDATE]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return __last_updateDirtyFlag ;
    }

    /**
     * 获取 [QUIZZ_MARK]
     */
    @JsonProperty("quizz_mark")
    public Double getQuizz_mark(){
        return quizz_mark ;
    }

    /**
     * 设置 [QUIZZ_MARK]
     */
    @JsonProperty("quizz_mark")
    public void setQuizz_mark(Double  quizz_mark){
        this.quizz_mark = quizz_mark ;
        this.quizz_markDirtyFlag = true ;
    }

    /**
     * 获取 [QUIZZ_MARK]脏标记
     */
    @JsonIgnore
    public boolean getQuizz_markDirtyFlag(){
        return quizz_markDirtyFlag ;
    }

    /**
     * 获取 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return create_date ;
    }

    /**
     * 设置 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return create_dateDirtyFlag ;
    }

    /**
     * 获取 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return display_name ;
    }

    /**
     * 设置 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

    /**
     * 获取 [DISPLAY_NAME]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return display_nameDirtyFlag ;
    }

    /**
     * 获取 [ANSWER_TYPE]
     */
    @JsonProperty("answer_type")
    public String getAnswer_type(){
        return answer_type ;
    }

    /**
     * 设置 [ANSWER_TYPE]
     */
    @JsonProperty("answer_type")
    public void setAnswer_type(String  answer_type){
        this.answer_type = answer_type ;
        this.answer_typeDirtyFlag = true ;
    }

    /**
     * 获取 [ANSWER_TYPE]脏标记
     */
    @JsonIgnore
    public boolean getAnswer_typeDirtyFlag(){
        return answer_typeDirtyFlag ;
    }

    /**
     * 获取 [VALUE_TEXT]
     */
    @JsonProperty("value_text")
    public String getValue_text(){
        return value_text ;
    }

    /**
     * 设置 [VALUE_TEXT]
     */
    @JsonProperty("value_text")
    public void setValue_text(String  value_text){
        this.value_text = value_text ;
        this.value_textDirtyFlag = true ;
    }

    /**
     * 获取 [VALUE_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getValue_textDirtyFlag(){
        return value_textDirtyFlag ;
    }

    /**
     * 获取 [VALUE_NUMBER]
     */
    @JsonProperty("value_number")
    public Double getValue_number(){
        return value_number ;
    }

    /**
     * 设置 [VALUE_NUMBER]
     */
    @JsonProperty("value_number")
    public void setValue_number(Double  value_number){
        this.value_number = value_number ;
        this.value_numberDirtyFlag = true ;
    }

    /**
     * 获取 [VALUE_NUMBER]脏标记
     */
    @JsonIgnore
    public boolean getValue_numberDirtyFlag(){
        return value_numberDirtyFlag ;
    }

    /**
     * 获取 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return write_date ;
    }

    /**
     * 设置 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return write_dateDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return write_uid_text ;
    }

    /**
     * 设置 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return write_uid_textDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return create_uid_text ;
    }

    /**
     * 设置 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return create_uid_textDirtyFlag ;
    }

    /**
     * 获取 [PAGE_ID]
     */
    @JsonProperty("page_id")
    public Integer getPage_id(){
        return page_id ;
    }

    /**
     * 设置 [PAGE_ID]
     */
    @JsonProperty("page_id")
    public void setPage_id(Integer  page_id){
        this.page_id = page_id ;
        this.page_idDirtyFlag = true ;
    }

    /**
     * 获取 [PAGE_ID]脏标记
     */
    @JsonIgnore
    public boolean getPage_idDirtyFlag(){
        return page_idDirtyFlag ;
    }

    /**
     * 获取 [USER_INPUT_ID]
     */
    @JsonProperty("user_input_id")
    public Integer getUser_input_id(){
        return user_input_id ;
    }

    /**
     * 设置 [USER_INPUT_ID]
     */
    @JsonProperty("user_input_id")
    public void setUser_input_id(Integer  user_input_id){
        this.user_input_id = user_input_id ;
        this.user_input_idDirtyFlag = true ;
    }

    /**
     * 获取 [USER_INPUT_ID]脏标记
     */
    @JsonIgnore
    public boolean getUser_input_idDirtyFlag(){
        return user_input_idDirtyFlag ;
    }

    /**
     * 获取 [VALUE_SUGGESTED]
     */
    @JsonProperty("value_suggested")
    public Integer getValue_suggested(){
        return value_suggested ;
    }

    /**
     * 设置 [VALUE_SUGGESTED]
     */
    @JsonProperty("value_suggested")
    public void setValue_suggested(Integer  value_suggested){
        this.value_suggested = value_suggested ;
        this.value_suggestedDirtyFlag = true ;
    }

    /**
     * 获取 [VALUE_SUGGESTED]脏标记
     */
    @JsonIgnore
    public boolean getValue_suggestedDirtyFlag(){
        return value_suggestedDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return create_uid ;
    }

    /**
     * 设置 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return create_uidDirtyFlag ;
    }

    /**
     * 获取 [VALUE_SUGGESTED_ROW]
     */
    @JsonProperty("value_suggested_row")
    public Integer getValue_suggested_row(){
        return value_suggested_row ;
    }

    /**
     * 设置 [VALUE_SUGGESTED_ROW]
     */
    @JsonProperty("value_suggested_row")
    public void setValue_suggested_row(Integer  value_suggested_row){
        this.value_suggested_row = value_suggested_row ;
        this.value_suggested_rowDirtyFlag = true ;
    }

    /**
     * 获取 [VALUE_SUGGESTED_ROW]脏标记
     */
    @JsonIgnore
    public boolean getValue_suggested_rowDirtyFlag(){
        return value_suggested_rowDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return write_uid ;
    }

    /**
     * 设置 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return write_uidDirtyFlag ;
    }

    /**
     * 获取 [QUESTION_ID]
     */
    @JsonProperty("question_id")
    public Integer getQuestion_id(){
        return question_id ;
    }

    /**
     * 设置 [QUESTION_ID]
     */
    @JsonProperty("question_id")
    public void setQuestion_id(Integer  question_id){
        this.question_id = question_id ;
        this.question_idDirtyFlag = true ;
    }

    /**
     * 获取 [QUESTION_ID]脏标记
     */
    @JsonIgnore
    public boolean getQuestion_idDirtyFlag(){
        return question_idDirtyFlag ;
    }

    /**
     * 获取 [SURVEY_ID]
     */
    @JsonProperty("survey_id")
    public Integer getSurvey_id(){
        return survey_id ;
    }

    /**
     * 设置 [SURVEY_ID]
     */
    @JsonProperty("survey_id")
    public void setSurvey_id(Integer  survey_id){
        this.survey_id = survey_id ;
        this.survey_idDirtyFlag = true ;
    }

    /**
     * 获取 [SURVEY_ID]脏标记
     */
    @JsonIgnore
    public boolean getSurvey_idDirtyFlag(){
        return survey_idDirtyFlag ;
    }



    public Survey_user_input_line toDO() {
        Survey_user_input_line srfdomain = new Survey_user_input_line();
        if(getValue_dateDirtyFlag())
            srfdomain.setValue_date(value_date);
        if(getValue_free_textDirtyFlag())
            srfdomain.setValue_free_text(value_free_text);
        if(getIdDirtyFlag())
            srfdomain.setId(id);
        if(getDate_createDirtyFlag())
            srfdomain.setDate_create(date_create);
        if(getSkippedDirtyFlag())
            srfdomain.setSkipped(skipped);
        if(get__last_updateDirtyFlag())
            srfdomain.set__last_update(__last_update);
        if(getQuizz_markDirtyFlag())
            srfdomain.setQuizz_mark(quizz_mark);
        if(getCreate_dateDirtyFlag())
            srfdomain.setCreate_date(create_date);
        if(getDisplay_nameDirtyFlag())
            srfdomain.setDisplay_name(display_name);
        if(getAnswer_typeDirtyFlag())
            srfdomain.setAnswer_type(answer_type);
        if(getValue_textDirtyFlag())
            srfdomain.setValue_text(value_text);
        if(getValue_numberDirtyFlag())
            srfdomain.setValue_number(value_number);
        if(getWrite_dateDirtyFlag())
            srfdomain.setWrite_date(write_date);
        if(getWrite_uid_textDirtyFlag())
            srfdomain.setWrite_uid_text(write_uid_text);
        if(getCreate_uid_textDirtyFlag())
            srfdomain.setCreate_uid_text(create_uid_text);
        if(getPage_idDirtyFlag())
            srfdomain.setPage_id(page_id);
        if(getUser_input_idDirtyFlag())
            srfdomain.setUser_input_id(user_input_id);
        if(getValue_suggestedDirtyFlag())
            srfdomain.setValue_suggested(value_suggested);
        if(getCreate_uidDirtyFlag())
            srfdomain.setCreate_uid(create_uid);
        if(getValue_suggested_rowDirtyFlag())
            srfdomain.setValue_suggested_row(value_suggested_row);
        if(getWrite_uidDirtyFlag())
            srfdomain.setWrite_uid(write_uid);
        if(getQuestion_idDirtyFlag())
            srfdomain.setQuestion_id(question_id);
        if(getSurvey_idDirtyFlag())
            srfdomain.setSurvey_id(survey_id);

        return srfdomain;
    }

    public void fromDO(Survey_user_input_line srfdomain) {
        if(srfdomain == null )
          return ;
        if(srfdomain.getValue_dateDirtyFlag())
            this.setValue_date(srfdomain.getValue_date());
        if(srfdomain.getValue_free_textDirtyFlag())
            this.setValue_free_text(srfdomain.getValue_free_text());
        if(srfdomain.getIdDirtyFlag())
            this.setId(srfdomain.getId());
        if(srfdomain.getDate_createDirtyFlag())
            this.setDate_create(srfdomain.getDate_create());
        if(srfdomain.getSkippedDirtyFlag())
            this.setSkipped(srfdomain.getSkipped());
        if(srfdomain.get__last_updateDirtyFlag())
            this.set__last_update(srfdomain.get__last_update());
        if(srfdomain.getQuizz_markDirtyFlag())
            this.setQuizz_mark(srfdomain.getQuizz_mark());
        if(srfdomain.getCreate_dateDirtyFlag())
            this.setCreate_date(srfdomain.getCreate_date());
        if(srfdomain.getDisplay_nameDirtyFlag())
            this.setDisplay_name(srfdomain.getDisplay_name());
        if(srfdomain.getAnswer_typeDirtyFlag())
            this.setAnswer_type(srfdomain.getAnswer_type());
        if(srfdomain.getValue_textDirtyFlag())
            this.setValue_text(srfdomain.getValue_text());
        if(srfdomain.getValue_numberDirtyFlag())
            this.setValue_number(srfdomain.getValue_number());
        if(srfdomain.getWrite_dateDirtyFlag())
            this.setWrite_date(srfdomain.getWrite_date());
        if(srfdomain.getWrite_uid_textDirtyFlag())
            this.setWrite_uid_text(srfdomain.getWrite_uid_text());
        if(srfdomain.getCreate_uid_textDirtyFlag())
            this.setCreate_uid_text(srfdomain.getCreate_uid_text());
        if(srfdomain.getPage_idDirtyFlag())
            this.setPage_id(srfdomain.getPage_id());
        if(srfdomain.getUser_input_idDirtyFlag())
            this.setUser_input_id(srfdomain.getUser_input_id());
        if(srfdomain.getValue_suggestedDirtyFlag())
            this.setValue_suggested(srfdomain.getValue_suggested());
        if(srfdomain.getCreate_uidDirtyFlag())
            this.setCreate_uid(srfdomain.getCreate_uid());
        if(srfdomain.getValue_suggested_rowDirtyFlag())
            this.setValue_suggested_row(srfdomain.getValue_suggested_row());
        if(srfdomain.getWrite_uidDirtyFlag())
            this.setWrite_uid(srfdomain.getWrite_uid());
        if(srfdomain.getQuestion_idDirtyFlag())
            this.setQuestion_id(srfdomain.getQuestion_id());
        if(srfdomain.getSurvey_idDirtyFlag())
            this.setSurvey_id(srfdomain.getSurvey_id());

    }

    public List<Survey_user_input_lineDTO> fromDOPage(List<Survey_user_input_line> poPage)   {
        if(poPage == null)
            return null;
        List<Survey_user_input_lineDTO> dtos=new ArrayList<Survey_user_input_lineDTO>();
        for(Survey_user_input_line domain : poPage) {
            Survey_user_input_lineDTO dto = new Survey_user_input_lineDTO();
            dto.fromDO(domain);
            dtos.add(dto);
        }
        return dtos;
    }
}

