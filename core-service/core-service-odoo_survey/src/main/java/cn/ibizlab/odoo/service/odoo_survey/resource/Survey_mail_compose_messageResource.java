package cn.ibizlab.odoo.service.odoo_survey.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_survey.dto.Survey_mail_compose_messageDTO;
import cn.ibizlab.odoo.core.odoo_survey.domain.Survey_mail_compose_message;
import cn.ibizlab.odoo.core.odoo_survey.service.ISurvey_mail_compose_messageService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_survey.filter.Survey_mail_compose_messageSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Survey_mail_compose_message" })
@RestController
@RequestMapping("")
public class Survey_mail_compose_messageResource {

    @Autowired
    private ISurvey_mail_compose_messageService survey_mail_compose_messageService;

    public ISurvey_mail_compose_messageService getSurvey_mail_compose_messageService() {
        return this.survey_mail_compose_messageService;
    }

    @ApiOperation(value = "建立数据", tags = {"Survey_mail_compose_message" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_survey/survey_mail_compose_messages")

    public ResponseEntity<Survey_mail_compose_messageDTO> create(@RequestBody Survey_mail_compose_messageDTO survey_mail_compose_messagedto) {
        Survey_mail_compose_messageDTO dto = new Survey_mail_compose_messageDTO();
        Survey_mail_compose_message domain = survey_mail_compose_messagedto.toDO();
		survey_mail_compose_messageService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "删除数据", tags = {"Survey_mail_compose_message" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_survey/survey_mail_compose_messages/{survey_mail_compose_message_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("survey_mail_compose_message_id") Integer survey_mail_compose_message_id) {
        Survey_mail_compose_messageDTO survey_mail_compose_messagedto = new Survey_mail_compose_messageDTO();
		Survey_mail_compose_message domain = new Survey_mail_compose_message();
		survey_mail_compose_messagedto.setId(survey_mail_compose_message_id);
		domain.setId(survey_mail_compose_message_id);
        Boolean rst = survey_mail_compose_messageService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "批建立数据", tags = {"Survey_mail_compose_message" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_survey/survey_mail_compose_messages/createBatch")
    public ResponseEntity<Boolean> createBatchSurvey_mail_compose_message(@RequestBody List<Survey_mail_compose_messageDTO> survey_mail_compose_messagedtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "获取数据", tags = {"Survey_mail_compose_message" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_survey/survey_mail_compose_messages/{survey_mail_compose_message_id}")
    public ResponseEntity<Survey_mail_compose_messageDTO> get(@PathVariable("survey_mail_compose_message_id") Integer survey_mail_compose_message_id) {
        Survey_mail_compose_messageDTO dto = new Survey_mail_compose_messageDTO();
        Survey_mail_compose_message domain = survey_mail_compose_messageService.get(survey_mail_compose_message_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "更新数据", tags = {"Survey_mail_compose_message" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_survey/survey_mail_compose_messages/{survey_mail_compose_message_id}")

    public ResponseEntity<Survey_mail_compose_messageDTO> update(@PathVariable("survey_mail_compose_message_id") Integer survey_mail_compose_message_id, @RequestBody Survey_mail_compose_messageDTO survey_mail_compose_messagedto) {
		Survey_mail_compose_message domain = survey_mail_compose_messagedto.toDO();
        domain.setId(survey_mail_compose_message_id);
		survey_mail_compose_messageService.update(domain);
		Survey_mail_compose_messageDTO dto = new Survey_mail_compose_messageDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批更新数据", tags = {"Survey_mail_compose_message" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_survey/survey_mail_compose_messages/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Survey_mail_compose_messageDTO> survey_mail_compose_messagedtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "批删除数据", tags = {"Survey_mail_compose_message" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_survey/survey_mail_compose_messages/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Survey_mail_compose_messageDTO> survey_mail_compose_messagedtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Survey_mail_compose_message" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_survey/survey_mail_compose_messages/fetchdefault")
	public ResponseEntity<Page<Survey_mail_compose_messageDTO>> fetchDefault(Survey_mail_compose_messageSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Survey_mail_compose_messageDTO> list = new ArrayList<Survey_mail_compose_messageDTO>();
        
        Page<Survey_mail_compose_message> domains = survey_mail_compose_messageService.searchDefault(context) ;
        for(Survey_mail_compose_message survey_mail_compose_message : domains.getContent()){
            Survey_mail_compose_messageDTO dto = new Survey_mail_compose_messageDTO();
            dto.fromDO(survey_mail_compose_message);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
