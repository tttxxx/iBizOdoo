package cn.ibizlab.odoo.service.odoo_base_import.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_base_import.dto.Base_import_tests_models_char_statesDTO;
import cn.ibizlab.odoo.core.odoo_base_import.domain.Base_import_tests_models_char_states;
import cn.ibizlab.odoo.core.odoo_base_import.service.IBase_import_tests_models_char_statesService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_base_import.filter.Base_import_tests_models_char_statesSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Base_import_tests_models_char_states" })
@RestController
@RequestMapping("")
public class Base_import_tests_models_char_statesResource {

    @Autowired
    private IBase_import_tests_models_char_statesService base_import_tests_models_char_statesService;

    public IBase_import_tests_models_char_statesService getBase_import_tests_models_char_statesService() {
        return this.base_import_tests_models_char_statesService;
    }

    @ApiOperation(value = "获取数据", tags = {"Base_import_tests_models_char_states" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_base_import/base_import_tests_models_char_states/{base_import_tests_models_char_states_id}")
    public ResponseEntity<Base_import_tests_models_char_statesDTO> get(@PathVariable("base_import_tests_models_char_states_id") Integer base_import_tests_models_char_states_id) {
        Base_import_tests_models_char_statesDTO dto = new Base_import_tests_models_char_statesDTO();
        Base_import_tests_models_char_states domain = base_import_tests_models_char_statesService.get(base_import_tests_models_char_states_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "更新数据", tags = {"Base_import_tests_models_char_states" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_base_import/base_import_tests_models_char_states/{base_import_tests_models_char_states_id}")

    public ResponseEntity<Base_import_tests_models_char_statesDTO> update(@PathVariable("base_import_tests_models_char_states_id") Integer base_import_tests_models_char_states_id, @RequestBody Base_import_tests_models_char_statesDTO base_import_tests_models_char_statesdto) {
		Base_import_tests_models_char_states domain = base_import_tests_models_char_statesdto.toDO();
        domain.setId(base_import_tests_models_char_states_id);
		base_import_tests_models_char_statesService.update(domain);
		Base_import_tests_models_char_statesDTO dto = new Base_import_tests_models_char_statesDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批建立数据", tags = {"Base_import_tests_models_char_states" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_base_import/base_import_tests_models_char_states/createBatch")
    public ResponseEntity<Boolean> createBatchBase_import_tests_models_char_states(@RequestBody List<Base_import_tests_models_char_statesDTO> base_import_tests_models_char_statesdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "建立数据", tags = {"Base_import_tests_models_char_states" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_base_import/base_import_tests_models_char_states")

    public ResponseEntity<Base_import_tests_models_char_statesDTO> create(@RequestBody Base_import_tests_models_char_statesDTO base_import_tests_models_char_statesdto) {
        Base_import_tests_models_char_statesDTO dto = new Base_import_tests_models_char_statesDTO();
        Base_import_tests_models_char_states domain = base_import_tests_models_char_statesdto.toDO();
		base_import_tests_models_char_statesService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "删除数据", tags = {"Base_import_tests_models_char_states" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_base_import/base_import_tests_models_char_states/{base_import_tests_models_char_states_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("base_import_tests_models_char_states_id") Integer base_import_tests_models_char_states_id) {
        Base_import_tests_models_char_statesDTO base_import_tests_models_char_statesdto = new Base_import_tests_models_char_statesDTO();
		Base_import_tests_models_char_states domain = new Base_import_tests_models_char_states();
		base_import_tests_models_char_statesdto.setId(base_import_tests_models_char_states_id);
		domain.setId(base_import_tests_models_char_states_id);
        Boolean rst = base_import_tests_models_char_statesService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "批更新数据", tags = {"Base_import_tests_models_char_states" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_base_import/base_import_tests_models_char_states/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Base_import_tests_models_char_statesDTO> base_import_tests_models_char_statesdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "批删除数据", tags = {"Base_import_tests_models_char_states" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_base_import/base_import_tests_models_char_states/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Base_import_tests_models_char_statesDTO> base_import_tests_models_char_statesdtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Base_import_tests_models_char_states" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_base_import/base_import_tests_models_char_states/fetchdefault")
	public ResponseEntity<Page<Base_import_tests_models_char_statesDTO>> fetchDefault(Base_import_tests_models_char_statesSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Base_import_tests_models_char_statesDTO> list = new ArrayList<Base_import_tests_models_char_statesDTO>();
        
        Page<Base_import_tests_models_char_states> domains = base_import_tests_models_char_statesService.searchDefault(context) ;
        for(Base_import_tests_models_char_states base_import_tests_models_char_states : domains.getContent()){
            Base_import_tests_models_char_statesDTO dto = new Base_import_tests_models_char_statesDTO();
            dto.fromDO(base_import_tests_models_char_states);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
