package cn.ibizlab.odoo.service.odoo_base_import.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_base_import.dto.Base_import_tests_models_char_requiredDTO;
import cn.ibizlab.odoo.core.odoo_base_import.domain.Base_import_tests_models_char_required;
import cn.ibizlab.odoo.core.odoo_base_import.service.IBase_import_tests_models_char_requiredService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_base_import.filter.Base_import_tests_models_char_requiredSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Base_import_tests_models_char_required" })
@RestController
@RequestMapping("")
public class Base_import_tests_models_char_requiredResource {

    @Autowired
    private IBase_import_tests_models_char_requiredService base_import_tests_models_char_requiredService;

    public IBase_import_tests_models_char_requiredService getBase_import_tests_models_char_requiredService() {
        return this.base_import_tests_models_char_requiredService;
    }

    @ApiOperation(value = "建立数据", tags = {"Base_import_tests_models_char_required" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_base_import/base_import_tests_models_char_requireds")

    public ResponseEntity<Base_import_tests_models_char_requiredDTO> create(@RequestBody Base_import_tests_models_char_requiredDTO base_import_tests_models_char_requireddto) {
        Base_import_tests_models_char_requiredDTO dto = new Base_import_tests_models_char_requiredDTO();
        Base_import_tests_models_char_required domain = base_import_tests_models_char_requireddto.toDO();
		base_import_tests_models_char_requiredService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "删除数据", tags = {"Base_import_tests_models_char_required" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_base_import/base_import_tests_models_char_requireds/{base_import_tests_models_char_required_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("base_import_tests_models_char_required_id") Integer base_import_tests_models_char_required_id) {
        Base_import_tests_models_char_requiredDTO base_import_tests_models_char_requireddto = new Base_import_tests_models_char_requiredDTO();
		Base_import_tests_models_char_required domain = new Base_import_tests_models_char_required();
		base_import_tests_models_char_requireddto.setId(base_import_tests_models_char_required_id);
		domain.setId(base_import_tests_models_char_required_id);
        Boolean rst = base_import_tests_models_char_requiredService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "批删除数据", tags = {"Base_import_tests_models_char_required" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_base_import/base_import_tests_models_char_requireds/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Base_import_tests_models_char_requiredDTO> base_import_tests_models_char_requireddtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "批建立数据", tags = {"Base_import_tests_models_char_required" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_base_import/base_import_tests_models_char_requireds/createBatch")
    public ResponseEntity<Boolean> createBatchBase_import_tests_models_char_required(@RequestBody List<Base_import_tests_models_char_requiredDTO> base_import_tests_models_char_requireddtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "批更新数据", tags = {"Base_import_tests_models_char_required" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_base_import/base_import_tests_models_char_requireds/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Base_import_tests_models_char_requiredDTO> base_import_tests_models_char_requireddtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "获取数据", tags = {"Base_import_tests_models_char_required" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_base_import/base_import_tests_models_char_requireds/{base_import_tests_models_char_required_id}")
    public ResponseEntity<Base_import_tests_models_char_requiredDTO> get(@PathVariable("base_import_tests_models_char_required_id") Integer base_import_tests_models_char_required_id) {
        Base_import_tests_models_char_requiredDTO dto = new Base_import_tests_models_char_requiredDTO();
        Base_import_tests_models_char_required domain = base_import_tests_models_char_requiredService.get(base_import_tests_models_char_required_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "更新数据", tags = {"Base_import_tests_models_char_required" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_base_import/base_import_tests_models_char_requireds/{base_import_tests_models_char_required_id}")

    public ResponseEntity<Base_import_tests_models_char_requiredDTO> update(@PathVariable("base_import_tests_models_char_required_id") Integer base_import_tests_models_char_required_id, @RequestBody Base_import_tests_models_char_requiredDTO base_import_tests_models_char_requireddto) {
		Base_import_tests_models_char_required domain = base_import_tests_models_char_requireddto.toDO();
        domain.setId(base_import_tests_models_char_required_id);
		base_import_tests_models_char_requiredService.update(domain);
		Base_import_tests_models_char_requiredDTO dto = new Base_import_tests_models_char_requiredDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Base_import_tests_models_char_required" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_base_import/base_import_tests_models_char_requireds/fetchdefault")
	public ResponseEntity<Page<Base_import_tests_models_char_requiredDTO>> fetchDefault(Base_import_tests_models_char_requiredSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Base_import_tests_models_char_requiredDTO> list = new ArrayList<Base_import_tests_models_char_requiredDTO>();
        
        Page<Base_import_tests_models_char_required> domains = base_import_tests_models_char_requiredService.searchDefault(context) ;
        for(Base_import_tests_models_char_required base_import_tests_models_char_required : domains.getContent()){
            Base_import_tests_models_char_requiredDTO dto = new Base_import_tests_models_char_requiredDTO();
            dto.fromDO(base_import_tests_models_char_required);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
