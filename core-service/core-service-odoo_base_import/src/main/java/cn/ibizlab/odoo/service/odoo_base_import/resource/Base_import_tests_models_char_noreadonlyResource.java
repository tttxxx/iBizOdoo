package cn.ibizlab.odoo.service.odoo_base_import.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_base_import.dto.Base_import_tests_models_char_noreadonlyDTO;
import cn.ibizlab.odoo.core.odoo_base_import.domain.Base_import_tests_models_char_noreadonly;
import cn.ibizlab.odoo.core.odoo_base_import.service.IBase_import_tests_models_char_noreadonlyService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_base_import.filter.Base_import_tests_models_char_noreadonlySearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Base_import_tests_models_char_noreadonly" })
@RestController
@RequestMapping("")
public class Base_import_tests_models_char_noreadonlyResource {

    @Autowired
    private IBase_import_tests_models_char_noreadonlyService base_import_tests_models_char_noreadonlyService;

    public IBase_import_tests_models_char_noreadonlyService getBase_import_tests_models_char_noreadonlyService() {
        return this.base_import_tests_models_char_noreadonlyService;
    }

    @ApiOperation(value = "获取数据", tags = {"Base_import_tests_models_char_noreadonly" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_base_import/base_import_tests_models_char_noreadonlies/{base_import_tests_models_char_noreadonly_id}")
    public ResponseEntity<Base_import_tests_models_char_noreadonlyDTO> get(@PathVariable("base_import_tests_models_char_noreadonly_id") Integer base_import_tests_models_char_noreadonly_id) {
        Base_import_tests_models_char_noreadonlyDTO dto = new Base_import_tests_models_char_noreadonlyDTO();
        Base_import_tests_models_char_noreadonly domain = base_import_tests_models_char_noreadonlyService.get(base_import_tests_models_char_noreadonly_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批建立数据", tags = {"Base_import_tests_models_char_noreadonly" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_base_import/base_import_tests_models_char_noreadonlies/createBatch")
    public ResponseEntity<Boolean> createBatchBase_import_tests_models_char_noreadonly(@RequestBody List<Base_import_tests_models_char_noreadonlyDTO> base_import_tests_models_char_noreadonlydtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "更新数据", tags = {"Base_import_tests_models_char_noreadonly" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_base_import/base_import_tests_models_char_noreadonlies/{base_import_tests_models_char_noreadonly_id}")

    public ResponseEntity<Base_import_tests_models_char_noreadonlyDTO> update(@PathVariable("base_import_tests_models_char_noreadonly_id") Integer base_import_tests_models_char_noreadonly_id, @RequestBody Base_import_tests_models_char_noreadonlyDTO base_import_tests_models_char_noreadonlydto) {
		Base_import_tests_models_char_noreadonly domain = base_import_tests_models_char_noreadonlydto.toDO();
        domain.setId(base_import_tests_models_char_noreadonly_id);
		base_import_tests_models_char_noreadonlyService.update(domain);
		Base_import_tests_models_char_noreadonlyDTO dto = new Base_import_tests_models_char_noreadonlyDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "建立数据", tags = {"Base_import_tests_models_char_noreadonly" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_base_import/base_import_tests_models_char_noreadonlies")

    public ResponseEntity<Base_import_tests_models_char_noreadonlyDTO> create(@RequestBody Base_import_tests_models_char_noreadonlyDTO base_import_tests_models_char_noreadonlydto) {
        Base_import_tests_models_char_noreadonlyDTO dto = new Base_import_tests_models_char_noreadonlyDTO();
        Base_import_tests_models_char_noreadonly domain = base_import_tests_models_char_noreadonlydto.toDO();
		base_import_tests_models_char_noreadonlyService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批删除数据", tags = {"Base_import_tests_models_char_noreadonly" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_base_import/base_import_tests_models_char_noreadonlies/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Base_import_tests_models_char_noreadonlyDTO> base_import_tests_models_char_noreadonlydtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "删除数据", tags = {"Base_import_tests_models_char_noreadonly" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_base_import/base_import_tests_models_char_noreadonlies/{base_import_tests_models_char_noreadonly_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("base_import_tests_models_char_noreadonly_id") Integer base_import_tests_models_char_noreadonly_id) {
        Base_import_tests_models_char_noreadonlyDTO base_import_tests_models_char_noreadonlydto = new Base_import_tests_models_char_noreadonlyDTO();
		Base_import_tests_models_char_noreadonly domain = new Base_import_tests_models_char_noreadonly();
		base_import_tests_models_char_noreadonlydto.setId(base_import_tests_models_char_noreadonly_id);
		domain.setId(base_import_tests_models_char_noreadonly_id);
        Boolean rst = base_import_tests_models_char_noreadonlyService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "批更新数据", tags = {"Base_import_tests_models_char_noreadonly" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_base_import/base_import_tests_models_char_noreadonlies/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Base_import_tests_models_char_noreadonlyDTO> base_import_tests_models_char_noreadonlydtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Base_import_tests_models_char_noreadonly" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_base_import/base_import_tests_models_char_noreadonlies/fetchdefault")
	public ResponseEntity<Page<Base_import_tests_models_char_noreadonlyDTO>> fetchDefault(Base_import_tests_models_char_noreadonlySearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Base_import_tests_models_char_noreadonlyDTO> list = new ArrayList<Base_import_tests_models_char_noreadonlyDTO>();
        
        Page<Base_import_tests_models_char_noreadonly> domains = base_import_tests_models_char_noreadonlyService.searchDefault(context) ;
        for(Base_import_tests_models_char_noreadonly base_import_tests_models_char_noreadonly : domains.getContent()){
            Base_import_tests_models_char_noreadonlyDTO dto = new Base_import_tests_models_char_noreadonlyDTO();
            dto.fromDO(base_import_tests_models_char_noreadonly);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
