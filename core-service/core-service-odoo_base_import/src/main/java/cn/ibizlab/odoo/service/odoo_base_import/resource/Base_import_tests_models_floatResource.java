package cn.ibizlab.odoo.service.odoo_base_import.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_base_import.dto.Base_import_tests_models_floatDTO;
import cn.ibizlab.odoo.core.odoo_base_import.domain.Base_import_tests_models_float;
import cn.ibizlab.odoo.core.odoo_base_import.service.IBase_import_tests_models_floatService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_base_import.filter.Base_import_tests_models_floatSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Base_import_tests_models_float" })
@RestController
@RequestMapping("")
public class Base_import_tests_models_floatResource {

    @Autowired
    private IBase_import_tests_models_floatService base_import_tests_models_floatService;

    public IBase_import_tests_models_floatService getBase_import_tests_models_floatService() {
        return this.base_import_tests_models_floatService;
    }

    @ApiOperation(value = "获取数据", tags = {"Base_import_tests_models_float" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_base_import/base_import_tests_models_floats/{base_import_tests_models_float_id}")
    public ResponseEntity<Base_import_tests_models_floatDTO> get(@PathVariable("base_import_tests_models_float_id") Integer base_import_tests_models_float_id) {
        Base_import_tests_models_floatDTO dto = new Base_import_tests_models_floatDTO();
        Base_import_tests_models_float domain = base_import_tests_models_floatService.get(base_import_tests_models_float_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批建立数据", tags = {"Base_import_tests_models_float" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_base_import/base_import_tests_models_floats/createBatch")
    public ResponseEntity<Boolean> createBatchBase_import_tests_models_float(@RequestBody List<Base_import_tests_models_floatDTO> base_import_tests_models_floatdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "批删除数据", tags = {"Base_import_tests_models_float" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_base_import/base_import_tests_models_floats/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Base_import_tests_models_floatDTO> base_import_tests_models_floatdtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "批更新数据", tags = {"Base_import_tests_models_float" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_base_import/base_import_tests_models_floats/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Base_import_tests_models_floatDTO> base_import_tests_models_floatdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "删除数据", tags = {"Base_import_tests_models_float" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_base_import/base_import_tests_models_floats/{base_import_tests_models_float_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("base_import_tests_models_float_id") Integer base_import_tests_models_float_id) {
        Base_import_tests_models_floatDTO base_import_tests_models_floatdto = new Base_import_tests_models_floatDTO();
		Base_import_tests_models_float domain = new Base_import_tests_models_float();
		base_import_tests_models_floatdto.setId(base_import_tests_models_float_id);
		domain.setId(base_import_tests_models_float_id);
        Boolean rst = base_import_tests_models_floatService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "更新数据", tags = {"Base_import_tests_models_float" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_base_import/base_import_tests_models_floats/{base_import_tests_models_float_id}")

    public ResponseEntity<Base_import_tests_models_floatDTO> update(@PathVariable("base_import_tests_models_float_id") Integer base_import_tests_models_float_id, @RequestBody Base_import_tests_models_floatDTO base_import_tests_models_floatdto) {
		Base_import_tests_models_float domain = base_import_tests_models_floatdto.toDO();
        domain.setId(base_import_tests_models_float_id);
		base_import_tests_models_floatService.update(domain);
		Base_import_tests_models_floatDTO dto = new Base_import_tests_models_floatDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "建立数据", tags = {"Base_import_tests_models_float" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_base_import/base_import_tests_models_floats")

    public ResponseEntity<Base_import_tests_models_floatDTO> create(@RequestBody Base_import_tests_models_floatDTO base_import_tests_models_floatdto) {
        Base_import_tests_models_floatDTO dto = new Base_import_tests_models_floatDTO();
        Base_import_tests_models_float domain = base_import_tests_models_floatdto.toDO();
		base_import_tests_models_floatService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Base_import_tests_models_float" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_base_import/base_import_tests_models_floats/fetchdefault")
	public ResponseEntity<Page<Base_import_tests_models_floatDTO>> fetchDefault(Base_import_tests_models_floatSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Base_import_tests_models_floatDTO> list = new ArrayList<Base_import_tests_models_floatDTO>();
        
        Page<Base_import_tests_models_float> domains = base_import_tests_models_floatService.searchDefault(context) ;
        for(Base_import_tests_models_float base_import_tests_models_float : domains.getContent()){
            Base_import_tests_models_floatDTO dto = new Base_import_tests_models_floatDTO();
            dto.fromDO(base_import_tests_models_float);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
