package cn.ibizlab.odoo.service.odoo_base_import.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_base_import.dto.Base_import_tests_models_m2oDTO;
import cn.ibizlab.odoo.core.odoo_base_import.domain.Base_import_tests_models_m2o;
import cn.ibizlab.odoo.core.odoo_base_import.service.IBase_import_tests_models_m2oService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_base_import.filter.Base_import_tests_models_m2oSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Base_import_tests_models_m2o" })
@RestController
@RequestMapping("")
public class Base_import_tests_models_m2oResource {

    @Autowired
    private IBase_import_tests_models_m2oService base_import_tests_models_m2oService;

    public IBase_import_tests_models_m2oService getBase_import_tests_models_m2oService() {
        return this.base_import_tests_models_m2oService;
    }

    @ApiOperation(value = "建立数据", tags = {"Base_import_tests_models_m2o" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_base_import/base_import_tests_models_m2os")

    public ResponseEntity<Base_import_tests_models_m2oDTO> create(@RequestBody Base_import_tests_models_m2oDTO base_import_tests_models_m2odto) {
        Base_import_tests_models_m2oDTO dto = new Base_import_tests_models_m2oDTO();
        Base_import_tests_models_m2o domain = base_import_tests_models_m2odto.toDO();
		base_import_tests_models_m2oService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批更新数据", tags = {"Base_import_tests_models_m2o" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_base_import/base_import_tests_models_m2os/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Base_import_tests_models_m2oDTO> base_import_tests_models_m2odtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "批删除数据", tags = {"Base_import_tests_models_m2o" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_base_import/base_import_tests_models_m2os/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Base_import_tests_models_m2oDTO> base_import_tests_models_m2odtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "更新数据", tags = {"Base_import_tests_models_m2o" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_base_import/base_import_tests_models_m2os/{base_import_tests_models_m2o_id}")

    public ResponseEntity<Base_import_tests_models_m2oDTO> update(@PathVariable("base_import_tests_models_m2o_id") Integer base_import_tests_models_m2o_id, @RequestBody Base_import_tests_models_m2oDTO base_import_tests_models_m2odto) {
		Base_import_tests_models_m2o domain = base_import_tests_models_m2odto.toDO();
        domain.setId(base_import_tests_models_m2o_id);
		base_import_tests_models_m2oService.update(domain);
		Base_import_tests_models_m2oDTO dto = new Base_import_tests_models_m2oDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "删除数据", tags = {"Base_import_tests_models_m2o" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_base_import/base_import_tests_models_m2os/{base_import_tests_models_m2o_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("base_import_tests_models_m2o_id") Integer base_import_tests_models_m2o_id) {
        Base_import_tests_models_m2oDTO base_import_tests_models_m2odto = new Base_import_tests_models_m2oDTO();
		Base_import_tests_models_m2o domain = new Base_import_tests_models_m2o();
		base_import_tests_models_m2odto.setId(base_import_tests_models_m2o_id);
		domain.setId(base_import_tests_models_m2o_id);
        Boolean rst = base_import_tests_models_m2oService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "获取数据", tags = {"Base_import_tests_models_m2o" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_base_import/base_import_tests_models_m2os/{base_import_tests_models_m2o_id}")
    public ResponseEntity<Base_import_tests_models_m2oDTO> get(@PathVariable("base_import_tests_models_m2o_id") Integer base_import_tests_models_m2o_id) {
        Base_import_tests_models_m2oDTO dto = new Base_import_tests_models_m2oDTO();
        Base_import_tests_models_m2o domain = base_import_tests_models_m2oService.get(base_import_tests_models_m2o_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批建立数据", tags = {"Base_import_tests_models_m2o" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_base_import/base_import_tests_models_m2os/createBatch")
    public ResponseEntity<Boolean> createBatchBase_import_tests_models_m2o(@RequestBody List<Base_import_tests_models_m2oDTO> base_import_tests_models_m2odtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Base_import_tests_models_m2o" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_base_import/base_import_tests_models_m2os/fetchdefault")
	public ResponseEntity<Page<Base_import_tests_models_m2oDTO>> fetchDefault(Base_import_tests_models_m2oSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Base_import_tests_models_m2oDTO> list = new ArrayList<Base_import_tests_models_m2oDTO>();
        
        Page<Base_import_tests_models_m2o> domains = base_import_tests_models_m2oService.searchDefault(context) ;
        for(Base_import_tests_models_m2o base_import_tests_models_m2o : domains.getContent()){
            Base_import_tests_models_m2oDTO dto = new Base_import_tests_models_m2oDTO();
            dto.fromDO(base_import_tests_models_m2o);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
