package cn.ibizlab.odoo.service.odoo_base_import.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_base_import.dto.Base_import_tests_models_charDTO;
import cn.ibizlab.odoo.core.odoo_base_import.domain.Base_import_tests_models_char;
import cn.ibizlab.odoo.core.odoo_base_import.service.IBase_import_tests_models_charService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_base_import.filter.Base_import_tests_models_charSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Base_import_tests_models_char" })
@RestController
@RequestMapping("")
public class Base_import_tests_models_charResource {

    @Autowired
    private IBase_import_tests_models_charService base_import_tests_models_charService;

    public IBase_import_tests_models_charService getBase_import_tests_models_charService() {
        return this.base_import_tests_models_charService;
    }

    @ApiOperation(value = "批建立数据", tags = {"Base_import_tests_models_char" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_base_import/base_import_tests_models_chars/createBatch")
    public ResponseEntity<Boolean> createBatchBase_import_tests_models_char(@RequestBody List<Base_import_tests_models_charDTO> base_import_tests_models_chardtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "更新数据", tags = {"Base_import_tests_models_char" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_base_import/base_import_tests_models_chars/{base_import_tests_models_char_id}")

    public ResponseEntity<Base_import_tests_models_charDTO> update(@PathVariable("base_import_tests_models_char_id") Integer base_import_tests_models_char_id, @RequestBody Base_import_tests_models_charDTO base_import_tests_models_chardto) {
		Base_import_tests_models_char domain = base_import_tests_models_chardto.toDO();
        domain.setId(base_import_tests_models_char_id);
		base_import_tests_models_charService.update(domain);
		Base_import_tests_models_charDTO dto = new Base_import_tests_models_charDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "删除数据", tags = {"Base_import_tests_models_char" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_base_import/base_import_tests_models_chars/{base_import_tests_models_char_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("base_import_tests_models_char_id") Integer base_import_tests_models_char_id) {
        Base_import_tests_models_charDTO base_import_tests_models_chardto = new Base_import_tests_models_charDTO();
		Base_import_tests_models_char domain = new Base_import_tests_models_char();
		base_import_tests_models_chardto.setId(base_import_tests_models_char_id);
		domain.setId(base_import_tests_models_char_id);
        Boolean rst = base_import_tests_models_charService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "批更新数据", tags = {"Base_import_tests_models_char" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_base_import/base_import_tests_models_chars/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Base_import_tests_models_charDTO> base_import_tests_models_chardtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "批删除数据", tags = {"Base_import_tests_models_char" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_base_import/base_import_tests_models_chars/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Base_import_tests_models_charDTO> base_import_tests_models_chardtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "建立数据", tags = {"Base_import_tests_models_char" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_base_import/base_import_tests_models_chars")

    public ResponseEntity<Base_import_tests_models_charDTO> create(@RequestBody Base_import_tests_models_charDTO base_import_tests_models_chardto) {
        Base_import_tests_models_charDTO dto = new Base_import_tests_models_charDTO();
        Base_import_tests_models_char domain = base_import_tests_models_chardto.toDO();
		base_import_tests_models_charService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取数据", tags = {"Base_import_tests_models_char" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_base_import/base_import_tests_models_chars/{base_import_tests_models_char_id}")
    public ResponseEntity<Base_import_tests_models_charDTO> get(@PathVariable("base_import_tests_models_char_id") Integer base_import_tests_models_char_id) {
        Base_import_tests_models_charDTO dto = new Base_import_tests_models_charDTO();
        Base_import_tests_models_char domain = base_import_tests_models_charService.get(base_import_tests_models_char_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Base_import_tests_models_char" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_base_import/base_import_tests_models_chars/fetchdefault")
	public ResponseEntity<Page<Base_import_tests_models_charDTO>> fetchDefault(Base_import_tests_models_charSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Base_import_tests_models_charDTO> list = new ArrayList<Base_import_tests_models_charDTO>();
        
        Page<Base_import_tests_models_char> domains = base_import_tests_models_charService.searchDefault(context) ;
        for(Base_import_tests_models_char base_import_tests_models_char : domains.getContent()){
            Base_import_tests_models_charDTO dto = new Base_import_tests_models_charDTO();
            dto.fromDO(base_import_tests_models_char);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
