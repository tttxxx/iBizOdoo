package cn.ibizlab.odoo.service.odoo_base_import.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_base_import.dto.Base_import_mappingDTO;
import cn.ibizlab.odoo.core.odoo_base_import.domain.Base_import_mapping;
import cn.ibizlab.odoo.core.odoo_base_import.service.IBase_import_mappingService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_base_import.filter.Base_import_mappingSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Base_import_mapping" })
@RestController
@RequestMapping("")
public class Base_import_mappingResource {

    @Autowired
    private IBase_import_mappingService base_import_mappingService;

    public IBase_import_mappingService getBase_import_mappingService() {
        return this.base_import_mappingService;
    }

    @ApiOperation(value = "批删除数据", tags = {"Base_import_mapping" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_base_import/base_import_mappings/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Base_import_mappingDTO> base_import_mappingdtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "更新数据", tags = {"Base_import_mapping" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_base_import/base_import_mappings/{base_import_mapping_id}")

    public ResponseEntity<Base_import_mappingDTO> update(@PathVariable("base_import_mapping_id") Integer base_import_mapping_id, @RequestBody Base_import_mappingDTO base_import_mappingdto) {
		Base_import_mapping domain = base_import_mappingdto.toDO();
        domain.setId(base_import_mapping_id);
		base_import_mappingService.update(domain);
		Base_import_mappingDTO dto = new Base_import_mappingDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批更新数据", tags = {"Base_import_mapping" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_base_import/base_import_mappings/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Base_import_mappingDTO> base_import_mappingdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "获取数据", tags = {"Base_import_mapping" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_base_import/base_import_mappings/{base_import_mapping_id}")
    public ResponseEntity<Base_import_mappingDTO> get(@PathVariable("base_import_mapping_id") Integer base_import_mapping_id) {
        Base_import_mappingDTO dto = new Base_import_mappingDTO();
        Base_import_mapping domain = base_import_mappingService.get(base_import_mapping_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "建立数据", tags = {"Base_import_mapping" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_base_import/base_import_mappings")

    public ResponseEntity<Base_import_mappingDTO> create(@RequestBody Base_import_mappingDTO base_import_mappingdto) {
        Base_import_mappingDTO dto = new Base_import_mappingDTO();
        Base_import_mapping domain = base_import_mappingdto.toDO();
		base_import_mappingService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "删除数据", tags = {"Base_import_mapping" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_base_import/base_import_mappings/{base_import_mapping_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("base_import_mapping_id") Integer base_import_mapping_id) {
        Base_import_mappingDTO base_import_mappingdto = new Base_import_mappingDTO();
		Base_import_mapping domain = new Base_import_mapping();
		base_import_mappingdto.setId(base_import_mapping_id);
		domain.setId(base_import_mapping_id);
        Boolean rst = base_import_mappingService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "批建立数据", tags = {"Base_import_mapping" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_base_import/base_import_mappings/createBatch")
    public ResponseEntity<Boolean> createBatchBase_import_mapping(@RequestBody List<Base_import_mappingDTO> base_import_mappingdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Base_import_mapping" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_base_import/base_import_mappings/fetchdefault")
	public ResponseEntity<Page<Base_import_mappingDTO>> fetchDefault(Base_import_mappingSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Base_import_mappingDTO> list = new ArrayList<Base_import_mappingDTO>();
        
        Page<Base_import_mapping> domains = base_import_mappingService.searchDefault(context) ;
        for(Base_import_mapping base_import_mapping : domains.getContent()){
            Base_import_mappingDTO dto = new Base_import_mappingDTO();
            dto.fromDO(base_import_mapping);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
