package cn.ibizlab.odoo.service.odoo_base_import.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_base_import.dto.Base_import_tests_models_char_stillreadonlyDTO;
import cn.ibizlab.odoo.core.odoo_base_import.domain.Base_import_tests_models_char_stillreadonly;
import cn.ibizlab.odoo.core.odoo_base_import.service.IBase_import_tests_models_char_stillreadonlyService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_base_import.filter.Base_import_tests_models_char_stillreadonlySearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Base_import_tests_models_char_stillreadonly" })
@RestController
@RequestMapping("")
public class Base_import_tests_models_char_stillreadonlyResource {

    @Autowired
    private IBase_import_tests_models_char_stillreadonlyService base_import_tests_models_char_stillreadonlyService;

    public IBase_import_tests_models_char_stillreadonlyService getBase_import_tests_models_char_stillreadonlyService() {
        return this.base_import_tests_models_char_stillreadonlyService;
    }

    @ApiOperation(value = "批建立数据", tags = {"Base_import_tests_models_char_stillreadonly" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_base_import/base_import_tests_models_char_stillreadonlies/createBatch")
    public ResponseEntity<Boolean> createBatchBase_import_tests_models_char_stillreadonly(@RequestBody List<Base_import_tests_models_char_stillreadonlyDTO> base_import_tests_models_char_stillreadonlydtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "批更新数据", tags = {"Base_import_tests_models_char_stillreadonly" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_base_import/base_import_tests_models_char_stillreadonlies/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Base_import_tests_models_char_stillreadonlyDTO> base_import_tests_models_char_stillreadonlydtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "批删除数据", tags = {"Base_import_tests_models_char_stillreadonly" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_base_import/base_import_tests_models_char_stillreadonlies/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Base_import_tests_models_char_stillreadonlyDTO> base_import_tests_models_char_stillreadonlydtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "删除数据", tags = {"Base_import_tests_models_char_stillreadonly" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_base_import/base_import_tests_models_char_stillreadonlies/{base_import_tests_models_char_stillreadonly_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("base_import_tests_models_char_stillreadonly_id") Integer base_import_tests_models_char_stillreadonly_id) {
        Base_import_tests_models_char_stillreadonlyDTO base_import_tests_models_char_stillreadonlydto = new Base_import_tests_models_char_stillreadonlyDTO();
		Base_import_tests_models_char_stillreadonly domain = new Base_import_tests_models_char_stillreadonly();
		base_import_tests_models_char_stillreadonlydto.setId(base_import_tests_models_char_stillreadonly_id);
		domain.setId(base_import_tests_models_char_stillreadonly_id);
        Boolean rst = base_import_tests_models_char_stillreadonlyService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "建立数据", tags = {"Base_import_tests_models_char_stillreadonly" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_base_import/base_import_tests_models_char_stillreadonlies")

    public ResponseEntity<Base_import_tests_models_char_stillreadonlyDTO> create(@RequestBody Base_import_tests_models_char_stillreadonlyDTO base_import_tests_models_char_stillreadonlydto) {
        Base_import_tests_models_char_stillreadonlyDTO dto = new Base_import_tests_models_char_stillreadonlyDTO();
        Base_import_tests_models_char_stillreadonly domain = base_import_tests_models_char_stillreadonlydto.toDO();
		base_import_tests_models_char_stillreadonlyService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "更新数据", tags = {"Base_import_tests_models_char_stillreadonly" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_base_import/base_import_tests_models_char_stillreadonlies/{base_import_tests_models_char_stillreadonly_id}")

    public ResponseEntity<Base_import_tests_models_char_stillreadonlyDTO> update(@PathVariable("base_import_tests_models_char_stillreadonly_id") Integer base_import_tests_models_char_stillreadonly_id, @RequestBody Base_import_tests_models_char_stillreadonlyDTO base_import_tests_models_char_stillreadonlydto) {
		Base_import_tests_models_char_stillreadonly domain = base_import_tests_models_char_stillreadonlydto.toDO();
        domain.setId(base_import_tests_models_char_stillreadonly_id);
		base_import_tests_models_char_stillreadonlyService.update(domain);
		Base_import_tests_models_char_stillreadonlyDTO dto = new Base_import_tests_models_char_stillreadonlyDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取数据", tags = {"Base_import_tests_models_char_stillreadonly" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_base_import/base_import_tests_models_char_stillreadonlies/{base_import_tests_models_char_stillreadonly_id}")
    public ResponseEntity<Base_import_tests_models_char_stillreadonlyDTO> get(@PathVariable("base_import_tests_models_char_stillreadonly_id") Integer base_import_tests_models_char_stillreadonly_id) {
        Base_import_tests_models_char_stillreadonlyDTO dto = new Base_import_tests_models_char_stillreadonlyDTO();
        Base_import_tests_models_char_stillreadonly domain = base_import_tests_models_char_stillreadonlyService.get(base_import_tests_models_char_stillreadonly_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Base_import_tests_models_char_stillreadonly" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_base_import/base_import_tests_models_char_stillreadonlies/fetchdefault")
	public ResponseEntity<Page<Base_import_tests_models_char_stillreadonlyDTO>> fetchDefault(Base_import_tests_models_char_stillreadonlySearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Base_import_tests_models_char_stillreadonlyDTO> list = new ArrayList<Base_import_tests_models_char_stillreadonlyDTO>();
        
        Page<Base_import_tests_models_char_stillreadonly> domains = base_import_tests_models_char_stillreadonlyService.searchDefault(context) ;
        for(Base_import_tests_models_char_stillreadonly base_import_tests_models_char_stillreadonly : domains.getContent()){
            Base_import_tests_models_char_stillreadonlyDTO dto = new Base_import_tests_models_char_stillreadonlyDTO();
            dto.fromDO(base_import_tests_models_char_stillreadonly);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
