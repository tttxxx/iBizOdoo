package cn.ibizlab.odoo.service.odoo_base_import.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_base_import.dto.Base_import_tests_models_o2mDTO;
import cn.ibizlab.odoo.core.odoo_base_import.domain.Base_import_tests_models_o2m;
import cn.ibizlab.odoo.core.odoo_base_import.service.IBase_import_tests_models_o2mService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_base_import.filter.Base_import_tests_models_o2mSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Base_import_tests_models_o2m" })
@RestController
@RequestMapping("")
public class Base_import_tests_models_o2mResource {

    @Autowired
    private IBase_import_tests_models_o2mService base_import_tests_models_o2mService;

    public IBase_import_tests_models_o2mService getBase_import_tests_models_o2mService() {
        return this.base_import_tests_models_o2mService;
    }

    @ApiOperation(value = "更新数据", tags = {"Base_import_tests_models_o2m" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_base_import/base_import_tests_models_o2ms/{base_import_tests_models_o2m_id}")

    public ResponseEntity<Base_import_tests_models_o2mDTO> update(@PathVariable("base_import_tests_models_o2m_id") Integer base_import_tests_models_o2m_id, @RequestBody Base_import_tests_models_o2mDTO base_import_tests_models_o2mdto) {
		Base_import_tests_models_o2m domain = base_import_tests_models_o2mdto.toDO();
        domain.setId(base_import_tests_models_o2m_id);
		base_import_tests_models_o2mService.update(domain);
		Base_import_tests_models_o2mDTO dto = new Base_import_tests_models_o2mDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批更新数据", tags = {"Base_import_tests_models_o2m" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_base_import/base_import_tests_models_o2ms/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Base_import_tests_models_o2mDTO> base_import_tests_models_o2mdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "建立数据", tags = {"Base_import_tests_models_o2m" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_base_import/base_import_tests_models_o2ms")

    public ResponseEntity<Base_import_tests_models_o2mDTO> create(@RequestBody Base_import_tests_models_o2mDTO base_import_tests_models_o2mdto) {
        Base_import_tests_models_o2mDTO dto = new Base_import_tests_models_o2mDTO();
        Base_import_tests_models_o2m domain = base_import_tests_models_o2mdto.toDO();
		base_import_tests_models_o2mService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批删除数据", tags = {"Base_import_tests_models_o2m" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_base_import/base_import_tests_models_o2ms/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Base_import_tests_models_o2mDTO> base_import_tests_models_o2mdtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "获取数据", tags = {"Base_import_tests_models_o2m" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_base_import/base_import_tests_models_o2ms/{base_import_tests_models_o2m_id}")
    public ResponseEntity<Base_import_tests_models_o2mDTO> get(@PathVariable("base_import_tests_models_o2m_id") Integer base_import_tests_models_o2m_id) {
        Base_import_tests_models_o2mDTO dto = new Base_import_tests_models_o2mDTO();
        Base_import_tests_models_o2m domain = base_import_tests_models_o2mService.get(base_import_tests_models_o2m_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "删除数据", tags = {"Base_import_tests_models_o2m" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_base_import/base_import_tests_models_o2ms/{base_import_tests_models_o2m_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("base_import_tests_models_o2m_id") Integer base_import_tests_models_o2m_id) {
        Base_import_tests_models_o2mDTO base_import_tests_models_o2mdto = new Base_import_tests_models_o2mDTO();
		Base_import_tests_models_o2m domain = new Base_import_tests_models_o2m();
		base_import_tests_models_o2mdto.setId(base_import_tests_models_o2m_id);
		domain.setId(base_import_tests_models_o2m_id);
        Boolean rst = base_import_tests_models_o2mService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "批建立数据", tags = {"Base_import_tests_models_o2m" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_base_import/base_import_tests_models_o2ms/createBatch")
    public ResponseEntity<Boolean> createBatchBase_import_tests_models_o2m(@RequestBody List<Base_import_tests_models_o2mDTO> base_import_tests_models_o2mdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Base_import_tests_models_o2m" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_base_import/base_import_tests_models_o2ms/fetchdefault")
	public ResponseEntity<Page<Base_import_tests_models_o2mDTO>> fetchDefault(Base_import_tests_models_o2mSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Base_import_tests_models_o2mDTO> list = new ArrayList<Base_import_tests_models_o2mDTO>();
        
        Page<Base_import_tests_models_o2m> domains = base_import_tests_models_o2mService.searchDefault(context) ;
        for(Base_import_tests_models_o2m base_import_tests_models_o2m : domains.getContent()){
            Base_import_tests_models_o2mDTO dto = new Base_import_tests_models_o2mDTO();
            dto.fromDO(base_import_tests_models_o2m);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
