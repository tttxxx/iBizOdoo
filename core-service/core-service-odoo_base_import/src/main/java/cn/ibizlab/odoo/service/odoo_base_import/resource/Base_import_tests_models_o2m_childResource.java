package cn.ibizlab.odoo.service.odoo_base_import.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_base_import.dto.Base_import_tests_models_o2m_childDTO;
import cn.ibizlab.odoo.core.odoo_base_import.domain.Base_import_tests_models_o2m_child;
import cn.ibizlab.odoo.core.odoo_base_import.service.IBase_import_tests_models_o2m_childService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_base_import.filter.Base_import_tests_models_o2m_childSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Base_import_tests_models_o2m_child" })
@RestController
@RequestMapping("")
public class Base_import_tests_models_o2m_childResource {

    @Autowired
    private IBase_import_tests_models_o2m_childService base_import_tests_models_o2m_childService;

    public IBase_import_tests_models_o2m_childService getBase_import_tests_models_o2m_childService() {
        return this.base_import_tests_models_o2m_childService;
    }

    @ApiOperation(value = "删除数据", tags = {"Base_import_tests_models_o2m_child" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_base_import/base_import_tests_models_o2m_children/{base_import_tests_models_o2m_child_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("base_import_tests_models_o2m_child_id") Integer base_import_tests_models_o2m_child_id) {
        Base_import_tests_models_o2m_childDTO base_import_tests_models_o2m_childdto = new Base_import_tests_models_o2m_childDTO();
		Base_import_tests_models_o2m_child domain = new Base_import_tests_models_o2m_child();
		base_import_tests_models_o2m_childdto.setId(base_import_tests_models_o2m_child_id);
		domain.setId(base_import_tests_models_o2m_child_id);
        Boolean rst = base_import_tests_models_o2m_childService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "建立数据", tags = {"Base_import_tests_models_o2m_child" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_base_import/base_import_tests_models_o2m_children")

    public ResponseEntity<Base_import_tests_models_o2m_childDTO> create(@RequestBody Base_import_tests_models_o2m_childDTO base_import_tests_models_o2m_childdto) {
        Base_import_tests_models_o2m_childDTO dto = new Base_import_tests_models_o2m_childDTO();
        Base_import_tests_models_o2m_child domain = base_import_tests_models_o2m_childdto.toDO();
		base_import_tests_models_o2m_childService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批更新数据", tags = {"Base_import_tests_models_o2m_child" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_base_import/base_import_tests_models_o2m_children/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Base_import_tests_models_o2m_childDTO> base_import_tests_models_o2m_childdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "批建立数据", tags = {"Base_import_tests_models_o2m_child" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_base_import/base_import_tests_models_o2m_children/createBatch")
    public ResponseEntity<Boolean> createBatchBase_import_tests_models_o2m_child(@RequestBody List<Base_import_tests_models_o2m_childDTO> base_import_tests_models_o2m_childdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "获取数据", tags = {"Base_import_tests_models_o2m_child" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_base_import/base_import_tests_models_o2m_children/{base_import_tests_models_o2m_child_id}")
    public ResponseEntity<Base_import_tests_models_o2m_childDTO> get(@PathVariable("base_import_tests_models_o2m_child_id") Integer base_import_tests_models_o2m_child_id) {
        Base_import_tests_models_o2m_childDTO dto = new Base_import_tests_models_o2m_childDTO();
        Base_import_tests_models_o2m_child domain = base_import_tests_models_o2m_childService.get(base_import_tests_models_o2m_child_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "更新数据", tags = {"Base_import_tests_models_o2m_child" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_base_import/base_import_tests_models_o2m_children/{base_import_tests_models_o2m_child_id}")

    public ResponseEntity<Base_import_tests_models_o2m_childDTO> update(@PathVariable("base_import_tests_models_o2m_child_id") Integer base_import_tests_models_o2m_child_id, @RequestBody Base_import_tests_models_o2m_childDTO base_import_tests_models_o2m_childdto) {
		Base_import_tests_models_o2m_child domain = base_import_tests_models_o2m_childdto.toDO();
        domain.setId(base_import_tests_models_o2m_child_id);
		base_import_tests_models_o2m_childService.update(domain);
		Base_import_tests_models_o2m_childDTO dto = new Base_import_tests_models_o2m_childDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批删除数据", tags = {"Base_import_tests_models_o2m_child" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_base_import/base_import_tests_models_o2m_children/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Base_import_tests_models_o2m_childDTO> base_import_tests_models_o2m_childdtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Base_import_tests_models_o2m_child" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_base_import/base_import_tests_models_o2m_children/fetchdefault")
	public ResponseEntity<Page<Base_import_tests_models_o2m_childDTO>> fetchDefault(Base_import_tests_models_o2m_childSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Base_import_tests_models_o2m_childDTO> list = new ArrayList<Base_import_tests_models_o2m_childDTO>();
        
        Page<Base_import_tests_models_o2m_child> domains = base_import_tests_models_o2m_childService.searchDefault(context) ;
        for(Base_import_tests_models_o2m_child base_import_tests_models_o2m_child : domains.getContent()){
            Base_import_tests_models_o2m_childDTO dto = new Base_import_tests_models_o2m_childDTO();
            dto.fromDO(base_import_tests_models_o2m_child);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
