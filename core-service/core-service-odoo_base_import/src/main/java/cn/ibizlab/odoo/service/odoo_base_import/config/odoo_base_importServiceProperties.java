package cn.ibizlab.odoo.service.odoo_base_import.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import lombok.Data;

@ConfigurationProperties(prefix = "service.odoo.base.import")
@Data
public class odoo_base_importServiceProperties {

	private boolean enabled;

	private boolean auth;


}