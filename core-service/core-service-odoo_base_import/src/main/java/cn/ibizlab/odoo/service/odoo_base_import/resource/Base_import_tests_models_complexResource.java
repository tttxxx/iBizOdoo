package cn.ibizlab.odoo.service.odoo_base_import.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_base_import.dto.Base_import_tests_models_complexDTO;
import cn.ibizlab.odoo.core.odoo_base_import.domain.Base_import_tests_models_complex;
import cn.ibizlab.odoo.core.odoo_base_import.service.IBase_import_tests_models_complexService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_base_import.filter.Base_import_tests_models_complexSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Base_import_tests_models_complex" })
@RestController
@RequestMapping("")
public class Base_import_tests_models_complexResource {

    @Autowired
    private IBase_import_tests_models_complexService base_import_tests_models_complexService;

    public IBase_import_tests_models_complexService getBase_import_tests_models_complexService() {
        return this.base_import_tests_models_complexService;
    }

    @ApiOperation(value = "批删除数据", tags = {"Base_import_tests_models_complex" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_base_import/base_import_tests_models_complices/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Base_import_tests_models_complexDTO> base_import_tests_models_complexdtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "获取数据", tags = {"Base_import_tests_models_complex" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_base_import/base_import_tests_models_complices/{base_import_tests_models_complex_id}")
    public ResponseEntity<Base_import_tests_models_complexDTO> get(@PathVariable("base_import_tests_models_complex_id") Integer base_import_tests_models_complex_id) {
        Base_import_tests_models_complexDTO dto = new Base_import_tests_models_complexDTO();
        Base_import_tests_models_complex domain = base_import_tests_models_complexService.get(base_import_tests_models_complex_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "建立数据", tags = {"Base_import_tests_models_complex" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_base_import/base_import_tests_models_complices")

    public ResponseEntity<Base_import_tests_models_complexDTO> create(@RequestBody Base_import_tests_models_complexDTO base_import_tests_models_complexdto) {
        Base_import_tests_models_complexDTO dto = new Base_import_tests_models_complexDTO();
        Base_import_tests_models_complex domain = base_import_tests_models_complexdto.toDO();
		base_import_tests_models_complexService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "更新数据", tags = {"Base_import_tests_models_complex" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_base_import/base_import_tests_models_complices/{base_import_tests_models_complex_id}")

    public ResponseEntity<Base_import_tests_models_complexDTO> update(@PathVariable("base_import_tests_models_complex_id") Integer base_import_tests_models_complex_id, @RequestBody Base_import_tests_models_complexDTO base_import_tests_models_complexdto) {
		Base_import_tests_models_complex domain = base_import_tests_models_complexdto.toDO();
        domain.setId(base_import_tests_models_complex_id);
		base_import_tests_models_complexService.update(domain);
		Base_import_tests_models_complexDTO dto = new Base_import_tests_models_complexDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "删除数据", tags = {"Base_import_tests_models_complex" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_base_import/base_import_tests_models_complices/{base_import_tests_models_complex_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("base_import_tests_models_complex_id") Integer base_import_tests_models_complex_id) {
        Base_import_tests_models_complexDTO base_import_tests_models_complexdto = new Base_import_tests_models_complexDTO();
		Base_import_tests_models_complex domain = new Base_import_tests_models_complex();
		base_import_tests_models_complexdto.setId(base_import_tests_models_complex_id);
		domain.setId(base_import_tests_models_complex_id);
        Boolean rst = base_import_tests_models_complexService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "批更新数据", tags = {"Base_import_tests_models_complex" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_base_import/base_import_tests_models_complices/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Base_import_tests_models_complexDTO> base_import_tests_models_complexdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "批建立数据", tags = {"Base_import_tests_models_complex" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_base_import/base_import_tests_models_complices/createBatch")
    public ResponseEntity<Boolean> createBatchBase_import_tests_models_complex(@RequestBody List<Base_import_tests_models_complexDTO> base_import_tests_models_complexdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Base_import_tests_models_complex" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_base_import/base_import_tests_models_complices/fetchdefault")
	public ResponseEntity<Page<Base_import_tests_models_complexDTO>> fetchDefault(Base_import_tests_models_complexSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Base_import_tests_models_complexDTO> list = new ArrayList<Base_import_tests_models_complexDTO>();
        
        Page<Base_import_tests_models_complex> domains = base_import_tests_models_complexService.searchDefault(context) ;
        for(Base_import_tests_models_complex base_import_tests_models_complex : domains.getContent()){
            Base_import_tests_models_complexDTO dto = new Base_import_tests_models_complexDTO();
            dto.fromDO(base_import_tests_models_complex);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
