package cn.ibizlab.odoo.service.odoo_account.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_account.dto.Account_account_typeDTO;
import cn.ibizlab.odoo.core.odoo_account.domain.Account_account_type;
import cn.ibizlab.odoo.core.odoo_account.service.IAccount_account_typeService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_account_typeSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Account_account_type" })
@RestController
@RequestMapping("")
public class Account_account_typeResource {

    @Autowired
    private IAccount_account_typeService account_account_typeService;

    public IAccount_account_typeService getAccount_account_typeService() {
        return this.account_account_typeService;
    }

    @ApiOperation(value = "批更新数据", tags = {"Account_account_type" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_account/account_account_types/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Account_account_typeDTO> account_account_typedtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "批删除数据", tags = {"Account_account_type" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_account/account_account_types/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Account_account_typeDTO> account_account_typedtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "获取数据", tags = {"Account_account_type" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_account/account_account_types/{account_account_type_id}")
    public ResponseEntity<Account_account_typeDTO> get(@PathVariable("account_account_type_id") Integer account_account_type_id) {
        Account_account_typeDTO dto = new Account_account_typeDTO();
        Account_account_type domain = account_account_typeService.get(account_account_type_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批建立数据", tags = {"Account_account_type" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_account/account_account_types/createBatch")
    public ResponseEntity<Boolean> createBatchAccount_account_type(@RequestBody List<Account_account_typeDTO> account_account_typedtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "更新数据", tags = {"Account_account_type" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_account/account_account_types/{account_account_type_id}")

    public ResponseEntity<Account_account_typeDTO> update(@PathVariable("account_account_type_id") Integer account_account_type_id, @RequestBody Account_account_typeDTO account_account_typedto) {
		Account_account_type domain = account_account_typedto.toDO();
        domain.setId(account_account_type_id);
		account_account_typeService.update(domain);
		Account_account_typeDTO dto = new Account_account_typeDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "建立数据", tags = {"Account_account_type" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_account/account_account_types")

    public ResponseEntity<Account_account_typeDTO> create(@RequestBody Account_account_typeDTO account_account_typedto) {
        Account_account_typeDTO dto = new Account_account_typeDTO();
        Account_account_type domain = account_account_typedto.toDO();
		account_account_typeService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "删除数据", tags = {"Account_account_type" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_account/account_account_types/{account_account_type_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("account_account_type_id") Integer account_account_type_id) {
        Account_account_typeDTO account_account_typedto = new Account_account_typeDTO();
		Account_account_type domain = new Account_account_type();
		account_account_typedto.setId(account_account_type_id);
		domain.setId(account_account_type_id);
        Boolean rst = account_account_typeService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

	@ApiOperation(value = "获取默认查询", tags = {"Account_account_type" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_account/account_account_types/fetchdefault")
	public ResponseEntity<Page<Account_account_typeDTO>> fetchDefault(Account_account_typeSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Account_account_typeDTO> list = new ArrayList<Account_account_typeDTO>();
        
        Page<Account_account_type> domains = account_account_typeService.searchDefault(context) ;
        for(Account_account_type account_account_type : domains.getContent()){
            Account_account_typeDTO dto = new Account_account_typeDTO();
            dto.fromDO(account_account_type);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
