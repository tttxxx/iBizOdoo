package cn.ibizlab.odoo.service.odoo_account.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_account.dto.Account_setup_bank_manual_configDTO;
import cn.ibizlab.odoo.core.odoo_account.domain.Account_setup_bank_manual_config;
import cn.ibizlab.odoo.core.odoo_account.service.IAccount_setup_bank_manual_configService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_setup_bank_manual_configSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Account_setup_bank_manual_config" })
@RestController
@RequestMapping("")
public class Account_setup_bank_manual_configResource {

    @Autowired
    private IAccount_setup_bank_manual_configService account_setup_bank_manual_configService;

    public IAccount_setup_bank_manual_configService getAccount_setup_bank_manual_configService() {
        return this.account_setup_bank_manual_configService;
    }

    @ApiOperation(value = "建立数据", tags = {"Account_setup_bank_manual_config" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_account/account_setup_bank_manual_configs")

    public ResponseEntity<Account_setup_bank_manual_configDTO> create(@RequestBody Account_setup_bank_manual_configDTO account_setup_bank_manual_configdto) {
        Account_setup_bank_manual_configDTO dto = new Account_setup_bank_manual_configDTO();
        Account_setup_bank_manual_config domain = account_setup_bank_manual_configdto.toDO();
		account_setup_bank_manual_configService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "更新数据", tags = {"Account_setup_bank_manual_config" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_account/account_setup_bank_manual_configs/{account_setup_bank_manual_config_id}")

    public ResponseEntity<Account_setup_bank_manual_configDTO> update(@PathVariable("account_setup_bank_manual_config_id") Integer account_setup_bank_manual_config_id, @RequestBody Account_setup_bank_manual_configDTO account_setup_bank_manual_configdto) {
		Account_setup_bank_manual_config domain = account_setup_bank_manual_configdto.toDO();
        domain.setId(account_setup_bank_manual_config_id);
		account_setup_bank_manual_configService.update(domain);
		Account_setup_bank_manual_configDTO dto = new Account_setup_bank_manual_configDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批更新数据", tags = {"Account_setup_bank_manual_config" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_account/account_setup_bank_manual_configs/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Account_setup_bank_manual_configDTO> account_setup_bank_manual_configdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "删除数据", tags = {"Account_setup_bank_manual_config" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_account/account_setup_bank_manual_configs/{account_setup_bank_manual_config_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("account_setup_bank_manual_config_id") Integer account_setup_bank_manual_config_id) {
        Account_setup_bank_manual_configDTO account_setup_bank_manual_configdto = new Account_setup_bank_manual_configDTO();
		Account_setup_bank_manual_config domain = new Account_setup_bank_manual_config();
		account_setup_bank_manual_configdto.setId(account_setup_bank_manual_config_id);
		domain.setId(account_setup_bank_manual_config_id);
        Boolean rst = account_setup_bank_manual_configService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "批建立数据", tags = {"Account_setup_bank_manual_config" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_account/account_setup_bank_manual_configs/createBatch")
    public ResponseEntity<Boolean> createBatchAccount_setup_bank_manual_config(@RequestBody List<Account_setup_bank_manual_configDTO> account_setup_bank_manual_configdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "获取数据", tags = {"Account_setup_bank_manual_config" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_account/account_setup_bank_manual_configs/{account_setup_bank_manual_config_id}")
    public ResponseEntity<Account_setup_bank_manual_configDTO> get(@PathVariable("account_setup_bank_manual_config_id") Integer account_setup_bank_manual_config_id) {
        Account_setup_bank_manual_configDTO dto = new Account_setup_bank_manual_configDTO();
        Account_setup_bank_manual_config domain = account_setup_bank_manual_configService.get(account_setup_bank_manual_config_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批删除数据", tags = {"Account_setup_bank_manual_config" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_account/account_setup_bank_manual_configs/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Account_setup_bank_manual_configDTO> account_setup_bank_manual_configdtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Account_setup_bank_manual_config" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_account/account_setup_bank_manual_configs/fetchdefault")
	public ResponseEntity<Page<Account_setup_bank_manual_configDTO>> fetchDefault(Account_setup_bank_manual_configSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Account_setup_bank_manual_configDTO> list = new ArrayList<Account_setup_bank_manual_configDTO>();
        
        Page<Account_setup_bank_manual_config> domains = account_setup_bank_manual_configService.searchDefault(context) ;
        for(Account_setup_bank_manual_config account_setup_bank_manual_config : domains.getContent()){
            Account_setup_bank_manual_configDTO dto = new Account_setup_bank_manual_configDTO();
            dto.fromDO(account_setup_bank_manual_config);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
