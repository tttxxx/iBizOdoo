package cn.ibizlab.odoo.service.odoo_account.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_account.dto.Account_financial_year_opDTO;
import cn.ibizlab.odoo.core.odoo_account.domain.Account_financial_year_op;
import cn.ibizlab.odoo.core.odoo_account.service.IAccount_financial_year_opService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_financial_year_opSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Account_financial_year_op" })
@RestController
@RequestMapping("")
public class Account_financial_year_opResource {

    @Autowired
    private IAccount_financial_year_opService account_financial_year_opService;

    public IAccount_financial_year_opService getAccount_financial_year_opService() {
        return this.account_financial_year_opService;
    }

    @ApiOperation(value = "更新数据", tags = {"Account_financial_year_op" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_account/account_financial_year_ops/{account_financial_year_op_id}")

    public ResponseEntity<Account_financial_year_opDTO> update(@PathVariable("account_financial_year_op_id") Integer account_financial_year_op_id, @RequestBody Account_financial_year_opDTO account_financial_year_opdto) {
		Account_financial_year_op domain = account_financial_year_opdto.toDO();
        domain.setId(account_financial_year_op_id);
		account_financial_year_opService.update(domain);
		Account_financial_year_opDTO dto = new Account_financial_year_opDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批更新数据", tags = {"Account_financial_year_op" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_account/account_financial_year_ops/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Account_financial_year_opDTO> account_financial_year_opdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "建立数据", tags = {"Account_financial_year_op" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_account/account_financial_year_ops")

    public ResponseEntity<Account_financial_year_opDTO> create(@RequestBody Account_financial_year_opDTO account_financial_year_opdto) {
        Account_financial_year_opDTO dto = new Account_financial_year_opDTO();
        Account_financial_year_op domain = account_financial_year_opdto.toDO();
		account_financial_year_opService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取数据", tags = {"Account_financial_year_op" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_account/account_financial_year_ops/{account_financial_year_op_id}")
    public ResponseEntity<Account_financial_year_opDTO> get(@PathVariable("account_financial_year_op_id") Integer account_financial_year_op_id) {
        Account_financial_year_opDTO dto = new Account_financial_year_opDTO();
        Account_financial_year_op domain = account_financial_year_opService.get(account_financial_year_op_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批建立数据", tags = {"Account_financial_year_op" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_account/account_financial_year_ops/createBatch")
    public ResponseEntity<Boolean> createBatchAccount_financial_year_op(@RequestBody List<Account_financial_year_opDTO> account_financial_year_opdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "删除数据", tags = {"Account_financial_year_op" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_account/account_financial_year_ops/{account_financial_year_op_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("account_financial_year_op_id") Integer account_financial_year_op_id) {
        Account_financial_year_opDTO account_financial_year_opdto = new Account_financial_year_opDTO();
		Account_financial_year_op domain = new Account_financial_year_op();
		account_financial_year_opdto.setId(account_financial_year_op_id);
		domain.setId(account_financial_year_op_id);
        Boolean rst = account_financial_year_opService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "批删除数据", tags = {"Account_financial_year_op" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_account/account_financial_year_ops/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Account_financial_year_opDTO> account_financial_year_opdtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Account_financial_year_op" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_account/account_financial_year_ops/fetchdefault")
	public ResponseEntity<Page<Account_financial_year_opDTO>> fetchDefault(Account_financial_year_opSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Account_financial_year_opDTO> list = new ArrayList<Account_financial_year_opDTO>();
        
        Page<Account_financial_year_op> domains = account_financial_year_opService.searchDefault(context) ;
        for(Account_financial_year_op account_financial_year_op : domains.getContent()){
            Account_financial_year_opDTO dto = new Account_financial_year_opDTO();
            dto.fromDO(account_financial_year_op);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
