package cn.ibizlab.odoo.service.odoo_account.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_account.dto.Account_fiscal_yearDTO;
import cn.ibizlab.odoo.core.odoo_account.domain.Account_fiscal_year;
import cn.ibizlab.odoo.core.odoo_account.service.IAccount_fiscal_yearService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_fiscal_yearSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Account_fiscal_year" })
@RestController
@RequestMapping("")
public class Account_fiscal_yearResource {

    @Autowired
    private IAccount_fiscal_yearService account_fiscal_yearService;

    public IAccount_fiscal_yearService getAccount_fiscal_yearService() {
        return this.account_fiscal_yearService;
    }

    @ApiOperation(value = "批删除数据", tags = {"Account_fiscal_year" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_account/account_fiscal_years/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Account_fiscal_yearDTO> account_fiscal_yeardtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "删除数据", tags = {"Account_fiscal_year" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_account/account_fiscal_years/{account_fiscal_year_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("account_fiscal_year_id") Integer account_fiscal_year_id) {
        Account_fiscal_yearDTO account_fiscal_yeardto = new Account_fiscal_yearDTO();
		Account_fiscal_year domain = new Account_fiscal_year();
		account_fiscal_yeardto.setId(account_fiscal_year_id);
		domain.setId(account_fiscal_year_id);
        Boolean rst = account_fiscal_yearService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "更新数据", tags = {"Account_fiscal_year" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_account/account_fiscal_years/{account_fiscal_year_id}")

    public ResponseEntity<Account_fiscal_yearDTO> update(@PathVariable("account_fiscal_year_id") Integer account_fiscal_year_id, @RequestBody Account_fiscal_yearDTO account_fiscal_yeardto) {
		Account_fiscal_year domain = account_fiscal_yeardto.toDO();
        domain.setId(account_fiscal_year_id);
		account_fiscal_yearService.update(domain);
		Account_fiscal_yearDTO dto = new Account_fiscal_yearDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批建立数据", tags = {"Account_fiscal_year" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_account/account_fiscal_years/createBatch")
    public ResponseEntity<Boolean> createBatchAccount_fiscal_year(@RequestBody List<Account_fiscal_yearDTO> account_fiscal_yeardtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "获取数据", tags = {"Account_fiscal_year" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_account/account_fiscal_years/{account_fiscal_year_id}")
    public ResponseEntity<Account_fiscal_yearDTO> get(@PathVariable("account_fiscal_year_id") Integer account_fiscal_year_id) {
        Account_fiscal_yearDTO dto = new Account_fiscal_yearDTO();
        Account_fiscal_year domain = account_fiscal_yearService.get(account_fiscal_year_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批更新数据", tags = {"Account_fiscal_year" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_account/account_fiscal_years/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Account_fiscal_yearDTO> account_fiscal_yeardtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "建立数据", tags = {"Account_fiscal_year" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_account/account_fiscal_years")

    public ResponseEntity<Account_fiscal_yearDTO> create(@RequestBody Account_fiscal_yearDTO account_fiscal_yeardto) {
        Account_fiscal_yearDTO dto = new Account_fiscal_yearDTO();
        Account_fiscal_year domain = account_fiscal_yeardto.toDO();
		account_fiscal_yearService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Account_fiscal_year" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_account/account_fiscal_years/fetchdefault")
	public ResponseEntity<Page<Account_fiscal_yearDTO>> fetchDefault(Account_fiscal_yearSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Account_fiscal_yearDTO> list = new ArrayList<Account_fiscal_yearDTO>();
        
        Page<Account_fiscal_year> domains = account_fiscal_yearService.searchDefault(context) ;
        for(Account_fiscal_year account_fiscal_year : domains.getContent()){
            Account_fiscal_yearDTO dto = new Account_fiscal_yearDTO();
            dto.fromDO(account_fiscal_year);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
