package cn.ibizlab.odoo.service.odoo_account.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.core.odoo_account.valuerule.anno.account_move.*;
import cn.ibizlab.odoo.core.odoo_account.domain.Account_move;
import org.springframework.cglib.beans.BeanCopier;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 服务DTO对象[Account_moveDTO]
 */
public class Account_moveDTO implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @Account_moveDisplay_nameDefault(info = "默认规则")
    private String display_name;

    @JsonIgnore
    private boolean display_nameDirtyFlag;

    /**
     * 属性 [CREATE_DATE]
     *
     */
    @Account_moveCreate_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp create_date;

    @JsonIgnore
    private boolean create_dateDirtyFlag;

    /**
     * 属性 [NARRATION]
     *
     */
    @Account_moveNarrationDefault(info = "默认规则")
    private String narration;

    @JsonIgnore
    private boolean narrationDirtyFlag;

    /**
     * 属性 [MATCHED_PERCENTAGE]
     *
     */
    @Account_moveMatched_percentageDefault(info = "默认规则")
    private Double matched_percentage;

    @JsonIgnore
    private boolean matched_percentageDirtyFlag;

    /**
     * 属性 [TAX_TYPE_DOMAIN]
     *
     */
    @Account_moveTax_type_domainDefault(info = "默认规则")
    private String tax_type_domain;

    @JsonIgnore
    private boolean tax_type_domainDirtyFlag;

    /**
     * 属性 [NAME]
     *
     */
    @Account_moveNameDefault(info = "默认规则")
    private String name;

    @JsonIgnore
    private boolean nameDirtyFlag;

    /**
     * 属性 [STATE]
     *
     */
    @Account_moveStateDefault(info = "默认规则")
    private String state;

    @JsonIgnore
    private boolean stateDirtyFlag;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @Account_move__last_updateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp __last_update;

    @JsonIgnore
    private boolean __last_updateDirtyFlag;

    /**
     * 属性 [REF]
     *
     */
    @Account_moveRefDefault(info = "默认规则")
    private String ref;

    @JsonIgnore
    private boolean refDirtyFlag;

    /**
     * 属性 [DUMMY_ACCOUNT_ID]
     *
     */
    @Account_moveDummy_account_idDefault(info = "默认规则")
    private Integer dummy_account_id;

    @JsonIgnore
    private boolean dummy_account_idDirtyFlag;

    /**
     * 属性 [AUTO_REVERSE]
     *
     */
    @Account_moveAuto_reverseDefault(info = "默认规则")
    private String auto_reverse;

    @JsonIgnore
    private boolean auto_reverseDirtyFlag;

    /**
     * 属性 [DATE]
     *
     */
    @Account_moveDateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    private Timestamp date;

    @JsonIgnore
    private boolean dateDirtyFlag;

    /**
     * 属性 [AMOUNT]
     *
     */
    @Account_moveAmountDefault(info = "默认规则")
    private Double amount;

    @JsonIgnore
    private boolean amountDirtyFlag;

    /**
     * 属性 [ID]
     *
     */
    @Account_moveIdDefault(info = "默认规则")
    private Integer id;

    @JsonIgnore
    private boolean idDirtyFlag;

    /**
     * 属性 [WRITE_DATE]
     *
     */
    @Account_moveWrite_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp write_date;

    @JsonIgnore
    private boolean write_dateDirtyFlag;

    /**
     * 属性 [REVERSE_DATE]
     *
     */
    @Account_moveReverse_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    private Timestamp reverse_date;

    @JsonIgnore
    private boolean reverse_dateDirtyFlag;

    /**
     * 属性 [LINE_IDS]
     *
     */
    @Account_moveLine_idsDefault(info = "默认规则")
    private String line_ids;

    @JsonIgnore
    private boolean line_idsDirtyFlag;

    /**
     * 属性 [CURRENCY_ID_TEXT]
     *
     */
    @Account_moveCurrency_id_textDefault(info = "默认规则")
    private String currency_id_text;

    @JsonIgnore
    private boolean currency_id_textDirtyFlag;

    /**
     * 属性 [PARTNER_ID_TEXT]
     *
     */
    @Account_movePartner_id_textDefault(info = "默认规则")
    private String partner_id_text;

    @JsonIgnore
    private boolean partner_id_textDirtyFlag;

    /**
     * 属性 [WRITE_UID_TEXT]
     *
     */
    @Account_moveWrite_uid_textDefault(info = "默认规则")
    private String write_uid_text;

    @JsonIgnore
    private boolean write_uid_textDirtyFlag;

    /**
     * 属性 [REVERSE_ENTRY_ID_TEXT]
     *
     */
    @Account_moveReverse_entry_id_textDefault(info = "默认规则")
    private String reverse_entry_id_text;

    @JsonIgnore
    private boolean reverse_entry_id_textDirtyFlag;

    /**
     * 属性 [STOCK_MOVE_ID_TEXT]
     *
     */
    @Account_moveStock_move_id_textDefault(info = "默认规则")
    private String stock_move_id_text;

    @JsonIgnore
    private boolean stock_move_id_textDirtyFlag;

    /**
     * 属性 [JOURNAL_ID_TEXT]
     *
     */
    @Account_moveJournal_id_textDefault(info = "默认规则")
    private String journal_id_text;

    @JsonIgnore
    private boolean journal_id_textDirtyFlag;

    /**
     * 属性 [CREATE_UID_TEXT]
     *
     */
    @Account_moveCreate_uid_textDefault(info = "默认规则")
    private String create_uid_text;

    @JsonIgnore
    private boolean create_uid_textDirtyFlag;

    /**
     * 属性 [COMPANY_ID_TEXT]
     *
     */
    @Account_moveCompany_id_textDefault(info = "默认规则")
    private String company_id_text;

    @JsonIgnore
    private boolean company_id_textDirtyFlag;

    /**
     * 属性 [STOCK_MOVE_ID]
     *
     */
    @Account_moveStock_move_idDefault(info = "默认规则")
    private Integer stock_move_id;

    @JsonIgnore
    private boolean stock_move_idDirtyFlag;

    /**
     * 属性 [COMPANY_ID]
     *
     */
    @Account_moveCompany_idDefault(info = "默认规则")
    private Integer company_id;

    @JsonIgnore
    private boolean company_idDirtyFlag;

    /**
     * 属性 [JOURNAL_ID]
     *
     */
    @Account_moveJournal_idDefault(info = "默认规则")
    private Integer journal_id;

    @JsonIgnore
    private boolean journal_idDirtyFlag;

    /**
     * 属性 [TAX_CASH_BASIS_REC_ID]
     *
     */
    @Account_moveTax_cash_basis_rec_idDefault(info = "默认规则")
    private Integer tax_cash_basis_rec_id;

    @JsonIgnore
    private boolean tax_cash_basis_rec_idDirtyFlag;

    /**
     * 属性 [PARTNER_ID]
     *
     */
    @Account_movePartner_idDefault(info = "默认规则")
    private Integer partner_id;

    @JsonIgnore
    private boolean partner_idDirtyFlag;

    /**
     * 属性 [CURRENCY_ID]
     *
     */
    @Account_moveCurrency_idDefault(info = "默认规则")
    private Integer currency_id;

    @JsonIgnore
    private boolean currency_idDirtyFlag;

    /**
     * 属性 [WRITE_UID]
     *
     */
    @Account_moveWrite_uidDefault(info = "默认规则")
    private Integer write_uid;

    @JsonIgnore
    private boolean write_uidDirtyFlag;

    /**
     * 属性 [CREATE_UID]
     *
     */
    @Account_moveCreate_uidDefault(info = "默认规则")
    private Integer create_uid;

    @JsonIgnore
    private boolean create_uidDirtyFlag;

    /**
     * 属性 [REVERSE_ENTRY_ID]
     *
     */
    @Account_moveReverse_entry_idDefault(info = "默认规则")
    private Integer reverse_entry_id;

    @JsonIgnore
    private boolean reverse_entry_idDirtyFlag;


    /**
     * 获取 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return display_name ;
    }

    /**
     * 设置 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

    /**
     * 获取 [DISPLAY_NAME]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return display_nameDirtyFlag ;
    }

    /**
     * 获取 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return create_date ;
    }

    /**
     * 设置 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return create_dateDirtyFlag ;
    }

    /**
     * 获取 [NARRATION]
     */
    @JsonProperty("narration")
    public String getNarration(){
        return narration ;
    }

    /**
     * 设置 [NARRATION]
     */
    @JsonProperty("narration")
    public void setNarration(String  narration){
        this.narration = narration ;
        this.narrationDirtyFlag = true ;
    }

    /**
     * 获取 [NARRATION]脏标记
     */
    @JsonIgnore
    public boolean getNarrationDirtyFlag(){
        return narrationDirtyFlag ;
    }

    /**
     * 获取 [MATCHED_PERCENTAGE]
     */
    @JsonProperty("matched_percentage")
    public Double getMatched_percentage(){
        return matched_percentage ;
    }

    /**
     * 设置 [MATCHED_PERCENTAGE]
     */
    @JsonProperty("matched_percentage")
    public void setMatched_percentage(Double  matched_percentage){
        this.matched_percentage = matched_percentage ;
        this.matched_percentageDirtyFlag = true ;
    }

    /**
     * 获取 [MATCHED_PERCENTAGE]脏标记
     */
    @JsonIgnore
    public boolean getMatched_percentageDirtyFlag(){
        return matched_percentageDirtyFlag ;
    }

    /**
     * 获取 [TAX_TYPE_DOMAIN]
     */
    @JsonProperty("tax_type_domain")
    public String getTax_type_domain(){
        return tax_type_domain ;
    }

    /**
     * 设置 [TAX_TYPE_DOMAIN]
     */
    @JsonProperty("tax_type_domain")
    public void setTax_type_domain(String  tax_type_domain){
        this.tax_type_domain = tax_type_domain ;
        this.tax_type_domainDirtyFlag = true ;
    }

    /**
     * 获取 [TAX_TYPE_DOMAIN]脏标记
     */
    @JsonIgnore
    public boolean getTax_type_domainDirtyFlag(){
        return tax_type_domainDirtyFlag ;
    }

    /**
     * 获取 [NAME]
     */
    @JsonProperty("name")
    public String getName(){
        return name ;
    }

    /**
     * 设置 [NAME]
     */
    @JsonProperty("name")
    public void setName(String  name){
        this.name = name ;
        this.nameDirtyFlag = true ;
    }

    /**
     * 获取 [NAME]脏标记
     */
    @JsonIgnore
    public boolean getNameDirtyFlag(){
        return nameDirtyFlag ;
    }

    /**
     * 获取 [STATE]
     */
    @JsonProperty("state")
    public String getState(){
        return state ;
    }

    /**
     * 设置 [STATE]
     */
    @JsonProperty("state")
    public void setState(String  state){
        this.state = state ;
        this.stateDirtyFlag = true ;
    }

    /**
     * 获取 [STATE]脏标记
     */
    @JsonIgnore
    public boolean getStateDirtyFlag(){
        return stateDirtyFlag ;
    }

    /**
     * 获取 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return __last_update ;
    }

    /**
     * 设置 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

    /**
     * 获取 [__LAST_UPDATE]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return __last_updateDirtyFlag ;
    }

    /**
     * 获取 [REF]
     */
    @JsonProperty("ref")
    public String getRef(){
        return ref ;
    }

    /**
     * 设置 [REF]
     */
    @JsonProperty("ref")
    public void setRef(String  ref){
        this.ref = ref ;
        this.refDirtyFlag = true ;
    }

    /**
     * 获取 [REF]脏标记
     */
    @JsonIgnore
    public boolean getRefDirtyFlag(){
        return refDirtyFlag ;
    }

    /**
     * 获取 [DUMMY_ACCOUNT_ID]
     */
    @JsonProperty("dummy_account_id")
    public Integer getDummy_account_id(){
        return dummy_account_id ;
    }

    /**
     * 设置 [DUMMY_ACCOUNT_ID]
     */
    @JsonProperty("dummy_account_id")
    public void setDummy_account_id(Integer  dummy_account_id){
        this.dummy_account_id = dummy_account_id ;
        this.dummy_account_idDirtyFlag = true ;
    }

    /**
     * 获取 [DUMMY_ACCOUNT_ID]脏标记
     */
    @JsonIgnore
    public boolean getDummy_account_idDirtyFlag(){
        return dummy_account_idDirtyFlag ;
    }

    /**
     * 获取 [AUTO_REVERSE]
     */
    @JsonProperty("auto_reverse")
    public String getAuto_reverse(){
        return auto_reverse ;
    }

    /**
     * 设置 [AUTO_REVERSE]
     */
    @JsonProperty("auto_reverse")
    public void setAuto_reverse(String  auto_reverse){
        this.auto_reverse = auto_reverse ;
        this.auto_reverseDirtyFlag = true ;
    }

    /**
     * 获取 [AUTO_REVERSE]脏标记
     */
    @JsonIgnore
    public boolean getAuto_reverseDirtyFlag(){
        return auto_reverseDirtyFlag ;
    }

    /**
     * 获取 [DATE]
     */
    @JsonProperty("date")
    public Timestamp getDate(){
        return date ;
    }

    /**
     * 设置 [DATE]
     */
    @JsonProperty("date")
    public void setDate(Timestamp  date){
        this.date = date ;
        this.dateDirtyFlag = true ;
    }

    /**
     * 获取 [DATE]脏标记
     */
    @JsonIgnore
    public boolean getDateDirtyFlag(){
        return dateDirtyFlag ;
    }

    /**
     * 获取 [AMOUNT]
     */
    @JsonProperty("amount")
    public Double getAmount(){
        return amount ;
    }

    /**
     * 设置 [AMOUNT]
     */
    @JsonProperty("amount")
    public void setAmount(Double  amount){
        this.amount = amount ;
        this.amountDirtyFlag = true ;
    }

    /**
     * 获取 [AMOUNT]脏标记
     */
    @JsonIgnore
    public boolean getAmountDirtyFlag(){
        return amountDirtyFlag ;
    }

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

    /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return idDirtyFlag ;
    }

    /**
     * 获取 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return write_date ;
    }

    /**
     * 设置 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return write_dateDirtyFlag ;
    }

    /**
     * 获取 [REVERSE_DATE]
     */
    @JsonProperty("reverse_date")
    public Timestamp getReverse_date(){
        return reverse_date ;
    }

    /**
     * 设置 [REVERSE_DATE]
     */
    @JsonProperty("reverse_date")
    public void setReverse_date(Timestamp  reverse_date){
        this.reverse_date = reverse_date ;
        this.reverse_dateDirtyFlag = true ;
    }

    /**
     * 获取 [REVERSE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getReverse_dateDirtyFlag(){
        return reverse_dateDirtyFlag ;
    }

    /**
     * 获取 [LINE_IDS]
     */
    @JsonProperty("line_ids")
    public String getLine_ids(){
        return line_ids ;
    }

    /**
     * 设置 [LINE_IDS]
     */
    @JsonProperty("line_ids")
    public void setLine_ids(String  line_ids){
        this.line_ids = line_ids ;
        this.line_idsDirtyFlag = true ;
    }

    /**
     * 获取 [LINE_IDS]脏标记
     */
    @JsonIgnore
    public boolean getLine_idsDirtyFlag(){
        return line_idsDirtyFlag ;
    }

    /**
     * 获取 [CURRENCY_ID_TEXT]
     */
    @JsonProperty("currency_id_text")
    public String getCurrency_id_text(){
        return currency_id_text ;
    }

    /**
     * 设置 [CURRENCY_ID_TEXT]
     */
    @JsonProperty("currency_id_text")
    public void setCurrency_id_text(String  currency_id_text){
        this.currency_id_text = currency_id_text ;
        this.currency_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [CURRENCY_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getCurrency_id_textDirtyFlag(){
        return currency_id_textDirtyFlag ;
    }

    /**
     * 获取 [PARTNER_ID_TEXT]
     */
    @JsonProperty("partner_id_text")
    public String getPartner_id_text(){
        return partner_id_text ;
    }

    /**
     * 设置 [PARTNER_ID_TEXT]
     */
    @JsonProperty("partner_id_text")
    public void setPartner_id_text(String  partner_id_text){
        this.partner_id_text = partner_id_text ;
        this.partner_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [PARTNER_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getPartner_id_textDirtyFlag(){
        return partner_id_textDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return write_uid_text ;
    }

    /**
     * 设置 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return write_uid_textDirtyFlag ;
    }

    /**
     * 获取 [REVERSE_ENTRY_ID_TEXT]
     */
    @JsonProperty("reverse_entry_id_text")
    public String getReverse_entry_id_text(){
        return reverse_entry_id_text ;
    }

    /**
     * 设置 [REVERSE_ENTRY_ID_TEXT]
     */
    @JsonProperty("reverse_entry_id_text")
    public void setReverse_entry_id_text(String  reverse_entry_id_text){
        this.reverse_entry_id_text = reverse_entry_id_text ;
        this.reverse_entry_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [REVERSE_ENTRY_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getReverse_entry_id_textDirtyFlag(){
        return reverse_entry_id_textDirtyFlag ;
    }

    /**
     * 获取 [STOCK_MOVE_ID_TEXT]
     */
    @JsonProperty("stock_move_id_text")
    public String getStock_move_id_text(){
        return stock_move_id_text ;
    }

    /**
     * 设置 [STOCK_MOVE_ID_TEXT]
     */
    @JsonProperty("stock_move_id_text")
    public void setStock_move_id_text(String  stock_move_id_text){
        this.stock_move_id_text = stock_move_id_text ;
        this.stock_move_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [STOCK_MOVE_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getStock_move_id_textDirtyFlag(){
        return stock_move_id_textDirtyFlag ;
    }

    /**
     * 获取 [JOURNAL_ID_TEXT]
     */
    @JsonProperty("journal_id_text")
    public String getJournal_id_text(){
        return journal_id_text ;
    }

    /**
     * 设置 [JOURNAL_ID_TEXT]
     */
    @JsonProperty("journal_id_text")
    public void setJournal_id_text(String  journal_id_text){
        this.journal_id_text = journal_id_text ;
        this.journal_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [JOURNAL_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getJournal_id_textDirtyFlag(){
        return journal_id_textDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return create_uid_text ;
    }

    /**
     * 设置 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return create_uid_textDirtyFlag ;
    }

    /**
     * 获取 [COMPANY_ID_TEXT]
     */
    @JsonProperty("company_id_text")
    public String getCompany_id_text(){
        return company_id_text ;
    }

    /**
     * 设置 [COMPANY_ID_TEXT]
     */
    @JsonProperty("company_id_text")
    public void setCompany_id_text(String  company_id_text){
        this.company_id_text = company_id_text ;
        this.company_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [COMPANY_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getCompany_id_textDirtyFlag(){
        return company_id_textDirtyFlag ;
    }

    /**
     * 获取 [STOCK_MOVE_ID]
     */
    @JsonProperty("stock_move_id")
    public Integer getStock_move_id(){
        return stock_move_id ;
    }

    /**
     * 设置 [STOCK_MOVE_ID]
     */
    @JsonProperty("stock_move_id")
    public void setStock_move_id(Integer  stock_move_id){
        this.stock_move_id = stock_move_id ;
        this.stock_move_idDirtyFlag = true ;
    }

    /**
     * 获取 [STOCK_MOVE_ID]脏标记
     */
    @JsonIgnore
    public boolean getStock_move_idDirtyFlag(){
        return stock_move_idDirtyFlag ;
    }

    /**
     * 获取 [COMPANY_ID]
     */
    @JsonProperty("company_id")
    public Integer getCompany_id(){
        return company_id ;
    }

    /**
     * 设置 [COMPANY_ID]
     */
    @JsonProperty("company_id")
    public void setCompany_id(Integer  company_id){
        this.company_id = company_id ;
        this.company_idDirtyFlag = true ;
    }

    /**
     * 获取 [COMPANY_ID]脏标记
     */
    @JsonIgnore
    public boolean getCompany_idDirtyFlag(){
        return company_idDirtyFlag ;
    }

    /**
     * 获取 [JOURNAL_ID]
     */
    @JsonProperty("journal_id")
    public Integer getJournal_id(){
        return journal_id ;
    }

    /**
     * 设置 [JOURNAL_ID]
     */
    @JsonProperty("journal_id")
    public void setJournal_id(Integer  journal_id){
        this.journal_id = journal_id ;
        this.journal_idDirtyFlag = true ;
    }

    /**
     * 获取 [JOURNAL_ID]脏标记
     */
    @JsonIgnore
    public boolean getJournal_idDirtyFlag(){
        return journal_idDirtyFlag ;
    }

    /**
     * 获取 [TAX_CASH_BASIS_REC_ID]
     */
    @JsonProperty("tax_cash_basis_rec_id")
    public Integer getTax_cash_basis_rec_id(){
        return tax_cash_basis_rec_id ;
    }

    /**
     * 设置 [TAX_CASH_BASIS_REC_ID]
     */
    @JsonProperty("tax_cash_basis_rec_id")
    public void setTax_cash_basis_rec_id(Integer  tax_cash_basis_rec_id){
        this.tax_cash_basis_rec_id = tax_cash_basis_rec_id ;
        this.tax_cash_basis_rec_idDirtyFlag = true ;
    }

    /**
     * 获取 [TAX_CASH_BASIS_REC_ID]脏标记
     */
    @JsonIgnore
    public boolean getTax_cash_basis_rec_idDirtyFlag(){
        return tax_cash_basis_rec_idDirtyFlag ;
    }

    /**
     * 获取 [PARTNER_ID]
     */
    @JsonProperty("partner_id")
    public Integer getPartner_id(){
        return partner_id ;
    }

    /**
     * 设置 [PARTNER_ID]
     */
    @JsonProperty("partner_id")
    public void setPartner_id(Integer  partner_id){
        this.partner_id = partner_id ;
        this.partner_idDirtyFlag = true ;
    }

    /**
     * 获取 [PARTNER_ID]脏标记
     */
    @JsonIgnore
    public boolean getPartner_idDirtyFlag(){
        return partner_idDirtyFlag ;
    }

    /**
     * 获取 [CURRENCY_ID]
     */
    @JsonProperty("currency_id")
    public Integer getCurrency_id(){
        return currency_id ;
    }

    /**
     * 设置 [CURRENCY_ID]
     */
    @JsonProperty("currency_id")
    public void setCurrency_id(Integer  currency_id){
        this.currency_id = currency_id ;
        this.currency_idDirtyFlag = true ;
    }

    /**
     * 获取 [CURRENCY_ID]脏标记
     */
    @JsonIgnore
    public boolean getCurrency_idDirtyFlag(){
        return currency_idDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return write_uid ;
    }

    /**
     * 设置 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return write_uidDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return create_uid ;
    }

    /**
     * 设置 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return create_uidDirtyFlag ;
    }

    /**
     * 获取 [REVERSE_ENTRY_ID]
     */
    @JsonProperty("reverse_entry_id")
    public Integer getReverse_entry_id(){
        return reverse_entry_id ;
    }

    /**
     * 设置 [REVERSE_ENTRY_ID]
     */
    @JsonProperty("reverse_entry_id")
    public void setReverse_entry_id(Integer  reverse_entry_id){
        this.reverse_entry_id = reverse_entry_id ;
        this.reverse_entry_idDirtyFlag = true ;
    }

    /**
     * 获取 [REVERSE_ENTRY_ID]脏标记
     */
    @JsonIgnore
    public boolean getReverse_entry_idDirtyFlag(){
        return reverse_entry_idDirtyFlag ;
    }



    public Account_move toDO() {
        Account_move srfdomain = new Account_move();
        if(getDisplay_nameDirtyFlag())
            srfdomain.setDisplay_name(display_name);
        if(getCreate_dateDirtyFlag())
            srfdomain.setCreate_date(create_date);
        if(getNarrationDirtyFlag())
            srfdomain.setNarration(narration);
        if(getMatched_percentageDirtyFlag())
            srfdomain.setMatched_percentage(matched_percentage);
        if(getTax_type_domainDirtyFlag())
            srfdomain.setTax_type_domain(tax_type_domain);
        if(getNameDirtyFlag())
            srfdomain.setName(name);
        if(getStateDirtyFlag())
            srfdomain.setState(state);
        if(get__last_updateDirtyFlag())
            srfdomain.set__last_update(__last_update);
        if(getRefDirtyFlag())
            srfdomain.setRef(ref);
        if(getDummy_account_idDirtyFlag())
            srfdomain.setDummy_account_id(dummy_account_id);
        if(getAuto_reverseDirtyFlag())
            srfdomain.setAuto_reverse(auto_reverse);
        if(getDateDirtyFlag())
            srfdomain.setDate(date);
        if(getAmountDirtyFlag())
            srfdomain.setAmount(amount);
        if(getIdDirtyFlag())
            srfdomain.setId(id);
        if(getWrite_dateDirtyFlag())
            srfdomain.setWrite_date(write_date);
        if(getReverse_dateDirtyFlag())
            srfdomain.setReverse_date(reverse_date);
        if(getLine_idsDirtyFlag())
            srfdomain.setLine_ids(line_ids);
        if(getCurrency_id_textDirtyFlag())
            srfdomain.setCurrency_id_text(currency_id_text);
        if(getPartner_id_textDirtyFlag())
            srfdomain.setPartner_id_text(partner_id_text);
        if(getWrite_uid_textDirtyFlag())
            srfdomain.setWrite_uid_text(write_uid_text);
        if(getReverse_entry_id_textDirtyFlag())
            srfdomain.setReverse_entry_id_text(reverse_entry_id_text);
        if(getStock_move_id_textDirtyFlag())
            srfdomain.setStock_move_id_text(stock_move_id_text);
        if(getJournal_id_textDirtyFlag())
            srfdomain.setJournal_id_text(journal_id_text);
        if(getCreate_uid_textDirtyFlag())
            srfdomain.setCreate_uid_text(create_uid_text);
        if(getCompany_id_textDirtyFlag())
            srfdomain.setCompany_id_text(company_id_text);
        if(getStock_move_idDirtyFlag())
            srfdomain.setStock_move_id(stock_move_id);
        if(getCompany_idDirtyFlag())
            srfdomain.setCompany_id(company_id);
        if(getJournal_idDirtyFlag())
            srfdomain.setJournal_id(journal_id);
        if(getTax_cash_basis_rec_idDirtyFlag())
            srfdomain.setTax_cash_basis_rec_id(tax_cash_basis_rec_id);
        if(getPartner_idDirtyFlag())
            srfdomain.setPartner_id(partner_id);
        if(getCurrency_idDirtyFlag())
            srfdomain.setCurrency_id(currency_id);
        if(getWrite_uidDirtyFlag())
            srfdomain.setWrite_uid(write_uid);
        if(getCreate_uidDirtyFlag())
            srfdomain.setCreate_uid(create_uid);
        if(getReverse_entry_idDirtyFlag())
            srfdomain.setReverse_entry_id(reverse_entry_id);

        return srfdomain;
    }

    public void fromDO(Account_move srfdomain) {
        if(srfdomain == null )
          return ;
        if(srfdomain.getDisplay_nameDirtyFlag())
            this.setDisplay_name(srfdomain.getDisplay_name());
        if(srfdomain.getCreate_dateDirtyFlag())
            this.setCreate_date(srfdomain.getCreate_date());
        if(srfdomain.getNarrationDirtyFlag())
            this.setNarration(srfdomain.getNarration());
        if(srfdomain.getMatched_percentageDirtyFlag())
            this.setMatched_percentage(srfdomain.getMatched_percentage());
        if(srfdomain.getTax_type_domainDirtyFlag())
            this.setTax_type_domain(srfdomain.getTax_type_domain());
        if(srfdomain.getNameDirtyFlag())
            this.setName(srfdomain.getName());
        if(srfdomain.getStateDirtyFlag())
            this.setState(srfdomain.getState());
        if(srfdomain.get__last_updateDirtyFlag())
            this.set__last_update(srfdomain.get__last_update());
        if(srfdomain.getRefDirtyFlag())
            this.setRef(srfdomain.getRef());
        if(srfdomain.getDummy_account_idDirtyFlag())
            this.setDummy_account_id(srfdomain.getDummy_account_id());
        if(srfdomain.getAuto_reverseDirtyFlag())
            this.setAuto_reverse(srfdomain.getAuto_reverse());
        if(srfdomain.getDateDirtyFlag())
            this.setDate(srfdomain.getDate());
        if(srfdomain.getAmountDirtyFlag())
            this.setAmount(srfdomain.getAmount());
        if(srfdomain.getIdDirtyFlag())
            this.setId(srfdomain.getId());
        if(srfdomain.getWrite_dateDirtyFlag())
            this.setWrite_date(srfdomain.getWrite_date());
        if(srfdomain.getReverse_dateDirtyFlag())
            this.setReverse_date(srfdomain.getReverse_date());
        if(srfdomain.getLine_idsDirtyFlag())
            this.setLine_ids(srfdomain.getLine_ids());
        if(srfdomain.getCurrency_id_textDirtyFlag())
            this.setCurrency_id_text(srfdomain.getCurrency_id_text());
        if(srfdomain.getPartner_id_textDirtyFlag())
            this.setPartner_id_text(srfdomain.getPartner_id_text());
        if(srfdomain.getWrite_uid_textDirtyFlag())
            this.setWrite_uid_text(srfdomain.getWrite_uid_text());
        if(srfdomain.getReverse_entry_id_textDirtyFlag())
            this.setReverse_entry_id_text(srfdomain.getReverse_entry_id_text());
        if(srfdomain.getStock_move_id_textDirtyFlag())
            this.setStock_move_id_text(srfdomain.getStock_move_id_text());
        if(srfdomain.getJournal_id_textDirtyFlag())
            this.setJournal_id_text(srfdomain.getJournal_id_text());
        if(srfdomain.getCreate_uid_textDirtyFlag())
            this.setCreate_uid_text(srfdomain.getCreate_uid_text());
        if(srfdomain.getCompany_id_textDirtyFlag())
            this.setCompany_id_text(srfdomain.getCompany_id_text());
        if(srfdomain.getStock_move_idDirtyFlag())
            this.setStock_move_id(srfdomain.getStock_move_id());
        if(srfdomain.getCompany_idDirtyFlag())
            this.setCompany_id(srfdomain.getCompany_id());
        if(srfdomain.getJournal_idDirtyFlag())
            this.setJournal_id(srfdomain.getJournal_id());
        if(srfdomain.getTax_cash_basis_rec_idDirtyFlag())
            this.setTax_cash_basis_rec_id(srfdomain.getTax_cash_basis_rec_id());
        if(srfdomain.getPartner_idDirtyFlag())
            this.setPartner_id(srfdomain.getPartner_id());
        if(srfdomain.getCurrency_idDirtyFlag())
            this.setCurrency_id(srfdomain.getCurrency_id());
        if(srfdomain.getWrite_uidDirtyFlag())
            this.setWrite_uid(srfdomain.getWrite_uid());
        if(srfdomain.getCreate_uidDirtyFlag())
            this.setCreate_uid(srfdomain.getCreate_uid());
        if(srfdomain.getReverse_entry_idDirtyFlag())
            this.setReverse_entry_id(srfdomain.getReverse_entry_id());

    }

    public List<Account_moveDTO> fromDOPage(List<Account_move> poPage)   {
        if(poPage == null)
            return null;
        List<Account_moveDTO> dtos=new ArrayList<Account_moveDTO>();
        for(Account_move domain : poPage) {
            Account_moveDTO dto = new Account_moveDTO();
            dto.fromDO(domain);
            dtos.add(dto);
        }
        return dtos;
    }
}

