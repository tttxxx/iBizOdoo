package cn.ibizlab.odoo.service.odoo_account.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_account.dto.Account_move_reversalDTO;
import cn.ibizlab.odoo.core.odoo_account.domain.Account_move_reversal;
import cn.ibizlab.odoo.core.odoo_account.service.IAccount_move_reversalService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_move_reversalSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Account_move_reversal" })
@RestController
@RequestMapping("")
public class Account_move_reversalResource {

    @Autowired
    private IAccount_move_reversalService account_move_reversalService;

    public IAccount_move_reversalService getAccount_move_reversalService() {
        return this.account_move_reversalService;
    }

    @ApiOperation(value = "获取数据", tags = {"Account_move_reversal" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_account/account_move_reversals/{account_move_reversal_id}")
    public ResponseEntity<Account_move_reversalDTO> get(@PathVariable("account_move_reversal_id") Integer account_move_reversal_id) {
        Account_move_reversalDTO dto = new Account_move_reversalDTO();
        Account_move_reversal domain = account_move_reversalService.get(account_move_reversal_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批删除数据", tags = {"Account_move_reversal" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_account/account_move_reversals/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Account_move_reversalDTO> account_move_reversaldtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "批建立数据", tags = {"Account_move_reversal" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_account/account_move_reversals/createBatch")
    public ResponseEntity<Boolean> createBatchAccount_move_reversal(@RequestBody List<Account_move_reversalDTO> account_move_reversaldtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "批更新数据", tags = {"Account_move_reversal" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_account/account_move_reversals/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Account_move_reversalDTO> account_move_reversaldtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "更新数据", tags = {"Account_move_reversal" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_account/account_move_reversals/{account_move_reversal_id}")

    public ResponseEntity<Account_move_reversalDTO> update(@PathVariable("account_move_reversal_id") Integer account_move_reversal_id, @RequestBody Account_move_reversalDTO account_move_reversaldto) {
		Account_move_reversal domain = account_move_reversaldto.toDO();
        domain.setId(account_move_reversal_id);
		account_move_reversalService.update(domain);
		Account_move_reversalDTO dto = new Account_move_reversalDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "删除数据", tags = {"Account_move_reversal" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_account/account_move_reversals/{account_move_reversal_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("account_move_reversal_id") Integer account_move_reversal_id) {
        Account_move_reversalDTO account_move_reversaldto = new Account_move_reversalDTO();
		Account_move_reversal domain = new Account_move_reversal();
		account_move_reversaldto.setId(account_move_reversal_id);
		domain.setId(account_move_reversal_id);
        Boolean rst = account_move_reversalService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "建立数据", tags = {"Account_move_reversal" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_account/account_move_reversals")

    public ResponseEntity<Account_move_reversalDTO> create(@RequestBody Account_move_reversalDTO account_move_reversaldto) {
        Account_move_reversalDTO dto = new Account_move_reversalDTO();
        Account_move_reversal domain = account_move_reversaldto.toDO();
		account_move_reversalService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Account_move_reversal" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_account/account_move_reversals/fetchdefault")
	public ResponseEntity<Page<Account_move_reversalDTO>> fetchDefault(Account_move_reversalSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Account_move_reversalDTO> list = new ArrayList<Account_move_reversalDTO>();
        
        Page<Account_move_reversal> domains = account_move_reversalService.searchDefault(context) ;
        for(Account_move_reversal account_move_reversal : domains.getContent()){
            Account_move_reversalDTO dto = new Account_move_reversalDTO();
            dto.fromDO(account_move_reversal);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
