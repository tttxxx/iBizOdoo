package cn.ibizlab.odoo.service.odoo_account.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.core.odoo_account.valuerule.anno.account_fiscal_position_tax.*;
import cn.ibizlab.odoo.core.odoo_account.domain.Account_fiscal_position_tax;
import org.springframework.cglib.beans.BeanCopier;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 服务DTO对象[Account_fiscal_position_taxDTO]
 */
public class Account_fiscal_position_taxDTO implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [CREATE_DATE]
     *
     */
    @Account_fiscal_position_taxCreate_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp create_date;

    @JsonIgnore
    private boolean create_dateDirtyFlag;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @Account_fiscal_position_taxDisplay_nameDefault(info = "默认规则")
    private String display_name;

    @JsonIgnore
    private boolean display_nameDirtyFlag;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @Account_fiscal_position_tax__last_updateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp __last_update;

    @JsonIgnore
    private boolean __last_updateDirtyFlag;

    /**
     * 属性 [WRITE_DATE]
     *
     */
    @Account_fiscal_position_taxWrite_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp write_date;

    @JsonIgnore
    private boolean write_dateDirtyFlag;

    /**
     * 属性 [ID]
     *
     */
    @Account_fiscal_position_taxIdDefault(info = "默认规则")
    private Integer id;

    @JsonIgnore
    private boolean idDirtyFlag;

    /**
     * 属性 [CREATE_UID_TEXT]
     *
     */
    @Account_fiscal_position_taxCreate_uid_textDefault(info = "默认规则")
    private String create_uid_text;

    @JsonIgnore
    private boolean create_uid_textDirtyFlag;

    /**
     * 属性 [TAX_SRC_ID_TEXT]
     *
     */
    @Account_fiscal_position_taxTax_src_id_textDefault(info = "默认规则")
    private String tax_src_id_text;

    @JsonIgnore
    private boolean tax_src_id_textDirtyFlag;

    /**
     * 属性 [POSITION_ID_TEXT]
     *
     */
    @Account_fiscal_position_taxPosition_id_textDefault(info = "默认规则")
    private String position_id_text;

    @JsonIgnore
    private boolean position_id_textDirtyFlag;

    /**
     * 属性 [TAX_DEST_ID_TEXT]
     *
     */
    @Account_fiscal_position_taxTax_dest_id_textDefault(info = "默认规则")
    private String tax_dest_id_text;

    @JsonIgnore
    private boolean tax_dest_id_textDirtyFlag;

    /**
     * 属性 [WRITE_UID_TEXT]
     *
     */
    @Account_fiscal_position_taxWrite_uid_textDefault(info = "默认规则")
    private String write_uid_text;

    @JsonIgnore
    private boolean write_uid_textDirtyFlag;

    /**
     * 属性 [WRITE_UID]
     *
     */
    @Account_fiscal_position_taxWrite_uidDefault(info = "默认规则")
    private Integer write_uid;

    @JsonIgnore
    private boolean write_uidDirtyFlag;

    /**
     * 属性 [POSITION_ID]
     *
     */
    @Account_fiscal_position_taxPosition_idDefault(info = "默认规则")
    private Integer position_id;

    @JsonIgnore
    private boolean position_idDirtyFlag;

    /**
     * 属性 [TAX_DEST_ID]
     *
     */
    @Account_fiscal_position_taxTax_dest_idDefault(info = "默认规则")
    private Integer tax_dest_id;

    @JsonIgnore
    private boolean tax_dest_idDirtyFlag;

    /**
     * 属性 [TAX_SRC_ID]
     *
     */
    @Account_fiscal_position_taxTax_src_idDefault(info = "默认规则")
    private Integer tax_src_id;

    @JsonIgnore
    private boolean tax_src_idDirtyFlag;

    /**
     * 属性 [CREATE_UID]
     *
     */
    @Account_fiscal_position_taxCreate_uidDefault(info = "默认规则")
    private Integer create_uid;

    @JsonIgnore
    private boolean create_uidDirtyFlag;


    /**
     * 获取 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return create_date ;
    }

    /**
     * 设置 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return create_dateDirtyFlag ;
    }

    /**
     * 获取 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return display_name ;
    }

    /**
     * 设置 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

    /**
     * 获取 [DISPLAY_NAME]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return display_nameDirtyFlag ;
    }

    /**
     * 获取 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return __last_update ;
    }

    /**
     * 设置 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

    /**
     * 获取 [__LAST_UPDATE]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return __last_updateDirtyFlag ;
    }

    /**
     * 获取 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return write_date ;
    }

    /**
     * 设置 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return write_dateDirtyFlag ;
    }

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

    /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return idDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return create_uid_text ;
    }

    /**
     * 设置 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return create_uid_textDirtyFlag ;
    }

    /**
     * 获取 [TAX_SRC_ID_TEXT]
     */
    @JsonProperty("tax_src_id_text")
    public String getTax_src_id_text(){
        return tax_src_id_text ;
    }

    /**
     * 设置 [TAX_SRC_ID_TEXT]
     */
    @JsonProperty("tax_src_id_text")
    public void setTax_src_id_text(String  tax_src_id_text){
        this.tax_src_id_text = tax_src_id_text ;
        this.tax_src_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [TAX_SRC_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getTax_src_id_textDirtyFlag(){
        return tax_src_id_textDirtyFlag ;
    }

    /**
     * 获取 [POSITION_ID_TEXT]
     */
    @JsonProperty("position_id_text")
    public String getPosition_id_text(){
        return position_id_text ;
    }

    /**
     * 设置 [POSITION_ID_TEXT]
     */
    @JsonProperty("position_id_text")
    public void setPosition_id_text(String  position_id_text){
        this.position_id_text = position_id_text ;
        this.position_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [POSITION_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getPosition_id_textDirtyFlag(){
        return position_id_textDirtyFlag ;
    }

    /**
     * 获取 [TAX_DEST_ID_TEXT]
     */
    @JsonProperty("tax_dest_id_text")
    public String getTax_dest_id_text(){
        return tax_dest_id_text ;
    }

    /**
     * 设置 [TAX_DEST_ID_TEXT]
     */
    @JsonProperty("tax_dest_id_text")
    public void setTax_dest_id_text(String  tax_dest_id_text){
        this.tax_dest_id_text = tax_dest_id_text ;
        this.tax_dest_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [TAX_DEST_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getTax_dest_id_textDirtyFlag(){
        return tax_dest_id_textDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return write_uid_text ;
    }

    /**
     * 设置 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return write_uid_textDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return write_uid ;
    }

    /**
     * 设置 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return write_uidDirtyFlag ;
    }

    /**
     * 获取 [POSITION_ID]
     */
    @JsonProperty("position_id")
    public Integer getPosition_id(){
        return position_id ;
    }

    /**
     * 设置 [POSITION_ID]
     */
    @JsonProperty("position_id")
    public void setPosition_id(Integer  position_id){
        this.position_id = position_id ;
        this.position_idDirtyFlag = true ;
    }

    /**
     * 获取 [POSITION_ID]脏标记
     */
    @JsonIgnore
    public boolean getPosition_idDirtyFlag(){
        return position_idDirtyFlag ;
    }

    /**
     * 获取 [TAX_DEST_ID]
     */
    @JsonProperty("tax_dest_id")
    public Integer getTax_dest_id(){
        return tax_dest_id ;
    }

    /**
     * 设置 [TAX_DEST_ID]
     */
    @JsonProperty("tax_dest_id")
    public void setTax_dest_id(Integer  tax_dest_id){
        this.tax_dest_id = tax_dest_id ;
        this.tax_dest_idDirtyFlag = true ;
    }

    /**
     * 获取 [TAX_DEST_ID]脏标记
     */
    @JsonIgnore
    public boolean getTax_dest_idDirtyFlag(){
        return tax_dest_idDirtyFlag ;
    }

    /**
     * 获取 [TAX_SRC_ID]
     */
    @JsonProperty("tax_src_id")
    public Integer getTax_src_id(){
        return tax_src_id ;
    }

    /**
     * 设置 [TAX_SRC_ID]
     */
    @JsonProperty("tax_src_id")
    public void setTax_src_id(Integer  tax_src_id){
        this.tax_src_id = tax_src_id ;
        this.tax_src_idDirtyFlag = true ;
    }

    /**
     * 获取 [TAX_SRC_ID]脏标记
     */
    @JsonIgnore
    public boolean getTax_src_idDirtyFlag(){
        return tax_src_idDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return create_uid ;
    }

    /**
     * 设置 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return create_uidDirtyFlag ;
    }



    public Account_fiscal_position_tax toDO() {
        Account_fiscal_position_tax srfdomain = new Account_fiscal_position_tax();
        if(getCreate_dateDirtyFlag())
            srfdomain.setCreate_date(create_date);
        if(getDisplay_nameDirtyFlag())
            srfdomain.setDisplay_name(display_name);
        if(get__last_updateDirtyFlag())
            srfdomain.set__last_update(__last_update);
        if(getWrite_dateDirtyFlag())
            srfdomain.setWrite_date(write_date);
        if(getIdDirtyFlag())
            srfdomain.setId(id);
        if(getCreate_uid_textDirtyFlag())
            srfdomain.setCreate_uid_text(create_uid_text);
        if(getTax_src_id_textDirtyFlag())
            srfdomain.setTax_src_id_text(tax_src_id_text);
        if(getPosition_id_textDirtyFlag())
            srfdomain.setPosition_id_text(position_id_text);
        if(getTax_dest_id_textDirtyFlag())
            srfdomain.setTax_dest_id_text(tax_dest_id_text);
        if(getWrite_uid_textDirtyFlag())
            srfdomain.setWrite_uid_text(write_uid_text);
        if(getWrite_uidDirtyFlag())
            srfdomain.setWrite_uid(write_uid);
        if(getPosition_idDirtyFlag())
            srfdomain.setPosition_id(position_id);
        if(getTax_dest_idDirtyFlag())
            srfdomain.setTax_dest_id(tax_dest_id);
        if(getTax_src_idDirtyFlag())
            srfdomain.setTax_src_id(tax_src_id);
        if(getCreate_uidDirtyFlag())
            srfdomain.setCreate_uid(create_uid);

        return srfdomain;
    }

    public void fromDO(Account_fiscal_position_tax srfdomain) {
        if(srfdomain == null )
          return ;
        if(srfdomain.getCreate_dateDirtyFlag())
            this.setCreate_date(srfdomain.getCreate_date());
        if(srfdomain.getDisplay_nameDirtyFlag())
            this.setDisplay_name(srfdomain.getDisplay_name());
        if(srfdomain.get__last_updateDirtyFlag())
            this.set__last_update(srfdomain.get__last_update());
        if(srfdomain.getWrite_dateDirtyFlag())
            this.setWrite_date(srfdomain.getWrite_date());
        if(srfdomain.getIdDirtyFlag())
            this.setId(srfdomain.getId());
        if(srfdomain.getCreate_uid_textDirtyFlag())
            this.setCreate_uid_text(srfdomain.getCreate_uid_text());
        if(srfdomain.getTax_src_id_textDirtyFlag())
            this.setTax_src_id_text(srfdomain.getTax_src_id_text());
        if(srfdomain.getPosition_id_textDirtyFlag())
            this.setPosition_id_text(srfdomain.getPosition_id_text());
        if(srfdomain.getTax_dest_id_textDirtyFlag())
            this.setTax_dest_id_text(srfdomain.getTax_dest_id_text());
        if(srfdomain.getWrite_uid_textDirtyFlag())
            this.setWrite_uid_text(srfdomain.getWrite_uid_text());
        if(srfdomain.getWrite_uidDirtyFlag())
            this.setWrite_uid(srfdomain.getWrite_uid());
        if(srfdomain.getPosition_idDirtyFlag())
            this.setPosition_id(srfdomain.getPosition_id());
        if(srfdomain.getTax_dest_idDirtyFlag())
            this.setTax_dest_id(srfdomain.getTax_dest_id());
        if(srfdomain.getTax_src_idDirtyFlag())
            this.setTax_src_id(srfdomain.getTax_src_id());
        if(srfdomain.getCreate_uidDirtyFlag())
            this.setCreate_uid(srfdomain.getCreate_uid());

    }

    public List<Account_fiscal_position_taxDTO> fromDOPage(List<Account_fiscal_position_tax> poPage)   {
        if(poPage == null)
            return null;
        List<Account_fiscal_position_taxDTO> dtos=new ArrayList<Account_fiscal_position_taxDTO>();
        for(Account_fiscal_position_tax domain : poPage) {
            Account_fiscal_position_taxDTO dto = new Account_fiscal_position_taxDTO();
            dto.fromDO(domain);
            dtos.add(dto);
        }
        return dtos;
    }
}

