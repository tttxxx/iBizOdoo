package cn.ibizlab.odoo.service.odoo_account.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_account.dto.Account_fiscal_positionDTO;
import cn.ibizlab.odoo.core.odoo_account.domain.Account_fiscal_position;
import cn.ibizlab.odoo.core.odoo_account.service.IAccount_fiscal_positionService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_fiscal_positionSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Account_fiscal_position" })
@RestController
@RequestMapping("")
public class Account_fiscal_positionResource {

    @Autowired
    private IAccount_fiscal_positionService account_fiscal_positionService;

    public IAccount_fiscal_positionService getAccount_fiscal_positionService() {
        return this.account_fiscal_positionService;
    }

    @ApiOperation(value = "删除数据", tags = {"Account_fiscal_position" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_account/account_fiscal_positions/{account_fiscal_position_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("account_fiscal_position_id") Integer account_fiscal_position_id) {
        Account_fiscal_positionDTO account_fiscal_positiondto = new Account_fiscal_positionDTO();
		Account_fiscal_position domain = new Account_fiscal_position();
		account_fiscal_positiondto.setId(account_fiscal_position_id);
		domain.setId(account_fiscal_position_id);
        Boolean rst = account_fiscal_positionService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "批建立数据", tags = {"Account_fiscal_position" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_account/account_fiscal_positions/createBatch")
    public ResponseEntity<Boolean> createBatchAccount_fiscal_position(@RequestBody List<Account_fiscal_positionDTO> account_fiscal_positiondtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "建立数据", tags = {"Account_fiscal_position" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_account/account_fiscal_positions")

    public ResponseEntity<Account_fiscal_positionDTO> create(@RequestBody Account_fiscal_positionDTO account_fiscal_positiondto) {
        Account_fiscal_positionDTO dto = new Account_fiscal_positionDTO();
        Account_fiscal_position domain = account_fiscal_positiondto.toDO();
		account_fiscal_positionService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批删除数据", tags = {"Account_fiscal_position" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_account/account_fiscal_positions/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Account_fiscal_positionDTO> account_fiscal_positiondtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "更新数据", tags = {"Account_fiscal_position" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_account/account_fiscal_positions/{account_fiscal_position_id}")

    public ResponseEntity<Account_fiscal_positionDTO> update(@PathVariable("account_fiscal_position_id") Integer account_fiscal_position_id, @RequestBody Account_fiscal_positionDTO account_fiscal_positiondto) {
		Account_fiscal_position domain = account_fiscal_positiondto.toDO();
        domain.setId(account_fiscal_position_id);
		account_fiscal_positionService.update(domain);
		Account_fiscal_positionDTO dto = new Account_fiscal_positionDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批更新数据", tags = {"Account_fiscal_position" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_account/account_fiscal_positions/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Account_fiscal_positionDTO> account_fiscal_positiondtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "获取数据", tags = {"Account_fiscal_position" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_account/account_fiscal_positions/{account_fiscal_position_id}")
    public ResponseEntity<Account_fiscal_positionDTO> get(@PathVariable("account_fiscal_position_id") Integer account_fiscal_position_id) {
        Account_fiscal_positionDTO dto = new Account_fiscal_positionDTO();
        Account_fiscal_position domain = account_fiscal_positionService.get(account_fiscal_position_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Account_fiscal_position" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_account/account_fiscal_positions/fetchdefault")
	public ResponseEntity<Page<Account_fiscal_positionDTO>> fetchDefault(Account_fiscal_positionSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Account_fiscal_positionDTO> list = new ArrayList<Account_fiscal_positionDTO>();
        
        Page<Account_fiscal_position> domains = account_fiscal_positionService.searchDefault(context) ;
        for(Account_fiscal_position account_fiscal_position : domains.getContent()){
            Account_fiscal_positionDTO dto = new Account_fiscal_positionDTO();
            dto.fromDO(account_fiscal_position);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
