package cn.ibizlab.odoo.service.odoo_account.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_account.dto.Account_reconcile_modelDTO;
import cn.ibizlab.odoo.core.odoo_account.domain.Account_reconcile_model;
import cn.ibizlab.odoo.core.odoo_account.service.IAccount_reconcile_modelService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_reconcile_modelSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Account_reconcile_model" })
@RestController
@RequestMapping("")
public class Account_reconcile_modelResource {

    @Autowired
    private IAccount_reconcile_modelService account_reconcile_modelService;

    public IAccount_reconcile_modelService getAccount_reconcile_modelService() {
        return this.account_reconcile_modelService;
    }

    @ApiOperation(value = "获取数据", tags = {"Account_reconcile_model" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_account/account_reconcile_models/{account_reconcile_model_id}")
    public ResponseEntity<Account_reconcile_modelDTO> get(@PathVariable("account_reconcile_model_id") Integer account_reconcile_model_id) {
        Account_reconcile_modelDTO dto = new Account_reconcile_modelDTO();
        Account_reconcile_model domain = account_reconcile_modelService.get(account_reconcile_model_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批删除数据", tags = {"Account_reconcile_model" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_account/account_reconcile_models/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Account_reconcile_modelDTO> account_reconcile_modeldtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "批建立数据", tags = {"Account_reconcile_model" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_account/account_reconcile_models/createBatch")
    public ResponseEntity<Boolean> createBatchAccount_reconcile_model(@RequestBody List<Account_reconcile_modelDTO> account_reconcile_modeldtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "批更新数据", tags = {"Account_reconcile_model" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_account/account_reconcile_models/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Account_reconcile_modelDTO> account_reconcile_modeldtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "删除数据", tags = {"Account_reconcile_model" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_account/account_reconcile_models/{account_reconcile_model_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("account_reconcile_model_id") Integer account_reconcile_model_id) {
        Account_reconcile_modelDTO account_reconcile_modeldto = new Account_reconcile_modelDTO();
		Account_reconcile_model domain = new Account_reconcile_model();
		account_reconcile_modeldto.setId(account_reconcile_model_id);
		domain.setId(account_reconcile_model_id);
        Boolean rst = account_reconcile_modelService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "建立数据", tags = {"Account_reconcile_model" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_account/account_reconcile_models")

    public ResponseEntity<Account_reconcile_modelDTO> create(@RequestBody Account_reconcile_modelDTO account_reconcile_modeldto) {
        Account_reconcile_modelDTO dto = new Account_reconcile_modelDTO();
        Account_reconcile_model domain = account_reconcile_modeldto.toDO();
		account_reconcile_modelService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "更新数据", tags = {"Account_reconcile_model" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_account/account_reconcile_models/{account_reconcile_model_id}")

    public ResponseEntity<Account_reconcile_modelDTO> update(@PathVariable("account_reconcile_model_id") Integer account_reconcile_model_id, @RequestBody Account_reconcile_modelDTO account_reconcile_modeldto) {
		Account_reconcile_model domain = account_reconcile_modeldto.toDO();
        domain.setId(account_reconcile_model_id);
		account_reconcile_modelService.update(domain);
		Account_reconcile_modelDTO dto = new Account_reconcile_modelDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Account_reconcile_model" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_account/account_reconcile_models/fetchdefault")
	public ResponseEntity<Page<Account_reconcile_modelDTO>> fetchDefault(Account_reconcile_modelSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Account_reconcile_modelDTO> list = new ArrayList<Account_reconcile_modelDTO>();
        
        Page<Account_reconcile_model> domains = account_reconcile_modelService.searchDefault(context) ;
        for(Account_reconcile_model account_reconcile_model : domains.getContent()){
            Account_reconcile_modelDTO dto = new Account_reconcile_modelDTO();
            dto.fromDO(account_reconcile_model);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
