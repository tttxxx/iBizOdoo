package cn.ibizlab.odoo.service.odoo_account.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.core.odoo_account.valuerule.anno.account_analytic_tag.*;
import cn.ibizlab.odoo.core.odoo_account.domain.Account_analytic_tag;
import org.springframework.cglib.beans.BeanCopier;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 服务DTO对象[Account_analytic_tagDTO]
 */
public class Account_analytic_tagDTO implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [ANALYTIC_DISTRIBUTION_IDS]
     *
     */
    @Account_analytic_tagAnalytic_distribution_idsDefault(info = "默认规则")
    private String analytic_distribution_ids;

    @JsonIgnore
    private boolean analytic_distribution_idsDirtyFlag;

    /**
     * 属性 [ID]
     *
     */
    @Account_analytic_tagIdDefault(info = "默认规则")
    private Integer id;

    @JsonIgnore
    private boolean idDirtyFlag;

    /**
     * 属性 [ACTIVE_ANALYTIC_DISTRIBUTION]
     *
     */
    @Account_analytic_tagActive_analytic_distributionDefault(info = "默认规则")
    private String active_analytic_distribution;

    @JsonIgnore
    private boolean active_analytic_distributionDirtyFlag;

    /**
     * 属性 [NAME]
     *
     */
    @Account_analytic_tagNameDefault(info = "默认规则")
    private String name;

    @JsonIgnore
    private boolean nameDirtyFlag;

    /**
     * 属性 [ACTIVE]
     *
     */
    @Account_analytic_tagActiveDefault(info = "默认规则")
    private String active;

    @JsonIgnore
    private boolean activeDirtyFlag;

    /**
     * 属性 [WRITE_DATE]
     *
     */
    @Account_analytic_tagWrite_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp write_date;

    @JsonIgnore
    private boolean write_dateDirtyFlag;

    /**
     * 属性 [CREATE_DATE]
     *
     */
    @Account_analytic_tagCreate_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp create_date;

    @JsonIgnore
    private boolean create_dateDirtyFlag;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @Account_analytic_tag__last_updateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp __last_update;

    @JsonIgnore
    private boolean __last_updateDirtyFlag;

    /**
     * 属性 [COLOR]
     *
     */
    @Account_analytic_tagColorDefault(info = "默认规则")
    private Integer color;

    @JsonIgnore
    private boolean colorDirtyFlag;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @Account_analytic_tagDisplay_nameDefault(info = "默认规则")
    private String display_name;

    @JsonIgnore
    private boolean display_nameDirtyFlag;

    /**
     * 属性 [CREATE_UID_TEXT]
     *
     */
    @Account_analytic_tagCreate_uid_textDefault(info = "默认规则")
    private String create_uid_text;

    @JsonIgnore
    private boolean create_uid_textDirtyFlag;

    /**
     * 属性 [COMPANY_ID_TEXT]
     *
     */
    @Account_analytic_tagCompany_id_textDefault(info = "默认规则")
    private String company_id_text;

    @JsonIgnore
    private boolean company_id_textDirtyFlag;

    /**
     * 属性 [WRITE_UID_TEXT]
     *
     */
    @Account_analytic_tagWrite_uid_textDefault(info = "默认规则")
    private String write_uid_text;

    @JsonIgnore
    private boolean write_uid_textDirtyFlag;

    /**
     * 属性 [WRITE_UID]
     *
     */
    @Account_analytic_tagWrite_uidDefault(info = "默认规则")
    private Integer write_uid;

    @JsonIgnore
    private boolean write_uidDirtyFlag;

    /**
     * 属性 [CREATE_UID]
     *
     */
    @Account_analytic_tagCreate_uidDefault(info = "默认规则")
    private Integer create_uid;

    @JsonIgnore
    private boolean create_uidDirtyFlag;

    /**
     * 属性 [COMPANY_ID]
     *
     */
    @Account_analytic_tagCompany_idDefault(info = "默认规则")
    private Integer company_id;

    @JsonIgnore
    private boolean company_idDirtyFlag;


    /**
     * 获取 [ANALYTIC_DISTRIBUTION_IDS]
     */
    @JsonProperty("analytic_distribution_ids")
    public String getAnalytic_distribution_ids(){
        return analytic_distribution_ids ;
    }

    /**
     * 设置 [ANALYTIC_DISTRIBUTION_IDS]
     */
    @JsonProperty("analytic_distribution_ids")
    public void setAnalytic_distribution_ids(String  analytic_distribution_ids){
        this.analytic_distribution_ids = analytic_distribution_ids ;
        this.analytic_distribution_idsDirtyFlag = true ;
    }

    /**
     * 获取 [ANALYTIC_DISTRIBUTION_IDS]脏标记
     */
    @JsonIgnore
    public boolean getAnalytic_distribution_idsDirtyFlag(){
        return analytic_distribution_idsDirtyFlag ;
    }

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

    /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return idDirtyFlag ;
    }

    /**
     * 获取 [ACTIVE_ANALYTIC_DISTRIBUTION]
     */
    @JsonProperty("active_analytic_distribution")
    public String getActive_analytic_distribution(){
        return active_analytic_distribution ;
    }

    /**
     * 设置 [ACTIVE_ANALYTIC_DISTRIBUTION]
     */
    @JsonProperty("active_analytic_distribution")
    public void setActive_analytic_distribution(String  active_analytic_distribution){
        this.active_analytic_distribution = active_analytic_distribution ;
        this.active_analytic_distributionDirtyFlag = true ;
    }

    /**
     * 获取 [ACTIVE_ANALYTIC_DISTRIBUTION]脏标记
     */
    @JsonIgnore
    public boolean getActive_analytic_distributionDirtyFlag(){
        return active_analytic_distributionDirtyFlag ;
    }

    /**
     * 获取 [NAME]
     */
    @JsonProperty("name")
    public String getName(){
        return name ;
    }

    /**
     * 设置 [NAME]
     */
    @JsonProperty("name")
    public void setName(String  name){
        this.name = name ;
        this.nameDirtyFlag = true ;
    }

    /**
     * 获取 [NAME]脏标记
     */
    @JsonIgnore
    public boolean getNameDirtyFlag(){
        return nameDirtyFlag ;
    }

    /**
     * 获取 [ACTIVE]
     */
    @JsonProperty("active")
    public String getActive(){
        return active ;
    }

    /**
     * 设置 [ACTIVE]
     */
    @JsonProperty("active")
    public void setActive(String  active){
        this.active = active ;
        this.activeDirtyFlag = true ;
    }

    /**
     * 获取 [ACTIVE]脏标记
     */
    @JsonIgnore
    public boolean getActiveDirtyFlag(){
        return activeDirtyFlag ;
    }

    /**
     * 获取 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return write_date ;
    }

    /**
     * 设置 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return write_dateDirtyFlag ;
    }

    /**
     * 获取 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return create_date ;
    }

    /**
     * 设置 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return create_dateDirtyFlag ;
    }

    /**
     * 获取 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return __last_update ;
    }

    /**
     * 设置 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

    /**
     * 获取 [__LAST_UPDATE]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return __last_updateDirtyFlag ;
    }

    /**
     * 获取 [COLOR]
     */
    @JsonProperty("color")
    public Integer getColor(){
        return color ;
    }

    /**
     * 设置 [COLOR]
     */
    @JsonProperty("color")
    public void setColor(Integer  color){
        this.color = color ;
        this.colorDirtyFlag = true ;
    }

    /**
     * 获取 [COLOR]脏标记
     */
    @JsonIgnore
    public boolean getColorDirtyFlag(){
        return colorDirtyFlag ;
    }

    /**
     * 获取 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return display_name ;
    }

    /**
     * 设置 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

    /**
     * 获取 [DISPLAY_NAME]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return display_nameDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return create_uid_text ;
    }

    /**
     * 设置 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return create_uid_textDirtyFlag ;
    }

    /**
     * 获取 [COMPANY_ID_TEXT]
     */
    @JsonProperty("company_id_text")
    public String getCompany_id_text(){
        return company_id_text ;
    }

    /**
     * 设置 [COMPANY_ID_TEXT]
     */
    @JsonProperty("company_id_text")
    public void setCompany_id_text(String  company_id_text){
        this.company_id_text = company_id_text ;
        this.company_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [COMPANY_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getCompany_id_textDirtyFlag(){
        return company_id_textDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return write_uid_text ;
    }

    /**
     * 设置 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return write_uid_textDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return write_uid ;
    }

    /**
     * 设置 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return write_uidDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return create_uid ;
    }

    /**
     * 设置 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return create_uidDirtyFlag ;
    }

    /**
     * 获取 [COMPANY_ID]
     */
    @JsonProperty("company_id")
    public Integer getCompany_id(){
        return company_id ;
    }

    /**
     * 设置 [COMPANY_ID]
     */
    @JsonProperty("company_id")
    public void setCompany_id(Integer  company_id){
        this.company_id = company_id ;
        this.company_idDirtyFlag = true ;
    }

    /**
     * 获取 [COMPANY_ID]脏标记
     */
    @JsonIgnore
    public boolean getCompany_idDirtyFlag(){
        return company_idDirtyFlag ;
    }



    public Account_analytic_tag toDO() {
        Account_analytic_tag srfdomain = new Account_analytic_tag();
        if(getAnalytic_distribution_idsDirtyFlag())
            srfdomain.setAnalytic_distribution_ids(analytic_distribution_ids);
        if(getIdDirtyFlag())
            srfdomain.setId(id);
        if(getActive_analytic_distributionDirtyFlag())
            srfdomain.setActive_analytic_distribution(active_analytic_distribution);
        if(getNameDirtyFlag())
            srfdomain.setName(name);
        if(getActiveDirtyFlag())
            srfdomain.setActive(active);
        if(getWrite_dateDirtyFlag())
            srfdomain.setWrite_date(write_date);
        if(getCreate_dateDirtyFlag())
            srfdomain.setCreate_date(create_date);
        if(get__last_updateDirtyFlag())
            srfdomain.set__last_update(__last_update);
        if(getColorDirtyFlag())
            srfdomain.setColor(color);
        if(getDisplay_nameDirtyFlag())
            srfdomain.setDisplay_name(display_name);
        if(getCreate_uid_textDirtyFlag())
            srfdomain.setCreate_uid_text(create_uid_text);
        if(getCompany_id_textDirtyFlag())
            srfdomain.setCompany_id_text(company_id_text);
        if(getWrite_uid_textDirtyFlag())
            srfdomain.setWrite_uid_text(write_uid_text);
        if(getWrite_uidDirtyFlag())
            srfdomain.setWrite_uid(write_uid);
        if(getCreate_uidDirtyFlag())
            srfdomain.setCreate_uid(create_uid);
        if(getCompany_idDirtyFlag())
            srfdomain.setCompany_id(company_id);

        return srfdomain;
    }

    public void fromDO(Account_analytic_tag srfdomain) {
        if(srfdomain == null )
          return ;
        if(srfdomain.getAnalytic_distribution_idsDirtyFlag())
            this.setAnalytic_distribution_ids(srfdomain.getAnalytic_distribution_ids());
        if(srfdomain.getIdDirtyFlag())
            this.setId(srfdomain.getId());
        if(srfdomain.getActive_analytic_distributionDirtyFlag())
            this.setActive_analytic_distribution(srfdomain.getActive_analytic_distribution());
        if(srfdomain.getNameDirtyFlag())
            this.setName(srfdomain.getName());
        if(srfdomain.getActiveDirtyFlag())
            this.setActive(srfdomain.getActive());
        if(srfdomain.getWrite_dateDirtyFlag())
            this.setWrite_date(srfdomain.getWrite_date());
        if(srfdomain.getCreate_dateDirtyFlag())
            this.setCreate_date(srfdomain.getCreate_date());
        if(srfdomain.get__last_updateDirtyFlag())
            this.set__last_update(srfdomain.get__last_update());
        if(srfdomain.getColorDirtyFlag())
            this.setColor(srfdomain.getColor());
        if(srfdomain.getDisplay_nameDirtyFlag())
            this.setDisplay_name(srfdomain.getDisplay_name());
        if(srfdomain.getCreate_uid_textDirtyFlag())
            this.setCreate_uid_text(srfdomain.getCreate_uid_text());
        if(srfdomain.getCompany_id_textDirtyFlag())
            this.setCompany_id_text(srfdomain.getCompany_id_text());
        if(srfdomain.getWrite_uid_textDirtyFlag())
            this.setWrite_uid_text(srfdomain.getWrite_uid_text());
        if(srfdomain.getWrite_uidDirtyFlag())
            this.setWrite_uid(srfdomain.getWrite_uid());
        if(srfdomain.getCreate_uidDirtyFlag())
            this.setCreate_uid(srfdomain.getCreate_uid());
        if(srfdomain.getCompany_idDirtyFlag())
            this.setCompany_id(srfdomain.getCompany_id());

    }

    public List<Account_analytic_tagDTO> fromDOPage(List<Account_analytic_tag> poPage)   {
        if(poPage == null)
            return null;
        List<Account_analytic_tagDTO> dtos=new ArrayList<Account_analytic_tagDTO>();
        for(Account_analytic_tag domain : poPage) {
            Account_analytic_tagDTO dto = new Account_analytic_tagDTO();
            dto.fromDO(domain);
            dtos.add(dto);
        }
        return dtos;
    }
}

