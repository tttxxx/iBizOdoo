package cn.ibizlab.odoo.service.odoo_account.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_account.dto.Account_common_journal_reportDTO;
import cn.ibizlab.odoo.core.odoo_account.domain.Account_common_journal_report;
import cn.ibizlab.odoo.core.odoo_account.service.IAccount_common_journal_reportService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_common_journal_reportSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Account_common_journal_report" })
@RestController
@RequestMapping("")
public class Account_common_journal_reportResource {

    @Autowired
    private IAccount_common_journal_reportService account_common_journal_reportService;

    public IAccount_common_journal_reportService getAccount_common_journal_reportService() {
        return this.account_common_journal_reportService;
    }

    @ApiOperation(value = "批建立数据", tags = {"Account_common_journal_report" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_account/account_common_journal_reports/createBatch")
    public ResponseEntity<Boolean> createBatchAccount_common_journal_report(@RequestBody List<Account_common_journal_reportDTO> account_common_journal_reportdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "获取数据", tags = {"Account_common_journal_report" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_account/account_common_journal_reports/{account_common_journal_report_id}")
    public ResponseEntity<Account_common_journal_reportDTO> get(@PathVariable("account_common_journal_report_id") Integer account_common_journal_report_id) {
        Account_common_journal_reportDTO dto = new Account_common_journal_reportDTO();
        Account_common_journal_report domain = account_common_journal_reportService.get(account_common_journal_report_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "更新数据", tags = {"Account_common_journal_report" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_account/account_common_journal_reports/{account_common_journal_report_id}")

    public ResponseEntity<Account_common_journal_reportDTO> update(@PathVariable("account_common_journal_report_id") Integer account_common_journal_report_id, @RequestBody Account_common_journal_reportDTO account_common_journal_reportdto) {
		Account_common_journal_report domain = account_common_journal_reportdto.toDO();
        domain.setId(account_common_journal_report_id);
		account_common_journal_reportService.update(domain);
		Account_common_journal_reportDTO dto = new Account_common_journal_reportDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批删除数据", tags = {"Account_common_journal_report" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_account/account_common_journal_reports/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Account_common_journal_reportDTO> account_common_journal_reportdtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "批更新数据", tags = {"Account_common_journal_report" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_account/account_common_journal_reports/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Account_common_journal_reportDTO> account_common_journal_reportdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "删除数据", tags = {"Account_common_journal_report" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_account/account_common_journal_reports/{account_common_journal_report_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("account_common_journal_report_id") Integer account_common_journal_report_id) {
        Account_common_journal_reportDTO account_common_journal_reportdto = new Account_common_journal_reportDTO();
		Account_common_journal_report domain = new Account_common_journal_report();
		account_common_journal_reportdto.setId(account_common_journal_report_id);
		domain.setId(account_common_journal_report_id);
        Boolean rst = account_common_journal_reportService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "建立数据", tags = {"Account_common_journal_report" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_account/account_common_journal_reports")

    public ResponseEntity<Account_common_journal_reportDTO> create(@RequestBody Account_common_journal_reportDTO account_common_journal_reportdto) {
        Account_common_journal_reportDTO dto = new Account_common_journal_reportDTO();
        Account_common_journal_report domain = account_common_journal_reportdto.toDO();
		account_common_journal_reportService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Account_common_journal_report" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_account/account_common_journal_reports/fetchdefault")
	public ResponseEntity<Page<Account_common_journal_reportDTO>> fetchDefault(Account_common_journal_reportSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Account_common_journal_reportDTO> list = new ArrayList<Account_common_journal_reportDTO>();
        
        Page<Account_common_journal_report> domains = account_common_journal_reportService.searchDefault(context) ;
        for(Account_common_journal_report account_common_journal_report : domains.getContent()){
            Account_common_journal_reportDTO dto = new Account_common_journal_reportDTO();
            dto.fromDO(account_common_journal_report);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
