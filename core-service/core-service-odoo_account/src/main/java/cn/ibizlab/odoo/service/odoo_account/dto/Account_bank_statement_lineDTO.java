package cn.ibizlab.odoo.service.odoo_account.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.core.odoo_account.valuerule.anno.account_bank_statement_line.*;
import cn.ibizlab.odoo.core.odoo_account.domain.Account_bank_statement_line;
import org.springframework.cglib.beans.BeanCopier;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 服务DTO对象[Account_bank_statement_lineDTO]
 */
public class Account_bank_statement_lineDTO implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [PARTNER_NAME]
     *
     */
    @Account_bank_statement_linePartner_nameDefault(info = "默认规则")
    private String partner_name;

    @JsonIgnore
    private boolean partner_nameDirtyFlag;

    /**
     * 属性 [SEQUENCE]
     *
     */
    @Account_bank_statement_lineSequenceDefault(info = "默认规则")
    private Integer sequence;

    @JsonIgnore
    private boolean sequenceDirtyFlag;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @Account_bank_statement_lineDisplay_nameDefault(info = "默认规则")
    private String display_name;

    @JsonIgnore
    private boolean display_nameDirtyFlag;

    /**
     * 属性 [ID]
     *
     */
    @Account_bank_statement_lineIdDefault(info = "默认规则")
    private Integer id;

    @JsonIgnore
    private boolean idDirtyFlag;

    /**
     * 属性 [AMOUNT]
     *
     */
    @Account_bank_statement_lineAmountDefault(info = "默认规则")
    private Double amount;

    @JsonIgnore
    private boolean amountDirtyFlag;

    /**
     * 属性 [ACCOUNT_NUMBER]
     *
     */
    @Account_bank_statement_lineAccount_numberDefault(info = "默认规则")
    private String account_number;

    @JsonIgnore
    private boolean account_numberDirtyFlag;

    /**
     * 属性 [REF]
     *
     */
    @Account_bank_statement_lineRefDefault(info = "默认规则")
    private String ref;

    @JsonIgnore
    private boolean refDirtyFlag;

    /**
     * 属性 [NOTE]
     *
     */
    @Account_bank_statement_lineNoteDefault(info = "默认规则")
    private String note;

    @JsonIgnore
    private boolean noteDirtyFlag;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @Account_bank_statement_line__last_updateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp __last_update;

    @JsonIgnore
    private boolean __last_updateDirtyFlag;

    /**
     * 属性 [JOURNAL_ENTRY_IDS]
     *
     */
    @Account_bank_statement_lineJournal_entry_idsDefault(info = "默认规则")
    private String journal_entry_ids;

    @JsonIgnore
    private boolean journal_entry_idsDirtyFlag;

    /**
     * 属性 [NAME]
     *
     */
    @Account_bank_statement_lineNameDefault(info = "默认规则")
    private String name;

    @JsonIgnore
    private boolean nameDirtyFlag;

    /**
     * 属性 [MOVE_NAME]
     *
     */
    @Account_bank_statement_lineMove_nameDefault(info = "默认规则")
    private String move_name;

    @JsonIgnore
    private boolean move_nameDirtyFlag;

    /**
     * 属性 [UNIQUE_IMPORT_ID]
     *
     */
    @Account_bank_statement_lineUnique_import_idDefault(info = "默认规则")
    private String unique_import_id;

    @JsonIgnore
    private boolean unique_import_idDirtyFlag;

    /**
     * 属性 [WRITE_DATE]
     *
     */
    @Account_bank_statement_lineWrite_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp write_date;

    @JsonIgnore
    private boolean write_dateDirtyFlag;

    /**
     * 属性 [DATE]
     *
     */
    @Account_bank_statement_lineDateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    private Timestamp date;

    @JsonIgnore
    private boolean dateDirtyFlag;

    /**
     * 属性 [POS_STATEMENT_ID]
     *
     */
    @Account_bank_statement_linePos_statement_idDefault(info = "默认规则")
    private Integer pos_statement_id;

    @JsonIgnore
    private boolean pos_statement_idDirtyFlag;

    /**
     * 属性 [CREATE_DATE]
     *
     */
    @Account_bank_statement_lineCreate_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp create_date;

    @JsonIgnore
    private boolean create_dateDirtyFlag;

    /**
     * 属性 [AMOUNT_CURRENCY]
     *
     */
    @Account_bank_statement_lineAmount_currencyDefault(info = "默认规则")
    private Double amount_currency;

    @JsonIgnore
    private boolean amount_currencyDirtyFlag;

    /**
     * 属性 [COMPANY_ID_TEXT]
     *
     */
    @Account_bank_statement_lineCompany_id_textDefault(info = "默认规则")
    private String company_id_text;

    @JsonIgnore
    private boolean company_id_textDirtyFlag;

    /**
     * 属性 [CREATE_UID_TEXT]
     *
     */
    @Account_bank_statement_lineCreate_uid_textDefault(info = "默认规则")
    private String create_uid_text;

    @JsonIgnore
    private boolean create_uid_textDirtyFlag;

    /**
     * 属性 [JOURNAL_ID_TEXT]
     *
     */
    @Account_bank_statement_lineJournal_id_textDefault(info = "默认规则")
    private String journal_id_text;

    @JsonIgnore
    private boolean journal_id_textDirtyFlag;

    /**
     * 属性 [STATE]
     *
     */
    @Account_bank_statement_lineStateDefault(info = "默认规则")
    private String state;

    @JsonIgnore
    private boolean stateDirtyFlag;

    /**
     * 属性 [PARTNER_ID_TEXT]
     *
     */
    @Account_bank_statement_linePartner_id_textDefault(info = "默认规则")
    private String partner_id_text;

    @JsonIgnore
    private boolean partner_id_textDirtyFlag;

    /**
     * 属性 [WRITE_UID_TEXT]
     *
     */
    @Account_bank_statement_lineWrite_uid_textDefault(info = "默认规则")
    private String write_uid_text;

    @JsonIgnore
    private boolean write_uid_textDirtyFlag;

    /**
     * 属性 [JOURNAL_CURRENCY_ID]
     *
     */
    @Account_bank_statement_lineJournal_currency_idDefault(info = "默认规则")
    private Integer journal_currency_id;

    @JsonIgnore
    private boolean journal_currency_idDirtyFlag;

    /**
     * 属性 [CURRENCY_ID_TEXT]
     *
     */
    @Account_bank_statement_lineCurrency_id_textDefault(info = "默认规则")
    private String currency_id_text;

    @JsonIgnore
    private boolean currency_id_textDirtyFlag;

    /**
     * 属性 [STATEMENT_ID_TEXT]
     *
     */
    @Account_bank_statement_lineStatement_id_textDefault(info = "默认规则")
    private String statement_id_text;

    @JsonIgnore
    private boolean statement_id_textDirtyFlag;

    /**
     * 属性 [ACCOUNT_ID_TEXT]
     *
     */
    @Account_bank_statement_lineAccount_id_textDefault(info = "默认规则")
    private String account_id_text;

    @JsonIgnore
    private boolean account_id_textDirtyFlag;

    /**
     * 属性 [CREATE_UID]
     *
     */
    @Account_bank_statement_lineCreate_uidDefault(info = "默认规则")
    private Integer create_uid;

    @JsonIgnore
    private boolean create_uidDirtyFlag;

    /**
     * 属性 [ACCOUNT_ID]
     *
     */
    @Account_bank_statement_lineAccount_idDefault(info = "默认规则")
    private Integer account_id;

    @JsonIgnore
    private boolean account_idDirtyFlag;

    /**
     * 属性 [COMPANY_ID]
     *
     */
    @Account_bank_statement_lineCompany_idDefault(info = "默认规则")
    private Integer company_id;

    @JsonIgnore
    private boolean company_idDirtyFlag;

    /**
     * 属性 [CURRENCY_ID]
     *
     */
    @Account_bank_statement_lineCurrency_idDefault(info = "默认规则")
    private Integer currency_id;

    @JsonIgnore
    private boolean currency_idDirtyFlag;

    /**
     * 属性 [PARTNER_ID]
     *
     */
    @Account_bank_statement_linePartner_idDefault(info = "默认规则")
    private Integer partner_id;

    @JsonIgnore
    private boolean partner_idDirtyFlag;

    /**
     * 属性 [BANK_ACCOUNT_ID]
     *
     */
    @Account_bank_statement_lineBank_account_idDefault(info = "默认规则")
    private Integer bank_account_id;

    @JsonIgnore
    private boolean bank_account_idDirtyFlag;

    /**
     * 属性 [WRITE_UID]
     *
     */
    @Account_bank_statement_lineWrite_uidDefault(info = "默认规则")
    private Integer write_uid;

    @JsonIgnore
    private boolean write_uidDirtyFlag;

    /**
     * 属性 [JOURNAL_ID]
     *
     */
    @Account_bank_statement_lineJournal_idDefault(info = "默认规则")
    private Integer journal_id;

    @JsonIgnore
    private boolean journal_idDirtyFlag;

    /**
     * 属性 [STATEMENT_ID]
     *
     */
    @Account_bank_statement_lineStatement_idDefault(info = "默认规则")
    private Integer statement_id;

    @JsonIgnore
    private boolean statement_idDirtyFlag;


    /**
     * 获取 [PARTNER_NAME]
     */
    @JsonProperty("partner_name")
    public String getPartner_name(){
        return partner_name ;
    }

    /**
     * 设置 [PARTNER_NAME]
     */
    @JsonProperty("partner_name")
    public void setPartner_name(String  partner_name){
        this.partner_name = partner_name ;
        this.partner_nameDirtyFlag = true ;
    }

    /**
     * 获取 [PARTNER_NAME]脏标记
     */
    @JsonIgnore
    public boolean getPartner_nameDirtyFlag(){
        return partner_nameDirtyFlag ;
    }

    /**
     * 获取 [SEQUENCE]
     */
    @JsonProperty("sequence")
    public Integer getSequence(){
        return sequence ;
    }

    /**
     * 设置 [SEQUENCE]
     */
    @JsonProperty("sequence")
    public void setSequence(Integer  sequence){
        this.sequence = sequence ;
        this.sequenceDirtyFlag = true ;
    }

    /**
     * 获取 [SEQUENCE]脏标记
     */
    @JsonIgnore
    public boolean getSequenceDirtyFlag(){
        return sequenceDirtyFlag ;
    }

    /**
     * 获取 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return display_name ;
    }

    /**
     * 设置 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

    /**
     * 获取 [DISPLAY_NAME]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return display_nameDirtyFlag ;
    }

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

    /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return idDirtyFlag ;
    }

    /**
     * 获取 [AMOUNT]
     */
    @JsonProperty("amount")
    public Double getAmount(){
        return amount ;
    }

    /**
     * 设置 [AMOUNT]
     */
    @JsonProperty("amount")
    public void setAmount(Double  amount){
        this.amount = amount ;
        this.amountDirtyFlag = true ;
    }

    /**
     * 获取 [AMOUNT]脏标记
     */
    @JsonIgnore
    public boolean getAmountDirtyFlag(){
        return amountDirtyFlag ;
    }

    /**
     * 获取 [ACCOUNT_NUMBER]
     */
    @JsonProperty("account_number")
    public String getAccount_number(){
        return account_number ;
    }

    /**
     * 设置 [ACCOUNT_NUMBER]
     */
    @JsonProperty("account_number")
    public void setAccount_number(String  account_number){
        this.account_number = account_number ;
        this.account_numberDirtyFlag = true ;
    }

    /**
     * 获取 [ACCOUNT_NUMBER]脏标记
     */
    @JsonIgnore
    public boolean getAccount_numberDirtyFlag(){
        return account_numberDirtyFlag ;
    }

    /**
     * 获取 [REF]
     */
    @JsonProperty("ref")
    public String getRef(){
        return ref ;
    }

    /**
     * 设置 [REF]
     */
    @JsonProperty("ref")
    public void setRef(String  ref){
        this.ref = ref ;
        this.refDirtyFlag = true ;
    }

    /**
     * 获取 [REF]脏标记
     */
    @JsonIgnore
    public boolean getRefDirtyFlag(){
        return refDirtyFlag ;
    }

    /**
     * 获取 [NOTE]
     */
    @JsonProperty("note")
    public String getNote(){
        return note ;
    }

    /**
     * 设置 [NOTE]
     */
    @JsonProperty("note")
    public void setNote(String  note){
        this.note = note ;
        this.noteDirtyFlag = true ;
    }

    /**
     * 获取 [NOTE]脏标记
     */
    @JsonIgnore
    public boolean getNoteDirtyFlag(){
        return noteDirtyFlag ;
    }

    /**
     * 获取 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return __last_update ;
    }

    /**
     * 设置 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

    /**
     * 获取 [__LAST_UPDATE]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return __last_updateDirtyFlag ;
    }

    /**
     * 获取 [JOURNAL_ENTRY_IDS]
     */
    @JsonProperty("journal_entry_ids")
    public String getJournal_entry_ids(){
        return journal_entry_ids ;
    }

    /**
     * 设置 [JOURNAL_ENTRY_IDS]
     */
    @JsonProperty("journal_entry_ids")
    public void setJournal_entry_ids(String  journal_entry_ids){
        this.journal_entry_ids = journal_entry_ids ;
        this.journal_entry_idsDirtyFlag = true ;
    }

    /**
     * 获取 [JOURNAL_ENTRY_IDS]脏标记
     */
    @JsonIgnore
    public boolean getJournal_entry_idsDirtyFlag(){
        return journal_entry_idsDirtyFlag ;
    }

    /**
     * 获取 [NAME]
     */
    @JsonProperty("name")
    public String getName(){
        return name ;
    }

    /**
     * 设置 [NAME]
     */
    @JsonProperty("name")
    public void setName(String  name){
        this.name = name ;
        this.nameDirtyFlag = true ;
    }

    /**
     * 获取 [NAME]脏标记
     */
    @JsonIgnore
    public boolean getNameDirtyFlag(){
        return nameDirtyFlag ;
    }

    /**
     * 获取 [MOVE_NAME]
     */
    @JsonProperty("move_name")
    public String getMove_name(){
        return move_name ;
    }

    /**
     * 设置 [MOVE_NAME]
     */
    @JsonProperty("move_name")
    public void setMove_name(String  move_name){
        this.move_name = move_name ;
        this.move_nameDirtyFlag = true ;
    }

    /**
     * 获取 [MOVE_NAME]脏标记
     */
    @JsonIgnore
    public boolean getMove_nameDirtyFlag(){
        return move_nameDirtyFlag ;
    }

    /**
     * 获取 [UNIQUE_IMPORT_ID]
     */
    @JsonProperty("unique_import_id")
    public String getUnique_import_id(){
        return unique_import_id ;
    }

    /**
     * 设置 [UNIQUE_IMPORT_ID]
     */
    @JsonProperty("unique_import_id")
    public void setUnique_import_id(String  unique_import_id){
        this.unique_import_id = unique_import_id ;
        this.unique_import_idDirtyFlag = true ;
    }

    /**
     * 获取 [UNIQUE_IMPORT_ID]脏标记
     */
    @JsonIgnore
    public boolean getUnique_import_idDirtyFlag(){
        return unique_import_idDirtyFlag ;
    }

    /**
     * 获取 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return write_date ;
    }

    /**
     * 设置 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return write_dateDirtyFlag ;
    }

    /**
     * 获取 [DATE]
     */
    @JsonProperty("date")
    public Timestamp getDate(){
        return date ;
    }

    /**
     * 设置 [DATE]
     */
    @JsonProperty("date")
    public void setDate(Timestamp  date){
        this.date = date ;
        this.dateDirtyFlag = true ;
    }

    /**
     * 获取 [DATE]脏标记
     */
    @JsonIgnore
    public boolean getDateDirtyFlag(){
        return dateDirtyFlag ;
    }

    /**
     * 获取 [POS_STATEMENT_ID]
     */
    @JsonProperty("pos_statement_id")
    public Integer getPos_statement_id(){
        return pos_statement_id ;
    }

    /**
     * 设置 [POS_STATEMENT_ID]
     */
    @JsonProperty("pos_statement_id")
    public void setPos_statement_id(Integer  pos_statement_id){
        this.pos_statement_id = pos_statement_id ;
        this.pos_statement_idDirtyFlag = true ;
    }

    /**
     * 获取 [POS_STATEMENT_ID]脏标记
     */
    @JsonIgnore
    public boolean getPos_statement_idDirtyFlag(){
        return pos_statement_idDirtyFlag ;
    }

    /**
     * 获取 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return create_date ;
    }

    /**
     * 设置 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return create_dateDirtyFlag ;
    }

    /**
     * 获取 [AMOUNT_CURRENCY]
     */
    @JsonProperty("amount_currency")
    public Double getAmount_currency(){
        return amount_currency ;
    }

    /**
     * 设置 [AMOUNT_CURRENCY]
     */
    @JsonProperty("amount_currency")
    public void setAmount_currency(Double  amount_currency){
        this.amount_currency = amount_currency ;
        this.amount_currencyDirtyFlag = true ;
    }

    /**
     * 获取 [AMOUNT_CURRENCY]脏标记
     */
    @JsonIgnore
    public boolean getAmount_currencyDirtyFlag(){
        return amount_currencyDirtyFlag ;
    }

    /**
     * 获取 [COMPANY_ID_TEXT]
     */
    @JsonProperty("company_id_text")
    public String getCompany_id_text(){
        return company_id_text ;
    }

    /**
     * 设置 [COMPANY_ID_TEXT]
     */
    @JsonProperty("company_id_text")
    public void setCompany_id_text(String  company_id_text){
        this.company_id_text = company_id_text ;
        this.company_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [COMPANY_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getCompany_id_textDirtyFlag(){
        return company_id_textDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return create_uid_text ;
    }

    /**
     * 设置 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return create_uid_textDirtyFlag ;
    }

    /**
     * 获取 [JOURNAL_ID_TEXT]
     */
    @JsonProperty("journal_id_text")
    public String getJournal_id_text(){
        return journal_id_text ;
    }

    /**
     * 设置 [JOURNAL_ID_TEXT]
     */
    @JsonProperty("journal_id_text")
    public void setJournal_id_text(String  journal_id_text){
        this.journal_id_text = journal_id_text ;
        this.journal_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [JOURNAL_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getJournal_id_textDirtyFlag(){
        return journal_id_textDirtyFlag ;
    }

    /**
     * 获取 [STATE]
     */
    @JsonProperty("state")
    public String getState(){
        return state ;
    }

    /**
     * 设置 [STATE]
     */
    @JsonProperty("state")
    public void setState(String  state){
        this.state = state ;
        this.stateDirtyFlag = true ;
    }

    /**
     * 获取 [STATE]脏标记
     */
    @JsonIgnore
    public boolean getStateDirtyFlag(){
        return stateDirtyFlag ;
    }

    /**
     * 获取 [PARTNER_ID_TEXT]
     */
    @JsonProperty("partner_id_text")
    public String getPartner_id_text(){
        return partner_id_text ;
    }

    /**
     * 设置 [PARTNER_ID_TEXT]
     */
    @JsonProperty("partner_id_text")
    public void setPartner_id_text(String  partner_id_text){
        this.partner_id_text = partner_id_text ;
        this.partner_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [PARTNER_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getPartner_id_textDirtyFlag(){
        return partner_id_textDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return write_uid_text ;
    }

    /**
     * 设置 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return write_uid_textDirtyFlag ;
    }

    /**
     * 获取 [JOURNAL_CURRENCY_ID]
     */
    @JsonProperty("journal_currency_id")
    public Integer getJournal_currency_id(){
        return journal_currency_id ;
    }

    /**
     * 设置 [JOURNAL_CURRENCY_ID]
     */
    @JsonProperty("journal_currency_id")
    public void setJournal_currency_id(Integer  journal_currency_id){
        this.journal_currency_id = journal_currency_id ;
        this.journal_currency_idDirtyFlag = true ;
    }

    /**
     * 获取 [JOURNAL_CURRENCY_ID]脏标记
     */
    @JsonIgnore
    public boolean getJournal_currency_idDirtyFlag(){
        return journal_currency_idDirtyFlag ;
    }

    /**
     * 获取 [CURRENCY_ID_TEXT]
     */
    @JsonProperty("currency_id_text")
    public String getCurrency_id_text(){
        return currency_id_text ;
    }

    /**
     * 设置 [CURRENCY_ID_TEXT]
     */
    @JsonProperty("currency_id_text")
    public void setCurrency_id_text(String  currency_id_text){
        this.currency_id_text = currency_id_text ;
        this.currency_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [CURRENCY_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getCurrency_id_textDirtyFlag(){
        return currency_id_textDirtyFlag ;
    }

    /**
     * 获取 [STATEMENT_ID_TEXT]
     */
    @JsonProperty("statement_id_text")
    public String getStatement_id_text(){
        return statement_id_text ;
    }

    /**
     * 设置 [STATEMENT_ID_TEXT]
     */
    @JsonProperty("statement_id_text")
    public void setStatement_id_text(String  statement_id_text){
        this.statement_id_text = statement_id_text ;
        this.statement_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [STATEMENT_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getStatement_id_textDirtyFlag(){
        return statement_id_textDirtyFlag ;
    }

    /**
     * 获取 [ACCOUNT_ID_TEXT]
     */
    @JsonProperty("account_id_text")
    public String getAccount_id_text(){
        return account_id_text ;
    }

    /**
     * 设置 [ACCOUNT_ID_TEXT]
     */
    @JsonProperty("account_id_text")
    public void setAccount_id_text(String  account_id_text){
        this.account_id_text = account_id_text ;
        this.account_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [ACCOUNT_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getAccount_id_textDirtyFlag(){
        return account_id_textDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return create_uid ;
    }

    /**
     * 设置 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return create_uidDirtyFlag ;
    }

    /**
     * 获取 [ACCOUNT_ID]
     */
    @JsonProperty("account_id")
    public Integer getAccount_id(){
        return account_id ;
    }

    /**
     * 设置 [ACCOUNT_ID]
     */
    @JsonProperty("account_id")
    public void setAccount_id(Integer  account_id){
        this.account_id = account_id ;
        this.account_idDirtyFlag = true ;
    }

    /**
     * 获取 [ACCOUNT_ID]脏标记
     */
    @JsonIgnore
    public boolean getAccount_idDirtyFlag(){
        return account_idDirtyFlag ;
    }

    /**
     * 获取 [COMPANY_ID]
     */
    @JsonProperty("company_id")
    public Integer getCompany_id(){
        return company_id ;
    }

    /**
     * 设置 [COMPANY_ID]
     */
    @JsonProperty("company_id")
    public void setCompany_id(Integer  company_id){
        this.company_id = company_id ;
        this.company_idDirtyFlag = true ;
    }

    /**
     * 获取 [COMPANY_ID]脏标记
     */
    @JsonIgnore
    public boolean getCompany_idDirtyFlag(){
        return company_idDirtyFlag ;
    }

    /**
     * 获取 [CURRENCY_ID]
     */
    @JsonProperty("currency_id")
    public Integer getCurrency_id(){
        return currency_id ;
    }

    /**
     * 设置 [CURRENCY_ID]
     */
    @JsonProperty("currency_id")
    public void setCurrency_id(Integer  currency_id){
        this.currency_id = currency_id ;
        this.currency_idDirtyFlag = true ;
    }

    /**
     * 获取 [CURRENCY_ID]脏标记
     */
    @JsonIgnore
    public boolean getCurrency_idDirtyFlag(){
        return currency_idDirtyFlag ;
    }

    /**
     * 获取 [PARTNER_ID]
     */
    @JsonProperty("partner_id")
    public Integer getPartner_id(){
        return partner_id ;
    }

    /**
     * 设置 [PARTNER_ID]
     */
    @JsonProperty("partner_id")
    public void setPartner_id(Integer  partner_id){
        this.partner_id = partner_id ;
        this.partner_idDirtyFlag = true ;
    }

    /**
     * 获取 [PARTNER_ID]脏标记
     */
    @JsonIgnore
    public boolean getPartner_idDirtyFlag(){
        return partner_idDirtyFlag ;
    }

    /**
     * 获取 [BANK_ACCOUNT_ID]
     */
    @JsonProperty("bank_account_id")
    public Integer getBank_account_id(){
        return bank_account_id ;
    }

    /**
     * 设置 [BANK_ACCOUNT_ID]
     */
    @JsonProperty("bank_account_id")
    public void setBank_account_id(Integer  bank_account_id){
        this.bank_account_id = bank_account_id ;
        this.bank_account_idDirtyFlag = true ;
    }

    /**
     * 获取 [BANK_ACCOUNT_ID]脏标记
     */
    @JsonIgnore
    public boolean getBank_account_idDirtyFlag(){
        return bank_account_idDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return write_uid ;
    }

    /**
     * 设置 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return write_uidDirtyFlag ;
    }

    /**
     * 获取 [JOURNAL_ID]
     */
    @JsonProperty("journal_id")
    public Integer getJournal_id(){
        return journal_id ;
    }

    /**
     * 设置 [JOURNAL_ID]
     */
    @JsonProperty("journal_id")
    public void setJournal_id(Integer  journal_id){
        this.journal_id = journal_id ;
        this.journal_idDirtyFlag = true ;
    }

    /**
     * 获取 [JOURNAL_ID]脏标记
     */
    @JsonIgnore
    public boolean getJournal_idDirtyFlag(){
        return journal_idDirtyFlag ;
    }

    /**
     * 获取 [STATEMENT_ID]
     */
    @JsonProperty("statement_id")
    public Integer getStatement_id(){
        return statement_id ;
    }

    /**
     * 设置 [STATEMENT_ID]
     */
    @JsonProperty("statement_id")
    public void setStatement_id(Integer  statement_id){
        this.statement_id = statement_id ;
        this.statement_idDirtyFlag = true ;
    }

    /**
     * 获取 [STATEMENT_ID]脏标记
     */
    @JsonIgnore
    public boolean getStatement_idDirtyFlag(){
        return statement_idDirtyFlag ;
    }



    public Account_bank_statement_line toDO() {
        Account_bank_statement_line srfdomain = new Account_bank_statement_line();
        if(getPartner_nameDirtyFlag())
            srfdomain.setPartner_name(partner_name);
        if(getSequenceDirtyFlag())
            srfdomain.setSequence(sequence);
        if(getDisplay_nameDirtyFlag())
            srfdomain.setDisplay_name(display_name);
        if(getIdDirtyFlag())
            srfdomain.setId(id);
        if(getAmountDirtyFlag())
            srfdomain.setAmount(amount);
        if(getAccount_numberDirtyFlag())
            srfdomain.setAccount_number(account_number);
        if(getRefDirtyFlag())
            srfdomain.setRef(ref);
        if(getNoteDirtyFlag())
            srfdomain.setNote(note);
        if(get__last_updateDirtyFlag())
            srfdomain.set__last_update(__last_update);
        if(getJournal_entry_idsDirtyFlag())
            srfdomain.setJournal_entry_ids(journal_entry_ids);
        if(getNameDirtyFlag())
            srfdomain.setName(name);
        if(getMove_nameDirtyFlag())
            srfdomain.setMove_name(move_name);
        if(getUnique_import_idDirtyFlag())
            srfdomain.setUnique_import_id(unique_import_id);
        if(getWrite_dateDirtyFlag())
            srfdomain.setWrite_date(write_date);
        if(getDateDirtyFlag())
            srfdomain.setDate(date);
        if(getPos_statement_idDirtyFlag())
            srfdomain.setPos_statement_id(pos_statement_id);
        if(getCreate_dateDirtyFlag())
            srfdomain.setCreate_date(create_date);
        if(getAmount_currencyDirtyFlag())
            srfdomain.setAmount_currency(amount_currency);
        if(getCompany_id_textDirtyFlag())
            srfdomain.setCompany_id_text(company_id_text);
        if(getCreate_uid_textDirtyFlag())
            srfdomain.setCreate_uid_text(create_uid_text);
        if(getJournal_id_textDirtyFlag())
            srfdomain.setJournal_id_text(journal_id_text);
        if(getStateDirtyFlag())
            srfdomain.setState(state);
        if(getPartner_id_textDirtyFlag())
            srfdomain.setPartner_id_text(partner_id_text);
        if(getWrite_uid_textDirtyFlag())
            srfdomain.setWrite_uid_text(write_uid_text);
        if(getJournal_currency_idDirtyFlag())
            srfdomain.setJournal_currency_id(journal_currency_id);
        if(getCurrency_id_textDirtyFlag())
            srfdomain.setCurrency_id_text(currency_id_text);
        if(getStatement_id_textDirtyFlag())
            srfdomain.setStatement_id_text(statement_id_text);
        if(getAccount_id_textDirtyFlag())
            srfdomain.setAccount_id_text(account_id_text);
        if(getCreate_uidDirtyFlag())
            srfdomain.setCreate_uid(create_uid);
        if(getAccount_idDirtyFlag())
            srfdomain.setAccount_id(account_id);
        if(getCompany_idDirtyFlag())
            srfdomain.setCompany_id(company_id);
        if(getCurrency_idDirtyFlag())
            srfdomain.setCurrency_id(currency_id);
        if(getPartner_idDirtyFlag())
            srfdomain.setPartner_id(partner_id);
        if(getBank_account_idDirtyFlag())
            srfdomain.setBank_account_id(bank_account_id);
        if(getWrite_uidDirtyFlag())
            srfdomain.setWrite_uid(write_uid);
        if(getJournal_idDirtyFlag())
            srfdomain.setJournal_id(journal_id);
        if(getStatement_idDirtyFlag())
            srfdomain.setStatement_id(statement_id);

        return srfdomain;
    }

    public void fromDO(Account_bank_statement_line srfdomain) {
        if(srfdomain == null )
          return ;
        if(srfdomain.getPartner_nameDirtyFlag())
            this.setPartner_name(srfdomain.getPartner_name());
        if(srfdomain.getSequenceDirtyFlag())
            this.setSequence(srfdomain.getSequence());
        if(srfdomain.getDisplay_nameDirtyFlag())
            this.setDisplay_name(srfdomain.getDisplay_name());
        if(srfdomain.getIdDirtyFlag())
            this.setId(srfdomain.getId());
        if(srfdomain.getAmountDirtyFlag())
            this.setAmount(srfdomain.getAmount());
        if(srfdomain.getAccount_numberDirtyFlag())
            this.setAccount_number(srfdomain.getAccount_number());
        if(srfdomain.getRefDirtyFlag())
            this.setRef(srfdomain.getRef());
        if(srfdomain.getNoteDirtyFlag())
            this.setNote(srfdomain.getNote());
        if(srfdomain.get__last_updateDirtyFlag())
            this.set__last_update(srfdomain.get__last_update());
        if(srfdomain.getJournal_entry_idsDirtyFlag())
            this.setJournal_entry_ids(srfdomain.getJournal_entry_ids());
        if(srfdomain.getNameDirtyFlag())
            this.setName(srfdomain.getName());
        if(srfdomain.getMove_nameDirtyFlag())
            this.setMove_name(srfdomain.getMove_name());
        if(srfdomain.getUnique_import_idDirtyFlag())
            this.setUnique_import_id(srfdomain.getUnique_import_id());
        if(srfdomain.getWrite_dateDirtyFlag())
            this.setWrite_date(srfdomain.getWrite_date());
        if(srfdomain.getDateDirtyFlag())
            this.setDate(srfdomain.getDate());
        if(srfdomain.getPos_statement_idDirtyFlag())
            this.setPos_statement_id(srfdomain.getPos_statement_id());
        if(srfdomain.getCreate_dateDirtyFlag())
            this.setCreate_date(srfdomain.getCreate_date());
        if(srfdomain.getAmount_currencyDirtyFlag())
            this.setAmount_currency(srfdomain.getAmount_currency());
        if(srfdomain.getCompany_id_textDirtyFlag())
            this.setCompany_id_text(srfdomain.getCompany_id_text());
        if(srfdomain.getCreate_uid_textDirtyFlag())
            this.setCreate_uid_text(srfdomain.getCreate_uid_text());
        if(srfdomain.getJournal_id_textDirtyFlag())
            this.setJournal_id_text(srfdomain.getJournal_id_text());
        if(srfdomain.getStateDirtyFlag())
            this.setState(srfdomain.getState());
        if(srfdomain.getPartner_id_textDirtyFlag())
            this.setPartner_id_text(srfdomain.getPartner_id_text());
        if(srfdomain.getWrite_uid_textDirtyFlag())
            this.setWrite_uid_text(srfdomain.getWrite_uid_text());
        if(srfdomain.getJournal_currency_idDirtyFlag())
            this.setJournal_currency_id(srfdomain.getJournal_currency_id());
        if(srfdomain.getCurrency_id_textDirtyFlag())
            this.setCurrency_id_text(srfdomain.getCurrency_id_text());
        if(srfdomain.getStatement_id_textDirtyFlag())
            this.setStatement_id_text(srfdomain.getStatement_id_text());
        if(srfdomain.getAccount_id_textDirtyFlag())
            this.setAccount_id_text(srfdomain.getAccount_id_text());
        if(srfdomain.getCreate_uidDirtyFlag())
            this.setCreate_uid(srfdomain.getCreate_uid());
        if(srfdomain.getAccount_idDirtyFlag())
            this.setAccount_id(srfdomain.getAccount_id());
        if(srfdomain.getCompany_idDirtyFlag())
            this.setCompany_id(srfdomain.getCompany_id());
        if(srfdomain.getCurrency_idDirtyFlag())
            this.setCurrency_id(srfdomain.getCurrency_id());
        if(srfdomain.getPartner_idDirtyFlag())
            this.setPartner_id(srfdomain.getPartner_id());
        if(srfdomain.getBank_account_idDirtyFlag())
            this.setBank_account_id(srfdomain.getBank_account_id());
        if(srfdomain.getWrite_uidDirtyFlag())
            this.setWrite_uid(srfdomain.getWrite_uid());
        if(srfdomain.getJournal_idDirtyFlag())
            this.setJournal_id(srfdomain.getJournal_id());
        if(srfdomain.getStatement_idDirtyFlag())
            this.setStatement_id(srfdomain.getStatement_id());

    }

    public List<Account_bank_statement_lineDTO> fromDOPage(List<Account_bank_statement_line> poPage)   {
        if(poPage == null)
            return null;
        List<Account_bank_statement_lineDTO> dtos=new ArrayList<Account_bank_statement_lineDTO>();
        for(Account_bank_statement_line domain : poPage) {
            Account_bank_statement_lineDTO dto = new Account_bank_statement_lineDTO();
            dto.fromDO(domain);
            dtos.add(dto);
        }
        return dtos;
    }
}

