package cn.ibizlab.odoo.service.odoo_account.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.core.odoo_account.valuerule.anno.account_common_report.*;
import cn.ibizlab.odoo.core.odoo_account.domain.Account_common_report;
import org.springframework.cglib.beans.BeanCopier;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 服务DTO对象[Account_common_reportDTO]
 */
public class Account_common_reportDTO implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [DATE_TO]
     *
     */
    @Account_common_reportDate_toDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    private Timestamp date_to;

    @JsonIgnore
    private boolean date_toDirtyFlag;

    /**
     * 属性 [CREATE_DATE]
     *
     */
    @Account_common_reportCreate_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp create_date;

    @JsonIgnore
    private boolean create_dateDirtyFlag;

    /**
     * 属性 [ID]
     *
     */
    @Account_common_reportIdDefault(info = "默认规则")
    private Integer id;

    @JsonIgnore
    private boolean idDirtyFlag;

    /**
     * 属性 [DATE_FROM]
     *
     */
    @Account_common_reportDate_fromDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    private Timestamp date_from;

    @JsonIgnore
    private boolean date_fromDirtyFlag;

    /**
     * 属性 [WRITE_DATE]
     *
     */
    @Account_common_reportWrite_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp write_date;

    @JsonIgnore
    private boolean write_dateDirtyFlag;

    /**
     * 属性 [TARGET_MOVE]
     *
     */
    @Account_common_reportTarget_moveDefault(info = "默认规则")
    private String target_move;

    @JsonIgnore
    private boolean target_moveDirtyFlag;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @Account_common_report__last_updateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp __last_update;

    @JsonIgnore
    private boolean __last_updateDirtyFlag;

    /**
     * 属性 [JOURNAL_IDS]
     *
     */
    @Account_common_reportJournal_idsDefault(info = "默认规则")
    private String journal_ids;

    @JsonIgnore
    private boolean journal_idsDirtyFlag;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @Account_common_reportDisplay_nameDefault(info = "默认规则")
    private String display_name;

    @JsonIgnore
    private boolean display_nameDirtyFlag;

    /**
     * 属性 [WRITE_UID_TEXT]
     *
     */
    @Account_common_reportWrite_uid_textDefault(info = "默认规则")
    private String write_uid_text;

    @JsonIgnore
    private boolean write_uid_textDirtyFlag;

    /**
     * 属性 [COMPANY_ID_TEXT]
     *
     */
    @Account_common_reportCompany_id_textDefault(info = "默认规则")
    private String company_id_text;

    @JsonIgnore
    private boolean company_id_textDirtyFlag;

    /**
     * 属性 [CREATE_UID_TEXT]
     *
     */
    @Account_common_reportCreate_uid_textDefault(info = "默认规则")
    private String create_uid_text;

    @JsonIgnore
    private boolean create_uid_textDirtyFlag;

    /**
     * 属性 [COMPANY_ID]
     *
     */
    @Account_common_reportCompany_idDefault(info = "默认规则")
    private Integer company_id;

    @JsonIgnore
    private boolean company_idDirtyFlag;

    /**
     * 属性 [WRITE_UID]
     *
     */
    @Account_common_reportWrite_uidDefault(info = "默认规则")
    private Integer write_uid;

    @JsonIgnore
    private boolean write_uidDirtyFlag;

    /**
     * 属性 [CREATE_UID]
     *
     */
    @Account_common_reportCreate_uidDefault(info = "默认规则")
    private Integer create_uid;

    @JsonIgnore
    private boolean create_uidDirtyFlag;


    /**
     * 获取 [DATE_TO]
     */
    @JsonProperty("date_to")
    public Timestamp getDate_to(){
        return date_to ;
    }

    /**
     * 设置 [DATE_TO]
     */
    @JsonProperty("date_to")
    public void setDate_to(Timestamp  date_to){
        this.date_to = date_to ;
        this.date_toDirtyFlag = true ;
    }

    /**
     * 获取 [DATE_TO]脏标记
     */
    @JsonIgnore
    public boolean getDate_toDirtyFlag(){
        return date_toDirtyFlag ;
    }

    /**
     * 获取 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return create_date ;
    }

    /**
     * 设置 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return create_dateDirtyFlag ;
    }

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

    /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return idDirtyFlag ;
    }

    /**
     * 获取 [DATE_FROM]
     */
    @JsonProperty("date_from")
    public Timestamp getDate_from(){
        return date_from ;
    }

    /**
     * 设置 [DATE_FROM]
     */
    @JsonProperty("date_from")
    public void setDate_from(Timestamp  date_from){
        this.date_from = date_from ;
        this.date_fromDirtyFlag = true ;
    }

    /**
     * 获取 [DATE_FROM]脏标记
     */
    @JsonIgnore
    public boolean getDate_fromDirtyFlag(){
        return date_fromDirtyFlag ;
    }

    /**
     * 获取 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return write_date ;
    }

    /**
     * 设置 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return write_dateDirtyFlag ;
    }

    /**
     * 获取 [TARGET_MOVE]
     */
    @JsonProperty("target_move")
    public String getTarget_move(){
        return target_move ;
    }

    /**
     * 设置 [TARGET_MOVE]
     */
    @JsonProperty("target_move")
    public void setTarget_move(String  target_move){
        this.target_move = target_move ;
        this.target_moveDirtyFlag = true ;
    }

    /**
     * 获取 [TARGET_MOVE]脏标记
     */
    @JsonIgnore
    public boolean getTarget_moveDirtyFlag(){
        return target_moveDirtyFlag ;
    }

    /**
     * 获取 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return __last_update ;
    }

    /**
     * 设置 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

    /**
     * 获取 [__LAST_UPDATE]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return __last_updateDirtyFlag ;
    }

    /**
     * 获取 [JOURNAL_IDS]
     */
    @JsonProperty("journal_ids")
    public String getJournal_ids(){
        return journal_ids ;
    }

    /**
     * 设置 [JOURNAL_IDS]
     */
    @JsonProperty("journal_ids")
    public void setJournal_ids(String  journal_ids){
        this.journal_ids = journal_ids ;
        this.journal_idsDirtyFlag = true ;
    }

    /**
     * 获取 [JOURNAL_IDS]脏标记
     */
    @JsonIgnore
    public boolean getJournal_idsDirtyFlag(){
        return journal_idsDirtyFlag ;
    }

    /**
     * 获取 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return display_name ;
    }

    /**
     * 设置 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

    /**
     * 获取 [DISPLAY_NAME]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return display_nameDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return write_uid_text ;
    }

    /**
     * 设置 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return write_uid_textDirtyFlag ;
    }

    /**
     * 获取 [COMPANY_ID_TEXT]
     */
    @JsonProperty("company_id_text")
    public String getCompany_id_text(){
        return company_id_text ;
    }

    /**
     * 设置 [COMPANY_ID_TEXT]
     */
    @JsonProperty("company_id_text")
    public void setCompany_id_text(String  company_id_text){
        this.company_id_text = company_id_text ;
        this.company_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [COMPANY_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getCompany_id_textDirtyFlag(){
        return company_id_textDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return create_uid_text ;
    }

    /**
     * 设置 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return create_uid_textDirtyFlag ;
    }

    /**
     * 获取 [COMPANY_ID]
     */
    @JsonProperty("company_id")
    public Integer getCompany_id(){
        return company_id ;
    }

    /**
     * 设置 [COMPANY_ID]
     */
    @JsonProperty("company_id")
    public void setCompany_id(Integer  company_id){
        this.company_id = company_id ;
        this.company_idDirtyFlag = true ;
    }

    /**
     * 获取 [COMPANY_ID]脏标记
     */
    @JsonIgnore
    public boolean getCompany_idDirtyFlag(){
        return company_idDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return write_uid ;
    }

    /**
     * 设置 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return write_uidDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return create_uid ;
    }

    /**
     * 设置 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return create_uidDirtyFlag ;
    }



    public Account_common_report toDO() {
        Account_common_report srfdomain = new Account_common_report();
        if(getDate_toDirtyFlag())
            srfdomain.setDate_to(date_to);
        if(getCreate_dateDirtyFlag())
            srfdomain.setCreate_date(create_date);
        if(getIdDirtyFlag())
            srfdomain.setId(id);
        if(getDate_fromDirtyFlag())
            srfdomain.setDate_from(date_from);
        if(getWrite_dateDirtyFlag())
            srfdomain.setWrite_date(write_date);
        if(getTarget_moveDirtyFlag())
            srfdomain.setTarget_move(target_move);
        if(get__last_updateDirtyFlag())
            srfdomain.set__last_update(__last_update);
        if(getJournal_idsDirtyFlag())
            srfdomain.setJournal_ids(journal_ids);
        if(getDisplay_nameDirtyFlag())
            srfdomain.setDisplay_name(display_name);
        if(getWrite_uid_textDirtyFlag())
            srfdomain.setWrite_uid_text(write_uid_text);
        if(getCompany_id_textDirtyFlag())
            srfdomain.setCompany_id_text(company_id_text);
        if(getCreate_uid_textDirtyFlag())
            srfdomain.setCreate_uid_text(create_uid_text);
        if(getCompany_idDirtyFlag())
            srfdomain.setCompany_id(company_id);
        if(getWrite_uidDirtyFlag())
            srfdomain.setWrite_uid(write_uid);
        if(getCreate_uidDirtyFlag())
            srfdomain.setCreate_uid(create_uid);

        return srfdomain;
    }

    public void fromDO(Account_common_report srfdomain) {
        if(srfdomain == null )
          return ;
        if(srfdomain.getDate_toDirtyFlag())
            this.setDate_to(srfdomain.getDate_to());
        if(srfdomain.getCreate_dateDirtyFlag())
            this.setCreate_date(srfdomain.getCreate_date());
        if(srfdomain.getIdDirtyFlag())
            this.setId(srfdomain.getId());
        if(srfdomain.getDate_fromDirtyFlag())
            this.setDate_from(srfdomain.getDate_from());
        if(srfdomain.getWrite_dateDirtyFlag())
            this.setWrite_date(srfdomain.getWrite_date());
        if(srfdomain.getTarget_moveDirtyFlag())
            this.setTarget_move(srfdomain.getTarget_move());
        if(srfdomain.get__last_updateDirtyFlag())
            this.set__last_update(srfdomain.get__last_update());
        if(srfdomain.getJournal_idsDirtyFlag())
            this.setJournal_ids(srfdomain.getJournal_ids());
        if(srfdomain.getDisplay_nameDirtyFlag())
            this.setDisplay_name(srfdomain.getDisplay_name());
        if(srfdomain.getWrite_uid_textDirtyFlag())
            this.setWrite_uid_text(srfdomain.getWrite_uid_text());
        if(srfdomain.getCompany_id_textDirtyFlag())
            this.setCompany_id_text(srfdomain.getCompany_id_text());
        if(srfdomain.getCreate_uid_textDirtyFlag())
            this.setCreate_uid_text(srfdomain.getCreate_uid_text());
        if(srfdomain.getCompany_idDirtyFlag())
            this.setCompany_id(srfdomain.getCompany_id());
        if(srfdomain.getWrite_uidDirtyFlag())
            this.setWrite_uid(srfdomain.getWrite_uid());
        if(srfdomain.getCreate_uidDirtyFlag())
            this.setCreate_uid(srfdomain.getCreate_uid());

    }

    public List<Account_common_reportDTO> fromDOPage(List<Account_common_report> poPage)   {
        if(poPage == null)
            return null;
        List<Account_common_reportDTO> dtos=new ArrayList<Account_common_reportDTO>();
        for(Account_common_report domain : poPage) {
            Account_common_reportDTO dto = new Account_common_reportDTO();
            dto.fromDO(domain);
            dtos.add(dto);
        }
        return dtos;
    }
}

