package cn.ibizlab.odoo.service.odoo_account.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.core.odoo_account.valuerule.anno.account_abstract_payment.*;
import cn.ibizlab.odoo.core.odoo_account.domain.Account_abstract_payment;
import org.springframework.cglib.beans.BeanCopier;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 服务DTO对象[Account_abstract_paymentDTO]
 */
public class Account_abstract_paymentDTO implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [PAYMENT_DIFFERENCE_HANDLING]
     *
     */
    @Account_abstract_paymentPayment_difference_handlingDefault(info = "默认规则")
    private String payment_difference_handling;

    @JsonIgnore
    private boolean payment_difference_handlingDirtyFlag;

    /**
     * 属性 [PARTNER_TYPE]
     *
     */
    @Account_abstract_paymentPartner_typeDefault(info = "默认规则")
    private String partner_type;

    @JsonIgnore
    private boolean partner_typeDirtyFlag;

    /**
     * 属性 [AMOUNT]
     *
     */
    @Account_abstract_paymentAmountDefault(info = "默认规则")
    private Double amount;

    @JsonIgnore
    private boolean amountDirtyFlag;

    /**
     * 属性 [INVOICE_IDS]
     *
     */
    @Account_abstract_paymentInvoice_idsDefault(info = "默认规则")
    private String invoice_ids;

    @JsonIgnore
    private boolean invoice_idsDirtyFlag;

    /**
     * 属性 [PAYMENT_DIFFERENCE]
     *
     */
    @Account_abstract_paymentPayment_differenceDefault(info = "默认规则")
    private Double payment_difference;

    @JsonIgnore
    private boolean payment_differenceDirtyFlag;

    /**
     * 属性 [MULTI]
     *
     */
    @Account_abstract_paymentMultiDefault(info = "默认规则")
    private String multi;

    @JsonIgnore
    private boolean multiDirtyFlag;

    /**
     * 属性 [WRITEOFF_LABEL]
     *
     */
    @Account_abstract_paymentWriteoff_labelDefault(info = "默认规则")
    private String writeoff_label;

    @JsonIgnore
    private boolean writeoff_labelDirtyFlag;

    /**
     * 属性 [SHOW_PARTNER_BANK_ACCOUNT]
     *
     */
    @Account_abstract_paymentShow_partner_bank_accountDefault(info = "默认规则")
    private String show_partner_bank_account;

    @JsonIgnore
    private boolean show_partner_bank_accountDirtyFlag;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @Account_abstract_paymentDisplay_nameDefault(info = "默认规则")
    private String display_name;

    @JsonIgnore
    private boolean display_nameDirtyFlag;

    /**
     * 属性 [HIDE_PAYMENT_METHOD]
     *
     */
    @Account_abstract_paymentHide_payment_methodDefault(info = "默认规则")
    private String hide_payment_method;

    @JsonIgnore
    private boolean hide_payment_methodDirtyFlag;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @Account_abstract_payment__last_updateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp __last_update;

    @JsonIgnore
    private boolean __last_updateDirtyFlag;

    /**
     * 属性 [COMMUNICATION]
     *
     */
    @Account_abstract_paymentCommunicationDefault(info = "默认规则")
    private String communication;

    @JsonIgnore
    private boolean communicationDirtyFlag;

    /**
     * 属性 [ID]
     *
     */
    @Account_abstract_paymentIdDefault(info = "默认规则")
    private Integer id;

    @JsonIgnore
    private boolean idDirtyFlag;

    /**
     * 属性 [PAYMENT_TYPE]
     *
     */
    @Account_abstract_paymentPayment_typeDefault(info = "默认规则")
    private String payment_type;

    @JsonIgnore
    private boolean payment_typeDirtyFlag;

    /**
     * 属性 [PAYMENT_DATE]
     *
     */
    @Account_abstract_paymentPayment_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    private Timestamp payment_date;

    @JsonIgnore
    private boolean payment_dateDirtyFlag;

    /**
     * 属性 [JOURNAL_ID_TEXT]
     *
     */
    @Account_abstract_paymentJournal_id_textDefault(info = "默认规则")
    private String journal_id_text;

    @JsonIgnore
    private boolean journal_id_textDirtyFlag;

    /**
     * 属性 [CURRENCY_ID_TEXT]
     *
     */
    @Account_abstract_paymentCurrency_id_textDefault(info = "默认规则")
    private String currency_id_text;

    @JsonIgnore
    private boolean currency_id_textDirtyFlag;

    /**
     * 属性 [PAYMENT_METHOD_CODE]
     *
     */
    @Account_abstract_paymentPayment_method_codeDefault(info = "默认规则")
    private String payment_method_code;

    @JsonIgnore
    private boolean payment_method_codeDirtyFlag;

    /**
     * 属性 [WRITEOFF_ACCOUNT_ID_TEXT]
     *
     */
    @Account_abstract_paymentWriteoff_account_id_textDefault(info = "默认规则")
    private String writeoff_account_id_text;

    @JsonIgnore
    private boolean writeoff_account_id_textDirtyFlag;

    /**
     * 属性 [PARTNER_ID_TEXT]
     *
     */
    @Account_abstract_paymentPartner_id_textDefault(info = "默认规则")
    private String partner_id_text;

    @JsonIgnore
    private boolean partner_id_textDirtyFlag;

    /**
     * 属性 [PAYMENT_METHOD_ID_TEXT]
     *
     */
    @Account_abstract_paymentPayment_method_id_textDefault(info = "默认规则")
    private String payment_method_id_text;

    @JsonIgnore
    private boolean payment_method_id_textDirtyFlag;

    /**
     * 属性 [CURRENCY_ID]
     *
     */
    @Account_abstract_paymentCurrency_idDefault(info = "默认规则")
    private Integer currency_id;

    @JsonIgnore
    private boolean currency_idDirtyFlag;

    /**
     * 属性 [JOURNAL_ID]
     *
     */
    @Account_abstract_paymentJournal_idDefault(info = "默认规则")
    private Integer journal_id;

    @JsonIgnore
    private boolean journal_idDirtyFlag;

    /**
     * 属性 [PARTNER_BANK_ACCOUNT_ID]
     *
     */
    @Account_abstract_paymentPartner_bank_account_idDefault(info = "默认规则")
    private Integer partner_bank_account_id;

    @JsonIgnore
    private boolean partner_bank_account_idDirtyFlag;

    /**
     * 属性 [PAYMENT_METHOD_ID]
     *
     */
    @Account_abstract_paymentPayment_method_idDefault(info = "默认规则")
    private Integer payment_method_id;

    @JsonIgnore
    private boolean payment_method_idDirtyFlag;

    /**
     * 属性 [PARTNER_ID]
     *
     */
    @Account_abstract_paymentPartner_idDefault(info = "默认规则")
    private Integer partner_id;

    @JsonIgnore
    private boolean partner_idDirtyFlag;

    /**
     * 属性 [WRITEOFF_ACCOUNT_ID]
     *
     */
    @Account_abstract_paymentWriteoff_account_idDefault(info = "默认规则")
    private Integer writeoff_account_id;

    @JsonIgnore
    private boolean writeoff_account_idDirtyFlag;


    /**
     * 获取 [PAYMENT_DIFFERENCE_HANDLING]
     */
    @JsonProperty("payment_difference_handling")
    public String getPayment_difference_handling(){
        return payment_difference_handling ;
    }

    /**
     * 设置 [PAYMENT_DIFFERENCE_HANDLING]
     */
    @JsonProperty("payment_difference_handling")
    public void setPayment_difference_handling(String  payment_difference_handling){
        this.payment_difference_handling = payment_difference_handling ;
        this.payment_difference_handlingDirtyFlag = true ;
    }

    /**
     * 获取 [PAYMENT_DIFFERENCE_HANDLING]脏标记
     */
    @JsonIgnore
    public boolean getPayment_difference_handlingDirtyFlag(){
        return payment_difference_handlingDirtyFlag ;
    }

    /**
     * 获取 [PARTNER_TYPE]
     */
    @JsonProperty("partner_type")
    public String getPartner_type(){
        return partner_type ;
    }

    /**
     * 设置 [PARTNER_TYPE]
     */
    @JsonProperty("partner_type")
    public void setPartner_type(String  partner_type){
        this.partner_type = partner_type ;
        this.partner_typeDirtyFlag = true ;
    }

    /**
     * 获取 [PARTNER_TYPE]脏标记
     */
    @JsonIgnore
    public boolean getPartner_typeDirtyFlag(){
        return partner_typeDirtyFlag ;
    }

    /**
     * 获取 [AMOUNT]
     */
    @JsonProperty("amount")
    public Double getAmount(){
        return amount ;
    }

    /**
     * 设置 [AMOUNT]
     */
    @JsonProperty("amount")
    public void setAmount(Double  amount){
        this.amount = amount ;
        this.amountDirtyFlag = true ;
    }

    /**
     * 获取 [AMOUNT]脏标记
     */
    @JsonIgnore
    public boolean getAmountDirtyFlag(){
        return amountDirtyFlag ;
    }

    /**
     * 获取 [INVOICE_IDS]
     */
    @JsonProperty("invoice_ids")
    public String getInvoice_ids(){
        return invoice_ids ;
    }

    /**
     * 设置 [INVOICE_IDS]
     */
    @JsonProperty("invoice_ids")
    public void setInvoice_ids(String  invoice_ids){
        this.invoice_ids = invoice_ids ;
        this.invoice_idsDirtyFlag = true ;
    }

    /**
     * 获取 [INVOICE_IDS]脏标记
     */
    @JsonIgnore
    public boolean getInvoice_idsDirtyFlag(){
        return invoice_idsDirtyFlag ;
    }

    /**
     * 获取 [PAYMENT_DIFFERENCE]
     */
    @JsonProperty("payment_difference")
    public Double getPayment_difference(){
        return payment_difference ;
    }

    /**
     * 设置 [PAYMENT_DIFFERENCE]
     */
    @JsonProperty("payment_difference")
    public void setPayment_difference(Double  payment_difference){
        this.payment_difference = payment_difference ;
        this.payment_differenceDirtyFlag = true ;
    }

    /**
     * 获取 [PAYMENT_DIFFERENCE]脏标记
     */
    @JsonIgnore
    public boolean getPayment_differenceDirtyFlag(){
        return payment_differenceDirtyFlag ;
    }

    /**
     * 获取 [MULTI]
     */
    @JsonProperty("multi")
    public String getMulti(){
        return multi ;
    }

    /**
     * 设置 [MULTI]
     */
    @JsonProperty("multi")
    public void setMulti(String  multi){
        this.multi = multi ;
        this.multiDirtyFlag = true ;
    }

    /**
     * 获取 [MULTI]脏标记
     */
    @JsonIgnore
    public boolean getMultiDirtyFlag(){
        return multiDirtyFlag ;
    }

    /**
     * 获取 [WRITEOFF_LABEL]
     */
    @JsonProperty("writeoff_label")
    public String getWriteoff_label(){
        return writeoff_label ;
    }

    /**
     * 设置 [WRITEOFF_LABEL]
     */
    @JsonProperty("writeoff_label")
    public void setWriteoff_label(String  writeoff_label){
        this.writeoff_label = writeoff_label ;
        this.writeoff_labelDirtyFlag = true ;
    }

    /**
     * 获取 [WRITEOFF_LABEL]脏标记
     */
    @JsonIgnore
    public boolean getWriteoff_labelDirtyFlag(){
        return writeoff_labelDirtyFlag ;
    }

    /**
     * 获取 [SHOW_PARTNER_BANK_ACCOUNT]
     */
    @JsonProperty("show_partner_bank_account")
    public String getShow_partner_bank_account(){
        return show_partner_bank_account ;
    }

    /**
     * 设置 [SHOW_PARTNER_BANK_ACCOUNT]
     */
    @JsonProperty("show_partner_bank_account")
    public void setShow_partner_bank_account(String  show_partner_bank_account){
        this.show_partner_bank_account = show_partner_bank_account ;
        this.show_partner_bank_accountDirtyFlag = true ;
    }

    /**
     * 获取 [SHOW_PARTNER_BANK_ACCOUNT]脏标记
     */
    @JsonIgnore
    public boolean getShow_partner_bank_accountDirtyFlag(){
        return show_partner_bank_accountDirtyFlag ;
    }

    /**
     * 获取 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return display_name ;
    }

    /**
     * 设置 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

    /**
     * 获取 [DISPLAY_NAME]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return display_nameDirtyFlag ;
    }

    /**
     * 获取 [HIDE_PAYMENT_METHOD]
     */
    @JsonProperty("hide_payment_method")
    public String getHide_payment_method(){
        return hide_payment_method ;
    }

    /**
     * 设置 [HIDE_PAYMENT_METHOD]
     */
    @JsonProperty("hide_payment_method")
    public void setHide_payment_method(String  hide_payment_method){
        this.hide_payment_method = hide_payment_method ;
        this.hide_payment_methodDirtyFlag = true ;
    }

    /**
     * 获取 [HIDE_PAYMENT_METHOD]脏标记
     */
    @JsonIgnore
    public boolean getHide_payment_methodDirtyFlag(){
        return hide_payment_methodDirtyFlag ;
    }

    /**
     * 获取 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return __last_update ;
    }

    /**
     * 设置 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

    /**
     * 获取 [__LAST_UPDATE]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return __last_updateDirtyFlag ;
    }

    /**
     * 获取 [COMMUNICATION]
     */
    @JsonProperty("communication")
    public String getCommunication(){
        return communication ;
    }

    /**
     * 设置 [COMMUNICATION]
     */
    @JsonProperty("communication")
    public void setCommunication(String  communication){
        this.communication = communication ;
        this.communicationDirtyFlag = true ;
    }

    /**
     * 获取 [COMMUNICATION]脏标记
     */
    @JsonIgnore
    public boolean getCommunicationDirtyFlag(){
        return communicationDirtyFlag ;
    }

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

    /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return idDirtyFlag ;
    }

    /**
     * 获取 [PAYMENT_TYPE]
     */
    @JsonProperty("payment_type")
    public String getPayment_type(){
        return payment_type ;
    }

    /**
     * 设置 [PAYMENT_TYPE]
     */
    @JsonProperty("payment_type")
    public void setPayment_type(String  payment_type){
        this.payment_type = payment_type ;
        this.payment_typeDirtyFlag = true ;
    }

    /**
     * 获取 [PAYMENT_TYPE]脏标记
     */
    @JsonIgnore
    public boolean getPayment_typeDirtyFlag(){
        return payment_typeDirtyFlag ;
    }

    /**
     * 获取 [PAYMENT_DATE]
     */
    @JsonProperty("payment_date")
    public Timestamp getPayment_date(){
        return payment_date ;
    }

    /**
     * 设置 [PAYMENT_DATE]
     */
    @JsonProperty("payment_date")
    public void setPayment_date(Timestamp  payment_date){
        this.payment_date = payment_date ;
        this.payment_dateDirtyFlag = true ;
    }

    /**
     * 获取 [PAYMENT_DATE]脏标记
     */
    @JsonIgnore
    public boolean getPayment_dateDirtyFlag(){
        return payment_dateDirtyFlag ;
    }

    /**
     * 获取 [JOURNAL_ID_TEXT]
     */
    @JsonProperty("journal_id_text")
    public String getJournal_id_text(){
        return journal_id_text ;
    }

    /**
     * 设置 [JOURNAL_ID_TEXT]
     */
    @JsonProperty("journal_id_text")
    public void setJournal_id_text(String  journal_id_text){
        this.journal_id_text = journal_id_text ;
        this.journal_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [JOURNAL_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getJournal_id_textDirtyFlag(){
        return journal_id_textDirtyFlag ;
    }

    /**
     * 获取 [CURRENCY_ID_TEXT]
     */
    @JsonProperty("currency_id_text")
    public String getCurrency_id_text(){
        return currency_id_text ;
    }

    /**
     * 设置 [CURRENCY_ID_TEXT]
     */
    @JsonProperty("currency_id_text")
    public void setCurrency_id_text(String  currency_id_text){
        this.currency_id_text = currency_id_text ;
        this.currency_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [CURRENCY_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getCurrency_id_textDirtyFlag(){
        return currency_id_textDirtyFlag ;
    }

    /**
     * 获取 [PAYMENT_METHOD_CODE]
     */
    @JsonProperty("payment_method_code")
    public String getPayment_method_code(){
        return payment_method_code ;
    }

    /**
     * 设置 [PAYMENT_METHOD_CODE]
     */
    @JsonProperty("payment_method_code")
    public void setPayment_method_code(String  payment_method_code){
        this.payment_method_code = payment_method_code ;
        this.payment_method_codeDirtyFlag = true ;
    }

    /**
     * 获取 [PAYMENT_METHOD_CODE]脏标记
     */
    @JsonIgnore
    public boolean getPayment_method_codeDirtyFlag(){
        return payment_method_codeDirtyFlag ;
    }

    /**
     * 获取 [WRITEOFF_ACCOUNT_ID_TEXT]
     */
    @JsonProperty("writeoff_account_id_text")
    public String getWriteoff_account_id_text(){
        return writeoff_account_id_text ;
    }

    /**
     * 设置 [WRITEOFF_ACCOUNT_ID_TEXT]
     */
    @JsonProperty("writeoff_account_id_text")
    public void setWriteoff_account_id_text(String  writeoff_account_id_text){
        this.writeoff_account_id_text = writeoff_account_id_text ;
        this.writeoff_account_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [WRITEOFF_ACCOUNT_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getWriteoff_account_id_textDirtyFlag(){
        return writeoff_account_id_textDirtyFlag ;
    }

    /**
     * 获取 [PARTNER_ID_TEXT]
     */
    @JsonProperty("partner_id_text")
    public String getPartner_id_text(){
        return partner_id_text ;
    }

    /**
     * 设置 [PARTNER_ID_TEXT]
     */
    @JsonProperty("partner_id_text")
    public void setPartner_id_text(String  partner_id_text){
        this.partner_id_text = partner_id_text ;
        this.partner_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [PARTNER_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getPartner_id_textDirtyFlag(){
        return partner_id_textDirtyFlag ;
    }

    /**
     * 获取 [PAYMENT_METHOD_ID_TEXT]
     */
    @JsonProperty("payment_method_id_text")
    public String getPayment_method_id_text(){
        return payment_method_id_text ;
    }

    /**
     * 设置 [PAYMENT_METHOD_ID_TEXT]
     */
    @JsonProperty("payment_method_id_text")
    public void setPayment_method_id_text(String  payment_method_id_text){
        this.payment_method_id_text = payment_method_id_text ;
        this.payment_method_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [PAYMENT_METHOD_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getPayment_method_id_textDirtyFlag(){
        return payment_method_id_textDirtyFlag ;
    }

    /**
     * 获取 [CURRENCY_ID]
     */
    @JsonProperty("currency_id")
    public Integer getCurrency_id(){
        return currency_id ;
    }

    /**
     * 设置 [CURRENCY_ID]
     */
    @JsonProperty("currency_id")
    public void setCurrency_id(Integer  currency_id){
        this.currency_id = currency_id ;
        this.currency_idDirtyFlag = true ;
    }

    /**
     * 获取 [CURRENCY_ID]脏标记
     */
    @JsonIgnore
    public boolean getCurrency_idDirtyFlag(){
        return currency_idDirtyFlag ;
    }

    /**
     * 获取 [JOURNAL_ID]
     */
    @JsonProperty("journal_id")
    public Integer getJournal_id(){
        return journal_id ;
    }

    /**
     * 设置 [JOURNAL_ID]
     */
    @JsonProperty("journal_id")
    public void setJournal_id(Integer  journal_id){
        this.journal_id = journal_id ;
        this.journal_idDirtyFlag = true ;
    }

    /**
     * 获取 [JOURNAL_ID]脏标记
     */
    @JsonIgnore
    public boolean getJournal_idDirtyFlag(){
        return journal_idDirtyFlag ;
    }

    /**
     * 获取 [PARTNER_BANK_ACCOUNT_ID]
     */
    @JsonProperty("partner_bank_account_id")
    public Integer getPartner_bank_account_id(){
        return partner_bank_account_id ;
    }

    /**
     * 设置 [PARTNER_BANK_ACCOUNT_ID]
     */
    @JsonProperty("partner_bank_account_id")
    public void setPartner_bank_account_id(Integer  partner_bank_account_id){
        this.partner_bank_account_id = partner_bank_account_id ;
        this.partner_bank_account_idDirtyFlag = true ;
    }

    /**
     * 获取 [PARTNER_BANK_ACCOUNT_ID]脏标记
     */
    @JsonIgnore
    public boolean getPartner_bank_account_idDirtyFlag(){
        return partner_bank_account_idDirtyFlag ;
    }

    /**
     * 获取 [PAYMENT_METHOD_ID]
     */
    @JsonProperty("payment_method_id")
    public Integer getPayment_method_id(){
        return payment_method_id ;
    }

    /**
     * 设置 [PAYMENT_METHOD_ID]
     */
    @JsonProperty("payment_method_id")
    public void setPayment_method_id(Integer  payment_method_id){
        this.payment_method_id = payment_method_id ;
        this.payment_method_idDirtyFlag = true ;
    }

    /**
     * 获取 [PAYMENT_METHOD_ID]脏标记
     */
    @JsonIgnore
    public boolean getPayment_method_idDirtyFlag(){
        return payment_method_idDirtyFlag ;
    }

    /**
     * 获取 [PARTNER_ID]
     */
    @JsonProperty("partner_id")
    public Integer getPartner_id(){
        return partner_id ;
    }

    /**
     * 设置 [PARTNER_ID]
     */
    @JsonProperty("partner_id")
    public void setPartner_id(Integer  partner_id){
        this.partner_id = partner_id ;
        this.partner_idDirtyFlag = true ;
    }

    /**
     * 获取 [PARTNER_ID]脏标记
     */
    @JsonIgnore
    public boolean getPartner_idDirtyFlag(){
        return partner_idDirtyFlag ;
    }

    /**
     * 获取 [WRITEOFF_ACCOUNT_ID]
     */
    @JsonProperty("writeoff_account_id")
    public Integer getWriteoff_account_id(){
        return writeoff_account_id ;
    }

    /**
     * 设置 [WRITEOFF_ACCOUNT_ID]
     */
    @JsonProperty("writeoff_account_id")
    public void setWriteoff_account_id(Integer  writeoff_account_id){
        this.writeoff_account_id = writeoff_account_id ;
        this.writeoff_account_idDirtyFlag = true ;
    }

    /**
     * 获取 [WRITEOFF_ACCOUNT_ID]脏标记
     */
    @JsonIgnore
    public boolean getWriteoff_account_idDirtyFlag(){
        return writeoff_account_idDirtyFlag ;
    }



    public Account_abstract_payment toDO() {
        Account_abstract_payment srfdomain = new Account_abstract_payment();
        if(getPayment_difference_handlingDirtyFlag())
            srfdomain.setPayment_difference_handling(payment_difference_handling);
        if(getPartner_typeDirtyFlag())
            srfdomain.setPartner_type(partner_type);
        if(getAmountDirtyFlag())
            srfdomain.setAmount(amount);
        if(getInvoice_idsDirtyFlag())
            srfdomain.setInvoice_ids(invoice_ids);
        if(getPayment_differenceDirtyFlag())
            srfdomain.setPayment_difference(payment_difference);
        if(getMultiDirtyFlag())
            srfdomain.setMulti(multi);
        if(getWriteoff_labelDirtyFlag())
            srfdomain.setWriteoff_label(writeoff_label);
        if(getShow_partner_bank_accountDirtyFlag())
            srfdomain.setShow_partner_bank_account(show_partner_bank_account);
        if(getDisplay_nameDirtyFlag())
            srfdomain.setDisplay_name(display_name);
        if(getHide_payment_methodDirtyFlag())
            srfdomain.setHide_payment_method(hide_payment_method);
        if(get__last_updateDirtyFlag())
            srfdomain.set__last_update(__last_update);
        if(getCommunicationDirtyFlag())
            srfdomain.setCommunication(communication);
        if(getIdDirtyFlag())
            srfdomain.setId(id);
        if(getPayment_typeDirtyFlag())
            srfdomain.setPayment_type(payment_type);
        if(getPayment_dateDirtyFlag())
            srfdomain.setPayment_date(payment_date);
        if(getJournal_id_textDirtyFlag())
            srfdomain.setJournal_id_text(journal_id_text);
        if(getCurrency_id_textDirtyFlag())
            srfdomain.setCurrency_id_text(currency_id_text);
        if(getPayment_method_codeDirtyFlag())
            srfdomain.setPayment_method_code(payment_method_code);
        if(getWriteoff_account_id_textDirtyFlag())
            srfdomain.setWriteoff_account_id_text(writeoff_account_id_text);
        if(getPartner_id_textDirtyFlag())
            srfdomain.setPartner_id_text(partner_id_text);
        if(getPayment_method_id_textDirtyFlag())
            srfdomain.setPayment_method_id_text(payment_method_id_text);
        if(getCurrency_idDirtyFlag())
            srfdomain.setCurrency_id(currency_id);
        if(getJournal_idDirtyFlag())
            srfdomain.setJournal_id(journal_id);
        if(getPartner_bank_account_idDirtyFlag())
            srfdomain.setPartner_bank_account_id(partner_bank_account_id);
        if(getPayment_method_idDirtyFlag())
            srfdomain.setPayment_method_id(payment_method_id);
        if(getPartner_idDirtyFlag())
            srfdomain.setPartner_id(partner_id);
        if(getWriteoff_account_idDirtyFlag())
            srfdomain.setWriteoff_account_id(writeoff_account_id);

        return srfdomain;
    }

    public void fromDO(Account_abstract_payment srfdomain) {
        if(srfdomain == null )
          return ;
        if(srfdomain.getPayment_difference_handlingDirtyFlag())
            this.setPayment_difference_handling(srfdomain.getPayment_difference_handling());
        if(srfdomain.getPartner_typeDirtyFlag())
            this.setPartner_type(srfdomain.getPartner_type());
        if(srfdomain.getAmountDirtyFlag())
            this.setAmount(srfdomain.getAmount());
        if(srfdomain.getInvoice_idsDirtyFlag())
            this.setInvoice_ids(srfdomain.getInvoice_ids());
        if(srfdomain.getPayment_differenceDirtyFlag())
            this.setPayment_difference(srfdomain.getPayment_difference());
        if(srfdomain.getMultiDirtyFlag())
            this.setMulti(srfdomain.getMulti());
        if(srfdomain.getWriteoff_labelDirtyFlag())
            this.setWriteoff_label(srfdomain.getWriteoff_label());
        if(srfdomain.getShow_partner_bank_accountDirtyFlag())
            this.setShow_partner_bank_account(srfdomain.getShow_partner_bank_account());
        if(srfdomain.getDisplay_nameDirtyFlag())
            this.setDisplay_name(srfdomain.getDisplay_name());
        if(srfdomain.getHide_payment_methodDirtyFlag())
            this.setHide_payment_method(srfdomain.getHide_payment_method());
        if(srfdomain.get__last_updateDirtyFlag())
            this.set__last_update(srfdomain.get__last_update());
        if(srfdomain.getCommunicationDirtyFlag())
            this.setCommunication(srfdomain.getCommunication());
        if(srfdomain.getIdDirtyFlag())
            this.setId(srfdomain.getId());
        if(srfdomain.getPayment_typeDirtyFlag())
            this.setPayment_type(srfdomain.getPayment_type());
        if(srfdomain.getPayment_dateDirtyFlag())
            this.setPayment_date(srfdomain.getPayment_date());
        if(srfdomain.getJournal_id_textDirtyFlag())
            this.setJournal_id_text(srfdomain.getJournal_id_text());
        if(srfdomain.getCurrency_id_textDirtyFlag())
            this.setCurrency_id_text(srfdomain.getCurrency_id_text());
        if(srfdomain.getPayment_method_codeDirtyFlag())
            this.setPayment_method_code(srfdomain.getPayment_method_code());
        if(srfdomain.getWriteoff_account_id_textDirtyFlag())
            this.setWriteoff_account_id_text(srfdomain.getWriteoff_account_id_text());
        if(srfdomain.getPartner_id_textDirtyFlag())
            this.setPartner_id_text(srfdomain.getPartner_id_text());
        if(srfdomain.getPayment_method_id_textDirtyFlag())
            this.setPayment_method_id_text(srfdomain.getPayment_method_id_text());
        if(srfdomain.getCurrency_idDirtyFlag())
            this.setCurrency_id(srfdomain.getCurrency_id());
        if(srfdomain.getJournal_idDirtyFlag())
            this.setJournal_id(srfdomain.getJournal_id());
        if(srfdomain.getPartner_bank_account_idDirtyFlag())
            this.setPartner_bank_account_id(srfdomain.getPartner_bank_account_id());
        if(srfdomain.getPayment_method_idDirtyFlag())
            this.setPayment_method_id(srfdomain.getPayment_method_id());
        if(srfdomain.getPartner_idDirtyFlag())
            this.setPartner_id(srfdomain.getPartner_id());
        if(srfdomain.getWriteoff_account_idDirtyFlag())
            this.setWriteoff_account_id(srfdomain.getWriteoff_account_id());

    }

    public List<Account_abstract_paymentDTO> fromDOPage(List<Account_abstract_payment> poPage)   {
        if(poPage == null)
            return null;
        List<Account_abstract_paymentDTO> dtos=new ArrayList<Account_abstract_paymentDTO>();
        for(Account_abstract_payment domain : poPage) {
            Account_abstract_paymentDTO dto = new Account_abstract_paymentDTO();
            dto.fromDO(domain);
            dtos.add(dto);
        }
        return dtos;
    }
}

