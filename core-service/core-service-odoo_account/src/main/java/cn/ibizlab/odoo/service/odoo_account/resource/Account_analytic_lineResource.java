package cn.ibizlab.odoo.service.odoo_account.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_account.dto.Account_analytic_lineDTO;
import cn.ibizlab.odoo.core.odoo_account.domain.Account_analytic_line;
import cn.ibizlab.odoo.core.odoo_account.service.IAccount_analytic_lineService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_analytic_lineSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Account_analytic_line" })
@RestController
@RequestMapping("")
public class Account_analytic_lineResource {

    @Autowired
    private IAccount_analytic_lineService account_analytic_lineService;

    public IAccount_analytic_lineService getAccount_analytic_lineService() {
        return this.account_analytic_lineService;
    }

    @ApiOperation(value = "批更新数据", tags = {"Account_analytic_line" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_account/account_analytic_lines/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Account_analytic_lineDTO> account_analytic_linedtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "建立数据", tags = {"Account_analytic_line" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_account/account_analytic_lines")

    public ResponseEntity<Account_analytic_lineDTO> create(@RequestBody Account_analytic_lineDTO account_analytic_linedto) {
        Account_analytic_lineDTO dto = new Account_analytic_lineDTO();
        Account_analytic_line domain = account_analytic_linedto.toDO();
		account_analytic_lineService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取数据", tags = {"Account_analytic_line" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_account/account_analytic_lines/{account_analytic_line_id}")
    public ResponseEntity<Account_analytic_lineDTO> get(@PathVariable("account_analytic_line_id") Integer account_analytic_line_id) {
        Account_analytic_lineDTO dto = new Account_analytic_lineDTO();
        Account_analytic_line domain = account_analytic_lineService.get(account_analytic_line_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批建立数据", tags = {"Account_analytic_line" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_account/account_analytic_lines/createBatch")
    public ResponseEntity<Boolean> createBatchAccount_analytic_line(@RequestBody List<Account_analytic_lineDTO> account_analytic_linedtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "批删除数据", tags = {"Account_analytic_line" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_account/account_analytic_lines/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Account_analytic_lineDTO> account_analytic_linedtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "删除数据", tags = {"Account_analytic_line" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_account/account_analytic_lines/{account_analytic_line_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("account_analytic_line_id") Integer account_analytic_line_id) {
        Account_analytic_lineDTO account_analytic_linedto = new Account_analytic_lineDTO();
		Account_analytic_line domain = new Account_analytic_line();
		account_analytic_linedto.setId(account_analytic_line_id);
		domain.setId(account_analytic_line_id);
        Boolean rst = account_analytic_lineService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "更新数据", tags = {"Account_analytic_line" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_account/account_analytic_lines/{account_analytic_line_id}")

    public ResponseEntity<Account_analytic_lineDTO> update(@PathVariable("account_analytic_line_id") Integer account_analytic_line_id, @RequestBody Account_analytic_lineDTO account_analytic_linedto) {
		Account_analytic_line domain = account_analytic_linedto.toDO();
        domain.setId(account_analytic_line_id);
		account_analytic_lineService.update(domain);
		Account_analytic_lineDTO dto = new Account_analytic_lineDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Account_analytic_line" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_account/account_analytic_lines/fetchdefault")
	public ResponseEntity<Page<Account_analytic_lineDTO>> fetchDefault(Account_analytic_lineSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Account_analytic_lineDTO> list = new ArrayList<Account_analytic_lineDTO>();
        
        Page<Account_analytic_line> domains = account_analytic_lineService.searchDefault(context) ;
        for(Account_analytic_line account_analytic_line : domains.getContent()){
            Account_analytic_lineDTO dto = new Account_analytic_lineDTO();
            dto.fromDO(account_analytic_line);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
