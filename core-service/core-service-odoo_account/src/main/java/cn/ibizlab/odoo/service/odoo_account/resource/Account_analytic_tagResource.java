package cn.ibizlab.odoo.service.odoo_account.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_account.dto.Account_analytic_tagDTO;
import cn.ibizlab.odoo.core.odoo_account.domain.Account_analytic_tag;
import cn.ibizlab.odoo.core.odoo_account.service.IAccount_analytic_tagService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_analytic_tagSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Account_analytic_tag" })
@RestController
@RequestMapping("")
public class Account_analytic_tagResource {

    @Autowired
    private IAccount_analytic_tagService account_analytic_tagService;

    public IAccount_analytic_tagService getAccount_analytic_tagService() {
        return this.account_analytic_tagService;
    }

    @ApiOperation(value = "批建立数据", tags = {"Account_analytic_tag" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_account/account_analytic_tags/createBatch")
    public ResponseEntity<Boolean> createBatchAccount_analytic_tag(@RequestBody List<Account_analytic_tagDTO> account_analytic_tagdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "获取数据", tags = {"Account_analytic_tag" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_account/account_analytic_tags/{account_analytic_tag_id}")
    public ResponseEntity<Account_analytic_tagDTO> get(@PathVariable("account_analytic_tag_id") Integer account_analytic_tag_id) {
        Account_analytic_tagDTO dto = new Account_analytic_tagDTO();
        Account_analytic_tag domain = account_analytic_tagService.get(account_analytic_tag_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批更新数据", tags = {"Account_analytic_tag" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_account/account_analytic_tags/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Account_analytic_tagDTO> account_analytic_tagdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "批删除数据", tags = {"Account_analytic_tag" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_account/account_analytic_tags/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Account_analytic_tagDTO> account_analytic_tagdtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "删除数据", tags = {"Account_analytic_tag" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_account/account_analytic_tags/{account_analytic_tag_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("account_analytic_tag_id") Integer account_analytic_tag_id) {
        Account_analytic_tagDTO account_analytic_tagdto = new Account_analytic_tagDTO();
		Account_analytic_tag domain = new Account_analytic_tag();
		account_analytic_tagdto.setId(account_analytic_tag_id);
		domain.setId(account_analytic_tag_id);
        Boolean rst = account_analytic_tagService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "建立数据", tags = {"Account_analytic_tag" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_account/account_analytic_tags")

    public ResponseEntity<Account_analytic_tagDTO> create(@RequestBody Account_analytic_tagDTO account_analytic_tagdto) {
        Account_analytic_tagDTO dto = new Account_analytic_tagDTO();
        Account_analytic_tag domain = account_analytic_tagdto.toDO();
		account_analytic_tagService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "更新数据", tags = {"Account_analytic_tag" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_account/account_analytic_tags/{account_analytic_tag_id}")

    public ResponseEntity<Account_analytic_tagDTO> update(@PathVariable("account_analytic_tag_id") Integer account_analytic_tag_id, @RequestBody Account_analytic_tagDTO account_analytic_tagdto) {
		Account_analytic_tag domain = account_analytic_tagdto.toDO();
        domain.setId(account_analytic_tag_id);
		account_analytic_tagService.update(domain);
		Account_analytic_tagDTO dto = new Account_analytic_tagDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Account_analytic_tag" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_account/account_analytic_tags/fetchdefault")
	public ResponseEntity<Page<Account_analytic_tagDTO>> fetchDefault(Account_analytic_tagSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Account_analytic_tagDTO> list = new ArrayList<Account_analytic_tagDTO>();
        
        Page<Account_analytic_tag> domains = account_analytic_tagService.searchDefault(context) ;
        for(Account_analytic_tag account_analytic_tag : domains.getContent()){
            Account_analytic_tagDTO dto = new Account_analytic_tagDTO();
            dto.fromDO(account_analytic_tag);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
