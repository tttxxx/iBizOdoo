package cn.ibizlab.odoo.service.odoo_account.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_account.dto.Account_payment_termDTO;
import cn.ibizlab.odoo.core.odoo_account.domain.Account_payment_term;
import cn.ibizlab.odoo.core.odoo_account.service.IAccount_payment_termService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_payment_termSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Account_payment_term" })
@RestController
@RequestMapping("")
public class Account_payment_termResource {

    @Autowired
    private IAccount_payment_termService account_payment_termService;

    public IAccount_payment_termService getAccount_payment_termService() {
        return this.account_payment_termService;
    }

    @ApiOperation(value = "批建立数据", tags = {"Account_payment_term" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_account/account_payment_terms/createBatch")
    public ResponseEntity<Boolean> createBatchAccount_payment_term(@RequestBody List<Account_payment_termDTO> account_payment_termdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "批删除数据", tags = {"Account_payment_term" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_account/account_payment_terms/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Account_payment_termDTO> account_payment_termdtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "删除数据", tags = {"Account_payment_term" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_account/account_payment_terms/{account_payment_term_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("account_payment_term_id") Integer account_payment_term_id) {
        Account_payment_termDTO account_payment_termdto = new Account_payment_termDTO();
		Account_payment_term domain = new Account_payment_term();
		account_payment_termdto.setId(account_payment_term_id);
		domain.setId(account_payment_term_id);
        Boolean rst = account_payment_termService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "批更新数据", tags = {"Account_payment_term" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_account/account_payment_terms/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Account_payment_termDTO> account_payment_termdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "建立数据", tags = {"Account_payment_term" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_account/account_payment_terms")

    public ResponseEntity<Account_payment_termDTO> create(@RequestBody Account_payment_termDTO account_payment_termdto) {
        Account_payment_termDTO dto = new Account_payment_termDTO();
        Account_payment_term domain = account_payment_termdto.toDO();
		account_payment_termService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取数据", tags = {"Account_payment_term" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_account/account_payment_terms/{account_payment_term_id}")
    public ResponseEntity<Account_payment_termDTO> get(@PathVariable("account_payment_term_id") Integer account_payment_term_id) {
        Account_payment_termDTO dto = new Account_payment_termDTO();
        Account_payment_term domain = account_payment_termService.get(account_payment_term_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "更新数据", tags = {"Account_payment_term" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_account/account_payment_terms/{account_payment_term_id}")

    public ResponseEntity<Account_payment_termDTO> update(@PathVariable("account_payment_term_id") Integer account_payment_term_id, @RequestBody Account_payment_termDTO account_payment_termdto) {
		Account_payment_term domain = account_payment_termdto.toDO();
        domain.setId(account_payment_term_id);
		account_payment_termService.update(domain);
		Account_payment_termDTO dto = new Account_payment_termDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Account_payment_term" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_account/account_payment_terms/fetchdefault")
	public ResponseEntity<Page<Account_payment_termDTO>> fetchDefault(Account_payment_termSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Account_payment_termDTO> list = new ArrayList<Account_payment_termDTO>();
        
        Page<Account_payment_term> domains = account_payment_termService.searchDefault(context) ;
        for(Account_payment_term account_payment_term : domains.getContent()){
            Account_payment_termDTO dto = new Account_payment_termDTO();
            dto.fromDO(account_payment_term);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
