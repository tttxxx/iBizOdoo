package cn.ibizlab.odoo.service.odoo_account.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_account.dto.Account_incotermsDTO;
import cn.ibizlab.odoo.core.odoo_account.domain.Account_incoterms;
import cn.ibizlab.odoo.core.odoo_account.service.IAccount_incotermsService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_incotermsSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Account_incoterms" })
@RestController
@RequestMapping("")
public class Account_incotermsResource {

    @Autowired
    private IAccount_incotermsService account_incotermsService;

    public IAccount_incotermsService getAccount_incotermsService() {
        return this.account_incotermsService;
    }

    @ApiOperation(value = "批建立数据", tags = {"Account_incoterms" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_account/account_incoterms/createBatch")
    public ResponseEntity<Boolean> createBatchAccount_incoterms(@RequestBody List<Account_incotermsDTO> account_incotermsdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "获取数据", tags = {"Account_incoterms" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_account/account_incoterms/{account_incoterms_id}")
    public ResponseEntity<Account_incotermsDTO> get(@PathVariable("account_incoterms_id") Integer account_incoterms_id) {
        Account_incotermsDTO dto = new Account_incotermsDTO();
        Account_incoterms domain = account_incotermsService.get(account_incoterms_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批删除数据", tags = {"Account_incoterms" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_account/account_incoterms/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Account_incotermsDTO> account_incotermsdtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "建立数据", tags = {"Account_incoterms" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_account/account_incoterms")

    public ResponseEntity<Account_incotermsDTO> create(@RequestBody Account_incotermsDTO account_incotermsdto) {
        Account_incotermsDTO dto = new Account_incotermsDTO();
        Account_incoterms domain = account_incotermsdto.toDO();
		account_incotermsService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批更新数据", tags = {"Account_incoterms" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_account/account_incoterms/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Account_incotermsDTO> account_incotermsdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "删除数据", tags = {"Account_incoterms" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_account/account_incoterms/{account_incoterms_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("account_incoterms_id") Integer account_incoterms_id) {
        Account_incotermsDTO account_incotermsdto = new Account_incotermsDTO();
		Account_incoterms domain = new Account_incoterms();
		account_incotermsdto.setId(account_incoterms_id);
		domain.setId(account_incoterms_id);
        Boolean rst = account_incotermsService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "更新数据", tags = {"Account_incoterms" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_account/account_incoterms/{account_incoterms_id}")

    public ResponseEntity<Account_incotermsDTO> update(@PathVariable("account_incoterms_id") Integer account_incoterms_id, @RequestBody Account_incotermsDTO account_incotermsdto) {
		Account_incoterms domain = account_incotermsdto.toDO();
        domain.setId(account_incoterms_id);
		account_incotermsService.update(domain);
		Account_incotermsDTO dto = new Account_incotermsDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Account_incoterms" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_account/account_incoterms/fetchdefault")
	public ResponseEntity<Page<Account_incotermsDTO>> fetchDefault(Account_incotermsSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Account_incotermsDTO> list = new ArrayList<Account_incotermsDTO>();
        
        Page<Account_incoterms> domains = account_incotermsService.searchDefault(context) ;
        for(Account_incoterms account_incoterms : domains.getContent()){
            Account_incotermsDTO dto = new Account_incotermsDTO();
            dto.fromDO(account_incoterms);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
