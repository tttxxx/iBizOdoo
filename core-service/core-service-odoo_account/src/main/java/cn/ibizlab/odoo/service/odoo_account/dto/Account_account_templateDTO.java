package cn.ibizlab.odoo.service.odoo_account.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.core.odoo_account.valuerule.anno.account_account_template.*;
import cn.ibizlab.odoo.core.odoo_account.domain.Account_account_template;
import org.springframework.cglib.beans.BeanCopier;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 服务DTO对象[Account_account_templateDTO]
 */
public class Account_account_templateDTO implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [CODE]
     *
     */
    @Account_account_templateCodeDefault(info = "默认规则")
    private String code;

    @JsonIgnore
    private boolean codeDirtyFlag;

    /**
     * 属性 [TAG_IDS]
     *
     */
    @Account_account_templateTag_idsDefault(info = "默认规则")
    private String tag_ids;

    @JsonIgnore
    private boolean tag_idsDirtyFlag;

    /**
     * 属性 [NOCREATE]
     *
     */
    @Account_account_templateNocreateDefault(info = "默认规则")
    private String nocreate;

    @JsonIgnore
    private boolean nocreateDirtyFlag;

    /**
     * 属性 [TAX_IDS]
     *
     */
    @Account_account_templateTax_idsDefault(info = "默认规则")
    private String tax_ids;

    @JsonIgnore
    private boolean tax_idsDirtyFlag;

    /**
     * 属性 [CREATE_DATE]
     *
     */
    @Account_account_templateCreate_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp create_date;

    @JsonIgnore
    private boolean create_dateDirtyFlag;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @Account_account_templateDisplay_nameDefault(info = "默认规则")
    private String display_name;

    @JsonIgnore
    private boolean display_nameDirtyFlag;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @Account_account_template__last_updateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp __last_update;

    @JsonIgnore
    private boolean __last_updateDirtyFlag;

    /**
     * 属性 [NAME]
     *
     */
    @Account_account_templateNameDefault(info = "默认规则")
    private String name;

    @JsonIgnore
    private boolean nameDirtyFlag;

    /**
     * 属性 [ID]
     *
     */
    @Account_account_templateIdDefault(info = "默认规则")
    private Integer id;

    @JsonIgnore
    private boolean idDirtyFlag;

    /**
     * 属性 [NOTE]
     *
     */
    @Account_account_templateNoteDefault(info = "默认规则")
    private String note;

    @JsonIgnore
    private boolean noteDirtyFlag;

    /**
     * 属性 [WRITE_DATE]
     *
     */
    @Account_account_templateWrite_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp write_date;

    @JsonIgnore
    private boolean write_dateDirtyFlag;

    /**
     * 属性 [RECONCILE]
     *
     */
    @Account_account_templateReconcileDefault(info = "默认规则")
    private String reconcile;

    @JsonIgnore
    private boolean reconcileDirtyFlag;

    /**
     * 属性 [CHART_TEMPLATE_ID_TEXT]
     *
     */
    @Account_account_templateChart_template_id_textDefault(info = "默认规则")
    private String chart_template_id_text;

    @JsonIgnore
    private boolean chart_template_id_textDirtyFlag;

    /**
     * 属性 [CURRENCY_ID_TEXT]
     *
     */
    @Account_account_templateCurrency_id_textDefault(info = "默认规则")
    private String currency_id_text;

    @JsonIgnore
    private boolean currency_id_textDirtyFlag;

    /**
     * 属性 [USER_TYPE_ID_TEXT]
     *
     */
    @Account_account_templateUser_type_id_textDefault(info = "默认规则")
    private String user_type_id_text;

    @JsonIgnore
    private boolean user_type_id_textDirtyFlag;

    /**
     * 属性 [CREATE_UID_TEXT]
     *
     */
    @Account_account_templateCreate_uid_textDefault(info = "默认规则")
    private String create_uid_text;

    @JsonIgnore
    private boolean create_uid_textDirtyFlag;

    /**
     * 属性 [WRITE_UID_TEXT]
     *
     */
    @Account_account_templateWrite_uid_textDefault(info = "默认规则")
    private String write_uid_text;

    @JsonIgnore
    private boolean write_uid_textDirtyFlag;

    /**
     * 属性 [GROUP_ID_TEXT]
     *
     */
    @Account_account_templateGroup_id_textDefault(info = "默认规则")
    private String group_id_text;

    @JsonIgnore
    private boolean group_id_textDirtyFlag;

    /**
     * 属性 [WRITE_UID]
     *
     */
    @Account_account_templateWrite_uidDefault(info = "默认规则")
    private Integer write_uid;

    @JsonIgnore
    private boolean write_uidDirtyFlag;

    /**
     * 属性 [CREATE_UID]
     *
     */
    @Account_account_templateCreate_uidDefault(info = "默认规则")
    private Integer create_uid;

    @JsonIgnore
    private boolean create_uidDirtyFlag;

    /**
     * 属性 [CHART_TEMPLATE_ID]
     *
     */
    @Account_account_templateChart_template_idDefault(info = "默认规则")
    private Integer chart_template_id;

    @JsonIgnore
    private boolean chart_template_idDirtyFlag;

    /**
     * 属性 [CURRENCY_ID]
     *
     */
    @Account_account_templateCurrency_idDefault(info = "默认规则")
    private Integer currency_id;

    @JsonIgnore
    private boolean currency_idDirtyFlag;

    /**
     * 属性 [USER_TYPE_ID]
     *
     */
    @Account_account_templateUser_type_idDefault(info = "默认规则")
    private Integer user_type_id;

    @JsonIgnore
    private boolean user_type_idDirtyFlag;

    /**
     * 属性 [GROUP_ID]
     *
     */
    @Account_account_templateGroup_idDefault(info = "默认规则")
    private Integer group_id;

    @JsonIgnore
    private boolean group_idDirtyFlag;


    /**
     * 获取 [CODE]
     */
    @JsonProperty("code")
    public String getCode(){
        return code ;
    }

    /**
     * 设置 [CODE]
     */
    @JsonProperty("code")
    public void setCode(String  code){
        this.code = code ;
        this.codeDirtyFlag = true ;
    }

    /**
     * 获取 [CODE]脏标记
     */
    @JsonIgnore
    public boolean getCodeDirtyFlag(){
        return codeDirtyFlag ;
    }

    /**
     * 获取 [TAG_IDS]
     */
    @JsonProperty("tag_ids")
    public String getTag_ids(){
        return tag_ids ;
    }

    /**
     * 设置 [TAG_IDS]
     */
    @JsonProperty("tag_ids")
    public void setTag_ids(String  tag_ids){
        this.tag_ids = tag_ids ;
        this.tag_idsDirtyFlag = true ;
    }

    /**
     * 获取 [TAG_IDS]脏标记
     */
    @JsonIgnore
    public boolean getTag_idsDirtyFlag(){
        return tag_idsDirtyFlag ;
    }

    /**
     * 获取 [NOCREATE]
     */
    @JsonProperty("nocreate")
    public String getNocreate(){
        return nocreate ;
    }

    /**
     * 设置 [NOCREATE]
     */
    @JsonProperty("nocreate")
    public void setNocreate(String  nocreate){
        this.nocreate = nocreate ;
        this.nocreateDirtyFlag = true ;
    }

    /**
     * 获取 [NOCREATE]脏标记
     */
    @JsonIgnore
    public boolean getNocreateDirtyFlag(){
        return nocreateDirtyFlag ;
    }

    /**
     * 获取 [TAX_IDS]
     */
    @JsonProperty("tax_ids")
    public String getTax_ids(){
        return tax_ids ;
    }

    /**
     * 设置 [TAX_IDS]
     */
    @JsonProperty("tax_ids")
    public void setTax_ids(String  tax_ids){
        this.tax_ids = tax_ids ;
        this.tax_idsDirtyFlag = true ;
    }

    /**
     * 获取 [TAX_IDS]脏标记
     */
    @JsonIgnore
    public boolean getTax_idsDirtyFlag(){
        return tax_idsDirtyFlag ;
    }

    /**
     * 获取 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return create_date ;
    }

    /**
     * 设置 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return create_dateDirtyFlag ;
    }

    /**
     * 获取 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return display_name ;
    }

    /**
     * 设置 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

    /**
     * 获取 [DISPLAY_NAME]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return display_nameDirtyFlag ;
    }

    /**
     * 获取 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return __last_update ;
    }

    /**
     * 设置 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

    /**
     * 获取 [__LAST_UPDATE]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return __last_updateDirtyFlag ;
    }

    /**
     * 获取 [NAME]
     */
    @JsonProperty("name")
    public String getName(){
        return name ;
    }

    /**
     * 设置 [NAME]
     */
    @JsonProperty("name")
    public void setName(String  name){
        this.name = name ;
        this.nameDirtyFlag = true ;
    }

    /**
     * 获取 [NAME]脏标记
     */
    @JsonIgnore
    public boolean getNameDirtyFlag(){
        return nameDirtyFlag ;
    }

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

    /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return idDirtyFlag ;
    }

    /**
     * 获取 [NOTE]
     */
    @JsonProperty("note")
    public String getNote(){
        return note ;
    }

    /**
     * 设置 [NOTE]
     */
    @JsonProperty("note")
    public void setNote(String  note){
        this.note = note ;
        this.noteDirtyFlag = true ;
    }

    /**
     * 获取 [NOTE]脏标记
     */
    @JsonIgnore
    public boolean getNoteDirtyFlag(){
        return noteDirtyFlag ;
    }

    /**
     * 获取 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return write_date ;
    }

    /**
     * 设置 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return write_dateDirtyFlag ;
    }

    /**
     * 获取 [RECONCILE]
     */
    @JsonProperty("reconcile")
    public String getReconcile(){
        return reconcile ;
    }

    /**
     * 设置 [RECONCILE]
     */
    @JsonProperty("reconcile")
    public void setReconcile(String  reconcile){
        this.reconcile = reconcile ;
        this.reconcileDirtyFlag = true ;
    }

    /**
     * 获取 [RECONCILE]脏标记
     */
    @JsonIgnore
    public boolean getReconcileDirtyFlag(){
        return reconcileDirtyFlag ;
    }

    /**
     * 获取 [CHART_TEMPLATE_ID_TEXT]
     */
    @JsonProperty("chart_template_id_text")
    public String getChart_template_id_text(){
        return chart_template_id_text ;
    }

    /**
     * 设置 [CHART_TEMPLATE_ID_TEXT]
     */
    @JsonProperty("chart_template_id_text")
    public void setChart_template_id_text(String  chart_template_id_text){
        this.chart_template_id_text = chart_template_id_text ;
        this.chart_template_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [CHART_TEMPLATE_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getChart_template_id_textDirtyFlag(){
        return chart_template_id_textDirtyFlag ;
    }

    /**
     * 获取 [CURRENCY_ID_TEXT]
     */
    @JsonProperty("currency_id_text")
    public String getCurrency_id_text(){
        return currency_id_text ;
    }

    /**
     * 设置 [CURRENCY_ID_TEXT]
     */
    @JsonProperty("currency_id_text")
    public void setCurrency_id_text(String  currency_id_text){
        this.currency_id_text = currency_id_text ;
        this.currency_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [CURRENCY_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getCurrency_id_textDirtyFlag(){
        return currency_id_textDirtyFlag ;
    }

    /**
     * 获取 [USER_TYPE_ID_TEXT]
     */
    @JsonProperty("user_type_id_text")
    public String getUser_type_id_text(){
        return user_type_id_text ;
    }

    /**
     * 设置 [USER_TYPE_ID_TEXT]
     */
    @JsonProperty("user_type_id_text")
    public void setUser_type_id_text(String  user_type_id_text){
        this.user_type_id_text = user_type_id_text ;
        this.user_type_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [USER_TYPE_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getUser_type_id_textDirtyFlag(){
        return user_type_id_textDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return create_uid_text ;
    }

    /**
     * 设置 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return create_uid_textDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return write_uid_text ;
    }

    /**
     * 设置 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return write_uid_textDirtyFlag ;
    }

    /**
     * 获取 [GROUP_ID_TEXT]
     */
    @JsonProperty("group_id_text")
    public String getGroup_id_text(){
        return group_id_text ;
    }

    /**
     * 设置 [GROUP_ID_TEXT]
     */
    @JsonProperty("group_id_text")
    public void setGroup_id_text(String  group_id_text){
        this.group_id_text = group_id_text ;
        this.group_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [GROUP_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getGroup_id_textDirtyFlag(){
        return group_id_textDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return write_uid ;
    }

    /**
     * 设置 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return write_uidDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return create_uid ;
    }

    /**
     * 设置 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return create_uidDirtyFlag ;
    }

    /**
     * 获取 [CHART_TEMPLATE_ID]
     */
    @JsonProperty("chart_template_id")
    public Integer getChart_template_id(){
        return chart_template_id ;
    }

    /**
     * 设置 [CHART_TEMPLATE_ID]
     */
    @JsonProperty("chart_template_id")
    public void setChart_template_id(Integer  chart_template_id){
        this.chart_template_id = chart_template_id ;
        this.chart_template_idDirtyFlag = true ;
    }

    /**
     * 获取 [CHART_TEMPLATE_ID]脏标记
     */
    @JsonIgnore
    public boolean getChart_template_idDirtyFlag(){
        return chart_template_idDirtyFlag ;
    }

    /**
     * 获取 [CURRENCY_ID]
     */
    @JsonProperty("currency_id")
    public Integer getCurrency_id(){
        return currency_id ;
    }

    /**
     * 设置 [CURRENCY_ID]
     */
    @JsonProperty("currency_id")
    public void setCurrency_id(Integer  currency_id){
        this.currency_id = currency_id ;
        this.currency_idDirtyFlag = true ;
    }

    /**
     * 获取 [CURRENCY_ID]脏标记
     */
    @JsonIgnore
    public boolean getCurrency_idDirtyFlag(){
        return currency_idDirtyFlag ;
    }

    /**
     * 获取 [USER_TYPE_ID]
     */
    @JsonProperty("user_type_id")
    public Integer getUser_type_id(){
        return user_type_id ;
    }

    /**
     * 设置 [USER_TYPE_ID]
     */
    @JsonProperty("user_type_id")
    public void setUser_type_id(Integer  user_type_id){
        this.user_type_id = user_type_id ;
        this.user_type_idDirtyFlag = true ;
    }

    /**
     * 获取 [USER_TYPE_ID]脏标记
     */
    @JsonIgnore
    public boolean getUser_type_idDirtyFlag(){
        return user_type_idDirtyFlag ;
    }

    /**
     * 获取 [GROUP_ID]
     */
    @JsonProperty("group_id")
    public Integer getGroup_id(){
        return group_id ;
    }

    /**
     * 设置 [GROUP_ID]
     */
    @JsonProperty("group_id")
    public void setGroup_id(Integer  group_id){
        this.group_id = group_id ;
        this.group_idDirtyFlag = true ;
    }

    /**
     * 获取 [GROUP_ID]脏标记
     */
    @JsonIgnore
    public boolean getGroup_idDirtyFlag(){
        return group_idDirtyFlag ;
    }



    public Account_account_template toDO() {
        Account_account_template srfdomain = new Account_account_template();
        if(getCodeDirtyFlag())
            srfdomain.setCode(code);
        if(getTag_idsDirtyFlag())
            srfdomain.setTag_ids(tag_ids);
        if(getNocreateDirtyFlag())
            srfdomain.setNocreate(nocreate);
        if(getTax_idsDirtyFlag())
            srfdomain.setTax_ids(tax_ids);
        if(getCreate_dateDirtyFlag())
            srfdomain.setCreate_date(create_date);
        if(getDisplay_nameDirtyFlag())
            srfdomain.setDisplay_name(display_name);
        if(get__last_updateDirtyFlag())
            srfdomain.set__last_update(__last_update);
        if(getNameDirtyFlag())
            srfdomain.setName(name);
        if(getIdDirtyFlag())
            srfdomain.setId(id);
        if(getNoteDirtyFlag())
            srfdomain.setNote(note);
        if(getWrite_dateDirtyFlag())
            srfdomain.setWrite_date(write_date);
        if(getReconcileDirtyFlag())
            srfdomain.setReconcile(reconcile);
        if(getChart_template_id_textDirtyFlag())
            srfdomain.setChart_template_id_text(chart_template_id_text);
        if(getCurrency_id_textDirtyFlag())
            srfdomain.setCurrency_id_text(currency_id_text);
        if(getUser_type_id_textDirtyFlag())
            srfdomain.setUser_type_id_text(user_type_id_text);
        if(getCreate_uid_textDirtyFlag())
            srfdomain.setCreate_uid_text(create_uid_text);
        if(getWrite_uid_textDirtyFlag())
            srfdomain.setWrite_uid_text(write_uid_text);
        if(getGroup_id_textDirtyFlag())
            srfdomain.setGroup_id_text(group_id_text);
        if(getWrite_uidDirtyFlag())
            srfdomain.setWrite_uid(write_uid);
        if(getCreate_uidDirtyFlag())
            srfdomain.setCreate_uid(create_uid);
        if(getChart_template_idDirtyFlag())
            srfdomain.setChart_template_id(chart_template_id);
        if(getCurrency_idDirtyFlag())
            srfdomain.setCurrency_id(currency_id);
        if(getUser_type_idDirtyFlag())
            srfdomain.setUser_type_id(user_type_id);
        if(getGroup_idDirtyFlag())
            srfdomain.setGroup_id(group_id);

        return srfdomain;
    }

    public void fromDO(Account_account_template srfdomain) {
        if(srfdomain == null )
          return ;
        if(srfdomain.getCodeDirtyFlag())
            this.setCode(srfdomain.getCode());
        if(srfdomain.getTag_idsDirtyFlag())
            this.setTag_ids(srfdomain.getTag_ids());
        if(srfdomain.getNocreateDirtyFlag())
            this.setNocreate(srfdomain.getNocreate());
        if(srfdomain.getTax_idsDirtyFlag())
            this.setTax_ids(srfdomain.getTax_ids());
        if(srfdomain.getCreate_dateDirtyFlag())
            this.setCreate_date(srfdomain.getCreate_date());
        if(srfdomain.getDisplay_nameDirtyFlag())
            this.setDisplay_name(srfdomain.getDisplay_name());
        if(srfdomain.get__last_updateDirtyFlag())
            this.set__last_update(srfdomain.get__last_update());
        if(srfdomain.getNameDirtyFlag())
            this.setName(srfdomain.getName());
        if(srfdomain.getIdDirtyFlag())
            this.setId(srfdomain.getId());
        if(srfdomain.getNoteDirtyFlag())
            this.setNote(srfdomain.getNote());
        if(srfdomain.getWrite_dateDirtyFlag())
            this.setWrite_date(srfdomain.getWrite_date());
        if(srfdomain.getReconcileDirtyFlag())
            this.setReconcile(srfdomain.getReconcile());
        if(srfdomain.getChart_template_id_textDirtyFlag())
            this.setChart_template_id_text(srfdomain.getChart_template_id_text());
        if(srfdomain.getCurrency_id_textDirtyFlag())
            this.setCurrency_id_text(srfdomain.getCurrency_id_text());
        if(srfdomain.getUser_type_id_textDirtyFlag())
            this.setUser_type_id_text(srfdomain.getUser_type_id_text());
        if(srfdomain.getCreate_uid_textDirtyFlag())
            this.setCreate_uid_text(srfdomain.getCreate_uid_text());
        if(srfdomain.getWrite_uid_textDirtyFlag())
            this.setWrite_uid_text(srfdomain.getWrite_uid_text());
        if(srfdomain.getGroup_id_textDirtyFlag())
            this.setGroup_id_text(srfdomain.getGroup_id_text());
        if(srfdomain.getWrite_uidDirtyFlag())
            this.setWrite_uid(srfdomain.getWrite_uid());
        if(srfdomain.getCreate_uidDirtyFlag())
            this.setCreate_uid(srfdomain.getCreate_uid());
        if(srfdomain.getChart_template_idDirtyFlag())
            this.setChart_template_id(srfdomain.getChart_template_id());
        if(srfdomain.getCurrency_idDirtyFlag())
            this.setCurrency_id(srfdomain.getCurrency_id());
        if(srfdomain.getUser_type_idDirtyFlag())
            this.setUser_type_id(srfdomain.getUser_type_id());
        if(srfdomain.getGroup_idDirtyFlag())
            this.setGroup_id(srfdomain.getGroup_id());

    }

    public List<Account_account_templateDTO> fromDOPage(List<Account_account_template> poPage)   {
        if(poPage == null)
            return null;
        List<Account_account_templateDTO> dtos=new ArrayList<Account_account_templateDTO>();
        for(Account_account_template domain : poPage) {
            Account_account_templateDTO dto = new Account_account_templateDTO();
            dto.fromDO(domain);
            dtos.add(dto);
        }
        return dtos;
    }
}

