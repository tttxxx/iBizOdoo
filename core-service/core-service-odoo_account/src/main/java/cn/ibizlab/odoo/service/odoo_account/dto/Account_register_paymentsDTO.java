package cn.ibizlab.odoo.service.odoo_account.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.core.odoo_account.valuerule.anno.account_register_payments.*;
import cn.ibizlab.odoo.core.odoo_account.domain.Account_register_payments;
import org.springframework.cglib.beans.BeanCopier;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 服务DTO对象[Account_register_paymentsDTO]
 */
public class Account_register_paymentsDTO implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [SHOW_COMMUNICATION_FIELD]
     *
     */
    @Account_register_paymentsShow_communication_fieldDefault(info = "默认规则")
    private String show_communication_field;

    @JsonIgnore
    private boolean show_communication_fieldDirtyFlag;

    /**
     * 属性 [PAYMENT_TYPE]
     *
     */
    @Account_register_paymentsPayment_typeDefault(info = "默认规则")
    private String payment_type;

    @JsonIgnore
    private boolean payment_typeDirtyFlag;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @Account_register_paymentsDisplay_nameDefault(info = "默认规则")
    private String display_name;

    @JsonIgnore
    private boolean display_nameDirtyFlag;

    /**
     * 属性 [PARTNER_TYPE]
     *
     */
    @Account_register_paymentsPartner_typeDefault(info = "默认规则")
    private String partner_type;

    @JsonIgnore
    private boolean partner_typeDirtyFlag;

    /**
     * 属性 [COMMUNICATION]
     *
     */
    @Account_register_paymentsCommunicationDefault(info = "默认规则")
    private String communication;

    @JsonIgnore
    private boolean communicationDirtyFlag;

    /**
     * 属性 [PAYMENT_DIFFERENCE]
     *
     */
    @Account_register_paymentsPayment_differenceDefault(info = "默认规则")
    private Double payment_difference;

    @JsonIgnore
    private boolean payment_differenceDirtyFlag;

    /**
     * 属性 [SHOW_PARTNER_BANK_ACCOUNT]
     *
     */
    @Account_register_paymentsShow_partner_bank_accountDefault(info = "默认规则")
    private String show_partner_bank_account;

    @JsonIgnore
    private boolean show_partner_bank_accountDirtyFlag;

    /**
     * 属性 [GROUP_INVOICES]
     *
     */
    @Account_register_paymentsGroup_invoicesDefault(info = "默认规则")
    private String group_invoices;

    @JsonIgnore
    private boolean group_invoicesDirtyFlag;

    /**
     * 属性 [HIDE_PAYMENT_METHOD]
     *
     */
    @Account_register_paymentsHide_payment_methodDefault(info = "默认规则")
    private String hide_payment_method;

    @JsonIgnore
    private boolean hide_payment_methodDirtyFlag;

    /**
     * 属性 [INVOICE_IDS]
     *
     */
    @Account_register_paymentsInvoice_idsDefault(info = "默认规则")
    private String invoice_ids;

    @JsonIgnore
    private boolean invoice_idsDirtyFlag;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @Account_register_payments__last_updateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp __last_update;

    @JsonIgnore
    private boolean __last_updateDirtyFlag;

    /**
     * 属性 [PAYMENT_DATE]
     *
     */
    @Account_register_paymentsPayment_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    private Timestamp payment_date;

    @JsonIgnore
    private boolean payment_dateDirtyFlag;

    /**
     * 属性 [WRITEOFF_LABEL]
     *
     */
    @Account_register_paymentsWriteoff_labelDefault(info = "默认规则")
    private String writeoff_label;

    @JsonIgnore
    private boolean writeoff_labelDirtyFlag;

    /**
     * 属性 [MULTI]
     *
     */
    @Account_register_paymentsMultiDefault(info = "默认规则")
    private String multi;

    @JsonIgnore
    private boolean multiDirtyFlag;

    /**
     * 属性 [WRITE_DATE]
     *
     */
    @Account_register_paymentsWrite_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp write_date;

    @JsonIgnore
    private boolean write_dateDirtyFlag;

    /**
     * 属性 [CREATE_DATE]
     *
     */
    @Account_register_paymentsCreate_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp create_date;

    @JsonIgnore
    private boolean create_dateDirtyFlag;

    /**
     * 属性 [ID]
     *
     */
    @Account_register_paymentsIdDefault(info = "默认规则")
    private Integer id;

    @JsonIgnore
    private boolean idDirtyFlag;

    /**
     * 属性 [PAYMENT_DIFFERENCE_HANDLING]
     *
     */
    @Account_register_paymentsPayment_difference_handlingDefault(info = "默认规则")
    private String payment_difference_handling;

    @JsonIgnore
    private boolean payment_difference_handlingDirtyFlag;

    /**
     * 属性 [AMOUNT]
     *
     */
    @Account_register_paymentsAmountDefault(info = "默认规则")
    private Double amount;

    @JsonIgnore
    private boolean amountDirtyFlag;

    /**
     * 属性 [CURRENCY_ID_TEXT]
     *
     */
    @Account_register_paymentsCurrency_id_textDefault(info = "默认规则")
    private String currency_id_text;

    @JsonIgnore
    private boolean currency_id_textDirtyFlag;

    /**
     * 属性 [WRITE_UID_TEXT]
     *
     */
    @Account_register_paymentsWrite_uid_textDefault(info = "默认规则")
    private String write_uid_text;

    @JsonIgnore
    private boolean write_uid_textDirtyFlag;

    /**
     * 属性 [PAYMENT_METHOD_CODE]
     *
     */
    @Account_register_paymentsPayment_method_codeDefault(info = "默认规则")
    private String payment_method_code;

    @JsonIgnore
    private boolean payment_method_codeDirtyFlag;

    /**
     * 属性 [PAYMENT_METHOD_ID_TEXT]
     *
     */
    @Account_register_paymentsPayment_method_id_textDefault(info = "默认规则")
    private String payment_method_id_text;

    @JsonIgnore
    private boolean payment_method_id_textDirtyFlag;

    /**
     * 属性 [CREATE_UID_TEXT]
     *
     */
    @Account_register_paymentsCreate_uid_textDefault(info = "默认规则")
    private String create_uid_text;

    @JsonIgnore
    private boolean create_uid_textDirtyFlag;

    /**
     * 属性 [JOURNAL_ID_TEXT]
     *
     */
    @Account_register_paymentsJournal_id_textDefault(info = "默认规则")
    private String journal_id_text;

    @JsonIgnore
    private boolean journal_id_textDirtyFlag;

    /**
     * 属性 [PARTNER_ID_TEXT]
     *
     */
    @Account_register_paymentsPartner_id_textDefault(info = "默认规则")
    private String partner_id_text;

    @JsonIgnore
    private boolean partner_id_textDirtyFlag;

    /**
     * 属性 [WRITEOFF_ACCOUNT_ID_TEXT]
     *
     */
    @Account_register_paymentsWriteoff_account_id_textDefault(info = "默认规则")
    private String writeoff_account_id_text;

    @JsonIgnore
    private boolean writeoff_account_id_textDirtyFlag;

    /**
     * 属性 [WRITEOFF_ACCOUNT_ID]
     *
     */
    @Account_register_paymentsWriteoff_account_idDefault(info = "默认规则")
    private Integer writeoff_account_id;

    @JsonIgnore
    private boolean writeoff_account_idDirtyFlag;

    /**
     * 属性 [PARTNER_BANK_ACCOUNT_ID]
     *
     */
    @Account_register_paymentsPartner_bank_account_idDefault(info = "默认规则")
    private Integer partner_bank_account_id;

    @JsonIgnore
    private boolean partner_bank_account_idDirtyFlag;

    /**
     * 属性 [WRITE_UID]
     *
     */
    @Account_register_paymentsWrite_uidDefault(info = "默认规则")
    private Integer write_uid;

    @JsonIgnore
    private boolean write_uidDirtyFlag;

    /**
     * 属性 [CREATE_UID]
     *
     */
    @Account_register_paymentsCreate_uidDefault(info = "默认规则")
    private Integer create_uid;

    @JsonIgnore
    private boolean create_uidDirtyFlag;

    /**
     * 属性 [PARTNER_ID]
     *
     */
    @Account_register_paymentsPartner_idDefault(info = "默认规则")
    private Integer partner_id;

    @JsonIgnore
    private boolean partner_idDirtyFlag;

    /**
     * 属性 [JOURNAL_ID]
     *
     */
    @Account_register_paymentsJournal_idDefault(info = "默认规则")
    private Integer journal_id;

    @JsonIgnore
    private boolean journal_idDirtyFlag;

    /**
     * 属性 [CURRENCY_ID]
     *
     */
    @Account_register_paymentsCurrency_idDefault(info = "默认规则")
    private Integer currency_id;

    @JsonIgnore
    private boolean currency_idDirtyFlag;

    /**
     * 属性 [PAYMENT_METHOD_ID]
     *
     */
    @Account_register_paymentsPayment_method_idDefault(info = "默认规则")
    private Integer payment_method_id;

    @JsonIgnore
    private boolean payment_method_idDirtyFlag;


    /**
     * 获取 [SHOW_COMMUNICATION_FIELD]
     */
    @JsonProperty("show_communication_field")
    public String getShow_communication_field(){
        return show_communication_field ;
    }

    /**
     * 设置 [SHOW_COMMUNICATION_FIELD]
     */
    @JsonProperty("show_communication_field")
    public void setShow_communication_field(String  show_communication_field){
        this.show_communication_field = show_communication_field ;
        this.show_communication_fieldDirtyFlag = true ;
    }

    /**
     * 获取 [SHOW_COMMUNICATION_FIELD]脏标记
     */
    @JsonIgnore
    public boolean getShow_communication_fieldDirtyFlag(){
        return show_communication_fieldDirtyFlag ;
    }

    /**
     * 获取 [PAYMENT_TYPE]
     */
    @JsonProperty("payment_type")
    public String getPayment_type(){
        return payment_type ;
    }

    /**
     * 设置 [PAYMENT_TYPE]
     */
    @JsonProperty("payment_type")
    public void setPayment_type(String  payment_type){
        this.payment_type = payment_type ;
        this.payment_typeDirtyFlag = true ;
    }

    /**
     * 获取 [PAYMENT_TYPE]脏标记
     */
    @JsonIgnore
    public boolean getPayment_typeDirtyFlag(){
        return payment_typeDirtyFlag ;
    }

    /**
     * 获取 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return display_name ;
    }

    /**
     * 设置 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

    /**
     * 获取 [DISPLAY_NAME]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return display_nameDirtyFlag ;
    }

    /**
     * 获取 [PARTNER_TYPE]
     */
    @JsonProperty("partner_type")
    public String getPartner_type(){
        return partner_type ;
    }

    /**
     * 设置 [PARTNER_TYPE]
     */
    @JsonProperty("partner_type")
    public void setPartner_type(String  partner_type){
        this.partner_type = partner_type ;
        this.partner_typeDirtyFlag = true ;
    }

    /**
     * 获取 [PARTNER_TYPE]脏标记
     */
    @JsonIgnore
    public boolean getPartner_typeDirtyFlag(){
        return partner_typeDirtyFlag ;
    }

    /**
     * 获取 [COMMUNICATION]
     */
    @JsonProperty("communication")
    public String getCommunication(){
        return communication ;
    }

    /**
     * 设置 [COMMUNICATION]
     */
    @JsonProperty("communication")
    public void setCommunication(String  communication){
        this.communication = communication ;
        this.communicationDirtyFlag = true ;
    }

    /**
     * 获取 [COMMUNICATION]脏标记
     */
    @JsonIgnore
    public boolean getCommunicationDirtyFlag(){
        return communicationDirtyFlag ;
    }

    /**
     * 获取 [PAYMENT_DIFFERENCE]
     */
    @JsonProperty("payment_difference")
    public Double getPayment_difference(){
        return payment_difference ;
    }

    /**
     * 设置 [PAYMENT_DIFFERENCE]
     */
    @JsonProperty("payment_difference")
    public void setPayment_difference(Double  payment_difference){
        this.payment_difference = payment_difference ;
        this.payment_differenceDirtyFlag = true ;
    }

    /**
     * 获取 [PAYMENT_DIFFERENCE]脏标记
     */
    @JsonIgnore
    public boolean getPayment_differenceDirtyFlag(){
        return payment_differenceDirtyFlag ;
    }

    /**
     * 获取 [SHOW_PARTNER_BANK_ACCOUNT]
     */
    @JsonProperty("show_partner_bank_account")
    public String getShow_partner_bank_account(){
        return show_partner_bank_account ;
    }

    /**
     * 设置 [SHOW_PARTNER_BANK_ACCOUNT]
     */
    @JsonProperty("show_partner_bank_account")
    public void setShow_partner_bank_account(String  show_partner_bank_account){
        this.show_partner_bank_account = show_partner_bank_account ;
        this.show_partner_bank_accountDirtyFlag = true ;
    }

    /**
     * 获取 [SHOW_PARTNER_BANK_ACCOUNT]脏标记
     */
    @JsonIgnore
    public boolean getShow_partner_bank_accountDirtyFlag(){
        return show_partner_bank_accountDirtyFlag ;
    }

    /**
     * 获取 [GROUP_INVOICES]
     */
    @JsonProperty("group_invoices")
    public String getGroup_invoices(){
        return group_invoices ;
    }

    /**
     * 设置 [GROUP_INVOICES]
     */
    @JsonProperty("group_invoices")
    public void setGroup_invoices(String  group_invoices){
        this.group_invoices = group_invoices ;
        this.group_invoicesDirtyFlag = true ;
    }

    /**
     * 获取 [GROUP_INVOICES]脏标记
     */
    @JsonIgnore
    public boolean getGroup_invoicesDirtyFlag(){
        return group_invoicesDirtyFlag ;
    }

    /**
     * 获取 [HIDE_PAYMENT_METHOD]
     */
    @JsonProperty("hide_payment_method")
    public String getHide_payment_method(){
        return hide_payment_method ;
    }

    /**
     * 设置 [HIDE_PAYMENT_METHOD]
     */
    @JsonProperty("hide_payment_method")
    public void setHide_payment_method(String  hide_payment_method){
        this.hide_payment_method = hide_payment_method ;
        this.hide_payment_methodDirtyFlag = true ;
    }

    /**
     * 获取 [HIDE_PAYMENT_METHOD]脏标记
     */
    @JsonIgnore
    public boolean getHide_payment_methodDirtyFlag(){
        return hide_payment_methodDirtyFlag ;
    }

    /**
     * 获取 [INVOICE_IDS]
     */
    @JsonProperty("invoice_ids")
    public String getInvoice_ids(){
        return invoice_ids ;
    }

    /**
     * 设置 [INVOICE_IDS]
     */
    @JsonProperty("invoice_ids")
    public void setInvoice_ids(String  invoice_ids){
        this.invoice_ids = invoice_ids ;
        this.invoice_idsDirtyFlag = true ;
    }

    /**
     * 获取 [INVOICE_IDS]脏标记
     */
    @JsonIgnore
    public boolean getInvoice_idsDirtyFlag(){
        return invoice_idsDirtyFlag ;
    }

    /**
     * 获取 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return __last_update ;
    }

    /**
     * 设置 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

    /**
     * 获取 [__LAST_UPDATE]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return __last_updateDirtyFlag ;
    }

    /**
     * 获取 [PAYMENT_DATE]
     */
    @JsonProperty("payment_date")
    public Timestamp getPayment_date(){
        return payment_date ;
    }

    /**
     * 设置 [PAYMENT_DATE]
     */
    @JsonProperty("payment_date")
    public void setPayment_date(Timestamp  payment_date){
        this.payment_date = payment_date ;
        this.payment_dateDirtyFlag = true ;
    }

    /**
     * 获取 [PAYMENT_DATE]脏标记
     */
    @JsonIgnore
    public boolean getPayment_dateDirtyFlag(){
        return payment_dateDirtyFlag ;
    }

    /**
     * 获取 [WRITEOFF_LABEL]
     */
    @JsonProperty("writeoff_label")
    public String getWriteoff_label(){
        return writeoff_label ;
    }

    /**
     * 设置 [WRITEOFF_LABEL]
     */
    @JsonProperty("writeoff_label")
    public void setWriteoff_label(String  writeoff_label){
        this.writeoff_label = writeoff_label ;
        this.writeoff_labelDirtyFlag = true ;
    }

    /**
     * 获取 [WRITEOFF_LABEL]脏标记
     */
    @JsonIgnore
    public boolean getWriteoff_labelDirtyFlag(){
        return writeoff_labelDirtyFlag ;
    }

    /**
     * 获取 [MULTI]
     */
    @JsonProperty("multi")
    public String getMulti(){
        return multi ;
    }

    /**
     * 设置 [MULTI]
     */
    @JsonProperty("multi")
    public void setMulti(String  multi){
        this.multi = multi ;
        this.multiDirtyFlag = true ;
    }

    /**
     * 获取 [MULTI]脏标记
     */
    @JsonIgnore
    public boolean getMultiDirtyFlag(){
        return multiDirtyFlag ;
    }

    /**
     * 获取 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return write_date ;
    }

    /**
     * 设置 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return write_dateDirtyFlag ;
    }

    /**
     * 获取 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return create_date ;
    }

    /**
     * 设置 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return create_dateDirtyFlag ;
    }

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

    /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return idDirtyFlag ;
    }

    /**
     * 获取 [PAYMENT_DIFFERENCE_HANDLING]
     */
    @JsonProperty("payment_difference_handling")
    public String getPayment_difference_handling(){
        return payment_difference_handling ;
    }

    /**
     * 设置 [PAYMENT_DIFFERENCE_HANDLING]
     */
    @JsonProperty("payment_difference_handling")
    public void setPayment_difference_handling(String  payment_difference_handling){
        this.payment_difference_handling = payment_difference_handling ;
        this.payment_difference_handlingDirtyFlag = true ;
    }

    /**
     * 获取 [PAYMENT_DIFFERENCE_HANDLING]脏标记
     */
    @JsonIgnore
    public boolean getPayment_difference_handlingDirtyFlag(){
        return payment_difference_handlingDirtyFlag ;
    }

    /**
     * 获取 [AMOUNT]
     */
    @JsonProperty("amount")
    public Double getAmount(){
        return amount ;
    }

    /**
     * 设置 [AMOUNT]
     */
    @JsonProperty("amount")
    public void setAmount(Double  amount){
        this.amount = amount ;
        this.amountDirtyFlag = true ;
    }

    /**
     * 获取 [AMOUNT]脏标记
     */
    @JsonIgnore
    public boolean getAmountDirtyFlag(){
        return amountDirtyFlag ;
    }

    /**
     * 获取 [CURRENCY_ID_TEXT]
     */
    @JsonProperty("currency_id_text")
    public String getCurrency_id_text(){
        return currency_id_text ;
    }

    /**
     * 设置 [CURRENCY_ID_TEXT]
     */
    @JsonProperty("currency_id_text")
    public void setCurrency_id_text(String  currency_id_text){
        this.currency_id_text = currency_id_text ;
        this.currency_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [CURRENCY_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getCurrency_id_textDirtyFlag(){
        return currency_id_textDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return write_uid_text ;
    }

    /**
     * 设置 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return write_uid_textDirtyFlag ;
    }

    /**
     * 获取 [PAYMENT_METHOD_CODE]
     */
    @JsonProperty("payment_method_code")
    public String getPayment_method_code(){
        return payment_method_code ;
    }

    /**
     * 设置 [PAYMENT_METHOD_CODE]
     */
    @JsonProperty("payment_method_code")
    public void setPayment_method_code(String  payment_method_code){
        this.payment_method_code = payment_method_code ;
        this.payment_method_codeDirtyFlag = true ;
    }

    /**
     * 获取 [PAYMENT_METHOD_CODE]脏标记
     */
    @JsonIgnore
    public boolean getPayment_method_codeDirtyFlag(){
        return payment_method_codeDirtyFlag ;
    }

    /**
     * 获取 [PAYMENT_METHOD_ID_TEXT]
     */
    @JsonProperty("payment_method_id_text")
    public String getPayment_method_id_text(){
        return payment_method_id_text ;
    }

    /**
     * 设置 [PAYMENT_METHOD_ID_TEXT]
     */
    @JsonProperty("payment_method_id_text")
    public void setPayment_method_id_text(String  payment_method_id_text){
        this.payment_method_id_text = payment_method_id_text ;
        this.payment_method_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [PAYMENT_METHOD_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getPayment_method_id_textDirtyFlag(){
        return payment_method_id_textDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return create_uid_text ;
    }

    /**
     * 设置 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return create_uid_textDirtyFlag ;
    }

    /**
     * 获取 [JOURNAL_ID_TEXT]
     */
    @JsonProperty("journal_id_text")
    public String getJournal_id_text(){
        return journal_id_text ;
    }

    /**
     * 设置 [JOURNAL_ID_TEXT]
     */
    @JsonProperty("journal_id_text")
    public void setJournal_id_text(String  journal_id_text){
        this.journal_id_text = journal_id_text ;
        this.journal_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [JOURNAL_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getJournal_id_textDirtyFlag(){
        return journal_id_textDirtyFlag ;
    }

    /**
     * 获取 [PARTNER_ID_TEXT]
     */
    @JsonProperty("partner_id_text")
    public String getPartner_id_text(){
        return partner_id_text ;
    }

    /**
     * 设置 [PARTNER_ID_TEXT]
     */
    @JsonProperty("partner_id_text")
    public void setPartner_id_text(String  partner_id_text){
        this.partner_id_text = partner_id_text ;
        this.partner_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [PARTNER_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getPartner_id_textDirtyFlag(){
        return partner_id_textDirtyFlag ;
    }

    /**
     * 获取 [WRITEOFF_ACCOUNT_ID_TEXT]
     */
    @JsonProperty("writeoff_account_id_text")
    public String getWriteoff_account_id_text(){
        return writeoff_account_id_text ;
    }

    /**
     * 设置 [WRITEOFF_ACCOUNT_ID_TEXT]
     */
    @JsonProperty("writeoff_account_id_text")
    public void setWriteoff_account_id_text(String  writeoff_account_id_text){
        this.writeoff_account_id_text = writeoff_account_id_text ;
        this.writeoff_account_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [WRITEOFF_ACCOUNT_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getWriteoff_account_id_textDirtyFlag(){
        return writeoff_account_id_textDirtyFlag ;
    }

    /**
     * 获取 [WRITEOFF_ACCOUNT_ID]
     */
    @JsonProperty("writeoff_account_id")
    public Integer getWriteoff_account_id(){
        return writeoff_account_id ;
    }

    /**
     * 设置 [WRITEOFF_ACCOUNT_ID]
     */
    @JsonProperty("writeoff_account_id")
    public void setWriteoff_account_id(Integer  writeoff_account_id){
        this.writeoff_account_id = writeoff_account_id ;
        this.writeoff_account_idDirtyFlag = true ;
    }

    /**
     * 获取 [WRITEOFF_ACCOUNT_ID]脏标记
     */
    @JsonIgnore
    public boolean getWriteoff_account_idDirtyFlag(){
        return writeoff_account_idDirtyFlag ;
    }

    /**
     * 获取 [PARTNER_BANK_ACCOUNT_ID]
     */
    @JsonProperty("partner_bank_account_id")
    public Integer getPartner_bank_account_id(){
        return partner_bank_account_id ;
    }

    /**
     * 设置 [PARTNER_BANK_ACCOUNT_ID]
     */
    @JsonProperty("partner_bank_account_id")
    public void setPartner_bank_account_id(Integer  partner_bank_account_id){
        this.partner_bank_account_id = partner_bank_account_id ;
        this.partner_bank_account_idDirtyFlag = true ;
    }

    /**
     * 获取 [PARTNER_BANK_ACCOUNT_ID]脏标记
     */
    @JsonIgnore
    public boolean getPartner_bank_account_idDirtyFlag(){
        return partner_bank_account_idDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return write_uid ;
    }

    /**
     * 设置 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return write_uidDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return create_uid ;
    }

    /**
     * 设置 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return create_uidDirtyFlag ;
    }

    /**
     * 获取 [PARTNER_ID]
     */
    @JsonProperty("partner_id")
    public Integer getPartner_id(){
        return partner_id ;
    }

    /**
     * 设置 [PARTNER_ID]
     */
    @JsonProperty("partner_id")
    public void setPartner_id(Integer  partner_id){
        this.partner_id = partner_id ;
        this.partner_idDirtyFlag = true ;
    }

    /**
     * 获取 [PARTNER_ID]脏标记
     */
    @JsonIgnore
    public boolean getPartner_idDirtyFlag(){
        return partner_idDirtyFlag ;
    }

    /**
     * 获取 [JOURNAL_ID]
     */
    @JsonProperty("journal_id")
    public Integer getJournal_id(){
        return journal_id ;
    }

    /**
     * 设置 [JOURNAL_ID]
     */
    @JsonProperty("journal_id")
    public void setJournal_id(Integer  journal_id){
        this.journal_id = journal_id ;
        this.journal_idDirtyFlag = true ;
    }

    /**
     * 获取 [JOURNAL_ID]脏标记
     */
    @JsonIgnore
    public boolean getJournal_idDirtyFlag(){
        return journal_idDirtyFlag ;
    }

    /**
     * 获取 [CURRENCY_ID]
     */
    @JsonProperty("currency_id")
    public Integer getCurrency_id(){
        return currency_id ;
    }

    /**
     * 设置 [CURRENCY_ID]
     */
    @JsonProperty("currency_id")
    public void setCurrency_id(Integer  currency_id){
        this.currency_id = currency_id ;
        this.currency_idDirtyFlag = true ;
    }

    /**
     * 获取 [CURRENCY_ID]脏标记
     */
    @JsonIgnore
    public boolean getCurrency_idDirtyFlag(){
        return currency_idDirtyFlag ;
    }

    /**
     * 获取 [PAYMENT_METHOD_ID]
     */
    @JsonProperty("payment_method_id")
    public Integer getPayment_method_id(){
        return payment_method_id ;
    }

    /**
     * 设置 [PAYMENT_METHOD_ID]
     */
    @JsonProperty("payment_method_id")
    public void setPayment_method_id(Integer  payment_method_id){
        this.payment_method_id = payment_method_id ;
        this.payment_method_idDirtyFlag = true ;
    }

    /**
     * 获取 [PAYMENT_METHOD_ID]脏标记
     */
    @JsonIgnore
    public boolean getPayment_method_idDirtyFlag(){
        return payment_method_idDirtyFlag ;
    }



    public Account_register_payments toDO() {
        Account_register_payments srfdomain = new Account_register_payments();
        if(getShow_communication_fieldDirtyFlag())
            srfdomain.setShow_communication_field(show_communication_field);
        if(getPayment_typeDirtyFlag())
            srfdomain.setPayment_type(payment_type);
        if(getDisplay_nameDirtyFlag())
            srfdomain.setDisplay_name(display_name);
        if(getPartner_typeDirtyFlag())
            srfdomain.setPartner_type(partner_type);
        if(getCommunicationDirtyFlag())
            srfdomain.setCommunication(communication);
        if(getPayment_differenceDirtyFlag())
            srfdomain.setPayment_difference(payment_difference);
        if(getShow_partner_bank_accountDirtyFlag())
            srfdomain.setShow_partner_bank_account(show_partner_bank_account);
        if(getGroup_invoicesDirtyFlag())
            srfdomain.setGroup_invoices(group_invoices);
        if(getHide_payment_methodDirtyFlag())
            srfdomain.setHide_payment_method(hide_payment_method);
        if(getInvoice_idsDirtyFlag())
            srfdomain.setInvoice_ids(invoice_ids);
        if(get__last_updateDirtyFlag())
            srfdomain.set__last_update(__last_update);
        if(getPayment_dateDirtyFlag())
            srfdomain.setPayment_date(payment_date);
        if(getWriteoff_labelDirtyFlag())
            srfdomain.setWriteoff_label(writeoff_label);
        if(getMultiDirtyFlag())
            srfdomain.setMulti(multi);
        if(getWrite_dateDirtyFlag())
            srfdomain.setWrite_date(write_date);
        if(getCreate_dateDirtyFlag())
            srfdomain.setCreate_date(create_date);
        if(getIdDirtyFlag())
            srfdomain.setId(id);
        if(getPayment_difference_handlingDirtyFlag())
            srfdomain.setPayment_difference_handling(payment_difference_handling);
        if(getAmountDirtyFlag())
            srfdomain.setAmount(amount);
        if(getCurrency_id_textDirtyFlag())
            srfdomain.setCurrency_id_text(currency_id_text);
        if(getWrite_uid_textDirtyFlag())
            srfdomain.setWrite_uid_text(write_uid_text);
        if(getPayment_method_codeDirtyFlag())
            srfdomain.setPayment_method_code(payment_method_code);
        if(getPayment_method_id_textDirtyFlag())
            srfdomain.setPayment_method_id_text(payment_method_id_text);
        if(getCreate_uid_textDirtyFlag())
            srfdomain.setCreate_uid_text(create_uid_text);
        if(getJournal_id_textDirtyFlag())
            srfdomain.setJournal_id_text(journal_id_text);
        if(getPartner_id_textDirtyFlag())
            srfdomain.setPartner_id_text(partner_id_text);
        if(getWriteoff_account_id_textDirtyFlag())
            srfdomain.setWriteoff_account_id_text(writeoff_account_id_text);
        if(getWriteoff_account_idDirtyFlag())
            srfdomain.setWriteoff_account_id(writeoff_account_id);
        if(getPartner_bank_account_idDirtyFlag())
            srfdomain.setPartner_bank_account_id(partner_bank_account_id);
        if(getWrite_uidDirtyFlag())
            srfdomain.setWrite_uid(write_uid);
        if(getCreate_uidDirtyFlag())
            srfdomain.setCreate_uid(create_uid);
        if(getPartner_idDirtyFlag())
            srfdomain.setPartner_id(partner_id);
        if(getJournal_idDirtyFlag())
            srfdomain.setJournal_id(journal_id);
        if(getCurrency_idDirtyFlag())
            srfdomain.setCurrency_id(currency_id);
        if(getPayment_method_idDirtyFlag())
            srfdomain.setPayment_method_id(payment_method_id);

        return srfdomain;
    }

    public void fromDO(Account_register_payments srfdomain) {
        if(srfdomain == null )
          return ;
        if(srfdomain.getShow_communication_fieldDirtyFlag())
            this.setShow_communication_field(srfdomain.getShow_communication_field());
        if(srfdomain.getPayment_typeDirtyFlag())
            this.setPayment_type(srfdomain.getPayment_type());
        if(srfdomain.getDisplay_nameDirtyFlag())
            this.setDisplay_name(srfdomain.getDisplay_name());
        if(srfdomain.getPartner_typeDirtyFlag())
            this.setPartner_type(srfdomain.getPartner_type());
        if(srfdomain.getCommunicationDirtyFlag())
            this.setCommunication(srfdomain.getCommunication());
        if(srfdomain.getPayment_differenceDirtyFlag())
            this.setPayment_difference(srfdomain.getPayment_difference());
        if(srfdomain.getShow_partner_bank_accountDirtyFlag())
            this.setShow_partner_bank_account(srfdomain.getShow_partner_bank_account());
        if(srfdomain.getGroup_invoicesDirtyFlag())
            this.setGroup_invoices(srfdomain.getGroup_invoices());
        if(srfdomain.getHide_payment_methodDirtyFlag())
            this.setHide_payment_method(srfdomain.getHide_payment_method());
        if(srfdomain.getInvoice_idsDirtyFlag())
            this.setInvoice_ids(srfdomain.getInvoice_ids());
        if(srfdomain.get__last_updateDirtyFlag())
            this.set__last_update(srfdomain.get__last_update());
        if(srfdomain.getPayment_dateDirtyFlag())
            this.setPayment_date(srfdomain.getPayment_date());
        if(srfdomain.getWriteoff_labelDirtyFlag())
            this.setWriteoff_label(srfdomain.getWriteoff_label());
        if(srfdomain.getMultiDirtyFlag())
            this.setMulti(srfdomain.getMulti());
        if(srfdomain.getWrite_dateDirtyFlag())
            this.setWrite_date(srfdomain.getWrite_date());
        if(srfdomain.getCreate_dateDirtyFlag())
            this.setCreate_date(srfdomain.getCreate_date());
        if(srfdomain.getIdDirtyFlag())
            this.setId(srfdomain.getId());
        if(srfdomain.getPayment_difference_handlingDirtyFlag())
            this.setPayment_difference_handling(srfdomain.getPayment_difference_handling());
        if(srfdomain.getAmountDirtyFlag())
            this.setAmount(srfdomain.getAmount());
        if(srfdomain.getCurrency_id_textDirtyFlag())
            this.setCurrency_id_text(srfdomain.getCurrency_id_text());
        if(srfdomain.getWrite_uid_textDirtyFlag())
            this.setWrite_uid_text(srfdomain.getWrite_uid_text());
        if(srfdomain.getPayment_method_codeDirtyFlag())
            this.setPayment_method_code(srfdomain.getPayment_method_code());
        if(srfdomain.getPayment_method_id_textDirtyFlag())
            this.setPayment_method_id_text(srfdomain.getPayment_method_id_text());
        if(srfdomain.getCreate_uid_textDirtyFlag())
            this.setCreate_uid_text(srfdomain.getCreate_uid_text());
        if(srfdomain.getJournal_id_textDirtyFlag())
            this.setJournal_id_text(srfdomain.getJournal_id_text());
        if(srfdomain.getPartner_id_textDirtyFlag())
            this.setPartner_id_text(srfdomain.getPartner_id_text());
        if(srfdomain.getWriteoff_account_id_textDirtyFlag())
            this.setWriteoff_account_id_text(srfdomain.getWriteoff_account_id_text());
        if(srfdomain.getWriteoff_account_idDirtyFlag())
            this.setWriteoff_account_id(srfdomain.getWriteoff_account_id());
        if(srfdomain.getPartner_bank_account_idDirtyFlag())
            this.setPartner_bank_account_id(srfdomain.getPartner_bank_account_id());
        if(srfdomain.getWrite_uidDirtyFlag())
            this.setWrite_uid(srfdomain.getWrite_uid());
        if(srfdomain.getCreate_uidDirtyFlag())
            this.setCreate_uid(srfdomain.getCreate_uid());
        if(srfdomain.getPartner_idDirtyFlag())
            this.setPartner_id(srfdomain.getPartner_id());
        if(srfdomain.getJournal_idDirtyFlag())
            this.setJournal_id(srfdomain.getJournal_id());
        if(srfdomain.getCurrency_idDirtyFlag())
            this.setCurrency_id(srfdomain.getCurrency_id());
        if(srfdomain.getPayment_method_idDirtyFlag())
            this.setPayment_method_id(srfdomain.getPayment_method_id());

    }

    public List<Account_register_paymentsDTO> fromDOPage(List<Account_register_payments> poPage)   {
        if(poPage == null)
            return null;
        List<Account_register_paymentsDTO> dtos=new ArrayList<Account_register_paymentsDTO>();
        for(Account_register_payments domain : poPage) {
            Account_register_paymentsDTO dto = new Account_register_paymentsDTO();
            dto.fromDO(domain);
            dtos.add(dto);
        }
        return dtos;
    }
}

