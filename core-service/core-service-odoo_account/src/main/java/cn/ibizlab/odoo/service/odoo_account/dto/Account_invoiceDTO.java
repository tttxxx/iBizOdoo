package cn.ibizlab.odoo.service.odoo_account.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.core.odoo_account.valuerule.anno.account_invoice.*;
import cn.ibizlab.odoo.core.odoo_account.domain.Account_invoice;
import org.springframework.cglib.beans.BeanCopier;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 服务DTO对象[Account_invoiceDTO]
 */
public class Account_invoiceDTO implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [OUTSTANDING_CREDITS_DEBITS_WIDGET]
     *
     */
    @Account_invoiceOutstanding_credits_debits_widgetDefault(info = "默认规则")
    private String outstanding_credits_debits_widget;

    @JsonIgnore
    private boolean outstanding_credits_debits_widgetDirtyFlag;

    /**
     * 属性 [ACTIVITY_DATE_DEADLINE]
     *
     */
    @Account_invoiceActivity_date_deadlineDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    private Timestamp activity_date_deadline;

    @JsonIgnore
    private boolean activity_date_deadlineDirtyFlag;

    /**
     * 属性 [MESSAGE_CHANNEL_IDS]
     *
     */
    @Account_invoiceMessage_channel_idsDefault(info = "默认规则")
    private String message_channel_ids;

    @JsonIgnore
    private boolean message_channel_idsDirtyFlag;

    /**
     * 属性 [MOVE_NAME]
     *
     */
    @Account_invoiceMove_nameDefault(info = "默认规则")
    private String move_name;

    @JsonIgnore
    private boolean move_nameDirtyFlag;

    /**
     * 属性 [ACTIVITY_SUMMARY]
     *
     */
    @Account_invoiceActivity_summaryDefault(info = "默认规则")
    private String activity_summary;

    @JsonIgnore
    private boolean activity_summaryDirtyFlag;

    /**
     * 属性 [ACCESS_URL]
     *
     */
    @Account_invoiceAccess_urlDefault(info = "默认规则")
    private String access_url;

    @JsonIgnore
    private boolean access_urlDirtyFlag;

    /**
     * 属性 [PAYMENT_MOVE_LINE_IDS]
     *
     */
    @Account_invoicePayment_move_line_idsDefault(info = "默认规则")
    private String payment_move_line_ids;

    @JsonIgnore
    private boolean payment_move_line_idsDirtyFlag;

    /**
     * 属性 [ORIGIN]
     *
     */
    @Account_invoiceOriginDefault(info = "默认规则")
    private String origin;

    @JsonIgnore
    private boolean originDirtyFlag;

    /**
     * 属性 [DATE]
     *
     */
    @Account_invoiceDateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    private Timestamp date;

    @JsonIgnore
    private boolean dateDirtyFlag;

    /**
     * 属性 [AMOUNT_TOTAL_COMPANY_SIGNED]
     *
     */
    @Account_invoiceAmount_total_company_signedDefault(info = "默认规则")
    private Double amount_total_company_signed;

    @JsonIgnore
    private boolean amount_total_company_signedDirtyFlag;

    /**
     * 属性 [MESSAGE_NEEDACTION_COUNTER]
     *
     */
    @Account_invoiceMessage_needaction_counterDefault(info = "默认规则")
    private Integer message_needaction_counter;

    @JsonIgnore
    private boolean message_needaction_counterDirtyFlag;

    /**
     * 属性 [AMOUNT_UNTAXED_SIGNED]
     *
     */
    @Account_invoiceAmount_untaxed_signedDefault(info = "默认规则")
    private Double amount_untaxed_signed;

    @JsonIgnore
    private boolean amount_untaxed_signedDirtyFlag;

    /**
     * 属性 [RESIDUAL]
     *
     */
    @Account_invoiceResidualDefault(info = "默认规则")
    private Double residual;

    @JsonIgnore
    private boolean residualDirtyFlag;

    /**
     * 属性 [MESSAGE_MAIN_ATTACHMENT_ID]
     *
     */
    @Account_invoiceMessage_main_attachment_idDefault(info = "默认规则")
    private Integer message_main_attachment_id;

    @JsonIgnore
    private boolean message_main_attachment_idDirtyFlag;

    /**
     * 属性 [HAS_OUTSTANDING]
     *
     */
    @Account_invoiceHas_outstandingDefault(info = "默认规则")
    private String has_outstanding;

    @JsonIgnore
    private boolean has_outstandingDirtyFlag;

    /**
     * 属性 [COMMENT]
     *
     */
    @Account_invoiceCommentDefault(info = "默认规则")
    private String comment;

    @JsonIgnore
    private boolean commentDirtyFlag;

    /**
     * 属性 [MESSAGE_UNREAD]
     *
     */
    @Account_invoiceMessage_unreadDefault(info = "默认规则")
    private String message_unread;

    @JsonIgnore
    private boolean message_unreadDirtyFlag;

    /**
     * 属性 [ACTIVITY_TYPE_ID]
     *
     */
    @Account_invoiceActivity_type_idDefault(info = "默认规则")
    private Integer activity_type_id;

    @JsonIgnore
    private boolean activity_type_idDirtyFlag;

    /**
     * 属性 [AMOUNT_BY_GROUP]
     *
     */
    @Account_invoiceAmount_by_groupDefault(info = "默认规则")
    private byte[] amount_by_group;

    @JsonIgnore
    private boolean amount_by_groupDirtyFlag;

    /**
     * 属性 [PAYMENTS_WIDGET]
     *
     */
    @Account_invoicePayments_widgetDefault(info = "默认规则")
    private String payments_widget;

    @JsonIgnore
    private boolean payments_widgetDirtyFlag;

    /**
     * 属性 [RECONCILED]
     *
     */
    @Account_invoiceReconciledDefault(info = "默认规则")
    private String reconciled;

    @JsonIgnore
    private boolean reconciledDirtyFlag;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @Account_invoiceDisplay_nameDefault(info = "默认规则")
    private String display_name;

    @JsonIgnore
    private boolean display_nameDirtyFlag;

    /**
     * 属性 [SENT]
     *
     */
    @Account_invoiceSentDefault(info = "默认规则")
    private String sent;

    @JsonIgnore
    private boolean sentDirtyFlag;

    /**
     * 属性 [AMOUNT_TOTAL_SIGNED]
     *
     */
    @Account_invoiceAmount_total_signedDefault(info = "默认规则")
    private Double amount_total_signed;

    @JsonIgnore
    private boolean amount_total_signedDirtyFlag;

    /**
     * 属性 [INVOICE_LINE_IDS]
     *
     */
    @Account_invoiceInvoice_line_idsDefault(info = "默认规则")
    private String invoice_line_ids;

    @JsonIgnore
    private boolean invoice_line_idsDirtyFlag;

    /**
     * 属性 [WRITE_DATE]
     *
     */
    @Account_invoiceWrite_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp write_date;

    @JsonIgnore
    private boolean write_dateDirtyFlag;

    /**
     * 属性 [ACCESS_WARNING]
     *
     */
    @Account_invoiceAccess_warningDefault(info = "默认规则")
    private String access_warning;

    @JsonIgnore
    private boolean access_warningDirtyFlag;

    /**
     * 属性 [CREATE_DATE]
     *
     */
    @Account_invoiceCreate_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp create_date;

    @JsonIgnore
    private boolean create_dateDirtyFlag;

    /**
     * 属性 [ACTIVITY_STATE]
     *
     */
    @Account_invoiceActivity_stateDefault(info = "默认规则")
    private String activity_state;

    @JsonIgnore
    private boolean activity_stateDirtyFlag;

    /**
     * 属性 [RESIDUAL_SIGNED]
     *
     */
    @Account_invoiceResidual_signedDefault(info = "默认规则")
    private Double residual_signed;

    @JsonIgnore
    private boolean residual_signedDirtyFlag;

    /**
     * 属性 [MESSAGE_UNREAD_COUNTER]
     *
     */
    @Account_invoiceMessage_unread_counterDefault(info = "默认规则")
    private Integer message_unread_counter;

    @JsonIgnore
    private boolean message_unread_counterDirtyFlag;

    /**
     * 属性 [ACCESS_TOKEN]
     *
     */
    @Account_invoiceAccess_tokenDefault(info = "默认规则")
    private String access_token;

    @JsonIgnore
    private boolean access_tokenDirtyFlag;

    /**
     * 属性 [VENDOR_DISPLAY_NAME]
     *
     */
    @Account_invoiceVendor_display_nameDefault(info = "默认规则")
    private String vendor_display_name;

    @JsonIgnore
    private boolean vendor_display_nameDirtyFlag;

    /**
     * 属性 [MESSAGE_ATTACHMENT_COUNT]
     *
     */
    @Account_invoiceMessage_attachment_countDefault(info = "默认规则")
    private Integer message_attachment_count;

    @JsonIgnore
    private boolean message_attachment_countDirtyFlag;

    /**
     * 属性 [MESSAGE_IDS]
     *
     */
    @Account_invoiceMessage_idsDefault(info = "默认规则")
    private String message_ids;

    @JsonIgnore
    private boolean message_idsDirtyFlag;

    /**
     * 属性 [AMOUNT_UNTAXED]
     *
     */
    @Account_invoiceAmount_untaxedDefault(info = "默认规则")
    private Double amount_untaxed;

    @JsonIgnore
    private boolean amount_untaxedDirtyFlag;

    /**
     * 属性 [TRANSACTION_IDS]
     *
     */
    @Account_invoiceTransaction_idsDefault(info = "默认规则")
    private String transaction_ids;

    @JsonIgnore
    private boolean transaction_idsDirtyFlag;

    /**
     * 属性 [DATE_DUE]
     *
     */
    @Account_invoiceDate_dueDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    private Timestamp date_due;

    @JsonIgnore
    private boolean date_dueDirtyFlag;

    /**
     * 属性 [MESSAGE_PARTNER_IDS]
     *
     */
    @Account_invoiceMessage_partner_idsDefault(info = "默认规则")
    private String message_partner_ids;

    @JsonIgnore
    private boolean message_partner_idsDirtyFlag;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @Account_invoice__last_updateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp __last_update;

    @JsonIgnore
    private boolean __last_updateDirtyFlag;

    /**
     * 属性 [AUTHORIZED_TRANSACTION_IDS]
     *
     */
    @Account_invoiceAuthorized_transaction_idsDefault(info = "默认规则")
    private String authorized_transaction_ids;

    @JsonIgnore
    private boolean authorized_transaction_idsDirtyFlag;

    /**
     * 属性 [TAX_LINE_IDS]
     *
     */
    @Account_invoiceTax_line_idsDefault(info = "默认规则")
    private String tax_line_ids;

    @JsonIgnore
    private boolean tax_line_idsDirtyFlag;

    /**
     * 属性 [MESSAGE_FOLLOWER_IDS]
     *
     */
    @Account_invoiceMessage_follower_idsDefault(info = "默认规则")
    private String message_follower_ids;

    @JsonIgnore
    private boolean message_follower_idsDirtyFlag;

    /**
     * 属性 [SOURCE_EMAIL]
     *
     */
    @Account_invoiceSource_emailDefault(info = "默认规则")
    private String source_email;

    @JsonIgnore
    private boolean source_emailDirtyFlag;

    /**
     * 属性 [AMOUNT_TAX]
     *
     */
    @Account_invoiceAmount_taxDefault(info = "默认规则")
    private Double amount_tax;

    @JsonIgnore
    private boolean amount_taxDirtyFlag;

    /**
     * 属性 [MESSAGE_IS_FOLLOWER]
     *
     */
    @Account_invoiceMessage_is_followerDefault(info = "默认规则")
    private String message_is_follower;

    @JsonIgnore
    private boolean message_is_followerDirtyFlag;

    /**
     * 属性 [STATE]
     *
     */
    @Account_invoiceStateDefault(info = "默认规则")
    private String state;

    @JsonIgnore
    private boolean stateDirtyFlag;

    /**
     * 属性 [MESSAGE_HAS_ERROR]
     *
     */
    @Account_invoiceMessage_has_errorDefault(info = "默认规则")
    private String message_has_error;

    @JsonIgnore
    private boolean message_has_errorDirtyFlag;

    /**
     * 属性 [AMOUNT_UNTAXED_INVOICE_SIGNED]
     *
     */
    @Account_invoiceAmount_untaxed_invoice_signedDefault(info = "默认规则")
    private Double amount_untaxed_invoice_signed;

    @JsonIgnore
    private boolean amount_untaxed_invoice_signedDirtyFlag;

    /**
     * 属性 [MESSAGE_NEEDACTION]
     *
     */
    @Account_invoiceMessage_needactionDefault(info = "默认规则")
    private String message_needaction;

    @JsonIgnore
    private boolean message_needactionDirtyFlag;

    /**
     * 属性 [SEQUENCE_NUMBER_NEXT_PREFIX]
     *
     */
    @Account_invoiceSequence_number_next_prefixDefault(info = "默认规则")
    private String sequence_number_next_prefix;

    @JsonIgnore
    private boolean sequence_number_next_prefixDirtyFlag;

    /**
     * 属性 [ID]
     *
     */
    @Account_invoiceIdDefault(info = "默认规则")
    private Integer id;

    @JsonIgnore
    private boolean idDirtyFlag;

    /**
     * 属性 [REFUND_INVOICE_IDS]
     *
     */
    @Account_invoiceRefund_invoice_idsDefault(info = "默认规则")
    private String refund_invoice_ids;

    @JsonIgnore
    private boolean refund_invoice_idsDirtyFlag;

    /**
     * 属性 [RESIDUAL_COMPANY_SIGNED]
     *
     */
    @Account_invoiceResidual_company_signedDefault(info = "默认规则")
    private Double residual_company_signed;

    @JsonIgnore
    private boolean residual_company_signedDirtyFlag;

    /**
     * 属性 [DATE_INVOICE]
     *
     */
    @Account_invoiceDate_invoiceDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    private Timestamp date_invoice;

    @JsonIgnore
    private boolean date_invoiceDirtyFlag;

    /**
     * 属性 [NAME]
     *
     */
    @Account_invoiceNameDefault(info = "默认规则")
    private String name;

    @JsonIgnore
    private boolean nameDirtyFlag;

    /**
     * 属性 [ACTIVITY_USER_ID]
     *
     */
    @Account_invoiceActivity_user_idDefault(info = "默认规则")
    private Integer activity_user_id;

    @JsonIgnore
    private boolean activity_user_idDirtyFlag;

    /**
     * 属性 [REFERENCE]
     *
     */
    @Account_invoiceReferenceDefault(info = "默认规则")
    private String reference;

    @JsonIgnore
    private boolean referenceDirtyFlag;

    /**
     * 属性 [WEBSITE_ID]
     *
     */
    @Account_invoiceWebsite_idDefault(info = "默认规则")
    private Integer website_id;

    @JsonIgnore
    private boolean website_idDirtyFlag;

    /**
     * 属性 [SEQUENCE_NUMBER_NEXT]
     *
     */
    @Account_invoiceSequence_number_nextDefault(info = "默认规则")
    private String sequence_number_next;

    @JsonIgnore
    private boolean sequence_number_nextDirtyFlag;

    /**
     * 属性 [WEBSITE_MESSAGE_IDS]
     *
     */
    @Account_invoiceWebsite_message_idsDefault(info = "默认规则")
    private String website_message_ids;

    @JsonIgnore
    private boolean website_message_idsDirtyFlag;

    /**
     * 属性 [AMOUNT_TAX_SIGNED]
     *
     */
    @Account_invoiceAmount_tax_signedDefault(info = "默认规则")
    private Double amount_tax_signed;

    @JsonIgnore
    private boolean amount_tax_signedDirtyFlag;

    /**
     * 属性 [INVOICE_ICON]
     *
     */
    @Account_invoiceInvoice_iconDefault(info = "默认规则")
    private String invoice_icon;

    @JsonIgnore
    private boolean invoice_iconDirtyFlag;

    /**
     * 属性 [PAYMENT_IDS]
     *
     */
    @Account_invoicePayment_idsDefault(info = "默认规则")
    private String payment_ids;

    @JsonIgnore
    private boolean payment_idsDirtyFlag;

    /**
     * 属性 [MESSAGE_HAS_ERROR_COUNTER]
     *
     */
    @Account_invoiceMessage_has_error_counterDefault(info = "默认规则")
    private Integer message_has_error_counter;

    @JsonIgnore
    private boolean message_has_error_counterDirtyFlag;

    /**
     * 属性 [AMOUNT_TOTAL]
     *
     */
    @Account_invoiceAmount_totalDefault(info = "默认规则")
    private Double amount_total;

    @JsonIgnore
    private boolean amount_totalDirtyFlag;

    /**
     * 属性 [ACTIVITY_IDS]
     *
     */
    @Account_invoiceActivity_idsDefault(info = "默认规则")
    private String activity_ids;

    @JsonIgnore
    private boolean activity_idsDirtyFlag;

    /**
     * 属性 [TYPE]
     *
     */
    @Account_invoiceTypeDefault(info = "默认规则")
    private String type;

    @JsonIgnore
    private boolean typeDirtyFlag;

    /**
     * 属性 [JOURNAL_ID_TEXT]
     *
     */
    @Account_invoiceJournal_id_textDefault(info = "默认规则")
    private String journal_id_text;

    @JsonIgnore
    private boolean journal_id_textDirtyFlag;

    /**
     * 属性 [PURCHASE_ID_TEXT]
     *
     */
    @Account_invoicePurchase_id_textDefault(info = "默认规则")
    private String purchase_id_text;

    @JsonIgnore
    private boolean purchase_id_textDirtyFlag;

    /**
     * 属性 [VENDOR_BILL_PURCHASE_ID_TEXT]
     *
     */
    @Account_invoiceVendor_bill_purchase_id_textDefault(info = "默认规则")
    private String vendor_bill_purchase_id_text;

    @JsonIgnore
    private boolean vendor_bill_purchase_id_textDirtyFlag;

    /**
     * 属性 [SOURCE_ID_TEXT]
     *
     */
    @Account_invoiceSource_id_textDefault(info = "默认规则")
    private String source_id_text;

    @JsonIgnore
    private boolean source_id_textDirtyFlag;

    /**
     * 属性 [CAMPAIGN_ID_TEXT]
     *
     */
    @Account_invoiceCampaign_id_textDefault(info = "默认规则")
    private String campaign_id_text;

    @JsonIgnore
    private boolean campaign_id_textDirtyFlag;

    /**
     * 属性 [PARTNER_SHIPPING_ID_TEXT]
     *
     */
    @Account_invoicePartner_shipping_id_textDefault(info = "默认规则")
    private String partner_shipping_id_text;

    @JsonIgnore
    private boolean partner_shipping_id_textDirtyFlag;

    /**
     * 属性 [CURRENCY_ID_TEXT]
     *
     */
    @Account_invoiceCurrency_id_textDefault(info = "默认规则")
    private String currency_id_text;

    @JsonIgnore
    private boolean currency_id_textDirtyFlag;

    /**
     * 属性 [WRITE_UID_TEXT]
     *
     */
    @Account_invoiceWrite_uid_textDefault(info = "默认规则")
    private String write_uid_text;

    @JsonIgnore
    private boolean write_uid_textDirtyFlag;

    /**
     * 属性 [INCOTERMS_ID_TEXT]
     *
     */
    @Account_invoiceIncoterms_id_textDefault(info = "默认规则")
    private String incoterms_id_text;

    @JsonIgnore
    private boolean incoterms_id_textDirtyFlag;

    /**
     * 属性 [COMPANY_CURRENCY_ID]
     *
     */
    @Account_invoiceCompany_currency_idDefault(info = "默认规则")
    private Integer company_currency_id;

    @JsonIgnore
    private boolean company_currency_idDirtyFlag;

    /**
     * 属性 [ACCOUNT_ID_TEXT]
     *
     */
    @Account_invoiceAccount_id_textDefault(info = "默认规则")
    private String account_id_text;

    @JsonIgnore
    private boolean account_id_textDirtyFlag;

    /**
     * 属性 [INCOTERM_ID_TEXT]
     *
     */
    @Account_invoiceIncoterm_id_textDefault(info = "默认规则")
    private String incoterm_id_text;

    @JsonIgnore
    private boolean incoterm_id_textDirtyFlag;

    /**
     * 属性 [PARTNER_ID_TEXT]
     *
     */
    @Account_invoicePartner_id_textDefault(info = "默认规则")
    private String partner_id_text;

    @JsonIgnore
    private boolean partner_id_textDirtyFlag;

    /**
     * 属性 [MEDIUM_ID_TEXT]
     *
     */
    @Account_invoiceMedium_id_textDefault(info = "默认规则")
    private String medium_id_text;

    @JsonIgnore
    private boolean medium_id_textDirtyFlag;

    /**
     * 属性 [CREATE_UID_TEXT]
     *
     */
    @Account_invoiceCreate_uid_textDefault(info = "默认规则")
    private String create_uid_text;

    @JsonIgnore
    private boolean create_uid_textDirtyFlag;

    /**
     * 属性 [COMPANY_ID_TEXT]
     *
     */
    @Account_invoiceCompany_id_textDefault(info = "默认规则")
    private String company_id_text;

    @JsonIgnore
    private boolean company_id_textDirtyFlag;

    /**
     * 属性 [USER_ID_TEXT]
     *
     */
    @Account_invoiceUser_id_textDefault(info = "默认规则")
    private String user_id_text;

    @JsonIgnore
    private boolean user_id_textDirtyFlag;

    /**
     * 属性 [CASH_ROUNDING_ID_TEXT]
     *
     */
    @Account_invoiceCash_rounding_id_textDefault(info = "默认规则")
    private String cash_rounding_id_text;

    @JsonIgnore
    private boolean cash_rounding_id_textDirtyFlag;

    /**
     * 属性 [VENDOR_BILL_ID_TEXT]
     *
     */
    @Account_invoiceVendor_bill_id_textDefault(info = "默认规则")
    private String vendor_bill_id_text;

    @JsonIgnore
    private boolean vendor_bill_id_textDirtyFlag;

    /**
     * 属性 [REFUND_INVOICE_ID_TEXT]
     *
     */
    @Account_invoiceRefund_invoice_id_textDefault(info = "默认规则")
    private String refund_invoice_id_text;

    @JsonIgnore
    private boolean refund_invoice_id_textDirtyFlag;

    /**
     * 属性 [FISCAL_POSITION_ID_TEXT]
     *
     */
    @Account_invoiceFiscal_position_id_textDefault(info = "默认规则")
    private String fiscal_position_id_text;

    @JsonIgnore
    private boolean fiscal_position_id_textDirtyFlag;

    /**
     * 属性 [NUMBER]
     *
     */
    @Account_invoiceNumberDefault(info = "默认规则")
    private String number;

    @JsonIgnore
    private boolean numberDirtyFlag;

    /**
     * 属性 [COMMERCIAL_PARTNER_ID_TEXT]
     *
     */
    @Account_invoiceCommercial_partner_id_textDefault(info = "默认规则")
    private String commercial_partner_id_text;

    @JsonIgnore
    private boolean commercial_partner_id_textDirtyFlag;

    /**
     * 属性 [TEAM_ID_TEXT]
     *
     */
    @Account_invoiceTeam_id_textDefault(info = "默认规则")
    private String team_id_text;

    @JsonIgnore
    private boolean team_id_textDirtyFlag;

    /**
     * 属性 [PAYMENT_TERM_ID_TEXT]
     *
     */
    @Account_invoicePayment_term_id_textDefault(info = "默认规则")
    private String payment_term_id_text;

    @JsonIgnore
    private boolean payment_term_id_textDirtyFlag;

    /**
     * 属性 [USER_ID]
     *
     */
    @Account_invoiceUser_idDefault(info = "默认规则")
    private Integer user_id;

    @JsonIgnore
    private boolean user_idDirtyFlag;

    /**
     * 属性 [TEAM_ID]
     *
     */
    @Account_invoiceTeam_idDefault(info = "默认规则")
    private Integer team_id;

    @JsonIgnore
    private boolean team_idDirtyFlag;

    /**
     * 属性 [CREATE_UID]
     *
     */
    @Account_invoiceCreate_uidDefault(info = "默认规则")
    private Integer create_uid;

    @JsonIgnore
    private boolean create_uidDirtyFlag;

    /**
     * 属性 [ACCOUNT_ID]
     *
     */
    @Account_invoiceAccount_idDefault(info = "默认规则")
    private Integer account_id;

    @JsonIgnore
    private boolean account_idDirtyFlag;

    /**
     * 属性 [MEDIUM_ID]
     *
     */
    @Account_invoiceMedium_idDefault(info = "默认规则")
    private Integer medium_id;

    @JsonIgnore
    private boolean medium_idDirtyFlag;

    /**
     * 属性 [COMPANY_ID]
     *
     */
    @Account_invoiceCompany_idDefault(info = "默认规则")
    private Integer company_id;

    @JsonIgnore
    private boolean company_idDirtyFlag;

    /**
     * 属性 [MOVE_ID]
     *
     */
    @Account_invoiceMove_idDefault(info = "默认规则")
    private Integer move_id;

    @JsonIgnore
    private boolean move_idDirtyFlag;

    /**
     * 属性 [PAYMENT_TERM_ID]
     *
     */
    @Account_invoicePayment_term_idDefault(info = "默认规则")
    private Integer payment_term_id;

    @JsonIgnore
    private boolean payment_term_idDirtyFlag;

    /**
     * 属性 [JOURNAL_ID]
     *
     */
    @Account_invoiceJournal_idDefault(info = "默认规则")
    private Integer journal_id;

    @JsonIgnore
    private boolean journal_idDirtyFlag;

    /**
     * 属性 [INCOTERM_ID]
     *
     */
    @Account_invoiceIncoterm_idDefault(info = "默认规则")
    private Integer incoterm_id;

    @JsonIgnore
    private boolean incoterm_idDirtyFlag;

    /**
     * 属性 [PURCHASE_ID]
     *
     */
    @Account_invoicePurchase_idDefault(info = "默认规则")
    private Integer purchase_id;

    @JsonIgnore
    private boolean purchase_idDirtyFlag;

    /**
     * 属性 [FISCAL_POSITION_ID]
     *
     */
    @Account_invoiceFiscal_position_idDefault(info = "默认规则")
    private Integer fiscal_position_id;

    @JsonIgnore
    private boolean fiscal_position_idDirtyFlag;

    /**
     * 属性 [REFUND_INVOICE_ID]
     *
     */
    @Account_invoiceRefund_invoice_idDefault(info = "默认规则")
    private Integer refund_invoice_id;

    @JsonIgnore
    private boolean refund_invoice_idDirtyFlag;

    /**
     * 属性 [CURRENCY_ID]
     *
     */
    @Account_invoiceCurrency_idDefault(info = "默认规则")
    private Integer currency_id;

    @JsonIgnore
    private boolean currency_idDirtyFlag;

    /**
     * 属性 [PARTNER_ID]
     *
     */
    @Account_invoicePartner_idDefault(info = "默认规则")
    private Integer partner_id;

    @JsonIgnore
    private boolean partner_idDirtyFlag;

    /**
     * 属性 [PARTNER_BANK_ID]
     *
     */
    @Account_invoicePartner_bank_idDefault(info = "默认规则")
    private Integer partner_bank_id;

    @JsonIgnore
    private boolean partner_bank_idDirtyFlag;

    /**
     * 属性 [CASH_ROUNDING_ID]
     *
     */
    @Account_invoiceCash_rounding_idDefault(info = "默认规则")
    private Integer cash_rounding_id;

    @JsonIgnore
    private boolean cash_rounding_idDirtyFlag;

    /**
     * 属性 [COMMERCIAL_PARTNER_ID]
     *
     */
    @Account_invoiceCommercial_partner_idDefault(info = "默认规则")
    private Integer commercial_partner_id;

    @JsonIgnore
    private boolean commercial_partner_idDirtyFlag;

    /**
     * 属性 [PARTNER_SHIPPING_ID]
     *
     */
    @Account_invoicePartner_shipping_idDefault(info = "默认规则")
    private Integer partner_shipping_id;

    @JsonIgnore
    private boolean partner_shipping_idDirtyFlag;

    /**
     * 属性 [INCOTERMS_ID]
     *
     */
    @Account_invoiceIncoterms_idDefault(info = "默认规则")
    private Integer incoterms_id;

    @JsonIgnore
    private boolean incoterms_idDirtyFlag;

    /**
     * 属性 [SOURCE_ID]
     *
     */
    @Account_invoiceSource_idDefault(info = "默认规则")
    private Integer source_id;

    @JsonIgnore
    private boolean source_idDirtyFlag;

    /**
     * 属性 [VENDOR_BILL_ID]
     *
     */
    @Account_invoiceVendor_bill_idDefault(info = "默认规则")
    private Integer vendor_bill_id;

    @JsonIgnore
    private boolean vendor_bill_idDirtyFlag;

    /**
     * 属性 [WRITE_UID]
     *
     */
    @Account_invoiceWrite_uidDefault(info = "默认规则")
    private Integer write_uid;

    @JsonIgnore
    private boolean write_uidDirtyFlag;

    /**
     * 属性 [VENDOR_BILL_PURCHASE_ID]
     *
     */
    @Account_invoiceVendor_bill_purchase_idDefault(info = "默认规则")
    private Integer vendor_bill_purchase_id;

    @JsonIgnore
    private boolean vendor_bill_purchase_idDirtyFlag;

    /**
     * 属性 [CAMPAIGN_ID]
     *
     */
    @Account_invoiceCampaign_idDefault(info = "默认规则")
    private Integer campaign_id;

    @JsonIgnore
    private boolean campaign_idDirtyFlag;


    /**
     * 获取 [OUTSTANDING_CREDITS_DEBITS_WIDGET]
     */
    @JsonProperty("outstanding_credits_debits_widget")
    public String getOutstanding_credits_debits_widget(){
        return outstanding_credits_debits_widget ;
    }

    /**
     * 设置 [OUTSTANDING_CREDITS_DEBITS_WIDGET]
     */
    @JsonProperty("outstanding_credits_debits_widget")
    public void setOutstanding_credits_debits_widget(String  outstanding_credits_debits_widget){
        this.outstanding_credits_debits_widget = outstanding_credits_debits_widget ;
        this.outstanding_credits_debits_widgetDirtyFlag = true ;
    }

    /**
     * 获取 [OUTSTANDING_CREDITS_DEBITS_WIDGET]脏标记
     */
    @JsonIgnore
    public boolean getOutstanding_credits_debits_widgetDirtyFlag(){
        return outstanding_credits_debits_widgetDirtyFlag ;
    }

    /**
     * 获取 [ACTIVITY_DATE_DEADLINE]
     */
    @JsonProperty("activity_date_deadline")
    public Timestamp getActivity_date_deadline(){
        return activity_date_deadline ;
    }

    /**
     * 设置 [ACTIVITY_DATE_DEADLINE]
     */
    @JsonProperty("activity_date_deadline")
    public void setActivity_date_deadline(Timestamp  activity_date_deadline){
        this.activity_date_deadline = activity_date_deadline ;
        this.activity_date_deadlineDirtyFlag = true ;
    }

    /**
     * 获取 [ACTIVITY_DATE_DEADLINE]脏标记
     */
    @JsonIgnore
    public boolean getActivity_date_deadlineDirtyFlag(){
        return activity_date_deadlineDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_CHANNEL_IDS]
     */
    @JsonProperty("message_channel_ids")
    public String getMessage_channel_ids(){
        return message_channel_ids ;
    }

    /**
     * 设置 [MESSAGE_CHANNEL_IDS]
     */
    @JsonProperty("message_channel_ids")
    public void setMessage_channel_ids(String  message_channel_ids){
        this.message_channel_ids = message_channel_ids ;
        this.message_channel_idsDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_CHANNEL_IDS]脏标记
     */
    @JsonIgnore
    public boolean getMessage_channel_idsDirtyFlag(){
        return message_channel_idsDirtyFlag ;
    }

    /**
     * 获取 [MOVE_NAME]
     */
    @JsonProperty("move_name")
    public String getMove_name(){
        return move_name ;
    }

    /**
     * 设置 [MOVE_NAME]
     */
    @JsonProperty("move_name")
    public void setMove_name(String  move_name){
        this.move_name = move_name ;
        this.move_nameDirtyFlag = true ;
    }

    /**
     * 获取 [MOVE_NAME]脏标记
     */
    @JsonIgnore
    public boolean getMove_nameDirtyFlag(){
        return move_nameDirtyFlag ;
    }

    /**
     * 获取 [ACTIVITY_SUMMARY]
     */
    @JsonProperty("activity_summary")
    public String getActivity_summary(){
        return activity_summary ;
    }

    /**
     * 设置 [ACTIVITY_SUMMARY]
     */
    @JsonProperty("activity_summary")
    public void setActivity_summary(String  activity_summary){
        this.activity_summary = activity_summary ;
        this.activity_summaryDirtyFlag = true ;
    }

    /**
     * 获取 [ACTIVITY_SUMMARY]脏标记
     */
    @JsonIgnore
    public boolean getActivity_summaryDirtyFlag(){
        return activity_summaryDirtyFlag ;
    }

    /**
     * 获取 [ACCESS_URL]
     */
    @JsonProperty("access_url")
    public String getAccess_url(){
        return access_url ;
    }

    /**
     * 设置 [ACCESS_URL]
     */
    @JsonProperty("access_url")
    public void setAccess_url(String  access_url){
        this.access_url = access_url ;
        this.access_urlDirtyFlag = true ;
    }

    /**
     * 获取 [ACCESS_URL]脏标记
     */
    @JsonIgnore
    public boolean getAccess_urlDirtyFlag(){
        return access_urlDirtyFlag ;
    }

    /**
     * 获取 [PAYMENT_MOVE_LINE_IDS]
     */
    @JsonProperty("payment_move_line_ids")
    public String getPayment_move_line_ids(){
        return payment_move_line_ids ;
    }

    /**
     * 设置 [PAYMENT_MOVE_LINE_IDS]
     */
    @JsonProperty("payment_move_line_ids")
    public void setPayment_move_line_ids(String  payment_move_line_ids){
        this.payment_move_line_ids = payment_move_line_ids ;
        this.payment_move_line_idsDirtyFlag = true ;
    }

    /**
     * 获取 [PAYMENT_MOVE_LINE_IDS]脏标记
     */
    @JsonIgnore
    public boolean getPayment_move_line_idsDirtyFlag(){
        return payment_move_line_idsDirtyFlag ;
    }

    /**
     * 获取 [ORIGIN]
     */
    @JsonProperty("origin")
    public String getOrigin(){
        return origin ;
    }

    /**
     * 设置 [ORIGIN]
     */
    @JsonProperty("origin")
    public void setOrigin(String  origin){
        this.origin = origin ;
        this.originDirtyFlag = true ;
    }

    /**
     * 获取 [ORIGIN]脏标记
     */
    @JsonIgnore
    public boolean getOriginDirtyFlag(){
        return originDirtyFlag ;
    }

    /**
     * 获取 [DATE]
     */
    @JsonProperty("date")
    public Timestamp getDate(){
        return date ;
    }

    /**
     * 设置 [DATE]
     */
    @JsonProperty("date")
    public void setDate(Timestamp  date){
        this.date = date ;
        this.dateDirtyFlag = true ;
    }

    /**
     * 获取 [DATE]脏标记
     */
    @JsonIgnore
    public boolean getDateDirtyFlag(){
        return dateDirtyFlag ;
    }

    /**
     * 获取 [AMOUNT_TOTAL_COMPANY_SIGNED]
     */
    @JsonProperty("amount_total_company_signed")
    public Double getAmount_total_company_signed(){
        return amount_total_company_signed ;
    }

    /**
     * 设置 [AMOUNT_TOTAL_COMPANY_SIGNED]
     */
    @JsonProperty("amount_total_company_signed")
    public void setAmount_total_company_signed(Double  amount_total_company_signed){
        this.amount_total_company_signed = amount_total_company_signed ;
        this.amount_total_company_signedDirtyFlag = true ;
    }

    /**
     * 获取 [AMOUNT_TOTAL_COMPANY_SIGNED]脏标记
     */
    @JsonIgnore
    public boolean getAmount_total_company_signedDirtyFlag(){
        return amount_total_company_signedDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_NEEDACTION_COUNTER]
     */
    @JsonProperty("message_needaction_counter")
    public Integer getMessage_needaction_counter(){
        return message_needaction_counter ;
    }

    /**
     * 设置 [MESSAGE_NEEDACTION_COUNTER]
     */
    @JsonProperty("message_needaction_counter")
    public void setMessage_needaction_counter(Integer  message_needaction_counter){
        this.message_needaction_counter = message_needaction_counter ;
        this.message_needaction_counterDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_NEEDACTION_COUNTER]脏标记
     */
    @JsonIgnore
    public boolean getMessage_needaction_counterDirtyFlag(){
        return message_needaction_counterDirtyFlag ;
    }

    /**
     * 获取 [AMOUNT_UNTAXED_SIGNED]
     */
    @JsonProperty("amount_untaxed_signed")
    public Double getAmount_untaxed_signed(){
        return amount_untaxed_signed ;
    }

    /**
     * 设置 [AMOUNT_UNTAXED_SIGNED]
     */
    @JsonProperty("amount_untaxed_signed")
    public void setAmount_untaxed_signed(Double  amount_untaxed_signed){
        this.amount_untaxed_signed = amount_untaxed_signed ;
        this.amount_untaxed_signedDirtyFlag = true ;
    }

    /**
     * 获取 [AMOUNT_UNTAXED_SIGNED]脏标记
     */
    @JsonIgnore
    public boolean getAmount_untaxed_signedDirtyFlag(){
        return amount_untaxed_signedDirtyFlag ;
    }

    /**
     * 获取 [RESIDUAL]
     */
    @JsonProperty("residual")
    public Double getResidual(){
        return residual ;
    }

    /**
     * 设置 [RESIDUAL]
     */
    @JsonProperty("residual")
    public void setResidual(Double  residual){
        this.residual = residual ;
        this.residualDirtyFlag = true ;
    }

    /**
     * 获取 [RESIDUAL]脏标记
     */
    @JsonIgnore
    public boolean getResidualDirtyFlag(){
        return residualDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_MAIN_ATTACHMENT_ID]
     */
    @JsonProperty("message_main_attachment_id")
    public Integer getMessage_main_attachment_id(){
        return message_main_attachment_id ;
    }

    /**
     * 设置 [MESSAGE_MAIN_ATTACHMENT_ID]
     */
    @JsonProperty("message_main_attachment_id")
    public void setMessage_main_attachment_id(Integer  message_main_attachment_id){
        this.message_main_attachment_id = message_main_attachment_id ;
        this.message_main_attachment_idDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_MAIN_ATTACHMENT_ID]脏标记
     */
    @JsonIgnore
    public boolean getMessage_main_attachment_idDirtyFlag(){
        return message_main_attachment_idDirtyFlag ;
    }

    /**
     * 获取 [HAS_OUTSTANDING]
     */
    @JsonProperty("has_outstanding")
    public String getHas_outstanding(){
        return has_outstanding ;
    }

    /**
     * 设置 [HAS_OUTSTANDING]
     */
    @JsonProperty("has_outstanding")
    public void setHas_outstanding(String  has_outstanding){
        this.has_outstanding = has_outstanding ;
        this.has_outstandingDirtyFlag = true ;
    }

    /**
     * 获取 [HAS_OUTSTANDING]脏标记
     */
    @JsonIgnore
    public boolean getHas_outstandingDirtyFlag(){
        return has_outstandingDirtyFlag ;
    }

    /**
     * 获取 [COMMENT]
     */
    @JsonProperty("comment")
    public String getComment(){
        return comment ;
    }

    /**
     * 设置 [COMMENT]
     */
    @JsonProperty("comment")
    public void setComment(String  comment){
        this.comment = comment ;
        this.commentDirtyFlag = true ;
    }

    /**
     * 获取 [COMMENT]脏标记
     */
    @JsonIgnore
    public boolean getCommentDirtyFlag(){
        return commentDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_UNREAD]
     */
    @JsonProperty("message_unread")
    public String getMessage_unread(){
        return message_unread ;
    }

    /**
     * 设置 [MESSAGE_UNREAD]
     */
    @JsonProperty("message_unread")
    public void setMessage_unread(String  message_unread){
        this.message_unread = message_unread ;
        this.message_unreadDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_UNREAD]脏标记
     */
    @JsonIgnore
    public boolean getMessage_unreadDirtyFlag(){
        return message_unreadDirtyFlag ;
    }

    /**
     * 获取 [ACTIVITY_TYPE_ID]
     */
    @JsonProperty("activity_type_id")
    public Integer getActivity_type_id(){
        return activity_type_id ;
    }

    /**
     * 设置 [ACTIVITY_TYPE_ID]
     */
    @JsonProperty("activity_type_id")
    public void setActivity_type_id(Integer  activity_type_id){
        this.activity_type_id = activity_type_id ;
        this.activity_type_idDirtyFlag = true ;
    }

    /**
     * 获取 [ACTIVITY_TYPE_ID]脏标记
     */
    @JsonIgnore
    public boolean getActivity_type_idDirtyFlag(){
        return activity_type_idDirtyFlag ;
    }

    /**
     * 获取 [AMOUNT_BY_GROUP]
     */
    @JsonProperty("amount_by_group")
    public byte[] getAmount_by_group(){
        return amount_by_group ;
    }

    /**
     * 设置 [AMOUNT_BY_GROUP]
     */
    @JsonProperty("amount_by_group")
    public void setAmount_by_group(byte[]  amount_by_group){
        this.amount_by_group = amount_by_group ;
        this.amount_by_groupDirtyFlag = true ;
    }

    /**
     * 获取 [AMOUNT_BY_GROUP]脏标记
     */
    @JsonIgnore
    public boolean getAmount_by_groupDirtyFlag(){
        return amount_by_groupDirtyFlag ;
    }

    /**
     * 获取 [PAYMENTS_WIDGET]
     */
    @JsonProperty("payments_widget")
    public String getPayments_widget(){
        return payments_widget ;
    }

    /**
     * 设置 [PAYMENTS_WIDGET]
     */
    @JsonProperty("payments_widget")
    public void setPayments_widget(String  payments_widget){
        this.payments_widget = payments_widget ;
        this.payments_widgetDirtyFlag = true ;
    }

    /**
     * 获取 [PAYMENTS_WIDGET]脏标记
     */
    @JsonIgnore
    public boolean getPayments_widgetDirtyFlag(){
        return payments_widgetDirtyFlag ;
    }

    /**
     * 获取 [RECONCILED]
     */
    @JsonProperty("reconciled")
    public String getReconciled(){
        return reconciled ;
    }

    /**
     * 设置 [RECONCILED]
     */
    @JsonProperty("reconciled")
    public void setReconciled(String  reconciled){
        this.reconciled = reconciled ;
        this.reconciledDirtyFlag = true ;
    }

    /**
     * 获取 [RECONCILED]脏标记
     */
    @JsonIgnore
    public boolean getReconciledDirtyFlag(){
        return reconciledDirtyFlag ;
    }

    /**
     * 获取 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return display_name ;
    }

    /**
     * 设置 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

    /**
     * 获取 [DISPLAY_NAME]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return display_nameDirtyFlag ;
    }

    /**
     * 获取 [SENT]
     */
    @JsonProperty("sent")
    public String getSent(){
        return sent ;
    }

    /**
     * 设置 [SENT]
     */
    @JsonProperty("sent")
    public void setSent(String  sent){
        this.sent = sent ;
        this.sentDirtyFlag = true ;
    }

    /**
     * 获取 [SENT]脏标记
     */
    @JsonIgnore
    public boolean getSentDirtyFlag(){
        return sentDirtyFlag ;
    }

    /**
     * 获取 [AMOUNT_TOTAL_SIGNED]
     */
    @JsonProperty("amount_total_signed")
    public Double getAmount_total_signed(){
        return amount_total_signed ;
    }

    /**
     * 设置 [AMOUNT_TOTAL_SIGNED]
     */
    @JsonProperty("amount_total_signed")
    public void setAmount_total_signed(Double  amount_total_signed){
        this.amount_total_signed = amount_total_signed ;
        this.amount_total_signedDirtyFlag = true ;
    }

    /**
     * 获取 [AMOUNT_TOTAL_SIGNED]脏标记
     */
    @JsonIgnore
    public boolean getAmount_total_signedDirtyFlag(){
        return amount_total_signedDirtyFlag ;
    }

    /**
     * 获取 [INVOICE_LINE_IDS]
     */
    @JsonProperty("invoice_line_ids")
    public String getInvoice_line_ids(){
        return invoice_line_ids ;
    }

    /**
     * 设置 [INVOICE_LINE_IDS]
     */
    @JsonProperty("invoice_line_ids")
    public void setInvoice_line_ids(String  invoice_line_ids){
        this.invoice_line_ids = invoice_line_ids ;
        this.invoice_line_idsDirtyFlag = true ;
    }

    /**
     * 获取 [INVOICE_LINE_IDS]脏标记
     */
    @JsonIgnore
    public boolean getInvoice_line_idsDirtyFlag(){
        return invoice_line_idsDirtyFlag ;
    }

    /**
     * 获取 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return write_date ;
    }

    /**
     * 设置 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return write_dateDirtyFlag ;
    }

    /**
     * 获取 [ACCESS_WARNING]
     */
    @JsonProperty("access_warning")
    public String getAccess_warning(){
        return access_warning ;
    }

    /**
     * 设置 [ACCESS_WARNING]
     */
    @JsonProperty("access_warning")
    public void setAccess_warning(String  access_warning){
        this.access_warning = access_warning ;
        this.access_warningDirtyFlag = true ;
    }

    /**
     * 获取 [ACCESS_WARNING]脏标记
     */
    @JsonIgnore
    public boolean getAccess_warningDirtyFlag(){
        return access_warningDirtyFlag ;
    }

    /**
     * 获取 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return create_date ;
    }

    /**
     * 设置 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return create_dateDirtyFlag ;
    }

    /**
     * 获取 [ACTIVITY_STATE]
     */
    @JsonProperty("activity_state")
    public String getActivity_state(){
        return activity_state ;
    }

    /**
     * 设置 [ACTIVITY_STATE]
     */
    @JsonProperty("activity_state")
    public void setActivity_state(String  activity_state){
        this.activity_state = activity_state ;
        this.activity_stateDirtyFlag = true ;
    }

    /**
     * 获取 [ACTIVITY_STATE]脏标记
     */
    @JsonIgnore
    public boolean getActivity_stateDirtyFlag(){
        return activity_stateDirtyFlag ;
    }

    /**
     * 获取 [RESIDUAL_SIGNED]
     */
    @JsonProperty("residual_signed")
    public Double getResidual_signed(){
        return residual_signed ;
    }

    /**
     * 设置 [RESIDUAL_SIGNED]
     */
    @JsonProperty("residual_signed")
    public void setResidual_signed(Double  residual_signed){
        this.residual_signed = residual_signed ;
        this.residual_signedDirtyFlag = true ;
    }

    /**
     * 获取 [RESIDUAL_SIGNED]脏标记
     */
    @JsonIgnore
    public boolean getResidual_signedDirtyFlag(){
        return residual_signedDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_UNREAD_COUNTER]
     */
    @JsonProperty("message_unread_counter")
    public Integer getMessage_unread_counter(){
        return message_unread_counter ;
    }

    /**
     * 设置 [MESSAGE_UNREAD_COUNTER]
     */
    @JsonProperty("message_unread_counter")
    public void setMessage_unread_counter(Integer  message_unread_counter){
        this.message_unread_counter = message_unread_counter ;
        this.message_unread_counterDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_UNREAD_COUNTER]脏标记
     */
    @JsonIgnore
    public boolean getMessage_unread_counterDirtyFlag(){
        return message_unread_counterDirtyFlag ;
    }

    /**
     * 获取 [ACCESS_TOKEN]
     */
    @JsonProperty("access_token")
    public String getAccess_token(){
        return access_token ;
    }

    /**
     * 设置 [ACCESS_TOKEN]
     */
    @JsonProperty("access_token")
    public void setAccess_token(String  access_token){
        this.access_token = access_token ;
        this.access_tokenDirtyFlag = true ;
    }

    /**
     * 获取 [ACCESS_TOKEN]脏标记
     */
    @JsonIgnore
    public boolean getAccess_tokenDirtyFlag(){
        return access_tokenDirtyFlag ;
    }

    /**
     * 获取 [VENDOR_DISPLAY_NAME]
     */
    @JsonProperty("vendor_display_name")
    public String getVendor_display_name(){
        return vendor_display_name ;
    }

    /**
     * 设置 [VENDOR_DISPLAY_NAME]
     */
    @JsonProperty("vendor_display_name")
    public void setVendor_display_name(String  vendor_display_name){
        this.vendor_display_name = vendor_display_name ;
        this.vendor_display_nameDirtyFlag = true ;
    }

    /**
     * 获取 [VENDOR_DISPLAY_NAME]脏标记
     */
    @JsonIgnore
    public boolean getVendor_display_nameDirtyFlag(){
        return vendor_display_nameDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_ATTACHMENT_COUNT]
     */
    @JsonProperty("message_attachment_count")
    public Integer getMessage_attachment_count(){
        return message_attachment_count ;
    }

    /**
     * 设置 [MESSAGE_ATTACHMENT_COUNT]
     */
    @JsonProperty("message_attachment_count")
    public void setMessage_attachment_count(Integer  message_attachment_count){
        this.message_attachment_count = message_attachment_count ;
        this.message_attachment_countDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_ATTACHMENT_COUNT]脏标记
     */
    @JsonIgnore
    public boolean getMessage_attachment_countDirtyFlag(){
        return message_attachment_countDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_IDS]
     */
    @JsonProperty("message_ids")
    public String getMessage_ids(){
        return message_ids ;
    }

    /**
     * 设置 [MESSAGE_IDS]
     */
    @JsonProperty("message_ids")
    public void setMessage_ids(String  message_ids){
        this.message_ids = message_ids ;
        this.message_idsDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_IDS]脏标记
     */
    @JsonIgnore
    public boolean getMessage_idsDirtyFlag(){
        return message_idsDirtyFlag ;
    }

    /**
     * 获取 [AMOUNT_UNTAXED]
     */
    @JsonProperty("amount_untaxed")
    public Double getAmount_untaxed(){
        return amount_untaxed ;
    }

    /**
     * 设置 [AMOUNT_UNTAXED]
     */
    @JsonProperty("amount_untaxed")
    public void setAmount_untaxed(Double  amount_untaxed){
        this.amount_untaxed = amount_untaxed ;
        this.amount_untaxedDirtyFlag = true ;
    }

    /**
     * 获取 [AMOUNT_UNTAXED]脏标记
     */
    @JsonIgnore
    public boolean getAmount_untaxedDirtyFlag(){
        return amount_untaxedDirtyFlag ;
    }

    /**
     * 获取 [TRANSACTION_IDS]
     */
    @JsonProperty("transaction_ids")
    public String getTransaction_ids(){
        return transaction_ids ;
    }

    /**
     * 设置 [TRANSACTION_IDS]
     */
    @JsonProperty("transaction_ids")
    public void setTransaction_ids(String  transaction_ids){
        this.transaction_ids = transaction_ids ;
        this.transaction_idsDirtyFlag = true ;
    }

    /**
     * 获取 [TRANSACTION_IDS]脏标记
     */
    @JsonIgnore
    public boolean getTransaction_idsDirtyFlag(){
        return transaction_idsDirtyFlag ;
    }

    /**
     * 获取 [DATE_DUE]
     */
    @JsonProperty("date_due")
    public Timestamp getDate_due(){
        return date_due ;
    }

    /**
     * 设置 [DATE_DUE]
     */
    @JsonProperty("date_due")
    public void setDate_due(Timestamp  date_due){
        this.date_due = date_due ;
        this.date_dueDirtyFlag = true ;
    }

    /**
     * 获取 [DATE_DUE]脏标记
     */
    @JsonIgnore
    public boolean getDate_dueDirtyFlag(){
        return date_dueDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_PARTNER_IDS]
     */
    @JsonProperty("message_partner_ids")
    public String getMessage_partner_ids(){
        return message_partner_ids ;
    }

    /**
     * 设置 [MESSAGE_PARTNER_IDS]
     */
    @JsonProperty("message_partner_ids")
    public void setMessage_partner_ids(String  message_partner_ids){
        this.message_partner_ids = message_partner_ids ;
        this.message_partner_idsDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_PARTNER_IDS]脏标记
     */
    @JsonIgnore
    public boolean getMessage_partner_idsDirtyFlag(){
        return message_partner_idsDirtyFlag ;
    }

    /**
     * 获取 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return __last_update ;
    }

    /**
     * 设置 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

    /**
     * 获取 [__LAST_UPDATE]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return __last_updateDirtyFlag ;
    }

    /**
     * 获取 [AUTHORIZED_TRANSACTION_IDS]
     */
    @JsonProperty("authorized_transaction_ids")
    public String getAuthorized_transaction_ids(){
        return authorized_transaction_ids ;
    }

    /**
     * 设置 [AUTHORIZED_TRANSACTION_IDS]
     */
    @JsonProperty("authorized_transaction_ids")
    public void setAuthorized_transaction_ids(String  authorized_transaction_ids){
        this.authorized_transaction_ids = authorized_transaction_ids ;
        this.authorized_transaction_idsDirtyFlag = true ;
    }

    /**
     * 获取 [AUTHORIZED_TRANSACTION_IDS]脏标记
     */
    @JsonIgnore
    public boolean getAuthorized_transaction_idsDirtyFlag(){
        return authorized_transaction_idsDirtyFlag ;
    }

    /**
     * 获取 [TAX_LINE_IDS]
     */
    @JsonProperty("tax_line_ids")
    public String getTax_line_ids(){
        return tax_line_ids ;
    }

    /**
     * 设置 [TAX_LINE_IDS]
     */
    @JsonProperty("tax_line_ids")
    public void setTax_line_ids(String  tax_line_ids){
        this.tax_line_ids = tax_line_ids ;
        this.tax_line_idsDirtyFlag = true ;
    }

    /**
     * 获取 [TAX_LINE_IDS]脏标记
     */
    @JsonIgnore
    public boolean getTax_line_idsDirtyFlag(){
        return tax_line_idsDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_FOLLOWER_IDS]
     */
    @JsonProperty("message_follower_ids")
    public String getMessage_follower_ids(){
        return message_follower_ids ;
    }

    /**
     * 设置 [MESSAGE_FOLLOWER_IDS]
     */
    @JsonProperty("message_follower_ids")
    public void setMessage_follower_ids(String  message_follower_ids){
        this.message_follower_ids = message_follower_ids ;
        this.message_follower_idsDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_FOLLOWER_IDS]脏标记
     */
    @JsonIgnore
    public boolean getMessage_follower_idsDirtyFlag(){
        return message_follower_idsDirtyFlag ;
    }

    /**
     * 获取 [SOURCE_EMAIL]
     */
    @JsonProperty("source_email")
    public String getSource_email(){
        return source_email ;
    }

    /**
     * 设置 [SOURCE_EMAIL]
     */
    @JsonProperty("source_email")
    public void setSource_email(String  source_email){
        this.source_email = source_email ;
        this.source_emailDirtyFlag = true ;
    }

    /**
     * 获取 [SOURCE_EMAIL]脏标记
     */
    @JsonIgnore
    public boolean getSource_emailDirtyFlag(){
        return source_emailDirtyFlag ;
    }

    /**
     * 获取 [AMOUNT_TAX]
     */
    @JsonProperty("amount_tax")
    public Double getAmount_tax(){
        return amount_tax ;
    }

    /**
     * 设置 [AMOUNT_TAX]
     */
    @JsonProperty("amount_tax")
    public void setAmount_tax(Double  amount_tax){
        this.amount_tax = amount_tax ;
        this.amount_taxDirtyFlag = true ;
    }

    /**
     * 获取 [AMOUNT_TAX]脏标记
     */
    @JsonIgnore
    public boolean getAmount_taxDirtyFlag(){
        return amount_taxDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_IS_FOLLOWER]
     */
    @JsonProperty("message_is_follower")
    public String getMessage_is_follower(){
        return message_is_follower ;
    }

    /**
     * 设置 [MESSAGE_IS_FOLLOWER]
     */
    @JsonProperty("message_is_follower")
    public void setMessage_is_follower(String  message_is_follower){
        this.message_is_follower = message_is_follower ;
        this.message_is_followerDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_IS_FOLLOWER]脏标记
     */
    @JsonIgnore
    public boolean getMessage_is_followerDirtyFlag(){
        return message_is_followerDirtyFlag ;
    }

    /**
     * 获取 [STATE]
     */
    @JsonProperty("state")
    public String getState(){
        return state ;
    }

    /**
     * 设置 [STATE]
     */
    @JsonProperty("state")
    public void setState(String  state){
        this.state = state ;
        this.stateDirtyFlag = true ;
    }

    /**
     * 获取 [STATE]脏标记
     */
    @JsonIgnore
    public boolean getStateDirtyFlag(){
        return stateDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_HAS_ERROR]
     */
    @JsonProperty("message_has_error")
    public String getMessage_has_error(){
        return message_has_error ;
    }

    /**
     * 设置 [MESSAGE_HAS_ERROR]
     */
    @JsonProperty("message_has_error")
    public void setMessage_has_error(String  message_has_error){
        this.message_has_error = message_has_error ;
        this.message_has_errorDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_HAS_ERROR]脏标记
     */
    @JsonIgnore
    public boolean getMessage_has_errorDirtyFlag(){
        return message_has_errorDirtyFlag ;
    }

    /**
     * 获取 [AMOUNT_UNTAXED_INVOICE_SIGNED]
     */
    @JsonProperty("amount_untaxed_invoice_signed")
    public Double getAmount_untaxed_invoice_signed(){
        return amount_untaxed_invoice_signed ;
    }

    /**
     * 设置 [AMOUNT_UNTAXED_INVOICE_SIGNED]
     */
    @JsonProperty("amount_untaxed_invoice_signed")
    public void setAmount_untaxed_invoice_signed(Double  amount_untaxed_invoice_signed){
        this.amount_untaxed_invoice_signed = amount_untaxed_invoice_signed ;
        this.amount_untaxed_invoice_signedDirtyFlag = true ;
    }

    /**
     * 获取 [AMOUNT_UNTAXED_INVOICE_SIGNED]脏标记
     */
    @JsonIgnore
    public boolean getAmount_untaxed_invoice_signedDirtyFlag(){
        return amount_untaxed_invoice_signedDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_NEEDACTION]
     */
    @JsonProperty("message_needaction")
    public String getMessage_needaction(){
        return message_needaction ;
    }

    /**
     * 设置 [MESSAGE_NEEDACTION]
     */
    @JsonProperty("message_needaction")
    public void setMessage_needaction(String  message_needaction){
        this.message_needaction = message_needaction ;
        this.message_needactionDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_NEEDACTION]脏标记
     */
    @JsonIgnore
    public boolean getMessage_needactionDirtyFlag(){
        return message_needactionDirtyFlag ;
    }

    /**
     * 获取 [SEQUENCE_NUMBER_NEXT_PREFIX]
     */
    @JsonProperty("sequence_number_next_prefix")
    public String getSequence_number_next_prefix(){
        return sequence_number_next_prefix ;
    }

    /**
     * 设置 [SEQUENCE_NUMBER_NEXT_PREFIX]
     */
    @JsonProperty("sequence_number_next_prefix")
    public void setSequence_number_next_prefix(String  sequence_number_next_prefix){
        this.sequence_number_next_prefix = sequence_number_next_prefix ;
        this.sequence_number_next_prefixDirtyFlag = true ;
    }

    /**
     * 获取 [SEQUENCE_NUMBER_NEXT_PREFIX]脏标记
     */
    @JsonIgnore
    public boolean getSequence_number_next_prefixDirtyFlag(){
        return sequence_number_next_prefixDirtyFlag ;
    }

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

    /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return idDirtyFlag ;
    }

    /**
     * 获取 [REFUND_INVOICE_IDS]
     */
    @JsonProperty("refund_invoice_ids")
    public String getRefund_invoice_ids(){
        return refund_invoice_ids ;
    }

    /**
     * 设置 [REFUND_INVOICE_IDS]
     */
    @JsonProperty("refund_invoice_ids")
    public void setRefund_invoice_ids(String  refund_invoice_ids){
        this.refund_invoice_ids = refund_invoice_ids ;
        this.refund_invoice_idsDirtyFlag = true ;
    }

    /**
     * 获取 [REFUND_INVOICE_IDS]脏标记
     */
    @JsonIgnore
    public boolean getRefund_invoice_idsDirtyFlag(){
        return refund_invoice_idsDirtyFlag ;
    }

    /**
     * 获取 [RESIDUAL_COMPANY_SIGNED]
     */
    @JsonProperty("residual_company_signed")
    public Double getResidual_company_signed(){
        return residual_company_signed ;
    }

    /**
     * 设置 [RESIDUAL_COMPANY_SIGNED]
     */
    @JsonProperty("residual_company_signed")
    public void setResidual_company_signed(Double  residual_company_signed){
        this.residual_company_signed = residual_company_signed ;
        this.residual_company_signedDirtyFlag = true ;
    }

    /**
     * 获取 [RESIDUAL_COMPANY_SIGNED]脏标记
     */
    @JsonIgnore
    public boolean getResidual_company_signedDirtyFlag(){
        return residual_company_signedDirtyFlag ;
    }

    /**
     * 获取 [DATE_INVOICE]
     */
    @JsonProperty("date_invoice")
    public Timestamp getDate_invoice(){
        return date_invoice ;
    }

    /**
     * 设置 [DATE_INVOICE]
     */
    @JsonProperty("date_invoice")
    public void setDate_invoice(Timestamp  date_invoice){
        this.date_invoice = date_invoice ;
        this.date_invoiceDirtyFlag = true ;
    }

    /**
     * 获取 [DATE_INVOICE]脏标记
     */
    @JsonIgnore
    public boolean getDate_invoiceDirtyFlag(){
        return date_invoiceDirtyFlag ;
    }

    /**
     * 获取 [NAME]
     */
    @JsonProperty("name")
    public String getName(){
        return name ;
    }

    /**
     * 设置 [NAME]
     */
    @JsonProperty("name")
    public void setName(String  name){
        this.name = name ;
        this.nameDirtyFlag = true ;
    }

    /**
     * 获取 [NAME]脏标记
     */
    @JsonIgnore
    public boolean getNameDirtyFlag(){
        return nameDirtyFlag ;
    }

    /**
     * 获取 [ACTIVITY_USER_ID]
     */
    @JsonProperty("activity_user_id")
    public Integer getActivity_user_id(){
        return activity_user_id ;
    }

    /**
     * 设置 [ACTIVITY_USER_ID]
     */
    @JsonProperty("activity_user_id")
    public void setActivity_user_id(Integer  activity_user_id){
        this.activity_user_id = activity_user_id ;
        this.activity_user_idDirtyFlag = true ;
    }

    /**
     * 获取 [ACTIVITY_USER_ID]脏标记
     */
    @JsonIgnore
    public boolean getActivity_user_idDirtyFlag(){
        return activity_user_idDirtyFlag ;
    }

    /**
     * 获取 [REFERENCE]
     */
    @JsonProperty("reference")
    public String getReference(){
        return reference ;
    }

    /**
     * 设置 [REFERENCE]
     */
    @JsonProperty("reference")
    public void setReference(String  reference){
        this.reference = reference ;
        this.referenceDirtyFlag = true ;
    }

    /**
     * 获取 [REFERENCE]脏标记
     */
    @JsonIgnore
    public boolean getReferenceDirtyFlag(){
        return referenceDirtyFlag ;
    }

    /**
     * 获取 [WEBSITE_ID]
     */
    @JsonProperty("website_id")
    public Integer getWebsite_id(){
        return website_id ;
    }

    /**
     * 设置 [WEBSITE_ID]
     */
    @JsonProperty("website_id")
    public void setWebsite_id(Integer  website_id){
        this.website_id = website_id ;
        this.website_idDirtyFlag = true ;
    }

    /**
     * 获取 [WEBSITE_ID]脏标记
     */
    @JsonIgnore
    public boolean getWebsite_idDirtyFlag(){
        return website_idDirtyFlag ;
    }

    /**
     * 获取 [SEQUENCE_NUMBER_NEXT]
     */
    @JsonProperty("sequence_number_next")
    public String getSequence_number_next(){
        return sequence_number_next ;
    }

    /**
     * 设置 [SEQUENCE_NUMBER_NEXT]
     */
    @JsonProperty("sequence_number_next")
    public void setSequence_number_next(String  sequence_number_next){
        this.sequence_number_next = sequence_number_next ;
        this.sequence_number_nextDirtyFlag = true ;
    }

    /**
     * 获取 [SEQUENCE_NUMBER_NEXT]脏标记
     */
    @JsonIgnore
    public boolean getSequence_number_nextDirtyFlag(){
        return sequence_number_nextDirtyFlag ;
    }

    /**
     * 获取 [WEBSITE_MESSAGE_IDS]
     */
    @JsonProperty("website_message_ids")
    public String getWebsite_message_ids(){
        return website_message_ids ;
    }

    /**
     * 设置 [WEBSITE_MESSAGE_IDS]
     */
    @JsonProperty("website_message_ids")
    public void setWebsite_message_ids(String  website_message_ids){
        this.website_message_ids = website_message_ids ;
        this.website_message_idsDirtyFlag = true ;
    }

    /**
     * 获取 [WEBSITE_MESSAGE_IDS]脏标记
     */
    @JsonIgnore
    public boolean getWebsite_message_idsDirtyFlag(){
        return website_message_idsDirtyFlag ;
    }

    /**
     * 获取 [AMOUNT_TAX_SIGNED]
     */
    @JsonProperty("amount_tax_signed")
    public Double getAmount_tax_signed(){
        return amount_tax_signed ;
    }

    /**
     * 设置 [AMOUNT_TAX_SIGNED]
     */
    @JsonProperty("amount_tax_signed")
    public void setAmount_tax_signed(Double  amount_tax_signed){
        this.amount_tax_signed = amount_tax_signed ;
        this.amount_tax_signedDirtyFlag = true ;
    }

    /**
     * 获取 [AMOUNT_TAX_SIGNED]脏标记
     */
    @JsonIgnore
    public boolean getAmount_tax_signedDirtyFlag(){
        return amount_tax_signedDirtyFlag ;
    }

    /**
     * 获取 [INVOICE_ICON]
     */
    @JsonProperty("invoice_icon")
    public String getInvoice_icon(){
        return invoice_icon ;
    }

    /**
     * 设置 [INVOICE_ICON]
     */
    @JsonProperty("invoice_icon")
    public void setInvoice_icon(String  invoice_icon){
        this.invoice_icon = invoice_icon ;
        this.invoice_iconDirtyFlag = true ;
    }

    /**
     * 获取 [INVOICE_ICON]脏标记
     */
    @JsonIgnore
    public boolean getInvoice_iconDirtyFlag(){
        return invoice_iconDirtyFlag ;
    }

    /**
     * 获取 [PAYMENT_IDS]
     */
    @JsonProperty("payment_ids")
    public String getPayment_ids(){
        return payment_ids ;
    }

    /**
     * 设置 [PAYMENT_IDS]
     */
    @JsonProperty("payment_ids")
    public void setPayment_ids(String  payment_ids){
        this.payment_ids = payment_ids ;
        this.payment_idsDirtyFlag = true ;
    }

    /**
     * 获取 [PAYMENT_IDS]脏标记
     */
    @JsonIgnore
    public boolean getPayment_idsDirtyFlag(){
        return payment_idsDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_HAS_ERROR_COUNTER]
     */
    @JsonProperty("message_has_error_counter")
    public Integer getMessage_has_error_counter(){
        return message_has_error_counter ;
    }

    /**
     * 设置 [MESSAGE_HAS_ERROR_COUNTER]
     */
    @JsonProperty("message_has_error_counter")
    public void setMessage_has_error_counter(Integer  message_has_error_counter){
        this.message_has_error_counter = message_has_error_counter ;
        this.message_has_error_counterDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_HAS_ERROR_COUNTER]脏标记
     */
    @JsonIgnore
    public boolean getMessage_has_error_counterDirtyFlag(){
        return message_has_error_counterDirtyFlag ;
    }

    /**
     * 获取 [AMOUNT_TOTAL]
     */
    @JsonProperty("amount_total")
    public Double getAmount_total(){
        return amount_total ;
    }

    /**
     * 设置 [AMOUNT_TOTAL]
     */
    @JsonProperty("amount_total")
    public void setAmount_total(Double  amount_total){
        this.amount_total = amount_total ;
        this.amount_totalDirtyFlag = true ;
    }

    /**
     * 获取 [AMOUNT_TOTAL]脏标记
     */
    @JsonIgnore
    public boolean getAmount_totalDirtyFlag(){
        return amount_totalDirtyFlag ;
    }

    /**
     * 获取 [ACTIVITY_IDS]
     */
    @JsonProperty("activity_ids")
    public String getActivity_ids(){
        return activity_ids ;
    }

    /**
     * 设置 [ACTIVITY_IDS]
     */
    @JsonProperty("activity_ids")
    public void setActivity_ids(String  activity_ids){
        this.activity_ids = activity_ids ;
        this.activity_idsDirtyFlag = true ;
    }

    /**
     * 获取 [ACTIVITY_IDS]脏标记
     */
    @JsonIgnore
    public boolean getActivity_idsDirtyFlag(){
        return activity_idsDirtyFlag ;
    }

    /**
     * 获取 [TYPE]
     */
    @JsonProperty("type")
    public String getType(){
        return type ;
    }

    /**
     * 设置 [TYPE]
     */
    @JsonProperty("type")
    public void setType(String  type){
        this.type = type ;
        this.typeDirtyFlag = true ;
    }

    /**
     * 获取 [TYPE]脏标记
     */
    @JsonIgnore
    public boolean getTypeDirtyFlag(){
        return typeDirtyFlag ;
    }

    /**
     * 获取 [JOURNAL_ID_TEXT]
     */
    @JsonProperty("journal_id_text")
    public String getJournal_id_text(){
        return journal_id_text ;
    }

    /**
     * 设置 [JOURNAL_ID_TEXT]
     */
    @JsonProperty("journal_id_text")
    public void setJournal_id_text(String  journal_id_text){
        this.journal_id_text = journal_id_text ;
        this.journal_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [JOURNAL_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getJournal_id_textDirtyFlag(){
        return journal_id_textDirtyFlag ;
    }

    /**
     * 获取 [PURCHASE_ID_TEXT]
     */
    @JsonProperty("purchase_id_text")
    public String getPurchase_id_text(){
        return purchase_id_text ;
    }

    /**
     * 设置 [PURCHASE_ID_TEXT]
     */
    @JsonProperty("purchase_id_text")
    public void setPurchase_id_text(String  purchase_id_text){
        this.purchase_id_text = purchase_id_text ;
        this.purchase_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [PURCHASE_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getPurchase_id_textDirtyFlag(){
        return purchase_id_textDirtyFlag ;
    }

    /**
     * 获取 [VENDOR_BILL_PURCHASE_ID_TEXT]
     */
    @JsonProperty("vendor_bill_purchase_id_text")
    public String getVendor_bill_purchase_id_text(){
        return vendor_bill_purchase_id_text ;
    }

    /**
     * 设置 [VENDOR_BILL_PURCHASE_ID_TEXT]
     */
    @JsonProperty("vendor_bill_purchase_id_text")
    public void setVendor_bill_purchase_id_text(String  vendor_bill_purchase_id_text){
        this.vendor_bill_purchase_id_text = vendor_bill_purchase_id_text ;
        this.vendor_bill_purchase_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [VENDOR_BILL_PURCHASE_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getVendor_bill_purchase_id_textDirtyFlag(){
        return vendor_bill_purchase_id_textDirtyFlag ;
    }

    /**
     * 获取 [SOURCE_ID_TEXT]
     */
    @JsonProperty("source_id_text")
    public String getSource_id_text(){
        return source_id_text ;
    }

    /**
     * 设置 [SOURCE_ID_TEXT]
     */
    @JsonProperty("source_id_text")
    public void setSource_id_text(String  source_id_text){
        this.source_id_text = source_id_text ;
        this.source_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [SOURCE_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getSource_id_textDirtyFlag(){
        return source_id_textDirtyFlag ;
    }

    /**
     * 获取 [CAMPAIGN_ID_TEXT]
     */
    @JsonProperty("campaign_id_text")
    public String getCampaign_id_text(){
        return campaign_id_text ;
    }

    /**
     * 设置 [CAMPAIGN_ID_TEXT]
     */
    @JsonProperty("campaign_id_text")
    public void setCampaign_id_text(String  campaign_id_text){
        this.campaign_id_text = campaign_id_text ;
        this.campaign_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [CAMPAIGN_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getCampaign_id_textDirtyFlag(){
        return campaign_id_textDirtyFlag ;
    }

    /**
     * 获取 [PARTNER_SHIPPING_ID_TEXT]
     */
    @JsonProperty("partner_shipping_id_text")
    public String getPartner_shipping_id_text(){
        return partner_shipping_id_text ;
    }

    /**
     * 设置 [PARTNER_SHIPPING_ID_TEXT]
     */
    @JsonProperty("partner_shipping_id_text")
    public void setPartner_shipping_id_text(String  partner_shipping_id_text){
        this.partner_shipping_id_text = partner_shipping_id_text ;
        this.partner_shipping_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [PARTNER_SHIPPING_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getPartner_shipping_id_textDirtyFlag(){
        return partner_shipping_id_textDirtyFlag ;
    }

    /**
     * 获取 [CURRENCY_ID_TEXT]
     */
    @JsonProperty("currency_id_text")
    public String getCurrency_id_text(){
        return currency_id_text ;
    }

    /**
     * 设置 [CURRENCY_ID_TEXT]
     */
    @JsonProperty("currency_id_text")
    public void setCurrency_id_text(String  currency_id_text){
        this.currency_id_text = currency_id_text ;
        this.currency_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [CURRENCY_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getCurrency_id_textDirtyFlag(){
        return currency_id_textDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return write_uid_text ;
    }

    /**
     * 设置 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return write_uid_textDirtyFlag ;
    }

    /**
     * 获取 [INCOTERMS_ID_TEXT]
     */
    @JsonProperty("incoterms_id_text")
    public String getIncoterms_id_text(){
        return incoterms_id_text ;
    }

    /**
     * 设置 [INCOTERMS_ID_TEXT]
     */
    @JsonProperty("incoterms_id_text")
    public void setIncoterms_id_text(String  incoterms_id_text){
        this.incoterms_id_text = incoterms_id_text ;
        this.incoterms_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [INCOTERMS_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getIncoterms_id_textDirtyFlag(){
        return incoterms_id_textDirtyFlag ;
    }

    /**
     * 获取 [COMPANY_CURRENCY_ID]
     */
    @JsonProperty("company_currency_id")
    public Integer getCompany_currency_id(){
        return company_currency_id ;
    }

    /**
     * 设置 [COMPANY_CURRENCY_ID]
     */
    @JsonProperty("company_currency_id")
    public void setCompany_currency_id(Integer  company_currency_id){
        this.company_currency_id = company_currency_id ;
        this.company_currency_idDirtyFlag = true ;
    }

    /**
     * 获取 [COMPANY_CURRENCY_ID]脏标记
     */
    @JsonIgnore
    public boolean getCompany_currency_idDirtyFlag(){
        return company_currency_idDirtyFlag ;
    }

    /**
     * 获取 [ACCOUNT_ID_TEXT]
     */
    @JsonProperty("account_id_text")
    public String getAccount_id_text(){
        return account_id_text ;
    }

    /**
     * 设置 [ACCOUNT_ID_TEXT]
     */
    @JsonProperty("account_id_text")
    public void setAccount_id_text(String  account_id_text){
        this.account_id_text = account_id_text ;
        this.account_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [ACCOUNT_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getAccount_id_textDirtyFlag(){
        return account_id_textDirtyFlag ;
    }

    /**
     * 获取 [INCOTERM_ID_TEXT]
     */
    @JsonProperty("incoterm_id_text")
    public String getIncoterm_id_text(){
        return incoterm_id_text ;
    }

    /**
     * 设置 [INCOTERM_ID_TEXT]
     */
    @JsonProperty("incoterm_id_text")
    public void setIncoterm_id_text(String  incoterm_id_text){
        this.incoterm_id_text = incoterm_id_text ;
        this.incoterm_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [INCOTERM_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getIncoterm_id_textDirtyFlag(){
        return incoterm_id_textDirtyFlag ;
    }

    /**
     * 获取 [PARTNER_ID_TEXT]
     */
    @JsonProperty("partner_id_text")
    public String getPartner_id_text(){
        return partner_id_text ;
    }

    /**
     * 设置 [PARTNER_ID_TEXT]
     */
    @JsonProperty("partner_id_text")
    public void setPartner_id_text(String  partner_id_text){
        this.partner_id_text = partner_id_text ;
        this.partner_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [PARTNER_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getPartner_id_textDirtyFlag(){
        return partner_id_textDirtyFlag ;
    }

    /**
     * 获取 [MEDIUM_ID_TEXT]
     */
    @JsonProperty("medium_id_text")
    public String getMedium_id_text(){
        return medium_id_text ;
    }

    /**
     * 设置 [MEDIUM_ID_TEXT]
     */
    @JsonProperty("medium_id_text")
    public void setMedium_id_text(String  medium_id_text){
        this.medium_id_text = medium_id_text ;
        this.medium_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [MEDIUM_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getMedium_id_textDirtyFlag(){
        return medium_id_textDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return create_uid_text ;
    }

    /**
     * 设置 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return create_uid_textDirtyFlag ;
    }

    /**
     * 获取 [COMPANY_ID_TEXT]
     */
    @JsonProperty("company_id_text")
    public String getCompany_id_text(){
        return company_id_text ;
    }

    /**
     * 设置 [COMPANY_ID_TEXT]
     */
    @JsonProperty("company_id_text")
    public void setCompany_id_text(String  company_id_text){
        this.company_id_text = company_id_text ;
        this.company_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [COMPANY_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getCompany_id_textDirtyFlag(){
        return company_id_textDirtyFlag ;
    }

    /**
     * 获取 [USER_ID_TEXT]
     */
    @JsonProperty("user_id_text")
    public String getUser_id_text(){
        return user_id_text ;
    }

    /**
     * 设置 [USER_ID_TEXT]
     */
    @JsonProperty("user_id_text")
    public void setUser_id_text(String  user_id_text){
        this.user_id_text = user_id_text ;
        this.user_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [USER_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getUser_id_textDirtyFlag(){
        return user_id_textDirtyFlag ;
    }

    /**
     * 获取 [CASH_ROUNDING_ID_TEXT]
     */
    @JsonProperty("cash_rounding_id_text")
    public String getCash_rounding_id_text(){
        return cash_rounding_id_text ;
    }

    /**
     * 设置 [CASH_ROUNDING_ID_TEXT]
     */
    @JsonProperty("cash_rounding_id_text")
    public void setCash_rounding_id_text(String  cash_rounding_id_text){
        this.cash_rounding_id_text = cash_rounding_id_text ;
        this.cash_rounding_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [CASH_ROUNDING_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getCash_rounding_id_textDirtyFlag(){
        return cash_rounding_id_textDirtyFlag ;
    }

    /**
     * 获取 [VENDOR_BILL_ID_TEXT]
     */
    @JsonProperty("vendor_bill_id_text")
    public String getVendor_bill_id_text(){
        return vendor_bill_id_text ;
    }

    /**
     * 设置 [VENDOR_BILL_ID_TEXT]
     */
    @JsonProperty("vendor_bill_id_text")
    public void setVendor_bill_id_text(String  vendor_bill_id_text){
        this.vendor_bill_id_text = vendor_bill_id_text ;
        this.vendor_bill_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [VENDOR_BILL_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getVendor_bill_id_textDirtyFlag(){
        return vendor_bill_id_textDirtyFlag ;
    }

    /**
     * 获取 [REFUND_INVOICE_ID_TEXT]
     */
    @JsonProperty("refund_invoice_id_text")
    public String getRefund_invoice_id_text(){
        return refund_invoice_id_text ;
    }

    /**
     * 设置 [REFUND_INVOICE_ID_TEXT]
     */
    @JsonProperty("refund_invoice_id_text")
    public void setRefund_invoice_id_text(String  refund_invoice_id_text){
        this.refund_invoice_id_text = refund_invoice_id_text ;
        this.refund_invoice_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [REFUND_INVOICE_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getRefund_invoice_id_textDirtyFlag(){
        return refund_invoice_id_textDirtyFlag ;
    }

    /**
     * 获取 [FISCAL_POSITION_ID_TEXT]
     */
    @JsonProperty("fiscal_position_id_text")
    public String getFiscal_position_id_text(){
        return fiscal_position_id_text ;
    }

    /**
     * 设置 [FISCAL_POSITION_ID_TEXT]
     */
    @JsonProperty("fiscal_position_id_text")
    public void setFiscal_position_id_text(String  fiscal_position_id_text){
        this.fiscal_position_id_text = fiscal_position_id_text ;
        this.fiscal_position_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [FISCAL_POSITION_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getFiscal_position_id_textDirtyFlag(){
        return fiscal_position_id_textDirtyFlag ;
    }

    /**
     * 获取 [NUMBER]
     */
    @JsonProperty("number")
    public String getNumber(){
        return number ;
    }

    /**
     * 设置 [NUMBER]
     */
    @JsonProperty("number")
    public void setNumber(String  number){
        this.number = number ;
        this.numberDirtyFlag = true ;
    }

    /**
     * 获取 [NUMBER]脏标记
     */
    @JsonIgnore
    public boolean getNumberDirtyFlag(){
        return numberDirtyFlag ;
    }

    /**
     * 获取 [COMMERCIAL_PARTNER_ID_TEXT]
     */
    @JsonProperty("commercial_partner_id_text")
    public String getCommercial_partner_id_text(){
        return commercial_partner_id_text ;
    }

    /**
     * 设置 [COMMERCIAL_PARTNER_ID_TEXT]
     */
    @JsonProperty("commercial_partner_id_text")
    public void setCommercial_partner_id_text(String  commercial_partner_id_text){
        this.commercial_partner_id_text = commercial_partner_id_text ;
        this.commercial_partner_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [COMMERCIAL_PARTNER_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getCommercial_partner_id_textDirtyFlag(){
        return commercial_partner_id_textDirtyFlag ;
    }

    /**
     * 获取 [TEAM_ID_TEXT]
     */
    @JsonProperty("team_id_text")
    public String getTeam_id_text(){
        return team_id_text ;
    }

    /**
     * 设置 [TEAM_ID_TEXT]
     */
    @JsonProperty("team_id_text")
    public void setTeam_id_text(String  team_id_text){
        this.team_id_text = team_id_text ;
        this.team_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [TEAM_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getTeam_id_textDirtyFlag(){
        return team_id_textDirtyFlag ;
    }

    /**
     * 获取 [PAYMENT_TERM_ID_TEXT]
     */
    @JsonProperty("payment_term_id_text")
    public String getPayment_term_id_text(){
        return payment_term_id_text ;
    }

    /**
     * 设置 [PAYMENT_TERM_ID_TEXT]
     */
    @JsonProperty("payment_term_id_text")
    public void setPayment_term_id_text(String  payment_term_id_text){
        this.payment_term_id_text = payment_term_id_text ;
        this.payment_term_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [PAYMENT_TERM_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getPayment_term_id_textDirtyFlag(){
        return payment_term_id_textDirtyFlag ;
    }

    /**
     * 获取 [USER_ID]
     */
    @JsonProperty("user_id")
    public Integer getUser_id(){
        return user_id ;
    }

    /**
     * 设置 [USER_ID]
     */
    @JsonProperty("user_id")
    public void setUser_id(Integer  user_id){
        this.user_id = user_id ;
        this.user_idDirtyFlag = true ;
    }

    /**
     * 获取 [USER_ID]脏标记
     */
    @JsonIgnore
    public boolean getUser_idDirtyFlag(){
        return user_idDirtyFlag ;
    }

    /**
     * 获取 [TEAM_ID]
     */
    @JsonProperty("team_id")
    public Integer getTeam_id(){
        return team_id ;
    }

    /**
     * 设置 [TEAM_ID]
     */
    @JsonProperty("team_id")
    public void setTeam_id(Integer  team_id){
        this.team_id = team_id ;
        this.team_idDirtyFlag = true ;
    }

    /**
     * 获取 [TEAM_ID]脏标记
     */
    @JsonIgnore
    public boolean getTeam_idDirtyFlag(){
        return team_idDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return create_uid ;
    }

    /**
     * 设置 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return create_uidDirtyFlag ;
    }

    /**
     * 获取 [ACCOUNT_ID]
     */
    @JsonProperty("account_id")
    public Integer getAccount_id(){
        return account_id ;
    }

    /**
     * 设置 [ACCOUNT_ID]
     */
    @JsonProperty("account_id")
    public void setAccount_id(Integer  account_id){
        this.account_id = account_id ;
        this.account_idDirtyFlag = true ;
    }

    /**
     * 获取 [ACCOUNT_ID]脏标记
     */
    @JsonIgnore
    public boolean getAccount_idDirtyFlag(){
        return account_idDirtyFlag ;
    }

    /**
     * 获取 [MEDIUM_ID]
     */
    @JsonProperty("medium_id")
    public Integer getMedium_id(){
        return medium_id ;
    }

    /**
     * 设置 [MEDIUM_ID]
     */
    @JsonProperty("medium_id")
    public void setMedium_id(Integer  medium_id){
        this.medium_id = medium_id ;
        this.medium_idDirtyFlag = true ;
    }

    /**
     * 获取 [MEDIUM_ID]脏标记
     */
    @JsonIgnore
    public boolean getMedium_idDirtyFlag(){
        return medium_idDirtyFlag ;
    }

    /**
     * 获取 [COMPANY_ID]
     */
    @JsonProperty("company_id")
    public Integer getCompany_id(){
        return company_id ;
    }

    /**
     * 设置 [COMPANY_ID]
     */
    @JsonProperty("company_id")
    public void setCompany_id(Integer  company_id){
        this.company_id = company_id ;
        this.company_idDirtyFlag = true ;
    }

    /**
     * 获取 [COMPANY_ID]脏标记
     */
    @JsonIgnore
    public boolean getCompany_idDirtyFlag(){
        return company_idDirtyFlag ;
    }

    /**
     * 获取 [MOVE_ID]
     */
    @JsonProperty("move_id")
    public Integer getMove_id(){
        return move_id ;
    }

    /**
     * 设置 [MOVE_ID]
     */
    @JsonProperty("move_id")
    public void setMove_id(Integer  move_id){
        this.move_id = move_id ;
        this.move_idDirtyFlag = true ;
    }

    /**
     * 获取 [MOVE_ID]脏标记
     */
    @JsonIgnore
    public boolean getMove_idDirtyFlag(){
        return move_idDirtyFlag ;
    }

    /**
     * 获取 [PAYMENT_TERM_ID]
     */
    @JsonProperty("payment_term_id")
    public Integer getPayment_term_id(){
        return payment_term_id ;
    }

    /**
     * 设置 [PAYMENT_TERM_ID]
     */
    @JsonProperty("payment_term_id")
    public void setPayment_term_id(Integer  payment_term_id){
        this.payment_term_id = payment_term_id ;
        this.payment_term_idDirtyFlag = true ;
    }

    /**
     * 获取 [PAYMENT_TERM_ID]脏标记
     */
    @JsonIgnore
    public boolean getPayment_term_idDirtyFlag(){
        return payment_term_idDirtyFlag ;
    }

    /**
     * 获取 [JOURNAL_ID]
     */
    @JsonProperty("journal_id")
    public Integer getJournal_id(){
        return journal_id ;
    }

    /**
     * 设置 [JOURNAL_ID]
     */
    @JsonProperty("journal_id")
    public void setJournal_id(Integer  journal_id){
        this.journal_id = journal_id ;
        this.journal_idDirtyFlag = true ;
    }

    /**
     * 获取 [JOURNAL_ID]脏标记
     */
    @JsonIgnore
    public boolean getJournal_idDirtyFlag(){
        return journal_idDirtyFlag ;
    }

    /**
     * 获取 [INCOTERM_ID]
     */
    @JsonProperty("incoterm_id")
    public Integer getIncoterm_id(){
        return incoterm_id ;
    }

    /**
     * 设置 [INCOTERM_ID]
     */
    @JsonProperty("incoterm_id")
    public void setIncoterm_id(Integer  incoterm_id){
        this.incoterm_id = incoterm_id ;
        this.incoterm_idDirtyFlag = true ;
    }

    /**
     * 获取 [INCOTERM_ID]脏标记
     */
    @JsonIgnore
    public boolean getIncoterm_idDirtyFlag(){
        return incoterm_idDirtyFlag ;
    }

    /**
     * 获取 [PURCHASE_ID]
     */
    @JsonProperty("purchase_id")
    public Integer getPurchase_id(){
        return purchase_id ;
    }

    /**
     * 设置 [PURCHASE_ID]
     */
    @JsonProperty("purchase_id")
    public void setPurchase_id(Integer  purchase_id){
        this.purchase_id = purchase_id ;
        this.purchase_idDirtyFlag = true ;
    }

    /**
     * 获取 [PURCHASE_ID]脏标记
     */
    @JsonIgnore
    public boolean getPurchase_idDirtyFlag(){
        return purchase_idDirtyFlag ;
    }

    /**
     * 获取 [FISCAL_POSITION_ID]
     */
    @JsonProperty("fiscal_position_id")
    public Integer getFiscal_position_id(){
        return fiscal_position_id ;
    }

    /**
     * 设置 [FISCAL_POSITION_ID]
     */
    @JsonProperty("fiscal_position_id")
    public void setFiscal_position_id(Integer  fiscal_position_id){
        this.fiscal_position_id = fiscal_position_id ;
        this.fiscal_position_idDirtyFlag = true ;
    }

    /**
     * 获取 [FISCAL_POSITION_ID]脏标记
     */
    @JsonIgnore
    public boolean getFiscal_position_idDirtyFlag(){
        return fiscal_position_idDirtyFlag ;
    }

    /**
     * 获取 [REFUND_INVOICE_ID]
     */
    @JsonProperty("refund_invoice_id")
    public Integer getRefund_invoice_id(){
        return refund_invoice_id ;
    }

    /**
     * 设置 [REFUND_INVOICE_ID]
     */
    @JsonProperty("refund_invoice_id")
    public void setRefund_invoice_id(Integer  refund_invoice_id){
        this.refund_invoice_id = refund_invoice_id ;
        this.refund_invoice_idDirtyFlag = true ;
    }

    /**
     * 获取 [REFUND_INVOICE_ID]脏标记
     */
    @JsonIgnore
    public boolean getRefund_invoice_idDirtyFlag(){
        return refund_invoice_idDirtyFlag ;
    }

    /**
     * 获取 [CURRENCY_ID]
     */
    @JsonProperty("currency_id")
    public Integer getCurrency_id(){
        return currency_id ;
    }

    /**
     * 设置 [CURRENCY_ID]
     */
    @JsonProperty("currency_id")
    public void setCurrency_id(Integer  currency_id){
        this.currency_id = currency_id ;
        this.currency_idDirtyFlag = true ;
    }

    /**
     * 获取 [CURRENCY_ID]脏标记
     */
    @JsonIgnore
    public boolean getCurrency_idDirtyFlag(){
        return currency_idDirtyFlag ;
    }

    /**
     * 获取 [PARTNER_ID]
     */
    @JsonProperty("partner_id")
    public Integer getPartner_id(){
        return partner_id ;
    }

    /**
     * 设置 [PARTNER_ID]
     */
    @JsonProperty("partner_id")
    public void setPartner_id(Integer  partner_id){
        this.partner_id = partner_id ;
        this.partner_idDirtyFlag = true ;
    }

    /**
     * 获取 [PARTNER_ID]脏标记
     */
    @JsonIgnore
    public boolean getPartner_idDirtyFlag(){
        return partner_idDirtyFlag ;
    }

    /**
     * 获取 [PARTNER_BANK_ID]
     */
    @JsonProperty("partner_bank_id")
    public Integer getPartner_bank_id(){
        return partner_bank_id ;
    }

    /**
     * 设置 [PARTNER_BANK_ID]
     */
    @JsonProperty("partner_bank_id")
    public void setPartner_bank_id(Integer  partner_bank_id){
        this.partner_bank_id = partner_bank_id ;
        this.partner_bank_idDirtyFlag = true ;
    }

    /**
     * 获取 [PARTNER_BANK_ID]脏标记
     */
    @JsonIgnore
    public boolean getPartner_bank_idDirtyFlag(){
        return partner_bank_idDirtyFlag ;
    }

    /**
     * 获取 [CASH_ROUNDING_ID]
     */
    @JsonProperty("cash_rounding_id")
    public Integer getCash_rounding_id(){
        return cash_rounding_id ;
    }

    /**
     * 设置 [CASH_ROUNDING_ID]
     */
    @JsonProperty("cash_rounding_id")
    public void setCash_rounding_id(Integer  cash_rounding_id){
        this.cash_rounding_id = cash_rounding_id ;
        this.cash_rounding_idDirtyFlag = true ;
    }

    /**
     * 获取 [CASH_ROUNDING_ID]脏标记
     */
    @JsonIgnore
    public boolean getCash_rounding_idDirtyFlag(){
        return cash_rounding_idDirtyFlag ;
    }

    /**
     * 获取 [COMMERCIAL_PARTNER_ID]
     */
    @JsonProperty("commercial_partner_id")
    public Integer getCommercial_partner_id(){
        return commercial_partner_id ;
    }

    /**
     * 设置 [COMMERCIAL_PARTNER_ID]
     */
    @JsonProperty("commercial_partner_id")
    public void setCommercial_partner_id(Integer  commercial_partner_id){
        this.commercial_partner_id = commercial_partner_id ;
        this.commercial_partner_idDirtyFlag = true ;
    }

    /**
     * 获取 [COMMERCIAL_PARTNER_ID]脏标记
     */
    @JsonIgnore
    public boolean getCommercial_partner_idDirtyFlag(){
        return commercial_partner_idDirtyFlag ;
    }

    /**
     * 获取 [PARTNER_SHIPPING_ID]
     */
    @JsonProperty("partner_shipping_id")
    public Integer getPartner_shipping_id(){
        return partner_shipping_id ;
    }

    /**
     * 设置 [PARTNER_SHIPPING_ID]
     */
    @JsonProperty("partner_shipping_id")
    public void setPartner_shipping_id(Integer  partner_shipping_id){
        this.partner_shipping_id = partner_shipping_id ;
        this.partner_shipping_idDirtyFlag = true ;
    }

    /**
     * 获取 [PARTNER_SHIPPING_ID]脏标记
     */
    @JsonIgnore
    public boolean getPartner_shipping_idDirtyFlag(){
        return partner_shipping_idDirtyFlag ;
    }

    /**
     * 获取 [INCOTERMS_ID]
     */
    @JsonProperty("incoterms_id")
    public Integer getIncoterms_id(){
        return incoterms_id ;
    }

    /**
     * 设置 [INCOTERMS_ID]
     */
    @JsonProperty("incoterms_id")
    public void setIncoterms_id(Integer  incoterms_id){
        this.incoterms_id = incoterms_id ;
        this.incoterms_idDirtyFlag = true ;
    }

    /**
     * 获取 [INCOTERMS_ID]脏标记
     */
    @JsonIgnore
    public boolean getIncoterms_idDirtyFlag(){
        return incoterms_idDirtyFlag ;
    }

    /**
     * 获取 [SOURCE_ID]
     */
    @JsonProperty("source_id")
    public Integer getSource_id(){
        return source_id ;
    }

    /**
     * 设置 [SOURCE_ID]
     */
    @JsonProperty("source_id")
    public void setSource_id(Integer  source_id){
        this.source_id = source_id ;
        this.source_idDirtyFlag = true ;
    }

    /**
     * 获取 [SOURCE_ID]脏标记
     */
    @JsonIgnore
    public boolean getSource_idDirtyFlag(){
        return source_idDirtyFlag ;
    }

    /**
     * 获取 [VENDOR_BILL_ID]
     */
    @JsonProperty("vendor_bill_id")
    public Integer getVendor_bill_id(){
        return vendor_bill_id ;
    }

    /**
     * 设置 [VENDOR_BILL_ID]
     */
    @JsonProperty("vendor_bill_id")
    public void setVendor_bill_id(Integer  vendor_bill_id){
        this.vendor_bill_id = vendor_bill_id ;
        this.vendor_bill_idDirtyFlag = true ;
    }

    /**
     * 获取 [VENDOR_BILL_ID]脏标记
     */
    @JsonIgnore
    public boolean getVendor_bill_idDirtyFlag(){
        return vendor_bill_idDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return write_uid ;
    }

    /**
     * 设置 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return write_uidDirtyFlag ;
    }

    /**
     * 获取 [VENDOR_BILL_PURCHASE_ID]
     */
    @JsonProperty("vendor_bill_purchase_id")
    public Integer getVendor_bill_purchase_id(){
        return vendor_bill_purchase_id ;
    }

    /**
     * 设置 [VENDOR_BILL_PURCHASE_ID]
     */
    @JsonProperty("vendor_bill_purchase_id")
    public void setVendor_bill_purchase_id(Integer  vendor_bill_purchase_id){
        this.vendor_bill_purchase_id = vendor_bill_purchase_id ;
        this.vendor_bill_purchase_idDirtyFlag = true ;
    }

    /**
     * 获取 [VENDOR_BILL_PURCHASE_ID]脏标记
     */
    @JsonIgnore
    public boolean getVendor_bill_purchase_idDirtyFlag(){
        return vendor_bill_purchase_idDirtyFlag ;
    }

    /**
     * 获取 [CAMPAIGN_ID]
     */
    @JsonProperty("campaign_id")
    public Integer getCampaign_id(){
        return campaign_id ;
    }

    /**
     * 设置 [CAMPAIGN_ID]
     */
    @JsonProperty("campaign_id")
    public void setCampaign_id(Integer  campaign_id){
        this.campaign_id = campaign_id ;
        this.campaign_idDirtyFlag = true ;
    }

    /**
     * 获取 [CAMPAIGN_ID]脏标记
     */
    @JsonIgnore
    public boolean getCampaign_idDirtyFlag(){
        return campaign_idDirtyFlag ;
    }



    public Account_invoice toDO() {
        Account_invoice srfdomain = new Account_invoice();
        if(getOutstanding_credits_debits_widgetDirtyFlag())
            srfdomain.setOutstanding_credits_debits_widget(outstanding_credits_debits_widget);
        if(getActivity_date_deadlineDirtyFlag())
            srfdomain.setActivity_date_deadline(activity_date_deadline);
        if(getMessage_channel_idsDirtyFlag())
            srfdomain.setMessage_channel_ids(message_channel_ids);
        if(getMove_nameDirtyFlag())
            srfdomain.setMove_name(move_name);
        if(getActivity_summaryDirtyFlag())
            srfdomain.setActivity_summary(activity_summary);
        if(getAccess_urlDirtyFlag())
            srfdomain.setAccess_url(access_url);
        if(getPayment_move_line_idsDirtyFlag())
            srfdomain.setPayment_move_line_ids(payment_move_line_ids);
        if(getOriginDirtyFlag())
            srfdomain.setOrigin(origin);
        if(getDateDirtyFlag())
            srfdomain.setDate(date);
        if(getAmount_total_company_signedDirtyFlag())
            srfdomain.setAmount_total_company_signed(amount_total_company_signed);
        if(getMessage_needaction_counterDirtyFlag())
            srfdomain.setMessage_needaction_counter(message_needaction_counter);
        if(getAmount_untaxed_signedDirtyFlag())
            srfdomain.setAmount_untaxed_signed(amount_untaxed_signed);
        if(getResidualDirtyFlag())
            srfdomain.setResidual(residual);
        if(getMessage_main_attachment_idDirtyFlag())
            srfdomain.setMessage_main_attachment_id(message_main_attachment_id);
        if(getHas_outstandingDirtyFlag())
            srfdomain.setHas_outstanding(has_outstanding);
        if(getCommentDirtyFlag())
            srfdomain.setComment(comment);
        if(getMessage_unreadDirtyFlag())
            srfdomain.setMessage_unread(message_unread);
        if(getActivity_type_idDirtyFlag())
            srfdomain.setActivity_type_id(activity_type_id);
        if(getAmount_by_groupDirtyFlag())
            srfdomain.setAmount_by_group(amount_by_group);
        if(getPayments_widgetDirtyFlag())
            srfdomain.setPayments_widget(payments_widget);
        if(getReconciledDirtyFlag())
            srfdomain.setReconciled(reconciled);
        if(getDisplay_nameDirtyFlag())
            srfdomain.setDisplay_name(display_name);
        if(getSentDirtyFlag())
            srfdomain.setSent(sent);
        if(getAmount_total_signedDirtyFlag())
            srfdomain.setAmount_total_signed(amount_total_signed);
        if(getInvoice_line_idsDirtyFlag())
            srfdomain.setInvoice_line_ids(invoice_line_ids);
        if(getWrite_dateDirtyFlag())
            srfdomain.setWrite_date(write_date);
        if(getAccess_warningDirtyFlag())
            srfdomain.setAccess_warning(access_warning);
        if(getCreate_dateDirtyFlag())
            srfdomain.setCreate_date(create_date);
        if(getActivity_stateDirtyFlag())
            srfdomain.setActivity_state(activity_state);
        if(getResidual_signedDirtyFlag())
            srfdomain.setResidual_signed(residual_signed);
        if(getMessage_unread_counterDirtyFlag())
            srfdomain.setMessage_unread_counter(message_unread_counter);
        if(getAccess_tokenDirtyFlag())
            srfdomain.setAccess_token(access_token);
        if(getVendor_display_nameDirtyFlag())
            srfdomain.setVendor_display_name(vendor_display_name);
        if(getMessage_attachment_countDirtyFlag())
            srfdomain.setMessage_attachment_count(message_attachment_count);
        if(getMessage_idsDirtyFlag())
            srfdomain.setMessage_ids(message_ids);
        if(getAmount_untaxedDirtyFlag())
            srfdomain.setAmount_untaxed(amount_untaxed);
        if(getTransaction_idsDirtyFlag())
            srfdomain.setTransaction_ids(transaction_ids);
        if(getDate_dueDirtyFlag())
            srfdomain.setDate_due(date_due);
        if(getMessage_partner_idsDirtyFlag())
            srfdomain.setMessage_partner_ids(message_partner_ids);
        if(get__last_updateDirtyFlag())
            srfdomain.set__last_update(__last_update);
        if(getAuthorized_transaction_idsDirtyFlag())
            srfdomain.setAuthorized_transaction_ids(authorized_transaction_ids);
        if(getTax_line_idsDirtyFlag())
            srfdomain.setTax_line_ids(tax_line_ids);
        if(getMessage_follower_idsDirtyFlag())
            srfdomain.setMessage_follower_ids(message_follower_ids);
        if(getSource_emailDirtyFlag())
            srfdomain.setSource_email(source_email);
        if(getAmount_taxDirtyFlag())
            srfdomain.setAmount_tax(amount_tax);
        if(getMessage_is_followerDirtyFlag())
            srfdomain.setMessage_is_follower(message_is_follower);
        if(getStateDirtyFlag())
            srfdomain.setState(state);
        if(getMessage_has_errorDirtyFlag())
            srfdomain.setMessage_has_error(message_has_error);
        if(getAmount_untaxed_invoice_signedDirtyFlag())
            srfdomain.setAmount_untaxed_invoice_signed(amount_untaxed_invoice_signed);
        if(getMessage_needactionDirtyFlag())
            srfdomain.setMessage_needaction(message_needaction);
        if(getSequence_number_next_prefixDirtyFlag())
            srfdomain.setSequence_number_next_prefix(sequence_number_next_prefix);
        if(getIdDirtyFlag())
            srfdomain.setId(id);
        if(getRefund_invoice_idsDirtyFlag())
            srfdomain.setRefund_invoice_ids(refund_invoice_ids);
        if(getResidual_company_signedDirtyFlag())
            srfdomain.setResidual_company_signed(residual_company_signed);
        if(getDate_invoiceDirtyFlag())
            srfdomain.setDate_invoice(date_invoice);
        if(getNameDirtyFlag())
            srfdomain.setName(name);
        if(getActivity_user_idDirtyFlag())
            srfdomain.setActivity_user_id(activity_user_id);
        if(getReferenceDirtyFlag())
            srfdomain.setReference(reference);
        if(getWebsite_idDirtyFlag())
            srfdomain.setWebsite_id(website_id);
        if(getSequence_number_nextDirtyFlag())
            srfdomain.setSequence_number_next(sequence_number_next);
        if(getWebsite_message_idsDirtyFlag())
            srfdomain.setWebsite_message_ids(website_message_ids);
        if(getAmount_tax_signedDirtyFlag())
            srfdomain.setAmount_tax_signed(amount_tax_signed);
        if(getInvoice_iconDirtyFlag())
            srfdomain.setInvoice_icon(invoice_icon);
        if(getPayment_idsDirtyFlag())
            srfdomain.setPayment_ids(payment_ids);
        if(getMessage_has_error_counterDirtyFlag())
            srfdomain.setMessage_has_error_counter(message_has_error_counter);
        if(getAmount_totalDirtyFlag())
            srfdomain.setAmount_total(amount_total);
        if(getActivity_idsDirtyFlag())
            srfdomain.setActivity_ids(activity_ids);
        if(getTypeDirtyFlag())
            srfdomain.setType(type);
        if(getJournal_id_textDirtyFlag())
            srfdomain.setJournal_id_text(journal_id_text);
        if(getPurchase_id_textDirtyFlag())
            srfdomain.setPurchase_id_text(purchase_id_text);
        if(getVendor_bill_purchase_id_textDirtyFlag())
            srfdomain.setVendor_bill_purchase_id_text(vendor_bill_purchase_id_text);
        if(getSource_id_textDirtyFlag())
            srfdomain.setSource_id_text(source_id_text);
        if(getCampaign_id_textDirtyFlag())
            srfdomain.setCampaign_id_text(campaign_id_text);
        if(getPartner_shipping_id_textDirtyFlag())
            srfdomain.setPartner_shipping_id_text(partner_shipping_id_text);
        if(getCurrency_id_textDirtyFlag())
            srfdomain.setCurrency_id_text(currency_id_text);
        if(getWrite_uid_textDirtyFlag())
            srfdomain.setWrite_uid_text(write_uid_text);
        if(getIncoterms_id_textDirtyFlag())
            srfdomain.setIncoterms_id_text(incoterms_id_text);
        if(getCompany_currency_idDirtyFlag())
            srfdomain.setCompany_currency_id(company_currency_id);
        if(getAccount_id_textDirtyFlag())
            srfdomain.setAccount_id_text(account_id_text);
        if(getIncoterm_id_textDirtyFlag())
            srfdomain.setIncoterm_id_text(incoterm_id_text);
        if(getPartner_id_textDirtyFlag())
            srfdomain.setPartner_id_text(partner_id_text);
        if(getMedium_id_textDirtyFlag())
            srfdomain.setMedium_id_text(medium_id_text);
        if(getCreate_uid_textDirtyFlag())
            srfdomain.setCreate_uid_text(create_uid_text);
        if(getCompany_id_textDirtyFlag())
            srfdomain.setCompany_id_text(company_id_text);
        if(getUser_id_textDirtyFlag())
            srfdomain.setUser_id_text(user_id_text);
        if(getCash_rounding_id_textDirtyFlag())
            srfdomain.setCash_rounding_id_text(cash_rounding_id_text);
        if(getVendor_bill_id_textDirtyFlag())
            srfdomain.setVendor_bill_id_text(vendor_bill_id_text);
        if(getRefund_invoice_id_textDirtyFlag())
            srfdomain.setRefund_invoice_id_text(refund_invoice_id_text);
        if(getFiscal_position_id_textDirtyFlag())
            srfdomain.setFiscal_position_id_text(fiscal_position_id_text);
        if(getNumberDirtyFlag())
            srfdomain.setNumber(number);
        if(getCommercial_partner_id_textDirtyFlag())
            srfdomain.setCommercial_partner_id_text(commercial_partner_id_text);
        if(getTeam_id_textDirtyFlag())
            srfdomain.setTeam_id_text(team_id_text);
        if(getPayment_term_id_textDirtyFlag())
            srfdomain.setPayment_term_id_text(payment_term_id_text);
        if(getUser_idDirtyFlag())
            srfdomain.setUser_id(user_id);
        if(getTeam_idDirtyFlag())
            srfdomain.setTeam_id(team_id);
        if(getCreate_uidDirtyFlag())
            srfdomain.setCreate_uid(create_uid);
        if(getAccount_idDirtyFlag())
            srfdomain.setAccount_id(account_id);
        if(getMedium_idDirtyFlag())
            srfdomain.setMedium_id(medium_id);
        if(getCompany_idDirtyFlag())
            srfdomain.setCompany_id(company_id);
        if(getMove_idDirtyFlag())
            srfdomain.setMove_id(move_id);
        if(getPayment_term_idDirtyFlag())
            srfdomain.setPayment_term_id(payment_term_id);
        if(getJournal_idDirtyFlag())
            srfdomain.setJournal_id(journal_id);
        if(getIncoterm_idDirtyFlag())
            srfdomain.setIncoterm_id(incoterm_id);
        if(getPurchase_idDirtyFlag())
            srfdomain.setPurchase_id(purchase_id);
        if(getFiscal_position_idDirtyFlag())
            srfdomain.setFiscal_position_id(fiscal_position_id);
        if(getRefund_invoice_idDirtyFlag())
            srfdomain.setRefund_invoice_id(refund_invoice_id);
        if(getCurrency_idDirtyFlag())
            srfdomain.setCurrency_id(currency_id);
        if(getPartner_idDirtyFlag())
            srfdomain.setPartner_id(partner_id);
        if(getPartner_bank_idDirtyFlag())
            srfdomain.setPartner_bank_id(partner_bank_id);
        if(getCash_rounding_idDirtyFlag())
            srfdomain.setCash_rounding_id(cash_rounding_id);
        if(getCommercial_partner_idDirtyFlag())
            srfdomain.setCommercial_partner_id(commercial_partner_id);
        if(getPartner_shipping_idDirtyFlag())
            srfdomain.setPartner_shipping_id(partner_shipping_id);
        if(getIncoterms_idDirtyFlag())
            srfdomain.setIncoterms_id(incoterms_id);
        if(getSource_idDirtyFlag())
            srfdomain.setSource_id(source_id);
        if(getVendor_bill_idDirtyFlag())
            srfdomain.setVendor_bill_id(vendor_bill_id);
        if(getWrite_uidDirtyFlag())
            srfdomain.setWrite_uid(write_uid);
        if(getVendor_bill_purchase_idDirtyFlag())
            srfdomain.setVendor_bill_purchase_id(vendor_bill_purchase_id);
        if(getCampaign_idDirtyFlag())
            srfdomain.setCampaign_id(campaign_id);

        return srfdomain;
    }

    public void fromDO(Account_invoice srfdomain) {
        if(srfdomain == null )
          return ;
        if(srfdomain.getOutstanding_credits_debits_widgetDirtyFlag())
            this.setOutstanding_credits_debits_widget(srfdomain.getOutstanding_credits_debits_widget());
        if(srfdomain.getActivity_date_deadlineDirtyFlag())
            this.setActivity_date_deadline(srfdomain.getActivity_date_deadline());
        if(srfdomain.getMessage_channel_idsDirtyFlag())
            this.setMessage_channel_ids(srfdomain.getMessage_channel_ids());
        if(srfdomain.getMove_nameDirtyFlag())
            this.setMove_name(srfdomain.getMove_name());
        if(srfdomain.getActivity_summaryDirtyFlag())
            this.setActivity_summary(srfdomain.getActivity_summary());
        if(srfdomain.getAccess_urlDirtyFlag())
            this.setAccess_url(srfdomain.getAccess_url());
        if(srfdomain.getPayment_move_line_idsDirtyFlag())
            this.setPayment_move_line_ids(srfdomain.getPayment_move_line_ids());
        if(srfdomain.getOriginDirtyFlag())
            this.setOrigin(srfdomain.getOrigin());
        if(srfdomain.getDateDirtyFlag())
            this.setDate(srfdomain.getDate());
        if(srfdomain.getAmount_total_company_signedDirtyFlag())
            this.setAmount_total_company_signed(srfdomain.getAmount_total_company_signed());
        if(srfdomain.getMessage_needaction_counterDirtyFlag())
            this.setMessage_needaction_counter(srfdomain.getMessage_needaction_counter());
        if(srfdomain.getAmount_untaxed_signedDirtyFlag())
            this.setAmount_untaxed_signed(srfdomain.getAmount_untaxed_signed());
        if(srfdomain.getResidualDirtyFlag())
            this.setResidual(srfdomain.getResidual());
        if(srfdomain.getMessage_main_attachment_idDirtyFlag())
            this.setMessage_main_attachment_id(srfdomain.getMessage_main_attachment_id());
        if(srfdomain.getHas_outstandingDirtyFlag())
            this.setHas_outstanding(srfdomain.getHas_outstanding());
        if(srfdomain.getCommentDirtyFlag())
            this.setComment(srfdomain.getComment());
        if(srfdomain.getMessage_unreadDirtyFlag())
            this.setMessage_unread(srfdomain.getMessage_unread());
        if(srfdomain.getActivity_type_idDirtyFlag())
            this.setActivity_type_id(srfdomain.getActivity_type_id());
        if(srfdomain.getAmount_by_groupDirtyFlag())
            this.setAmount_by_group(srfdomain.getAmount_by_group());
        if(srfdomain.getPayments_widgetDirtyFlag())
            this.setPayments_widget(srfdomain.getPayments_widget());
        if(srfdomain.getReconciledDirtyFlag())
            this.setReconciled(srfdomain.getReconciled());
        if(srfdomain.getDisplay_nameDirtyFlag())
            this.setDisplay_name(srfdomain.getDisplay_name());
        if(srfdomain.getSentDirtyFlag())
            this.setSent(srfdomain.getSent());
        if(srfdomain.getAmount_total_signedDirtyFlag())
            this.setAmount_total_signed(srfdomain.getAmount_total_signed());
        if(srfdomain.getInvoice_line_idsDirtyFlag())
            this.setInvoice_line_ids(srfdomain.getInvoice_line_ids());
        if(srfdomain.getWrite_dateDirtyFlag())
            this.setWrite_date(srfdomain.getWrite_date());
        if(srfdomain.getAccess_warningDirtyFlag())
            this.setAccess_warning(srfdomain.getAccess_warning());
        if(srfdomain.getCreate_dateDirtyFlag())
            this.setCreate_date(srfdomain.getCreate_date());
        if(srfdomain.getActivity_stateDirtyFlag())
            this.setActivity_state(srfdomain.getActivity_state());
        if(srfdomain.getResidual_signedDirtyFlag())
            this.setResidual_signed(srfdomain.getResidual_signed());
        if(srfdomain.getMessage_unread_counterDirtyFlag())
            this.setMessage_unread_counter(srfdomain.getMessage_unread_counter());
        if(srfdomain.getAccess_tokenDirtyFlag())
            this.setAccess_token(srfdomain.getAccess_token());
        if(srfdomain.getVendor_display_nameDirtyFlag())
            this.setVendor_display_name(srfdomain.getVendor_display_name());
        if(srfdomain.getMessage_attachment_countDirtyFlag())
            this.setMessage_attachment_count(srfdomain.getMessage_attachment_count());
        if(srfdomain.getMessage_idsDirtyFlag())
            this.setMessage_ids(srfdomain.getMessage_ids());
        if(srfdomain.getAmount_untaxedDirtyFlag())
            this.setAmount_untaxed(srfdomain.getAmount_untaxed());
        if(srfdomain.getTransaction_idsDirtyFlag())
            this.setTransaction_ids(srfdomain.getTransaction_ids());
        if(srfdomain.getDate_dueDirtyFlag())
            this.setDate_due(srfdomain.getDate_due());
        if(srfdomain.getMessage_partner_idsDirtyFlag())
            this.setMessage_partner_ids(srfdomain.getMessage_partner_ids());
        if(srfdomain.get__last_updateDirtyFlag())
            this.set__last_update(srfdomain.get__last_update());
        if(srfdomain.getAuthorized_transaction_idsDirtyFlag())
            this.setAuthorized_transaction_ids(srfdomain.getAuthorized_transaction_ids());
        if(srfdomain.getTax_line_idsDirtyFlag())
            this.setTax_line_ids(srfdomain.getTax_line_ids());
        if(srfdomain.getMessage_follower_idsDirtyFlag())
            this.setMessage_follower_ids(srfdomain.getMessage_follower_ids());
        if(srfdomain.getSource_emailDirtyFlag())
            this.setSource_email(srfdomain.getSource_email());
        if(srfdomain.getAmount_taxDirtyFlag())
            this.setAmount_tax(srfdomain.getAmount_tax());
        if(srfdomain.getMessage_is_followerDirtyFlag())
            this.setMessage_is_follower(srfdomain.getMessage_is_follower());
        if(srfdomain.getStateDirtyFlag())
            this.setState(srfdomain.getState());
        if(srfdomain.getMessage_has_errorDirtyFlag())
            this.setMessage_has_error(srfdomain.getMessage_has_error());
        if(srfdomain.getAmount_untaxed_invoice_signedDirtyFlag())
            this.setAmount_untaxed_invoice_signed(srfdomain.getAmount_untaxed_invoice_signed());
        if(srfdomain.getMessage_needactionDirtyFlag())
            this.setMessage_needaction(srfdomain.getMessage_needaction());
        if(srfdomain.getSequence_number_next_prefixDirtyFlag())
            this.setSequence_number_next_prefix(srfdomain.getSequence_number_next_prefix());
        if(srfdomain.getIdDirtyFlag())
            this.setId(srfdomain.getId());
        if(srfdomain.getRefund_invoice_idsDirtyFlag())
            this.setRefund_invoice_ids(srfdomain.getRefund_invoice_ids());
        if(srfdomain.getResidual_company_signedDirtyFlag())
            this.setResidual_company_signed(srfdomain.getResidual_company_signed());
        if(srfdomain.getDate_invoiceDirtyFlag())
            this.setDate_invoice(srfdomain.getDate_invoice());
        if(srfdomain.getNameDirtyFlag())
            this.setName(srfdomain.getName());
        if(srfdomain.getActivity_user_idDirtyFlag())
            this.setActivity_user_id(srfdomain.getActivity_user_id());
        if(srfdomain.getReferenceDirtyFlag())
            this.setReference(srfdomain.getReference());
        if(srfdomain.getWebsite_idDirtyFlag())
            this.setWebsite_id(srfdomain.getWebsite_id());
        if(srfdomain.getSequence_number_nextDirtyFlag())
            this.setSequence_number_next(srfdomain.getSequence_number_next());
        if(srfdomain.getWebsite_message_idsDirtyFlag())
            this.setWebsite_message_ids(srfdomain.getWebsite_message_ids());
        if(srfdomain.getAmount_tax_signedDirtyFlag())
            this.setAmount_tax_signed(srfdomain.getAmount_tax_signed());
        if(srfdomain.getInvoice_iconDirtyFlag())
            this.setInvoice_icon(srfdomain.getInvoice_icon());
        if(srfdomain.getPayment_idsDirtyFlag())
            this.setPayment_ids(srfdomain.getPayment_ids());
        if(srfdomain.getMessage_has_error_counterDirtyFlag())
            this.setMessage_has_error_counter(srfdomain.getMessage_has_error_counter());
        if(srfdomain.getAmount_totalDirtyFlag())
            this.setAmount_total(srfdomain.getAmount_total());
        if(srfdomain.getActivity_idsDirtyFlag())
            this.setActivity_ids(srfdomain.getActivity_ids());
        if(srfdomain.getTypeDirtyFlag())
            this.setType(srfdomain.getType());
        if(srfdomain.getJournal_id_textDirtyFlag())
            this.setJournal_id_text(srfdomain.getJournal_id_text());
        if(srfdomain.getPurchase_id_textDirtyFlag())
            this.setPurchase_id_text(srfdomain.getPurchase_id_text());
        if(srfdomain.getVendor_bill_purchase_id_textDirtyFlag())
            this.setVendor_bill_purchase_id_text(srfdomain.getVendor_bill_purchase_id_text());
        if(srfdomain.getSource_id_textDirtyFlag())
            this.setSource_id_text(srfdomain.getSource_id_text());
        if(srfdomain.getCampaign_id_textDirtyFlag())
            this.setCampaign_id_text(srfdomain.getCampaign_id_text());
        if(srfdomain.getPartner_shipping_id_textDirtyFlag())
            this.setPartner_shipping_id_text(srfdomain.getPartner_shipping_id_text());
        if(srfdomain.getCurrency_id_textDirtyFlag())
            this.setCurrency_id_text(srfdomain.getCurrency_id_text());
        if(srfdomain.getWrite_uid_textDirtyFlag())
            this.setWrite_uid_text(srfdomain.getWrite_uid_text());
        if(srfdomain.getIncoterms_id_textDirtyFlag())
            this.setIncoterms_id_text(srfdomain.getIncoterms_id_text());
        if(srfdomain.getCompany_currency_idDirtyFlag())
            this.setCompany_currency_id(srfdomain.getCompany_currency_id());
        if(srfdomain.getAccount_id_textDirtyFlag())
            this.setAccount_id_text(srfdomain.getAccount_id_text());
        if(srfdomain.getIncoterm_id_textDirtyFlag())
            this.setIncoterm_id_text(srfdomain.getIncoterm_id_text());
        if(srfdomain.getPartner_id_textDirtyFlag())
            this.setPartner_id_text(srfdomain.getPartner_id_text());
        if(srfdomain.getMedium_id_textDirtyFlag())
            this.setMedium_id_text(srfdomain.getMedium_id_text());
        if(srfdomain.getCreate_uid_textDirtyFlag())
            this.setCreate_uid_text(srfdomain.getCreate_uid_text());
        if(srfdomain.getCompany_id_textDirtyFlag())
            this.setCompany_id_text(srfdomain.getCompany_id_text());
        if(srfdomain.getUser_id_textDirtyFlag())
            this.setUser_id_text(srfdomain.getUser_id_text());
        if(srfdomain.getCash_rounding_id_textDirtyFlag())
            this.setCash_rounding_id_text(srfdomain.getCash_rounding_id_text());
        if(srfdomain.getVendor_bill_id_textDirtyFlag())
            this.setVendor_bill_id_text(srfdomain.getVendor_bill_id_text());
        if(srfdomain.getRefund_invoice_id_textDirtyFlag())
            this.setRefund_invoice_id_text(srfdomain.getRefund_invoice_id_text());
        if(srfdomain.getFiscal_position_id_textDirtyFlag())
            this.setFiscal_position_id_text(srfdomain.getFiscal_position_id_text());
        if(srfdomain.getNumberDirtyFlag())
            this.setNumber(srfdomain.getNumber());
        if(srfdomain.getCommercial_partner_id_textDirtyFlag())
            this.setCommercial_partner_id_text(srfdomain.getCommercial_partner_id_text());
        if(srfdomain.getTeam_id_textDirtyFlag())
            this.setTeam_id_text(srfdomain.getTeam_id_text());
        if(srfdomain.getPayment_term_id_textDirtyFlag())
            this.setPayment_term_id_text(srfdomain.getPayment_term_id_text());
        if(srfdomain.getUser_idDirtyFlag())
            this.setUser_id(srfdomain.getUser_id());
        if(srfdomain.getTeam_idDirtyFlag())
            this.setTeam_id(srfdomain.getTeam_id());
        if(srfdomain.getCreate_uidDirtyFlag())
            this.setCreate_uid(srfdomain.getCreate_uid());
        if(srfdomain.getAccount_idDirtyFlag())
            this.setAccount_id(srfdomain.getAccount_id());
        if(srfdomain.getMedium_idDirtyFlag())
            this.setMedium_id(srfdomain.getMedium_id());
        if(srfdomain.getCompany_idDirtyFlag())
            this.setCompany_id(srfdomain.getCompany_id());
        if(srfdomain.getMove_idDirtyFlag())
            this.setMove_id(srfdomain.getMove_id());
        if(srfdomain.getPayment_term_idDirtyFlag())
            this.setPayment_term_id(srfdomain.getPayment_term_id());
        if(srfdomain.getJournal_idDirtyFlag())
            this.setJournal_id(srfdomain.getJournal_id());
        if(srfdomain.getIncoterm_idDirtyFlag())
            this.setIncoterm_id(srfdomain.getIncoterm_id());
        if(srfdomain.getPurchase_idDirtyFlag())
            this.setPurchase_id(srfdomain.getPurchase_id());
        if(srfdomain.getFiscal_position_idDirtyFlag())
            this.setFiscal_position_id(srfdomain.getFiscal_position_id());
        if(srfdomain.getRefund_invoice_idDirtyFlag())
            this.setRefund_invoice_id(srfdomain.getRefund_invoice_id());
        if(srfdomain.getCurrency_idDirtyFlag())
            this.setCurrency_id(srfdomain.getCurrency_id());
        if(srfdomain.getPartner_idDirtyFlag())
            this.setPartner_id(srfdomain.getPartner_id());
        if(srfdomain.getPartner_bank_idDirtyFlag())
            this.setPartner_bank_id(srfdomain.getPartner_bank_id());
        if(srfdomain.getCash_rounding_idDirtyFlag())
            this.setCash_rounding_id(srfdomain.getCash_rounding_id());
        if(srfdomain.getCommercial_partner_idDirtyFlag())
            this.setCommercial_partner_id(srfdomain.getCommercial_partner_id());
        if(srfdomain.getPartner_shipping_idDirtyFlag())
            this.setPartner_shipping_id(srfdomain.getPartner_shipping_id());
        if(srfdomain.getIncoterms_idDirtyFlag())
            this.setIncoterms_id(srfdomain.getIncoterms_id());
        if(srfdomain.getSource_idDirtyFlag())
            this.setSource_id(srfdomain.getSource_id());
        if(srfdomain.getVendor_bill_idDirtyFlag())
            this.setVendor_bill_id(srfdomain.getVendor_bill_id());
        if(srfdomain.getWrite_uidDirtyFlag())
            this.setWrite_uid(srfdomain.getWrite_uid());
        if(srfdomain.getVendor_bill_purchase_idDirtyFlag())
            this.setVendor_bill_purchase_id(srfdomain.getVendor_bill_purchase_id());
        if(srfdomain.getCampaign_idDirtyFlag())
            this.setCampaign_id(srfdomain.getCampaign_id());

    }

    public List<Account_invoiceDTO> fromDOPage(List<Account_invoice> poPage)   {
        if(poPage == null)
            return null;
        List<Account_invoiceDTO> dtos=new ArrayList<Account_invoiceDTO>();
        for(Account_invoice domain : poPage) {
            Account_invoiceDTO dto = new Account_invoiceDTO();
            dto.fromDO(domain);
            dtos.add(dto);
        }
        return dtos;
    }
}

