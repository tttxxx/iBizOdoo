package cn.ibizlab.odoo.service.odoo_account.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.core.odoo_account.valuerule.anno.account_full_reconcile.*;
import cn.ibizlab.odoo.core.odoo_account.domain.Account_full_reconcile;
import org.springframework.cglib.beans.BeanCopier;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 服务DTO对象[Account_full_reconcileDTO]
 */
public class Account_full_reconcileDTO implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [PARTIAL_RECONCILE_IDS]
     *
     */
    @Account_full_reconcilePartial_reconcile_idsDefault(info = "默认规则")
    private String partial_reconcile_ids;

    @JsonIgnore
    private boolean partial_reconcile_idsDirtyFlag;

    /**
     * 属性 [WRITE_DATE]
     *
     */
    @Account_full_reconcileWrite_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp write_date;

    @JsonIgnore
    private boolean write_dateDirtyFlag;

    /**
     * 属性 [ID]
     *
     */
    @Account_full_reconcileIdDefault(info = "默认规则")
    private Integer id;

    @JsonIgnore
    private boolean idDirtyFlag;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @Account_full_reconcile__last_updateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp __last_update;

    @JsonIgnore
    private boolean __last_updateDirtyFlag;

    /**
     * 属性 [NAME]
     *
     */
    @Account_full_reconcileNameDefault(info = "默认规则")
    private String name;

    @JsonIgnore
    private boolean nameDirtyFlag;

    /**
     * 属性 [CREATE_DATE]
     *
     */
    @Account_full_reconcileCreate_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp create_date;

    @JsonIgnore
    private boolean create_dateDirtyFlag;

    /**
     * 属性 [RECONCILED_LINE_IDS]
     *
     */
    @Account_full_reconcileReconciled_line_idsDefault(info = "默认规则")
    private String reconciled_line_ids;

    @JsonIgnore
    private boolean reconciled_line_idsDirtyFlag;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @Account_full_reconcileDisplay_nameDefault(info = "默认规则")
    private String display_name;

    @JsonIgnore
    private boolean display_nameDirtyFlag;

    /**
     * 属性 [WRITE_UID_TEXT]
     *
     */
    @Account_full_reconcileWrite_uid_textDefault(info = "默认规则")
    private String write_uid_text;

    @JsonIgnore
    private boolean write_uid_textDirtyFlag;

    /**
     * 属性 [EXCHANGE_MOVE_ID_TEXT]
     *
     */
    @Account_full_reconcileExchange_move_id_textDefault(info = "默认规则")
    private String exchange_move_id_text;

    @JsonIgnore
    private boolean exchange_move_id_textDirtyFlag;

    /**
     * 属性 [CREATE_UID_TEXT]
     *
     */
    @Account_full_reconcileCreate_uid_textDefault(info = "默认规则")
    private String create_uid_text;

    @JsonIgnore
    private boolean create_uid_textDirtyFlag;

    /**
     * 属性 [WRITE_UID]
     *
     */
    @Account_full_reconcileWrite_uidDefault(info = "默认规则")
    private Integer write_uid;

    @JsonIgnore
    private boolean write_uidDirtyFlag;

    /**
     * 属性 [CREATE_UID]
     *
     */
    @Account_full_reconcileCreate_uidDefault(info = "默认规则")
    private Integer create_uid;

    @JsonIgnore
    private boolean create_uidDirtyFlag;

    /**
     * 属性 [EXCHANGE_MOVE_ID]
     *
     */
    @Account_full_reconcileExchange_move_idDefault(info = "默认规则")
    private Integer exchange_move_id;

    @JsonIgnore
    private boolean exchange_move_idDirtyFlag;


    /**
     * 获取 [PARTIAL_RECONCILE_IDS]
     */
    @JsonProperty("partial_reconcile_ids")
    public String getPartial_reconcile_ids(){
        return partial_reconcile_ids ;
    }

    /**
     * 设置 [PARTIAL_RECONCILE_IDS]
     */
    @JsonProperty("partial_reconcile_ids")
    public void setPartial_reconcile_ids(String  partial_reconcile_ids){
        this.partial_reconcile_ids = partial_reconcile_ids ;
        this.partial_reconcile_idsDirtyFlag = true ;
    }

    /**
     * 获取 [PARTIAL_RECONCILE_IDS]脏标记
     */
    @JsonIgnore
    public boolean getPartial_reconcile_idsDirtyFlag(){
        return partial_reconcile_idsDirtyFlag ;
    }

    /**
     * 获取 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return write_date ;
    }

    /**
     * 设置 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return write_dateDirtyFlag ;
    }

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

    /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return idDirtyFlag ;
    }

    /**
     * 获取 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return __last_update ;
    }

    /**
     * 设置 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

    /**
     * 获取 [__LAST_UPDATE]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return __last_updateDirtyFlag ;
    }

    /**
     * 获取 [NAME]
     */
    @JsonProperty("name")
    public String getName(){
        return name ;
    }

    /**
     * 设置 [NAME]
     */
    @JsonProperty("name")
    public void setName(String  name){
        this.name = name ;
        this.nameDirtyFlag = true ;
    }

    /**
     * 获取 [NAME]脏标记
     */
    @JsonIgnore
    public boolean getNameDirtyFlag(){
        return nameDirtyFlag ;
    }

    /**
     * 获取 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return create_date ;
    }

    /**
     * 设置 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return create_dateDirtyFlag ;
    }

    /**
     * 获取 [RECONCILED_LINE_IDS]
     */
    @JsonProperty("reconciled_line_ids")
    public String getReconciled_line_ids(){
        return reconciled_line_ids ;
    }

    /**
     * 设置 [RECONCILED_LINE_IDS]
     */
    @JsonProperty("reconciled_line_ids")
    public void setReconciled_line_ids(String  reconciled_line_ids){
        this.reconciled_line_ids = reconciled_line_ids ;
        this.reconciled_line_idsDirtyFlag = true ;
    }

    /**
     * 获取 [RECONCILED_LINE_IDS]脏标记
     */
    @JsonIgnore
    public boolean getReconciled_line_idsDirtyFlag(){
        return reconciled_line_idsDirtyFlag ;
    }

    /**
     * 获取 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return display_name ;
    }

    /**
     * 设置 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

    /**
     * 获取 [DISPLAY_NAME]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return display_nameDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return write_uid_text ;
    }

    /**
     * 设置 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return write_uid_textDirtyFlag ;
    }

    /**
     * 获取 [EXCHANGE_MOVE_ID_TEXT]
     */
    @JsonProperty("exchange_move_id_text")
    public String getExchange_move_id_text(){
        return exchange_move_id_text ;
    }

    /**
     * 设置 [EXCHANGE_MOVE_ID_TEXT]
     */
    @JsonProperty("exchange_move_id_text")
    public void setExchange_move_id_text(String  exchange_move_id_text){
        this.exchange_move_id_text = exchange_move_id_text ;
        this.exchange_move_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [EXCHANGE_MOVE_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getExchange_move_id_textDirtyFlag(){
        return exchange_move_id_textDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return create_uid_text ;
    }

    /**
     * 设置 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return create_uid_textDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return write_uid ;
    }

    /**
     * 设置 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return write_uidDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return create_uid ;
    }

    /**
     * 设置 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return create_uidDirtyFlag ;
    }

    /**
     * 获取 [EXCHANGE_MOVE_ID]
     */
    @JsonProperty("exchange_move_id")
    public Integer getExchange_move_id(){
        return exchange_move_id ;
    }

    /**
     * 设置 [EXCHANGE_MOVE_ID]
     */
    @JsonProperty("exchange_move_id")
    public void setExchange_move_id(Integer  exchange_move_id){
        this.exchange_move_id = exchange_move_id ;
        this.exchange_move_idDirtyFlag = true ;
    }

    /**
     * 获取 [EXCHANGE_MOVE_ID]脏标记
     */
    @JsonIgnore
    public boolean getExchange_move_idDirtyFlag(){
        return exchange_move_idDirtyFlag ;
    }



    public Account_full_reconcile toDO() {
        Account_full_reconcile srfdomain = new Account_full_reconcile();
        if(getPartial_reconcile_idsDirtyFlag())
            srfdomain.setPartial_reconcile_ids(partial_reconcile_ids);
        if(getWrite_dateDirtyFlag())
            srfdomain.setWrite_date(write_date);
        if(getIdDirtyFlag())
            srfdomain.setId(id);
        if(get__last_updateDirtyFlag())
            srfdomain.set__last_update(__last_update);
        if(getNameDirtyFlag())
            srfdomain.setName(name);
        if(getCreate_dateDirtyFlag())
            srfdomain.setCreate_date(create_date);
        if(getReconciled_line_idsDirtyFlag())
            srfdomain.setReconciled_line_ids(reconciled_line_ids);
        if(getDisplay_nameDirtyFlag())
            srfdomain.setDisplay_name(display_name);
        if(getWrite_uid_textDirtyFlag())
            srfdomain.setWrite_uid_text(write_uid_text);
        if(getExchange_move_id_textDirtyFlag())
            srfdomain.setExchange_move_id_text(exchange_move_id_text);
        if(getCreate_uid_textDirtyFlag())
            srfdomain.setCreate_uid_text(create_uid_text);
        if(getWrite_uidDirtyFlag())
            srfdomain.setWrite_uid(write_uid);
        if(getCreate_uidDirtyFlag())
            srfdomain.setCreate_uid(create_uid);
        if(getExchange_move_idDirtyFlag())
            srfdomain.setExchange_move_id(exchange_move_id);

        return srfdomain;
    }

    public void fromDO(Account_full_reconcile srfdomain) {
        if(srfdomain == null )
          return ;
        if(srfdomain.getPartial_reconcile_idsDirtyFlag())
            this.setPartial_reconcile_ids(srfdomain.getPartial_reconcile_ids());
        if(srfdomain.getWrite_dateDirtyFlag())
            this.setWrite_date(srfdomain.getWrite_date());
        if(srfdomain.getIdDirtyFlag())
            this.setId(srfdomain.getId());
        if(srfdomain.get__last_updateDirtyFlag())
            this.set__last_update(srfdomain.get__last_update());
        if(srfdomain.getNameDirtyFlag())
            this.setName(srfdomain.getName());
        if(srfdomain.getCreate_dateDirtyFlag())
            this.setCreate_date(srfdomain.getCreate_date());
        if(srfdomain.getReconciled_line_idsDirtyFlag())
            this.setReconciled_line_ids(srfdomain.getReconciled_line_ids());
        if(srfdomain.getDisplay_nameDirtyFlag())
            this.setDisplay_name(srfdomain.getDisplay_name());
        if(srfdomain.getWrite_uid_textDirtyFlag())
            this.setWrite_uid_text(srfdomain.getWrite_uid_text());
        if(srfdomain.getExchange_move_id_textDirtyFlag())
            this.setExchange_move_id_text(srfdomain.getExchange_move_id_text());
        if(srfdomain.getCreate_uid_textDirtyFlag())
            this.setCreate_uid_text(srfdomain.getCreate_uid_text());
        if(srfdomain.getWrite_uidDirtyFlag())
            this.setWrite_uid(srfdomain.getWrite_uid());
        if(srfdomain.getCreate_uidDirtyFlag())
            this.setCreate_uid(srfdomain.getCreate_uid());
        if(srfdomain.getExchange_move_idDirtyFlag())
            this.setExchange_move_id(srfdomain.getExchange_move_id());

    }

    public List<Account_full_reconcileDTO> fromDOPage(List<Account_full_reconcile> poPage)   {
        if(poPage == null)
            return null;
        List<Account_full_reconcileDTO> dtos=new ArrayList<Account_full_reconcileDTO>();
        for(Account_full_reconcile domain : poPage) {
            Account_full_reconcileDTO dto = new Account_full_reconcileDTO();
            dto.fromDO(domain);
            dtos.add(dto);
        }
        return dtos;
    }
}

