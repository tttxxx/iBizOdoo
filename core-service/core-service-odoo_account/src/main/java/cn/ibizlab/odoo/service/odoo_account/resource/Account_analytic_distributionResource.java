package cn.ibizlab.odoo.service.odoo_account.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_account.dto.Account_analytic_distributionDTO;
import cn.ibizlab.odoo.core.odoo_account.domain.Account_analytic_distribution;
import cn.ibizlab.odoo.core.odoo_account.service.IAccount_analytic_distributionService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_analytic_distributionSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Account_analytic_distribution" })
@RestController
@RequestMapping("")
public class Account_analytic_distributionResource {

    @Autowired
    private IAccount_analytic_distributionService account_analytic_distributionService;

    public IAccount_analytic_distributionService getAccount_analytic_distributionService() {
        return this.account_analytic_distributionService;
    }

    @ApiOperation(value = "批删除数据", tags = {"Account_analytic_distribution" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_account/account_analytic_distributions/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Account_analytic_distributionDTO> account_analytic_distributiondtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "批建立数据", tags = {"Account_analytic_distribution" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_account/account_analytic_distributions/createBatch")
    public ResponseEntity<Boolean> createBatchAccount_analytic_distribution(@RequestBody List<Account_analytic_distributionDTO> account_analytic_distributiondtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "删除数据", tags = {"Account_analytic_distribution" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_account/account_analytic_distributions/{account_analytic_distribution_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("account_analytic_distribution_id") Integer account_analytic_distribution_id) {
        Account_analytic_distributionDTO account_analytic_distributiondto = new Account_analytic_distributionDTO();
		Account_analytic_distribution domain = new Account_analytic_distribution();
		account_analytic_distributiondto.setId(account_analytic_distribution_id);
		domain.setId(account_analytic_distribution_id);
        Boolean rst = account_analytic_distributionService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "批更新数据", tags = {"Account_analytic_distribution" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_account/account_analytic_distributions/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Account_analytic_distributionDTO> account_analytic_distributiondtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "建立数据", tags = {"Account_analytic_distribution" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_account/account_analytic_distributions")

    public ResponseEntity<Account_analytic_distributionDTO> create(@RequestBody Account_analytic_distributionDTO account_analytic_distributiondto) {
        Account_analytic_distributionDTO dto = new Account_analytic_distributionDTO();
        Account_analytic_distribution domain = account_analytic_distributiondto.toDO();
		account_analytic_distributionService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "更新数据", tags = {"Account_analytic_distribution" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_account/account_analytic_distributions/{account_analytic_distribution_id}")

    public ResponseEntity<Account_analytic_distributionDTO> update(@PathVariable("account_analytic_distribution_id") Integer account_analytic_distribution_id, @RequestBody Account_analytic_distributionDTO account_analytic_distributiondto) {
		Account_analytic_distribution domain = account_analytic_distributiondto.toDO();
        domain.setId(account_analytic_distribution_id);
		account_analytic_distributionService.update(domain);
		Account_analytic_distributionDTO dto = new Account_analytic_distributionDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取数据", tags = {"Account_analytic_distribution" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_account/account_analytic_distributions/{account_analytic_distribution_id}")
    public ResponseEntity<Account_analytic_distributionDTO> get(@PathVariable("account_analytic_distribution_id") Integer account_analytic_distribution_id) {
        Account_analytic_distributionDTO dto = new Account_analytic_distributionDTO();
        Account_analytic_distribution domain = account_analytic_distributionService.get(account_analytic_distribution_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Account_analytic_distribution" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_account/account_analytic_distributions/fetchdefault")
	public ResponseEntity<Page<Account_analytic_distributionDTO>> fetchDefault(Account_analytic_distributionSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Account_analytic_distributionDTO> list = new ArrayList<Account_analytic_distributionDTO>();
        
        Page<Account_analytic_distribution> domains = account_analytic_distributionService.searchDefault(context) ;
        for(Account_analytic_distribution account_analytic_distribution : domains.getContent()){
            Account_analytic_distributionDTO dto = new Account_analytic_distributionDTO();
            dto.fromDO(account_analytic_distribution);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
