package cn.ibizlab.odoo.service.odoo_account.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.core.odoo_account.valuerule.anno.account_reconcile_model.*;
import cn.ibizlab.odoo.core.odoo_account.domain.Account_reconcile_model;
import org.springframework.cglib.beans.BeanCopier;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 服务DTO对象[Account_reconcile_modelDTO]
 */
public class Account_reconcile_modelDTO implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [LABEL]
     *
     */
    @Account_reconcile_modelLabelDefault(info = "默认规则")
    private String label;

    @JsonIgnore
    private boolean labelDirtyFlag;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @Account_reconcile_modelDisplay_nameDefault(info = "默认规则")
    private String display_name;

    @JsonIgnore
    private boolean display_nameDirtyFlag;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @Account_reconcile_model__last_updateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp __last_update;

    @JsonIgnore
    private boolean __last_updateDirtyFlag;

    /**
     * 属性 [FORCE_TAX_INCLUDED]
     *
     */
    @Account_reconcile_modelForce_tax_includedDefault(info = "默认规则")
    private String force_tax_included;

    @JsonIgnore
    private boolean force_tax_includedDirtyFlag;

    /**
     * 属性 [AMOUNT_TYPE]
     *
     */
    @Account_reconcile_modelAmount_typeDefault(info = "默认规则")
    private String amount_type;

    @JsonIgnore
    private boolean amount_typeDirtyFlag;

    /**
     * 属性 [CREATE_DATE]
     *
     */
    @Account_reconcile_modelCreate_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp create_date;

    @JsonIgnore
    private boolean create_dateDirtyFlag;

    /**
     * 属性 [FORCE_SECOND_TAX_INCLUDED]
     *
     */
    @Account_reconcile_modelForce_second_tax_includedDefault(info = "默认规则")
    private String force_second_tax_included;

    @JsonIgnore
    private boolean force_second_tax_includedDirtyFlag;

    /**
     * 属性 [AUTO_RECONCILE]
     *
     */
    @Account_reconcile_modelAuto_reconcileDefault(info = "默认规则")
    private String auto_reconcile;

    @JsonIgnore
    private boolean auto_reconcileDirtyFlag;

    /**
     * 属性 [MATCH_TOTAL_AMOUNT]
     *
     */
    @Account_reconcile_modelMatch_total_amountDefault(info = "默认规则")
    private String match_total_amount;

    @JsonIgnore
    private boolean match_total_amountDirtyFlag;

    /**
     * 属性 [NAME]
     *
     */
    @Account_reconcile_modelNameDefault(info = "默认规则")
    private String name;

    @JsonIgnore
    private boolean nameDirtyFlag;

    /**
     * 属性 [MATCH_JOURNAL_IDS]
     *
     */
    @Account_reconcile_modelMatch_journal_idsDefault(info = "默认规则")
    private String match_journal_ids;

    @JsonIgnore
    private boolean match_journal_idsDirtyFlag;

    /**
     * 属性 [HAS_SECOND_LINE]
     *
     */
    @Account_reconcile_modelHas_second_lineDefault(info = "默认规则")
    private String has_second_line;

    @JsonIgnore
    private boolean has_second_lineDirtyFlag;

    /**
     * 属性 [ID]
     *
     */
    @Account_reconcile_modelIdDefault(info = "默认规则")
    private Integer id;

    @JsonIgnore
    private boolean idDirtyFlag;

    /**
     * 属性 [MATCH_NATURE]
     *
     */
    @Account_reconcile_modelMatch_natureDefault(info = "默认规则")
    private String match_nature;

    @JsonIgnore
    private boolean match_natureDirtyFlag;

    /**
     * 属性 [SECOND_AMOUNT_TYPE]
     *
     */
    @Account_reconcile_modelSecond_amount_typeDefault(info = "默认规则")
    private String second_amount_type;

    @JsonIgnore
    private boolean second_amount_typeDirtyFlag;

    /**
     * 属性 [MATCH_LABEL]
     *
     */
    @Account_reconcile_modelMatch_labelDefault(info = "默认规则")
    private String match_label;

    @JsonIgnore
    private boolean match_labelDirtyFlag;

    /**
     * 属性 [MATCH_AMOUNT_MAX]
     *
     */
    @Account_reconcile_modelMatch_amount_maxDefault(info = "默认规则")
    private Double match_amount_max;

    @JsonIgnore
    private boolean match_amount_maxDirtyFlag;

    /**
     * 属性 [MATCH_TOTAL_AMOUNT_PARAM]
     *
     */
    @Account_reconcile_modelMatch_total_amount_paramDefault(info = "默认规则")
    private Double match_total_amount_param;

    @JsonIgnore
    private boolean match_total_amount_paramDirtyFlag;

    /**
     * 属性 [WRITE_DATE]
     *
     */
    @Account_reconcile_modelWrite_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp write_date;

    @JsonIgnore
    private boolean write_dateDirtyFlag;

    /**
     * 属性 [SECOND_AMOUNT]
     *
     */
    @Account_reconcile_modelSecond_amountDefault(info = "默认规则")
    private Double second_amount;

    @JsonIgnore
    private boolean second_amountDirtyFlag;

    /**
     * 属性 [MATCH_AMOUNT]
     *
     */
    @Account_reconcile_modelMatch_amountDefault(info = "默认规则")
    private String match_amount;

    @JsonIgnore
    private boolean match_amountDirtyFlag;

    /**
     * 属性 [ANALYTIC_TAG_IDS]
     *
     */
    @Account_reconcile_modelAnalytic_tag_idsDefault(info = "默认规则")
    private String analytic_tag_ids;

    @JsonIgnore
    private boolean analytic_tag_idsDirtyFlag;

    /**
     * 属性 [SEQUENCE]
     *
     */
    @Account_reconcile_modelSequenceDefault(info = "默认规则")
    private Integer sequence;

    @JsonIgnore
    private boolean sequenceDirtyFlag;

    /**
     * 属性 [RULE_TYPE]
     *
     */
    @Account_reconcile_modelRule_typeDefault(info = "默认规则")
    private String rule_type;

    @JsonIgnore
    private boolean rule_typeDirtyFlag;

    /**
     * 属性 [MATCH_PARTNER_IDS]
     *
     */
    @Account_reconcile_modelMatch_partner_idsDefault(info = "默认规则")
    private String match_partner_ids;

    @JsonIgnore
    private boolean match_partner_idsDirtyFlag;

    /**
     * 属性 [MATCH_PARTNER_CATEGORY_IDS]
     *
     */
    @Account_reconcile_modelMatch_partner_category_idsDefault(info = "默认规则")
    private String match_partner_category_ids;

    @JsonIgnore
    private boolean match_partner_category_idsDirtyFlag;

    /**
     * 属性 [SECOND_LABEL]
     *
     */
    @Account_reconcile_modelSecond_labelDefault(info = "默认规则")
    private String second_label;

    @JsonIgnore
    private boolean second_labelDirtyFlag;

    /**
     * 属性 [MATCH_PARTNER]
     *
     */
    @Account_reconcile_modelMatch_partnerDefault(info = "默认规则")
    private String match_partner;

    @JsonIgnore
    private boolean match_partnerDirtyFlag;

    /**
     * 属性 [MATCH_AMOUNT_MIN]
     *
     */
    @Account_reconcile_modelMatch_amount_minDefault(info = "默认规则")
    private Double match_amount_min;

    @JsonIgnore
    private boolean match_amount_minDirtyFlag;

    /**
     * 属性 [MATCH_SAME_CURRENCY]
     *
     */
    @Account_reconcile_modelMatch_same_currencyDefault(info = "默认规则")
    private String match_same_currency;

    @JsonIgnore
    private boolean match_same_currencyDirtyFlag;

    /**
     * 属性 [SECOND_ANALYTIC_TAG_IDS]
     *
     */
    @Account_reconcile_modelSecond_analytic_tag_idsDefault(info = "默认规则")
    private String second_analytic_tag_ids;

    @JsonIgnore
    private boolean second_analytic_tag_idsDirtyFlag;

    /**
     * 属性 [AMOUNT]
     *
     */
    @Account_reconcile_modelAmountDefault(info = "默认规则")
    private Double amount;

    @JsonIgnore
    private boolean amountDirtyFlag;

    /**
     * 属性 [MATCH_LABEL_PARAM]
     *
     */
    @Account_reconcile_modelMatch_label_paramDefault(info = "默认规则")
    private String match_label_param;

    @JsonIgnore
    private boolean match_label_paramDirtyFlag;

    /**
     * 属性 [WRITE_UID_TEXT]
     *
     */
    @Account_reconcile_modelWrite_uid_textDefault(info = "默认规则")
    private String write_uid_text;

    @JsonIgnore
    private boolean write_uid_textDirtyFlag;

    /**
     * 属性 [SECOND_ANALYTIC_ACCOUNT_ID_TEXT]
     *
     */
    @Account_reconcile_modelSecond_analytic_account_id_textDefault(info = "默认规则")
    private String second_analytic_account_id_text;

    @JsonIgnore
    private boolean second_analytic_account_id_textDirtyFlag;

    /**
     * 属性 [SECOND_TAX_ID_TEXT]
     *
     */
    @Account_reconcile_modelSecond_tax_id_textDefault(info = "默认规则")
    private String second_tax_id_text;

    @JsonIgnore
    private boolean second_tax_id_textDirtyFlag;

    /**
     * 属性 [SECOND_TAX_AMOUNT_TYPE]
     *
     */
    @Account_reconcile_modelSecond_tax_amount_typeDefault(info = "默认规则")
    private String second_tax_amount_type;

    @JsonIgnore
    private boolean second_tax_amount_typeDirtyFlag;

    /**
     * 属性 [TAX_AMOUNT_TYPE]
     *
     */
    @Account_reconcile_modelTax_amount_typeDefault(info = "默认规则")
    private String tax_amount_type;

    @JsonIgnore
    private boolean tax_amount_typeDirtyFlag;

    /**
     * 属性 [JOURNAL_ID_TEXT]
     *
     */
    @Account_reconcile_modelJournal_id_textDefault(info = "默认规则")
    private String journal_id_text;

    @JsonIgnore
    private boolean journal_id_textDirtyFlag;

    /**
     * 属性 [SECOND_JOURNAL_ID_TEXT]
     *
     */
    @Account_reconcile_modelSecond_journal_id_textDefault(info = "默认规则")
    private String second_journal_id_text;

    @JsonIgnore
    private boolean second_journal_id_textDirtyFlag;

    /**
     * 属性 [IS_TAX_PRICE_INCLUDED]
     *
     */
    @Account_reconcile_modelIs_tax_price_includedDefault(info = "默认规则")
    private String is_tax_price_included;

    @JsonIgnore
    private boolean is_tax_price_includedDirtyFlag;

    /**
     * 属性 [SECOND_ACCOUNT_ID_TEXT]
     *
     */
    @Account_reconcile_modelSecond_account_id_textDefault(info = "默认规则")
    private String second_account_id_text;

    @JsonIgnore
    private boolean second_account_id_textDirtyFlag;

    /**
     * 属性 [COMPANY_ID_TEXT]
     *
     */
    @Account_reconcile_modelCompany_id_textDefault(info = "默认规则")
    private String company_id_text;

    @JsonIgnore
    private boolean company_id_textDirtyFlag;

    /**
     * 属性 [ANALYTIC_ACCOUNT_ID_TEXT]
     *
     */
    @Account_reconcile_modelAnalytic_account_id_textDefault(info = "默认规则")
    private String analytic_account_id_text;

    @JsonIgnore
    private boolean analytic_account_id_textDirtyFlag;

    /**
     * 属性 [ACCOUNT_ID_TEXT]
     *
     */
    @Account_reconcile_modelAccount_id_textDefault(info = "默认规则")
    private String account_id_text;

    @JsonIgnore
    private boolean account_id_textDirtyFlag;

    /**
     * 属性 [CREATE_UID_TEXT]
     *
     */
    @Account_reconcile_modelCreate_uid_textDefault(info = "默认规则")
    private String create_uid_text;

    @JsonIgnore
    private boolean create_uid_textDirtyFlag;

    /**
     * 属性 [TAX_ID_TEXT]
     *
     */
    @Account_reconcile_modelTax_id_textDefault(info = "默认规则")
    private String tax_id_text;

    @JsonIgnore
    private boolean tax_id_textDirtyFlag;

    /**
     * 属性 [IS_SECOND_TAX_PRICE_INCLUDED]
     *
     */
    @Account_reconcile_modelIs_second_tax_price_includedDefault(info = "默认规则")
    private String is_second_tax_price_included;

    @JsonIgnore
    private boolean is_second_tax_price_includedDirtyFlag;

    /**
     * 属性 [SECOND_ANALYTIC_ACCOUNT_ID]
     *
     */
    @Account_reconcile_modelSecond_analytic_account_idDefault(info = "默认规则")
    private Integer second_analytic_account_id;

    @JsonIgnore
    private boolean second_analytic_account_idDirtyFlag;

    /**
     * 属性 [COMPANY_ID]
     *
     */
    @Account_reconcile_modelCompany_idDefault(info = "默认规则")
    private Integer company_id;

    @JsonIgnore
    private boolean company_idDirtyFlag;

    /**
     * 属性 [TAX_ID]
     *
     */
    @Account_reconcile_modelTax_idDefault(info = "默认规则")
    private Integer tax_id;

    @JsonIgnore
    private boolean tax_idDirtyFlag;

    /**
     * 属性 [SECOND_ACCOUNT_ID]
     *
     */
    @Account_reconcile_modelSecond_account_idDefault(info = "默认规则")
    private Integer second_account_id;

    @JsonIgnore
    private boolean second_account_idDirtyFlag;

    /**
     * 属性 [SECOND_JOURNAL_ID]
     *
     */
    @Account_reconcile_modelSecond_journal_idDefault(info = "默认规则")
    private Integer second_journal_id;

    @JsonIgnore
    private boolean second_journal_idDirtyFlag;

    /**
     * 属性 [ANALYTIC_ACCOUNT_ID]
     *
     */
    @Account_reconcile_modelAnalytic_account_idDefault(info = "默认规则")
    private Integer analytic_account_id;

    @JsonIgnore
    private boolean analytic_account_idDirtyFlag;

    /**
     * 属性 [WRITE_UID]
     *
     */
    @Account_reconcile_modelWrite_uidDefault(info = "默认规则")
    private Integer write_uid;

    @JsonIgnore
    private boolean write_uidDirtyFlag;

    /**
     * 属性 [SECOND_TAX_ID]
     *
     */
    @Account_reconcile_modelSecond_tax_idDefault(info = "默认规则")
    private Integer second_tax_id;

    @JsonIgnore
    private boolean second_tax_idDirtyFlag;

    /**
     * 属性 [ACCOUNT_ID]
     *
     */
    @Account_reconcile_modelAccount_idDefault(info = "默认规则")
    private Integer account_id;

    @JsonIgnore
    private boolean account_idDirtyFlag;

    /**
     * 属性 [JOURNAL_ID]
     *
     */
    @Account_reconcile_modelJournal_idDefault(info = "默认规则")
    private Integer journal_id;

    @JsonIgnore
    private boolean journal_idDirtyFlag;

    /**
     * 属性 [CREATE_UID]
     *
     */
    @Account_reconcile_modelCreate_uidDefault(info = "默认规则")
    private Integer create_uid;

    @JsonIgnore
    private boolean create_uidDirtyFlag;


    /**
     * 获取 [LABEL]
     */
    @JsonProperty("label")
    public String getLabel(){
        return label ;
    }

    /**
     * 设置 [LABEL]
     */
    @JsonProperty("label")
    public void setLabel(String  label){
        this.label = label ;
        this.labelDirtyFlag = true ;
    }

    /**
     * 获取 [LABEL]脏标记
     */
    @JsonIgnore
    public boolean getLabelDirtyFlag(){
        return labelDirtyFlag ;
    }

    /**
     * 获取 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return display_name ;
    }

    /**
     * 设置 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

    /**
     * 获取 [DISPLAY_NAME]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return display_nameDirtyFlag ;
    }

    /**
     * 获取 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return __last_update ;
    }

    /**
     * 设置 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

    /**
     * 获取 [__LAST_UPDATE]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return __last_updateDirtyFlag ;
    }

    /**
     * 获取 [FORCE_TAX_INCLUDED]
     */
    @JsonProperty("force_tax_included")
    public String getForce_tax_included(){
        return force_tax_included ;
    }

    /**
     * 设置 [FORCE_TAX_INCLUDED]
     */
    @JsonProperty("force_tax_included")
    public void setForce_tax_included(String  force_tax_included){
        this.force_tax_included = force_tax_included ;
        this.force_tax_includedDirtyFlag = true ;
    }

    /**
     * 获取 [FORCE_TAX_INCLUDED]脏标记
     */
    @JsonIgnore
    public boolean getForce_tax_includedDirtyFlag(){
        return force_tax_includedDirtyFlag ;
    }

    /**
     * 获取 [AMOUNT_TYPE]
     */
    @JsonProperty("amount_type")
    public String getAmount_type(){
        return amount_type ;
    }

    /**
     * 设置 [AMOUNT_TYPE]
     */
    @JsonProperty("amount_type")
    public void setAmount_type(String  amount_type){
        this.amount_type = amount_type ;
        this.amount_typeDirtyFlag = true ;
    }

    /**
     * 获取 [AMOUNT_TYPE]脏标记
     */
    @JsonIgnore
    public boolean getAmount_typeDirtyFlag(){
        return amount_typeDirtyFlag ;
    }

    /**
     * 获取 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return create_date ;
    }

    /**
     * 设置 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return create_dateDirtyFlag ;
    }

    /**
     * 获取 [FORCE_SECOND_TAX_INCLUDED]
     */
    @JsonProperty("force_second_tax_included")
    public String getForce_second_tax_included(){
        return force_second_tax_included ;
    }

    /**
     * 设置 [FORCE_SECOND_TAX_INCLUDED]
     */
    @JsonProperty("force_second_tax_included")
    public void setForce_second_tax_included(String  force_second_tax_included){
        this.force_second_tax_included = force_second_tax_included ;
        this.force_second_tax_includedDirtyFlag = true ;
    }

    /**
     * 获取 [FORCE_SECOND_TAX_INCLUDED]脏标记
     */
    @JsonIgnore
    public boolean getForce_second_tax_includedDirtyFlag(){
        return force_second_tax_includedDirtyFlag ;
    }

    /**
     * 获取 [AUTO_RECONCILE]
     */
    @JsonProperty("auto_reconcile")
    public String getAuto_reconcile(){
        return auto_reconcile ;
    }

    /**
     * 设置 [AUTO_RECONCILE]
     */
    @JsonProperty("auto_reconcile")
    public void setAuto_reconcile(String  auto_reconcile){
        this.auto_reconcile = auto_reconcile ;
        this.auto_reconcileDirtyFlag = true ;
    }

    /**
     * 获取 [AUTO_RECONCILE]脏标记
     */
    @JsonIgnore
    public boolean getAuto_reconcileDirtyFlag(){
        return auto_reconcileDirtyFlag ;
    }

    /**
     * 获取 [MATCH_TOTAL_AMOUNT]
     */
    @JsonProperty("match_total_amount")
    public String getMatch_total_amount(){
        return match_total_amount ;
    }

    /**
     * 设置 [MATCH_TOTAL_AMOUNT]
     */
    @JsonProperty("match_total_amount")
    public void setMatch_total_amount(String  match_total_amount){
        this.match_total_amount = match_total_amount ;
        this.match_total_amountDirtyFlag = true ;
    }

    /**
     * 获取 [MATCH_TOTAL_AMOUNT]脏标记
     */
    @JsonIgnore
    public boolean getMatch_total_amountDirtyFlag(){
        return match_total_amountDirtyFlag ;
    }

    /**
     * 获取 [NAME]
     */
    @JsonProperty("name")
    public String getName(){
        return name ;
    }

    /**
     * 设置 [NAME]
     */
    @JsonProperty("name")
    public void setName(String  name){
        this.name = name ;
        this.nameDirtyFlag = true ;
    }

    /**
     * 获取 [NAME]脏标记
     */
    @JsonIgnore
    public boolean getNameDirtyFlag(){
        return nameDirtyFlag ;
    }

    /**
     * 获取 [MATCH_JOURNAL_IDS]
     */
    @JsonProperty("match_journal_ids")
    public String getMatch_journal_ids(){
        return match_journal_ids ;
    }

    /**
     * 设置 [MATCH_JOURNAL_IDS]
     */
    @JsonProperty("match_journal_ids")
    public void setMatch_journal_ids(String  match_journal_ids){
        this.match_journal_ids = match_journal_ids ;
        this.match_journal_idsDirtyFlag = true ;
    }

    /**
     * 获取 [MATCH_JOURNAL_IDS]脏标记
     */
    @JsonIgnore
    public boolean getMatch_journal_idsDirtyFlag(){
        return match_journal_idsDirtyFlag ;
    }

    /**
     * 获取 [HAS_SECOND_LINE]
     */
    @JsonProperty("has_second_line")
    public String getHas_second_line(){
        return has_second_line ;
    }

    /**
     * 设置 [HAS_SECOND_LINE]
     */
    @JsonProperty("has_second_line")
    public void setHas_second_line(String  has_second_line){
        this.has_second_line = has_second_line ;
        this.has_second_lineDirtyFlag = true ;
    }

    /**
     * 获取 [HAS_SECOND_LINE]脏标记
     */
    @JsonIgnore
    public boolean getHas_second_lineDirtyFlag(){
        return has_second_lineDirtyFlag ;
    }

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

    /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return idDirtyFlag ;
    }

    /**
     * 获取 [MATCH_NATURE]
     */
    @JsonProperty("match_nature")
    public String getMatch_nature(){
        return match_nature ;
    }

    /**
     * 设置 [MATCH_NATURE]
     */
    @JsonProperty("match_nature")
    public void setMatch_nature(String  match_nature){
        this.match_nature = match_nature ;
        this.match_natureDirtyFlag = true ;
    }

    /**
     * 获取 [MATCH_NATURE]脏标记
     */
    @JsonIgnore
    public boolean getMatch_natureDirtyFlag(){
        return match_natureDirtyFlag ;
    }

    /**
     * 获取 [SECOND_AMOUNT_TYPE]
     */
    @JsonProperty("second_amount_type")
    public String getSecond_amount_type(){
        return second_amount_type ;
    }

    /**
     * 设置 [SECOND_AMOUNT_TYPE]
     */
    @JsonProperty("second_amount_type")
    public void setSecond_amount_type(String  second_amount_type){
        this.second_amount_type = second_amount_type ;
        this.second_amount_typeDirtyFlag = true ;
    }

    /**
     * 获取 [SECOND_AMOUNT_TYPE]脏标记
     */
    @JsonIgnore
    public boolean getSecond_amount_typeDirtyFlag(){
        return second_amount_typeDirtyFlag ;
    }

    /**
     * 获取 [MATCH_LABEL]
     */
    @JsonProperty("match_label")
    public String getMatch_label(){
        return match_label ;
    }

    /**
     * 设置 [MATCH_LABEL]
     */
    @JsonProperty("match_label")
    public void setMatch_label(String  match_label){
        this.match_label = match_label ;
        this.match_labelDirtyFlag = true ;
    }

    /**
     * 获取 [MATCH_LABEL]脏标记
     */
    @JsonIgnore
    public boolean getMatch_labelDirtyFlag(){
        return match_labelDirtyFlag ;
    }

    /**
     * 获取 [MATCH_AMOUNT_MAX]
     */
    @JsonProperty("match_amount_max")
    public Double getMatch_amount_max(){
        return match_amount_max ;
    }

    /**
     * 设置 [MATCH_AMOUNT_MAX]
     */
    @JsonProperty("match_amount_max")
    public void setMatch_amount_max(Double  match_amount_max){
        this.match_amount_max = match_amount_max ;
        this.match_amount_maxDirtyFlag = true ;
    }

    /**
     * 获取 [MATCH_AMOUNT_MAX]脏标记
     */
    @JsonIgnore
    public boolean getMatch_amount_maxDirtyFlag(){
        return match_amount_maxDirtyFlag ;
    }

    /**
     * 获取 [MATCH_TOTAL_AMOUNT_PARAM]
     */
    @JsonProperty("match_total_amount_param")
    public Double getMatch_total_amount_param(){
        return match_total_amount_param ;
    }

    /**
     * 设置 [MATCH_TOTAL_AMOUNT_PARAM]
     */
    @JsonProperty("match_total_amount_param")
    public void setMatch_total_amount_param(Double  match_total_amount_param){
        this.match_total_amount_param = match_total_amount_param ;
        this.match_total_amount_paramDirtyFlag = true ;
    }

    /**
     * 获取 [MATCH_TOTAL_AMOUNT_PARAM]脏标记
     */
    @JsonIgnore
    public boolean getMatch_total_amount_paramDirtyFlag(){
        return match_total_amount_paramDirtyFlag ;
    }

    /**
     * 获取 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return write_date ;
    }

    /**
     * 设置 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return write_dateDirtyFlag ;
    }

    /**
     * 获取 [SECOND_AMOUNT]
     */
    @JsonProperty("second_amount")
    public Double getSecond_amount(){
        return second_amount ;
    }

    /**
     * 设置 [SECOND_AMOUNT]
     */
    @JsonProperty("second_amount")
    public void setSecond_amount(Double  second_amount){
        this.second_amount = second_amount ;
        this.second_amountDirtyFlag = true ;
    }

    /**
     * 获取 [SECOND_AMOUNT]脏标记
     */
    @JsonIgnore
    public boolean getSecond_amountDirtyFlag(){
        return second_amountDirtyFlag ;
    }

    /**
     * 获取 [MATCH_AMOUNT]
     */
    @JsonProperty("match_amount")
    public String getMatch_amount(){
        return match_amount ;
    }

    /**
     * 设置 [MATCH_AMOUNT]
     */
    @JsonProperty("match_amount")
    public void setMatch_amount(String  match_amount){
        this.match_amount = match_amount ;
        this.match_amountDirtyFlag = true ;
    }

    /**
     * 获取 [MATCH_AMOUNT]脏标记
     */
    @JsonIgnore
    public boolean getMatch_amountDirtyFlag(){
        return match_amountDirtyFlag ;
    }

    /**
     * 获取 [ANALYTIC_TAG_IDS]
     */
    @JsonProperty("analytic_tag_ids")
    public String getAnalytic_tag_ids(){
        return analytic_tag_ids ;
    }

    /**
     * 设置 [ANALYTIC_TAG_IDS]
     */
    @JsonProperty("analytic_tag_ids")
    public void setAnalytic_tag_ids(String  analytic_tag_ids){
        this.analytic_tag_ids = analytic_tag_ids ;
        this.analytic_tag_idsDirtyFlag = true ;
    }

    /**
     * 获取 [ANALYTIC_TAG_IDS]脏标记
     */
    @JsonIgnore
    public boolean getAnalytic_tag_idsDirtyFlag(){
        return analytic_tag_idsDirtyFlag ;
    }

    /**
     * 获取 [SEQUENCE]
     */
    @JsonProperty("sequence")
    public Integer getSequence(){
        return sequence ;
    }

    /**
     * 设置 [SEQUENCE]
     */
    @JsonProperty("sequence")
    public void setSequence(Integer  sequence){
        this.sequence = sequence ;
        this.sequenceDirtyFlag = true ;
    }

    /**
     * 获取 [SEQUENCE]脏标记
     */
    @JsonIgnore
    public boolean getSequenceDirtyFlag(){
        return sequenceDirtyFlag ;
    }

    /**
     * 获取 [RULE_TYPE]
     */
    @JsonProperty("rule_type")
    public String getRule_type(){
        return rule_type ;
    }

    /**
     * 设置 [RULE_TYPE]
     */
    @JsonProperty("rule_type")
    public void setRule_type(String  rule_type){
        this.rule_type = rule_type ;
        this.rule_typeDirtyFlag = true ;
    }

    /**
     * 获取 [RULE_TYPE]脏标记
     */
    @JsonIgnore
    public boolean getRule_typeDirtyFlag(){
        return rule_typeDirtyFlag ;
    }

    /**
     * 获取 [MATCH_PARTNER_IDS]
     */
    @JsonProperty("match_partner_ids")
    public String getMatch_partner_ids(){
        return match_partner_ids ;
    }

    /**
     * 设置 [MATCH_PARTNER_IDS]
     */
    @JsonProperty("match_partner_ids")
    public void setMatch_partner_ids(String  match_partner_ids){
        this.match_partner_ids = match_partner_ids ;
        this.match_partner_idsDirtyFlag = true ;
    }

    /**
     * 获取 [MATCH_PARTNER_IDS]脏标记
     */
    @JsonIgnore
    public boolean getMatch_partner_idsDirtyFlag(){
        return match_partner_idsDirtyFlag ;
    }

    /**
     * 获取 [MATCH_PARTNER_CATEGORY_IDS]
     */
    @JsonProperty("match_partner_category_ids")
    public String getMatch_partner_category_ids(){
        return match_partner_category_ids ;
    }

    /**
     * 设置 [MATCH_PARTNER_CATEGORY_IDS]
     */
    @JsonProperty("match_partner_category_ids")
    public void setMatch_partner_category_ids(String  match_partner_category_ids){
        this.match_partner_category_ids = match_partner_category_ids ;
        this.match_partner_category_idsDirtyFlag = true ;
    }

    /**
     * 获取 [MATCH_PARTNER_CATEGORY_IDS]脏标记
     */
    @JsonIgnore
    public boolean getMatch_partner_category_idsDirtyFlag(){
        return match_partner_category_idsDirtyFlag ;
    }

    /**
     * 获取 [SECOND_LABEL]
     */
    @JsonProperty("second_label")
    public String getSecond_label(){
        return second_label ;
    }

    /**
     * 设置 [SECOND_LABEL]
     */
    @JsonProperty("second_label")
    public void setSecond_label(String  second_label){
        this.second_label = second_label ;
        this.second_labelDirtyFlag = true ;
    }

    /**
     * 获取 [SECOND_LABEL]脏标记
     */
    @JsonIgnore
    public boolean getSecond_labelDirtyFlag(){
        return second_labelDirtyFlag ;
    }

    /**
     * 获取 [MATCH_PARTNER]
     */
    @JsonProperty("match_partner")
    public String getMatch_partner(){
        return match_partner ;
    }

    /**
     * 设置 [MATCH_PARTNER]
     */
    @JsonProperty("match_partner")
    public void setMatch_partner(String  match_partner){
        this.match_partner = match_partner ;
        this.match_partnerDirtyFlag = true ;
    }

    /**
     * 获取 [MATCH_PARTNER]脏标记
     */
    @JsonIgnore
    public boolean getMatch_partnerDirtyFlag(){
        return match_partnerDirtyFlag ;
    }

    /**
     * 获取 [MATCH_AMOUNT_MIN]
     */
    @JsonProperty("match_amount_min")
    public Double getMatch_amount_min(){
        return match_amount_min ;
    }

    /**
     * 设置 [MATCH_AMOUNT_MIN]
     */
    @JsonProperty("match_amount_min")
    public void setMatch_amount_min(Double  match_amount_min){
        this.match_amount_min = match_amount_min ;
        this.match_amount_minDirtyFlag = true ;
    }

    /**
     * 获取 [MATCH_AMOUNT_MIN]脏标记
     */
    @JsonIgnore
    public boolean getMatch_amount_minDirtyFlag(){
        return match_amount_minDirtyFlag ;
    }

    /**
     * 获取 [MATCH_SAME_CURRENCY]
     */
    @JsonProperty("match_same_currency")
    public String getMatch_same_currency(){
        return match_same_currency ;
    }

    /**
     * 设置 [MATCH_SAME_CURRENCY]
     */
    @JsonProperty("match_same_currency")
    public void setMatch_same_currency(String  match_same_currency){
        this.match_same_currency = match_same_currency ;
        this.match_same_currencyDirtyFlag = true ;
    }

    /**
     * 获取 [MATCH_SAME_CURRENCY]脏标记
     */
    @JsonIgnore
    public boolean getMatch_same_currencyDirtyFlag(){
        return match_same_currencyDirtyFlag ;
    }

    /**
     * 获取 [SECOND_ANALYTIC_TAG_IDS]
     */
    @JsonProperty("second_analytic_tag_ids")
    public String getSecond_analytic_tag_ids(){
        return second_analytic_tag_ids ;
    }

    /**
     * 设置 [SECOND_ANALYTIC_TAG_IDS]
     */
    @JsonProperty("second_analytic_tag_ids")
    public void setSecond_analytic_tag_ids(String  second_analytic_tag_ids){
        this.second_analytic_tag_ids = second_analytic_tag_ids ;
        this.second_analytic_tag_idsDirtyFlag = true ;
    }

    /**
     * 获取 [SECOND_ANALYTIC_TAG_IDS]脏标记
     */
    @JsonIgnore
    public boolean getSecond_analytic_tag_idsDirtyFlag(){
        return second_analytic_tag_idsDirtyFlag ;
    }

    /**
     * 获取 [AMOUNT]
     */
    @JsonProperty("amount")
    public Double getAmount(){
        return amount ;
    }

    /**
     * 设置 [AMOUNT]
     */
    @JsonProperty("amount")
    public void setAmount(Double  amount){
        this.amount = amount ;
        this.amountDirtyFlag = true ;
    }

    /**
     * 获取 [AMOUNT]脏标记
     */
    @JsonIgnore
    public boolean getAmountDirtyFlag(){
        return amountDirtyFlag ;
    }

    /**
     * 获取 [MATCH_LABEL_PARAM]
     */
    @JsonProperty("match_label_param")
    public String getMatch_label_param(){
        return match_label_param ;
    }

    /**
     * 设置 [MATCH_LABEL_PARAM]
     */
    @JsonProperty("match_label_param")
    public void setMatch_label_param(String  match_label_param){
        this.match_label_param = match_label_param ;
        this.match_label_paramDirtyFlag = true ;
    }

    /**
     * 获取 [MATCH_LABEL_PARAM]脏标记
     */
    @JsonIgnore
    public boolean getMatch_label_paramDirtyFlag(){
        return match_label_paramDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return write_uid_text ;
    }

    /**
     * 设置 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return write_uid_textDirtyFlag ;
    }

    /**
     * 获取 [SECOND_ANALYTIC_ACCOUNT_ID_TEXT]
     */
    @JsonProperty("second_analytic_account_id_text")
    public String getSecond_analytic_account_id_text(){
        return second_analytic_account_id_text ;
    }

    /**
     * 设置 [SECOND_ANALYTIC_ACCOUNT_ID_TEXT]
     */
    @JsonProperty("second_analytic_account_id_text")
    public void setSecond_analytic_account_id_text(String  second_analytic_account_id_text){
        this.second_analytic_account_id_text = second_analytic_account_id_text ;
        this.second_analytic_account_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [SECOND_ANALYTIC_ACCOUNT_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getSecond_analytic_account_id_textDirtyFlag(){
        return second_analytic_account_id_textDirtyFlag ;
    }

    /**
     * 获取 [SECOND_TAX_ID_TEXT]
     */
    @JsonProperty("second_tax_id_text")
    public String getSecond_tax_id_text(){
        return second_tax_id_text ;
    }

    /**
     * 设置 [SECOND_TAX_ID_TEXT]
     */
    @JsonProperty("second_tax_id_text")
    public void setSecond_tax_id_text(String  second_tax_id_text){
        this.second_tax_id_text = second_tax_id_text ;
        this.second_tax_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [SECOND_TAX_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getSecond_tax_id_textDirtyFlag(){
        return second_tax_id_textDirtyFlag ;
    }

    /**
     * 获取 [SECOND_TAX_AMOUNT_TYPE]
     */
    @JsonProperty("second_tax_amount_type")
    public String getSecond_tax_amount_type(){
        return second_tax_amount_type ;
    }

    /**
     * 设置 [SECOND_TAX_AMOUNT_TYPE]
     */
    @JsonProperty("second_tax_amount_type")
    public void setSecond_tax_amount_type(String  second_tax_amount_type){
        this.second_tax_amount_type = second_tax_amount_type ;
        this.second_tax_amount_typeDirtyFlag = true ;
    }

    /**
     * 获取 [SECOND_TAX_AMOUNT_TYPE]脏标记
     */
    @JsonIgnore
    public boolean getSecond_tax_amount_typeDirtyFlag(){
        return second_tax_amount_typeDirtyFlag ;
    }

    /**
     * 获取 [TAX_AMOUNT_TYPE]
     */
    @JsonProperty("tax_amount_type")
    public String getTax_amount_type(){
        return tax_amount_type ;
    }

    /**
     * 设置 [TAX_AMOUNT_TYPE]
     */
    @JsonProperty("tax_amount_type")
    public void setTax_amount_type(String  tax_amount_type){
        this.tax_amount_type = tax_amount_type ;
        this.tax_amount_typeDirtyFlag = true ;
    }

    /**
     * 获取 [TAX_AMOUNT_TYPE]脏标记
     */
    @JsonIgnore
    public boolean getTax_amount_typeDirtyFlag(){
        return tax_amount_typeDirtyFlag ;
    }

    /**
     * 获取 [JOURNAL_ID_TEXT]
     */
    @JsonProperty("journal_id_text")
    public String getJournal_id_text(){
        return journal_id_text ;
    }

    /**
     * 设置 [JOURNAL_ID_TEXT]
     */
    @JsonProperty("journal_id_text")
    public void setJournal_id_text(String  journal_id_text){
        this.journal_id_text = journal_id_text ;
        this.journal_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [JOURNAL_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getJournal_id_textDirtyFlag(){
        return journal_id_textDirtyFlag ;
    }

    /**
     * 获取 [SECOND_JOURNAL_ID_TEXT]
     */
    @JsonProperty("second_journal_id_text")
    public String getSecond_journal_id_text(){
        return second_journal_id_text ;
    }

    /**
     * 设置 [SECOND_JOURNAL_ID_TEXT]
     */
    @JsonProperty("second_journal_id_text")
    public void setSecond_journal_id_text(String  second_journal_id_text){
        this.second_journal_id_text = second_journal_id_text ;
        this.second_journal_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [SECOND_JOURNAL_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getSecond_journal_id_textDirtyFlag(){
        return second_journal_id_textDirtyFlag ;
    }

    /**
     * 获取 [IS_TAX_PRICE_INCLUDED]
     */
    @JsonProperty("is_tax_price_included")
    public String getIs_tax_price_included(){
        return is_tax_price_included ;
    }

    /**
     * 设置 [IS_TAX_PRICE_INCLUDED]
     */
    @JsonProperty("is_tax_price_included")
    public void setIs_tax_price_included(String  is_tax_price_included){
        this.is_tax_price_included = is_tax_price_included ;
        this.is_tax_price_includedDirtyFlag = true ;
    }

    /**
     * 获取 [IS_TAX_PRICE_INCLUDED]脏标记
     */
    @JsonIgnore
    public boolean getIs_tax_price_includedDirtyFlag(){
        return is_tax_price_includedDirtyFlag ;
    }

    /**
     * 获取 [SECOND_ACCOUNT_ID_TEXT]
     */
    @JsonProperty("second_account_id_text")
    public String getSecond_account_id_text(){
        return second_account_id_text ;
    }

    /**
     * 设置 [SECOND_ACCOUNT_ID_TEXT]
     */
    @JsonProperty("second_account_id_text")
    public void setSecond_account_id_text(String  second_account_id_text){
        this.second_account_id_text = second_account_id_text ;
        this.second_account_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [SECOND_ACCOUNT_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getSecond_account_id_textDirtyFlag(){
        return second_account_id_textDirtyFlag ;
    }

    /**
     * 获取 [COMPANY_ID_TEXT]
     */
    @JsonProperty("company_id_text")
    public String getCompany_id_text(){
        return company_id_text ;
    }

    /**
     * 设置 [COMPANY_ID_TEXT]
     */
    @JsonProperty("company_id_text")
    public void setCompany_id_text(String  company_id_text){
        this.company_id_text = company_id_text ;
        this.company_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [COMPANY_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getCompany_id_textDirtyFlag(){
        return company_id_textDirtyFlag ;
    }

    /**
     * 获取 [ANALYTIC_ACCOUNT_ID_TEXT]
     */
    @JsonProperty("analytic_account_id_text")
    public String getAnalytic_account_id_text(){
        return analytic_account_id_text ;
    }

    /**
     * 设置 [ANALYTIC_ACCOUNT_ID_TEXT]
     */
    @JsonProperty("analytic_account_id_text")
    public void setAnalytic_account_id_text(String  analytic_account_id_text){
        this.analytic_account_id_text = analytic_account_id_text ;
        this.analytic_account_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [ANALYTIC_ACCOUNT_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getAnalytic_account_id_textDirtyFlag(){
        return analytic_account_id_textDirtyFlag ;
    }

    /**
     * 获取 [ACCOUNT_ID_TEXT]
     */
    @JsonProperty("account_id_text")
    public String getAccount_id_text(){
        return account_id_text ;
    }

    /**
     * 设置 [ACCOUNT_ID_TEXT]
     */
    @JsonProperty("account_id_text")
    public void setAccount_id_text(String  account_id_text){
        this.account_id_text = account_id_text ;
        this.account_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [ACCOUNT_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getAccount_id_textDirtyFlag(){
        return account_id_textDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return create_uid_text ;
    }

    /**
     * 设置 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return create_uid_textDirtyFlag ;
    }

    /**
     * 获取 [TAX_ID_TEXT]
     */
    @JsonProperty("tax_id_text")
    public String getTax_id_text(){
        return tax_id_text ;
    }

    /**
     * 设置 [TAX_ID_TEXT]
     */
    @JsonProperty("tax_id_text")
    public void setTax_id_text(String  tax_id_text){
        this.tax_id_text = tax_id_text ;
        this.tax_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [TAX_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getTax_id_textDirtyFlag(){
        return tax_id_textDirtyFlag ;
    }

    /**
     * 获取 [IS_SECOND_TAX_PRICE_INCLUDED]
     */
    @JsonProperty("is_second_tax_price_included")
    public String getIs_second_tax_price_included(){
        return is_second_tax_price_included ;
    }

    /**
     * 设置 [IS_SECOND_TAX_PRICE_INCLUDED]
     */
    @JsonProperty("is_second_tax_price_included")
    public void setIs_second_tax_price_included(String  is_second_tax_price_included){
        this.is_second_tax_price_included = is_second_tax_price_included ;
        this.is_second_tax_price_includedDirtyFlag = true ;
    }

    /**
     * 获取 [IS_SECOND_TAX_PRICE_INCLUDED]脏标记
     */
    @JsonIgnore
    public boolean getIs_second_tax_price_includedDirtyFlag(){
        return is_second_tax_price_includedDirtyFlag ;
    }

    /**
     * 获取 [SECOND_ANALYTIC_ACCOUNT_ID]
     */
    @JsonProperty("second_analytic_account_id")
    public Integer getSecond_analytic_account_id(){
        return second_analytic_account_id ;
    }

    /**
     * 设置 [SECOND_ANALYTIC_ACCOUNT_ID]
     */
    @JsonProperty("second_analytic_account_id")
    public void setSecond_analytic_account_id(Integer  second_analytic_account_id){
        this.second_analytic_account_id = second_analytic_account_id ;
        this.second_analytic_account_idDirtyFlag = true ;
    }

    /**
     * 获取 [SECOND_ANALYTIC_ACCOUNT_ID]脏标记
     */
    @JsonIgnore
    public boolean getSecond_analytic_account_idDirtyFlag(){
        return second_analytic_account_idDirtyFlag ;
    }

    /**
     * 获取 [COMPANY_ID]
     */
    @JsonProperty("company_id")
    public Integer getCompany_id(){
        return company_id ;
    }

    /**
     * 设置 [COMPANY_ID]
     */
    @JsonProperty("company_id")
    public void setCompany_id(Integer  company_id){
        this.company_id = company_id ;
        this.company_idDirtyFlag = true ;
    }

    /**
     * 获取 [COMPANY_ID]脏标记
     */
    @JsonIgnore
    public boolean getCompany_idDirtyFlag(){
        return company_idDirtyFlag ;
    }

    /**
     * 获取 [TAX_ID]
     */
    @JsonProperty("tax_id")
    public Integer getTax_id(){
        return tax_id ;
    }

    /**
     * 设置 [TAX_ID]
     */
    @JsonProperty("tax_id")
    public void setTax_id(Integer  tax_id){
        this.tax_id = tax_id ;
        this.tax_idDirtyFlag = true ;
    }

    /**
     * 获取 [TAX_ID]脏标记
     */
    @JsonIgnore
    public boolean getTax_idDirtyFlag(){
        return tax_idDirtyFlag ;
    }

    /**
     * 获取 [SECOND_ACCOUNT_ID]
     */
    @JsonProperty("second_account_id")
    public Integer getSecond_account_id(){
        return second_account_id ;
    }

    /**
     * 设置 [SECOND_ACCOUNT_ID]
     */
    @JsonProperty("second_account_id")
    public void setSecond_account_id(Integer  second_account_id){
        this.second_account_id = second_account_id ;
        this.second_account_idDirtyFlag = true ;
    }

    /**
     * 获取 [SECOND_ACCOUNT_ID]脏标记
     */
    @JsonIgnore
    public boolean getSecond_account_idDirtyFlag(){
        return second_account_idDirtyFlag ;
    }

    /**
     * 获取 [SECOND_JOURNAL_ID]
     */
    @JsonProperty("second_journal_id")
    public Integer getSecond_journal_id(){
        return second_journal_id ;
    }

    /**
     * 设置 [SECOND_JOURNAL_ID]
     */
    @JsonProperty("second_journal_id")
    public void setSecond_journal_id(Integer  second_journal_id){
        this.second_journal_id = second_journal_id ;
        this.second_journal_idDirtyFlag = true ;
    }

    /**
     * 获取 [SECOND_JOURNAL_ID]脏标记
     */
    @JsonIgnore
    public boolean getSecond_journal_idDirtyFlag(){
        return second_journal_idDirtyFlag ;
    }

    /**
     * 获取 [ANALYTIC_ACCOUNT_ID]
     */
    @JsonProperty("analytic_account_id")
    public Integer getAnalytic_account_id(){
        return analytic_account_id ;
    }

    /**
     * 设置 [ANALYTIC_ACCOUNT_ID]
     */
    @JsonProperty("analytic_account_id")
    public void setAnalytic_account_id(Integer  analytic_account_id){
        this.analytic_account_id = analytic_account_id ;
        this.analytic_account_idDirtyFlag = true ;
    }

    /**
     * 获取 [ANALYTIC_ACCOUNT_ID]脏标记
     */
    @JsonIgnore
    public boolean getAnalytic_account_idDirtyFlag(){
        return analytic_account_idDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return write_uid ;
    }

    /**
     * 设置 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return write_uidDirtyFlag ;
    }

    /**
     * 获取 [SECOND_TAX_ID]
     */
    @JsonProperty("second_tax_id")
    public Integer getSecond_tax_id(){
        return second_tax_id ;
    }

    /**
     * 设置 [SECOND_TAX_ID]
     */
    @JsonProperty("second_tax_id")
    public void setSecond_tax_id(Integer  second_tax_id){
        this.second_tax_id = second_tax_id ;
        this.second_tax_idDirtyFlag = true ;
    }

    /**
     * 获取 [SECOND_TAX_ID]脏标记
     */
    @JsonIgnore
    public boolean getSecond_tax_idDirtyFlag(){
        return second_tax_idDirtyFlag ;
    }

    /**
     * 获取 [ACCOUNT_ID]
     */
    @JsonProperty("account_id")
    public Integer getAccount_id(){
        return account_id ;
    }

    /**
     * 设置 [ACCOUNT_ID]
     */
    @JsonProperty("account_id")
    public void setAccount_id(Integer  account_id){
        this.account_id = account_id ;
        this.account_idDirtyFlag = true ;
    }

    /**
     * 获取 [ACCOUNT_ID]脏标记
     */
    @JsonIgnore
    public boolean getAccount_idDirtyFlag(){
        return account_idDirtyFlag ;
    }

    /**
     * 获取 [JOURNAL_ID]
     */
    @JsonProperty("journal_id")
    public Integer getJournal_id(){
        return journal_id ;
    }

    /**
     * 设置 [JOURNAL_ID]
     */
    @JsonProperty("journal_id")
    public void setJournal_id(Integer  journal_id){
        this.journal_id = journal_id ;
        this.journal_idDirtyFlag = true ;
    }

    /**
     * 获取 [JOURNAL_ID]脏标记
     */
    @JsonIgnore
    public boolean getJournal_idDirtyFlag(){
        return journal_idDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return create_uid ;
    }

    /**
     * 设置 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return create_uidDirtyFlag ;
    }



    public Account_reconcile_model toDO() {
        Account_reconcile_model srfdomain = new Account_reconcile_model();
        if(getLabelDirtyFlag())
            srfdomain.setLabel(label);
        if(getDisplay_nameDirtyFlag())
            srfdomain.setDisplay_name(display_name);
        if(get__last_updateDirtyFlag())
            srfdomain.set__last_update(__last_update);
        if(getForce_tax_includedDirtyFlag())
            srfdomain.setForce_tax_included(force_tax_included);
        if(getAmount_typeDirtyFlag())
            srfdomain.setAmount_type(amount_type);
        if(getCreate_dateDirtyFlag())
            srfdomain.setCreate_date(create_date);
        if(getForce_second_tax_includedDirtyFlag())
            srfdomain.setForce_second_tax_included(force_second_tax_included);
        if(getAuto_reconcileDirtyFlag())
            srfdomain.setAuto_reconcile(auto_reconcile);
        if(getMatch_total_amountDirtyFlag())
            srfdomain.setMatch_total_amount(match_total_amount);
        if(getNameDirtyFlag())
            srfdomain.setName(name);
        if(getMatch_journal_idsDirtyFlag())
            srfdomain.setMatch_journal_ids(match_journal_ids);
        if(getHas_second_lineDirtyFlag())
            srfdomain.setHas_second_line(has_second_line);
        if(getIdDirtyFlag())
            srfdomain.setId(id);
        if(getMatch_natureDirtyFlag())
            srfdomain.setMatch_nature(match_nature);
        if(getSecond_amount_typeDirtyFlag())
            srfdomain.setSecond_amount_type(second_amount_type);
        if(getMatch_labelDirtyFlag())
            srfdomain.setMatch_label(match_label);
        if(getMatch_amount_maxDirtyFlag())
            srfdomain.setMatch_amount_max(match_amount_max);
        if(getMatch_total_amount_paramDirtyFlag())
            srfdomain.setMatch_total_amount_param(match_total_amount_param);
        if(getWrite_dateDirtyFlag())
            srfdomain.setWrite_date(write_date);
        if(getSecond_amountDirtyFlag())
            srfdomain.setSecond_amount(second_amount);
        if(getMatch_amountDirtyFlag())
            srfdomain.setMatch_amount(match_amount);
        if(getAnalytic_tag_idsDirtyFlag())
            srfdomain.setAnalytic_tag_ids(analytic_tag_ids);
        if(getSequenceDirtyFlag())
            srfdomain.setSequence(sequence);
        if(getRule_typeDirtyFlag())
            srfdomain.setRule_type(rule_type);
        if(getMatch_partner_idsDirtyFlag())
            srfdomain.setMatch_partner_ids(match_partner_ids);
        if(getMatch_partner_category_idsDirtyFlag())
            srfdomain.setMatch_partner_category_ids(match_partner_category_ids);
        if(getSecond_labelDirtyFlag())
            srfdomain.setSecond_label(second_label);
        if(getMatch_partnerDirtyFlag())
            srfdomain.setMatch_partner(match_partner);
        if(getMatch_amount_minDirtyFlag())
            srfdomain.setMatch_amount_min(match_amount_min);
        if(getMatch_same_currencyDirtyFlag())
            srfdomain.setMatch_same_currency(match_same_currency);
        if(getSecond_analytic_tag_idsDirtyFlag())
            srfdomain.setSecond_analytic_tag_ids(second_analytic_tag_ids);
        if(getAmountDirtyFlag())
            srfdomain.setAmount(amount);
        if(getMatch_label_paramDirtyFlag())
            srfdomain.setMatch_label_param(match_label_param);
        if(getWrite_uid_textDirtyFlag())
            srfdomain.setWrite_uid_text(write_uid_text);
        if(getSecond_analytic_account_id_textDirtyFlag())
            srfdomain.setSecond_analytic_account_id_text(second_analytic_account_id_text);
        if(getSecond_tax_id_textDirtyFlag())
            srfdomain.setSecond_tax_id_text(second_tax_id_text);
        if(getSecond_tax_amount_typeDirtyFlag())
            srfdomain.setSecond_tax_amount_type(second_tax_amount_type);
        if(getTax_amount_typeDirtyFlag())
            srfdomain.setTax_amount_type(tax_amount_type);
        if(getJournal_id_textDirtyFlag())
            srfdomain.setJournal_id_text(journal_id_text);
        if(getSecond_journal_id_textDirtyFlag())
            srfdomain.setSecond_journal_id_text(second_journal_id_text);
        if(getIs_tax_price_includedDirtyFlag())
            srfdomain.setIs_tax_price_included(is_tax_price_included);
        if(getSecond_account_id_textDirtyFlag())
            srfdomain.setSecond_account_id_text(second_account_id_text);
        if(getCompany_id_textDirtyFlag())
            srfdomain.setCompany_id_text(company_id_text);
        if(getAnalytic_account_id_textDirtyFlag())
            srfdomain.setAnalytic_account_id_text(analytic_account_id_text);
        if(getAccount_id_textDirtyFlag())
            srfdomain.setAccount_id_text(account_id_text);
        if(getCreate_uid_textDirtyFlag())
            srfdomain.setCreate_uid_text(create_uid_text);
        if(getTax_id_textDirtyFlag())
            srfdomain.setTax_id_text(tax_id_text);
        if(getIs_second_tax_price_includedDirtyFlag())
            srfdomain.setIs_second_tax_price_included(is_second_tax_price_included);
        if(getSecond_analytic_account_idDirtyFlag())
            srfdomain.setSecond_analytic_account_id(second_analytic_account_id);
        if(getCompany_idDirtyFlag())
            srfdomain.setCompany_id(company_id);
        if(getTax_idDirtyFlag())
            srfdomain.setTax_id(tax_id);
        if(getSecond_account_idDirtyFlag())
            srfdomain.setSecond_account_id(second_account_id);
        if(getSecond_journal_idDirtyFlag())
            srfdomain.setSecond_journal_id(second_journal_id);
        if(getAnalytic_account_idDirtyFlag())
            srfdomain.setAnalytic_account_id(analytic_account_id);
        if(getWrite_uidDirtyFlag())
            srfdomain.setWrite_uid(write_uid);
        if(getSecond_tax_idDirtyFlag())
            srfdomain.setSecond_tax_id(second_tax_id);
        if(getAccount_idDirtyFlag())
            srfdomain.setAccount_id(account_id);
        if(getJournal_idDirtyFlag())
            srfdomain.setJournal_id(journal_id);
        if(getCreate_uidDirtyFlag())
            srfdomain.setCreate_uid(create_uid);

        return srfdomain;
    }

    public void fromDO(Account_reconcile_model srfdomain) {
        if(srfdomain == null )
          return ;
        if(srfdomain.getLabelDirtyFlag())
            this.setLabel(srfdomain.getLabel());
        if(srfdomain.getDisplay_nameDirtyFlag())
            this.setDisplay_name(srfdomain.getDisplay_name());
        if(srfdomain.get__last_updateDirtyFlag())
            this.set__last_update(srfdomain.get__last_update());
        if(srfdomain.getForce_tax_includedDirtyFlag())
            this.setForce_tax_included(srfdomain.getForce_tax_included());
        if(srfdomain.getAmount_typeDirtyFlag())
            this.setAmount_type(srfdomain.getAmount_type());
        if(srfdomain.getCreate_dateDirtyFlag())
            this.setCreate_date(srfdomain.getCreate_date());
        if(srfdomain.getForce_second_tax_includedDirtyFlag())
            this.setForce_second_tax_included(srfdomain.getForce_second_tax_included());
        if(srfdomain.getAuto_reconcileDirtyFlag())
            this.setAuto_reconcile(srfdomain.getAuto_reconcile());
        if(srfdomain.getMatch_total_amountDirtyFlag())
            this.setMatch_total_amount(srfdomain.getMatch_total_amount());
        if(srfdomain.getNameDirtyFlag())
            this.setName(srfdomain.getName());
        if(srfdomain.getMatch_journal_idsDirtyFlag())
            this.setMatch_journal_ids(srfdomain.getMatch_journal_ids());
        if(srfdomain.getHas_second_lineDirtyFlag())
            this.setHas_second_line(srfdomain.getHas_second_line());
        if(srfdomain.getIdDirtyFlag())
            this.setId(srfdomain.getId());
        if(srfdomain.getMatch_natureDirtyFlag())
            this.setMatch_nature(srfdomain.getMatch_nature());
        if(srfdomain.getSecond_amount_typeDirtyFlag())
            this.setSecond_amount_type(srfdomain.getSecond_amount_type());
        if(srfdomain.getMatch_labelDirtyFlag())
            this.setMatch_label(srfdomain.getMatch_label());
        if(srfdomain.getMatch_amount_maxDirtyFlag())
            this.setMatch_amount_max(srfdomain.getMatch_amount_max());
        if(srfdomain.getMatch_total_amount_paramDirtyFlag())
            this.setMatch_total_amount_param(srfdomain.getMatch_total_amount_param());
        if(srfdomain.getWrite_dateDirtyFlag())
            this.setWrite_date(srfdomain.getWrite_date());
        if(srfdomain.getSecond_amountDirtyFlag())
            this.setSecond_amount(srfdomain.getSecond_amount());
        if(srfdomain.getMatch_amountDirtyFlag())
            this.setMatch_amount(srfdomain.getMatch_amount());
        if(srfdomain.getAnalytic_tag_idsDirtyFlag())
            this.setAnalytic_tag_ids(srfdomain.getAnalytic_tag_ids());
        if(srfdomain.getSequenceDirtyFlag())
            this.setSequence(srfdomain.getSequence());
        if(srfdomain.getRule_typeDirtyFlag())
            this.setRule_type(srfdomain.getRule_type());
        if(srfdomain.getMatch_partner_idsDirtyFlag())
            this.setMatch_partner_ids(srfdomain.getMatch_partner_ids());
        if(srfdomain.getMatch_partner_category_idsDirtyFlag())
            this.setMatch_partner_category_ids(srfdomain.getMatch_partner_category_ids());
        if(srfdomain.getSecond_labelDirtyFlag())
            this.setSecond_label(srfdomain.getSecond_label());
        if(srfdomain.getMatch_partnerDirtyFlag())
            this.setMatch_partner(srfdomain.getMatch_partner());
        if(srfdomain.getMatch_amount_minDirtyFlag())
            this.setMatch_amount_min(srfdomain.getMatch_amount_min());
        if(srfdomain.getMatch_same_currencyDirtyFlag())
            this.setMatch_same_currency(srfdomain.getMatch_same_currency());
        if(srfdomain.getSecond_analytic_tag_idsDirtyFlag())
            this.setSecond_analytic_tag_ids(srfdomain.getSecond_analytic_tag_ids());
        if(srfdomain.getAmountDirtyFlag())
            this.setAmount(srfdomain.getAmount());
        if(srfdomain.getMatch_label_paramDirtyFlag())
            this.setMatch_label_param(srfdomain.getMatch_label_param());
        if(srfdomain.getWrite_uid_textDirtyFlag())
            this.setWrite_uid_text(srfdomain.getWrite_uid_text());
        if(srfdomain.getSecond_analytic_account_id_textDirtyFlag())
            this.setSecond_analytic_account_id_text(srfdomain.getSecond_analytic_account_id_text());
        if(srfdomain.getSecond_tax_id_textDirtyFlag())
            this.setSecond_tax_id_text(srfdomain.getSecond_tax_id_text());
        if(srfdomain.getSecond_tax_amount_typeDirtyFlag())
            this.setSecond_tax_amount_type(srfdomain.getSecond_tax_amount_type());
        if(srfdomain.getTax_amount_typeDirtyFlag())
            this.setTax_amount_type(srfdomain.getTax_amount_type());
        if(srfdomain.getJournal_id_textDirtyFlag())
            this.setJournal_id_text(srfdomain.getJournal_id_text());
        if(srfdomain.getSecond_journal_id_textDirtyFlag())
            this.setSecond_journal_id_text(srfdomain.getSecond_journal_id_text());
        if(srfdomain.getIs_tax_price_includedDirtyFlag())
            this.setIs_tax_price_included(srfdomain.getIs_tax_price_included());
        if(srfdomain.getSecond_account_id_textDirtyFlag())
            this.setSecond_account_id_text(srfdomain.getSecond_account_id_text());
        if(srfdomain.getCompany_id_textDirtyFlag())
            this.setCompany_id_text(srfdomain.getCompany_id_text());
        if(srfdomain.getAnalytic_account_id_textDirtyFlag())
            this.setAnalytic_account_id_text(srfdomain.getAnalytic_account_id_text());
        if(srfdomain.getAccount_id_textDirtyFlag())
            this.setAccount_id_text(srfdomain.getAccount_id_text());
        if(srfdomain.getCreate_uid_textDirtyFlag())
            this.setCreate_uid_text(srfdomain.getCreate_uid_text());
        if(srfdomain.getTax_id_textDirtyFlag())
            this.setTax_id_text(srfdomain.getTax_id_text());
        if(srfdomain.getIs_second_tax_price_includedDirtyFlag())
            this.setIs_second_tax_price_included(srfdomain.getIs_second_tax_price_included());
        if(srfdomain.getSecond_analytic_account_idDirtyFlag())
            this.setSecond_analytic_account_id(srfdomain.getSecond_analytic_account_id());
        if(srfdomain.getCompany_idDirtyFlag())
            this.setCompany_id(srfdomain.getCompany_id());
        if(srfdomain.getTax_idDirtyFlag())
            this.setTax_id(srfdomain.getTax_id());
        if(srfdomain.getSecond_account_idDirtyFlag())
            this.setSecond_account_id(srfdomain.getSecond_account_id());
        if(srfdomain.getSecond_journal_idDirtyFlag())
            this.setSecond_journal_id(srfdomain.getSecond_journal_id());
        if(srfdomain.getAnalytic_account_idDirtyFlag())
            this.setAnalytic_account_id(srfdomain.getAnalytic_account_id());
        if(srfdomain.getWrite_uidDirtyFlag())
            this.setWrite_uid(srfdomain.getWrite_uid());
        if(srfdomain.getSecond_tax_idDirtyFlag())
            this.setSecond_tax_id(srfdomain.getSecond_tax_id());
        if(srfdomain.getAccount_idDirtyFlag())
            this.setAccount_id(srfdomain.getAccount_id());
        if(srfdomain.getJournal_idDirtyFlag())
            this.setJournal_id(srfdomain.getJournal_id());
        if(srfdomain.getCreate_uidDirtyFlag())
            this.setCreate_uid(srfdomain.getCreate_uid());

    }

    public List<Account_reconcile_modelDTO> fromDOPage(List<Account_reconcile_model> poPage)   {
        if(poPage == null)
            return null;
        List<Account_reconcile_modelDTO> dtos=new ArrayList<Account_reconcile_modelDTO>();
        for(Account_reconcile_model domain : poPage) {
            Account_reconcile_modelDTO dto = new Account_reconcile_modelDTO();
            dto.fromDO(domain);
            dtos.add(dto);
        }
        return dtos;
    }
}

