package cn.ibizlab.odoo.service.odoo_account.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_account.dto.Account_taxDTO;
import cn.ibizlab.odoo.core.odoo_account.domain.Account_tax;
import cn.ibizlab.odoo.core.odoo_account.service.IAccount_taxService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_taxSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Account_tax" })
@RestController
@RequestMapping("")
public class Account_taxResource {

    @Autowired
    private IAccount_taxService account_taxService;

    public IAccount_taxService getAccount_taxService() {
        return this.account_taxService;
    }

    @ApiOperation(value = "批删除数据", tags = {"Account_tax" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_account/account_taxes/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Account_taxDTO> account_taxdtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "删除数据", tags = {"Account_tax" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_account/account_taxes/{account_tax_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("account_tax_id") Integer account_tax_id) {
        Account_taxDTO account_taxdto = new Account_taxDTO();
		Account_tax domain = new Account_tax();
		account_taxdto.setId(account_tax_id);
		domain.setId(account_tax_id);
        Boolean rst = account_taxService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "更新数据", tags = {"Account_tax" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_account/account_taxes/{account_tax_id}")

    public ResponseEntity<Account_taxDTO> update(@PathVariable("account_tax_id") Integer account_tax_id, @RequestBody Account_taxDTO account_taxdto) {
		Account_tax domain = account_taxdto.toDO();
        domain.setId(account_tax_id);
		account_taxService.update(domain);
		Account_taxDTO dto = new Account_taxDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "建立数据", tags = {"Account_tax" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_account/account_taxes")

    public ResponseEntity<Account_taxDTO> create(@RequestBody Account_taxDTO account_taxdto) {
        Account_taxDTO dto = new Account_taxDTO();
        Account_tax domain = account_taxdto.toDO();
		account_taxService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批更新数据", tags = {"Account_tax" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_account/account_taxes/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Account_taxDTO> account_taxdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "批建立数据", tags = {"Account_tax" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_account/account_taxes/createBatch")
    public ResponseEntity<Boolean> createBatchAccount_tax(@RequestBody List<Account_taxDTO> account_taxdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "获取数据", tags = {"Account_tax" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_account/account_taxes/{account_tax_id}")
    public ResponseEntity<Account_taxDTO> get(@PathVariable("account_tax_id") Integer account_tax_id) {
        Account_taxDTO dto = new Account_taxDTO();
        Account_tax domain = account_taxService.get(account_tax_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Account_tax" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_account/account_taxes/fetchdefault")
	public ResponseEntity<Page<Account_taxDTO>> fetchDefault(Account_taxSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Account_taxDTO> list = new ArrayList<Account_taxDTO>();
        
        Page<Account_tax> domains = account_taxService.searchDefault(context) ;
        for(Account_tax account_tax : domains.getContent()){
            Account_taxDTO dto = new Account_taxDTO();
            dto.fromDO(account_tax);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
