package cn.ibizlab.odoo.service.odoo_account.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_account.dto.Account_invoice_taxDTO;
import cn.ibizlab.odoo.core.odoo_account.domain.Account_invoice_tax;
import cn.ibizlab.odoo.core.odoo_account.service.IAccount_invoice_taxService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_invoice_taxSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Account_invoice_tax" })
@RestController
@RequestMapping("")
public class Account_invoice_taxResource {

    @Autowired
    private IAccount_invoice_taxService account_invoice_taxService;

    public IAccount_invoice_taxService getAccount_invoice_taxService() {
        return this.account_invoice_taxService;
    }

    @ApiOperation(value = "批更新数据", tags = {"Account_invoice_tax" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_account/account_invoice_taxes/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Account_invoice_taxDTO> account_invoice_taxdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "批删除数据", tags = {"Account_invoice_tax" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_account/account_invoice_taxes/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Account_invoice_taxDTO> account_invoice_taxdtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "批建立数据", tags = {"Account_invoice_tax" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_account/account_invoice_taxes/createBatch")
    public ResponseEntity<Boolean> createBatchAccount_invoice_tax(@RequestBody List<Account_invoice_taxDTO> account_invoice_taxdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "更新数据", tags = {"Account_invoice_tax" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_account/account_invoice_taxes/{account_invoice_tax_id}")

    public ResponseEntity<Account_invoice_taxDTO> update(@PathVariable("account_invoice_tax_id") Integer account_invoice_tax_id, @RequestBody Account_invoice_taxDTO account_invoice_taxdto) {
		Account_invoice_tax domain = account_invoice_taxdto.toDO();
        domain.setId(account_invoice_tax_id);
		account_invoice_taxService.update(domain);
		Account_invoice_taxDTO dto = new Account_invoice_taxDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取数据", tags = {"Account_invoice_tax" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_account/account_invoice_taxes/{account_invoice_tax_id}")
    public ResponseEntity<Account_invoice_taxDTO> get(@PathVariable("account_invoice_tax_id") Integer account_invoice_tax_id) {
        Account_invoice_taxDTO dto = new Account_invoice_taxDTO();
        Account_invoice_tax domain = account_invoice_taxService.get(account_invoice_tax_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "建立数据", tags = {"Account_invoice_tax" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_account/account_invoice_taxes")

    public ResponseEntity<Account_invoice_taxDTO> create(@RequestBody Account_invoice_taxDTO account_invoice_taxdto) {
        Account_invoice_taxDTO dto = new Account_invoice_taxDTO();
        Account_invoice_tax domain = account_invoice_taxdto.toDO();
		account_invoice_taxService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "删除数据", tags = {"Account_invoice_tax" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_account/account_invoice_taxes/{account_invoice_tax_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("account_invoice_tax_id") Integer account_invoice_tax_id) {
        Account_invoice_taxDTO account_invoice_taxdto = new Account_invoice_taxDTO();
		Account_invoice_tax domain = new Account_invoice_tax();
		account_invoice_taxdto.setId(account_invoice_tax_id);
		domain.setId(account_invoice_tax_id);
        Boolean rst = account_invoice_taxService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

	@ApiOperation(value = "获取默认查询", tags = {"Account_invoice_tax" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_account/account_invoice_taxes/fetchdefault")
	public ResponseEntity<Page<Account_invoice_taxDTO>> fetchDefault(Account_invoice_taxSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Account_invoice_taxDTO> list = new ArrayList<Account_invoice_taxDTO>();
        
        Page<Account_invoice_tax> domains = account_invoice_taxService.searchDefault(context) ;
        for(Account_invoice_tax account_invoice_tax : domains.getContent()){
            Account_invoice_taxDTO dto = new Account_invoice_taxDTO();
            dto.fromDO(account_invoice_tax);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
