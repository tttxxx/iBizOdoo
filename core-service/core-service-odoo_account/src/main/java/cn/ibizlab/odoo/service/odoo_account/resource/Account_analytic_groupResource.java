package cn.ibizlab.odoo.service.odoo_account.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_account.dto.Account_analytic_groupDTO;
import cn.ibizlab.odoo.core.odoo_account.domain.Account_analytic_group;
import cn.ibizlab.odoo.core.odoo_account.service.IAccount_analytic_groupService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_analytic_groupSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Account_analytic_group" })
@RestController
@RequestMapping("")
public class Account_analytic_groupResource {

    @Autowired
    private IAccount_analytic_groupService account_analytic_groupService;

    public IAccount_analytic_groupService getAccount_analytic_groupService() {
        return this.account_analytic_groupService;
    }

    @ApiOperation(value = "批建立数据", tags = {"Account_analytic_group" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_account/account_analytic_groups/createBatch")
    public ResponseEntity<Boolean> createBatchAccount_analytic_group(@RequestBody List<Account_analytic_groupDTO> account_analytic_groupdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "删除数据", tags = {"Account_analytic_group" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_account/account_analytic_groups/{account_analytic_group_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("account_analytic_group_id") Integer account_analytic_group_id) {
        Account_analytic_groupDTO account_analytic_groupdto = new Account_analytic_groupDTO();
		Account_analytic_group domain = new Account_analytic_group();
		account_analytic_groupdto.setId(account_analytic_group_id);
		domain.setId(account_analytic_group_id);
        Boolean rst = account_analytic_groupService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "建立数据", tags = {"Account_analytic_group" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_account/account_analytic_groups")

    public ResponseEntity<Account_analytic_groupDTO> create(@RequestBody Account_analytic_groupDTO account_analytic_groupdto) {
        Account_analytic_groupDTO dto = new Account_analytic_groupDTO();
        Account_analytic_group domain = account_analytic_groupdto.toDO();
		account_analytic_groupService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "更新数据", tags = {"Account_analytic_group" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_account/account_analytic_groups/{account_analytic_group_id}")

    public ResponseEntity<Account_analytic_groupDTO> update(@PathVariable("account_analytic_group_id") Integer account_analytic_group_id, @RequestBody Account_analytic_groupDTO account_analytic_groupdto) {
		Account_analytic_group domain = account_analytic_groupdto.toDO();
        domain.setId(account_analytic_group_id);
		account_analytic_groupService.update(domain);
		Account_analytic_groupDTO dto = new Account_analytic_groupDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批删除数据", tags = {"Account_analytic_group" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_account/account_analytic_groups/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Account_analytic_groupDTO> account_analytic_groupdtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "批更新数据", tags = {"Account_analytic_group" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_account/account_analytic_groups/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Account_analytic_groupDTO> account_analytic_groupdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "获取数据", tags = {"Account_analytic_group" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_account/account_analytic_groups/{account_analytic_group_id}")
    public ResponseEntity<Account_analytic_groupDTO> get(@PathVariable("account_analytic_group_id") Integer account_analytic_group_id) {
        Account_analytic_groupDTO dto = new Account_analytic_groupDTO();
        Account_analytic_group domain = account_analytic_groupService.get(account_analytic_group_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Account_analytic_group" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_account/account_analytic_groups/fetchdefault")
	public ResponseEntity<Page<Account_analytic_groupDTO>> fetchDefault(Account_analytic_groupSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Account_analytic_groupDTO> list = new ArrayList<Account_analytic_groupDTO>();
        
        Page<Account_analytic_group> domains = account_analytic_groupService.searchDefault(context) ;
        for(Account_analytic_group account_analytic_group : domains.getContent()){
            Account_analytic_groupDTO dto = new Account_analytic_groupDTO();
            dto.fromDO(account_analytic_group);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
