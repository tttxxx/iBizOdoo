package cn.ibizlab.odoo.service.odoo_account.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.core.odoo_account.valuerule.anno.account_financial_year_op.*;
import cn.ibizlab.odoo.core.odoo_account.domain.Account_financial_year_op;
import org.springframework.cglib.beans.BeanCopier;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 服务DTO对象[Account_financial_year_opDTO]
 */
public class Account_financial_year_opDTO implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [ID]
     *
     */
    @Account_financial_year_opIdDefault(info = "默认规则")
    private Integer id;

    @JsonIgnore
    private boolean idDirtyFlag;

    /**
     * 属性 [OPENING_MOVE_POSTED]
     *
     */
    @Account_financial_year_opOpening_move_postedDefault(info = "默认规则")
    private String opening_move_posted;

    @JsonIgnore
    private boolean opening_move_postedDirtyFlag;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @Account_financial_year_op__last_updateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp __last_update;

    @JsonIgnore
    private boolean __last_updateDirtyFlag;

    /**
     * 属性 [CREATE_DATE]
     *
     */
    @Account_financial_year_opCreate_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp create_date;

    @JsonIgnore
    private boolean create_dateDirtyFlag;

    /**
     * 属性 [WRITE_DATE]
     *
     */
    @Account_financial_year_opWrite_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp write_date;

    @JsonIgnore
    private boolean write_dateDirtyFlag;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @Account_financial_year_opDisplay_nameDefault(info = "默认规则")
    private String display_name;

    @JsonIgnore
    private boolean display_nameDirtyFlag;

    /**
     * 属性 [FISCALYEAR_LAST_DAY]
     *
     */
    @Account_financial_year_opFiscalyear_last_dayDefault(info = "默认规则")
    private Integer fiscalyear_last_day;

    @JsonIgnore
    private boolean fiscalyear_last_dayDirtyFlag;

    /**
     * 属性 [FISCALYEAR_LAST_MONTH]
     *
     */
    @Account_financial_year_opFiscalyear_last_monthDefault(info = "默认规则")
    private String fiscalyear_last_month;

    @JsonIgnore
    private boolean fiscalyear_last_monthDirtyFlag;

    /**
     * 属性 [OPENING_DATE]
     *
     */
    @Account_financial_year_opOpening_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    private Timestamp opening_date;

    @JsonIgnore
    private boolean opening_dateDirtyFlag;

    /**
     * 属性 [WRITE_UID_TEXT]
     *
     */
    @Account_financial_year_opWrite_uid_textDefault(info = "默认规则")
    private String write_uid_text;

    @JsonIgnore
    private boolean write_uid_textDirtyFlag;

    /**
     * 属性 [COMPANY_ID_TEXT]
     *
     */
    @Account_financial_year_opCompany_id_textDefault(info = "默认规则")
    private String company_id_text;

    @JsonIgnore
    private boolean company_id_textDirtyFlag;

    /**
     * 属性 [CREATE_UID_TEXT]
     *
     */
    @Account_financial_year_opCreate_uid_textDefault(info = "默认规则")
    private String create_uid_text;

    @JsonIgnore
    private boolean create_uid_textDirtyFlag;

    /**
     * 属性 [WRITE_UID]
     *
     */
    @Account_financial_year_opWrite_uidDefault(info = "默认规则")
    private Integer write_uid;

    @JsonIgnore
    private boolean write_uidDirtyFlag;

    /**
     * 属性 [COMPANY_ID]
     *
     */
    @Account_financial_year_opCompany_idDefault(info = "默认规则")
    private Integer company_id;

    @JsonIgnore
    private boolean company_idDirtyFlag;

    /**
     * 属性 [CREATE_UID]
     *
     */
    @Account_financial_year_opCreate_uidDefault(info = "默认规则")
    private Integer create_uid;

    @JsonIgnore
    private boolean create_uidDirtyFlag;


    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

    /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return idDirtyFlag ;
    }

    /**
     * 获取 [OPENING_MOVE_POSTED]
     */
    @JsonProperty("opening_move_posted")
    public String getOpening_move_posted(){
        return opening_move_posted ;
    }

    /**
     * 设置 [OPENING_MOVE_POSTED]
     */
    @JsonProperty("opening_move_posted")
    public void setOpening_move_posted(String  opening_move_posted){
        this.opening_move_posted = opening_move_posted ;
        this.opening_move_postedDirtyFlag = true ;
    }

    /**
     * 获取 [OPENING_MOVE_POSTED]脏标记
     */
    @JsonIgnore
    public boolean getOpening_move_postedDirtyFlag(){
        return opening_move_postedDirtyFlag ;
    }

    /**
     * 获取 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return __last_update ;
    }

    /**
     * 设置 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

    /**
     * 获取 [__LAST_UPDATE]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return __last_updateDirtyFlag ;
    }

    /**
     * 获取 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return create_date ;
    }

    /**
     * 设置 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return create_dateDirtyFlag ;
    }

    /**
     * 获取 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return write_date ;
    }

    /**
     * 设置 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return write_dateDirtyFlag ;
    }

    /**
     * 获取 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return display_name ;
    }

    /**
     * 设置 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

    /**
     * 获取 [DISPLAY_NAME]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return display_nameDirtyFlag ;
    }

    /**
     * 获取 [FISCALYEAR_LAST_DAY]
     */
    @JsonProperty("fiscalyear_last_day")
    public Integer getFiscalyear_last_day(){
        return fiscalyear_last_day ;
    }

    /**
     * 设置 [FISCALYEAR_LAST_DAY]
     */
    @JsonProperty("fiscalyear_last_day")
    public void setFiscalyear_last_day(Integer  fiscalyear_last_day){
        this.fiscalyear_last_day = fiscalyear_last_day ;
        this.fiscalyear_last_dayDirtyFlag = true ;
    }

    /**
     * 获取 [FISCALYEAR_LAST_DAY]脏标记
     */
    @JsonIgnore
    public boolean getFiscalyear_last_dayDirtyFlag(){
        return fiscalyear_last_dayDirtyFlag ;
    }

    /**
     * 获取 [FISCALYEAR_LAST_MONTH]
     */
    @JsonProperty("fiscalyear_last_month")
    public String getFiscalyear_last_month(){
        return fiscalyear_last_month ;
    }

    /**
     * 设置 [FISCALYEAR_LAST_MONTH]
     */
    @JsonProperty("fiscalyear_last_month")
    public void setFiscalyear_last_month(String  fiscalyear_last_month){
        this.fiscalyear_last_month = fiscalyear_last_month ;
        this.fiscalyear_last_monthDirtyFlag = true ;
    }

    /**
     * 获取 [FISCALYEAR_LAST_MONTH]脏标记
     */
    @JsonIgnore
    public boolean getFiscalyear_last_monthDirtyFlag(){
        return fiscalyear_last_monthDirtyFlag ;
    }

    /**
     * 获取 [OPENING_DATE]
     */
    @JsonProperty("opening_date")
    public Timestamp getOpening_date(){
        return opening_date ;
    }

    /**
     * 设置 [OPENING_DATE]
     */
    @JsonProperty("opening_date")
    public void setOpening_date(Timestamp  opening_date){
        this.opening_date = opening_date ;
        this.opening_dateDirtyFlag = true ;
    }

    /**
     * 获取 [OPENING_DATE]脏标记
     */
    @JsonIgnore
    public boolean getOpening_dateDirtyFlag(){
        return opening_dateDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return write_uid_text ;
    }

    /**
     * 设置 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return write_uid_textDirtyFlag ;
    }

    /**
     * 获取 [COMPANY_ID_TEXT]
     */
    @JsonProperty("company_id_text")
    public String getCompany_id_text(){
        return company_id_text ;
    }

    /**
     * 设置 [COMPANY_ID_TEXT]
     */
    @JsonProperty("company_id_text")
    public void setCompany_id_text(String  company_id_text){
        this.company_id_text = company_id_text ;
        this.company_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [COMPANY_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getCompany_id_textDirtyFlag(){
        return company_id_textDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return create_uid_text ;
    }

    /**
     * 设置 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return create_uid_textDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return write_uid ;
    }

    /**
     * 设置 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return write_uidDirtyFlag ;
    }

    /**
     * 获取 [COMPANY_ID]
     */
    @JsonProperty("company_id")
    public Integer getCompany_id(){
        return company_id ;
    }

    /**
     * 设置 [COMPANY_ID]
     */
    @JsonProperty("company_id")
    public void setCompany_id(Integer  company_id){
        this.company_id = company_id ;
        this.company_idDirtyFlag = true ;
    }

    /**
     * 获取 [COMPANY_ID]脏标记
     */
    @JsonIgnore
    public boolean getCompany_idDirtyFlag(){
        return company_idDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return create_uid ;
    }

    /**
     * 设置 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return create_uidDirtyFlag ;
    }



    public Account_financial_year_op toDO() {
        Account_financial_year_op srfdomain = new Account_financial_year_op();
        if(getIdDirtyFlag())
            srfdomain.setId(id);
        if(getOpening_move_postedDirtyFlag())
            srfdomain.setOpening_move_posted(opening_move_posted);
        if(get__last_updateDirtyFlag())
            srfdomain.set__last_update(__last_update);
        if(getCreate_dateDirtyFlag())
            srfdomain.setCreate_date(create_date);
        if(getWrite_dateDirtyFlag())
            srfdomain.setWrite_date(write_date);
        if(getDisplay_nameDirtyFlag())
            srfdomain.setDisplay_name(display_name);
        if(getFiscalyear_last_dayDirtyFlag())
            srfdomain.setFiscalyear_last_day(fiscalyear_last_day);
        if(getFiscalyear_last_monthDirtyFlag())
            srfdomain.setFiscalyear_last_month(fiscalyear_last_month);
        if(getOpening_dateDirtyFlag())
            srfdomain.setOpening_date(opening_date);
        if(getWrite_uid_textDirtyFlag())
            srfdomain.setWrite_uid_text(write_uid_text);
        if(getCompany_id_textDirtyFlag())
            srfdomain.setCompany_id_text(company_id_text);
        if(getCreate_uid_textDirtyFlag())
            srfdomain.setCreate_uid_text(create_uid_text);
        if(getWrite_uidDirtyFlag())
            srfdomain.setWrite_uid(write_uid);
        if(getCompany_idDirtyFlag())
            srfdomain.setCompany_id(company_id);
        if(getCreate_uidDirtyFlag())
            srfdomain.setCreate_uid(create_uid);

        return srfdomain;
    }

    public void fromDO(Account_financial_year_op srfdomain) {
        if(srfdomain == null )
          return ;
        if(srfdomain.getIdDirtyFlag())
            this.setId(srfdomain.getId());
        if(srfdomain.getOpening_move_postedDirtyFlag())
            this.setOpening_move_posted(srfdomain.getOpening_move_posted());
        if(srfdomain.get__last_updateDirtyFlag())
            this.set__last_update(srfdomain.get__last_update());
        if(srfdomain.getCreate_dateDirtyFlag())
            this.setCreate_date(srfdomain.getCreate_date());
        if(srfdomain.getWrite_dateDirtyFlag())
            this.setWrite_date(srfdomain.getWrite_date());
        if(srfdomain.getDisplay_nameDirtyFlag())
            this.setDisplay_name(srfdomain.getDisplay_name());
        if(srfdomain.getFiscalyear_last_dayDirtyFlag())
            this.setFiscalyear_last_day(srfdomain.getFiscalyear_last_day());
        if(srfdomain.getFiscalyear_last_monthDirtyFlag())
            this.setFiscalyear_last_month(srfdomain.getFiscalyear_last_month());
        if(srfdomain.getOpening_dateDirtyFlag())
            this.setOpening_date(srfdomain.getOpening_date());
        if(srfdomain.getWrite_uid_textDirtyFlag())
            this.setWrite_uid_text(srfdomain.getWrite_uid_text());
        if(srfdomain.getCompany_id_textDirtyFlag())
            this.setCompany_id_text(srfdomain.getCompany_id_text());
        if(srfdomain.getCreate_uid_textDirtyFlag())
            this.setCreate_uid_text(srfdomain.getCreate_uid_text());
        if(srfdomain.getWrite_uidDirtyFlag())
            this.setWrite_uid(srfdomain.getWrite_uid());
        if(srfdomain.getCompany_idDirtyFlag())
            this.setCompany_id(srfdomain.getCompany_id());
        if(srfdomain.getCreate_uidDirtyFlag())
            this.setCreate_uid(srfdomain.getCreate_uid());

    }

    public List<Account_financial_year_opDTO> fromDOPage(List<Account_financial_year_op> poPage)   {
        if(poPage == null)
            return null;
        List<Account_financial_year_opDTO> dtos=new ArrayList<Account_financial_year_opDTO>();
        for(Account_financial_year_op domain : poPage) {
            Account_financial_year_opDTO dto = new Account_financial_year_opDTO();
            dto.fromDO(domain);
            dtos.add(dto);
        }
        return dtos;
    }
}

