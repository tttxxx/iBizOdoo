package cn.ibizlab.odoo.service.odoo_account.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.core.odoo_account.valuerule.anno.account_move_line.*;
import cn.ibizlab.odoo.core.odoo_account.domain.Account_move_line;
import org.springframework.cglib.beans.BeanCopier;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 服务DTO对象[Account_move_lineDTO]
 */
public class Account_move_lineDTO implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [ANALYTIC_TAG_IDS]
     *
     */
    @Account_move_lineAnalytic_tag_idsDefault(info = "默认规则")
    private String analytic_tag_ids;

    @JsonIgnore
    private boolean analytic_tag_idsDirtyFlag;

    /**
     * 属性 [WRITE_DATE]
     *
     */
    @Account_move_lineWrite_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp write_date;

    @JsonIgnore
    private boolean write_dateDirtyFlag;

    /**
     * 属性 [NAME]
     *
     */
    @Account_move_lineNameDefault(info = "默认规则")
    private String name;

    @JsonIgnore
    private boolean nameDirtyFlag;

    /**
     * 属性 [BALANCE]
     *
     */
    @Account_move_lineBalanceDefault(info = "默认规则")
    private Double balance;

    @JsonIgnore
    private boolean balanceDirtyFlag;

    /**
     * 属性 [TAX_IDS]
     *
     */
    @Account_move_lineTax_idsDefault(info = "默认规则")
    private String tax_ids;

    @JsonIgnore
    private boolean tax_idsDirtyFlag;

    /**
     * 属性 [AMOUNT_CURRENCY]
     *
     */
    @Account_move_lineAmount_currencyDefault(info = "默认规则")
    private Double amount_currency;

    @JsonIgnore
    private boolean amount_currencyDirtyFlag;

    /**
     * 属性 [TAX_LINE_GROUPING_KEY]
     *
     */
    @Account_move_lineTax_line_grouping_keyDefault(info = "默认规则")
    private String tax_line_grouping_key;

    @JsonIgnore
    private boolean tax_line_grouping_keyDirtyFlag;

    /**
     * 属性 [ID]
     *
     */
    @Account_move_lineIdDefault(info = "默认规则")
    private Integer id;

    @JsonIgnore
    private boolean idDirtyFlag;

    /**
     * 属性 [RECONCILED]
     *
     */
    @Account_move_lineReconciledDefault(info = "默认规则")
    private String reconciled;

    @JsonIgnore
    private boolean reconciledDirtyFlag;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @Account_move_lineDisplay_nameDefault(info = "默认规则")
    private String display_name;

    @JsonIgnore
    private boolean display_nameDirtyFlag;

    /**
     * 属性 [BALANCE_CASH_BASIS]
     *
     */
    @Account_move_lineBalance_cash_basisDefault(info = "默认规则")
    private Double balance_cash_basis;

    @JsonIgnore
    private boolean balance_cash_basisDirtyFlag;

    /**
     * 属性 [CREDIT_CASH_BASIS]
     *
     */
    @Account_move_lineCredit_cash_basisDefault(info = "默认规则")
    private Double credit_cash_basis;

    @JsonIgnore
    private boolean credit_cash_basisDirtyFlag;

    /**
     * 属性 [QUANTITY]
     *
     */
    @Account_move_lineQuantityDefault(info = "默认规则")
    private Double quantity;

    @JsonIgnore
    private boolean quantityDirtyFlag;

    /**
     * 属性 [AMOUNT_RESIDUAL]
     *
     */
    @Account_move_lineAmount_residualDefault(info = "默认规则")
    private Double amount_residual;

    @JsonIgnore
    private boolean amount_residualDirtyFlag;

    /**
     * 属性 [RECOMPUTE_TAX_LINE]
     *
     */
    @Account_move_lineRecompute_tax_lineDefault(info = "默认规则")
    private String recompute_tax_line;

    @JsonIgnore
    private boolean recompute_tax_lineDirtyFlag;

    /**
     * 属性 [TAX_EXIGIBLE]
     *
     */
    @Account_move_lineTax_exigibleDefault(info = "默认规则")
    private String tax_exigible;

    @JsonIgnore
    private boolean tax_exigibleDirtyFlag;

    /**
     * 属性 [AMOUNT_RESIDUAL_CURRENCY]
     *
     */
    @Account_move_lineAmount_residual_currencyDefault(info = "默认规则")
    private Double amount_residual_currency;

    @JsonIgnore
    private boolean amount_residual_currencyDirtyFlag;

    /**
     * 属性 [TAX_BASE_AMOUNT]
     *
     */
    @Account_move_lineTax_base_amountDefault(info = "默认规则")
    private Double tax_base_amount;

    @JsonIgnore
    private boolean tax_base_amountDirtyFlag;

    /**
     * 属性 [PARENT_STATE]
     *
     */
    @Account_move_lineParent_stateDefault(info = "默认规则")
    private String parent_state;

    @JsonIgnore
    private boolean parent_stateDirtyFlag;

    /**
     * 属性 [BLOCKED]
     *
     */
    @Account_move_lineBlockedDefault(info = "默认规则")
    private String blocked;

    @JsonIgnore
    private boolean blockedDirtyFlag;

    /**
     * 属性 [CREATE_DATE]
     *
     */
    @Account_move_lineCreate_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp create_date;

    @JsonIgnore
    private boolean create_dateDirtyFlag;

    /**
     * 属性 [MATCHED_CREDIT_IDS]
     *
     */
    @Account_move_lineMatched_credit_idsDefault(info = "默认规则")
    private String matched_credit_ids;

    @JsonIgnore
    private boolean matched_credit_idsDirtyFlag;

    /**
     * 属性 [ANALYTIC_LINE_IDS]
     *
     */
    @Account_move_lineAnalytic_line_idsDefault(info = "默认规则")
    private String analytic_line_ids;

    @JsonIgnore
    private boolean analytic_line_idsDirtyFlag;

    /**
     * 属性 [DATE_MATURITY]
     *
     */
    @Account_move_lineDate_maturityDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    private Timestamp date_maturity;

    @JsonIgnore
    private boolean date_maturityDirtyFlag;

    /**
     * 属性 [DEBIT]
     *
     */
    @Account_move_lineDebitDefault(info = "默认规则")
    private Double debit;

    @JsonIgnore
    private boolean debitDirtyFlag;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @Account_move_line__last_updateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp __last_update;

    @JsonIgnore
    private boolean __last_updateDirtyFlag;

    /**
     * 属性 [COUNTERPART]
     *
     */
    @Account_move_lineCounterpartDefault(info = "默认规则")
    private String counterpart;

    @JsonIgnore
    private boolean counterpartDirtyFlag;

    /**
     * 属性 [MATCHED_DEBIT_IDS]
     *
     */
    @Account_move_lineMatched_debit_idsDefault(info = "默认规则")
    private String matched_debit_ids;

    @JsonIgnore
    private boolean matched_debit_idsDirtyFlag;

    /**
     * 属性 [DEBIT_CASH_BASIS]
     *
     */
    @Account_move_lineDebit_cash_basisDefault(info = "默认规则")
    private Double debit_cash_basis;

    @JsonIgnore
    private boolean debit_cash_basisDirtyFlag;

    /**
     * 属性 [CREDIT]
     *
     */
    @Account_move_lineCreditDefault(info = "默认规则")
    private Double credit;

    @JsonIgnore
    private boolean creditDirtyFlag;

    /**
     * 属性 [NARRATION]
     *
     */
    @Account_move_lineNarrationDefault(info = "默认规则")
    private String narration;

    @JsonIgnore
    private boolean narrationDirtyFlag;

    /**
     * 属性 [PRODUCT_ID_TEXT]
     *
     */
    @Account_move_lineProduct_id_textDefault(info = "默认规则")
    private String product_id_text;

    @JsonIgnore
    private boolean product_id_textDirtyFlag;

    /**
     * 属性 [USER_TYPE_ID_TEXT]
     *
     */
    @Account_move_lineUser_type_id_textDefault(info = "默认规则")
    private String user_type_id_text;

    @JsonIgnore
    private boolean user_type_id_textDirtyFlag;

    /**
     * 属性 [PAYMENT_ID_TEXT]
     *
     */
    @Account_move_linePayment_id_textDefault(info = "默认规则")
    private String payment_id_text;

    @JsonIgnore
    private boolean payment_id_textDirtyFlag;

    /**
     * 属性 [TAX_LINE_ID_TEXT]
     *
     */
    @Account_move_lineTax_line_id_textDefault(info = "默认规则")
    private String tax_line_id_text;

    @JsonIgnore
    private boolean tax_line_id_textDirtyFlag;

    /**
     * 属性 [FULL_RECONCILE_ID_TEXT]
     *
     */
    @Account_move_lineFull_reconcile_id_textDefault(info = "默认规则")
    private String full_reconcile_id_text;

    @JsonIgnore
    private boolean full_reconcile_id_textDirtyFlag;

    /**
     * 属性 [EXPENSE_ID_TEXT]
     *
     */
    @Account_move_lineExpense_id_textDefault(info = "默认规则")
    private String expense_id_text;

    @JsonIgnore
    private boolean expense_id_textDirtyFlag;

    /**
     * 属性 [MOVE_ID_TEXT]
     *
     */
    @Account_move_lineMove_id_textDefault(info = "默认规则")
    private String move_id_text;

    @JsonIgnore
    private boolean move_id_textDirtyFlag;

    /**
     * 属性 [JOURNAL_ID_TEXT]
     *
     */
    @Account_move_lineJournal_id_textDefault(info = "默认规则")
    private String journal_id_text;

    @JsonIgnore
    private boolean journal_id_textDirtyFlag;

    /**
     * 属性 [COMPANY_CURRENCY_ID_TEXT]
     *
     */
    @Account_move_lineCompany_currency_id_textDefault(info = "默认规则")
    private String company_currency_id_text;

    @JsonIgnore
    private boolean company_currency_id_textDirtyFlag;

    /**
     * 属性 [INVOICE_ID_TEXT]
     *
     */
    @Account_move_lineInvoice_id_textDefault(info = "默认规则")
    private String invoice_id_text;

    @JsonIgnore
    private boolean invoice_id_textDirtyFlag;

    /**
     * 属性 [ANALYTIC_ACCOUNT_ID_TEXT]
     *
     */
    @Account_move_lineAnalytic_account_id_textDefault(info = "默认规则")
    private String analytic_account_id_text;

    @JsonIgnore
    private boolean analytic_account_id_textDirtyFlag;

    /**
     * 属性 [CREATE_UID_TEXT]
     *
     */
    @Account_move_lineCreate_uid_textDefault(info = "默认规则")
    private String create_uid_text;

    @JsonIgnore
    private boolean create_uid_textDirtyFlag;

    /**
     * 属性 [CURRENCY_ID_TEXT]
     *
     */
    @Account_move_lineCurrency_id_textDefault(info = "默认规则")
    private String currency_id_text;

    @JsonIgnore
    private boolean currency_id_textDirtyFlag;

    /**
     * 属性 [REF]
     *
     */
    @Account_move_lineRefDefault(info = "默认规则")
    private String ref;

    @JsonIgnore
    private boolean refDirtyFlag;

    /**
     * 属性 [COMPANY_ID_TEXT]
     *
     */
    @Account_move_lineCompany_id_textDefault(info = "默认规则")
    private String company_id_text;

    @JsonIgnore
    private boolean company_id_textDirtyFlag;

    /**
     * 属性 [STATEMENT_ID_TEXT]
     *
     */
    @Account_move_lineStatement_id_textDefault(info = "默认规则")
    private String statement_id_text;

    @JsonIgnore
    private boolean statement_id_textDirtyFlag;

    /**
     * 属性 [STATEMENT_LINE_ID_TEXT]
     *
     */
    @Account_move_lineStatement_line_id_textDefault(info = "默认规则")
    private String statement_line_id_text;

    @JsonIgnore
    private boolean statement_line_id_textDirtyFlag;

    /**
     * 属性 [PARTNER_ID_TEXT]
     *
     */
    @Account_move_linePartner_id_textDefault(info = "默认规则")
    private String partner_id_text;

    @JsonIgnore
    private boolean partner_id_textDirtyFlag;

    /**
     * 属性 [ACCOUNT_ID_TEXT]
     *
     */
    @Account_move_lineAccount_id_textDefault(info = "默认规则")
    private String account_id_text;

    @JsonIgnore
    private boolean account_id_textDirtyFlag;

    /**
     * 属性 [DATE]
     *
     */
    @Account_move_lineDateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    private Timestamp date;

    @JsonIgnore
    private boolean dateDirtyFlag;

    /**
     * 属性 [PRODUCT_UOM_ID_TEXT]
     *
     */
    @Account_move_lineProduct_uom_id_textDefault(info = "默认规则")
    private String product_uom_id_text;

    @JsonIgnore
    private boolean product_uom_id_textDirtyFlag;

    /**
     * 属性 [WRITE_UID_TEXT]
     *
     */
    @Account_move_lineWrite_uid_textDefault(info = "默认规则")
    private String write_uid_text;

    @JsonIgnore
    private boolean write_uid_textDirtyFlag;

    /**
     * 属性 [MOVE_ID]
     *
     */
    @Account_move_lineMove_idDefault(info = "默认规则")
    private Integer move_id;

    @JsonIgnore
    private boolean move_idDirtyFlag;

    /**
     * 属性 [CURRENCY_ID]
     *
     */
    @Account_move_lineCurrency_idDefault(info = "默认规则")
    private Integer currency_id;

    @JsonIgnore
    private boolean currency_idDirtyFlag;

    /**
     * 属性 [COMPANY_ID]
     *
     */
    @Account_move_lineCompany_idDefault(info = "默认规则")
    private Integer company_id;

    @JsonIgnore
    private boolean company_idDirtyFlag;

    /**
     * 属性 [PAYMENT_ID]
     *
     */
    @Account_move_linePayment_idDefault(info = "默认规则")
    private Integer payment_id;

    @JsonIgnore
    private boolean payment_idDirtyFlag;

    /**
     * 属性 [COMPANY_CURRENCY_ID]
     *
     */
    @Account_move_lineCompany_currency_idDefault(info = "默认规则")
    private Integer company_currency_id;

    @JsonIgnore
    private boolean company_currency_idDirtyFlag;

    /**
     * 属性 [JOURNAL_ID]
     *
     */
    @Account_move_lineJournal_idDefault(info = "默认规则")
    private Integer journal_id;

    @JsonIgnore
    private boolean journal_idDirtyFlag;

    /**
     * 属性 [ANALYTIC_ACCOUNT_ID]
     *
     */
    @Account_move_lineAnalytic_account_idDefault(info = "默认规则")
    private Integer analytic_account_id;

    @JsonIgnore
    private boolean analytic_account_idDirtyFlag;

    /**
     * 属性 [INVOICE_ID]
     *
     */
    @Account_move_lineInvoice_idDefault(info = "默认规则")
    private Integer invoice_id;

    @JsonIgnore
    private boolean invoice_idDirtyFlag;

    /**
     * 属性 [TAX_LINE_ID]
     *
     */
    @Account_move_lineTax_line_idDefault(info = "默认规则")
    private Integer tax_line_id;

    @JsonIgnore
    private boolean tax_line_idDirtyFlag;

    /**
     * 属性 [FULL_RECONCILE_ID]
     *
     */
    @Account_move_lineFull_reconcile_idDefault(info = "默认规则")
    private Integer full_reconcile_id;

    @JsonIgnore
    private boolean full_reconcile_idDirtyFlag;

    /**
     * 属性 [STATEMENT_LINE_ID]
     *
     */
    @Account_move_lineStatement_line_idDefault(info = "默认规则")
    private Integer statement_line_id;

    @JsonIgnore
    private boolean statement_line_idDirtyFlag;

    /**
     * 属性 [CREATE_UID]
     *
     */
    @Account_move_lineCreate_uidDefault(info = "默认规则")
    private Integer create_uid;

    @JsonIgnore
    private boolean create_uidDirtyFlag;

    /**
     * 属性 [PRODUCT_ID]
     *
     */
    @Account_move_lineProduct_idDefault(info = "默认规则")
    private Integer product_id;

    @JsonIgnore
    private boolean product_idDirtyFlag;

    /**
     * 属性 [PRODUCT_UOM_ID]
     *
     */
    @Account_move_lineProduct_uom_idDefault(info = "默认规则")
    private Integer product_uom_id;

    @JsonIgnore
    private boolean product_uom_idDirtyFlag;

    /**
     * 属性 [PARTNER_ID]
     *
     */
    @Account_move_linePartner_idDefault(info = "默认规则")
    private Integer partner_id;

    @JsonIgnore
    private boolean partner_idDirtyFlag;

    /**
     * 属性 [ACCOUNT_ID]
     *
     */
    @Account_move_lineAccount_idDefault(info = "默认规则")
    private Integer account_id;

    @JsonIgnore
    private boolean account_idDirtyFlag;

    /**
     * 属性 [STATEMENT_ID]
     *
     */
    @Account_move_lineStatement_idDefault(info = "默认规则")
    private Integer statement_id;

    @JsonIgnore
    private boolean statement_idDirtyFlag;

    /**
     * 属性 [WRITE_UID]
     *
     */
    @Account_move_lineWrite_uidDefault(info = "默认规则")
    private Integer write_uid;

    @JsonIgnore
    private boolean write_uidDirtyFlag;

    /**
     * 属性 [USER_TYPE_ID]
     *
     */
    @Account_move_lineUser_type_idDefault(info = "默认规则")
    private Integer user_type_id;

    @JsonIgnore
    private boolean user_type_idDirtyFlag;

    /**
     * 属性 [EXPENSE_ID]
     *
     */
    @Account_move_lineExpense_idDefault(info = "默认规则")
    private Integer expense_id;

    @JsonIgnore
    private boolean expense_idDirtyFlag;


    /**
     * 获取 [ANALYTIC_TAG_IDS]
     */
    @JsonProperty("analytic_tag_ids")
    public String getAnalytic_tag_ids(){
        return analytic_tag_ids ;
    }

    /**
     * 设置 [ANALYTIC_TAG_IDS]
     */
    @JsonProperty("analytic_tag_ids")
    public void setAnalytic_tag_ids(String  analytic_tag_ids){
        this.analytic_tag_ids = analytic_tag_ids ;
        this.analytic_tag_idsDirtyFlag = true ;
    }

    /**
     * 获取 [ANALYTIC_TAG_IDS]脏标记
     */
    @JsonIgnore
    public boolean getAnalytic_tag_idsDirtyFlag(){
        return analytic_tag_idsDirtyFlag ;
    }

    /**
     * 获取 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return write_date ;
    }

    /**
     * 设置 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return write_dateDirtyFlag ;
    }

    /**
     * 获取 [NAME]
     */
    @JsonProperty("name")
    public String getName(){
        return name ;
    }

    /**
     * 设置 [NAME]
     */
    @JsonProperty("name")
    public void setName(String  name){
        this.name = name ;
        this.nameDirtyFlag = true ;
    }

    /**
     * 获取 [NAME]脏标记
     */
    @JsonIgnore
    public boolean getNameDirtyFlag(){
        return nameDirtyFlag ;
    }

    /**
     * 获取 [BALANCE]
     */
    @JsonProperty("balance")
    public Double getBalance(){
        return balance ;
    }

    /**
     * 设置 [BALANCE]
     */
    @JsonProperty("balance")
    public void setBalance(Double  balance){
        this.balance = balance ;
        this.balanceDirtyFlag = true ;
    }

    /**
     * 获取 [BALANCE]脏标记
     */
    @JsonIgnore
    public boolean getBalanceDirtyFlag(){
        return balanceDirtyFlag ;
    }

    /**
     * 获取 [TAX_IDS]
     */
    @JsonProperty("tax_ids")
    public String getTax_ids(){
        return tax_ids ;
    }

    /**
     * 设置 [TAX_IDS]
     */
    @JsonProperty("tax_ids")
    public void setTax_ids(String  tax_ids){
        this.tax_ids = tax_ids ;
        this.tax_idsDirtyFlag = true ;
    }

    /**
     * 获取 [TAX_IDS]脏标记
     */
    @JsonIgnore
    public boolean getTax_idsDirtyFlag(){
        return tax_idsDirtyFlag ;
    }

    /**
     * 获取 [AMOUNT_CURRENCY]
     */
    @JsonProperty("amount_currency")
    public Double getAmount_currency(){
        return amount_currency ;
    }

    /**
     * 设置 [AMOUNT_CURRENCY]
     */
    @JsonProperty("amount_currency")
    public void setAmount_currency(Double  amount_currency){
        this.amount_currency = amount_currency ;
        this.amount_currencyDirtyFlag = true ;
    }

    /**
     * 获取 [AMOUNT_CURRENCY]脏标记
     */
    @JsonIgnore
    public boolean getAmount_currencyDirtyFlag(){
        return amount_currencyDirtyFlag ;
    }

    /**
     * 获取 [TAX_LINE_GROUPING_KEY]
     */
    @JsonProperty("tax_line_grouping_key")
    public String getTax_line_grouping_key(){
        return tax_line_grouping_key ;
    }

    /**
     * 设置 [TAX_LINE_GROUPING_KEY]
     */
    @JsonProperty("tax_line_grouping_key")
    public void setTax_line_grouping_key(String  tax_line_grouping_key){
        this.tax_line_grouping_key = tax_line_grouping_key ;
        this.tax_line_grouping_keyDirtyFlag = true ;
    }

    /**
     * 获取 [TAX_LINE_GROUPING_KEY]脏标记
     */
    @JsonIgnore
    public boolean getTax_line_grouping_keyDirtyFlag(){
        return tax_line_grouping_keyDirtyFlag ;
    }

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

    /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return idDirtyFlag ;
    }

    /**
     * 获取 [RECONCILED]
     */
    @JsonProperty("reconciled")
    public String getReconciled(){
        return reconciled ;
    }

    /**
     * 设置 [RECONCILED]
     */
    @JsonProperty("reconciled")
    public void setReconciled(String  reconciled){
        this.reconciled = reconciled ;
        this.reconciledDirtyFlag = true ;
    }

    /**
     * 获取 [RECONCILED]脏标记
     */
    @JsonIgnore
    public boolean getReconciledDirtyFlag(){
        return reconciledDirtyFlag ;
    }

    /**
     * 获取 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return display_name ;
    }

    /**
     * 设置 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

    /**
     * 获取 [DISPLAY_NAME]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return display_nameDirtyFlag ;
    }

    /**
     * 获取 [BALANCE_CASH_BASIS]
     */
    @JsonProperty("balance_cash_basis")
    public Double getBalance_cash_basis(){
        return balance_cash_basis ;
    }

    /**
     * 设置 [BALANCE_CASH_BASIS]
     */
    @JsonProperty("balance_cash_basis")
    public void setBalance_cash_basis(Double  balance_cash_basis){
        this.balance_cash_basis = balance_cash_basis ;
        this.balance_cash_basisDirtyFlag = true ;
    }

    /**
     * 获取 [BALANCE_CASH_BASIS]脏标记
     */
    @JsonIgnore
    public boolean getBalance_cash_basisDirtyFlag(){
        return balance_cash_basisDirtyFlag ;
    }

    /**
     * 获取 [CREDIT_CASH_BASIS]
     */
    @JsonProperty("credit_cash_basis")
    public Double getCredit_cash_basis(){
        return credit_cash_basis ;
    }

    /**
     * 设置 [CREDIT_CASH_BASIS]
     */
    @JsonProperty("credit_cash_basis")
    public void setCredit_cash_basis(Double  credit_cash_basis){
        this.credit_cash_basis = credit_cash_basis ;
        this.credit_cash_basisDirtyFlag = true ;
    }

    /**
     * 获取 [CREDIT_CASH_BASIS]脏标记
     */
    @JsonIgnore
    public boolean getCredit_cash_basisDirtyFlag(){
        return credit_cash_basisDirtyFlag ;
    }

    /**
     * 获取 [QUANTITY]
     */
    @JsonProperty("quantity")
    public Double getQuantity(){
        return quantity ;
    }

    /**
     * 设置 [QUANTITY]
     */
    @JsonProperty("quantity")
    public void setQuantity(Double  quantity){
        this.quantity = quantity ;
        this.quantityDirtyFlag = true ;
    }

    /**
     * 获取 [QUANTITY]脏标记
     */
    @JsonIgnore
    public boolean getQuantityDirtyFlag(){
        return quantityDirtyFlag ;
    }

    /**
     * 获取 [AMOUNT_RESIDUAL]
     */
    @JsonProperty("amount_residual")
    public Double getAmount_residual(){
        return amount_residual ;
    }

    /**
     * 设置 [AMOUNT_RESIDUAL]
     */
    @JsonProperty("amount_residual")
    public void setAmount_residual(Double  amount_residual){
        this.amount_residual = amount_residual ;
        this.amount_residualDirtyFlag = true ;
    }

    /**
     * 获取 [AMOUNT_RESIDUAL]脏标记
     */
    @JsonIgnore
    public boolean getAmount_residualDirtyFlag(){
        return amount_residualDirtyFlag ;
    }

    /**
     * 获取 [RECOMPUTE_TAX_LINE]
     */
    @JsonProperty("recompute_tax_line")
    public String getRecompute_tax_line(){
        return recompute_tax_line ;
    }

    /**
     * 设置 [RECOMPUTE_TAX_LINE]
     */
    @JsonProperty("recompute_tax_line")
    public void setRecompute_tax_line(String  recompute_tax_line){
        this.recompute_tax_line = recompute_tax_line ;
        this.recompute_tax_lineDirtyFlag = true ;
    }

    /**
     * 获取 [RECOMPUTE_TAX_LINE]脏标记
     */
    @JsonIgnore
    public boolean getRecompute_tax_lineDirtyFlag(){
        return recompute_tax_lineDirtyFlag ;
    }

    /**
     * 获取 [TAX_EXIGIBLE]
     */
    @JsonProperty("tax_exigible")
    public String getTax_exigible(){
        return tax_exigible ;
    }

    /**
     * 设置 [TAX_EXIGIBLE]
     */
    @JsonProperty("tax_exigible")
    public void setTax_exigible(String  tax_exigible){
        this.tax_exigible = tax_exigible ;
        this.tax_exigibleDirtyFlag = true ;
    }

    /**
     * 获取 [TAX_EXIGIBLE]脏标记
     */
    @JsonIgnore
    public boolean getTax_exigibleDirtyFlag(){
        return tax_exigibleDirtyFlag ;
    }

    /**
     * 获取 [AMOUNT_RESIDUAL_CURRENCY]
     */
    @JsonProperty("amount_residual_currency")
    public Double getAmount_residual_currency(){
        return amount_residual_currency ;
    }

    /**
     * 设置 [AMOUNT_RESIDUAL_CURRENCY]
     */
    @JsonProperty("amount_residual_currency")
    public void setAmount_residual_currency(Double  amount_residual_currency){
        this.amount_residual_currency = amount_residual_currency ;
        this.amount_residual_currencyDirtyFlag = true ;
    }

    /**
     * 获取 [AMOUNT_RESIDUAL_CURRENCY]脏标记
     */
    @JsonIgnore
    public boolean getAmount_residual_currencyDirtyFlag(){
        return amount_residual_currencyDirtyFlag ;
    }

    /**
     * 获取 [TAX_BASE_AMOUNT]
     */
    @JsonProperty("tax_base_amount")
    public Double getTax_base_amount(){
        return tax_base_amount ;
    }

    /**
     * 设置 [TAX_BASE_AMOUNT]
     */
    @JsonProperty("tax_base_amount")
    public void setTax_base_amount(Double  tax_base_amount){
        this.tax_base_amount = tax_base_amount ;
        this.tax_base_amountDirtyFlag = true ;
    }

    /**
     * 获取 [TAX_BASE_AMOUNT]脏标记
     */
    @JsonIgnore
    public boolean getTax_base_amountDirtyFlag(){
        return tax_base_amountDirtyFlag ;
    }

    /**
     * 获取 [PARENT_STATE]
     */
    @JsonProperty("parent_state")
    public String getParent_state(){
        return parent_state ;
    }

    /**
     * 设置 [PARENT_STATE]
     */
    @JsonProperty("parent_state")
    public void setParent_state(String  parent_state){
        this.parent_state = parent_state ;
        this.parent_stateDirtyFlag = true ;
    }

    /**
     * 获取 [PARENT_STATE]脏标记
     */
    @JsonIgnore
    public boolean getParent_stateDirtyFlag(){
        return parent_stateDirtyFlag ;
    }

    /**
     * 获取 [BLOCKED]
     */
    @JsonProperty("blocked")
    public String getBlocked(){
        return blocked ;
    }

    /**
     * 设置 [BLOCKED]
     */
    @JsonProperty("blocked")
    public void setBlocked(String  blocked){
        this.blocked = blocked ;
        this.blockedDirtyFlag = true ;
    }

    /**
     * 获取 [BLOCKED]脏标记
     */
    @JsonIgnore
    public boolean getBlockedDirtyFlag(){
        return blockedDirtyFlag ;
    }

    /**
     * 获取 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return create_date ;
    }

    /**
     * 设置 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return create_dateDirtyFlag ;
    }

    /**
     * 获取 [MATCHED_CREDIT_IDS]
     */
    @JsonProperty("matched_credit_ids")
    public String getMatched_credit_ids(){
        return matched_credit_ids ;
    }

    /**
     * 设置 [MATCHED_CREDIT_IDS]
     */
    @JsonProperty("matched_credit_ids")
    public void setMatched_credit_ids(String  matched_credit_ids){
        this.matched_credit_ids = matched_credit_ids ;
        this.matched_credit_idsDirtyFlag = true ;
    }

    /**
     * 获取 [MATCHED_CREDIT_IDS]脏标记
     */
    @JsonIgnore
    public boolean getMatched_credit_idsDirtyFlag(){
        return matched_credit_idsDirtyFlag ;
    }

    /**
     * 获取 [ANALYTIC_LINE_IDS]
     */
    @JsonProperty("analytic_line_ids")
    public String getAnalytic_line_ids(){
        return analytic_line_ids ;
    }

    /**
     * 设置 [ANALYTIC_LINE_IDS]
     */
    @JsonProperty("analytic_line_ids")
    public void setAnalytic_line_ids(String  analytic_line_ids){
        this.analytic_line_ids = analytic_line_ids ;
        this.analytic_line_idsDirtyFlag = true ;
    }

    /**
     * 获取 [ANALYTIC_LINE_IDS]脏标记
     */
    @JsonIgnore
    public boolean getAnalytic_line_idsDirtyFlag(){
        return analytic_line_idsDirtyFlag ;
    }

    /**
     * 获取 [DATE_MATURITY]
     */
    @JsonProperty("date_maturity")
    public Timestamp getDate_maturity(){
        return date_maturity ;
    }

    /**
     * 设置 [DATE_MATURITY]
     */
    @JsonProperty("date_maturity")
    public void setDate_maturity(Timestamp  date_maturity){
        this.date_maturity = date_maturity ;
        this.date_maturityDirtyFlag = true ;
    }

    /**
     * 获取 [DATE_MATURITY]脏标记
     */
    @JsonIgnore
    public boolean getDate_maturityDirtyFlag(){
        return date_maturityDirtyFlag ;
    }

    /**
     * 获取 [DEBIT]
     */
    @JsonProperty("debit")
    public Double getDebit(){
        return debit ;
    }

    /**
     * 设置 [DEBIT]
     */
    @JsonProperty("debit")
    public void setDebit(Double  debit){
        this.debit = debit ;
        this.debitDirtyFlag = true ;
    }

    /**
     * 获取 [DEBIT]脏标记
     */
    @JsonIgnore
    public boolean getDebitDirtyFlag(){
        return debitDirtyFlag ;
    }

    /**
     * 获取 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return __last_update ;
    }

    /**
     * 设置 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

    /**
     * 获取 [__LAST_UPDATE]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return __last_updateDirtyFlag ;
    }

    /**
     * 获取 [COUNTERPART]
     */
    @JsonProperty("counterpart")
    public String getCounterpart(){
        return counterpart ;
    }

    /**
     * 设置 [COUNTERPART]
     */
    @JsonProperty("counterpart")
    public void setCounterpart(String  counterpart){
        this.counterpart = counterpart ;
        this.counterpartDirtyFlag = true ;
    }

    /**
     * 获取 [COUNTERPART]脏标记
     */
    @JsonIgnore
    public boolean getCounterpartDirtyFlag(){
        return counterpartDirtyFlag ;
    }

    /**
     * 获取 [MATCHED_DEBIT_IDS]
     */
    @JsonProperty("matched_debit_ids")
    public String getMatched_debit_ids(){
        return matched_debit_ids ;
    }

    /**
     * 设置 [MATCHED_DEBIT_IDS]
     */
    @JsonProperty("matched_debit_ids")
    public void setMatched_debit_ids(String  matched_debit_ids){
        this.matched_debit_ids = matched_debit_ids ;
        this.matched_debit_idsDirtyFlag = true ;
    }

    /**
     * 获取 [MATCHED_DEBIT_IDS]脏标记
     */
    @JsonIgnore
    public boolean getMatched_debit_idsDirtyFlag(){
        return matched_debit_idsDirtyFlag ;
    }

    /**
     * 获取 [DEBIT_CASH_BASIS]
     */
    @JsonProperty("debit_cash_basis")
    public Double getDebit_cash_basis(){
        return debit_cash_basis ;
    }

    /**
     * 设置 [DEBIT_CASH_BASIS]
     */
    @JsonProperty("debit_cash_basis")
    public void setDebit_cash_basis(Double  debit_cash_basis){
        this.debit_cash_basis = debit_cash_basis ;
        this.debit_cash_basisDirtyFlag = true ;
    }

    /**
     * 获取 [DEBIT_CASH_BASIS]脏标记
     */
    @JsonIgnore
    public boolean getDebit_cash_basisDirtyFlag(){
        return debit_cash_basisDirtyFlag ;
    }

    /**
     * 获取 [CREDIT]
     */
    @JsonProperty("credit")
    public Double getCredit(){
        return credit ;
    }

    /**
     * 设置 [CREDIT]
     */
    @JsonProperty("credit")
    public void setCredit(Double  credit){
        this.credit = credit ;
        this.creditDirtyFlag = true ;
    }

    /**
     * 获取 [CREDIT]脏标记
     */
    @JsonIgnore
    public boolean getCreditDirtyFlag(){
        return creditDirtyFlag ;
    }

    /**
     * 获取 [NARRATION]
     */
    @JsonProperty("narration")
    public String getNarration(){
        return narration ;
    }

    /**
     * 设置 [NARRATION]
     */
    @JsonProperty("narration")
    public void setNarration(String  narration){
        this.narration = narration ;
        this.narrationDirtyFlag = true ;
    }

    /**
     * 获取 [NARRATION]脏标记
     */
    @JsonIgnore
    public boolean getNarrationDirtyFlag(){
        return narrationDirtyFlag ;
    }

    /**
     * 获取 [PRODUCT_ID_TEXT]
     */
    @JsonProperty("product_id_text")
    public String getProduct_id_text(){
        return product_id_text ;
    }

    /**
     * 设置 [PRODUCT_ID_TEXT]
     */
    @JsonProperty("product_id_text")
    public void setProduct_id_text(String  product_id_text){
        this.product_id_text = product_id_text ;
        this.product_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [PRODUCT_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getProduct_id_textDirtyFlag(){
        return product_id_textDirtyFlag ;
    }

    /**
     * 获取 [USER_TYPE_ID_TEXT]
     */
    @JsonProperty("user_type_id_text")
    public String getUser_type_id_text(){
        return user_type_id_text ;
    }

    /**
     * 设置 [USER_TYPE_ID_TEXT]
     */
    @JsonProperty("user_type_id_text")
    public void setUser_type_id_text(String  user_type_id_text){
        this.user_type_id_text = user_type_id_text ;
        this.user_type_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [USER_TYPE_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getUser_type_id_textDirtyFlag(){
        return user_type_id_textDirtyFlag ;
    }

    /**
     * 获取 [PAYMENT_ID_TEXT]
     */
    @JsonProperty("payment_id_text")
    public String getPayment_id_text(){
        return payment_id_text ;
    }

    /**
     * 设置 [PAYMENT_ID_TEXT]
     */
    @JsonProperty("payment_id_text")
    public void setPayment_id_text(String  payment_id_text){
        this.payment_id_text = payment_id_text ;
        this.payment_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [PAYMENT_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getPayment_id_textDirtyFlag(){
        return payment_id_textDirtyFlag ;
    }

    /**
     * 获取 [TAX_LINE_ID_TEXT]
     */
    @JsonProperty("tax_line_id_text")
    public String getTax_line_id_text(){
        return tax_line_id_text ;
    }

    /**
     * 设置 [TAX_LINE_ID_TEXT]
     */
    @JsonProperty("tax_line_id_text")
    public void setTax_line_id_text(String  tax_line_id_text){
        this.tax_line_id_text = tax_line_id_text ;
        this.tax_line_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [TAX_LINE_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getTax_line_id_textDirtyFlag(){
        return tax_line_id_textDirtyFlag ;
    }

    /**
     * 获取 [FULL_RECONCILE_ID_TEXT]
     */
    @JsonProperty("full_reconcile_id_text")
    public String getFull_reconcile_id_text(){
        return full_reconcile_id_text ;
    }

    /**
     * 设置 [FULL_RECONCILE_ID_TEXT]
     */
    @JsonProperty("full_reconcile_id_text")
    public void setFull_reconcile_id_text(String  full_reconcile_id_text){
        this.full_reconcile_id_text = full_reconcile_id_text ;
        this.full_reconcile_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [FULL_RECONCILE_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getFull_reconcile_id_textDirtyFlag(){
        return full_reconcile_id_textDirtyFlag ;
    }

    /**
     * 获取 [EXPENSE_ID_TEXT]
     */
    @JsonProperty("expense_id_text")
    public String getExpense_id_text(){
        return expense_id_text ;
    }

    /**
     * 设置 [EXPENSE_ID_TEXT]
     */
    @JsonProperty("expense_id_text")
    public void setExpense_id_text(String  expense_id_text){
        this.expense_id_text = expense_id_text ;
        this.expense_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [EXPENSE_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getExpense_id_textDirtyFlag(){
        return expense_id_textDirtyFlag ;
    }

    /**
     * 获取 [MOVE_ID_TEXT]
     */
    @JsonProperty("move_id_text")
    public String getMove_id_text(){
        return move_id_text ;
    }

    /**
     * 设置 [MOVE_ID_TEXT]
     */
    @JsonProperty("move_id_text")
    public void setMove_id_text(String  move_id_text){
        this.move_id_text = move_id_text ;
        this.move_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [MOVE_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getMove_id_textDirtyFlag(){
        return move_id_textDirtyFlag ;
    }

    /**
     * 获取 [JOURNAL_ID_TEXT]
     */
    @JsonProperty("journal_id_text")
    public String getJournal_id_text(){
        return journal_id_text ;
    }

    /**
     * 设置 [JOURNAL_ID_TEXT]
     */
    @JsonProperty("journal_id_text")
    public void setJournal_id_text(String  journal_id_text){
        this.journal_id_text = journal_id_text ;
        this.journal_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [JOURNAL_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getJournal_id_textDirtyFlag(){
        return journal_id_textDirtyFlag ;
    }

    /**
     * 获取 [COMPANY_CURRENCY_ID_TEXT]
     */
    @JsonProperty("company_currency_id_text")
    public String getCompany_currency_id_text(){
        return company_currency_id_text ;
    }

    /**
     * 设置 [COMPANY_CURRENCY_ID_TEXT]
     */
    @JsonProperty("company_currency_id_text")
    public void setCompany_currency_id_text(String  company_currency_id_text){
        this.company_currency_id_text = company_currency_id_text ;
        this.company_currency_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [COMPANY_CURRENCY_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getCompany_currency_id_textDirtyFlag(){
        return company_currency_id_textDirtyFlag ;
    }

    /**
     * 获取 [INVOICE_ID_TEXT]
     */
    @JsonProperty("invoice_id_text")
    public String getInvoice_id_text(){
        return invoice_id_text ;
    }

    /**
     * 设置 [INVOICE_ID_TEXT]
     */
    @JsonProperty("invoice_id_text")
    public void setInvoice_id_text(String  invoice_id_text){
        this.invoice_id_text = invoice_id_text ;
        this.invoice_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [INVOICE_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getInvoice_id_textDirtyFlag(){
        return invoice_id_textDirtyFlag ;
    }

    /**
     * 获取 [ANALYTIC_ACCOUNT_ID_TEXT]
     */
    @JsonProperty("analytic_account_id_text")
    public String getAnalytic_account_id_text(){
        return analytic_account_id_text ;
    }

    /**
     * 设置 [ANALYTIC_ACCOUNT_ID_TEXT]
     */
    @JsonProperty("analytic_account_id_text")
    public void setAnalytic_account_id_text(String  analytic_account_id_text){
        this.analytic_account_id_text = analytic_account_id_text ;
        this.analytic_account_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [ANALYTIC_ACCOUNT_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getAnalytic_account_id_textDirtyFlag(){
        return analytic_account_id_textDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return create_uid_text ;
    }

    /**
     * 设置 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return create_uid_textDirtyFlag ;
    }

    /**
     * 获取 [CURRENCY_ID_TEXT]
     */
    @JsonProperty("currency_id_text")
    public String getCurrency_id_text(){
        return currency_id_text ;
    }

    /**
     * 设置 [CURRENCY_ID_TEXT]
     */
    @JsonProperty("currency_id_text")
    public void setCurrency_id_text(String  currency_id_text){
        this.currency_id_text = currency_id_text ;
        this.currency_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [CURRENCY_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getCurrency_id_textDirtyFlag(){
        return currency_id_textDirtyFlag ;
    }

    /**
     * 获取 [REF]
     */
    @JsonProperty("ref")
    public String getRef(){
        return ref ;
    }

    /**
     * 设置 [REF]
     */
    @JsonProperty("ref")
    public void setRef(String  ref){
        this.ref = ref ;
        this.refDirtyFlag = true ;
    }

    /**
     * 获取 [REF]脏标记
     */
    @JsonIgnore
    public boolean getRefDirtyFlag(){
        return refDirtyFlag ;
    }

    /**
     * 获取 [COMPANY_ID_TEXT]
     */
    @JsonProperty("company_id_text")
    public String getCompany_id_text(){
        return company_id_text ;
    }

    /**
     * 设置 [COMPANY_ID_TEXT]
     */
    @JsonProperty("company_id_text")
    public void setCompany_id_text(String  company_id_text){
        this.company_id_text = company_id_text ;
        this.company_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [COMPANY_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getCompany_id_textDirtyFlag(){
        return company_id_textDirtyFlag ;
    }

    /**
     * 获取 [STATEMENT_ID_TEXT]
     */
    @JsonProperty("statement_id_text")
    public String getStatement_id_text(){
        return statement_id_text ;
    }

    /**
     * 设置 [STATEMENT_ID_TEXT]
     */
    @JsonProperty("statement_id_text")
    public void setStatement_id_text(String  statement_id_text){
        this.statement_id_text = statement_id_text ;
        this.statement_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [STATEMENT_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getStatement_id_textDirtyFlag(){
        return statement_id_textDirtyFlag ;
    }

    /**
     * 获取 [STATEMENT_LINE_ID_TEXT]
     */
    @JsonProperty("statement_line_id_text")
    public String getStatement_line_id_text(){
        return statement_line_id_text ;
    }

    /**
     * 设置 [STATEMENT_LINE_ID_TEXT]
     */
    @JsonProperty("statement_line_id_text")
    public void setStatement_line_id_text(String  statement_line_id_text){
        this.statement_line_id_text = statement_line_id_text ;
        this.statement_line_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [STATEMENT_LINE_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getStatement_line_id_textDirtyFlag(){
        return statement_line_id_textDirtyFlag ;
    }

    /**
     * 获取 [PARTNER_ID_TEXT]
     */
    @JsonProperty("partner_id_text")
    public String getPartner_id_text(){
        return partner_id_text ;
    }

    /**
     * 设置 [PARTNER_ID_TEXT]
     */
    @JsonProperty("partner_id_text")
    public void setPartner_id_text(String  partner_id_text){
        this.partner_id_text = partner_id_text ;
        this.partner_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [PARTNER_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getPartner_id_textDirtyFlag(){
        return partner_id_textDirtyFlag ;
    }

    /**
     * 获取 [ACCOUNT_ID_TEXT]
     */
    @JsonProperty("account_id_text")
    public String getAccount_id_text(){
        return account_id_text ;
    }

    /**
     * 设置 [ACCOUNT_ID_TEXT]
     */
    @JsonProperty("account_id_text")
    public void setAccount_id_text(String  account_id_text){
        this.account_id_text = account_id_text ;
        this.account_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [ACCOUNT_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getAccount_id_textDirtyFlag(){
        return account_id_textDirtyFlag ;
    }

    /**
     * 获取 [DATE]
     */
    @JsonProperty("date")
    public Timestamp getDate(){
        return date ;
    }

    /**
     * 设置 [DATE]
     */
    @JsonProperty("date")
    public void setDate(Timestamp  date){
        this.date = date ;
        this.dateDirtyFlag = true ;
    }

    /**
     * 获取 [DATE]脏标记
     */
    @JsonIgnore
    public boolean getDateDirtyFlag(){
        return dateDirtyFlag ;
    }

    /**
     * 获取 [PRODUCT_UOM_ID_TEXT]
     */
    @JsonProperty("product_uom_id_text")
    public String getProduct_uom_id_text(){
        return product_uom_id_text ;
    }

    /**
     * 设置 [PRODUCT_UOM_ID_TEXT]
     */
    @JsonProperty("product_uom_id_text")
    public void setProduct_uom_id_text(String  product_uom_id_text){
        this.product_uom_id_text = product_uom_id_text ;
        this.product_uom_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [PRODUCT_UOM_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getProduct_uom_id_textDirtyFlag(){
        return product_uom_id_textDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return write_uid_text ;
    }

    /**
     * 设置 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return write_uid_textDirtyFlag ;
    }

    /**
     * 获取 [MOVE_ID]
     */
    @JsonProperty("move_id")
    public Integer getMove_id(){
        return move_id ;
    }

    /**
     * 设置 [MOVE_ID]
     */
    @JsonProperty("move_id")
    public void setMove_id(Integer  move_id){
        this.move_id = move_id ;
        this.move_idDirtyFlag = true ;
    }

    /**
     * 获取 [MOVE_ID]脏标记
     */
    @JsonIgnore
    public boolean getMove_idDirtyFlag(){
        return move_idDirtyFlag ;
    }

    /**
     * 获取 [CURRENCY_ID]
     */
    @JsonProperty("currency_id")
    public Integer getCurrency_id(){
        return currency_id ;
    }

    /**
     * 设置 [CURRENCY_ID]
     */
    @JsonProperty("currency_id")
    public void setCurrency_id(Integer  currency_id){
        this.currency_id = currency_id ;
        this.currency_idDirtyFlag = true ;
    }

    /**
     * 获取 [CURRENCY_ID]脏标记
     */
    @JsonIgnore
    public boolean getCurrency_idDirtyFlag(){
        return currency_idDirtyFlag ;
    }

    /**
     * 获取 [COMPANY_ID]
     */
    @JsonProperty("company_id")
    public Integer getCompany_id(){
        return company_id ;
    }

    /**
     * 设置 [COMPANY_ID]
     */
    @JsonProperty("company_id")
    public void setCompany_id(Integer  company_id){
        this.company_id = company_id ;
        this.company_idDirtyFlag = true ;
    }

    /**
     * 获取 [COMPANY_ID]脏标记
     */
    @JsonIgnore
    public boolean getCompany_idDirtyFlag(){
        return company_idDirtyFlag ;
    }

    /**
     * 获取 [PAYMENT_ID]
     */
    @JsonProperty("payment_id")
    public Integer getPayment_id(){
        return payment_id ;
    }

    /**
     * 设置 [PAYMENT_ID]
     */
    @JsonProperty("payment_id")
    public void setPayment_id(Integer  payment_id){
        this.payment_id = payment_id ;
        this.payment_idDirtyFlag = true ;
    }

    /**
     * 获取 [PAYMENT_ID]脏标记
     */
    @JsonIgnore
    public boolean getPayment_idDirtyFlag(){
        return payment_idDirtyFlag ;
    }

    /**
     * 获取 [COMPANY_CURRENCY_ID]
     */
    @JsonProperty("company_currency_id")
    public Integer getCompany_currency_id(){
        return company_currency_id ;
    }

    /**
     * 设置 [COMPANY_CURRENCY_ID]
     */
    @JsonProperty("company_currency_id")
    public void setCompany_currency_id(Integer  company_currency_id){
        this.company_currency_id = company_currency_id ;
        this.company_currency_idDirtyFlag = true ;
    }

    /**
     * 获取 [COMPANY_CURRENCY_ID]脏标记
     */
    @JsonIgnore
    public boolean getCompany_currency_idDirtyFlag(){
        return company_currency_idDirtyFlag ;
    }

    /**
     * 获取 [JOURNAL_ID]
     */
    @JsonProperty("journal_id")
    public Integer getJournal_id(){
        return journal_id ;
    }

    /**
     * 设置 [JOURNAL_ID]
     */
    @JsonProperty("journal_id")
    public void setJournal_id(Integer  journal_id){
        this.journal_id = journal_id ;
        this.journal_idDirtyFlag = true ;
    }

    /**
     * 获取 [JOURNAL_ID]脏标记
     */
    @JsonIgnore
    public boolean getJournal_idDirtyFlag(){
        return journal_idDirtyFlag ;
    }

    /**
     * 获取 [ANALYTIC_ACCOUNT_ID]
     */
    @JsonProperty("analytic_account_id")
    public Integer getAnalytic_account_id(){
        return analytic_account_id ;
    }

    /**
     * 设置 [ANALYTIC_ACCOUNT_ID]
     */
    @JsonProperty("analytic_account_id")
    public void setAnalytic_account_id(Integer  analytic_account_id){
        this.analytic_account_id = analytic_account_id ;
        this.analytic_account_idDirtyFlag = true ;
    }

    /**
     * 获取 [ANALYTIC_ACCOUNT_ID]脏标记
     */
    @JsonIgnore
    public boolean getAnalytic_account_idDirtyFlag(){
        return analytic_account_idDirtyFlag ;
    }

    /**
     * 获取 [INVOICE_ID]
     */
    @JsonProperty("invoice_id")
    public Integer getInvoice_id(){
        return invoice_id ;
    }

    /**
     * 设置 [INVOICE_ID]
     */
    @JsonProperty("invoice_id")
    public void setInvoice_id(Integer  invoice_id){
        this.invoice_id = invoice_id ;
        this.invoice_idDirtyFlag = true ;
    }

    /**
     * 获取 [INVOICE_ID]脏标记
     */
    @JsonIgnore
    public boolean getInvoice_idDirtyFlag(){
        return invoice_idDirtyFlag ;
    }

    /**
     * 获取 [TAX_LINE_ID]
     */
    @JsonProperty("tax_line_id")
    public Integer getTax_line_id(){
        return tax_line_id ;
    }

    /**
     * 设置 [TAX_LINE_ID]
     */
    @JsonProperty("tax_line_id")
    public void setTax_line_id(Integer  tax_line_id){
        this.tax_line_id = tax_line_id ;
        this.tax_line_idDirtyFlag = true ;
    }

    /**
     * 获取 [TAX_LINE_ID]脏标记
     */
    @JsonIgnore
    public boolean getTax_line_idDirtyFlag(){
        return tax_line_idDirtyFlag ;
    }

    /**
     * 获取 [FULL_RECONCILE_ID]
     */
    @JsonProperty("full_reconcile_id")
    public Integer getFull_reconcile_id(){
        return full_reconcile_id ;
    }

    /**
     * 设置 [FULL_RECONCILE_ID]
     */
    @JsonProperty("full_reconcile_id")
    public void setFull_reconcile_id(Integer  full_reconcile_id){
        this.full_reconcile_id = full_reconcile_id ;
        this.full_reconcile_idDirtyFlag = true ;
    }

    /**
     * 获取 [FULL_RECONCILE_ID]脏标记
     */
    @JsonIgnore
    public boolean getFull_reconcile_idDirtyFlag(){
        return full_reconcile_idDirtyFlag ;
    }

    /**
     * 获取 [STATEMENT_LINE_ID]
     */
    @JsonProperty("statement_line_id")
    public Integer getStatement_line_id(){
        return statement_line_id ;
    }

    /**
     * 设置 [STATEMENT_LINE_ID]
     */
    @JsonProperty("statement_line_id")
    public void setStatement_line_id(Integer  statement_line_id){
        this.statement_line_id = statement_line_id ;
        this.statement_line_idDirtyFlag = true ;
    }

    /**
     * 获取 [STATEMENT_LINE_ID]脏标记
     */
    @JsonIgnore
    public boolean getStatement_line_idDirtyFlag(){
        return statement_line_idDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return create_uid ;
    }

    /**
     * 设置 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return create_uidDirtyFlag ;
    }

    /**
     * 获取 [PRODUCT_ID]
     */
    @JsonProperty("product_id")
    public Integer getProduct_id(){
        return product_id ;
    }

    /**
     * 设置 [PRODUCT_ID]
     */
    @JsonProperty("product_id")
    public void setProduct_id(Integer  product_id){
        this.product_id = product_id ;
        this.product_idDirtyFlag = true ;
    }

    /**
     * 获取 [PRODUCT_ID]脏标记
     */
    @JsonIgnore
    public boolean getProduct_idDirtyFlag(){
        return product_idDirtyFlag ;
    }

    /**
     * 获取 [PRODUCT_UOM_ID]
     */
    @JsonProperty("product_uom_id")
    public Integer getProduct_uom_id(){
        return product_uom_id ;
    }

    /**
     * 设置 [PRODUCT_UOM_ID]
     */
    @JsonProperty("product_uom_id")
    public void setProduct_uom_id(Integer  product_uom_id){
        this.product_uom_id = product_uom_id ;
        this.product_uom_idDirtyFlag = true ;
    }

    /**
     * 获取 [PRODUCT_UOM_ID]脏标记
     */
    @JsonIgnore
    public boolean getProduct_uom_idDirtyFlag(){
        return product_uom_idDirtyFlag ;
    }

    /**
     * 获取 [PARTNER_ID]
     */
    @JsonProperty("partner_id")
    public Integer getPartner_id(){
        return partner_id ;
    }

    /**
     * 设置 [PARTNER_ID]
     */
    @JsonProperty("partner_id")
    public void setPartner_id(Integer  partner_id){
        this.partner_id = partner_id ;
        this.partner_idDirtyFlag = true ;
    }

    /**
     * 获取 [PARTNER_ID]脏标记
     */
    @JsonIgnore
    public boolean getPartner_idDirtyFlag(){
        return partner_idDirtyFlag ;
    }

    /**
     * 获取 [ACCOUNT_ID]
     */
    @JsonProperty("account_id")
    public Integer getAccount_id(){
        return account_id ;
    }

    /**
     * 设置 [ACCOUNT_ID]
     */
    @JsonProperty("account_id")
    public void setAccount_id(Integer  account_id){
        this.account_id = account_id ;
        this.account_idDirtyFlag = true ;
    }

    /**
     * 获取 [ACCOUNT_ID]脏标记
     */
    @JsonIgnore
    public boolean getAccount_idDirtyFlag(){
        return account_idDirtyFlag ;
    }

    /**
     * 获取 [STATEMENT_ID]
     */
    @JsonProperty("statement_id")
    public Integer getStatement_id(){
        return statement_id ;
    }

    /**
     * 设置 [STATEMENT_ID]
     */
    @JsonProperty("statement_id")
    public void setStatement_id(Integer  statement_id){
        this.statement_id = statement_id ;
        this.statement_idDirtyFlag = true ;
    }

    /**
     * 获取 [STATEMENT_ID]脏标记
     */
    @JsonIgnore
    public boolean getStatement_idDirtyFlag(){
        return statement_idDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return write_uid ;
    }

    /**
     * 设置 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return write_uidDirtyFlag ;
    }

    /**
     * 获取 [USER_TYPE_ID]
     */
    @JsonProperty("user_type_id")
    public Integer getUser_type_id(){
        return user_type_id ;
    }

    /**
     * 设置 [USER_TYPE_ID]
     */
    @JsonProperty("user_type_id")
    public void setUser_type_id(Integer  user_type_id){
        this.user_type_id = user_type_id ;
        this.user_type_idDirtyFlag = true ;
    }

    /**
     * 获取 [USER_TYPE_ID]脏标记
     */
    @JsonIgnore
    public boolean getUser_type_idDirtyFlag(){
        return user_type_idDirtyFlag ;
    }

    /**
     * 获取 [EXPENSE_ID]
     */
    @JsonProperty("expense_id")
    public Integer getExpense_id(){
        return expense_id ;
    }

    /**
     * 设置 [EXPENSE_ID]
     */
    @JsonProperty("expense_id")
    public void setExpense_id(Integer  expense_id){
        this.expense_id = expense_id ;
        this.expense_idDirtyFlag = true ;
    }

    /**
     * 获取 [EXPENSE_ID]脏标记
     */
    @JsonIgnore
    public boolean getExpense_idDirtyFlag(){
        return expense_idDirtyFlag ;
    }



    public Account_move_line toDO() {
        Account_move_line srfdomain = new Account_move_line();
        if(getAnalytic_tag_idsDirtyFlag())
            srfdomain.setAnalytic_tag_ids(analytic_tag_ids);
        if(getWrite_dateDirtyFlag())
            srfdomain.setWrite_date(write_date);
        if(getNameDirtyFlag())
            srfdomain.setName(name);
        if(getBalanceDirtyFlag())
            srfdomain.setBalance(balance);
        if(getTax_idsDirtyFlag())
            srfdomain.setTax_ids(tax_ids);
        if(getAmount_currencyDirtyFlag())
            srfdomain.setAmount_currency(amount_currency);
        if(getTax_line_grouping_keyDirtyFlag())
            srfdomain.setTax_line_grouping_key(tax_line_grouping_key);
        if(getIdDirtyFlag())
            srfdomain.setId(id);
        if(getReconciledDirtyFlag())
            srfdomain.setReconciled(reconciled);
        if(getDisplay_nameDirtyFlag())
            srfdomain.setDisplay_name(display_name);
        if(getBalance_cash_basisDirtyFlag())
            srfdomain.setBalance_cash_basis(balance_cash_basis);
        if(getCredit_cash_basisDirtyFlag())
            srfdomain.setCredit_cash_basis(credit_cash_basis);
        if(getQuantityDirtyFlag())
            srfdomain.setQuantity(quantity);
        if(getAmount_residualDirtyFlag())
            srfdomain.setAmount_residual(amount_residual);
        if(getRecompute_tax_lineDirtyFlag())
            srfdomain.setRecompute_tax_line(recompute_tax_line);
        if(getTax_exigibleDirtyFlag())
            srfdomain.setTax_exigible(tax_exigible);
        if(getAmount_residual_currencyDirtyFlag())
            srfdomain.setAmount_residual_currency(amount_residual_currency);
        if(getTax_base_amountDirtyFlag())
            srfdomain.setTax_base_amount(tax_base_amount);
        if(getParent_stateDirtyFlag())
            srfdomain.setParent_state(parent_state);
        if(getBlockedDirtyFlag())
            srfdomain.setBlocked(blocked);
        if(getCreate_dateDirtyFlag())
            srfdomain.setCreate_date(create_date);
        if(getMatched_credit_idsDirtyFlag())
            srfdomain.setMatched_credit_ids(matched_credit_ids);
        if(getAnalytic_line_idsDirtyFlag())
            srfdomain.setAnalytic_line_ids(analytic_line_ids);
        if(getDate_maturityDirtyFlag())
            srfdomain.setDate_maturity(date_maturity);
        if(getDebitDirtyFlag())
            srfdomain.setDebit(debit);
        if(get__last_updateDirtyFlag())
            srfdomain.set__last_update(__last_update);
        if(getCounterpartDirtyFlag())
            srfdomain.setCounterpart(counterpart);
        if(getMatched_debit_idsDirtyFlag())
            srfdomain.setMatched_debit_ids(matched_debit_ids);
        if(getDebit_cash_basisDirtyFlag())
            srfdomain.setDebit_cash_basis(debit_cash_basis);
        if(getCreditDirtyFlag())
            srfdomain.setCredit(credit);
        if(getNarrationDirtyFlag())
            srfdomain.setNarration(narration);
        if(getProduct_id_textDirtyFlag())
            srfdomain.setProduct_id_text(product_id_text);
        if(getUser_type_id_textDirtyFlag())
            srfdomain.setUser_type_id_text(user_type_id_text);
        if(getPayment_id_textDirtyFlag())
            srfdomain.setPayment_id_text(payment_id_text);
        if(getTax_line_id_textDirtyFlag())
            srfdomain.setTax_line_id_text(tax_line_id_text);
        if(getFull_reconcile_id_textDirtyFlag())
            srfdomain.setFull_reconcile_id_text(full_reconcile_id_text);
        if(getExpense_id_textDirtyFlag())
            srfdomain.setExpense_id_text(expense_id_text);
        if(getMove_id_textDirtyFlag())
            srfdomain.setMove_id_text(move_id_text);
        if(getJournal_id_textDirtyFlag())
            srfdomain.setJournal_id_text(journal_id_text);
        if(getCompany_currency_id_textDirtyFlag())
            srfdomain.setCompany_currency_id_text(company_currency_id_text);
        if(getInvoice_id_textDirtyFlag())
            srfdomain.setInvoice_id_text(invoice_id_text);
        if(getAnalytic_account_id_textDirtyFlag())
            srfdomain.setAnalytic_account_id_text(analytic_account_id_text);
        if(getCreate_uid_textDirtyFlag())
            srfdomain.setCreate_uid_text(create_uid_text);
        if(getCurrency_id_textDirtyFlag())
            srfdomain.setCurrency_id_text(currency_id_text);
        if(getRefDirtyFlag())
            srfdomain.setRef(ref);
        if(getCompany_id_textDirtyFlag())
            srfdomain.setCompany_id_text(company_id_text);
        if(getStatement_id_textDirtyFlag())
            srfdomain.setStatement_id_text(statement_id_text);
        if(getStatement_line_id_textDirtyFlag())
            srfdomain.setStatement_line_id_text(statement_line_id_text);
        if(getPartner_id_textDirtyFlag())
            srfdomain.setPartner_id_text(partner_id_text);
        if(getAccount_id_textDirtyFlag())
            srfdomain.setAccount_id_text(account_id_text);
        if(getDateDirtyFlag())
            srfdomain.setDate(date);
        if(getProduct_uom_id_textDirtyFlag())
            srfdomain.setProduct_uom_id_text(product_uom_id_text);
        if(getWrite_uid_textDirtyFlag())
            srfdomain.setWrite_uid_text(write_uid_text);
        if(getMove_idDirtyFlag())
            srfdomain.setMove_id(move_id);
        if(getCurrency_idDirtyFlag())
            srfdomain.setCurrency_id(currency_id);
        if(getCompany_idDirtyFlag())
            srfdomain.setCompany_id(company_id);
        if(getPayment_idDirtyFlag())
            srfdomain.setPayment_id(payment_id);
        if(getCompany_currency_idDirtyFlag())
            srfdomain.setCompany_currency_id(company_currency_id);
        if(getJournal_idDirtyFlag())
            srfdomain.setJournal_id(journal_id);
        if(getAnalytic_account_idDirtyFlag())
            srfdomain.setAnalytic_account_id(analytic_account_id);
        if(getInvoice_idDirtyFlag())
            srfdomain.setInvoice_id(invoice_id);
        if(getTax_line_idDirtyFlag())
            srfdomain.setTax_line_id(tax_line_id);
        if(getFull_reconcile_idDirtyFlag())
            srfdomain.setFull_reconcile_id(full_reconcile_id);
        if(getStatement_line_idDirtyFlag())
            srfdomain.setStatement_line_id(statement_line_id);
        if(getCreate_uidDirtyFlag())
            srfdomain.setCreate_uid(create_uid);
        if(getProduct_idDirtyFlag())
            srfdomain.setProduct_id(product_id);
        if(getProduct_uom_idDirtyFlag())
            srfdomain.setProduct_uom_id(product_uom_id);
        if(getPartner_idDirtyFlag())
            srfdomain.setPartner_id(partner_id);
        if(getAccount_idDirtyFlag())
            srfdomain.setAccount_id(account_id);
        if(getStatement_idDirtyFlag())
            srfdomain.setStatement_id(statement_id);
        if(getWrite_uidDirtyFlag())
            srfdomain.setWrite_uid(write_uid);
        if(getUser_type_idDirtyFlag())
            srfdomain.setUser_type_id(user_type_id);
        if(getExpense_idDirtyFlag())
            srfdomain.setExpense_id(expense_id);

        return srfdomain;
    }

    public void fromDO(Account_move_line srfdomain) {
        if(srfdomain == null )
          return ;
        if(srfdomain.getAnalytic_tag_idsDirtyFlag())
            this.setAnalytic_tag_ids(srfdomain.getAnalytic_tag_ids());
        if(srfdomain.getWrite_dateDirtyFlag())
            this.setWrite_date(srfdomain.getWrite_date());
        if(srfdomain.getNameDirtyFlag())
            this.setName(srfdomain.getName());
        if(srfdomain.getBalanceDirtyFlag())
            this.setBalance(srfdomain.getBalance());
        if(srfdomain.getTax_idsDirtyFlag())
            this.setTax_ids(srfdomain.getTax_ids());
        if(srfdomain.getAmount_currencyDirtyFlag())
            this.setAmount_currency(srfdomain.getAmount_currency());
        if(srfdomain.getTax_line_grouping_keyDirtyFlag())
            this.setTax_line_grouping_key(srfdomain.getTax_line_grouping_key());
        if(srfdomain.getIdDirtyFlag())
            this.setId(srfdomain.getId());
        if(srfdomain.getReconciledDirtyFlag())
            this.setReconciled(srfdomain.getReconciled());
        if(srfdomain.getDisplay_nameDirtyFlag())
            this.setDisplay_name(srfdomain.getDisplay_name());
        if(srfdomain.getBalance_cash_basisDirtyFlag())
            this.setBalance_cash_basis(srfdomain.getBalance_cash_basis());
        if(srfdomain.getCredit_cash_basisDirtyFlag())
            this.setCredit_cash_basis(srfdomain.getCredit_cash_basis());
        if(srfdomain.getQuantityDirtyFlag())
            this.setQuantity(srfdomain.getQuantity());
        if(srfdomain.getAmount_residualDirtyFlag())
            this.setAmount_residual(srfdomain.getAmount_residual());
        if(srfdomain.getRecompute_tax_lineDirtyFlag())
            this.setRecompute_tax_line(srfdomain.getRecompute_tax_line());
        if(srfdomain.getTax_exigibleDirtyFlag())
            this.setTax_exigible(srfdomain.getTax_exigible());
        if(srfdomain.getAmount_residual_currencyDirtyFlag())
            this.setAmount_residual_currency(srfdomain.getAmount_residual_currency());
        if(srfdomain.getTax_base_amountDirtyFlag())
            this.setTax_base_amount(srfdomain.getTax_base_amount());
        if(srfdomain.getParent_stateDirtyFlag())
            this.setParent_state(srfdomain.getParent_state());
        if(srfdomain.getBlockedDirtyFlag())
            this.setBlocked(srfdomain.getBlocked());
        if(srfdomain.getCreate_dateDirtyFlag())
            this.setCreate_date(srfdomain.getCreate_date());
        if(srfdomain.getMatched_credit_idsDirtyFlag())
            this.setMatched_credit_ids(srfdomain.getMatched_credit_ids());
        if(srfdomain.getAnalytic_line_idsDirtyFlag())
            this.setAnalytic_line_ids(srfdomain.getAnalytic_line_ids());
        if(srfdomain.getDate_maturityDirtyFlag())
            this.setDate_maturity(srfdomain.getDate_maturity());
        if(srfdomain.getDebitDirtyFlag())
            this.setDebit(srfdomain.getDebit());
        if(srfdomain.get__last_updateDirtyFlag())
            this.set__last_update(srfdomain.get__last_update());
        if(srfdomain.getCounterpartDirtyFlag())
            this.setCounterpart(srfdomain.getCounterpart());
        if(srfdomain.getMatched_debit_idsDirtyFlag())
            this.setMatched_debit_ids(srfdomain.getMatched_debit_ids());
        if(srfdomain.getDebit_cash_basisDirtyFlag())
            this.setDebit_cash_basis(srfdomain.getDebit_cash_basis());
        if(srfdomain.getCreditDirtyFlag())
            this.setCredit(srfdomain.getCredit());
        if(srfdomain.getNarrationDirtyFlag())
            this.setNarration(srfdomain.getNarration());
        if(srfdomain.getProduct_id_textDirtyFlag())
            this.setProduct_id_text(srfdomain.getProduct_id_text());
        if(srfdomain.getUser_type_id_textDirtyFlag())
            this.setUser_type_id_text(srfdomain.getUser_type_id_text());
        if(srfdomain.getPayment_id_textDirtyFlag())
            this.setPayment_id_text(srfdomain.getPayment_id_text());
        if(srfdomain.getTax_line_id_textDirtyFlag())
            this.setTax_line_id_text(srfdomain.getTax_line_id_text());
        if(srfdomain.getFull_reconcile_id_textDirtyFlag())
            this.setFull_reconcile_id_text(srfdomain.getFull_reconcile_id_text());
        if(srfdomain.getExpense_id_textDirtyFlag())
            this.setExpense_id_text(srfdomain.getExpense_id_text());
        if(srfdomain.getMove_id_textDirtyFlag())
            this.setMove_id_text(srfdomain.getMove_id_text());
        if(srfdomain.getJournal_id_textDirtyFlag())
            this.setJournal_id_text(srfdomain.getJournal_id_text());
        if(srfdomain.getCompany_currency_id_textDirtyFlag())
            this.setCompany_currency_id_text(srfdomain.getCompany_currency_id_text());
        if(srfdomain.getInvoice_id_textDirtyFlag())
            this.setInvoice_id_text(srfdomain.getInvoice_id_text());
        if(srfdomain.getAnalytic_account_id_textDirtyFlag())
            this.setAnalytic_account_id_text(srfdomain.getAnalytic_account_id_text());
        if(srfdomain.getCreate_uid_textDirtyFlag())
            this.setCreate_uid_text(srfdomain.getCreate_uid_text());
        if(srfdomain.getCurrency_id_textDirtyFlag())
            this.setCurrency_id_text(srfdomain.getCurrency_id_text());
        if(srfdomain.getRefDirtyFlag())
            this.setRef(srfdomain.getRef());
        if(srfdomain.getCompany_id_textDirtyFlag())
            this.setCompany_id_text(srfdomain.getCompany_id_text());
        if(srfdomain.getStatement_id_textDirtyFlag())
            this.setStatement_id_text(srfdomain.getStatement_id_text());
        if(srfdomain.getStatement_line_id_textDirtyFlag())
            this.setStatement_line_id_text(srfdomain.getStatement_line_id_text());
        if(srfdomain.getPartner_id_textDirtyFlag())
            this.setPartner_id_text(srfdomain.getPartner_id_text());
        if(srfdomain.getAccount_id_textDirtyFlag())
            this.setAccount_id_text(srfdomain.getAccount_id_text());
        if(srfdomain.getDateDirtyFlag())
            this.setDate(srfdomain.getDate());
        if(srfdomain.getProduct_uom_id_textDirtyFlag())
            this.setProduct_uom_id_text(srfdomain.getProduct_uom_id_text());
        if(srfdomain.getWrite_uid_textDirtyFlag())
            this.setWrite_uid_text(srfdomain.getWrite_uid_text());
        if(srfdomain.getMove_idDirtyFlag())
            this.setMove_id(srfdomain.getMove_id());
        if(srfdomain.getCurrency_idDirtyFlag())
            this.setCurrency_id(srfdomain.getCurrency_id());
        if(srfdomain.getCompany_idDirtyFlag())
            this.setCompany_id(srfdomain.getCompany_id());
        if(srfdomain.getPayment_idDirtyFlag())
            this.setPayment_id(srfdomain.getPayment_id());
        if(srfdomain.getCompany_currency_idDirtyFlag())
            this.setCompany_currency_id(srfdomain.getCompany_currency_id());
        if(srfdomain.getJournal_idDirtyFlag())
            this.setJournal_id(srfdomain.getJournal_id());
        if(srfdomain.getAnalytic_account_idDirtyFlag())
            this.setAnalytic_account_id(srfdomain.getAnalytic_account_id());
        if(srfdomain.getInvoice_idDirtyFlag())
            this.setInvoice_id(srfdomain.getInvoice_id());
        if(srfdomain.getTax_line_idDirtyFlag())
            this.setTax_line_id(srfdomain.getTax_line_id());
        if(srfdomain.getFull_reconcile_idDirtyFlag())
            this.setFull_reconcile_id(srfdomain.getFull_reconcile_id());
        if(srfdomain.getStatement_line_idDirtyFlag())
            this.setStatement_line_id(srfdomain.getStatement_line_id());
        if(srfdomain.getCreate_uidDirtyFlag())
            this.setCreate_uid(srfdomain.getCreate_uid());
        if(srfdomain.getProduct_idDirtyFlag())
            this.setProduct_id(srfdomain.getProduct_id());
        if(srfdomain.getProduct_uom_idDirtyFlag())
            this.setProduct_uom_id(srfdomain.getProduct_uom_id());
        if(srfdomain.getPartner_idDirtyFlag())
            this.setPartner_id(srfdomain.getPartner_id());
        if(srfdomain.getAccount_idDirtyFlag())
            this.setAccount_id(srfdomain.getAccount_id());
        if(srfdomain.getStatement_idDirtyFlag())
            this.setStatement_id(srfdomain.getStatement_id());
        if(srfdomain.getWrite_uidDirtyFlag())
            this.setWrite_uid(srfdomain.getWrite_uid());
        if(srfdomain.getUser_type_idDirtyFlag())
            this.setUser_type_id(srfdomain.getUser_type_id());
        if(srfdomain.getExpense_idDirtyFlag())
            this.setExpense_id(srfdomain.getExpense_id());

    }

    public List<Account_move_lineDTO> fromDOPage(List<Account_move_line> poPage)   {
        if(poPage == null)
            return null;
        List<Account_move_lineDTO> dtos=new ArrayList<Account_move_lineDTO>();
        for(Account_move_line domain : poPage) {
            Account_move_lineDTO dto = new Account_move_lineDTO();
            dto.fromDO(domain);
            dtos.add(dto);
        }
        return dtos;
    }
}

