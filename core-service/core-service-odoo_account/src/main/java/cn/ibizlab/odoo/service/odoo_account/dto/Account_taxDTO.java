package cn.ibizlab.odoo.service.odoo_account.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.core.odoo_account.valuerule.anno.account_tax.*;
import cn.ibizlab.odoo.core.odoo_account.domain.Account_tax;
import org.springframework.cglib.beans.BeanCopier;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 服务DTO对象[Account_taxDTO]
 */
public class Account_taxDTO implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [ID]
     *
     */
    @Account_taxIdDefault(info = "默认规则")
    private Integer id;

    @JsonIgnore
    private boolean idDirtyFlag;

    /**
     * 属性 [CREATE_DATE]
     *
     */
    @Account_taxCreate_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp create_date;

    @JsonIgnore
    private boolean create_dateDirtyFlag;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @Account_taxDisplay_nameDefault(info = "默认规则")
    private String display_name;

    @JsonIgnore
    private boolean display_nameDirtyFlag;

    /**
     * 属性 [TYPE_TAX_USE]
     *
     */
    @Account_taxType_tax_useDefault(info = "默认规则")
    private String type_tax_use;

    @JsonIgnore
    private boolean type_tax_useDirtyFlag;

    /**
     * 属性 [ACTIVE]
     *
     */
    @Account_taxActiveDefault(info = "默认规则")
    private String active;

    @JsonIgnore
    private boolean activeDirtyFlag;

    /**
     * 属性 [CHILDREN_TAX_IDS]
     *
     */
    @Account_taxChildren_tax_idsDefault(info = "默认规则")
    private String children_tax_ids;

    @JsonIgnore
    private boolean children_tax_idsDirtyFlag;

    /**
     * 属性 [ANALYTIC]
     *
     */
    @Account_taxAnalyticDefault(info = "默认规则")
    private String analytic;

    @JsonIgnore
    private boolean analyticDirtyFlag;

    /**
     * 属性 [PRICE_INCLUDE]
     *
     */
    @Account_taxPrice_includeDefault(info = "默认规则")
    private String price_include;

    @JsonIgnore
    private boolean price_includeDirtyFlag;

    /**
     * 属性 [DESCRIPTION]
     *
     */
    @Account_taxDescriptionDefault(info = "默认规则")
    private String description;

    @JsonIgnore
    private boolean descriptionDirtyFlag;

    /**
     * 属性 [WRITE_DATE]
     *
     */
    @Account_taxWrite_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp write_date;

    @JsonIgnore
    private boolean write_dateDirtyFlag;

    /**
     * 属性 [TAX_EXIGIBILITY]
     *
     */
    @Account_taxTax_exigibilityDefault(info = "默认规则")
    private String tax_exigibility;

    @JsonIgnore
    private boolean tax_exigibilityDirtyFlag;

    /**
     * 属性 [NAME]
     *
     */
    @Account_taxNameDefault(info = "默认规则")
    private String name;

    @JsonIgnore
    private boolean nameDirtyFlag;

    /**
     * 属性 [TAG_IDS]
     *
     */
    @Account_taxTag_idsDefault(info = "默认规则")
    private String tag_ids;

    @JsonIgnore
    private boolean tag_idsDirtyFlag;

    /**
     * 属性 [AMOUNT_TYPE]
     *
     */
    @Account_taxAmount_typeDefault(info = "默认规则")
    private String amount_type;

    @JsonIgnore
    private boolean amount_typeDirtyFlag;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @Account_tax__last_updateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp __last_update;

    @JsonIgnore
    private boolean __last_updateDirtyFlag;

    /**
     * 属性 [AMOUNT]
     *
     */
    @Account_taxAmountDefault(info = "默认规则")
    private Double amount;

    @JsonIgnore
    private boolean amountDirtyFlag;

    /**
     * 属性 [SEQUENCE]
     *
     */
    @Account_taxSequenceDefault(info = "默认规则")
    private Integer sequence;

    @JsonIgnore
    private boolean sequenceDirtyFlag;

    /**
     * 属性 [INCLUDE_BASE_AMOUNT]
     *
     */
    @Account_taxInclude_base_amountDefault(info = "默认规则")
    private String include_base_amount;

    @JsonIgnore
    private boolean include_base_amountDirtyFlag;

    /**
     * 属性 [CASH_BASIS_BASE_ACCOUNT_ID_TEXT]
     *
     */
    @Account_taxCash_basis_base_account_id_textDefault(info = "默认规则")
    private String cash_basis_base_account_id_text;

    @JsonIgnore
    private boolean cash_basis_base_account_id_textDirtyFlag;

    /**
     * 属性 [CASH_BASIS_ACCOUNT_ID_TEXT]
     *
     */
    @Account_taxCash_basis_account_id_textDefault(info = "默认规则")
    private String cash_basis_account_id_text;

    @JsonIgnore
    private boolean cash_basis_account_id_textDirtyFlag;

    /**
     * 属性 [HIDE_TAX_EXIGIBILITY]
     *
     */
    @Account_taxHide_tax_exigibilityDefault(info = "默认规则")
    private String hide_tax_exigibility;

    @JsonIgnore
    private boolean hide_tax_exigibilityDirtyFlag;

    /**
     * 属性 [TAX_GROUP_ID_TEXT]
     *
     */
    @Account_taxTax_group_id_textDefault(info = "默认规则")
    private String tax_group_id_text;

    @JsonIgnore
    private boolean tax_group_id_textDirtyFlag;

    /**
     * 属性 [COMPANY_ID_TEXT]
     *
     */
    @Account_taxCompany_id_textDefault(info = "默认规则")
    private String company_id_text;

    @JsonIgnore
    private boolean company_id_textDirtyFlag;

    /**
     * 属性 [REFUND_ACCOUNT_ID_TEXT]
     *
     */
    @Account_taxRefund_account_id_textDefault(info = "默认规则")
    private String refund_account_id_text;

    @JsonIgnore
    private boolean refund_account_id_textDirtyFlag;

    /**
     * 属性 [ACCOUNT_ID_TEXT]
     *
     */
    @Account_taxAccount_id_textDefault(info = "默认规则")
    private String account_id_text;

    @JsonIgnore
    private boolean account_id_textDirtyFlag;

    /**
     * 属性 [WRITE_UID_TEXT]
     *
     */
    @Account_taxWrite_uid_textDefault(info = "默认规则")
    private String write_uid_text;

    @JsonIgnore
    private boolean write_uid_textDirtyFlag;

    /**
     * 属性 [CREATE_UID_TEXT]
     *
     */
    @Account_taxCreate_uid_textDefault(info = "默认规则")
    private String create_uid_text;

    @JsonIgnore
    private boolean create_uid_textDirtyFlag;

    /**
     * 属性 [REFUND_ACCOUNT_ID]
     *
     */
    @Account_taxRefund_account_idDefault(info = "默认规则")
    private Integer refund_account_id;

    @JsonIgnore
    private boolean refund_account_idDirtyFlag;

    /**
     * 属性 [CASH_BASIS_ACCOUNT_ID]
     *
     */
    @Account_taxCash_basis_account_idDefault(info = "默认规则")
    private Integer cash_basis_account_id;

    @JsonIgnore
    private boolean cash_basis_account_idDirtyFlag;

    /**
     * 属性 [CREATE_UID]
     *
     */
    @Account_taxCreate_uidDefault(info = "默认规则")
    private Integer create_uid;

    @JsonIgnore
    private boolean create_uidDirtyFlag;

    /**
     * 属性 [WRITE_UID]
     *
     */
    @Account_taxWrite_uidDefault(info = "默认规则")
    private Integer write_uid;

    @JsonIgnore
    private boolean write_uidDirtyFlag;

    /**
     * 属性 [ACCOUNT_ID]
     *
     */
    @Account_taxAccount_idDefault(info = "默认规则")
    private Integer account_id;

    @JsonIgnore
    private boolean account_idDirtyFlag;

    /**
     * 属性 [CASH_BASIS_BASE_ACCOUNT_ID]
     *
     */
    @Account_taxCash_basis_base_account_idDefault(info = "默认规则")
    private Integer cash_basis_base_account_id;

    @JsonIgnore
    private boolean cash_basis_base_account_idDirtyFlag;

    /**
     * 属性 [TAX_GROUP_ID]
     *
     */
    @Account_taxTax_group_idDefault(info = "默认规则")
    private Integer tax_group_id;

    @JsonIgnore
    private boolean tax_group_idDirtyFlag;

    /**
     * 属性 [COMPANY_ID]
     *
     */
    @Account_taxCompany_idDefault(info = "默认规则")
    private Integer company_id;

    @JsonIgnore
    private boolean company_idDirtyFlag;


    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

    /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return idDirtyFlag ;
    }

    /**
     * 获取 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return create_date ;
    }

    /**
     * 设置 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return create_dateDirtyFlag ;
    }

    /**
     * 获取 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return display_name ;
    }

    /**
     * 设置 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

    /**
     * 获取 [DISPLAY_NAME]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return display_nameDirtyFlag ;
    }

    /**
     * 获取 [TYPE_TAX_USE]
     */
    @JsonProperty("type_tax_use")
    public String getType_tax_use(){
        return type_tax_use ;
    }

    /**
     * 设置 [TYPE_TAX_USE]
     */
    @JsonProperty("type_tax_use")
    public void setType_tax_use(String  type_tax_use){
        this.type_tax_use = type_tax_use ;
        this.type_tax_useDirtyFlag = true ;
    }

    /**
     * 获取 [TYPE_TAX_USE]脏标记
     */
    @JsonIgnore
    public boolean getType_tax_useDirtyFlag(){
        return type_tax_useDirtyFlag ;
    }

    /**
     * 获取 [ACTIVE]
     */
    @JsonProperty("active")
    public String getActive(){
        return active ;
    }

    /**
     * 设置 [ACTIVE]
     */
    @JsonProperty("active")
    public void setActive(String  active){
        this.active = active ;
        this.activeDirtyFlag = true ;
    }

    /**
     * 获取 [ACTIVE]脏标记
     */
    @JsonIgnore
    public boolean getActiveDirtyFlag(){
        return activeDirtyFlag ;
    }

    /**
     * 获取 [CHILDREN_TAX_IDS]
     */
    @JsonProperty("children_tax_ids")
    public String getChildren_tax_ids(){
        return children_tax_ids ;
    }

    /**
     * 设置 [CHILDREN_TAX_IDS]
     */
    @JsonProperty("children_tax_ids")
    public void setChildren_tax_ids(String  children_tax_ids){
        this.children_tax_ids = children_tax_ids ;
        this.children_tax_idsDirtyFlag = true ;
    }

    /**
     * 获取 [CHILDREN_TAX_IDS]脏标记
     */
    @JsonIgnore
    public boolean getChildren_tax_idsDirtyFlag(){
        return children_tax_idsDirtyFlag ;
    }

    /**
     * 获取 [ANALYTIC]
     */
    @JsonProperty("analytic")
    public String getAnalytic(){
        return analytic ;
    }

    /**
     * 设置 [ANALYTIC]
     */
    @JsonProperty("analytic")
    public void setAnalytic(String  analytic){
        this.analytic = analytic ;
        this.analyticDirtyFlag = true ;
    }

    /**
     * 获取 [ANALYTIC]脏标记
     */
    @JsonIgnore
    public boolean getAnalyticDirtyFlag(){
        return analyticDirtyFlag ;
    }

    /**
     * 获取 [PRICE_INCLUDE]
     */
    @JsonProperty("price_include")
    public String getPrice_include(){
        return price_include ;
    }

    /**
     * 设置 [PRICE_INCLUDE]
     */
    @JsonProperty("price_include")
    public void setPrice_include(String  price_include){
        this.price_include = price_include ;
        this.price_includeDirtyFlag = true ;
    }

    /**
     * 获取 [PRICE_INCLUDE]脏标记
     */
    @JsonIgnore
    public boolean getPrice_includeDirtyFlag(){
        return price_includeDirtyFlag ;
    }

    /**
     * 获取 [DESCRIPTION]
     */
    @JsonProperty("description")
    public String getDescription(){
        return description ;
    }

    /**
     * 设置 [DESCRIPTION]
     */
    @JsonProperty("description")
    public void setDescription(String  description){
        this.description = description ;
        this.descriptionDirtyFlag = true ;
    }

    /**
     * 获取 [DESCRIPTION]脏标记
     */
    @JsonIgnore
    public boolean getDescriptionDirtyFlag(){
        return descriptionDirtyFlag ;
    }

    /**
     * 获取 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return write_date ;
    }

    /**
     * 设置 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return write_dateDirtyFlag ;
    }

    /**
     * 获取 [TAX_EXIGIBILITY]
     */
    @JsonProperty("tax_exigibility")
    public String getTax_exigibility(){
        return tax_exigibility ;
    }

    /**
     * 设置 [TAX_EXIGIBILITY]
     */
    @JsonProperty("tax_exigibility")
    public void setTax_exigibility(String  tax_exigibility){
        this.tax_exigibility = tax_exigibility ;
        this.tax_exigibilityDirtyFlag = true ;
    }

    /**
     * 获取 [TAX_EXIGIBILITY]脏标记
     */
    @JsonIgnore
    public boolean getTax_exigibilityDirtyFlag(){
        return tax_exigibilityDirtyFlag ;
    }

    /**
     * 获取 [NAME]
     */
    @JsonProperty("name")
    public String getName(){
        return name ;
    }

    /**
     * 设置 [NAME]
     */
    @JsonProperty("name")
    public void setName(String  name){
        this.name = name ;
        this.nameDirtyFlag = true ;
    }

    /**
     * 获取 [NAME]脏标记
     */
    @JsonIgnore
    public boolean getNameDirtyFlag(){
        return nameDirtyFlag ;
    }

    /**
     * 获取 [TAG_IDS]
     */
    @JsonProperty("tag_ids")
    public String getTag_ids(){
        return tag_ids ;
    }

    /**
     * 设置 [TAG_IDS]
     */
    @JsonProperty("tag_ids")
    public void setTag_ids(String  tag_ids){
        this.tag_ids = tag_ids ;
        this.tag_idsDirtyFlag = true ;
    }

    /**
     * 获取 [TAG_IDS]脏标记
     */
    @JsonIgnore
    public boolean getTag_idsDirtyFlag(){
        return tag_idsDirtyFlag ;
    }

    /**
     * 获取 [AMOUNT_TYPE]
     */
    @JsonProperty("amount_type")
    public String getAmount_type(){
        return amount_type ;
    }

    /**
     * 设置 [AMOUNT_TYPE]
     */
    @JsonProperty("amount_type")
    public void setAmount_type(String  amount_type){
        this.amount_type = amount_type ;
        this.amount_typeDirtyFlag = true ;
    }

    /**
     * 获取 [AMOUNT_TYPE]脏标记
     */
    @JsonIgnore
    public boolean getAmount_typeDirtyFlag(){
        return amount_typeDirtyFlag ;
    }

    /**
     * 获取 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return __last_update ;
    }

    /**
     * 设置 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

    /**
     * 获取 [__LAST_UPDATE]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return __last_updateDirtyFlag ;
    }

    /**
     * 获取 [AMOUNT]
     */
    @JsonProperty("amount")
    public Double getAmount(){
        return amount ;
    }

    /**
     * 设置 [AMOUNT]
     */
    @JsonProperty("amount")
    public void setAmount(Double  amount){
        this.amount = amount ;
        this.amountDirtyFlag = true ;
    }

    /**
     * 获取 [AMOUNT]脏标记
     */
    @JsonIgnore
    public boolean getAmountDirtyFlag(){
        return amountDirtyFlag ;
    }

    /**
     * 获取 [SEQUENCE]
     */
    @JsonProperty("sequence")
    public Integer getSequence(){
        return sequence ;
    }

    /**
     * 设置 [SEQUENCE]
     */
    @JsonProperty("sequence")
    public void setSequence(Integer  sequence){
        this.sequence = sequence ;
        this.sequenceDirtyFlag = true ;
    }

    /**
     * 获取 [SEQUENCE]脏标记
     */
    @JsonIgnore
    public boolean getSequenceDirtyFlag(){
        return sequenceDirtyFlag ;
    }

    /**
     * 获取 [INCLUDE_BASE_AMOUNT]
     */
    @JsonProperty("include_base_amount")
    public String getInclude_base_amount(){
        return include_base_amount ;
    }

    /**
     * 设置 [INCLUDE_BASE_AMOUNT]
     */
    @JsonProperty("include_base_amount")
    public void setInclude_base_amount(String  include_base_amount){
        this.include_base_amount = include_base_amount ;
        this.include_base_amountDirtyFlag = true ;
    }

    /**
     * 获取 [INCLUDE_BASE_AMOUNT]脏标记
     */
    @JsonIgnore
    public boolean getInclude_base_amountDirtyFlag(){
        return include_base_amountDirtyFlag ;
    }

    /**
     * 获取 [CASH_BASIS_BASE_ACCOUNT_ID_TEXT]
     */
    @JsonProperty("cash_basis_base_account_id_text")
    public String getCash_basis_base_account_id_text(){
        return cash_basis_base_account_id_text ;
    }

    /**
     * 设置 [CASH_BASIS_BASE_ACCOUNT_ID_TEXT]
     */
    @JsonProperty("cash_basis_base_account_id_text")
    public void setCash_basis_base_account_id_text(String  cash_basis_base_account_id_text){
        this.cash_basis_base_account_id_text = cash_basis_base_account_id_text ;
        this.cash_basis_base_account_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [CASH_BASIS_BASE_ACCOUNT_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getCash_basis_base_account_id_textDirtyFlag(){
        return cash_basis_base_account_id_textDirtyFlag ;
    }

    /**
     * 获取 [CASH_BASIS_ACCOUNT_ID_TEXT]
     */
    @JsonProperty("cash_basis_account_id_text")
    public String getCash_basis_account_id_text(){
        return cash_basis_account_id_text ;
    }

    /**
     * 设置 [CASH_BASIS_ACCOUNT_ID_TEXT]
     */
    @JsonProperty("cash_basis_account_id_text")
    public void setCash_basis_account_id_text(String  cash_basis_account_id_text){
        this.cash_basis_account_id_text = cash_basis_account_id_text ;
        this.cash_basis_account_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [CASH_BASIS_ACCOUNT_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getCash_basis_account_id_textDirtyFlag(){
        return cash_basis_account_id_textDirtyFlag ;
    }

    /**
     * 获取 [HIDE_TAX_EXIGIBILITY]
     */
    @JsonProperty("hide_tax_exigibility")
    public String getHide_tax_exigibility(){
        return hide_tax_exigibility ;
    }

    /**
     * 设置 [HIDE_TAX_EXIGIBILITY]
     */
    @JsonProperty("hide_tax_exigibility")
    public void setHide_tax_exigibility(String  hide_tax_exigibility){
        this.hide_tax_exigibility = hide_tax_exigibility ;
        this.hide_tax_exigibilityDirtyFlag = true ;
    }

    /**
     * 获取 [HIDE_TAX_EXIGIBILITY]脏标记
     */
    @JsonIgnore
    public boolean getHide_tax_exigibilityDirtyFlag(){
        return hide_tax_exigibilityDirtyFlag ;
    }

    /**
     * 获取 [TAX_GROUP_ID_TEXT]
     */
    @JsonProperty("tax_group_id_text")
    public String getTax_group_id_text(){
        return tax_group_id_text ;
    }

    /**
     * 设置 [TAX_GROUP_ID_TEXT]
     */
    @JsonProperty("tax_group_id_text")
    public void setTax_group_id_text(String  tax_group_id_text){
        this.tax_group_id_text = tax_group_id_text ;
        this.tax_group_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [TAX_GROUP_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getTax_group_id_textDirtyFlag(){
        return tax_group_id_textDirtyFlag ;
    }

    /**
     * 获取 [COMPANY_ID_TEXT]
     */
    @JsonProperty("company_id_text")
    public String getCompany_id_text(){
        return company_id_text ;
    }

    /**
     * 设置 [COMPANY_ID_TEXT]
     */
    @JsonProperty("company_id_text")
    public void setCompany_id_text(String  company_id_text){
        this.company_id_text = company_id_text ;
        this.company_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [COMPANY_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getCompany_id_textDirtyFlag(){
        return company_id_textDirtyFlag ;
    }

    /**
     * 获取 [REFUND_ACCOUNT_ID_TEXT]
     */
    @JsonProperty("refund_account_id_text")
    public String getRefund_account_id_text(){
        return refund_account_id_text ;
    }

    /**
     * 设置 [REFUND_ACCOUNT_ID_TEXT]
     */
    @JsonProperty("refund_account_id_text")
    public void setRefund_account_id_text(String  refund_account_id_text){
        this.refund_account_id_text = refund_account_id_text ;
        this.refund_account_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [REFUND_ACCOUNT_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getRefund_account_id_textDirtyFlag(){
        return refund_account_id_textDirtyFlag ;
    }

    /**
     * 获取 [ACCOUNT_ID_TEXT]
     */
    @JsonProperty("account_id_text")
    public String getAccount_id_text(){
        return account_id_text ;
    }

    /**
     * 设置 [ACCOUNT_ID_TEXT]
     */
    @JsonProperty("account_id_text")
    public void setAccount_id_text(String  account_id_text){
        this.account_id_text = account_id_text ;
        this.account_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [ACCOUNT_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getAccount_id_textDirtyFlag(){
        return account_id_textDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return write_uid_text ;
    }

    /**
     * 设置 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return write_uid_textDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return create_uid_text ;
    }

    /**
     * 设置 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return create_uid_textDirtyFlag ;
    }

    /**
     * 获取 [REFUND_ACCOUNT_ID]
     */
    @JsonProperty("refund_account_id")
    public Integer getRefund_account_id(){
        return refund_account_id ;
    }

    /**
     * 设置 [REFUND_ACCOUNT_ID]
     */
    @JsonProperty("refund_account_id")
    public void setRefund_account_id(Integer  refund_account_id){
        this.refund_account_id = refund_account_id ;
        this.refund_account_idDirtyFlag = true ;
    }

    /**
     * 获取 [REFUND_ACCOUNT_ID]脏标记
     */
    @JsonIgnore
    public boolean getRefund_account_idDirtyFlag(){
        return refund_account_idDirtyFlag ;
    }

    /**
     * 获取 [CASH_BASIS_ACCOUNT_ID]
     */
    @JsonProperty("cash_basis_account_id")
    public Integer getCash_basis_account_id(){
        return cash_basis_account_id ;
    }

    /**
     * 设置 [CASH_BASIS_ACCOUNT_ID]
     */
    @JsonProperty("cash_basis_account_id")
    public void setCash_basis_account_id(Integer  cash_basis_account_id){
        this.cash_basis_account_id = cash_basis_account_id ;
        this.cash_basis_account_idDirtyFlag = true ;
    }

    /**
     * 获取 [CASH_BASIS_ACCOUNT_ID]脏标记
     */
    @JsonIgnore
    public boolean getCash_basis_account_idDirtyFlag(){
        return cash_basis_account_idDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return create_uid ;
    }

    /**
     * 设置 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return create_uidDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return write_uid ;
    }

    /**
     * 设置 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return write_uidDirtyFlag ;
    }

    /**
     * 获取 [ACCOUNT_ID]
     */
    @JsonProperty("account_id")
    public Integer getAccount_id(){
        return account_id ;
    }

    /**
     * 设置 [ACCOUNT_ID]
     */
    @JsonProperty("account_id")
    public void setAccount_id(Integer  account_id){
        this.account_id = account_id ;
        this.account_idDirtyFlag = true ;
    }

    /**
     * 获取 [ACCOUNT_ID]脏标记
     */
    @JsonIgnore
    public boolean getAccount_idDirtyFlag(){
        return account_idDirtyFlag ;
    }

    /**
     * 获取 [CASH_BASIS_BASE_ACCOUNT_ID]
     */
    @JsonProperty("cash_basis_base_account_id")
    public Integer getCash_basis_base_account_id(){
        return cash_basis_base_account_id ;
    }

    /**
     * 设置 [CASH_BASIS_BASE_ACCOUNT_ID]
     */
    @JsonProperty("cash_basis_base_account_id")
    public void setCash_basis_base_account_id(Integer  cash_basis_base_account_id){
        this.cash_basis_base_account_id = cash_basis_base_account_id ;
        this.cash_basis_base_account_idDirtyFlag = true ;
    }

    /**
     * 获取 [CASH_BASIS_BASE_ACCOUNT_ID]脏标记
     */
    @JsonIgnore
    public boolean getCash_basis_base_account_idDirtyFlag(){
        return cash_basis_base_account_idDirtyFlag ;
    }

    /**
     * 获取 [TAX_GROUP_ID]
     */
    @JsonProperty("tax_group_id")
    public Integer getTax_group_id(){
        return tax_group_id ;
    }

    /**
     * 设置 [TAX_GROUP_ID]
     */
    @JsonProperty("tax_group_id")
    public void setTax_group_id(Integer  tax_group_id){
        this.tax_group_id = tax_group_id ;
        this.tax_group_idDirtyFlag = true ;
    }

    /**
     * 获取 [TAX_GROUP_ID]脏标记
     */
    @JsonIgnore
    public boolean getTax_group_idDirtyFlag(){
        return tax_group_idDirtyFlag ;
    }

    /**
     * 获取 [COMPANY_ID]
     */
    @JsonProperty("company_id")
    public Integer getCompany_id(){
        return company_id ;
    }

    /**
     * 设置 [COMPANY_ID]
     */
    @JsonProperty("company_id")
    public void setCompany_id(Integer  company_id){
        this.company_id = company_id ;
        this.company_idDirtyFlag = true ;
    }

    /**
     * 获取 [COMPANY_ID]脏标记
     */
    @JsonIgnore
    public boolean getCompany_idDirtyFlag(){
        return company_idDirtyFlag ;
    }



    public Account_tax toDO() {
        Account_tax srfdomain = new Account_tax();
        if(getIdDirtyFlag())
            srfdomain.setId(id);
        if(getCreate_dateDirtyFlag())
            srfdomain.setCreate_date(create_date);
        if(getDisplay_nameDirtyFlag())
            srfdomain.setDisplay_name(display_name);
        if(getType_tax_useDirtyFlag())
            srfdomain.setType_tax_use(type_tax_use);
        if(getActiveDirtyFlag())
            srfdomain.setActive(active);
        if(getChildren_tax_idsDirtyFlag())
            srfdomain.setChildren_tax_ids(children_tax_ids);
        if(getAnalyticDirtyFlag())
            srfdomain.setAnalytic(analytic);
        if(getPrice_includeDirtyFlag())
            srfdomain.setPrice_include(price_include);
        if(getDescriptionDirtyFlag())
            srfdomain.setDescription(description);
        if(getWrite_dateDirtyFlag())
            srfdomain.setWrite_date(write_date);
        if(getTax_exigibilityDirtyFlag())
            srfdomain.setTax_exigibility(tax_exigibility);
        if(getNameDirtyFlag())
            srfdomain.setName(name);
        if(getTag_idsDirtyFlag())
            srfdomain.setTag_ids(tag_ids);
        if(getAmount_typeDirtyFlag())
            srfdomain.setAmount_type(amount_type);
        if(get__last_updateDirtyFlag())
            srfdomain.set__last_update(__last_update);
        if(getAmountDirtyFlag())
            srfdomain.setAmount(amount);
        if(getSequenceDirtyFlag())
            srfdomain.setSequence(sequence);
        if(getInclude_base_amountDirtyFlag())
            srfdomain.setInclude_base_amount(include_base_amount);
        if(getCash_basis_base_account_id_textDirtyFlag())
            srfdomain.setCash_basis_base_account_id_text(cash_basis_base_account_id_text);
        if(getCash_basis_account_id_textDirtyFlag())
            srfdomain.setCash_basis_account_id_text(cash_basis_account_id_text);
        if(getHide_tax_exigibilityDirtyFlag())
            srfdomain.setHide_tax_exigibility(hide_tax_exigibility);
        if(getTax_group_id_textDirtyFlag())
            srfdomain.setTax_group_id_text(tax_group_id_text);
        if(getCompany_id_textDirtyFlag())
            srfdomain.setCompany_id_text(company_id_text);
        if(getRefund_account_id_textDirtyFlag())
            srfdomain.setRefund_account_id_text(refund_account_id_text);
        if(getAccount_id_textDirtyFlag())
            srfdomain.setAccount_id_text(account_id_text);
        if(getWrite_uid_textDirtyFlag())
            srfdomain.setWrite_uid_text(write_uid_text);
        if(getCreate_uid_textDirtyFlag())
            srfdomain.setCreate_uid_text(create_uid_text);
        if(getRefund_account_idDirtyFlag())
            srfdomain.setRefund_account_id(refund_account_id);
        if(getCash_basis_account_idDirtyFlag())
            srfdomain.setCash_basis_account_id(cash_basis_account_id);
        if(getCreate_uidDirtyFlag())
            srfdomain.setCreate_uid(create_uid);
        if(getWrite_uidDirtyFlag())
            srfdomain.setWrite_uid(write_uid);
        if(getAccount_idDirtyFlag())
            srfdomain.setAccount_id(account_id);
        if(getCash_basis_base_account_idDirtyFlag())
            srfdomain.setCash_basis_base_account_id(cash_basis_base_account_id);
        if(getTax_group_idDirtyFlag())
            srfdomain.setTax_group_id(tax_group_id);
        if(getCompany_idDirtyFlag())
            srfdomain.setCompany_id(company_id);

        return srfdomain;
    }

    public void fromDO(Account_tax srfdomain) {
        if(srfdomain == null )
          return ;
        if(srfdomain.getIdDirtyFlag())
            this.setId(srfdomain.getId());
        if(srfdomain.getCreate_dateDirtyFlag())
            this.setCreate_date(srfdomain.getCreate_date());
        if(srfdomain.getDisplay_nameDirtyFlag())
            this.setDisplay_name(srfdomain.getDisplay_name());
        if(srfdomain.getType_tax_useDirtyFlag())
            this.setType_tax_use(srfdomain.getType_tax_use());
        if(srfdomain.getActiveDirtyFlag())
            this.setActive(srfdomain.getActive());
        if(srfdomain.getChildren_tax_idsDirtyFlag())
            this.setChildren_tax_ids(srfdomain.getChildren_tax_ids());
        if(srfdomain.getAnalyticDirtyFlag())
            this.setAnalytic(srfdomain.getAnalytic());
        if(srfdomain.getPrice_includeDirtyFlag())
            this.setPrice_include(srfdomain.getPrice_include());
        if(srfdomain.getDescriptionDirtyFlag())
            this.setDescription(srfdomain.getDescription());
        if(srfdomain.getWrite_dateDirtyFlag())
            this.setWrite_date(srfdomain.getWrite_date());
        if(srfdomain.getTax_exigibilityDirtyFlag())
            this.setTax_exigibility(srfdomain.getTax_exigibility());
        if(srfdomain.getNameDirtyFlag())
            this.setName(srfdomain.getName());
        if(srfdomain.getTag_idsDirtyFlag())
            this.setTag_ids(srfdomain.getTag_ids());
        if(srfdomain.getAmount_typeDirtyFlag())
            this.setAmount_type(srfdomain.getAmount_type());
        if(srfdomain.get__last_updateDirtyFlag())
            this.set__last_update(srfdomain.get__last_update());
        if(srfdomain.getAmountDirtyFlag())
            this.setAmount(srfdomain.getAmount());
        if(srfdomain.getSequenceDirtyFlag())
            this.setSequence(srfdomain.getSequence());
        if(srfdomain.getInclude_base_amountDirtyFlag())
            this.setInclude_base_amount(srfdomain.getInclude_base_amount());
        if(srfdomain.getCash_basis_base_account_id_textDirtyFlag())
            this.setCash_basis_base_account_id_text(srfdomain.getCash_basis_base_account_id_text());
        if(srfdomain.getCash_basis_account_id_textDirtyFlag())
            this.setCash_basis_account_id_text(srfdomain.getCash_basis_account_id_text());
        if(srfdomain.getHide_tax_exigibilityDirtyFlag())
            this.setHide_tax_exigibility(srfdomain.getHide_tax_exigibility());
        if(srfdomain.getTax_group_id_textDirtyFlag())
            this.setTax_group_id_text(srfdomain.getTax_group_id_text());
        if(srfdomain.getCompany_id_textDirtyFlag())
            this.setCompany_id_text(srfdomain.getCompany_id_text());
        if(srfdomain.getRefund_account_id_textDirtyFlag())
            this.setRefund_account_id_text(srfdomain.getRefund_account_id_text());
        if(srfdomain.getAccount_id_textDirtyFlag())
            this.setAccount_id_text(srfdomain.getAccount_id_text());
        if(srfdomain.getWrite_uid_textDirtyFlag())
            this.setWrite_uid_text(srfdomain.getWrite_uid_text());
        if(srfdomain.getCreate_uid_textDirtyFlag())
            this.setCreate_uid_text(srfdomain.getCreate_uid_text());
        if(srfdomain.getRefund_account_idDirtyFlag())
            this.setRefund_account_id(srfdomain.getRefund_account_id());
        if(srfdomain.getCash_basis_account_idDirtyFlag())
            this.setCash_basis_account_id(srfdomain.getCash_basis_account_id());
        if(srfdomain.getCreate_uidDirtyFlag())
            this.setCreate_uid(srfdomain.getCreate_uid());
        if(srfdomain.getWrite_uidDirtyFlag())
            this.setWrite_uid(srfdomain.getWrite_uid());
        if(srfdomain.getAccount_idDirtyFlag())
            this.setAccount_id(srfdomain.getAccount_id());
        if(srfdomain.getCash_basis_base_account_idDirtyFlag())
            this.setCash_basis_base_account_id(srfdomain.getCash_basis_base_account_id());
        if(srfdomain.getTax_group_idDirtyFlag())
            this.setTax_group_id(srfdomain.getTax_group_id());
        if(srfdomain.getCompany_idDirtyFlag())
            this.setCompany_id(srfdomain.getCompany_id());

    }

    public List<Account_taxDTO> fromDOPage(List<Account_tax> poPage)   {
        if(poPage == null)
            return null;
        List<Account_taxDTO> dtos=new ArrayList<Account_taxDTO>();
        for(Account_tax domain : poPage) {
            Account_taxDTO dto = new Account_taxDTO();
            dto.fromDO(domain);
            dtos.add(dto);
        }
        return dtos;
    }
}

