package cn.ibizlab.odoo.service.odoo_account.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_account.dto.Account_invoice_refundDTO;
import cn.ibizlab.odoo.core.odoo_account.domain.Account_invoice_refund;
import cn.ibizlab.odoo.core.odoo_account.service.IAccount_invoice_refundService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_invoice_refundSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Account_invoice_refund" })
@RestController
@RequestMapping("")
public class Account_invoice_refundResource {

    @Autowired
    private IAccount_invoice_refundService account_invoice_refundService;

    public IAccount_invoice_refundService getAccount_invoice_refundService() {
        return this.account_invoice_refundService;
    }

    @ApiOperation(value = "获取数据", tags = {"Account_invoice_refund" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_account/account_invoice_refunds/{account_invoice_refund_id}")
    public ResponseEntity<Account_invoice_refundDTO> get(@PathVariable("account_invoice_refund_id") Integer account_invoice_refund_id) {
        Account_invoice_refundDTO dto = new Account_invoice_refundDTO();
        Account_invoice_refund domain = account_invoice_refundService.get(account_invoice_refund_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批建立数据", tags = {"Account_invoice_refund" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_account/account_invoice_refunds/createBatch")
    public ResponseEntity<Boolean> createBatchAccount_invoice_refund(@RequestBody List<Account_invoice_refundDTO> account_invoice_refunddtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "批删除数据", tags = {"Account_invoice_refund" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_account/account_invoice_refunds/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Account_invoice_refundDTO> account_invoice_refunddtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "批更新数据", tags = {"Account_invoice_refund" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_account/account_invoice_refunds/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Account_invoice_refundDTO> account_invoice_refunddtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "建立数据", tags = {"Account_invoice_refund" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_account/account_invoice_refunds")

    public ResponseEntity<Account_invoice_refundDTO> create(@RequestBody Account_invoice_refundDTO account_invoice_refunddto) {
        Account_invoice_refundDTO dto = new Account_invoice_refundDTO();
        Account_invoice_refund domain = account_invoice_refunddto.toDO();
		account_invoice_refundService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "更新数据", tags = {"Account_invoice_refund" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_account/account_invoice_refunds/{account_invoice_refund_id}")

    public ResponseEntity<Account_invoice_refundDTO> update(@PathVariable("account_invoice_refund_id") Integer account_invoice_refund_id, @RequestBody Account_invoice_refundDTO account_invoice_refunddto) {
		Account_invoice_refund domain = account_invoice_refunddto.toDO();
        domain.setId(account_invoice_refund_id);
		account_invoice_refundService.update(domain);
		Account_invoice_refundDTO dto = new Account_invoice_refundDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "删除数据", tags = {"Account_invoice_refund" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_account/account_invoice_refunds/{account_invoice_refund_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("account_invoice_refund_id") Integer account_invoice_refund_id) {
        Account_invoice_refundDTO account_invoice_refunddto = new Account_invoice_refundDTO();
		Account_invoice_refund domain = new Account_invoice_refund();
		account_invoice_refunddto.setId(account_invoice_refund_id);
		domain.setId(account_invoice_refund_id);
        Boolean rst = account_invoice_refundService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

	@ApiOperation(value = "获取默认查询", tags = {"Account_invoice_refund" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_account/account_invoice_refunds/fetchdefault")
	public ResponseEntity<Page<Account_invoice_refundDTO>> fetchDefault(Account_invoice_refundSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Account_invoice_refundDTO> list = new ArrayList<Account_invoice_refundDTO>();
        
        Page<Account_invoice_refund> domains = account_invoice_refundService.searchDefault(context) ;
        for(Account_invoice_refund account_invoice_refund : domains.getContent()){
            Account_invoice_refundDTO dto = new Account_invoice_refundDTO();
            dto.fromDO(account_invoice_refund);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
