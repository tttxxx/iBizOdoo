package cn.ibizlab.odoo.service.odoo_account.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_account.dto.Account_bank_statement_import_journal_creationDTO;
import cn.ibizlab.odoo.core.odoo_account.domain.Account_bank_statement_import_journal_creation;
import cn.ibizlab.odoo.core.odoo_account.service.IAccount_bank_statement_import_journal_creationService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_bank_statement_import_journal_creationSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Account_bank_statement_import_journal_creation" })
@RestController
@RequestMapping("")
public class Account_bank_statement_import_journal_creationResource {

    @Autowired
    private IAccount_bank_statement_import_journal_creationService account_bank_statement_import_journal_creationService;

    public IAccount_bank_statement_import_journal_creationService getAccount_bank_statement_import_journal_creationService() {
        return this.account_bank_statement_import_journal_creationService;
    }

    @ApiOperation(value = "建立数据", tags = {"Account_bank_statement_import_journal_creation" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_account/account_bank_statement_import_journal_creations")

    public ResponseEntity<Account_bank_statement_import_journal_creationDTO> create(@RequestBody Account_bank_statement_import_journal_creationDTO account_bank_statement_import_journal_creationdto) {
        Account_bank_statement_import_journal_creationDTO dto = new Account_bank_statement_import_journal_creationDTO();
        Account_bank_statement_import_journal_creation domain = account_bank_statement_import_journal_creationdto.toDO();
		account_bank_statement_import_journal_creationService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "删除数据", tags = {"Account_bank_statement_import_journal_creation" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_account/account_bank_statement_import_journal_creations/{account_bank_statement_import_journal_creation_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("account_bank_statement_import_journal_creation_id") Integer account_bank_statement_import_journal_creation_id) {
        Account_bank_statement_import_journal_creationDTO account_bank_statement_import_journal_creationdto = new Account_bank_statement_import_journal_creationDTO();
		Account_bank_statement_import_journal_creation domain = new Account_bank_statement_import_journal_creation();
		account_bank_statement_import_journal_creationdto.setId(account_bank_statement_import_journal_creation_id);
		domain.setId(account_bank_statement_import_journal_creation_id);
        Boolean rst = account_bank_statement_import_journal_creationService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "批建立数据", tags = {"Account_bank_statement_import_journal_creation" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_account/account_bank_statement_import_journal_creations/createBatch")
    public ResponseEntity<Boolean> createBatchAccount_bank_statement_import_journal_creation(@RequestBody List<Account_bank_statement_import_journal_creationDTO> account_bank_statement_import_journal_creationdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "更新数据", tags = {"Account_bank_statement_import_journal_creation" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_account/account_bank_statement_import_journal_creations/{account_bank_statement_import_journal_creation_id}")

    public ResponseEntity<Account_bank_statement_import_journal_creationDTO> update(@PathVariable("account_bank_statement_import_journal_creation_id") Integer account_bank_statement_import_journal_creation_id, @RequestBody Account_bank_statement_import_journal_creationDTO account_bank_statement_import_journal_creationdto) {
		Account_bank_statement_import_journal_creation domain = account_bank_statement_import_journal_creationdto.toDO();
        domain.setId(account_bank_statement_import_journal_creation_id);
		account_bank_statement_import_journal_creationService.update(domain);
		Account_bank_statement_import_journal_creationDTO dto = new Account_bank_statement_import_journal_creationDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取数据", tags = {"Account_bank_statement_import_journal_creation" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_account/account_bank_statement_import_journal_creations/{account_bank_statement_import_journal_creation_id}")
    public ResponseEntity<Account_bank_statement_import_journal_creationDTO> get(@PathVariable("account_bank_statement_import_journal_creation_id") Integer account_bank_statement_import_journal_creation_id) {
        Account_bank_statement_import_journal_creationDTO dto = new Account_bank_statement_import_journal_creationDTO();
        Account_bank_statement_import_journal_creation domain = account_bank_statement_import_journal_creationService.get(account_bank_statement_import_journal_creation_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批更新数据", tags = {"Account_bank_statement_import_journal_creation" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_account/account_bank_statement_import_journal_creations/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Account_bank_statement_import_journal_creationDTO> account_bank_statement_import_journal_creationdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "批删除数据", tags = {"Account_bank_statement_import_journal_creation" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_account/account_bank_statement_import_journal_creations/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Account_bank_statement_import_journal_creationDTO> account_bank_statement_import_journal_creationdtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Account_bank_statement_import_journal_creation" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_account/account_bank_statement_import_journal_creations/fetchdefault")
	public ResponseEntity<Page<Account_bank_statement_import_journal_creationDTO>> fetchDefault(Account_bank_statement_import_journal_creationSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Account_bank_statement_import_journal_creationDTO> list = new ArrayList<Account_bank_statement_import_journal_creationDTO>();
        
        Page<Account_bank_statement_import_journal_creation> domains = account_bank_statement_import_journal_creationService.searchDefault(context) ;
        for(Account_bank_statement_import_journal_creation account_bank_statement_import_journal_creation : domains.getContent()){
            Account_bank_statement_import_journal_creationDTO dto = new Account_bank_statement_import_journal_creationDTO();
            dto.fromDO(account_bank_statement_import_journal_creation);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
