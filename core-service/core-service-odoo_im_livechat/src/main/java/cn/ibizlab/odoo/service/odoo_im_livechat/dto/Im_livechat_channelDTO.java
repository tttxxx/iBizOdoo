package cn.ibizlab.odoo.service.odoo_im_livechat.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.core.odoo_im_livechat.valuerule.anno.im_livechat_channel.*;
import cn.ibizlab.odoo.core.odoo_im_livechat.domain.Im_livechat_channel;
import org.springframework.cglib.beans.BeanCopier;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 服务DTO对象[Im_livechat_channelDTO]
 */
public class Im_livechat_channelDTO implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [USER_IDS]
     *
     */
    @Im_livechat_channelUser_idsDefault(info = "默认规则")
    private String user_ids;

    @JsonIgnore
    private boolean user_idsDirtyFlag;

    /**
     * 属性 [WEBSITE_URL]
     *
     */
    @Im_livechat_channelWebsite_urlDefault(info = "默认规则")
    private String website_url;

    @JsonIgnore
    private boolean website_urlDirtyFlag;

    /**
     * 属性 [RULE_IDS]
     *
     */
    @Im_livechat_channelRule_idsDefault(info = "默认规则")
    private String rule_ids;

    @JsonIgnore
    private boolean rule_idsDirtyFlag;

    /**
     * 属性 [IMAGE]
     *
     */
    @Im_livechat_channelImageDefault(info = "默认规则")
    private byte[] image;

    @JsonIgnore
    private boolean imageDirtyFlag;

    /**
     * 属性 [WEB_PAGE]
     *
     */
    @Im_livechat_channelWeb_pageDefault(info = "默认规则")
    private String web_page;

    @JsonIgnore
    private boolean web_pageDirtyFlag;

    /**
     * 属性 [IS_PUBLISHED]
     *
     */
    @Im_livechat_channelIs_publishedDefault(info = "默认规则")
    private String is_published;

    @JsonIgnore
    private boolean is_publishedDirtyFlag;

    /**
     * 属性 [SCRIPT_EXTERNAL]
     *
     */
    @Im_livechat_channelScript_externalDefault(info = "默认规则")
    private String script_external;

    @JsonIgnore
    private boolean script_externalDirtyFlag;

    /**
     * 属性 [NBR_CHANNEL]
     *
     */
    @Im_livechat_channelNbr_channelDefault(info = "默认规则")
    private Integer nbr_channel;

    @JsonIgnore
    private boolean nbr_channelDirtyFlag;

    /**
     * 属性 [BUTTON_TEXT]
     *
     */
    @Im_livechat_channelButton_textDefault(info = "默认规则")
    private String button_text;

    @JsonIgnore
    private boolean button_textDirtyFlag;

    /**
     * 属性 [WEBSITE_DESCRIPTION]
     *
     */
    @Im_livechat_channelWebsite_descriptionDefault(info = "默认规则")
    private String website_description;

    @JsonIgnore
    private boolean website_descriptionDirtyFlag;

    /**
     * 属性 [CHANNEL_IDS]
     *
     */
    @Im_livechat_channelChannel_idsDefault(info = "默认规则")
    private String channel_ids;

    @JsonIgnore
    private boolean channel_idsDirtyFlag;

    /**
     * 属性 [NAME]
     *
     */
    @Im_livechat_channelNameDefault(info = "默认规则")
    private String name;

    @JsonIgnore
    private boolean nameDirtyFlag;

    /**
     * 属性 [WEBSITE_PUBLISHED]
     *
     */
    @Im_livechat_channelWebsite_publishedDefault(info = "默认规则")
    private String website_published;

    @JsonIgnore
    private boolean website_publishedDirtyFlag;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @Im_livechat_channel__last_updateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp __last_update;

    @JsonIgnore
    private boolean __last_updateDirtyFlag;

    /**
     * 属性 [CREATE_DATE]
     *
     */
    @Im_livechat_channelCreate_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp create_date;

    @JsonIgnore
    private boolean create_dateDirtyFlag;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @Im_livechat_channelDisplay_nameDefault(info = "默认规则")
    private String display_name;

    @JsonIgnore
    private boolean display_nameDirtyFlag;

    /**
     * 属性 [ID]
     *
     */
    @Im_livechat_channelIdDefault(info = "默认规则")
    private Integer id;

    @JsonIgnore
    private boolean idDirtyFlag;

    /**
     * 属性 [IMAGE_MEDIUM]
     *
     */
    @Im_livechat_channelImage_mediumDefault(info = "默认规则")
    private byte[] image_medium;

    @JsonIgnore
    private boolean image_mediumDirtyFlag;

    /**
     * 属性 [RATING_PERCENTAGE_SATISFACTION]
     *
     */
    @Im_livechat_channelRating_percentage_satisfactionDefault(info = "默认规则")
    private Integer rating_percentage_satisfaction;

    @JsonIgnore
    private boolean rating_percentage_satisfactionDirtyFlag;

    /**
     * 属性 [INPUT_PLACEHOLDER]
     *
     */
    @Im_livechat_channelInput_placeholderDefault(info = "默认规则")
    private String input_placeholder;

    @JsonIgnore
    private boolean input_placeholderDirtyFlag;

    /**
     * 属性 [IMAGE_SMALL]
     *
     */
    @Im_livechat_channelImage_smallDefault(info = "默认规则")
    private byte[] image_small;

    @JsonIgnore
    private boolean image_smallDirtyFlag;

    /**
     * 属性 [ARE_YOU_INSIDE]
     *
     */
    @Im_livechat_channelAre_you_insideDefault(info = "默认规则")
    private String are_you_inside;

    @JsonIgnore
    private boolean are_you_insideDirtyFlag;

    /**
     * 属性 [DEFAULT_MESSAGE]
     *
     */
    @Im_livechat_channelDefault_messageDefault(info = "默认规则")
    private String default_message;

    @JsonIgnore
    private boolean default_messageDirtyFlag;

    /**
     * 属性 [WRITE_DATE]
     *
     */
    @Im_livechat_channelWrite_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp write_date;

    @JsonIgnore
    private boolean write_dateDirtyFlag;

    /**
     * 属性 [WRITE_UID_TEXT]
     *
     */
    @Im_livechat_channelWrite_uid_textDefault(info = "默认规则")
    private String write_uid_text;

    @JsonIgnore
    private boolean write_uid_textDirtyFlag;

    /**
     * 属性 [CREATE_UID_TEXT]
     *
     */
    @Im_livechat_channelCreate_uid_textDefault(info = "默认规则")
    private String create_uid_text;

    @JsonIgnore
    private boolean create_uid_textDirtyFlag;

    /**
     * 属性 [WRITE_UID]
     *
     */
    @Im_livechat_channelWrite_uidDefault(info = "默认规则")
    private Integer write_uid;

    @JsonIgnore
    private boolean write_uidDirtyFlag;

    /**
     * 属性 [CREATE_UID]
     *
     */
    @Im_livechat_channelCreate_uidDefault(info = "默认规则")
    private Integer create_uid;

    @JsonIgnore
    private boolean create_uidDirtyFlag;


    /**
     * 获取 [USER_IDS]
     */
    @JsonProperty("user_ids")
    public String getUser_ids(){
        return user_ids ;
    }

    /**
     * 设置 [USER_IDS]
     */
    @JsonProperty("user_ids")
    public void setUser_ids(String  user_ids){
        this.user_ids = user_ids ;
        this.user_idsDirtyFlag = true ;
    }

    /**
     * 获取 [USER_IDS]脏标记
     */
    @JsonIgnore
    public boolean getUser_idsDirtyFlag(){
        return user_idsDirtyFlag ;
    }

    /**
     * 获取 [WEBSITE_URL]
     */
    @JsonProperty("website_url")
    public String getWebsite_url(){
        return website_url ;
    }

    /**
     * 设置 [WEBSITE_URL]
     */
    @JsonProperty("website_url")
    public void setWebsite_url(String  website_url){
        this.website_url = website_url ;
        this.website_urlDirtyFlag = true ;
    }

    /**
     * 获取 [WEBSITE_URL]脏标记
     */
    @JsonIgnore
    public boolean getWebsite_urlDirtyFlag(){
        return website_urlDirtyFlag ;
    }

    /**
     * 获取 [RULE_IDS]
     */
    @JsonProperty("rule_ids")
    public String getRule_ids(){
        return rule_ids ;
    }

    /**
     * 设置 [RULE_IDS]
     */
    @JsonProperty("rule_ids")
    public void setRule_ids(String  rule_ids){
        this.rule_ids = rule_ids ;
        this.rule_idsDirtyFlag = true ;
    }

    /**
     * 获取 [RULE_IDS]脏标记
     */
    @JsonIgnore
    public boolean getRule_idsDirtyFlag(){
        return rule_idsDirtyFlag ;
    }

    /**
     * 获取 [IMAGE]
     */
    @JsonProperty("image")
    public byte[] getImage(){
        return image ;
    }

    /**
     * 设置 [IMAGE]
     */
    @JsonProperty("image")
    public void setImage(byte[]  image){
        this.image = image ;
        this.imageDirtyFlag = true ;
    }

    /**
     * 获取 [IMAGE]脏标记
     */
    @JsonIgnore
    public boolean getImageDirtyFlag(){
        return imageDirtyFlag ;
    }

    /**
     * 获取 [WEB_PAGE]
     */
    @JsonProperty("web_page")
    public String getWeb_page(){
        return web_page ;
    }

    /**
     * 设置 [WEB_PAGE]
     */
    @JsonProperty("web_page")
    public void setWeb_page(String  web_page){
        this.web_page = web_page ;
        this.web_pageDirtyFlag = true ;
    }

    /**
     * 获取 [WEB_PAGE]脏标记
     */
    @JsonIgnore
    public boolean getWeb_pageDirtyFlag(){
        return web_pageDirtyFlag ;
    }

    /**
     * 获取 [IS_PUBLISHED]
     */
    @JsonProperty("is_published")
    public String getIs_published(){
        return is_published ;
    }

    /**
     * 设置 [IS_PUBLISHED]
     */
    @JsonProperty("is_published")
    public void setIs_published(String  is_published){
        this.is_published = is_published ;
        this.is_publishedDirtyFlag = true ;
    }

    /**
     * 获取 [IS_PUBLISHED]脏标记
     */
    @JsonIgnore
    public boolean getIs_publishedDirtyFlag(){
        return is_publishedDirtyFlag ;
    }

    /**
     * 获取 [SCRIPT_EXTERNAL]
     */
    @JsonProperty("script_external")
    public String getScript_external(){
        return script_external ;
    }

    /**
     * 设置 [SCRIPT_EXTERNAL]
     */
    @JsonProperty("script_external")
    public void setScript_external(String  script_external){
        this.script_external = script_external ;
        this.script_externalDirtyFlag = true ;
    }

    /**
     * 获取 [SCRIPT_EXTERNAL]脏标记
     */
    @JsonIgnore
    public boolean getScript_externalDirtyFlag(){
        return script_externalDirtyFlag ;
    }

    /**
     * 获取 [NBR_CHANNEL]
     */
    @JsonProperty("nbr_channel")
    public Integer getNbr_channel(){
        return nbr_channel ;
    }

    /**
     * 设置 [NBR_CHANNEL]
     */
    @JsonProperty("nbr_channel")
    public void setNbr_channel(Integer  nbr_channel){
        this.nbr_channel = nbr_channel ;
        this.nbr_channelDirtyFlag = true ;
    }

    /**
     * 获取 [NBR_CHANNEL]脏标记
     */
    @JsonIgnore
    public boolean getNbr_channelDirtyFlag(){
        return nbr_channelDirtyFlag ;
    }

    /**
     * 获取 [BUTTON_TEXT]
     */
    @JsonProperty("button_text")
    public String getButton_text(){
        return button_text ;
    }

    /**
     * 设置 [BUTTON_TEXT]
     */
    @JsonProperty("button_text")
    public void setButton_text(String  button_text){
        this.button_text = button_text ;
        this.button_textDirtyFlag = true ;
    }

    /**
     * 获取 [BUTTON_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getButton_textDirtyFlag(){
        return button_textDirtyFlag ;
    }

    /**
     * 获取 [WEBSITE_DESCRIPTION]
     */
    @JsonProperty("website_description")
    public String getWebsite_description(){
        return website_description ;
    }

    /**
     * 设置 [WEBSITE_DESCRIPTION]
     */
    @JsonProperty("website_description")
    public void setWebsite_description(String  website_description){
        this.website_description = website_description ;
        this.website_descriptionDirtyFlag = true ;
    }

    /**
     * 获取 [WEBSITE_DESCRIPTION]脏标记
     */
    @JsonIgnore
    public boolean getWebsite_descriptionDirtyFlag(){
        return website_descriptionDirtyFlag ;
    }

    /**
     * 获取 [CHANNEL_IDS]
     */
    @JsonProperty("channel_ids")
    public String getChannel_ids(){
        return channel_ids ;
    }

    /**
     * 设置 [CHANNEL_IDS]
     */
    @JsonProperty("channel_ids")
    public void setChannel_ids(String  channel_ids){
        this.channel_ids = channel_ids ;
        this.channel_idsDirtyFlag = true ;
    }

    /**
     * 获取 [CHANNEL_IDS]脏标记
     */
    @JsonIgnore
    public boolean getChannel_idsDirtyFlag(){
        return channel_idsDirtyFlag ;
    }

    /**
     * 获取 [NAME]
     */
    @JsonProperty("name")
    public String getName(){
        return name ;
    }

    /**
     * 设置 [NAME]
     */
    @JsonProperty("name")
    public void setName(String  name){
        this.name = name ;
        this.nameDirtyFlag = true ;
    }

    /**
     * 获取 [NAME]脏标记
     */
    @JsonIgnore
    public boolean getNameDirtyFlag(){
        return nameDirtyFlag ;
    }

    /**
     * 获取 [WEBSITE_PUBLISHED]
     */
    @JsonProperty("website_published")
    public String getWebsite_published(){
        return website_published ;
    }

    /**
     * 设置 [WEBSITE_PUBLISHED]
     */
    @JsonProperty("website_published")
    public void setWebsite_published(String  website_published){
        this.website_published = website_published ;
        this.website_publishedDirtyFlag = true ;
    }

    /**
     * 获取 [WEBSITE_PUBLISHED]脏标记
     */
    @JsonIgnore
    public boolean getWebsite_publishedDirtyFlag(){
        return website_publishedDirtyFlag ;
    }

    /**
     * 获取 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return __last_update ;
    }

    /**
     * 设置 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

    /**
     * 获取 [__LAST_UPDATE]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return __last_updateDirtyFlag ;
    }

    /**
     * 获取 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return create_date ;
    }

    /**
     * 设置 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return create_dateDirtyFlag ;
    }

    /**
     * 获取 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return display_name ;
    }

    /**
     * 设置 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

    /**
     * 获取 [DISPLAY_NAME]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return display_nameDirtyFlag ;
    }

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

    /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return idDirtyFlag ;
    }

    /**
     * 获取 [IMAGE_MEDIUM]
     */
    @JsonProperty("image_medium")
    public byte[] getImage_medium(){
        return image_medium ;
    }

    /**
     * 设置 [IMAGE_MEDIUM]
     */
    @JsonProperty("image_medium")
    public void setImage_medium(byte[]  image_medium){
        this.image_medium = image_medium ;
        this.image_mediumDirtyFlag = true ;
    }

    /**
     * 获取 [IMAGE_MEDIUM]脏标记
     */
    @JsonIgnore
    public boolean getImage_mediumDirtyFlag(){
        return image_mediumDirtyFlag ;
    }

    /**
     * 获取 [RATING_PERCENTAGE_SATISFACTION]
     */
    @JsonProperty("rating_percentage_satisfaction")
    public Integer getRating_percentage_satisfaction(){
        return rating_percentage_satisfaction ;
    }

    /**
     * 设置 [RATING_PERCENTAGE_SATISFACTION]
     */
    @JsonProperty("rating_percentage_satisfaction")
    public void setRating_percentage_satisfaction(Integer  rating_percentage_satisfaction){
        this.rating_percentage_satisfaction = rating_percentage_satisfaction ;
        this.rating_percentage_satisfactionDirtyFlag = true ;
    }

    /**
     * 获取 [RATING_PERCENTAGE_SATISFACTION]脏标记
     */
    @JsonIgnore
    public boolean getRating_percentage_satisfactionDirtyFlag(){
        return rating_percentage_satisfactionDirtyFlag ;
    }

    /**
     * 获取 [INPUT_PLACEHOLDER]
     */
    @JsonProperty("input_placeholder")
    public String getInput_placeholder(){
        return input_placeholder ;
    }

    /**
     * 设置 [INPUT_PLACEHOLDER]
     */
    @JsonProperty("input_placeholder")
    public void setInput_placeholder(String  input_placeholder){
        this.input_placeholder = input_placeholder ;
        this.input_placeholderDirtyFlag = true ;
    }

    /**
     * 获取 [INPUT_PLACEHOLDER]脏标记
     */
    @JsonIgnore
    public boolean getInput_placeholderDirtyFlag(){
        return input_placeholderDirtyFlag ;
    }

    /**
     * 获取 [IMAGE_SMALL]
     */
    @JsonProperty("image_small")
    public byte[] getImage_small(){
        return image_small ;
    }

    /**
     * 设置 [IMAGE_SMALL]
     */
    @JsonProperty("image_small")
    public void setImage_small(byte[]  image_small){
        this.image_small = image_small ;
        this.image_smallDirtyFlag = true ;
    }

    /**
     * 获取 [IMAGE_SMALL]脏标记
     */
    @JsonIgnore
    public boolean getImage_smallDirtyFlag(){
        return image_smallDirtyFlag ;
    }

    /**
     * 获取 [ARE_YOU_INSIDE]
     */
    @JsonProperty("are_you_inside")
    public String getAre_you_inside(){
        return are_you_inside ;
    }

    /**
     * 设置 [ARE_YOU_INSIDE]
     */
    @JsonProperty("are_you_inside")
    public void setAre_you_inside(String  are_you_inside){
        this.are_you_inside = are_you_inside ;
        this.are_you_insideDirtyFlag = true ;
    }

    /**
     * 获取 [ARE_YOU_INSIDE]脏标记
     */
    @JsonIgnore
    public boolean getAre_you_insideDirtyFlag(){
        return are_you_insideDirtyFlag ;
    }

    /**
     * 获取 [DEFAULT_MESSAGE]
     */
    @JsonProperty("default_message")
    public String getDefault_message(){
        return default_message ;
    }

    /**
     * 设置 [DEFAULT_MESSAGE]
     */
    @JsonProperty("default_message")
    public void setDefault_message(String  default_message){
        this.default_message = default_message ;
        this.default_messageDirtyFlag = true ;
    }

    /**
     * 获取 [DEFAULT_MESSAGE]脏标记
     */
    @JsonIgnore
    public boolean getDefault_messageDirtyFlag(){
        return default_messageDirtyFlag ;
    }

    /**
     * 获取 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return write_date ;
    }

    /**
     * 设置 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return write_dateDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return write_uid_text ;
    }

    /**
     * 设置 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return write_uid_textDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return create_uid_text ;
    }

    /**
     * 设置 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return create_uid_textDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return write_uid ;
    }

    /**
     * 设置 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return write_uidDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return create_uid ;
    }

    /**
     * 设置 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return create_uidDirtyFlag ;
    }



    public Im_livechat_channel toDO() {
        Im_livechat_channel srfdomain = new Im_livechat_channel();
        if(getUser_idsDirtyFlag())
            srfdomain.setUser_ids(user_ids);
        if(getWebsite_urlDirtyFlag())
            srfdomain.setWebsite_url(website_url);
        if(getRule_idsDirtyFlag())
            srfdomain.setRule_ids(rule_ids);
        if(getImageDirtyFlag())
            srfdomain.setImage(image);
        if(getWeb_pageDirtyFlag())
            srfdomain.setWeb_page(web_page);
        if(getIs_publishedDirtyFlag())
            srfdomain.setIs_published(is_published);
        if(getScript_externalDirtyFlag())
            srfdomain.setScript_external(script_external);
        if(getNbr_channelDirtyFlag())
            srfdomain.setNbr_channel(nbr_channel);
        if(getButton_textDirtyFlag())
            srfdomain.setButton_text(button_text);
        if(getWebsite_descriptionDirtyFlag())
            srfdomain.setWebsite_description(website_description);
        if(getChannel_idsDirtyFlag())
            srfdomain.setChannel_ids(channel_ids);
        if(getNameDirtyFlag())
            srfdomain.setName(name);
        if(getWebsite_publishedDirtyFlag())
            srfdomain.setWebsite_published(website_published);
        if(get__last_updateDirtyFlag())
            srfdomain.set__last_update(__last_update);
        if(getCreate_dateDirtyFlag())
            srfdomain.setCreate_date(create_date);
        if(getDisplay_nameDirtyFlag())
            srfdomain.setDisplay_name(display_name);
        if(getIdDirtyFlag())
            srfdomain.setId(id);
        if(getImage_mediumDirtyFlag())
            srfdomain.setImage_medium(image_medium);
        if(getRating_percentage_satisfactionDirtyFlag())
            srfdomain.setRating_percentage_satisfaction(rating_percentage_satisfaction);
        if(getInput_placeholderDirtyFlag())
            srfdomain.setInput_placeholder(input_placeholder);
        if(getImage_smallDirtyFlag())
            srfdomain.setImage_small(image_small);
        if(getAre_you_insideDirtyFlag())
            srfdomain.setAre_you_inside(are_you_inside);
        if(getDefault_messageDirtyFlag())
            srfdomain.setDefault_message(default_message);
        if(getWrite_dateDirtyFlag())
            srfdomain.setWrite_date(write_date);
        if(getWrite_uid_textDirtyFlag())
            srfdomain.setWrite_uid_text(write_uid_text);
        if(getCreate_uid_textDirtyFlag())
            srfdomain.setCreate_uid_text(create_uid_text);
        if(getWrite_uidDirtyFlag())
            srfdomain.setWrite_uid(write_uid);
        if(getCreate_uidDirtyFlag())
            srfdomain.setCreate_uid(create_uid);

        return srfdomain;
    }

    public void fromDO(Im_livechat_channel srfdomain) {
        if(srfdomain == null )
          return ;
        if(srfdomain.getUser_idsDirtyFlag())
            this.setUser_ids(srfdomain.getUser_ids());
        if(srfdomain.getWebsite_urlDirtyFlag())
            this.setWebsite_url(srfdomain.getWebsite_url());
        if(srfdomain.getRule_idsDirtyFlag())
            this.setRule_ids(srfdomain.getRule_ids());
        if(srfdomain.getImageDirtyFlag())
            this.setImage(srfdomain.getImage());
        if(srfdomain.getWeb_pageDirtyFlag())
            this.setWeb_page(srfdomain.getWeb_page());
        if(srfdomain.getIs_publishedDirtyFlag())
            this.setIs_published(srfdomain.getIs_published());
        if(srfdomain.getScript_externalDirtyFlag())
            this.setScript_external(srfdomain.getScript_external());
        if(srfdomain.getNbr_channelDirtyFlag())
            this.setNbr_channel(srfdomain.getNbr_channel());
        if(srfdomain.getButton_textDirtyFlag())
            this.setButton_text(srfdomain.getButton_text());
        if(srfdomain.getWebsite_descriptionDirtyFlag())
            this.setWebsite_description(srfdomain.getWebsite_description());
        if(srfdomain.getChannel_idsDirtyFlag())
            this.setChannel_ids(srfdomain.getChannel_ids());
        if(srfdomain.getNameDirtyFlag())
            this.setName(srfdomain.getName());
        if(srfdomain.getWebsite_publishedDirtyFlag())
            this.setWebsite_published(srfdomain.getWebsite_published());
        if(srfdomain.get__last_updateDirtyFlag())
            this.set__last_update(srfdomain.get__last_update());
        if(srfdomain.getCreate_dateDirtyFlag())
            this.setCreate_date(srfdomain.getCreate_date());
        if(srfdomain.getDisplay_nameDirtyFlag())
            this.setDisplay_name(srfdomain.getDisplay_name());
        if(srfdomain.getIdDirtyFlag())
            this.setId(srfdomain.getId());
        if(srfdomain.getImage_mediumDirtyFlag())
            this.setImage_medium(srfdomain.getImage_medium());
        if(srfdomain.getRating_percentage_satisfactionDirtyFlag())
            this.setRating_percentage_satisfaction(srfdomain.getRating_percentage_satisfaction());
        if(srfdomain.getInput_placeholderDirtyFlag())
            this.setInput_placeholder(srfdomain.getInput_placeholder());
        if(srfdomain.getImage_smallDirtyFlag())
            this.setImage_small(srfdomain.getImage_small());
        if(srfdomain.getAre_you_insideDirtyFlag())
            this.setAre_you_inside(srfdomain.getAre_you_inside());
        if(srfdomain.getDefault_messageDirtyFlag())
            this.setDefault_message(srfdomain.getDefault_message());
        if(srfdomain.getWrite_dateDirtyFlag())
            this.setWrite_date(srfdomain.getWrite_date());
        if(srfdomain.getWrite_uid_textDirtyFlag())
            this.setWrite_uid_text(srfdomain.getWrite_uid_text());
        if(srfdomain.getCreate_uid_textDirtyFlag())
            this.setCreate_uid_text(srfdomain.getCreate_uid_text());
        if(srfdomain.getWrite_uidDirtyFlag())
            this.setWrite_uid(srfdomain.getWrite_uid());
        if(srfdomain.getCreate_uidDirtyFlag())
            this.setCreate_uid(srfdomain.getCreate_uid());

    }

    public List<Im_livechat_channelDTO> fromDOPage(List<Im_livechat_channel> poPage)   {
        if(poPage == null)
            return null;
        List<Im_livechat_channelDTO> dtos=new ArrayList<Im_livechat_channelDTO>();
        for(Im_livechat_channel domain : poPage) {
            Im_livechat_channelDTO dto = new Im_livechat_channelDTO();
            dto.fromDO(domain);
            dtos.add(dto);
        }
        return dtos;
    }
}

