package cn.ibizlab.odoo.service.odoo_im_livechat.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.core.odoo_im_livechat.valuerule.anno.im_livechat_channel_rule.*;
import cn.ibizlab.odoo.core.odoo_im_livechat.domain.Im_livechat_channel_rule;
import org.springframework.cglib.beans.BeanCopier;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 服务DTO对象[Im_livechat_channel_ruleDTO]
 */
public class Im_livechat_channel_ruleDTO implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [SEQUENCE]
     *
     */
    @Im_livechat_channel_ruleSequenceDefault(info = "默认规则")
    private Integer sequence;

    @JsonIgnore
    private boolean sequenceDirtyFlag;

    /**
     * 属性 [AUTO_POPUP_TIMER]
     *
     */
    @Im_livechat_channel_ruleAuto_popup_timerDefault(info = "默认规则")
    private Integer auto_popup_timer;

    @JsonIgnore
    private boolean auto_popup_timerDirtyFlag;

    /**
     * 属性 [WRITE_DATE]
     *
     */
    @Im_livechat_channel_ruleWrite_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp write_date;

    @JsonIgnore
    private boolean write_dateDirtyFlag;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @Im_livechat_channel_ruleDisplay_nameDefault(info = "默认规则")
    private String display_name;

    @JsonIgnore
    private boolean display_nameDirtyFlag;

    /**
     * 属性 [REGEX_URL]
     *
     */
    @Im_livechat_channel_ruleRegex_urlDefault(info = "默认规则")
    private String regex_url;

    @JsonIgnore
    private boolean regex_urlDirtyFlag;

    /**
     * 属性 [ID]
     *
     */
    @Im_livechat_channel_ruleIdDefault(info = "默认规则")
    private Integer id;

    @JsonIgnore
    private boolean idDirtyFlag;

    /**
     * 属性 [ACTION]
     *
     */
    @Im_livechat_channel_ruleActionDefault(info = "默认规则")
    private String action;

    @JsonIgnore
    private boolean actionDirtyFlag;

    /**
     * 属性 [CREATE_DATE]
     *
     */
    @Im_livechat_channel_ruleCreate_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp create_date;

    @JsonIgnore
    private boolean create_dateDirtyFlag;

    /**
     * 属性 [COUNTRY_IDS]
     *
     */
    @Im_livechat_channel_ruleCountry_idsDefault(info = "默认规则")
    private String country_ids;

    @JsonIgnore
    private boolean country_idsDirtyFlag;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @Im_livechat_channel_rule__last_updateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp __last_update;

    @JsonIgnore
    private boolean __last_updateDirtyFlag;

    /**
     * 属性 [CREATE_UID_TEXT]
     *
     */
    @Im_livechat_channel_ruleCreate_uid_textDefault(info = "默认规则")
    private String create_uid_text;

    @JsonIgnore
    private boolean create_uid_textDirtyFlag;

    /**
     * 属性 [CHANNEL_ID_TEXT]
     *
     */
    @Im_livechat_channel_ruleChannel_id_textDefault(info = "默认规则")
    private String channel_id_text;

    @JsonIgnore
    private boolean channel_id_textDirtyFlag;

    /**
     * 属性 [WRITE_UID_TEXT]
     *
     */
    @Im_livechat_channel_ruleWrite_uid_textDefault(info = "默认规则")
    private String write_uid_text;

    @JsonIgnore
    private boolean write_uid_textDirtyFlag;

    /**
     * 属性 [CHANNEL_ID]
     *
     */
    @Im_livechat_channel_ruleChannel_idDefault(info = "默认规则")
    private Integer channel_id;

    @JsonIgnore
    private boolean channel_idDirtyFlag;

    /**
     * 属性 [CREATE_UID]
     *
     */
    @Im_livechat_channel_ruleCreate_uidDefault(info = "默认规则")
    private Integer create_uid;

    @JsonIgnore
    private boolean create_uidDirtyFlag;

    /**
     * 属性 [WRITE_UID]
     *
     */
    @Im_livechat_channel_ruleWrite_uidDefault(info = "默认规则")
    private Integer write_uid;

    @JsonIgnore
    private boolean write_uidDirtyFlag;


    /**
     * 获取 [SEQUENCE]
     */
    @JsonProperty("sequence")
    public Integer getSequence(){
        return sequence ;
    }

    /**
     * 设置 [SEQUENCE]
     */
    @JsonProperty("sequence")
    public void setSequence(Integer  sequence){
        this.sequence = sequence ;
        this.sequenceDirtyFlag = true ;
    }

    /**
     * 获取 [SEQUENCE]脏标记
     */
    @JsonIgnore
    public boolean getSequenceDirtyFlag(){
        return sequenceDirtyFlag ;
    }

    /**
     * 获取 [AUTO_POPUP_TIMER]
     */
    @JsonProperty("auto_popup_timer")
    public Integer getAuto_popup_timer(){
        return auto_popup_timer ;
    }

    /**
     * 设置 [AUTO_POPUP_TIMER]
     */
    @JsonProperty("auto_popup_timer")
    public void setAuto_popup_timer(Integer  auto_popup_timer){
        this.auto_popup_timer = auto_popup_timer ;
        this.auto_popup_timerDirtyFlag = true ;
    }

    /**
     * 获取 [AUTO_POPUP_TIMER]脏标记
     */
    @JsonIgnore
    public boolean getAuto_popup_timerDirtyFlag(){
        return auto_popup_timerDirtyFlag ;
    }

    /**
     * 获取 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return write_date ;
    }

    /**
     * 设置 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return write_dateDirtyFlag ;
    }

    /**
     * 获取 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return display_name ;
    }

    /**
     * 设置 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

    /**
     * 获取 [DISPLAY_NAME]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return display_nameDirtyFlag ;
    }

    /**
     * 获取 [REGEX_URL]
     */
    @JsonProperty("regex_url")
    public String getRegex_url(){
        return regex_url ;
    }

    /**
     * 设置 [REGEX_URL]
     */
    @JsonProperty("regex_url")
    public void setRegex_url(String  regex_url){
        this.regex_url = regex_url ;
        this.regex_urlDirtyFlag = true ;
    }

    /**
     * 获取 [REGEX_URL]脏标记
     */
    @JsonIgnore
    public boolean getRegex_urlDirtyFlag(){
        return regex_urlDirtyFlag ;
    }

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

    /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return idDirtyFlag ;
    }

    /**
     * 获取 [ACTION]
     */
    @JsonProperty("action")
    public String getAction(){
        return action ;
    }

    /**
     * 设置 [ACTION]
     */
    @JsonProperty("action")
    public void setAction(String  action){
        this.action = action ;
        this.actionDirtyFlag = true ;
    }

    /**
     * 获取 [ACTION]脏标记
     */
    @JsonIgnore
    public boolean getActionDirtyFlag(){
        return actionDirtyFlag ;
    }

    /**
     * 获取 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return create_date ;
    }

    /**
     * 设置 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return create_dateDirtyFlag ;
    }

    /**
     * 获取 [COUNTRY_IDS]
     */
    @JsonProperty("country_ids")
    public String getCountry_ids(){
        return country_ids ;
    }

    /**
     * 设置 [COUNTRY_IDS]
     */
    @JsonProperty("country_ids")
    public void setCountry_ids(String  country_ids){
        this.country_ids = country_ids ;
        this.country_idsDirtyFlag = true ;
    }

    /**
     * 获取 [COUNTRY_IDS]脏标记
     */
    @JsonIgnore
    public boolean getCountry_idsDirtyFlag(){
        return country_idsDirtyFlag ;
    }

    /**
     * 获取 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return __last_update ;
    }

    /**
     * 设置 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

    /**
     * 获取 [__LAST_UPDATE]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return __last_updateDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return create_uid_text ;
    }

    /**
     * 设置 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return create_uid_textDirtyFlag ;
    }

    /**
     * 获取 [CHANNEL_ID_TEXT]
     */
    @JsonProperty("channel_id_text")
    public String getChannel_id_text(){
        return channel_id_text ;
    }

    /**
     * 设置 [CHANNEL_ID_TEXT]
     */
    @JsonProperty("channel_id_text")
    public void setChannel_id_text(String  channel_id_text){
        this.channel_id_text = channel_id_text ;
        this.channel_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [CHANNEL_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getChannel_id_textDirtyFlag(){
        return channel_id_textDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return write_uid_text ;
    }

    /**
     * 设置 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return write_uid_textDirtyFlag ;
    }

    /**
     * 获取 [CHANNEL_ID]
     */
    @JsonProperty("channel_id")
    public Integer getChannel_id(){
        return channel_id ;
    }

    /**
     * 设置 [CHANNEL_ID]
     */
    @JsonProperty("channel_id")
    public void setChannel_id(Integer  channel_id){
        this.channel_id = channel_id ;
        this.channel_idDirtyFlag = true ;
    }

    /**
     * 获取 [CHANNEL_ID]脏标记
     */
    @JsonIgnore
    public boolean getChannel_idDirtyFlag(){
        return channel_idDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return create_uid ;
    }

    /**
     * 设置 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return create_uidDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return write_uid ;
    }

    /**
     * 设置 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return write_uidDirtyFlag ;
    }



    public Im_livechat_channel_rule toDO() {
        Im_livechat_channel_rule srfdomain = new Im_livechat_channel_rule();
        if(getSequenceDirtyFlag())
            srfdomain.setSequence(sequence);
        if(getAuto_popup_timerDirtyFlag())
            srfdomain.setAuto_popup_timer(auto_popup_timer);
        if(getWrite_dateDirtyFlag())
            srfdomain.setWrite_date(write_date);
        if(getDisplay_nameDirtyFlag())
            srfdomain.setDisplay_name(display_name);
        if(getRegex_urlDirtyFlag())
            srfdomain.setRegex_url(regex_url);
        if(getIdDirtyFlag())
            srfdomain.setId(id);
        if(getActionDirtyFlag())
            srfdomain.setAction(action);
        if(getCreate_dateDirtyFlag())
            srfdomain.setCreate_date(create_date);
        if(getCountry_idsDirtyFlag())
            srfdomain.setCountry_ids(country_ids);
        if(get__last_updateDirtyFlag())
            srfdomain.set__last_update(__last_update);
        if(getCreate_uid_textDirtyFlag())
            srfdomain.setCreate_uid_text(create_uid_text);
        if(getChannel_id_textDirtyFlag())
            srfdomain.setChannel_id_text(channel_id_text);
        if(getWrite_uid_textDirtyFlag())
            srfdomain.setWrite_uid_text(write_uid_text);
        if(getChannel_idDirtyFlag())
            srfdomain.setChannel_id(channel_id);
        if(getCreate_uidDirtyFlag())
            srfdomain.setCreate_uid(create_uid);
        if(getWrite_uidDirtyFlag())
            srfdomain.setWrite_uid(write_uid);

        return srfdomain;
    }

    public void fromDO(Im_livechat_channel_rule srfdomain) {
        if(srfdomain == null )
          return ;
        if(srfdomain.getSequenceDirtyFlag())
            this.setSequence(srfdomain.getSequence());
        if(srfdomain.getAuto_popup_timerDirtyFlag())
            this.setAuto_popup_timer(srfdomain.getAuto_popup_timer());
        if(srfdomain.getWrite_dateDirtyFlag())
            this.setWrite_date(srfdomain.getWrite_date());
        if(srfdomain.getDisplay_nameDirtyFlag())
            this.setDisplay_name(srfdomain.getDisplay_name());
        if(srfdomain.getRegex_urlDirtyFlag())
            this.setRegex_url(srfdomain.getRegex_url());
        if(srfdomain.getIdDirtyFlag())
            this.setId(srfdomain.getId());
        if(srfdomain.getActionDirtyFlag())
            this.setAction(srfdomain.getAction());
        if(srfdomain.getCreate_dateDirtyFlag())
            this.setCreate_date(srfdomain.getCreate_date());
        if(srfdomain.getCountry_idsDirtyFlag())
            this.setCountry_ids(srfdomain.getCountry_ids());
        if(srfdomain.get__last_updateDirtyFlag())
            this.set__last_update(srfdomain.get__last_update());
        if(srfdomain.getCreate_uid_textDirtyFlag())
            this.setCreate_uid_text(srfdomain.getCreate_uid_text());
        if(srfdomain.getChannel_id_textDirtyFlag())
            this.setChannel_id_text(srfdomain.getChannel_id_text());
        if(srfdomain.getWrite_uid_textDirtyFlag())
            this.setWrite_uid_text(srfdomain.getWrite_uid_text());
        if(srfdomain.getChannel_idDirtyFlag())
            this.setChannel_id(srfdomain.getChannel_id());
        if(srfdomain.getCreate_uidDirtyFlag())
            this.setCreate_uid(srfdomain.getCreate_uid());
        if(srfdomain.getWrite_uidDirtyFlag())
            this.setWrite_uid(srfdomain.getWrite_uid());

    }

    public List<Im_livechat_channel_ruleDTO> fromDOPage(List<Im_livechat_channel_rule> poPage)   {
        if(poPage == null)
            return null;
        List<Im_livechat_channel_ruleDTO> dtos=new ArrayList<Im_livechat_channel_ruleDTO>();
        for(Im_livechat_channel_rule domain : poPage) {
            Im_livechat_channel_ruleDTO dto = new Im_livechat_channel_ruleDTO();
            dto.fromDO(domain);
            dtos.add(dto);
        }
        return dtos;
    }
}

