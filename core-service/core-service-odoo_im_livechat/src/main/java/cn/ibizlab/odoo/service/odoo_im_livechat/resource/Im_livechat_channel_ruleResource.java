package cn.ibizlab.odoo.service.odoo_im_livechat.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_im_livechat.dto.Im_livechat_channel_ruleDTO;
import cn.ibizlab.odoo.core.odoo_im_livechat.domain.Im_livechat_channel_rule;
import cn.ibizlab.odoo.core.odoo_im_livechat.service.IIm_livechat_channel_ruleService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_im_livechat.filter.Im_livechat_channel_ruleSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Im_livechat_channel_rule" })
@RestController
@RequestMapping("")
public class Im_livechat_channel_ruleResource {

    @Autowired
    private IIm_livechat_channel_ruleService im_livechat_channel_ruleService;

    public IIm_livechat_channel_ruleService getIm_livechat_channel_ruleService() {
        return this.im_livechat_channel_ruleService;
    }

    @ApiOperation(value = "批更新数据", tags = {"Im_livechat_channel_rule" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_im_livechat/im_livechat_channel_rules/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Im_livechat_channel_ruleDTO> im_livechat_channel_ruledtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "获取数据", tags = {"Im_livechat_channel_rule" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_im_livechat/im_livechat_channel_rules/{im_livechat_channel_rule_id}")
    public ResponseEntity<Im_livechat_channel_ruleDTO> get(@PathVariable("im_livechat_channel_rule_id") Integer im_livechat_channel_rule_id) {
        Im_livechat_channel_ruleDTO dto = new Im_livechat_channel_ruleDTO();
        Im_livechat_channel_rule domain = im_livechat_channel_ruleService.get(im_livechat_channel_rule_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批建立数据", tags = {"Im_livechat_channel_rule" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_im_livechat/im_livechat_channel_rules/createBatch")
    public ResponseEntity<Boolean> createBatchIm_livechat_channel_rule(@RequestBody List<Im_livechat_channel_ruleDTO> im_livechat_channel_ruledtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "更新数据", tags = {"Im_livechat_channel_rule" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_im_livechat/im_livechat_channel_rules/{im_livechat_channel_rule_id}")

    public ResponseEntity<Im_livechat_channel_ruleDTO> update(@PathVariable("im_livechat_channel_rule_id") Integer im_livechat_channel_rule_id, @RequestBody Im_livechat_channel_ruleDTO im_livechat_channel_ruledto) {
		Im_livechat_channel_rule domain = im_livechat_channel_ruledto.toDO();
        domain.setId(im_livechat_channel_rule_id);
		im_livechat_channel_ruleService.update(domain);
		Im_livechat_channel_ruleDTO dto = new Im_livechat_channel_ruleDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "建立数据", tags = {"Im_livechat_channel_rule" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_im_livechat/im_livechat_channel_rules")

    public ResponseEntity<Im_livechat_channel_ruleDTO> create(@RequestBody Im_livechat_channel_ruleDTO im_livechat_channel_ruledto) {
        Im_livechat_channel_ruleDTO dto = new Im_livechat_channel_ruleDTO();
        Im_livechat_channel_rule domain = im_livechat_channel_ruledto.toDO();
		im_livechat_channel_ruleService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "删除数据", tags = {"Im_livechat_channel_rule" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_im_livechat/im_livechat_channel_rules/{im_livechat_channel_rule_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("im_livechat_channel_rule_id") Integer im_livechat_channel_rule_id) {
        Im_livechat_channel_ruleDTO im_livechat_channel_ruledto = new Im_livechat_channel_ruleDTO();
		Im_livechat_channel_rule domain = new Im_livechat_channel_rule();
		im_livechat_channel_ruledto.setId(im_livechat_channel_rule_id);
		domain.setId(im_livechat_channel_rule_id);
        Boolean rst = im_livechat_channel_ruleService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "批删除数据", tags = {"Im_livechat_channel_rule" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_im_livechat/im_livechat_channel_rules/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Im_livechat_channel_ruleDTO> im_livechat_channel_ruledtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Im_livechat_channel_rule" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_im_livechat/im_livechat_channel_rules/fetchdefault")
	public ResponseEntity<Page<Im_livechat_channel_ruleDTO>> fetchDefault(Im_livechat_channel_ruleSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Im_livechat_channel_ruleDTO> list = new ArrayList<Im_livechat_channel_ruleDTO>();
        
        Page<Im_livechat_channel_rule> domains = im_livechat_channel_ruleService.searchDefault(context) ;
        for(Im_livechat_channel_rule im_livechat_channel_rule : domains.getContent()){
            Im_livechat_channel_ruleDTO dto = new Im_livechat_channel_ruleDTO();
            dto.fromDO(im_livechat_channel_rule);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
