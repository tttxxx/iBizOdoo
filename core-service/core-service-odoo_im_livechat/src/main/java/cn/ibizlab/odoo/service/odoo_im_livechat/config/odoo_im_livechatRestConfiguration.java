package cn.ibizlab.odoo.service.odoo_im_livechat.config;

import org.springframework.context.annotation.ComponentScan;

@ComponentScan("cn.ibizlab.odoo.service.odoo_im_livechat")
public class odoo_im_livechatRestConfiguration {

}
