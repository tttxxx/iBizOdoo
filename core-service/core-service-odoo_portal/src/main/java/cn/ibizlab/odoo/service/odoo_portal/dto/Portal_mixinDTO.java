package cn.ibizlab.odoo.service.odoo_portal.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.core.odoo_portal.valuerule.anno.portal_mixin.*;
import cn.ibizlab.odoo.core.odoo_portal.domain.Portal_mixin;
import org.springframework.cglib.beans.BeanCopier;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 服务DTO对象[Portal_mixinDTO]
 */
public class Portal_mixinDTO implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [ACCESS_WARNING]
     *
     */
    @Portal_mixinAccess_warningDefault(info = "默认规则")
    private String access_warning;

    @JsonIgnore
    private boolean access_warningDirtyFlag;

    /**
     * 属性 [ID]
     *
     */
    @Portal_mixinIdDefault(info = "默认规则")
    private Integer id;

    @JsonIgnore
    private boolean idDirtyFlag;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @Portal_mixin__last_updateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp __last_update;

    @JsonIgnore
    private boolean __last_updateDirtyFlag;

    /**
     * 属性 [ACCESS_URL]
     *
     */
    @Portal_mixinAccess_urlDefault(info = "默认规则")
    private String access_url;

    @JsonIgnore
    private boolean access_urlDirtyFlag;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @Portal_mixinDisplay_nameDefault(info = "默认规则")
    private String display_name;

    @JsonIgnore
    private boolean display_nameDirtyFlag;

    /**
     * 属性 [ACCESS_TOKEN]
     *
     */
    @Portal_mixinAccess_tokenDefault(info = "默认规则")
    private String access_token;

    @JsonIgnore
    private boolean access_tokenDirtyFlag;


    /**
     * 获取 [ACCESS_WARNING]
     */
    @JsonProperty("access_warning")
    public String getAccess_warning(){
        return access_warning ;
    }

    /**
     * 设置 [ACCESS_WARNING]
     */
    @JsonProperty("access_warning")
    public void setAccess_warning(String  access_warning){
        this.access_warning = access_warning ;
        this.access_warningDirtyFlag = true ;
    }

    /**
     * 获取 [ACCESS_WARNING]脏标记
     */
    @JsonIgnore
    public boolean getAccess_warningDirtyFlag(){
        return access_warningDirtyFlag ;
    }

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

    /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return idDirtyFlag ;
    }

    /**
     * 获取 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return __last_update ;
    }

    /**
     * 设置 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

    /**
     * 获取 [__LAST_UPDATE]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return __last_updateDirtyFlag ;
    }

    /**
     * 获取 [ACCESS_URL]
     */
    @JsonProperty("access_url")
    public String getAccess_url(){
        return access_url ;
    }

    /**
     * 设置 [ACCESS_URL]
     */
    @JsonProperty("access_url")
    public void setAccess_url(String  access_url){
        this.access_url = access_url ;
        this.access_urlDirtyFlag = true ;
    }

    /**
     * 获取 [ACCESS_URL]脏标记
     */
    @JsonIgnore
    public boolean getAccess_urlDirtyFlag(){
        return access_urlDirtyFlag ;
    }

    /**
     * 获取 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return display_name ;
    }

    /**
     * 设置 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

    /**
     * 获取 [DISPLAY_NAME]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return display_nameDirtyFlag ;
    }

    /**
     * 获取 [ACCESS_TOKEN]
     */
    @JsonProperty("access_token")
    public String getAccess_token(){
        return access_token ;
    }

    /**
     * 设置 [ACCESS_TOKEN]
     */
    @JsonProperty("access_token")
    public void setAccess_token(String  access_token){
        this.access_token = access_token ;
        this.access_tokenDirtyFlag = true ;
    }

    /**
     * 获取 [ACCESS_TOKEN]脏标记
     */
    @JsonIgnore
    public boolean getAccess_tokenDirtyFlag(){
        return access_tokenDirtyFlag ;
    }



    public Portal_mixin toDO() {
        Portal_mixin srfdomain = new Portal_mixin();
        if(getAccess_warningDirtyFlag())
            srfdomain.setAccess_warning(access_warning);
        if(getIdDirtyFlag())
            srfdomain.setId(id);
        if(get__last_updateDirtyFlag())
            srfdomain.set__last_update(__last_update);
        if(getAccess_urlDirtyFlag())
            srfdomain.setAccess_url(access_url);
        if(getDisplay_nameDirtyFlag())
            srfdomain.setDisplay_name(display_name);
        if(getAccess_tokenDirtyFlag())
            srfdomain.setAccess_token(access_token);

        return srfdomain;
    }

    public void fromDO(Portal_mixin srfdomain) {
        if(srfdomain == null )
          return ;
        if(srfdomain.getAccess_warningDirtyFlag())
            this.setAccess_warning(srfdomain.getAccess_warning());
        if(srfdomain.getIdDirtyFlag())
            this.setId(srfdomain.getId());
        if(srfdomain.get__last_updateDirtyFlag())
            this.set__last_update(srfdomain.get__last_update());
        if(srfdomain.getAccess_urlDirtyFlag())
            this.setAccess_url(srfdomain.getAccess_url());
        if(srfdomain.getDisplay_nameDirtyFlag())
            this.setDisplay_name(srfdomain.getDisplay_name());
        if(srfdomain.getAccess_tokenDirtyFlag())
            this.setAccess_token(srfdomain.getAccess_token());

    }

    public List<Portal_mixinDTO> fromDOPage(List<Portal_mixin> poPage)   {
        if(poPage == null)
            return null;
        List<Portal_mixinDTO> dtos=new ArrayList<Portal_mixinDTO>();
        for(Portal_mixin domain : poPage) {
            Portal_mixinDTO dto = new Portal_mixinDTO();
            dto.fromDO(domain);
            dtos.add(dto);
        }
        return dtos;
    }
}

