package cn.ibizlab.odoo.service.odoo_portal.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_portal.dto.Portal_wizard_userDTO;
import cn.ibizlab.odoo.core.odoo_portal.domain.Portal_wizard_user;
import cn.ibizlab.odoo.core.odoo_portal.service.IPortal_wizard_userService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_portal.filter.Portal_wizard_userSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Portal_wizard_user" })
@RestController
@RequestMapping("")
public class Portal_wizard_userResource {

    @Autowired
    private IPortal_wizard_userService portal_wizard_userService;

    public IPortal_wizard_userService getPortal_wizard_userService() {
        return this.portal_wizard_userService;
    }

    @ApiOperation(value = "删除数据", tags = {"Portal_wizard_user" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_portal/portal_wizard_users/{portal_wizard_user_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("portal_wizard_user_id") Integer portal_wizard_user_id) {
        Portal_wizard_userDTO portal_wizard_userdto = new Portal_wizard_userDTO();
		Portal_wizard_user domain = new Portal_wizard_user();
		portal_wizard_userdto.setId(portal_wizard_user_id);
		domain.setId(portal_wizard_user_id);
        Boolean rst = portal_wizard_userService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "批更新数据", tags = {"Portal_wizard_user" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_portal/portal_wizard_users/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Portal_wizard_userDTO> portal_wizard_userdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "批删除数据", tags = {"Portal_wizard_user" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_portal/portal_wizard_users/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Portal_wizard_userDTO> portal_wizard_userdtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "批建立数据", tags = {"Portal_wizard_user" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_portal/portal_wizard_users/createBatch")
    public ResponseEntity<Boolean> createBatchPortal_wizard_user(@RequestBody List<Portal_wizard_userDTO> portal_wizard_userdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "建立数据", tags = {"Portal_wizard_user" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_portal/portal_wizard_users")

    public ResponseEntity<Portal_wizard_userDTO> create(@RequestBody Portal_wizard_userDTO portal_wizard_userdto) {
        Portal_wizard_userDTO dto = new Portal_wizard_userDTO();
        Portal_wizard_user domain = portal_wizard_userdto.toDO();
		portal_wizard_userService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取数据", tags = {"Portal_wizard_user" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_portal/portal_wizard_users/{portal_wizard_user_id}")
    public ResponseEntity<Portal_wizard_userDTO> get(@PathVariable("portal_wizard_user_id") Integer portal_wizard_user_id) {
        Portal_wizard_userDTO dto = new Portal_wizard_userDTO();
        Portal_wizard_user domain = portal_wizard_userService.get(portal_wizard_user_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "更新数据", tags = {"Portal_wizard_user" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_portal/portal_wizard_users/{portal_wizard_user_id}")

    public ResponseEntity<Portal_wizard_userDTO> update(@PathVariable("portal_wizard_user_id") Integer portal_wizard_user_id, @RequestBody Portal_wizard_userDTO portal_wizard_userdto) {
		Portal_wizard_user domain = portal_wizard_userdto.toDO();
        domain.setId(portal_wizard_user_id);
		portal_wizard_userService.update(domain);
		Portal_wizard_userDTO dto = new Portal_wizard_userDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Portal_wizard_user" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_portal/portal_wizard_users/fetchdefault")
	public ResponseEntity<Page<Portal_wizard_userDTO>> fetchDefault(Portal_wizard_userSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Portal_wizard_userDTO> list = new ArrayList<Portal_wizard_userDTO>();
        
        Page<Portal_wizard_user> domains = portal_wizard_userService.searchDefault(context) ;
        for(Portal_wizard_user portal_wizard_user : domains.getContent()){
            Portal_wizard_userDTO dto = new Portal_wizard_userDTO();
            dto.fromDO(portal_wizard_user);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
