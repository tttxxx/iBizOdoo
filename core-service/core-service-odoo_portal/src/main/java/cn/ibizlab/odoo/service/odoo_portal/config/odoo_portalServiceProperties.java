package cn.ibizlab.odoo.service.odoo_portal.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import lombok.Data;

@ConfigurationProperties(prefix = "service.odoo.portal")
@Data
public class odoo_portalServiceProperties {

	private boolean enabled;

	private boolean auth;


}