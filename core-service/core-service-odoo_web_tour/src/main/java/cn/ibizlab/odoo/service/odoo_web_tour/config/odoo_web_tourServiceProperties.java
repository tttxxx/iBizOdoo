package cn.ibizlab.odoo.service.odoo_web_tour.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import lombok.Data;

@ConfigurationProperties(prefix = "service.odoo.web.tour")
@Data
public class odoo_web_tourServiceProperties {

	private boolean enabled;

	private boolean auth;


}