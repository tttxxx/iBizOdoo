package cn.ibizlab.odoo.service.odoo_board.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_board.dto.Board_boardDTO;
import cn.ibizlab.odoo.core.odoo_board.domain.Board_board;
import cn.ibizlab.odoo.core.odoo_board.service.IBoard_boardService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_board.filter.Board_boardSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Board_board" })
@RestController
@RequestMapping("")
public class Board_boardResource {

    @Autowired
    private IBoard_boardService board_boardService;

    public IBoard_boardService getBoard_boardService() {
        return this.board_boardService;
    }

    @ApiOperation(value = "更新数据", tags = {"Board_board" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_board/board_boards/{board_board_id}")

    public ResponseEntity<Board_boardDTO> update(@PathVariable("board_board_id") Integer board_board_id, @RequestBody Board_boardDTO board_boarddto) {
		Board_board domain = board_boarddto.toDO();
        domain.setId(board_board_id);
		board_boardService.update(domain);
		Board_boardDTO dto = new Board_boardDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批删除数据", tags = {"Board_board" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_board/board_boards/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Board_boardDTO> board_boarddtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "批更新数据", tags = {"Board_board" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_board/board_boards/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Board_boardDTO> board_boarddtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "删除数据", tags = {"Board_board" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_board/board_boards/{board_board_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("board_board_id") Integer board_board_id) {
        Board_boardDTO board_boarddto = new Board_boardDTO();
		Board_board domain = new Board_board();
		board_boarddto.setId(board_board_id);
		domain.setId(board_board_id);
        Boolean rst = board_boardService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "建立数据", tags = {"Board_board" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_board/board_boards")

    public ResponseEntity<Board_boardDTO> create(@RequestBody Board_boardDTO board_boarddto) {
        Board_boardDTO dto = new Board_boardDTO();
        Board_board domain = board_boarddto.toDO();
		board_boardService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取数据", tags = {"Board_board" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_board/board_boards/{board_board_id}")
    public ResponseEntity<Board_boardDTO> get(@PathVariable("board_board_id") Integer board_board_id) {
        Board_boardDTO dto = new Board_boardDTO();
        Board_board domain = board_boardService.get(board_board_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批建立数据", tags = {"Board_board" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_board/board_boards/createBatch")
    public ResponseEntity<Boolean> createBatchBoard_board(@RequestBody List<Board_boardDTO> board_boarddtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Board_board" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_board/board_boards/fetchdefault")
	public ResponseEntity<Page<Board_boardDTO>> fetchDefault(Board_boardSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Board_boardDTO> list = new ArrayList<Board_boardDTO>();
        
        Page<Board_board> domains = board_boardService.searchDefault(context) ;
        for(Board_board board_board : domains.getContent()){
            Board_boardDTO dto = new Board_boardDTO();
            dto.fromDO(board_board);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
