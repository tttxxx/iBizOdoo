package cn.ibizlab.odoo.service.odoo_snailmail.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_snailmail.dto.Snailmail_letterDTO;
import cn.ibizlab.odoo.core.odoo_snailmail.domain.Snailmail_letter;
import cn.ibizlab.odoo.core.odoo_snailmail.service.ISnailmail_letterService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_snailmail.filter.Snailmail_letterSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Snailmail_letter" })
@RestController
@RequestMapping("")
public class Snailmail_letterResource {

    @Autowired
    private ISnailmail_letterService snailmail_letterService;

    public ISnailmail_letterService getSnailmail_letterService() {
        return this.snailmail_letterService;
    }

    @ApiOperation(value = "批建立数据", tags = {"Snailmail_letter" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_snailmail/snailmail_letters/createBatch")
    public ResponseEntity<Boolean> createBatchSnailmail_letter(@RequestBody List<Snailmail_letterDTO> snailmail_letterdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "删除数据", tags = {"Snailmail_letter" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_snailmail/snailmail_letters/{snailmail_letter_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("snailmail_letter_id") Integer snailmail_letter_id) {
        Snailmail_letterDTO snailmail_letterdto = new Snailmail_letterDTO();
		Snailmail_letter domain = new Snailmail_letter();
		snailmail_letterdto.setId(snailmail_letter_id);
		domain.setId(snailmail_letter_id);
        Boolean rst = snailmail_letterService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "批删除数据", tags = {"Snailmail_letter" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_snailmail/snailmail_letters/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Snailmail_letterDTO> snailmail_letterdtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "获取数据", tags = {"Snailmail_letter" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_snailmail/snailmail_letters/{snailmail_letter_id}")
    public ResponseEntity<Snailmail_letterDTO> get(@PathVariable("snailmail_letter_id") Integer snailmail_letter_id) {
        Snailmail_letterDTO dto = new Snailmail_letterDTO();
        Snailmail_letter domain = snailmail_letterService.get(snailmail_letter_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批更新数据", tags = {"Snailmail_letter" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_snailmail/snailmail_letters/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Snailmail_letterDTO> snailmail_letterdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "更新数据", tags = {"Snailmail_letter" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_snailmail/snailmail_letters/{snailmail_letter_id}")

    public ResponseEntity<Snailmail_letterDTO> update(@PathVariable("snailmail_letter_id") Integer snailmail_letter_id, @RequestBody Snailmail_letterDTO snailmail_letterdto) {
		Snailmail_letter domain = snailmail_letterdto.toDO();
        domain.setId(snailmail_letter_id);
		snailmail_letterService.update(domain);
		Snailmail_letterDTO dto = new Snailmail_letterDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "建立数据", tags = {"Snailmail_letter" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_snailmail/snailmail_letters")

    public ResponseEntity<Snailmail_letterDTO> create(@RequestBody Snailmail_letterDTO snailmail_letterdto) {
        Snailmail_letterDTO dto = new Snailmail_letterDTO();
        Snailmail_letter domain = snailmail_letterdto.toDO();
		snailmail_letterService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Snailmail_letter" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_snailmail/snailmail_letters/fetchdefault")
	public ResponseEntity<Page<Snailmail_letterDTO>> fetchDefault(Snailmail_letterSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Snailmail_letterDTO> list = new ArrayList<Snailmail_letterDTO>();
        
        Page<Snailmail_letter> domains = snailmail_letterService.searchDefault(context) ;
        for(Snailmail_letter snailmail_letter : domains.getContent()){
            Snailmail_letterDTO dto = new Snailmail_letterDTO();
            dto.fromDO(snailmail_letter);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
