package cn.ibizlab.odoo.service.odoo_snailmail.config;

import org.springframework.context.annotation.ComponentScan;

@ComponentScan("cn.ibizlab.odoo.service.odoo_snailmail")
public class odoo_snailmailRestConfiguration {

}
