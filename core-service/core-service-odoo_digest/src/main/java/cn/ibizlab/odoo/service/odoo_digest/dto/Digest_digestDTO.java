package cn.ibizlab.odoo.service.odoo_digest.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.core.odoo_digest.valuerule.anno.digest_digest.*;
import cn.ibizlab.odoo.core.odoo_digest.domain.Digest_digest;
import org.springframework.cglib.beans.BeanCopier;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 服务DTO对象[Digest_digestDTO]
 */
public class Digest_digestDTO implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [NAME]
     *
     */
    @Digest_digestNameDefault(info = "默认规则")
    private String name;

    @JsonIgnore
    private boolean nameDirtyFlag;

    /**
     * 属性 [KPI_WEBSITE_SALE_TOTAL_VALUE]
     *
     */
    @Digest_digestKpi_website_sale_total_valueDefault(info = "默认规则")
    private Double kpi_website_sale_total_value;

    @JsonIgnore
    private boolean kpi_website_sale_total_valueDirtyFlag;

    /**
     * 属性 [CREATE_DATE]
     *
     */
    @Digest_digestCreate_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp create_date;

    @JsonIgnore
    private boolean create_dateDirtyFlag;

    /**
     * 属性 [KPI_CRM_LEAD_CREATED_VALUE]
     *
     */
    @Digest_digestKpi_crm_lead_created_valueDefault(info = "默认规则")
    private Integer kpi_crm_lead_created_value;

    @JsonIgnore
    private boolean kpi_crm_lead_created_valueDirtyFlag;

    /**
     * 属性 [KPI_CRM_OPPORTUNITIES_WON]
     *
     */
    @Digest_digestKpi_crm_opportunities_wonDefault(info = "默认规则")
    private String kpi_crm_opportunities_won;

    @JsonIgnore
    private boolean kpi_crm_opportunities_wonDirtyFlag;

    /**
     * 属性 [AVAILABLE_FIELDS]
     *
     */
    @Digest_digestAvailable_fieldsDefault(info = "默认规则")
    private String available_fields;

    @JsonIgnore
    private boolean available_fieldsDirtyFlag;

    /**
     * 属性 [STATE]
     *
     */
    @Digest_digestStateDefault(info = "默认规则")
    private String state;

    @JsonIgnore
    private boolean stateDirtyFlag;

    /**
     * 属性 [USER_IDS]
     *
     */
    @Digest_digestUser_idsDefault(info = "默认规则")
    private String user_ids;

    @JsonIgnore
    private boolean user_idsDirtyFlag;

    /**
     * 属性 [KPI_CRM_OPPORTUNITIES_WON_VALUE]
     *
     */
    @Digest_digestKpi_crm_opportunities_won_valueDefault(info = "默认规则")
    private Integer kpi_crm_opportunities_won_value;

    @JsonIgnore
    private boolean kpi_crm_opportunities_won_valueDirtyFlag;

    /**
     * 属性 [KPI_HR_RECRUITMENT_NEW_COLLEAGUES]
     *
     */
    @Digest_digestKpi_hr_recruitment_new_colleaguesDefault(info = "默认规则")
    private String kpi_hr_recruitment_new_colleagues;

    @JsonIgnore
    private boolean kpi_hr_recruitment_new_colleaguesDirtyFlag;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @Digest_digest__last_updateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp __last_update;

    @JsonIgnore
    private boolean __last_updateDirtyFlag;

    /**
     * 属性 [KPI_POS_TOTAL]
     *
     */
    @Digest_digestKpi_pos_totalDefault(info = "默认规则")
    private String kpi_pos_total;

    @JsonIgnore
    private boolean kpi_pos_totalDirtyFlag;

    /**
     * 属性 [WRITE_DATE]
     *
     */
    @Digest_digestWrite_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp write_date;

    @JsonIgnore
    private boolean write_dateDirtyFlag;

    /**
     * 属性 [KPI_POS_TOTAL_VALUE]
     *
     */
    @Digest_digestKpi_pos_total_valueDefault(info = "默认规则")
    private Double kpi_pos_total_value;

    @JsonIgnore
    private boolean kpi_pos_total_valueDirtyFlag;

    /**
     * 属性 [KPI_WEBSITE_SALE_TOTAL]
     *
     */
    @Digest_digestKpi_website_sale_totalDefault(info = "默认规则")
    private String kpi_website_sale_total;

    @JsonIgnore
    private boolean kpi_website_sale_totalDirtyFlag;

    /**
     * 属性 [KPI_ALL_SALE_TOTAL]
     *
     */
    @Digest_digestKpi_all_sale_totalDefault(info = "默认规则")
    private String kpi_all_sale_total;

    @JsonIgnore
    private boolean kpi_all_sale_totalDirtyFlag;

    /**
     * 属性 [NEXT_RUN_DATE]
     *
     */
    @Digest_digestNext_run_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    private Timestamp next_run_date;

    @JsonIgnore
    private boolean next_run_dateDirtyFlag;

    /**
     * 属性 [KPI_RES_USERS_CONNECTED]
     *
     */
    @Digest_digestKpi_res_users_connectedDefault(info = "默认规则")
    private String kpi_res_users_connected;

    @JsonIgnore
    private boolean kpi_res_users_connectedDirtyFlag;

    /**
     * 属性 [KPI_RES_USERS_CONNECTED_VALUE]
     *
     */
    @Digest_digestKpi_res_users_connected_valueDefault(info = "默认规则")
    private Integer kpi_res_users_connected_value;

    @JsonIgnore
    private boolean kpi_res_users_connected_valueDirtyFlag;

    /**
     * 属性 [PERIODICITY]
     *
     */
    @Digest_digestPeriodicityDefault(info = "默认规则")
    private String periodicity;

    @JsonIgnore
    private boolean periodicityDirtyFlag;

    /**
     * 属性 [ID]
     *
     */
    @Digest_digestIdDefault(info = "默认规则")
    private Integer id;

    @JsonIgnore
    private boolean idDirtyFlag;

    /**
     * 属性 [KPI_HR_RECRUITMENT_NEW_COLLEAGUES_VALUE]
     *
     */
    @Digest_digestKpi_hr_recruitment_new_colleagues_valueDefault(info = "默认规则")
    private Integer kpi_hr_recruitment_new_colleagues_value;

    @JsonIgnore
    private boolean kpi_hr_recruitment_new_colleagues_valueDirtyFlag;

    /**
     * 属性 [KPI_ACCOUNT_TOTAL_REVENUE]
     *
     */
    @Digest_digestKpi_account_total_revenueDefault(info = "默认规则")
    private String kpi_account_total_revenue;

    @JsonIgnore
    private boolean kpi_account_total_revenueDirtyFlag;

    /**
     * 属性 [KPI_CRM_LEAD_CREATED]
     *
     */
    @Digest_digestKpi_crm_lead_createdDefault(info = "默认规则")
    private String kpi_crm_lead_created;

    @JsonIgnore
    private boolean kpi_crm_lead_createdDirtyFlag;

    /**
     * 属性 [KPI_PROJECT_TASK_OPENED]
     *
     */
    @Digest_digestKpi_project_task_openedDefault(info = "默认规则")
    private String kpi_project_task_opened;

    @JsonIgnore
    private boolean kpi_project_task_openedDirtyFlag;

    /**
     * 属性 [KPI_ALL_SALE_TOTAL_VALUE]
     *
     */
    @Digest_digestKpi_all_sale_total_valueDefault(info = "默认规则")
    private Double kpi_all_sale_total_value;

    @JsonIgnore
    private boolean kpi_all_sale_total_valueDirtyFlag;

    /**
     * 属性 [IS_SUBSCRIBED]
     *
     */
    @Digest_digestIs_subscribedDefault(info = "默认规则")
    private String is_subscribed;

    @JsonIgnore
    private boolean is_subscribedDirtyFlag;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @Digest_digestDisplay_nameDefault(info = "默认规则")
    private String display_name;

    @JsonIgnore
    private boolean display_nameDirtyFlag;

    /**
     * 属性 [KPI_PROJECT_TASK_OPENED_VALUE]
     *
     */
    @Digest_digestKpi_project_task_opened_valueDefault(info = "默认规则")
    private Integer kpi_project_task_opened_value;

    @JsonIgnore
    private boolean kpi_project_task_opened_valueDirtyFlag;

    /**
     * 属性 [KPI_ACCOUNT_TOTAL_REVENUE_VALUE]
     *
     */
    @Digest_digestKpi_account_total_revenue_valueDefault(info = "默认规则")
    private Double kpi_account_total_revenue_value;

    @JsonIgnore
    private boolean kpi_account_total_revenue_valueDirtyFlag;

    /**
     * 属性 [KPI_MAIL_MESSAGE_TOTAL_VALUE]
     *
     */
    @Digest_digestKpi_mail_message_total_valueDefault(info = "默认规则")
    private Integer kpi_mail_message_total_value;

    @JsonIgnore
    private boolean kpi_mail_message_total_valueDirtyFlag;

    /**
     * 属性 [KPI_MAIL_MESSAGE_TOTAL]
     *
     */
    @Digest_digestKpi_mail_message_totalDefault(info = "默认规则")
    private String kpi_mail_message_total;

    @JsonIgnore
    private boolean kpi_mail_message_totalDirtyFlag;

    /**
     * 属性 [WRITE_UID_TEXT]
     *
     */
    @Digest_digestWrite_uid_textDefault(info = "默认规则")
    private String write_uid_text;

    @JsonIgnore
    private boolean write_uid_textDirtyFlag;

    /**
     * 属性 [TEMPLATE_ID_TEXT]
     *
     */
    @Digest_digestTemplate_id_textDefault(info = "默认规则")
    private String template_id_text;

    @JsonIgnore
    private boolean template_id_textDirtyFlag;

    /**
     * 属性 [COMPANY_ID_TEXT]
     *
     */
    @Digest_digestCompany_id_textDefault(info = "默认规则")
    private String company_id_text;

    @JsonIgnore
    private boolean company_id_textDirtyFlag;

    /**
     * 属性 [CURRENCY_ID]
     *
     */
    @Digest_digestCurrency_idDefault(info = "默认规则")
    private Integer currency_id;

    @JsonIgnore
    private boolean currency_idDirtyFlag;

    /**
     * 属性 [CREATE_UID_TEXT]
     *
     */
    @Digest_digestCreate_uid_textDefault(info = "默认规则")
    private String create_uid_text;

    @JsonIgnore
    private boolean create_uid_textDirtyFlag;

    /**
     * 属性 [CREATE_UID]
     *
     */
    @Digest_digestCreate_uidDefault(info = "默认规则")
    private Integer create_uid;

    @JsonIgnore
    private boolean create_uidDirtyFlag;

    /**
     * 属性 [WRITE_UID]
     *
     */
    @Digest_digestWrite_uidDefault(info = "默认规则")
    private Integer write_uid;

    @JsonIgnore
    private boolean write_uidDirtyFlag;

    /**
     * 属性 [COMPANY_ID]
     *
     */
    @Digest_digestCompany_idDefault(info = "默认规则")
    private Integer company_id;

    @JsonIgnore
    private boolean company_idDirtyFlag;

    /**
     * 属性 [TEMPLATE_ID]
     *
     */
    @Digest_digestTemplate_idDefault(info = "默认规则")
    private Integer template_id;

    @JsonIgnore
    private boolean template_idDirtyFlag;


    /**
     * 获取 [NAME]
     */
    @JsonProperty("name")
    public String getName(){
        return name ;
    }

    /**
     * 设置 [NAME]
     */
    @JsonProperty("name")
    public void setName(String  name){
        this.name = name ;
        this.nameDirtyFlag = true ;
    }

    /**
     * 获取 [NAME]脏标记
     */
    @JsonIgnore
    public boolean getNameDirtyFlag(){
        return nameDirtyFlag ;
    }

    /**
     * 获取 [KPI_WEBSITE_SALE_TOTAL_VALUE]
     */
    @JsonProperty("kpi_website_sale_total_value")
    public Double getKpi_website_sale_total_value(){
        return kpi_website_sale_total_value ;
    }

    /**
     * 设置 [KPI_WEBSITE_SALE_TOTAL_VALUE]
     */
    @JsonProperty("kpi_website_sale_total_value")
    public void setKpi_website_sale_total_value(Double  kpi_website_sale_total_value){
        this.kpi_website_sale_total_value = kpi_website_sale_total_value ;
        this.kpi_website_sale_total_valueDirtyFlag = true ;
    }

    /**
     * 获取 [KPI_WEBSITE_SALE_TOTAL_VALUE]脏标记
     */
    @JsonIgnore
    public boolean getKpi_website_sale_total_valueDirtyFlag(){
        return kpi_website_sale_total_valueDirtyFlag ;
    }

    /**
     * 获取 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return create_date ;
    }

    /**
     * 设置 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return create_dateDirtyFlag ;
    }

    /**
     * 获取 [KPI_CRM_LEAD_CREATED_VALUE]
     */
    @JsonProperty("kpi_crm_lead_created_value")
    public Integer getKpi_crm_lead_created_value(){
        return kpi_crm_lead_created_value ;
    }

    /**
     * 设置 [KPI_CRM_LEAD_CREATED_VALUE]
     */
    @JsonProperty("kpi_crm_lead_created_value")
    public void setKpi_crm_lead_created_value(Integer  kpi_crm_lead_created_value){
        this.kpi_crm_lead_created_value = kpi_crm_lead_created_value ;
        this.kpi_crm_lead_created_valueDirtyFlag = true ;
    }

    /**
     * 获取 [KPI_CRM_LEAD_CREATED_VALUE]脏标记
     */
    @JsonIgnore
    public boolean getKpi_crm_lead_created_valueDirtyFlag(){
        return kpi_crm_lead_created_valueDirtyFlag ;
    }

    /**
     * 获取 [KPI_CRM_OPPORTUNITIES_WON]
     */
    @JsonProperty("kpi_crm_opportunities_won")
    public String getKpi_crm_opportunities_won(){
        return kpi_crm_opportunities_won ;
    }

    /**
     * 设置 [KPI_CRM_OPPORTUNITIES_WON]
     */
    @JsonProperty("kpi_crm_opportunities_won")
    public void setKpi_crm_opportunities_won(String  kpi_crm_opportunities_won){
        this.kpi_crm_opportunities_won = kpi_crm_opportunities_won ;
        this.kpi_crm_opportunities_wonDirtyFlag = true ;
    }

    /**
     * 获取 [KPI_CRM_OPPORTUNITIES_WON]脏标记
     */
    @JsonIgnore
    public boolean getKpi_crm_opportunities_wonDirtyFlag(){
        return kpi_crm_opportunities_wonDirtyFlag ;
    }

    /**
     * 获取 [AVAILABLE_FIELDS]
     */
    @JsonProperty("available_fields")
    public String getAvailable_fields(){
        return available_fields ;
    }

    /**
     * 设置 [AVAILABLE_FIELDS]
     */
    @JsonProperty("available_fields")
    public void setAvailable_fields(String  available_fields){
        this.available_fields = available_fields ;
        this.available_fieldsDirtyFlag = true ;
    }

    /**
     * 获取 [AVAILABLE_FIELDS]脏标记
     */
    @JsonIgnore
    public boolean getAvailable_fieldsDirtyFlag(){
        return available_fieldsDirtyFlag ;
    }

    /**
     * 获取 [STATE]
     */
    @JsonProperty("state")
    public String getState(){
        return state ;
    }

    /**
     * 设置 [STATE]
     */
    @JsonProperty("state")
    public void setState(String  state){
        this.state = state ;
        this.stateDirtyFlag = true ;
    }

    /**
     * 获取 [STATE]脏标记
     */
    @JsonIgnore
    public boolean getStateDirtyFlag(){
        return stateDirtyFlag ;
    }

    /**
     * 获取 [USER_IDS]
     */
    @JsonProperty("user_ids")
    public String getUser_ids(){
        return user_ids ;
    }

    /**
     * 设置 [USER_IDS]
     */
    @JsonProperty("user_ids")
    public void setUser_ids(String  user_ids){
        this.user_ids = user_ids ;
        this.user_idsDirtyFlag = true ;
    }

    /**
     * 获取 [USER_IDS]脏标记
     */
    @JsonIgnore
    public boolean getUser_idsDirtyFlag(){
        return user_idsDirtyFlag ;
    }

    /**
     * 获取 [KPI_CRM_OPPORTUNITIES_WON_VALUE]
     */
    @JsonProperty("kpi_crm_opportunities_won_value")
    public Integer getKpi_crm_opportunities_won_value(){
        return kpi_crm_opportunities_won_value ;
    }

    /**
     * 设置 [KPI_CRM_OPPORTUNITIES_WON_VALUE]
     */
    @JsonProperty("kpi_crm_opportunities_won_value")
    public void setKpi_crm_opportunities_won_value(Integer  kpi_crm_opportunities_won_value){
        this.kpi_crm_opportunities_won_value = kpi_crm_opportunities_won_value ;
        this.kpi_crm_opportunities_won_valueDirtyFlag = true ;
    }

    /**
     * 获取 [KPI_CRM_OPPORTUNITIES_WON_VALUE]脏标记
     */
    @JsonIgnore
    public boolean getKpi_crm_opportunities_won_valueDirtyFlag(){
        return kpi_crm_opportunities_won_valueDirtyFlag ;
    }

    /**
     * 获取 [KPI_HR_RECRUITMENT_NEW_COLLEAGUES]
     */
    @JsonProperty("kpi_hr_recruitment_new_colleagues")
    public String getKpi_hr_recruitment_new_colleagues(){
        return kpi_hr_recruitment_new_colleagues ;
    }

    /**
     * 设置 [KPI_HR_RECRUITMENT_NEW_COLLEAGUES]
     */
    @JsonProperty("kpi_hr_recruitment_new_colleagues")
    public void setKpi_hr_recruitment_new_colleagues(String  kpi_hr_recruitment_new_colleagues){
        this.kpi_hr_recruitment_new_colleagues = kpi_hr_recruitment_new_colleagues ;
        this.kpi_hr_recruitment_new_colleaguesDirtyFlag = true ;
    }

    /**
     * 获取 [KPI_HR_RECRUITMENT_NEW_COLLEAGUES]脏标记
     */
    @JsonIgnore
    public boolean getKpi_hr_recruitment_new_colleaguesDirtyFlag(){
        return kpi_hr_recruitment_new_colleaguesDirtyFlag ;
    }

    /**
     * 获取 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return __last_update ;
    }

    /**
     * 设置 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

    /**
     * 获取 [__LAST_UPDATE]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return __last_updateDirtyFlag ;
    }

    /**
     * 获取 [KPI_POS_TOTAL]
     */
    @JsonProperty("kpi_pos_total")
    public String getKpi_pos_total(){
        return kpi_pos_total ;
    }

    /**
     * 设置 [KPI_POS_TOTAL]
     */
    @JsonProperty("kpi_pos_total")
    public void setKpi_pos_total(String  kpi_pos_total){
        this.kpi_pos_total = kpi_pos_total ;
        this.kpi_pos_totalDirtyFlag = true ;
    }

    /**
     * 获取 [KPI_POS_TOTAL]脏标记
     */
    @JsonIgnore
    public boolean getKpi_pos_totalDirtyFlag(){
        return kpi_pos_totalDirtyFlag ;
    }

    /**
     * 获取 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return write_date ;
    }

    /**
     * 设置 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return write_dateDirtyFlag ;
    }

    /**
     * 获取 [KPI_POS_TOTAL_VALUE]
     */
    @JsonProperty("kpi_pos_total_value")
    public Double getKpi_pos_total_value(){
        return kpi_pos_total_value ;
    }

    /**
     * 设置 [KPI_POS_TOTAL_VALUE]
     */
    @JsonProperty("kpi_pos_total_value")
    public void setKpi_pos_total_value(Double  kpi_pos_total_value){
        this.kpi_pos_total_value = kpi_pos_total_value ;
        this.kpi_pos_total_valueDirtyFlag = true ;
    }

    /**
     * 获取 [KPI_POS_TOTAL_VALUE]脏标记
     */
    @JsonIgnore
    public boolean getKpi_pos_total_valueDirtyFlag(){
        return kpi_pos_total_valueDirtyFlag ;
    }

    /**
     * 获取 [KPI_WEBSITE_SALE_TOTAL]
     */
    @JsonProperty("kpi_website_sale_total")
    public String getKpi_website_sale_total(){
        return kpi_website_sale_total ;
    }

    /**
     * 设置 [KPI_WEBSITE_SALE_TOTAL]
     */
    @JsonProperty("kpi_website_sale_total")
    public void setKpi_website_sale_total(String  kpi_website_sale_total){
        this.kpi_website_sale_total = kpi_website_sale_total ;
        this.kpi_website_sale_totalDirtyFlag = true ;
    }

    /**
     * 获取 [KPI_WEBSITE_SALE_TOTAL]脏标记
     */
    @JsonIgnore
    public boolean getKpi_website_sale_totalDirtyFlag(){
        return kpi_website_sale_totalDirtyFlag ;
    }

    /**
     * 获取 [KPI_ALL_SALE_TOTAL]
     */
    @JsonProperty("kpi_all_sale_total")
    public String getKpi_all_sale_total(){
        return kpi_all_sale_total ;
    }

    /**
     * 设置 [KPI_ALL_SALE_TOTAL]
     */
    @JsonProperty("kpi_all_sale_total")
    public void setKpi_all_sale_total(String  kpi_all_sale_total){
        this.kpi_all_sale_total = kpi_all_sale_total ;
        this.kpi_all_sale_totalDirtyFlag = true ;
    }

    /**
     * 获取 [KPI_ALL_SALE_TOTAL]脏标记
     */
    @JsonIgnore
    public boolean getKpi_all_sale_totalDirtyFlag(){
        return kpi_all_sale_totalDirtyFlag ;
    }

    /**
     * 获取 [NEXT_RUN_DATE]
     */
    @JsonProperty("next_run_date")
    public Timestamp getNext_run_date(){
        return next_run_date ;
    }

    /**
     * 设置 [NEXT_RUN_DATE]
     */
    @JsonProperty("next_run_date")
    public void setNext_run_date(Timestamp  next_run_date){
        this.next_run_date = next_run_date ;
        this.next_run_dateDirtyFlag = true ;
    }

    /**
     * 获取 [NEXT_RUN_DATE]脏标记
     */
    @JsonIgnore
    public boolean getNext_run_dateDirtyFlag(){
        return next_run_dateDirtyFlag ;
    }

    /**
     * 获取 [KPI_RES_USERS_CONNECTED]
     */
    @JsonProperty("kpi_res_users_connected")
    public String getKpi_res_users_connected(){
        return kpi_res_users_connected ;
    }

    /**
     * 设置 [KPI_RES_USERS_CONNECTED]
     */
    @JsonProperty("kpi_res_users_connected")
    public void setKpi_res_users_connected(String  kpi_res_users_connected){
        this.kpi_res_users_connected = kpi_res_users_connected ;
        this.kpi_res_users_connectedDirtyFlag = true ;
    }

    /**
     * 获取 [KPI_RES_USERS_CONNECTED]脏标记
     */
    @JsonIgnore
    public boolean getKpi_res_users_connectedDirtyFlag(){
        return kpi_res_users_connectedDirtyFlag ;
    }

    /**
     * 获取 [KPI_RES_USERS_CONNECTED_VALUE]
     */
    @JsonProperty("kpi_res_users_connected_value")
    public Integer getKpi_res_users_connected_value(){
        return kpi_res_users_connected_value ;
    }

    /**
     * 设置 [KPI_RES_USERS_CONNECTED_VALUE]
     */
    @JsonProperty("kpi_res_users_connected_value")
    public void setKpi_res_users_connected_value(Integer  kpi_res_users_connected_value){
        this.kpi_res_users_connected_value = kpi_res_users_connected_value ;
        this.kpi_res_users_connected_valueDirtyFlag = true ;
    }

    /**
     * 获取 [KPI_RES_USERS_CONNECTED_VALUE]脏标记
     */
    @JsonIgnore
    public boolean getKpi_res_users_connected_valueDirtyFlag(){
        return kpi_res_users_connected_valueDirtyFlag ;
    }

    /**
     * 获取 [PERIODICITY]
     */
    @JsonProperty("periodicity")
    public String getPeriodicity(){
        return periodicity ;
    }

    /**
     * 设置 [PERIODICITY]
     */
    @JsonProperty("periodicity")
    public void setPeriodicity(String  periodicity){
        this.periodicity = periodicity ;
        this.periodicityDirtyFlag = true ;
    }

    /**
     * 获取 [PERIODICITY]脏标记
     */
    @JsonIgnore
    public boolean getPeriodicityDirtyFlag(){
        return periodicityDirtyFlag ;
    }

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

    /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return idDirtyFlag ;
    }

    /**
     * 获取 [KPI_HR_RECRUITMENT_NEW_COLLEAGUES_VALUE]
     */
    @JsonProperty("kpi_hr_recruitment_new_colleagues_value")
    public Integer getKpi_hr_recruitment_new_colleagues_value(){
        return kpi_hr_recruitment_new_colleagues_value ;
    }

    /**
     * 设置 [KPI_HR_RECRUITMENT_NEW_COLLEAGUES_VALUE]
     */
    @JsonProperty("kpi_hr_recruitment_new_colleagues_value")
    public void setKpi_hr_recruitment_new_colleagues_value(Integer  kpi_hr_recruitment_new_colleagues_value){
        this.kpi_hr_recruitment_new_colleagues_value = kpi_hr_recruitment_new_colleagues_value ;
        this.kpi_hr_recruitment_new_colleagues_valueDirtyFlag = true ;
    }

    /**
     * 获取 [KPI_HR_RECRUITMENT_NEW_COLLEAGUES_VALUE]脏标记
     */
    @JsonIgnore
    public boolean getKpi_hr_recruitment_new_colleagues_valueDirtyFlag(){
        return kpi_hr_recruitment_new_colleagues_valueDirtyFlag ;
    }

    /**
     * 获取 [KPI_ACCOUNT_TOTAL_REVENUE]
     */
    @JsonProperty("kpi_account_total_revenue")
    public String getKpi_account_total_revenue(){
        return kpi_account_total_revenue ;
    }

    /**
     * 设置 [KPI_ACCOUNT_TOTAL_REVENUE]
     */
    @JsonProperty("kpi_account_total_revenue")
    public void setKpi_account_total_revenue(String  kpi_account_total_revenue){
        this.kpi_account_total_revenue = kpi_account_total_revenue ;
        this.kpi_account_total_revenueDirtyFlag = true ;
    }

    /**
     * 获取 [KPI_ACCOUNT_TOTAL_REVENUE]脏标记
     */
    @JsonIgnore
    public boolean getKpi_account_total_revenueDirtyFlag(){
        return kpi_account_total_revenueDirtyFlag ;
    }

    /**
     * 获取 [KPI_CRM_LEAD_CREATED]
     */
    @JsonProperty("kpi_crm_lead_created")
    public String getKpi_crm_lead_created(){
        return kpi_crm_lead_created ;
    }

    /**
     * 设置 [KPI_CRM_LEAD_CREATED]
     */
    @JsonProperty("kpi_crm_lead_created")
    public void setKpi_crm_lead_created(String  kpi_crm_lead_created){
        this.kpi_crm_lead_created = kpi_crm_lead_created ;
        this.kpi_crm_lead_createdDirtyFlag = true ;
    }

    /**
     * 获取 [KPI_CRM_LEAD_CREATED]脏标记
     */
    @JsonIgnore
    public boolean getKpi_crm_lead_createdDirtyFlag(){
        return kpi_crm_lead_createdDirtyFlag ;
    }

    /**
     * 获取 [KPI_PROJECT_TASK_OPENED]
     */
    @JsonProperty("kpi_project_task_opened")
    public String getKpi_project_task_opened(){
        return kpi_project_task_opened ;
    }

    /**
     * 设置 [KPI_PROJECT_TASK_OPENED]
     */
    @JsonProperty("kpi_project_task_opened")
    public void setKpi_project_task_opened(String  kpi_project_task_opened){
        this.kpi_project_task_opened = kpi_project_task_opened ;
        this.kpi_project_task_openedDirtyFlag = true ;
    }

    /**
     * 获取 [KPI_PROJECT_TASK_OPENED]脏标记
     */
    @JsonIgnore
    public boolean getKpi_project_task_openedDirtyFlag(){
        return kpi_project_task_openedDirtyFlag ;
    }

    /**
     * 获取 [KPI_ALL_SALE_TOTAL_VALUE]
     */
    @JsonProperty("kpi_all_sale_total_value")
    public Double getKpi_all_sale_total_value(){
        return kpi_all_sale_total_value ;
    }

    /**
     * 设置 [KPI_ALL_SALE_TOTAL_VALUE]
     */
    @JsonProperty("kpi_all_sale_total_value")
    public void setKpi_all_sale_total_value(Double  kpi_all_sale_total_value){
        this.kpi_all_sale_total_value = kpi_all_sale_total_value ;
        this.kpi_all_sale_total_valueDirtyFlag = true ;
    }

    /**
     * 获取 [KPI_ALL_SALE_TOTAL_VALUE]脏标记
     */
    @JsonIgnore
    public boolean getKpi_all_sale_total_valueDirtyFlag(){
        return kpi_all_sale_total_valueDirtyFlag ;
    }

    /**
     * 获取 [IS_SUBSCRIBED]
     */
    @JsonProperty("is_subscribed")
    public String getIs_subscribed(){
        return is_subscribed ;
    }

    /**
     * 设置 [IS_SUBSCRIBED]
     */
    @JsonProperty("is_subscribed")
    public void setIs_subscribed(String  is_subscribed){
        this.is_subscribed = is_subscribed ;
        this.is_subscribedDirtyFlag = true ;
    }

    /**
     * 获取 [IS_SUBSCRIBED]脏标记
     */
    @JsonIgnore
    public boolean getIs_subscribedDirtyFlag(){
        return is_subscribedDirtyFlag ;
    }

    /**
     * 获取 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return display_name ;
    }

    /**
     * 设置 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

    /**
     * 获取 [DISPLAY_NAME]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return display_nameDirtyFlag ;
    }

    /**
     * 获取 [KPI_PROJECT_TASK_OPENED_VALUE]
     */
    @JsonProperty("kpi_project_task_opened_value")
    public Integer getKpi_project_task_opened_value(){
        return kpi_project_task_opened_value ;
    }

    /**
     * 设置 [KPI_PROJECT_TASK_OPENED_VALUE]
     */
    @JsonProperty("kpi_project_task_opened_value")
    public void setKpi_project_task_opened_value(Integer  kpi_project_task_opened_value){
        this.kpi_project_task_opened_value = kpi_project_task_opened_value ;
        this.kpi_project_task_opened_valueDirtyFlag = true ;
    }

    /**
     * 获取 [KPI_PROJECT_TASK_OPENED_VALUE]脏标记
     */
    @JsonIgnore
    public boolean getKpi_project_task_opened_valueDirtyFlag(){
        return kpi_project_task_opened_valueDirtyFlag ;
    }

    /**
     * 获取 [KPI_ACCOUNT_TOTAL_REVENUE_VALUE]
     */
    @JsonProperty("kpi_account_total_revenue_value")
    public Double getKpi_account_total_revenue_value(){
        return kpi_account_total_revenue_value ;
    }

    /**
     * 设置 [KPI_ACCOUNT_TOTAL_REVENUE_VALUE]
     */
    @JsonProperty("kpi_account_total_revenue_value")
    public void setKpi_account_total_revenue_value(Double  kpi_account_total_revenue_value){
        this.kpi_account_total_revenue_value = kpi_account_total_revenue_value ;
        this.kpi_account_total_revenue_valueDirtyFlag = true ;
    }

    /**
     * 获取 [KPI_ACCOUNT_TOTAL_REVENUE_VALUE]脏标记
     */
    @JsonIgnore
    public boolean getKpi_account_total_revenue_valueDirtyFlag(){
        return kpi_account_total_revenue_valueDirtyFlag ;
    }

    /**
     * 获取 [KPI_MAIL_MESSAGE_TOTAL_VALUE]
     */
    @JsonProperty("kpi_mail_message_total_value")
    public Integer getKpi_mail_message_total_value(){
        return kpi_mail_message_total_value ;
    }

    /**
     * 设置 [KPI_MAIL_MESSAGE_TOTAL_VALUE]
     */
    @JsonProperty("kpi_mail_message_total_value")
    public void setKpi_mail_message_total_value(Integer  kpi_mail_message_total_value){
        this.kpi_mail_message_total_value = kpi_mail_message_total_value ;
        this.kpi_mail_message_total_valueDirtyFlag = true ;
    }

    /**
     * 获取 [KPI_MAIL_MESSAGE_TOTAL_VALUE]脏标记
     */
    @JsonIgnore
    public boolean getKpi_mail_message_total_valueDirtyFlag(){
        return kpi_mail_message_total_valueDirtyFlag ;
    }

    /**
     * 获取 [KPI_MAIL_MESSAGE_TOTAL]
     */
    @JsonProperty("kpi_mail_message_total")
    public String getKpi_mail_message_total(){
        return kpi_mail_message_total ;
    }

    /**
     * 设置 [KPI_MAIL_MESSAGE_TOTAL]
     */
    @JsonProperty("kpi_mail_message_total")
    public void setKpi_mail_message_total(String  kpi_mail_message_total){
        this.kpi_mail_message_total = kpi_mail_message_total ;
        this.kpi_mail_message_totalDirtyFlag = true ;
    }

    /**
     * 获取 [KPI_MAIL_MESSAGE_TOTAL]脏标记
     */
    @JsonIgnore
    public boolean getKpi_mail_message_totalDirtyFlag(){
        return kpi_mail_message_totalDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return write_uid_text ;
    }

    /**
     * 设置 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return write_uid_textDirtyFlag ;
    }

    /**
     * 获取 [TEMPLATE_ID_TEXT]
     */
    @JsonProperty("template_id_text")
    public String getTemplate_id_text(){
        return template_id_text ;
    }

    /**
     * 设置 [TEMPLATE_ID_TEXT]
     */
    @JsonProperty("template_id_text")
    public void setTemplate_id_text(String  template_id_text){
        this.template_id_text = template_id_text ;
        this.template_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [TEMPLATE_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getTemplate_id_textDirtyFlag(){
        return template_id_textDirtyFlag ;
    }

    /**
     * 获取 [COMPANY_ID_TEXT]
     */
    @JsonProperty("company_id_text")
    public String getCompany_id_text(){
        return company_id_text ;
    }

    /**
     * 设置 [COMPANY_ID_TEXT]
     */
    @JsonProperty("company_id_text")
    public void setCompany_id_text(String  company_id_text){
        this.company_id_text = company_id_text ;
        this.company_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [COMPANY_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getCompany_id_textDirtyFlag(){
        return company_id_textDirtyFlag ;
    }

    /**
     * 获取 [CURRENCY_ID]
     */
    @JsonProperty("currency_id")
    public Integer getCurrency_id(){
        return currency_id ;
    }

    /**
     * 设置 [CURRENCY_ID]
     */
    @JsonProperty("currency_id")
    public void setCurrency_id(Integer  currency_id){
        this.currency_id = currency_id ;
        this.currency_idDirtyFlag = true ;
    }

    /**
     * 获取 [CURRENCY_ID]脏标记
     */
    @JsonIgnore
    public boolean getCurrency_idDirtyFlag(){
        return currency_idDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return create_uid_text ;
    }

    /**
     * 设置 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return create_uid_textDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return create_uid ;
    }

    /**
     * 设置 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return create_uidDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return write_uid ;
    }

    /**
     * 设置 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return write_uidDirtyFlag ;
    }

    /**
     * 获取 [COMPANY_ID]
     */
    @JsonProperty("company_id")
    public Integer getCompany_id(){
        return company_id ;
    }

    /**
     * 设置 [COMPANY_ID]
     */
    @JsonProperty("company_id")
    public void setCompany_id(Integer  company_id){
        this.company_id = company_id ;
        this.company_idDirtyFlag = true ;
    }

    /**
     * 获取 [COMPANY_ID]脏标记
     */
    @JsonIgnore
    public boolean getCompany_idDirtyFlag(){
        return company_idDirtyFlag ;
    }

    /**
     * 获取 [TEMPLATE_ID]
     */
    @JsonProperty("template_id")
    public Integer getTemplate_id(){
        return template_id ;
    }

    /**
     * 设置 [TEMPLATE_ID]
     */
    @JsonProperty("template_id")
    public void setTemplate_id(Integer  template_id){
        this.template_id = template_id ;
        this.template_idDirtyFlag = true ;
    }

    /**
     * 获取 [TEMPLATE_ID]脏标记
     */
    @JsonIgnore
    public boolean getTemplate_idDirtyFlag(){
        return template_idDirtyFlag ;
    }



    public Digest_digest toDO() {
        Digest_digest srfdomain = new Digest_digest();
        if(getNameDirtyFlag())
            srfdomain.setName(name);
        if(getKpi_website_sale_total_valueDirtyFlag())
            srfdomain.setKpi_website_sale_total_value(kpi_website_sale_total_value);
        if(getCreate_dateDirtyFlag())
            srfdomain.setCreate_date(create_date);
        if(getKpi_crm_lead_created_valueDirtyFlag())
            srfdomain.setKpi_crm_lead_created_value(kpi_crm_lead_created_value);
        if(getKpi_crm_opportunities_wonDirtyFlag())
            srfdomain.setKpi_crm_opportunities_won(kpi_crm_opportunities_won);
        if(getAvailable_fieldsDirtyFlag())
            srfdomain.setAvailable_fields(available_fields);
        if(getStateDirtyFlag())
            srfdomain.setState(state);
        if(getUser_idsDirtyFlag())
            srfdomain.setUser_ids(user_ids);
        if(getKpi_crm_opportunities_won_valueDirtyFlag())
            srfdomain.setKpi_crm_opportunities_won_value(kpi_crm_opportunities_won_value);
        if(getKpi_hr_recruitment_new_colleaguesDirtyFlag())
            srfdomain.setKpi_hr_recruitment_new_colleagues(kpi_hr_recruitment_new_colleagues);
        if(get__last_updateDirtyFlag())
            srfdomain.set__last_update(__last_update);
        if(getKpi_pos_totalDirtyFlag())
            srfdomain.setKpi_pos_total(kpi_pos_total);
        if(getWrite_dateDirtyFlag())
            srfdomain.setWrite_date(write_date);
        if(getKpi_pos_total_valueDirtyFlag())
            srfdomain.setKpi_pos_total_value(kpi_pos_total_value);
        if(getKpi_website_sale_totalDirtyFlag())
            srfdomain.setKpi_website_sale_total(kpi_website_sale_total);
        if(getKpi_all_sale_totalDirtyFlag())
            srfdomain.setKpi_all_sale_total(kpi_all_sale_total);
        if(getNext_run_dateDirtyFlag())
            srfdomain.setNext_run_date(next_run_date);
        if(getKpi_res_users_connectedDirtyFlag())
            srfdomain.setKpi_res_users_connected(kpi_res_users_connected);
        if(getKpi_res_users_connected_valueDirtyFlag())
            srfdomain.setKpi_res_users_connected_value(kpi_res_users_connected_value);
        if(getPeriodicityDirtyFlag())
            srfdomain.setPeriodicity(periodicity);
        if(getIdDirtyFlag())
            srfdomain.setId(id);
        if(getKpi_hr_recruitment_new_colleagues_valueDirtyFlag())
            srfdomain.setKpi_hr_recruitment_new_colleagues_value(kpi_hr_recruitment_new_colleagues_value);
        if(getKpi_account_total_revenueDirtyFlag())
            srfdomain.setKpi_account_total_revenue(kpi_account_total_revenue);
        if(getKpi_crm_lead_createdDirtyFlag())
            srfdomain.setKpi_crm_lead_created(kpi_crm_lead_created);
        if(getKpi_project_task_openedDirtyFlag())
            srfdomain.setKpi_project_task_opened(kpi_project_task_opened);
        if(getKpi_all_sale_total_valueDirtyFlag())
            srfdomain.setKpi_all_sale_total_value(kpi_all_sale_total_value);
        if(getIs_subscribedDirtyFlag())
            srfdomain.setIs_subscribed(is_subscribed);
        if(getDisplay_nameDirtyFlag())
            srfdomain.setDisplay_name(display_name);
        if(getKpi_project_task_opened_valueDirtyFlag())
            srfdomain.setKpi_project_task_opened_value(kpi_project_task_opened_value);
        if(getKpi_account_total_revenue_valueDirtyFlag())
            srfdomain.setKpi_account_total_revenue_value(kpi_account_total_revenue_value);
        if(getKpi_mail_message_total_valueDirtyFlag())
            srfdomain.setKpi_mail_message_total_value(kpi_mail_message_total_value);
        if(getKpi_mail_message_totalDirtyFlag())
            srfdomain.setKpi_mail_message_total(kpi_mail_message_total);
        if(getWrite_uid_textDirtyFlag())
            srfdomain.setWrite_uid_text(write_uid_text);
        if(getTemplate_id_textDirtyFlag())
            srfdomain.setTemplate_id_text(template_id_text);
        if(getCompany_id_textDirtyFlag())
            srfdomain.setCompany_id_text(company_id_text);
        if(getCurrency_idDirtyFlag())
            srfdomain.setCurrency_id(currency_id);
        if(getCreate_uid_textDirtyFlag())
            srfdomain.setCreate_uid_text(create_uid_text);
        if(getCreate_uidDirtyFlag())
            srfdomain.setCreate_uid(create_uid);
        if(getWrite_uidDirtyFlag())
            srfdomain.setWrite_uid(write_uid);
        if(getCompany_idDirtyFlag())
            srfdomain.setCompany_id(company_id);
        if(getTemplate_idDirtyFlag())
            srfdomain.setTemplate_id(template_id);

        return srfdomain;
    }

    public void fromDO(Digest_digest srfdomain) {
        if(srfdomain == null )
          return ;
        if(srfdomain.getNameDirtyFlag())
            this.setName(srfdomain.getName());
        if(srfdomain.getKpi_website_sale_total_valueDirtyFlag())
            this.setKpi_website_sale_total_value(srfdomain.getKpi_website_sale_total_value());
        if(srfdomain.getCreate_dateDirtyFlag())
            this.setCreate_date(srfdomain.getCreate_date());
        if(srfdomain.getKpi_crm_lead_created_valueDirtyFlag())
            this.setKpi_crm_lead_created_value(srfdomain.getKpi_crm_lead_created_value());
        if(srfdomain.getKpi_crm_opportunities_wonDirtyFlag())
            this.setKpi_crm_opportunities_won(srfdomain.getKpi_crm_opportunities_won());
        if(srfdomain.getAvailable_fieldsDirtyFlag())
            this.setAvailable_fields(srfdomain.getAvailable_fields());
        if(srfdomain.getStateDirtyFlag())
            this.setState(srfdomain.getState());
        if(srfdomain.getUser_idsDirtyFlag())
            this.setUser_ids(srfdomain.getUser_ids());
        if(srfdomain.getKpi_crm_opportunities_won_valueDirtyFlag())
            this.setKpi_crm_opportunities_won_value(srfdomain.getKpi_crm_opportunities_won_value());
        if(srfdomain.getKpi_hr_recruitment_new_colleaguesDirtyFlag())
            this.setKpi_hr_recruitment_new_colleagues(srfdomain.getKpi_hr_recruitment_new_colleagues());
        if(srfdomain.get__last_updateDirtyFlag())
            this.set__last_update(srfdomain.get__last_update());
        if(srfdomain.getKpi_pos_totalDirtyFlag())
            this.setKpi_pos_total(srfdomain.getKpi_pos_total());
        if(srfdomain.getWrite_dateDirtyFlag())
            this.setWrite_date(srfdomain.getWrite_date());
        if(srfdomain.getKpi_pos_total_valueDirtyFlag())
            this.setKpi_pos_total_value(srfdomain.getKpi_pos_total_value());
        if(srfdomain.getKpi_website_sale_totalDirtyFlag())
            this.setKpi_website_sale_total(srfdomain.getKpi_website_sale_total());
        if(srfdomain.getKpi_all_sale_totalDirtyFlag())
            this.setKpi_all_sale_total(srfdomain.getKpi_all_sale_total());
        if(srfdomain.getNext_run_dateDirtyFlag())
            this.setNext_run_date(srfdomain.getNext_run_date());
        if(srfdomain.getKpi_res_users_connectedDirtyFlag())
            this.setKpi_res_users_connected(srfdomain.getKpi_res_users_connected());
        if(srfdomain.getKpi_res_users_connected_valueDirtyFlag())
            this.setKpi_res_users_connected_value(srfdomain.getKpi_res_users_connected_value());
        if(srfdomain.getPeriodicityDirtyFlag())
            this.setPeriodicity(srfdomain.getPeriodicity());
        if(srfdomain.getIdDirtyFlag())
            this.setId(srfdomain.getId());
        if(srfdomain.getKpi_hr_recruitment_new_colleagues_valueDirtyFlag())
            this.setKpi_hr_recruitment_new_colleagues_value(srfdomain.getKpi_hr_recruitment_new_colleagues_value());
        if(srfdomain.getKpi_account_total_revenueDirtyFlag())
            this.setKpi_account_total_revenue(srfdomain.getKpi_account_total_revenue());
        if(srfdomain.getKpi_crm_lead_createdDirtyFlag())
            this.setKpi_crm_lead_created(srfdomain.getKpi_crm_lead_created());
        if(srfdomain.getKpi_project_task_openedDirtyFlag())
            this.setKpi_project_task_opened(srfdomain.getKpi_project_task_opened());
        if(srfdomain.getKpi_all_sale_total_valueDirtyFlag())
            this.setKpi_all_sale_total_value(srfdomain.getKpi_all_sale_total_value());
        if(srfdomain.getIs_subscribedDirtyFlag())
            this.setIs_subscribed(srfdomain.getIs_subscribed());
        if(srfdomain.getDisplay_nameDirtyFlag())
            this.setDisplay_name(srfdomain.getDisplay_name());
        if(srfdomain.getKpi_project_task_opened_valueDirtyFlag())
            this.setKpi_project_task_opened_value(srfdomain.getKpi_project_task_opened_value());
        if(srfdomain.getKpi_account_total_revenue_valueDirtyFlag())
            this.setKpi_account_total_revenue_value(srfdomain.getKpi_account_total_revenue_value());
        if(srfdomain.getKpi_mail_message_total_valueDirtyFlag())
            this.setKpi_mail_message_total_value(srfdomain.getKpi_mail_message_total_value());
        if(srfdomain.getKpi_mail_message_totalDirtyFlag())
            this.setKpi_mail_message_total(srfdomain.getKpi_mail_message_total());
        if(srfdomain.getWrite_uid_textDirtyFlag())
            this.setWrite_uid_text(srfdomain.getWrite_uid_text());
        if(srfdomain.getTemplate_id_textDirtyFlag())
            this.setTemplate_id_text(srfdomain.getTemplate_id_text());
        if(srfdomain.getCompany_id_textDirtyFlag())
            this.setCompany_id_text(srfdomain.getCompany_id_text());
        if(srfdomain.getCurrency_idDirtyFlag())
            this.setCurrency_id(srfdomain.getCurrency_id());
        if(srfdomain.getCreate_uid_textDirtyFlag())
            this.setCreate_uid_text(srfdomain.getCreate_uid_text());
        if(srfdomain.getCreate_uidDirtyFlag())
            this.setCreate_uid(srfdomain.getCreate_uid());
        if(srfdomain.getWrite_uidDirtyFlag())
            this.setWrite_uid(srfdomain.getWrite_uid());
        if(srfdomain.getCompany_idDirtyFlag())
            this.setCompany_id(srfdomain.getCompany_id());
        if(srfdomain.getTemplate_idDirtyFlag())
            this.setTemplate_id(srfdomain.getTemplate_id());

    }

    public List<Digest_digestDTO> fromDOPage(List<Digest_digest> poPage)   {
        if(poPage == null)
            return null;
        List<Digest_digestDTO> dtos=new ArrayList<Digest_digestDTO>();
        for(Digest_digest domain : poPage) {
            Digest_digestDTO dto = new Digest_digestDTO();
            dto.fromDO(domain);
            dtos.add(dto);
        }
        return dtos;
    }
}

