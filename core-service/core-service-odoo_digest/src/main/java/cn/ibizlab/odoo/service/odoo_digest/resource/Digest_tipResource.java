package cn.ibizlab.odoo.service.odoo_digest.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_digest.dto.Digest_tipDTO;
import cn.ibizlab.odoo.core.odoo_digest.domain.Digest_tip;
import cn.ibizlab.odoo.core.odoo_digest.service.IDigest_tipService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_digest.filter.Digest_tipSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Digest_tip" })
@RestController
@RequestMapping("")
public class Digest_tipResource {

    @Autowired
    private IDigest_tipService digest_tipService;

    public IDigest_tipService getDigest_tipService() {
        return this.digest_tipService;
    }

    @ApiOperation(value = "批更新数据", tags = {"Digest_tip" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_digest/digest_tips/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Digest_tipDTO> digest_tipdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "建立数据", tags = {"Digest_tip" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_digest/digest_tips")

    public ResponseEntity<Digest_tipDTO> create(@RequestBody Digest_tipDTO digest_tipdto) {
        Digest_tipDTO dto = new Digest_tipDTO();
        Digest_tip domain = digest_tipdto.toDO();
		digest_tipService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批建立数据", tags = {"Digest_tip" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_digest/digest_tips/createBatch")
    public ResponseEntity<Boolean> createBatchDigest_tip(@RequestBody List<Digest_tipDTO> digest_tipdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "删除数据", tags = {"Digest_tip" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_digest/digest_tips/{digest_tip_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("digest_tip_id") Integer digest_tip_id) {
        Digest_tipDTO digest_tipdto = new Digest_tipDTO();
		Digest_tip domain = new Digest_tip();
		digest_tipdto.setId(digest_tip_id);
		domain.setId(digest_tip_id);
        Boolean rst = digest_tipService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "批删除数据", tags = {"Digest_tip" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_digest/digest_tips/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Digest_tipDTO> digest_tipdtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "更新数据", tags = {"Digest_tip" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_digest/digest_tips/{digest_tip_id}")

    public ResponseEntity<Digest_tipDTO> update(@PathVariable("digest_tip_id") Integer digest_tip_id, @RequestBody Digest_tipDTO digest_tipdto) {
		Digest_tip domain = digest_tipdto.toDO();
        domain.setId(digest_tip_id);
		digest_tipService.update(domain);
		Digest_tipDTO dto = new Digest_tipDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取数据", tags = {"Digest_tip" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_digest/digest_tips/{digest_tip_id}")
    public ResponseEntity<Digest_tipDTO> get(@PathVariable("digest_tip_id") Integer digest_tip_id) {
        Digest_tipDTO dto = new Digest_tipDTO();
        Digest_tip domain = digest_tipService.get(digest_tip_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Digest_tip" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_digest/digest_tips/fetchdefault")
	public ResponseEntity<Page<Digest_tipDTO>> fetchDefault(Digest_tipSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Digest_tipDTO> list = new ArrayList<Digest_tipDTO>();
        
        Page<Digest_tip> domains = digest_tipService.searchDefault(context) ;
        for(Digest_tip digest_tip : domains.getContent()){
            Digest_tipDTO dto = new Digest_tipDTO();
            dto.fromDO(digest_tip);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
