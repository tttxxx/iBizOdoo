package cn.ibizlab.odoo.service.odoo_barcodes.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import lombok.Data;

@ConfigurationProperties(prefix = "service.odoo.barcodes")
@Data
public class odoo_barcodesServiceProperties {

	private boolean enabled;

	private boolean auth;


}