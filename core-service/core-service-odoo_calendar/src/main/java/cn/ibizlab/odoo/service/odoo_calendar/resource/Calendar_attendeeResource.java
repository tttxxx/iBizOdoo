package cn.ibizlab.odoo.service.odoo_calendar.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_calendar.dto.Calendar_attendeeDTO;
import cn.ibizlab.odoo.core.odoo_calendar.domain.Calendar_attendee;
import cn.ibizlab.odoo.core.odoo_calendar.service.ICalendar_attendeeService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_calendar.filter.Calendar_attendeeSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Calendar_attendee" })
@RestController
@RequestMapping("")
public class Calendar_attendeeResource {

    @Autowired
    private ICalendar_attendeeService calendar_attendeeService;

    public ICalendar_attendeeService getCalendar_attendeeService() {
        return this.calendar_attendeeService;
    }

    @ApiOperation(value = "批更新数据", tags = {"Calendar_attendee" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_calendar/calendar_attendees/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Calendar_attendeeDTO> calendar_attendeedtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "批建立数据", tags = {"Calendar_attendee" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_calendar/calendar_attendees/createBatch")
    public ResponseEntity<Boolean> createBatchCalendar_attendee(@RequestBody List<Calendar_attendeeDTO> calendar_attendeedtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "更新数据", tags = {"Calendar_attendee" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_calendar/calendar_attendees/{calendar_attendee_id}")

    public ResponseEntity<Calendar_attendeeDTO> update(@PathVariable("calendar_attendee_id") Integer calendar_attendee_id, @RequestBody Calendar_attendeeDTO calendar_attendeedto) {
		Calendar_attendee domain = calendar_attendeedto.toDO();
        domain.setId(calendar_attendee_id);
		calendar_attendeeService.update(domain);
		Calendar_attendeeDTO dto = new Calendar_attendeeDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "删除数据", tags = {"Calendar_attendee" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_calendar/calendar_attendees/{calendar_attendee_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("calendar_attendee_id") Integer calendar_attendee_id) {
        Calendar_attendeeDTO calendar_attendeedto = new Calendar_attendeeDTO();
		Calendar_attendee domain = new Calendar_attendee();
		calendar_attendeedto.setId(calendar_attendee_id);
		domain.setId(calendar_attendee_id);
        Boolean rst = calendar_attendeeService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "建立数据", tags = {"Calendar_attendee" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_calendar/calendar_attendees")

    public ResponseEntity<Calendar_attendeeDTO> create(@RequestBody Calendar_attendeeDTO calendar_attendeedto) {
        Calendar_attendeeDTO dto = new Calendar_attendeeDTO();
        Calendar_attendee domain = calendar_attendeedto.toDO();
		calendar_attendeeService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取数据", tags = {"Calendar_attendee" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_calendar/calendar_attendees/{calendar_attendee_id}")
    public ResponseEntity<Calendar_attendeeDTO> get(@PathVariable("calendar_attendee_id") Integer calendar_attendee_id) {
        Calendar_attendeeDTO dto = new Calendar_attendeeDTO();
        Calendar_attendee domain = calendar_attendeeService.get(calendar_attendee_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批删除数据", tags = {"Calendar_attendee" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_calendar/calendar_attendees/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Calendar_attendeeDTO> calendar_attendeedtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Calendar_attendee" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_calendar/calendar_attendees/fetchdefault")
	public ResponseEntity<Page<Calendar_attendeeDTO>> fetchDefault(Calendar_attendeeSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Calendar_attendeeDTO> list = new ArrayList<Calendar_attendeeDTO>();
        
        Page<Calendar_attendee> domains = calendar_attendeeService.searchDefault(context) ;
        for(Calendar_attendee calendar_attendee : domains.getContent()){
            Calendar_attendeeDTO dto = new Calendar_attendeeDTO();
            dto.fromDO(calendar_attendee);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
