package cn.ibizlab.odoo.service.odoo_calendar.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_calendar.dto.Calendar_contactsDTO;
import cn.ibizlab.odoo.core.odoo_calendar.domain.Calendar_contacts;
import cn.ibizlab.odoo.core.odoo_calendar.service.ICalendar_contactsService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_calendar.filter.Calendar_contactsSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Calendar_contacts" })
@RestController
@RequestMapping("")
public class Calendar_contactsResource {

    @Autowired
    private ICalendar_contactsService calendar_contactsService;

    public ICalendar_contactsService getCalendar_contactsService() {
        return this.calendar_contactsService;
    }

    @ApiOperation(value = "批删除数据", tags = {"Calendar_contacts" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_calendar/calendar_contacts/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Calendar_contactsDTO> calendar_contactsdtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "获取数据", tags = {"Calendar_contacts" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_calendar/calendar_contacts/{calendar_contacts_id}")
    public ResponseEntity<Calendar_contactsDTO> get(@PathVariable("calendar_contacts_id") Integer calendar_contacts_id) {
        Calendar_contactsDTO dto = new Calendar_contactsDTO();
        Calendar_contacts domain = calendar_contactsService.get(calendar_contacts_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "删除数据", tags = {"Calendar_contacts" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_calendar/calendar_contacts/{calendar_contacts_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("calendar_contacts_id") Integer calendar_contacts_id) {
        Calendar_contactsDTO calendar_contactsdto = new Calendar_contactsDTO();
		Calendar_contacts domain = new Calendar_contacts();
		calendar_contactsdto.setId(calendar_contacts_id);
		domain.setId(calendar_contacts_id);
        Boolean rst = calendar_contactsService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "批建立数据", tags = {"Calendar_contacts" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_calendar/calendar_contacts/createBatch")
    public ResponseEntity<Boolean> createBatchCalendar_contacts(@RequestBody List<Calendar_contactsDTO> calendar_contactsdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "批更新数据", tags = {"Calendar_contacts" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_calendar/calendar_contacts/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Calendar_contactsDTO> calendar_contactsdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "更新数据", tags = {"Calendar_contacts" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_calendar/calendar_contacts/{calendar_contacts_id}")

    public ResponseEntity<Calendar_contactsDTO> update(@PathVariable("calendar_contacts_id") Integer calendar_contacts_id, @RequestBody Calendar_contactsDTO calendar_contactsdto) {
		Calendar_contacts domain = calendar_contactsdto.toDO();
        domain.setId(calendar_contacts_id);
		calendar_contactsService.update(domain);
		Calendar_contactsDTO dto = new Calendar_contactsDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "建立数据", tags = {"Calendar_contacts" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_calendar/calendar_contacts")

    public ResponseEntity<Calendar_contactsDTO> create(@RequestBody Calendar_contactsDTO calendar_contactsdto) {
        Calendar_contactsDTO dto = new Calendar_contactsDTO();
        Calendar_contacts domain = calendar_contactsdto.toDO();
		calendar_contactsService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Calendar_contacts" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_calendar/calendar_contacts/fetchdefault")
	public ResponseEntity<Page<Calendar_contactsDTO>> fetchDefault(Calendar_contactsSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Calendar_contactsDTO> list = new ArrayList<Calendar_contactsDTO>();
        
        Page<Calendar_contacts> domains = calendar_contactsService.searchDefault(context) ;
        for(Calendar_contacts calendar_contacts : domains.getContent()){
            Calendar_contactsDTO dto = new Calendar_contactsDTO();
            dto.fromDO(calendar_contacts);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
