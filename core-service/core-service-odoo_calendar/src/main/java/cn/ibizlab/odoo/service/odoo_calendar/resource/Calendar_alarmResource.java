package cn.ibizlab.odoo.service.odoo_calendar.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_calendar.dto.Calendar_alarmDTO;
import cn.ibizlab.odoo.core.odoo_calendar.domain.Calendar_alarm;
import cn.ibizlab.odoo.core.odoo_calendar.service.ICalendar_alarmService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_calendar.filter.Calendar_alarmSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Calendar_alarm" })
@RestController
@RequestMapping("")
public class Calendar_alarmResource {

    @Autowired
    private ICalendar_alarmService calendar_alarmService;

    public ICalendar_alarmService getCalendar_alarmService() {
        return this.calendar_alarmService;
    }

    @ApiOperation(value = "获取数据", tags = {"Calendar_alarm" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_calendar/calendar_alarms/{calendar_alarm_id}")
    public ResponseEntity<Calendar_alarmDTO> get(@PathVariable("calendar_alarm_id") Integer calendar_alarm_id) {
        Calendar_alarmDTO dto = new Calendar_alarmDTO();
        Calendar_alarm domain = calendar_alarmService.get(calendar_alarm_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批建立数据", tags = {"Calendar_alarm" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_calendar/calendar_alarms/createBatch")
    public ResponseEntity<Boolean> createBatchCalendar_alarm(@RequestBody List<Calendar_alarmDTO> calendar_alarmdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "批更新数据", tags = {"Calendar_alarm" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_calendar/calendar_alarms/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Calendar_alarmDTO> calendar_alarmdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "删除数据", tags = {"Calendar_alarm" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_calendar/calendar_alarms/{calendar_alarm_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("calendar_alarm_id") Integer calendar_alarm_id) {
        Calendar_alarmDTO calendar_alarmdto = new Calendar_alarmDTO();
		Calendar_alarm domain = new Calendar_alarm();
		calendar_alarmdto.setId(calendar_alarm_id);
		domain.setId(calendar_alarm_id);
        Boolean rst = calendar_alarmService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "建立数据", tags = {"Calendar_alarm" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_calendar/calendar_alarms")

    public ResponseEntity<Calendar_alarmDTO> create(@RequestBody Calendar_alarmDTO calendar_alarmdto) {
        Calendar_alarmDTO dto = new Calendar_alarmDTO();
        Calendar_alarm domain = calendar_alarmdto.toDO();
		calendar_alarmService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "更新数据", tags = {"Calendar_alarm" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_calendar/calendar_alarms/{calendar_alarm_id}")

    public ResponseEntity<Calendar_alarmDTO> update(@PathVariable("calendar_alarm_id") Integer calendar_alarm_id, @RequestBody Calendar_alarmDTO calendar_alarmdto) {
		Calendar_alarm domain = calendar_alarmdto.toDO();
        domain.setId(calendar_alarm_id);
		calendar_alarmService.update(domain);
		Calendar_alarmDTO dto = new Calendar_alarmDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批删除数据", tags = {"Calendar_alarm" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_calendar/calendar_alarms/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Calendar_alarmDTO> calendar_alarmdtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Calendar_alarm" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_calendar/calendar_alarms/fetchdefault")
	public ResponseEntity<Page<Calendar_alarmDTO>> fetchDefault(Calendar_alarmSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Calendar_alarmDTO> list = new ArrayList<Calendar_alarmDTO>();
        
        Page<Calendar_alarm> domains = calendar_alarmService.searchDefault(context) ;
        for(Calendar_alarm calendar_alarm : domains.getContent()){
            Calendar_alarmDTO dto = new Calendar_alarmDTO();
            dto.fromDO(calendar_alarm);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
