package cn.ibizlab.odoo.service.odoo_website.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_website.dto.Website_seo_metadataDTO;
import cn.ibizlab.odoo.core.odoo_website.domain.Website_seo_metadata;
import cn.ibizlab.odoo.core.odoo_website.service.IWebsite_seo_metadataService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_website.filter.Website_seo_metadataSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Website_seo_metadata" })
@RestController
@RequestMapping("")
public class Website_seo_metadataResource {

    @Autowired
    private IWebsite_seo_metadataService website_seo_metadataService;

    public IWebsite_seo_metadataService getWebsite_seo_metadataService() {
        return this.website_seo_metadataService;
    }

    @ApiOperation(value = "建立数据", tags = {"Website_seo_metadata" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_website/website_seo_metadata")

    public ResponseEntity<Website_seo_metadataDTO> create(@RequestBody Website_seo_metadataDTO website_seo_metadatadto) {
        Website_seo_metadataDTO dto = new Website_seo_metadataDTO();
        Website_seo_metadata domain = website_seo_metadatadto.toDO();
		website_seo_metadataService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批建立数据", tags = {"Website_seo_metadata" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_website/website_seo_metadata/createBatch")
    public ResponseEntity<Boolean> createBatchWebsite_seo_metadata(@RequestBody List<Website_seo_metadataDTO> website_seo_metadatadtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "批更新数据", tags = {"Website_seo_metadata" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_website/website_seo_metadata/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Website_seo_metadataDTO> website_seo_metadatadtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "更新数据", tags = {"Website_seo_metadata" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_website/website_seo_metadata/{website_seo_metadata_id}")

    public ResponseEntity<Website_seo_metadataDTO> update(@PathVariable("website_seo_metadata_id") Integer website_seo_metadata_id, @RequestBody Website_seo_metadataDTO website_seo_metadatadto) {
		Website_seo_metadata domain = website_seo_metadatadto.toDO();
        domain.setId(website_seo_metadata_id);
		website_seo_metadataService.update(domain);
		Website_seo_metadataDTO dto = new Website_seo_metadataDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取数据", tags = {"Website_seo_metadata" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_website/website_seo_metadata/{website_seo_metadata_id}")
    public ResponseEntity<Website_seo_metadataDTO> get(@PathVariable("website_seo_metadata_id") Integer website_seo_metadata_id) {
        Website_seo_metadataDTO dto = new Website_seo_metadataDTO();
        Website_seo_metadata domain = website_seo_metadataService.get(website_seo_metadata_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批删除数据", tags = {"Website_seo_metadata" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_website/website_seo_metadata/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Website_seo_metadataDTO> website_seo_metadatadtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "删除数据", tags = {"Website_seo_metadata" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_website/website_seo_metadata/{website_seo_metadata_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("website_seo_metadata_id") Integer website_seo_metadata_id) {
        Website_seo_metadataDTO website_seo_metadatadto = new Website_seo_metadataDTO();
		Website_seo_metadata domain = new Website_seo_metadata();
		website_seo_metadatadto.setId(website_seo_metadata_id);
		domain.setId(website_seo_metadata_id);
        Boolean rst = website_seo_metadataService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

	@ApiOperation(value = "获取默认查询", tags = {"Website_seo_metadata" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_website/website_seo_metadata/fetchdefault")
	public ResponseEntity<Page<Website_seo_metadataDTO>> fetchDefault(Website_seo_metadataSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Website_seo_metadataDTO> list = new ArrayList<Website_seo_metadataDTO>();
        
        Page<Website_seo_metadata> domains = website_seo_metadataService.searchDefault(context) ;
        for(Website_seo_metadata website_seo_metadata : domains.getContent()){
            Website_seo_metadataDTO dto = new Website_seo_metadataDTO();
            dto.fromDO(website_seo_metadata);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
