package cn.ibizlab.odoo.service.odoo_website.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.core.odoo_website.valuerule.anno.website_sale_payment_acquirer_onboarding_wizard.*;
import cn.ibizlab.odoo.core.odoo_website.domain.Website_sale_payment_acquirer_onboarding_wizard;
import org.springframework.cglib.beans.BeanCopier;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 服务DTO对象[Website_sale_payment_acquirer_onboarding_wizardDTO]
 */
public class Website_sale_payment_acquirer_onboarding_wizardDTO implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [PAYPAL_EMAIL_ACCOUNT]
     *
     */
    @Website_sale_payment_acquirer_onboarding_wizardPaypal_email_accountDefault(info = "默认规则")
    private String paypal_email_account;

    @JsonIgnore
    private boolean paypal_email_accountDirtyFlag;

    /**
     * 属性 [JOURNAL_NAME]
     *
     */
    @Website_sale_payment_acquirer_onboarding_wizardJournal_nameDefault(info = "默认规则")
    private String journal_name;

    @JsonIgnore
    private boolean journal_nameDirtyFlag;

    /**
     * 属性 [STRIPE_SECRET_KEY]
     *
     */
    @Website_sale_payment_acquirer_onboarding_wizardStripe_secret_keyDefault(info = "默认规则")
    private String stripe_secret_key;

    @JsonIgnore
    private boolean stripe_secret_keyDirtyFlag;

    /**
     * 属性 [ACC_NUMBER]
     *
     */
    @Website_sale_payment_acquirer_onboarding_wizardAcc_numberDefault(info = "默认规则")
    private String acc_number;

    @JsonIgnore
    private boolean acc_numberDirtyFlag;

    /**
     * 属性 [MANUAL_POST_MSG]
     *
     */
    @Website_sale_payment_acquirer_onboarding_wizardManual_post_msgDefault(info = "默认规则")
    private String manual_post_msg;

    @JsonIgnore
    private boolean manual_post_msgDirtyFlag;

    /**
     * 属性 [PAYMENT_METHOD]
     *
     */
    @Website_sale_payment_acquirer_onboarding_wizardPayment_methodDefault(info = "默认规则")
    private String payment_method;

    @JsonIgnore
    private boolean payment_methodDirtyFlag;

    /**
     * 属性 [PAYPAL_PDT_TOKEN]
     *
     */
    @Website_sale_payment_acquirer_onboarding_wizardPaypal_pdt_tokenDefault(info = "默认规则")
    private String paypal_pdt_token;

    @JsonIgnore
    private boolean paypal_pdt_tokenDirtyFlag;

    /**
     * 属性 [MANUAL_NAME]
     *
     */
    @Website_sale_payment_acquirer_onboarding_wizardManual_nameDefault(info = "默认规则")
    private String manual_name;

    @JsonIgnore
    private boolean manual_nameDirtyFlag;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @Website_sale_payment_acquirer_onboarding_wizard__last_updateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp __last_update;

    @JsonIgnore
    private boolean __last_updateDirtyFlag;

    /**
     * 属性 [PAYPAL_SELLER_ACCOUNT]
     *
     */
    @Website_sale_payment_acquirer_onboarding_wizardPaypal_seller_accountDefault(info = "默认规则")
    private String paypal_seller_account;

    @JsonIgnore
    private boolean paypal_seller_accountDirtyFlag;

    /**
     * 属性 [CREATE_DATE]
     *
     */
    @Website_sale_payment_acquirer_onboarding_wizardCreate_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp create_date;

    @JsonIgnore
    private boolean create_dateDirtyFlag;

    /**
     * 属性 [ID]
     *
     */
    @Website_sale_payment_acquirer_onboarding_wizardIdDefault(info = "默认规则")
    private Integer id;

    @JsonIgnore
    private boolean idDirtyFlag;

    /**
     * 属性 [WRITE_DATE]
     *
     */
    @Website_sale_payment_acquirer_onboarding_wizardWrite_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp write_date;

    @JsonIgnore
    private boolean write_dateDirtyFlag;

    /**
     * 属性 [STRIPE_PUBLISHABLE_KEY]
     *
     */
    @Website_sale_payment_acquirer_onboarding_wizardStripe_publishable_keyDefault(info = "默认规则")
    private String stripe_publishable_key;

    @JsonIgnore
    private boolean stripe_publishable_keyDirtyFlag;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @Website_sale_payment_acquirer_onboarding_wizardDisplay_nameDefault(info = "默认规则")
    private String display_name;

    @JsonIgnore
    private boolean display_nameDirtyFlag;

    /**
     * 属性 [WRITE_UID_TEXT]
     *
     */
    @Website_sale_payment_acquirer_onboarding_wizardWrite_uid_textDefault(info = "默认规则")
    private String write_uid_text;

    @JsonIgnore
    private boolean write_uid_textDirtyFlag;

    /**
     * 属性 [CREATE_UID_TEXT]
     *
     */
    @Website_sale_payment_acquirer_onboarding_wizardCreate_uid_textDefault(info = "默认规则")
    private String create_uid_text;

    @JsonIgnore
    private boolean create_uid_textDirtyFlag;

    /**
     * 属性 [WRITE_UID]
     *
     */
    @Website_sale_payment_acquirer_onboarding_wizardWrite_uidDefault(info = "默认规则")
    private Integer write_uid;

    @JsonIgnore
    private boolean write_uidDirtyFlag;

    /**
     * 属性 [CREATE_UID]
     *
     */
    @Website_sale_payment_acquirer_onboarding_wizardCreate_uidDefault(info = "默认规则")
    private Integer create_uid;

    @JsonIgnore
    private boolean create_uidDirtyFlag;


    /**
     * 获取 [PAYPAL_EMAIL_ACCOUNT]
     */
    @JsonProperty("paypal_email_account")
    public String getPaypal_email_account(){
        return paypal_email_account ;
    }

    /**
     * 设置 [PAYPAL_EMAIL_ACCOUNT]
     */
    @JsonProperty("paypal_email_account")
    public void setPaypal_email_account(String  paypal_email_account){
        this.paypal_email_account = paypal_email_account ;
        this.paypal_email_accountDirtyFlag = true ;
    }

    /**
     * 获取 [PAYPAL_EMAIL_ACCOUNT]脏标记
     */
    @JsonIgnore
    public boolean getPaypal_email_accountDirtyFlag(){
        return paypal_email_accountDirtyFlag ;
    }

    /**
     * 获取 [JOURNAL_NAME]
     */
    @JsonProperty("journal_name")
    public String getJournal_name(){
        return journal_name ;
    }

    /**
     * 设置 [JOURNAL_NAME]
     */
    @JsonProperty("journal_name")
    public void setJournal_name(String  journal_name){
        this.journal_name = journal_name ;
        this.journal_nameDirtyFlag = true ;
    }

    /**
     * 获取 [JOURNAL_NAME]脏标记
     */
    @JsonIgnore
    public boolean getJournal_nameDirtyFlag(){
        return journal_nameDirtyFlag ;
    }

    /**
     * 获取 [STRIPE_SECRET_KEY]
     */
    @JsonProperty("stripe_secret_key")
    public String getStripe_secret_key(){
        return stripe_secret_key ;
    }

    /**
     * 设置 [STRIPE_SECRET_KEY]
     */
    @JsonProperty("stripe_secret_key")
    public void setStripe_secret_key(String  stripe_secret_key){
        this.stripe_secret_key = stripe_secret_key ;
        this.stripe_secret_keyDirtyFlag = true ;
    }

    /**
     * 获取 [STRIPE_SECRET_KEY]脏标记
     */
    @JsonIgnore
    public boolean getStripe_secret_keyDirtyFlag(){
        return stripe_secret_keyDirtyFlag ;
    }

    /**
     * 获取 [ACC_NUMBER]
     */
    @JsonProperty("acc_number")
    public String getAcc_number(){
        return acc_number ;
    }

    /**
     * 设置 [ACC_NUMBER]
     */
    @JsonProperty("acc_number")
    public void setAcc_number(String  acc_number){
        this.acc_number = acc_number ;
        this.acc_numberDirtyFlag = true ;
    }

    /**
     * 获取 [ACC_NUMBER]脏标记
     */
    @JsonIgnore
    public boolean getAcc_numberDirtyFlag(){
        return acc_numberDirtyFlag ;
    }

    /**
     * 获取 [MANUAL_POST_MSG]
     */
    @JsonProperty("manual_post_msg")
    public String getManual_post_msg(){
        return manual_post_msg ;
    }

    /**
     * 设置 [MANUAL_POST_MSG]
     */
    @JsonProperty("manual_post_msg")
    public void setManual_post_msg(String  manual_post_msg){
        this.manual_post_msg = manual_post_msg ;
        this.manual_post_msgDirtyFlag = true ;
    }

    /**
     * 获取 [MANUAL_POST_MSG]脏标记
     */
    @JsonIgnore
    public boolean getManual_post_msgDirtyFlag(){
        return manual_post_msgDirtyFlag ;
    }

    /**
     * 获取 [PAYMENT_METHOD]
     */
    @JsonProperty("payment_method")
    public String getPayment_method(){
        return payment_method ;
    }

    /**
     * 设置 [PAYMENT_METHOD]
     */
    @JsonProperty("payment_method")
    public void setPayment_method(String  payment_method){
        this.payment_method = payment_method ;
        this.payment_methodDirtyFlag = true ;
    }

    /**
     * 获取 [PAYMENT_METHOD]脏标记
     */
    @JsonIgnore
    public boolean getPayment_methodDirtyFlag(){
        return payment_methodDirtyFlag ;
    }

    /**
     * 获取 [PAYPAL_PDT_TOKEN]
     */
    @JsonProperty("paypal_pdt_token")
    public String getPaypal_pdt_token(){
        return paypal_pdt_token ;
    }

    /**
     * 设置 [PAYPAL_PDT_TOKEN]
     */
    @JsonProperty("paypal_pdt_token")
    public void setPaypal_pdt_token(String  paypal_pdt_token){
        this.paypal_pdt_token = paypal_pdt_token ;
        this.paypal_pdt_tokenDirtyFlag = true ;
    }

    /**
     * 获取 [PAYPAL_PDT_TOKEN]脏标记
     */
    @JsonIgnore
    public boolean getPaypal_pdt_tokenDirtyFlag(){
        return paypal_pdt_tokenDirtyFlag ;
    }

    /**
     * 获取 [MANUAL_NAME]
     */
    @JsonProperty("manual_name")
    public String getManual_name(){
        return manual_name ;
    }

    /**
     * 设置 [MANUAL_NAME]
     */
    @JsonProperty("manual_name")
    public void setManual_name(String  manual_name){
        this.manual_name = manual_name ;
        this.manual_nameDirtyFlag = true ;
    }

    /**
     * 获取 [MANUAL_NAME]脏标记
     */
    @JsonIgnore
    public boolean getManual_nameDirtyFlag(){
        return manual_nameDirtyFlag ;
    }

    /**
     * 获取 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return __last_update ;
    }

    /**
     * 设置 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

    /**
     * 获取 [__LAST_UPDATE]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return __last_updateDirtyFlag ;
    }

    /**
     * 获取 [PAYPAL_SELLER_ACCOUNT]
     */
    @JsonProperty("paypal_seller_account")
    public String getPaypal_seller_account(){
        return paypal_seller_account ;
    }

    /**
     * 设置 [PAYPAL_SELLER_ACCOUNT]
     */
    @JsonProperty("paypal_seller_account")
    public void setPaypal_seller_account(String  paypal_seller_account){
        this.paypal_seller_account = paypal_seller_account ;
        this.paypal_seller_accountDirtyFlag = true ;
    }

    /**
     * 获取 [PAYPAL_SELLER_ACCOUNT]脏标记
     */
    @JsonIgnore
    public boolean getPaypal_seller_accountDirtyFlag(){
        return paypal_seller_accountDirtyFlag ;
    }

    /**
     * 获取 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return create_date ;
    }

    /**
     * 设置 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return create_dateDirtyFlag ;
    }

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

    /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return idDirtyFlag ;
    }

    /**
     * 获取 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return write_date ;
    }

    /**
     * 设置 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return write_dateDirtyFlag ;
    }

    /**
     * 获取 [STRIPE_PUBLISHABLE_KEY]
     */
    @JsonProperty("stripe_publishable_key")
    public String getStripe_publishable_key(){
        return stripe_publishable_key ;
    }

    /**
     * 设置 [STRIPE_PUBLISHABLE_KEY]
     */
    @JsonProperty("stripe_publishable_key")
    public void setStripe_publishable_key(String  stripe_publishable_key){
        this.stripe_publishable_key = stripe_publishable_key ;
        this.stripe_publishable_keyDirtyFlag = true ;
    }

    /**
     * 获取 [STRIPE_PUBLISHABLE_KEY]脏标记
     */
    @JsonIgnore
    public boolean getStripe_publishable_keyDirtyFlag(){
        return stripe_publishable_keyDirtyFlag ;
    }

    /**
     * 获取 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return display_name ;
    }

    /**
     * 设置 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

    /**
     * 获取 [DISPLAY_NAME]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return display_nameDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return write_uid_text ;
    }

    /**
     * 设置 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return write_uid_textDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return create_uid_text ;
    }

    /**
     * 设置 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return create_uid_textDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return write_uid ;
    }

    /**
     * 设置 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return write_uidDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return create_uid ;
    }

    /**
     * 设置 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return create_uidDirtyFlag ;
    }



    public Website_sale_payment_acquirer_onboarding_wizard toDO() {
        Website_sale_payment_acquirer_onboarding_wizard srfdomain = new Website_sale_payment_acquirer_onboarding_wizard();
        if(getPaypal_email_accountDirtyFlag())
            srfdomain.setPaypal_email_account(paypal_email_account);
        if(getJournal_nameDirtyFlag())
            srfdomain.setJournal_name(journal_name);
        if(getStripe_secret_keyDirtyFlag())
            srfdomain.setStripe_secret_key(stripe_secret_key);
        if(getAcc_numberDirtyFlag())
            srfdomain.setAcc_number(acc_number);
        if(getManual_post_msgDirtyFlag())
            srfdomain.setManual_post_msg(manual_post_msg);
        if(getPayment_methodDirtyFlag())
            srfdomain.setPayment_method(payment_method);
        if(getPaypal_pdt_tokenDirtyFlag())
            srfdomain.setPaypal_pdt_token(paypal_pdt_token);
        if(getManual_nameDirtyFlag())
            srfdomain.setManual_name(manual_name);
        if(get__last_updateDirtyFlag())
            srfdomain.set__last_update(__last_update);
        if(getPaypal_seller_accountDirtyFlag())
            srfdomain.setPaypal_seller_account(paypal_seller_account);
        if(getCreate_dateDirtyFlag())
            srfdomain.setCreate_date(create_date);
        if(getIdDirtyFlag())
            srfdomain.setId(id);
        if(getWrite_dateDirtyFlag())
            srfdomain.setWrite_date(write_date);
        if(getStripe_publishable_keyDirtyFlag())
            srfdomain.setStripe_publishable_key(stripe_publishable_key);
        if(getDisplay_nameDirtyFlag())
            srfdomain.setDisplay_name(display_name);
        if(getWrite_uid_textDirtyFlag())
            srfdomain.setWrite_uid_text(write_uid_text);
        if(getCreate_uid_textDirtyFlag())
            srfdomain.setCreate_uid_text(create_uid_text);
        if(getWrite_uidDirtyFlag())
            srfdomain.setWrite_uid(write_uid);
        if(getCreate_uidDirtyFlag())
            srfdomain.setCreate_uid(create_uid);

        return srfdomain;
    }

    public void fromDO(Website_sale_payment_acquirer_onboarding_wizard srfdomain) {
        if(srfdomain == null )
          return ;
        if(srfdomain.getPaypal_email_accountDirtyFlag())
            this.setPaypal_email_account(srfdomain.getPaypal_email_account());
        if(srfdomain.getJournal_nameDirtyFlag())
            this.setJournal_name(srfdomain.getJournal_name());
        if(srfdomain.getStripe_secret_keyDirtyFlag())
            this.setStripe_secret_key(srfdomain.getStripe_secret_key());
        if(srfdomain.getAcc_numberDirtyFlag())
            this.setAcc_number(srfdomain.getAcc_number());
        if(srfdomain.getManual_post_msgDirtyFlag())
            this.setManual_post_msg(srfdomain.getManual_post_msg());
        if(srfdomain.getPayment_methodDirtyFlag())
            this.setPayment_method(srfdomain.getPayment_method());
        if(srfdomain.getPaypal_pdt_tokenDirtyFlag())
            this.setPaypal_pdt_token(srfdomain.getPaypal_pdt_token());
        if(srfdomain.getManual_nameDirtyFlag())
            this.setManual_name(srfdomain.getManual_name());
        if(srfdomain.get__last_updateDirtyFlag())
            this.set__last_update(srfdomain.get__last_update());
        if(srfdomain.getPaypal_seller_accountDirtyFlag())
            this.setPaypal_seller_account(srfdomain.getPaypal_seller_account());
        if(srfdomain.getCreate_dateDirtyFlag())
            this.setCreate_date(srfdomain.getCreate_date());
        if(srfdomain.getIdDirtyFlag())
            this.setId(srfdomain.getId());
        if(srfdomain.getWrite_dateDirtyFlag())
            this.setWrite_date(srfdomain.getWrite_date());
        if(srfdomain.getStripe_publishable_keyDirtyFlag())
            this.setStripe_publishable_key(srfdomain.getStripe_publishable_key());
        if(srfdomain.getDisplay_nameDirtyFlag())
            this.setDisplay_name(srfdomain.getDisplay_name());
        if(srfdomain.getWrite_uid_textDirtyFlag())
            this.setWrite_uid_text(srfdomain.getWrite_uid_text());
        if(srfdomain.getCreate_uid_textDirtyFlag())
            this.setCreate_uid_text(srfdomain.getCreate_uid_text());
        if(srfdomain.getWrite_uidDirtyFlag())
            this.setWrite_uid(srfdomain.getWrite_uid());
        if(srfdomain.getCreate_uidDirtyFlag())
            this.setCreate_uid(srfdomain.getCreate_uid());

    }

    public List<Website_sale_payment_acquirer_onboarding_wizardDTO> fromDOPage(List<Website_sale_payment_acquirer_onboarding_wizard> poPage)   {
        if(poPage == null)
            return null;
        List<Website_sale_payment_acquirer_onboarding_wizardDTO> dtos=new ArrayList<Website_sale_payment_acquirer_onboarding_wizardDTO>();
        for(Website_sale_payment_acquirer_onboarding_wizard domain : poPage) {
            Website_sale_payment_acquirer_onboarding_wizardDTO dto = new Website_sale_payment_acquirer_onboarding_wizardDTO();
            dto.fromDO(domain);
            dtos.add(dto);
        }
        return dtos;
    }
}

