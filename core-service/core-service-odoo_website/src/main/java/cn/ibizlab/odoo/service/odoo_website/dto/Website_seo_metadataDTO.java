package cn.ibizlab.odoo.service.odoo_website.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.core.odoo_website.valuerule.anno.website_seo_metadata.*;
import cn.ibizlab.odoo.core.odoo_website.domain.Website_seo_metadata;
import org.springframework.cglib.beans.BeanCopier;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 服务DTO对象[Website_seo_metadataDTO]
 */
public class Website_seo_metadataDTO implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @Website_seo_metadataDisplay_nameDefault(info = "默认规则")
    private String display_name;

    @JsonIgnore
    private boolean display_nameDirtyFlag;

    /**
     * 属性 [ID]
     *
     */
    @Website_seo_metadataIdDefault(info = "默认规则")
    private Integer id;

    @JsonIgnore
    private boolean idDirtyFlag;

    /**
     * 属性 [WEBSITE_META_OG_IMG]
     *
     */
    @Website_seo_metadataWebsite_meta_og_imgDefault(info = "默认规则")
    private String website_meta_og_img;

    @JsonIgnore
    private boolean website_meta_og_imgDirtyFlag;

    /**
     * 属性 [WEBSITE_META_TITLE]
     *
     */
    @Website_seo_metadataWebsite_meta_titleDefault(info = "默认规则")
    private String website_meta_title;

    @JsonIgnore
    private boolean website_meta_titleDirtyFlag;

    /**
     * 属性 [WEBSITE_META_KEYWORDS]
     *
     */
    @Website_seo_metadataWebsite_meta_keywordsDefault(info = "默认规则")
    private String website_meta_keywords;

    @JsonIgnore
    private boolean website_meta_keywordsDirtyFlag;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @Website_seo_metadata__last_updateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp __last_update;

    @JsonIgnore
    private boolean __last_updateDirtyFlag;

    /**
     * 属性 [WEBSITE_META_DESCRIPTION]
     *
     */
    @Website_seo_metadataWebsite_meta_descriptionDefault(info = "默认规则")
    private String website_meta_description;

    @JsonIgnore
    private boolean website_meta_descriptionDirtyFlag;

    /**
     * 属性 [IS_SEO_OPTIMIZED]
     *
     */
    @Website_seo_metadataIs_seo_optimizedDefault(info = "默认规则")
    private String is_seo_optimized;

    @JsonIgnore
    private boolean is_seo_optimizedDirtyFlag;


    /**
     * 获取 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return display_name ;
    }

    /**
     * 设置 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

    /**
     * 获取 [DISPLAY_NAME]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return display_nameDirtyFlag ;
    }

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

    /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return idDirtyFlag ;
    }

    /**
     * 获取 [WEBSITE_META_OG_IMG]
     */
    @JsonProperty("website_meta_og_img")
    public String getWebsite_meta_og_img(){
        return website_meta_og_img ;
    }

    /**
     * 设置 [WEBSITE_META_OG_IMG]
     */
    @JsonProperty("website_meta_og_img")
    public void setWebsite_meta_og_img(String  website_meta_og_img){
        this.website_meta_og_img = website_meta_og_img ;
        this.website_meta_og_imgDirtyFlag = true ;
    }

    /**
     * 获取 [WEBSITE_META_OG_IMG]脏标记
     */
    @JsonIgnore
    public boolean getWebsite_meta_og_imgDirtyFlag(){
        return website_meta_og_imgDirtyFlag ;
    }

    /**
     * 获取 [WEBSITE_META_TITLE]
     */
    @JsonProperty("website_meta_title")
    public String getWebsite_meta_title(){
        return website_meta_title ;
    }

    /**
     * 设置 [WEBSITE_META_TITLE]
     */
    @JsonProperty("website_meta_title")
    public void setWebsite_meta_title(String  website_meta_title){
        this.website_meta_title = website_meta_title ;
        this.website_meta_titleDirtyFlag = true ;
    }

    /**
     * 获取 [WEBSITE_META_TITLE]脏标记
     */
    @JsonIgnore
    public boolean getWebsite_meta_titleDirtyFlag(){
        return website_meta_titleDirtyFlag ;
    }

    /**
     * 获取 [WEBSITE_META_KEYWORDS]
     */
    @JsonProperty("website_meta_keywords")
    public String getWebsite_meta_keywords(){
        return website_meta_keywords ;
    }

    /**
     * 设置 [WEBSITE_META_KEYWORDS]
     */
    @JsonProperty("website_meta_keywords")
    public void setWebsite_meta_keywords(String  website_meta_keywords){
        this.website_meta_keywords = website_meta_keywords ;
        this.website_meta_keywordsDirtyFlag = true ;
    }

    /**
     * 获取 [WEBSITE_META_KEYWORDS]脏标记
     */
    @JsonIgnore
    public boolean getWebsite_meta_keywordsDirtyFlag(){
        return website_meta_keywordsDirtyFlag ;
    }

    /**
     * 获取 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return __last_update ;
    }

    /**
     * 设置 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

    /**
     * 获取 [__LAST_UPDATE]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return __last_updateDirtyFlag ;
    }

    /**
     * 获取 [WEBSITE_META_DESCRIPTION]
     */
    @JsonProperty("website_meta_description")
    public String getWebsite_meta_description(){
        return website_meta_description ;
    }

    /**
     * 设置 [WEBSITE_META_DESCRIPTION]
     */
    @JsonProperty("website_meta_description")
    public void setWebsite_meta_description(String  website_meta_description){
        this.website_meta_description = website_meta_description ;
        this.website_meta_descriptionDirtyFlag = true ;
    }

    /**
     * 获取 [WEBSITE_META_DESCRIPTION]脏标记
     */
    @JsonIgnore
    public boolean getWebsite_meta_descriptionDirtyFlag(){
        return website_meta_descriptionDirtyFlag ;
    }

    /**
     * 获取 [IS_SEO_OPTIMIZED]
     */
    @JsonProperty("is_seo_optimized")
    public String getIs_seo_optimized(){
        return is_seo_optimized ;
    }

    /**
     * 设置 [IS_SEO_OPTIMIZED]
     */
    @JsonProperty("is_seo_optimized")
    public void setIs_seo_optimized(String  is_seo_optimized){
        this.is_seo_optimized = is_seo_optimized ;
        this.is_seo_optimizedDirtyFlag = true ;
    }

    /**
     * 获取 [IS_SEO_OPTIMIZED]脏标记
     */
    @JsonIgnore
    public boolean getIs_seo_optimizedDirtyFlag(){
        return is_seo_optimizedDirtyFlag ;
    }



    public Website_seo_metadata toDO() {
        Website_seo_metadata srfdomain = new Website_seo_metadata();
        if(getDisplay_nameDirtyFlag())
            srfdomain.setDisplay_name(display_name);
        if(getIdDirtyFlag())
            srfdomain.setId(id);
        if(getWebsite_meta_og_imgDirtyFlag())
            srfdomain.setWebsite_meta_og_img(website_meta_og_img);
        if(getWebsite_meta_titleDirtyFlag())
            srfdomain.setWebsite_meta_title(website_meta_title);
        if(getWebsite_meta_keywordsDirtyFlag())
            srfdomain.setWebsite_meta_keywords(website_meta_keywords);
        if(get__last_updateDirtyFlag())
            srfdomain.set__last_update(__last_update);
        if(getWebsite_meta_descriptionDirtyFlag())
            srfdomain.setWebsite_meta_description(website_meta_description);
        if(getIs_seo_optimizedDirtyFlag())
            srfdomain.setIs_seo_optimized(is_seo_optimized);

        return srfdomain;
    }

    public void fromDO(Website_seo_metadata srfdomain) {
        if(srfdomain == null )
          return ;
        if(srfdomain.getDisplay_nameDirtyFlag())
            this.setDisplay_name(srfdomain.getDisplay_name());
        if(srfdomain.getIdDirtyFlag())
            this.setId(srfdomain.getId());
        if(srfdomain.getWebsite_meta_og_imgDirtyFlag())
            this.setWebsite_meta_og_img(srfdomain.getWebsite_meta_og_img());
        if(srfdomain.getWebsite_meta_titleDirtyFlag())
            this.setWebsite_meta_title(srfdomain.getWebsite_meta_title());
        if(srfdomain.getWebsite_meta_keywordsDirtyFlag())
            this.setWebsite_meta_keywords(srfdomain.getWebsite_meta_keywords());
        if(srfdomain.get__last_updateDirtyFlag())
            this.set__last_update(srfdomain.get__last_update());
        if(srfdomain.getWebsite_meta_descriptionDirtyFlag())
            this.setWebsite_meta_description(srfdomain.getWebsite_meta_description());
        if(srfdomain.getIs_seo_optimizedDirtyFlag())
            this.setIs_seo_optimized(srfdomain.getIs_seo_optimized());

    }

    public List<Website_seo_metadataDTO> fromDOPage(List<Website_seo_metadata> poPage)   {
        if(poPage == null)
            return null;
        List<Website_seo_metadataDTO> dtos=new ArrayList<Website_seo_metadataDTO>();
        for(Website_seo_metadata domain : poPage) {
            Website_seo_metadataDTO dto = new Website_seo_metadataDTO();
            dto.fromDO(domain);
            dtos.add(dto);
        }
        return dtos;
    }
}

