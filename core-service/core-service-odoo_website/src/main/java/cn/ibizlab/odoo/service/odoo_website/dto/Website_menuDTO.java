package cn.ibizlab.odoo.service.odoo_website.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.core.odoo_website.valuerule.anno.website_menu.*;
import cn.ibizlab.odoo.core.odoo_website.domain.Website_menu;
import org.springframework.cglib.beans.BeanCopier;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 服务DTO对象[Website_menuDTO]
 */
public class Website_menuDTO implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [THEME_TEMPLATE_ID]
     *
     */
    @Website_menuTheme_template_idDefault(info = "默认规则")
    private Integer theme_template_id;

    @JsonIgnore
    private boolean theme_template_idDirtyFlag;

    /**
     * 属性 [IS_VISIBLE]
     *
     */
    @Website_menuIs_visibleDefault(info = "默认规则")
    private String is_visible;

    @JsonIgnore
    private boolean is_visibleDirtyFlag;

    /**
     * 属性 [URL]
     *
     */
    @Website_menuUrlDefault(info = "默认规则")
    private String url;

    @JsonIgnore
    private boolean urlDirtyFlag;

    /**
     * 属性 [NEW_WINDOW]
     *
     */
    @Website_menuNew_windowDefault(info = "默认规则")
    private String new_window;

    @JsonIgnore
    private boolean new_windowDirtyFlag;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @Website_menu__last_updateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp __last_update;

    @JsonIgnore
    private boolean __last_updateDirtyFlag;

    /**
     * 属性 [PARENT_PATH]
     *
     */
    @Website_menuParent_pathDefault(info = "默认规则")
    private String parent_path;

    @JsonIgnore
    private boolean parent_pathDirtyFlag;

    /**
     * 属性 [CREATE_DATE]
     *
     */
    @Website_menuCreate_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp create_date;

    @JsonIgnore
    private boolean create_dateDirtyFlag;

    /**
     * 属性 [WEBSITE_ID]
     *
     */
    @Website_menuWebsite_idDefault(info = "默认规则")
    private Integer website_id;

    @JsonIgnore
    private boolean website_idDirtyFlag;

    /**
     * 属性 [SEQUENCE]
     *
     */
    @Website_menuSequenceDefault(info = "默认规则")
    private Integer sequence;

    @JsonIgnore
    private boolean sequenceDirtyFlag;

    /**
     * 属性 [ID]
     *
     */
    @Website_menuIdDefault(info = "默认规则")
    private Integer id;

    @JsonIgnore
    private boolean idDirtyFlag;

    /**
     * 属性 [CHILD_ID]
     *
     */
    @Website_menuChild_idDefault(info = "默认规则")
    private String child_id;

    @JsonIgnore
    private boolean child_idDirtyFlag;

    /**
     * 属性 [WRITE_DATE]
     *
     */
    @Website_menuWrite_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp write_date;

    @JsonIgnore
    private boolean write_dateDirtyFlag;

    /**
     * 属性 [NAME]
     *
     */
    @Website_menuNameDefault(info = "默认规则")
    private String name;

    @JsonIgnore
    private boolean nameDirtyFlag;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @Website_menuDisplay_nameDefault(info = "默认规则")
    private String display_name;

    @JsonIgnore
    private boolean display_nameDirtyFlag;

    /**
     * 属性 [CREATE_UID_TEXT]
     *
     */
    @Website_menuCreate_uid_textDefault(info = "默认规则")
    private String create_uid_text;

    @JsonIgnore
    private boolean create_uid_textDirtyFlag;

    /**
     * 属性 [PAGE_ID_TEXT]
     *
     */
    @Website_menuPage_id_textDefault(info = "默认规则")
    private String page_id_text;

    @JsonIgnore
    private boolean page_id_textDirtyFlag;

    /**
     * 属性 [PARENT_ID_TEXT]
     *
     */
    @Website_menuParent_id_textDefault(info = "默认规则")
    private String parent_id_text;

    @JsonIgnore
    private boolean parent_id_textDirtyFlag;

    /**
     * 属性 [WRITE_UID_TEXT]
     *
     */
    @Website_menuWrite_uid_textDefault(info = "默认规则")
    private String write_uid_text;

    @JsonIgnore
    private boolean write_uid_textDirtyFlag;

    /**
     * 属性 [WRITE_UID]
     *
     */
    @Website_menuWrite_uidDefault(info = "默认规则")
    private Integer write_uid;

    @JsonIgnore
    private boolean write_uidDirtyFlag;

    /**
     * 属性 [PAGE_ID]
     *
     */
    @Website_menuPage_idDefault(info = "默认规则")
    private Integer page_id;

    @JsonIgnore
    private boolean page_idDirtyFlag;

    /**
     * 属性 [PARENT_ID]
     *
     */
    @Website_menuParent_idDefault(info = "默认规则")
    private Integer parent_id;

    @JsonIgnore
    private boolean parent_idDirtyFlag;

    /**
     * 属性 [CREATE_UID]
     *
     */
    @Website_menuCreate_uidDefault(info = "默认规则")
    private Integer create_uid;

    @JsonIgnore
    private boolean create_uidDirtyFlag;


    /**
     * 获取 [THEME_TEMPLATE_ID]
     */
    @JsonProperty("theme_template_id")
    public Integer getTheme_template_id(){
        return theme_template_id ;
    }

    /**
     * 设置 [THEME_TEMPLATE_ID]
     */
    @JsonProperty("theme_template_id")
    public void setTheme_template_id(Integer  theme_template_id){
        this.theme_template_id = theme_template_id ;
        this.theme_template_idDirtyFlag = true ;
    }

    /**
     * 获取 [THEME_TEMPLATE_ID]脏标记
     */
    @JsonIgnore
    public boolean getTheme_template_idDirtyFlag(){
        return theme_template_idDirtyFlag ;
    }

    /**
     * 获取 [IS_VISIBLE]
     */
    @JsonProperty("is_visible")
    public String getIs_visible(){
        return is_visible ;
    }

    /**
     * 设置 [IS_VISIBLE]
     */
    @JsonProperty("is_visible")
    public void setIs_visible(String  is_visible){
        this.is_visible = is_visible ;
        this.is_visibleDirtyFlag = true ;
    }

    /**
     * 获取 [IS_VISIBLE]脏标记
     */
    @JsonIgnore
    public boolean getIs_visibleDirtyFlag(){
        return is_visibleDirtyFlag ;
    }

    /**
     * 获取 [URL]
     */
    @JsonProperty("url")
    public String getUrl(){
        return url ;
    }

    /**
     * 设置 [URL]
     */
    @JsonProperty("url")
    public void setUrl(String  url){
        this.url = url ;
        this.urlDirtyFlag = true ;
    }

    /**
     * 获取 [URL]脏标记
     */
    @JsonIgnore
    public boolean getUrlDirtyFlag(){
        return urlDirtyFlag ;
    }

    /**
     * 获取 [NEW_WINDOW]
     */
    @JsonProperty("new_window")
    public String getNew_window(){
        return new_window ;
    }

    /**
     * 设置 [NEW_WINDOW]
     */
    @JsonProperty("new_window")
    public void setNew_window(String  new_window){
        this.new_window = new_window ;
        this.new_windowDirtyFlag = true ;
    }

    /**
     * 获取 [NEW_WINDOW]脏标记
     */
    @JsonIgnore
    public boolean getNew_windowDirtyFlag(){
        return new_windowDirtyFlag ;
    }

    /**
     * 获取 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return __last_update ;
    }

    /**
     * 设置 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

    /**
     * 获取 [__LAST_UPDATE]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return __last_updateDirtyFlag ;
    }

    /**
     * 获取 [PARENT_PATH]
     */
    @JsonProperty("parent_path")
    public String getParent_path(){
        return parent_path ;
    }

    /**
     * 设置 [PARENT_PATH]
     */
    @JsonProperty("parent_path")
    public void setParent_path(String  parent_path){
        this.parent_path = parent_path ;
        this.parent_pathDirtyFlag = true ;
    }

    /**
     * 获取 [PARENT_PATH]脏标记
     */
    @JsonIgnore
    public boolean getParent_pathDirtyFlag(){
        return parent_pathDirtyFlag ;
    }

    /**
     * 获取 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return create_date ;
    }

    /**
     * 设置 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return create_dateDirtyFlag ;
    }

    /**
     * 获取 [WEBSITE_ID]
     */
    @JsonProperty("website_id")
    public Integer getWebsite_id(){
        return website_id ;
    }

    /**
     * 设置 [WEBSITE_ID]
     */
    @JsonProperty("website_id")
    public void setWebsite_id(Integer  website_id){
        this.website_id = website_id ;
        this.website_idDirtyFlag = true ;
    }

    /**
     * 获取 [WEBSITE_ID]脏标记
     */
    @JsonIgnore
    public boolean getWebsite_idDirtyFlag(){
        return website_idDirtyFlag ;
    }

    /**
     * 获取 [SEQUENCE]
     */
    @JsonProperty("sequence")
    public Integer getSequence(){
        return sequence ;
    }

    /**
     * 设置 [SEQUENCE]
     */
    @JsonProperty("sequence")
    public void setSequence(Integer  sequence){
        this.sequence = sequence ;
        this.sequenceDirtyFlag = true ;
    }

    /**
     * 获取 [SEQUENCE]脏标记
     */
    @JsonIgnore
    public boolean getSequenceDirtyFlag(){
        return sequenceDirtyFlag ;
    }

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

    /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return idDirtyFlag ;
    }

    /**
     * 获取 [CHILD_ID]
     */
    @JsonProperty("child_id")
    public String getChild_id(){
        return child_id ;
    }

    /**
     * 设置 [CHILD_ID]
     */
    @JsonProperty("child_id")
    public void setChild_id(String  child_id){
        this.child_id = child_id ;
        this.child_idDirtyFlag = true ;
    }

    /**
     * 获取 [CHILD_ID]脏标记
     */
    @JsonIgnore
    public boolean getChild_idDirtyFlag(){
        return child_idDirtyFlag ;
    }

    /**
     * 获取 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return write_date ;
    }

    /**
     * 设置 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return write_dateDirtyFlag ;
    }

    /**
     * 获取 [NAME]
     */
    @JsonProperty("name")
    public String getName(){
        return name ;
    }

    /**
     * 设置 [NAME]
     */
    @JsonProperty("name")
    public void setName(String  name){
        this.name = name ;
        this.nameDirtyFlag = true ;
    }

    /**
     * 获取 [NAME]脏标记
     */
    @JsonIgnore
    public boolean getNameDirtyFlag(){
        return nameDirtyFlag ;
    }

    /**
     * 获取 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return display_name ;
    }

    /**
     * 设置 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

    /**
     * 获取 [DISPLAY_NAME]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return display_nameDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return create_uid_text ;
    }

    /**
     * 设置 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return create_uid_textDirtyFlag ;
    }

    /**
     * 获取 [PAGE_ID_TEXT]
     */
    @JsonProperty("page_id_text")
    public String getPage_id_text(){
        return page_id_text ;
    }

    /**
     * 设置 [PAGE_ID_TEXT]
     */
    @JsonProperty("page_id_text")
    public void setPage_id_text(String  page_id_text){
        this.page_id_text = page_id_text ;
        this.page_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [PAGE_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getPage_id_textDirtyFlag(){
        return page_id_textDirtyFlag ;
    }

    /**
     * 获取 [PARENT_ID_TEXT]
     */
    @JsonProperty("parent_id_text")
    public String getParent_id_text(){
        return parent_id_text ;
    }

    /**
     * 设置 [PARENT_ID_TEXT]
     */
    @JsonProperty("parent_id_text")
    public void setParent_id_text(String  parent_id_text){
        this.parent_id_text = parent_id_text ;
        this.parent_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [PARENT_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getParent_id_textDirtyFlag(){
        return parent_id_textDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return write_uid_text ;
    }

    /**
     * 设置 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return write_uid_textDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return write_uid ;
    }

    /**
     * 设置 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return write_uidDirtyFlag ;
    }

    /**
     * 获取 [PAGE_ID]
     */
    @JsonProperty("page_id")
    public Integer getPage_id(){
        return page_id ;
    }

    /**
     * 设置 [PAGE_ID]
     */
    @JsonProperty("page_id")
    public void setPage_id(Integer  page_id){
        this.page_id = page_id ;
        this.page_idDirtyFlag = true ;
    }

    /**
     * 获取 [PAGE_ID]脏标记
     */
    @JsonIgnore
    public boolean getPage_idDirtyFlag(){
        return page_idDirtyFlag ;
    }

    /**
     * 获取 [PARENT_ID]
     */
    @JsonProperty("parent_id")
    public Integer getParent_id(){
        return parent_id ;
    }

    /**
     * 设置 [PARENT_ID]
     */
    @JsonProperty("parent_id")
    public void setParent_id(Integer  parent_id){
        this.parent_id = parent_id ;
        this.parent_idDirtyFlag = true ;
    }

    /**
     * 获取 [PARENT_ID]脏标记
     */
    @JsonIgnore
    public boolean getParent_idDirtyFlag(){
        return parent_idDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return create_uid ;
    }

    /**
     * 设置 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return create_uidDirtyFlag ;
    }



    public Website_menu toDO() {
        Website_menu srfdomain = new Website_menu();
        if(getTheme_template_idDirtyFlag())
            srfdomain.setTheme_template_id(theme_template_id);
        if(getIs_visibleDirtyFlag())
            srfdomain.setIs_visible(is_visible);
        if(getUrlDirtyFlag())
            srfdomain.setUrl(url);
        if(getNew_windowDirtyFlag())
            srfdomain.setNew_window(new_window);
        if(get__last_updateDirtyFlag())
            srfdomain.set__last_update(__last_update);
        if(getParent_pathDirtyFlag())
            srfdomain.setParent_path(parent_path);
        if(getCreate_dateDirtyFlag())
            srfdomain.setCreate_date(create_date);
        if(getWebsite_idDirtyFlag())
            srfdomain.setWebsite_id(website_id);
        if(getSequenceDirtyFlag())
            srfdomain.setSequence(sequence);
        if(getIdDirtyFlag())
            srfdomain.setId(id);
        if(getChild_idDirtyFlag())
            srfdomain.setChild_id(child_id);
        if(getWrite_dateDirtyFlag())
            srfdomain.setWrite_date(write_date);
        if(getNameDirtyFlag())
            srfdomain.setName(name);
        if(getDisplay_nameDirtyFlag())
            srfdomain.setDisplay_name(display_name);
        if(getCreate_uid_textDirtyFlag())
            srfdomain.setCreate_uid_text(create_uid_text);
        if(getPage_id_textDirtyFlag())
            srfdomain.setPage_id_text(page_id_text);
        if(getParent_id_textDirtyFlag())
            srfdomain.setParent_id_text(parent_id_text);
        if(getWrite_uid_textDirtyFlag())
            srfdomain.setWrite_uid_text(write_uid_text);
        if(getWrite_uidDirtyFlag())
            srfdomain.setWrite_uid(write_uid);
        if(getPage_idDirtyFlag())
            srfdomain.setPage_id(page_id);
        if(getParent_idDirtyFlag())
            srfdomain.setParent_id(parent_id);
        if(getCreate_uidDirtyFlag())
            srfdomain.setCreate_uid(create_uid);

        return srfdomain;
    }

    public void fromDO(Website_menu srfdomain) {
        if(srfdomain == null )
          return ;
        if(srfdomain.getTheme_template_idDirtyFlag())
            this.setTheme_template_id(srfdomain.getTheme_template_id());
        if(srfdomain.getIs_visibleDirtyFlag())
            this.setIs_visible(srfdomain.getIs_visible());
        if(srfdomain.getUrlDirtyFlag())
            this.setUrl(srfdomain.getUrl());
        if(srfdomain.getNew_windowDirtyFlag())
            this.setNew_window(srfdomain.getNew_window());
        if(srfdomain.get__last_updateDirtyFlag())
            this.set__last_update(srfdomain.get__last_update());
        if(srfdomain.getParent_pathDirtyFlag())
            this.setParent_path(srfdomain.getParent_path());
        if(srfdomain.getCreate_dateDirtyFlag())
            this.setCreate_date(srfdomain.getCreate_date());
        if(srfdomain.getWebsite_idDirtyFlag())
            this.setWebsite_id(srfdomain.getWebsite_id());
        if(srfdomain.getSequenceDirtyFlag())
            this.setSequence(srfdomain.getSequence());
        if(srfdomain.getIdDirtyFlag())
            this.setId(srfdomain.getId());
        if(srfdomain.getChild_idDirtyFlag())
            this.setChild_id(srfdomain.getChild_id());
        if(srfdomain.getWrite_dateDirtyFlag())
            this.setWrite_date(srfdomain.getWrite_date());
        if(srfdomain.getNameDirtyFlag())
            this.setName(srfdomain.getName());
        if(srfdomain.getDisplay_nameDirtyFlag())
            this.setDisplay_name(srfdomain.getDisplay_name());
        if(srfdomain.getCreate_uid_textDirtyFlag())
            this.setCreate_uid_text(srfdomain.getCreate_uid_text());
        if(srfdomain.getPage_id_textDirtyFlag())
            this.setPage_id_text(srfdomain.getPage_id_text());
        if(srfdomain.getParent_id_textDirtyFlag())
            this.setParent_id_text(srfdomain.getParent_id_text());
        if(srfdomain.getWrite_uid_textDirtyFlag())
            this.setWrite_uid_text(srfdomain.getWrite_uid_text());
        if(srfdomain.getWrite_uidDirtyFlag())
            this.setWrite_uid(srfdomain.getWrite_uid());
        if(srfdomain.getPage_idDirtyFlag())
            this.setPage_id(srfdomain.getPage_id());
        if(srfdomain.getParent_idDirtyFlag())
            this.setParent_id(srfdomain.getParent_id());
        if(srfdomain.getCreate_uidDirtyFlag())
            this.setCreate_uid(srfdomain.getCreate_uid());

    }

    public List<Website_menuDTO> fromDOPage(List<Website_menu> poPage)   {
        if(poPage == null)
            return null;
        List<Website_menuDTO> dtos=new ArrayList<Website_menuDTO>();
        for(Website_menu domain : poPage) {
            Website_menuDTO dto = new Website_menuDTO();
            dto.fromDO(domain);
            dtos.add(dto);
        }
        return dtos;
    }
}

