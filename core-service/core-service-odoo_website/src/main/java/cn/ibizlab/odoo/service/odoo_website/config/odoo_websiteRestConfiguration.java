package cn.ibizlab.odoo.service.odoo_website.config;

import org.springframework.context.annotation.ComponentScan;

@ComponentScan("cn.ibizlab.odoo.service.odoo_website")
public class odoo_websiteRestConfiguration {

}
