package cn.ibizlab.odoo.service.odoo_event.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_event.dto.Event_eventDTO;
import cn.ibizlab.odoo.core.odoo_event.domain.Event_event;
import cn.ibizlab.odoo.core.odoo_event.service.IEvent_eventService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_event.filter.Event_eventSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Event_event" })
@RestController
@RequestMapping("")
public class Event_eventResource {

    @Autowired
    private IEvent_eventService event_eventService;

    public IEvent_eventService getEvent_eventService() {
        return this.event_eventService;
    }

    @ApiOperation(value = "获取数据", tags = {"Event_event" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_event/event_events/{event_event_id}")
    public ResponseEntity<Event_eventDTO> get(@PathVariable("event_event_id") Integer event_event_id) {
        Event_eventDTO dto = new Event_eventDTO();
        Event_event domain = event_eventService.get(event_event_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "建立数据", tags = {"Event_event" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_event/event_events")

    public ResponseEntity<Event_eventDTO> create(@RequestBody Event_eventDTO event_eventdto) {
        Event_eventDTO dto = new Event_eventDTO();
        Event_event domain = event_eventdto.toDO();
		event_eventService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批删除数据", tags = {"Event_event" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_event/event_events/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Event_eventDTO> event_eventdtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "更新数据", tags = {"Event_event" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_event/event_events/{event_event_id}")

    public ResponseEntity<Event_eventDTO> update(@PathVariable("event_event_id") Integer event_event_id, @RequestBody Event_eventDTO event_eventdto) {
		Event_event domain = event_eventdto.toDO();
        domain.setId(event_event_id);
		event_eventService.update(domain);
		Event_eventDTO dto = new Event_eventDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批建立数据", tags = {"Event_event" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_event/event_events/createBatch")
    public ResponseEntity<Boolean> createBatchEvent_event(@RequestBody List<Event_eventDTO> event_eventdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "批更新数据", tags = {"Event_event" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_event/event_events/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Event_eventDTO> event_eventdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "删除数据", tags = {"Event_event" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_event/event_events/{event_event_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("event_event_id") Integer event_event_id) {
        Event_eventDTO event_eventdto = new Event_eventDTO();
		Event_event domain = new Event_event();
		event_eventdto.setId(event_event_id);
		domain.setId(event_event_id);
        Boolean rst = event_eventService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

	@ApiOperation(value = "获取默认查询", tags = {"Event_event" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_event/event_events/fetchdefault")
	public ResponseEntity<Page<Event_eventDTO>> fetchDefault(Event_eventSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Event_eventDTO> list = new ArrayList<Event_eventDTO>();
        
        Page<Event_event> domains = event_eventService.searchDefault(context) ;
        for(Event_event event_event : domains.getContent()){
            Event_eventDTO dto = new Event_eventDTO();
            dto.fromDO(event_event);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
