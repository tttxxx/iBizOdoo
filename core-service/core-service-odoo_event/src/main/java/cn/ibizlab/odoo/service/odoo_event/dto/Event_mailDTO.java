package cn.ibizlab.odoo.service.odoo_event.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.core.odoo_event.valuerule.anno.event_mail.*;
import cn.ibizlab.odoo.core.odoo_event.domain.Event_mail;
import org.springframework.cglib.beans.BeanCopier;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 服务DTO对象[Event_mailDTO]
 */
public class Event_mailDTO implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [MAIL_REGISTRATION_IDS]
     *
     */
    @Event_mailMail_registration_idsDefault(info = "默认规则")
    private String mail_registration_ids;

    @JsonIgnore
    private boolean mail_registration_idsDirtyFlag;

    /**
     * 属性 [ID]
     *
     */
    @Event_mailIdDefault(info = "默认规则")
    private Integer id;

    @JsonIgnore
    private boolean idDirtyFlag;

    /**
     * 属性 [INTERVAL_NBR]
     *
     */
    @Event_mailInterval_nbrDefault(info = "默认规则")
    private Integer interval_nbr;

    @JsonIgnore
    private boolean interval_nbrDirtyFlag;

    /**
     * 属性 [DONE]
     *
     */
    @Event_mailDoneDefault(info = "默认规则")
    private String done;

    @JsonIgnore
    private boolean doneDirtyFlag;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @Event_mail__last_updateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp __last_update;

    @JsonIgnore
    private boolean __last_updateDirtyFlag;

    /**
     * 属性 [WRITE_DATE]
     *
     */
    @Event_mailWrite_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp write_date;

    @JsonIgnore
    private boolean write_dateDirtyFlag;

    /**
     * 属性 [INTERVAL_TYPE]
     *
     */
    @Event_mailInterval_typeDefault(info = "默认规则")
    private String interval_type;

    @JsonIgnore
    private boolean interval_typeDirtyFlag;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @Event_mailDisplay_nameDefault(info = "默认规则")
    private String display_name;

    @JsonIgnore
    private boolean display_nameDirtyFlag;

    /**
     * 属性 [MAIL_SENT]
     *
     */
    @Event_mailMail_sentDefault(info = "默认规则")
    private String mail_sent;

    @JsonIgnore
    private boolean mail_sentDirtyFlag;

    /**
     * 属性 [SEQUENCE]
     *
     */
    @Event_mailSequenceDefault(info = "默认规则")
    private Integer sequence;

    @JsonIgnore
    private boolean sequenceDirtyFlag;

    /**
     * 属性 [SCHEDULED_DATE]
     *
     */
    @Event_mailScheduled_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp scheduled_date;

    @JsonIgnore
    private boolean scheduled_dateDirtyFlag;

    /**
     * 属性 [INTERVAL_UNIT]
     *
     */
    @Event_mailInterval_unitDefault(info = "默认规则")
    private String interval_unit;

    @JsonIgnore
    private boolean interval_unitDirtyFlag;

    /**
     * 属性 [CREATE_DATE]
     *
     */
    @Event_mailCreate_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp create_date;

    @JsonIgnore
    private boolean create_dateDirtyFlag;

    /**
     * 属性 [EVENT_ID_TEXT]
     *
     */
    @Event_mailEvent_id_textDefault(info = "默认规则")
    private String event_id_text;

    @JsonIgnore
    private boolean event_id_textDirtyFlag;

    /**
     * 属性 [WRITE_UID_TEXT]
     *
     */
    @Event_mailWrite_uid_textDefault(info = "默认规则")
    private String write_uid_text;

    @JsonIgnore
    private boolean write_uid_textDirtyFlag;

    /**
     * 属性 [TEMPLATE_ID_TEXT]
     *
     */
    @Event_mailTemplate_id_textDefault(info = "默认规则")
    private String template_id_text;

    @JsonIgnore
    private boolean template_id_textDirtyFlag;

    /**
     * 属性 [CREATE_UID_TEXT]
     *
     */
    @Event_mailCreate_uid_textDefault(info = "默认规则")
    private String create_uid_text;

    @JsonIgnore
    private boolean create_uid_textDirtyFlag;

    /**
     * 属性 [CREATE_UID]
     *
     */
    @Event_mailCreate_uidDefault(info = "默认规则")
    private Integer create_uid;

    @JsonIgnore
    private boolean create_uidDirtyFlag;

    /**
     * 属性 [EVENT_ID]
     *
     */
    @Event_mailEvent_idDefault(info = "默认规则")
    private Integer event_id;

    @JsonIgnore
    private boolean event_idDirtyFlag;

    /**
     * 属性 [WRITE_UID]
     *
     */
    @Event_mailWrite_uidDefault(info = "默认规则")
    private Integer write_uid;

    @JsonIgnore
    private boolean write_uidDirtyFlag;

    /**
     * 属性 [TEMPLATE_ID]
     *
     */
    @Event_mailTemplate_idDefault(info = "默认规则")
    private Integer template_id;

    @JsonIgnore
    private boolean template_idDirtyFlag;


    /**
     * 获取 [MAIL_REGISTRATION_IDS]
     */
    @JsonProperty("mail_registration_ids")
    public String getMail_registration_ids(){
        return mail_registration_ids ;
    }

    /**
     * 设置 [MAIL_REGISTRATION_IDS]
     */
    @JsonProperty("mail_registration_ids")
    public void setMail_registration_ids(String  mail_registration_ids){
        this.mail_registration_ids = mail_registration_ids ;
        this.mail_registration_idsDirtyFlag = true ;
    }

    /**
     * 获取 [MAIL_REGISTRATION_IDS]脏标记
     */
    @JsonIgnore
    public boolean getMail_registration_idsDirtyFlag(){
        return mail_registration_idsDirtyFlag ;
    }

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

    /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return idDirtyFlag ;
    }

    /**
     * 获取 [INTERVAL_NBR]
     */
    @JsonProperty("interval_nbr")
    public Integer getInterval_nbr(){
        return interval_nbr ;
    }

    /**
     * 设置 [INTERVAL_NBR]
     */
    @JsonProperty("interval_nbr")
    public void setInterval_nbr(Integer  interval_nbr){
        this.interval_nbr = interval_nbr ;
        this.interval_nbrDirtyFlag = true ;
    }

    /**
     * 获取 [INTERVAL_NBR]脏标记
     */
    @JsonIgnore
    public boolean getInterval_nbrDirtyFlag(){
        return interval_nbrDirtyFlag ;
    }

    /**
     * 获取 [DONE]
     */
    @JsonProperty("done")
    public String getDone(){
        return done ;
    }

    /**
     * 设置 [DONE]
     */
    @JsonProperty("done")
    public void setDone(String  done){
        this.done = done ;
        this.doneDirtyFlag = true ;
    }

    /**
     * 获取 [DONE]脏标记
     */
    @JsonIgnore
    public boolean getDoneDirtyFlag(){
        return doneDirtyFlag ;
    }

    /**
     * 获取 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return __last_update ;
    }

    /**
     * 设置 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

    /**
     * 获取 [__LAST_UPDATE]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return __last_updateDirtyFlag ;
    }

    /**
     * 获取 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return write_date ;
    }

    /**
     * 设置 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return write_dateDirtyFlag ;
    }

    /**
     * 获取 [INTERVAL_TYPE]
     */
    @JsonProperty("interval_type")
    public String getInterval_type(){
        return interval_type ;
    }

    /**
     * 设置 [INTERVAL_TYPE]
     */
    @JsonProperty("interval_type")
    public void setInterval_type(String  interval_type){
        this.interval_type = interval_type ;
        this.interval_typeDirtyFlag = true ;
    }

    /**
     * 获取 [INTERVAL_TYPE]脏标记
     */
    @JsonIgnore
    public boolean getInterval_typeDirtyFlag(){
        return interval_typeDirtyFlag ;
    }

    /**
     * 获取 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return display_name ;
    }

    /**
     * 设置 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

    /**
     * 获取 [DISPLAY_NAME]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return display_nameDirtyFlag ;
    }

    /**
     * 获取 [MAIL_SENT]
     */
    @JsonProperty("mail_sent")
    public String getMail_sent(){
        return mail_sent ;
    }

    /**
     * 设置 [MAIL_SENT]
     */
    @JsonProperty("mail_sent")
    public void setMail_sent(String  mail_sent){
        this.mail_sent = mail_sent ;
        this.mail_sentDirtyFlag = true ;
    }

    /**
     * 获取 [MAIL_SENT]脏标记
     */
    @JsonIgnore
    public boolean getMail_sentDirtyFlag(){
        return mail_sentDirtyFlag ;
    }

    /**
     * 获取 [SEQUENCE]
     */
    @JsonProperty("sequence")
    public Integer getSequence(){
        return sequence ;
    }

    /**
     * 设置 [SEQUENCE]
     */
    @JsonProperty("sequence")
    public void setSequence(Integer  sequence){
        this.sequence = sequence ;
        this.sequenceDirtyFlag = true ;
    }

    /**
     * 获取 [SEQUENCE]脏标记
     */
    @JsonIgnore
    public boolean getSequenceDirtyFlag(){
        return sequenceDirtyFlag ;
    }

    /**
     * 获取 [SCHEDULED_DATE]
     */
    @JsonProperty("scheduled_date")
    public Timestamp getScheduled_date(){
        return scheduled_date ;
    }

    /**
     * 设置 [SCHEDULED_DATE]
     */
    @JsonProperty("scheduled_date")
    public void setScheduled_date(Timestamp  scheduled_date){
        this.scheduled_date = scheduled_date ;
        this.scheduled_dateDirtyFlag = true ;
    }

    /**
     * 获取 [SCHEDULED_DATE]脏标记
     */
    @JsonIgnore
    public boolean getScheduled_dateDirtyFlag(){
        return scheduled_dateDirtyFlag ;
    }

    /**
     * 获取 [INTERVAL_UNIT]
     */
    @JsonProperty("interval_unit")
    public String getInterval_unit(){
        return interval_unit ;
    }

    /**
     * 设置 [INTERVAL_UNIT]
     */
    @JsonProperty("interval_unit")
    public void setInterval_unit(String  interval_unit){
        this.interval_unit = interval_unit ;
        this.interval_unitDirtyFlag = true ;
    }

    /**
     * 获取 [INTERVAL_UNIT]脏标记
     */
    @JsonIgnore
    public boolean getInterval_unitDirtyFlag(){
        return interval_unitDirtyFlag ;
    }

    /**
     * 获取 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return create_date ;
    }

    /**
     * 设置 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return create_dateDirtyFlag ;
    }

    /**
     * 获取 [EVENT_ID_TEXT]
     */
    @JsonProperty("event_id_text")
    public String getEvent_id_text(){
        return event_id_text ;
    }

    /**
     * 设置 [EVENT_ID_TEXT]
     */
    @JsonProperty("event_id_text")
    public void setEvent_id_text(String  event_id_text){
        this.event_id_text = event_id_text ;
        this.event_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [EVENT_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getEvent_id_textDirtyFlag(){
        return event_id_textDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return write_uid_text ;
    }

    /**
     * 设置 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return write_uid_textDirtyFlag ;
    }

    /**
     * 获取 [TEMPLATE_ID_TEXT]
     */
    @JsonProperty("template_id_text")
    public String getTemplate_id_text(){
        return template_id_text ;
    }

    /**
     * 设置 [TEMPLATE_ID_TEXT]
     */
    @JsonProperty("template_id_text")
    public void setTemplate_id_text(String  template_id_text){
        this.template_id_text = template_id_text ;
        this.template_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [TEMPLATE_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getTemplate_id_textDirtyFlag(){
        return template_id_textDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return create_uid_text ;
    }

    /**
     * 设置 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return create_uid_textDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return create_uid ;
    }

    /**
     * 设置 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return create_uidDirtyFlag ;
    }

    /**
     * 获取 [EVENT_ID]
     */
    @JsonProperty("event_id")
    public Integer getEvent_id(){
        return event_id ;
    }

    /**
     * 设置 [EVENT_ID]
     */
    @JsonProperty("event_id")
    public void setEvent_id(Integer  event_id){
        this.event_id = event_id ;
        this.event_idDirtyFlag = true ;
    }

    /**
     * 获取 [EVENT_ID]脏标记
     */
    @JsonIgnore
    public boolean getEvent_idDirtyFlag(){
        return event_idDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return write_uid ;
    }

    /**
     * 设置 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return write_uidDirtyFlag ;
    }

    /**
     * 获取 [TEMPLATE_ID]
     */
    @JsonProperty("template_id")
    public Integer getTemplate_id(){
        return template_id ;
    }

    /**
     * 设置 [TEMPLATE_ID]
     */
    @JsonProperty("template_id")
    public void setTemplate_id(Integer  template_id){
        this.template_id = template_id ;
        this.template_idDirtyFlag = true ;
    }

    /**
     * 获取 [TEMPLATE_ID]脏标记
     */
    @JsonIgnore
    public boolean getTemplate_idDirtyFlag(){
        return template_idDirtyFlag ;
    }



    public Event_mail toDO() {
        Event_mail srfdomain = new Event_mail();
        if(getMail_registration_idsDirtyFlag())
            srfdomain.setMail_registration_ids(mail_registration_ids);
        if(getIdDirtyFlag())
            srfdomain.setId(id);
        if(getInterval_nbrDirtyFlag())
            srfdomain.setInterval_nbr(interval_nbr);
        if(getDoneDirtyFlag())
            srfdomain.setDone(done);
        if(get__last_updateDirtyFlag())
            srfdomain.set__last_update(__last_update);
        if(getWrite_dateDirtyFlag())
            srfdomain.setWrite_date(write_date);
        if(getInterval_typeDirtyFlag())
            srfdomain.setInterval_type(interval_type);
        if(getDisplay_nameDirtyFlag())
            srfdomain.setDisplay_name(display_name);
        if(getMail_sentDirtyFlag())
            srfdomain.setMail_sent(mail_sent);
        if(getSequenceDirtyFlag())
            srfdomain.setSequence(sequence);
        if(getScheduled_dateDirtyFlag())
            srfdomain.setScheduled_date(scheduled_date);
        if(getInterval_unitDirtyFlag())
            srfdomain.setInterval_unit(interval_unit);
        if(getCreate_dateDirtyFlag())
            srfdomain.setCreate_date(create_date);
        if(getEvent_id_textDirtyFlag())
            srfdomain.setEvent_id_text(event_id_text);
        if(getWrite_uid_textDirtyFlag())
            srfdomain.setWrite_uid_text(write_uid_text);
        if(getTemplate_id_textDirtyFlag())
            srfdomain.setTemplate_id_text(template_id_text);
        if(getCreate_uid_textDirtyFlag())
            srfdomain.setCreate_uid_text(create_uid_text);
        if(getCreate_uidDirtyFlag())
            srfdomain.setCreate_uid(create_uid);
        if(getEvent_idDirtyFlag())
            srfdomain.setEvent_id(event_id);
        if(getWrite_uidDirtyFlag())
            srfdomain.setWrite_uid(write_uid);
        if(getTemplate_idDirtyFlag())
            srfdomain.setTemplate_id(template_id);

        return srfdomain;
    }

    public void fromDO(Event_mail srfdomain) {
        if(srfdomain == null )
          return ;
        if(srfdomain.getMail_registration_idsDirtyFlag())
            this.setMail_registration_ids(srfdomain.getMail_registration_ids());
        if(srfdomain.getIdDirtyFlag())
            this.setId(srfdomain.getId());
        if(srfdomain.getInterval_nbrDirtyFlag())
            this.setInterval_nbr(srfdomain.getInterval_nbr());
        if(srfdomain.getDoneDirtyFlag())
            this.setDone(srfdomain.getDone());
        if(srfdomain.get__last_updateDirtyFlag())
            this.set__last_update(srfdomain.get__last_update());
        if(srfdomain.getWrite_dateDirtyFlag())
            this.setWrite_date(srfdomain.getWrite_date());
        if(srfdomain.getInterval_typeDirtyFlag())
            this.setInterval_type(srfdomain.getInterval_type());
        if(srfdomain.getDisplay_nameDirtyFlag())
            this.setDisplay_name(srfdomain.getDisplay_name());
        if(srfdomain.getMail_sentDirtyFlag())
            this.setMail_sent(srfdomain.getMail_sent());
        if(srfdomain.getSequenceDirtyFlag())
            this.setSequence(srfdomain.getSequence());
        if(srfdomain.getScheduled_dateDirtyFlag())
            this.setScheduled_date(srfdomain.getScheduled_date());
        if(srfdomain.getInterval_unitDirtyFlag())
            this.setInterval_unit(srfdomain.getInterval_unit());
        if(srfdomain.getCreate_dateDirtyFlag())
            this.setCreate_date(srfdomain.getCreate_date());
        if(srfdomain.getEvent_id_textDirtyFlag())
            this.setEvent_id_text(srfdomain.getEvent_id_text());
        if(srfdomain.getWrite_uid_textDirtyFlag())
            this.setWrite_uid_text(srfdomain.getWrite_uid_text());
        if(srfdomain.getTemplate_id_textDirtyFlag())
            this.setTemplate_id_text(srfdomain.getTemplate_id_text());
        if(srfdomain.getCreate_uid_textDirtyFlag())
            this.setCreate_uid_text(srfdomain.getCreate_uid_text());
        if(srfdomain.getCreate_uidDirtyFlag())
            this.setCreate_uid(srfdomain.getCreate_uid());
        if(srfdomain.getEvent_idDirtyFlag())
            this.setEvent_id(srfdomain.getEvent_id());
        if(srfdomain.getWrite_uidDirtyFlag())
            this.setWrite_uid(srfdomain.getWrite_uid());
        if(srfdomain.getTemplate_idDirtyFlag())
            this.setTemplate_id(srfdomain.getTemplate_id());

    }

    public List<Event_mailDTO> fromDOPage(List<Event_mail> poPage)   {
        if(poPage == null)
            return null;
        List<Event_mailDTO> dtos=new ArrayList<Event_mailDTO>();
        for(Event_mail domain : poPage) {
            Event_mailDTO dto = new Event_mailDTO();
            dto.fromDO(domain);
            dtos.add(dto);
        }
        return dtos;
    }
}

