package cn.ibizlab.odoo.service.odoo_event.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_event.dto.Event_type_mailDTO;
import cn.ibizlab.odoo.core.odoo_event.domain.Event_type_mail;
import cn.ibizlab.odoo.core.odoo_event.service.IEvent_type_mailService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_event.filter.Event_type_mailSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Event_type_mail" })
@RestController
@RequestMapping("")
public class Event_type_mailResource {

    @Autowired
    private IEvent_type_mailService event_type_mailService;

    public IEvent_type_mailService getEvent_type_mailService() {
        return this.event_type_mailService;
    }

    @ApiOperation(value = "批更新数据", tags = {"Event_type_mail" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_event/event_type_mails/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Event_type_mailDTO> event_type_maildtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "删除数据", tags = {"Event_type_mail" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_event/event_type_mails/{event_type_mail_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("event_type_mail_id") Integer event_type_mail_id) {
        Event_type_mailDTO event_type_maildto = new Event_type_mailDTO();
		Event_type_mail domain = new Event_type_mail();
		event_type_maildto.setId(event_type_mail_id);
		domain.setId(event_type_mail_id);
        Boolean rst = event_type_mailService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "建立数据", tags = {"Event_type_mail" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_event/event_type_mails")

    public ResponseEntity<Event_type_mailDTO> create(@RequestBody Event_type_mailDTO event_type_maildto) {
        Event_type_mailDTO dto = new Event_type_mailDTO();
        Event_type_mail domain = event_type_maildto.toDO();
		event_type_mailService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "更新数据", tags = {"Event_type_mail" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_event/event_type_mails/{event_type_mail_id}")

    public ResponseEntity<Event_type_mailDTO> update(@PathVariable("event_type_mail_id") Integer event_type_mail_id, @RequestBody Event_type_mailDTO event_type_maildto) {
		Event_type_mail domain = event_type_maildto.toDO();
        domain.setId(event_type_mail_id);
		event_type_mailService.update(domain);
		Event_type_mailDTO dto = new Event_type_mailDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取数据", tags = {"Event_type_mail" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_event/event_type_mails/{event_type_mail_id}")
    public ResponseEntity<Event_type_mailDTO> get(@PathVariable("event_type_mail_id") Integer event_type_mail_id) {
        Event_type_mailDTO dto = new Event_type_mailDTO();
        Event_type_mail domain = event_type_mailService.get(event_type_mail_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批删除数据", tags = {"Event_type_mail" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_event/event_type_mails/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Event_type_mailDTO> event_type_maildtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "批建立数据", tags = {"Event_type_mail" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_event/event_type_mails/createBatch")
    public ResponseEntity<Boolean> createBatchEvent_type_mail(@RequestBody List<Event_type_mailDTO> event_type_maildtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Event_type_mail" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_event/event_type_mails/fetchdefault")
	public ResponseEntity<Page<Event_type_mailDTO>> fetchDefault(Event_type_mailSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Event_type_mailDTO> list = new ArrayList<Event_type_mailDTO>();
        
        Page<Event_type_mail> domains = event_type_mailService.searchDefault(context) ;
        for(Event_type_mail event_type_mail : domains.getContent()){
            Event_type_mailDTO dto = new Event_type_mailDTO();
            dto.fromDO(event_type_mail);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
