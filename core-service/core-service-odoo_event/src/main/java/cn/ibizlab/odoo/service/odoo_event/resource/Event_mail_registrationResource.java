package cn.ibizlab.odoo.service.odoo_event.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_event.dto.Event_mail_registrationDTO;
import cn.ibizlab.odoo.core.odoo_event.domain.Event_mail_registration;
import cn.ibizlab.odoo.core.odoo_event.service.IEvent_mail_registrationService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_event.filter.Event_mail_registrationSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Event_mail_registration" })
@RestController
@RequestMapping("")
public class Event_mail_registrationResource {

    @Autowired
    private IEvent_mail_registrationService event_mail_registrationService;

    public IEvent_mail_registrationService getEvent_mail_registrationService() {
        return this.event_mail_registrationService;
    }

    @ApiOperation(value = "删除数据", tags = {"Event_mail_registration" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_event/event_mail_registrations/{event_mail_registration_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("event_mail_registration_id") Integer event_mail_registration_id) {
        Event_mail_registrationDTO event_mail_registrationdto = new Event_mail_registrationDTO();
		Event_mail_registration domain = new Event_mail_registration();
		event_mail_registrationdto.setId(event_mail_registration_id);
		domain.setId(event_mail_registration_id);
        Boolean rst = event_mail_registrationService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "更新数据", tags = {"Event_mail_registration" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_event/event_mail_registrations/{event_mail_registration_id}")

    public ResponseEntity<Event_mail_registrationDTO> update(@PathVariable("event_mail_registration_id") Integer event_mail_registration_id, @RequestBody Event_mail_registrationDTO event_mail_registrationdto) {
		Event_mail_registration domain = event_mail_registrationdto.toDO();
        domain.setId(event_mail_registration_id);
		event_mail_registrationService.update(domain);
		Event_mail_registrationDTO dto = new Event_mail_registrationDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批建立数据", tags = {"Event_mail_registration" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_event/event_mail_registrations/createBatch")
    public ResponseEntity<Boolean> createBatchEvent_mail_registration(@RequestBody List<Event_mail_registrationDTO> event_mail_registrationdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "批更新数据", tags = {"Event_mail_registration" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_event/event_mail_registrations/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Event_mail_registrationDTO> event_mail_registrationdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "建立数据", tags = {"Event_mail_registration" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_event/event_mail_registrations")

    public ResponseEntity<Event_mail_registrationDTO> create(@RequestBody Event_mail_registrationDTO event_mail_registrationdto) {
        Event_mail_registrationDTO dto = new Event_mail_registrationDTO();
        Event_mail_registration domain = event_mail_registrationdto.toDO();
		event_mail_registrationService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批删除数据", tags = {"Event_mail_registration" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_event/event_mail_registrations/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Event_mail_registrationDTO> event_mail_registrationdtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "获取数据", tags = {"Event_mail_registration" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_event/event_mail_registrations/{event_mail_registration_id}")
    public ResponseEntity<Event_mail_registrationDTO> get(@PathVariable("event_mail_registration_id") Integer event_mail_registration_id) {
        Event_mail_registrationDTO dto = new Event_mail_registrationDTO();
        Event_mail_registration domain = event_mail_registrationService.get(event_mail_registration_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Event_mail_registration" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_event/event_mail_registrations/fetchdefault")
	public ResponseEntity<Page<Event_mail_registrationDTO>> fetchDefault(Event_mail_registrationSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Event_mail_registrationDTO> list = new ArrayList<Event_mail_registrationDTO>();
        
        Page<Event_mail_registration> domains = event_mail_registrationService.searchDefault(context) ;
        for(Event_mail_registration event_mail_registration : domains.getContent()){
            Event_mail_registrationDTO dto = new Event_mail_registrationDTO();
            dto.fromDO(event_mail_registration);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
