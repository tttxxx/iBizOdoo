package cn.ibizlab.odoo.service.odoo_event.config;

import org.springframework.context.annotation.ComponentScan;

@ComponentScan("cn.ibizlab.odoo.service.odoo_event")
public class odoo_eventRestConfiguration {

}
