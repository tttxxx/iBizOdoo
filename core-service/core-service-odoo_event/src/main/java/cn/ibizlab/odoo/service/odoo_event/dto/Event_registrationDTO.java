package cn.ibizlab.odoo.service.odoo_event.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.core.odoo_event.valuerule.anno.event_registration.*;
import cn.ibizlab.odoo.core.odoo_event.domain.Event_registration;
import org.springframework.cglib.beans.BeanCopier;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 服务DTO对象[Event_registrationDTO]
 */
public class Event_registrationDTO implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [MESSAGE_MAIN_ATTACHMENT_ID]
     *
     */
    @Event_registrationMessage_main_attachment_idDefault(info = "默认规则")
    private Integer message_main_attachment_id;

    @JsonIgnore
    private boolean message_main_attachment_idDirtyFlag;

    /**
     * 属性 [PHONE]
     *
     */
    @Event_registrationPhoneDefault(info = "默认规则")
    private String phone;

    @JsonIgnore
    private boolean phoneDirtyFlag;

    /**
     * 属性 [DATE_OPEN]
     *
     */
    @Event_registrationDate_openDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp date_open;

    @JsonIgnore
    private boolean date_openDirtyFlag;

    /**
     * 属性 [STATE]
     *
     */
    @Event_registrationStateDefault(info = "默认规则")
    private String state;

    @JsonIgnore
    private boolean stateDirtyFlag;

    /**
     * 属性 [MESSAGE_UNREAD]
     *
     */
    @Event_registrationMessage_unreadDefault(info = "默认规则")
    private String message_unread;

    @JsonIgnore
    private boolean message_unreadDirtyFlag;

    /**
     * 属性 [MESSAGE_FOLLOWER_IDS]
     *
     */
    @Event_registrationMessage_follower_idsDefault(info = "默认规则")
    private String message_follower_ids;

    @JsonIgnore
    private boolean message_follower_idsDirtyFlag;

    /**
     * 属性 [MESSAGE_IS_FOLLOWER]
     *
     */
    @Event_registrationMessage_is_followerDefault(info = "默认规则")
    private String message_is_follower;

    @JsonIgnore
    private boolean message_is_followerDirtyFlag;

    /**
     * 属性 [WRITE_DATE]
     *
     */
    @Event_registrationWrite_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp write_date;

    @JsonIgnore
    private boolean write_dateDirtyFlag;

    /**
     * 属性 [ORIGIN]
     *
     */
    @Event_registrationOriginDefault(info = "默认规则")
    private String origin;

    @JsonIgnore
    private boolean originDirtyFlag;

    /**
     * 属性 [MESSAGE_CHANNEL_IDS]
     *
     */
    @Event_registrationMessage_channel_idsDefault(info = "默认规则")
    private String message_channel_ids;

    @JsonIgnore
    private boolean message_channel_idsDirtyFlag;

    /**
     * 属性 [MESSAGE_HAS_ERROR_COUNTER]
     *
     */
    @Event_registrationMessage_has_error_counterDefault(info = "默认规则")
    private Integer message_has_error_counter;

    @JsonIgnore
    private boolean message_has_error_counterDirtyFlag;

    /**
     * 属性 [MESSAGE_PARTNER_IDS]
     *
     */
    @Event_registrationMessage_partner_idsDefault(info = "默认规则")
    private String message_partner_ids;

    @JsonIgnore
    private boolean message_partner_idsDirtyFlag;

    /**
     * 属性 [MESSAGE_ATTACHMENT_COUNT]
     *
     */
    @Event_registrationMessage_attachment_countDefault(info = "默认规则")
    private Integer message_attachment_count;

    @JsonIgnore
    private boolean message_attachment_countDirtyFlag;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @Event_registration__last_updateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp __last_update;

    @JsonIgnore
    private boolean __last_updateDirtyFlag;

    /**
     * 属性 [EMAIL]
     *
     */
    @Event_registrationEmailDefault(info = "默认规则")
    private String email;

    @JsonIgnore
    private boolean emailDirtyFlag;

    /**
     * 属性 [MESSAGE_HAS_ERROR]
     *
     */
    @Event_registrationMessage_has_errorDefault(info = "默认规则")
    private String message_has_error;

    @JsonIgnore
    private boolean message_has_errorDirtyFlag;

    /**
     * 属性 [MESSAGE_UNREAD_COUNTER]
     *
     */
    @Event_registrationMessage_unread_counterDefault(info = "默认规则")
    private Integer message_unread_counter;

    @JsonIgnore
    private boolean message_unread_counterDirtyFlag;

    /**
     * 属性 [MESSAGE_NEEDACTION]
     *
     */
    @Event_registrationMessage_needactionDefault(info = "默认规则")
    private String message_needaction;

    @JsonIgnore
    private boolean message_needactionDirtyFlag;

    /**
     * 属性 [ID]
     *
     */
    @Event_registrationIdDefault(info = "默认规则")
    private Integer id;

    @JsonIgnore
    private boolean idDirtyFlag;

    /**
     * 属性 [MESSAGE_NEEDACTION_COUNTER]
     *
     */
    @Event_registrationMessage_needaction_counterDefault(info = "默认规则")
    private Integer message_needaction_counter;

    @JsonIgnore
    private boolean message_needaction_counterDirtyFlag;

    /**
     * 属性 [NAME]
     *
     */
    @Event_registrationNameDefault(info = "默认规则")
    private String name;

    @JsonIgnore
    private boolean nameDirtyFlag;

    /**
     * 属性 [DATE_CLOSED]
     *
     */
    @Event_registrationDate_closedDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp date_closed;

    @JsonIgnore
    private boolean date_closedDirtyFlag;

    /**
     * 属性 [MESSAGE_IDS]
     *
     */
    @Event_registrationMessage_idsDefault(info = "默认规则")
    private String message_ids;

    @JsonIgnore
    private boolean message_idsDirtyFlag;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @Event_registrationDisplay_nameDefault(info = "默认规则")
    private String display_name;

    @JsonIgnore
    private boolean display_nameDirtyFlag;

    /**
     * 属性 [CREATE_DATE]
     *
     */
    @Event_registrationCreate_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp create_date;

    @JsonIgnore
    private boolean create_dateDirtyFlag;

    /**
     * 属性 [WEBSITE_MESSAGE_IDS]
     *
     */
    @Event_registrationWebsite_message_idsDefault(info = "默认规则")
    private String website_message_ids;

    @JsonIgnore
    private boolean website_message_idsDirtyFlag;

    /**
     * 属性 [EVENT_BEGIN_DATE]
     *
     */
    @Event_registrationEvent_begin_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp event_begin_date;

    @JsonIgnore
    private boolean event_begin_dateDirtyFlag;

    /**
     * 属性 [CREATE_UID_TEXT]
     *
     */
    @Event_registrationCreate_uid_textDefault(info = "默认规则")
    private String create_uid_text;

    @JsonIgnore
    private boolean create_uid_textDirtyFlag;

    /**
     * 属性 [SALE_ORDER_ID_TEXT]
     *
     */
    @Event_registrationSale_order_id_textDefault(info = "默认规则")
    private String sale_order_id_text;

    @JsonIgnore
    private boolean sale_order_id_textDirtyFlag;

    /**
     * 属性 [EVENT_END_DATE]
     *
     */
    @Event_registrationEvent_end_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp event_end_date;

    @JsonIgnore
    private boolean event_end_dateDirtyFlag;

    /**
     * 属性 [SALE_ORDER_LINE_ID_TEXT]
     *
     */
    @Event_registrationSale_order_line_id_textDefault(info = "默认规则")
    private String sale_order_line_id_text;

    @JsonIgnore
    private boolean sale_order_line_id_textDirtyFlag;

    /**
     * 属性 [WRITE_UID_TEXT]
     *
     */
    @Event_registrationWrite_uid_textDefault(info = "默认规则")
    private String write_uid_text;

    @JsonIgnore
    private boolean write_uid_textDirtyFlag;

    /**
     * 属性 [EVENT_TICKET_ID_TEXT]
     *
     */
    @Event_registrationEvent_ticket_id_textDefault(info = "默认规则")
    private String event_ticket_id_text;

    @JsonIgnore
    private boolean event_ticket_id_textDirtyFlag;

    /**
     * 属性 [COMPANY_ID_TEXT]
     *
     */
    @Event_registrationCompany_id_textDefault(info = "默认规则")
    private String company_id_text;

    @JsonIgnore
    private boolean company_id_textDirtyFlag;

    /**
     * 属性 [EVENT_ID_TEXT]
     *
     */
    @Event_registrationEvent_id_textDefault(info = "默认规则")
    private String event_id_text;

    @JsonIgnore
    private boolean event_id_textDirtyFlag;

    /**
     * 属性 [PARTNER_ID_TEXT]
     *
     */
    @Event_registrationPartner_id_textDefault(info = "默认规则")
    private String partner_id_text;

    @JsonIgnore
    private boolean partner_id_textDirtyFlag;

    /**
     * 属性 [EVENT_ID]
     *
     */
    @Event_registrationEvent_idDefault(info = "默认规则")
    private Integer event_id;

    @JsonIgnore
    private boolean event_idDirtyFlag;

    /**
     * 属性 [CREATE_UID]
     *
     */
    @Event_registrationCreate_uidDefault(info = "默认规则")
    private Integer create_uid;

    @JsonIgnore
    private boolean create_uidDirtyFlag;

    /**
     * 属性 [SALE_ORDER_LINE_ID]
     *
     */
    @Event_registrationSale_order_line_idDefault(info = "默认规则")
    private Integer sale_order_line_id;

    @JsonIgnore
    private boolean sale_order_line_idDirtyFlag;

    /**
     * 属性 [PARTNER_ID]
     *
     */
    @Event_registrationPartner_idDefault(info = "默认规则")
    private Integer partner_id;

    @JsonIgnore
    private boolean partner_idDirtyFlag;

    /**
     * 属性 [WRITE_UID]
     *
     */
    @Event_registrationWrite_uidDefault(info = "默认规则")
    private Integer write_uid;

    @JsonIgnore
    private boolean write_uidDirtyFlag;

    /**
     * 属性 [EVENT_TICKET_ID]
     *
     */
    @Event_registrationEvent_ticket_idDefault(info = "默认规则")
    private Integer event_ticket_id;

    @JsonIgnore
    private boolean event_ticket_idDirtyFlag;

    /**
     * 属性 [COMPANY_ID]
     *
     */
    @Event_registrationCompany_idDefault(info = "默认规则")
    private Integer company_id;

    @JsonIgnore
    private boolean company_idDirtyFlag;

    /**
     * 属性 [SALE_ORDER_ID]
     *
     */
    @Event_registrationSale_order_idDefault(info = "默认规则")
    private Integer sale_order_id;

    @JsonIgnore
    private boolean sale_order_idDirtyFlag;


    /**
     * 获取 [MESSAGE_MAIN_ATTACHMENT_ID]
     */
    @JsonProperty("message_main_attachment_id")
    public Integer getMessage_main_attachment_id(){
        return message_main_attachment_id ;
    }

    /**
     * 设置 [MESSAGE_MAIN_ATTACHMENT_ID]
     */
    @JsonProperty("message_main_attachment_id")
    public void setMessage_main_attachment_id(Integer  message_main_attachment_id){
        this.message_main_attachment_id = message_main_attachment_id ;
        this.message_main_attachment_idDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_MAIN_ATTACHMENT_ID]脏标记
     */
    @JsonIgnore
    public boolean getMessage_main_attachment_idDirtyFlag(){
        return message_main_attachment_idDirtyFlag ;
    }

    /**
     * 获取 [PHONE]
     */
    @JsonProperty("phone")
    public String getPhone(){
        return phone ;
    }

    /**
     * 设置 [PHONE]
     */
    @JsonProperty("phone")
    public void setPhone(String  phone){
        this.phone = phone ;
        this.phoneDirtyFlag = true ;
    }

    /**
     * 获取 [PHONE]脏标记
     */
    @JsonIgnore
    public boolean getPhoneDirtyFlag(){
        return phoneDirtyFlag ;
    }

    /**
     * 获取 [DATE_OPEN]
     */
    @JsonProperty("date_open")
    public Timestamp getDate_open(){
        return date_open ;
    }

    /**
     * 设置 [DATE_OPEN]
     */
    @JsonProperty("date_open")
    public void setDate_open(Timestamp  date_open){
        this.date_open = date_open ;
        this.date_openDirtyFlag = true ;
    }

    /**
     * 获取 [DATE_OPEN]脏标记
     */
    @JsonIgnore
    public boolean getDate_openDirtyFlag(){
        return date_openDirtyFlag ;
    }

    /**
     * 获取 [STATE]
     */
    @JsonProperty("state")
    public String getState(){
        return state ;
    }

    /**
     * 设置 [STATE]
     */
    @JsonProperty("state")
    public void setState(String  state){
        this.state = state ;
        this.stateDirtyFlag = true ;
    }

    /**
     * 获取 [STATE]脏标记
     */
    @JsonIgnore
    public boolean getStateDirtyFlag(){
        return stateDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_UNREAD]
     */
    @JsonProperty("message_unread")
    public String getMessage_unread(){
        return message_unread ;
    }

    /**
     * 设置 [MESSAGE_UNREAD]
     */
    @JsonProperty("message_unread")
    public void setMessage_unread(String  message_unread){
        this.message_unread = message_unread ;
        this.message_unreadDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_UNREAD]脏标记
     */
    @JsonIgnore
    public boolean getMessage_unreadDirtyFlag(){
        return message_unreadDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_FOLLOWER_IDS]
     */
    @JsonProperty("message_follower_ids")
    public String getMessage_follower_ids(){
        return message_follower_ids ;
    }

    /**
     * 设置 [MESSAGE_FOLLOWER_IDS]
     */
    @JsonProperty("message_follower_ids")
    public void setMessage_follower_ids(String  message_follower_ids){
        this.message_follower_ids = message_follower_ids ;
        this.message_follower_idsDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_FOLLOWER_IDS]脏标记
     */
    @JsonIgnore
    public boolean getMessage_follower_idsDirtyFlag(){
        return message_follower_idsDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_IS_FOLLOWER]
     */
    @JsonProperty("message_is_follower")
    public String getMessage_is_follower(){
        return message_is_follower ;
    }

    /**
     * 设置 [MESSAGE_IS_FOLLOWER]
     */
    @JsonProperty("message_is_follower")
    public void setMessage_is_follower(String  message_is_follower){
        this.message_is_follower = message_is_follower ;
        this.message_is_followerDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_IS_FOLLOWER]脏标记
     */
    @JsonIgnore
    public boolean getMessage_is_followerDirtyFlag(){
        return message_is_followerDirtyFlag ;
    }

    /**
     * 获取 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return write_date ;
    }

    /**
     * 设置 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return write_dateDirtyFlag ;
    }

    /**
     * 获取 [ORIGIN]
     */
    @JsonProperty("origin")
    public String getOrigin(){
        return origin ;
    }

    /**
     * 设置 [ORIGIN]
     */
    @JsonProperty("origin")
    public void setOrigin(String  origin){
        this.origin = origin ;
        this.originDirtyFlag = true ;
    }

    /**
     * 获取 [ORIGIN]脏标记
     */
    @JsonIgnore
    public boolean getOriginDirtyFlag(){
        return originDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_CHANNEL_IDS]
     */
    @JsonProperty("message_channel_ids")
    public String getMessage_channel_ids(){
        return message_channel_ids ;
    }

    /**
     * 设置 [MESSAGE_CHANNEL_IDS]
     */
    @JsonProperty("message_channel_ids")
    public void setMessage_channel_ids(String  message_channel_ids){
        this.message_channel_ids = message_channel_ids ;
        this.message_channel_idsDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_CHANNEL_IDS]脏标记
     */
    @JsonIgnore
    public boolean getMessage_channel_idsDirtyFlag(){
        return message_channel_idsDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_HAS_ERROR_COUNTER]
     */
    @JsonProperty("message_has_error_counter")
    public Integer getMessage_has_error_counter(){
        return message_has_error_counter ;
    }

    /**
     * 设置 [MESSAGE_HAS_ERROR_COUNTER]
     */
    @JsonProperty("message_has_error_counter")
    public void setMessage_has_error_counter(Integer  message_has_error_counter){
        this.message_has_error_counter = message_has_error_counter ;
        this.message_has_error_counterDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_HAS_ERROR_COUNTER]脏标记
     */
    @JsonIgnore
    public boolean getMessage_has_error_counterDirtyFlag(){
        return message_has_error_counterDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_PARTNER_IDS]
     */
    @JsonProperty("message_partner_ids")
    public String getMessage_partner_ids(){
        return message_partner_ids ;
    }

    /**
     * 设置 [MESSAGE_PARTNER_IDS]
     */
    @JsonProperty("message_partner_ids")
    public void setMessage_partner_ids(String  message_partner_ids){
        this.message_partner_ids = message_partner_ids ;
        this.message_partner_idsDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_PARTNER_IDS]脏标记
     */
    @JsonIgnore
    public boolean getMessage_partner_idsDirtyFlag(){
        return message_partner_idsDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_ATTACHMENT_COUNT]
     */
    @JsonProperty("message_attachment_count")
    public Integer getMessage_attachment_count(){
        return message_attachment_count ;
    }

    /**
     * 设置 [MESSAGE_ATTACHMENT_COUNT]
     */
    @JsonProperty("message_attachment_count")
    public void setMessage_attachment_count(Integer  message_attachment_count){
        this.message_attachment_count = message_attachment_count ;
        this.message_attachment_countDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_ATTACHMENT_COUNT]脏标记
     */
    @JsonIgnore
    public boolean getMessage_attachment_countDirtyFlag(){
        return message_attachment_countDirtyFlag ;
    }

    /**
     * 获取 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return __last_update ;
    }

    /**
     * 设置 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

    /**
     * 获取 [__LAST_UPDATE]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return __last_updateDirtyFlag ;
    }

    /**
     * 获取 [EMAIL]
     */
    @JsonProperty("email")
    public String getEmail(){
        return email ;
    }

    /**
     * 设置 [EMAIL]
     */
    @JsonProperty("email")
    public void setEmail(String  email){
        this.email = email ;
        this.emailDirtyFlag = true ;
    }

    /**
     * 获取 [EMAIL]脏标记
     */
    @JsonIgnore
    public boolean getEmailDirtyFlag(){
        return emailDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_HAS_ERROR]
     */
    @JsonProperty("message_has_error")
    public String getMessage_has_error(){
        return message_has_error ;
    }

    /**
     * 设置 [MESSAGE_HAS_ERROR]
     */
    @JsonProperty("message_has_error")
    public void setMessage_has_error(String  message_has_error){
        this.message_has_error = message_has_error ;
        this.message_has_errorDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_HAS_ERROR]脏标记
     */
    @JsonIgnore
    public boolean getMessage_has_errorDirtyFlag(){
        return message_has_errorDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_UNREAD_COUNTER]
     */
    @JsonProperty("message_unread_counter")
    public Integer getMessage_unread_counter(){
        return message_unread_counter ;
    }

    /**
     * 设置 [MESSAGE_UNREAD_COUNTER]
     */
    @JsonProperty("message_unread_counter")
    public void setMessage_unread_counter(Integer  message_unread_counter){
        this.message_unread_counter = message_unread_counter ;
        this.message_unread_counterDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_UNREAD_COUNTER]脏标记
     */
    @JsonIgnore
    public boolean getMessage_unread_counterDirtyFlag(){
        return message_unread_counterDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_NEEDACTION]
     */
    @JsonProperty("message_needaction")
    public String getMessage_needaction(){
        return message_needaction ;
    }

    /**
     * 设置 [MESSAGE_NEEDACTION]
     */
    @JsonProperty("message_needaction")
    public void setMessage_needaction(String  message_needaction){
        this.message_needaction = message_needaction ;
        this.message_needactionDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_NEEDACTION]脏标记
     */
    @JsonIgnore
    public boolean getMessage_needactionDirtyFlag(){
        return message_needactionDirtyFlag ;
    }

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

    /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return idDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_NEEDACTION_COUNTER]
     */
    @JsonProperty("message_needaction_counter")
    public Integer getMessage_needaction_counter(){
        return message_needaction_counter ;
    }

    /**
     * 设置 [MESSAGE_NEEDACTION_COUNTER]
     */
    @JsonProperty("message_needaction_counter")
    public void setMessage_needaction_counter(Integer  message_needaction_counter){
        this.message_needaction_counter = message_needaction_counter ;
        this.message_needaction_counterDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_NEEDACTION_COUNTER]脏标记
     */
    @JsonIgnore
    public boolean getMessage_needaction_counterDirtyFlag(){
        return message_needaction_counterDirtyFlag ;
    }

    /**
     * 获取 [NAME]
     */
    @JsonProperty("name")
    public String getName(){
        return name ;
    }

    /**
     * 设置 [NAME]
     */
    @JsonProperty("name")
    public void setName(String  name){
        this.name = name ;
        this.nameDirtyFlag = true ;
    }

    /**
     * 获取 [NAME]脏标记
     */
    @JsonIgnore
    public boolean getNameDirtyFlag(){
        return nameDirtyFlag ;
    }

    /**
     * 获取 [DATE_CLOSED]
     */
    @JsonProperty("date_closed")
    public Timestamp getDate_closed(){
        return date_closed ;
    }

    /**
     * 设置 [DATE_CLOSED]
     */
    @JsonProperty("date_closed")
    public void setDate_closed(Timestamp  date_closed){
        this.date_closed = date_closed ;
        this.date_closedDirtyFlag = true ;
    }

    /**
     * 获取 [DATE_CLOSED]脏标记
     */
    @JsonIgnore
    public boolean getDate_closedDirtyFlag(){
        return date_closedDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_IDS]
     */
    @JsonProperty("message_ids")
    public String getMessage_ids(){
        return message_ids ;
    }

    /**
     * 设置 [MESSAGE_IDS]
     */
    @JsonProperty("message_ids")
    public void setMessage_ids(String  message_ids){
        this.message_ids = message_ids ;
        this.message_idsDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_IDS]脏标记
     */
    @JsonIgnore
    public boolean getMessage_idsDirtyFlag(){
        return message_idsDirtyFlag ;
    }

    /**
     * 获取 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return display_name ;
    }

    /**
     * 设置 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

    /**
     * 获取 [DISPLAY_NAME]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return display_nameDirtyFlag ;
    }

    /**
     * 获取 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return create_date ;
    }

    /**
     * 设置 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return create_dateDirtyFlag ;
    }

    /**
     * 获取 [WEBSITE_MESSAGE_IDS]
     */
    @JsonProperty("website_message_ids")
    public String getWebsite_message_ids(){
        return website_message_ids ;
    }

    /**
     * 设置 [WEBSITE_MESSAGE_IDS]
     */
    @JsonProperty("website_message_ids")
    public void setWebsite_message_ids(String  website_message_ids){
        this.website_message_ids = website_message_ids ;
        this.website_message_idsDirtyFlag = true ;
    }

    /**
     * 获取 [WEBSITE_MESSAGE_IDS]脏标记
     */
    @JsonIgnore
    public boolean getWebsite_message_idsDirtyFlag(){
        return website_message_idsDirtyFlag ;
    }

    /**
     * 获取 [EVENT_BEGIN_DATE]
     */
    @JsonProperty("event_begin_date")
    public Timestamp getEvent_begin_date(){
        return event_begin_date ;
    }

    /**
     * 设置 [EVENT_BEGIN_DATE]
     */
    @JsonProperty("event_begin_date")
    public void setEvent_begin_date(Timestamp  event_begin_date){
        this.event_begin_date = event_begin_date ;
        this.event_begin_dateDirtyFlag = true ;
    }

    /**
     * 获取 [EVENT_BEGIN_DATE]脏标记
     */
    @JsonIgnore
    public boolean getEvent_begin_dateDirtyFlag(){
        return event_begin_dateDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return create_uid_text ;
    }

    /**
     * 设置 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return create_uid_textDirtyFlag ;
    }

    /**
     * 获取 [SALE_ORDER_ID_TEXT]
     */
    @JsonProperty("sale_order_id_text")
    public String getSale_order_id_text(){
        return sale_order_id_text ;
    }

    /**
     * 设置 [SALE_ORDER_ID_TEXT]
     */
    @JsonProperty("sale_order_id_text")
    public void setSale_order_id_text(String  sale_order_id_text){
        this.sale_order_id_text = sale_order_id_text ;
        this.sale_order_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [SALE_ORDER_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getSale_order_id_textDirtyFlag(){
        return sale_order_id_textDirtyFlag ;
    }

    /**
     * 获取 [EVENT_END_DATE]
     */
    @JsonProperty("event_end_date")
    public Timestamp getEvent_end_date(){
        return event_end_date ;
    }

    /**
     * 设置 [EVENT_END_DATE]
     */
    @JsonProperty("event_end_date")
    public void setEvent_end_date(Timestamp  event_end_date){
        this.event_end_date = event_end_date ;
        this.event_end_dateDirtyFlag = true ;
    }

    /**
     * 获取 [EVENT_END_DATE]脏标记
     */
    @JsonIgnore
    public boolean getEvent_end_dateDirtyFlag(){
        return event_end_dateDirtyFlag ;
    }

    /**
     * 获取 [SALE_ORDER_LINE_ID_TEXT]
     */
    @JsonProperty("sale_order_line_id_text")
    public String getSale_order_line_id_text(){
        return sale_order_line_id_text ;
    }

    /**
     * 设置 [SALE_ORDER_LINE_ID_TEXT]
     */
    @JsonProperty("sale_order_line_id_text")
    public void setSale_order_line_id_text(String  sale_order_line_id_text){
        this.sale_order_line_id_text = sale_order_line_id_text ;
        this.sale_order_line_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [SALE_ORDER_LINE_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getSale_order_line_id_textDirtyFlag(){
        return sale_order_line_id_textDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return write_uid_text ;
    }

    /**
     * 设置 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return write_uid_textDirtyFlag ;
    }

    /**
     * 获取 [EVENT_TICKET_ID_TEXT]
     */
    @JsonProperty("event_ticket_id_text")
    public String getEvent_ticket_id_text(){
        return event_ticket_id_text ;
    }

    /**
     * 设置 [EVENT_TICKET_ID_TEXT]
     */
    @JsonProperty("event_ticket_id_text")
    public void setEvent_ticket_id_text(String  event_ticket_id_text){
        this.event_ticket_id_text = event_ticket_id_text ;
        this.event_ticket_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [EVENT_TICKET_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getEvent_ticket_id_textDirtyFlag(){
        return event_ticket_id_textDirtyFlag ;
    }

    /**
     * 获取 [COMPANY_ID_TEXT]
     */
    @JsonProperty("company_id_text")
    public String getCompany_id_text(){
        return company_id_text ;
    }

    /**
     * 设置 [COMPANY_ID_TEXT]
     */
    @JsonProperty("company_id_text")
    public void setCompany_id_text(String  company_id_text){
        this.company_id_text = company_id_text ;
        this.company_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [COMPANY_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getCompany_id_textDirtyFlag(){
        return company_id_textDirtyFlag ;
    }

    /**
     * 获取 [EVENT_ID_TEXT]
     */
    @JsonProperty("event_id_text")
    public String getEvent_id_text(){
        return event_id_text ;
    }

    /**
     * 设置 [EVENT_ID_TEXT]
     */
    @JsonProperty("event_id_text")
    public void setEvent_id_text(String  event_id_text){
        this.event_id_text = event_id_text ;
        this.event_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [EVENT_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getEvent_id_textDirtyFlag(){
        return event_id_textDirtyFlag ;
    }

    /**
     * 获取 [PARTNER_ID_TEXT]
     */
    @JsonProperty("partner_id_text")
    public String getPartner_id_text(){
        return partner_id_text ;
    }

    /**
     * 设置 [PARTNER_ID_TEXT]
     */
    @JsonProperty("partner_id_text")
    public void setPartner_id_text(String  partner_id_text){
        this.partner_id_text = partner_id_text ;
        this.partner_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [PARTNER_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getPartner_id_textDirtyFlag(){
        return partner_id_textDirtyFlag ;
    }

    /**
     * 获取 [EVENT_ID]
     */
    @JsonProperty("event_id")
    public Integer getEvent_id(){
        return event_id ;
    }

    /**
     * 设置 [EVENT_ID]
     */
    @JsonProperty("event_id")
    public void setEvent_id(Integer  event_id){
        this.event_id = event_id ;
        this.event_idDirtyFlag = true ;
    }

    /**
     * 获取 [EVENT_ID]脏标记
     */
    @JsonIgnore
    public boolean getEvent_idDirtyFlag(){
        return event_idDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return create_uid ;
    }

    /**
     * 设置 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return create_uidDirtyFlag ;
    }

    /**
     * 获取 [SALE_ORDER_LINE_ID]
     */
    @JsonProperty("sale_order_line_id")
    public Integer getSale_order_line_id(){
        return sale_order_line_id ;
    }

    /**
     * 设置 [SALE_ORDER_LINE_ID]
     */
    @JsonProperty("sale_order_line_id")
    public void setSale_order_line_id(Integer  sale_order_line_id){
        this.sale_order_line_id = sale_order_line_id ;
        this.sale_order_line_idDirtyFlag = true ;
    }

    /**
     * 获取 [SALE_ORDER_LINE_ID]脏标记
     */
    @JsonIgnore
    public boolean getSale_order_line_idDirtyFlag(){
        return sale_order_line_idDirtyFlag ;
    }

    /**
     * 获取 [PARTNER_ID]
     */
    @JsonProperty("partner_id")
    public Integer getPartner_id(){
        return partner_id ;
    }

    /**
     * 设置 [PARTNER_ID]
     */
    @JsonProperty("partner_id")
    public void setPartner_id(Integer  partner_id){
        this.partner_id = partner_id ;
        this.partner_idDirtyFlag = true ;
    }

    /**
     * 获取 [PARTNER_ID]脏标记
     */
    @JsonIgnore
    public boolean getPartner_idDirtyFlag(){
        return partner_idDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return write_uid ;
    }

    /**
     * 设置 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return write_uidDirtyFlag ;
    }

    /**
     * 获取 [EVENT_TICKET_ID]
     */
    @JsonProperty("event_ticket_id")
    public Integer getEvent_ticket_id(){
        return event_ticket_id ;
    }

    /**
     * 设置 [EVENT_TICKET_ID]
     */
    @JsonProperty("event_ticket_id")
    public void setEvent_ticket_id(Integer  event_ticket_id){
        this.event_ticket_id = event_ticket_id ;
        this.event_ticket_idDirtyFlag = true ;
    }

    /**
     * 获取 [EVENT_TICKET_ID]脏标记
     */
    @JsonIgnore
    public boolean getEvent_ticket_idDirtyFlag(){
        return event_ticket_idDirtyFlag ;
    }

    /**
     * 获取 [COMPANY_ID]
     */
    @JsonProperty("company_id")
    public Integer getCompany_id(){
        return company_id ;
    }

    /**
     * 设置 [COMPANY_ID]
     */
    @JsonProperty("company_id")
    public void setCompany_id(Integer  company_id){
        this.company_id = company_id ;
        this.company_idDirtyFlag = true ;
    }

    /**
     * 获取 [COMPANY_ID]脏标记
     */
    @JsonIgnore
    public boolean getCompany_idDirtyFlag(){
        return company_idDirtyFlag ;
    }

    /**
     * 获取 [SALE_ORDER_ID]
     */
    @JsonProperty("sale_order_id")
    public Integer getSale_order_id(){
        return sale_order_id ;
    }

    /**
     * 设置 [SALE_ORDER_ID]
     */
    @JsonProperty("sale_order_id")
    public void setSale_order_id(Integer  sale_order_id){
        this.sale_order_id = sale_order_id ;
        this.sale_order_idDirtyFlag = true ;
    }

    /**
     * 获取 [SALE_ORDER_ID]脏标记
     */
    @JsonIgnore
    public boolean getSale_order_idDirtyFlag(){
        return sale_order_idDirtyFlag ;
    }



    public Event_registration toDO() {
        Event_registration srfdomain = new Event_registration();
        if(getMessage_main_attachment_idDirtyFlag())
            srfdomain.setMessage_main_attachment_id(message_main_attachment_id);
        if(getPhoneDirtyFlag())
            srfdomain.setPhone(phone);
        if(getDate_openDirtyFlag())
            srfdomain.setDate_open(date_open);
        if(getStateDirtyFlag())
            srfdomain.setState(state);
        if(getMessage_unreadDirtyFlag())
            srfdomain.setMessage_unread(message_unread);
        if(getMessage_follower_idsDirtyFlag())
            srfdomain.setMessage_follower_ids(message_follower_ids);
        if(getMessage_is_followerDirtyFlag())
            srfdomain.setMessage_is_follower(message_is_follower);
        if(getWrite_dateDirtyFlag())
            srfdomain.setWrite_date(write_date);
        if(getOriginDirtyFlag())
            srfdomain.setOrigin(origin);
        if(getMessage_channel_idsDirtyFlag())
            srfdomain.setMessage_channel_ids(message_channel_ids);
        if(getMessage_has_error_counterDirtyFlag())
            srfdomain.setMessage_has_error_counter(message_has_error_counter);
        if(getMessage_partner_idsDirtyFlag())
            srfdomain.setMessage_partner_ids(message_partner_ids);
        if(getMessage_attachment_countDirtyFlag())
            srfdomain.setMessage_attachment_count(message_attachment_count);
        if(get__last_updateDirtyFlag())
            srfdomain.set__last_update(__last_update);
        if(getEmailDirtyFlag())
            srfdomain.setEmail(email);
        if(getMessage_has_errorDirtyFlag())
            srfdomain.setMessage_has_error(message_has_error);
        if(getMessage_unread_counterDirtyFlag())
            srfdomain.setMessage_unread_counter(message_unread_counter);
        if(getMessage_needactionDirtyFlag())
            srfdomain.setMessage_needaction(message_needaction);
        if(getIdDirtyFlag())
            srfdomain.setId(id);
        if(getMessage_needaction_counterDirtyFlag())
            srfdomain.setMessage_needaction_counter(message_needaction_counter);
        if(getNameDirtyFlag())
            srfdomain.setName(name);
        if(getDate_closedDirtyFlag())
            srfdomain.setDate_closed(date_closed);
        if(getMessage_idsDirtyFlag())
            srfdomain.setMessage_ids(message_ids);
        if(getDisplay_nameDirtyFlag())
            srfdomain.setDisplay_name(display_name);
        if(getCreate_dateDirtyFlag())
            srfdomain.setCreate_date(create_date);
        if(getWebsite_message_idsDirtyFlag())
            srfdomain.setWebsite_message_ids(website_message_ids);
        if(getEvent_begin_dateDirtyFlag())
            srfdomain.setEvent_begin_date(event_begin_date);
        if(getCreate_uid_textDirtyFlag())
            srfdomain.setCreate_uid_text(create_uid_text);
        if(getSale_order_id_textDirtyFlag())
            srfdomain.setSale_order_id_text(sale_order_id_text);
        if(getEvent_end_dateDirtyFlag())
            srfdomain.setEvent_end_date(event_end_date);
        if(getSale_order_line_id_textDirtyFlag())
            srfdomain.setSale_order_line_id_text(sale_order_line_id_text);
        if(getWrite_uid_textDirtyFlag())
            srfdomain.setWrite_uid_text(write_uid_text);
        if(getEvent_ticket_id_textDirtyFlag())
            srfdomain.setEvent_ticket_id_text(event_ticket_id_text);
        if(getCompany_id_textDirtyFlag())
            srfdomain.setCompany_id_text(company_id_text);
        if(getEvent_id_textDirtyFlag())
            srfdomain.setEvent_id_text(event_id_text);
        if(getPartner_id_textDirtyFlag())
            srfdomain.setPartner_id_text(partner_id_text);
        if(getEvent_idDirtyFlag())
            srfdomain.setEvent_id(event_id);
        if(getCreate_uidDirtyFlag())
            srfdomain.setCreate_uid(create_uid);
        if(getSale_order_line_idDirtyFlag())
            srfdomain.setSale_order_line_id(sale_order_line_id);
        if(getPartner_idDirtyFlag())
            srfdomain.setPartner_id(partner_id);
        if(getWrite_uidDirtyFlag())
            srfdomain.setWrite_uid(write_uid);
        if(getEvent_ticket_idDirtyFlag())
            srfdomain.setEvent_ticket_id(event_ticket_id);
        if(getCompany_idDirtyFlag())
            srfdomain.setCompany_id(company_id);
        if(getSale_order_idDirtyFlag())
            srfdomain.setSale_order_id(sale_order_id);

        return srfdomain;
    }

    public void fromDO(Event_registration srfdomain) {
        if(srfdomain == null )
          return ;
        if(srfdomain.getMessage_main_attachment_idDirtyFlag())
            this.setMessage_main_attachment_id(srfdomain.getMessage_main_attachment_id());
        if(srfdomain.getPhoneDirtyFlag())
            this.setPhone(srfdomain.getPhone());
        if(srfdomain.getDate_openDirtyFlag())
            this.setDate_open(srfdomain.getDate_open());
        if(srfdomain.getStateDirtyFlag())
            this.setState(srfdomain.getState());
        if(srfdomain.getMessage_unreadDirtyFlag())
            this.setMessage_unread(srfdomain.getMessage_unread());
        if(srfdomain.getMessage_follower_idsDirtyFlag())
            this.setMessage_follower_ids(srfdomain.getMessage_follower_ids());
        if(srfdomain.getMessage_is_followerDirtyFlag())
            this.setMessage_is_follower(srfdomain.getMessage_is_follower());
        if(srfdomain.getWrite_dateDirtyFlag())
            this.setWrite_date(srfdomain.getWrite_date());
        if(srfdomain.getOriginDirtyFlag())
            this.setOrigin(srfdomain.getOrigin());
        if(srfdomain.getMessage_channel_idsDirtyFlag())
            this.setMessage_channel_ids(srfdomain.getMessage_channel_ids());
        if(srfdomain.getMessage_has_error_counterDirtyFlag())
            this.setMessage_has_error_counter(srfdomain.getMessage_has_error_counter());
        if(srfdomain.getMessage_partner_idsDirtyFlag())
            this.setMessage_partner_ids(srfdomain.getMessage_partner_ids());
        if(srfdomain.getMessage_attachment_countDirtyFlag())
            this.setMessage_attachment_count(srfdomain.getMessage_attachment_count());
        if(srfdomain.get__last_updateDirtyFlag())
            this.set__last_update(srfdomain.get__last_update());
        if(srfdomain.getEmailDirtyFlag())
            this.setEmail(srfdomain.getEmail());
        if(srfdomain.getMessage_has_errorDirtyFlag())
            this.setMessage_has_error(srfdomain.getMessage_has_error());
        if(srfdomain.getMessage_unread_counterDirtyFlag())
            this.setMessage_unread_counter(srfdomain.getMessage_unread_counter());
        if(srfdomain.getMessage_needactionDirtyFlag())
            this.setMessage_needaction(srfdomain.getMessage_needaction());
        if(srfdomain.getIdDirtyFlag())
            this.setId(srfdomain.getId());
        if(srfdomain.getMessage_needaction_counterDirtyFlag())
            this.setMessage_needaction_counter(srfdomain.getMessage_needaction_counter());
        if(srfdomain.getNameDirtyFlag())
            this.setName(srfdomain.getName());
        if(srfdomain.getDate_closedDirtyFlag())
            this.setDate_closed(srfdomain.getDate_closed());
        if(srfdomain.getMessage_idsDirtyFlag())
            this.setMessage_ids(srfdomain.getMessage_ids());
        if(srfdomain.getDisplay_nameDirtyFlag())
            this.setDisplay_name(srfdomain.getDisplay_name());
        if(srfdomain.getCreate_dateDirtyFlag())
            this.setCreate_date(srfdomain.getCreate_date());
        if(srfdomain.getWebsite_message_idsDirtyFlag())
            this.setWebsite_message_ids(srfdomain.getWebsite_message_ids());
        if(srfdomain.getEvent_begin_dateDirtyFlag())
            this.setEvent_begin_date(srfdomain.getEvent_begin_date());
        if(srfdomain.getCreate_uid_textDirtyFlag())
            this.setCreate_uid_text(srfdomain.getCreate_uid_text());
        if(srfdomain.getSale_order_id_textDirtyFlag())
            this.setSale_order_id_text(srfdomain.getSale_order_id_text());
        if(srfdomain.getEvent_end_dateDirtyFlag())
            this.setEvent_end_date(srfdomain.getEvent_end_date());
        if(srfdomain.getSale_order_line_id_textDirtyFlag())
            this.setSale_order_line_id_text(srfdomain.getSale_order_line_id_text());
        if(srfdomain.getWrite_uid_textDirtyFlag())
            this.setWrite_uid_text(srfdomain.getWrite_uid_text());
        if(srfdomain.getEvent_ticket_id_textDirtyFlag())
            this.setEvent_ticket_id_text(srfdomain.getEvent_ticket_id_text());
        if(srfdomain.getCompany_id_textDirtyFlag())
            this.setCompany_id_text(srfdomain.getCompany_id_text());
        if(srfdomain.getEvent_id_textDirtyFlag())
            this.setEvent_id_text(srfdomain.getEvent_id_text());
        if(srfdomain.getPartner_id_textDirtyFlag())
            this.setPartner_id_text(srfdomain.getPartner_id_text());
        if(srfdomain.getEvent_idDirtyFlag())
            this.setEvent_id(srfdomain.getEvent_id());
        if(srfdomain.getCreate_uidDirtyFlag())
            this.setCreate_uid(srfdomain.getCreate_uid());
        if(srfdomain.getSale_order_line_idDirtyFlag())
            this.setSale_order_line_id(srfdomain.getSale_order_line_id());
        if(srfdomain.getPartner_idDirtyFlag())
            this.setPartner_id(srfdomain.getPartner_id());
        if(srfdomain.getWrite_uidDirtyFlag())
            this.setWrite_uid(srfdomain.getWrite_uid());
        if(srfdomain.getEvent_ticket_idDirtyFlag())
            this.setEvent_ticket_id(srfdomain.getEvent_ticket_id());
        if(srfdomain.getCompany_idDirtyFlag())
            this.setCompany_id(srfdomain.getCompany_id());
        if(srfdomain.getSale_order_idDirtyFlag())
            this.setSale_order_id(srfdomain.getSale_order_id());

    }

    public List<Event_registrationDTO> fromDOPage(List<Event_registration> poPage)   {
        if(poPage == null)
            return null;
        List<Event_registrationDTO> dtos=new ArrayList<Event_registrationDTO>();
        for(Event_registration domain : poPage) {
            Event_registrationDTO dto = new Event_registrationDTO();
            dto.fromDO(domain);
            dtos.add(dto);
        }
        return dtos;
    }
}

