package cn.ibizlab.odoo.service.odoo_event.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_event.dto.Event_event_ticketDTO;
import cn.ibizlab.odoo.core.odoo_event.domain.Event_event_ticket;
import cn.ibizlab.odoo.core.odoo_event.service.IEvent_event_ticketService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_event.filter.Event_event_ticketSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Event_event_ticket" })
@RestController
@RequestMapping("")
public class Event_event_ticketResource {

    @Autowired
    private IEvent_event_ticketService event_event_ticketService;

    public IEvent_event_ticketService getEvent_event_ticketService() {
        return this.event_event_ticketService;
    }

    @ApiOperation(value = "批更新数据", tags = {"Event_event_ticket" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_event/event_event_tickets/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Event_event_ticketDTO> event_event_ticketdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "更新数据", tags = {"Event_event_ticket" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_event/event_event_tickets/{event_event_ticket_id}")

    public ResponseEntity<Event_event_ticketDTO> update(@PathVariable("event_event_ticket_id") Integer event_event_ticket_id, @RequestBody Event_event_ticketDTO event_event_ticketdto) {
		Event_event_ticket domain = event_event_ticketdto.toDO();
        domain.setId(event_event_ticket_id);
		event_event_ticketService.update(domain);
		Event_event_ticketDTO dto = new Event_event_ticketDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取数据", tags = {"Event_event_ticket" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_event/event_event_tickets/{event_event_ticket_id}")
    public ResponseEntity<Event_event_ticketDTO> get(@PathVariable("event_event_ticket_id") Integer event_event_ticket_id) {
        Event_event_ticketDTO dto = new Event_event_ticketDTO();
        Event_event_ticket domain = event_event_ticketService.get(event_event_ticket_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批删除数据", tags = {"Event_event_ticket" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_event/event_event_tickets/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Event_event_ticketDTO> event_event_ticketdtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "批建立数据", tags = {"Event_event_ticket" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_event/event_event_tickets/createBatch")
    public ResponseEntity<Boolean> createBatchEvent_event_ticket(@RequestBody List<Event_event_ticketDTO> event_event_ticketdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "删除数据", tags = {"Event_event_ticket" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_event/event_event_tickets/{event_event_ticket_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("event_event_ticket_id") Integer event_event_ticket_id) {
        Event_event_ticketDTO event_event_ticketdto = new Event_event_ticketDTO();
		Event_event_ticket domain = new Event_event_ticket();
		event_event_ticketdto.setId(event_event_ticket_id);
		domain.setId(event_event_ticket_id);
        Boolean rst = event_event_ticketService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "建立数据", tags = {"Event_event_ticket" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_event/event_event_tickets")

    public ResponseEntity<Event_event_ticketDTO> create(@RequestBody Event_event_ticketDTO event_event_ticketdto) {
        Event_event_ticketDTO dto = new Event_event_ticketDTO();
        Event_event_ticket domain = event_event_ticketdto.toDO();
		event_event_ticketService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Event_event_ticket" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_event/event_event_tickets/fetchdefault")
	public ResponseEntity<Page<Event_event_ticketDTO>> fetchDefault(Event_event_ticketSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Event_event_ticketDTO> list = new ArrayList<Event_event_ticketDTO>();
        
        Page<Event_event_ticket> domains = event_event_ticketService.searchDefault(context) ;
        for(Event_event_ticket event_event_ticket : domains.getContent()){
            Event_event_ticketDTO dto = new Event_event_ticketDTO();
            dto.fromDO(event_event_ticket);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
