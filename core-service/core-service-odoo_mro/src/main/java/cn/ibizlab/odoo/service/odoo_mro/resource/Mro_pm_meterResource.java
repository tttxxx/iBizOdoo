package cn.ibizlab.odoo.service.odoo_mro.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_mro.dto.Mro_pm_meterDTO;
import cn.ibizlab.odoo.core.odoo_mro.domain.Mro_pm_meter;
import cn.ibizlab.odoo.core.odoo_mro.service.IMro_pm_meterService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_mro.filter.Mro_pm_meterSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Mro_pm_meter" })
@RestController
@RequestMapping("")
public class Mro_pm_meterResource {

    @Autowired
    private IMro_pm_meterService mro_pm_meterService;

    public IMro_pm_meterService getMro_pm_meterService() {
        return this.mro_pm_meterService;
    }

    @ApiOperation(value = "批更新数据", tags = {"Mro_pm_meter" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_mro/mro_pm_meters/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Mro_pm_meterDTO> mro_pm_meterdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "更新数据", tags = {"Mro_pm_meter" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_mro/mro_pm_meters/{mro_pm_meter_id}")

    public ResponseEntity<Mro_pm_meterDTO> update(@PathVariable("mro_pm_meter_id") Integer mro_pm_meter_id, @RequestBody Mro_pm_meterDTO mro_pm_meterdto) {
		Mro_pm_meter domain = mro_pm_meterdto.toDO();
        domain.setId(mro_pm_meter_id);
		mro_pm_meterService.update(domain);
		Mro_pm_meterDTO dto = new Mro_pm_meterDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "删除数据", tags = {"Mro_pm_meter" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_mro/mro_pm_meters/{mro_pm_meter_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("mro_pm_meter_id") Integer mro_pm_meter_id) {
        Mro_pm_meterDTO mro_pm_meterdto = new Mro_pm_meterDTO();
		Mro_pm_meter domain = new Mro_pm_meter();
		mro_pm_meterdto.setId(mro_pm_meter_id);
		domain.setId(mro_pm_meter_id);
        Boolean rst = mro_pm_meterService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "获取数据", tags = {"Mro_pm_meter" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_mro/mro_pm_meters/{mro_pm_meter_id}")
    public ResponseEntity<Mro_pm_meterDTO> get(@PathVariable("mro_pm_meter_id") Integer mro_pm_meter_id) {
        Mro_pm_meterDTO dto = new Mro_pm_meterDTO();
        Mro_pm_meter domain = mro_pm_meterService.get(mro_pm_meter_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批建立数据", tags = {"Mro_pm_meter" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_mro/mro_pm_meters/createBatch")
    public ResponseEntity<Boolean> createBatchMro_pm_meter(@RequestBody List<Mro_pm_meterDTO> mro_pm_meterdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "建立数据", tags = {"Mro_pm_meter" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_mro/mro_pm_meters")

    public ResponseEntity<Mro_pm_meterDTO> create(@RequestBody Mro_pm_meterDTO mro_pm_meterdto) {
        Mro_pm_meterDTO dto = new Mro_pm_meterDTO();
        Mro_pm_meter domain = mro_pm_meterdto.toDO();
		mro_pm_meterService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批删除数据", tags = {"Mro_pm_meter" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_mro/mro_pm_meters/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Mro_pm_meterDTO> mro_pm_meterdtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Mro_pm_meter" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_mro/mro_pm_meters/fetchdefault")
	public ResponseEntity<Page<Mro_pm_meterDTO>> fetchDefault(Mro_pm_meterSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Mro_pm_meterDTO> list = new ArrayList<Mro_pm_meterDTO>();
        
        Page<Mro_pm_meter> domains = mro_pm_meterService.searchDefault(context) ;
        for(Mro_pm_meter mro_pm_meter : domains.getContent()){
            Mro_pm_meterDTO dto = new Mro_pm_meterDTO();
            dto.fromDO(mro_pm_meter);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
