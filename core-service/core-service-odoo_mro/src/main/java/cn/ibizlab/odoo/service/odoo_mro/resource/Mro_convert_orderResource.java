package cn.ibizlab.odoo.service.odoo_mro.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_mro.dto.Mro_convert_orderDTO;
import cn.ibizlab.odoo.core.odoo_mro.domain.Mro_convert_order;
import cn.ibizlab.odoo.core.odoo_mro.service.IMro_convert_orderService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_mro.filter.Mro_convert_orderSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Mro_convert_order" })
@RestController
@RequestMapping("")
public class Mro_convert_orderResource {

    @Autowired
    private IMro_convert_orderService mro_convert_orderService;

    public IMro_convert_orderService getMro_convert_orderService() {
        return this.mro_convert_orderService;
    }

    @ApiOperation(value = "批建立数据", tags = {"Mro_convert_order" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_mro/mro_convert_orders/createBatch")
    public ResponseEntity<Boolean> createBatchMro_convert_order(@RequestBody List<Mro_convert_orderDTO> mro_convert_orderdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "建立数据", tags = {"Mro_convert_order" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_mro/mro_convert_orders")

    public ResponseEntity<Mro_convert_orderDTO> create(@RequestBody Mro_convert_orderDTO mro_convert_orderdto) {
        Mro_convert_orderDTO dto = new Mro_convert_orderDTO();
        Mro_convert_order domain = mro_convert_orderdto.toDO();
		mro_convert_orderService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取数据", tags = {"Mro_convert_order" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_mro/mro_convert_orders/{mro_convert_order_id}")
    public ResponseEntity<Mro_convert_orderDTO> get(@PathVariable("mro_convert_order_id") Integer mro_convert_order_id) {
        Mro_convert_orderDTO dto = new Mro_convert_orderDTO();
        Mro_convert_order domain = mro_convert_orderService.get(mro_convert_order_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "更新数据", tags = {"Mro_convert_order" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_mro/mro_convert_orders/{mro_convert_order_id}")

    public ResponseEntity<Mro_convert_orderDTO> update(@PathVariable("mro_convert_order_id") Integer mro_convert_order_id, @RequestBody Mro_convert_orderDTO mro_convert_orderdto) {
		Mro_convert_order domain = mro_convert_orderdto.toDO();
        domain.setId(mro_convert_order_id);
		mro_convert_orderService.update(domain);
		Mro_convert_orderDTO dto = new Mro_convert_orderDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批更新数据", tags = {"Mro_convert_order" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_mro/mro_convert_orders/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Mro_convert_orderDTO> mro_convert_orderdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "批删除数据", tags = {"Mro_convert_order" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_mro/mro_convert_orders/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Mro_convert_orderDTO> mro_convert_orderdtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "删除数据", tags = {"Mro_convert_order" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_mro/mro_convert_orders/{mro_convert_order_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("mro_convert_order_id") Integer mro_convert_order_id) {
        Mro_convert_orderDTO mro_convert_orderdto = new Mro_convert_orderDTO();
		Mro_convert_order domain = new Mro_convert_order();
		mro_convert_orderdto.setId(mro_convert_order_id);
		domain.setId(mro_convert_order_id);
        Boolean rst = mro_convert_orderService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

	@ApiOperation(value = "获取默认查询", tags = {"Mro_convert_order" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_mro/mro_convert_orders/fetchdefault")
	public ResponseEntity<Page<Mro_convert_orderDTO>> fetchDefault(Mro_convert_orderSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Mro_convert_orderDTO> list = new ArrayList<Mro_convert_orderDTO>();
        
        Page<Mro_convert_order> domains = mro_convert_orderService.searchDefault(context) ;
        for(Mro_convert_order mro_convert_order : domains.getContent()){
            Mro_convert_orderDTO dto = new Mro_convert_orderDTO();
            dto.fromDO(mro_convert_order);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
