package cn.ibizlab.odoo.service.odoo_mro.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_mro.dto.Mro_workorderDTO;
import cn.ibizlab.odoo.core.odoo_mro.domain.Mro_workorder;
import cn.ibizlab.odoo.core.odoo_mro.service.IMro_workorderService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_mro.filter.Mro_workorderSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Mro_workorder" })
@RestController
@RequestMapping("")
public class Mro_workorderResource {

    @Autowired
    private IMro_workorderService mro_workorderService;

    public IMro_workorderService getMro_workorderService() {
        return this.mro_workorderService;
    }

    @ApiOperation(value = "删除数据", tags = {"Mro_workorder" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_mro/mro_workorders/{mro_workorder_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("mro_workorder_id") Integer mro_workorder_id) {
        Mro_workorderDTO mro_workorderdto = new Mro_workorderDTO();
		Mro_workorder domain = new Mro_workorder();
		mro_workorderdto.setId(mro_workorder_id);
		domain.setId(mro_workorder_id);
        Boolean rst = mro_workorderService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "批建立数据", tags = {"Mro_workorder" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_mro/mro_workorders/createBatch")
    public ResponseEntity<Boolean> createBatchMro_workorder(@RequestBody List<Mro_workorderDTO> mro_workorderdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "更新数据", tags = {"Mro_workorder" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_mro/mro_workorders/{mro_workorder_id}")

    public ResponseEntity<Mro_workorderDTO> update(@PathVariable("mro_workorder_id") Integer mro_workorder_id, @RequestBody Mro_workorderDTO mro_workorderdto) {
		Mro_workorder domain = mro_workorderdto.toDO();
        domain.setId(mro_workorder_id);
		mro_workorderService.update(domain);
		Mro_workorderDTO dto = new Mro_workorderDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批更新数据", tags = {"Mro_workorder" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_mro/mro_workorders/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Mro_workorderDTO> mro_workorderdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "批删除数据", tags = {"Mro_workorder" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_mro/mro_workorders/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Mro_workorderDTO> mro_workorderdtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "建立数据", tags = {"Mro_workorder" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_mro/mro_workorders")

    public ResponseEntity<Mro_workorderDTO> create(@RequestBody Mro_workorderDTO mro_workorderdto) {
        Mro_workorderDTO dto = new Mro_workorderDTO();
        Mro_workorder domain = mro_workorderdto.toDO();
		mro_workorderService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取数据", tags = {"Mro_workorder" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_mro/mro_workorders/{mro_workorder_id}")
    public ResponseEntity<Mro_workorderDTO> get(@PathVariable("mro_workorder_id") Integer mro_workorder_id) {
        Mro_workorderDTO dto = new Mro_workorderDTO();
        Mro_workorder domain = mro_workorderService.get(mro_workorder_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Mro_workorder" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_mro/mro_workorders/fetchdefault")
	public ResponseEntity<Page<Mro_workorderDTO>> fetchDefault(Mro_workorderSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Mro_workorderDTO> list = new ArrayList<Mro_workorderDTO>();
        
        Page<Mro_workorder> domains = mro_workorderService.searchDefault(context) ;
        for(Mro_workorder mro_workorder : domains.getContent()){
            Mro_workorderDTO dto = new Mro_workorderDTO();
            dto.fromDO(mro_workorder);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
