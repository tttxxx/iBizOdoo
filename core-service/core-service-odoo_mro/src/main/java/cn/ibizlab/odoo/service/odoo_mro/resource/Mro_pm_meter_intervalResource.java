package cn.ibizlab.odoo.service.odoo_mro.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_mro.dto.Mro_pm_meter_intervalDTO;
import cn.ibizlab.odoo.core.odoo_mro.domain.Mro_pm_meter_interval;
import cn.ibizlab.odoo.core.odoo_mro.service.IMro_pm_meter_intervalService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_mro.filter.Mro_pm_meter_intervalSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Mro_pm_meter_interval" })
@RestController
@RequestMapping("")
public class Mro_pm_meter_intervalResource {

    @Autowired
    private IMro_pm_meter_intervalService mro_pm_meter_intervalService;

    public IMro_pm_meter_intervalService getMro_pm_meter_intervalService() {
        return this.mro_pm_meter_intervalService;
    }

    @ApiOperation(value = "建立数据", tags = {"Mro_pm_meter_interval" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_mro/mro_pm_meter_intervals")

    public ResponseEntity<Mro_pm_meter_intervalDTO> create(@RequestBody Mro_pm_meter_intervalDTO mro_pm_meter_intervaldto) {
        Mro_pm_meter_intervalDTO dto = new Mro_pm_meter_intervalDTO();
        Mro_pm_meter_interval domain = mro_pm_meter_intervaldto.toDO();
		mro_pm_meter_intervalService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批建立数据", tags = {"Mro_pm_meter_interval" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_mro/mro_pm_meter_intervals/createBatch")
    public ResponseEntity<Boolean> createBatchMro_pm_meter_interval(@RequestBody List<Mro_pm_meter_intervalDTO> mro_pm_meter_intervaldtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "获取数据", tags = {"Mro_pm_meter_interval" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_mro/mro_pm_meter_intervals/{mro_pm_meter_interval_id}")
    public ResponseEntity<Mro_pm_meter_intervalDTO> get(@PathVariable("mro_pm_meter_interval_id") Integer mro_pm_meter_interval_id) {
        Mro_pm_meter_intervalDTO dto = new Mro_pm_meter_intervalDTO();
        Mro_pm_meter_interval domain = mro_pm_meter_intervalService.get(mro_pm_meter_interval_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "删除数据", tags = {"Mro_pm_meter_interval" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_mro/mro_pm_meter_intervals/{mro_pm_meter_interval_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("mro_pm_meter_interval_id") Integer mro_pm_meter_interval_id) {
        Mro_pm_meter_intervalDTO mro_pm_meter_intervaldto = new Mro_pm_meter_intervalDTO();
		Mro_pm_meter_interval domain = new Mro_pm_meter_interval();
		mro_pm_meter_intervaldto.setId(mro_pm_meter_interval_id);
		domain.setId(mro_pm_meter_interval_id);
        Boolean rst = mro_pm_meter_intervalService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "批更新数据", tags = {"Mro_pm_meter_interval" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_mro/mro_pm_meter_intervals/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Mro_pm_meter_intervalDTO> mro_pm_meter_intervaldtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "更新数据", tags = {"Mro_pm_meter_interval" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_mro/mro_pm_meter_intervals/{mro_pm_meter_interval_id}")

    public ResponseEntity<Mro_pm_meter_intervalDTO> update(@PathVariable("mro_pm_meter_interval_id") Integer mro_pm_meter_interval_id, @RequestBody Mro_pm_meter_intervalDTO mro_pm_meter_intervaldto) {
		Mro_pm_meter_interval domain = mro_pm_meter_intervaldto.toDO();
        domain.setId(mro_pm_meter_interval_id);
		mro_pm_meter_intervalService.update(domain);
		Mro_pm_meter_intervalDTO dto = new Mro_pm_meter_intervalDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批删除数据", tags = {"Mro_pm_meter_interval" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_mro/mro_pm_meter_intervals/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Mro_pm_meter_intervalDTO> mro_pm_meter_intervaldtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Mro_pm_meter_interval" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_mro/mro_pm_meter_intervals/fetchdefault")
	public ResponseEntity<Page<Mro_pm_meter_intervalDTO>> fetchDefault(Mro_pm_meter_intervalSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Mro_pm_meter_intervalDTO> list = new ArrayList<Mro_pm_meter_intervalDTO>();
        
        Page<Mro_pm_meter_interval> domains = mro_pm_meter_intervalService.searchDefault(context) ;
        for(Mro_pm_meter_interval mro_pm_meter_interval : domains.getContent()){
            Mro_pm_meter_intervalDTO dto = new Mro_pm_meter_intervalDTO();
            dto.fromDO(mro_pm_meter_interval);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
