package cn.ibizlab.odoo.service.odoo_mro.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_mro.dto.Mro_order_parts_lineDTO;
import cn.ibizlab.odoo.core.odoo_mro.domain.Mro_order_parts_line;
import cn.ibizlab.odoo.core.odoo_mro.service.IMro_order_parts_lineService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_mro.filter.Mro_order_parts_lineSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Mro_order_parts_line" })
@RestController
@RequestMapping("")
public class Mro_order_parts_lineResource {

    @Autowired
    private IMro_order_parts_lineService mro_order_parts_lineService;

    public IMro_order_parts_lineService getMro_order_parts_lineService() {
        return this.mro_order_parts_lineService;
    }

    @ApiOperation(value = "批删除数据", tags = {"Mro_order_parts_line" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_mro/mro_order_parts_lines/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Mro_order_parts_lineDTO> mro_order_parts_linedtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "删除数据", tags = {"Mro_order_parts_line" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_mro/mro_order_parts_lines/{mro_order_parts_line_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("mro_order_parts_line_id") Integer mro_order_parts_line_id) {
        Mro_order_parts_lineDTO mro_order_parts_linedto = new Mro_order_parts_lineDTO();
		Mro_order_parts_line domain = new Mro_order_parts_line();
		mro_order_parts_linedto.setId(mro_order_parts_line_id);
		domain.setId(mro_order_parts_line_id);
        Boolean rst = mro_order_parts_lineService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "获取数据", tags = {"Mro_order_parts_line" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_mro/mro_order_parts_lines/{mro_order_parts_line_id}")
    public ResponseEntity<Mro_order_parts_lineDTO> get(@PathVariable("mro_order_parts_line_id") Integer mro_order_parts_line_id) {
        Mro_order_parts_lineDTO dto = new Mro_order_parts_lineDTO();
        Mro_order_parts_line domain = mro_order_parts_lineService.get(mro_order_parts_line_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批更新数据", tags = {"Mro_order_parts_line" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_mro/mro_order_parts_lines/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Mro_order_parts_lineDTO> mro_order_parts_linedtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "更新数据", tags = {"Mro_order_parts_line" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_mro/mro_order_parts_lines/{mro_order_parts_line_id}")

    public ResponseEntity<Mro_order_parts_lineDTO> update(@PathVariable("mro_order_parts_line_id") Integer mro_order_parts_line_id, @RequestBody Mro_order_parts_lineDTO mro_order_parts_linedto) {
		Mro_order_parts_line domain = mro_order_parts_linedto.toDO();
        domain.setId(mro_order_parts_line_id);
		mro_order_parts_lineService.update(domain);
		Mro_order_parts_lineDTO dto = new Mro_order_parts_lineDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批建立数据", tags = {"Mro_order_parts_line" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_mro/mro_order_parts_lines/createBatch")
    public ResponseEntity<Boolean> createBatchMro_order_parts_line(@RequestBody List<Mro_order_parts_lineDTO> mro_order_parts_linedtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "建立数据", tags = {"Mro_order_parts_line" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_mro/mro_order_parts_lines")

    public ResponseEntity<Mro_order_parts_lineDTO> create(@RequestBody Mro_order_parts_lineDTO mro_order_parts_linedto) {
        Mro_order_parts_lineDTO dto = new Mro_order_parts_lineDTO();
        Mro_order_parts_line domain = mro_order_parts_linedto.toDO();
		mro_order_parts_lineService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Mro_order_parts_line" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_mro/mro_order_parts_lines/fetchdefault")
	public ResponseEntity<Page<Mro_order_parts_lineDTO>> fetchDefault(Mro_order_parts_lineSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Mro_order_parts_lineDTO> list = new ArrayList<Mro_order_parts_lineDTO>();
        
        Page<Mro_order_parts_line> domains = mro_order_parts_lineService.searchDefault(context) ;
        for(Mro_order_parts_line mro_order_parts_line : domains.getContent()){
            Mro_order_parts_lineDTO dto = new Mro_order_parts_lineDTO();
            dto.fromDO(mro_order_parts_line);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
