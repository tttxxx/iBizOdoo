package cn.ibizlab.odoo.service.odoo_mro.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.core.odoo_mro.valuerule.anno.mro_pm_rule_line.*;
import cn.ibizlab.odoo.core.odoo_mro.domain.Mro_pm_rule_line;
import org.springframework.cglib.beans.BeanCopier;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 服务DTO对象[Mro_pm_rule_lineDTO]
 */
public class Mro_pm_rule_lineDTO implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [CREATE_DATE]
     *
     */
    @Mro_pm_rule_lineCreate_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp create_date;

    @JsonIgnore
    private boolean create_dateDirtyFlag;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @Mro_pm_rule_lineDisplay_nameDefault(info = "默认规则")
    private String display_name;

    @JsonIgnore
    private boolean display_nameDirtyFlag;

    /**
     * 属性 [WRITE_DATE]
     *
     */
    @Mro_pm_rule_lineWrite_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp write_date;

    @JsonIgnore
    private boolean write_dateDirtyFlag;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @Mro_pm_rule_line__last_updateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp __last_update;

    @JsonIgnore
    private boolean __last_updateDirtyFlag;

    /**
     * 属性 [ID]
     *
     */
    @Mro_pm_rule_lineIdDefault(info = "默认规则")
    private Integer id;

    @JsonIgnore
    private boolean idDirtyFlag;

    /**
     * 属性 [PM_RULE_ID_TEXT]
     *
     */
    @Mro_pm_rule_linePm_rule_id_textDefault(info = "默认规则")
    private String pm_rule_id_text;

    @JsonIgnore
    private boolean pm_rule_id_textDirtyFlag;

    /**
     * 属性 [METER_INTERVAL_ID_TEXT]
     *
     */
    @Mro_pm_rule_lineMeter_interval_id_textDefault(info = "默认规则")
    private String meter_interval_id_text;

    @JsonIgnore
    private boolean meter_interval_id_textDirtyFlag;

    /**
     * 属性 [CREATE_UID_TEXT]
     *
     */
    @Mro_pm_rule_lineCreate_uid_textDefault(info = "默认规则")
    private String create_uid_text;

    @JsonIgnore
    private boolean create_uid_textDirtyFlag;

    /**
     * 属性 [TASK_ID_TEXT]
     *
     */
    @Mro_pm_rule_lineTask_id_textDefault(info = "默认规则")
    private String task_id_text;

    @JsonIgnore
    private boolean task_id_textDirtyFlag;

    /**
     * 属性 [WRITE_UID_TEXT]
     *
     */
    @Mro_pm_rule_lineWrite_uid_textDefault(info = "默认规则")
    private String write_uid_text;

    @JsonIgnore
    private boolean write_uid_textDirtyFlag;

    /**
     * 属性 [CREATE_UID]
     *
     */
    @Mro_pm_rule_lineCreate_uidDefault(info = "默认规则")
    private Integer create_uid;

    @JsonIgnore
    private boolean create_uidDirtyFlag;

    /**
     * 属性 [TASK_ID]
     *
     */
    @Mro_pm_rule_lineTask_idDefault(info = "默认规则")
    private Integer task_id;

    @JsonIgnore
    private boolean task_idDirtyFlag;

    /**
     * 属性 [METER_INTERVAL_ID]
     *
     */
    @Mro_pm_rule_lineMeter_interval_idDefault(info = "默认规则")
    private Integer meter_interval_id;

    @JsonIgnore
    private boolean meter_interval_idDirtyFlag;

    /**
     * 属性 [PM_RULE_ID]
     *
     */
    @Mro_pm_rule_linePm_rule_idDefault(info = "默认规则")
    private Integer pm_rule_id;

    @JsonIgnore
    private boolean pm_rule_idDirtyFlag;

    /**
     * 属性 [WRITE_UID]
     *
     */
    @Mro_pm_rule_lineWrite_uidDefault(info = "默认规则")
    private Integer write_uid;

    @JsonIgnore
    private boolean write_uidDirtyFlag;


    /**
     * 获取 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return create_date ;
    }

    /**
     * 设置 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return create_dateDirtyFlag ;
    }

    /**
     * 获取 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return display_name ;
    }

    /**
     * 设置 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

    /**
     * 获取 [DISPLAY_NAME]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return display_nameDirtyFlag ;
    }

    /**
     * 获取 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return write_date ;
    }

    /**
     * 设置 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return write_dateDirtyFlag ;
    }

    /**
     * 获取 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return __last_update ;
    }

    /**
     * 设置 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

    /**
     * 获取 [__LAST_UPDATE]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return __last_updateDirtyFlag ;
    }

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

    /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return idDirtyFlag ;
    }

    /**
     * 获取 [PM_RULE_ID_TEXT]
     */
    @JsonProperty("pm_rule_id_text")
    public String getPm_rule_id_text(){
        return pm_rule_id_text ;
    }

    /**
     * 设置 [PM_RULE_ID_TEXT]
     */
    @JsonProperty("pm_rule_id_text")
    public void setPm_rule_id_text(String  pm_rule_id_text){
        this.pm_rule_id_text = pm_rule_id_text ;
        this.pm_rule_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [PM_RULE_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getPm_rule_id_textDirtyFlag(){
        return pm_rule_id_textDirtyFlag ;
    }

    /**
     * 获取 [METER_INTERVAL_ID_TEXT]
     */
    @JsonProperty("meter_interval_id_text")
    public String getMeter_interval_id_text(){
        return meter_interval_id_text ;
    }

    /**
     * 设置 [METER_INTERVAL_ID_TEXT]
     */
    @JsonProperty("meter_interval_id_text")
    public void setMeter_interval_id_text(String  meter_interval_id_text){
        this.meter_interval_id_text = meter_interval_id_text ;
        this.meter_interval_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [METER_INTERVAL_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getMeter_interval_id_textDirtyFlag(){
        return meter_interval_id_textDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return create_uid_text ;
    }

    /**
     * 设置 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return create_uid_textDirtyFlag ;
    }

    /**
     * 获取 [TASK_ID_TEXT]
     */
    @JsonProperty("task_id_text")
    public String getTask_id_text(){
        return task_id_text ;
    }

    /**
     * 设置 [TASK_ID_TEXT]
     */
    @JsonProperty("task_id_text")
    public void setTask_id_text(String  task_id_text){
        this.task_id_text = task_id_text ;
        this.task_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [TASK_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getTask_id_textDirtyFlag(){
        return task_id_textDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return write_uid_text ;
    }

    /**
     * 设置 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return write_uid_textDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return create_uid ;
    }

    /**
     * 设置 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return create_uidDirtyFlag ;
    }

    /**
     * 获取 [TASK_ID]
     */
    @JsonProperty("task_id")
    public Integer getTask_id(){
        return task_id ;
    }

    /**
     * 设置 [TASK_ID]
     */
    @JsonProperty("task_id")
    public void setTask_id(Integer  task_id){
        this.task_id = task_id ;
        this.task_idDirtyFlag = true ;
    }

    /**
     * 获取 [TASK_ID]脏标记
     */
    @JsonIgnore
    public boolean getTask_idDirtyFlag(){
        return task_idDirtyFlag ;
    }

    /**
     * 获取 [METER_INTERVAL_ID]
     */
    @JsonProperty("meter_interval_id")
    public Integer getMeter_interval_id(){
        return meter_interval_id ;
    }

    /**
     * 设置 [METER_INTERVAL_ID]
     */
    @JsonProperty("meter_interval_id")
    public void setMeter_interval_id(Integer  meter_interval_id){
        this.meter_interval_id = meter_interval_id ;
        this.meter_interval_idDirtyFlag = true ;
    }

    /**
     * 获取 [METER_INTERVAL_ID]脏标记
     */
    @JsonIgnore
    public boolean getMeter_interval_idDirtyFlag(){
        return meter_interval_idDirtyFlag ;
    }

    /**
     * 获取 [PM_RULE_ID]
     */
    @JsonProperty("pm_rule_id")
    public Integer getPm_rule_id(){
        return pm_rule_id ;
    }

    /**
     * 设置 [PM_RULE_ID]
     */
    @JsonProperty("pm_rule_id")
    public void setPm_rule_id(Integer  pm_rule_id){
        this.pm_rule_id = pm_rule_id ;
        this.pm_rule_idDirtyFlag = true ;
    }

    /**
     * 获取 [PM_RULE_ID]脏标记
     */
    @JsonIgnore
    public boolean getPm_rule_idDirtyFlag(){
        return pm_rule_idDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return write_uid ;
    }

    /**
     * 设置 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return write_uidDirtyFlag ;
    }



    public Mro_pm_rule_line toDO() {
        Mro_pm_rule_line srfdomain = new Mro_pm_rule_line();
        if(getCreate_dateDirtyFlag())
            srfdomain.setCreate_date(create_date);
        if(getDisplay_nameDirtyFlag())
            srfdomain.setDisplay_name(display_name);
        if(getWrite_dateDirtyFlag())
            srfdomain.setWrite_date(write_date);
        if(get__last_updateDirtyFlag())
            srfdomain.set__last_update(__last_update);
        if(getIdDirtyFlag())
            srfdomain.setId(id);
        if(getPm_rule_id_textDirtyFlag())
            srfdomain.setPm_rule_id_text(pm_rule_id_text);
        if(getMeter_interval_id_textDirtyFlag())
            srfdomain.setMeter_interval_id_text(meter_interval_id_text);
        if(getCreate_uid_textDirtyFlag())
            srfdomain.setCreate_uid_text(create_uid_text);
        if(getTask_id_textDirtyFlag())
            srfdomain.setTask_id_text(task_id_text);
        if(getWrite_uid_textDirtyFlag())
            srfdomain.setWrite_uid_text(write_uid_text);
        if(getCreate_uidDirtyFlag())
            srfdomain.setCreate_uid(create_uid);
        if(getTask_idDirtyFlag())
            srfdomain.setTask_id(task_id);
        if(getMeter_interval_idDirtyFlag())
            srfdomain.setMeter_interval_id(meter_interval_id);
        if(getPm_rule_idDirtyFlag())
            srfdomain.setPm_rule_id(pm_rule_id);
        if(getWrite_uidDirtyFlag())
            srfdomain.setWrite_uid(write_uid);

        return srfdomain;
    }

    public void fromDO(Mro_pm_rule_line srfdomain) {
        if(srfdomain == null )
          return ;
        if(srfdomain.getCreate_dateDirtyFlag())
            this.setCreate_date(srfdomain.getCreate_date());
        if(srfdomain.getDisplay_nameDirtyFlag())
            this.setDisplay_name(srfdomain.getDisplay_name());
        if(srfdomain.getWrite_dateDirtyFlag())
            this.setWrite_date(srfdomain.getWrite_date());
        if(srfdomain.get__last_updateDirtyFlag())
            this.set__last_update(srfdomain.get__last_update());
        if(srfdomain.getIdDirtyFlag())
            this.setId(srfdomain.getId());
        if(srfdomain.getPm_rule_id_textDirtyFlag())
            this.setPm_rule_id_text(srfdomain.getPm_rule_id_text());
        if(srfdomain.getMeter_interval_id_textDirtyFlag())
            this.setMeter_interval_id_text(srfdomain.getMeter_interval_id_text());
        if(srfdomain.getCreate_uid_textDirtyFlag())
            this.setCreate_uid_text(srfdomain.getCreate_uid_text());
        if(srfdomain.getTask_id_textDirtyFlag())
            this.setTask_id_text(srfdomain.getTask_id_text());
        if(srfdomain.getWrite_uid_textDirtyFlag())
            this.setWrite_uid_text(srfdomain.getWrite_uid_text());
        if(srfdomain.getCreate_uidDirtyFlag())
            this.setCreate_uid(srfdomain.getCreate_uid());
        if(srfdomain.getTask_idDirtyFlag())
            this.setTask_id(srfdomain.getTask_id());
        if(srfdomain.getMeter_interval_idDirtyFlag())
            this.setMeter_interval_id(srfdomain.getMeter_interval_id());
        if(srfdomain.getPm_rule_idDirtyFlag())
            this.setPm_rule_id(srfdomain.getPm_rule_id());
        if(srfdomain.getWrite_uidDirtyFlag())
            this.setWrite_uid(srfdomain.getWrite_uid());

    }

    public List<Mro_pm_rule_lineDTO> fromDOPage(List<Mro_pm_rule_line> poPage)   {
        if(poPage == null)
            return null;
        List<Mro_pm_rule_lineDTO> dtos=new ArrayList<Mro_pm_rule_lineDTO>();
        for(Mro_pm_rule_line domain : poPage) {
            Mro_pm_rule_lineDTO dto = new Mro_pm_rule_lineDTO();
            dto.fromDO(domain);
            dtos.add(dto);
        }
        return dtos;
    }
}

