package cn.ibizlab.odoo.service.odoo_mro.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_mro.dto.Mro_pm_parameterDTO;
import cn.ibizlab.odoo.core.odoo_mro.domain.Mro_pm_parameter;
import cn.ibizlab.odoo.core.odoo_mro.service.IMro_pm_parameterService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_mro.filter.Mro_pm_parameterSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Mro_pm_parameter" })
@RestController
@RequestMapping("")
public class Mro_pm_parameterResource {

    @Autowired
    private IMro_pm_parameterService mro_pm_parameterService;

    public IMro_pm_parameterService getMro_pm_parameterService() {
        return this.mro_pm_parameterService;
    }

    @ApiOperation(value = "批建立数据", tags = {"Mro_pm_parameter" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_mro/mro_pm_parameters/createBatch")
    public ResponseEntity<Boolean> createBatchMro_pm_parameter(@RequestBody List<Mro_pm_parameterDTO> mro_pm_parameterdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "更新数据", tags = {"Mro_pm_parameter" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_mro/mro_pm_parameters/{mro_pm_parameter_id}")

    public ResponseEntity<Mro_pm_parameterDTO> update(@PathVariable("mro_pm_parameter_id") Integer mro_pm_parameter_id, @RequestBody Mro_pm_parameterDTO mro_pm_parameterdto) {
		Mro_pm_parameter domain = mro_pm_parameterdto.toDO();
        domain.setId(mro_pm_parameter_id);
		mro_pm_parameterService.update(domain);
		Mro_pm_parameterDTO dto = new Mro_pm_parameterDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批删除数据", tags = {"Mro_pm_parameter" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_mro/mro_pm_parameters/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Mro_pm_parameterDTO> mro_pm_parameterdtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "删除数据", tags = {"Mro_pm_parameter" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_mro/mro_pm_parameters/{mro_pm_parameter_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("mro_pm_parameter_id") Integer mro_pm_parameter_id) {
        Mro_pm_parameterDTO mro_pm_parameterdto = new Mro_pm_parameterDTO();
		Mro_pm_parameter domain = new Mro_pm_parameter();
		mro_pm_parameterdto.setId(mro_pm_parameter_id);
		domain.setId(mro_pm_parameter_id);
        Boolean rst = mro_pm_parameterService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "建立数据", tags = {"Mro_pm_parameter" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_mro/mro_pm_parameters")

    public ResponseEntity<Mro_pm_parameterDTO> create(@RequestBody Mro_pm_parameterDTO mro_pm_parameterdto) {
        Mro_pm_parameterDTO dto = new Mro_pm_parameterDTO();
        Mro_pm_parameter domain = mro_pm_parameterdto.toDO();
		mro_pm_parameterService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取数据", tags = {"Mro_pm_parameter" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_mro/mro_pm_parameters/{mro_pm_parameter_id}")
    public ResponseEntity<Mro_pm_parameterDTO> get(@PathVariable("mro_pm_parameter_id") Integer mro_pm_parameter_id) {
        Mro_pm_parameterDTO dto = new Mro_pm_parameterDTO();
        Mro_pm_parameter domain = mro_pm_parameterService.get(mro_pm_parameter_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批更新数据", tags = {"Mro_pm_parameter" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_mro/mro_pm_parameters/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Mro_pm_parameterDTO> mro_pm_parameterdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Mro_pm_parameter" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_mro/mro_pm_parameters/fetchdefault")
	public ResponseEntity<Page<Mro_pm_parameterDTO>> fetchDefault(Mro_pm_parameterSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Mro_pm_parameterDTO> list = new ArrayList<Mro_pm_parameterDTO>();
        
        Page<Mro_pm_parameter> domains = mro_pm_parameterService.searchDefault(context) ;
        for(Mro_pm_parameter mro_pm_parameter : domains.getContent()){
            Mro_pm_parameterDTO dto = new Mro_pm_parameterDTO();
            dto.fromDO(mro_pm_parameter);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
