package cn.ibizlab.odoo.service.odoo_mro.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.core.odoo_mro.valuerule.anno.mro_pm_meter.*;
import cn.ibizlab.odoo.core.odoo_mro.domain.Mro_pm_meter;
import org.springframework.cglib.beans.BeanCopier;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 服务DTO对象[Mro_pm_meterDTO]
 */
public class Mro_pm_meterDTO implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [ID]
     *
     */
    @Mro_pm_meterIdDefault(info = "默认规则")
    private Integer id;

    @JsonIgnore
    private boolean idDirtyFlag;

    /**
     * 属性 [MIN_UTILIZATION]
     *
     */
    @Mro_pm_meterMin_utilizationDefault(info = "默认规则")
    private Double min_utilization;

    @JsonIgnore
    private boolean min_utilizationDirtyFlag;

    /**
     * 属性 [DATE]
     *
     */
    @Mro_pm_meterDateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    private Timestamp date;

    @JsonIgnore
    private boolean dateDirtyFlag;

    /**
     * 属性 [READING_TYPE]
     *
     */
    @Mro_pm_meterReading_typeDefault(info = "默认规则")
    private String reading_type;

    @JsonIgnore
    private boolean reading_typeDirtyFlag;

    /**
     * 属性 [VALUE]
     *
     */
    @Mro_pm_meterValueDefault(info = "默认规则")
    private Double value;

    @JsonIgnore
    private boolean valueDirtyFlag;

    /**
     * 属性 [METER_LINE_IDS]
     *
     */
    @Mro_pm_meterMeter_line_idsDefault(info = "默认规则")
    private String meter_line_ids;

    @JsonIgnore
    private boolean meter_line_idsDirtyFlag;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @Mro_pm_meterDisplay_nameDefault(info = "默认规则")
    private String display_name;

    @JsonIgnore
    private boolean display_nameDirtyFlag;

    /**
     * 属性 [WRITE_DATE]
     *
     */
    @Mro_pm_meterWrite_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp write_date;

    @JsonIgnore
    private boolean write_dateDirtyFlag;

    /**
     * 属性 [VIEW_LINE_IDS]
     *
     */
    @Mro_pm_meterView_line_idsDefault(info = "默认规则")
    private String view_line_ids;

    @JsonIgnore
    private boolean view_line_idsDirtyFlag;

    /**
     * 属性 [STATE]
     *
     */
    @Mro_pm_meterStateDefault(info = "默认规则")
    private String state;

    @JsonIgnore
    private boolean stateDirtyFlag;

    /**
     * 属性 [CREATE_DATE]
     *
     */
    @Mro_pm_meterCreate_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp create_date;

    @JsonIgnore
    private boolean create_dateDirtyFlag;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @Mro_pm_meter__last_updateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp __last_update;

    @JsonIgnore
    private boolean __last_updateDirtyFlag;

    /**
     * 属性 [UTILIZATION]
     *
     */
    @Mro_pm_meterUtilizationDefault(info = "默认规则")
    private Double utilization;

    @JsonIgnore
    private boolean utilizationDirtyFlag;

    /**
     * 属性 [NEW_VALUE]
     *
     */
    @Mro_pm_meterNew_valueDefault(info = "默认规则")
    private Double new_value;

    @JsonIgnore
    private boolean new_valueDirtyFlag;

    /**
     * 属性 [AV_TIME]
     *
     */
    @Mro_pm_meterAv_timeDefault(info = "默认规则")
    private Double av_time;

    @JsonIgnore
    private boolean av_timeDirtyFlag;

    /**
     * 属性 [TOTAL_VALUE]
     *
     */
    @Mro_pm_meterTotal_valueDefault(info = "默认规则")
    private Double total_value;

    @JsonIgnore
    private boolean total_valueDirtyFlag;

    /**
     * 属性 [NAME_TEXT]
     *
     */
    @Mro_pm_meterName_textDefault(info = "默认规则")
    private String name_text;

    @JsonIgnore
    private boolean name_textDirtyFlag;

    /**
     * 属性 [CREATE_UID_TEXT]
     *
     */
    @Mro_pm_meterCreate_uid_textDefault(info = "默认规则")
    private String create_uid_text;

    @JsonIgnore
    private boolean create_uid_textDirtyFlag;

    /**
     * 属性 [METER_UOM]
     *
     */
    @Mro_pm_meterMeter_uomDefault(info = "默认规则")
    private Integer meter_uom;

    @JsonIgnore
    private boolean meter_uomDirtyFlag;

    /**
     * 属性 [WRITE_UID_TEXT]
     *
     */
    @Mro_pm_meterWrite_uid_textDefault(info = "默认规则")
    private String write_uid_text;

    @JsonIgnore
    private boolean write_uid_textDirtyFlag;

    /**
     * 属性 [ASSET_ID_TEXT]
     *
     */
    @Mro_pm_meterAsset_id_textDefault(info = "默认规则")
    private String asset_id_text;

    @JsonIgnore
    private boolean asset_id_textDirtyFlag;

    /**
     * 属性 [PARENT_RATIO_ID_TEXT]
     *
     */
    @Mro_pm_meterParent_ratio_id_textDefault(info = "默认规则")
    private String parent_ratio_id_text;

    @JsonIgnore
    private boolean parent_ratio_id_textDirtyFlag;

    /**
     * 属性 [PARENT_METER_ID]
     *
     */
    @Mro_pm_meterParent_meter_idDefault(info = "默认规则")
    private Integer parent_meter_id;

    @JsonIgnore
    private boolean parent_meter_idDirtyFlag;

    /**
     * 属性 [PARENT_RATIO_ID]
     *
     */
    @Mro_pm_meterParent_ratio_idDefault(info = "默认规则")
    private Integer parent_ratio_id;

    @JsonIgnore
    private boolean parent_ratio_idDirtyFlag;

    /**
     * 属性 [NAME]
     *
     */
    @Mro_pm_meterNameDefault(info = "默认规则")
    private Integer name;

    @JsonIgnore
    private boolean nameDirtyFlag;

    /**
     * 属性 [WRITE_UID]
     *
     */
    @Mro_pm_meterWrite_uidDefault(info = "默认规则")
    private Integer write_uid;

    @JsonIgnore
    private boolean write_uidDirtyFlag;

    /**
     * 属性 [CREATE_UID]
     *
     */
    @Mro_pm_meterCreate_uidDefault(info = "默认规则")
    private Integer create_uid;

    @JsonIgnore
    private boolean create_uidDirtyFlag;

    /**
     * 属性 [ASSET_ID]
     *
     */
    @Mro_pm_meterAsset_idDefault(info = "默认规则")
    private Integer asset_id;

    @JsonIgnore
    private boolean asset_idDirtyFlag;


    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

    /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return idDirtyFlag ;
    }

    /**
     * 获取 [MIN_UTILIZATION]
     */
    @JsonProperty("min_utilization")
    public Double getMin_utilization(){
        return min_utilization ;
    }

    /**
     * 设置 [MIN_UTILIZATION]
     */
    @JsonProperty("min_utilization")
    public void setMin_utilization(Double  min_utilization){
        this.min_utilization = min_utilization ;
        this.min_utilizationDirtyFlag = true ;
    }

    /**
     * 获取 [MIN_UTILIZATION]脏标记
     */
    @JsonIgnore
    public boolean getMin_utilizationDirtyFlag(){
        return min_utilizationDirtyFlag ;
    }

    /**
     * 获取 [DATE]
     */
    @JsonProperty("date")
    public Timestamp getDate(){
        return date ;
    }

    /**
     * 设置 [DATE]
     */
    @JsonProperty("date")
    public void setDate(Timestamp  date){
        this.date = date ;
        this.dateDirtyFlag = true ;
    }

    /**
     * 获取 [DATE]脏标记
     */
    @JsonIgnore
    public boolean getDateDirtyFlag(){
        return dateDirtyFlag ;
    }

    /**
     * 获取 [READING_TYPE]
     */
    @JsonProperty("reading_type")
    public String getReading_type(){
        return reading_type ;
    }

    /**
     * 设置 [READING_TYPE]
     */
    @JsonProperty("reading_type")
    public void setReading_type(String  reading_type){
        this.reading_type = reading_type ;
        this.reading_typeDirtyFlag = true ;
    }

    /**
     * 获取 [READING_TYPE]脏标记
     */
    @JsonIgnore
    public boolean getReading_typeDirtyFlag(){
        return reading_typeDirtyFlag ;
    }

    /**
     * 获取 [VALUE]
     */
    @JsonProperty("value")
    public Double getValue(){
        return value ;
    }

    /**
     * 设置 [VALUE]
     */
    @JsonProperty("value")
    public void setValue(Double  value){
        this.value = value ;
        this.valueDirtyFlag = true ;
    }

    /**
     * 获取 [VALUE]脏标记
     */
    @JsonIgnore
    public boolean getValueDirtyFlag(){
        return valueDirtyFlag ;
    }

    /**
     * 获取 [METER_LINE_IDS]
     */
    @JsonProperty("meter_line_ids")
    public String getMeter_line_ids(){
        return meter_line_ids ;
    }

    /**
     * 设置 [METER_LINE_IDS]
     */
    @JsonProperty("meter_line_ids")
    public void setMeter_line_ids(String  meter_line_ids){
        this.meter_line_ids = meter_line_ids ;
        this.meter_line_idsDirtyFlag = true ;
    }

    /**
     * 获取 [METER_LINE_IDS]脏标记
     */
    @JsonIgnore
    public boolean getMeter_line_idsDirtyFlag(){
        return meter_line_idsDirtyFlag ;
    }

    /**
     * 获取 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return display_name ;
    }

    /**
     * 设置 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

    /**
     * 获取 [DISPLAY_NAME]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return display_nameDirtyFlag ;
    }

    /**
     * 获取 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return write_date ;
    }

    /**
     * 设置 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return write_dateDirtyFlag ;
    }

    /**
     * 获取 [VIEW_LINE_IDS]
     */
    @JsonProperty("view_line_ids")
    public String getView_line_ids(){
        return view_line_ids ;
    }

    /**
     * 设置 [VIEW_LINE_IDS]
     */
    @JsonProperty("view_line_ids")
    public void setView_line_ids(String  view_line_ids){
        this.view_line_ids = view_line_ids ;
        this.view_line_idsDirtyFlag = true ;
    }

    /**
     * 获取 [VIEW_LINE_IDS]脏标记
     */
    @JsonIgnore
    public boolean getView_line_idsDirtyFlag(){
        return view_line_idsDirtyFlag ;
    }

    /**
     * 获取 [STATE]
     */
    @JsonProperty("state")
    public String getState(){
        return state ;
    }

    /**
     * 设置 [STATE]
     */
    @JsonProperty("state")
    public void setState(String  state){
        this.state = state ;
        this.stateDirtyFlag = true ;
    }

    /**
     * 获取 [STATE]脏标记
     */
    @JsonIgnore
    public boolean getStateDirtyFlag(){
        return stateDirtyFlag ;
    }

    /**
     * 获取 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return create_date ;
    }

    /**
     * 设置 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return create_dateDirtyFlag ;
    }

    /**
     * 获取 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return __last_update ;
    }

    /**
     * 设置 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

    /**
     * 获取 [__LAST_UPDATE]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return __last_updateDirtyFlag ;
    }

    /**
     * 获取 [UTILIZATION]
     */
    @JsonProperty("utilization")
    public Double getUtilization(){
        return utilization ;
    }

    /**
     * 设置 [UTILIZATION]
     */
    @JsonProperty("utilization")
    public void setUtilization(Double  utilization){
        this.utilization = utilization ;
        this.utilizationDirtyFlag = true ;
    }

    /**
     * 获取 [UTILIZATION]脏标记
     */
    @JsonIgnore
    public boolean getUtilizationDirtyFlag(){
        return utilizationDirtyFlag ;
    }

    /**
     * 获取 [NEW_VALUE]
     */
    @JsonProperty("new_value")
    public Double getNew_value(){
        return new_value ;
    }

    /**
     * 设置 [NEW_VALUE]
     */
    @JsonProperty("new_value")
    public void setNew_value(Double  new_value){
        this.new_value = new_value ;
        this.new_valueDirtyFlag = true ;
    }

    /**
     * 获取 [NEW_VALUE]脏标记
     */
    @JsonIgnore
    public boolean getNew_valueDirtyFlag(){
        return new_valueDirtyFlag ;
    }

    /**
     * 获取 [AV_TIME]
     */
    @JsonProperty("av_time")
    public Double getAv_time(){
        return av_time ;
    }

    /**
     * 设置 [AV_TIME]
     */
    @JsonProperty("av_time")
    public void setAv_time(Double  av_time){
        this.av_time = av_time ;
        this.av_timeDirtyFlag = true ;
    }

    /**
     * 获取 [AV_TIME]脏标记
     */
    @JsonIgnore
    public boolean getAv_timeDirtyFlag(){
        return av_timeDirtyFlag ;
    }

    /**
     * 获取 [TOTAL_VALUE]
     */
    @JsonProperty("total_value")
    public Double getTotal_value(){
        return total_value ;
    }

    /**
     * 设置 [TOTAL_VALUE]
     */
    @JsonProperty("total_value")
    public void setTotal_value(Double  total_value){
        this.total_value = total_value ;
        this.total_valueDirtyFlag = true ;
    }

    /**
     * 获取 [TOTAL_VALUE]脏标记
     */
    @JsonIgnore
    public boolean getTotal_valueDirtyFlag(){
        return total_valueDirtyFlag ;
    }

    /**
     * 获取 [NAME_TEXT]
     */
    @JsonProperty("name_text")
    public String getName_text(){
        return name_text ;
    }

    /**
     * 设置 [NAME_TEXT]
     */
    @JsonProperty("name_text")
    public void setName_text(String  name_text){
        this.name_text = name_text ;
        this.name_textDirtyFlag = true ;
    }

    /**
     * 获取 [NAME_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getName_textDirtyFlag(){
        return name_textDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return create_uid_text ;
    }

    /**
     * 设置 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return create_uid_textDirtyFlag ;
    }

    /**
     * 获取 [METER_UOM]
     */
    @JsonProperty("meter_uom")
    public Integer getMeter_uom(){
        return meter_uom ;
    }

    /**
     * 设置 [METER_UOM]
     */
    @JsonProperty("meter_uom")
    public void setMeter_uom(Integer  meter_uom){
        this.meter_uom = meter_uom ;
        this.meter_uomDirtyFlag = true ;
    }

    /**
     * 获取 [METER_UOM]脏标记
     */
    @JsonIgnore
    public boolean getMeter_uomDirtyFlag(){
        return meter_uomDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return write_uid_text ;
    }

    /**
     * 设置 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return write_uid_textDirtyFlag ;
    }

    /**
     * 获取 [ASSET_ID_TEXT]
     */
    @JsonProperty("asset_id_text")
    public String getAsset_id_text(){
        return asset_id_text ;
    }

    /**
     * 设置 [ASSET_ID_TEXT]
     */
    @JsonProperty("asset_id_text")
    public void setAsset_id_text(String  asset_id_text){
        this.asset_id_text = asset_id_text ;
        this.asset_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [ASSET_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getAsset_id_textDirtyFlag(){
        return asset_id_textDirtyFlag ;
    }

    /**
     * 获取 [PARENT_RATIO_ID_TEXT]
     */
    @JsonProperty("parent_ratio_id_text")
    public String getParent_ratio_id_text(){
        return parent_ratio_id_text ;
    }

    /**
     * 设置 [PARENT_RATIO_ID_TEXT]
     */
    @JsonProperty("parent_ratio_id_text")
    public void setParent_ratio_id_text(String  parent_ratio_id_text){
        this.parent_ratio_id_text = parent_ratio_id_text ;
        this.parent_ratio_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [PARENT_RATIO_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getParent_ratio_id_textDirtyFlag(){
        return parent_ratio_id_textDirtyFlag ;
    }

    /**
     * 获取 [PARENT_METER_ID]
     */
    @JsonProperty("parent_meter_id")
    public Integer getParent_meter_id(){
        return parent_meter_id ;
    }

    /**
     * 设置 [PARENT_METER_ID]
     */
    @JsonProperty("parent_meter_id")
    public void setParent_meter_id(Integer  parent_meter_id){
        this.parent_meter_id = parent_meter_id ;
        this.parent_meter_idDirtyFlag = true ;
    }

    /**
     * 获取 [PARENT_METER_ID]脏标记
     */
    @JsonIgnore
    public boolean getParent_meter_idDirtyFlag(){
        return parent_meter_idDirtyFlag ;
    }

    /**
     * 获取 [PARENT_RATIO_ID]
     */
    @JsonProperty("parent_ratio_id")
    public Integer getParent_ratio_id(){
        return parent_ratio_id ;
    }

    /**
     * 设置 [PARENT_RATIO_ID]
     */
    @JsonProperty("parent_ratio_id")
    public void setParent_ratio_id(Integer  parent_ratio_id){
        this.parent_ratio_id = parent_ratio_id ;
        this.parent_ratio_idDirtyFlag = true ;
    }

    /**
     * 获取 [PARENT_RATIO_ID]脏标记
     */
    @JsonIgnore
    public boolean getParent_ratio_idDirtyFlag(){
        return parent_ratio_idDirtyFlag ;
    }

    /**
     * 获取 [NAME]
     */
    @JsonProperty("name")
    public Integer getName(){
        return name ;
    }

    /**
     * 设置 [NAME]
     */
    @JsonProperty("name")
    public void setName(Integer  name){
        this.name = name ;
        this.nameDirtyFlag = true ;
    }

    /**
     * 获取 [NAME]脏标记
     */
    @JsonIgnore
    public boolean getNameDirtyFlag(){
        return nameDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return write_uid ;
    }

    /**
     * 设置 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return write_uidDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return create_uid ;
    }

    /**
     * 设置 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return create_uidDirtyFlag ;
    }

    /**
     * 获取 [ASSET_ID]
     */
    @JsonProperty("asset_id")
    public Integer getAsset_id(){
        return asset_id ;
    }

    /**
     * 设置 [ASSET_ID]
     */
    @JsonProperty("asset_id")
    public void setAsset_id(Integer  asset_id){
        this.asset_id = asset_id ;
        this.asset_idDirtyFlag = true ;
    }

    /**
     * 获取 [ASSET_ID]脏标记
     */
    @JsonIgnore
    public boolean getAsset_idDirtyFlag(){
        return asset_idDirtyFlag ;
    }



    public Mro_pm_meter toDO() {
        Mro_pm_meter srfdomain = new Mro_pm_meter();
        if(getIdDirtyFlag())
            srfdomain.setId(id);
        if(getMin_utilizationDirtyFlag())
            srfdomain.setMin_utilization(min_utilization);
        if(getDateDirtyFlag())
            srfdomain.setDate(date);
        if(getReading_typeDirtyFlag())
            srfdomain.setReading_type(reading_type);
        if(getValueDirtyFlag())
            srfdomain.setValue(value);
        if(getMeter_line_idsDirtyFlag())
            srfdomain.setMeter_line_ids(meter_line_ids);
        if(getDisplay_nameDirtyFlag())
            srfdomain.setDisplay_name(display_name);
        if(getWrite_dateDirtyFlag())
            srfdomain.setWrite_date(write_date);
        if(getView_line_idsDirtyFlag())
            srfdomain.setView_line_ids(view_line_ids);
        if(getStateDirtyFlag())
            srfdomain.setState(state);
        if(getCreate_dateDirtyFlag())
            srfdomain.setCreate_date(create_date);
        if(get__last_updateDirtyFlag())
            srfdomain.set__last_update(__last_update);
        if(getUtilizationDirtyFlag())
            srfdomain.setUtilization(utilization);
        if(getNew_valueDirtyFlag())
            srfdomain.setNew_value(new_value);
        if(getAv_timeDirtyFlag())
            srfdomain.setAv_time(av_time);
        if(getTotal_valueDirtyFlag())
            srfdomain.setTotal_value(total_value);
        if(getName_textDirtyFlag())
            srfdomain.setName_text(name_text);
        if(getCreate_uid_textDirtyFlag())
            srfdomain.setCreate_uid_text(create_uid_text);
        if(getMeter_uomDirtyFlag())
            srfdomain.setMeter_uom(meter_uom);
        if(getWrite_uid_textDirtyFlag())
            srfdomain.setWrite_uid_text(write_uid_text);
        if(getAsset_id_textDirtyFlag())
            srfdomain.setAsset_id_text(asset_id_text);
        if(getParent_ratio_id_textDirtyFlag())
            srfdomain.setParent_ratio_id_text(parent_ratio_id_text);
        if(getParent_meter_idDirtyFlag())
            srfdomain.setParent_meter_id(parent_meter_id);
        if(getParent_ratio_idDirtyFlag())
            srfdomain.setParent_ratio_id(parent_ratio_id);
        if(getNameDirtyFlag())
            srfdomain.setName(name);
        if(getWrite_uidDirtyFlag())
            srfdomain.setWrite_uid(write_uid);
        if(getCreate_uidDirtyFlag())
            srfdomain.setCreate_uid(create_uid);
        if(getAsset_idDirtyFlag())
            srfdomain.setAsset_id(asset_id);

        return srfdomain;
    }

    public void fromDO(Mro_pm_meter srfdomain) {
        if(srfdomain == null )
          return ;
        if(srfdomain.getIdDirtyFlag())
            this.setId(srfdomain.getId());
        if(srfdomain.getMin_utilizationDirtyFlag())
            this.setMin_utilization(srfdomain.getMin_utilization());
        if(srfdomain.getDateDirtyFlag())
            this.setDate(srfdomain.getDate());
        if(srfdomain.getReading_typeDirtyFlag())
            this.setReading_type(srfdomain.getReading_type());
        if(srfdomain.getValueDirtyFlag())
            this.setValue(srfdomain.getValue());
        if(srfdomain.getMeter_line_idsDirtyFlag())
            this.setMeter_line_ids(srfdomain.getMeter_line_ids());
        if(srfdomain.getDisplay_nameDirtyFlag())
            this.setDisplay_name(srfdomain.getDisplay_name());
        if(srfdomain.getWrite_dateDirtyFlag())
            this.setWrite_date(srfdomain.getWrite_date());
        if(srfdomain.getView_line_idsDirtyFlag())
            this.setView_line_ids(srfdomain.getView_line_ids());
        if(srfdomain.getStateDirtyFlag())
            this.setState(srfdomain.getState());
        if(srfdomain.getCreate_dateDirtyFlag())
            this.setCreate_date(srfdomain.getCreate_date());
        if(srfdomain.get__last_updateDirtyFlag())
            this.set__last_update(srfdomain.get__last_update());
        if(srfdomain.getUtilizationDirtyFlag())
            this.setUtilization(srfdomain.getUtilization());
        if(srfdomain.getNew_valueDirtyFlag())
            this.setNew_value(srfdomain.getNew_value());
        if(srfdomain.getAv_timeDirtyFlag())
            this.setAv_time(srfdomain.getAv_time());
        if(srfdomain.getTotal_valueDirtyFlag())
            this.setTotal_value(srfdomain.getTotal_value());
        if(srfdomain.getName_textDirtyFlag())
            this.setName_text(srfdomain.getName_text());
        if(srfdomain.getCreate_uid_textDirtyFlag())
            this.setCreate_uid_text(srfdomain.getCreate_uid_text());
        if(srfdomain.getMeter_uomDirtyFlag())
            this.setMeter_uom(srfdomain.getMeter_uom());
        if(srfdomain.getWrite_uid_textDirtyFlag())
            this.setWrite_uid_text(srfdomain.getWrite_uid_text());
        if(srfdomain.getAsset_id_textDirtyFlag())
            this.setAsset_id_text(srfdomain.getAsset_id_text());
        if(srfdomain.getParent_ratio_id_textDirtyFlag())
            this.setParent_ratio_id_text(srfdomain.getParent_ratio_id_text());
        if(srfdomain.getParent_meter_idDirtyFlag())
            this.setParent_meter_id(srfdomain.getParent_meter_id());
        if(srfdomain.getParent_ratio_idDirtyFlag())
            this.setParent_ratio_id(srfdomain.getParent_ratio_id());
        if(srfdomain.getNameDirtyFlag())
            this.setName(srfdomain.getName());
        if(srfdomain.getWrite_uidDirtyFlag())
            this.setWrite_uid(srfdomain.getWrite_uid());
        if(srfdomain.getCreate_uidDirtyFlag())
            this.setCreate_uid(srfdomain.getCreate_uid());
        if(srfdomain.getAsset_idDirtyFlag())
            this.setAsset_id(srfdomain.getAsset_id());

    }

    public List<Mro_pm_meterDTO> fromDOPage(List<Mro_pm_meter> poPage)   {
        if(poPage == null)
            return null;
        List<Mro_pm_meterDTO> dtos=new ArrayList<Mro_pm_meterDTO>();
        for(Mro_pm_meter domain : poPage) {
            Mro_pm_meterDTO dto = new Mro_pm_meterDTO();
            dto.fromDO(domain);
            dtos.add(dto);
        }
        return dtos;
    }
}

