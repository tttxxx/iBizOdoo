package cn.ibizlab.odoo.service.odoo_mro.config;

import org.springframework.context.annotation.ComponentScan;

@ComponentScan("cn.ibizlab.odoo.service.odoo_mro")
public class odoo_mroRestConfiguration {

}
