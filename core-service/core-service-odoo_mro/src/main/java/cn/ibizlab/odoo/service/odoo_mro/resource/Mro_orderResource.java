package cn.ibizlab.odoo.service.odoo_mro.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_mro.dto.Mro_orderDTO;
import cn.ibizlab.odoo.core.odoo_mro.domain.Mro_order;
import cn.ibizlab.odoo.core.odoo_mro.service.IMro_orderService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_mro.filter.Mro_orderSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Mro_order" })
@RestController
@RequestMapping("")
public class Mro_orderResource {

    @Autowired
    private IMro_orderService mro_orderService;

    public IMro_orderService getMro_orderService() {
        return this.mro_orderService;
    }

    @ApiOperation(value = "建立数据", tags = {"Mro_order" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_mro/mro_orders")

    public ResponseEntity<Mro_orderDTO> create(@RequestBody Mro_orderDTO mro_orderdto) {
        Mro_orderDTO dto = new Mro_orderDTO();
        Mro_order domain = mro_orderdto.toDO();
		mro_orderService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "删除数据", tags = {"Mro_order" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_mro/mro_orders/{mro_order_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("mro_order_id") Integer mro_order_id) {
        Mro_orderDTO mro_orderdto = new Mro_orderDTO();
		Mro_order domain = new Mro_order();
		mro_orderdto.setId(mro_order_id);
		domain.setId(mro_order_id);
        Boolean rst = mro_orderService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "获取数据", tags = {"Mro_order" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_mro/mro_orders/{mro_order_id}")
    public ResponseEntity<Mro_orderDTO> get(@PathVariable("mro_order_id") Integer mro_order_id) {
        Mro_orderDTO dto = new Mro_orderDTO();
        Mro_order domain = mro_orderService.get(mro_order_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "更新数据", tags = {"Mro_order" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_mro/mro_orders/{mro_order_id}")

    public ResponseEntity<Mro_orderDTO> update(@PathVariable("mro_order_id") Integer mro_order_id, @RequestBody Mro_orderDTO mro_orderdto) {
		Mro_order domain = mro_orderdto.toDO();
        domain.setId(mro_order_id);
		mro_orderService.update(domain);
		Mro_orderDTO dto = new Mro_orderDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批更新数据", tags = {"Mro_order" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_mro/mro_orders/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Mro_orderDTO> mro_orderdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "批删除数据", tags = {"Mro_order" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_mro/mro_orders/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Mro_orderDTO> mro_orderdtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "批建立数据", tags = {"Mro_order" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_mro/mro_orders/createBatch")
    public ResponseEntity<Boolean> createBatchMro_order(@RequestBody List<Mro_orderDTO> mro_orderdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Mro_order" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_mro/mro_orders/fetchdefault")
	public ResponseEntity<Page<Mro_orderDTO>> fetchDefault(Mro_orderSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Mro_orderDTO> list = new ArrayList<Mro_orderDTO>();
        
        Page<Mro_order> domains = mro_orderService.searchDefault(context) ;
        for(Mro_order mro_order : domains.getContent()){
            Mro_orderDTO dto = new Mro_orderDTO();
            dto.fromDO(mro_order);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
