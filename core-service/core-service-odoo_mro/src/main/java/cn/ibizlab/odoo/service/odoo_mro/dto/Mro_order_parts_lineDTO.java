package cn.ibizlab.odoo.service.odoo_mro.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.core.odoo_mro.valuerule.anno.mro_order_parts_line.*;
import cn.ibizlab.odoo.core.odoo_mro.domain.Mro_order_parts_line;
import org.springframework.cglib.beans.BeanCopier;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 服务DTO对象[Mro_order_parts_lineDTO]
 */
public class Mro_order_parts_lineDTO implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [NAME]
     *
     */
    @Mro_order_parts_lineNameDefault(info = "默认规则")
    private String name;

    @JsonIgnore
    private boolean nameDirtyFlag;

    /**
     * 属性 [CREATE_DATE]
     *
     */
    @Mro_order_parts_lineCreate_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp create_date;

    @JsonIgnore
    private boolean create_dateDirtyFlag;

    /**
     * 属性 [ID]
     *
     */
    @Mro_order_parts_lineIdDefault(info = "默认规则")
    private Integer id;

    @JsonIgnore
    private boolean idDirtyFlag;

    /**
     * 属性 [WRITE_DATE]
     *
     */
    @Mro_order_parts_lineWrite_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp write_date;

    @JsonIgnore
    private boolean write_dateDirtyFlag;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @Mro_order_parts_line__last_updateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp __last_update;

    @JsonIgnore
    private boolean __last_updateDirtyFlag;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @Mro_order_parts_lineDisplay_nameDefault(info = "默认规则")
    private String display_name;

    @JsonIgnore
    private boolean display_nameDirtyFlag;

    /**
     * 属性 [PARTS_QTY]
     *
     */
    @Mro_order_parts_lineParts_qtyDefault(info = "默认规则")
    private Double parts_qty;

    @JsonIgnore
    private boolean parts_qtyDirtyFlag;

    /**
     * 属性 [CREATE_UID_TEXT]
     *
     */
    @Mro_order_parts_lineCreate_uid_textDefault(info = "默认规则")
    private String create_uid_text;

    @JsonIgnore
    private boolean create_uid_textDirtyFlag;

    /**
     * 属性 [PARTS_UOM_TEXT]
     *
     */
    @Mro_order_parts_lineParts_uom_textDefault(info = "默认规则")
    private String parts_uom_text;

    @JsonIgnore
    private boolean parts_uom_textDirtyFlag;

    /**
     * 属性 [WRITE_UID_TEXT]
     *
     */
    @Mro_order_parts_lineWrite_uid_textDefault(info = "默认规则")
    private String write_uid_text;

    @JsonIgnore
    private boolean write_uid_textDirtyFlag;

    /**
     * 属性 [PARTS_ID_TEXT]
     *
     */
    @Mro_order_parts_lineParts_id_textDefault(info = "默认规则")
    private String parts_id_text;

    @JsonIgnore
    private boolean parts_id_textDirtyFlag;

    /**
     * 属性 [MAINTENANCE_ID_TEXT]
     *
     */
    @Mro_order_parts_lineMaintenance_id_textDefault(info = "默认规则")
    private String maintenance_id_text;

    @JsonIgnore
    private boolean maintenance_id_textDirtyFlag;

    /**
     * 属性 [PARTS_ID]
     *
     */
    @Mro_order_parts_lineParts_idDefault(info = "默认规则")
    private Integer parts_id;

    @JsonIgnore
    private boolean parts_idDirtyFlag;

    /**
     * 属性 [MAINTENANCE_ID]
     *
     */
    @Mro_order_parts_lineMaintenance_idDefault(info = "默认规则")
    private Integer maintenance_id;

    @JsonIgnore
    private boolean maintenance_idDirtyFlag;

    /**
     * 属性 [WRITE_UID]
     *
     */
    @Mro_order_parts_lineWrite_uidDefault(info = "默认规则")
    private Integer write_uid;

    @JsonIgnore
    private boolean write_uidDirtyFlag;

    /**
     * 属性 [PARTS_UOM]
     *
     */
    @Mro_order_parts_lineParts_uomDefault(info = "默认规则")
    private Integer parts_uom;

    @JsonIgnore
    private boolean parts_uomDirtyFlag;

    /**
     * 属性 [CREATE_UID]
     *
     */
    @Mro_order_parts_lineCreate_uidDefault(info = "默认规则")
    private Integer create_uid;

    @JsonIgnore
    private boolean create_uidDirtyFlag;


    /**
     * 获取 [NAME]
     */
    @JsonProperty("name")
    public String getName(){
        return name ;
    }

    /**
     * 设置 [NAME]
     */
    @JsonProperty("name")
    public void setName(String  name){
        this.name = name ;
        this.nameDirtyFlag = true ;
    }

    /**
     * 获取 [NAME]脏标记
     */
    @JsonIgnore
    public boolean getNameDirtyFlag(){
        return nameDirtyFlag ;
    }

    /**
     * 获取 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return create_date ;
    }

    /**
     * 设置 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return create_dateDirtyFlag ;
    }

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

    /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return idDirtyFlag ;
    }

    /**
     * 获取 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return write_date ;
    }

    /**
     * 设置 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return write_dateDirtyFlag ;
    }

    /**
     * 获取 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return __last_update ;
    }

    /**
     * 设置 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

    /**
     * 获取 [__LAST_UPDATE]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return __last_updateDirtyFlag ;
    }

    /**
     * 获取 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return display_name ;
    }

    /**
     * 设置 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

    /**
     * 获取 [DISPLAY_NAME]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return display_nameDirtyFlag ;
    }

    /**
     * 获取 [PARTS_QTY]
     */
    @JsonProperty("parts_qty")
    public Double getParts_qty(){
        return parts_qty ;
    }

    /**
     * 设置 [PARTS_QTY]
     */
    @JsonProperty("parts_qty")
    public void setParts_qty(Double  parts_qty){
        this.parts_qty = parts_qty ;
        this.parts_qtyDirtyFlag = true ;
    }

    /**
     * 获取 [PARTS_QTY]脏标记
     */
    @JsonIgnore
    public boolean getParts_qtyDirtyFlag(){
        return parts_qtyDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return create_uid_text ;
    }

    /**
     * 设置 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return create_uid_textDirtyFlag ;
    }

    /**
     * 获取 [PARTS_UOM_TEXT]
     */
    @JsonProperty("parts_uom_text")
    public String getParts_uom_text(){
        return parts_uom_text ;
    }

    /**
     * 设置 [PARTS_UOM_TEXT]
     */
    @JsonProperty("parts_uom_text")
    public void setParts_uom_text(String  parts_uom_text){
        this.parts_uom_text = parts_uom_text ;
        this.parts_uom_textDirtyFlag = true ;
    }

    /**
     * 获取 [PARTS_UOM_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getParts_uom_textDirtyFlag(){
        return parts_uom_textDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return write_uid_text ;
    }

    /**
     * 设置 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return write_uid_textDirtyFlag ;
    }

    /**
     * 获取 [PARTS_ID_TEXT]
     */
    @JsonProperty("parts_id_text")
    public String getParts_id_text(){
        return parts_id_text ;
    }

    /**
     * 设置 [PARTS_ID_TEXT]
     */
    @JsonProperty("parts_id_text")
    public void setParts_id_text(String  parts_id_text){
        this.parts_id_text = parts_id_text ;
        this.parts_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [PARTS_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getParts_id_textDirtyFlag(){
        return parts_id_textDirtyFlag ;
    }

    /**
     * 获取 [MAINTENANCE_ID_TEXT]
     */
    @JsonProperty("maintenance_id_text")
    public String getMaintenance_id_text(){
        return maintenance_id_text ;
    }

    /**
     * 设置 [MAINTENANCE_ID_TEXT]
     */
    @JsonProperty("maintenance_id_text")
    public void setMaintenance_id_text(String  maintenance_id_text){
        this.maintenance_id_text = maintenance_id_text ;
        this.maintenance_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [MAINTENANCE_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getMaintenance_id_textDirtyFlag(){
        return maintenance_id_textDirtyFlag ;
    }

    /**
     * 获取 [PARTS_ID]
     */
    @JsonProperty("parts_id")
    public Integer getParts_id(){
        return parts_id ;
    }

    /**
     * 设置 [PARTS_ID]
     */
    @JsonProperty("parts_id")
    public void setParts_id(Integer  parts_id){
        this.parts_id = parts_id ;
        this.parts_idDirtyFlag = true ;
    }

    /**
     * 获取 [PARTS_ID]脏标记
     */
    @JsonIgnore
    public boolean getParts_idDirtyFlag(){
        return parts_idDirtyFlag ;
    }

    /**
     * 获取 [MAINTENANCE_ID]
     */
    @JsonProperty("maintenance_id")
    public Integer getMaintenance_id(){
        return maintenance_id ;
    }

    /**
     * 设置 [MAINTENANCE_ID]
     */
    @JsonProperty("maintenance_id")
    public void setMaintenance_id(Integer  maintenance_id){
        this.maintenance_id = maintenance_id ;
        this.maintenance_idDirtyFlag = true ;
    }

    /**
     * 获取 [MAINTENANCE_ID]脏标记
     */
    @JsonIgnore
    public boolean getMaintenance_idDirtyFlag(){
        return maintenance_idDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return write_uid ;
    }

    /**
     * 设置 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return write_uidDirtyFlag ;
    }

    /**
     * 获取 [PARTS_UOM]
     */
    @JsonProperty("parts_uom")
    public Integer getParts_uom(){
        return parts_uom ;
    }

    /**
     * 设置 [PARTS_UOM]
     */
    @JsonProperty("parts_uom")
    public void setParts_uom(Integer  parts_uom){
        this.parts_uom = parts_uom ;
        this.parts_uomDirtyFlag = true ;
    }

    /**
     * 获取 [PARTS_UOM]脏标记
     */
    @JsonIgnore
    public boolean getParts_uomDirtyFlag(){
        return parts_uomDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return create_uid ;
    }

    /**
     * 设置 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return create_uidDirtyFlag ;
    }



    public Mro_order_parts_line toDO() {
        Mro_order_parts_line srfdomain = new Mro_order_parts_line();
        if(getNameDirtyFlag())
            srfdomain.setName(name);
        if(getCreate_dateDirtyFlag())
            srfdomain.setCreate_date(create_date);
        if(getIdDirtyFlag())
            srfdomain.setId(id);
        if(getWrite_dateDirtyFlag())
            srfdomain.setWrite_date(write_date);
        if(get__last_updateDirtyFlag())
            srfdomain.set__last_update(__last_update);
        if(getDisplay_nameDirtyFlag())
            srfdomain.setDisplay_name(display_name);
        if(getParts_qtyDirtyFlag())
            srfdomain.setParts_qty(parts_qty);
        if(getCreate_uid_textDirtyFlag())
            srfdomain.setCreate_uid_text(create_uid_text);
        if(getParts_uom_textDirtyFlag())
            srfdomain.setParts_uom_text(parts_uom_text);
        if(getWrite_uid_textDirtyFlag())
            srfdomain.setWrite_uid_text(write_uid_text);
        if(getParts_id_textDirtyFlag())
            srfdomain.setParts_id_text(parts_id_text);
        if(getMaintenance_id_textDirtyFlag())
            srfdomain.setMaintenance_id_text(maintenance_id_text);
        if(getParts_idDirtyFlag())
            srfdomain.setParts_id(parts_id);
        if(getMaintenance_idDirtyFlag())
            srfdomain.setMaintenance_id(maintenance_id);
        if(getWrite_uidDirtyFlag())
            srfdomain.setWrite_uid(write_uid);
        if(getParts_uomDirtyFlag())
            srfdomain.setParts_uom(parts_uom);
        if(getCreate_uidDirtyFlag())
            srfdomain.setCreate_uid(create_uid);

        return srfdomain;
    }

    public void fromDO(Mro_order_parts_line srfdomain) {
        if(srfdomain == null )
          return ;
        if(srfdomain.getNameDirtyFlag())
            this.setName(srfdomain.getName());
        if(srfdomain.getCreate_dateDirtyFlag())
            this.setCreate_date(srfdomain.getCreate_date());
        if(srfdomain.getIdDirtyFlag())
            this.setId(srfdomain.getId());
        if(srfdomain.getWrite_dateDirtyFlag())
            this.setWrite_date(srfdomain.getWrite_date());
        if(srfdomain.get__last_updateDirtyFlag())
            this.set__last_update(srfdomain.get__last_update());
        if(srfdomain.getDisplay_nameDirtyFlag())
            this.setDisplay_name(srfdomain.getDisplay_name());
        if(srfdomain.getParts_qtyDirtyFlag())
            this.setParts_qty(srfdomain.getParts_qty());
        if(srfdomain.getCreate_uid_textDirtyFlag())
            this.setCreate_uid_text(srfdomain.getCreate_uid_text());
        if(srfdomain.getParts_uom_textDirtyFlag())
            this.setParts_uom_text(srfdomain.getParts_uom_text());
        if(srfdomain.getWrite_uid_textDirtyFlag())
            this.setWrite_uid_text(srfdomain.getWrite_uid_text());
        if(srfdomain.getParts_id_textDirtyFlag())
            this.setParts_id_text(srfdomain.getParts_id_text());
        if(srfdomain.getMaintenance_id_textDirtyFlag())
            this.setMaintenance_id_text(srfdomain.getMaintenance_id_text());
        if(srfdomain.getParts_idDirtyFlag())
            this.setParts_id(srfdomain.getParts_id());
        if(srfdomain.getMaintenance_idDirtyFlag())
            this.setMaintenance_id(srfdomain.getMaintenance_id());
        if(srfdomain.getWrite_uidDirtyFlag())
            this.setWrite_uid(srfdomain.getWrite_uid());
        if(srfdomain.getParts_uomDirtyFlag())
            this.setParts_uom(srfdomain.getParts_uom());
        if(srfdomain.getCreate_uidDirtyFlag())
            this.setCreate_uid(srfdomain.getCreate_uid());

    }

    public List<Mro_order_parts_lineDTO> fromDOPage(List<Mro_order_parts_line> poPage)   {
        if(poPage == null)
            return null;
        List<Mro_order_parts_lineDTO> dtos=new ArrayList<Mro_order_parts_lineDTO>();
        for(Mro_order_parts_line domain : poPage) {
            Mro_order_parts_lineDTO dto = new Mro_order_parts_lineDTO();
            dto.fromDO(domain);
            dtos.add(dto);
        }
        return dtos;
    }
}

