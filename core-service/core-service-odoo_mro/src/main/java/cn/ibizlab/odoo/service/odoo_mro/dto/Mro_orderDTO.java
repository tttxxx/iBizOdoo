package cn.ibizlab.odoo.service.odoo_mro.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.core.odoo_mro.valuerule.anno.mro_order.*;
import cn.ibizlab.odoo.core.odoo_mro.domain.Mro_order;
import org.springframework.cglib.beans.BeanCopier;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 服务DTO对象[Mro_orderDTO]
 */
public class Mro_orderDTO implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [MESSAGE_IDS]
     *
     */
    @Mro_orderMessage_idsDefault(info = "默认规则")
    private String message_ids;

    @JsonIgnore
    private boolean message_idsDirtyFlag;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @Mro_order__last_updateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp __last_update;

    @JsonIgnore
    private boolean __last_updateDirtyFlag;

    /**
     * 属性 [MESSAGE_PARTNER_IDS]
     *
     */
    @Mro_orderMessage_partner_idsDefault(info = "默认规则")
    private String message_partner_ids;

    @JsonIgnore
    private boolean message_partner_idsDirtyFlag;

    /**
     * 属性 [MAINTENANCE_TYPE]
     *
     */
    @Mro_orderMaintenance_typeDefault(info = "默认规则")
    private String maintenance_type;

    @JsonIgnore
    private boolean maintenance_typeDirtyFlag;

    /**
     * 属性 [MESSAGE_UNREAD_COUNTER]
     *
     */
    @Mro_orderMessage_unread_counterDefault(info = "默认规则")
    private Integer message_unread_counter;

    @JsonIgnore
    private boolean message_unread_counterDirtyFlag;

    /**
     * 属性 [DOCUMENTATION_DESCRIPTION]
     *
     */
    @Mro_orderDocumentation_descriptionDefault(info = "默认规则")
    private String documentation_description;

    @JsonIgnore
    private boolean documentation_descriptionDirtyFlag;

    /**
     * 属性 [PARTS_MOVED_LINES]
     *
     */
    @Mro_orderParts_moved_linesDefault(info = "默认规则")
    private String parts_moved_lines;

    @JsonIgnore
    private boolean parts_moved_linesDirtyFlag;

    /**
     * 属性 [MESSAGE_CHANNEL_IDS]
     *
     */
    @Mro_orderMessage_channel_idsDefault(info = "默认规则")
    private String message_channel_ids;

    @JsonIgnore
    private boolean message_channel_idsDirtyFlag;

    /**
     * 属性 [CATEGORY_IDS]
     *
     */
    @Mro_orderCategory_idsDefault(info = "默认规则")
    private String category_ids;

    @JsonIgnore
    private boolean category_idsDirtyFlag;

    /**
     * 属性 [DATE_SCHEDULED]
     *
     */
    @Mro_orderDate_scheduledDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp date_scheduled;

    @JsonIgnore
    private boolean date_scheduledDirtyFlag;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @Mro_orderDisplay_nameDefault(info = "默认规则")
    private String display_name;

    @JsonIgnore
    private boolean display_nameDirtyFlag;

    /**
     * 属性 [LABOR_DESCRIPTION]
     *
     */
    @Mro_orderLabor_descriptionDefault(info = "默认规则")
    private String labor_description;

    @JsonIgnore
    private boolean labor_descriptionDirtyFlag;

    /**
     * 属性 [MESSAGE_UNREAD]
     *
     */
    @Mro_orderMessage_unreadDefault(info = "默认规则")
    private String message_unread;

    @JsonIgnore
    private boolean message_unreadDirtyFlag;

    /**
     * 属性 [MESSAGE_FOLLOWER_IDS]
     *
     */
    @Mro_orderMessage_follower_idsDefault(info = "默认规则")
    private String message_follower_ids;

    @JsonIgnore
    private boolean message_follower_idsDirtyFlag;

    /**
     * 属性 [ORIGIN]
     *
     */
    @Mro_orderOriginDefault(info = "默认规则")
    private String origin;

    @JsonIgnore
    private boolean originDirtyFlag;

    /**
     * 属性 [MESSAGE_HAS_ERROR_COUNTER]
     *
     */
    @Mro_orderMessage_has_error_counterDefault(info = "默认规则")
    private Integer message_has_error_counter;

    @JsonIgnore
    private boolean message_has_error_counterDirtyFlag;

    /**
     * 属性 [WRITE_DATE]
     *
     */
    @Mro_orderWrite_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp write_date;

    @JsonIgnore
    private boolean write_dateDirtyFlag;

    /**
     * 属性 [DESCRIPTION]
     *
     */
    @Mro_orderDescriptionDefault(info = "默认规则")
    private String description;

    @JsonIgnore
    private boolean descriptionDirtyFlag;

    /**
     * 属性 [PARTS_READY_LINES]
     *
     */
    @Mro_orderParts_ready_linesDefault(info = "默认规则")
    private String parts_ready_lines;

    @JsonIgnore
    private boolean parts_ready_linesDirtyFlag;

    /**
     * 属性 [WEBSITE_MESSAGE_IDS]
     *
     */
    @Mro_orderWebsite_message_idsDefault(info = "默认规则")
    private String website_message_ids;

    @JsonIgnore
    private boolean website_message_idsDirtyFlag;

    /**
     * 属性 [PARTS_MOVE_LINES]
     *
     */
    @Mro_orderParts_move_linesDefault(info = "默认规则")
    private String parts_move_lines;

    @JsonIgnore
    private boolean parts_move_linesDirtyFlag;

    /**
     * 属性 [MESSAGE_IS_FOLLOWER]
     *
     */
    @Mro_orderMessage_is_followerDefault(info = "默认规则")
    private String message_is_follower;

    @JsonIgnore
    private boolean message_is_followerDirtyFlag;

    /**
     * 属性 [MESSAGE_NEEDACTION_COUNTER]
     *
     */
    @Mro_orderMessage_needaction_counterDefault(info = "默认规则")
    private Integer message_needaction_counter;

    @JsonIgnore
    private boolean message_needaction_counterDirtyFlag;

    /**
     * 属性 [OPERATIONS_DESCRIPTION]
     *
     */
    @Mro_orderOperations_descriptionDefault(info = "默认规则")
    private String operations_description;

    @JsonIgnore
    private boolean operations_descriptionDirtyFlag;

    /**
     * 属性 [PROBLEM_DESCRIPTION]
     *
     */
    @Mro_orderProblem_descriptionDefault(info = "默认规则")
    private String problem_description;

    @JsonIgnore
    private boolean problem_descriptionDirtyFlag;

    /**
     * 属性 [MESSAGE_ATTACHMENT_COUNT]
     *
     */
    @Mro_orderMessage_attachment_countDefault(info = "默认规则")
    private Integer message_attachment_count;

    @JsonIgnore
    private boolean message_attachment_countDirtyFlag;

    /**
     * 属性 [DATE_PLANNED]
     *
     */
    @Mro_orderDate_plannedDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp date_planned;

    @JsonIgnore
    private boolean date_plannedDirtyFlag;

    /**
     * 属性 [PROCUREMENT_GROUP_ID]
     *
     */
    @Mro_orderProcurement_group_idDefault(info = "默认规则")
    private Integer procurement_group_id;

    @JsonIgnore
    private boolean procurement_group_idDirtyFlag;

    /**
     * 属性 [DATE_EXECUTION]
     *
     */
    @Mro_orderDate_executionDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp date_execution;

    @JsonIgnore
    private boolean date_executionDirtyFlag;

    /**
     * 属性 [MESSAGE_HAS_ERROR]
     *
     */
    @Mro_orderMessage_has_errorDefault(info = "默认规则")
    private String message_has_error;

    @JsonIgnore
    private boolean message_has_errorDirtyFlag;

    /**
     * 属性 [MESSAGE_MAIN_ATTACHMENT_ID]
     *
     */
    @Mro_orderMessage_main_attachment_idDefault(info = "默认规则")
    private Integer message_main_attachment_id;

    @JsonIgnore
    private boolean message_main_attachment_idDirtyFlag;

    /**
     * 属性 [ID]
     *
     */
    @Mro_orderIdDefault(info = "默认规则")
    private Integer id;

    @JsonIgnore
    private boolean idDirtyFlag;

    /**
     * 属性 [PARTS_LINES]
     *
     */
    @Mro_orderParts_linesDefault(info = "默认规则")
    private String parts_lines;

    @JsonIgnore
    private boolean parts_linesDirtyFlag;

    /**
     * 属性 [NAME]
     *
     */
    @Mro_orderNameDefault(info = "默认规则")
    private String name;

    @JsonIgnore
    private boolean nameDirtyFlag;

    /**
     * 属性 [STATE]
     *
     */
    @Mro_orderStateDefault(info = "默认规则")
    private String state;

    @JsonIgnore
    private boolean stateDirtyFlag;

    /**
     * 属性 [CREATE_DATE]
     *
     */
    @Mro_orderCreate_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp create_date;

    @JsonIgnore
    private boolean create_dateDirtyFlag;

    /**
     * 属性 [MESSAGE_NEEDACTION]
     *
     */
    @Mro_orderMessage_needactionDefault(info = "默认规则")
    private String message_needaction;

    @JsonIgnore
    private boolean message_needactionDirtyFlag;

    /**
     * 属性 [TOOLS_DESCRIPTION]
     *
     */
    @Mro_orderTools_descriptionDefault(info = "默认规则")
    private String tools_description;

    @JsonIgnore
    private boolean tools_descriptionDirtyFlag;

    /**
     * 属性 [CREATE_UID_TEXT]
     *
     */
    @Mro_orderCreate_uid_textDefault(info = "默认规则")
    private String create_uid_text;

    @JsonIgnore
    private boolean create_uid_textDirtyFlag;

    /**
     * 属性 [WRITE_UID_TEXT]
     *
     */
    @Mro_orderWrite_uid_textDefault(info = "默认规则")
    private String write_uid_text;

    @JsonIgnore
    private boolean write_uid_textDirtyFlag;

    /**
     * 属性 [WO_ID_TEXT]
     *
     */
    @Mro_orderWo_id_textDefault(info = "默认规则")
    private String wo_id_text;

    @JsonIgnore
    private boolean wo_id_textDirtyFlag;

    /**
     * 属性 [ASSET_ID_TEXT]
     *
     */
    @Mro_orderAsset_id_textDefault(info = "默认规则")
    private String asset_id_text;

    @JsonIgnore
    private boolean asset_id_textDirtyFlag;

    /**
     * 属性 [TASK_ID_TEXT]
     *
     */
    @Mro_orderTask_id_textDefault(info = "默认规则")
    private String task_id_text;

    @JsonIgnore
    private boolean task_id_textDirtyFlag;

    /**
     * 属性 [COMPANY_ID_TEXT]
     *
     */
    @Mro_orderCompany_id_textDefault(info = "默认规则")
    private String company_id_text;

    @JsonIgnore
    private boolean company_id_textDirtyFlag;

    /**
     * 属性 [REQUEST_ID_TEXT]
     *
     */
    @Mro_orderRequest_id_textDefault(info = "默认规则")
    private String request_id_text;

    @JsonIgnore
    private boolean request_id_textDirtyFlag;

    /**
     * 属性 [USER_ID_TEXT]
     *
     */
    @Mro_orderUser_id_textDefault(info = "默认规则")
    private String user_id_text;

    @JsonIgnore
    private boolean user_id_textDirtyFlag;

    /**
     * 属性 [REQUEST_ID]
     *
     */
    @Mro_orderRequest_idDefault(info = "默认规则")
    private Integer request_id;

    @JsonIgnore
    private boolean request_idDirtyFlag;

    /**
     * 属性 [TASK_ID]
     *
     */
    @Mro_orderTask_idDefault(info = "默认规则")
    private Integer task_id;

    @JsonIgnore
    private boolean task_idDirtyFlag;

    /**
     * 属性 [WO_ID]
     *
     */
    @Mro_orderWo_idDefault(info = "默认规则")
    private Integer wo_id;

    @JsonIgnore
    private boolean wo_idDirtyFlag;

    /**
     * 属性 [COMPANY_ID]
     *
     */
    @Mro_orderCompany_idDefault(info = "默认规则")
    private Integer company_id;

    @JsonIgnore
    private boolean company_idDirtyFlag;

    /**
     * 属性 [USER_ID]
     *
     */
    @Mro_orderUser_idDefault(info = "默认规则")
    private Integer user_id;

    @JsonIgnore
    private boolean user_idDirtyFlag;

    /**
     * 属性 [ASSET_ID]
     *
     */
    @Mro_orderAsset_idDefault(info = "默认规则")
    private Integer asset_id;

    @JsonIgnore
    private boolean asset_idDirtyFlag;

    /**
     * 属性 [CREATE_UID]
     *
     */
    @Mro_orderCreate_uidDefault(info = "默认规则")
    private Integer create_uid;

    @JsonIgnore
    private boolean create_uidDirtyFlag;

    /**
     * 属性 [WRITE_UID]
     *
     */
    @Mro_orderWrite_uidDefault(info = "默认规则")
    private Integer write_uid;

    @JsonIgnore
    private boolean write_uidDirtyFlag;


    /**
     * 获取 [MESSAGE_IDS]
     */
    @JsonProperty("message_ids")
    public String getMessage_ids(){
        return message_ids ;
    }

    /**
     * 设置 [MESSAGE_IDS]
     */
    @JsonProperty("message_ids")
    public void setMessage_ids(String  message_ids){
        this.message_ids = message_ids ;
        this.message_idsDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_IDS]脏标记
     */
    @JsonIgnore
    public boolean getMessage_idsDirtyFlag(){
        return message_idsDirtyFlag ;
    }

    /**
     * 获取 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return __last_update ;
    }

    /**
     * 设置 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

    /**
     * 获取 [__LAST_UPDATE]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return __last_updateDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_PARTNER_IDS]
     */
    @JsonProperty("message_partner_ids")
    public String getMessage_partner_ids(){
        return message_partner_ids ;
    }

    /**
     * 设置 [MESSAGE_PARTNER_IDS]
     */
    @JsonProperty("message_partner_ids")
    public void setMessage_partner_ids(String  message_partner_ids){
        this.message_partner_ids = message_partner_ids ;
        this.message_partner_idsDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_PARTNER_IDS]脏标记
     */
    @JsonIgnore
    public boolean getMessage_partner_idsDirtyFlag(){
        return message_partner_idsDirtyFlag ;
    }

    /**
     * 获取 [MAINTENANCE_TYPE]
     */
    @JsonProperty("maintenance_type")
    public String getMaintenance_type(){
        return maintenance_type ;
    }

    /**
     * 设置 [MAINTENANCE_TYPE]
     */
    @JsonProperty("maintenance_type")
    public void setMaintenance_type(String  maintenance_type){
        this.maintenance_type = maintenance_type ;
        this.maintenance_typeDirtyFlag = true ;
    }

    /**
     * 获取 [MAINTENANCE_TYPE]脏标记
     */
    @JsonIgnore
    public boolean getMaintenance_typeDirtyFlag(){
        return maintenance_typeDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_UNREAD_COUNTER]
     */
    @JsonProperty("message_unread_counter")
    public Integer getMessage_unread_counter(){
        return message_unread_counter ;
    }

    /**
     * 设置 [MESSAGE_UNREAD_COUNTER]
     */
    @JsonProperty("message_unread_counter")
    public void setMessage_unread_counter(Integer  message_unread_counter){
        this.message_unread_counter = message_unread_counter ;
        this.message_unread_counterDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_UNREAD_COUNTER]脏标记
     */
    @JsonIgnore
    public boolean getMessage_unread_counterDirtyFlag(){
        return message_unread_counterDirtyFlag ;
    }

    /**
     * 获取 [DOCUMENTATION_DESCRIPTION]
     */
    @JsonProperty("documentation_description")
    public String getDocumentation_description(){
        return documentation_description ;
    }

    /**
     * 设置 [DOCUMENTATION_DESCRIPTION]
     */
    @JsonProperty("documentation_description")
    public void setDocumentation_description(String  documentation_description){
        this.documentation_description = documentation_description ;
        this.documentation_descriptionDirtyFlag = true ;
    }

    /**
     * 获取 [DOCUMENTATION_DESCRIPTION]脏标记
     */
    @JsonIgnore
    public boolean getDocumentation_descriptionDirtyFlag(){
        return documentation_descriptionDirtyFlag ;
    }

    /**
     * 获取 [PARTS_MOVED_LINES]
     */
    @JsonProperty("parts_moved_lines")
    public String getParts_moved_lines(){
        return parts_moved_lines ;
    }

    /**
     * 设置 [PARTS_MOVED_LINES]
     */
    @JsonProperty("parts_moved_lines")
    public void setParts_moved_lines(String  parts_moved_lines){
        this.parts_moved_lines = parts_moved_lines ;
        this.parts_moved_linesDirtyFlag = true ;
    }

    /**
     * 获取 [PARTS_MOVED_LINES]脏标记
     */
    @JsonIgnore
    public boolean getParts_moved_linesDirtyFlag(){
        return parts_moved_linesDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_CHANNEL_IDS]
     */
    @JsonProperty("message_channel_ids")
    public String getMessage_channel_ids(){
        return message_channel_ids ;
    }

    /**
     * 设置 [MESSAGE_CHANNEL_IDS]
     */
    @JsonProperty("message_channel_ids")
    public void setMessage_channel_ids(String  message_channel_ids){
        this.message_channel_ids = message_channel_ids ;
        this.message_channel_idsDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_CHANNEL_IDS]脏标记
     */
    @JsonIgnore
    public boolean getMessage_channel_idsDirtyFlag(){
        return message_channel_idsDirtyFlag ;
    }

    /**
     * 获取 [CATEGORY_IDS]
     */
    @JsonProperty("category_ids")
    public String getCategory_ids(){
        return category_ids ;
    }

    /**
     * 设置 [CATEGORY_IDS]
     */
    @JsonProperty("category_ids")
    public void setCategory_ids(String  category_ids){
        this.category_ids = category_ids ;
        this.category_idsDirtyFlag = true ;
    }

    /**
     * 获取 [CATEGORY_IDS]脏标记
     */
    @JsonIgnore
    public boolean getCategory_idsDirtyFlag(){
        return category_idsDirtyFlag ;
    }

    /**
     * 获取 [DATE_SCHEDULED]
     */
    @JsonProperty("date_scheduled")
    public Timestamp getDate_scheduled(){
        return date_scheduled ;
    }

    /**
     * 设置 [DATE_SCHEDULED]
     */
    @JsonProperty("date_scheduled")
    public void setDate_scheduled(Timestamp  date_scheduled){
        this.date_scheduled = date_scheduled ;
        this.date_scheduledDirtyFlag = true ;
    }

    /**
     * 获取 [DATE_SCHEDULED]脏标记
     */
    @JsonIgnore
    public boolean getDate_scheduledDirtyFlag(){
        return date_scheduledDirtyFlag ;
    }

    /**
     * 获取 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return display_name ;
    }

    /**
     * 设置 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

    /**
     * 获取 [DISPLAY_NAME]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return display_nameDirtyFlag ;
    }

    /**
     * 获取 [LABOR_DESCRIPTION]
     */
    @JsonProperty("labor_description")
    public String getLabor_description(){
        return labor_description ;
    }

    /**
     * 设置 [LABOR_DESCRIPTION]
     */
    @JsonProperty("labor_description")
    public void setLabor_description(String  labor_description){
        this.labor_description = labor_description ;
        this.labor_descriptionDirtyFlag = true ;
    }

    /**
     * 获取 [LABOR_DESCRIPTION]脏标记
     */
    @JsonIgnore
    public boolean getLabor_descriptionDirtyFlag(){
        return labor_descriptionDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_UNREAD]
     */
    @JsonProperty("message_unread")
    public String getMessage_unread(){
        return message_unread ;
    }

    /**
     * 设置 [MESSAGE_UNREAD]
     */
    @JsonProperty("message_unread")
    public void setMessage_unread(String  message_unread){
        this.message_unread = message_unread ;
        this.message_unreadDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_UNREAD]脏标记
     */
    @JsonIgnore
    public boolean getMessage_unreadDirtyFlag(){
        return message_unreadDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_FOLLOWER_IDS]
     */
    @JsonProperty("message_follower_ids")
    public String getMessage_follower_ids(){
        return message_follower_ids ;
    }

    /**
     * 设置 [MESSAGE_FOLLOWER_IDS]
     */
    @JsonProperty("message_follower_ids")
    public void setMessage_follower_ids(String  message_follower_ids){
        this.message_follower_ids = message_follower_ids ;
        this.message_follower_idsDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_FOLLOWER_IDS]脏标记
     */
    @JsonIgnore
    public boolean getMessage_follower_idsDirtyFlag(){
        return message_follower_idsDirtyFlag ;
    }

    /**
     * 获取 [ORIGIN]
     */
    @JsonProperty("origin")
    public String getOrigin(){
        return origin ;
    }

    /**
     * 设置 [ORIGIN]
     */
    @JsonProperty("origin")
    public void setOrigin(String  origin){
        this.origin = origin ;
        this.originDirtyFlag = true ;
    }

    /**
     * 获取 [ORIGIN]脏标记
     */
    @JsonIgnore
    public boolean getOriginDirtyFlag(){
        return originDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_HAS_ERROR_COUNTER]
     */
    @JsonProperty("message_has_error_counter")
    public Integer getMessage_has_error_counter(){
        return message_has_error_counter ;
    }

    /**
     * 设置 [MESSAGE_HAS_ERROR_COUNTER]
     */
    @JsonProperty("message_has_error_counter")
    public void setMessage_has_error_counter(Integer  message_has_error_counter){
        this.message_has_error_counter = message_has_error_counter ;
        this.message_has_error_counterDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_HAS_ERROR_COUNTER]脏标记
     */
    @JsonIgnore
    public boolean getMessage_has_error_counterDirtyFlag(){
        return message_has_error_counterDirtyFlag ;
    }

    /**
     * 获取 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return write_date ;
    }

    /**
     * 设置 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return write_dateDirtyFlag ;
    }

    /**
     * 获取 [DESCRIPTION]
     */
    @JsonProperty("description")
    public String getDescription(){
        return description ;
    }

    /**
     * 设置 [DESCRIPTION]
     */
    @JsonProperty("description")
    public void setDescription(String  description){
        this.description = description ;
        this.descriptionDirtyFlag = true ;
    }

    /**
     * 获取 [DESCRIPTION]脏标记
     */
    @JsonIgnore
    public boolean getDescriptionDirtyFlag(){
        return descriptionDirtyFlag ;
    }

    /**
     * 获取 [PARTS_READY_LINES]
     */
    @JsonProperty("parts_ready_lines")
    public String getParts_ready_lines(){
        return parts_ready_lines ;
    }

    /**
     * 设置 [PARTS_READY_LINES]
     */
    @JsonProperty("parts_ready_lines")
    public void setParts_ready_lines(String  parts_ready_lines){
        this.parts_ready_lines = parts_ready_lines ;
        this.parts_ready_linesDirtyFlag = true ;
    }

    /**
     * 获取 [PARTS_READY_LINES]脏标记
     */
    @JsonIgnore
    public boolean getParts_ready_linesDirtyFlag(){
        return parts_ready_linesDirtyFlag ;
    }

    /**
     * 获取 [WEBSITE_MESSAGE_IDS]
     */
    @JsonProperty("website_message_ids")
    public String getWebsite_message_ids(){
        return website_message_ids ;
    }

    /**
     * 设置 [WEBSITE_MESSAGE_IDS]
     */
    @JsonProperty("website_message_ids")
    public void setWebsite_message_ids(String  website_message_ids){
        this.website_message_ids = website_message_ids ;
        this.website_message_idsDirtyFlag = true ;
    }

    /**
     * 获取 [WEBSITE_MESSAGE_IDS]脏标记
     */
    @JsonIgnore
    public boolean getWebsite_message_idsDirtyFlag(){
        return website_message_idsDirtyFlag ;
    }

    /**
     * 获取 [PARTS_MOVE_LINES]
     */
    @JsonProperty("parts_move_lines")
    public String getParts_move_lines(){
        return parts_move_lines ;
    }

    /**
     * 设置 [PARTS_MOVE_LINES]
     */
    @JsonProperty("parts_move_lines")
    public void setParts_move_lines(String  parts_move_lines){
        this.parts_move_lines = parts_move_lines ;
        this.parts_move_linesDirtyFlag = true ;
    }

    /**
     * 获取 [PARTS_MOVE_LINES]脏标记
     */
    @JsonIgnore
    public boolean getParts_move_linesDirtyFlag(){
        return parts_move_linesDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_IS_FOLLOWER]
     */
    @JsonProperty("message_is_follower")
    public String getMessage_is_follower(){
        return message_is_follower ;
    }

    /**
     * 设置 [MESSAGE_IS_FOLLOWER]
     */
    @JsonProperty("message_is_follower")
    public void setMessage_is_follower(String  message_is_follower){
        this.message_is_follower = message_is_follower ;
        this.message_is_followerDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_IS_FOLLOWER]脏标记
     */
    @JsonIgnore
    public boolean getMessage_is_followerDirtyFlag(){
        return message_is_followerDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_NEEDACTION_COUNTER]
     */
    @JsonProperty("message_needaction_counter")
    public Integer getMessage_needaction_counter(){
        return message_needaction_counter ;
    }

    /**
     * 设置 [MESSAGE_NEEDACTION_COUNTER]
     */
    @JsonProperty("message_needaction_counter")
    public void setMessage_needaction_counter(Integer  message_needaction_counter){
        this.message_needaction_counter = message_needaction_counter ;
        this.message_needaction_counterDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_NEEDACTION_COUNTER]脏标记
     */
    @JsonIgnore
    public boolean getMessage_needaction_counterDirtyFlag(){
        return message_needaction_counterDirtyFlag ;
    }

    /**
     * 获取 [OPERATIONS_DESCRIPTION]
     */
    @JsonProperty("operations_description")
    public String getOperations_description(){
        return operations_description ;
    }

    /**
     * 设置 [OPERATIONS_DESCRIPTION]
     */
    @JsonProperty("operations_description")
    public void setOperations_description(String  operations_description){
        this.operations_description = operations_description ;
        this.operations_descriptionDirtyFlag = true ;
    }

    /**
     * 获取 [OPERATIONS_DESCRIPTION]脏标记
     */
    @JsonIgnore
    public boolean getOperations_descriptionDirtyFlag(){
        return operations_descriptionDirtyFlag ;
    }

    /**
     * 获取 [PROBLEM_DESCRIPTION]
     */
    @JsonProperty("problem_description")
    public String getProblem_description(){
        return problem_description ;
    }

    /**
     * 设置 [PROBLEM_DESCRIPTION]
     */
    @JsonProperty("problem_description")
    public void setProblem_description(String  problem_description){
        this.problem_description = problem_description ;
        this.problem_descriptionDirtyFlag = true ;
    }

    /**
     * 获取 [PROBLEM_DESCRIPTION]脏标记
     */
    @JsonIgnore
    public boolean getProblem_descriptionDirtyFlag(){
        return problem_descriptionDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_ATTACHMENT_COUNT]
     */
    @JsonProperty("message_attachment_count")
    public Integer getMessage_attachment_count(){
        return message_attachment_count ;
    }

    /**
     * 设置 [MESSAGE_ATTACHMENT_COUNT]
     */
    @JsonProperty("message_attachment_count")
    public void setMessage_attachment_count(Integer  message_attachment_count){
        this.message_attachment_count = message_attachment_count ;
        this.message_attachment_countDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_ATTACHMENT_COUNT]脏标记
     */
    @JsonIgnore
    public boolean getMessage_attachment_countDirtyFlag(){
        return message_attachment_countDirtyFlag ;
    }

    /**
     * 获取 [DATE_PLANNED]
     */
    @JsonProperty("date_planned")
    public Timestamp getDate_planned(){
        return date_planned ;
    }

    /**
     * 设置 [DATE_PLANNED]
     */
    @JsonProperty("date_planned")
    public void setDate_planned(Timestamp  date_planned){
        this.date_planned = date_planned ;
        this.date_plannedDirtyFlag = true ;
    }

    /**
     * 获取 [DATE_PLANNED]脏标记
     */
    @JsonIgnore
    public boolean getDate_plannedDirtyFlag(){
        return date_plannedDirtyFlag ;
    }

    /**
     * 获取 [PROCUREMENT_GROUP_ID]
     */
    @JsonProperty("procurement_group_id")
    public Integer getProcurement_group_id(){
        return procurement_group_id ;
    }

    /**
     * 设置 [PROCUREMENT_GROUP_ID]
     */
    @JsonProperty("procurement_group_id")
    public void setProcurement_group_id(Integer  procurement_group_id){
        this.procurement_group_id = procurement_group_id ;
        this.procurement_group_idDirtyFlag = true ;
    }

    /**
     * 获取 [PROCUREMENT_GROUP_ID]脏标记
     */
    @JsonIgnore
    public boolean getProcurement_group_idDirtyFlag(){
        return procurement_group_idDirtyFlag ;
    }

    /**
     * 获取 [DATE_EXECUTION]
     */
    @JsonProperty("date_execution")
    public Timestamp getDate_execution(){
        return date_execution ;
    }

    /**
     * 设置 [DATE_EXECUTION]
     */
    @JsonProperty("date_execution")
    public void setDate_execution(Timestamp  date_execution){
        this.date_execution = date_execution ;
        this.date_executionDirtyFlag = true ;
    }

    /**
     * 获取 [DATE_EXECUTION]脏标记
     */
    @JsonIgnore
    public boolean getDate_executionDirtyFlag(){
        return date_executionDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_HAS_ERROR]
     */
    @JsonProperty("message_has_error")
    public String getMessage_has_error(){
        return message_has_error ;
    }

    /**
     * 设置 [MESSAGE_HAS_ERROR]
     */
    @JsonProperty("message_has_error")
    public void setMessage_has_error(String  message_has_error){
        this.message_has_error = message_has_error ;
        this.message_has_errorDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_HAS_ERROR]脏标记
     */
    @JsonIgnore
    public boolean getMessage_has_errorDirtyFlag(){
        return message_has_errorDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_MAIN_ATTACHMENT_ID]
     */
    @JsonProperty("message_main_attachment_id")
    public Integer getMessage_main_attachment_id(){
        return message_main_attachment_id ;
    }

    /**
     * 设置 [MESSAGE_MAIN_ATTACHMENT_ID]
     */
    @JsonProperty("message_main_attachment_id")
    public void setMessage_main_attachment_id(Integer  message_main_attachment_id){
        this.message_main_attachment_id = message_main_attachment_id ;
        this.message_main_attachment_idDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_MAIN_ATTACHMENT_ID]脏标记
     */
    @JsonIgnore
    public boolean getMessage_main_attachment_idDirtyFlag(){
        return message_main_attachment_idDirtyFlag ;
    }

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

    /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return idDirtyFlag ;
    }

    /**
     * 获取 [PARTS_LINES]
     */
    @JsonProperty("parts_lines")
    public String getParts_lines(){
        return parts_lines ;
    }

    /**
     * 设置 [PARTS_LINES]
     */
    @JsonProperty("parts_lines")
    public void setParts_lines(String  parts_lines){
        this.parts_lines = parts_lines ;
        this.parts_linesDirtyFlag = true ;
    }

    /**
     * 获取 [PARTS_LINES]脏标记
     */
    @JsonIgnore
    public boolean getParts_linesDirtyFlag(){
        return parts_linesDirtyFlag ;
    }

    /**
     * 获取 [NAME]
     */
    @JsonProperty("name")
    public String getName(){
        return name ;
    }

    /**
     * 设置 [NAME]
     */
    @JsonProperty("name")
    public void setName(String  name){
        this.name = name ;
        this.nameDirtyFlag = true ;
    }

    /**
     * 获取 [NAME]脏标记
     */
    @JsonIgnore
    public boolean getNameDirtyFlag(){
        return nameDirtyFlag ;
    }

    /**
     * 获取 [STATE]
     */
    @JsonProperty("state")
    public String getState(){
        return state ;
    }

    /**
     * 设置 [STATE]
     */
    @JsonProperty("state")
    public void setState(String  state){
        this.state = state ;
        this.stateDirtyFlag = true ;
    }

    /**
     * 获取 [STATE]脏标记
     */
    @JsonIgnore
    public boolean getStateDirtyFlag(){
        return stateDirtyFlag ;
    }

    /**
     * 获取 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return create_date ;
    }

    /**
     * 设置 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return create_dateDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_NEEDACTION]
     */
    @JsonProperty("message_needaction")
    public String getMessage_needaction(){
        return message_needaction ;
    }

    /**
     * 设置 [MESSAGE_NEEDACTION]
     */
    @JsonProperty("message_needaction")
    public void setMessage_needaction(String  message_needaction){
        this.message_needaction = message_needaction ;
        this.message_needactionDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_NEEDACTION]脏标记
     */
    @JsonIgnore
    public boolean getMessage_needactionDirtyFlag(){
        return message_needactionDirtyFlag ;
    }

    /**
     * 获取 [TOOLS_DESCRIPTION]
     */
    @JsonProperty("tools_description")
    public String getTools_description(){
        return tools_description ;
    }

    /**
     * 设置 [TOOLS_DESCRIPTION]
     */
    @JsonProperty("tools_description")
    public void setTools_description(String  tools_description){
        this.tools_description = tools_description ;
        this.tools_descriptionDirtyFlag = true ;
    }

    /**
     * 获取 [TOOLS_DESCRIPTION]脏标记
     */
    @JsonIgnore
    public boolean getTools_descriptionDirtyFlag(){
        return tools_descriptionDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return create_uid_text ;
    }

    /**
     * 设置 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return create_uid_textDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return write_uid_text ;
    }

    /**
     * 设置 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return write_uid_textDirtyFlag ;
    }

    /**
     * 获取 [WO_ID_TEXT]
     */
    @JsonProperty("wo_id_text")
    public String getWo_id_text(){
        return wo_id_text ;
    }

    /**
     * 设置 [WO_ID_TEXT]
     */
    @JsonProperty("wo_id_text")
    public void setWo_id_text(String  wo_id_text){
        this.wo_id_text = wo_id_text ;
        this.wo_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [WO_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getWo_id_textDirtyFlag(){
        return wo_id_textDirtyFlag ;
    }

    /**
     * 获取 [ASSET_ID_TEXT]
     */
    @JsonProperty("asset_id_text")
    public String getAsset_id_text(){
        return asset_id_text ;
    }

    /**
     * 设置 [ASSET_ID_TEXT]
     */
    @JsonProperty("asset_id_text")
    public void setAsset_id_text(String  asset_id_text){
        this.asset_id_text = asset_id_text ;
        this.asset_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [ASSET_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getAsset_id_textDirtyFlag(){
        return asset_id_textDirtyFlag ;
    }

    /**
     * 获取 [TASK_ID_TEXT]
     */
    @JsonProperty("task_id_text")
    public String getTask_id_text(){
        return task_id_text ;
    }

    /**
     * 设置 [TASK_ID_TEXT]
     */
    @JsonProperty("task_id_text")
    public void setTask_id_text(String  task_id_text){
        this.task_id_text = task_id_text ;
        this.task_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [TASK_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getTask_id_textDirtyFlag(){
        return task_id_textDirtyFlag ;
    }

    /**
     * 获取 [COMPANY_ID_TEXT]
     */
    @JsonProperty("company_id_text")
    public String getCompany_id_text(){
        return company_id_text ;
    }

    /**
     * 设置 [COMPANY_ID_TEXT]
     */
    @JsonProperty("company_id_text")
    public void setCompany_id_text(String  company_id_text){
        this.company_id_text = company_id_text ;
        this.company_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [COMPANY_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getCompany_id_textDirtyFlag(){
        return company_id_textDirtyFlag ;
    }

    /**
     * 获取 [REQUEST_ID_TEXT]
     */
    @JsonProperty("request_id_text")
    public String getRequest_id_text(){
        return request_id_text ;
    }

    /**
     * 设置 [REQUEST_ID_TEXT]
     */
    @JsonProperty("request_id_text")
    public void setRequest_id_text(String  request_id_text){
        this.request_id_text = request_id_text ;
        this.request_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [REQUEST_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getRequest_id_textDirtyFlag(){
        return request_id_textDirtyFlag ;
    }

    /**
     * 获取 [USER_ID_TEXT]
     */
    @JsonProperty("user_id_text")
    public String getUser_id_text(){
        return user_id_text ;
    }

    /**
     * 设置 [USER_ID_TEXT]
     */
    @JsonProperty("user_id_text")
    public void setUser_id_text(String  user_id_text){
        this.user_id_text = user_id_text ;
        this.user_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [USER_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getUser_id_textDirtyFlag(){
        return user_id_textDirtyFlag ;
    }

    /**
     * 获取 [REQUEST_ID]
     */
    @JsonProperty("request_id")
    public Integer getRequest_id(){
        return request_id ;
    }

    /**
     * 设置 [REQUEST_ID]
     */
    @JsonProperty("request_id")
    public void setRequest_id(Integer  request_id){
        this.request_id = request_id ;
        this.request_idDirtyFlag = true ;
    }

    /**
     * 获取 [REQUEST_ID]脏标记
     */
    @JsonIgnore
    public boolean getRequest_idDirtyFlag(){
        return request_idDirtyFlag ;
    }

    /**
     * 获取 [TASK_ID]
     */
    @JsonProperty("task_id")
    public Integer getTask_id(){
        return task_id ;
    }

    /**
     * 设置 [TASK_ID]
     */
    @JsonProperty("task_id")
    public void setTask_id(Integer  task_id){
        this.task_id = task_id ;
        this.task_idDirtyFlag = true ;
    }

    /**
     * 获取 [TASK_ID]脏标记
     */
    @JsonIgnore
    public boolean getTask_idDirtyFlag(){
        return task_idDirtyFlag ;
    }

    /**
     * 获取 [WO_ID]
     */
    @JsonProperty("wo_id")
    public Integer getWo_id(){
        return wo_id ;
    }

    /**
     * 设置 [WO_ID]
     */
    @JsonProperty("wo_id")
    public void setWo_id(Integer  wo_id){
        this.wo_id = wo_id ;
        this.wo_idDirtyFlag = true ;
    }

    /**
     * 获取 [WO_ID]脏标记
     */
    @JsonIgnore
    public boolean getWo_idDirtyFlag(){
        return wo_idDirtyFlag ;
    }

    /**
     * 获取 [COMPANY_ID]
     */
    @JsonProperty("company_id")
    public Integer getCompany_id(){
        return company_id ;
    }

    /**
     * 设置 [COMPANY_ID]
     */
    @JsonProperty("company_id")
    public void setCompany_id(Integer  company_id){
        this.company_id = company_id ;
        this.company_idDirtyFlag = true ;
    }

    /**
     * 获取 [COMPANY_ID]脏标记
     */
    @JsonIgnore
    public boolean getCompany_idDirtyFlag(){
        return company_idDirtyFlag ;
    }

    /**
     * 获取 [USER_ID]
     */
    @JsonProperty("user_id")
    public Integer getUser_id(){
        return user_id ;
    }

    /**
     * 设置 [USER_ID]
     */
    @JsonProperty("user_id")
    public void setUser_id(Integer  user_id){
        this.user_id = user_id ;
        this.user_idDirtyFlag = true ;
    }

    /**
     * 获取 [USER_ID]脏标记
     */
    @JsonIgnore
    public boolean getUser_idDirtyFlag(){
        return user_idDirtyFlag ;
    }

    /**
     * 获取 [ASSET_ID]
     */
    @JsonProperty("asset_id")
    public Integer getAsset_id(){
        return asset_id ;
    }

    /**
     * 设置 [ASSET_ID]
     */
    @JsonProperty("asset_id")
    public void setAsset_id(Integer  asset_id){
        this.asset_id = asset_id ;
        this.asset_idDirtyFlag = true ;
    }

    /**
     * 获取 [ASSET_ID]脏标记
     */
    @JsonIgnore
    public boolean getAsset_idDirtyFlag(){
        return asset_idDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return create_uid ;
    }

    /**
     * 设置 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return create_uidDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return write_uid ;
    }

    /**
     * 设置 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return write_uidDirtyFlag ;
    }



    public Mro_order toDO() {
        Mro_order srfdomain = new Mro_order();
        if(getMessage_idsDirtyFlag())
            srfdomain.setMessage_ids(message_ids);
        if(get__last_updateDirtyFlag())
            srfdomain.set__last_update(__last_update);
        if(getMessage_partner_idsDirtyFlag())
            srfdomain.setMessage_partner_ids(message_partner_ids);
        if(getMaintenance_typeDirtyFlag())
            srfdomain.setMaintenance_type(maintenance_type);
        if(getMessage_unread_counterDirtyFlag())
            srfdomain.setMessage_unread_counter(message_unread_counter);
        if(getDocumentation_descriptionDirtyFlag())
            srfdomain.setDocumentation_description(documentation_description);
        if(getParts_moved_linesDirtyFlag())
            srfdomain.setParts_moved_lines(parts_moved_lines);
        if(getMessage_channel_idsDirtyFlag())
            srfdomain.setMessage_channel_ids(message_channel_ids);
        if(getCategory_idsDirtyFlag())
            srfdomain.setCategory_ids(category_ids);
        if(getDate_scheduledDirtyFlag())
            srfdomain.setDate_scheduled(date_scheduled);
        if(getDisplay_nameDirtyFlag())
            srfdomain.setDisplay_name(display_name);
        if(getLabor_descriptionDirtyFlag())
            srfdomain.setLabor_description(labor_description);
        if(getMessage_unreadDirtyFlag())
            srfdomain.setMessage_unread(message_unread);
        if(getMessage_follower_idsDirtyFlag())
            srfdomain.setMessage_follower_ids(message_follower_ids);
        if(getOriginDirtyFlag())
            srfdomain.setOrigin(origin);
        if(getMessage_has_error_counterDirtyFlag())
            srfdomain.setMessage_has_error_counter(message_has_error_counter);
        if(getWrite_dateDirtyFlag())
            srfdomain.setWrite_date(write_date);
        if(getDescriptionDirtyFlag())
            srfdomain.setDescription(description);
        if(getParts_ready_linesDirtyFlag())
            srfdomain.setParts_ready_lines(parts_ready_lines);
        if(getWebsite_message_idsDirtyFlag())
            srfdomain.setWebsite_message_ids(website_message_ids);
        if(getParts_move_linesDirtyFlag())
            srfdomain.setParts_move_lines(parts_move_lines);
        if(getMessage_is_followerDirtyFlag())
            srfdomain.setMessage_is_follower(message_is_follower);
        if(getMessage_needaction_counterDirtyFlag())
            srfdomain.setMessage_needaction_counter(message_needaction_counter);
        if(getOperations_descriptionDirtyFlag())
            srfdomain.setOperations_description(operations_description);
        if(getProblem_descriptionDirtyFlag())
            srfdomain.setProblem_description(problem_description);
        if(getMessage_attachment_countDirtyFlag())
            srfdomain.setMessage_attachment_count(message_attachment_count);
        if(getDate_plannedDirtyFlag())
            srfdomain.setDate_planned(date_planned);
        if(getProcurement_group_idDirtyFlag())
            srfdomain.setProcurement_group_id(procurement_group_id);
        if(getDate_executionDirtyFlag())
            srfdomain.setDate_execution(date_execution);
        if(getMessage_has_errorDirtyFlag())
            srfdomain.setMessage_has_error(message_has_error);
        if(getMessage_main_attachment_idDirtyFlag())
            srfdomain.setMessage_main_attachment_id(message_main_attachment_id);
        if(getIdDirtyFlag())
            srfdomain.setId(id);
        if(getParts_linesDirtyFlag())
            srfdomain.setParts_lines(parts_lines);
        if(getNameDirtyFlag())
            srfdomain.setName(name);
        if(getStateDirtyFlag())
            srfdomain.setState(state);
        if(getCreate_dateDirtyFlag())
            srfdomain.setCreate_date(create_date);
        if(getMessage_needactionDirtyFlag())
            srfdomain.setMessage_needaction(message_needaction);
        if(getTools_descriptionDirtyFlag())
            srfdomain.setTools_description(tools_description);
        if(getCreate_uid_textDirtyFlag())
            srfdomain.setCreate_uid_text(create_uid_text);
        if(getWrite_uid_textDirtyFlag())
            srfdomain.setWrite_uid_text(write_uid_text);
        if(getWo_id_textDirtyFlag())
            srfdomain.setWo_id_text(wo_id_text);
        if(getAsset_id_textDirtyFlag())
            srfdomain.setAsset_id_text(asset_id_text);
        if(getTask_id_textDirtyFlag())
            srfdomain.setTask_id_text(task_id_text);
        if(getCompany_id_textDirtyFlag())
            srfdomain.setCompany_id_text(company_id_text);
        if(getRequest_id_textDirtyFlag())
            srfdomain.setRequest_id_text(request_id_text);
        if(getUser_id_textDirtyFlag())
            srfdomain.setUser_id_text(user_id_text);
        if(getRequest_idDirtyFlag())
            srfdomain.setRequest_id(request_id);
        if(getTask_idDirtyFlag())
            srfdomain.setTask_id(task_id);
        if(getWo_idDirtyFlag())
            srfdomain.setWo_id(wo_id);
        if(getCompany_idDirtyFlag())
            srfdomain.setCompany_id(company_id);
        if(getUser_idDirtyFlag())
            srfdomain.setUser_id(user_id);
        if(getAsset_idDirtyFlag())
            srfdomain.setAsset_id(asset_id);
        if(getCreate_uidDirtyFlag())
            srfdomain.setCreate_uid(create_uid);
        if(getWrite_uidDirtyFlag())
            srfdomain.setWrite_uid(write_uid);

        return srfdomain;
    }

    public void fromDO(Mro_order srfdomain) {
        if(srfdomain == null )
          return ;
        if(srfdomain.getMessage_idsDirtyFlag())
            this.setMessage_ids(srfdomain.getMessage_ids());
        if(srfdomain.get__last_updateDirtyFlag())
            this.set__last_update(srfdomain.get__last_update());
        if(srfdomain.getMessage_partner_idsDirtyFlag())
            this.setMessage_partner_ids(srfdomain.getMessage_partner_ids());
        if(srfdomain.getMaintenance_typeDirtyFlag())
            this.setMaintenance_type(srfdomain.getMaintenance_type());
        if(srfdomain.getMessage_unread_counterDirtyFlag())
            this.setMessage_unread_counter(srfdomain.getMessage_unread_counter());
        if(srfdomain.getDocumentation_descriptionDirtyFlag())
            this.setDocumentation_description(srfdomain.getDocumentation_description());
        if(srfdomain.getParts_moved_linesDirtyFlag())
            this.setParts_moved_lines(srfdomain.getParts_moved_lines());
        if(srfdomain.getMessage_channel_idsDirtyFlag())
            this.setMessage_channel_ids(srfdomain.getMessage_channel_ids());
        if(srfdomain.getCategory_idsDirtyFlag())
            this.setCategory_ids(srfdomain.getCategory_ids());
        if(srfdomain.getDate_scheduledDirtyFlag())
            this.setDate_scheduled(srfdomain.getDate_scheduled());
        if(srfdomain.getDisplay_nameDirtyFlag())
            this.setDisplay_name(srfdomain.getDisplay_name());
        if(srfdomain.getLabor_descriptionDirtyFlag())
            this.setLabor_description(srfdomain.getLabor_description());
        if(srfdomain.getMessage_unreadDirtyFlag())
            this.setMessage_unread(srfdomain.getMessage_unread());
        if(srfdomain.getMessage_follower_idsDirtyFlag())
            this.setMessage_follower_ids(srfdomain.getMessage_follower_ids());
        if(srfdomain.getOriginDirtyFlag())
            this.setOrigin(srfdomain.getOrigin());
        if(srfdomain.getMessage_has_error_counterDirtyFlag())
            this.setMessage_has_error_counter(srfdomain.getMessage_has_error_counter());
        if(srfdomain.getWrite_dateDirtyFlag())
            this.setWrite_date(srfdomain.getWrite_date());
        if(srfdomain.getDescriptionDirtyFlag())
            this.setDescription(srfdomain.getDescription());
        if(srfdomain.getParts_ready_linesDirtyFlag())
            this.setParts_ready_lines(srfdomain.getParts_ready_lines());
        if(srfdomain.getWebsite_message_idsDirtyFlag())
            this.setWebsite_message_ids(srfdomain.getWebsite_message_ids());
        if(srfdomain.getParts_move_linesDirtyFlag())
            this.setParts_move_lines(srfdomain.getParts_move_lines());
        if(srfdomain.getMessage_is_followerDirtyFlag())
            this.setMessage_is_follower(srfdomain.getMessage_is_follower());
        if(srfdomain.getMessage_needaction_counterDirtyFlag())
            this.setMessage_needaction_counter(srfdomain.getMessage_needaction_counter());
        if(srfdomain.getOperations_descriptionDirtyFlag())
            this.setOperations_description(srfdomain.getOperations_description());
        if(srfdomain.getProblem_descriptionDirtyFlag())
            this.setProblem_description(srfdomain.getProblem_description());
        if(srfdomain.getMessage_attachment_countDirtyFlag())
            this.setMessage_attachment_count(srfdomain.getMessage_attachment_count());
        if(srfdomain.getDate_plannedDirtyFlag())
            this.setDate_planned(srfdomain.getDate_planned());
        if(srfdomain.getProcurement_group_idDirtyFlag())
            this.setProcurement_group_id(srfdomain.getProcurement_group_id());
        if(srfdomain.getDate_executionDirtyFlag())
            this.setDate_execution(srfdomain.getDate_execution());
        if(srfdomain.getMessage_has_errorDirtyFlag())
            this.setMessage_has_error(srfdomain.getMessage_has_error());
        if(srfdomain.getMessage_main_attachment_idDirtyFlag())
            this.setMessage_main_attachment_id(srfdomain.getMessage_main_attachment_id());
        if(srfdomain.getIdDirtyFlag())
            this.setId(srfdomain.getId());
        if(srfdomain.getParts_linesDirtyFlag())
            this.setParts_lines(srfdomain.getParts_lines());
        if(srfdomain.getNameDirtyFlag())
            this.setName(srfdomain.getName());
        if(srfdomain.getStateDirtyFlag())
            this.setState(srfdomain.getState());
        if(srfdomain.getCreate_dateDirtyFlag())
            this.setCreate_date(srfdomain.getCreate_date());
        if(srfdomain.getMessage_needactionDirtyFlag())
            this.setMessage_needaction(srfdomain.getMessage_needaction());
        if(srfdomain.getTools_descriptionDirtyFlag())
            this.setTools_description(srfdomain.getTools_description());
        if(srfdomain.getCreate_uid_textDirtyFlag())
            this.setCreate_uid_text(srfdomain.getCreate_uid_text());
        if(srfdomain.getWrite_uid_textDirtyFlag())
            this.setWrite_uid_text(srfdomain.getWrite_uid_text());
        if(srfdomain.getWo_id_textDirtyFlag())
            this.setWo_id_text(srfdomain.getWo_id_text());
        if(srfdomain.getAsset_id_textDirtyFlag())
            this.setAsset_id_text(srfdomain.getAsset_id_text());
        if(srfdomain.getTask_id_textDirtyFlag())
            this.setTask_id_text(srfdomain.getTask_id_text());
        if(srfdomain.getCompany_id_textDirtyFlag())
            this.setCompany_id_text(srfdomain.getCompany_id_text());
        if(srfdomain.getRequest_id_textDirtyFlag())
            this.setRequest_id_text(srfdomain.getRequest_id_text());
        if(srfdomain.getUser_id_textDirtyFlag())
            this.setUser_id_text(srfdomain.getUser_id_text());
        if(srfdomain.getRequest_idDirtyFlag())
            this.setRequest_id(srfdomain.getRequest_id());
        if(srfdomain.getTask_idDirtyFlag())
            this.setTask_id(srfdomain.getTask_id());
        if(srfdomain.getWo_idDirtyFlag())
            this.setWo_id(srfdomain.getWo_id());
        if(srfdomain.getCompany_idDirtyFlag())
            this.setCompany_id(srfdomain.getCompany_id());
        if(srfdomain.getUser_idDirtyFlag())
            this.setUser_id(srfdomain.getUser_id());
        if(srfdomain.getAsset_idDirtyFlag())
            this.setAsset_id(srfdomain.getAsset_id());
        if(srfdomain.getCreate_uidDirtyFlag())
            this.setCreate_uid(srfdomain.getCreate_uid());
        if(srfdomain.getWrite_uidDirtyFlag())
            this.setWrite_uid(srfdomain.getWrite_uid());

    }

    public List<Mro_orderDTO> fromDOPage(List<Mro_order> poPage)   {
        if(poPage == null)
            return null;
        List<Mro_orderDTO> dtos=new ArrayList<Mro_orderDTO>();
        for(Mro_order domain : poPage) {
            Mro_orderDTO dto = new Mro_orderDTO();
            dto.fromDO(domain);
            dtos.add(dto);
        }
        return dtos;
    }
}

