package cn.ibizlab.odoo.service.odoo_mro.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_mro.dto.Mro_request_rejectDTO;
import cn.ibizlab.odoo.core.odoo_mro.domain.Mro_request_reject;
import cn.ibizlab.odoo.core.odoo_mro.service.IMro_request_rejectService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_mro.filter.Mro_request_rejectSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Mro_request_reject" })
@RestController
@RequestMapping("")
public class Mro_request_rejectResource {

    @Autowired
    private IMro_request_rejectService mro_request_rejectService;

    public IMro_request_rejectService getMro_request_rejectService() {
        return this.mro_request_rejectService;
    }

    @ApiOperation(value = "建立数据", tags = {"Mro_request_reject" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_mro/mro_request_rejects")

    public ResponseEntity<Mro_request_rejectDTO> create(@RequestBody Mro_request_rejectDTO mro_request_rejectdto) {
        Mro_request_rejectDTO dto = new Mro_request_rejectDTO();
        Mro_request_reject domain = mro_request_rejectdto.toDO();
		mro_request_rejectService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批删除数据", tags = {"Mro_request_reject" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_mro/mro_request_rejects/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Mro_request_rejectDTO> mro_request_rejectdtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "批更新数据", tags = {"Mro_request_reject" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_mro/mro_request_rejects/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Mro_request_rejectDTO> mro_request_rejectdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "删除数据", tags = {"Mro_request_reject" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_mro/mro_request_rejects/{mro_request_reject_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("mro_request_reject_id") Integer mro_request_reject_id) {
        Mro_request_rejectDTO mro_request_rejectdto = new Mro_request_rejectDTO();
		Mro_request_reject domain = new Mro_request_reject();
		mro_request_rejectdto.setId(mro_request_reject_id);
		domain.setId(mro_request_reject_id);
        Boolean rst = mro_request_rejectService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "批建立数据", tags = {"Mro_request_reject" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_mro/mro_request_rejects/createBatch")
    public ResponseEntity<Boolean> createBatchMro_request_reject(@RequestBody List<Mro_request_rejectDTO> mro_request_rejectdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "获取数据", tags = {"Mro_request_reject" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_mro/mro_request_rejects/{mro_request_reject_id}")
    public ResponseEntity<Mro_request_rejectDTO> get(@PathVariable("mro_request_reject_id") Integer mro_request_reject_id) {
        Mro_request_rejectDTO dto = new Mro_request_rejectDTO();
        Mro_request_reject domain = mro_request_rejectService.get(mro_request_reject_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "更新数据", tags = {"Mro_request_reject" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_mro/mro_request_rejects/{mro_request_reject_id}")

    public ResponseEntity<Mro_request_rejectDTO> update(@PathVariable("mro_request_reject_id") Integer mro_request_reject_id, @RequestBody Mro_request_rejectDTO mro_request_rejectdto) {
		Mro_request_reject domain = mro_request_rejectdto.toDO();
        domain.setId(mro_request_reject_id);
		mro_request_rejectService.update(domain);
		Mro_request_rejectDTO dto = new Mro_request_rejectDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Mro_request_reject" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_mro/mro_request_rejects/fetchdefault")
	public ResponseEntity<Page<Mro_request_rejectDTO>> fetchDefault(Mro_request_rejectSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Mro_request_rejectDTO> list = new ArrayList<Mro_request_rejectDTO>();
        
        Page<Mro_request_reject> domains = mro_request_rejectService.searchDefault(context) ;
        for(Mro_request_reject mro_request_reject : domains.getContent()){
            Mro_request_rejectDTO dto = new Mro_request_rejectDTO();
            dto.fromDO(mro_request_reject);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
