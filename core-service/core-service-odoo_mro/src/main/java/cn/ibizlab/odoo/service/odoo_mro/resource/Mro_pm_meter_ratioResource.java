package cn.ibizlab.odoo.service.odoo_mro.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_mro.dto.Mro_pm_meter_ratioDTO;
import cn.ibizlab.odoo.core.odoo_mro.domain.Mro_pm_meter_ratio;
import cn.ibizlab.odoo.core.odoo_mro.service.IMro_pm_meter_ratioService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_mro.filter.Mro_pm_meter_ratioSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Mro_pm_meter_ratio" })
@RestController
@RequestMapping("")
public class Mro_pm_meter_ratioResource {

    @Autowired
    private IMro_pm_meter_ratioService mro_pm_meter_ratioService;

    public IMro_pm_meter_ratioService getMro_pm_meter_ratioService() {
        return this.mro_pm_meter_ratioService;
    }

    @ApiOperation(value = "获取数据", tags = {"Mro_pm_meter_ratio" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_mro/mro_pm_meter_ratios/{mro_pm_meter_ratio_id}")
    public ResponseEntity<Mro_pm_meter_ratioDTO> get(@PathVariable("mro_pm_meter_ratio_id") Integer mro_pm_meter_ratio_id) {
        Mro_pm_meter_ratioDTO dto = new Mro_pm_meter_ratioDTO();
        Mro_pm_meter_ratio domain = mro_pm_meter_ratioService.get(mro_pm_meter_ratio_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批删除数据", tags = {"Mro_pm_meter_ratio" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_mro/mro_pm_meter_ratios/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Mro_pm_meter_ratioDTO> mro_pm_meter_ratiodtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "更新数据", tags = {"Mro_pm_meter_ratio" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_mro/mro_pm_meter_ratios/{mro_pm_meter_ratio_id}")

    public ResponseEntity<Mro_pm_meter_ratioDTO> update(@PathVariable("mro_pm_meter_ratio_id") Integer mro_pm_meter_ratio_id, @RequestBody Mro_pm_meter_ratioDTO mro_pm_meter_ratiodto) {
		Mro_pm_meter_ratio domain = mro_pm_meter_ratiodto.toDO();
        domain.setId(mro_pm_meter_ratio_id);
		mro_pm_meter_ratioService.update(domain);
		Mro_pm_meter_ratioDTO dto = new Mro_pm_meter_ratioDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "删除数据", tags = {"Mro_pm_meter_ratio" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_mro/mro_pm_meter_ratios/{mro_pm_meter_ratio_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("mro_pm_meter_ratio_id") Integer mro_pm_meter_ratio_id) {
        Mro_pm_meter_ratioDTO mro_pm_meter_ratiodto = new Mro_pm_meter_ratioDTO();
		Mro_pm_meter_ratio domain = new Mro_pm_meter_ratio();
		mro_pm_meter_ratiodto.setId(mro_pm_meter_ratio_id);
		domain.setId(mro_pm_meter_ratio_id);
        Boolean rst = mro_pm_meter_ratioService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "建立数据", tags = {"Mro_pm_meter_ratio" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_mro/mro_pm_meter_ratios")

    public ResponseEntity<Mro_pm_meter_ratioDTO> create(@RequestBody Mro_pm_meter_ratioDTO mro_pm_meter_ratiodto) {
        Mro_pm_meter_ratioDTO dto = new Mro_pm_meter_ratioDTO();
        Mro_pm_meter_ratio domain = mro_pm_meter_ratiodto.toDO();
		mro_pm_meter_ratioService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批更新数据", tags = {"Mro_pm_meter_ratio" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_mro/mro_pm_meter_ratios/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Mro_pm_meter_ratioDTO> mro_pm_meter_ratiodtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "批建立数据", tags = {"Mro_pm_meter_ratio" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_mro/mro_pm_meter_ratios/createBatch")
    public ResponseEntity<Boolean> createBatchMro_pm_meter_ratio(@RequestBody List<Mro_pm_meter_ratioDTO> mro_pm_meter_ratiodtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Mro_pm_meter_ratio" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_mro/mro_pm_meter_ratios/fetchdefault")
	public ResponseEntity<Page<Mro_pm_meter_ratioDTO>> fetchDefault(Mro_pm_meter_ratioSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Mro_pm_meter_ratioDTO> list = new ArrayList<Mro_pm_meter_ratioDTO>();
        
        Page<Mro_pm_meter_ratio> domains = mro_pm_meter_ratioService.searchDefault(context) ;
        for(Mro_pm_meter_ratio mro_pm_meter_ratio : domains.getContent()){
            Mro_pm_meter_ratioDTO dto = new Mro_pm_meter_ratioDTO();
            dto.fromDO(mro_pm_meter_ratio);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
