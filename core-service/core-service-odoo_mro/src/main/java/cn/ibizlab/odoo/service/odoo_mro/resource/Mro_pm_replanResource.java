package cn.ibizlab.odoo.service.odoo_mro.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_mro.dto.Mro_pm_replanDTO;
import cn.ibizlab.odoo.core.odoo_mro.domain.Mro_pm_replan;
import cn.ibizlab.odoo.core.odoo_mro.service.IMro_pm_replanService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_mro.filter.Mro_pm_replanSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Mro_pm_replan" })
@RestController
@RequestMapping("")
public class Mro_pm_replanResource {

    @Autowired
    private IMro_pm_replanService mro_pm_replanService;

    public IMro_pm_replanService getMro_pm_replanService() {
        return this.mro_pm_replanService;
    }

    @ApiOperation(value = "建立数据", tags = {"Mro_pm_replan" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_mro/mro_pm_replans")

    public ResponseEntity<Mro_pm_replanDTO> create(@RequestBody Mro_pm_replanDTO mro_pm_replandto) {
        Mro_pm_replanDTO dto = new Mro_pm_replanDTO();
        Mro_pm_replan domain = mro_pm_replandto.toDO();
		mro_pm_replanService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批删除数据", tags = {"Mro_pm_replan" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_mro/mro_pm_replans/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Mro_pm_replanDTO> mro_pm_replandtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "删除数据", tags = {"Mro_pm_replan" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_mro/mro_pm_replans/{mro_pm_replan_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("mro_pm_replan_id") Integer mro_pm_replan_id) {
        Mro_pm_replanDTO mro_pm_replandto = new Mro_pm_replanDTO();
		Mro_pm_replan domain = new Mro_pm_replan();
		mro_pm_replandto.setId(mro_pm_replan_id);
		domain.setId(mro_pm_replan_id);
        Boolean rst = mro_pm_replanService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "批建立数据", tags = {"Mro_pm_replan" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_mro/mro_pm_replans/createBatch")
    public ResponseEntity<Boolean> createBatchMro_pm_replan(@RequestBody List<Mro_pm_replanDTO> mro_pm_replandtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "更新数据", tags = {"Mro_pm_replan" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_mro/mro_pm_replans/{mro_pm_replan_id}")

    public ResponseEntity<Mro_pm_replanDTO> update(@PathVariable("mro_pm_replan_id") Integer mro_pm_replan_id, @RequestBody Mro_pm_replanDTO mro_pm_replandto) {
		Mro_pm_replan domain = mro_pm_replandto.toDO();
        domain.setId(mro_pm_replan_id);
		mro_pm_replanService.update(domain);
		Mro_pm_replanDTO dto = new Mro_pm_replanDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批更新数据", tags = {"Mro_pm_replan" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_mro/mro_pm_replans/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Mro_pm_replanDTO> mro_pm_replandtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "获取数据", tags = {"Mro_pm_replan" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_mro/mro_pm_replans/{mro_pm_replan_id}")
    public ResponseEntity<Mro_pm_replanDTO> get(@PathVariable("mro_pm_replan_id") Integer mro_pm_replan_id) {
        Mro_pm_replanDTO dto = new Mro_pm_replanDTO();
        Mro_pm_replan domain = mro_pm_replanService.get(mro_pm_replan_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Mro_pm_replan" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_mro/mro_pm_replans/fetchdefault")
	public ResponseEntity<Page<Mro_pm_replanDTO>> fetchDefault(Mro_pm_replanSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Mro_pm_replanDTO> list = new ArrayList<Mro_pm_replanDTO>();
        
        Page<Mro_pm_replan> domains = mro_pm_replanService.searchDefault(context) ;
        for(Mro_pm_replan mro_pm_replan : domains.getContent()){
            Mro_pm_replanDTO dto = new Mro_pm_replanDTO();
            dto.fromDO(mro_pm_replan);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
