package cn.ibizlab.odoo.service.odoo_mro.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.core.odoo_mro.valuerule.anno.mro_pm_meter_ratio.*;
import cn.ibizlab.odoo.core.odoo_mro.domain.Mro_pm_meter_ratio;
import org.springframework.cglib.beans.BeanCopier;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 服务DTO对象[Mro_pm_meter_ratioDTO]
 */
public class Mro_pm_meter_ratioDTO implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [NAME]
     *
     */
    @Mro_pm_meter_ratioNameDefault(info = "默认规则")
    private String name;

    @JsonIgnore
    private boolean nameDirtyFlag;

    /**
     * 属性 [RATIO_TYPE]
     *
     */
    @Mro_pm_meter_ratioRatio_typeDefault(info = "默认规则")
    private String ratio_type;

    @JsonIgnore
    private boolean ratio_typeDirtyFlag;

    /**
     * 属性 [ID]
     *
     */
    @Mro_pm_meter_ratioIdDefault(info = "默认规则")
    private Integer id;

    @JsonIgnore
    private boolean idDirtyFlag;

    /**
     * 属性 [PRECISION]
     *
     */
    @Mro_pm_meter_ratioPrecisionDefault(info = "默认规则")
    private Double precision;

    @JsonIgnore
    private boolean precisionDirtyFlag;

    /**
     * 属性 [RATIO]
     *
     */
    @Mro_pm_meter_ratioRatioDefault(info = "默认规则")
    private Double ratio;

    @JsonIgnore
    private boolean ratioDirtyFlag;

    /**
     * 属性 [WRITE_DATE]
     *
     */
    @Mro_pm_meter_ratioWrite_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp write_date;

    @JsonIgnore
    private boolean write_dateDirtyFlag;

    /**
     * 属性 [CREATE_DATE]
     *
     */
    @Mro_pm_meter_ratioCreate_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp create_date;

    @JsonIgnore
    private boolean create_dateDirtyFlag;

    /**
     * 属性 [ROUNDING_TYPE]
     *
     */
    @Mro_pm_meter_ratioRounding_typeDefault(info = "默认规则")
    private String rounding_type;

    @JsonIgnore
    private boolean rounding_typeDirtyFlag;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @Mro_pm_meter_ratio__last_updateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp __last_update;

    @JsonIgnore
    private boolean __last_updateDirtyFlag;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @Mro_pm_meter_ratioDisplay_nameDefault(info = "默认规则")
    private String display_name;

    @JsonIgnore
    private boolean display_nameDirtyFlag;

    /**
     * 属性 [CREATE_UID_TEXT]
     *
     */
    @Mro_pm_meter_ratioCreate_uid_textDefault(info = "默认规则")
    private String create_uid_text;

    @JsonIgnore
    private boolean create_uid_textDirtyFlag;

    /**
     * 属性 [WRITE_UID_TEXT]
     *
     */
    @Mro_pm_meter_ratioWrite_uid_textDefault(info = "默认规则")
    private String write_uid_text;

    @JsonIgnore
    private boolean write_uid_textDirtyFlag;

    /**
     * 属性 [WRITE_UID]
     *
     */
    @Mro_pm_meter_ratioWrite_uidDefault(info = "默认规则")
    private Integer write_uid;

    @JsonIgnore
    private boolean write_uidDirtyFlag;

    /**
     * 属性 [CREATE_UID]
     *
     */
    @Mro_pm_meter_ratioCreate_uidDefault(info = "默认规则")
    private Integer create_uid;

    @JsonIgnore
    private boolean create_uidDirtyFlag;


    /**
     * 获取 [NAME]
     */
    @JsonProperty("name")
    public String getName(){
        return name ;
    }

    /**
     * 设置 [NAME]
     */
    @JsonProperty("name")
    public void setName(String  name){
        this.name = name ;
        this.nameDirtyFlag = true ;
    }

    /**
     * 获取 [NAME]脏标记
     */
    @JsonIgnore
    public boolean getNameDirtyFlag(){
        return nameDirtyFlag ;
    }

    /**
     * 获取 [RATIO_TYPE]
     */
    @JsonProperty("ratio_type")
    public String getRatio_type(){
        return ratio_type ;
    }

    /**
     * 设置 [RATIO_TYPE]
     */
    @JsonProperty("ratio_type")
    public void setRatio_type(String  ratio_type){
        this.ratio_type = ratio_type ;
        this.ratio_typeDirtyFlag = true ;
    }

    /**
     * 获取 [RATIO_TYPE]脏标记
     */
    @JsonIgnore
    public boolean getRatio_typeDirtyFlag(){
        return ratio_typeDirtyFlag ;
    }

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

    /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return idDirtyFlag ;
    }

    /**
     * 获取 [PRECISION]
     */
    @JsonProperty("precision")
    public Double getPrecision(){
        return precision ;
    }

    /**
     * 设置 [PRECISION]
     */
    @JsonProperty("precision")
    public void setPrecision(Double  precision){
        this.precision = precision ;
        this.precisionDirtyFlag = true ;
    }

    /**
     * 获取 [PRECISION]脏标记
     */
    @JsonIgnore
    public boolean getPrecisionDirtyFlag(){
        return precisionDirtyFlag ;
    }

    /**
     * 获取 [RATIO]
     */
    @JsonProperty("ratio")
    public Double getRatio(){
        return ratio ;
    }

    /**
     * 设置 [RATIO]
     */
    @JsonProperty("ratio")
    public void setRatio(Double  ratio){
        this.ratio = ratio ;
        this.ratioDirtyFlag = true ;
    }

    /**
     * 获取 [RATIO]脏标记
     */
    @JsonIgnore
    public boolean getRatioDirtyFlag(){
        return ratioDirtyFlag ;
    }

    /**
     * 获取 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return write_date ;
    }

    /**
     * 设置 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return write_dateDirtyFlag ;
    }

    /**
     * 获取 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return create_date ;
    }

    /**
     * 设置 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return create_dateDirtyFlag ;
    }

    /**
     * 获取 [ROUNDING_TYPE]
     */
    @JsonProperty("rounding_type")
    public String getRounding_type(){
        return rounding_type ;
    }

    /**
     * 设置 [ROUNDING_TYPE]
     */
    @JsonProperty("rounding_type")
    public void setRounding_type(String  rounding_type){
        this.rounding_type = rounding_type ;
        this.rounding_typeDirtyFlag = true ;
    }

    /**
     * 获取 [ROUNDING_TYPE]脏标记
     */
    @JsonIgnore
    public boolean getRounding_typeDirtyFlag(){
        return rounding_typeDirtyFlag ;
    }

    /**
     * 获取 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return __last_update ;
    }

    /**
     * 设置 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

    /**
     * 获取 [__LAST_UPDATE]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return __last_updateDirtyFlag ;
    }

    /**
     * 获取 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return display_name ;
    }

    /**
     * 设置 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

    /**
     * 获取 [DISPLAY_NAME]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return display_nameDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return create_uid_text ;
    }

    /**
     * 设置 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return create_uid_textDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return write_uid_text ;
    }

    /**
     * 设置 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return write_uid_textDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return write_uid ;
    }

    /**
     * 设置 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return write_uidDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return create_uid ;
    }

    /**
     * 设置 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return create_uidDirtyFlag ;
    }



    public Mro_pm_meter_ratio toDO() {
        Mro_pm_meter_ratio srfdomain = new Mro_pm_meter_ratio();
        if(getNameDirtyFlag())
            srfdomain.setName(name);
        if(getRatio_typeDirtyFlag())
            srfdomain.setRatio_type(ratio_type);
        if(getIdDirtyFlag())
            srfdomain.setId(id);
        if(getPrecisionDirtyFlag())
            srfdomain.setPrecision(precision);
        if(getRatioDirtyFlag())
            srfdomain.setRatio(ratio);
        if(getWrite_dateDirtyFlag())
            srfdomain.setWrite_date(write_date);
        if(getCreate_dateDirtyFlag())
            srfdomain.setCreate_date(create_date);
        if(getRounding_typeDirtyFlag())
            srfdomain.setRounding_type(rounding_type);
        if(get__last_updateDirtyFlag())
            srfdomain.set__last_update(__last_update);
        if(getDisplay_nameDirtyFlag())
            srfdomain.setDisplay_name(display_name);
        if(getCreate_uid_textDirtyFlag())
            srfdomain.setCreate_uid_text(create_uid_text);
        if(getWrite_uid_textDirtyFlag())
            srfdomain.setWrite_uid_text(write_uid_text);
        if(getWrite_uidDirtyFlag())
            srfdomain.setWrite_uid(write_uid);
        if(getCreate_uidDirtyFlag())
            srfdomain.setCreate_uid(create_uid);

        return srfdomain;
    }

    public void fromDO(Mro_pm_meter_ratio srfdomain) {
        if(srfdomain == null )
          return ;
        if(srfdomain.getNameDirtyFlag())
            this.setName(srfdomain.getName());
        if(srfdomain.getRatio_typeDirtyFlag())
            this.setRatio_type(srfdomain.getRatio_type());
        if(srfdomain.getIdDirtyFlag())
            this.setId(srfdomain.getId());
        if(srfdomain.getPrecisionDirtyFlag())
            this.setPrecision(srfdomain.getPrecision());
        if(srfdomain.getRatioDirtyFlag())
            this.setRatio(srfdomain.getRatio());
        if(srfdomain.getWrite_dateDirtyFlag())
            this.setWrite_date(srfdomain.getWrite_date());
        if(srfdomain.getCreate_dateDirtyFlag())
            this.setCreate_date(srfdomain.getCreate_date());
        if(srfdomain.getRounding_typeDirtyFlag())
            this.setRounding_type(srfdomain.getRounding_type());
        if(srfdomain.get__last_updateDirtyFlag())
            this.set__last_update(srfdomain.get__last_update());
        if(srfdomain.getDisplay_nameDirtyFlag())
            this.setDisplay_name(srfdomain.getDisplay_name());
        if(srfdomain.getCreate_uid_textDirtyFlag())
            this.setCreate_uid_text(srfdomain.getCreate_uid_text());
        if(srfdomain.getWrite_uid_textDirtyFlag())
            this.setWrite_uid_text(srfdomain.getWrite_uid_text());
        if(srfdomain.getWrite_uidDirtyFlag())
            this.setWrite_uid(srfdomain.getWrite_uid());
        if(srfdomain.getCreate_uidDirtyFlag())
            this.setCreate_uid(srfdomain.getCreate_uid());

    }

    public List<Mro_pm_meter_ratioDTO> fromDOPage(List<Mro_pm_meter_ratio> poPage)   {
        if(poPage == null)
            return null;
        List<Mro_pm_meter_ratioDTO> dtos=new ArrayList<Mro_pm_meter_ratioDTO>();
        for(Mro_pm_meter_ratio domain : poPage) {
            Mro_pm_meter_ratioDTO dto = new Mro_pm_meter_ratioDTO();
            dto.fromDO(domain);
            dtos.add(dto);
        }
        return dtos;
    }
}

