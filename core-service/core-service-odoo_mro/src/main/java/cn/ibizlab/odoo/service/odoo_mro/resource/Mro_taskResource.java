package cn.ibizlab.odoo.service.odoo_mro.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_mro.dto.Mro_taskDTO;
import cn.ibizlab.odoo.core.odoo_mro.domain.Mro_task;
import cn.ibizlab.odoo.core.odoo_mro.service.IMro_taskService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_mro.filter.Mro_taskSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Mro_task" })
@RestController
@RequestMapping("")
public class Mro_taskResource {

    @Autowired
    private IMro_taskService mro_taskService;

    public IMro_taskService getMro_taskService() {
        return this.mro_taskService;
    }

    @ApiOperation(value = "批删除数据", tags = {"Mro_task" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_mro/mro_tasks/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Mro_taskDTO> mro_taskdtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "更新数据", tags = {"Mro_task" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_mro/mro_tasks/{mro_task_id}")

    public ResponseEntity<Mro_taskDTO> update(@PathVariable("mro_task_id") Integer mro_task_id, @RequestBody Mro_taskDTO mro_taskdto) {
		Mro_task domain = mro_taskdto.toDO();
        domain.setId(mro_task_id);
		mro_taskService.update(domain);
		Mro_taskDTO dto = new Mro_taskDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取数据", tags = {"Mro_task" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_mro/mro_tasks/{mro_task_id}")
    public ResponseEntity<Mro_taskDTO> get(@PathVariable("mro_task_id") Integer mro_task_id) {
        Mro_taskDTO dto = new Mro_taskDTO();
        Mro_task domain = mro_taskService.get(mro_task_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批更新数据", tags = {"Mro_task" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_mro/mro_tasks/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Mro_taskDTO> mro_taskdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "删除数据", tags = {"Mro_task" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_mro/mro_tasks/{mro_task_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("mro_task_id") Integer mro_task_id) {
        Mro_taskDTO mro_taskdto = new Mro_taskDTO();
		Mro_task domain = new Mro_task();
		mro_taskdto.setId(mro_task_id);
		domain.setId(mro_task_id);
        Boolean rst = mro_taskService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "建立数据", tags = {"Mro_task" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_mro/mro_tasks")

    public ResponseEntity<Mro_taskDTO> create(@RequestBody Mro_taskDTO mro_taskdto) {
        Mro_taskDTO dto = new Mro_taskDTO();
        Mro_task domain = mro_taskdto.toDO();
		mro_taskService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批建立数据", tags = {"Mro_task" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_mro/mro_tasks/createBatch")
    public ResponseEntity<Boolean> createBatchMro_task(@RequestBody List<Mro_taskDTO> mro_taskdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Mro_task" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_mro/mro_tasks/fetchdefault")
	public ResponseEntity<Page<Mro_taskDTO>> fetchDefault(Mro_taskSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Mro_taskDTO> list = new ArrayList<Mro_taskDTO>();
        
        Page<Mro_task> domains = mro_taskService.searchDefault(context) ;
        for(Mro_task mro_task : domains.getContent()){
            Mro_taskDTO dto = new Mro_taskDTO();
            dto.fromDO(mro_task);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
