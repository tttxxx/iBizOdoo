package cn.ibizlab.odoo.service.odoo_mro.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.core.odoo_mro.valuerule.anno.mro_task.*;
import cn.ibizlab.odoo.core.odoo_mro.domain.Mro_task;
import org.springframework.cglib.beans.BeanCopier;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 服务DTO对象[Mro_taskDTO]
 */
public class Mro_taskDTO implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [PARTS_LINES]
     *
     */
    @Mro_taskParts_linesDefault(info = "默认规则")
    private String parts_lines;

    @JsonIgnore
    private boolean parts_linesDirtyFlag;

    /**
     * 属性 [NAME]
     *
     */
    @Mro_taskNameDefault(info = "默认规则")
    private String name;

    @JsonIgnore
    private boolean nameDirtyFlag;

    /**
     * 属性 [ID]
     *
     */
    @Mro_taskIdDefault(info = "默认规则")
    private Integer id;

    @JsonIgnore
    private boolean idDirtyFlag;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @Mro_taskDisplay_nameDefault(info = "默认规则")
    private String display_name;

    @JsonIgnore
    private boolean display_nameDirtyFlag;

    /**
     * 属性 [ACTIVE]
     *
     */
    @Mro_taskActiveDefault(info = "默认规则")
    private String active;

    @JsonIgnore
    private boolean activeDirtyFlag;

    /**
     * 属性 [TOOLS_DESCRIPTION]
     *
     */
    @Mro_taskTools_descriptionDefault(info = "默认规则")
    private String tools_description;

    @JsonIgnore
    private boolean tools_descriptionDirtyFlag;

    /**
     * 属性 [LABOR_DESCRIPTION]
     *
     */
    @Mro_taskLabor_descriptionDefault(info = "默认规则")
    private String labor_description;

    @JsonIgnore
    private boolean labor_descriptionDirtyFlag;

    /**
     * 属性 [WRITE_DATE]
     *
     */
    @Mro_taskWrite_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp write_date;

    @JsonIgnore
    private boolean write_dateDirtyFlag;

    /**
     * 属性 [MAINTENANCE_TYPE]
     *
     */
    @Mro_taskMaintenance_typeDefault(info = "默认规则")
    private String maintenance_type;

    @JsonIgnore
    private boolean maintenance_typeDirtyFlag;

    /**
     * 属性 [OPERATIONS_DESCRIPTION]
     *
     */
    @Mro_taskOperations_descriptionDefault(info = "默认规则")
    private String operations_description;

    @JsonIgnore
    private boolean operations_descriptionDirtyFlag;

    /**
     * 属性 [DOCUMENTATION_DESCRIPTION]
     *
     */
    @Mro_taskDocumentation_descriptionDefault(info = "默认规则")
    private String documentation_description;

    @JsonIgnore
    private boolean documentation_descriptionDirtyFlag;

    /**
     * 属性 [CREATE_DATE]
     *
     */
    @Mro_taskCreate_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp create_date;

    @JsonIgnore
    private boolean create_dateDirtyFlag;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @Mro_task__last_updateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp __last_update;

    @JsonIgnore
    private boolean __last_updateDirtyFlag;

    /**
     * 属性 [WRITE_UID_TEXT]
     *
     */
    @Mro_taskWrite_uid_textDefault(info = "默认规则")
    private String write_uid_text;

    @JsonIgnore
    private boolean write_uid_textDirtyFlag;

    /**
     * 属性 [CATEGORY_ID_TEXT]
     *
     */
    @Mro_taskCategory_id_textDefault(info = "默认规则")
    private String category_id_text;

    @JsonIgnore
    private boolean category_id_textDirtyFlag;

    /**
     * 属性 [CREATE_UID_TEXT]
     *
     */
    @Mro_taskCreate_uid_textDefault(info = "默认规则")
    private String create_uid_text;

    @JsonIgnore
    private boolean create_uid_textDirtyFlag;

    /**
     * 属性 [CATEGORY_ID]
     *
     */
    @Mro_taskCategory_idDefault(info = "默认规则")
    private Integer category_id;

    @JsonIgnore
    private boolean category_idDirtyFlag;

    /**
     * 属性 [CREATE_UID]
     *
     */
    @Mro_taskCreate_uidDefault(info = "默认规则")
    private Integer create_uid;

    @JsonIgnore
    private boolean create_uidDirtyFlag;

    /**
     * 属性 [WRITE_UID]
     *
     */
    @Mro_taskWrite_uidDefault(info = "默认规则")
    private Integer write_uid;

    @JsonIgnore
    private boolean write_uidDirtyFlag;


    /**
     * 获取 [PARTS_LINES]
     */
    @JsonProperty("parts_lines")
    public String getParts_lines(){
        return parts_lines ;
    }

    /**
     * 设置 [PARTS_LINES]
     */
    @JsonProperty("parts_lines")
    public void setParts_lines(String  parts_lines){
        this.parts_lines = parts_lines ;
        this.parts_linesDirtyFlag = true ;
    }

    /**
     * 获取 [PARTS_LINES]脏标记
     */
    @JsonIgnore
    public boolean getParts_linesDirtyFlag(){
        return parts_linesDirtyFlag ;
    }

    /**
     * 获取 [NAME]
     */
    @JsonProperty("name")
    public String getName(){
        return name ;
    }

    /**
     * 设置 [NAME]
     */
    @JsonProperty("name")
    public void setName(String  name){
        this.name = name ;
        this.nameDirtyFlag = true ;
    }

    /**
     * 获取 [NAME]脏标记
     */
    @JsonIgnore
    public boolean getNameDirtyFlag(){
        return nameDirtyFlag ;
    }

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

    /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return idDirtyFlag ;
    }

    /**
     * 获取 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return display_name ;
    }

    /**
     * 设置 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

    /**
     * 获取 [DISPLAY_NAME]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return display_nameDirtyFlag ;
    }

    /**
     * 获取 [ACTIVE]
     */
    @JsonProperty("active")
    public String getActive(){
        return active ;
    }

    /**
     * 设置 [ACTIVE]
     */
    @JsonProperty("active")
    public void setActive(String  active){
        this.active = active ;
        this.activeDirtyFlag = true ;
    }

    /**
     * 获取 [ACTIVE]脏标记
     */
    @JsonIgnore
    public boolean getActiveDirtyFlag(){
        return activeDirtyFlag ;
    }

    /**
     * 获取 [TOOLS_DESCRIPTION]
     */
    @JsonProperty("tools_description")
    public String getTools_description(){
        return tools_description ;
    }

    /**
     * 设置 [TOOLS_DESCRIPTION]
     */
    @JsonProperty("tools_description")
    public void setTools_description(String  tools_description){
        this.tools_description = tools_description ;
        this.tools_descriptionDirtyFlag = true ;
    }

    /**
     * 获取 [TOOLS_DESCRIPTION]脏标记
     */
    @JsonIgnore
    public boolean getTools_descriptionDirtyFlag(){
        return tools_descriptionDirtyFlag ;
    }

    /**
     * 获取 [LABOR_DESCRIPTION]
     */
    @JsonProperty("labor_description")
    public String getLabor_description(){
        return labor_description ;
    }

    /**
     * 设置 [LABOR_DESCRIPTION]
     */
    @JsonProperty("labor_description")
    public void setLabor_description(String  labor_description){
        this.labor_description = labor_description ;
        this.labor_descriptionDirtyFlag = true ;
    }

    /**
     * 获取 [LABOR_DESCRIPTION]脏标记
     */
    @JsonIgnore
    public boolean getLabor_descriptionDirtyFlag(){
        return labor_descriptionDirtyFlag ;
    }

    /**
     * 获取 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return write_date ;
    }

    /**
     * 设置 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return write_dateDirtyFlag ;
    }

    /**
     * 获取 [MAINTENANCE_TYPE]
     */
    @JsonProperty("maintenance_type")
    public String getMaintenance_type(){
        return maintenance_type ;
    }

    /**
     * 设置 [MAINTENANCE_TYPE]
     */
    @JsonProperty("maintenance_type")
    public void setMaintenance_type(String  maintenance_type){
        this.maintenance_type = maintenance_type ;
        this.maintenance_typeDirtyFlag = true ;
    }

    /**
     * 获取 [MAINTENANCE_TYPE]脏标记
     */
    @JsonIgnore
    public boolean getMaintenance_typeDirtyFlag(){
        return maintenance_typeDirtyFlag ;
    }

    /**
     * 获取 [OPERATIONS_DESCRIPTION]
     */
    @JsonProperty("operations_description")
    public String getOperations_description(){
        return operations_description ;
    }

    /**
     * 设置 [OPERATIONS_DESCRIPTION]
     */
    @JsonProperty("operations_description")
    public void setOperations_description(String  operations_description){
        this.operations_description = operations_description ;
        this.operations_descriptionDirtyFlag = true ;
    }

    /**
     * 获取 [OPERATIONS_DESCRIPTION]脏标记
     */
    @JsonIgnore
    public boolean getOperations_descriptionDirtyFlag(){
        return operations_descriptionDirtyFlag ;
    }

    /**
     * 获取 [DOCUMENTATION_DESCRIPTION]
     */
    @JsonProperty("documentation_description")
    public String getDocumentation_description(){
        return documentation_description ;
    }

    /**
     * 设置 [DOCUMENTATION_DESCRIPTION]
     */
    @JsonProperty("documentation_description")
    public void setDocumentation_description(String  documentation_description){
        this.documentation_description = documentation_description ;
        this.documentation_descriptionDirtyFlag = true ;
    }

    /**
     * 获取 [DOCUMENTATION_DESCRIPTION]脏标记
     */
    @JsonIgnore
    public boolean getDocumentation_descriptionDirtyFlag(){
        return documentation_descriptionDirtyFlag ;
    }

    /**
     * 获取 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return create_date ;
    }

    /**
     * 设置 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return create_dateDirtyFlag ;
    }

    /**
     * 获取 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return __last_update ;
    }

    /**
     * 设置 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

    /**
     * 获取 [__LAST_UPDATE]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return __last_updateDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return write_uid_text ;
    }

    /**
     * 设置 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return write_uid_textDirtyFlag ;
    }

    /**
     * 获取 [CATEGORY_ID_TEXT]
     */
    @JsonProperty("category_id_text")
    public String getCategory_id_text(){
        return category_id_text ;
    }

    /**
     * 设置 [CATEGORY_ID_TEXT]
     */
    @JsonProperty("category_id_text")
    public void setCategory_id_text(String  category_id_text){
        this.category_id_text = category_id_text ;
        this.category_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [CATEGORY_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getCategory_id_textDirtyFlag(){
        return category_id_textDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return create_uid_text ;
    }

    /**
     * 设置 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return create_uid_textDirtyFlag ;
    }

    /**
     * 获取 [CATEGORY_ID]
     */
    @JsonProperty("category_id")
    public Integer getCategory_id(){
        return category_id ;
    }

    /**
     * 设置 [CATEGORY_ID]
     */
    @JsonProperty("category_id")
    public void setCategory_id(Integer  category_id){
        this.category_id = category_id ;
        this.category_idDirtyFlag = true ;
    }

    /**
     * 获取 [CATEGORY_ID]脏标记
     */
    @JsonIgnore
    public boolean getCategory_idDirtyFlag(){
        return category_idDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return create_uid ;
    }

    /**
     * 设置 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return create_uidDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return write_uid ;
    }

    /**
     * 设置 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return write_uidDirtyFlag ;
    }



    public Mro_task toDO() {
        Mro_task srfdomain = new Mro_task();
        if(getParts_linesDirtyFlag())
            srfdomain.setParts_lines(parts_lines);
        if(getNameDirtyFlag())
            srfdomain.setName(name);
        if(getIdDirtyFlag())
            srfdomain.setId(id);
        if(getDisplay_nameDirtyFlag())
            srfdomain.setDisplay_name(display_name);
        if(getActiveDirtyFlag())
            srfdomain.setActive(active);
        if(getTools_descriptionDirtyFlag())
            srfdomain.setTools_description(tools_description);
        if(getLabor_descriptionDirtyFlag())
            srfdomain.setLabor_description(labor_description);
        if(getWrite_dateDirtyFlag())
            srfdomain.setWrite_date(write_date);
        if(getMaintenance_typeDirtyFlag())
            srfdomain.setMaintenance_type(maintenance_type);
        if(getOperations_descriptionDirtyFlag())
            srfdomain.setOperations_description(operations_description);
        if(getDocumentation_descriptionDirtyFlag())
            srfdomain.setDocumentation_description(documentation_description);
        if(getCreate_dateDirtyFlag())
            srfdomain.setCreate_date(create_date);
        if(get__last_updateDirtyFlag())
            srfdomain.set__last_update(__last_update);
        if(getWrite_uid_textDirtyFlag())
            srfdomain.setWrite_uid_text(write_uid_text);
        if(getCategory_id_textDirtyFlag())
            srfdomain.setCategory_id_text(category_id_text);
        if(getCreate_uid_textDirtyFlag())
            srfdomain.setCreate_uid_text(create_uid_text);
        if(getCategory_idDirtyFlag())
            srfdomain.setCategory_id(category_id);
        if(getCreate_uidDirtyFlag())
            srfdomain.setCreate_uid(create_uid);
        if(getWrite_uidDirtyFlag())
            srfdomain.setWrite_uid(write_uid);

        return srfdomain;
    }

    public void fromDO(Mro_task srfdomain) {
        if(srfdomain == null )
          return ;
        if(srfdomain.getParts_linesDirtyFlag())
            this.setParts_lines(srfdomain.getParts_lines());
        if(srfdomain.getNameDirtyFlag())
            this.setName(srfdomain.getName());
        if(srfdomain.getIdDirtyFlag())
            this.setId(srfdomain.getId());
        if(srfdomain.getDisplay_nameDirtyFlag())
            this.setDisplay_name(srfdomain.getDisplay_name());
        if(srfdomain.getActiveDirtyFlag())
            this.setActive(srfdomain.getActive());
        if(srfdomain.getTools_descriptionDirtyFlag())
            this.setTools_description(srfdomain.getTools_description());
        if(srfdomain.getLabor_descriptionDirtyFlag())
            this.setLabor_description(srfdomain.getLabor_description());
        if(srfdomain.getWrite_dateDirtyFlag())
            this.setWrite_date(srfdomain.getWrite_date());
        if(srfdomain.getMaintenance_typeDirtyFlag())
            this.setMaintenance_type(srfdomain.getMaintenance_type());
        if(srfdomain.getOperations_descriptionDirtyFlag())
            this.setOperations_description(srfdomain.getOperations_description());
        if(srfdomain.getDocumentation_descriptionDirtyFlag())
            this.setDocumentation_description(srfdomain.getDocumentation_description());
        if(srfdomain.getCreate_dateDirtyFlag())
            this.setCreate_date(srfdomain.getCreate_date());
        if(srfdomain.get__last_updateDirtyFlag())
            this.set__last_update(srfdomain.get__last_update());
        if(srfdomain.getWrite_uid_textDirtyFlag())
            this.setWrite_uid_text(srfdomain.getWrite_uid_text());
        if(srfdomain.getCategory_id_textDirtyFlag())
            this.setCategory_id_text(srfdomain.getCategory_id_text());
        if(srfdomain.getCreate_uid_textDirtyFlag())
            this.setCreate_uid_text(srfdomain.getCreate_uid_text());
        if(srfdomain.getCategory_idDirtyFlag())
            this.setCategory_id(srfdomain.getCategory_id());
        if(srfdomain.getCreate_uidDirtyFlag())
            this.setCreate_uid(srfdomain.getCreate_uid());
        if(srfdomain.getWrite_uidDirtyFlag())
            this.setWrite_uid(srfdomain.getWrite_uid());

    }

    public List<Mro_taskDTO> fromDOPage(List<Mro_task> poPage)   {
        if(poPage == null)
            return null;
        List<Mro_taskDTO> dtos=new ArrayList<Mro_taskDTO>();
        for(Mro_task domain : poPage) {
            Mro_taskDTO dto = new Mro_taskDTO();
            dto.fromDO(domain);
            dtos.add(dto);
        }
        return dtos;
    }
}

