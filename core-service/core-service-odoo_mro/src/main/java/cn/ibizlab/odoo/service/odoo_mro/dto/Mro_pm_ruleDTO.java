package cn.ibizlab.odoo.service.odoo_mro.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.core.odoo_mro.valuerule.anno.mro_pm_rule.*;
import cn.ibizlab.odoo.core.odoo_mro.domain.Mro_pm_rule;
import org.springframework.cglib.beans.BeanCopier;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 服务DTO对象[Mro_pm_ruleDTO]
 */
public class Mro_pm_ruleDTO implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [WRITE_DATE]
     *
     */
    @Mro_pm_ruleWrite_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp write_date;

    @JsonIgnore
    private boolean write_dateDirtyFlag;

    /**
     * 属性 [HORIZON]
     *
     */
    @Mro_pm_ruleHorizonDefault(info = "默认规则")
    private Double horizon;

    @JsonIgnore
    private boolean horizonDirtyFlag;

    /**
     * 属性 [PM_RULES_LINE_IDS]
     *
     */
    @Mro_pm_rulePm_rules_line_idsDefault(info = "默认规则")
    private String pm_rules_line_ids;

    @JsonIgnore
    private boolean pm_rules_line_idsDirtyFlag;

    /**
     * 属性 [NAME]
     *
     */
    @Mro_pm_ruleNameDefault(info = "默认规则")
    private String name;

    @JsonIgnore
    private boolean nameDirtyFlag;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @Mro_pm_rule__last_updateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp __last_update;

    @JsonIgnore
    private boolean __last_updateDirtyFlag;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @Mro_pm_ruleDisplay_nameDefault(info = "默认规则")
    private String display_name;

    @JsonIgnore
    private boolean display_nameDirtyFlag;

    /**
     * 属性 [ID]
     *
     */
    @Mro_pm_ruleIdDefault(info = "默认规则")
    private Integer id;

    @JsonIgnore
    private boolean idDirtyFlag;

    /**
     * 属性 [CREATE_DATE]
     *
     */
    @Mro_pm_ruleCreate_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp create_date;

    @JsonIgnore
    private boolean create_dateDirtyFlag;

    /**
     * 属性 [ACTIVE]
     *
     */
    @Mro_pm_ruleActiveDefault(info = "默认规则")
    private String active;

    @JsonIgnore
    private boolean activeDirtyFlag;

    /**
     * 属性 [CREATE_UID_TEXT]
     *
     */
    @Mro_pm_ruleCreate_uid_textDefault(info = "默认规则")
    private String create_uid_text;

    @JsonIgnore
    private boolean create_uid_textDirtyFlag;

    /**
     * 属性 [PARAMETER_ID_TEXT]
     *
     */
    @Mro_pm_ruleParameter_id_textDefault(info = "默认规则")
    private String parameter_id_text;

    @JsonIgnore
    private boolean parameter_id_textDirtyFlag;

    /**
     * 属性 [CATEGORY_ID_TEXT]
     *
     */
    @Mro_pm_ruleCategory_id_textDefault(info = "默认规则")
    private String category_id_text;

    @JsonIgnore
    private boolean category_id_textDirtyFlag;

    /**
     * 属性 [WRITE_UID_TEXT]
     *
     */
    @Mro_pm_ruleWrite_uid_textDefault(info = "默认规则")
    private String write_uid_text;

    @JsonIgnore
    private boolean write_uid_textDirtyFlag;

    /**
     * 属性 [PARAMETER_UOM]
     *
     */
    @Mro_pm_ruleParameter_uomDefault(info = "默认规则")
    private Integer parameter_uom;

    @JsonIgnore
    private boolean parameter_uomDirtyFlag;

    /**
     * 属性 [PARAMETER_ID]
     *
     */
    @Mro_pm_ruleParameter_idDefault(info = "默认规则")
    private Integer parameter_id;

    @JsonIgnore
    private boolean parameter_idDirtyFlag;

    /**
     * 属性 [CREATE_UID]
     *
     */
    @Mro_pm_ruleCreate_uidDefault(info = "默认规则")
    private Integer create_uid;

    @JsonIgnore
    private boolean create_uidDirtyFlag;

    /**
     * 属性 [CATEGORY_ID]
     *
     */
    @Mro_pm_ruleCategory_idDefault(info = "默认规则")
    private Integer category_id;

    @JsonIgnore
    private boolean category_idDirtyFlag;

    /**
     * 属性 [WRITE_UID]
     *
     */
    @Mro_pm_ruleWrite_uidDefault(info = "默认规则")
    private Integer write_uid;

    @JsonIgnore
    private boolean write_uidDirtyFlag;


    /**
     * 获取 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return write_date ;
    }

    /**
     * 设置 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return write_dateDirtyFlag ;
    }

    /**
     * 获取 [HORIZON]
     */
    @JsonProperty("horizon")
    public Double getHorizon(){
        return horizon ;
    }

    /**
     * 设置 [HORIZON]
     */
    @JsonProperty("horizon")
    public void setHorizon(Double  horizon){
        this.horizon = horizon ;
        this.horizonDirtyFlag = true ;
    }

    /**
     * 获取 [HORIZON]脏标记
     */
    @JsonIgnore
    public boolean getHorizonDirtyFlag(){
        return horizonDirtyFlag ;
    }

    /**
     * 获取 [PM_RULES_LINE_IDS]
     */
    @JsonProperty("pm_rules_line_ids")
    public String getPm_rules_line_ids(){
        return pm_rules_line_ids ;
    }

    /**
     * 设置 [PM_RULES_LINE_IDS]
     */
    @JsonProperty("pm_rules_line_ids")
    public void setPm_rules_line_ids(String  pm_rules_line_ids){
        this.pm_rules_line_ids = pm_rules_line_ids ;
        this.pm_rules_line_idsDirtyFlag = true ;
    }

    /**
     * 获取 [PM_RULES_LINE_IDS]脏标记
     */
    @JsonIgnore
    public boolean getPm_rules_line_idsDirtyFlag(){
        return pm_rules_line_idsDirtyFlag ;
    }

    /**
     * 获取 [NAME]
     */
    @JsonProperty("name")
    public String getName(){
        return name ;
    }

    /**
     * 设置 [NAME]
     */
    @JsonProperty("name")
    public void setName(String  name){
        this.name = name ;
        this.nameDirtyFlag = true ;
    }

    /**
     * 获取 [NAME]脏标记
     */
    @JsonIgnore
    public boolean getNameDirtyFlag(){
        return nameDirtyFlag ;
    }

    /**
     * 获取 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return __last_update ;
    }

    /**
     * 设置 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

    /**
     * 获取 [__LAST_UPDATE]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return __last_updateDirtyFlag ;
    }

    /**
     * 获取 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return display_name ;
    }

    /**
     * 设置 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

    /**
     * 获取 [DISPLAY_NAME]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return display_nameDirtyFlag ;
    }

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

    /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return idDirtyFlag ;
    }

    /**
     * 获取 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return create_date ;
    }

    /**
     * 设置 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return create_dateDirtyFlag ;
    }

    /**
     * 获取 [ACTIVE]
     */
    @JsonProperty("active")
    public String getActive(){
        return active ;
    }

    /**
     * 设置 [ACTIVE]
     */
    @JsonProperty("active")
    public void setActive(String  active){
        this.active = active ;
        this.activeDirtyFlag = true ;
    }

    /**
     * 获取 [ACTIVE]脏标记
     */
    @JsonIgnore
    public boolean getActiveDirtyFlag(){
        return activeDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return create_uid_text ;
    }

    /**
     * 设置 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return create_uid_textDirtyFlag ;
    }

    /**
     * 获取 [PARAMETER_ID_TEXT]
     */
    @JsonProperty("parameter_id_text")
    public String getParameter_id_text(){
        return parameter_id_text ;
    }

    /**
     * 设置 [PARAMETER_ID_TEXT]
     */
    @JsonProperty("parameter_id_text")
    public void setParameter_id_text(String  parameter_id_text){
        this.parameter_id_text = parameter_id_text ;
        this.parameter_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [PARAMETER_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getParameter_id_textDirtyFlag(){
        return parameter_id_textDirtyFlag ;
    }

    /**
     * 获取 [CATEGORY_ID_TEXT]
     */
    @JsonProperty("category_id_text")
    public String getCategory_id_text(){
        return category_id_text ;
    }

    /**
     * 设置 [CATEGORY_ID_TEXT]
     */
    @JsonProperty("category_id_text")
    public void setCategory_id_text(String  category_id_text){
        this.category_id_text = category_id_text ;
        this.category_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [CATEGORY_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getCategory_id_textDirtyFlag(){
        return category_id_textDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return write_uid_text ;
    }

    /**
     * 设置 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return write_uid_textDirtyFlag ;
    }

    /**
     * 获取 [PARAMETER_UOM]
     */
    @JsonProperty("parameter_uom")
    public Integer getParameter_uom(){
        return parameter_uom ;
    }

    /**
     * 设置 [PARAMETER_UOM]
     */
    @JsonProperty("parameter_uom")
    public void setParameter_uom(Integer  parameter_uom){
        this.parameter_uom = parameter_uom ;
        this.parameter_uomDirtyFlag = true ;
    }

    /**
     * 获取 [PARAMETER_UOM]脏标记
     */
    @JsonIgnore
    public boolean getParameter_uomDirtyFlag(){
        return parameter_uomDirtyFlag ;
    }

    /**
     * 获取 [PARAMETER_ID]
     */
    @JsonProperty("parameter_id")
    public Integer getParameter_id(){
        return parameter_id ;
    }

    /**
     * 设置 [PARAMETER_ID]
     */
    @JsonProperty("parameter_id")
    public void setParameter_id(Integer  parameter_id){
        this.parameter_id = parameter_id ;
        this.parameter_idDirtyFlag = true ;
    }

    /**
     * 获取 [PARAMETER_ID]脏标记
     */
    @JsonIgnore
    public boolean getParameter_idDirtyFlag(){
        return parameter_idDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return create_uid ;
    }

    /**
     * 设置 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return create_uidDirtyFlag ;
    }

    /**
     * 获取 [CATEGORY_ID]
     */
    @JsonProperty("category_id")
    public Integer getCategory_id(){
        return category_id ;
    }

    /**
     * 设置 [CATEGORY_ID]
     */
    @JsonProperty("category_id")
    public void setCategory_id(Integer  category_id){
        this.category_id = category_id ;
        this.category_idDirtyFlag = true ;
    }

    /**
     * 获取 [CATEGORY_ID]脏标记
     */
    @JsonIgnore
    public boolean getCategory_idDirtyFlag(){
        return category_idDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return write_uid ;
    }

    /**
     * 设置 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return write_uidDirtyFlag ;
    }



    public Mro_pm_rule toDO() {
        Mro_pm_rule srfdomain = new Mro_pm_rule();
        if(getWrite_dateDirtyFlag())
            srfdomain.setWrite_date(write_date);
        if(getHorizonDirtyFlag())
            srfdomain.setHorizon(horizon);
        if(getPm_rules_line_idsDirtyFlag())
            srfdomain.setPm_rules_line_ids(pm_rules_line_ids);
        if(getNameDirtyFlag())
            srfdomain.setName(name);
        if(get__last_updateDirtyFlag())
            srfdomain.set__last_update(__last_update);
        if(getDisplay_nameDirtyFlag())
            srfdomain.setDisplay_name(display_name);
        if(getIdDirtyFlag())
            srfdomain.setId(id);
        if(getCreate_dateDirtyFlag())
            srfdomain.setCreate_date(create_date);
        if(getActiveDirtyFlag())
            srfdomain.setActive(active);
        if(getCreate_uid_textDirtyFlag())
            srfdomain.setCreate_uid_text(create_uid_text);
        if(getParameter_id_textDirtyFlag())
            srfdomain.setParameter_id_text(parameter_id_text);
        if(getCategory_id_textDirtyFlag())
            srfdomain.setCategory_id_text(category_id_text);
        if(getWrite_uid_textDirtyFlag())
            srfdomain.setWrite_uid_text(write_uid_text);
        if(getParameter_uomDirtyFlag())
            srfdomain.setParameter_uom(parameter_uom);
        if(getParameter_idDirtyFlag())
            srfdomain.setParameter_id(parameter_id);
        if(getCreate_uidDirtyFlag())
            srfdomain.setCreate_uid(create_uid);
        if(getCategory_idDirtyFlag())
            srfdomain.setCategory_id(category_id);
        if(getWrite_uidDirtyFlag())
            srfdomain.setWrite_uid(write_uid);

        return srfdomain;
    }

    public void fromDO(Mro_pm_rule srfdomain) {
        if(srfdomain == null )
          return ;
        if(srfdomain.getWrite_dateDirtyFlag())
            this.setWrite_date(srfdomain.getWrite_date());
        if(srfdomain.getHorizonDirtyFlag())
            this.setHorizon(srfdomain.getHorizon());
        if(srfdomain.getPm_rules_line_idsDirtyFlag())
            this.setPm_rules_line_ids(srfdomain.getPm_rules_line_ids());
        if(srfdomain.getNameDirtyFlag())
            this.setName(srfdomain.getName());
        if(srfdomain.get__last_updateDirtyFlag())
            this.set__last_update(srfdomain.get__last_update());
        if(srfdomain.getDisplay_nameDirtyFlag())
            this.setDisplay_name(srfdomain.getDisplay_name());
        if(srfdomain.getIdDirtyFlag())
            this.setId(srfdomain.getId());
        if(srfdomain.getCreate_dateDirtyFlag())
            this.setCreate_date(srfdomain.getCreate_date());
        if(srfdomain.getActiveDirtyFlag())
            this.setActive(srfdomain.getActive());
        if(srfdomain.getCreate_uid_textDirtyFlag())
            this.setCreate_uid_text(srfdomain.getCreate_uid_text());
        if(srfdomain.getParameter_id_textDirtyFlag())
            this.setParameter_id_text(srfdomain.getParameter_id_text());
        if(srfdomain.getCategory_id_textDirtyFlag())
            this.setCategory_id_text(srfdomain.getCategory_id_text());
        if(srfdomain.getWrite_uid_textDirtyFlag())
            this.setWrite_uid_text(srfdomain.getWrite_uid_text());
        if(srfdomain.getParameter_uomDirtyFlag())
            this.setParameter_uom(srfdomain.getParameter_uom());
        if(srfdomain.getParameter_idDirtyFlag())
            this.setParameter_id(srfdomain.getParameter_id());
        if(srfdomain.getCreate_uidDirtyFlag())
            this.setCreate_uid(srfdomain.getCreate_uid());
        if(srfdomain.getCategory_idDirtyFlag())
            this.setCategory_id(srfdomain.getCategory_id());
        if(srfdomain.getWrite_uidDirtyFlag())
            this.setWrite_uid(srfdomain.getWrite_uid());

    }

    public List<Mro_pm_ruleDTO> fromDOPage(List<Mro_pm_rule> poPage)   {
        if(poPage == null)
            return null;
        List<Mro_pm_ruleDTO> dtos=new ArrayList<Mro_pm_ruleDTO>();
        for(Mro_pm_rule domain : poPage) {
            Mro_pm_ruleDTO dto = new Mro_pm_ruleDTO();
            dto.fromDO(domain);
            dtos.add(dto);
        }
        return dtos;
    }
}

