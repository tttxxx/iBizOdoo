package cn.ibizlab.odoo.service.odoo_mro.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_mro.dto.Mro_requestDTO;
import cn.ibizlab.odoo.core.odoo_mro.domain.Mro_request;
import cn.ibizlab.odoo.core.odoo_mro.service.IMro_requestService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_mro.filter.Mro_requestSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Mro_request" })
@RestController
@RequestMapping("")
public class Mro_requestResource {

    @Autowired
    private IMro_requestService mro_requestService;

    public IMro_requestService getMro_requestService() {
        return this.mro_requestService;
    }

    @ApiOperation(value = "批更新数据", tags = {"Mro_request" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_mro/mro_requests/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Mro_requestDTO> mro_requestdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "更新数据", tags = {"Mro_request" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_mro/mro_requests/{mro_request_id}")

    public ResponseEntity<Mro_requestDTO> update(@PathVariable("mro_request_id") Integer mro_request_id, @RequestBody Mro_requestDTO mro_requestdto) {
		Mro_request domain = mro_requestdto.toDO();
        domain.setId(mro_request_id);
		mro_requestService.update(domain);
		Mro_requestDTO dto = new Mro_requestDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "删除数据", tags = {"Mro_request" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_mro/mro_requests/{mro_request_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("mro_request_id") Integer mro_request_id) {
        Mro_requestDTO mro_requestdto = new Mro_requestDTO();
		Mro_request domain = new Mro_request();
		mro_requestdto.setId(mro_request_id);
		domain.setId(mro_request_id);
        Boolean rst = mro_requestService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "获取数据", tags = {"Mro_request" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_mro/mro_requests/{mro_request_id}")
    public ResponseEntity<Mro_requestDTO> get(@PathVariable("mro_request_id") Integer mro_request_id) {
        Mro_requestDTO dto = new Mro_requestDTO();
        Mro_request domain = mro_requestService.get(mro_request_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "建立数据", tags = {"Mro_request" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_mro/mro_requests")

    public ResponseEntity<Mro_requestDTO> create(@RequestBody Mro_requestDTO mro_requestdto) {
        Mro_requestDTO dto = new Mro_requestDTO();
        Mro_request domain = mro_requestdto.toDO();
		mro_requestService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批建立数据", tags = {"Mro_request" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_mro/mro_requests/createBatch")
    public ResponseEntity<Boolean> createBatchMro_request(@RequestBody List<Mro_requestDTO> mro_requestdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "批删除数据", tags = {"Mro_request" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_mro/mro_requests/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Mro_requestDTO> mro_requestdtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Mro_request" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_mro/mro_requests/fetchdefault")
	public ResponseEntity<Page<Mro_requestDTO>> fetchDefault(Mro_requestSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Mro_requestDTO> list = new ArrayList<Mro_requestDTO>();
        
        Page<Mro_request> domains = mro_requestService.searchDefault(context) ;
        for(Mro_request mro_request : domains.getContent()){
            Mro_requestDTO dto = new Mro_requestDTO();
            dto.fromDO(mro_request);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
