package cn.ibizlab.odoo.service.odoo_repair.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_repair.dto.Repair_order_make_invoiceDTO;
import cn.ibizlab.odoo.core.odoo_repair.domain.Repair_order_make_invoice;
import cn.ibizlab.odoo.core.odoo_repair.service.IRepair_order_make_invoiceService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_repair.filter.Repair_order_make_invoiceSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Repair_order_make_invoice" })
@RestController
@RequestMapping("")
public class Repair_order_make_invoiceResource {

    @Autowired
    private IRepair_order_make_invoiceService repair_order_make_invoiceService;

    public IRepair_order_make_invoiceService getRepair_order_make_invoiceService() {
        return this.repair_order_make_invoiceService;
    }

    @ApiOperation(value = "更新数据", tags = {"Repair_order_make_invoice" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_repair/repair_order_make_invoices/{repair_order_make_invoice_id}")

    public ResponseEntity<Repair_order_make_invoiceDTO> update(@PathVariable("repair_order_make_invoice_id") Integer repair_order_make_invoice_id, @RequestBody Repair_order_make_invoiceDTO repair_order_make_invoicedto) {
		Repair_order_make_invoice domain = repair_order_make_invoicedto.toDO();
        domain.setId(repair_order_make_invoice_id);
		repair_order_make_invoiceService.update(domain);
		Repair_order_make_invoiceDTO dto = new Repair_order_make_invoiceDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取数据", tags = {"Repair_order_make_invoice" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_repair/repair_order_make_invoices/{repair_order_make_invoice_id}")
    public ResponseEntity<Repair_order_make_invoiceDTO> get(@PathVariable("repair_order_make_invoice_id") Integer repair_order_make_invoice_id) {
        Repair_order_make_invoiceDTO dto = new Repair_order_make_invoiceDTO();
        Repair_order_make_invoice domain = repair_order_make_invoiceService.get(repair_order_make_invoice_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批删除数据", tags = {"Repair_order_make_invoice" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_repair/repair_order_make_invoices/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Repair_order_make_invoiceDTO> repair_order_make_invoicedtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "批建立数据", tags = {"Repair_order_make_invoice" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_repair/repair_order_make_invoices/createBatch")
    public ResponseEntity<Boolean> createBatchRepair_order_make_invoice(@RequestBody List<Repair_order_make_invoiceDTO> repair_order_make_invoicedtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "删除数据", tags = {"Repair_order_make_invoice" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_repair/repair_order_make_invoices/{repair_order_make_invoice_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("repair_order_make_invoice_id") Integer repair_order_make_invoice_id) {
        Repair_order_make_invoiceDTO repair_order_make_invoicedto = new Repair_order_make_invoiceDTO();
		Repair_order_make_invoice domain = new Repair_order_make_invoice();
		repair_order_make_invoicedto.setId(repair_order_make_invoice_id);
		domain.setId(repair_order_make_invoice_id);
        Boolean rst = repair_order_make_invoiceService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "建立数据", tags = {"Repair_order_make_invoice" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_repair/repair_order_make_invoices")

    public ResponseEntity<Repair_order_make_invoiceDTO> create(@RequestBody Repair_order_make_invoiceDTO repair_order_make_invoicedto) {
        Repair_order_make_invoiceDTO dto = new Repair_order_make_invoiceDTO();
        Repair_order_make_invoice domain = repair_order_make_invoicedto.toDO();
		repair_order_make_invoiceService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批更新数据", tags = {"Repair_order_make_invoice" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_repair/repair_order_make_invoices/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Repair_order_make_invoiceDTO> repair_order_make_invoicedtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Repair_order_make_invoice" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_repair/repair_order_make_invoices/fetchdefault")
	public ResponseEntity<Page<Repair_order_make_invoiceDTO>> fetchDefault(Repair_order_make_invoiceSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Repair_order_make_invoiceDTO> list = new ArrayList<Repair_order_make_invoiceDTO>();
        
        Page<Repair_order_make_invoice> domains = repair_order_make_invoiceService.searchDefault(context) ;
        for(Repair_order_make_invoice repair_order_make_invoice : domains.getContent()){
            Repair_order_make_invoiceDTO dto = new Repair_order_make_invoiceDTO();
            dto.fromDO(repair_order_make_invoice);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
