package cn.ibizlab.odoo.service.odoo_repair.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.core.odoo_repair.valuerule.anno.repair_line.*;
import cn.ibizlab.odoo.core.odoo_repair.domain.Repair_line;
import org.springframework.cglib.beans.BeanCopier;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 服务DTO对象[Repair_lineDTO]
 */
public class Repair_lineDTO implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @Repair_lineDisplay_nameDefault(info = "默认规则")
    private String display_name;

    @JsonIgnore
    private boolean display_nameDirtyFlag;

    /**
     * 属性 [WRITE_DATE]
     *
     */
    @Repair_lineWrite_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp write_date;

    @JsonIgnore
    private boolean write_dateDirtyFlag;

    /**
     * 属性 [NAME]
     *
     */
    @Repair_lineNameDefault(info = "默认规则")
    private String name;

    @JsonIgnore
    private boolean nameDirtyFlag;

    /**
     * 属性 [PRODUCT_UOM_QTY]
     *
     */
    @Repair_lineProduct_uom_qtyDefault(info = "默认规则")
    private Double product_uom_qty;

    @JsonIgnore
    private boolean product_uom_qtyDirtyFlag;

    /**
     * 属性 [INVOICED]
     *
     */
    @Repair_lineInvoicedDefault(info = "默认规则")
    private String invoiced;

    @JsonIgnore
    private boolean invoicedDirtyFlag;

    /**
     * 属性 [STATE]
     *
     */
    @Repair_lineStateDefault(info = "默认规则")
    private String state;

    @JsonIgnore
    private boolean stateDirtyFlag;

    /**
     * 属性 [TYPE]
     *
     */
    @Repair_lineTypeDefault(info = "默认规则")
    private String type;

    @JsonIgnore
    private boolean typeDirtyFlag;

    /**
     * 属性 [PRICE_UNIT]
     *
     */
    @Repair_linePrice_unitDefault(info = "默认规则")
    private Double price_unit;

    @JsonIgnore
    private boolean price_unitDirtyFlag;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @Repair_line__last_updateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp __last_update;

    @JsonIgnore
    private boolean __last_updateDirtyFlag;

    /**
     * 属性 [CREATE_DATE]
     *
     */
    @Repair_lineCreate_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp create_date;

    @JsonIgnore
    private boolean create_dateDirtyFlag;

    /**
     * 属性 [ID]
     *
     */
    @Repair_lineIdDefault(info = "默认规则")
    private Integer id;

    @JsonIgnore
    private boolean idDirtyFlag;

    /**
     * 属性 [TAX_ID]
     *
     */
    @Repair_lineTax_idDefault(info = "默认规则")
    private String tax_id;

    @JsonIgnore
    private boolean tax_idDirtyFlag;

    /**
     * 属性 [PRICE_SUBTOTAL]
     *
     */
    @Repair_linePrice_subtotalDefault(info = "默认规则")
    private Double price_subtotal;

    @JsonIgnore
    private boolean price_subtotalDirtyFlag;

    /**
     * 属性 [LOCATION_DEST_ID_TEXT]
     *
     */
    @Repair_lineLocation_dest_id_textDefault(info = "默认规则")
    private String location_dest_id_text;

    @JsonIgnore
    private boolean location_dest_id_textDirtyFlag;

    /**
     * 属性 [WRITE_UID_TEXT]
     *
     */
    @Repair_lineWrite_uid_textDefault(info = "默认规则")
    private String write_uid_text;

    @JsonIgnore
    private boolean write_uid_textDirtyFlag;

    /**
     * 属性 [MOVE_ID_TEXT]
     *
     */
    @Repair_lineMove_id_textDefault(info = "默认规则")
    private String move_id_text;

    @JsonIgnore
    private boolean move_id_textDirtyFlag;

    /**
     * 属性 [LOCATION_ID_TEXT]
     *
     */
    @Repair_lineLocation_id_textDefault(info = "默认规则")
    private String location_id_text;

    @JsonIgnore
    private boolean location_id_textDirtyFlag;

    /**
     * 属性 [LOT_ID_TEXT]
     *
     */
    @Repair_lineLot_id_textDefault(info = "默认规则")
    private String lot_id_text;

    @JsonIgnore
    private boolean lot_id_textDirtyFlag;

    /**
     * 属性 [PRODUCT_UOM_TEXT]
     *
     */
    @Repair_lineProduct_uom_textDefault(info = "默认规则")
    private String product_uom_text;

    @JsonIgnore
    private boolean product_uom_textDirtyFlag;

    /**
     * 属性 [INVOICE_LINE_ID_TEXT]
     *
     */
    @Repair_lineInvoice_line_id_textDefault(info = "默认规则")
    private String invoice_line_id_text;

    @JsonIgnore
    private boolean invoice_line_id_textDirtyFlag;

    /**
     * 属性 [CREATE_UID_TEXT]
     *
     */
    @Repair_lineCreate_uid_textDefault(info = "默认规则")
    private String create_uid_text;

    @JsonIgnore
    private boolean create_uid_textDirtyFlag;

    /**
     * 属性 [PRODUCT_ID_TEXT]
     *
     */
    @Repair_lineProduct_id_textDefault(info = "默认规则")
    private String product_id_text;

    @JsonIgnore
    private boolean product_id_textDirtyFlag;

    /**
     * 属性 [REPAIR_ID_TEXT]
     *
     */
    @Repair_lineRepair_id_textDefault(info = "默认规则")
    private String repair_id_text;

    @JsonIgnore
    private boolean repair_id_textDirtyFlag;

    /**
     * 属性 [LOT_ID]
     *
     */
    @Repair_lineLot_idDefault(info = "默认规则")
    private Integer lot_id;

    @JsonIgnore
    private boolean lot_idDirtyFlag;

    /**
     * 属性 [REPAIR_ID]
     *
     */
    @Repair_lineRepair_idDefault(info = "默认规则")
    private Integer repair_id;

    @JsonIgnore
    private boolean repair_idDirtyFlag;

    /**
     * 属性 [MOVE_ID]
     *
     */
    @Repair_lineMove_idDefault(info = "默认规则")
    private Integer move_id;

    @JsonIgnore
    private boolean move_idDirtyFlag;

    /**
     * 属性 [INVOICE_LINE_ID]
     *
     */
    @Repair_lineInvoice_line_idDefault(info = "默认规则")
    private Integer invoice_line_id;

    @JsonIgnore
    private boolean invoice_line_idDirtyFlag;

    /**
     * 属性 [PRODUCT_ID]
     *
     */
    @Repair_lineProduct_idDefault(info = "默认规则")
    private Integer product_id;

    @JsonIgnore
    private boolean product_idDirtyFlag;

    /**
     * 属性 [LOCATION_ID]
     *
     */
    @Repair_lineLocation_idDefault(info = "默认规则")
    private Integer location_id;

    @JsonIgnore
    private boolean location_idDirtyFlag;

    /**
     * 属性 [CREATE_UID]
     *
     */
    @Repair_lineCreate_uidDefault(info = "默认规则")
    private Integer create_uid;

    @JsonIgnore
    private boolean create_uidDirtyFlag;

    /**
     * 属性 [LOCATION_DEST_ID]
     *
     */
    @Repair_lineLocation_dest_idDefault(info = "默认规则")
    private Integer location_dest_id;

    @JsonIgnore
    private boolean location_dest_idDirtyFlag;

    /**
     * 属性 [WRITE_UID]
     *
     */
    @Repair_lineWrite_uidDefault(info = "默认规则")
    private Integer write_uid;

    @JsonIgnore
    private boolean write_uidDirtyFlag;

    /**
     * 属性 [PRODUCT_UOM]
     *
     */
    @Repair_lineProduct_uomDefault(info = "默认规则")
    private Integer product_uom;

    @JsonIgnore
    private boolean product_uomDirtyFlag;


    /**
     * 获取 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return display_name ;
    }

    /**
     * 设置 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

    /**
     * 获取 [DISPLAY_NAME]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return display_nameDirtyFlag ;
    }

    /**
     * 获取 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return write_date ;
    }

    /**
     * 设置 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return write_dateDirtyFlag ;
    }

    /**
     * 获取 [NAME]
     */
    @JsonProperty("name")
    public String getName(){
        return name ;
    }

    /**
     * 设置 [NAME]
     */
    @JsonProperty("name")
    public void setName(String  name){
        this.name = name ;
        this.nameDirtyFlag = true ;
    }

    /**
     * 获取 [NAME]脏标记
     */
    @JsonIgnore
    public boolean getNameDirtyFlag(){
        return nameDirtyFlag ;
    }

    /**
     * 获取 [PRODUCT_UOM_QTY]
     */
    @JsonProperty("product_uom_qty")
    public Double getProduct_uom_qty(){
        return product_uom_qty ;
    }

    /**
     * 设置 [PRODUCT_UOM_QTY]
     */
    @JsonProperty("product_uom_qty")
    public void setProduct_uom_qty(Double  product_uom_qty){
        this.product_uom_qty = product_uom_qty ;
        this.product_uom_qtyDirtyFlag = true ;
    }

    /**
     * 获取 [PRODUCT_UOM_QTY]脏标记
     */
    @JsonIgnore
    public boolean getProduct_uom_qtyDirtyFlag(){
        return product_uom_qtyDirtyFlag ;
    }

    /**
     * 获取 [INVOICED]
     */
    @JsonProperty("invoiced")
    public String getInvoiced(){
        return invoiced ;
    }

    /**
     * 设置 [INVOICED]
     */
    @JsonProperty("invoiced")
    public void setInvoiced(String  invoiced){
        this.invoiced = invoiced ;
        this.invoicedDirtyFlag = true ;
    }

    /**
     * 获取 [INVOICED]脏标记
     */
    @JsonIgnore
    public boolean getInvoicedDirtyFlag(){
        return invoicedDirtyFlag ;
    }

    /**
     * 获取 [STATE]
     */
    @JsonProperty("state")
    public String getState(){
        return state ;
    }

    /**
     * 设置 [STATE]
     */
    @JsonProperty("state")
    public void setState(String  state){
        this.state = state ;
        this.stateDirtyFlag = true ;
    }

    /**
     * 获取 [STATE]脏标记
     */
    @JsonIgnore
    public boolean getStateDirtyFlag(){
        return stateDirtyFlag ;
    }

    /**
     * 获取 [TYPE]
     */
    @JsonProperty("type")
    public String getType(){
        return type ;
    }

    /**
     * 设置 [TYPE]
     */
    @JsonProperty("type")
    public void setType(String  type){
        this.type = type ;
        this.typeDirtyFlag = true ;
    }

    /**
     * 获取 [TYPE]脏标记
     */
    @JsonIgnore
    public boolean getTypeDirtyFlag(){
        return typeDirtyFlag ;
    }

    /**
     * 获取 [PRICE_UNIT]
     */
    @JsonProperty("price_unit")
    public Double getPrice_unit(){
        return price_unit ;
    }

    /**
     * 设置 [PRICE_UNIT]
     */
    @JsonProperty("price_unit")
    public void setPrice_unit(Double  price_unit){
        this.price_unit = price_unit ;
        this.price_unitDirtyFlag = true ;
    }

    /**
     * 获取 [PRICE_UNIT]脏标记
     */
    @JsonIgnore
    public boolean getPrice_unitDirtyFlag(){
        return price_unitDirtyFlag ;
    }

    /**
     * 获取 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return __last_update ;
    }

    /**
     * 设置 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

    /**
     * 获取 [__LAST_UPDATE]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return __last_updateDirtyFlag ;
    }

    /**
     * 获取 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return create_date ;
    }

    /**
     * 设置 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return create_dateDirtyFlag ;
    }

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

    /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return idDirtyFlag ;
    }

    /**
     * 获取 [TAX_ID]
     */
    @JsonProperty("tax_id")
    public String getTax_id(){
        return tax_id ;
    }

    /**
     * 设置 [TAX_ID]
     */
    @JsonProperty("tax_id")
    public void setTax_id(String  tax_id){
        this.tax_id = tax_id ;
        this.tax_idDirtyFlag = true ;
    }

    /**
     * 获取 [TAX_ID]脏标记
     */
    @JsonIgnore
    public boolean getTax_idDirtyFlag(){
        return tax_idDirtyFlag ;
    }

    /**
     * 获取 [PRICE_SUBTOTAL]
     */
    @JsonProperty("price_subtotal")
    public Double getPrice_subtotal(){
        return price_subtotal ;
    }

    /**
     * 设置 [PRICE_SUBTOTAL]
     */
    @JsonProperty("price_subtotal")
    public void setPrice_subtotal(Double  price_subtotal){
        this.price_subtotal = price_subtotal ;
        this.price_subtotalDirtyFlag = true ;
    }

    /**
     * 获取 [PRICE_SUBTOTAL]脏标记
     */
    @JsonIgnore
    public boolean getPrice_subtotalDirtyFlag(){
        return price_subtotalDirtyFlag ;
    }

    /**
     * 获取 [LOCATION_DEST_ID_TEXT]
     */
    @JsonProperty("location_dest_id_text")
    public String getLocation_dest_id_text(){
        return location_dest_id_text ;
    }

    /**
     * 设置 [LOCATION_DEST_ID_TEXT]
     */
    @JsonProperty("location_dest_id_text")
    public void setLocation_dest_id_text(String  location_dest_id_text){
        this.location_dest_id_text = location_dest_id_text ;
        this.location_dest_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [LOCATION_DEST_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getLocation_dest_id_textDirtyFlag(){
        return location_dest_id_textDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return write_uid_text ;
    }

    /**
     * 设置 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return write_uid_textDirtyFlag ;
    }

    /**
     * 获取 [MOVE_ID_TEXT]
     */
    @JsonProperty("move_id_text")
    public String getMove_id_text(){
        return move_id_text ;
    }

    /**
     * 设置 [MOVE_ID_TEXT]
     */
    @JsonProperty("move_id_text")
    public void setMove_id_text(String  move_id_text){
        this.move_id_text = move_id_text ;
        this.move_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [MOVE_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getMove_id_textDirtyFlag(){
        return move_id_textDirtyFlag ;
    }

    /**
     * 获取 [LOCATION_ID_TEXT]
     */
    @JsonProperty("location_id_text")
    public String getLocation_id_text(){
        return location_id_text ;
    }

    /**
     * 设置 [LOCATION_ID_TEXT]
     */
    @JsonProperty("location_id_text")
    public void setLocation_id_text(String  location_id_text){
        this.location_id_text = location_id_text ;
        this.location_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [LOCATION_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getLocation_id_textDirtyFlag(){
        return location_id_textDirtyFlag ;
    }

    /**
     * 获取 [LOT_ID_TEXT]
     */
    @JsonProperty("lot_id_text")
    public String getLot_id_text(){
        return lot_id_text ;
    }

    /**
     * 设置 [LOT_ID_TEXT]
     */
    @JsonProperty("lot_id_text")
    public void setLot_id_text(String  lot_id_text){
        this.lot_id_text = lot_id_text ;
        this.lot_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [LOT_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getLot_id_textDirtyFlag(){
        return lot_id_textDirtyFlag ;
    }

    /**
     * 获取 [PRODUCT_UOM_TEXT]
     */
    @JsonProperty("product_uom_text")
    public String getProduct_uom_text(){
        return product_uom_text ;
    }

    /**
     * 设置 [PRODUCT_UOM_TEXT]
     */
    @JsonProperty("product_uom_text")
    public void setProduct_uom_text(String  product_uom_text){
        this.product_uom_text = product_uom_text ;
        this.product_uom_textDirtyFlag = true ;
    }

    /**
     * 获取 [PRODUCT_UOM_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getProduct_uom_textDirtyFlag(){
        return product_uom_textDirtyFlag ;
    }

    /**
     * 获取 [INVOICE_LINE_ID_TEXT]
     */
    @JsonProperty("invoice_line_id_text")
    public String getInvoice_line_id_text(){
        return invoice_line_id_text ;
    }

    /**
     * 设置 [INVOICE_LINE_ID_TEXT]
     */
    @JsonProperty("invoice_line_id_text")
    public void setInvoice_line_id_text(String  invoice_line_id_text){
        this.invoice_line_id_text = invoice_line_id_text ;
        this.invoice_line_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [INVOICE_LINE_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getInvoice_line_id_textDirtyFlag(){
        return invoice_line_id_textDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return create_uid_text ;
    }

    /**
     * 设置 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return create_uid_textDirtyFlag ;
    }

    /**
     * 获取 [PRODUCT_ID_TEXT]
     */
    @JsonProperty("product_id_text")
    public String getProduct_id_text(){
        return product_id_text ;
    }

    /**
     * 设置 [PRODUCT_ID_TEXT]
     */
    @JsonProperty("product_id_text")
    public void setProduct_id_text(String  product_id_text){
        this.product_id_text = product_id_text ;
        this.product_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [PRODUCT_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getProduct_id_textDirtyFlag(){
        return product_id_textDirtyFlag ;
    }

    /**
     * 获取 [REPAIR_ID_TEXT]
     */
    @JsonProperty("repair_id_text")
    public String getRepair_id_text(){
        return repair_id_text ;
    }

    /**
     * 设置 [REPAIR_ID_TEXT]
     */
    @JsonProperty("repair_id_text")
    public void setRepair_id_text(String  repair_id_text){
        this.repair_id_text = repair_id_text ;
        this.repair_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [REPAIR_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getRepair_id_textDirtyFlag(){
        return repair_id_textDirtyFlag ;
    }

    /**
     * 获取 [LOT_ID]
     */
    @JsonProperty("lot_id")
    public Integer getLot_id(){
        return lot_id ;
    }

    /**
     * 设置 [LOT_ID]
     */
    @JsonProperty("lot_id")
    public void setLot_id(Integer  lot_id){
        this.lot_id = lot_id ;
        this.lot_idDirtyFlag = true ;
    }

    /**
     * 获取 [LOT_ID]脏标记
     */
    @JsonIgnore
    public boolean getLot_idDirtyFlag(){
        return lot_idDirtyFlag ;
    }

    /**
     * 获取 [REPAIR_ID]
     */
    @JsonProperty("repair_id")
    public Integer getRepair_id(){
        return repair_id ;
    }

    /**
     * 设置 [REPAIR_ID]
     */
    @JsonProperty("repair_id")
    public void setRepair_id(Integer  repair_id){
        this.repair_id = repair_id ;
        this.repair_idDirtyFlag = true ;
    }

    /**
     * 获取 [REPAIR_ID]脏标记
     */
    @JsonIgnore
    public boolean getRepair_idDirtyFlag(){
        return repair_idDirtyFlag ;
    }

    /**
     * 获取 [MOVE_ID]
     */
    @JsonProperty("move_id")
    public Integer getMove_id(){
        return move_id ;
    }

    /**
     * 设置 [MOVE_ID]
     */
    @JsonProperty("move_id")
    public void setMove_id(Integer  move_id){
        this.move_id = move_id ;
        this.move_idDirtyFlag = true ;
    }

    /**
     * 获取 [MOVE_ID]脏标记
     */
    @JsonIgnore
    public boolean getMove_idDirtyFlag(){
        return move_idDirtyFlag ;
    }

    /**
     * 获取 [INVOICE_LINE_ID]
     */
    @JsonProperty("invoice_line_id")
    public Integer getInvoice_line_id(){
        return invoice_line_id ;
    }

    /**
     * 设置 [INVOICE_LINE_ID]
     */
    @JsonProperty("invoice_line_id")
    public void setInvoice_line_id(Integer  invoice_line_id){
        this.invoice_line_id = invoice_line_id ;
        this.invoice_line_idDirtyFlag = true ;
    }

    /**
     * 获取 [INVOICE_LINE_ID]脏标记
     */
    @JsonIgnore
    public boolean getInvoice_line_idDirtyFlag(){
        return invoice_line_idDirtyFlag ;
    }

    /**
     * 获取 [PRODUCT_ID]
     */
    @JsonProperty("product_id")
    public Integer getProduct_id(){
        return product_id ;
    }

    /**
     * 设置 [PRODUCT_ID]
     */
    @JsonProperty("product_id")
    public void setProduct_id(Integer  product_id){
        this.product_id = product_id ;
        this.product_idDirtyFlag = true ;
    }

    /**
     * 获取 [PRODUCT_ID]脏标记
     */
    @JsonIgnore
    public boolean getProduct_idDirtyFlag(){
        return product_idDirtyFlag ;
    }

    /**
     * 获取 [LOCATION_ID]
     */
    @JsonProperty("location_id")
    public Integer getLocation_id(){
        return location_id ;
    }

    /**
     * 设置 [LOCATION_ID]
     */
    @JsonProperty("location_id")
    public void setLocation_id(Integer  location_id){
        this.location_id = location_id ;
        this.location_idDirtyFlag = true ;
    }

    /**
     * 获取 [LOCATION_ID]脏标记
     */
    @JsonIgnore
    public boolean getLocation_idDirtyFlag(){
        return location_idDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return create_uid ;
    }

    /**
     * 设置 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return create_uidDirtyFlag ;
    }

    /**
     * 获取 [LOCATION_DEST_ID]
     */
    @JsonProperty("location_dest_id")
    public Integer getLocation_dest_id(){
        return location_dest_id ;
    }

    /**
     * 设置 [LOCATION_DEST_ID]
     */
    @JsonProperty("location_dest_id")
    public void setLocation_dest_id(Integer  location_dest_id){
        this.location_dest_id = location_dest_id ;
        this.location_dest_idDirtyFlag = true ;
    }

    /**
     * 获取 [LOCATION_DEST_ID]脏标记
     */
    @JsonIgnore
    public boolean getLocation_dest_idDirtyFlag(){
        return location_dest_idDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return write_uid ;
    }

    /**
     * 设置 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return write_uidDirtyFlag ;
    }

    /**
     * 获取 [PRODUCT_UOM]
     */
    @JsonProperty("product_uom")
    public Integer getProduct_uom(){
        return product_uom ;
    }

    /**
     * 设置 [PRODUCT_UOM]
     */
    @JsonProperty("product_uom")
    public void setProduct_uom(Integer  product_uom){
        this.product_uom = product_uom ;
        this.product_uomDirtyFlag = true ;
    }

    /**
     * 获取 [PRODUCT_UOM]脏标记
     */
    @JsonIgnore
    public boolean getProduct_uomDirtyFlag(){
        return product_uomDirtyFlag ;
    }



    public Repair_line toDO() {
        Repair_line srfdomain = new Repair_line();
        if(getDisplay_nameDirtyFlag())
            srfdomain.setDisplay_name(display_name);
        if(getWrite_dateDirtyFlag())
            srfdomain.setWrite_date(write_date);
        if(getNameDirtyFlag())
            srfdomain.setName(name);
        if(getProduct_uom_qtyDirtyFlag())
            srfdomain.setProduct_uom_qty(product_uom_qty);
        if(getInvoicedDirtyFlag())
            srfdomain.setInvoiced(invoiced);
        if(getStateDirtyFlag())
            srfdomain.setState(state);
        if(getTypeDirtyFlag())
            srfdomain.setType(type);
        if(getPrice_unitDirtyFlag())
            srfdomain.setPrice_unit(price_unit);
        if(get__last_updateDirtyFlag())
            srfdomain.set__last_update(__last_update);
        if(getCreate_dateDirtyFlag())
            srfdomain.setCreate_date(create_date);
        if(getIdDirtyFlag())
            srfdomain.setId(id);
        if(getTax_idDirtyFlag())
            srfdomain.setTax_id(tax_id);
        if(getPrice_subtotalDirtyFlag())
            srfdomain.setPrice_subtotal(price_subtotal);
        if(getLocation_dest_id_textDirtyFlag())
            srfdomain.setLocation_dest_id_text(location_dest_id_text);
        if(getWrite_uid_textDirtyFlag())
            srfdomain.setWrite_uid_text(write_uid_text);
        if(getMove_id_textDirtyFlag())
            srfdomain.setMove_id_text(move_id_text);
        if(getLocation_id_textDirtyFlag())
            srfdomain.setLocation_id_text(location_id_text);
        if(getLot_id_textDirtyFlag())
            srfdomain.setLot_id_text(lot_id_text);
        if(getProduct_uom_textDirtyFlag())
            srfdomain.setProduct_uom_text(product_uom_text);
        if(getInvoice_line_id_textDirtyFlag())
            srfdomain.setInvoice_line_id_text(invoice_line_id_text);
        if(getCreate_uid_textDirtyFlag())
            srfdomain.setCreate_uid_text(create_uid_text);
        if(getProduct_id_textDirtyFlag())
            srfdomain.setProduct_id_text(product_id_text);
        if(getRepair_id_textDirtyFlag())
            srfdomain.setRepair_id_text(repair_id_text);
        if(getLot_idDirtyFlag())
            srfdomain.setLot_id(lot_id);
        if(getRepair_idDirtyFlag())
            srfdomain.setRepair_id(repair_id);
        if(getMove_idDirtyFlag())
            srfdomain.setMove_id(move_id);
        if(getInvoice_line_idDirtyFlag())
            srfdomain.setInvoice_line_id(invoice_line_id);
        if(getProduct_idDirtyFlag())
            srfdomain.setProduct_id(product_id);
        if(getLocation_idDirtyFlag())
            srfdomain.setLocation_id(location_id);
        if(getCreate_uidDirtyFlag())
            srfdomain.setCreate_uid(create_uid);
        if(getLocation_dest_idDirtyFlag())
            srfdomain.setLocation_dest_id(location_dest_id);
        if(getWrite_uidDirtyFlag())
            srfdomain.setWrite_uid(write_uid);
        if(getProduct_uomDirtyFlag())
            srfdomain.setProduct_uom(product_uom);

        return srfdomain;
    }

    public void fromDO(Repair_line srfdomain) {
        if(srfdomain == null )
          return ;
        if(srfdomain.getDisplay_nameDirtyFlag())
            this.setDisplay_name(srfdomain.getDisplay_name());
        if(srfdomain.getWrite_dateDirtyFlag())
            this.setWrite_date(srfdomain.getWrite_date());
        if(srfdomain.getNameDirtyFlag())
            this.setName(srfdomain.getName());
        if(srfdomain.getProduct_uom_qtyDirtyFlag())
            this.setProduct_uom_qty(srfdomain.getProduct_uom_qty());
        if(srfdomain.getInvoicedDirtyFlag())
            this.setInvoiced(srfdomain.getInvoiced());
        if(srfdomain.getStateDirtyFlag())
            this.setState(srfdomain.getState());
        if(srfdomain.getTypeDirtyFlag())
            this.setType(srfdomain.getType());
        if(srfdomain.getPrice_unitDirtyFlag())
            this.setPrice_unit(srfdomain.getPrice_unit());
        if(srfdomain.get__last_updateDirtyFlag())
            this.set__last_update(srfdomain.get__last_update());
        if(srfdomain.getCreate_dateDirtyFlag())
            this.setCreate_date(srfdomain.getCreate_date());
        if(srfdomain.getIdDirtyFlag())
            this.setId(srfdomain.getId());
        if(srfdomain.getTax_idDirtyFlag())
            this.setTax_id(srfdomain.getTax_id());
        if(srfdomain.getPrice_subtotalDirtyFlag())
            this.setPrice_subtotal(srfdomain.getPrice_subtotal());
        if(srfdomain.getLocation_dest_id_textDirtyFlag())
            this.setLocation_dest_id_text(srfdomain.getLocation_dest_id_text());
        if(srfdomain.getWrite_uid_textDirtyFlag())
            this.setWrite_uid_text(srfdomain.getWrite_uid_text());
        if(srfdomain.getMove_id_textDirtyFlag())
            this.setMove_id_text(srfdomain.getMove_id_text());
        if(srfdomain.getLocation_id_textDirtyFlag())
            this.setLocation_id_text(srfdomain.getLocation_id_text());
        if(srfdomain.getLot_id_textDirtyFlag())
            this.setLot_id_text(srfdomain.getLot_id_text());
        if(srfdomain.getProduct_uom_textDirtyFlag())
            this.setProduct_uom_text(srfdomain.getProduct_uom_text());
        if(srfdomain.getInvoice_line_id_textDirtyFlag())
            this.setInvoice_line_id_text(srfdomain.getInvoice_line_id_text());
        if(srfdomain.getCreate_uid_textDirtyFlag())
            this.setCreate_uid_text(srfdomain.getCreate_uid_text());
        if(srfdomain.getProduct_id_textDirtyFlag())
            this.setProduct_id_text(srfdomain.getProduct_id_text());
        if(srfdomain.getRepair_id_textDirtyFlag())
            this.setRepair_id_text(srfdomain.getRepair_id_text());
        if(srfdomain.getLot_idDirtyFlag())
            this.setLot_id(srfdomain.getLot_id());
        if(srfdomain.getRepair_idDirtyFlag())
            this.setRepair_id(srfdomain.getRepair_id());
        if(srfdomain.getMove_idDirtyFlag())
            this.setMove_id(srfdomain.getMove_id());
        if(srfdomain.getInvoice_line_idDirtyFlag())
            this.setInvoice_line_id(srfdomain.getInvoice_line_id());
        if(srfdomain.getProduct_idDirtyFlag())
            this.setProduct_id(srfdomain.getProduct_id());
        if(srfdomain.getLocation_idDirtyFlag())
            this.setLocation_id(srfdomain.getLocation_id());
        if(srfdomain.getCreate_uidDirtyFlag())
            this.setCreate_uid(srfdomain.getCreate_uid());
        if(srfdomain.getLocation_dest_idDirtyFlag())
            this.setLocation_dest_id(srfdomain.getLocation_dest_id());
        if(srfdomain.getWrite_uidDirtyFlag())
            this.setWrite_uid(srfdomain.getWrite_uid());
        if(srfdomain.getProduct_uomDirtyFlag())
            this.setProduct_uom(srfdomain.getProduct_uom());

    }

    public List<Repair_lineDTO> fromDOPage(List<Repair_line> poPage)   {
        if(poPage == null)
            return null;
        List<Repair_lineDTO> dtos=new ArrayList<Repair_lineDTO>();
        for(Repair_line domain : poPage) {
            Repair_lineDTO dto = new Repair_lineDTO();
            dto.fromDO(domain);
            dtos.add(dto);
        }
        return dtos;
    }
}

