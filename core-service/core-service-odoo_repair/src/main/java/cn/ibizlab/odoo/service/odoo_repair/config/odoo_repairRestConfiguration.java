package cn.ibizlab.odoo.service.odoo_repair.config;

import org.springframework.context.annotation.ComponentScan;

@ComponentScan("cn.ibizlab.odoo.service.odoo_repair")
public class odoo_repairRestConfiguration {

}
