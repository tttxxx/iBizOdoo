package cn.ibizlab.odoo.service.odoo_gamification.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_gamification.dto.Gamification_goal_definitionDTO;
import cn.ibizlab.odoo.core.odoo_gamification.domain.Gamification_goal_definition;
import cn.ibizlab.odoo.core.odoo_gamification.service.IGamification_goal_definitionService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_gamification.filter.Gamification_goal_definitionSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Gamification_goal_definition" })
@RestController
@RequestMapping("")
public class Gamification_goal_definitionResource {

    @Autowired
    private IGamification_goal_definitionService gamification_goal_definitionService;

    public IGamification_goal_definitionService getGamification_goal_definitionService() {
        return this.gamification_goal_definitionService;
    }

    @ApiOperation(value = "获取数据", tags = {"Gamification_goal_definition" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_gamification/gamification_goal_definitions/{gamification_goal_definition_id}")
    public ResponseEntity<Gamification_goal_definitionDTO> get(@PathVariable("gamification_goal_definition_id") Integer gamification_goal_definition_id) {
        Gamification_goal_definitionDTO dto = new Gamification_goal_definitionDTO();
        Gamification_goal_definition domain = gamification_goal_definitionService.get(gamification_goal_definition_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "更新数据", tags = {"Gamification_goal_definition" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_gamification/gamification_goal_definitions/{gamification_goal_definition_id}")

    public ResponseEntity<Gamification_goal_definitionDTO> update(@PathVariable("gamification_goal_definition_id") Integer gamification_goal_definition_id, @RequestBody Gamification_goal_definitionDTO gamification_goal_definitiondto) {
		Gamification_goal_definition domain = gamification_goal_definitiondto.toDO();
        domain.setId(gamification_goal_definition_id);
		gamification_goal_definitionService.update(domain);
		Gamification_goal_definitionDTO dto = new Gamification_goal_definitionDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "建立数据", tags = {"Gamification_goal_definition" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_gamification/gamification_goal_definitions")

    public ResponseEntity<Gamification_goal_definitionDTO> create(@RequestBody Gamification_goal_definitionDTO gamification_goal_definitiondto) {
        Gamification_goal_definitionDTO dto = new Gamification_goal_definitionDTO();
        Gamification_goal_definition domain = gamification_goal_definitiondto.toDO();
		gamification_goal_definitionService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批建立数据", tags = {"Gamification_goal_definition" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_gamification/gamification_goal_definitions/createBatch")
    public ResponseEntity<Boolean> createBatchGamification_goal_definition(@RequestBody List<Gamification_goal_definitionDTO> gamification_goal_definitiondtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "删除数据", tags = {"Gamification_goal_definition" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_gamification/gamification_goal_definitions/{gamification_goal_definition_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("gamification_goal_definition_id") Integer gamification_goal_definition_id) {
        Gamification_goal_definitionDTO gamification_goal_definitiondto = new Gamification_goal_definitionDTO();
		Gamification_goal_definition domain = new Gamification_goal_definition();
		gamification_goal_definitiondto.setId(gamification_goal_definition_id);
		domain.setId(gamification_goal_definition_id);
        Boolean rst = gamification_goal_definitionService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "批更新数据", tags = {"Gamification_goal_definition" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_gamification/gamification_goal_definitions/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Gamification_goal_definitionDTO> gamification_goal_definitiondtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "批删除数据", tags = {"Gamification_goal_definition" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_gamification/gamification_goal_definitions/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Gamification_goal_definitionDTO> gamification_goal_definitiondtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Gamification_goal_definition" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_gamification/gamification_goal_definitions/fetchdefault")
	public ResponseEntity<Page<Gamification_goal_definitionDTO>> fetchDefault(Gamification_goal_definitionSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Gamification_goal_definitionDTO> list = new ArrayList<Gamification_goal_definitionDTO>();
        
        Page<Gamification_goal_definition> domains = gamification_goal_definitionService.searchDefault(context) ;
        for(Gamification_goal_definition gamification_goal_definition : domains.getContent()){
            Gamification_goal_definitionDTO dto = new Gamification_goal_definitionDTO();
            dto.fromDO(gamification_goal_definition);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
