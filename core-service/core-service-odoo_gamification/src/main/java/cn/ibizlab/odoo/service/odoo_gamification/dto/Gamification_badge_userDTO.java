package cn.ibizlab.odoo.service.odoo_gamification.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.core.odoo_gamification.valuerule.anno.gamification_badge_user.*;
import cn.ibizlab.odoo.core.odoo_gamification.domain.Gamification_badge_user;
import org.springframework.cglib.beans.BeanCopier;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 服务DTO对象[Gamification_badge_userDTO]
 */
public class Gamification_badge_userDTO implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [CREATE_DATE]
     *
     */
    @Gamification_badge_userCreate_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp create_date;

    @JsonIgnore
    private boolean create_dateDirtyFlag;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @Gamification_badge_userDisplay_nameDefault(info = "默认规则")
    private String display_name;

    @JsonIgnore
    private boolean display_nameDirtyFlag;

    /**
     * 属性 [ID]
     *
     */
    @Gamification_badge_userIdDefault(info = "默认规则")
    private Integer id;

    @JsonIgnore
    private boolean idDirtyFlag;

    /**
     * 属性 [WRITE_DATE]
     *
     */
    @Gamification_badge_userWrite_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp write_date;

    @JsonIgnore
    private boolean write_dateDirtyFlag;

    /**
     * 属性 [COMMENT]
     *
     */
    @Gamification_badge_userCommentDefault(info = "默认规则")
    private String comment;

    @JsonIgnore
    private boolean commentDirtyFlag;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @Gamification_badge_user__last_updateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp __last_update;

    @JsonIgnore
    private boolean __last_updateDirtyFlag;

    /**
     * 属性 [USER_ID_TEXT]
     *
     */
    @Gamification_badge_userUser_id_textDefault(info = "默认规则")
    private String user_id_text;

    @JsonIgnore
    private boolean user_id_textDirtyFlag;

    /**
     * 属性 [SENDER_ID_TEXT]
     *
     */
    @Gamification_badge_userSender_id_textDefault(info = "默认规则")
    private String sender_id_text;

    @JsonIgnore
    private boolean sender_id_textDirtyFlag;

    /**
     * 属性 [LEVEL]
     *
     */
    @Gamification_badge_userLevelDefault(info = "默认规则")
    private String level;

    @JsonIgnore
    private boolean levelDirtyFlag;

    /**
     * 属性 [WRITE_UID_TEXT]
     *
     */
    @Gamification_badge_userWrite_uid_textDefault(info = "默认规则")
    private String write_uid_text;

    @JsonIgnore
    private boolean write_uid_textDirtyFlag;

    /**
     * 属性 [CREATE_UID_TEXT]
     *
     */
    @Gamification_badge_userCreate_uid_textDefault(info = "默认规则")
    private String create_uid_text;

    @JsonIgnore
    private boolean create_uid_textDirtyFlag;

    /**
     * 属性 [BADGE_NAME]
     *
     */
    @Gamification_badge_userBadge_nameDefault(info = "默认规则")
    private String badge_name;

    @JsonIgnore
    private boolean badge_nameDirtyFlag;

    /**
     * 属性 [CHALLENGE_ID_TEXT]
     *
     */
    @Gamification_badge_userChallenge_id_textDefault(info = "默认规则")
    private String challenge_id_text;

    @JsonIgnore
    private boolean challenge_id_textDirtyFlag;

    /**
     * 属性 [EMPLOYEE_ID_TEXT]
     *
     */
    @Gamification_badge_userEmployee_id_textDefault(info = "默认规则")
    private String employee_id_text;

    @JsonIgnore
    private boolean employee_id_textDirtyFlag;

    /**
     * 属性 [SENDER_ID]
     *
     */
    @Gamification_badge_userSender_idDefault(info = "默认规则")
    private Integer sender_id;

    @JsonIgnore
    private boolean sender_idDirtyFlag;

    /**
     * 属性 [BADGE_ID]
     *
     */
    @Gamification_badge_userBadge_idDefault(info = "默认规则")
    private Integer badge_id;

    @JsonIgnore
    private boolean badge_idDirtyFlag;

    /**
     * 属性 [CHALLENGE_ID]
     *
     */
    @Gamification_badge_userChallenge_idDefault(info = "默认规则")
    private Integer challenge_id;

    @JsonIgnore
    private boolean challenge_idDirtyFlag;

    /**
     * 属性 [EMPLOYEE_ID]
     *
     */
    @Gamification_badge_userEmployee_idDefault(info = "默认规则")
    private Integer employee_id;

    @JsonIgnore
    private boolean employee_idDirtyFlag;

    /**
     * 属性 [CREATE_UID]
     *
     */
    @Gamification_badge_userCreate_uidDefault(info = "默认规则")
    private Integer create_uid;

    @JsonIgnore
    private boolean create_uidDirtyFlag;

    /**
     * 属性 [WRITE_UID]
     *
     */
    @Gamification_badge_userWrite_uidDefault(info = "默认规则")
    private Integer write_uid;

    @JsonIgnore
    private boolean write_uidDirtyFlag;

    /**
     * 属性 [USER_ID]
     *
     */
    @Gamification_badge_userUser_idDefault(info = "默认规则")
    private Integer user_id;

    @JsonIgnore
    private boolean user_idDirtyFlag;


    /**
     * 获取 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return create_date ;
    }

    /**
     * 设置 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return create_dateDirtyFlag ;
    }

    /**
     * 获取 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return display_name ;
    }

    /**
     * 设置 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

    /**
     * 获取 [DISPLAY_NAME]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return display_nameDirtyFlag ;
    }

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

    /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return idDirtyFlag ;
    }

    /**
     * 获取 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return write_date ;
    }

    /**
     * 设置 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return write_dateDirtyFlag ;
    }

    /**
     * 获取 [COMMENT]
     */
    @JsonProperty("comment")
    public String getComment(){
        return comment ;
    }

    /**
     * 设置 [COMMENT]
     */
    @JsonProperty("comment")
    public void setComment(String  comment){
        this.comment = comment ;
        this.commentDirtyFlag = true ;
    }

    /**
     * 获取 [COMMENT]脏标记
     */
    @JsonIgnore
    public boolean getCommentDirtyFlag(){
        return commentDirtyFlag ;
    }

    /**
     * 获取 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return __last_update ;
    }

    /**
     * 设置 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

    /**
     * 获取 [__LAST_UPDATE]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return __last_updateDirtyFlag ;
    }

    /**
     * 获取 [USER_ID_TEXT]
     */
    @JsonProperty("user_id_text")
    public String getUser_id_text(){
        return user_id_text ;
    }

    /**
     * 设置 [USER_ID_TEXT]
     */
    @JsonProperty("user_id_text")
    public void setUser_id_text(String  user_id_text){
        this.user_id_text = user_id_text ;
        this.user_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [USER_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getUser_id_textDirtyFlag(){
        return user_id_textDirtyFlag ;
    }

    /**
     * 获取 [SENDER_ID_TEXT]
     */
    @JsonProperty("sender_id_text")
    public String getSender_id_text(){
        return sender_id_text ;
    }

    /**
     * 设置 [SENDER_ID_TEXT]
     */
    @JsonProperty("sender_id_text")
    public void setSender_id_text(String  sender_id_text){
        this.sender_id_text = sender_id_text ;
        this.sender_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [SENDER_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getSender_id_textDirtyFlag(){
        return sender_id_textDirtyFlag ;
    }

    /**
     * 获取 [LEVEL]
     */
    @JsonProperty("level")
    public String getLevel(){
        return level ;
    }

    /**
     * 设置 [LEVEL]
     */
    @JsonProperty("level")
    public void setLevel(String  level){
        this.level = level ;
        this.levelDirtyFlag = true ;
    }

    /**
     * 获取 [LEVEL]脏标记
     */
    @JsonIgnore
    public boolean getLevelDirtyFlag(){
        return levelDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return write_uid_text ;
    }

    /**
     * 设置 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return write_uid_textDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return create_uid_text ;
    }

    /**
     * 设置 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return create_uid_textDirtyFlag ;
    }

    /**
     * 获取 [BADGE_NAME]
     */
    @JsonProperty("badge_name")
    public String getBadge_name(){
        return badge_name ;
    }

    /**
     * 设置 [BADGE_NAME]
     */
    @JsonProperty("badge_name")
    public void setBadge_name(String  badge_name){
        this.badge_name = badge_name ;
        this.badge_nameDirtyFlag = true ;
    }

    /**
     * 获取 [BADGE_NAME]脏标记
     */
    @JsonIgnore
    public boolean getBadge_nameDirtyFlag(){
        return badge_nameDirtyFlag ;
    }

    /**
     * 获取 [CHALLENGE_ID_TEXT]
     */
    @JsonProperty("challenge_id_text")
    public String getChallenge_id_text(){
        return challenge_id_text ;
    }

    /**
     * 设置 [CHALLENGE_ID_TEXT]
     */
    @JsonProperty("challenge_id_text")
    public void setChallenge_id_text(String  challenge_id_text){
        this.challenge_id_text = challenge_id_text ;
        this.challenge_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [CHALLENGE_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getChallenge_id_textDirtyFlag(){
        return challenge_id_textDirtyFlag ;
    }

    /**
     * 获取 [EMPLOYEE_ID_TEXT]
     */
    @JsonProperty("employee_id_text")
    public String getEmployee_id_text(){
        return employee_id_text ;
    }

    /**
     * 设置 [EMPLOYEE_ID_TEXT]
     */
    @JsonProperty("employee_id_text")
    public void setEmployee_id_text(String  employee_id_text){
        this.employee_id_text = employee_id_text ;
        this.employee_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [EMPLOYEE_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getEmployee_id_textDirtyFlag(){
        return employee_id_textDirtyFlag ;
    }

    /**
     * 获取 [SENDER_ID]
     */
    @JsonProperty("sender_id")
    public Integer getSender_id(){
        return sender_id ;
    }

    /**
     * 设置 [SENDER_ID]
     */
    @JsonProperty("sender_id")
    public void setSender_id(Integer  sender_id){
        this.sender_id = sender_id ;
        this.sender_idDirtyFlag = true ;
    }

    /**
     * 获取 [SENDER_ID]脏标记
     */
    @JsonIgnore
    public boolean getSender_idDirtyFlag(){
        return sender_idDirtyFlag ;
    }

    /**
     * 获取 [BADGE_ID]
     */
    @JsonProperty("badge_id")
    public Integer getBadge_id(){
        return badge_id ;
    }

    /**
     * 设置 [BADGE_ID]
     */
    @JsonProperty("badge_id")
    public void setBadge_id(Integer  badge_id){
        this.badge_id = badge_id ;
        this.badge_idDirtyFlag = true ;
    }

    /**
     * 获取 [BADGE_ID]脏标记
     */
    @JsonIgnore
    public boolean getBadge_idDirtyFlag(){
        return badge_idDirtyFlag ;
    }

    /**
     * 获取 [CHALLENGE_ID]
     */
    @JsonProperty("challenge_id")
    public Integer getChallenge_id(){
        return challenge_id ;
    }

    /**
     * 设置 [CHALLENGE_ID]
     */
    @JsonProperty("challenge_id")
    public void setChallenge_id(Integer  challenge_id){
        this.challenge_id = challenge_id ;
        this.challenge_idDirtyFlag = true ;
    }

    /**
     * 获取 [CHALLENGE_ID]脏标记
     */
    @JsonIgnore
    public boolean getChallenge_idDirtyFlag(){
        return challenge_idDirtyFlag ;
    }

    /**
     * 获取 [EMPLOYEE_ID]
     */
    @JsonProperty("employee_id")
    public Integer getEmployee_id(){
        return employee_id ;
    }

    /**
     * 设置 [EMPLOYEE_ID]
     */
    @JsonProperty("employee_id")
    public void setEmployee_id(Integer  employee_id){
        this.employee_id = employee_id ;
        this.employee_idDirtyFlag = true ;
    }

    /**
     * 获取 [EMPLOYEE_ID]脏标记
     */
    @JsonIgnore
    public boolean getEmployee_idDirtyFlag(){
        return employee_idDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return create_uid ;
    }

    /**
     * 设置 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return create_uidDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return write_uid ;
    }

    /**
     * 设置 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return write_uidDirtyFlag ;
    }

    /**
     * 获取 [USER_ID]
     */
    @JsonProperty("user_id")
    public Integer getUser_id(){
        return user_id ;
    }

    /**
     * 设置 [USER_ID]
     */
    @JsonProperty("user_id")
    public void setUser_id(Integer  user_id){
        this.user_id = user_id ;
        this.user_idDirtyFlag = true ;
    }

    /**
     * 获取 [USER_ID]脏标记
     */
    @JsonIgnore
    public boolean getUser_idDirtyFlag(){
        return user_idDirtyFlag ;
    }



    public Gamification_badge_user toDO() {
        Gamification_badge_user srfdomain = new Gamification_badge_user();
        if(getCreate_dateDirtyFlag())
            srfdomain.setCreate_date(create_date);
        if(getDisplay_nameDirtyFlag())
            srfdomain.setDisplay_name(display_name);
        if(getIdDirtyFlag())
            srfdomain.setId(id);
        if(getWrite_dateDirtyFlag())
            srfdomain.setWrite_date(write_date);
        if(getCommentDirtyFlag())
            srfdomain.setComment(comment);
        if(get__last_updateDirtyFlag())
            srfdomain.set__last_update(__last_update);
        if(getUser_id_textDirtyFlag())
            srfdomain.setUser_id_text(user_id_text);
        if(getSender_id_textDirtyFlag())
            srfdomain.setSender_id_text(sender_id_text);
        if(getLevelDirtyFlag())
            srfdomain.setLevel(level);
        if(getWrite_uid_textDirtyFlag())
            srfdomain.setWrite_uid_text(write_uid_text);
        if(getCreate_uid_textDirtyFlag())
            srfdomain.setCreate_uid_text(create_uid_text);
        if(getBadge_nameDirtyFlag())
            srfdomain.setBadge_name(badge_name);
        if(getChallenge_id_textDirtyFlag())
            srfdomain.setChallenge_id_text(challenge_id_text);
        if(getEmployee_id_textDirtyFlag())
            srfdomain.setEmployee_id_text(employee_id_text);
        if(getSender_idDirtyFlag())
            srfdomain.setSender_id(sender_id);
        if(getBadge_idDirtyFlag())
            srfdomain.setBadge_id(badge_id);
        if(getChallenge_idDirtyFlag())
            srfdomain.setChallenge_id(challenge_id);
        if(getEmployee_idDirtyFlag())
            srfdomain.setEmployee_id(employee_id);
        if(getCreate_uidDirtyFlag())
            srfdomain.setCreate_uid(create_uid);
        if(getWrite_uidDirtyFlag())
            srfdomain.setWrite_uid(write_uid);
        if(getUser_idDirtyFlag())
            srfdomain.setUser_id(user_id);

        return srfdomain;
    }

    public void fromDO(Gamification_badge_user srfdomain) {
        if(srfdomain == null )
          return ;
        if(srfdomain.getCreate_dateDirtyFlag())
            this.setCreate_date(srfdomain.getCreate_date());
        if(srfdomain.getDisplay_nameDirtyFlag())
            this.setDisplay_name(srfdomain.getDisplay_name());
        if(srfdomain.getIdDirtyFlag())
            this.setId(srfdomain.getId());
        if(srfdomain.getWrite_dateDirtyFlag())
            this.setWrite_date(srfdomain.getWrite_date());
        if(srfdomain.getCommentDirtyFlag())
            this.setComment(srfdomain.getComment());
        if(srfdomain.get__last_updateDirtyFlag())
            this.set__last_update(srfdomain.get__last_update());
        if(srfdomain.getUser_id_textDirtyFlag())
            this.setUser_id_text(srfdomain.getUser_id_text());
        if(srfdomain.getSender_id_textDirtyFlag())
            this.setSender_id_text(srfdomain.getSender_id_text());
        if(srfdomain.getLevelDirtyFlag())
            this.setLevel(srfdomain.getLevel());
        if(srfdomain.getWrite_uid_textDirtyFlag())
            this.setWrite_uid_text(srfdomain.getWrite_uid_text());
        if(srfdomain.getCreate_uid_textDirtyFlag())
            this.setCreate_uid_text(srfdomain.getCreate_uid_text());
        if(srfdomain.getBadge_nameDirtyFlag())
            this.setBadge_name(srfdomain.getBadge_name());
        if(srfdomain.getChallenge_id_textDirtyFlag())
            this.setChallenge_id_text(srfdomain.getChallenge_id_text());
        if(srfdomain.getEmployee_id_textDirtyFlag())
            this.setEmployee_id_text(srfdomain.getEmployee_id_text());
        if(srfdomain.getSender_idDirtyFlag())
            this.setSender_id(srfdomain.getSender_id());
        if(srfdomain.getBadge_idDirtyFlag())
            this.setBadge_id(srfdomain.getBadge_id());
        if(srfdomain.getChallenge_idDirtyFlag())
            this.setChallenge_id(srfdomain.getChallenge_id());
        if(srfdomain.getEmployee_idDirtyFlag())
            this.setEmployee_id(srfdomain.getEmployee_id());
        if(srfdomain.getCreate_uidDirtyFlag())
            this.setCreate_uid(srfdomain.getCreate_uid());
        if(srfdomain.getWrite_uidDirtyFlag())
            this.setWrite_uid(srfdomain.getWrite_uid());
        if(srfdomain.getUser_idDirtyFlag())
            this.setUser_id(srfdomain.getUser_id());

    }

    public List<Gamification_badge_userDTO> fromDOPage(List<Gamification_badge_user> poPage)   {
        if(poPage == null)
            return null;
        List<Gamification_badge_userDTO> dtos=new ArrayList<Gamification_badge_userDTO>();
        for(Gamification_badge_user domain : poPage) {
            Gamification_badge_userDTO dto = new Gamification_badge_userDTO();
            dto.fromDO(domain);
            dtos.add(dto);
        }
        return dtos;
    }
}

