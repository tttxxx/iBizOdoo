package cn.ibizlab.odoo.service.odoo_gamification.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_gamification.dto.Gamification_badge_user_wizardDTO;
import cn.ibizlab.odoo.core.odoo_gamification.domain.Gamification_badge_user_wizard;
import cn.ibizlab.odoo.core.odoo_gamification.service.IGamification_badge_user_wizardService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_gamification.filter.Gamification_badge_user_wizardSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Gamification_badge_user_wizard" })
@RestController
@RequestMapping("")
public class Gamification_badge_user_wizardResource {

    @Autowired
    private IGamification_badge_user_wizardService gamification_badge_user_wizardService;

    public IGamification_badge_user_wizardService getGamification_badge_user_wizardService() {
        return this.gamification_badge_user_wizardService;
    }

    @ApiOperation(value = "批更新数据", tags = {"Gamification_badge_user_wizard" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_gamification/gamification_badge_user_wizards/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Gamification_badge_user_wizardDTO> gamification_badge_user_wizarddtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "更新数据", tags = {"Gamification_badge_user_wizard" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_gamification/gamification_badge_user_wizards/{gamification_badge_user_wizard_id}")

    public ResponseEntity<Gamification_badge_user_wizardDTO> update(@PathVariable("gamification_badge_user_wizard_id") Integer gamification_badge_user_wizard_id, @RequestBody Gamification_badge_user_wizardDTO gamification_badge_user_wizarddto) {
		Gamification_badge_user_wizard domain = gamification_badge_user_wizarddto.toDO();
        domain.setId(gamification_badge_user_wizard_id);
		gamification_badge_user_wizardService.update(domain);
		Gamification_badge_user_wizardDTO dto = new Gamification_badge_user_wizardDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取数据", tags = {"Gamification_badge_user_wizard" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_gamification/gamification_badge_user_wizards/{gamification_badge_user_wizard_id}")
    public ResponseEntity<Gamification_badge_user_wizardDTO> get(@PathVariable("gamification_badge_user_wizard_id") Integer gamification_badge_user_wizard_id) {
        Gamification_badge_user_wizardDTO dto = new Gamification_badge_user_wizardDTO();
        Gamification_badge_user_wizard domain = gamification_badge_user_wizardService.get(gamification_badge_user_wizard_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "建立数据", tags = {"Gamification_badge_user_wizard" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_gamification/gamification_badge_user_wizards")

    public ResponseEntity<Gamification_badge_user_wizardDTO> create(@RequestBody Gamification_badge_user_wizardDTO gamification_badge_user_wizarddto) {
        Gamification_badge_user_wizardDTO dto = new Gamification_badge_user_wizardDTO();
        Gamification_badge_user_wizard domain = gamification_badge_user_wizarddto.toDO();
		gamification_badge_user_wizardService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "删除数据", tags = {"Gamification_badge_user_wizard" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_gamification/gamification_badge_user_wizards/{gamification_badge_user_wizard_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("gamification_badge_user_wizard_id") Integer gamification_badge_user_wizard_id) {
        Gamification_badge_user_wizardDTO gamification_badge_user_wizarddto = new Gamification_badge_user_wizardDTO();
		Gamification_badge_user_wizard domain = new Gamification_badge_user_wizard();
		gamification_badge_user_wizarddto.setId(gamification_badge_user_wizard_id);
		domain.setId(gamification_badge_user_wizard_id);
        Boolean rst = gamification_badge_user_wizardService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "批删除数据", tags = {"Gamification_badge_user_wizard" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_gamification/gamification_badge_user_wizards/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Gamification_badge_user_wizardDTO> gamification_badge_user_wizarddtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "批建立数据", tags = {"Gamification_badge_user_wizard" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_gamification/gamification_badge_user_wizards/createBatch")
    public ResponseEntity<Boolean> createBatchGamification_badge_user_wizard(@RequestBody List<Gamification_badge_user_wizardDTO> gamification_badge_user_wizarddtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Gamification_badge_user_wizard" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_gamification/gamification_badge_user_wizards/fetchdefault")
	public ResponseEntity<Page<Gamification_badge_user_wizardDTO>> fetchDefault(Gamification_badge_user_wizardSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Gamification_badge_user_wizardDTO> list = new ArrayList<Gamification_badge_user_wizardDTO>();
        
        Page<Gamification_badge_user_wizard> domains = gamification_badge_user_wizardService.searchDefault(context) ;
        for(Gamification_badge_user_wizard gamification_badge_user_wizard : domains.getContent()){
            Gamification_badge_user_wizardDTO dto = new Gamification_badge_user_wizardDTO();
            dto.fromDO(gamification_badge_user_wizard);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
