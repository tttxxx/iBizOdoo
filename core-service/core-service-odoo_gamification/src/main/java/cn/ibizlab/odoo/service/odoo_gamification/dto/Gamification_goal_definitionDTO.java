package cn.ibizlab.odoo.service.odoo_gamification.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.core.odoo_gamification.valuerule.anno.gamification_goal_definition.*;
import cn.ibizlab.odoo.core.odoo_gamification.domain.Gamification_goal_definition;
import org.springframework.cglib.beans.BeanCopier;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 服务DTO对象[Gamification_goal_definitionDTO]
 */
public class Gamification_goal_definitionDTO implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [SUFFIX]
     *
     */
    @Gamification_goal_definitionSuffixDefault(info = "默认规则")
    private String suffix;

    @JsonIgnore
    private boolean suffixDirtyFlag;

    /**
     * 属性 [BATCH_DISTINCTIVE_FIELD]
     *
     */
    @Gamification_goal_definitionBatch_distinctive_fieldDefault(info = "默认规则")
    private Integer batch_distinctive_field;

    @JsonIgnore
    private boolean batch_distinctive_fieldDirtyFlag;

    /**
     * 属性 [MONETARY]
     *
     */
    @Gamification_goal_definitionMonetaryDefault(info = "默认规则")
    private String monetary;

    @JsonIgnore
    private boolean monetaryDirtyFlag;

    /**
     * 属性 [CONDITION]
     *
     */
    @Gamification_goal_definitionConditionDefault(info = "默认规则")
    private String condition;

    @JsonIgnore
    private boolean conditionDirtyFlag;

    /**
     * 属性 [COMPUTE_CODE]
     *
     */
    @Gamification_goal_definitionCompute_codeDefault(info = "默认规则")
    private String compute_code;

    @JsonIgnore
    private boolean compute_codeDirtyFlag;

    /**
     * 属性 [COMPUTATION_MODE]
     *
     */
    @Gamification_goal_definitionComputation_modeDefault(info = "默认规则")
    private String computation_mode;

    @JsonIgnore
    private boolean computation_modeDirtyFlag;

    /**
     * 属性 [ACTION_ID]
     *
     */
    @Gamification_goal_definitionAction_idDefault(info = "默认规则")
    private Integer action_id;

    @JsonIgnore
    private boolean action_idDirtyFlag;

    /**
     * 属性 [ID]
     *
     */
    @Gamification_goal_definitionIdDefault(info = "默认规则")
    private Integer id;

    @JsonIgnore
    private boolean idDirtyFlag;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @Gamification_goal_definition__last_updateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp __last_update;

    @JsonIgnore
    private boolean __last_updateDirtyFlag;

    /**
     * 属性 [DOMAIN]
     *
     */
    @Gamification_goal_definitionDomainDefault(info = "默认规则")
    private String domain;

    @JsonIgnore
    private boolean domainDirtyFlag;

    /**
     * 属性 [WRITE_DATE]
     *
     */
    @Gamification_goal_definitionWrite_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp write_date;

    @JsonIgnore
    private boolean write_dateDirtyFlag;

    /**
     * 属性 [FIELD_ID]
     *
     */
    @Gamification_goal_definitionField_idDefault(info = "默认规则")
    private Integer field_id;

    @JsonIgnore
    private boolean field_idDirtyFlag;

    /**
     * 属性 [RES_ID_FIELD]
     *
     */
    @Gamification_goal_definitionRes_id_fieldDefault(info = "默认规则")
    private String res_id_field;

    @JsonIgnore
    private boolean res_id_fieldDirtyFlag;

    /**
     * 属性 [BATCH_MODE]
     *
     */
    @Gamification_goal_definitionBatch_modeDefault(info = "默认规则")
    private String batch_mode;

    @JsonIgnore
    private boolean batch_modeDirtyFlag;

    /**
     * 属性 [MODEL_ID]
     *
     */
    @Gamification_goal_definitionModel_idDefault(info = "默认规则")
    private Integer model_id;

    @JsonIgnore
    private boolean model_idDirtyFlag;

    /**
     * 属性 [DESCRIPTION]
     *
     */
    @Gamification_goal_definitionDescriptionDefault(info = "默认规则")
    private String description;

    @JsonIgnore
    private boolean descriptionDirtyFlag;

    /**
     * 属性 [FIELD_DATE_ID]
     *
     */
    @Gamification_goal_definitionField_date_idDefault(info = "默认规则")
    private Integer field_date_id;

    @JsonIgnore
    private boolean field_date_idDirtyFlag;

    /**
     * 属性 [DISPLAY_MODE]
     *
     */
    @Gamification_goal_definitionDisplay_modeDefault(info = "默认规则")
    private String display_mode;

    @JsonIgnore
    private boolean display_modeDirtyFlag;

    /**
     * 属性 [FULL_SUFFIX]
     *
     */
    @Gamification_goal_definitionFull_suffixDefault(info = "默认规则")
    private String full_suffix;

    @JsonIgnore
    private boolean full_suffixDirtyFlag;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @Gamification_goal_definitionDisplay_nameDefault(info = "默认规则")
    private String display_name;

    @JsonIgnore
    private boolean display_nameDirtyFlag;

    /**
     * 属性 [BATCH_USER_EXPRESSION]
     *
     */
    @Gamification_goal_definitionBatch_user_expressionDefault(info = "默认规则")
    private String batch_user_expression;

    @JsonIgnore
    private boolean batch_user_expressionDirtyFlag;

    /**
     * 属性 [CREATE_DATE]
     *
     */
    @Gamification_goal_definitionCreate_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp create_date;

    @JsonIgnore
    private boolean create_dateDirtyFlag;

    /**
     * 属性 [NAME]
     *
     */
    @Gamification_goal_definitionNameDefault(info = "默认规则")
    private String name;

    @JsonIgnore
    private boolean nameDirtyFlag;

    /**
     * 属性 [CREATE_UID_TEXT]
     *
     */
    @Gamification_goal_definitionCreate_uid_textDefault(info = "默认规则")
    private String create_uid_text;

    @JsonIgnore
    private boolean create_uid_textDirtyFlag;

    /**
     * 属性 [WRITE_UID_TEXT]
     *
     */
    @Gamification_goal_definitionWrite_uid_textDefault(info = "默认规则")
    private String write_uid_text;

    @JsonIgnore
    private boolean write_uid_textDirtyFlag;

    /**
     * 属性 [WRITE_UID]
     *
     */
    @Gamification_goal_definitionWrite_uidDefault(info = "默认规则")
    private Integer write_uid;

    @JsonIgnore
    private boolean write_uidDirtyFlag;

    /**
     * 属性 [CREATE_UID]
     *
     */
    @Gamification_goal_definitionCreate_uidDefault(info = "默认规则")
    private Integer create_uid;

    @JsonIgnore
    private boolean create_uidDirtyFlag;


    /**
     * 获取 [SUFFIX]
     */
    @JsonProperty("suffix")
    public String getSuffix(){
        return suffix ;
    }

    /**
     * 设置 [SUFFIX]
     */
    @JsonProperty("suffix")
    public void setSuffix(String  suffix){
        this.suffix = suffix ;
        this.suffixDirtyFlag = true ;
    }

    /**
     * 获取 [SUFFIX]脏标记
     */
    @JsonIgnore
    public boolean getSuffixDirtyFlag(){
        return suffixDirtyFlag ;
    }

    /**
     * 获取 [BATCH_DISTINCTIVE_FIELD]
     */
    @JsonProperty("batch_distinctive_field")
    public Integer getBatch_distinctive_field(){
        return batch_distinctive_field ;
    }

    /**
     * 设置 [BATCH_DISTINCTIVE_FIELD]
     */
    @JsonProperty("batch_distinctive_field")
    public void setBatch_distinctive_field(Integer  batch_distinctive_field){
        this.batch_distinctive_field = batch_distinctive_field ;
        this.batch_distinctive_fieldDirtyFlag = true ;
    }

    /**
     * 获取 [BATCH_DISTINCTIVE_FIELD]脏标记
     */
    @JsonIgnore
    public boolean getBatch_distinctive_fieldDirtyFlag(){
        return batch_distinctive_fieldDirtyFlag ;
    }

    /**
     * 获取 [MONETARY]
     */
    @JsonProperty("monetary")
    public String getMonetary(){
        return monetary ;
    }

    /**
     * 设置 [MONETARY]
     */
    @JsonProperty("monetary")
    public void setMonetary(String  monetary){
        this.monetary = monetary ;
        this.monetaryDirtyFlag = true ;
    }

    /**
     * 获取 [MONETARY]脏标记
     */
    @JsonIgnore
    public boolean getMonetaryDirtyFlag(){
        return monetaryDirtyFlag ;
    }

    /**
     * 获取 [CONDITION]
     */
    @JsonProperty("condition")
    public String getCondition(){
        return condition ;
    }

    /**
     * 设置 [CONDITION]
     */
    @JsonProperty("condition")
    public void setCondition(String  condition){
        this.condition = condition ;
        this.conditionDirtyFlag = true ;
    }

    /**
     * 获取 [CONDITION]脏标记
     */
    @JsonIgnore
    public boolean getConditionDirtyFlag(){
        return conditionDirtyFlag ;
    }

    /**
     * 获取 [COMPUTE_CODE]
     */
    @JsonProperty("compute_code")
    public String getCompute_code(){
        return compute_code ;
    }

    /**
     * 设置 [COMPUTE_CODE]
     */
    @JsonProperty("compute_code")
    public void setCompute_code(String  compute_code){
        this.compute_code = compute_code ;
        this.compute_codeDirtyFlag = true ;
    }

    /**
     * 获取 [COMPUTE_CODE]脏标记
     */
    @JsonIgnore
    public boolean getCompute_codeDirtyFlag(){
        return compute_codeDirtyFlag ;
    }

    /**
     * 获取 [COMPUTATION_MODE]
     */
    @JsonProperty("computation_mode")
    public String getComputation_mode(){
        return computation_mode ;
    }

    /**
     * 设置 [COMPUTATION_MODE]
     */
    @JsonProperty("computation_mode")
    public void setComputation_mode(String  computation_mode){
        this.computation_mode = computation_mode ;
        this.computation_modeDirtyFlag = true ;
    }

    /**
     * 获取 [COMPUTATION_MODE]脏标记
     */
    @JsonIgnore
    public boolean getComputation_modeDirtyFlag(){
        return computation_modeDirtyFlag ;
    }

    /**
     * 获取 [ACTION_ID]
     */
    @JsonProperty("action_id")
    public Integer getAction_id(){
        return action_id ;
    }

    /**
     * 设置 [ACTION_ID]
     */
    @JsonProperty("action_id")
    public void setAction_id(Integer  action_id){
        this.action_id = action_id ;
        this.action_idDirtyFlag = true ;
    }

    /**
     * 获取 [ACTION_ID]脏标记
     */
    @JsonIgnore
    public boolean getAction_idDirtyFlag(){
        return action_idDirtyFlag ;
    }

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

    /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return idDirtyFlag ;
    }

    /**
     * 获取 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return __last_update ;
    }

    /**
     * 设置 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

    /**
     * 获取 [__LAST_UPDATE]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return __last_updateDirtyFlag ;
    }

    /**
     * 获取 [DOMAIN]
     */
    @JsonProperty("domain")
    public String getDomain(){
        return domain ;
    }

    /**
     * 设置 [DOMAIN]
     */
    @JsonProperty("domain")
    public void setDomain(String  domain){
        this.domain = domain ;
        this.domainDirtyFlag = true ;
    }

    /**
     * 获取 [DOMAIN]脏标记
     */
    @JsonIgnore
    public boolean getDomainDirtyFlag(){
        return domainDirtyFlag ;
    }

    /**
     * 获取 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return write_date ;
    }

    /**
     * 设置 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return write_dateDirtyFlag ;
    }

    /**
     * 获取 [FIELD_ID]
     */
    @JsonProperty("field_id")
    public Integer getField_id(){
        return field_id ;
    }

    /**
     * 设置 [FIELD_ID]
     */
    @JsonProperty("field_id")
    public void setField_id(Integer  field_id){
        this.field_id = field_id ;
        this.field_idDirtyFlag = true ;
    }

    /**
     * 获取 [FIELD_ID]脏标记
     */
    @JsonIgnore
    public boolean getField_idDirtyFlag(){
        return field_idDirtyFlag ;
    }

    /**
     * 获取 [RES_ID_FIELD]
     */
    @JsonProperty("res_id_field")
    public String getRes_id_field(){
        return res_id_field ;
    }

    /**
     * 设置 [RES_ID_FIELD]
     */
    @JsonProperty("res_id_field")
    public void setRes_id_field(String  res_id_field){
        this.res_id_field = res_id_field ;
        this.res_id_fieldDirtyFlag = true ;
    }

    /**
     * 获取 [RES_ID_FIELD]脏标记
     */
    @JsonIgnore
    public boolean getRes_id_fieldDirtyFlag(){
        return res_id_fieldDirtyFlag ;
    }

    /**
     * 获取 [BATCH_MODE]
     */
    @JsonProperty("batch_mode")
    public String getBatch_mode(){
        return batch_mode ;
    }

    /**
     * 设置 [BATCH_MODE]
     */
    @JsonProperty("batch_mode")
    public void setBatch_mode(String  batch_mode){
        this.batch_mode = batch_mode ;
        this.batch_modeDirtyFlag = true ;
    }

    /**
     * 获取 [BATCH_MODE]脏标记
     */
    @JsonIgnore
    public boolean getBatch_modeDirtyFlag(){
        return batch_modeDirtyFlag ;
    }

    /**
     * 获取 [MODEL_ID]
     */
    @JsonProperty("model_id")
    public Integer getModel_id(){
        return model_id ;
    }

    /**
     * 设置 [MODEL_ID]
     */
    @JsonProperty("model_id")
    public void setModel_id(Integer  model_id){
        this.model_id = model_id ;
        this.model_idDirtyFlag = true ;
    }

    /**
     * 获取 [MODEL_ID]脏标记
     */
    @JsonIgnore
    public boolean getModel_idDirtyFlag(){
        return model_idDirtyFlag ;
    }

    /**
     * 获取 [DESCRIPTION]
     */
    @JsonProperty("description")
    public String getDescription(){
        return description ;
    }

    /**
     * 设置 [DESCRIPTION]
     */
    @JsonProperty("description")
    public void setDescription(String  description){
        this.description = description ;
        this.descriptionDirtyFlag = true ;
    }

    /**
     * 获取 [DESCRIPTION]脏标记
     */
    @JsonIgnore
    public boolean getDescriptionDirtyFlag(){
        return descriptionDirtyFlag ;
    }

    /**
     * 获取 [FIELD_DATE_ID]
     */
    @JsonProperty("field_date_id")
    public Integer getField_date_id(){
        return field_date_id ;
    }

    /**
     * 设置 [FIELD_DATE_ID]
     */
    @JsonProperty("field_date_id")
    public void setField_date_id(Integer  field_date_id){
        this.field_date_id = field_date_id ;
        this.field_date_idDirtyFlag = true ;
    }

    /**
     * 获取 [FIELD_DATE_ID]脏标记
     */
    @JsonIgnore
    public boolean getField_date_idDirtyFlag(){
        return field_date_idDirtyFlag ;
    }

    /**
     * 获取 [DISPLAY_MODE]
     */
    @JsonProperty("display_mode")
    public String getDisplay_mode(){
        return display_mode ;
    }

    /**
     * 设置 [DISPLAY_MODE]
     */
    @JsonProperty("display_mode")
    public void setDisplay_mode(String  display_mode){
        this.display_mode = display_mode ;
        this.display_modeDirtyFlag = true ;
    }

    /**
     * 获取 [DISPLAY_MODE]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_modeDirtyFlag(){
        return display_modeDirtyFlag ;
    }

    /**
     * 获取 [FULL_SUFFIX]
     */
    @JsonProperty("full_suffix")
    public String getFull_suffix(){
        return full_suffix ;
    }

    /**
     * 设置 [FULL_SUFFIX]
     */
    @JsonProperty("full_suffix")
    public void setFull_suffix(String  full_suffix){
        this.full_suffix = full_suffix ;
        this.full_suffixDirtyFlag = true ;
    }

    /**
     * 获取 [FULL_SUFFIX]脏标记
     */
    @JsonIgnore
    public boolean getFull_suffixDirtyFlag(){
        return full_suffixDirtyFlag ;
    }

    /**
     * 获取 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return display_name ;
    }

    /**
     * 设置 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

    /**
     * 获取 [DISPLAY_NAME]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return display_nameDirtyFlag ;
    }

    /**
     * 获取 [BATCH_USER_EXPRESSION]
     */
    @JsonProperty("batch_user_expression")
    public String getBatch_user_expression(){
        return batch_user_expression ;
    }

    /**
     * 设置 [BATCH_USER_EXPRESSION]
     */
    @JsonProperty("batch_user_expression")
    public void setBatch_user_expression(String  batch_user_expression){
        this.batch_user_expression = batch_user_expression ;
        this.batch_user_expressionDirtyFlag = true ;
    }

    /**
     * 获取 [BATCH_USER_EXPRESSION]脏标记
     */
    @JsonIgnore
    public boolean getBatch_user_expressionDirtyFlag(){
        return batch_user_expressionDirtyFlag ;
    }

    /**
     * 获取 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return create_date ;
    }

    /**
     * 设置 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return create_dateDirtyFlag ;
    }

    /**
     * 获取 [NAME]
     */
    @JsonProperty("name")
    public String getName(){
        return name ;
    }

    /**
     * 设置 [NAME]
     */
    @JsonProperty("name")
    public void setName(String  name){
        this.name = name ;
        this.nameDirtyFlag = true ;
    }

    /**
     * 获取 [NAME]脏标记
     */
    @JsonIgnore
    public boolean getNameDirtyFlag(){
        return nameDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return create_uid_text ;
    }

    /**
     * 设置 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return create_uid_textDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return write_uid_text ;
    }

    /**
     * 设置 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return write_uid_textDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return write_uid ;
    }

    /**
     * 设置 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return write_uidDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return create_uid ;
    }

    /**
     * 设置 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return create_uidDirtyFlag ;
    }



    public Gamification_goal_definition toDO() {
        Gamification_goal_definition srfdomain = new Gamification_goal_definition();
        if(getSuffixDirtyFlag())
            srfdomain.setSuffix(suffix);
        if(getBatch_distinctive_fieldDirtyFlag())
            srfdomain.setBatch_distinctive_field(batch_distinctive_field);
        if(getMonetaryDirtyFlag())
            srfdomain.setMonetary(monetary);
        if(getConditionDirtyFlag())
            srfdomain.setCondition(condition);
        if(getCompute_codeDirtyFlag())
            srfdomain.setCompute_code(compute_code);
        if(getComputation_modeDirtyFlag())
            srfdomain.setComputation_mode(computation_mode);
        if(getAction_idDirtyFlag())
            srfdomain.setAction_id(action_id);
        if(getIdDirtyFlag())
            srfdomain.setId(id);
        if(get__last_updateDirtyFlag())
            srfdomain.set__last_update(__last_update);
        if(getDomainDirtyFlag())
            srfdomain.setDomain(domain);
        if(getWrite_dateDirtyFlag())
            srfdomain.setWrite_date(write_date);
        if(getField_idDirtyFlag())
            srfdomain.setField_id(field_id);
        if(getRes_id_fieldDirtyFlag())
            srfdomain.setRes_id_field(res_id_field);
        if(getBatch_modeDirtyFlag())
            srfdomain.setBatch_mode(batch_mode);
        if(getModel_idDirtyFlag())
            srfdomain.setModel_id(model_id);
        if(getDescriptionDirtyFlag())
            srfdomain.setDescription(description);
        if(getField_date_idDirtyFlag())
            srfdomain.setField_date_id(field_date_id);
        if(getDisplay_modeDirtyFlag())
            srfdomain.setDisplay_mode(display_mode);
        if(getFull_suffixDirtyFlag())
            srfdomain.setFull_suffix(full_suffix);
        if(getDisplay_nameDirtyFlag())
            srfdomain.setDisplay_name(display_name);
        if(getBatch_user_expressionDirtyFlag())
            srfdomain.setBatch_user_expression(batch_user_expression);
        if(getCreate_dateDirtyFlag())
            srfdomain.setCreate_date(create_date);
        if(getNameDirtyFlag())
            srfdomain.setName(name);
        if(getCreate_uid_textDirtyFlag())
            srfdomain.setCreate_uid_text(create_uid_text);
        if(getWrite_uid_textDirtyFlag())
            srfdomain.setWrite_uid_text(write_uid_text);
        if(getWrite_uidDirtyFlag())
            srfdomain.setWrite_uid(write_uid);
        if(getCreate_uidDirtyFlag())
            srfdomain.setCreate_uid(create_uid);

        return srfdomain;
    }

    public void fromDO(Gamification_goal_definition srfdomain) {
        if(srfdomain == null )
          return ;
        if(srfdomain.getSuffixDirtyFlag())
            this.setSuffix(srfdomain.getSuffix());
        if(srfdomain.getBatch_distinctive_fieldDirtyFlag())
            this.setBatch_distinctive_field(srfdomain.getBatch_distinctive_field());
        if(srfdomain.getMonetaryDirtyFlag())
            this.setMonetary(srfdomain.getMonetary());
        if(srfdomain.getConditionDirtyFlag())
            this.setCondition(srfdomain.getCondition());
        if(srfdomain.getCompute_codeDirtyFlag())
            this.setCompute_code(srfdomain.getCompute_code());
        if(srfdomain.getComputation_modeDirtyFlag())
            this.setComputation_mode(srfdomain.getComputation_mode());
        if(srfdomain.getAction_idDirtyFlag())
            this.setAction_id(srfdomain.getAction_id());
        if(srfdomain.getIdDirtyFlag())
            this.setId(srfdomain.getId());
        if(srfdomain.get__last_updateDirtyFlag())
            this.set__last_update(srfdomain.get__last_update());
        if(srfdomain.getDomainDirtyFlag())
            this.setDomain(srfdomain.getDomain());
        if(srfdomain.getWrite_dateDirtyFlag())
            this.setWrite_date(srfdomain.getWrite_date());
        if(srfdomain.getField_idDirtyFlag())
            this.setField_id(srfdomain.getField_id());
        if(srfdomain.getRes_id_fieldDirtyFlag())
            this.setRes_id_field(srfdomain.getRes_id_field());
        if(srfdomain.getBatch_modeDirtyFlag())
            this.setBatch_mode(srfdomain.getBatch_mode());
        if(srfdomain.getModel_idDirtyFlag())
            this.setModel_id(srfdomain.getModel_id());
        if(srfdomain.getDescriptionDirtyFlag())
            this.setDescription(srfdomain.getDescription());
        if(srfdomain.getField_date_idDirtyFlag())
            this.setField_date_id(srfdomain.getField_date_id());
        if(srfdomain.getDisplay_modeDirtyFlag())
            this.setDisplay_mode(srfdomain.getDisplay_mode());
        if(srfdomain.getFull_suffixDirtyFlag())
            this.setFull_suffix(srfdomain.getFull_suffix());
        if(srfdomain.getDisplay_nameDirtyFlag())
            this.setDisplay_name(srfdomain.getDisplay_name());
        if(srfdomain.getBatch_user_expressionDirtyFlag())
            this.setBatch_user_expression(srfdomain.getBatch_user_expression());
        if(srfdomain.getCreate_dateDirtyFlag())
            this.setCreate_date(srfdomain.getCreate_date());
        if(srfdomain.getNameDirtyFlag())
            this.setName(srfdomain.getName());
        if(srfdomain.getCreate_uid_textDirtyFlag())
            this.setCreate_uid_text(srfdomain.getCreate_uid_text());
        if(srfdomain.getWrite_uid_textDirtyFlag())
            this.setWrite_uid_text(srfdomain.getWrite_uid_text());
        if(srfdomain.getWrite_uidDirtyFlag())
            this.setWrite_uid(srfdomain.getWrite_uid());
        if(srfdomain.getCreate_uidDirtyFlag())
            this.setCreate_uid(srfdomain.getCreate_uid());

    }

    public List<Gamification_goal_definitionDTO> fromDOPage(List<Gamification_goal_definition> poPage)   {
        if(poPage == null)
            return null;
        List<Gamification_goal_definitionDTO> dtos=new ArrayList<Gamification_goal_definitionDTO>();
        for(Gamification_goal_definition domain : poPage) {
            Gamification_goal_definitionDTO dto = new Gamification_goal_definitionDTO();
            dto.fromDO(domain);
            dtos.add(dto);
        }
        return dtos;
    }
}

