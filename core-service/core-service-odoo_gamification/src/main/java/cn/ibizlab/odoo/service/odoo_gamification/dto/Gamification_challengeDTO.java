package cn.ibizlab.odoo.service.odoo_gamification.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.core.odoo_gamification.valuerule.anno.gamification_challenge.*;
import cn.ibizlab.odoo.core.odoo_gamification.domain.Gamification_challenge;
import org.springframework.cglib.beans.BeanCopier;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 服务DTO对象[Gamification_challengeDTO]
 */
public class Gamification_challengeDTO implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [WEBSITE_MESSAGE_IDS]
     *
     */
    @Gamification_challengeWebsite_message_idsDefault(info = "默认规则")
    private String website_message_ids;

    @JsonIgnore
    private boolean website_message_idsDirtyFlag;

    /**
     * 属性 [NAME]
     *
     */
    @Gamification_challengeNameDefault(info = "默认规则")
    private String name;

    @JsonIgnore
    private boolean nameDirtyFlag;

    /**
     * 属性 [MESSAGE_NEEDACTION]
     *
     */
    @Gamification_challengeMessage_needactionDefault(info = "默认规则")
    private String message_needaction;

    @JsonIgnore
    private boolean message_needactionDirtyFlag;

    /**
     * 属性 [ID]
     *
     */
    @Gamification_challengeIdDefault(info = "默认规则")
    private Integer id;

    @JsonIgnore
    private boolean idDirtyFlag;

    /**
     * 属性 [CREATE_DATE]
     *
     */
    @Gamification_challengeCreate_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp create_date;

    @JsonIgnore
    private boolean create_dateDirtyFlag;

    /**
     * 属性 [MESSAGE_UNREAD]
     *
     */
    @Gamification_challengeMessage_unreadDefault(info = "默认规则")
    private String message_unread;

    @JsonIgnore
    private boolean message_unreadDirtyFlag;

    /**
     * 属性 [START_DATE]
     *
     */
    @Gamification_challengeStart_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    private Timestamp start_date;

    @JsonIgnore
    private boolean start_dateDirtyFlag;

    /**
     * 属性 [MESSAGE_HAS_ERROR_COUNTER]
     *
     */
    @Gamification_challengeMessage_has_error_counterDefault(info = "默认规则")
    private Integer message_has_error_counter;

    @JsonIgnore
    private boolean message_has_error_counterDirtyFlag;

    /**
     * 属性 [MESSAGE_MAIN_ATTACHMENT_ID]
     *
     */
    @Gamification_challengeMessage_main_attachment_idDefault(info = "默认规则")
    private Integer message_main_attachment_id;

    @JsonIgnore
    private boolean message_main_attachment_idDirtyFlag;

    /**
     * 属性 [MESSAGE_HAS_ERROR]
     *
     */
    @Gamification_challengeMessage_has_errorDefault(info = "默认规则")
    private String message_has_error;

    @JsonIgnore
    private boolean message_has_errorDirtyFlag;

    /**
     * 属性 [LAST_REPORT_DATE]
     *
     */
    @Gamification_challengeLast_report_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    private Timestamp last_report_date;

    @JsonIgnore
    private boolean last_report_dateDirtyFlag;

    /**
     * 属性 [END_DATE]
     *
     */
    @Gamification_challengeEnd_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    private Timestamp end_date;

    @JsonIgnore
    private boolean end_dateDirtyFlag;

    /**
     * 属性 [MESSAGE_FOLLOWER_IDS]
     *
     */
    @Gamification_challengeMessage_follower_idsDefault(info = "默认规则")
    private String message_follower_ids;

    @JsonIgnore
    private boolean message_follower_idsDirtyFlag;

    /**
     * 属性 [MESSAGE_ATTACHMENT_COUNT]
     *
     */
    @Gamification_challengeMessage_attachment_countDefault(info = "默认规则")
    private Integer message_attachment_count;

    @JsonIgnore
    private boolean message_attachment_countDirtyFlag;

    /**
     * 属性 [MESSAGE_IS_FOLLOWER]
     *
     */
    @Gamification_challengeMessage_is_followerDefault(info = "默认规则")
    private String message_is_follower;

    @JsonIgnore
    private boolean message_is_followerDirtyFlag;

    /**
     * 属性 [USER_DOMAIN]
     *
     */
    @Gamification_challengeUser_domainDefault(info = "默认规则")
    private String user_domain;

    @JsonIgnore
    private boolean user_domainDirtyFlag;

    /**
     * 属性 [CATEGORY]
     *
     */
    @Gamification_challengeCategoryDefault(info = "默认规则")
    private String category;

    @JsonIgnore
    private boolean categoryDirtyFlag;

    /**
     * 属性 [DESCRIPTION]
     *
     */
    @Gamification_challengeDescriptionDefault(info = "默认规则")
    private String description;

    @JsonIgnore
    private boolean descriptionDirtyFlag;

    /**
     * 属性 [USER_IDS]
     *
     */
    @Gamification_challengeUser_idsDefault(info = "默认规则")
    private String user_ids;

    @JsonIgnore
    private boolean user_idsDirtyFlag;

    /**
     * 属性 [MESSAGE_UNREAD_COUNTER]
     *
     */
    @Gamification_challengeMessage_unread_counterDefault(info = "默认规则")
    private Integer message_unread_counter;

    @JsonIgnore
    private boolean message_unread_counterDirtyFlag;

    /**
     * 属性 [LINE_IDS]
     *
     */
    @Gamification_challengeLine_idsDefault(info = "默认规则")
    private String line_ids;

    @JsonIgnore
    private boolean line_idsDirtyFlag;

    /**
     * 属性 [REPORT_MESSAGE_FREQUENCY]
     *
     */
    @Gamification_challengeReport_message_frequencyDefault(info = "默认规则")
    private String report_message_frequency;

    @JsonIgnore
    private boolean report_message_frequencyDirtyFlag;

    /**
     * 属性 [MESSAGE_PARTNER_IDS]
     *
     */
    @Gamification_challengeMessage_partner_idsDefault(info = "默认规则")
    private String message_partner_ids;

    @JsonIgnore
    private boolean message_partner_idsDirtyFlag;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @Gamification_challengeDisplay_nameDefault(info = "默认规则")
    private String display_name;

    @JsonIgnore
    private boolean display_nameDirtyFlag;

    /**
     * 属性 [MESSAGE_CHANNEL_IDS]
     *
     */
    @Gamification_challengeMessage_channel_idsDefault(info = "默认规则")
    private String message_channel_ids;

    @JsonIgnore
    private boolean message_channel_idsDirtyFlag;

    /**
     * 属性 [MESSAGE_IDS]
     *
     */
    @Gamification_challengeMessage_idsDefault(info = "默认规则")
    private String message_ids;

    @JsonIgnore
    private boolean message_idsDirtyFlag;

    /**
     * 属性 [REWARD_REALTIME]
     *
     */
    @Gamification_challengeReward_realtimeDefault(info = "默认规则")
    private String reward_realtime;

    @JsonIgnore
    private boolean reward_realtimeDirtyFlag;

    /**
     * 属性 [REWARD_FAILURE]
     *
     */
    @Gamification_challengeReward_failureDefault(info = "默认规则")
    private String reward_failure;

    @JsonIgnore
    private boolean reward_failureDirtyFlag;

    /**
     * 属性 [PERIOD]
     *
     */
    @Gamification_challengePeriodDefault(info = "默认规则")
    private String period;

    @JsonIgnore
    private boolean periodDirtyFlag;

    /**
     * 属性 [VISIBILITY_MODE]
     *
     */
    @Gamification_challengeVisibility_modeDefault(info = "默认规则")
    private String visibility_mode;

    @JsonIgnore
    private boolean visibility_modeDirtyFlag;

    /**
     * 属性 [REMIND_UPDATE_DELAY]
     *
     */
    @Gamification_challengeRemind_update_delayDefault(info = "默认规则")
    private Integer remind_update_delay;

    @JsonIgnore
    private boolean remind_update_delayDirtyFlag;

    /**
     * 属性 [WRITE_DATE]
     *
     */
    @Gamification_challengeWrite_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp write_date;

    @JsonIgnore
    private boolean write_dateDirtyFlag;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @Gamification_challenge__last_updateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp __last_update;

    @JsonIgnore
    private boolean __last_updateDirtyFlag;

    /**
     * 属性 [INVITED_USER_IDS]
     *
     */
    @Gamification_challengeInvited_user_idsDefault(info = "默认规则")
    private String invited_user_ids;

    @JsonIgnore
    private boolean invited_user_idsDirtyFlag;

    /**
     * 属性 [MESSAGE_NEEDACTION_COUNTER]
     *
     */
    @Gamification_challengeMessage_needaction_counterDefault(info = "默认规则")
    private Integer message_needaction_counter;

    @JsonIgnore
    private boolean message_needaction_counterDirtyFlag;

    /**
     * 属性 [STATE]
     *
     */
    @Gamification_challengeStateDefault(info = "默认规则")
    private String state;

    @JsonIgnore
    private boolean stateDirtyFlag;

    /**
     * 属性 [NEXT_REPORT_DATE]
     *
     */
    @Gamification_challengeNext_report_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    private Timestamp next_report_date;

    @JsonIgnore
    private boolean next_report_dateDirtyFlag;

    /**
     * 属性 [MANAGER_ID_TEXT]
     *
     */
    @Gamification_challengeManager_id_textDefault(info = "默认规则")
    private String manager_id_text;

    @JsonIgnore
    private boolean manager_id_textDirtyFlag;

    /**
     * 属性 [REPORT_MESSAGE_GROUP_ID_TEXT]
     *
     */
    @Gamification_challengeReport_message_group_id_textDefault(info = "默认规则")
    private String report_message_group_id_text;

    @JsonIgnore
    private boolean report_message_group_id_textDirtyFlag;

    /**
     * 属性 [REPORT_TEMPLATE_ID_TEXT]
     *
     */
    @Gamification_challengeReport_template_id_textDefault(info = "默认规则")
    private String report_template_id_text;

    @JsonIgnore
    private boolean report_template_id_textDirtyFlag;

    /**
     * 属性 [REWARD_ID_TEXT]
     *
     */
    @Gamification_challengeReward_id_textDefault(info = "默认规则")
    private String reward_id_text;

    @JsonIgnore
    private boolean reward_id_textDirtyFlag;

    /**
     * 属性 [REWARD_FIRST_ID_TEXT]
     *
     */
    @Gamification_challengeReward_first_id_textDefault(info = "默认规则")
    private String reward_first_id_text;

    @JsonIgnore
    private boolean reward_first_id_textDirtyFlag;

    /**
     * 属性 [CREATE_UID_TEXT]
     *
     */
    @Gamification_challengeCreate_uid_textDefault(info = "默认规则")
    private String create_uid_text;

    @JsonIgnore
    private boolean create_uid_textDirtyFlag;

    /**
     * 属性 [WRITE_UID_TEXT]
     *
     */
    @Gamification_challengeWrite_uid_textDefault(info = "默认规则")
    private String write_uid_text;

    @JsonIgnore
    private boolean write_uid_textDirtyFlag;

    /**
     * 属性 [REWARD_SECOND_ID_TEXT]
     *
     */
    @Gamification_challengeReward_second_id_textDefault(info = "默认规则")
    private String reward_second_id_text;

    @JsonIgnore
    private boolean reward_second_id_textDirtyFlag;

    /**
     * 属性 [REWARD_THIRD_ID_TEXT]
     *
     */
    @Gamification_challengeReward_third_id_textDefault(info = "默认规则")
    private String reward_third_id_text;

    @JsonIgnore
    private boolean reward_third_id_textDirtyFlag;

    /**
     * 属性 [REWARD_FIRST_ID]
     *
     */
    @Gamification_challengeReward_first_idDefault(info = "默认规则")
    private Integer reward_first_id;

    @JsonIgnore
    private boolean reward_first_idDirtyFlag;

    /**
     * 属性 [REPORT_TEMPLATE_ID]
     *
     */
    @Gamification_challengeReport_template_idDefault(info = "默认规则")
    private Integer report_template_id;

    @JsonIgnore
    private boolean report_template_idDirtyFlag;

    /**
     * 属性 [REWARD_SECOND_ID]
     *
     */
    @Gamification_challengeReward_second_idDefault(info = "默认规则")
    private Integer reward_second_id;

    @JsonIgnore
    private boolean reward_second_idDirtyFlag;

    /**
     * 属性 [REWARD_THIRD_ID]
     *
     */
    @Gamification_challengeReward_third_idDefault(info = "默认规则")
    private Integer reward_third_id;

    @JsonIgnore
    private boolean reward_third_idDirtyFlag;

    /**
     * 属性 [REPORT_MESSAGE_GROUP_ID]
     *
     */
    @Gamification_challengeReport_message_group_idDefault(info = "默认规则")
    private Integer report_message_group_id;

    @JsonIgnore
    private boolean report_message_group_idDirtyFlag;

    /**
     * 属性 [REWARD_ID]
     *
     */
    @Gamification_challengeReward_idDefault(info = "默认规则")
    private Integer reward_id;

    @JsonIgnore
    private boolean reward_idDirtyFlag;

    /**
     * 属性 [MANAGER_ID]
     *
     */
    @Gamification_challengeManager_idDefault(info = "默认规则")
    private Integer manager_id;

    @JsonIgnore
    private boolean manager_idDirtyFlag;

    /**
     * 属性 [WRITE_UID]
     *
     */
    @Gamification_challengeWrite_uidDefault(info = "默认规则")
    private Integer write_uid;

    @JsonIgnore
    private boolean write_uidDirtyFlag;

    /**
     * 属性 [CREATE_UID]
     *
     */
    @Gamification_challengeCreate_uidDefault(info = "默认规则")
    private Integer create_uid;

    @JsonIgnore
    private boolean create_uidDirtyFlag;


    /**
     * 获取 [WEBSITE_MESSAGE_IDS]
     */
    @JsonProperty("website_message_ids")
    public String getWebsite_message_ids(){
        return website_message_ids ;
    }

    /**
     * 设置 [WEBSITE_MESSAGE_IDS]
     */
    @JsonProperty("website_message_ids")
    public void setWebsite_message_ids(String  website_message_ids){
        this.website_message_ids = website_message_ids ;
        this.website_message_idsDirtyFlag = true ;
    }

    /**
     * 获取 [WEBSITE_MESSAGE_IDS]脏标记
     */
    @JsonIgnore
    public boolean getWebsite_message_idsDirtyFlag(){
        return website_message_idsDirtyFlag ;
    }

    /**
     * 获取 [NAME]
     */
    @JsonProperty("name")
    public String getName(){
        return name ;
    }

    /**
     * 设置 [NAME]
     */
    @JsonProperty("name")
    public void setName(String  name){
        this.name = name ;
        this.nameDirtyFlag = true ;
    }

    /**
     * 获取 [NAME]脏标记
     */
    @JsonIgnore
    public boolean getNameDirtyFlag(){
        return nameDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_NEEDACTION]
     */
    @JsonProperty("message_needaction")
    public String getMessage_needaction(){
        return message_needaction ;
    }

    /**
     * 设置 [MESSAGE_NEEDACTION]
     */
    @JsonProperty("message_needaction")
    public void setMessage_needaction(String  message_needaction){
        this.message_needaction = message_needaction ;
        this.message_needactionDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_NEEDACTION]脏标记
     */
    @JsonIgnore
    public boolean getMessage_needactionDirtyFlag(){
        return message_needactionDirtyFlag ;
    }

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

    /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return idDirtyFlag ;
    }

    /**
     * 获取 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return create_date ;
    }

    /**
     * 设置 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return create_dateDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_UNREAD]
     */
    @JsonProperty("message_unread")
    public String getMessage_unread(){
        return message_unread ;
    }

    /**
     * 设置 [MESSAGE_UNREAD]
     */
    @JsonProperty("message_unread")
    public void setMessage_unread(String  message_unread){
        this.message_unread = message_unread ;
        this.message_unreadDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_UNREAD]脏标记
     */
    @JsonIgnore
    public boolean getMessage_unreadDirtyFlag(){
        return message_unreadDirtyFlag ;
    }

    /**
     * 获取 [START_DATE]
     */
    @JsonProperty("start_date")
    public Timestamp getStart_date(){
        return start_date ;
    }

    /**
     * 设置 [START_DATE]
     */
    @JsonProperty("start_date")
    public void setStart_date(Timestamp  start_date){
        this.start_date = start_date ;
        this.start_dateDirtyFlag = true ;
    }

    /**
     * 获取 [START_DATE]脏标记
     */
    @JsonIgnore
    public boolean getStart_dateDirtyFlag(){
        return start_dateDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_HAS_ERROR_COUNTER]
     */
    @JsonProperty("message_has_error_counter")
    public Integer getMessage_has_error_counter(){
        return message_has_error_counter ;
    }

    /**
     * 设置 [MESSAGE_HAS_ERROR_COUNTER]
     */
    @JsonProperty("message_has_error_counter")
    public void setMessage_has_error_counter(Integer  message_has_error_counter){
        this.message_has_error_counter = message_has_error_counter ;
        this.message_has_error_counterDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_HAS_ERROR_COUNTER]脏标记
     */
    @JsonIgnore
    public boolean getMessage_has_error_counterDirtyFlag(){
        return message_has_error_counterDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_MAIN_ATTACHMENT_ID]
     */
    @JsonProperty("message_main_attachment_id")
    public Integer getMessage_main_attachment_id(){
        return message_main_attachment_id ;
    }

    /**
     * 设置 [MESSAGE_MAIN_ATTACHMENT_ID]
     */
    @JsonProperty("message_main_attachment_id")
    public void setMessage_main_attachment_id(Integer  message_main_attachment_id){
        this.message_main_attachment_id = message_main_attachment_id ;
        this.message_main_attachment_idDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_MAIN_ATTACHMENT_ID]脏标记
     */
    @JsonIgnore
    public boolean getMessage_main_attachment_idDirtyFlag(){
        return message_main_attachment_idDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_HAS_ERROR]
     */
    @JsonProperty("message_has_error")
    public String getMessage_has_error(){
        return message_has_error ;
    }

    /**
     * 设置 [MESSAGE_HAS_ERROR]
     */
    @JsonProperty("message_has_error")
    public void setMessage_has_error(String  message_has_error){
        this.message_has_error = message_has_error ;
        this.message_has_errorDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_HAS_ERROR]脏标记
     */
    @JsonIgnore
    public boolean getMessage_has_errorDirtyFlag(){
        return message_has_errorDirtyFlag ;
    }

    /**
     * 获取 [LAST_REPORT_DATE]
     */
    @JsonProperty("last_report_date")
    public Timestamp getLast_report_date(){
        return last_report_date ;
    }

    /**
     * 设置 [LAST_REPORT_DATE]
     */
    @JsonProperty("last_report_date")
    public void setLast_report_date(Timestamp  last_report_date){
        this.last_report_date = last_report_date ;
        this.last_report_dateDirtyFlag = true ;
    }

    /**
     * 获取 [LAST_REPORT_DATE]脏标记
     */
    @JsonIgnore
    public boolean getLast_report_dateDirtyFlag(){
        return last_report_dateDirtyFlag ;
    }

    /**
     * 获取 [END_DATE]
     */
    @JsonProperty("end_date")
    public Timestamp getEnd_date(){
        return end_date ;
    }

    /**
     * 设置 [END_DATE]
     */
    @JsonProperty("end_date")
    public void setEnd_date(Timestamp  end_date){
        this.end_date = end_date ;
        this.end_dateDirtyFlag = true ;
    }

    /**
     * 获取 [END_DATE]脏标记
     */
    @JsonIgnore
    public boolean getEnd_dateDirtyFlag(){
        return end_dateDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_FOLLOWER_IDS]
     */
    @JsonProperty("message_follower_ids")
    public String getMessage_follower_ids(){
        return message_follower_ids ;
    }

    /**
     * 设置 [MESSAGE_FOLLOWER_IDS]
     */
    @JsonProperty("message_follower_ids")
    public void setMessage_follower_ids(String  message_follower_ids){
        this.message_follower_ids = message_follower_ids ;
        this.message_follower_idsDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_FOLLOWER_IDS]脏标记
     */
    @JsonIgnore
    public boolean getMessage_follower_idsDirtyFlag(){
        return message_follower_idsDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_ATTACHMENT_COUNT]
     */
    @JsonProperty("message_attachment_count")
    public Integer getMessage_attachment_count(){
        return message_attachment_count ;
    }

    /**
     * 设置 [MESSAGE_ATTACHMENT_COUNT]
     */
    @JsonProperty("message_attachment_count")
    public void setMessage_attachment_count(Integer  message_attachment_count){
        this.message_attachment_count = message_attachment_count ;
        this.message_attachment_countDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_ATTACHMENT_COUNT]脏标记
     */
    @JsonIgnore
    public boolean getMessage_attachment_countDirtyFlag(){
        return message_attachment_countDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_IS_FOLLOWER]
     */
    @JsonProperty("message_is_follower")
    public String getMessage_is_follower(){
        return message_is_follower ;
    }

    /**
     * 设置 [MESSAGE_IS_FOLLOWER]
     */
    @JsonProperty("message_is_follower")
    public void setMessage_is_follower(String  message_is_follower){
        this.message_is_follower = message_is_follower ;
        this.message_is_followerDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_IS_FOLLOWER]脏标记
     */
    @JsonIgnore
    public boolean getMessage_is_followerDirtyFlag(){
        return message_is_followerDirtyFlag ;
    }

    /**
     * 获取 [USER_DOMAIN]
     */
    @JsonProperty("user_domain")
    public String getUser_domain(){
        return user_domain ;
    }

    /**
     * 设置 [USER_DOMAIN]
     */
    @JsonProperty("user_domain")
    public void setUser_domain(String  user_domain){
        this.user_domain = user_domain ;
        this.user_domainDirtyFlag = true ;
    }

    /**
     * 获取 [USER_DOMAIN]脏标记
     */
    @JsonIgnore
    public boolean getUser_domainDirtyFlag(){
        return user_domainDirtyFlag ;
    }

    /**
     * 获取 [CATEGORY]
     */
    @JsonProperty("category")
    public String getCategory(){
        return category ;
    }

    /**
     * 设置 [CATEGORY]
     */
    @JsonProperty("category")
    public void setCategory(String  category){
        this.category = category ;
        this.categoryDirtyFlag = true ;
    }

    /**
     * 获取 [CATEGORY]脏标记
     */
    @JsonIgnore
    public boolean getCategoryDirtyFlag(){
        return categoryDirtyFlag ;
    }

    /**
     * 获取 [DESCRIPTION]
     */
    @JsonProperty("description")
    public String getDescription(){
        return description ;
    }

    /**
     * 设置 [DESCRIPTION]
     */
    @JsonProperty("description")
    public void setDescription(String  description){
        this.description = description ;
        this.descriptionDirtyFlag = true ;
    }

    /**
     * 获取 [DESCRIPTION]脏标记
     */
    @JsonIgnore
    public boolean getDescriptionDirtyFlag(){
        return descriptionDirtyFlag ;
    }

    /**
     * 获取 [USER_IDS]
     */
    @JsonProperty("user_ids")
    public String getUser_ids(){
        return user_ids ;
    }

    /**
     * 设置 [USER_IDS]
     */
    @JsonProperty("user_ids")
    public void setUser_ids(String  user_ids){
        this.user_ids = user_ids ;
        this.user_idsDirtyFlag = true ;
    }

    /**
     * 获取 [USER_IDS]脏标记
     */
    @JsonIgnore
    public boolean getUser_idsDirtyFlag(){
        return user_idsDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_UNREAD_COUNTER]
     */
    @JsonProperty("message_unread_counter")
    public Integer getMessage_unread_counter(){
        return message_unread_counter ;
    }

    /**
     * 设置 [MESSAGE_UNREAD_COUNTER]
     */
    @JsonProperty("message_unread_counter")
    public void setMessage_unread_counter(Integer  message_unread_counter){
        this.message_unread_counter = message_unread_counter ;
        this.message_unread_counterDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_UNREAD_COUNTER]脏标记
     */
    @JsonIgnore
    public boolean getMessage_unread_counterDirtyFlag(){
        return message_unread_counterDirtyFlag ;
    }

    /**
     * 获取 [LINE_IDS]
     */
    @JsonProperty("line_ids")
    public String getLine_ids(){
        return line_ids ;
    }

    /**
     * 设置 [LINE_IDS]
     */
    @JsonProperty("line_ids")
    public void setLine_ids(String  line_ids){
        this.line_ids = line_ids ;
        this.line_idsDirtyFlag = true ;
    }

    /**
     * 获取 [LINE_IDS]脏标记
     */
    @JsonIgnore
    public boolean getLine_idsDirtyFlag(){
        return line_idsDirtyFlag ;
    }

    /**
     * 获取 [REPORT_MESSAGE_FREQUENCY]
     */
    @JsonProperty("report_message_frequency")
    public String getReport_message_frequency(){
        return report_message_frequency ;
    }

    /**
     * 设置 [REPORT_MESSAGE_FREQUENCY]
     */
    @JsonProperty("report_message_frequency")
    public void setReport_message_frequency(String  report_message_frequency){
        this.report_message_frequency = report_message_frequency ;
        this.report_message_frequencyDirtyFlag = true ;
    }

    /**
     * 获取 [REPORT_MESSAGE_FREQUENCY]脏标记
     */
    @JsonIgnore
    public boolean getReport_message_frequencyDirtyFlag(){
        return report_message_frequencyDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_PARTNER_IDS]
     */
    @JsonProperty("message_partner_ids")
    public String getMessage_partner_ids(){
        return message_partner_ids ;
    }

    /**
     * 设置 [MESSAGE_PARTNER_IDS]
     */
    @JsonProperty("message_partner_ids")
    public void setMessage_partner_ids(String  message_partner_ids){
        this.message_partner_ids = message_partner_ids ;
        this.message_partner_idsDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_PARTNER_IDS]脏标记
     */
    @JsonIgnore
    public boolean getMessage_partner_idsDirtyFlag(){
        return message_partner_idsDirtyFlag ;
    }

    /**
     * 获取 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return display_name ;
    }

    /**
     * 设置 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

    /**
     * 获取 [DISPLAY_NAME]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return display_nameDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_CHANNEL_IDS]
     */
    @JsonProperty("message_channel_ids")
    public String getMessage_channel_ids(){
        return message_channel_ids ;
    }

    /**
     * 设置 [MESSAGE_CHANNEL_IDS]
     */
    @JsonProperty("message_channel_ids")
    public void setMessage_channel_ids(String  message_channel_ids){
        this.message_channel_ids = message_channel_ids ;
        this.message_channel_idsDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_CHANNEL_IDS]脏标记
     */
    @JsonIgnore
    public boolean getMessage_channel_idsDirtyFlag(){
        return message_channel_idsDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_IDS]
     */
    @JsonProperty("message_ids")
    public String getMessage_ids(){
        return message_ids ;
    }

    /**
     * 设置 [MESSAGE_IDS]
     */
    @JsonProperty("message_ids")
    public void setMessage_ids(String  message_ids){
        this.message_ids = message_ids ;
        this.message_idsDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_IDS]脏标记
     */
    @JsonIgnore
    public boolean getMessage_idsDirtyFlag(){
        return message_idsDirtyFlag ;
    }

    /**
     * 获取 [REWARD_REALTIME]
     */
    @JsonProperty("reward_realtime")
    public String getReward_realtime(){
        return reward_realtime ;
    }

    /**
     * 设置 [REWARD_REALTIME]
     */
    @JsonProperty("reward_realtime")
    public void setReward_realtime(String  reward_realtime){
        this.reward_realtime = reward_realtime ;
        this.reward_realtimeDirtyFlag = true ;
    }

    /**
     * 获取 [REWARD_REALTIME]脏标记
     */
    @JsonIgnore
    public boolean getReward_realtimeDirtyFlag(){
        return reward_realtimeDirtyFlag ;
    }

    /**
     * 获取 [REWARD_FAILURE]
     */
    @JsonProperty("reward_failure")
    public String getReward_failure(){
        return reward_failure ;
    }

    /**
     * 设置 [REWARD_FAILURE]
     */
    @JsonProperty("reward_failure")
    public void setReward_failure(String  reward_failure){
        this.reward_failure = reward_failure ;
        this.reward_failureDirtyFlag = true ;
    }

    /**
     * 获取 [REWARD_FAILURE]脏标记
     */
    @JsonIgnore
    public boolean getReward_failureDirtyFlag(){
        return reward_failureDirtyFlag ;
    }

    /**
     * 获取 [PERIOD]
     */
    @JsonProperty("period")
    public String getPeriod(){
        return period ;
    }

    /**
     * 设置 [PERIOD]
     */
    @JsonProperty("period")
    public void setPeriod(String  period){
        this.period = period ;
        this.periodDirtyFlag = true ;
    }

    /**
     * 获取 [PERIOD]脏标记
     */
    @JsonIgnore
    public boolean getPeriodDirtyFlag(){
        return periodDirtyFlag ;
    }

    /**
     * 获取 [VISIBILITY_MODE]
     */
    @JsonProperty("visibility_mode")
    public String getVisibility_mode(){
        return visibility_mode ;
    }

    /**
     * 设置 [VISIBILITY_MODE]
     */
    @JsonProperty("visibility_mode")
    public void setVisibility_mode(String  visibility_mode){
        this.visibility_mode = visibility_mode ;
        this.visibility_modeDirtyFlag = true ;
    }

    /**
     * 获取 [VISIBILITY_MODE]脏标记
     */
    @JsonIgnore
    public boolean getVisibility_modeDirtyFlag(){
        return visibility_modeDirtyFlag ;
    }

    /**
     * 获取 [REMIND_UPDATE_DELAY]
     */
    @JsonProperty("remind_update_delay")
    public Integer getRemind_update_delay(){
        return remind_update_delay ;
    }

    /**
     * 设置 [REMIND_UPDATE_DELAY]
     */
    @JsonProperty("remind_update_delay")
    public void setRemind_update_delay(Integer  remind_update_delay){
        this.remind_update_delay = remind_update_delay ;
        this.remind_update_delayDirtyFlag = true ;
    }

    /**
     * 获取 [REMIND_UPDATE_DELAY]脏标记
     */
    @JsonIgnore
    public boolean getRemind_update_delayDirtyFlag(){
        return remind_update_delayDirtyFlag ;
    }

    /**
     * 获取 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return write_date ;
    }

    /**
     * 设置 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return write_dateDirtyFlag ;
    }

    /**
     * 获取 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return __last_update ;
    }

    /**
     * 设置 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

    /**
     * 获取 [__LAST_UPDATE]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return __last_updateDirtyFlag ;
    }

    /**
     * 获取 [INVITED_USER_IDS]
     */
    @JsonProperty("invited_user_ids")
    public String getInvited_user_ids(){
        return invited_user_ids ;
    }

    /**
     * 设置 [INVITED_USER_IDS]
     */
    @JsonProperty("invited_user_ids")
    public void setInvited_user_ids(String  invited_user_ids){
        this.invited_user_ids = invited_user_ids ;
        this.invited_user_idsDirtyFlag = true ;
    }

    /**
     * 获取 [INVITED_USER_IDS]脏标记
     */
    @JsonIgnore
    public boolean getInvited_user_idsDirtyFlag(){
        return invited_user_idsDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_NEEDACTION_COUNTER]
     */
    @JsonProperty("message_needaction_counter")
    public Integer getMessage_needaction_counter(){
        return message_needaction_counter ;
    }

    /**
     * 设置 [MESSAGE_NEEDACTION_COUNTER]
     */
    @JsonProperty("message_needaction_counter")
    public void setMessage_needaction_counter(Integer  message_needaction_counter){
        this.message_needaction_counter = message_needaction_counter ;
        this.message_needaction_counterDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_NEEDACTION_COUNTER]脏标记
     */
    @JsonIgnore
    public boolean getMessage_needaction_counterDirtyFlag(){
        return message_needaction_counterDirtyFlag ;
    }

    /**
     * 获取 [STATE]
     */
    @JsonProperty("state")
    public String getState(){
        return state ;
    }

    /**
     * 设置 [STATE]
     */
    @JsonProperty("state")
    public void setState(String  state){
        this.state = state ;
        this.stateDirtyFlag = true ;
    }

    /**
     * 获取 [STATE]脏标记
     */
    @JsonIgnore
    public boolean getStateDirtyFlag(){
        return stateDirtyFlag ;
    }

    /**
     * 获取 [NEXT_REPORT_DATE]
     */
    @JsonProperty("next_report_date")
    public Timestamp getNext_report_date(){
        return next_report_date ;
    }

    /**
     * 设置 [NEXT_REPORT_DATE]
     */
    @JsonProperty("next_report_date")
    public void setNext_report_date(Timestamp  next_report_date){
        this.next_report_date = next_report_date ;
        this.next_report_dateDirtyFlag = true ;
    }

    /**
     * 获取 [NEXT_REPORT_DATE]脏标记
     */
    @JsonIgnore
    public boolean getNext_report_dateDirtyFlag(){
        return next_report_dateDirtyFlag ;
    }

    /**
     * 获取 [MANAGER_ID_TEXT]
     */
    @JsonProperty("manager_id_text")
    public String getManager_id_text(){
        return manager_id_text ;
    }

    /**
     * 设置 [MANAGER_ID_TEXT]
     */
    @JsonProperty("manager_id_text")
    public void setManager_id_text(String  manager_id_text){
        this.manager_id_text = manager_id_text ;
        this.manager_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [MANAGER_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getManager_id_textDirtyFlag(){
        return manager_id_textDirtyFlag ;
    }

    /**
     * 获取 [REPORT_MESSAGE_GROUP_ID_TEXT]
     */
    @JsonProperty("report_message_group_id_text")
    public String getReport_message_group_id_text(){
        return report_message_group_id_text ;
    }

    /**
     * 设置 [REPORT_MESSAGE_GROUP_ID_TEXT]
     */
    @JsonProperty("report_message_group_id_text")
    public void setReport_message_group_id_text(String  report_message_group_id_text){
        this.report_message_group_id_text = report_message_group_id_text ;
        this.report_message_group_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [REPORT_MESSAGE_GROUP_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getReport_message_group_id_textDirtyFlag(){
        return report_message_group_id_textDirtyFlag ;
    }

    /**
     * 获取 [REPORT_TEMPLATE_ID_TEXT]
     */
    @JsonProperty("report_template_id_text")
    public String getReport_template_id_text(){
        return report_template_id_text ;
    }

    /**
     * 设置 [REPORT_TEMPLATE_ID_TEXT]
     */
    @JsonProperty("report_template_id_text")
    public void setReport_template_id_text(String  report_template_id_text){
        this.report_template_id_text = report_template_id_text ;
        this.report_template_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [REPORT_TEMPLATE_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getReport_template_id_textDirtyFlag(){
        return report_template_id_textDirtyFlag ;
    }

    /**
     * 获取 [REWARD_ID_TEXT]
     */
    @JsonProperty("reward_id_text")
    public String getReward_id_text(){
        return reward_id_text ;
    }

    /**
     * 设置 [REWARD_ID_TEXT]
     */
    @JsonProperty("reward_id_text")
    public void setReward_id_text(String  reward_id_text){
        this.reward_id_text = reward_id_text ;
        this.reward_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [REWARD_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getReward_id_textDirtyFlag(){
        return reward_id_textDirtyFlag ;
    }

    /**
     * 获取 [REWARD_FIRST_ID_TEXT]
     */
    @JsonProperty("reward_first_id_text")
    public String getReward_first_id_text(){
        return reward_first_id_text ;
    }

    /**
     * 设置 [REWARD_FIRST_ID_TEXT]
     */
    @JsonProperty("reward_first_id_text")
    public void setReward_first_id_text(String  reward_first_id_text){
        this.reward_first_id_text = reward_first_id_text ;
        this.reward_first_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [REWARD_FIRST_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getReward_first_id_textDirtyFlag(){
        return reward_first_id_textDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return create_uid_text ;
    }

    /**
     * 设置 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return create_uid_textDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return write_uid_text ;
    }

    /**
     * 设置 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return write_uid_textDirtyFlag ;
    }

    /**
     * 获取 [REWARD_SECOND_ID_TEXT]
     */
    @JsonProperty("reward_second_id_text")
    public String getReward_second_id_text(){
        return reward_second_id_text ;
    }

    /**
     * 设置 [REWARD_SECOND_ID_TEXT]
     */
    @JsonProperty("reward_second_id_text")
    public void setReward_second_id_text(String  reward_second_id_text){
        this.reward_second_id_text = reward_second_id_text ;
        this.reward_second_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [REWARD_SECOND_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getReward_second_id_textDirtyFlag(){
        return reward_second_id_textDirtyFlag ;
    }

    /**
     * 获取 [REWARD_THIRD_ID_TEXT]
     */
    @JsonProperty("reward_third_id_text")
    public String getReward_third_id_text(){
        return reward_third_id_text ;
    }

    /**
     * 设置 [REWARD_THIRD_ID_TEXT]
     */
    @JsonProperty("reward_third_id_text")
    public void setReward_third_id_text(String  reward_third_id_text){
        this.reward_third_id_text = reward_third_id_text ;
        this.reward_third_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [REWARD_THIRD_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getReward_third_id_textDirtyFlag(){
        return reward_third_id_textDirtyFlag ;
    }

    /**
     * 获取 [REWARD_FIRST_ID]
     */
    @JsonProperty("reward_first_id")
    public Integer getReward_first_id(){
        return reward_first_id ;
    }

    /**
     * 设置 [REWARD_FIRST_ID]
     */
    @JsonProperty("reward_first_id")
    public void setReward_first_id(Integer  reward_first_id){
        this.reward_first_id = reward_first_id ;
        this.reward_first_idDirtyFlag = true ;
    }

    /**
     * 获取 [REWARD_FIRST_ID]脏标记
     */
    @JsonIgnore
    public boolean getReward_first_idDirtyFlag(){
        return reward_first_idDirtyFlag ;
    }

    /**
     * 获取 [REPORT_TEMPLATE_ID]
     */
    @JsonProperty("report_template_id")
    public Integer getReport_template_id(){
        return report_template_id ;
    }

    /**
     * 设置 [REPORT_TEMPLATE_ID]
     */
    @JsonProperty("report_template_id")
    public void setReport_template_id(Integer  report_template_id){
        this.report_template_id = report_template_id ;
        this.report_template_idDirtyFlag = true ;
    }

    /**
     * 获取 [REPORT_TEMPLATE_ID]脏标记
     */
    @JsonIgnore
    public boolean getReport_template_idDirtyFlag(){
        return report_template_idDirtyFlag ;
    }

    /**
     * 获取 [REWARD_SECOND_ID]
     */
    @JsonProperty("reward_second_id")
    public Integer getReward_second_id(){
        return reward_second_id ;
    }

    /**
     * 设置 [REWARD_SECOND_ID]
     */
    @JsonProperty("reward_second_id")
    public void setReward_second_id(Integer  reward_second_id){
        this.reward_second_id = reward_second_id ;
        this.reward_second_idDirtyFlag = true ;
    }

    /**
     * 获取 [REWARD_SECOND_ID]脏标记
     */
    @JsonIgnore
    public boolean getReward_second_idDirtyFlag(){
        return reward_second_idDirtyFlag ;
    }

    /**
     * 获取 [REWARD_THIRD_ID]
     */
    @JsonProperty("reward_third_id")
    public Integer getReward_third_id(){
        return reward_third_id ;
    }

    /**
     * 设置 [REWARD_THIRD_ID]
     */
    @JsonProperty("reward_third_id")
    public void setReward_third_id(Integer  reward_third_id){
        this.reward_third_id = reward_third_id ;
        this.reward_third_idDirtyFlag = true ;
    }

    /**
     * 获取 [REWARD_THIRD_ID]脏标记
     */
    @JsonIgnore
    public boolean getReward_third_idDirtyFlag(){
        return reward_third_idDirtyFlag ;
    }

    /**
     * 获取 [REPORT_MESSAGE_GROUP_ID]
     */
    @JsonProperty("report_message_group_id")
    public Integer getReport_message_group_id(){
        return report_message_group_id ;
    }

    /**
     * 设置 [REPORT_MESSAGE_GROUP_ID]
     */
    @JsonProperty("report_message_group_id")
    public void setReport_message_group_id(Integer  report_message_group_id){
        this.report_message_group_id = report_message_group_id ;
        this.report_message_group_idDirtyFlag = true ;
    }

    /**
     * 获取 [REPORT_MESSAGE_GROUP_ID]脏标记
     */
    @JsonIgnore
    public boolean getReport_message_group_idDirtyFlag(){
        return report_message_group_idDirtyFlag ;
    }

    /**
     * 获取 [REWARD_ID]
     */
    @JsonProperty("reward_id")
    public Integer getReward_id(){
        return reward_id ;
    }

    /**
     * 设置 [REWARD_ID]
     */
    @JsonProperty("reward_id")
    public void setReward_id(Integer  reward_id){
        this.reward_id = reward_id ;
        this.reward_idDirtyFlag = true ;
    }

    /**
     * 获取 [REWARD_ID]脏标记
     */
    @JsonIgnore
    public boolean getReward_idDirtyFlag(){
        return reward_idDirtyFlag ;
    }

    /**
     * 获取 [MANAGER_ID]
     */
    @JsonProperty("manager_id")
    public Integer getManager_id(){
        return manager_id ;
    }

    /**
     * 设置 [MANAGER_ID]
     */
    @JsonProperty("manager_id")
    public void setManager_id(Integer  manager_id){
        this.manager_id = manager_id ;
        this.manager_idDirtyFlag = true ;
    }

    /**
     * 获取 [MANAGER_ID]脏标记
     */
    @JsonIgnore
    public boolean getManager_idDirtyFlag(){
        return manager_idDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return write_uid ;
    }

    /**
     * 设置 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return write_uidDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return create_uid ;
    }

    /**
     * 设置 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return create_uidDirtyFlag ;
    }



    public Gamification_challenge toDO() {
        Gamification_challenge srfdomain = new Gamification_challenge();
        if(getWebsite_message_idsDirtyFlag())
            srfdomain.setWebsite_message_ids(website_message_ids);
        if(getNameDirtyFlag())
            srfdomain.setName(name);
        if(getMessage_needactionDirtyFlag())
            srfdomain.setMessage_needaction(message_needaction);
        if(getIdDirtyFlag())
            srfdomain.setId(id);
        if(getCreate_dateDirtyFlag())
            srfdomain.setCreate_date(create_date);
        if(getMessage_unreadDirtyFlag())
            srfdomain.setMessage_unread(message_unread);
        if(getStart_dateDirtyFlag())
            srfdomain.setStart_date(start_date);
        if(getMessage_has_error_counterDirtyFlag())
            srfdomain.setMessage_has_error_counter(message_has_error_counter);
        if(getMessage_main_attachment_idDirtyFlag())
            srfdomain.setMessage_main_attachment_id(message_main_attachment_id);
        if(getMessage_has_errorDirtyFlag())
            srfdomain.setMessage_has_error(message_has_error);
        if(getLast_report_dateDirtyFlag())
            srfdomain.setLast_report_date(last_report_date);
        if(getEnd_dateDirtyFlag())
            srfdomain.setEnd_date(end_date);
        if(getMessage_follower_idsDirtyFlag())
            srfdomain.setMessage_follower_ids(message_follower_ids);
        if(getMessage_attachment_countDirtyFlag())
            srfdomain.setMessage_attachment_count(message_attachment_count);
        if(getMessage_is_followerDirtyFlag())
            srfdomain.setMessage_is_follower(message_is_follower);
        if(getUser_domainDirtyFlag())
            srfdomain.setUser_domain(user_domain);
        if(getCategoryDirtyFlag())
            srfdomain.setCategory(category);
        if(getDescriptionDirtyFlag())
            srfdomain.setDescription(description);
        if(getUser_idsDirtyFlag())
            srfdomain.setUser_ids(user_ids);
        if(getMessage_unread_counterDirtyFlag())
            srfdomain.setMessage_unread_counter(message_unread_counter);
        if(getLine_idsDirtyFlag())
            srfdomain.setLine_ids(line_ids);
        if(getReport_message_frequencyDirtyFlag())
            srfdomain.setReport_message_frequency(report_message_frequency);
        if(getMessage_partner_idsDirtyFlag())
            srfdomain.setMessage_partner_ids(message_partner_ids);
        if(getDisplay_nameDirtyFlag())
            srfdomain.setDisplay_name(display_name);
        if(getMessage_channel_idsDirtyFlag())
            srfdomain.setMessage_channel_ids(message_channel_ids);
        if(getMessage_idsDirtyFlag())
            srfdomain.setMessage_ids(message_ids);
        if(getReward_realtimeDirtyFlag())
            srfdomain.setReward_realtime(reward_realtime);
        if(getReward_failureDirtyFlag())
            srfdomain.setReward_failure(reward_failure);
        if(getPeriodDirtyFlag())
            srfdomain.setPeriod(period);
        if(getVisibility_modeDirtyFlag())
            srfdomain.setVisibility_mode(visibility_mode);
        if(getRemind_update_delayDirtyFlag())
            srfdomain.setRemind_update_delay(remind_update_delay);
        if(getWrite_dateDirtyFlag())
            srfdomain.setWrite_date(write_date);
        if(get__last_updateDirtyFlag())
            srfdomain.set__last_update(__last_update);
        if(getInvited_user_idsDirtyFlag())
            srfdomain.setInvited_user_ids(invited_user_ids);
        if(getMessage_needaction_counterDirtyFlag())
            srfdomain.setMessage_needaction_counter(message_needaction_counter);
        if(getStateDirtyFlag())
            srfdomain.setState(state);
        if(getNext_report_dateDirtyFlag())
            srfdomain.setNext_report_date(next_report_date);
        if(getManager_id_textDirtyFlag())
            srfdomain.setManager_id_text(manager_id_text);
        if(getReport_message_group_id_textDirtyFlag())
            srfdomain.setReport_message_group_id_text(report_message_group_id_text);
        if(getReport_template_id_textDirtyFlag())
            srfdomain.setReport_template_id_text(report_template_id_text);
        if(getReward_id_textDirtyFlag())
            srfdomain.setReward_id_text(reward_id_text);
        if(getReward_first_id_textDirtyFlag())
            srfdomain.setReward_first_id_text(reward_first_id_text);
        if(getCreate_uid_textDirtyFlag())
            srfdomain.setCreate_uid_text(create_uid_text);
        if(getWrite_uid_textDirtyFlag())
            srfdomain.setWrite_uid_text(write_uid_text);
        if(getReward_second_id_textDirtyFlag())
            srfdomain.setReward_second_id_text(reward_second_id_text);
        if(getReward_third_id_textDirtyFlag())
            srfdomain.setReward_third_id_text(reward_third_id_text);
        if(getReward_first_idDirtyFlag())
            srfdomain.setReward_first_id(reward_first_id);
        if(getReport_template_idDirtyFlag())
            srfdomain.setReport_template_id(report_template_id);
        if(getReward_second_idDirtyFlag())
            srfdomain.setReward_second_id(reward_second_id);
        if(getReward_third_idDirtyFlag())
            srfdomain.setReward_third_id(reward_third_id);
        if(getReport_message_group_idDirtyFlag())
            srfdomain.setReport_message_group_id(report_message_group_id);
        if(getReward_idDirtyFlag())
            srfdomain.setReward_id(reward_id);
        if(getManager_idDirtyFlag())
            srfdomain.setManager_id(manager_id);
        if(getWrite_uidDirtyFlag())
            srfdomain.setWrite_uid(write_uid);
        if(getCreate_uidDirtyFlag())
            srfdomain.setCreate_uid(create_uid);

        return srfdomain;
    }

    public void fromDO(Gamification_challenge srfdomain) {
        if(srfdomain == null )
          return ;
        if(srfdomain.getWebsite_message_idsDirtyFlag())
            this.setWebsite_message_ids(srfdomain.getWebsite_message_ids());
        if(srfdomain.getNameDirtyFlag())
            this.setName(srfdomain.getName());
        if(srfdomain.getMessage_needactionDirtyFlag())
            this.setMessage_needaction(srfdomain.getMessage_needaction());
        if(srfdomain.getIdDirtyFlag())
            this.setId(srfdomain.getId());
        if(srfdomain.getCreate_dateDirtyFlag())
            this.setCreate_date(srfdomain.getCreate_date());
        if(srfdomain.getMessage_unreadDirtyFlag())
            this.setMessage_unread(srfdomain.getMessage_unread());
        if(srfdomain.getStart_dateDirtyFlag())
            this.setStart_date(srfdomain.getStart_date());
        if(srfdomain.getMessage_has_error_counterDirtyFlag())
            this.setMessage_has_error_counter(srfdomain.getMessage_has_error_counter());
        if(srfdomain.getMessage_main_attachment_idDirtyFlag())
            this.setMessage_main_attachment_id(srfdomain.getMessage_main_attachment_id());
        if(srfdomain.getMessage_has_errorDirtyFlag())
            this.setMessage_has_error(srfdomain.getMessage_has_error());
        if(srfdomain.getLast_report_dateDirtyFlag())
            this.setLast_report_date(srfdomain.getLast_report_date());
        if(srfdomain.getEnd_dateDirtyFlag())
            this.setEnd_date(srfdomain.getEnd_date());
        if(srfdomain.getMessage_follower_idsDirtyFlag())
            this.setMessage_follower_ids(srfdomain.getMessage_follower_ids());
        if(srfdomain.getMessage_attachment_countDirtyFlag())
            this.setMessage_attachment_count(srfdomain.getMessage_attachment_count());
        if(srfdomain.getMessage_is_followerDirtyFlag())
            this.setMessage_is_follower(srfdomain.getMessage_is_follower());
        if(srfdomain.getUser_domainDirtyFlag())
            this.setUser_domain(srfdomain.getUser_domain());
        if(srfdomain.getCategoryDirtyFlag())
            this.setCategory(srfdomain.getCategory());
        if(srfdomain.getDescriptionDirtyFlag())
            this.setDescription(srfdomain.getDescription());
        if(srfdomain.getUser_idsDirtyFlag())
            this.setUser_ids(srfdomain.getUser_ids());
        if(srfdomain.getMessage_unread_counterDirtyFlag())
            this.setMessage_unread_counter(srfdomain.getMessage_unread_counter());
        if(srfdomain.getLine_idsDirtyFlag())
            this.setLine_ids(srfdomain.getLine_ids());
        if(srfdomain.getReport_message_frequencyDirtyFlag())
            this.setReport_message_frequency(srfdomain.getReport_message_frequency());
        if(srfdomain.getMessage_partner_idsDirtyFlag())
            this.setMessage_partner_ids(srfdomain.getMessage_partner_ids());
        if(srfdomain.getDisplay_nameDirtyFlag())
            this.setDisplay_name(srfdomain.getDisplay_name());
        if(srfdomain.getMessage_channel_idsDirtyFlag())
            this.setMessage_channel_ids(srfdomain.getMessage_channel_ids());
        if(srfdomain.getMessage_idsDirtyFlag())
            this.setMessage_ids(srfdomain.getMessage_ids());
        if(srfdomain.getReward_realtimeDirtyFlag())
            this.setReward_realtime(srfdomain.getReward_realtime());
        if(srfdomain.getReward_failureDirtyFlag())
            this.setReward_failure(srfdomain.getReward_failure());
        if(srfdomain.getPeriodDirtyFlag())
            this.setPeriod(srfdomain.getPeriod());
        if(srfdomain.getVisibility_modeDirtyFlag())
            this.setVisibility_mode(srfdomain.getVisibility_mode());
        if(srfdomain.getRemind_update_delayDirtyFlag())
            this.setRemind_update_delay(srfdomain.getRemind_update_delay());
        if(srfdomain.getWrite_dateDirtyFlag())
            this.setWrite_date(srfdomain.getWrite_date());
        if(srfdomain.get__last_updateDirtyFlag())
            this.set__last_update(srfdomain.get__last_update());
        if(srfdomain.getInvited_user_idsDirtyFlag())
            this.setInvited_user_ids(srfdomain.getInvited_user_ids());
        if(srfdomain.getMessage_needaction_counterDirtyFlag())
            this.setMessage_needaction_counter(srfdomain.getMessage_needaction_counter());
        if(srfdomain.getStateDirtyFlag())
            this.setState(srfdomain.getState());
        if(srfdomain.getNext_report_dateDirtyFlag())
            this.setNext_report_date(srfdomain.getNext_report_date());
        if(srfdomain.getManager_id_textDirtyFlag())
            this.setManager_id_text(srfdomain.getManager_id_text());
        if(srfdomain.getReport_message_group_id_textDirtyFlag())
            this.setReport_message_group_id_text(srfdomain.getReport_message_group_id_text());
        if(srfdomain.getReport_template_id_textDirtyFlag())
            this.setReport_template_id_text(srfdomain.getReport_template_id_text());
        if(srfdomain.getReward_id_textDirtyFlag())
            this.setReward_id_text(srfdomain.getReward_id_text());
        if(srfdomain.getReward_first_id_textDirtyFlag())
            this.setReward_first_id_text(srfdomain.getReward_first_id_text());
        if(srfdomain.getCreate_uid_textDirtyFlag())
            this.setCreate_uid_text(srfdomain.getCreate_uid_text());
        if(srfdomain.getWrite_uid_textDirtyFlag())
            this.setWrite_uid_text(srfdomain.getWrite_uid_text());
        if(srfdomain.getReward_second_id_textDirtyFlag())
            this.setReward_second_id_text(srfdomain.getReward_second_id_text());
        if(srfdomain.getReward_third_id_textDirtyFlag())
            this.setReward_third_id_text(srfdomain.getReward_third_id_text());
        if(srfdomain.getReward_first_idDirtyFlag())
            this.setReward_first_id(srfdomain.getReward_first_id());
        if(srfdomain.getReport_template_idDirtyFlag())
            this.setReport_template_id(srfdomain.getReport_template_id());
        if(srfdomain.getReward_second_idDirtyFlag())
            this.setReward_second_id(srfdomain.getReward_second_id());
        if(srfdomain.getReward_third_idDirtyFlag())
            this.setReward_third_id(srfdomain.getReward_third_id());
        if(srfdomain.getReport_message_group_idDirtyFlag())
            this.setReport_message_group_id(srfdomain.getReport_message_group_id());
        if(srfdomain.getReward_idDirtyFlag())
            this.setReward_id(srfdomain.getReward_id());
        if(srfdomain.getManager_idDirtyFlag())
            this.setManager_id(srfdomain.getManager_id());
        if(srfdomain.getWrite_uidDirtyFlag())
            this.setWrite_uid(srfdomain.getWrite_uid());
        if(srfdomain.getCreate_uidDirtyFlag())
            this.setCreate_uid(srfdomain.getCreate_uid());

    }

    public List<Gamification_challengeDTO> fromDOPage(List<Gamification_challenge> poPage)   {
        if(poPage == null)
            return null;
        List<Gamification_challengeDTO> dtos=new ArrayList<Gamification_challengeDTO>();
        for(Gamification_challenge domain : poPage) {
            Gamification_challengeDTO dto = new Gamification_challengeDTO();
            dto.fromDO(domain);
            dtos.add(dto);
        }
        return dtos;
    }
}

