package cn.ibizlab.odoo.service.odoo_gamification.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_gamification.dto.Gamification_challengeDTO;
import cn.ibizlab.odoo.core.odoo_gamification.domain.Gamification_challenge;
import cn.ibizlab.odoo.core.odoo_gamification.service.IGamification_challengeService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_gamification.filter.Gamification_challengeSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Gamification_challenge" })
@RestController
@RequestMapping("")
public class Gamification_challengeResource {

    @Autowired
    private IGamification_challengeService gamification_challengeService;

    public IGamification_challengeService getGamification_challengeService() {
        return this.gamification_challengeService;
    }

    @ApiOperation(value = "删除数据", tags = {"Gamification_challenge" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_gamification/gamification_challenges/{gamification_challenge_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("gamification_challenge_id") Integer gamification_challenge_id) {
        Gamification_challengeDTO gamification_challengedto = new Gamification_challengeDTO();
		Gamification_challenge domain = new Gamification_challenge();
		gamification_challengedto.setId(gamification_challenge_id);
		domain.setId(gamification_challenge_id);
        Boolean rst = gamification_challengeService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "批更新数据", tags = {"Gamification_challenge" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_gamification/gamification_challenges/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Gamification_challengeDTO> gamification_challengedtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "获取数据", tags = {"Gamification_challenge" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_gamification/gamification_challenges/{gamification_challenge_id}")
    public ResponseEntity<Gamification_challengeDTO> get(@PathVariable("gamification_challenge_id") Integer gamification_challenge_id) {
        Gamification_challengeDTO dto = new Gamification_challengeDTO();
        Gamification_challenge domain = gamification_challengeService.get(gamification_challenge_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批删除数据", tags = {"Gamification_challenge" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_gamification/gamification_challenges/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Gamification_challengeDTO> gamification_challengedtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "更新数据", tags = {"Gamification_challenge" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_gamification/gamification_challenges/{gamification_challenge_id}")

    public ResponseEntity<Gamification_challengeDTO> update(@PathVariable("gamification_challenge_id") Integer gamification_challenge_id, @RequestBody Gamification_challengeDTO gamification_challengedto) {
		Gamification_challenge domain = gamification_challengedto.toDO();
        domain.setId(gamification_challenge_id);
		gamification_challengeService.update(domain);
		Gamification_challengeDTO dto = new Gamification_challengeDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "建立数据", tags = {"Gamification_challenge" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_gamification/gamification_challenges")

    public ResponseEntity<Gamification_challengeDTO> create(@RequestBody Gamification_challengeDTO gamification_challengedto) {
        Gamification_challengeDTO dto = new Gamification_challengeDTO();
        Gamification_challenge domain = gamification_challengedto.toDO();
		gamification_challengeService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批建立数据", tags = {"Gamification_challenge" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_gamification/gamification_challenges/createBatch")
    public ResponseEntity<Boolean> createBatchGamification_challenge(@RequestBody List<Gamification_challengeDTO> gamification_challengedtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Gamification_challenge" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_gamification/gamification_challenges/fetchdefault")
	public ResponseEntity<Page<Gamification_challengeDTO>> fetchDefault(Gamification_challengeSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Gamification_challengeDTO> list = new ArrayList<Gamification_challengeDTO>();
        
        Page<Gamification_challenge> domains = gamification_challengeService.searchDefault(context) ;
        for(Gamification_challenge gamification_challenge : domains.getContent()){
            Gamification_challengeDTO dto = new Gamification_challengeDTO();
            dto.fromDO(gamification_challenge);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
