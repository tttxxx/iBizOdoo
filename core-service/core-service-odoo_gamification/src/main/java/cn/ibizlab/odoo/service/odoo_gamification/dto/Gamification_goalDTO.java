package cn.ibizlab.odoo.service.odoo_gamification.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.core.odoo_gamification.valuerule.anno.gamification_goal.*;
import cn.ibizlab.odoo.core.odoo_gamification.domain.Gamification_goal;
import org.springframework.cglib.beans.BeanCopier;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 服务DTO对象[Gamification_goalDTO]
 */
public class Gamification_goalDTO implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [START_DATE]
     *
     */
    @Gamification_goalStart_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    private Timestamp start_date;

    @JsonIgnore
    private boolean start_dateDirtyFlag;

    /**
     * 属性 [TO_UPDATE]
     *
     */
    @Gamification_goalTo_updateDefault(info = "默认规则")
    private String to_update;

    @JsonIgnore
    private boolean to_updateDirtyFlag;

    /**
     * 属性 [WRITE_DATE]
     *
     */
    @Gamification_goalWrite_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp write_date;

    @JsonIgnore
    private boolean write_dateDirtyFlag;

    /**
     * 属性 [REMIND_UPDATE_DELAY]
     *
     */
    @Gamification_goalRemind_update_delayDefault(info = "默认规则")
    private Integer remind_update_delay;

    @JsonIgnore
    private boolean remind_update_delayDirtyFlag;

    /**
     * 属性 [STATE]
     *
     */
    @Gamification_goalStateDefault(info = "默认规则")
    private String state;

    @JsonIgnore
    private boolean stateDirtyFlag;

    /**
     * 属性 [COMPLETENESS]
     *
     */
    @Gamification_goalCompletenessDefault(info = "默认规则")
    private Double completeness;

    @JsonIgnore
    private boolean completenessDirtyFlag;

    /**
     * 属性 [TARGET_GOAL]
     *
     */
    @Gamification_goalTarget_goalDefault(info = "默认规则")
    private Double target_goal;

    @JsonIgnore
    private boolean target_goalDirtyFlag;

    /**
     * 属性 [CREATE_DATE]
     *
     */
    @Gamification_goalCreate_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp create_date;

    @JsonIgnore
    private boolean create_dateDirtyFlag;

    /**
     * 属性 [CLOSED]
     *
     */
    @Gamification_goalClosedDefault(info = "默认规则")
    private String closed;

    @JsonIgnore
    private boolean closedDirtyFlag;

    /**
     * 属性 [ID]
     *
     */
    @Gamification_goalIdDefault(info = "默认规则")
    private Integer id;

    @JsonIgnore
    private boolean idDirtyFlag;

    /**
     * 属性 [LAST_UPDATE]
     *
     */
    @Gamification_goalLast_updateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    private Timestamp last_update;

    @JsonIgnore
    private boolean last_updateDirtyFlag;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @Gamification_goalDisplay_nameDefault(info = "默认规则")
    private String display_name;

    @JsonIgnore
    private boolean display_nameDirtyFlag;

    /**
     * 属性 [END_DATE]
     *
     */
    @Gamification_goalEnd_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    private Timestamp end_date;

    @JsonIgnore
    private boolean end_dateDirtyFlag;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @Gamification_goal__last_updateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp __last_update;

    @JsonIgnore
    private boolean __last_updateDirtyFlag;

    /**
     * 属性 [CURRENT]
     *
     */
    @Gamification_goalCurrentDefault(info = "默认规则")
    private Double current;

    @JsonIgnore
    private boolean currentDirtyFlag;

    /**
     * 属性 [DEFINITION_DESCRIPTION]
     *
     */
    @Gamification_goalDefinition_descriptionDefault(info = "默认规则")
    private String definition_description;

    @JsonIgnore
    private boolean definition_descriptionDirtyFlag;

    /**
     * 属性 [CREATE_UID_TEXT]
     *
     */
    @Gamification_goalCreate_uid_textDefault(info = "默认规则")
    private String create_uid_text;

    @JsonIgnore
    private boolean create_uid_textDirtyFlag;

    /**
     * 属性 [DEFINITION_SUFFIX]
     *
     */
    @Gamification_goalDefinition_suffixDefault(info = "默认规则")
    private String definition_suffix;

    @JsonIgnore
    private boolean definition_suffixDirtyFlag;

    /**
     * 属性 [CHALLENGE_ID_TEXT]
     *
     */
    @Gamification_goalChallenge_id_textDefault(info = "默认规则")
    private String challenge_id_text;

    @JsonIgnore
    private boolean challenge_id_textDirtyFlag;

    /**
     * 属性 [WRITE_UID_TEXT]
     *
     */
    @Gamification_goalWrite_uid_textDefault(info = "默认规则")
    private String write_uid_text;

    @JsonIgnore
    private boolean write_uid_textDirtyFlag;

    /**
     * 属性 [DEFINITION_ID_TEXT]
     *
     */
    @Gamification_goalDefinition_id_textDefault(info = "默认规则")
    private String definition_id_text;

    @JsonIgnore
    private boolean definition_id_textDirtyFlag;

    /**
     * 属性 [DEFINITION_DISPLAY]
     *
     */
    @Gamification_goalDefinition_displayDefault(info = "默认规则")
    private String definition_display;

    @JsonIgnore
    private boolean definition_displayDirtyFlag;

    /**
     * 属性 [LINE_ID_TEXT]
     *
     */
    @Gamification_goalLine_id_textDefault(info = "默认规则")
    private String line_id_text;

    @JsonIgnore
    private boolean line_id_textDirtyFlag;

    /**
     * 属性 [USER_ID_TEXT]
     *
     */
    @Gamification_goalUser_id_textDefault(info = "默认规则")
    private String user_id_text;

    @JsonIgnore
    private boolean user_id_textDirtyFlag;

    /**
     * 属性 [DEFINITION_CONDITION]
     *
     */
    @Gamification_goalDefinition_conditionDefault(info = "默认规则")
    private String definition_condition;

    @JsonIgnore
    private boolean definition_conditionDirtyFlag;

    /**
     * 属性 [COMPUTATION_MODE]
     *
     */
    @Gamification_goalComputation_modeDefault(info = "默认规则")
    private String computation_mode;

    @JsonIgnore
    private boolean computation_modeDirtyFlag;

    /**
     * 属性 [WRITE_UID]
     *
     */
    @Gamification_goalWrite_uidDefault(info = "默认规则")
    private Integer write_uid;

    @JsonIgnore
    private boolean write_uidDirtyFlag;

    /**
     * 属性 [LINE_ID]
     *
     */
    @Gamification_goalLine_idDefault(info = "默认规则")
    private Integer line_id;

    @JsonIgnore
    private boolean line_idDirtyFlag;

    /**
     * 属性 [CREATE_UID]
     *
     */
    @Gamification_goalCreate_uidDefault(info = "默认规则")
    private Integer create_uid;

    @JsonIgnore
    private boolean create_uidDirtyFlag;

    /**
     * 属性 [DEFINITION_ID]
     *
     */
    @Gamification_goalDefinition_idDefault(info = "默认规则")
    private Integer definition_id;

    @JsonIgnore
    private boolean definition_idDirtyFlag;

    /**
     * 属性 [CHALLENGE_ID]
     *
     */
    @Gamification_goalChallenge_idDefault(info = "默认规则")
    private Integer challenge_id;

    @JsonIgnore
    private boolean challenge_idDirtyFlag;

    /**
     * 属性 [USER_ID]
     *
     */
    @Gamification_goalUser_idDefault(info = "默认规则")
    private Integer user_id;

    @JsonIgnore
    private boolean user_idDirtyFlag;


    /**
     * 获取 [START_DATE]
     */
    @JsonProperty("start_date")
    public Timestamp getStart_date(){
        return start_date ;
    }

    /**
     * 设置 [START_DATE]
     */
    @JsonProperty("start_date")
    public void setStart_date(Timestamp  start_date){
        this.start_date = start_date ;
        this.start_dateDirtyFlag = true ;
    }

    /**
     * 获取 [START_DATE]脏标记
     */
    @JsonIgnore
    public boolean getStart_dateDirtyFlag(){
        return start_dateDirtyFlag ;
    }

    /**
     * 获取 [TO_UPDATE]
     */
    @JsonProperty("to_update")
    public String getTo_update(){
        return to_update ;
    }

    /**
     * 设置 [TO_UPDATE]
     */
    @JsonProperty("to_update")
    public void setTo_update(String  to_update){
        this.to_update = to_update ;
        this.to_updateDirtyFlag = true ;
    }

    /**
     * 获取 [TO_UPDATE]脏标记
     */
    @JsonIgnore
    public boolean getTo_updateDirtyFlag(){
        return to_updateDirtyFlag ;
    }

    /**
     * 获取 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return write_date ;
    }

    /**
     * 设置 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return write_dateDirtyFlag ;
    }

    /**
     * 获取 [REMIND_UPDATE_DELAY]
     */
    @JsonProperty("remind_update_delay")
    public Integer getRemind_update_delay(){
        return remind_update_delay ;
    }

    /**
     * 设置 [REMIND_UPDATE_DELAY]
     */
    @JsonProperty("remind_update_delay")
    public void setRemind_update_delay(Integer  remind_update_delay){
        this.remind_update_delay = remind_update_delay ;
        this.remind_update_delayDirtyFlag = true ;
    }

    /**
     * 获取 [REMIND_UPDATE_DELAY]脏标记
     */
    @JsonIgnore
    public boolean getRemind_update_delayDirtyFlag(){
        return remind_update_delayDirtyFlag ;
    }

    /**
     * 获取 [STATE]
     */
    @JsonProperty("state")
    public String getState(){
        return state ;
    }

    /**
     * 设置 [STATE]
     */
    @JsonProperty("state")
    public void setState(String  state){
        this.state = state ;
        this.stateDirtyFlag = true ;
    }

    /**
     * 获取 [STATE]脏标记
     */
    @JsonIgnore
    public boolean getStateDirtyFlag(){
        return stateDirtyFlag ;
    }

    /**
     * 获取 [COMPLETENESS]
     */
    @JsonProperty("completeness")
    public Double getCompleteness(){
        return completeness ;
    }

    /**
     * 设置 [COMPLETENESS]
     */
    @JsonProperty("completeness")
    public void setCompleteness(Double  completeness){
        this.completeness = completeness ;
        this.completenessDirtyFlag = true ;
    }

    /**
     * 获取 [COMPLETENESS]脏标记
     */
    @JsonIgnore
    public boolean getCompletenessDirtyFlag(){
        return completenessDirtyFlag ;
    }

    /**
     * 获取 [TARGET_GOAL]
     */
    @JsonProperty("target_goal")
    public Double getTarget_goal(){
        return target_goal ;
    }

    /**
     * 设置 [TARGET_GOAL]
     */
    @JsonProperty("target_goal")
    public void setTarget_goal(Double  target_goal){
        this.target_goal = target_goal ;
        this.target_goalDirtyFlag = true ;
    }

    /**
     * 获取 [TARGET_GOAL]脏标记
     */
    @JsonIgnore
    public boolean getTarget_goalDirtyFlag(){
        return target_goalDirtyFlag ;
    }

    /**
     * 获取 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return create_date ;
    }

    /**
     * 设置 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return create_dateDirtyFlag ;
    }

    /**
     * 获取 [CLOSED]
     */
    @JsonProperty("closed")
    public String getClosed(){
        return closed ;
    }

    /**
     * 设置 [CLOSED]
     */
    @JsonProperty("closed")
    public void setClosed(String  closed){
        this.closed = closed ;
        this.closedDirtyFlag = true ;
    }

    /**
     * 获取 [CLOSED]脏标记
     */
    @JsonIgnore
    public boolean getClosedDirtyFlag(){
        return closedDirtyFlag ;
    }

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

    /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return idDirtyFlag ;
    }

    /**
     * 获取 [LAST_UPDATE]
     */
    @JsonProperty("last_update")
    public Timestamp getLast_update(){
        return last_update ;
    }

    /**
     * 设置 [LAST_UPDATE]
     */
    @JsonProperty("last_update")
    public void setLast_update(Timestamp  last_update){
        this.last_update = last_update ;
        this.last_updateDirtyFlag = true ;
    }

    /**
     * 获取 [LAST_UPDATE]脏标记
     */
    @JsonIgnore
    public boolean getLast_updateDirtyFlag(){
        return last_updateDirtyFlag ;
    }

    /**
     * 获取 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return display_name ;
    }

    /**
     * 设置 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

    /**
     * 获取 [DISPLAY_NAME]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return display_nameDirtyFlag ;
    }

    /**
     * 获取 [END_DATE]
     */
    @JsonProperty("end_date")
    public Timestamp getEnd_date(){
        return end_date ;
    }

    /**
     * 设置 [END_DATE]
     */
    @JsonProperty("end_date")
    public void setEnd_date(Timestamp  end_date){
        this.end_date = end_date ;
        this.end_dateDirtyFlag = true ;
    }

    /**
     * 获取 [END_DATE]脏标记
     */
    @JsonIgnore
    public boolean getEnd_dateDirtyFlag(){
        return end_dateDirtyFlag ;
    }

    /**
     * 获取 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return __last_update ;
    }

    /**
     * 设置 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

    /**
     * 获取 [__LAST_UPDATE]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return __last_updateDirtyFlag ;
    }

    /**
     * 获取 [CURRENT]
     */
    @JsonProperty("current")
    public Double getCurrent(){
        return current ;
    }

    /**
     * 设置 [CURRENT]
     */
    @JsonProperty("current")
    public void setCurrent(Double  current){
        this.current = current ;
        this.currentDirtyFlag = true ;
    }

    /**
     * 获取 [CURRENT]脏标记
     */
    @JsonIgnore
    public boolean getCurrentDirtyFlag(){
        return currentDirtyFlag ;
    }

    /**
     * 获取 [DEFINITION_DESCRIPTION]
     */
    @JsonProperty("definition_description")
    public String getDefinition_description(){
        return definition_description ;
    }

    /**
     * 设置 [DEFINITION_DESCRIPTION]
     */
    @JsonProperty("definition_description")
    public void setDefinition_description(String  definition_description){
        this.definition_description = definition_description ;
        this.definition_descriptionDirtyFlag = true ;
    }

    /**
     * 获取 [DEFINITION_DESCRIPTION]脏标记
     */
    @JsonIgnore
    public boolean getDefinition_descriptionDirtyFlag(){
        return definition_descriptionDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return create_uid_text ;
    }

    /**
     * 设置 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return create_uid_textDirtyFlag ;
    }

    /**
     * 获取 [DEFINITION_SUFFIX]
     */
    @JsonProperty("definition_suffix")
    public String getDefinition_suffix(){
        return definition_suffix ;
    }

    /**
     * 设置 [DEFINITION_SUFFIX]
     */
    @JsonProperty("definition_suffix")
    public void setDefinition_suffix(String  definition_suffix){
        this.definition_suffix = definition_suffix ;
        this.definition_suffixDirtyFlag = true ;
    }

    /**
     * 获取 [DEFINITION_SUFFIX]脏标记
     */
    @JsonIgnore
    public boolean getDefinition_suffixDirtyFlag(){
        return definition_suffixDirtyFlag ;
    }

    /**
     * 获取 [CHALLENGE_ID_TEXT]
     */
    @JsonProperty("challenge_id_text")
    public String getChallenge_id_text(){
        return challenge_id_text ;
    }

    /**
     * 设置 [CHALLENGE_ID_TEXT]
     */
    @JsonProperty("challenge_id_text")
    public void setChallenge_id_text(String  challenge_id_text){
        this.challenge_id_text = challenge_id_text ;
        this.challenge_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [CHALLENGE_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getChallenge_id_textDirtyFlag(){
        return challenge_id_textDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return write_uid_text ;
    }

    /**
     * 设置 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return write_uid_textDirtyFlag ;
    }

    /**
     * 获取 [DEFINITION_ID_TEXT]
     */
    @JsonProperty("definition_id_text")
    public String getDefinition_id_text(){
        return definition_id_text ;
    }

    /**
     * 设置 [DEFINITION_ID_TEXT]
     */
    @JsonProperty("definition_id_text")
    public void setDefinition_id_text(String  definition_id_text){
        this.definition_id_text = definition_id_text ;
        this.definition_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [DEFINITION_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getDefinition_id_textDirtyFlag(){
        return definition_id_textDirtyFlag ;
    }

    /**
     * 获取 [DEFINITION_DISPLAY]
     */
    @JsonProperty("definition_display")
    public String getDefinition_display(){
        return definition_display ;
    }

    /**
     * 设置 [DEFINITION_DISPLAY]
     */
    @JsonProperty("definition_display")
    public void setDefinition_display(String  definition_display){
        this.definition_display = definition_display ;
        this.definition_displayDirtyFlag = true ;
    }

    /**
     * 获取 [DEFINITION_DISPLAY]脏标记
     */
    @JsonIgnore
    public boolean getDefinition_displayDirtyFlag(){
        return definition_displayDirtyFlag ;
    }

    /**
     * 获取 [LINE_ID_TEXT]
     */
    @JsonProperty("line_id_text")
    public String getLine_id_text(){
        return line_id_text ;
    }

    /**
     * 设置 [LINE_ID_TEXT]
     */
    @JsonProperty("line_id_text")
    public void setLine_id_text(String  line_id_text){
        this.line_id_text = line_id_text ;
        this.line_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [LINE_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getLine_id_textDirtyFlag(){
        return line_id_textDirtyFlag ;
    }

    /**
     * 获取 [USER_ID_TEXT]
     */
    @JsonProperty("user_id_text")
    public String getUser_id_text(){
        return user_id_text ;
    }

    /**
     * 设置 [USER_ID_TEXT]
     */
    @JsonProperty("user_id_text")
    public void setUser_id_text(String  user_id_text){
        this.user_id_text = user_id_text ;
        this.user_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [USER_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getUser_id_textDirtyFlag(){
        return user_id_textDirtyFlag ;
    }

    /**
     * 获取 [DEFINITION_CONDITION]
     */
    @JsonProperty("definition_condition")
    public String getDefinition_condition(){
        return definition_condition ;
    }

    /**
     * 设置 [DEFINITION_CONDITION]
     */
    @JsonProperty("definition_condition")
    public void setDefinition_condition(String  definition_condition){
        this.definition_condition = definition_condition ;
        this.definition_conditionDirtyFlag = true ;
    }

    /**
     * 获取 [DEFINITION_CONDITION]脏标记
     */
    @JsonIgnore
    public boolean getDefinition_conditionDirtyFlag(){
        return definition_conditionDirtyFlag ;
    }

    /**
     * 获取 [COMPUTATION_MODE]
     */
    @JsonProperty("computation_mode")
    public String getComputation_mode(){
        return computation_mode ;
    }

    /**
     * 设置 [COMPUTATION_MODE]
     */
    @JsonProperty("computation_mode")
    public void setComputation_mode(String  computation_mode){
        this.computation_mode = computation_mode ;
        this.computation_modeDirtyFlag = true ;
    }

    /**
     * 获取 [COMPUTATION_MODE]脏标记
     */
    @JsonIgnore
    public boolean getComputation_modeDirtyFlag(){
        return computation_modeDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return write_uid ;
    }

    /**
     * 设置 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return write_uidDirtyFlag ;
    }

    /**
     * 获取 [LINE_ID]
     */
    @JsonProperty("line_id")
    public Integer getLine_id(){
        return line_id ;
    }

    /**
     * 设置 [LINE_ID]
     */
    @JsonProperty("line_id")
    public void setLine_id(Integer  line_id){
        this.line_id = line_id ;
        this.line_idDirtyFlag = true ;
    }

    /**
     * 获取 [LINE_ID]脏标记
     */
    @JsonIgnore
    public boolean getLine_idDirtyFlag(){
        return line_idDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return create_uid ;
    }

    /**
     * 设置 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return create_uidDirtyFlag ;
    }

    /**
     * 获取 [DEFINITION_ID]
     */
    @JsonProperty("definition_id")
    public Integer getDefinition_id(){
        return definition_id ;
    }

    /**
     * 设置 [DEFINITION_ID]
     */
    @JsonProperty("definition_id")
    public void setDefinition_id(Integer  definition_id){
        this.definition_id = definition_id ;
        this.definition_idDirtyFlag = true ;
    }

    /**
     * 获取 [DEFINITION_ID]脏标记
     */
    @JsonIgnore
    public boolean getDefinition_idDirtyFlag(){
        return definition_idDirtyFlag ;
    }

    /**
     * 获取 [CHALLENGE_ID]
     */
    @JsonProperty("challenge_id")
    public Integer getChallenge_id(){
        return challenge_id ;
    }

    /**
     * 设置 [CHALLENGE_ID]
     */
    @JsonProperty("challenge_id")
    public void setChallenge_id(Integer  challenge_id){
        this.challenge_id = challenge_id ;
        this.challenge_idDirtyFlag = true ;
    }

    /**
     * 获取 [CHALLENGE_ID]脏标记
     */
    @JsonIgnore
    public boolean getChallenge_idDirtyFlag(){
        return challenge_idDirtyFlag ;
    }

    /**
     * 获取 [USER_ID]
     */
    @JsonProperty("user_id")
    public Integer getUser_id(){
        return user_id ;
    }

    /**
     * 设置 [USER_ID]
     */
    @JsonProperty("user_id")
    public void setUser_id(Integer  user_id){
        this.user_id = user_id ;
        this.user_idDirtyFlag = true ;
    }

    /**
     * 获取 [USER_ID]脏标记
     */
    @JsonIgnore
    public boolean getUser_idDirtyFlag(){
        return user_idDirtyFlag ;
    }



    public Gamification_goal toDO() {
        Gamification_goal srfdomain = new Gamification_goal();
        if(getStart_dateDirtyFlag())
            srfdomain.setStart_date(start_date);
        if(getTo_updateDirtyFlag())
            srfdomain.setTo_update(to_update);
        if(getWrite_dateDirtyFlag())
            srfdomain.setWrite_date(write_date);
        if(getRemind_update_delayDirtyFlag())
            srfdomain.setRemind_update_delay(remind_update_delay);
        if(getStateDirtyFlag())
            srfdomain.setState(state);
        if(getCompletenessDirtyFlag())
            srfdomain.setCompleteness(completeness);
        if(getTarget_goalDirtyFlag())
            srfdomain.setTarget_goal(target_goal);
        if(getCreate_dateDirtyFlag())
            srfdomain.setCreate_date(create_date);
        if(getClosedDirtyFlag())
            srfdomain.setClosed(closed);
        if(getIdDirtyFlag())
            srfdomain.setId(id);
        if(getLast_updateDirtyFlag())
            srfdomain.setLast_update(last_update);
        if(getDisplay_nameDirtyFlag())
            srfdomain.setDisplay_name(display_name);
        if(getEnd_dateDirtyFlag())
            srfdomain.setEnd_date(end_date);
        if(get__last_updateDirtyFlag())
            srfdomain.set__last_update(__last_update);
        if(getCurrentDirtyFlag())
            srfdomain.setCurrent(current);
        if(getDefinition_descriptionDirtyFlag())
            srfdomain.setDefinition_description(definition_description);
        if(getCreate_uid_textDirtyFlag())
            srfdomain.setCreate_uid_text(create_uid_text);
        if(getDefinition_suffixDirtyFlag())
            srfdomain.setDefinition_suffix(definition_suffix);
        if(getChallenge_id_textDirtyFlag())
            srfdomain.setChallenge_id_text(challenge_id_text);
        if(getWrite_uid_textDirtyFlag())
            srfdomain.setWrite_uid_text(write_uid_text);
        if(getDefinition_id_textDirtyFlag())
            srfdomain.setDefinition_id_text(definition_id_text);
        if(getDefinition_displayDirtyFlag())
            srfdomain.setDefinition_display(definition_display);
        if(getLine_id_textDirtyFlag())
            srfdomain.setLine_id_text(line_id_text);
        if(getUser_id_textDirtyFlag())
            srfdomain.setUser_id_text(user_id_text);
        if(getDefinition_conditionDirtyFlag())
            srfdomain.setDefinition_condition(definition_condition);
        if(getComputation_modeDirtyFlag())
            srfdomain.setComputation_mode(computation_mode);
        if(getWrite_uidDirtyFlag())
            srfdomain.setWrite_uid(write_uid);
        if(getLine_idDirtyFlag())
            srfdomain.setLine_id(line_id);
        if(getCreate_uidDirtyFlag())
            srfdomain.setCreate_uid(create_uid);
        if(getDefinition_idDirtyFlag())
            srfdomain.setDefinition_id(definition_id);
        if(getChallenge_idDirtyFlag())
            srfdomain.setChallenge_id(challenge_id);
        if(getUser_idDirtyFlag())
            srfdomain.setUser_id(user_id);

        return srfdomain;
    }

    public void fromDO(Gamification_goal srfdomain) {
        if(srfdomain == null )
          return ;
        if(srfdomain.getStart_dateDirtyFlag())
            this.setStart_date(srfdomain.getStart_date());
        if(srfdomain.getTo_updateDirtyFlag())
            this.setTo_update(srfdomain.getTo_update());
        if(srfdomain.getWrite_dateDirtyFlag())
            this.setWrite_date(srfdomain.getWrite_date());
        if(srfdomain.getRemind_update_delayDirtyFlag())
            this.setRemind_update_delay(srfdomain.getRemind_update_delay());
        if(srfdomain.getStateDirtyFlag())
            this.setState(srfdomain.getState());
        if(srfdomain.getCompletenessDirtyFlag())
            this.setCompleteness(srfdomain.getCompleteness());
        if(srfdomain.getTarget_goalDirtyFlag())
            this.setTarget_goal(srfdomain.getTarget_goal());
        if(srfdomain.getCreate_dateDirtyFlag())
            this.setCreate_date(srfdomain.getCreate_date());
        if(srfdomain.getClosedDirtyFlag())
            this.setClosed(srfdomain.getClosed());
        if(srfdomain.getIdDirtyFlag())
            this.setId(srfdomain.getId());
        if(srfdomain.getLast_updateDirtyFlag())
            this.setLast_update(srfdomain.getLast_update());
        if(srfdomain.getDisplay_nameDirtyFlag())
            this.setDisplay_name(srfdomain.getDisplay_name());
        if(srfdomain.getEnd_dateDirtyFlag())
            this.setEnd_date(srfdomain.getEnd_date());
        if(srfdomain.get__last_updateDirtyFlag())
            this.set__last_update(srfdomain.get__last_update());
        if(srfdomain.getCurrentDirtyFlag())
            this.setCurrent(srfdomain.getCurrent());
        if(srfdomain.getDefinition_descriptionDirtyFlag())
            this.setDefinition_description(srfdomain.getDefinition_description());
        if(srfdomain.getCreate_uid_textDirtyFlag())
            this.setCreate_uid_text(srfdomain.getCreate_uid_text());
        if(srfdomain.getDefinition_suffixDirtyFlag())
            this.setDefinition_suffix(srfdomain.getDefinition_suffix());
        if(srfdomain.getChallenge_id_textDirtyFlag())
            this.setChallenge_id_text(srfdomain.getChallenge_id_text());
        if(srfdomain.getWrite_uid_textDirtyFlag())
            this.setWrite_uid_text(srfdomain.getWrite_uid_text());
        if(srfdomain.getDefinition_id_textDirtyFlag())
            this.setDefinition_id_text(srfdomain.getDefinition_id_text());
        if(srfdomain.getDefinition_displayDirtyFlag())
            this.setDefinition_display(srfdomain.getDefinition_display());
        if(srfdomain.getLine_id_textDirtyFlag())
            this.setLine_id_text(srfdomain.getLine_id_text());
        if(srfdomain.getUser_id_textDirtyFlag())
            this.setUser_id_text(srfdomain.getUser_id_text());
        if(srfdomain.getDefinition_conditionDirtyFlag())
            this.setDefinition_condition(srfdomain.getDefinition_condition());
        if(srfdomain.getComputation_modeDirtyFlag())
            this.setComputation_mode(srfdomain.getComputation_mode());
        if(srfdomain.getWrite_uidDirtyFlag())
            this.setWrite_uid(srfdomain.getWrite_uid());
        if(srfdomain.getLine_idDirtyFlag())
            this.setLine_id(srfdomain.getLine_id());
        if(srfdomain.getCreate_uidDirtyFlag())
            this.setCreate_uid(srfdomain.getCreate_uid());
        if(srfdomain.getDefinition_idDirtyFlag())
            this.setDefinition_id(srfdomain.getDefinition_id());
        if(srfdomain.getChallenge_idDirtyFlag())
            this.setChallenge_id(srfdomain.getChallenge_id());
        if(srfdomain.getUser_idDirtyFlag())
            this.setUser_id(srfdomain.getUser_id());

    }

    public List<Gamification_goalDTO> fromDOPage(List<Gamification_goal> poPage)   {
        if(poPage == null)
            return null;
        List<Gamification_goalDTO> dtos=new ArrayList<Gamification_goalDTO>();
        for(Gamification_goal domain : poPage) {
            Gamification_goalDTO dto = new Gamification_goalDTO();
            dto.fromDO(domain);
            dtos.add(dto);
        }
        return dtos;
    }
}

