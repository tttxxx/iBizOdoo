package cn.ibizlab.odoo.service.odoo_payment.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_payment.dto.Payment_iconDTO;
import cn.ibizlab.odoo.core.odoo_payment.domain.Payment_icon;
import cn.ibizlab.odoo.core.odoo_payment.service.IPayment_iconService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_payment.filter.Payment_iconSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Payment_icon" })
@RestController
@RequestMapping("")
public class Payment_iconResource {

    @Autowired
    private IPayment_iconService payment_iconService;

    public IPayment_iconService getPayment_iconService() {
        return this.payment_iconService;
    }

    @ApiOperation(value = "更新数据", tags = {"Payment_icon" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_payment/payment_icons/{payment_icon_id}")

    public ResponseEntity<Payment_iconDTO> update(@PathVariable("payment_icon_id") Integer payment_icon_id, @RequestBody Payment_iconDTO payment_icondto) {
		Payment_icon domain = payment_icondto.toDO();
        domain.setId(payment_icon_id);
		payment_iconService.update(domain);
		Payment_iconDTO dto = new Payment_iconDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批更新数据", tags = {"Payment_icon" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_payment/payment_icons/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Payment_iconDTO> payment_icondtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "删除数据", tags = {"Payment_icon" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_payment/payment_icons/{payment_icon_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("payment_icon_id") Integer payment_icon_id) {
        Payment_iconDTO payment_icondto = new Payment_iconDTO();
		Payment_icon domain = new Payment_icon();
		payment_icondto.setId(payment_icon_id);
		domain.setId(payment_icon_id);
        Boolean rst = payment_iconService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "获取数据", tags = {"Payment_icon" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_payment/payment_icons/{payment_icon_id}")
    public ResponseEntity<Payment_iconDTO> get(@PathVariable("payment_icon_id") Integer payment_icon_id) {
        Payment_iconDTO dto = new Payment_iconDTO();
        Payment_icon domain = payment_iconService.get(payment_icon_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批删除数据", tags = {"Payment_icon" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_payment/payment_icons/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Payment_iconDTO> payment_icondtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "建立数据", tags = {"Payment_icon" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_payment/payment_icons")

    public ResponseEntity<Payment_iconDTO> create(@RequestBody Payment_iconDTO payment_icondto) {
        Payment_iconDTO dto = new Payment_iconDTO();
        Payment_icon domain = payment_icondto.toDO();
		payment_iconService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批建立数据", tags = {"Payment_icon" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_payment/payment_icons/createBatch")
    public ResponseEntity<Boolean> createBatchPayment_icon(@RequestBody List<Payment_iconDTO> payment_icondtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Payment_icon" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_payment/payment_icons/fetchdefault")
	public ResponseEntity<Page<Payment_iconDTO>> fetchDefault(Payment_iconSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Payment_iconDTO> list = new ArrayList<Payment_iconDTO>();
        
        Page<Payment_icon> domains = payment_iconService.searchDefault(context) ;
        for(Payment_icon payment_icon : domains.getContent()){
            Payment_iconDTO dto = new Payment_iconDTO();
            dto.fromDO(payment_icon);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
