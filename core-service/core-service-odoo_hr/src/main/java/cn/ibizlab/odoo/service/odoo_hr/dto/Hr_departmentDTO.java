package cn.ibizlab.odoo.service.odoo_hr.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.core.odoo_hr.valuerule.anno.hr_department.*;
import cn.ibizlab.odoo.core.odoo_hr.domain.Hr_department;
import org.springframework.cglib.beans.BeanCopier;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 服务DTO对象[Hr_departmentDTO]
 */
public class Hr_departmentDTO implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [MESSAGE_NEEDACTION]
     *
     */
    @Hr_departmentMessage_needactionDefault(info = "默认规则")
    private String message_needaction;

    @JsonIgnore
    private boolean message_needactionDirtyFlag;

    /**
     * 属性 [MESSAGE_FOLLOWER_IDS]
     *
     */
    @Hr_departmentMessage_follower_idsDefault(info = "默认规则")
    private String message_follower_ids;

    @JsonIgnore
    private boolean message_follower_idsDirtyFlag;

    /**
     * 属性 [ABSENCE_OF_TODAY]
     *
     */
    @Hr_departmentAbsence_of_todayDefault(info = "默认规则")
    private Integer absence_of_today;

    @JsonIgnore
    private boolean absence_of_todayDirtyFlag;

    /**
     * 属性 [MESSAGE_UNREAD]
     *
     */
    @Hr_departmentMessage_unreadDefault(info = "默认规则")
    private String message_unread;

    @JsonIgnore
    private boolean message_unreadDirtyFlag;

    /**
     * 属性 [LEAVE_TO_APPROVE_COUNT]
     *
     */
    @Hr_departmentLeave_to_approve_countDefault(info = "默认规则")
    private Integer leave_to_approve_count;

    @JsonIgnore
    private boolean leave_to_approve_countDirtyFlag;

    /**
     * 属性 [COLOR]
     *
     */
    @Hr_departmentColorDefault(info = "默认规则")
    private Integer color;

    @JsonIgnore
    private boolean colorDirtyFlag;

    /**
     * 属性 [MESSAGE_PARTNER_IDS]
     *
     */
    @Hr_departmentMessage_partner_idsDefault(info = "默认规则")
    private String message_partner_ids;

    @JsonIgnore
    private boolean message_partner_idsDirtyFlag;

    /**
     * 属性 [MESSAGE_MAIN_ATTACHMENT_ID]
     *
     */
    @Hr_departmentMessage_main_attachment_idDefault(info = "默认规则")
    private Integer message_main_attachment_id;

    @JsonIgnore
    private boolean message_main_attachment_idDirtyFlag;

    /**
     * 属性 [MESSAGE_UNREAD_COUNTER]
     *
     */
    @Hr_departmentMessage_unread_counterDefault(info = "默认规则")
    private Integer message_unread_counter;

    @JsonIgnore
    private boolean message_unread_counterDirtyFlag;

    /**
     * 属性 [NAME]
     *
     */
    @Hr_departmentNameDefault(info = "默认规则")
    private String name;

    @JsonIgnore
    private boolean nameDirtyFlag;

    /**
     * 属性 [MESSAGE_IS_FOLLOWER]
     *
     */
    @Hr_departmentMessage_is_followerDefault(info = "默认规则")
    private String message_is_follower;

    @JsonIgnore
    private boolean message_is_followerDirtyFlag;

    /**
     * 属性 [ACTIVE]
     *
     */
    @Hr_departmentActiveDefault(info = "默认规则")
    private String active;

    @JsonIgnore
    private boolean activeDirtyFlag;

    /**
     * 属性 [WRITE_DATE]
     *
     */
    @Hr_departmentWrite_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp write_date;

    @JsonIgnore
    private boolean write_dateDirtyFlag;

    /**
     * 属性 [EXPENSE_SHEETS_TO_APPROVE_COUNT]
     *
     */
    @Hr_departmentExpense_sheets_to_approve_countDefault(info = "默认规则")
    private Integer expense_sheets_to_approve_count;

    @JsonIgnore
    private boolean expense_sheets_to_approve_countDirtyFlag;

    /**
     * 属性 [ALLOCATION_TO_APPROVE_COUNT]
     *
     */
    @Hr_departmentAllocation_to_approve_countDefault(info = "默认规则")
    private Integer allocation_to_approve_count;

    @JsonIgnore
    private boolean allocation_to_approve_countDirtyFlag;

    /**
     * 属性 [NEW_HIRED_EMPLOYEE]
     *
     */
    @Hr_departmentNew_hired_employeeDefault(info = "默认规则")
    private Integer new_hired_employee;

    @JsonIgnore
    private boolean new_hired_employeeDirtyFlag;

    /**
     * 属性 [NOTE]
     *
     */
    @Hr_departmentNoteDefault(info = "默认规则")
    private String note;

    @JsonIgnore
    private boolean noteDirtyFlag;

    /**
     * 属性 [CHILD_IDS]
     *
     */
    @Hr_departmentChild_idsDefault(info = "默认规则")
    private String child_ids;

    @JsonIgnore
    private boolean child_idsDirtyFlag;

    /**
     * 属性 [MESSAGE_CHANNEL_IDS]
     *
     */
    @Hr_departmentMessage_channel_idsDefault(info = "默认规则")
    private String message_channel_ids;

    @JsonIgnore
    private boolean message_channel_idsDirtyFlag;

    /**
     * 属性 [MESSAGE_HAS_ERROR_COUNTER]
     *
     */
    @Hr_departmentMessage_has_error_counterDefault(info = "默认规则")
    private Integer message_has_error_counter;

    @JsonIgnore
    private boolean message_has_error_counterDirtyFlag;

    /**
     * 属性 [EXPECTED_EMPLOYEE]
     *
     */
    @Hr_departmentExpected_employeeDefault(info = "默认规则")
    private Integer expected_employee;

    @JsonIgnore
    private boolean expected_employeeDirtyFlag;

    /**
     * 属性 [WEBSITE_MESSAGE_IDS]
     *
     */
    @Hr_departmentWebsite_message_idsDefault(info = "默认规则")
    private String website_message_ids;

    @JsonIgnore
    private boolean website_message_idsDirtyFlag;

    /**
     * 属性 [MESSAGE_HAS_ERROR]
     *
     */
    @Hr_departmentMessage_has_errorDefault(info = "默认规则")
    private String message_has_error;

    @JsonIgnore
    private boolean message_has_errorDirtyFlag;

    /**
     * 属性 [TOTAL_EMPLOYEE]
     *
     */
    @Hr_departmentTotal_employeeDefault(info = "默认规则")
    private Integer total_employee;

    @JsonIgnore
    private boolean total_employeeDirtyFlag;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @Hr_department__last_updateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp __last_update;

    @JsonIgnore
    private boolean __last_updateDirtyFlag;

    /**
     * 属性 [ID]
     *
     */
    @Hr_departmentIdDefault(info = "默认规则")
    private Integer id;

    @JsonIgnore
    private boolean idDirtyFlag;

    /**
     * 属性 [JOBS_IDS]
     *
     */
    @Hr_departmentJobs_idsDefault(info = "默认规则")
    private String jobs_ids;

    @JsonIgnore
    private boolean jobs_idsDirtyFlag;

    /**
     * 属性 [MESSAGE_IDS]
     *
     */
    @Hr_departmentMessage_idsDefault(info = "默认规则")
    private String message_ids;

    @JsonIgnore
    private boolean message_idsDirtyFlag;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @Hr_departmentDisplay_nameDefault(info = "默认规则")
    private String display_name;

    @JsonIgnore
    private boolean display_nameDirtyFlag;

    /**
     * 属性 [MESSAGE_ATTACHMENT_COUNT]
     *
     */
    @Hr_departmentMessage_attachment_countDefault(info = "默认规则")
    private Integer message_attachment_count;

    @JsonIgnore
    private boolean message_attachment_countDirtyFlag;

    /**
     * 属性 [NEW_APPLICANT_COUNT]
     *
     */
    @Hr_departmentNew_applicant_countDefault(info = "默认规则")
    private Integer new_applicant_count;

    @JsonIgnore
    private boolean new_applicant_countDirtyFlag;

    /**
     * 属性 [CREATE_DATE]
     *
     */
    @Hr_departmentCreate_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp create_date;

    @JsonIgnore
    private boolean create_dateDirtyFlag;

    /**
     * 属性 [COMPLETE_NAME]
     *
     */
    @Hr_departmentComplete_nameDefault(info = "默认规则")
    private String complete_name;

    @JsonIgnore
    private boolean complete_nameDirtyFlag;

    /**
     * 属性 [MESSAGE_NEEDACTION_COUNTER]
     *
     */
    @Hr_departmentMessage_needaction_counterDefault(info = "默认规则")
    private Integer message_needaction_counter;

    @JsonIgnore
    private boolean message_needaction_counterDirtyFlag;

    /**
     * 属性 [MEMBER_IDS]
     *
     */
    @Hr_departmentMember_idsDefault(info = "默认规则")
    private String member_ids;

    @JsonIgnore
    private boolean member_idsDirtyFlag;

    /**
     * 属性 [WRITE_UID_TEXT]
     *
     */
    @Hr_departmentWrite_uid_textDefault(info = "默认规则")
    private String write_uid_text;

    @JsonIgnore
    private boolean write_uid_textDirtyFlag;

    /**
     * 属性 [MANAGER_ID_TEXT]
     *
     */
    @Hr_departmentManager_id_textDefault(info = "默认规则")
    private String manager_id_text;

    @JsonIgnore
    private boolean manager_id_textDirtyFlag;

    /**
     * 属性 [CREATE_UID_TEXT]
     *
     */
    @Hr_departmentCreate_uid_textDefault(info = "默认规则")
    private String create_uid_text;

    @JsonIgnore
    private boolean create_uid_textDirtyFlag;

    /**
     * 属性 [PARENT_ID_TEXT]
     *
     */
    @Hr_departmentParent_id_textDefault(info = "默认规则")
    private String parent_id_text;

    @JsonIgnore
    private boolean parent_id_textDirtyFlag;

    /**
     * 属性 [COMPANY_ID_TEXT]
     *
     */
    @Hr_departmentCompany_id_textDefault(info = "默认规则")
    private String company_id_text;

    @JsonIgnore
    private boolean company_id_textDirtyFlag;

    /**
     * 属性 [MANAGER_ID]
     *
     */
    @Hr_departmentManager_idDefault(info = "默认规则")
    private Integer manager_id;

    @JsonIgnore
    private boolean manager_idDirtyFlag;

    /**
     * 属性 [CREATE_UID]
     *
     */
    @Hr_departmentCreate_uidDefault(info = "默认规则")
    private Integer create_uid;

    @JsonIgnore
    private boolean create_uidDirtyFlag;

    /**
     * 属性 [PARENT_ID]
     *
     */
    @Hr_departmentParent_idDefault(info = "默认规则")
    private Integer parent_id;

    @JsonIgnore
    private boolean parent_idDirtyFlag;

    /**
     * 属性 [WRITE_UID]
     *
     */
    @Hr_departmentWrite_uidDefault(info = "默认规则")
    private Integer write_uid;

    @JsonIgnore
    private boolean write_uidDirtyFlag;

    /**
     * 属性 [COMPANY_ID]
     *
     */
    @Hr_departmentCompany_idDefault(info = "默认规则")
    private Integer company_id;

    @JsonIgnore
    private boolean company_idDirtyFlag;


    /**
     * 获取 [MESSAGE_NEEDACTION]
     */
    @JsonProperty("message_needaction")
    public String getMessage_needaction(){
        return message_needaction ;
    }

    /**
     * 设置 [MESSAGE_NEEDACTION]
     */
    @JsonProperty("message_needaction")
    public void setMessage_needaction(String  message_needaction){
        this.message_needaction = message_needaction ;
        this.message_needactionDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_NEEDACTION]脏标记
     */
    @JsonIgnore
    public boolean getMessage_needactionDirtyFlag(){
        return message_needactionDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_FOLLOWER_IDS]
     */
    @JsonProperty("message_follower_ids")
    public String getMessage_follower_ids(){
        return message_follower_ids ;
    }

    /**
     * 设置 [MESSAGE_FOLLOWER_IDS]
     */
    @JsonProperty("message_follower_ids")
    public void setMessage_follower_ids(String  message_follower_ids){
        this.message_follower_ids = message_follower_ids ;
        this.message_follower_idsDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_FOLLOWER_IDS]脏标记
     */
    @JsonIgnore
    public boolean getMessage_follower_idsDirtyFlag(){
        return message_follower_idsDirtyFlag ;
    }

    /**
     * 获取 [ABSENCE_OF_TODAY]
     */
    @JsonProperty("absence_of_today")
    public Integer getAbsence_of_today(){
        return absence_of_today ;
    }

    /**
     * 设置 [ABSENCE_OF_TODAY]
     */
    @JsonProperty("absence_of_today")
    public void setAbsence_of_today(Integer  absence_of_today){
        this.absence_of_today = absence_of_today ;
        this.absence_of_todayDirtyFlag = true ;
    }

    /**
     * 获取 [ABSENCE_OF_TODAY]脏标记
     */
    @JsonIgnore
    public boolean getAbsence_of_todayDirtyFlag(){
        return absence_of_todayDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_UNREAD]
     */
    @JsonProperty("message_unread")
    public String getMessage_unread(){
        return message_unread ;
    }

    /**
     * 设置 [MESSAGE_UNREAD]
     */
    @JsonProperty("message_unread")
    public void setMessage_unread(String  message_unread){
        this.message_unread = message_unread ;
        this.message_unreadDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_UNREAD]脏标记
     */
    @JsonIgnore
    public boolean getMessage_unreadDirtyFlag(){
        return message_unreadDirtyFlag ;
    }

    /**
     * 获取 [LEAVE_TO_APPROVE_COUNT]
     */
    @JsonProperty("leave_to_approve_count")
    public Integer getLeave_to_approve_count(){
        return leave_to_approve_count ;
    }

    /**
     * 设置 [LEAVE_TO_APPROVE_COUNT]
     */
    @JsonProperty("leave_to_approve_count")
    public void setLeave_to_approve_count(Integer  leave_to_approve_count){
        this.leave_to_approve_count = leave_to_approve_count ;
        this.leave_to_approve_countDirtyFlag = true ;
    }

    /**
     * 获取 [LEAVE_TO_APPROVE_COUNT]脏标记
     */
    @JsonIgnore
    public boolean getLeave_to_approve_countDirtyFlag(){
        return leave_to_approve_countDirtyFlag ;
    }

    /**
     * 获取 [COLOR]
     */
    @JsonProperty("color")
    public Integer getColor(){
        return color ;
    }

    /**
     * 设置 [COLOR]
     */
    @JsonProperty("color")
    public void setColor(Integer  color){
        this.color = color ;
        this.colorDirtyFlag = true ;
    }

    /**
     * 获取 [COLOR]脏标记
     */
    @JsonIgnore
    public boolean getColorDirtyFlag(){
        return colorDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_PARTNER_IDS]
     */
    @JsonProperty("message_partner_ids")
    public String getMessage_partner_ids(){
        return message_partner_ids ;
    }

    /**
     * 设置 [MESSAGE_PARTNER_IDS]
     */
    @JsonProperty("message_partner_ids")
    public void setMessage_partner_ids(String  message_partner_ids){
        this.message_partner_ids = message_partner_ids ;
        this.message_partner_idsDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_PARTNER_IDS]脏标记
     */
    @JsonIgnore
    public boolean getMessage_partner_idsDirtyFlag(){
        return message_partner_idsDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_MAIN_ATTACHMENT_ID]
     */
    @JsonProperty("message_main_attachment_id")
    public Integer getMessage_main_attachment_id(){
        return message_main_attachment_id ;
    }

    /**
     * 设置 [MESSAGE_MAIN_ATTACHMENT_ID]
     */
    @JsonProperty("message_main_attachment_id")
    public void setMessage_main_attachment_id(Integer  message_main_attachment_id){
        this.message_main_attachment_id = message_main_attachment_id ;
        this.message_main_attachment_idDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_MAIN_ATTACHMENT_ID]脏标记
     */
    @JsonIgnore
    public boolean getMessage_main_attachment_idDirtyFlag(){
        return message_main_attachment_idDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_UNREAD_COUNTER]
     */
    @JsonProperty("message_unread_counter")
    public Integer getMessage_unread_counter(){
        return message_unread_counter ;
    }

    /**
     * 设置 [MESSAGE_UNREAD_COUNTER]
     */
    @JsonProperty("message_unread_counter")
    public void setMessage_unread_counter(Integer  message_unread_counter){
        this.message_unread_counter = message_unread_counter ;
        this.message_unread_counterDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_UNREAD_COUNTER]脏标记
     */
    @JsonIgnore
    public boolean getMessage_unread_counterDirtyFlag(){
        return message_unread_counterDirtyFlag ;
    }

    /**
     * 获取 [NAME]
     */
    @JsonProperty("name")
    public String getName(){
        return name ;
    }

    /**
     * 设置 [NAME]
     */
    @JsonProperty("name")
    public void setName(String  name){
        this.name = name ;
        this.nameDirtyFlag = true ;
    }

    /**
     * 获取 [NAME]脏标记
     */
    @JsonIgnore
    public boolean getNameDirtyFlag(){
        return nameDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_IS_FOLLOWER]
     */
    @JsonProperty("message_is_follower")
    public String getMessage_is_follower(){
        return message_is_follower ;
    }

    /**
     * 设置 [MESSAGE_IS_FOLLOWER]
     */
    @JsonProperty("message_is_follower")
    public void setMessage_is_follower(String  message_is_follower){
        this.message_is_follower = message_is_follower ;
        this.message_is_followerDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_IS_FOLLOWER]脏标记
     */
    @JsonIgnore
    public boolean getMessage_is_followerDirtyFlag(){
        return message_is_followerDirtyFlag ;
    }

    /**
     * 获取 [ACTIVE]
     */
    @JsonProperty("active")
    public String getActive(){
        return active ;
    }

    /**
     * 设置 [ACTIVE]
     */
    @JsonProperty("active")
    public void setActive(String  active){
        this.active = active ;
        this.activeDirtyFlag = true ;
    }

    /**
     * 获取 [ACTIVE]脏标记
     */
    @JsonIgnore
    public boolean getActiveDirtyFlag(){
        return activeDirtyFlag ;
    }

    /**
     * 获取 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return write_date ;
    }

    /**
     * 设置 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return write_dateDirtyFlag ;
    }

    /**
     * 获取 [EXPENSE_SHEETS_TO_APPROVE_COUNT]
     */
    @JsonProperty("expense_sheets_to_approve_count")
    public Integer getExpense_sheets_to_approve_count(){
        return expense_sheets_to_approve_count ;
    }

    /**
     * 设置 [EXPENSE_SHEETS_TO_APPROVE_COUNT]
     */
    @JsonProperty("expense_sheets_to_approve_count")
    public void setExpense_sheets_to_approve_count(Integer  expense_sheets_to_approve_count){
        this.expense_sheets_to_approve_count = expense_sheets_to_approve_count ;
        this.expense_sheets_to_approve_countDirtyFlag = true ;
    }

    /**
     * 获取 [EXPENSE_SHEETS_TO_APPROVE_COUNT]脏标记
     */
    @JsonIgnore
    public boolean getExpense_sheets_to_approve_countDirtyFlag(){
        return expense_sheets_to_approve_countDirtyFlag ;
    }

    /**
     * 获取 [ALLOCATION_TO_APPROVE_COUNT]
     */
    @JsonProperty("allocation_to_approve_count")
    public Integer getAllocation_to_approve_count(){
        return allocation_to_approve_count ;
    }

    /**
     * 设置 [ALLOCATION_TO_APPROVE_COUNT]
     */
    @JsonProperty("allocation_to_approve_count")
    public void setAllocation_to_approve_count(Integer  allocation_to_approve_count){
        this.allocation_to_approve_count = allocation_to_approve_count ;
        this.allocation_to_approve_countDirtyFlag = true ;
    }

    /**
     * 获取 [ALLOCATION_TO_APPROVE_COUNT]脏标记
     */
    @JsonIgnore
    public boolean getAllocation_to_approve_countDirtyFlag(){
        return allocation_to_approve_countDirtyFlag ;
    }

    /**
     * 获取 [NEW_HIRED_EMPLOYEE]
     */
    @JsonProperty("new_hired_employee")
    public Integer getNew_hired_employee(){
        return new_hired_employee ;
    }

    /**
     * 设置 [NEW_HIRED_EMPLOYEE]
     */
    @JsonProperty("new_hired_employee")
    public void setNew_hired_employee(Integer  new_hired_employee){
        this.new_hired_employee = new_hired_employee ;
        this.new_hired_employeeDirtyFlag = true ;
    }

    /**
     * 获取 [NEW_HIRED_EMPLOYEE]脏标记
     */
    @JsonIgnore
    public boolean getNew_hired_employeeDirtyFlag(){
        return new_hired_employeeDirtyFlag ;
    }

    /**
     * 获取 [NOTE]
     */
    @JsonProperty("note")
    public String getNote(){
        return note ;
    }

    /**
     * 设置 [NOTE]
     */
    @JsonProperty("note")
    public void setNote(String  note){
        this.note = note ;
        this.noteDirtyFlag = true ;
    }

    /**
     * 获取 [NOTE]脏标记
     */
    @JsonIgnore
    public boolean getNoteDirtyFlag(){
        return noteDirtyFlag ;
    }

    /**
     * 获取 [CHILD_IDS]
     */
    @JsonProperty("child_ids")
    public String getChild_ids(){
        return child_ids ;
    }

    /**
     * 设置 [CHILD_IDS]
     */
    @JsonProperty("child_ids")
    public void setChild_ids(String  child_ids){
        this.child_ids = child_ids ;
        this.child_idsDirtyFlag = true ;
    }

    /**
     * 获取 [CHILD_IDS]脏标记
     */
    @JsonIgnore
    public boolean getChild_idsDirtyFlag(){
        return child_idsDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_CHANNEL_IDS]
     */
    @JsonProperty("message_channel_ids")
    public String getMessage_channel_ids(){
        return message_channel_ids ;
    }

    /**
     * 设置 [MESSAGE_CHANNEL_IDS]
     */
    @JsonProperty("message_channel_ids")
    public void setMessage_channel_ids(String  message_channel_ids){
        this.message_channel_ids = message_channel_ids ;
        this.message_channel_idsDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_CHANNEL_IDS]脏标记
     */
    @JsonIgnore
    public boolean getMessage_channel_idsDirtyFlag(){
        return message_channel_idsDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_HAS_ERROR_COUNTER]
     */
    @JsonProperty("message_has_error_counter")
    public Integer getMessage_has_error_counter(){
        return message_has_error_counter ;
    }

    /**
     * 设置 [MESSAGE_HAS_ERROR_COUNTER]
     */
    @JsonProperty("message_has_error_counter")
    public void setMessage_has_error_counter(Integer  message_has_error_counter){
        this.message_has_error_counter = message_has_error_counter ;
        this.message_has_error_counterDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_HAS_ERROR_COUNTER]脏标记
     */
    @JsonIgnore
    public boolean getMessage_has_error_counterDirtyFlag(){
        return message_has_error_counterDirtyFlag ;
    }

    /**
     * 获取 [EXPECTED_EMPLOYEE]
     */
    @JsonProperty("expected_employee")
    public Integer getExpected_employee(){
        return expected_employee ;
    }

    /**
     * 设置 [EXPECTED_EMPLOYEE]
     */
    @JsonProperty("expected_employee")
    public void setExpected_employee(Integer  expected_employee){
        this.expected_employee = expected_employee ;
        this.expected_employeeDirtyFlag = true ;
    }

    /**
     * 获取 [EXPECTED_EMPLOYEE]脏标记
     */
    @JsonIgnore
    public boolean getExpected_employeeDirtyFlag(){
        return expected_employeeDirtyFlag ;
    }

    /**
     * 获取 [WEBSITE_MESSAGE_IDS]
     */
    @JsonProperty("website_message_ids")
    public String getWebsite_message_ids(){
        return website_message_ids ;
    }

    /**
     * 设置 [WEBSITE_MESSAGE_IDS]
     */
    @JsonProperty("website_message_ids")
    public void setWebsite_message_ids(String  website_message_ids){
        this.website_message_ids = website_message_ids ;
        this.website_message_idsDirtyFlag = true ;
    }

    /**
     * 获取 [WEBSITE_MESSAGE_IDS]脏标记
     */
    @JsonIgnore
    public boolean getWebsite_message_idsDirtyFlag(){
        return website_message_idsDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_HAS_ERROR]
     */
    @JsonProperty("message_has_error")
    public String getMessage_has_error(){
        return message_has_error ;
    }

    /**
     * 设置 [MESSAGE_HAS_ERROR]
     */
    @JsonProperty("message_has_error")
    public void setMessage_has_error(String  message_has_error){
        this.message_has_error = message_has_error ;
        this.message_has_errorDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_HAS_ERROR]脏标记
     */
    @JsonIgnore
    public boolean getMessage_has_errorDirtyFlag(){
        return message_has_errorDirtyFlag ;
    }

    /**
     * 获取 [TOTAL_EMPLOYEE]
     */
    @JsonProperty("total_employee")
    public Integer getTotal_employee(){
        return total_employee ;
    }

    /**
     * 设置 [TOTAL_EMPLOYEE]
     */
    @JsonProperty("total_employee")
    public void setTotal_employee(Integer  total_employee){
        this.total_employee = total_employee ;
        this.total_employeeDirtyFlag = true ;
    }

    /**
     * 获取 [TOTAL_EMPLOYEE]脏标记
     */
    @JsonIgnore
    public boolean getTotal_employeeDirtyFlag(){
        return total_employeeDirtyFlag ;
    }

    /**
     * 获取 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return __last_update ;
    }

    /**
     * 设置 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

    /**
     * 获取 [__LAST_UPDATE]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return __last_updateDirtyFlag ;
    }

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

    /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return idDirtyFlag ;
    }

    /**
     * 获取 [JOBS_IDS]
     */
    @JsonProperty("jobs_ids")
    public String getJobs_ids(){
        return jobs_ids ;
    }

    /**
     * 设置 [JOBS_IDS]
     */
    @JsonProperty("jobs_ids")
    public void setJobs_ids(String  jobs_ids){
        this.jobs_ids = jobs_ids ;
        this.jobs_idsDirtyFlag = true ;
    }

    /**
     * 获取 [JOBS_IDS]脏标记
     */
    @JsonIgnore
    public boolean getJobs_idsDirtyFlag(){
        return jobs_idsDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_IDS]
     */
    @JsonProperty("message_ids")
    public String getMessage_ids(){
        return message_ids ;
    }

    /**
     * 设置 [MESSAGE_IDS]
     */
    @JsonProperty("message_ids")
    public void setMessage_ids(String  message_ids){
        this.message_ids = message_ids ;
        this.message_idsDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_IDS]脏标记
     */
    @JsonIgnore
    public boolean getMessage_idsDirtyFlag(){
        return message_idsDirtyFlag ;
    }

    /**
     * 获取 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return display_name ;
    }

    /**
     * 设置 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

    /**
     * 获取 [DISPLAY_NAME]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return display_nameDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_ATTACHMENT_COUNT]
     */
    @JsonProperty("message_attachment_count")
    public Integer getMessage_attachment_count(){
        return message_attachment_count ;
    }

    /**
     * 设置 [MESSAGE_ATTACHMENT_COUNT]
     */
    @JsonProperty("message_attachment_count")
    public void setMessage_attachment_count(Integer  message_attachment_count){
        this.message_attachment_count = message_attachment_count ;
        this.message_attachment_countDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_ATTACHMENT_COUNT]脏标记
     */
    @JsonIgnore
    public boolean getMessage_attachment_countDirtyFlag(){
        return message_attachment_countDirtyFlag ;
    }

    /**
     * 获取 [NEW_APPLICANT_COUNT]
     */
    @JsonProperty("new_applicant_count")
    public Integer getNew_applicant_count(){
        return new_applicant_count ;
    }

    /**
     * 设置 [NEW_APPLICANT_COUNT]
     */
    @JsonProperty("new_applicant_count")
    public void setNew_applicant_count(Integer  new_applicant_count){
        this.new_applicant_count = new_applicant_count ;
        this.new_applicant_countDirtyFlag = true ;
    }

    /**
     * 获取 [NEW_APPLICANT_COUNT]脏标记
     */
    @JsonIgnore
    public boolean getNew_applicant_countDirtyFlag(){
        return new_applicant_countDirtyFlag ;
    }

    /**
     * 获取 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return create_date ;
    }

    /**
     * 设置 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return create_dateDirtyFlag ;
    }

    /**
     * 获取 [COMPLETE_NAME]
     */
    @JsonProperty("complete_name")
    public String getComplete_name(){
        return complete_name ;
    }

    /**
     * 设置 [COMPLETE_NAME]
     */
    @JsonProperty("complete_name")
    public void setComplete_name(String  complete_name){
        this.complete_name = complete_name ;
        this.complete_nameDirtyFlag = true ;
    }

    /**
     * 获取 [COMPLETE_NAME]脏标记
     */
    @JsonIgnore
    public boolean getComplete_nameDirtyFlag(){
        return complete_nameDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_NEEDACTION_COUNTER]
     */
    @JsonProperty("message_needaction_counter")
    public Integer getMessage_needaction_counter(){
        return message_needaction_counter ;
    }

    /**
     * 设置 [MESSAGE_NEEDACTION_COUNTER]
     */
    @JsonProperty("message_needaction_counter")
    public void setMessage_needaction_counter(Integer  message_needaction_counter){
        this.message_needaction_counter = message_needaction_counter ;
        this.message_needaction_counterDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_NEEDACTION_COUNTER]脏标记
     */
    @JsonIgnore
    public boolean getMessage_needaction_counterDirtyFlag(){
        return message_needaction_counterDirtyFlag ;
    }

    /**
     * 获取 [MEMBER_IDS]
     */
    @JsonProperty("member_ids")
    public String getMember_ids(){
        return member_ids ;
    }

    /**
     * 设置 [MEMBER_IDS]
     */
    @JsonProperty("member_ids")
    public void setMember_ids(String  member_ids){
        this.member_ids = member_ids ;
        this.member_idsDirtyFlag = true ;
    }

    /**
     * 获取 [MEMBER_IDS]脏标记
     */
    @JsonIgnore
    public boolean getMember_idsDirtyFlag(){
        return member_idsDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return write_uid_text ;
    }

    /**
     * 设置 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return write_uid_textDirtyFlag ;
    }

    /**
     * 获取 [MANAGER_ID_TEXT]
     */
    @JsonProperty("manager_id_text")
    public String getManager_id_text(){
        return manager_id_text ;
    }

    /**
     * 设置 [MANAGER_ID_TEXT]
     */
    @JsonProperty("manager_id_text")
    public void setManager_id_text(String  manager_id_text){
        this.manager_id_text = manager_id_text ;
        this.manager_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [MANAGER_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getManager_id_textDirtyFlag(){
        return manager_id_textDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return create_uid_text ;
    }

    /**
     * 设置 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return create_uid_textDirtyFlag ;
    }

    /**
     * 获取 [PARENT_ID_TEXT]
     */
    @JsonProperty("parent_id_text")
    public String getParent_id_text(){
        return parent_id_text ;
    }

    /**
     * 设置 [PARENT_ID_TEXT]
     */
    @JsonProperty("parent_id_text")
    public void setParent_id_text(String  parent_id_text){
        this.parent_id_text = parent_id_text ;
        this.parent_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [PARENT_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getParent_id_textDirtyFlag(){
        return parent_id_textDirtyFlag ;
    }

    /**
     * 获取 [COMPANY_ID_TEXT]
     */
    @JsonProperty("company_id_text")
    public String getCompany_id_text(){
        return company_id_text ;
    }

    /**
     * 设置 [COMPANY_ID_TEXT]
     */
    @JsonProperty("company_id_text")
    public void setCompany_id_text(String  company_id_text){
        this.company_id_text = company_id_text ;
        this.company_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [COMPANY_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getCompany_id_textDirtyFlag(){
        return company_id_textDirtyFlag ;
    }

    /**
     * 获取 [MANAGER_ID]
     */
    @JsonProperty("manager_id")
    public Integer getManager_id(){
        return manager_id ;
    }

    /**
     * 设置 [MANAGER_ID]
     */
    @JsonProperty("manager_id")
    public void setManager_id(Integer  manager_id){
        this.manager_id = manager_id ;
        this.manager_idDirtyFlag = true ;
    }

    /**
     * 获取 [MANAGER_ID]脏标记
     */
    @JsonIgnore
    public boolean getManager_idDirtyFlag(){
        return manager_idDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return create_uid ;
    }

    /**
     * 设置 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return create_uidDirtyFlag ;
    }

    /**
     * 获取 [PARENT_ID]
     */
    @JsonProperty("parent_id")
    public Integer getParent_id(){
        return parent_id ;
    }

    /**
     * 设置 [PARENT_ID]
     */
    @JsonProperty("parent_id")
    public void setParent_id(Integer  parent_id){
        this.parent_id = parent_id ;
        this.parent_idDirtyFlag = true ;
    }

    /**
     * 获取 [PARENT_ID]脏标记
     */
    @JsonIgnore
    public boolean getParent_idDirtyFlag(){
        return parent_idDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return write_uid ;
    }

    /**
     * 设置 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return write_uidDirtyFlag ;
    }

    /**
     * 获取 [COMPANY_ID]
     */
    @JsonProperty("company_id")
    public Integer getCompany_id(){
        return company_id ;
    }

    /**
     * 设置 [COMPANY_ID]
     */
    @JsonProperty("company_id")
    public void setCompany_id(Integer  company_id){
        this.company_id = company_id ;
        this.company_idDirtyFlag = true ;
    }

    /**
     * 获取 [COMPANY_ID]脏标记
     */
    @JsonIgnore
    public boolean getCompany_idDirtyFlag(){
        return company_idDirtyFlag ;
    }



    public Hr_department toDO() {
        Hr_department srfdomain = new Hr_department();
        if(getMessage_needactionDirtyFlag())
            srfdomain.setMessage_needaction(message_needaction);
        if(getMessage_follower_idsDirtyFlag())
            srfdomain.setMessage_follower_ids(message_follower_ids);
        if(getAbsence_of_todayDirtyFlag())
            srfdomain.setAbsence_of_today(absence_of_today);
        if(getMessage_unreadDirtyFlag())
            srfdomain.setMessage_unread(message_unread);
        if(getLeave_to_approve_countDirtyFlag())
            srfdomain.setLeave_to_approve_count(leave_to_approve_count);
        if(getColorDirtyFlag())
            srfdomain.setColor(color);
        if(getMessage_partner_idsDirtyFlag())
            srfdomain.setMessage_partner_ids(message_partner_ids);
        if(getMessage_main_attachment_idDirtyFlag())
            srfdomain.setMessage_main_attachment_id(message_main_attachment_id);
        if(getMessage_unread_counterDirtyFlag())
            srfdomain.setMessage_unread_counter(message_unread_counter);
        if(getNameDirtyFlag())
            srfdomain.setName(name);
        if(getMessage_is_followerDirtyFlag())
            srfdomain.setMessage_is_follower(message_is_follower);
        if(getActiveDirtyFlag())
            srfdomain.setActive(active);
        if(getWrite_dateDirtyFlag())
            srfdomain.setWrite_date(write_date);
        if(getExpense_sheets_to_approve_countDirtyFlag())
            srfdomain.setExpense_sheets_to_approve_count(expense_sheets_to_approve_count);
        if(getAllocation_to_approve_countDirtyFlag())
            srfdomain.setAllocation_to_approve_count(allocation_to_approve_count);
        if(getNew_hired_employeeDirtyFlag())
            srfdomain.setNew_hired_employee(new_hired_employee);
        if(getNoteDirtyFlag())
            srfdomain.setNote(note);
        if(getChild_idsDirtyFlag())
            srfdomain.setChild_ids(child_ids);
        if(getMessage_channel_idsDirtyFlag())
            srfdomain.setMessage_channel_ids(message_channel_ids);
        if(getMessage_has_error_counterDirtyFlag())
            srfdomain.setMessage_has_error_counter(message_has_error_counter);
        if(getExpected_employeeDirtyFlag())
            srfdomain.setExpected_employee(expected_employee);
        if(getWebsite_message_idsDirtyFlag())
            srfdomain.setWebsite_message_ids(website_message_ids);
        if(getMessage_has_errorDirtyFlag())
            srfdomain.setMessage_has_error(message_has_error);
        if(getTotal_employeeDirtyFlag())
            srfdomain.setTotal_employee(total_employee);
        if(get__last_updateDirtyFlag())
            srfdomain.set__last_update(__last_update);
        if(getIdDirtyFlag())
            srfdomain.setId(id);
        if(getJobs_idsDirtyFlag())
            srfdomain.setJobs_ids(jobs_ids);
        if(getMessage_idsDirtyFlag())
            srfdomain.setMessage_ids(message_ids);
        if(getDisplay_nameDirtyFlag())
            srfdomain.setDisplay_name(display_name);
        if(getMessage_attachment_countDirtyFlag())
            srfdomain.setMessage_attachment_count(message_attachment_count);
        if(getNew_applicant_countDirtyFlag())
            srfdomain.setNew_applicant_count(new_applicant_count);
        if(getCreate_dateDirtyFlag())
            srfdomain.setCreate_date(create_date);
        if(getComplete_nameDirtyFlag())
            srfdomain.setComplete_name(complete_name);
        if(getMessage_needaction_counterDirtyFlag())
            srfdomain.setMessage_needaction_counter(message_needaction_counter);
        if(getMember_idsDirtyFlag())
            srfdomain.setMember_ids(member_ids);
        if(getWrite_uid_textDirtyFlag())
            srfdomain.setWrite_uid_text(write_uid_text);
        if(getManager_id_textDirtyFlag())
            srfdomain.setManager_id_text(manager_id_text);
        if(getCreate_uid_textDirtyFlag())
            srfdomain.setCreate_uid_text(create_uid_text);
        if(getParent_id_textDirtyFlag())
            srfdomain.setParent_id_text(parent_id_text);
        if(getCompany_id_textDirtyFlag())
            srfdomain.setCompany_id_text(company_id_text);
        if(getManager_idDirtyFlag())
            srfdomain.setManager_id(manager_id);
        if(getCreate_uidDirtyFlag())
            srfdomain.setCreate_uid(create_uid);
        if(getParent_idDirtyFlag())
            srfdomain.setParent_id(parent_id);
        if(getWrite_uidDirtyFlag())
            srfdomain.setWrite_uid(write_uid);
        if(getCompany_idDirtyFlag())
            srfdomain.setCompany_id(company_id);

        return srfdomain;
    }

    public void fromDO(Hr_department srfdomain) {
        if(srfdomain == null )
          return ;
        if(srfdomain.getMessage_needactionDirtyFlag())
            this.setMessage_needaction(srfdomain.getMessage_needaction());
        if(srfdomain.getMessage_follower_idsDirtyFlag())
            this.setMessage_follower_ids(srfdomain.getMessage_follower_ids());
        if(srfdomain.getAbsence_of_todayDirtyFlag())
            this.setAbsence_of_today(srfdomain.getAbsence_of_today());
        if(srfdomain.getMessage_unreadDirtyFlag())
            this.setMessage_unread(srfdomain.getMessage_unread());
        if(srfdomain.getLeave_to_approve_countDirtyFlag())
            this.setLeave_to_approve_count(srfdomain.getLeave_to_approve_count());
        if(srfdomain.getColorDirtyFlag())
            this.setColor(srfdomain.getColor());
        if(srfdomain.getMessage_partner_idsDirtyFlag())
            this.setMessage_partner_ids(srfdomain.getMessage_partner_ids());
        if(srfdomain.getMessage_main_attachment_idDirtyFlag())
            this.setMessage_main_attachment_id(srfdomain.getMessage_main_attachment_id());
        if(srfdomain.getMessage_unread_counterDirtyFlag())
            this.setMessage_unread_counter(srfdomain.getMessage_unread_counter());
        if(srfdomain.getNameDirtyFlag())
            this.setName(srfdomain.getName());
        if(srfdomain.getMessage_is_followerDirtyFlag())
            this.setMessage_is_follower(srfdomain.getMessage_is_follower());
        if(srfdomain.getActiveDirtyFlag())
            this.setActive(srfdomain.getActive());
        if(srfdomain.getWrite_dateDirtyFlag())
            this.setWrite_date(srfdomain.getWrite_date());
        if(srfdomain.getExpense_sheets_to_approve_countDirtyFlag())
            this.setExpense_sheets_to_approve_count(srfdomain.getExpense_sheets_to_approve_count());
        if(srfdomain.getAllocation_to_approve_countDirtyFlag())
            this.setAllocation_to_approve_count(srfdomain.getAllocation_to_approve_count());
        if(srfdomain.getNew_hired_employeeDirtyFlag())
            this.setNew_hired_employee(srfdomain.getNew_hired_employee());
        if(srfdomain.getNoteDirtyFlag())
            this.setNote(srfdomain.getNote());
        if(srfdomain.getChild_idsDirtyFlag())
            this.setChild_ids(srfdomain.getChild_ids());
        if(srfdomain.getMessage_channel_idsDirtyFlag())
            this.setMessage_channel_ids(srfdomain.getMessage_channel_ids());
        if(srfdomain.getMessage_has_error_counterDirtyFlag())
            this.setMessage_has_error_counter(srfdomain.getMessage_has_error_counter());
        if(srfdomain.getExpected_employeeDirtyFlag())
            this.setExpected_employee(srfdomain.getExpected_employee());
        if(srfdomain.getWebsite_message_idsDirtyFlag())
            this.setWebsite_message_ids(srfdomain.getWebsite_message_ids());
        if(srfdomain.getMessage_has_errorDirtyFlag())
            this.setMessage_has_error(srfdomain.getMessage_has_error());
        if(srfdomain.getTotal_employeeDirtyFlag())
            this.setTotal_employee(srfdomain.getTotal_employee());
        if(srfdomain.get__last_updateDirtyFlag())
            this.set__last_update(srfdomain.get__last_update());
        if(srfdomain.getIdDirtyFlag())
            this.setId(srfdomain.getId());
        if(srfdomain.getJobs_idsDirtyFlag())
            this.setJobs_ids(srfdomain.getJobs_ids());
        if(srfdomain.getMessage_idsDirtyFlag())
            this.setMessage_ids(srfdomain.getMessage_ids());
        if(srfdomain.getDisplay_nameDirtyFlag())
            this.setDisplay_name(srfdomain.getDisplay_name());
        if(srfdomain.getMessage_attachment_countDirtyFlag())
            this.setMessage_attachment_count(srfdomain.getMessage_attachment_count());
        if(srfdomain.getNew_applicant_countDirtyFlag())
            this.setNew_applicant_count(srfdomain.getNew_applicant_count());
        if(srfdomain.getCreate_dateDirtyFlag())
            this.setCreate_date(srfdomain.getCreate_date());
        if(srfdomain.getComplete_nameDirtyFlag())
            this.setComplete_name(srfdomain.getComplete_name());
        if(srfdomain.getMessage_needaction_counterDirtyFlag())
            this.setMessage_needaction_counter(srfdomain.getMessage_needaction_counter());
        if(srfdomain.getMember_idsDirtyFlag())
            this.setMember_ids(srfdomain.getMember_ids());
        if(srfdomain.getWrite_uid_textDirtyFlag())
            this.setWrite_uid_text(srfdomain.getWrite_uid_text());
        if(srfdomain.getManager_id_textDirtyFlag())
            this.setManager_id_text(srfdomain.getManager_id_text());
        if(srfdomain.getCreate_uid_textDirtyFlag())
            this.setCreate_uid_text(srfdomain.getCreate_uid_text());
        if(srfdomain.getParent_id_textDirtyFlag())
            this.setParent_id_text(srfdomain.getParent_id_text());
        if(srfdomain.getCompany_id_textDirtyFlag())
            this.setCompany_id_text(srfdomain.getCompany_id_text());
        if(srfdomain.getManager_idDirtyFlag())
            this.setManager_id(srfdomain.getManager_id());
        if(srfdomain.getCreate_uidDirtyFlag())
            this.setCreate_uid(srfdomain.getCreate_uid());
        if(srfdomain.getParent_idDirtyFlag())
            this.setParent_id(srfdomain.getParent_id());
        if(srfdomain.getWrite_uidDirtyFlag())
            this.setWrite_uid(srfdomain.getWrite_uid());
        if(srfdomain.getCompany_idDirtyFlag())
            this.setCompany_id(srfdomain.getCompany_id());

    }

    public List<Hr_departmentDTO> fromDOPage(List<Hr_department> poPage)   {
        if(poPage == null)
            return null;
        List<Hr_departmentDTO> dtos=new ArrayList<Hr_departmentDTO>();
        for(Hr_department domain : poPage) {
            Hr_departmentDTO dto = new Hr_departmentDTO();
            dto.fromDO(domain);
            dtos.add(dto);
        }
        return dtos;
    }
}

