package cn.ibizlab.odoo.service.odoo_hr.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.core.odoo_hr.valuerule.anno.hr_job.*;
import cn.ibizlab.odoo.core.odoo_hr.domain.Hr_job;
import org.springframework.cglib.beans.BeanCopier;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 服务DTO对象[Hr_jobDTO]
 */
public class Hr_jobDTO implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [MESSAGE_IS_FOLLOWER]
     *
     */
    @Hr_jobMessage_is_followerDefault(info = "默认规则")
    private String message_is_follower;

    @JsonIgnore
    private boolean message_is_followerDirtyFlag;

    /**
     * 属性 [NO_OF_EMPLOYEE]
     *
     */
    @Hr_jobNo_of_employeeDefault(info = "默认规则")
    private Integer no_of_employee;

    @JsonIgnore
    private boolean no_of_employeeDirtyFlag;

    /**
     * 属性 [STATE]
     *
     */
    @Hr_jobStateDefault(info = "默认规则")
    private String state;

    @JsonIgnore
    private boolean stateDirtyFlag;

    /**
     * 属性 [MESSAGE_PARTNER_IDS]
     *
     */
    @Hr_jobMessage_partner_idsDefault(info = "默认规则")
    private String message_partner_ids;

    @JsonIgnore
    private boolean message_partner_idsDirtyFlag;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @Hr_job__last_updateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp __last_update;

    @JsonIgnore
    private boolean __last_updateDirtyFlag;

    /**
     * 属性 [NO_OF_HIRED_EMPLOYEE]
     *
     */
    @Hr_jobNo_of_hired_employeeDefault(info = "默认规则")
    private Integer no_of_hired_employee;

    @JsonIgnore
    private boolean no_of_hired_employeeDirtyFlag;

    /**
     * 属性 [MESSAGE_UNREAD_COUNTER]
     *
     */
    @Hr_jobMessage_unread_counterDefault(info = "默认规则")
    private Integer message_unread_counter;

    @JsonIgnore
    private boolean message_unread_counterDirtyFlag;

    /**
     * 属性 [WEBSITE_META_DESCRIPTION]
     *
     */
    @Hr_jobWebsite_meta_descriptionDefault(info = "默认规则")
    private String website_meta_description;

    @JsonIgnore
    private boolean website_meta_descriptionDirtyFlag;

    /**
     * 属性 [NO_OF_RECRUITMENT]
     *
     */
    @Hr_jobNo_of_recruitmentDefault(info = "默认规则")
    private Integer no_of_recruitment;

    @JsonIgnore
    private boolean no_of_recruitmentDirtyFlag;

    /**
     * 属性 [WRITE_DATE]
     *
     */
    @Hr_jobWrite_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp write_date;

    @JsonIgnore
    private boolean write_dateDirtyFlag;

    /**
     * 属性 [IS_SEO_OPTIMIZED]
     *
     */
    @Hr_jobIs_seo_optimizedDefault(info = "默认规则")
    private String is_seo_optimized;

    @JsonIgnore
    private boolean is_seo_optimizedDirtyFlag;

    /**
     * 属性 [MESSAGE_CHANNEL_IDS]
     *
     */
    @Hr_jobMessage_channel_idsDefault(info = "默认规则")
    private String message_channel_ids;

    @JsonIgnore
    private boolean message_channel_idsDirtyFlag;

    /**
     * 属性 [MESSAGE_MAIN_ATTACHMENT_ID]
     *
     */
    @Hr_jobMessage_main_attachment_idDefault(info = "默认规则")
    private Integer message_main_attachment_id;

    @JsonIgnore
    private boolean message_main_attachment_idDirtyFlag;

    /**
     * 属性 [NAME]
     *
     */
    @Hr_jobNameDefault(info = "默认规则")
    private String name;

    @JsonIgnore
    private boolean nameDirtyFlag;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @Hr_jobDisplay_nameDefault(info = "默认规则")
    private String display_name;

    @JsonIgnore
    private boolean display_nameDirtyFlag;

    /**
     * 属性 [DESCRIPTION]
     *
     */
    @Hr_jobDescriptionDefault(info = "默认规则")
    private String description;

    @JsonIgnore
    private boolean descriptionDirtyFlag;

    /**
     * 属性 [WEBSITE_URL]
     *
     */
    @Hr_jobWebsite_urlDefault(info = "默认规则")
    private String website_url;

    @JsonIgnore
    private boolean website_urlDirtyFlag;

    /**
     * 属性 [REQUIREMENTS]
     *
     */
    @Hr_jobRequirementsDefault(info = "默认规则")
    private String requirements;

    @JsonIgnore
    private boolean requirementsDirtyFlag;

    /**
     * 属性 [WEBSITE_META_TITLE]
     *
     */
    @Hr_jobWebsite_meta_titleDefault(info = "默认规则")
    private String website_meta_title;

    @JsonIgnore
    private boolean website_meta_titleDirtyFlag;

    /**
     * 属性 [APPLICATION_COUNT]
     *
     */
    @Hr_jobApplication_countDefault(info = "默认规则")
    private Integer application_count;

    @JsonIgnore
    private boolean application_countDirtyFlag;

    /**
     * 属性 [APPLICATION_IDS]
     *
     */
    @Hr_jobApplication_idsDefault(info = "默认规则")
    private String application_ids;

    @JsonIgnore
    private boolean application_idsDirtyFlag;

    /**
     * 属性 [IS_PUBLISHED]
     *
     */
    @Hr_jobIs_publishedDefault(info = "默认规则")
    private String is_published;

    @JsonIgnore
    private boolean is_publishedDirtyFlag;

    /**
     * 属性 [MESSAGE_HAS_ERROR_COUNTER]
     *
     */
    @Hr_jobMessage_has_error_counterDefault(info = "默认规则")
    private Integer message_has_error_counter;

    @JsonIgnore
    private boolean message_has_error_counterDirtyFlag;

    /**
     * 属性 [WEBSITE_META_OG_IMG]
     *
     */
    @Hr_jobWebsite_meta_og_imgDefault(info = "默认规则")
    private String website_meta_og_img;

    @JsonIgnore
    private boolean website_meta_og_imgDirtyFlag;

    /**
     * 属性 [EXPECTED_EMPLOYEES]
     *
     */
    @Hr_jobExpected_employeesDefault(info = "默认规则")
    private Integer expected_employees;

    @JsonIgnore
    private boolean expected_employeesDirtyFlag;

    /**
     * 属性 [CREATE_DATE]
     *
     */
    @Hr_jobCreate_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp create_date;

    @JsonIgnore
    private boolean create_dateDirtyFlag;

    /**
     * 属性 [WEBSITE_PUBLISHED]
     *
     */
    @Hr_jobWebsite_publishedDefault(info = "默认规则")
    private String website_published;

    @JsonIgnore
    private boolean website_publishedDirtyFlag;

    /**
     * 属性 [MESSAGE_IDS]
     *
     */
    @Hr_jobMessage_idsDefault(info = "默认规则")
    private String message_ids;

    @JsonIgnore
    private boolean message_idsDirtyFlag;

    /**
     * 属性 [MESSAGE_ATTACHMENT_COUNT]
     *
     */
    @Hr_jobMessage_attachment_countDefault(info = "默认规则")
    private Integer message_attachment_count;

    @JsonIgnore
    private boolean message_attachment_countDirtyFlag;

    /**
     * 属性 [WEBSITE_DESCRIPTION]
     *
     */
    @Hr_jobWebsite_descriptionDefault(info = "默认规则")
    private String website_description;

    @JsonIgnore
    private boolean website_descriptionDirtyFlag;

    /**
     * 属性 [ID]
     *
     */
    @Hr_jobIdDefault(info = "默认规则")
    private Integer id;

    @JsonIgnore
    private boolean idDirtyFlag;

    /**
     * 属性 [WEBSITE_MESSAGE_IDS]
     *
     */
    @Hr_jobWebsite_message_idsDefault(info = "默认规则")
    private String website_message_ids;

    @JsonIgnore
    private boolean website_message_idsDirtyFlag;

    /**
     * 属性 [COLOR]
     *
     */
    @Hr_jobColorDefault(info = "默认规则")
    private Integer color;

    @JsonIgnore
    private boolean colorDirtyFlag;

    /**
     * 属性 [MESSAGE_FOLLOWER_IDS]
     *
     */
    @Hr_jobMessage_follower_idsDefault(info = "默认规则")
    private String message_follower_ids;

    @JsonIgnore
    private boolean message_follower_idsDirtyFlag;

    /**
     * 属性 [MESSAGE_UNREAD]
     *
     */
    @Hr_jobMessage_unreadDefault(info = "默认规则")
    private String message_unread;

    @JsonIgnore
    private boolean message_unreadDirtyFlag;

    /**
     * 属性 [DOCUMENT_IDS]
     *
     */
    @Hr_jobDocument_idsDefault(info = "默认规则")
    private String document_ids;

    @JsonIgnore
    private boolean document_idsDirtyFlag;

    /**
     * 属性 [MESSAGE_NEEDACTION]
     *
     */
    @Hr_jobMessage_needactionDefault(info = "默认规则")
    private String message_needaction;

    @JsonIgnore
    private boolean message_needactionDirtyFlag;

    /**
     * 属性 [MESSAGE_NEEDACTION_COUNTER]
     *
     */
    @Hr_jobMessage_needaction_counterDefault(info = "默认规则")
    private Integer message_needaction_counter;

    @JsonIgnore
    private boolean message_needaction_counterDirtyFlag;

    /**
     * 属性 [MESSAGE_HAS_ERROR]
     *
     */
    @Hr_jobMessage_has_errorDefault(info = "默认规则")
    private String message_has_error;

    @JsonIgnore
    private boolean message_has_errorDirtyFlag;

    /**
     * 属性 [EMPLOYEE_IDS]
     *
     */
    @Hr_jobEmployee_idsDefault(info = "默认规则")
    private String employee_ids;

    @JsonIgnore
    private boolean employee_idsDirtyFlag;

    /**
     * 属性 [DOCUMENTS_COUNT]
     *
     */
    @Hr_jobDocuments_countDefault(info = "默认规则")
    private Integer documents_count;

    @JsonIgnore
    private boolean documents_countDirtyFlag;

    /**
     * 属性 [WEBSITE_ID]
     *
     */
    @Hr_jobWebsite_idDefault(info = "默认规则")
    private Integer website_id;

    @JsonIgnore
    private boolean website_idDirtyFlag;

    /**
     * 属性 [WEBSITE_META_KEYWORDS]
     *
     */
    @Hr_jobWebsite_meta_keywordsDefault(info = "默认规则")
    private String website_meta_keywords;

    @JsonIgnore
    private boolean website_meta_keywordsDirtyFlag;

    /**
     * 属性 [ALIAS_DEFAULTS]
     *
     */
    @Hr_jobAlias_defaultsDefault(info = "默认规则")
    private String alias_defaults;

    @JsonIgnore
    private boolean alias_defaultsDirtyFlag;

    /**
     * 属性 [ALIAS_NAME]
     *
     */
    @Hr_jobAlias_nameDefault(info = "默认规则")
    private String alias_name;

    @JsonIgnore
    private boolean alias_nameDirtyFlag;

    /**
     * 属性 [ALIAS_FORCE_THREAD_ID]
     *
     */
    @Hr_jobAlias_force_thread_idDefault(info = "默认规则")
    private Integer alias_force_thread_id;

    @JsonIgnore
    private boolean alias_force_thread_idDirtyFlag;

    /**
     * 属性 [ALIAS_DOMAIN]
     *
     */
    @Hr_jobAlias_domainDefault(info = "默认规则")
    private String alias_domain;

    @JsonIgnore
    private boolean alias_domainDirtyFlag;

    /**
     * 属性 [WRITE_UID_TEXT]
     *
     */
    @Hr_jobWrite_uid_textDefault(info = "默认规则")
    private String write_uid_text;

    @JsonIgnore
    private boolean write_uid_textDirtyFlag;

    /**
     * 属性 [ALIAS_PARENT_MODEL_ID]
     *
     */
    @Hr_jobAlias_parent_model_idDefault(info = "默认规则")
    private Integer alias_parent_model_id;

    @JsonIgnore
    private boolean alias_parent_model_idDirtyFlag;

    /**
     * 属性 [CREATE_UID_TEXT]
     *
     */
    @Hr_jobCreate_uid_textDefault(info = "默认规则")
    private String create_uid_text;

    @JsonIgnore
    private boolean create_uid_textDirtyFlag;

    /**
     * 属性 [ALIAS_PARENT_THREAD_ID]
     *
     */
    @Hr_jobAlias_parent_thread_idDefault(info = "默认规则")
    private Integer alias_parent_thread_id;

    @JsonIgnore
    private boolean alias_parent_thread_idDirtyFlag;

    /**
     * 属性 [ALIAS_MODEL_ID]
     *
     */
    @Hr_jobAlias_model_idDefault(info = "默认规则")
    private Integer alias_model_id;

    @JsonIgnore
    private boolean alias_model_idDirtyFlag;

    /**
     * 属性 [ADDRESS_ID_TEXT]
     *
     */
    @Hr_jobAddress_id_textDefault(info = "默认规则")
    private String address_id_text;

    @JsonIgnore
    private boolean address_id_textDirtyFlag;

    /**
     * 属性 [ALIAS_USER_ID]
     *
     */
    @Hr_jobAlias_user_idDefault(info = "默认规则")
    private Integer alias_user_id;

    @JsonIgnore
    private boolean alias_user_idDirtyFlag;

    /**
     * 属性 [DEPARTMENT_ID_TEXT]
     *
     */
    @Hr_jobDepartment_id_textDefault(info = "默认规则")
    private String department_id_text;

    @JsonIgnore
    private boolean department_id_textDirtyFlag;

    /**
     * 属性 [COMPANY_ID_TEXT]
     *
     */
    @Hr_jobCompany_id_textDefault(info = "默认规则")
    private String company_id_text;

    @JsonIgnore
    private boolean company_id_textDirtyFlag;

    /**
     * 属性 [USER_ID_TEXT]
     *
     */
    @Hr_jobUser_id_textDefault(info = "默认规则")
    private String user_id_text;

    @JsonIgnore
    private boolean user_id_textDirtyFlag;

    /**
     * 属性 [MANAGER_ID_TEXT]
     *
     */
    @Hr_jobManager_id_textDefault(info = "默认规则")
    private String manager_id_text;

    @JsonIgnore
    private boolean manager_id_textDirtyFlag;

    /**
     * 属性 [ALIAS_CONTACT]
     *
     */
    @Hr_jobAlias_contactDefault(info = "默认规则")
    private String alias_contact;

    @JsonIgnore
    private boolean alias_contactDirtyFlag;

    /**
     * 属性 [HR_RESPONSIBLE_ID_TEXT]
     *
     */
    @Hr_jobHr_responsible_id_textDefault(info = "默认规则")
    private String hr_responsible_id_text;

    @JsonIgnore
    private boolean hr_responsible_id_textDirtyFlag;

    /**
     * 属性 [COMPANY_ID]
     *
     */
    @Hr_jobCompany_idDefault(info = "默认规则")
    private Integer company_id;

    @JsonIgnore
    private boolean company_idDirtyFlag;

    /**
     * 属性 [WRITE_UID]
     *
     */
    @Hr_jobWrite_uidDefault(info = "默认规则")
    private Integer write_uid;

    @JsonIgnore
    private boolean write_uidDirtyFlag;

    /**
     * 属性 [MANAGER_ID]
     *
     */
    @Hr_jobManager_idDefault(info = "默认规则")
    private Integer manager_id;

    @JsonIgnore
    private boolean manager_idDirtyFlag;

    /**
     * 属性 [ADDRESS_ID]
     *
     */
    @Hr_jobAddress_idDefault(info = "默认规则")
    private Integer address_id;

    @JsonIgnore
    private boolean address_idDirtyFlag;

    /**
     * 属性 [CREATE_UID]
     *
     */
    @Hr_jobCreate_uidDefault(info = "默认规则")
    private Integer create_uid;

    @JsonIgnore
    private boolean create_uidDirtyFlag;

    /**
     * 属性 [HR_RESPONSIBLE_ID]
     *
     */
    @Hr_jobHr_responsible_idDefault(info = "默认规则")
    private Integer hr_responsible_id;

    @JsonIgnore
    private boolean hr_responsible_idDirtyFlag;

    /**
     * 属性 [DEPARTMENT_ID]
     *
     */
    @Hr_jobDepartment_idDefault(info = "默认规则")
    private Integer department_id;

    @JsonIgnore
    private boolean department_idDirtyFlag;

    /**
     * 属性 [ALIAS_ID]
     *
     */
    @Hr_jobAlias_idDefault(info = "默认规则")
    private Integer alias_id;

    @JsonIgnore
    private boolean alias_idDirtyFlag;

    /**
     * 属性 [USER_ID]
     *
     */
    @Hr_jobUser_idDefault(info = "默认规则")
    private Integer user_id;

    @JsonIgnore
    private boolean user_idDirtyFlag;


    /**
     * 获取 [MESSAGE_IS_FOLLOWER]
     */
    @JsonProperty("message_is_follower")
    public String getMessage_is_follower(){
        return message_is_follower ;
    }

    /**
     * 设置 [MESSAGE_IS_FOLLOWER]
     */
    @JsonProperty("message_is_follower")
    public void setMessage_is_follower(String  message_is_follower){
        this.message_is_follower = message_is_follower ;
        this.message_is_followerDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_IS_FOLLOWER]脏标记
     */
    @JsonIgnore
    public boolean getMessage_is_followerDirtyFlag(){
        return message_is_followerDirtyFlag ;
    }

    /**
     * 获取 [NO_OF_EMPLOYEE]
     */
    @JsonProperty("no_of_employee")
    public Integer getNo_of_employee(){
        return no_of_employee ;
    }

    /**
     * 设置 [NO_OF_EMPLOYEE]
     */
    @JsonProperty("no_of_employee")
    public void setNo_of_employee(Integer  no_of_employee){
        this.no_of_employee = no_of_employee ;
        this.no_of_employeeDirtyFlag = true ;
    }

    /**
     * 获取 [NO_OF_EMPLOYEE]脏标记
     */
    @JsonIgnore
    public boolean getNo_of_employeeDirtyFlag(){
        return no_of_employeeDirtyFlag ;
    }

    /**
     * 获取 [STATE]
     */
    @JsonProperty("state")
    public String getState(){
        return state ;
    }

    /**
     * 设置 [STATE]
     */
    @JsonProperty("state")
    public void setState(String  state){
        this.state = state ;
        this.stateDirtyFlag = true ;
    }

    /**
     * 获取 [STATE]脏标记
     */
    @JsonIgnore
    public boolean getStateDirtyFlag(){
        return stateDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_PARTNER_IDS]
     */
    @JsonProperty("message_partner_ids")
    public String getMessage_partner_ids(){
        return message_partner_ids ;
    }

    /**
     * 设置 [MESSAGE_PARTNER_IDS]
     */
    @JsonProperty("message_partner_ids")
    public void setMessage_partner_ids(String  message_partner_ids){
        this.message_partner_ids = message_partner_ids ;
        this.message_partner_idsDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_PARTNER_IDS]脏标记
     */
    @JsonIgnore
    public boolean getMessage_partner_idsDirtyFlag(){
        return message_partner_idsDirtyFlag ;
    }

    /**
     * 获取 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return __last_update ;
    }

    /**
     * 设置 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

    /**
     * 获取 [__LAST_UPDATE]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return __last_updateDirtyFlag ;
    }

    /**
     * 获取 [NO_OF_HIRED_EMPLOYEE]
     */
    @JsonProperty("no_of_hired_employee")
    public Integer getNo_of_hired_employee(){
        return no_of_hired_employee ;
    }

    /**
     * 设置 [NO_OF_HIRED_EMPLOYEE]
     */
    @JsonProperty("no_of_hired_employee")
    public void setNo_of_hired_employee(Integer  no_of_hired_employee){
        this.no_of_hired_employee = no_of_hired_employee ;
        this.no_of_hired_employeeDirtyFlag = true ;
    }

    /**
     * 获取 [NO_OF_HIRED_EMPLOYEE]脏标记
     */
    @JsonIgnore
    public boolean getNo_of_hired_employeeDirtyFlag(){
        return no_of_hired_employeeDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_UNREAD_COUNTER]
     */
    @JsonProperty("message_unread_counter")
    public Integer getMessage_unread_counter(){
        return message_unread_counter ;
    }

    /**
     * 设置 [MESSAGE_UNREAD_COUNTER]
     */
    @JsonProperty("message_unread_counter")
    public void setMessage_unread_counter(Integer  message_unread_counter){
        this.message_unread_counter = message_unread_counter ;
        this.message_unread_counterDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_UNREAD_COUNTER]脏标记
     */
    @JsonIgnore
    public boolean getMessage_unread_counterDirtyFlag(){
        return message_unread_counterDirtyFlag ;
    }

    /**
     * 获取 [WEBSITE_META_DESCRIPTION]
     */
    @JsonProperty("website_meta_description")
    public String getWebsite_meta_description(){
        return website_meta_description ;
    }

    /**
     * 设置 [WEBSITE_META_DESCRIPTION]
     */
    @JsonProperty("website_meta_description")
    public void setWebsite_meta_description(String  website_meta_description){
        this.website_meta_description = website_meta_description ;
        this.website_meta_descriptionDirtyFlag = true ;
    }

    /**
     * 获取 [WEBSITE_META_DESCRIPTION]脏标记
     */
    @JsonIgnore
    public boolean getWebsite_meta_descriptionDirtyFlag(){
        return website_meta_descriptionDirtyFlag ;
    }

    /**
     * 获取 [NO_OF_RECRUITMENT]
     */
    @JsonProperty("no_of_recruitment")
    public Integer getNo_of_recruitment(){
        return no_of_recruitment ;
    }

    /**
     * 设置 [NO_OF_RECRUITMENT]
     */
    @JsonProperty("no_of_recruitment")
    public void setNo_of_recruitment(Integer  no_of_recruitment){
        this.no_of_recruitment = no_of_recruitment ;
        this.no_of_recruitmentDirtyFlag = true ;
    }

    /**
     * 获取 [NO_OF_RECRUITMENT]脏标记
     */
    @JsonIgnore
    public boolean getNo_of_recruitmentDirtyFlag(){
        return no_of_recruitmentDirtyFlag ;
    }

    /**
     * 获取 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return write_date ;
    }

    /**
     * 设置 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return write_dateDirtyFlag ;
    }

    /**
     * 获取 [IS_SEO_OPTIMIZED]
     */
    @JsonProperty("is_seo_optimized")
    public String getIs_seo_optimized(){
        return is_seo_optimized ;
    }

    /**
     * 设置 [IS_SEO_OPTIMIZED]
     */
    @JsonProperty("is_seo_optimized")
    public void setIs_seo_optimized(String  is_seo_optimized){
        this.is_seo_optimized = is_seo_optimized ;
        this.is_seo_optimizedDirtyFlag = true ;
    }

    /**
     * 获取 [IS_SEO_OPTIMIZED]脏标记
     */
    @JsonIgnore
    public boolean getIs_seo_optimizedDirtyFlag(){
        return is_seo_optimizedDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_CHANNEL_IDS]
     */
    @JsonProperty("message_channel_ids")
    public String getMessage_channel_ids(){
        return message_channel_ids ;
    }

    /**
     * 设置 [MESSAGE_CHANNEL_IDS]
     */
    @JsonProperty("message_channel_ids")
    public void setMessage_channel_ids(String  message_channel_ids){
        this.message_channel_ids = message_channel_ids ;
        this.message_channel_idsDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_CHANNEL_IDS]脏标记
     */
    @JsonIgnore
    public boolean getMessage_channel_idsDirtyFlag(){
        return message_channel_idsDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_MAIN_ATTACHMENT_ID]
     */
    @JsonProperty("message_main_attachment_id")
    public Integer getMessage_main_attachment_id(){
        return message_main_attachment_id ;
    }

    /**
     * 设置 [MESSAGE_MAIN_ATTACHMENT_ID]
     */
    @JsonProperty("message_main_attachment_id")
    public void setMessage_main_attachment_id(Integer  message_main_attachment_id){
        this.message_main_attachment_id = message_main_attachment_id ;
        this.message_main_attachment_idDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_MAIN_ATTACHMENT_ID]脏标记
     */
    @JsonIgnore
    public boolean getMessage_main_attachment_idDirtyFlag(){
        return message_main_attachment_idDirtyFlag ;
    }

    /**
     * 获取 [NAME]
     */
    @JsonProperty("name")
    public String getName(){
        return name ;
    }

    /**
     * 设置 [NAME]
     */
    @JsonProperty("name")
    public void setName(String  name){
        this.name = name ;
        this.nameDirtyFlag = true ;
    }

    /**
     * 获取 [NAME]脏标记
     */
    @JsonIgnore
    public boolean getNameDirtyFlag(){
        return nameDirtyFlag ;
    }

    /**
     * 获取 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return display_name ;
    }

    /**
     * 设置 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

    /**
     * 获取 [DISPLAY_NAME]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return display_nameDirtyFlag ;
    }

    /**
     * 获取 [DESCRIPTION]
     */
    @JsonProperty("description")
    public String getDescription(){
        return description ;
    }

    /**
     * 设置 [DESCRIPTION]
     */
    @JsonProperty("description")
    public void setDescription(String  description){
        this.description = description ;
        this.descriptionDirtyFlag = true ;
    }

    /**
     * 获取 [DESCRIPTION]脏标记
     */
    @JsonIgnore
    public boolean getDescriptionDirtyFlag(){
        return descriptionDirtyFlag ;
    }

    /**
     * 获取 [WEBSITE_URL]
     */
    @JsonProperty("website_url")
    public String getWebsite_url(){
        return website_url ;
    }

    /**
     * 设置 [WEBSITE_URL]
     */
    @JsonProperty("website_url")
    public void setWebsite_url(String  website_url){
        this.website_url = website_url ;
        this.website_urlDirtyFlag = true ;
    }

    /**
     * 获取 [WEBSITE_URL]脏标记
     */
    @JsonIgnore
    public boolean getWebsite_urlDirtyFlag(){
        return website_urlDirtyFlag ;
    }

    /**
     * 获取 [REQUIREMENTS]
     */
    @JsonProperty("requirements")
    public String getRequirements(){
        return requirements ;
    }

    /**
     * 设置 [REQUIREMENTS]
     */
    @JsonProperty("requirements")
    public void setRequirements(String  requirements){
        this.requirements = requirements ;
        this.requirementsDirtyFlag = true ;
    }

    /**
     * 获取 [REQUIREMENTS]脏标记
     */
    @JsonIgnore
    public boolean getRequirementsDirtyFlag(){
        return requirementsDirtyFlag ;
    }

    /**
     * 获取 [WEBSITE_META_TITLE]
     */
    @JsonProperty("website_meta_title")
    public String getWebsite_meta_title(){
        return website_meta_title ;
    }

    /**
     * 设置 [WEBSITE_META_TITLE]
     */
    @JsonProperty("website_meta_title")
    public void setWebsite_meta_title(String  website_meta_title){
        this.website_meta_title = website_meta_title ;
        this.website_meta_titleDirtyFlag = true ;
    }

    /**
     * 获取 [WEBSITE_META_TITLE]脏标记
     */
    @JsonIgnore
    public boolean getWebsite_meta_titleDirtyFlag(){
        return website_meta_titleDirtyFlag ;
    }

    /**
     * 获取 [APPLICATION_COUNT]
     */
    @JsonProperty("application_count")
    public Integer getApplication_count(){
        return application_count ;
    }

    /**
     * 设置 [APPLICATION_COUNT]
     */
    @JsonProperty("application_count")
    public void setApplication_count(Integer  application_count){
        this.application_count = application_count ;
        this.application_countDirtyFlag = true ;
    }

    /**
     * 获取 [APPLICATION_COUNT]脏标记
     */
    @JsonIgnore
    public boolean getApplication_countDirtyFlag(){
        return application_countDirtyFlag ;
    }

    /**
     * 获取 [APPLICATION_IDS]
     */
    @JsonProperty("application_ids")
    public String getApplication_ids(){
        return application_ids ;
    }

    /**
     * 设置 [APPLICATION_IDS]
     */
    @JsonProperty("application_ids")
    public void setApplication_ids(String  application_ids){
        this.application_ids = application_ids ;
        this.application_idsDirtyFlag = true ;
    }

    /**
     * 获取 [APPLICATION_IDS]脏标记
     */
    @JsonIgnore
    public boolean getApplication_idsDirtyFlag(){
        return application_idsDirtyFlag ;
    }

    /**
     * 获取 [IS_PUBLISHED]
     */
    @JsonProperty("is_published")
    public String getIs_published(){
        return is_published ;
    }

    /**
     * 设置 [IS_PUBLISHED]
     */
    @JsonProperty("is_published")
    public void setIs_published(String  is_published){
        this.is_published = is_published ;
        this.is_publishedDirtyFlag = true ;
    }

    /**
     * 获取 [IS_PUBLISHED]脏标记
     */
    @JsonIgnore
    public boolean getIs_publishedDirtyFlag(){
        return is_publishedDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_HAS_ERROR_COUNTER]
     */
    @JsonProperty("message_has_error_counter")
    public Integer getMessage_has_error_counter(){
        return message_has_error_counter ;
    }

    /**
     * 设置 [MESSAGE_HAS_ERROR_COUNTER]
     */
    @JsonProperty("message_has_error_counter")
    public void setMessage_has_error_counter(Integer  message_has_error_counter){
        this.message_has_error_counter = message_has_error_counter ;
        this.message_has_error_counterDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_HAS_ERROR_COUNTER]脏标记
     */
    @JsonIgnore
    public boolean getMessage_has_error_counterDirtyFlag(){
        return message_has_error_counterDirtyFlag ;
    }

    /**
     * 获取 [WEBSITE_META_OG_IMG]
     */
    @JsonProperty("website_meta_og_img")
    public String getWebsite_meta_og_img(){
        return website_meta_og_img ;
    }

    /**
     * 设置 [WEBSITE_META_OG_IMG]
     */
    @JsonProperty("website_meta_og_img")
    public void setWebsite_meta_og_img(String  website_meta_og_img){
        this.website_meta_og_img = website_meta_og_img ;
        this.website_meta_og_imgDirtyFlag = true ;
    }

    /**
     * 获取 [WEBSITE_META_OG_IMG]脏标记
     */
    @JsonIgnore
    public boolean getWebsite_meta_og_imgDirtyFlag(){
        return website_meta_og_imgDirtyFlag ;
    }

    /**
     * 获取 [EXPECTED_EMPLOYEES]
     */
    @JsonProperty("expected_employees")
    public Integer getExpected_employees(){
        return expected_employees ;
    }

    /**
     * 设置 [EXPECTED_EMPLOYEES]
     */
    @JsonProperty("expected_employees")
    public void setExpected_employees(Integer  expected_employees){
        this.expected_employees = expected_employees ;
        this.expected_employeesDirtyFlag = true ;
    }

    /**
     * 获取 [EXPECTED_EMPLOYEES]脏标记
     */
    @JsonIgnore
    public boolean getExpected_employeesDirtyFlag(){
        return expected_employeesDirtyFlag ;
    }

    /**
     * 获取 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return create_date ;
    }

    /**
     * 设置 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return create_dateDirtyFlag ;
    }

    /**
     * 获取 [WEBSITE_PUBLISHED]
     */
    @JsonProperty("website_published")
    public String getWebsite_published(){
        return website_published ;
    }

    /**
     * 设置 [WEBSITE_PUBLISHED]
     */
    @JsonProperty("website_published")
    public void setWebsite_published(String  website_published){
        this.website_published = website_published ;
        this.website_publishedDirtyFlag = true ;
    }

    /**
     * 获取 [WEBSITE_PUBLISHED]脏标记
     */
    @JsonIgnore
    public boolean getWebsite_publishedDirtyFlag(){
        return website_publishedDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_IDS]
     */
    @JsonProperty("message_ids")
    public String getMessage_ids(){
        return message_ids ;
    }

    /**
     * 设置 [MESSAGE_IDS]
     */
    @JsonProperty("message_ids")
    public void setMessage_ids(String  message_ids){
        this.message_ids = message_ids ;
        this.message_idsDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_IDS]脏标记
     */
    @JsonIgnore
    public boolean getMessage_idsDirtyFlag(){
        return message_idsDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_ATTACHMENT_COUNT]
     */
    @JsonProperty("message_attachment_count")
    public Integer getMessage_attachment_count(){
        return message_attachment_count ;
    }

    /**
     * 设置 [MESSAGE_ATTACHMENT_COUNT]
     */
    @JsonProperty("message_attachment_count")
    public void setMessage_attachment_count(Integer  message_attachment_count){
        this.message_attachment_count = message_attachment_count ;
        this.message_attachment_countDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_ATTACHMENT_COUNT]脏标记
     */
    @JsonIgnore
    public boolean getMessage_attachment_countDirtyFlag(){
        return message_attachment_countDirtyFlag ;
    }

    /**
     * 获取 [WEBSITE_DESCRIPTION]
     */
    @JsonProperty("website_description")
    public String getWebsite_description(){
        return website_description ;
    }

    /**
     * 设置 [WEBSITE_DESCRIPTION]
     */
    @JsonProperty("website_description")
    public void setWebsite_description(String  website_description){
        this.website_description = website_description ;
        this.website_descriptionDirtyFlag = true ;
    }

    /**
     * 获取 [WEBSITE_DESCRIPTION]脏标记
     */
    @JsonIgnore
    public boolean getWebsite_descriptionDirtyFlag(){
        return website_descriptionDirtyFlag ;
    }

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

    /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return idDirtyFlag ;
    }

    /**
     * 获取 [WEBSITE_MESSAGE_IDS]
     */
    @JsonProperty("website_message_ids")
    public String getWebsite_message_ids(){
        return website_message_ids ;
    }

    /**
     * 设置 [WEBSITE_MESSAGE_IDS]
     */
    @JsonProperty("website_message_ids")
    public void setWebsite_message_ids(String  website_message_ids){
        this.website_message_ids = website_message_ids ;
        this.website_message_idsDirtyFlag = true ;
    }

    /**
     * 获取 [WEBSITE_MESSAGE_IDS]脏标记
     */
    @JsonIgnore
    public boolean getWebsite_message_idsDirtyFlag(){
        return website_message_idsDirtyFlag ;
    }

    /**
     * 获取 [COLOR]
     */
    @JsonProperty("color")
    public Integer getColor(){
        return color ;
    }

    /**
     * 设置 [COLOR]
     */
    @JsonProperty("color")
    public void setColor(Integer  color){
        this.color = color ;
        this.colorDirtyFlag = true ;
    }

    /**
     * 获取 [COLOR]脏标记
     */
    @JsonIgnore
    public boolean getColorDirtyFlag(){
        return colorDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_FOLLOWER_IDS]
     */
    @JsonProperty("message_follower_ids")
    public String getMessage_follower_ids(){
        return message_follower_ids ;
    }

    /**
     * 设置 [MESSAGE_FOLLOWER_IDS]
     */
    @JsonProperty("message_follower_ids")
    public void setMessage_follower_ids(String  message_follower_ids){
        this.message_follower_ids = message_follower_ids ;
        this.message_follower_idsDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_FOLLOWER_IDS]脏标记
     */
    @JsonIgnore
    public boolean getMessage_follower_idsDirtyFlag(){
        return message_follower_idsDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_UNREAD]
     */
    @JsonProperty("message_unread")
    public String getMessage_unread(){
        return message_unread ;
    }

    /**
     * 设置 [MESSAGE_UNREAD]
     */
    @JsonProperty("message_unread")
    public void setMessage_unread(String  message_unread){
        this.message_unread = message_unread ;
        this.message_unreadDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_UNREAD]脏标记
     */
    @JsonIgnore
    public boolean getMessage_unreadDirtyFlag(){
        return message_unreadDirtyFlag ;
    }

    /**
     * 获取 [DOCUMENT_IDS]
     */
    @JsonProperty("document_ids")
    public String getDocument_ids(){
        return document_ids ;
    }

    /**
     * 设置 [DOCUMENT_IDS]
     */
    @JsonProperty("document_ids")
    public void setDocument_ids(String  document_ids){
        this.document_ids = document_ids ;
        this.document_idsDirtyFlag = true ;
    }

    /**
     * 获取 [DOCUMENT_IDS]脏标记
     */
    @JsonIgnore
    public boolean getDocument_idsDirtyFlag(){
        return document_idsDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_NEEDACTION]
     */
    @JsonProperty("message_needaction")
    public String getMessage_needaction(){
        return message_needaction ;
    }

    /**
     * 设置 [MESSAGE_NEEDACTION]
     */
    @JsonProperty("message_needaction")
    public void setMessage_needaction(String  message_needaction){
        this.message_needaction = message_needaction ;
        this.message_needactionDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_NEEDACTION]脏标记
     */
    @JsonIgnore
    public boolean getMessage_needactionDirtyFlag(){
        return message_needactionDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_NEEDACTION_COUNTER]
     */
    @JsonProperty("message_needaction_counter")
    public Integer getMessage_needaction_counter(){
        return message_needaction_counter ;
    }

    /**
     * 设置 [MESSAGE_NEEDACTION_COUNTER]
     */
    @JsonProperty("message_needaction_counter")
    public void setMessage_needaction_counter(Integer  message_needaction_counter){
        this.message_needaction_counter = message_needaction_counter ;
        this.message_needaction_counterDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_NEEDACTION_COUNTER]脏标记
     */
    @JsonIgnore
    public boolean getMessage_needaction_counterDirtyFlag(){
        return message_needaction_counterDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_HAS_ERROR]
     */
    @JsonProperty("message_has_error")
    public String getMessage_has_error(){
        return message_has_error ;
    }

    /**
     * 设置 [MESSAGE_HAS_ERROR]
     */
    @JsonProperty("message_has_error")
    public void setMessage_has_error(String  message_has_error){
        this.message_has_error = message_has_error ;
        this.message_has_errorDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_HAS_ERROR]脏标记
     */
    @JsonIgnore
    public boolean getMessage_has_errorDirtyFlag(){
        return message_has_errorDirtyFlag ;
    }

    /**
     * 获取 [EMPLOYEE_IDS]
     */
    @JsonProperty("employee_ids")
    public String getEmployee_ids(){
        return employee_ids ;
    }

    /**
     * 设置 [EMPLOYEE_IDS]
     */
    @JsonProperty("employee_ids")
    public void setEmployee_ids(String  employee_ids){
        this.employee_ids = employee_ids ;
        this.employee_idsDirtyFlag = true ;
    }

    /**
     * 获取 [EMPLOYEE_IDS]脏标记
     */
    @JsonIgnore
    public boolean getEmployee_idsDirtyFlag(){
        return employee_idsDirtyFlag ;
    }

    /**
     * 获取 [DOCUMENTS_COUNT]
     */
    @JsonProperty("documents_count")
    public Integer getDocuments_count(){
        return documents_count ;
    }

    /**
     * 设置 [DOCUMENTS_COUNT]
     */
    @JsonProperty("documents_count")
    public void setDocuments_count(Integer  documents_count){
        this.documents_count = documents_count ;
        this.documents_countDirtyFlag = true ;
    }

    /**
     * 获取 [DOCUMENTS_COUNT]脏标记
     */
    @JsonIgnore
    public boolean getDocuments_countDirtyFlag(){
        return documents_countDirtyFlag ;
    }

    /**
     * 获取 [WEBSITE_ID]
     */
    @JsonProperty("website_id")
    public Integer getWebsite_id(){
        return website_id ;
    }

    /**
     * 设置 [WEBSITE_ID]
     */
    @JsonProperty("website_id")
    public void setWebsite_id(Integer  website_id){
        this.website_id = website_id ;
        this.website_idDirtyFlag = true ;
    }

    /**
     * 获取 [WEBSITE_ID]脏标记
     */
    @JsonIgnore
    public boolean getWebsite_idDirtyFlag(){
        return website_idDirtyFlag ;
    }

    /**
     * 获取 [WEBSITE_META_KEYWORDS]
     */
    @JsonProperty("website_meta_keywords")
    public String getWebsite_meta_keywords(){
        return website_meta_keywords ;
    }

    /**
     * 设置 [WEBSITE_META_KEYWORDS]
     */
    @JsonProperty("website_meta_keywords")
    public void setWebsite_meta_keywords(String  website_meta_keywords){
        this.website_meta_keywords = website_meta_keywords ;
        this.website_meta_keywordsDirtyFlag = true ;
    }

    /**
     * 获取 [WEBSITE_META_KEYWORDS]脏标记
     */
    @JsonIgnore
    public boolean getWebsite_meta_keywordsDirtyFlag(){
        return website_meta_keywordsDirtyFlag ;
    }

    /**
     * 获取 [ALIAS_DEFAULTS]
     */
    @JsonProperty("alias_defaults")
    public String getAlias_defaults(){
        return alias_defaults ;
    }

    /**
     * 设置 [ALIAS_DEFAULTS]
     */
    @JsonProperty("alias_defaults")
    public void setAlias_defaults(String  alias_defaults){
        this.alias_defaults = alias_defaults ;
        this.alias_defaultsDirtyFlag = true ;
    }

    /**
     * 获取 [ALIAS_DEFAULTS]脏标记
     */
    @JsonIgnore
    public boolean getAlias_defaultsDirtyFlag(){
        return alias_defaultsDirtyFlag ;
    }

    /**
     * 获取 [ALIAS_NAME]
     */
    @JsonProperty("alias_name")
    public String getAlias_name(){
        return alias_name ;
    }

    /**
     * 设置 [ALIAS_NAME]
     */
    @JsonProperty("alias_name")
    public void setAlias_name(String  alias_name){
        this.alias_name = alias_name ;
        this.alias_nameDirtyFlag = true ;
    }

    /**
     * 获取 [ALIAS_NAME]脏标记
     */
    @JsonIgnore
    public boolean getAlias_nameDirtyFlag(){
        return alias_nameDirtyFlag ;
    }

    /**
     * 获取 [ALIAS_FORCE_THREAD_ID]
     */
    @JsonProperty("alias_force_thread_id")
    public Integer getAlias_force_thread_id(){
        return alias_force_thread_id ;
    }

    /**
     * 设置 [ALIAS_FORCE_THREAD_ID]
     */
    @JsonProperty("alias_force_thread_id")
    public void setAlias_force_thread_id(Integer  alias_force_thread_id){
        this.alias_force_thread_id = alias_force_thread_id ;
        this.alias_force_thread_idDirtyFlag = true ;
    }

    /**
     * 获取 [ALIAS_FORCE_THREAD_ID]脏标记
     */
    @JsonIgnore
    public boolean getAlias_force_thread_idDirtyFlag(){
        return alias_force_thread_idDirtyFlag ;
    }

    /**
     * 获取 [ALIAS_DOMAIN]
     */
    @JsonProperty("alias_domain")
    public String getAlias_domain(){
        return alias_domain ;
    }

    /**
     * 设置 [ALIAS_DOMAIN]
     */
    @JsonProperty("alias_domain")
    public void setAlias_domain(String  alias_domain){
        this.alias_domain = alias_domain ;
        this.alias_domainDirtyFlag = true ;
    }

    /**
     * 获取 [ALIAS_DOMAIN]脏标记
     */
    @JsonIgnore
    public boolean getAlias_domainDirtyFlag(){
        return alias_domainDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return write_uid_text ;
    }

    /**
     * 设置 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return write_uid_textDirtyFlag ;
    }

    /**
     * 获取 [ALIAS_PARENT_MODEL_ID]
     */
    @JsonProperty("alias_parent_model_id")
    public Integer getAlias_parent_model_id(){
        return alias_parent_model_id ;
    }

    /**
     * 设置 [ALIAS_PARENT_MODEL_ID]
     */
    @JsonProperty("alias_parent_model_id")
    public void setAlias_parent_model_id(Integer  alias_parent_model_id){
        this.alias_parent_model_id = alias_parent_model_id ;
        this.alias_parent_model_idDirtyFlag = true ;
    }

    /**
     * 获取 [ALIAS_PARENT_MODEL_ID]脏标记
     */
    @JsonIgnore
    public boolean getAlias_parent_model_idDirtyFlag(){
        return alias_parent_model_idDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return create_uid_text ;
    }

    /**
     * 设置 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return create_uid_textDirtyFlag ;
    }

    /**
     * 获取 [ALIAS_PARENT_THREAD_ID]
     */
    @JsonProperty("alias_parent_thread_id")
    public Integer getAlias_parent_thread_id(){
        return alias_parent_thread_id ;
    }

    /**
     * 设置 [ALIAS_PARENT_THREAD_ID]
     */
    @JsonProperty("alias_parent_thread_id")
    public void setAlias_parent_thread_id(Integer  alias_parent_thread_id){
        this.alias_parent_thread_id = alias_parent_thread_id ;
        this.alias_parent_thread_idDirtyFlag = true ;
    }

    /**
     * 获取 [ALIAS_PARENT_THREAD_ID]脏标记
     */
    @JsonIgnore
    public boolean getAlias_parent_thread_idDirtyFlag(){
        return alias_parent_thread_idDirtyFlag ;
    }

    /**
     * 获取 [ALIAS_MODEL_ID]
     */
    @JsonProperty("alias_model_id")
    public Integer getAlias_model_id(){
        return alias_model_id ;
    }

    /**
     * 设置 [ALIAS_MODEL_ID]
     */
    @JsonProperty("alias_model_id")
    public void setAlias_model_id(Integer  alias_model_id){
        this.alias_model_id = alias_model_id ;
        this.alias_model_idDirtyFlag = true ;
    }

    /**
     * 获取 [ALIAS_MODEL_ID]脏标记
     */
    @JsonIgnore
    public boolean getAlias_model_idDirtyFlag(){
        return alias_model_idDirtyFlag ;
    }

    /**
     * 获取 [ADDRESS_ID_TEXT]
     */
    @JsonProperty("address_id_text")
    public String getAddress_id_text(){
        return address_id_text ;
    }

    /**
     * 设置 [ADDRESS_ID_TEXT]
     */
    @JsonProperty("address_id_text")
    public void setAddress_id_text(String  address_id_text){
        this.address_id_text = address_id_text ;
        this.address_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [ADDRESS_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getAddress_id_textDirtyFlag(){
        return address_id_textDirtyFlag ;
    }

    /**
     * 获取 [ALIAS_USER_ID]
     */
    @JsonProperty("alias_user_id")
    public Integer getAlias_user_id(){
        return alias_user_id ;
    }

    /**
     * 设置 [ALIAS_USER_ID]
     */
    @JsonProperty("alias_user_id")
    public void setAlias_user_id(Integer  alias_user_id){
        this.alias_user_id = alias_user_id ;
        this.alias_user_idDirtyFlag = true ;
    }

    /**
     * 获取 [ALIAS_USER_ID]脏标记
     */
    @JsonIgnore
    public boolean getAlias_user_idDirtyFlag(){
        return alias_user_idDirtyFlag ;
    }

    /**
     * 获取 [DEPARTMENT_ID_TEXT]
     */
    @JsonProperty("department_id_text")
    public String getDepartment_id_text(){
        return department_id_text ;
    }

    /**
     * 设置 [DEPARTMENT_ID_TEXT]
     */
    @JsonProperty("department_id_text")
    public void setDepartment_id_text(String  department_id_text){
        this.department_id_text = department_id_text ;
        this.department_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [DEPARTMENT_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getDepartment_id_textDirtyFlag(){
        return department_id_textDirtyFlag ;
    }

    /**
     * 获取 [COMPANY_ID_TEXT]
     */
    @JsonProperty("company_id_text")
    public String getCompany_id_text(){
        return company_id_text ;
    }

    /**
     * 设置 [COMPANY_ID_TEXT]
     */
    @JsonProperty("company_id_text")
    public void setCompany_id_text(String  company_id_text){
        this.company_id_text = company_id_text ;
        this.company_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [COMPANY_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getCompany_id_textDirtyFlag(){
        return company_id_textDirtyFlag ;
    }

    /**
     * 获取 [USER_ID_TEXT]
     */
    @JsonProperty("user_id_text")
    public String getUser_id_text(){
        return user_id_text ;
    }

    /**
     * 设置 [USER_ID_TEXT]
     */
    @JsonProperty("user_id_text")
    public void setUser_id_text(String  user_id_text){
        this.user_id_text = user_id_text ;
        this.user_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [USER_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getUser_id_textDirtyFlag(){
        return user_id_textDirtyFlag ;
    }

    /**
     * 获取 [MANAGER_ID_TEXT]
     */
    @JsonProperty("manager_id_text")
    public String getManager_id_text(){
        return manager_id_text ;
    }

    /**
     * 设置 [MANAGER_ID_TEXT]
     */
    @JsonProperty("manager_id_text")
    public void setManager_id_text(String  manager_id_text){
        this.manager_id_text = manager_id_text ;
        this.manager_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [MANAGER_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getManager_id_textDirtyFlag(){
        return manager_id_textDirtyFlag ;
    }

    /**
     * 获取 [ALIAS_CONTACT]
     */
    @JsonProperty("alias_contact")
    public String getAlias_contact(){
        return alias_contact ;
    }

    /**
     * 设置 [ALIAS_CONTACT]
     */
    @JsonProperty("alias_contact")
    public void setAlias_contact(String  alias_contact){
        this.alias_contact = alias_contact ;
        this.alias_contactDirtyFlag = true ;
    }

    /**
     * 获取 [ALIAS_CONTACT]脏标记
     */
    @JsonIgnore
    public boolean getAlias_contactDirtyFlag(){
        return alias_contactDirtyFlag ;
    }

    /**
     * 获取 [HR_RESPONSIBLE_ID_TEXT]
     */
    @JsonProperty("hr_responsible_id_text")
    public String getHr_responsible_id_text(){
        return hr_responsible_id_text ;
    }

    /**
     * 设置 [HR_RESPONSIBLE_ID_TEXT]
     */
    @JsonProperty("hr_responsible_id_text")
    public void setHr_responsible_id_text(String  hr_responsible_id_text){
        this.hr_responsible_id_text = hr_responsible_id_text ;
        this.hr_responsible_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [HR_RESPONSIBLE_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getHr_responsible_id_textDirtyFlag(){
        return hr_responsible_id_textDirtyFlag ;
    }

    /**
     * 获取 [COMPANY_ID]
     */
    @JsonProperty("company_id")
    public Integer getCompany_id(){
        return company_id ;
    }

    /**
     * 设置 [COMPANY_ID]
     */
    @JsonProperty("company_id")
    public void setCompany_id(Integer  company_id){
        this.company_id = company_id ;
        this.company_idDirtyFlag = true ;
    }

    /**
     * 获取 [COMPANY_ID]脏标记
     */
    @JsonIgnore
    public boolean getCompany_idDirtyFlag(){
        return company_idDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return write_uid ;
    }

    /**
     * 设置 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return write_uidDirtyFlag ;
    }

    /**
     * 获取 [MANAGER_ID]
     */
    @JsonProperty("manager_id")
    public Integer getManager_id(){
        return manager_id ;
    }

    /**
     * 设置 [MANAGER_ID]
     */
    @JsonProperty("manager_id")
    public void setManager_id(Integer  manager_id){
        this.manager_id = manager_id ;
        this.manager_idDirtyFlag = true ;
    }

    /**
     * 获取 [MANAGER_ID]脏标记
     */
    @JsonIgnore
    public boolean getManager_idDirtyFlag(){
        return manager_idDirtyFlag ;
    }

    /**
     * 获取 [ADDRESS_ID]
     */
    @JsonProperty("address_id")
    public Integer getAddress_id(){
        return address_id ;
    }

    /**
     * 设置 [ADDRESS_ID]
     */
    @JsonProperty("address_id")
    public void setAddress_id(Integer  address_id){
        this.address_id = address_id ;
        this.address_idDirtyFlag = true ;
    }

    /**
     * 获取 [ADDRESS_ID]脏标记
     */
    @JsonIgnore
    public boolean getAddress_idDirtyFlag(){
        return address_idDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return create_uid ;
    }

    /**
     * 设置 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return create_uidDirtyFlag ;
    }

    /**
     * 获取 [HR_RESPONSIBLE_ID]
     */
    @JsonProperty("hr_responsible_id")
    public Integer getHr_responsible_id(){
        return hr_responsible_id ;
    }

    /**
     * 设置 [HR_RESPONSIBLE_ID]
     */
    @JsonProperty("hr_responsible_id")
    public void setHr_responsible_id(Integer  hr_responsible_id){
        this.hr_responsible_id = hr_responsible_id ;
        this.hr_responsible_idDirtyFlag = true ;
    }

    /**
     * 获取 [HR_RESPONSIBLE_ID]脏标记
     */
    @JsonIgnore
    public boolean getHr_responsible_idDirtyFlag(){
        return hr_responsible_idDirtyFlag ;
    }

    /**
     * 获取 [DEPARTMENT_ID]
     */
    @JsonProperty("department_id")
    public Integer getDepartment_id(){
        return department_id ;
    }

    /**
     * 设置 [DEPARTMENT_ID]
     */
    @JsonProperty("department_id")
    public void setDepartment_id(Integer  department_id){
        this.department_id = department_id ;
        this.department_idDirtyFlag = true ;
    }

    /**
     * 获取 [DEPARTMENT_ID]脏标记
     */
    @JsonIgnore
    public boolean getDepartment_idDirtyFlag(){
        return department_idDirtyFlag ;
    }

    /**
     * 获取 [ALIAS_ID]
     */
    @JsonProperty("alias_id")
    public Integer getAlias_id(){
        return alias_id ;
    }

    /**
     * 设置 [ALIAS_ID]
     */
    @JsonProperty("alias_id")
    public void setAlias_id(Integer  alias_id){
        this.alias_id = alias_id ;
        this.alias_idDirtyFlag = true ;
    }

    /**
     * 获取 [ALIAS_ID]脏标记
     */
    @JsonIgnore
    public boolean getAlias_idDirtyFlag(){
        return alias_idDirtyFlag ;
    }

    /**
     * 获取 [USER_ID]
     */
    @JsonProperty("user_id")
    public Integer getUser_id(){
        return user_id ;
    }

    /**
     * 设置 [USER_ID]
     */
    @JsonProperty("user_id")
    public void setUser_id(Integer  user_id){
        this.user_id = user_id ;
        this.user_idDirtyFlag = true ;
    }

    /**
     * 获取 [USER_ID]脏标记
     */
    @JsonIgnore
    public boolean getUser_idDirtyFlag(){
        return user_idDirtyFlag ;
    }



    public Hr_job toDO() {
        Hr_job srfdomain = new Hr_job();
        if(getMessage_is_followerDirtyFlag())
            srfdomain.setMessage_is_follower(message_is_follower);
        if(getNo_of_employeeDirtyFlag())
            srfdomain.setNo_of_employee(no_of_employee);
        if(getStateDirtyFlag())
            srfdomain.setState(state);
        if(getMessage_partner_idsDirtyFlag())
            srfdomain.setMessage_partner_ids(message_partner_ids);
        if(get__last_updateDirtyFlag())
            srfdomain.set__last_update(__last_update);
        if(getNo_of_hired_employeeDirtyFlag())
            srfdomain.setNo_of_hired_employee(no_of_hired_employee);
        if(getMessage_unread_counterDirtyFlag())
            srfdomain.setMessage_unread_counter(message_unread_counter);
        if(getWebsite_meta_descriptionDirtyFlag())
            srfdomain.setWebsite_meta_description(website_meta_description);
        if(getNo_of_recruitmentDirtyFlag())
            srfdomain.setNo_of_recruitment(no_of_recruitment);
        if(getWrite_dateDirtyFlag())
            srfdomain.setWrite_date(write_date);
        if(getIs_seo_optimizedDirtyFlag())
            srfdomain.setIs_seo_optimized(is_seo_optimized);
        if(getMessage_channel_idsDirtyFlag())
            srfdomain.setMessage_channel_ids(message_channel_ids);
        if(getMessage_main_attachment_idDirtyFlag())
            srfdomain.setMessage_main_attachment_id(message_main_attachment_id);
        if(getNameDirtyFlag())
            srfdomain.setName(name);
        if(getDisplay_nameDirtyFlag())
            srfdomain.setDisplay_name(display_name);
        if(getDescriptionDirtyFlag())
            srfdomain.setDescription(description);
        if(getWebsite_urlDirtyFlag())
            srfdomain.setWebsite_url(website_url);
        if(getRequirementsDirtyFlag())
            srfdomain.setRequirements(requirements);
        if(getWebsite_meta_titleDirtyFlag())
            srfdomain.setWebsite_meta_title(website_meta_title);
        if(getApplication_countDirtyFlag())
            srfdomain.setApplication_count(application_count);
        if(getApplication_idsDirtyFlag())
            srfdomain.setApplication_ids(application_ids);
        if(getIs_publishedDirtyFlag())
            srfdomain.setIs_published(is_published);
        if(getMessage_has_error_counterDirtyFlag())
            srfdomain.setMessage_has_error_counter(message_has_error_counter);
        if(getWebsite_meta_og_imgDirtyFlag())
            srfdomain.setWebsite_meta_og_img(website_meta_og_img);
        if(getExpected_employeesDirtyFlag())
            srfdomain.setExpected_employees(expected_employees);
        if(getCreate_dateDirtyFlag())
            srfdomain.setCreate_date(create_date);
        if(getWebsite_publishedDirtyFlag())
            srfdomain.setWebsite_published(website_published);
        if(getMessage_idsDirtyFlag())
            srfdomain.setMessage_ids(message_ids);
        if(getMessage_attachment_countDirtyFlag())
            srfdomain.setMessage_attachment_count(message_attachment_count);
        if(getWebsite_descriptionDirtyFlag())
            srfdomain.setWebsite_description(website_description);
        if(getIdDirtyFlag())
            srfdomain.setId(id);
        if(getWebsite_message_idsDirtyFlag())
            srfdomain.setWebsite_message_ids(website_message_ids);
        if(getColorDirtyFlag())
            srfdomain.setColor(color);
        if(getMessage_follower_idsDirtyFlag())
            srfdomain.setMessage_follower_ids(message_follower_ids);
        if(getMessage_unreadDirtyFlag())
            srfdomain.setMessage_unread(message_unread);
        if(getDocument_idsDirtyFlag())
            srfdomain.setDocument_ids(document_ids);
        if(getMessage_needactionDirtyFlag())
            srfdomain.setMessage_needaction(message_needaction);
        if(getMessage_needaction_counterDirtyFlag())
            srfdomain.setMessage_needaction_counter(message_needaction_counter);
        if(getMessage_has_errorDirtyFlag())
            srfdomain.setMessage_has_error(message_has_error);
        if(getEmployee_idsDirtyFlag())
            srfdomain.setEmployee_ids(employee_ids);
        if(getDocuments_countDirtyFlag())
            srfdomain.setDocuments_count(documents_count);
        if(getWebsite_idDirtyFlag())
            srfdomain.setWebsite_id(website_id);
        if(getWebsite_meta_keywordsDirtyFlag())
            srfdomain.setWebsite_meta_keywords(website_meta_keywords);
        if(getAlias_defaultsDirtyFlag())
            srfdomain.setAlias_defaults(alias_defaults);
        if(getAlias_nameDirtyFlag())
            srfdomain.setAlias_name(alias_name);
        if(getAlias_force_thread_idDirtyFlag())
            srfdomain.setAlias_force_thread_id(alias_force_thread_id);
        if(getAlias_domainDirtyFlag())
            srfdomain.setAlias_domain(alias_domain);
        if(getWrite_uid_textDirtyFlag())
            srfdomain.setWrite_uid_text(write_uid_text);
        if(getAlias_parent_model_idDirtyFlag())
            srfdomain.setAlias_parent_model_id(alias_parent_model_id);
        if(getCreate_uid_textDirtyFlag())
            srfdomain.setCreate_uid_text(create_uid_text);
        if(getAlias_parent_thread_idDirtyFlag())
            srfdomain.setAlias_parent_thread_id(alias_parent_thread_id);
        if(getAlias_model_idDirtyFlag())
            srfdomain.setAlias_model_id(alias_model_id);
        if(getAddress_id_textDirtyFlag())
            srfdomain.setAddress_id_text(address_id_text);
        if(getAlias_user_idDirtyFlag())
            srfdomain.setAlias_user_id(alias_user_id);
        if(getDepartment_id_textDirtyFlag())
            srfdomain.setDepartment_id_text(department_id_text);
        if(getCompany_id_textDirtyFlag())
            srfdomain.setCompany_id_text(company_id_text);
        if(getUser_id_textDirtyFlag())
            srfdomain.setUser_id_text(user_id_text);
        if(getManager_id_textDirtyFlag())
            srfdomain.setManager_id_text(manager_id_text);
        if(getAlias_contactDirtyFlag())
            srfdomain.setAlias_contact(alias_contact);
        if(getHr_responsible_id_textDirtyFlag())
            srfdomain.setHr_responsible_id_text(hr_responsible_id_text);
        if(getCompany_idDirtyFlag())
            srfdomain.setCompany_id(company_id);
        if(getWrite_uidDirtyFlag())
            srfdomain.setWrite_uid(write_uid);
        if(getManager_idDirtyFlag())
            srfdomain.setManager_id(manager_id);
        if(getAddress_idDirtyFlag())
            srfdomain.setAddress_id(address_id);
        if(getCreate_uidDirtyFlag())
            srfdomain.setCreate_uid(create_uid);
        if(getHr_responsible_idDirtyFlag())
            srfdomain.setHr_responsible_id(hr_responsible_id);
        if(getDepartment_idDirtyFlag())
            srfdomain.setDepartment_id(department_id);
        if(getAlias_idDirtyFlag())
            srfdomain.setAlias_id(alias_id);
        if(getUser_idDirtyFlag())
            srfdomain.setUser_id(user_id);

        return srfdomain;
    }

    public void fromDO(Hr_job srfdomain) {
        if(srfdomain == null )
          return ;
        if(srfdomain.getMessage_is_followerDirtyFlag())
            this.setMessage_is_follower(srfdomain.getMessage_is_follower());
        if(srfdomain.getNo_of_employeeDirtyFlag())
            this.setNo_of_employee(srfdomain.getNo_of_employee());
        if(srfdomain.getStateDirtyFlag())
            this.setState(srfdomain.getState());
        if(srfdomain.getMessage_partner_idsDirtyFlag())
            this.setMessage_partner_ids(srfdomain.getMessage_partner_ids());
        if(srfdomain.get__last_updateDirtyFlag())
            this.set__last_update(srfdomain.get__last_update());
        if(srfdomain.getNo_of_hired_employeeDirtyFlag())
            this.setNo_of_hired_employee(srfdomain.getNo_of_hired_employee());
        if(srfdomain.getMessage_unread_counterDirtyFlag())
            this.setMessage_unread_counter(srfdomain.getMessage_unread_counter());
        if(srfdomain.getWebsite_meta_descriptionDirtyFlag())
            this.setWebsite_meta_description(srfdomain.getWebsite_meta_description());
        if(srfdomain.getNo_of_recruitmentDirtyFlag())
            this.setNo_of_recruitment(srfdomain.getNo_of_recruitment());
        if(srfdomain.getWrite_dateDirtyFlag())
            this.setWrite_date(srfdomain.getWrite_date());
        if(srfdomain.getIs_seo_optimizedDirtyFlag())
            this.setIs_seo_optimized(srfdomain.getIs_seo_optimized());
        if(srfdomain.getMessage_channel_idsDirtyFlag())
            this.setMessage_channel_ids(srfdomain.getMessage_channel_ids());
        if(srfdomain.getMessage_main_attachment_idDirtyFlag())
            this.setMessage_main_attachment_id(srfdomain.getMessage_main_attachment_id());
        if(srfdomain.getNameDirtyFlag())
            this.setName(srfdomain.getName());
        if(srfdomain.getDisplay_nameDirtyFlag())
            this.setDisplay_name(srfdomain.getDisplay_name());
        if(srfdomain.getDescriptionDirtyFlag())
            this.setDescription(srfdomain.getDescription());
        if(srfdomain.getWebsite_urlDirtyFlag())
            this.setWebsite_url(srfdomain.getWebsite_url());
        if(srfdomain.getRequirementsDirtyFlag())
            this.setRequirements(srfdomain.getRequirements());
        if(srfdomain.getWebsite_meta_titleDirtyFlag())
            this.setWebsite_meta_title(srfdomain.getWebsite_meta_title());
        if(srfdomain.getApplication_countDirtyFlag())
            this.setApplication_count(srfdomain.getApplication_count());
        if(srfdomain.getApplication_idsDirtyFlag())
            this.setApplication_ids(srfdomain.getApplication_ids());
        if(srfdomain.getIs_publishedDirtyFlag())
            this.setIs_published(srfdomain.getIs_published());
        if(srfdomain.getMessage_has_error_counterDirtyFlag())
            this.setMessage_has_error_counter(srfdomain.getMessage_has_error_counter());
        if(srfdomain.getWebsite_meta_og_imgDirtyFlag())
            this.setWebsite_meta_og_img(srfdomain.getWebsite_meta_og_img());
        if(srfdomain.getExpected_employeesDirtyFlag())
            this.setExpected_employees(srfdomain.getExpected_employees());
        if(srfdomain.getCreate_dateDirtyFlag())
            this.setCreate_date(srfdomain.getCreate_date());
        if(srfdomain.getWebsite_publishedDirtyFlag())
            this.setWebsite_published(srfdomain.getWebsite_published());
        if(srfdomain.getMessage_idsDirtyFlag())
            this.setMessage_ids(srfdomain.getMessage_ids());
        if(srfdomain.getMessage_attachment_countDirtyFlag())
            this.setMessage_attachment_count(srfdomain.getMessage_attachment_count());
        if(srfdomain.getWebsite_descriptionDirtyFlag())
            this.setWebsite_description(srfdomain.getWebsite_description());
        if(srfdomain.getIdDirtyFlag())
            this.setId(srfdomain.getId());
        if(srfdomain.getWebsite_message_idsDirtyFlag())
            this.setWebsite_message_ids(srfdomain.getWebsite_message_ids());
        if(srfdomain.getColorDirtyFlag())
            this.setColor(srfdomain.getColor());
        if(srfdomain.getMessage_follower_idsDirtyFlag())
            this.setMessage_follower_ids(srfdomain.getMessage_follower_ids());
        if(srfdomain.getMessage_unreadDirtyFlag())
            this.setMessage_unread(srfdomain.getMessage_unread());
        if(srfdomain.getDocument_idsDirtyFlag())
            this.setDocument_ids(srfdomain.getDocument_ids());
        if(srfdomain.getMessage_needactionDirtyFlag())
            this.setMessage_needaction(srfdomain.getMessage_needaction());
        if(srfdomain.getMessage_needaction_counterDirtyFlag())
            this.setMessage_needaction_counter(srfdomain.getMessage_needaction_counter());
        if(srfdomain.getMessage_has_errorDirtyFlag())
            this.setMessage_has_error(srfdomain.getMessage_has_error());
        if(srfdomain.getEmployee_idsDirtyFlag())
            this.setEmployee_ids(srfdomain.getEmployee_ids());
        if(srfdomain.getDocuments_countDirtyFlag())
            this.setDocuments_count(srfdomain.getDocuments_count());
        if(srfdomain.getWebsite_idDirtyFlag())
            this.setWebsite_id(srfdomain.getWebsite_id());
        if(srfdomain.getWebsite_meta_keywordsDirtyFlag())
            this.setWebsite_meta_keywords(srfdomain.getWebsite_meta_keywords());
        if(srfdomain.getAlias_defaultsDirtyFlag())
            this.setAlias_defaults(srfdomain.getAlias_defaults());
        if(srfdomain.getAlias_nameDirtyFlag())
            this.setAlias_name(srfdomain.getAlias_name());
        if(srfdomain.getAlias_force_thread_idDirtyFlag())
            this.setAlias_force_thread_id(srfdomain.getAlias_force_thread_id());
        if(srfdomain.getAlias_domainDirtyFlag())
            this.setAlias_domain(srfdomain.getAlias_domain());
        if(srfdomain.getWrite_uid_textDirtyFlag())
            this.setWrite_uid_text(srfdomain.getWrite_uid_text());
        if(srfdomain.getAlias_parent_model_idDirtyFlag())
            this.setAlias_parent_model_id(srfdomain.getAlias_parent_model_id());
        if(srfdomain.getCreate_uid_textDirtyFlag())
            this.setCreate_uid_text(srfdomain.getCreate_uid_text());
        if(srfdomain.getAlias_parent_thread_idDirtyFlag())
            this.setAlias_parent_thread_id(srfdomain.getAlias_parent_thread_id());
        if(srfdomain.getAlias_model_idDirtyFlag())
            this.setAlias_model_id(srfdomain.getAlias_model_id());
        if(srfdomain.getAddress_id_textDirtyFlag())
            this.setAddress_id_text(srfdomain.getAddress_id_text());
        if(srfdomain.getAlias_user_idDirtyFlag())
            this.setAlias_user_id(srfdomain.getAlias_user_id());
        if(srfdomain.getDepartment_id_textDirtyFlag())
            this.setDepartment_id_text(srfdomain.getDepartment_id_text());
        if(srfdomain.getCompany_id_textDirtyFlag())
            this.setCompany_id_text(srfdomain.getCompany_id_text());
        if(srfdomain.getUser_id_textDirtyFlag())
            this.setUser_id_text(srfdomain.getUser_id_text());
        if(srfdomain.getManager_id_textDirtyFlag())
            this.setManager_id_text(srfdomain.getManager_id_text());
        if(srfdomain.getAlias_contactDirtyFlag())
            this.setAlias_contact(srfdomain.getAlias_contact());
        if(srfdomain.getHr_responsible_id_textDirtyFlag())
            this.setHr_responsible_id_text(srfdomain.getHr_responsible_id_text());
        if(srfdomain.getCompany_idDirtyFlag())
            this.setCompany_id(srfdomain.getCompany_id());
        if(srfdomain.getWrite_uidDirtyFlag())
            this.setWrite_uid(srfdomain.getWrite_uid());
        if(srfdomain.getManager_idDirtyFlag())
            this.setManager_id(srfdomain.getManager_id());
        if(srfdomain.getAddress_idDirtyFlag())
            this.setAddress_id(srfdomain.getAddress_id());
        if(srfdomain.getCreate_uidDirtyFlag())
            this.setCreate_uid(srfdomain.getCreate_uid());
        if(srfdomain.getHr_responsible_idDirtyFlag())
            this.setHr_responsible_id(srfdomain.getHr_responsible_id());
        if(srfdomain.getDepartment_idDirtyFlag())
            this.setDepartment_id(srfdomain.getDepartment_id());
        if(srfdomain.getAlias_idDirtyFlag())
            this.setAlias_id(srfdomain.getAlias_id());
        if(srfdomain.getUser_idDirtyFlag())
            this.setUser_id(srfdomain.getUser_id());

    }

    public List<Hr_jobDTO> fromDOPage(List<Hr_job> poPage)   {
        if(poPage == null)
            return null;
        List<Hr_jobDTO> dtos=new ArrayList<Hr_jobDTO>();
        for(Hr_job domain : poPage) {
            Hr_jobDTO dto = new Hr_jobDTO();
            dto.fromDO(domain);
            dtos.add(dto);
        }
        return dtos;
    }
}

