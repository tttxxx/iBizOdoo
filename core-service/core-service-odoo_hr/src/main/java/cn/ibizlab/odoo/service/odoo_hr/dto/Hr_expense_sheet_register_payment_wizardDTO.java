package cn.ibizlab.odoo.service.odoo_hr.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.core.odoo_hr.valuerule.anno.hr_expense_sheet_register_payment_wizard.*;
import cn.ibizlab.odoo.core.odoo_hr.domain.Hr_expense_sheet_register_payment_wizard;
import org.springframework.cglib.beans.BeanCopier;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 服务DTO对象[Hr_expense_sheet_register_payment_wizardDTO]
 */
public class Hr_expense_sheet_register_payment_wizardDTO implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [AMOUNT]
     *
     */
    @Hr_expense_sheet_register_payment_wizardAmountDefault(info = "默认规则")
    private Double amount;

    @JsonIgnore
    private boolean amountDirtyFlag;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @Hr_expense_sheet_register_payment_wizard__last_updateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp __last_update;

    @JsonIgnore
    private boolean __last_updateDirtyFlag;

    /**
     * 属性 [COMMUNICATION]
     *
     */
    @Hr_expense_sheet_register_payment_wizardCommunicationDefault(info = "默认规则")
    private String communication;

    @JsonIgnore
    private boolean communicationDirtyFlag;

    /**
     * 属性 [HIDE_PAYMENT_METHOD]
     *
     */
    @Hr_expense_sheet_register_payment_wizardHide_payment_methodDefault(info = "默认规则")
    private String hide_payment_method;

    @JsonIgnore
    private boolean hide_payment_methodDirtyFlag;

    /**
     * 属性 [CREATE_DATE]
     *
     */
    @Hr_expense_sheet_register_payment_wizardCreate_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp create_date;

    @JsonIgnore
    private boolean create_dateDirtyFlag;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @Hr_expense_sheet_register_payment_wizardDisplay_nameDefault(info = "默认规则")
    private String display_name;

    @JsonIgnore
    private boolean display_nameDirtyFlag;

    /**
     * 属性 [SHOW_PARTNER_BANK_ACCOUNT]
     *
     */
    @Hr_expense_sheet_register_payment_wizardShow_partner_bank_accountDefault(info = "默认规则")
    private String show_partner_bank_account;

    @JsonIgnore
    private boolean show_partner_bank_accountDirtyFlag;

    /**
     * 属性 [PAYMENT_DATE]
     *
     */
    @Hr_expense_sheet_register_payment_wizardPayment_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    private Timestamp payment_date;

    @JsonIgnore
    private boolean payment_dateDirtyFlag;

    /**
     * 属性 [WRITE_DATE]
     *
     */
    @Hr_expense_sheet_register_payment_wizardWrite_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp write_date;

    @JsonIgnore
    private boolean write_dateDirtyFlag;

    /**
     * 属性 [ID]
     *
     */
    @Hr_expense_sheet_register_payment_wizardIdDefault(info = "默认规则")
    private Integer id;

    @JsonIgnore
    private boolean idDirtyFlag;

    /**
     * 属性 [JOURNAL_ID_TEXT]
     *
     */
    @Hr_expense_sheet_register_payment_wizardJournal_id_textDefault(info = "默认规则")
    private String journal_id_text;

    @JsonIgnore
    private boolean journal_id_textDirtyFlag;

    /**
     * 属性 [WRITE_UID_TEXT]
     *
     */
    @Hr_expense_sheet_register_payment_wizardWrite_uid_textDefault(info = "默认规则")
    private String write_uid_text;

    @JsonIgnore
    private boolean write_uid_textDirtyFlag;

    /**
     * 属性 [PARTNER_ID_TEXT]
     *
     */
    @Hr_expense_sheet_register_payment_wizardPartner_id_textDefault(info = "默认规则")
    private String partner_id_text;

    @JsonIgnore
    private boolean partner_id_textDirtyFlag;

    /**
     * 属性 [PAYMENT_METHOD_ID_TEXT]
     *
     */
    @Hr_expense_sheet_register_payment_wizardPayment_method_id_textDefault(info = "默认规则")
    private String payment_method_id_text;

    @JsonIgnore
    private boolean payment_method_id_textDirtyFlag;

    /**
     * 属性 [CREATE_UID_TEXT]
     *
     */
    @Hr_expense_sheet_register_payment_wizardCreate_uid_textDefault(info = "默认规则")
    private String create_uid_text;

    @JsonIgnore
    private boolean create_uid_textDirtyFlag;

    /**
     * 属性 [CURRENCY_ID_TEXT]
     *
     */
    @Hr_expense_sheet_register_payment_wizardCurrency_id_textDefault(info = "默认规则")
    private String currency_id_text;

    @JsonIgnore
    private boolean currency_id_textDirtyFlag;

    /**
     * 属性 [COMPANY_ID]
     *
     */
    @Hr_expense_sheet_register_payment_wizardCompany_idDefault(info = "默认规则")
    private Integer company_id;

    @JsonIgnore
    private boolean company_idDirtyFlag;

    /**
     * 属性 [PARTNER_ID]
     *
     */
    @Hr_expense_sheet_register_payment_wizardPartner_idDefault(info = "默认规则")
    private Integer partner_id;

    @JsonIgnore
    private boolean partner_idDirtyFlag;

    /**
     * 属性 [WRITE_UID]
     *
     */
    @Hr_expense_sheet_register_payment_wizardWrite_uidDefault(info = "默认规则")
    private Integer write_uid;

    @JsonIgnore
    private boolean write_uidDirtyFlag;

    /**
     * 属性 [PAYMENT_METHOD_ID]
     *
     */
    @Hr_expense_sheet_register_payment_wizardPayment_method_idDefault(info = "默认规则")
    private Integer payment_method_id;

    @JsonIgnore
    private boolean payment_method_idDirtyFlag;

    /**
     * 属性 [CREATE_UID]
     *
     */
    @Hr_expense_sheet_register_payment_wizardCreate_uidDefault(info = "默认规则")
    private Integer create_uid;

    @JsonIgnore
    private boolean create_uidDirtyFlag;

    /**
     * 属性 [PARTNER_BANK_ACCOUNT_ID]
     *
     */
    @Hr_expense_sheet_register_payment_wizardPartner_bank_account_idDefault(info = "默认规则")
    private Integer partner_bank_account_id;

    @JsonIgnore
    private boolean partner_bank_account_idDirtyFlag;

    /**
     * 属性 [CURRENCY_ID]
     *
     */
    @Hr_expense_sheet_register_payment_wizardCurrency_idDefault(info = "默认规则")
    private Integer currency_id;

    @JsonIgnore
    private boolean currency_idDirtyFlag;

    /**
     * 属性 [JOURNAL_ID]
     *
     */
    @Hr_expense_sheet_register_payment_wizardJournal_idDefault(info = "默认规则")
    private Integer journal_id;

    @JsonIgnore
    private boolean journal_idDirtyFlag;


    /**
     * 获取 [AMOUNT]
     */
    @JsonProperty("amount")
    public Double getAmount(){
        return amount ;
    }

    /**
     * 设置 [AMOUNT]
     */
    @JsonProperty("amount")
    public void setAmount(Double  amount){
        this.amount = amount ;
        this.amountDirtyFlag = true ;
    }

    /**
     * 获取 [AMOUNT]脏标记
     */
    @JsonIgnore
    public boolean getAmountDirtyFlag(){
        return amountDirtyFlag ;
    }

    /**
     * 获取 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return __last_update ;
    }

    /**
     * 设置 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

    /**
     * 获取 [__LAST_UPDATE]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return __last_updateDirtyFlag ;
    }

    /**
     * 获取 [COMMUNICATION]
     */
    @JsonProperty("communication")
    public String getCommunication(){
        return communication ;
    }

    /**
     * 设置 [COMMUNICATION]
     */
    @JsonProperty("communication")
    public void setCommunication(String  communication){
        this.communication = communication ;
        this.communicationDirtyFlag = true ;
    }

    /**
     * 获取 [COMMUNICATION]脏标记
     */
    @JsonIgnore
    public boolean getCommunicationDirtyFlag(){
        return communicationDirtyFlag ;
    }

    /**
     * 获取 [HIDE_PAYMENT_METHOD]
     */
    @JsonProperty("hide_payment_method")
    public String getHide_payment_method(){
        return hide_payment_method ;
    }

    /**
     * 设置 [HIDE_PAYMENT_METHOD]
     */
    @JsonProperty("hide_payment_method")
    public void setHide_payment_method(String  hide_payment_method){
        this.hide_payment_method = hide_payment_method ;
        this.hide_payment_methodDirtyFlag = true ;
    }

    /**
     * 获取 [HIDE_PAYMENT_METHOD]脏标记
     */
    @JsonIgnore
    public boolean getHide_payment_methodDirtyFlag(){
        return hide_payment_methodDirtyFlag ;
    }

    /**
     * 获取 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return create_date ;
    }

    /**
     * 设置 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return create_dateDirtyFlag ;
    }

    /**
     * 获取 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return display_name ;
    }

    /**
     * 设置 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

    /**
     * 获取 [DISPLAY_NAME]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return display_nameDirtyFlag ;
    }

    /**
     * 获取 [SHOW_PARTNER_BANK_ACCOUNT]
     */
    @JsonProperty("show_partner_bank_account")
    public String getShow_partner_bank_account(){
        return show_partner_bank_account ;
    }

    /**
     * 设置 [SHOW_PARTNER_BANK_ACCOUNT]
     */
    @JsonProperty("show_partner_bank_account")
    public void setShow_partner_bank_account(String  show_partner_bank_account){
        this.show_partner_bank_account = show_partner_bank_account ;
        this.show_partner_bank_accountDirtyFlag = true ;
    }

    /**
     * 获取 [SHOW_PARTNER_BANK_ACCOUNT]脏标记
     */
    @JsonIgnore
    public boolean getShow_partner_bank_accountDirtyFlag(){
        return show_partner_bank_accountDirtyFlag ;
    }

    /**
     * 获取 [PAYMENT_DATE]
     */
    @JsonProperty("payment_date")
    public Timestamp getPayment_date(){
        return payment_date ;
    }

    /**
     * 设置 [PAYMENT_DATE]
     */
    @JsonProperty("payment_date")
    public void setPayment_date(Timestamp  payment_date){
        this.payment_date = payment_date ;
        this.payment_dateDirtyFlag = true ;
    }

    /**
     * 获取 [PAYMENT_DATE]脏标记
     */
    @JsonIgnore
    public boolean getPayment_dateDirtyFlag(){
        return payment_dateDirtyFlag ;
    }

    /**
     * 获取 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return write_date ;
    }

    /**
     * 设置 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return write_dateDirtyFlag ;
    }

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

    /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return idDirtyFlag ;
    }

    /**
     * 获取 [JOURNAL_ID_TEXT]
     */
    @JsonProperty("journal_id_text")
    public String getJournal_id_text(){
        return journal_id_text ;
    }

    /**
     * 设置 [JOURNAL_ID_TEXT]
     */
    @JsonProperty("journal_id_text")
    public void setJournal_id_text(String  journal_id_text){
        this.journal_id_text = journal_id_text ;
        this.journal_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [JOURNAL_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getJournal_id_textDirtyFlag(){
        return journal_id_textDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return write_uid_text ;
    }

    /**
     * 设置 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return write_uid_textDirtyFlag ;
    }

    /**
     * 获取 [PARTNER_ID_TEXT]
     */
    @JsonProperty("partner_id_text")
    public String getPartner_id_text(){
        return partner_id_text ;
    }

    /**
     * 设置 [PARTNER_ID_TEXT]
     */
    @JsonProperty("partner_id_text")
    public void setPartner_id_text(String  partner_id_text){
        this.partner_id_text = partner_id_text ;
        this.partner_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [PARTNER_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getPartner_id_textDirtyFlag(){
        return partner_id_textDirtyFlag ;
    }

    /**
     * 获取 [PAYMENT_METHOD_ID_TEXT]
     */
    @JsonProperty("payment_method_id_text")
    public String getPayment_method_id_text(){
        return payment_method_id_text ;
    }

    /**
     * 设置 [PAYMENT_METHOD_ID_TEXT]
     */
    @JsonProperty("payment_method_id_text")
    public void setPayment_method_id_text(String  payment_method_id_text){
        this.payment_method_id_text = payment_method_id_text ;
        this.payment_method_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [PAYMENT_METHOD_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getPayment_method_id_textDirtyFlag(){
        return payment_method_id_textDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return create_uid_text ;
    }

    /**
     * 设置 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return create_uid_textDirtyFlag ;
    }

    /**
     * 获取 [CURRENCY_ID_TEXT]
     */
    @JsonProperty("currency_id_text")
    public String getCurrency_id_text(){
        return currency_id_text ;
    }

    /**
     * 设置 [CURRENCY_ID_TEXT]
     */
    @JsonProperty("currency_id_text")
    public void setCurrency_id_text(String  currency_id_text){
        this.currency_id_text = currency_id_text ;
        this.currency_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [CURRENCY_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getCurrency_id_textDirtyFlag(){
        return currency_id_textDirtyFlag ;
    }

    /**
     * 获取 [COMPANY_ID]
     */
    @JsonProperty("company_id")
    public Integer getCompany_id(){
        return company_id ;
    }

    /**
     * 设置 [COMPANY_ID]
     */
    @JsonProperty("company_id")
    public void setCompany_id(Integer  company_id){
        this.company_id = company_id ;
        this.company_idDirtyFlag = true ;
    }

    /**
     * 获取 [COMPANY_ID]脏标记
     */
    @JsonIgnore
    public boolean getCompany_idDirtyFlag(){
        return company_idDirtyFlag ;
    }

    /**
     * 获取 [PARTNER_ID]
     */
    @JsonProperty("partner_id")
    public Integer getPartner_id(){
        return partner_id ;
    }

    /**
     * 设置 [PARTNER_ID]
     */
    @JsonProperty("partner_id")
    public void setPartner_id(Integer  partner_id){
        this.partner_id = partner_id ;
        this.partner_idDirtyFlag = true ;
    }

    /**
     * 获取 [PARTNER_ID]脏标记
     */
    @JsonIgnore
    public boolean getPartner_idDirtyFlag(){
        return partner_idDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return write_uid ;
    }

    /**
     * 设置 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return write_uidDirtyFlag ;
    }

    /**
     * 获取 [PAYMENT_METHOD_ID]
     */
    @JsonProperty("payment_method_id")
    public Integer getPayment_method_id(){
        return payment_method_id ;
    }

    /**
     * 设置 [PAYMENT_METHOD_ID]
     */
    @JsonProperty("payment_method_id")
    public void setPayment_method_id(Integer  payment_method_id){
        this.payment_method_id = payment_method_id ;
        this.payment_method_idDirtyFlag = true ;
    }

    /**
     * 获取 [PAYMENT_METHOD_ID]脏标记
     */
    @JsonIgnore
    public boolean getPayment_method_idDirtyFlag(){
        return payment_method_idDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return create_uid ;
    }

    /**
     * 设置 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return create_uidDirtyFlag ;
    }

    /**
     * 获取 [PARTNER_BANK_ACCOUNT_ID]
     */
    @JsonProperty("partner_bank_account_id")
    public Integer getPartner_bank_account_id(){
        return partner_bank_account_id ;
    }

    /**
     * 设置 [PARTNER_BANK_ACCOUNT_ID]
     */
    @JsonProperty("partner_bank_account_id")
    public void setPartner_bank_account_id(Integer  partner_bank_account_id){
        this.partner_bank_account_id = partner_bank_account_id ;
        this.partner_bank_account_idDirtyFlag = true ;
    }

    /**
     * 获取 [PARTNER_BANK_ACCOUNT_ID]脏标记
     */
    @JsonIgnore
    public boolean getPartner_bank_account_idDirtyFlag(){
        return partner_bank_account_idDirtyFlag ;
    }

    /**
     * 获取 [CURRENCY_ID]
     */
    @JsonProperty("currency_id")
    public Integer getCurrency_id(){
        return currency_id ;
    }

    /**
     * 设置 [CURRENCY_ID]
     */
    @JsonProperty("currency_id")
    public void setCurrency_id(Integer  currency_id){
        this.currency_id = currency_id ;
        this.currency_idDirtyFlag = true ;
    }

    /**
     * 获取 [CURRENCY_ID]脏标记
     */
    @JsonIgnore
    public boolean getCurrency_idDirtyFlag(){
        return currency_idDirtyFlag ;
    }

    /**
     * 获取 [JOURNAL_ID]
     */
    @JsonProperty("journal_id")
    public Integer getJournal_id(){
        return journal_id ;
    }

    /**
     * 设置 [JOURNAL_ID]
     */
    @JsonProperty("journal_id")
    public void setJournal_id(Integer  journal_id){
        this.journal_id = journal_id ;
        this.journal_idDirtyFlag = true ;
    }

    /**
     * 获取 [JOURNAL_ID]脏标记
     */
    @JsonIgnore
    public boolean getJournal_idDirtyFlag(){
        return journal_idDirtyFlag ;
    }



    public Hr_expense_sheet_register_payment_wizard toDO() {
        Hr_expense_sheet_register_payment_wizard srfdomain = new Hr_expense_sheet_register_payment_wizard();
        if(getAmountDirtyFlag())
            srfdomain.setAmount(amount);
        if(get__last_updateDirtyFlag())
            srfdomain.set__last_update(__last_update);
        if(getCommunicationDirtyFlag())
            srfdomain.setCommunication(communication);
        if(getHide_payment_methodDirtyFlag())
            srfdomain.setHide_payment_method(hide_payment_method);
        if(getCreate_dateDirtyFlag())
            srfdomain.setCreate_date(create_date);
        if(getDisplay_nameDirtyFlag())
            srfdomain.setDisplay_name(display_name);
        if(getShow_partner_bank_accountDirtyFlag())
            srfdomain.setShow_partner_bank_account(show_partner_bank_account);
        if(getPayment_dateDirtyFlag())
            srfdomain.setPayment_date(payment_date);
        if(getWrite_dateDirtyFlag())
            srfdomain.setWrite_date(write_date);
        if(getIdDirtyFlag())
            srfdomain.setId(id);
        if(getJournal_id_textDirtyFlag())
            srfdomain.setJournal_id_text(journal_id_text);
        if(getWrite_uid_textDirtyFlag())
            srfdomain.setWrite_uid_text(write_uid_text);
        if(getPartner_id_textDirtyFlag())
            srfdomain.setPartner_id_text(partner_id_text);
        if(getPayment_method_id_textDirtyFlag())
            srfdomain.setPayment_method_id_text(payment_method_id_text);
        if(getCreate_uid_textDirtyFlag())
            srfdomain.setCreate_uid_text(create_uid_text);
        if(getCurrency_id_textDirtyFlag())
            srfdomain.setCurrency_id_text(currency_id_text);
        if(getCompany_idDirtyFlag())
            srfdomain.setCompany_id(company_id);
        if(getPartner_idDirtyFlag())
            srfdomain.setPartner_id(partner_id);
        if(getWrite_uidDirtyFlag())
            srfdomain.setWrite_uid(write_uid);
        if(getPayment_method_idDirtyFlag())
            srfdomain.setPayment_method_id(payment_method_id);
        if(getCreate_uidDirtyFlag())
            srfdomain.setCreate_uid(create_uid);
        if(getPartner_bank_account_idDirtyFlag())
            srfdomain.setPartner_bank_account_id(partner_bank_account_id);
        if(getCurrency_idDirtyFlag())
            srfdomain.setCurrency_id(currency_id);
        if(getJournal_idDirtyFlag())
            srfdomain.setJournal_id(journal_id);

        return srfdomain;
    }

    public void fromDO(Hr_expense_sheet_register_payment_wizard srfdomain) {
        if(srfdomain == null )
          return ;
        if(srfdomain.getAmountDirtyFlag())
            this.setAmount(srfdomain.getAmount());
        if(srfdomain.get__last_updateDirtyFlag())
            this.set__last_update(srfdomain.get__last_update());
        if(srfdomain.getCommunicationDirtyFlag())
            this.setCommunication(srfdomain.getCommunication());
        if(srfdomain.getHide_payment_methodDirtyFlag())
            this.setHide_payment_method(srfdomain.getHide_payment_method());
        if(srfdomain.getCreate_dateDirtyFlag())
            this.setCreate_date(srfdomain.getCreate_date());
        if(srfdomain.getDisplay_nameDirtyFlag())
            this.setDisplay_name(srfdomain.getDisplay_name());
        if(srfdomain.getShow_partner_bank_accountDirtyFlag())
            this.setShow_partner_bank_account(srfdomain.getShow_partner_bank_account());
        if(srfdomain.getPayment_dateDirtyFlag())
            this.setPayment_date(srfdomain.getPayment_date());
        if(srfdomain.getWrite_dateDirtyFlag())
            this.setWrite_date(srfdomain.getWrite_date());
        if(srfdomain.getIdDirtyFlag())
            this.setId(srfdomain.getId());
        if(srfdomain.getJournal_id_textDirtyFlag())
            this.setJournal_id_text(srfdomain.getJournal_id_text());
        if(srfdomain.getWrite_uid_textDirtyFlag())
            this.setWrite_uid_text(srfdomain.getWrite_uid_text());
        if(srfdomain.getPartner_id_textDirtyFlag())
            this.setPartner_id_text(srfdomain.getPartner_id_text());
        if(srfdomain.getPayment_method_id_textDirtyFlag())
            this.setPayment_method_id_text(srfdomain.getPayment_method_id_text());
        if(srfdomain.getCreate_uid_textDirtyFlag())
            this.setCreate_uid_text(srfdomain.getCreate_uid_text());
        if(srfdomain.getCurrency_id_textDirtyFlag())
            this.setCurrency_id_text(srfdomain.getCurrency_id_text());
        if(srfdomain.getCompany_idDirtyFlag())
            this.setCompany_id(srfdomain.getCompany_id());
        if(srfdomain.getPartner_idDirtyFlag())
            this.setPartner_id(srfdomain.getPartner_id());
        if(srfdomain.getWrite_uidDirtyFlag())
            this.setWrite_uid(srfdomain.getWrite_uid());
        if(srfdomain.getPayment_method_idDirtyFlag())
            this.setPayment_method_id(srfdomain.getPayment_method_id());
        if(srfdomain.getCreate_uidDirtyFlag())
            this.setCreate_uid(srfdomain.getCreate_uid());
        if(srfdomain.getPartner_bank_account_idDirtyFlag())
            this.setPartner_bank_account_id(srfdomain.getPartner_bank_account_id());
        if(srfdomain.getCurrency_idDirtyFlag())
            this.setCurrency_id(srfdomain.getCurrency_id());
        if(srfdomain.getJournal_idDirtyFlag())
            this.setJournal_id(srfdomain.getJournal_id());

    }

    public List<Hr_expense_sheet_register_payment_wizardDTO> fromDOPage(List<Hr_expense_sheet_register_payment_wizard> poPage)   {
        if(poPage == null)
            return null;
        List<Hr_expense_sheet_register_payment_wizardDTO> dtos=new ArrayList<Hr_expense_sheet_register_payment_wizardDTO>();
        for(Hr_expense_sheet_register_payment_wizard domain : poPage) {
            Hr_expense_sheet_register_payment_wizardDTO dto = new Hr_expense_sheet_register_payment_wizardDTO();
            dto.fromDO(domain);
            dtos.add(dto);
        }
        return dtos;
    }
}

