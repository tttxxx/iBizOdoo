package cn.ibizlab.odoo.service.odoo_hr.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_hr.dto.Hr_departmentDTO;
import cn.ibizlab.odoo.core.odoo_hr.domain.Hr_department;
import cn.ibizlab.odoo.core.odoo_hr.service.IHr_departmentService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_hr.filter.Hr_departmentSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Hr_department" })
@RestController
@RequestMapping("")
public class Hr_departmentResource {

    @Autowired
    private IHr_departmentService hr_departmentService;

    public IHr_departmentService getHr_departmentService() {
        return this.hr_departmentService;
    }

    @ApiOperation(value = "更新数据", tags = {"Hr_department" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_hr/hr_departments/{hr_department_id}")

    public ResponseEntity<Hr_departmentDTO> update(@PathVariable("hr_department_id") Integer hr_department_id, @RequestBody Hr_departmentDTO hr_departmentdto) {
		Hr_department domain = hr_departmentdto.toDO();
        domain.setId(hr_department_id);
		hr_departmentService.update(domain);
		Hr_departmentDTO dto = new Hr_departmentDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批建立数据", tags = {"Hr_department" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_hr/hr_departments/createBatch")
    public ResponseEntity<Boolean> createBatchHr_department(@RequestBody List<Hr_departmentDTO> hr_departmentdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "批更新数据", tags = {"Hr_department" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_hr/hr_departments/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Hr_departmentDTO> hr_departmentdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "批删除数据", tags = {"Hr_department" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_hr/hr_departments/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Hr_departmentDTO> hr_departmentdtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "建立数据", tags = {"Hr_department" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_hr/hr_departments")

    public ResponseEntity<Hr_departmentDTO> create(@RequestBody Hr_departmentDTO hr_departmentdto) {
        Hr_departmentDTO dto = new Hr_departmentDTO();
        Hr_department domain = hr_departmentdto.toDO();
		hr_departmentService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "删除数据", tags = {"Hr_department" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_hr/hr_departments/{hr_department_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("hr_department_id") Integer hr_department_id) {
        Hr_departmentDTO hr_departmentdto = new Hr_departmentDTO();
		Hr_department domain = new Hr_department();
		hr_departmentdto.setId(hr_department_id);
		domain.setId(hr_department_id);
        Boolean rst = hr_departmentService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "获取数据", tags = {"Hr_department" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_hr/hr_departments/{hr_department_id}")
    public ResponseEntity<Hr_departmentDTO> get(@PathVariable("hr_department_id") Integer hr_department_id) {
        Hr_departmentDTO dto = new Hr_departmentDTO();
        Hr_department domain = hr_departmentService.get(hr_department_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Hr_department" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_hr/hr_departments/fetchdefault")
	public ResponseEntity<Page<Hr_departmentDTO>> fetchDefault(Hr_departmentSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Hr_departmentDTO> list = new ArrayList<Hr_departmentDTO>();
        
        Page<Hr_department> domains = hr_departmentService.searchDefault(context) ;
        for(Hr_department hr_department : domains.getContent()){
            Hr_departmentDTO dto = new Hr_departmentDTO();
            dto.fromDO(hr_department);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
