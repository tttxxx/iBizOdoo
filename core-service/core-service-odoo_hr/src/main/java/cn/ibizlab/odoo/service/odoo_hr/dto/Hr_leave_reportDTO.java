package cn.ibizlab.odoo.service.odoo_hr.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.core.odoo_hr.valuerule.anno.hr_leave_report.*;
import cn.ibizlab.odoo.core.odoo_hr.domain.Hr_leave_report;
import org.springframework.cglib.beans.BeanCopier;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 服务DTO对象[Hr_leave_reportDTO]
 */
public class Hr_leave_reportDTO implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [DATE_TO]
     *
     */
    @Hr_leave_reportDate_toDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp date_to;

    @JsonIgnore
    private boolean date_toDirtyFlag;

    /**
     * 属性 [TYPE]
     *
     */
    @Hr_leave_reportTypeDefault(info = "默认规则")
    private String type;

    @JsonIgnore
    private boolean typeDirtyFlag;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @Hr_leave_report__last_updateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp __last_update;

    @JsonIgnore
    private boolean __last_updateDirtyFlag;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @Hr_leave_reportDisplay_nameDefault(info = "默认规则")
    private String display_name;

    @JsonIgnore
    private boolean display_nameDirtyFlag;

    /**
     * 属性 [NUMBER_OF_DAYS]
     *
     */
    @Hr_leave_reportNumber_of_daysDefault(info = "默认规则")
    private Double number_of_days;

    @JsonIgnore
    private boolean number_of_daysDirtyFlag;

    /**
     * 属性 [PAYSLIP_STATUS]
     *
     */
    @Hr_leave_reportPayslip_statusDefault(info = "默认规则")
    private String payslip_status;

    @JsonIgnore
    private boolean payslip_statusDirtyFlag;

    /**
     * 属性 [NAME]
     *
     */
    @Hr_leave_reportNameDefault(info = "默认规则")
    private String name;

    @JsonIgnore
    private boolean nameDirtyFlag;

    /**
     * 属性 [ID]
     *
     */
    @Hr_leave_reportIdDefault(info = "默认规则")
    private Integer id;

    @JsonIgnore
    private boolean idDirtyFlag;

    /**
     * 属性 [STATE]
     *
     */
    @Hr_leave_reportStateDefault(info = "默认规则")
    private String state;

    @JsonIgnore
    private boolean stateDirtyFlag;

    /**
     * 属性 [DATE_FROM]
     *
     */
    @Hr_leave_reportDate_fromDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp date_from;

    @JsonIgnore
    private boolean date_fromDirtyFlag;

    /**
     * 属性 [HOLIDAY_TYPE]
     *
     */
    @Hr_leave_reportHoliday_typeDefault(info = "默认规则")
    private String holiday_type;

    @JsonIgnore
    private boolean holiday_typeDirtyFlag;

    /**
     * 属性 [HOLIDAY_STATUS_ID_TEXT]
     *
     */
    @Hr_leave_reportHoliday_status_id_textDefault(info = "默认规则")
    private String holiday_status_id_text;

    @JsonIgnore
    private boolean holiday_status_id_textDirtyFlag;

    /**
     * 属性 [CATEGORY_ID_TEXT]
     *
     */
    @Hr_leave_reportCategory_id_textDefault(info = "默认规则")
    private String category_id_text;

    @JsonIgnore
    private boolean category_id_textDirtyFlag;

    /**
     * 属性 [EMPLOYEE_ID_TEXT]
     *
     */
    @Hr_leave_reportEmployee_id_textDefault(info = "默认规则")
    private String employee_id_text;

    @JsonIgnore
    private boolean employee_id_textDirtyFlag;

    /**
     * 属性 [DEPARTMENT_ID_TEXT]
     *
     */
    @Hr_leave_reportDepartment_id_textDefault(info = "默认规则")
    private String department_id_text;

    @JsonIgnore
    private boolean department_id_textDirtyFlag;

    /**
     * 属性 [CATEGORY_ID]
     *
     */
    @Hr_leave_reportCategory_idDefault(info = "默认规则")
    private Integer category_id;

    @JsonIgnore
    private boolean category_idDirtyFlag;

    /**
     * 属性 [EMPLOYEE_ID]
     *
     */
    @Hr_leave_reportEmployee_idDefault(info = "默认规则")
    private Integer employee_id;

    @JsonIgnore
    private boolean employee_idDirtyFlag;

    /**
     * 属性 [HOLIDAY_STATUS_ID]
     *
     */
    @Hr_leave_reportHoliday_status_idDefault(info = "默认规则")
    private Integer holiday_status_id;

    @JsonIgnore
    private boolean holiday_status_idDirtyFlag;

    /**
     * 属性 [DEPARTMENT_ID]
     *
     */
    @Hr_leave_reportDepartment_idDefault(info = "默认规则")
    private Integer department_id;

    @JsonIgnore
    private boolean department_idDirtyFlag;


    /**
     * 获取 [DATE_TO]
     */
    @JsonProperty("date_to")
    public Timestamp getDate_to(){
        return date_to ;
    }

    /**
     * 设置 [DATE_TO]
     */
    @JsonProperty("date_to")
    public void setDate_to(Timestamp  date_to){
        this.date_to = date_to ;
        this.date_toDirtyFlag = true ;
    }

    /**
     * 获取 [DATE_TO]脏标记
     */
    @JsonIgnore
    public boolean getDate_toDirtyFlag(){
        return date_toDirtyFlag ;
    }

    /**
     * 获取 [TYPE]
     */
    @JsonProperty("type")
    public String getType(){
        return type ;
    }

    /**
     * 设置 [TYPE]
     */
    @JsonProperty("type")
    public void setType(String  type){
        this.type = type ;
        this.typeDirtyFlag = true ;
    }

    /**
     * 获取 [TYPE]脏标记
     */
    @JsonIgnore
    public boolean getTypeDirtyFlag(){
        return typeDirtyFlag ;
    }

    /**
     * 获取 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return __last_update ;
    }

    /**
     * 设置 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

    /**
     * 获取 [__LAST_UPDATE]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return __last_updateDirtyFlag ;
    }

    /**
     * 获取 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return display_name ;
    }

    /**
     * 设置 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

    /**
     * 获取 [DISPLAY_NAME]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return display_nameDirtyFlag ;
    }

    /**
     * 获取 [NUMBER_OF_DAYS]
     */
    @JsonProperty("number_of_days")
    public Double getNumber_of_days(){
        return number_of_days ;
    }

    /**
     * 设置 [NUMBER_OF_DAYS]
     */
    @JsonProperty("number_of_days")
    public void setNumber_of_days(Double  number_of_days){
        this.number_of_days = number_of_days ;
        this.number_of_daysDirtyFlag = true ;
    }

    /**
     * 获取 [NUMBER_OF_DAYS]脏标记
     */
    @JsonIgnore
    public boolean getNumber_of_daysDirtyFlag(){
        return number_of_daysDirtyFlag ;
    }

    /**
     * 获取 [PAYSLIP_STATUS]
     */
    @JsonProperty("payslip_status")
    public String getPayslip_status(){
        return payslip_status ;
    }

    /**
     * 设置 [PAYSLIP_STATUS]
     */
    @JsonProperty("payslip_status")
    public void setPayslip_status(String  payslip_status){
        this.payslip_status = payslip_status ;
        this.payslip_statusDirtyFlag = true ;
    }

    /**
     * 获取 [PAYSLIP_STATUS]脏标记
     */
    @JsonIgnore
    public boolean getPayslip_statusDirtyFlag(){
        return payslip_statusDirtyFlag ;
    }

    /**
     * 获取 [NAME]
     */
    @JsonProperty("name")
    public String getName(){
        return name ;
    }

    /**
     * 设置 [NAME]
     */
    @JsonProperty("name")
    public void setName(String  name){
        this.name = name ;
        this.nameDirtyFlag = true ;
    }

    /**
     * 获取 [NAME]脏标记
     */
    @JsonIgnore
    public boolean getNameDirtyFlag(){
        return nameDirtyFlag ;
    }

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

    /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return idDirtyFlag ;
    }

    /**
     * 获取 [STATE]
     */
    @JsonProperty("state")
    public String getState(){
        return state ;
    }

    /**
     * 设置 [STATE]
     */
    @JsonProperty("state")
    public void setState(String  state){
        this.state = state ;
        this.stateDirtyFlag = true ;
    }

    /**
     * 获取 [STATE]脏标记
     */
    @JsonIgnore
    public boolean getStateDirtyFlag(){
        return stateDirtyFlag ;
    }

    /**
     * 获取 [DATE_FROM]
     */
    @JsonProperty("date_from")
    public Timestamp getDate_from(){
        return date_from ;
    }

    /**
     * 设置 [DATE_FROM]
     */
    @JsonProperty("date_from")
    public void setDate_from(Timestamp  date_from){
        this.date_from = date_from ;
        this.date_fromDirtyFlag = true ;
    }

    /**
     * 获取 [DATE_FROM]脏标记
     */
    @JsonIgnore
    public boolean getDate_fromDirtyFlag(){
        return date_fromDirtyFlag ;
    }

    /**
     * 获取 [HOLIDAY_TYPE]
     */
    @JsonProperty("holiday_type")
    public String getHoliday_type(){
        return holiday_type ;
    }

    /**
     * 设置 [HOLIDAY_TYPE]
     */
    @JsonProperty("holiday_type")
    public void setHoliday_type(String  holiday_type){
        this.holiday_type = holiday_type ;
        this.holiday_typeDirtyFlag = true ;
    }

    /**
     * 获取 [HOLIDAY_TYPE]脏标记
     */
    @JsonIgnore
    public boolean getHoliday_typeDirtyFlag(){
        return holiday_typeDirtyFlag ;
    }

    /**
     * 获取 [HOLIDAY_STATUS_ID_TEXT]
     */
    @JsonProperty("holiday_status_id_text")
    public String getHoliday_status_id_text(){
        return holiday_status_id_text ;
    }

    /**
     * 设置 [HOLIDAY_STATUS_ID_TEXT]
     */
    @JsonProperty("holiday_status_id_text")
    public void setHoliday_status_id_text(String  holiday_status_id_text){
        this.holiday_status_id_text = holiday_status_id_text ;
        this.holiday_status_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [HOLIDAY_STATUS_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getHoliday_status_id_textDirtyFlag(){
        return holiday_status_id_textDirtyFlag ;
    }

    /**
     * 获取 [CATEGORY_ID_TEXT]
     */
    @JsonProperty("category_id_text")
    public String getCategory_id_text(){
        return category_id_text ;
    }

    /**
     * 设置 [CATEGORY_ID_TEXT]
     */
    @JsonProperty("category_id_text")
    public void setCategory_id_text(String  category_id_text){
        this.category_id_text = category_id_text ;
        this.category_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [CATEGORY_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getCategory_id_textDirtyFlag(){
        return category_id_textDirtyFlag ;
    }

    /**
     * 获取 [EMPLOYEE_ID_TEXT]
     */
    @JsonProperty("employee_id_text")
    public String getEmployee_id_text(){
        return employee_id_text ;
    }

    /**
     * 设置 [EMPLOYEE_ID_TEXT]
     */
    @JsonProperty("employee_id_text")
    public void setEmployee_id_text(String  employee_id_text){
        this.employee_id_text = employee_id_text ;
        this.employee_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [EMPLOYEE_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getEmployee_id_textDirtyFlag(){
        return employee_id_textDirtyFlag ;
    }

    /**
     * 获取 [DEPARTMENT_ID_TEXT]
     */
    @JsonProperty("department_id_text")
    public String getDepartment_id_text(){
        return department_id_text ;
    }

    /**
     * 设置 [DEPARTMENT_ID_TEXT]
     */
    @JsonProperty("department_id_text")
    public void setDepartment_id_text(String  department_id_text){
        this.department_id_text = department_id_text ;
        this.department_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [DEPARTMENT_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getDepartment_id_textDirtyFlag(){
        return department_id_textDirtyFlag ;
    }

    /**
     * 获取 [CATEGORY_ID]
     */
    @JsonProperty("category_id")
    public Integer getCategory_id(){
        return category_id ;
    }

    /**
     * 设置 [CATEGORY_ID]
     */
    @JsonProperty("category_id")
    public void setCategory_id(Integer  category_id){
        this.category_id = category_id ;
        this.category_idDirtyFlag = true ;
    }

    /**
     * 获取 [CATEGORY_ID]脏标记
     */
    @JsonIgnore
    public boolean getCategory_idDirtyFlag(){
        return category_idDirtyFlag ;
    }

    /**
     * 获取 [EMPLOYEE_ID]
     */
    @JsonProperty("employee_id")
    public Integer getEmployee_id(){
        return employee_id ;
    }

    /**
     * 设置 [EMPLOYEE_ID]
     */
    @JsonProperty("employee_id")
    public void setEmployee_id(Integer  employee_id){
        this.employee_id = employee_id ;
        this.employee_idDirtyFlag = true ;
    }

    /**
     * 获取 [EMPLOYEE_ID]脏标记
     */
    @JsonIgnore
    public boolean getEmployee_idDirtyFlag(){
        return employee_idDirtyFlag ;
    }

    /**
     * 获取 [HOLIDAY_STATUS_ID]
     */
    @JsonProperty("holiday_status_id")
    public Integer getHoliday_status_id(){
        return holiday_status_id ;
    }

    /**
     * 设置 [HOLIDAY_STATUS_ID]
     */
    @JsonProperty("holiday_status_id")
    public void setHoliday_status_id(Integer  holiday_status_id){
        this.holiday_status_id = holiday_status_id ;
        this.holiday_status_idDirtyFlag = true ;
    }

    /**
     * 获取 [HOLIDAY_STATUS_ID]脏标记
     */
    @JsonIgnore
    public boolean getHoliday_status_idDirtyFlag(){
        return holiday_status_idDirtyFlag ;
    }

    /**
     * 获取 [DEPARTMENT_ID]
     */
    @JsonProperty("department_id")
    public Integer getDepartment_id(){
        return department_id ;
    }

    /**
     * 设置 [DEPARTMENT_ID]
     */
    @JsonProperty("department_id")
    public void setDepartment_id(Integer  department_id){
        this.department_id = department_id ;
        this.department_idDirtyFlag = true ;
    }

    /**
     * 获取 [DEPARTMENT_ID]脏标记
     */
    @JsonIgnore
    public boolean getDepartment_idDirtyFlag(){
        return department_idDirtyFlag ;
    }



    public Hr_leave_report toDO() {
        Hr_leave_report srfdomain = new Hr_leave_report();
        if(getDate_toDirtyFlag())
            srfdomain.setDate_to(date_to);
        if(getTypeDirtyFlag())
            srfdomain.setType(type);
        if(get__last_updateDirtyFlag())
            srfdomain.set__last_update(__last_update);
        if(getDisplay_nameDirtyFlag())
            srfdomain.setDisplay_name(display_name);
        if(getNumber_of_daysDirtyFlag())
            srfdomain.setNumber_of_days(number_of_days);
        if(getPayslip_statusDirtyFlag())
            srfdomain.setPayslip_status(payslip_status);
        if(getNameDirtyFlag())
            srfdomain.setName(name);
        if(getIdDirtyFlag())
            srfdomain.setId(id);
        if(getStateDirtyFlag())
            srfdomain.setState(state);
        if(getDate_fromDirtyFlag())
            srfdomain.setDate_from(date_from);
        if(getHoliday_typeDirtyFlag())
            srfdomain.setHoliday_type(holiday_type);
        if(getHoliday_status_id_textDirtyFlag())
            srfdomain.setHoliday_status_id_text(holiday_status_id_text);
        if(getCategory_id_textDirtyFlag())
            srfdomain.setCategory_id_text(category_id_text);
        if(getEmployee_id_textDirtyFlag())
            srfdomain.setEmployee_id_text(employee_id_text);
        if(getDepartment_id_textDirtyFlag())
            srfdomain.setDepartment_id_text(department_id_text);
        if(getCategory_idDirtyFlag())
            srfdomain.setCategory_id(category_id);
        if(getEmployee_idDirtyFlag())
            srfdomain.setEmployee_id(employee_id);
        if(getHoliday_status_idDirtyFlag())
            srfdomain.setHoliday_status_id(holiday_status_id);
        if(getDepartment_idDirtyFlag())
            srfdomain.setDepartment_id(department_id);

        return srfdomain;
    }

    public void fromDO(Hr_leave_report srfdomain) {
        if(srfdomain == null )
          return ;
        if(srfdomain.getDate_toDirtyFlag())
            this.setDate_to(srfdomain.getDate_to());
        if(srfdomain.getTypeDirtyFlag())
            this.setType(srfdomain.getType());
        if(srfdomain.get__last_updateDirtyFlag())
            this.set__last_update(srfdomain.get__last_update());
        if(srfdomain.getDisplay_nameDirtyFlag())
            this.setDisplay_name(srfdomain.getDisplay_name());
        if(srfdomain.getNumber_of_daysDirtyFlag())
            this.setNumber_of_days(srfdomain.getNumber_of_days());
        if(srfdomain.getPayslip_statusDirtyFlag())
            this.setPayslip_status(srfdomain.getPayslip_status());
        if(srfdomain.getNameDirtyFlag())
            this.setName(srfdomain.getName());
        if(srfdomain.getIdDirtyFlag())
            this.setId(srfdomain.getId());
        if(srfdomain.getStateDirtyFlag())
            this.setState(srfdomain.getState());
        if(srfdomain.getDate_fromDirtyFlag())
            this.setDate_from(srfdomain.getDate_from());
        if(srfdomain.getHoliday_typeDirtyFlag())
            this.setHoliday_type(srfdomain.getHoliday_type());
        if(srfdomain.getHoliday_status_id_textDirtyFlag())
            this.setHoliday_status_id_text(srfdomain.getHoliday_status_id_text());
        if(srfdomain.getCategory_id_textDirtyFlag())
            this.setCategory_id_text(srfdomain.getCategory_id_text());
        if(srfdomain.getEmployee_id_textDirtyFlag())
            this.setEmployee_id_text(srfdomain.getEmployee_id_text());
        if(srfdomain.getDepartment_id_textDirtyFlag())
            this.setDepartment_id_text(srfdomain.getDepartment_id_text());
        if(srfdomain.getCategory_idDirtyFlag())
            this.setCategory_id(srfdomain.getCategory_id());
        if(srfdomain.getEmployee_idDirtyFlag())
            this.setEmployee_id(srfdomain.getEmployee_id());
        if(srfdomain.getHoliday_status_idDirtyFlag())
            this.setHoliday_status_id(srfdomain.getHoliday_status_id());
        if(srfdomain.getDepartment_idDirtyFlag())
            this.setDepartment_id(srfdomain.getDepartment_id());

    }

    public List<Hr_leave_reportDTO> fromDOPage(List<Hr_leave_report> poPage)   {
        if(poPage == null)
            return null;
        List<Hr_leave_reportDTO> dtos=new ArrayList<Hr_leave_reportDTO>();
        for(Hr_leave_report domain : poPage) {
            Hr_leave_reportDTO dto = new Hr_leave_reportDTO();
            dto.fromDO(domain);
            dtos.add(dto);
        }
        return dtos;
    }
}

