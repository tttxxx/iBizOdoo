package cn.ibizlab.odoo.service.odoo_hr.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_hr.dto.Hr_leave_reportDTO;
import cn.ibizlab.odoo.core.odoo_hr.domain.Hr_leave_report;
import cn.ibizlab.odoo.core.odoo_hr.service.IHr_leave_reportService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_hr.filter.Hr_leave_reportSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Hr_leave_report" })
@RestController
@RequestMapping("")
public class Hr_leave_reportResource {

    @Autowired
    private IHr_leave_reportService hr_leave_reportService;

    public IHr_leave_reportService getHr_leave_reportService() {
        return this.hr_leave_reportService;
    }

    @ApiOperation(value = "批更新数据", tags = {"Hr_leave_report" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_hr/hr_leave_reports/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Hr_leave_reportDTO> hr_leave_reportdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "批删除数据", tags = {"Hr_leave_report" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_hr/hr_leave_reports/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Hr_leave_reportDTO> hr_leave_reportdtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "获取数据", tags = {"Hr_leave_report" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_hr/hr_leave_reports/{hr_leave_report_id}")
    public ResponseEntity<Hr_leave_reportDTO> get(@PathVariable("hr_leave_report_id") Integer hr_leave_report_id) {
        Hr_leave_reportDTO dto = new Hr_leave_reportDTO();
        Hr_leave_report domain = hr_leave_reportService.get(hr_leave_report_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "建立数据", tags = {"Hr_leave_report" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_hr/hr_leave_reports")

    public ResponseEntity<Hr_leave_reportDTO> create(@RequestBody Hr_leave_reportDTO hr_leave_reportdto) {
        Hr_leave_reportDTO dto = new Hr_leave_reportDTO();
        Hr_leave_report domain = hr_leave_reportdto.toDO();
		hr_leave_reportService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "更新数据", tags = {"Hr_leave_report" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_hr/hr_leave_reports/{hr_leave_report_id}")

    public ResponseEntity<Hr_leave_reportDTO> update(@PathVariable("hr_leave_report_id") Integer hr_leave_report_id, @RequestBody Hr_leave_reportDTO hr_leave_reportdto) {
		Hr_leave_report domain = hr_leave_reportdto.toDO();
        domain.setId(hr_leave_report_id);
		hr_leave_reportService.update(domain);
		Hr_leave_reportDTO dto = new Hr_leave_reportDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批建立数据", tags = {"Hr_leave_report" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_hr/hr_leave_reports/createBatch")
    public ResponseEntity<Boolean> createBatchHr_leave_report(@RequestBody List<Hr_leave_reportDTO> hr_leave_reportdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "删除数据", tags = {"Hr_leave_report" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_hr/hr_leave_reports/{hr_leave_report_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("hr_leave_report_id") Integer hr_leave_report_id) {
        Hr_leave_reportDTO hr_leave_reportdto = new Hr_leave_reportDTO();
		Hr_leave_report domain = new Hr_leave_report();
		hr_leave_reportdto.setId(hr_leave_report_id);
		domain.setId(hr_leave_report_id);
        Boolean rst = hr_leave_reportService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

	@ApiOperation(value = "获取默认查询", tags = {"Hr_leave_report" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_hr/hr_leave_reports/fetchdefault")
	public ResponseEntity<Page<Hr_leave_reportDTO>> fetchDefault(Hr_leave_reportSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Hr_leave_reportDTO> list = new ArrayList<Hr_leave_reportDTO>();
        
        Page<Hr_leave_report> domains = hr_leave_reportService.searchDefault(context) ;
        for(Hr_leave_report hr_leave_report : domains.getContent()){
            Hr_leave_reportDTO dto = new Hr_leave_reportDTO();
            dto.fromDO(hr_leave_report);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
