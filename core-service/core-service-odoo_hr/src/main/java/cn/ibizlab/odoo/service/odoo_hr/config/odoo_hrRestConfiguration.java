package cn.ibizlab.odoo.service.odoo_hr.config;

import org.springframework.context.annotation.ComponentScan;

@ComponentScan("cn.ibizlab.odoo.service.odoo_hr")
public class odoo_hrRestConfiguration {

}
