package cn.ibizlab.odoo.service.odoo_hr.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_hr.dto.Hr_leave_allocationDTO;
import cn.ibizlab.odoo.core.odoo_hr.domain.Hr_leave_allocation;
import cn.ibizlab.odoo.core.odoo_hr.service.IHr_leave_allocationService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_hr.filter.Hr_leave_allocationSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Hr_leave_allocation" })
@RestController
@RequestMapping("")
public class Hr_leave_allocationResource {

    @Autowired
    private IHr_leave_allocationService hr_leave_allocationService;

    public IHr_leave_allocationService getHr_leave_allocationService() {
        return this.hr_leave_allocationService;
    }

    @ApiOperation(value = "获取数据", tags = {"Hr_leave_allocation" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_hr/hr_leave_allocations/{hr_leave_allocation_id}")
    public ResponseEntity<Hr_leave_allocationDTO> get(@PathVariable("hr_leave_allocation_id") Integer hr_leave_allocation_id) {
        Hr_leave_allocationDTO dto = new Hr_leave_allocationDTO();
        Hr_leave_allocation domain = hr_leave_allocationService.get(hr_leave_allocation_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批更新数据", tags = {"Hr_leave_allocation" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_hr/hr_leave_allocations/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Hr_leave_allocationDTO> hr_leave_allocationdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "批删除数据", tags = {"Hr_leave_allocation" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_hr/hr_leave_allocations/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Hr_leave_allocationDTO> hr_leave_allocationdtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "删除数据", tags = {"Hr_leave_allocation" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_hr/hr_leave_allocations/{hr_leave_allocation_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("hr_leave_allocation_id") Integer hr_leave_allocation_id) {
        Hr_leave_allocationDTO hr_leave_allocationdto = new Hr_leave_allocationDTO();
		Hr_leave_allocation domain = new Hr_leave_allocation();
		hr_leave_allocationdto.setId(hr_leave_allocation_id);
		domain.setId(hr_leave_allocation_id);
        Boolean rst = hr_leave_allocationService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "批建立数据", tags = {"Hr_leave_allocation" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_hr/hr_leave_allocations/createBatch")
    public ResponseEntity<Boolean> createBatchHr_leave_allocation(@RequestBody List<Hr_leave_allocationDTO> hr_leave_allocationdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "更新数据", tags = {"Hr_leave_allocation" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_hr/hr_leave_allocations/{hr_leave_allocation_id}")

    public ResponseEntity<Hr_leave_allocationDTO> update(@PathVariable("hr_leave_allocation_id") Integer hr_leave_allocation_id, @RequestBody Hr_leave_allocationDTO hr_leave_allocationdto) {
		Hr_leave_allocation domain = hr_leave_allocationdto.toDO();
        domain.setId(hr_leave_allocation_id);
		hr_leave_allocationService.update(domain);
		Hr_leave_allocationDTO dto = new Hr_leave_allocationDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "建立数据", tags = {"Hr_leave_allocation" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_hr/hr_leave_allocations")

    public ResponseEntity<Hr_leave_allocationDTO> create(@RequestBody Hr_leave_allocationDTO hr_leave_allocationdto) {
        Hr_leave_allocationDTO dto = new Hr_leave_allocationDTO();
        Hr_leave_allocation domain = hr_leave_allocationdto.toDO();
		hr_leave_allocationService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Hr_leave_allocation" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_hr/hr_leave_allocations/fetchdefault")
	public ResponseEntity<Page<Hr_leave_allocationDTO>> fetchDefault(Hr_leave_allocationSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Hr_leave_allocationDTO> list = new ArrayList<Hr_leave_allocationDTO>();
        
        Page<Hr_leave_allocation> domains = hr_leave_allocationService.searchDefault(context) ;
        for(Hr_leave_allocation hr_leave_allocation : domains.getContent()){
            Hr_leave_allocationDTO dto = new Hr_leave_allocationDTO();
            dto.fromDO(hr_leave_allocation);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
