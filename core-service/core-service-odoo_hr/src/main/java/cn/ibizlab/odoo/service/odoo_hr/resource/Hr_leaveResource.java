package cn.ibizlab.odoo.service.odoo_hr.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_hr.dto.Hr_leaveDTO;
import cn.ibizlab.odoo.core.odoo_hr.domain.Hr_leave;
import cn.ibizlab.odoo.core.odoo_hr.service.IHr_leaveService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_hr.filter.Hr_leaveSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Hr_leave" })
@RestController
@RequestMapping("")
public class Hr_leaveResource {

    @Autowired
    private IHr_leaveService hr_leaveService;

    public IHr_leaveService getHr_leaveService() {
        return this.hr_leaveService;
    }

    @ApiOperation(value = "更新数据", tags = {"Hr_leave" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_hr/hr_leaves/{hr_leave_id}")

    public ResponseEntity<Hr_leaveDTO> update(@PathVariable("hr_leave_id") Integer hr_leave_id, @RequestBody Hr_leaveDTO hr_leavedto) {
		Hr_leave domain = hr_leavedto.toDO();
        domain.setId(hr_leave_id);
		hr_leaveService.update(domain);
		Hr_leaveDTO dto = new Hr_leaveDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批更新数据", tags = {"Hr_leave" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_hr/hr_leaves/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Hr_leaveDTO> hr_leavedtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "批删除数据", tags = {"Hr_leave" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_hr/hr_leaves/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Hr_leaveDTO> hr_leavedtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "建立数据", tags = {"Hr_leave" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_hr/hr_leaves")

    public ResponseEntity<Hr_leaveDTO> create(@RequestBody Hr_leaveDTO hr_leavedto) {
        Hr_leaveDTO dto = new Hr_leaveDTO();
        Hr_leave domain = hr_leavedto.toDO();
		hr_leaveService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批建立数据", tags = {"Hr_leave" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_hr/hr_leaves/createBatch")
    public ResponseEntity<Boolean> createBatchHr_leave(@RequestBody List<Hr_leaveDTO> hr_leavedtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "获取数据", tags = {"Hr_leave" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_hr/hr_leaves/{hr_leave_id}")
    public ResponseEntity<Hr_leaveDTO> get(@PathVariable("hr_leave_id") Integer hr_leave_id) {
        Hr_leaveDTO dto = new Hr_leaveDTO();
        Hr_leave domain = hr_leaveService.get(hr_leave_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "删除数据", tags = {"Hr_leave" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_hr/hr_leaves/{hr_leave_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("hr_leave_id") Integer hr_leave_id) {
        Hr_leaveDTO hr_leavedto = new Hr_leaveDTO();
		Hr_leave domain = new Hr_leave();
		hr_leavedto.setId(hr_leave_id);
		domain.setId(hr_leave_id);
        Boolean rst = hr_leaveService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

	@ApiOperation(value = "获取默认查询", tags = {"Hr_leave" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_hr/hr_leaves/fetchdefault")
	public ResponseEntity<Page<Hr_leaveDTO>> fetchDefault(Hr_leaveSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Hr_leaveDTO> list = new ArrayList<Hr_leaveDTO>();
        
        Page<Hr_leave> domains = hr_leaveService.searchDefault(context) ;
        for(Hr_leave hr_leave : domains.getContent()){
            Hr_leaveDTO dto = new Hr_leaveDTO();
            dto.fromDO(hr_leave);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
