package cn.ibizlab.odoo.service.odoo_hr.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import lombok.Data;

@ConfigurationProperties(prefix = "service.odoo.hr")
@Data
public class odoo_hrServiceProperties {

	private boolean enabled;

	private boolean auth;


}