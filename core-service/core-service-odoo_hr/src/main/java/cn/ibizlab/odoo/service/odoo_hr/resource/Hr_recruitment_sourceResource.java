package cn.ibizlab.odoo.service.odoo_hr.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_hr.dto.Hr_recruitment_sourceDTO;
import cn.ibizlab.odoo.core.odoo_hr.domain.Hr_recruitment_source;
import cn.ibizlab.odoo.core.odoo_hr.service.IHr_recruitment_sourceService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_hr.filter.Hr_recruitment_sourceSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Hr_recruitment_source" })
@RestController
@RequestMapping("")
public class Hr_recruitment_sourceResource {

    @Autowired
    private IHr_recruitment_sourceService hr_recruitment_sourceService;

    public IHr_recruitment_sourceService getHr_recruitment_sourceService() {
        return this.hr_recruitment_sourceService;
    }

    @ApiOperation(value = "批更新数据", tags = {"Hr_recruitment_source" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_hr/hr_recruitment_sources/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Hr_recruitment_sourceDTO> hr_recruitment_sourcedtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "批删除数据", tags = {"Hr_recruitment_source" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_hr/hr_recruitment_sources/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Hr_recruitment_sourceDTO> hr_recruitment_sourcedtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "批建立数据", tags = {"Hr_recruitment_source" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_hr/hr_recruitment_sources/createBatch")
    public ResponseEntity<Boolean> createBatchHr_recruitment_source(@RequestBody List<Hr_recruitment_sourceDTO> hr_recruitment_sourcedtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "获取数据", tags = {"Hr_recruitment_source" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_hr/hr_recruitment_sources/{hr_recruitment_source_id}")
    public ResponseEntity<Hr_recruitment_sourceDTO> get(@PathVariable("hr_recruitment_source_id") Integer hr_recruitment_source_id) {
        Hr_recruitment_sourceDTO dto = new Hr_recruitment_sourceDTO();
        Hr_recruitment_source domain = hr_recruitment_sourceService.get(hr_recruitment_source_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "更新数据", tags = {"Hr_recruitment_source" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_hr/hr_recruitment_sources/{hr_recruitment_source_id}")

    public ResponseEntity<Hr_recruitment_sourceDTO> update(@PathVariable("hr_recruitment_source_id") Integer hr_recruitment_source_id, @RequestBody Hr_recruitment_sourceDTO hr_recruitment_sourcedto) {
		Hr_recruitment_source domain = hr_recruitment_sourcedto.toDO();
        domain.setId(hr_recruitment_source_id);
		hr_recruitment_sourceService.update(domain);
		Hr_recruitment_sourceDTO dto = new Hr_recruitment_sourceDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "建立数据", tags = {"Hr_recruitment_source" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_hr/hr_recruitment_sources")

    public ResponseEntity<Hr_recruitment_sourceDTO> create(@RequestBody Hr_recruitment_sourceDTO hr_recruitment_sourcedto) {
        Hr_recruitment_sourceDTO dto = new Hr_recruitment_sourceDTO();
        Hr_recruitment_source domain = hr_recruitment_sourcedto.toDO();
		hr_recruitment_sourceService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "删除数据", tags = {"Hr_recruitment_source" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_hr/hr_recruitment_sources/{hr_recruitment_source_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("hr_recruitment_source_id") Integer hr_recruitment_source_id) {
        Hr_recruitment_sourceDTO hr_recruitment_sourcedto = new Hr_recruitment_sourceDTO();
		Hr_recruitment_source domain = new Hr_recruitment_source();
		hr_recruitment_sourcedto.setId(hr_recruitment_source_id);
		domain.setId(hr_recruitment_source_id);
        Boolean rst = hr_recruitment_sourceService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

	@ApiOperation(value = "获取默认查询", tags = {"Hr_recruitment_source" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_hr/hr_recruitment_sources/fetchdefault")
	public ResponseEntity<Page<Hr_recruitment_sourceDTO>> fetchDefault(Hr_recruitment_sourceSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Hr_recruitment_sourceDTO> list = new ArrayList<Hr_recruitment_sourceDTO>();
        
        Page<Hr_recruitment_source> domains = hr_recruitment_sourceService.searchDefault(context) ;
        for(Hr_recruitment_source hr_recruitment_source : domains.getContent()){
            Hr_recruitment_sourceDTO dto = new Hr_recruitment_sourceDTO();
            dto.fromDO(hr_recruitment_source);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
