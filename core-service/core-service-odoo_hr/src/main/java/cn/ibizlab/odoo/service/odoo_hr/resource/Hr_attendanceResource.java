package cn.ibizlab.odoo.service.odoo_hr.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_hr.dto.Hr_attendanceDTO;
import cn.ibizlab.odoo.core.odoo_hr.domain.Hr_attendance;
import cn.ibizlab.odoo.core.odoo_hr.service.IHr_attendanceService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_hr.filter.Hr_attendanceSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Hr_attendance" })
@RestController
@RequestMapping("")
public class Hr_attendanceResource {

    @Autowired
    private IHr_attendanceService hr_attendanceService;

    public IHr_attendanceService getHr_attendanceService() {
        return this.hr_attendanceService;
    }

    @ApiOperation(value = "删除数据", tags = {"Hr_attendance" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_hr/hr_attendances/{hr_attendance_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("hr_attendance_id") Integer hr_attendance_id) {
        Hr_attendanceDTO hr_attendancedto = new Hr_attendanceDTO();
		Hr_attendance domain = new Hr_attendance();
		hr_attendancedto.setId(hr_attendance_id);
		domain.setId(hr_attendance_id);
        Boolean rst = hr_attendanceService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "批更新数据", tags = {"Hr_attendance" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_hr/hr_attendances/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Hr_attendanceDTO> hr_attendancedtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "建立数据", tags = {"Hr_attendance" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_hr/hr_attendances")

    public ResponseEntity<Hr_attendanceDTO> create(@RequestBody Hr_attendanceDTO hr_attendancedto) {
        Hr_attendanceDTO dto = new Hr_attendanceDTO();
        Hr_attendance domain = hr_attendancedto.toDO();
		hr_attendanceService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "更新数据", tags = {"Hr_attendance" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_hr/hr_attendances/{hr_attendance_id}")

    public ResponseEntity<Hr_attendanceDTO> update(@PathVariable("hr_attendance_id") Integer hr_attendance_id, @RequestBody Hr_attendanceDTO hr_attendancedto) {
		Hr_attendance domain = hr_attendancedto.toDO();
        domain.setId(hr_attendance_id);
		hr_attendanceService.update(domain);
		Hr_attendanceDTO dto = new Hr_attendanceDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取数据", tags = {"Hr_attendance" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_hr/hr_attendances/{hr_attendance_id}")
    public ResponseEntity<Hr_attendanceDTO> get(@PathVariable("hr_attendance_id") Integer hr_attendance_id) {
        Hr_attendanceDTO dto = new Hr_attendanceDTO();
        Hr_attendance domain = hr_attendanceService.get(hr_attendance_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批删除数据", tags = {"Hr_attendance" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_hr/hr_attendances/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Hr_attendanceDTO> hr_attendancedtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "批建立数据", tags = {"Hr_attendance" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_hr/hr_attendances/createBatch")
    public ResponseEntity<Boolean> createBatchHr_attendance(@RequestBody List<Hr_attendanceDTO> hr_attendancedtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Hr_attendance" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_hr/hr_attendances/fetchdefault")
	public ResponseEntity<Page<Hr_attendanceDTO>> fetchDefault(Hr_attendanceSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Hr_attendanceDTO> list = new ArrayList<Hr_attendanceDTO>();
        
        Page<Hr_attendance> domains = hr_attendanceService.searchDefault(context) ;
        for(Hr_attendance hr_attendance : domains.getContent()){
            Hr_attendanceDTO dto = new Hr_attendanceDTO();
            dto.fromDO(hr_attendance);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
