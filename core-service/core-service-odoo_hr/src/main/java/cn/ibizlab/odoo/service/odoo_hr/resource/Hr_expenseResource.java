package cn.ibizlab.odoo.service.odoo_hr.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_hr.dto.Hr_expenseDTO;
import cn.ibizlab.odoo.core.odoo_hr.domain.Hr_expense;
import cn.ibizlab.odoo.core.odoo_hr.service.IHr_expenseService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_hr.filter.Hr_expenseSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Hr_expense" })
@RestController
@RequestMapping("")
public class Hr_expenseResource {

    @Autowired
    private IHr_expenseService hr_expenseService;

    public IHr_expenseService getHr_expenseService() {
        return this.hr_expenseService;
    }

    @ApiOperation(value = "批更新数据", tags = {"Hr_expense" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_hr/hr_expenses/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Hr_expenseDTO> hr_expensedtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "更新数据", tags = {"Hr_expense" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_hr/hr_expenses/{hr_expense_id}")

    public ResponseEntity<Hr_expenseDTO> update(@PathVariable("hr_expense_id") Integer hr_expense_id, @RequestBody Hr_expenseDTO hr_expensedto) {
		Hr_expense domain = hr_expensedto.toDO();
        domain.setId(hr_expense_id);
		hr_expenseService.update(domain);
		Hr_expenseDTO dto = new Hr_expenseDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "删除数据", tags = {"Hr_expense" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_hr/hr_expenses/{hr_expense_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("hr_expense_id") Integer hr_expense_id) {
        Hr_expenseDTO hr_expensedto = new Hr_expenseDTO();
		Hr_expense domain = new Hr_expense();
		hr_expensedto.setId(hr_expense_id);
		domain.setId(hr_expense_id);
        Boolean rst = hr_expenseService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "获取数据", tags = {"Hr_expense" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_hr/hr_expenses/{hr_expense_id}")
    public ResponseEntity<Hr_expenseDTO> get(@PathVariable("hr_expense_id") Integer hr_expense_id) {
        Hr_expenseDTO dto = new Hr_expenseDTO();
        Hr_expense domain = hr_expenseService.get(hr_expense_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批建立数据", tags = {"Hr_expense" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_hr/hr_expenses/createBatch")
    public ResponseEntity<Boolean> createBatchHr_expense(@RequestBody List<Hr_expenseDTO> hr_expensedtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "批删除数据", tags = {"Hr_expense" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_hr/hr_expenses/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Hr_expenseDTO> hr_expensedtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "建立数据", tags = {"Hr_expense" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_hr/hr_expenses")

    public ResponseEntity<Hr_expenseDTO> create(@RequestBody Hr_expenseDTO hr_expensedto) {
        Hr_expenseDTO dto = new Hr_expenseDTO();
        Hr_expense domain = hr_expensedto.toDO();
		hr_expenseService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Hr_expense" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_hr/hr_expenses/fetchdefault")
	public ResponseEntity<Page<Hr_expenseDTO>> fetchDefault(Hr_expenseSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Hr_expenseDTO> list = new ArrayList<Hr_expenseDTO>();
        
        Page<Hr_expense> domains = hr_expenseService.searchDefault(context) ;
        for(Hr_expense hr_expense : domains.getContent()){
            Hr_expenseDTO dto = new Hr_expenseDTO();
            dto.fromDO(hr_expense);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
