package cn.ibizlab.odoo.service.odoo_hr.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.core.odoo_hr.valuerule.anno.hr_expense_refuse_wizard.*;
import cn.ibizlab.odoo.core.odoo_hr.domain.Hr_expense_refuse_wizard;
import org.springframework.cglib.beans.BeanCopier;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 服务DTO对象[Hr_expense_refuse_wizardDTO]
 */
public class Hr_expense_refuse_wizardDTO implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [ID]
     *
     */
    @Hr_expense_refuse_wizardIdDefault(info = "默认规则")
    private Integer id;

    @JsonIgnore
    private boolean idDirtyFlag;

    /**
     * 属性 [HR_EXPENSE_IDS]
     *
     */
    @Hr_expense_refuse_wizardHr_expense_idsDefault(info = "默认规则")
    private String hr_expense_ids;

    @JsonIgnore
    private boolean hr_expense_idsDirtyFlag;

    /**
     * 属性 [CREATE_DATE]
     *
     */
    @Hr_expense_refuse_wizardCreate_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp create_date;

    @JsonIgnore
    private boolean create_dateDirtyFlag;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @Hr_expense_refuse_wizardDisplay_nameDefault(info = "默认规则")
    private String display_name;

    @JsonIgnore
    private boolean display_nameDirtyFlag;

    /**
     * 属性 [REASON]
     *
     */
    @Hr_expense_refuse_wizardReasonDefault(info = "默认规则")
    private String reason;

    @JsonIgnore
    private boolean reasonDirtyFlag;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @Hr_expense_refuse_wizard__last_updateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp __last_update;

    @JsonIgnore
    private boolean __last_updateDirtyFlag;

    /**
     * 属性 [WRITE_DATE]
     *
     */
    @Hr_expense_refuse_wizardWrite_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp write_date;

    @JsonIgnore
    private boolean write_dateDirtyFlag;

    /**
     * 属性 [HR_EXPENSE_SHEET_ID_TEXT]
     *
     */
    @Hr_expense_refuse_wizardHr_expense_sheet_id_textDefault(info = "默认规则")
    private String hr_expense_sheet_id_text;

    @JsonIgnore
    private boolean hr_expense_sheet_id_textDirtyFlag;

    /**
     * 属性 [CREATE_UID_TEXT]
     *
     */
    @Hr_expense_refuse_wizardCreate_uid_textDefault(info = "默认规则")
    private String create_uid_text;

    @JsonIgnore
    private boolean create_uid_textDirtyFlag;

    /**
     * 属性 [WRITE_UID_TEXT]
     *
     */
    @Hr_expense_refuse_wizardWrite_uid_textDefault(info = "默认规则")
    private String write_uid_text;

    @JsonIgnore
    private boolean write_uid_textDirtyFlag;

    /**
     * 属性 [CREATE_UID]
     *
     */
    @Hr_expense_refuse_wizardCreate_uidDefault(info = "默认规则")
    private Integer create_uid;

    @JsonIgnore
    private boolean create_uidDirtyFlag;

    /**
     * 属性 [WRITE_UID]
     *
     */
    @Hr_expense_refuse_wizardWrite_uidDefault(info = "默认规则")
    private Integer write_uid;

    @JsonIgnore
    private boolean write_uidDirtyFlag;

    /**
     * 属性 [HR_EXPENSE_SHEET_ID]
     *
     */
    @Hr_expense_refuse_wizardHr_expense_sheet_idDefault(info = "默认规则")
    private Integer hr_expense_sheet_id;

    @JsonIgnore
    private boolean hr_expense_sheet_idDirtyFlag;


    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

    /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return idDirtyFlag ;
    }

    /**
     * 获取 [HR_EXPENSE_IDS]
     */
    @JsonProperty("hr_expense_ids")
    public String getHr_expense_ids(){
        return hr_expense_ids ;
    }

    /**
     * 设置 [HR_EXPENSE_IDS]
     */
    @JsonProperty("hr_expense_ids")
    public void setHr_expense_ids(String  hr_expense_ids){
        this.hr_expense_ids = hr_expense_ids ;
        this.hr_expense_idsDirtyFlag = true ;
    }

    /**
     * 获取 [HR_EXPENSE_IDS]脏标记
     */
    @JsonIgnore
    public boolean getHr_expense_idsDirtyFlag(){
        return hr_expense_idsDirtyFlag ;
    }

    /**
     * 获取 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return create_date ;
    }

    /**
     * 设置 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return create_dateDirtyFlag ;
    }

    /**
     * 获取 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return display_name ;
    }

    /**
     * 设置 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

    /**
     * 获取 [DISPLAY_NAME]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return display_nameDirtyFlag ;
    }

    /**
     * 获取 [REASON]
     */
    @JsonProperty("reason")
    public String getReason(){
        return reason ;
    }

    /**
     * 设置 [REASON]
     */
    @JsonProperty("reason")
    public void setReason(String  reason){
        this.reason = reason ;
        this.reasonDirtyFlag = true ;
    }

    /**
     * 获取 [REASON]脏标记
     */
    @JsonIgnore
    public boolean getReasonDirtyFlag(){
        return reasonDirtyFlag ;
    }

    /**
     * 获取 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return __last_update ;
    }

    /**
     * 设置 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

    /**
     * 获取 [__LAST_UPDATE]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return __last_updateDirtyFlag ;
    }

    /**
     * 获取 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return write_date ;
    }

    /**
     * 设置 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return write_dateDirtyFlag ;
    }

    /**
     * 获取 [HR_EXPENSE_SHEET_ID_TEXT]
     */
    @JsonProperty("hr_expense_sheet_id_text")
    public String getHr_expense_sheet_id_text(){
        return hr_expense_sheet_id_text ;
    }

    /**
     * 设置 [HR_EXPENSE_SHEET_ID_TEXT]
     */
    @JsonProperty("hr_expense_sheet_id_text")
    public void setHr_expense_sheet_id_text(String  hr_expense_sheet_id_text){
        this.hr_expense_sheet_id_text = hr_expense_sheet_id_text ;
        this.hr_expense_sheet_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [HR_EXPENSE_SHEET_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getHr_expense_sheet_id_textDirtyFlag(){
        return hr_expense_sheet_id_textDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return create_uid_text ;
    }

    /**
     * 设置 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return create_uid_textDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return write_uid_text ;
    }

    /**
     * 设置 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return write_uid_textDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return create_uid ;
    }

    /**
     * 设置 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return create_uidDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return write_uid ;
    }

    /**
     * 设置 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return write_uidDirtyFlag ;
    }

    /**
     * 获取 [HR_EXPENSE_SHEET_ID]
     */
    @JsonProperty("hr_expense_sheet_id")
    public Integer getHr_expense_sheet_id(){
        return hr_expense_sheet_id ;
    }

    /**
     * 设置 [HR_EXPENSE_SHEET_ID]
     */
    @JsonProperty("hr_expense_sheet_id")
    public void setHr_expense_sheet_id(Integer  hr_expense_sheet_id){
        this.hr_expense_sheet_id = hr_expense_sheet_id ;
        this.hr_expense_sheet_idDirtyFlag = true ;
    }

    /**
     * 获取 [HR_EXPENSE_SHEET_ID]脏标记
     */
    @JsonIgnore
    public boolean getHr_expense_sheet_idDirtyFlag(){
        return hr_expense_sheet_idDirtyFlag ;
    }



    public Hr_expense_refuse_wizard toDO() {
        Hr_expense_refuse_wizard srfdomain = new Hr_expense_refuse_wizard();
        if(getIdDirtyFlag())
            srfdomain.setId(id);
        if(getHr_expense_idsDirtyFlag())
            srfdomain.setHr_expense_ids(hr_expense_ids);
        if(getCreate_dateDirtyFlag())
            srfdomain.setCreate_date(create_date);
        if(getDisplay_nameDirtyFlag())
            srfdomain.setDisplay_name(display_name);
        if(getReasonDirtyFlag())
            srfdomain.setReason(reason);
        if(get__last_updateDirtyFlag())
            srfdomain.set__last_update(__last_update);
        if(getWrite_dateDirtyFlag())
            srfdomain.setWrite_date(write_date);
        if(getHr_expense_sheet_id_textDirtyFlag())
            srfdomain.setHr_expense_sheet_id_text(hr_expense_sheet_id_text);
        if(getCreate_uid_textDirtyFlag())
            srfdomain.setCreate_uid_text(create_uid_text);
        if(getWrite_uid_textDirtyFlag())
            srfdomain.setWrite_uid_text(write_uid_text);
        if(getCreate_uidDirtyFlag())
            srfdomain.setCreate_uid(create_uid);
        if(getWrite_uidDirtyFlag())
            srfdomain.setWrite_uid(write_uid);
        if(getHr_expense_sheet_idDirtyFlag())
            srfdomain.setHr_expense_sheet_id(hr_expense_sheet_id);

        return srfdomain;
    }

    public void fromDO(Hr_expense_refuse_wizard srfdomain) {
        if(srfdomain == null )
          return ;
        if(srfdomain.getIdDirtyFlag())
            this.setId(srfdomain.getId());
        if(srfdomain.getHr_expense_idsDirtyFlag())
            this.setHr_expense_ids(srfdomain.getHr_expense_ids());
        if(srfdomain.getCreate_dateDirtyFlag())
            this.setCreate_date(srfdomain.getCreate_date());
        if(srfdomain.getDisplay_nameDirtyFlag())
            this.setDisplay_name(srfdomain.getDisplay_name());
        if(srfdomain.getReasonDirtyFlag())
            this.setReason(srfdomain.getReason());
        if(srfdomain.get__last_updateDirtyFlag())
            this.set__last_update(srfdomain.get__last_update());
        if(srfdomain.getWrite_dateDirtyFlag())
            this.setWrite_date(srfdomain.getWrite_date());
        if(srfdomain.getHr_expense_sheet_id_textDirtyFlag())
            this.setHr_expense_sheet_id_text(srfdomain.getHr_expense_sheet_id_text());
        if(srfdomain.getCreate_uid_textDirtyFlag())
            this.setCreate_uid_text(srfdomain.getCreate_uid_text());
        if(srfdomain.getWrite_uid_textDirtyFlag())
            this.setWrite_uid_text(srfdomain.getWrite_uid_text());
        if(srfdomain.getCreate_uidDirtyFlag())
            this.setCreate_uid(srfdomain.getCreate_uid());
        if(srfdomain.getWrite_uidDirtyFlag())
            this.setWrite_uid(srfdomain.getWrite_uid());
        if(srfdomain.getHr_expense_sheet_idDirtyFlag())
            this.setHr_expense_sheet_id(srfdomain.getHr_expense_sheet_id());

    }

    public List<Hr_expense_refuse_wizardDTO> fromDOPage(List<Hr_expense_refuse_wizard> poPage)   {
        if(poPage == null)
            return null;
        List<Hr_expense_refuse_wizardDTO> dtos=new ArrayList<Hr_expense_refuse_wizardDTO>();
        for(Hr_expense_refuse_wizard domain : poPage) {
            Hr_expense_refuse_wizardDTO dto = new Hr_expense_refuse_wizardDTO();
            dto.fromDO(domain);
            dtos.add(dto);
        }
        return dtos;
    }
}

