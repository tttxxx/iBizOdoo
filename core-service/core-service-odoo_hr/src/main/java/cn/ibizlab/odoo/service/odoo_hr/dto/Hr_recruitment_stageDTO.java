package cn.ibizlab.odoo.service.odoo_hr.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.core.odoo_hr.valuerule.anno.hr_recruitment_stage.*;
import cn.ibizlab.odoo.core.odoo_hr.domain.Hr_recruitment_stage;
import org.springframework.cglib.beans.BeanCopier;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 服务DTO对象[Hr_recruitment_stageDTO]
 */
public class Hr_recruitment_stageDTO implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [WRITE_DATE]
     *
     */
    @Hr_recruitment_stageWrite_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp write_date;

    @JsonIgnore
    private boolean write_dateDirtyFlag;

    /**
     * 属性 [REQUIREMENTS]
     *
     */
    @Hr_recruitment_stageRequirementsDefault(info = "默认规则")
    private String requirements;

    @JsonIgnore
    private boolean requirementsDirtyFlag;

    /**
     * 属性 [LEGEND_NORMAL]
     *
     */
    @Hr_recruitment_stageLegend_normalDefault(info = "默认规则")
    private String legend_normal;

    @JsonIgnore
    private boolean legend_normalDirtyFlag;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @Hr_recruitment_stage__last_updateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp __last_update;

    @JsonIgnore
    private boolean __last_updateDirtyFlag;

    /**
     * 属性 [NAME]
     *
     */
    @Hr_recruitment_stageNameDefault(info = "默认规则")
    private String name;

    @JsonIgnore
    private boolean nameDirtyFlag;

    /**
     * 属性 [FOLD]
     *
     */
    @Hr_recruitment_stageFoldDefault(info = "默认规则")
    private String fold;

    @JsonIgnore
    private boolean foldDirtyFlag;

    /**
     * 属性 [SEQUENCE]
     *
     */
    @Hr_recruitment_stageSequenceDefault(info = "默认规则")
    private Integer sequence;

    @JsonIgnore
    private boolean sequenceDirtyFlag;

    /**
     * 属性 [LEGEND_DONE]
     *
     */
    @Hr_recruitment_stageLegend_doneDefault(info = "默认规则")
    private String legend_done;

    @JsonIgnore
    private boolean legend_doneDirtyFlag;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @Hr_recruitment_stageDisplay_nameDefault(info = "默认规则")
    private String display_name;

    @JsonIgnore
    private boolean display_nameDirtyFlag;

    /**
     * 属性 [LEGEND_BLOCKED]
     *
     */
    @Hr_recruitment_stageLegend_blockedDefault(info = "默认规则")
    private String legend_blocked;

    @JsonIgnore
    private boolean legend_blockedDirtyFlag;

    /**
     * 属性 [ID]
     *
     */
    @Hr_recruitment_stageIdDefault(info = "默认规则")
    private Integer id;

    @JsonIgnore
    private boolean idDirtyFlag;

    /**
     * 属性 [CREATE_DATE]
     *
     */
    @Hr_recruitment_stageCreate_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp create_date;

    @JsonIgnore
    private boolean create_dateDirtyFlag;

    /**
     * 属性 [TEMPLATE_ID_TEXT]
     *
     */
    @Hr_recruitment_stageTemplate_id_textDefault(info = "默认规则")
    private String template_id_text;

    @JsonIgnore
    private boolean template_id_textDirtyFlag;

    /**
     * 属性 [WRITE_UID_TEXT]
     *
     */
    @Hr_recruitment_stageWrite_uid_textDefault(info = "默认规则")
    private String write_uid_text;

    @JsonIgnore
    private boolean write_uid_textDirtyFlag;

    /**
     * 属性 [JOB_ID_TEXT]
     *
     */
    @Hr_recruitment_stageJob_id_textDefault(info = "默认规则")
    private String job_id_text;

    @JsonIgnore
    private boolean job_id_textDirtyFlag;

    /**
     * 属性 [CREATE_UID_TEXT]
     *
     */
    @Hr_recruitment_stageCreate_uid_textDefault(info = "默认规则")
    private String create_uid_text;

    @JsonIgnore
    private boolean create_uid_textDirtyFlag;

    /**
     * 属性 [WRITE_UID]
     *
     */
    @Hr_recruitment_stageWrite_uidDefault(info = "默认规则")
    private Integer write_uid;

    @JsonIgnore
    private boolean write_uidDirtyFlag;

    /**
     * 属性 [CREATE_UID]
     *
     */
    @Hr_recruitment_stageCreate_uidDefault(info = "默认规则")
    private Integer create_uid;

    @JsonIgnore
    private boolean create_uidDirtyFlag;

    /**
     * 属性 [JOB_ID]
     *
     */
    @Hr_recruitment_stageJob_idDefault(info = "默认规则")
    private Integer job_id;

    @JsonIgnore
    private boolean job_idDirtyFlag;

    /**
     * 属性 [TEMPLATE_ID]
     *
     */
    @Hr_recruitment_stageTemplate_idDefault(info = "默认规则")
    private Integer template_id;

    @JsonIgnore
    private boolean template_idDirtyFlag;


    /**
     * 获取 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return write_date ;
    }

    /**
     * 设置 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return write_dateDirtyFlag ;
    }

    /**
     * 获取 [REQUIREMENTS]
     */
    @JsonProperty("requirements")
    public String getRequirements(){
        return requirements ;
    }

    /**
     * 设置 [REQUIREMENTS]
     */
    @JsonProperty("requirements")
    public void setRequirements(String  requirements){
        this.requirements = requirements ;
        this.requirementsDirtyFlag = true ;
    }

    /**
     * 获取 [REQUIREMENTS]脏标记
     */
    @JsonIgnore
    public boolean getRequirementsDirtyFlag(){
        return requirementsDirtyFlag ;
    }

    /**
     * 获取 [LEGEND_NORMAL]
     */
    @JsonProperty("legend_normal")
    public String getLegend_normal(){
        return legend_normal ;
    }

    /**
     * 设置 [LEGEND_NORMAL]
     */
    @JsonProperty("legend_normal")
    public void setLegend_normal(String  legend_normal){
        this.legend_normal = legend_normal ;
        this.legend_normalDirtyFlag = true ;
    }

    /**
     * 获取 [LEGEND_NORMAL]脏标记
     */
    @JsonIgnore
    public boolean getLegend_normalDirtyFlag(){
        return legend_normalDirtyFlag ;
    }

    /**
     * 获取 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return __last_update ;
    }

    /**
     * 设置 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

    /**
     * 获取 [__LAST_UPDATE]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return __last_updateDirtyFlag ;
    }

    /**
     * 获取 [NAME]
     */
    @JsonProperty("name")
    public String getName(){
        return name ;
    }

    /**
     * 设置 [NAME]
     */
    @JsonProperty("name")
    public void setName(String  name){
        this.name = name ;
        this.nameDirtyFlag = true ;
    }

    /**
     * 获取 [NAME]脏标记
     */
    @JsonIgnore
    public boolean getNameDirtyFlag(){
        return nameDirtyFlag ;
    }

    /**
     * 获取 [FOLD]
     */
    @JsonProperty("fold")
    public String getFold(){
        return fold ;
    }

    /**
     * 设置 [FOLD]
     */
    @JsonProperty("fold")
    public void setFold(String  fold){
        this.fold = fold ;
        this.foldDirtyFlag = true ;
    }

    /**
     * 获取 [FOLD]脏标记
     */
    @JsonIgnore
    public boolean getFoldDirtyFlag(){
        return foldDirtyFlag ;
    }

    /**
     * 获取 [SEQUENCE]
     */
    @JsonProperty("sequence")
    public Integer getSequence(){
        return sequence ;
    }

    /**
     * 设置 [SEQUENCE]
     */
    @JsonProperty("sequence")
    public void setSequence(Integer  sequence){
        this.sequence = sequence ;
        this.sequenceDirtyFlag = true ;
    }

    /**
     * 获取 [SEQUENCE]脏标记
     */
    @JsonIgnore
    public boolean getSequenceDirtyFlag(){
        return sequenceDirtyFlag ;
    }

    /**
     * 获取 [LEGEND_DONE]
     */
    @JsonProperty("legend_done")
    public String getLegend_done(){
        return legend_done ;
    }

    /**
     * 设置 [LEGEND_DONE]
     */
    @JsonProperty("legend_done")
    public void setLegend_done(String  legend_done){
        this.legend_done = legend_done ;
        this.legend_doneDirtyFlag = true ;
    }

    /**
     * 获取 [LEGEND_DONE]脏标记
     */
    @JsonIgnore
    public boolean getLegend_doneDirtyFlag(){
        return legend_doneDirtyFlag ;
    }

    /**
     * 获取 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return display_name ;
    }

    /**
     * 设置 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

    /**
     * 获取 [DISPLAY_NAME]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return display_nameDirtyFlag ;
    }

    /**
     * 获取 [LEGEND_BLOCKED]
     */
    @JsonProperty("legend_blocked")
    public String getLegend_blocked(){
        return legend_blocked ;
    }

    /**
     * 设置 [LEGEND_BLOCKED]
     */
    @JsonProperty("legend_blocked")
    public void setLegend_blocked(String  legend_blocked){
        this.legend_blocked = legend_blocked ;
        this.legend_blockedDirtyFlag = true ;
    }

    /**
     * 获取 [LEGEND_BLOCKED]脏标记
     */
    @JsonIgnore
    public boolean getLegend_blockedDirtyFlag(){
        return legend_blockedDirtyFlag ;
    }

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

    /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return idDirtyFlag ;
    }

    /**
     * 获取 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return create_date ;
    }

    /**
     * 设置 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return create_dateDirtyFlag ;
    }

    /**
     * 获取 [TEMPLATE_ID_TEXT]
     */
    @JsonProperty("template_id_text")
    public String getTemplate_id_text(){
        return template_id_text ;
    }

    /**
     * 设置 [TEMPLATE_ID_TEXT]
     */
    @JsonProperty("template_id_text")
    public void setTemplate_id_text(String  template_id_text){
        this.template_id_text = template_id_text ;
        this.template_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [TEMPLATE_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getTemplate_id_textDirtyFlag(){
        return template_id_textDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return write_uid_text ;
    }

    /**
     * 设置 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return write_uid_textDirtyFlag ;
    }

    /**
     * 获取 [JOB_ID_TEXT]
     */
    @JsonProperty("job_id_text")
    public String getJob_id_text(){
        return job_id_text ;
    }

    /**
     * 设置 [JOB_ID_TEXT]
     */
    @JsonProperty("job_id_text")
    public void setJob_id_text(String  job_id_text){
        this.job_id_text = job_id_text ;
        this.job_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [JOB_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getJob_id_textDirtyFlag(){
        return job_id_textDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return create_uid_text ;
    }

    /**
     * 设置 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return create_uid_textDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return write_uid ;
    }

    /**
     * 设置 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return write_uidDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return create_uid ;
    }

    /**
     * 设置 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return create_uidDirtyFlag ;
    }

    /**
     * 获取 [JOB_ID]
     */
    @JsonProperty("job_id")
    public Integer getJob_id(){
        return job_id ;
    }

    /**
     * 设置 [JOB_ID]
     */
    @JsonProperty("job_id")
    public void setJob_id(Integer  job_id){
        this.job_id = job_id ;
        this.job_idDirtyFlag = true ;
    }

    /**
     * 获取 [JOB_ID]脏标记
     */
    @JsonIgnore
    public boolean getJob_idDirtyFlag(){
        return job_idDirtyFlag ;
    }

    /**
     * 获取 [TEMPLATE_ID]
     */
    @JsonProperty("template_id")
    public Integer getTemplate_id(){
        return template_id ;
    }

    /**
     * 设置 [TEMPLATE_ID]
     */
    @JsonProperty("template_id")
    public void setTemplate_id(Integer  template_id){
        this.template_id = template_id ;
        this.template_idDirtyFlag = true ;
    }

    /**
     * 获取 [TEMPLATE_ID]脏标记
     */
    @JsonIgnore
    public boolean getTemplate_idDirtyFlag(){
        return template_idDirtyFlag ;
    }



    public Hr_recruitment_stage toDO() {
        Hr_recruitment_stage srfdomain = new Hr_recruitment_stage();
        if(getWrite_dateDirtyFlag())
            srfdomain.setWrite_date(write_date);
        if(getRequirementsDirtyFlag())
            srfdomain.setRequirements(requirements);
        if(getLegend_normalDirtyFlag())
            srfdomain.setLegend_normal(legend_normal);
        if(get__last_updateDirtyFlag())
            srfdomain.set__last_update(__last_update);
        if(getNameDirtyFlag())
            srfdomain.setName(name);
        if(getFoldDirtyFlag())
            srfdomain.setFold(fold);
        if(getSequenceDirtyFlag())
            srfdomain.setSequence(sequence);
        if(getLegend_doneDirtyFlag())
            srfdomain.setLegend_done(legend_done);
        if(getDisplay_nameDirtyFlag())
            srfdomain.setDisplay_name(display_name);
        if(getLegend_blockedDirtyFlag())
            srfdomain.setLegend_blocked(legend_blocked);
        if(getIdDirtyFlag())
            srfdomain.setId(id);
        if(getCreate_dateDirtyFlag())
            srfdomain.setCreate_date(create_date);
        if(getTemplate_id_textDirtyFlag())
            srfdomain.setTemplate_id_text(template_id_text);
        if(getWrite_uid_textDirtyFlag())
            srfdomain.setWrite_uid_text(write_uid_text);
        if(getJob_id_textDirtyFlag())
            srfdomain.setJob_id_text(job_id_text);
        if(getCreate_uid_textDirtyFlag())
            srfdomain.setCreate_uid_text(create_uid_text);
        if(getWrite_uidDirtyFlag())
            srfdomain.setWrite_uid(write_uid);
        if(getCreate_uidDirtyFlag())
            srfdomain.setCreate_uid(create_uid);
        if(getJob_idDirtyFlag())
            srfdomain.setJob_id(job_id);
        if(getTemplate_idDirtyFlag())
            srfdomain.setTemplate_id(template_id);

        return srfdomain;
    }

    public void fromDO(Hr_recruitment_stage srfdomain) {
        if(srfdomain == null )
          return ;
        if(srfdomain.getWrite_dateDirtyFlag())
            this.setWrite_date(srfdomain.getWrite_date());
        if(srfdomain.getRequirementsDirtyFlag())
            this.setRequirements(srfdomain.getRequirements());
        if(srfdomain.getLegend_normalDirtyFlag())
            this.setLegend_normal(srfdomain.getLegend_normal());
        if(srfdomain.get__last_updateDirtyFlag())
            this.set__last_update(srfdomain.get__last_update());
        if(srfdomain.getNameDirtyFlag())
            this.setName(srfdomain.getName());
        if(srfdomain.getFoldDirtyFlag())
            this.setFold(srfdomain.getFold());
        if(srfdomain.getSequenceDirtyFlag())
            this.setSequence(srfdomain.getSequence());
        if(srfdomain.getLegend_doneDirtyFlag())
            this.setLegend_done(srfdomain.getLegend_done());
        if(srfdomain.getDisplay_nameDirtyFlag())
            this.setDisplay_name(srfdomain.getDisplay_name());
        if(srfdomain.getLegend_blockedDirtyFlag())
            this.setLegend_blocked(srfdomain.getLegend_blocked());
        if(srfdomain.getIdDirtyFlag())
            this.setId(srfdomain.getId());
        if(srfdomain.getCreate_dateDirtyFlag())
            this.setCreate_date(srfdomain.getCreate_date());
        if(srfdomain.getTemplate_id_textDirtyFlag())
            this.setTemplate_id_text(srfdomain.getTemplate_id_text());
        if(srfdomain.getWrite_uid_textDirtyFlag())
            this.setWrite_uid_text(srfdomain.getWrite_uid_text());
        if(srfdomain.getJob_id_textDirtyFlag())
            this.setJob_id_text(srfdomain.getJob_id_text());
        if(srfdomain.getCreate_uid_textDirtyFlag())
            this.setCreate_uid_text(srfdomain.getCreate_uid_text());
        if(srfdomain.getWrite_uidDirtyFlag())
            this.setWrite_uid(srfdomain.getWrite_uid());
        if(srfdomain.getCreate_uidDirtyFlag())
            this.setCreate_uid(srfdomain.getCreate_uid());
        if(srfdomain.getJob_idDirtyFlag())
            this.setJob_id(srfdomain.getJob_id());
        if(srfdomain.getTemplate_idDirtyFlag())
            this.setTemplate_id(srfdomain.getTemplate_id());

    }

    public List<Hr_recruitment_stageDTO> fromDOPage(List<Hr_recruitment_stage> poPage)   {
        if(poPage == null)
            return null;
        List<Hr_recruitment_stageDTO> dtos=new ArrayList<Hr_recruitment_stageDTO>();
        for(Hr_recruitment_stage domain : poPage) {
            Hr_recruitment_stageDTO dto = new Hr_recruitment_stageDTO();
            dto.fromDO(domain);
            dtos.add(dto);
        }
        return dtos;
    }
}

