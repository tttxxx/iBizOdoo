package cn.ibizlab.odoo.service.odoo_hr.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_hr.dto.Hr_applicantDTO;
import cn.ibizlab.odoo.core.odoo_hr.domain.Hr_applicant;
import cn.ibizlab.odoo.core.odoo_hr.service.IHr_applicantService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_hr.filter.Hr_applicantSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Hr_applicant" })
@RestController
@RequestMapping("")
public class Hr_applicantResource {

    @Autowired
    private IHr_applicantService hr_applicantService;

    public IHr_applicantService getHr_applicantService() {
        return this.hr_applicantService;
    }

    @ApiOperation(value = "建立数据", tags = {"Hr_applicant" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_hr/hr_applicants")

    public ResponseEntity<Hr_applicantDTO> create(@RequestBody Hr_applicantDTO hr_applicantdto) {
        Hr_applicantDTO dto = new Hr_applicantDTO();
        Hr_applicant domain = hr_applicantdto.toDO();
		hr_applicantService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批删除数据", tags = {"Hr_applicant" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_hr/hr_applicants/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Hr_applicantDTO> hr_applicantdtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "删除数据", tags = {"Hr_applicant" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_hr/hr_applicants/{hr_applicant_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("hr_applicant_id") Integer hr_applicant_id) {
        Hr_applicantDTO hr_applicantdto = new Hr_applicantDTO();
		Hr_applicant domain = new Hr_applicant();
		hr_applicantdto.setId(hr_applicant_id);
		domain.setId(hr_applicant_id);
        Boolean rst = hr_applicantService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "批建立数据", tags = {"Hr_applicant" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_hr/hr_applicants/createBatch")
    public ResponseEntity<Boolean> createBatchHr_applicant(@RequestBody List<Hr_applicantDTO> hr_applicantdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "更新数据", tags = {"Hr_applicant" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_hr/hr_applicants/{hr_applicant_id}")

    public ResponseEntity<Hr_applicantDTO> update(@PathVariable("hr_applicant_id") Integer hr_applicant_id, @RequestBody Hr_applicantDTO hr_applicantdto) {
		Hr_applicant domain = hr_applicantdto.toDO();
        domain.setId(hr_applicant_id);
		hr_applicantService.update(domain);
		Hr_applicantDTO dto = new Hr_applicantDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批更新数据", tags = {"Hr_applicant" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_hr/hr_applicants/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Hr_applicantDTO> hr_applicantdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "获取数据", tags = {"Hr_applicant" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_hr/hr_applicants/{hr_applicant_id}")
    public ResponseEntity<Hr_applicantDTO> get(@PathVariable("hr_applicant_id") Integer hr_applicant_id) {
        Hr_applicantDTO dto = new Hr_applicantDTO();
        Hr_applicant domain = hr_applicantService.get(hr_applicant_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Hr_applicant" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_hr/hr_applicants/fetchdefault")
	public ResponseEntity<Page<Hr_applicantDTO>> fetchDefault(Hr_applicantSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Hr_applicantDTO> list = new ArrayList<Hr_applicantDTO>();
        
        Page<Hr_applicant> domains = hr_applicantService.searchDefault(context) ;
        for(Hr_applicant hr_applicant : domains.getContent()){
            Hr_applicantDTO dto = new Hr_applicantDTO();
            dto.fromDO(hr_applicant);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
