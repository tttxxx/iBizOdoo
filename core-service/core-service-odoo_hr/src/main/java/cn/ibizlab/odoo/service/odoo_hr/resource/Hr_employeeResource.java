package cn.ibizlab.odoo.service.odoo_hr.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_hr.dto.Hr_employeeDTO;
import cn.ibizlab.odoo.core.odoo_hr.domain.Hr_employee;
import cn.ibizlab.odoo.core.odoo_hr.service.IHr_employeeService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_hr.filter.Hr_employeeSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Hr_employee" })
@RestController
@RequestMapping("")
public class Hr_employeeResource {

    @Autowired
    private IHr_employeeService hr_employeeService;

    public IHr_employeeService getHr_employeeService() {
        return this.hr_employeeService;
    }

    @ApiOperation(value = "建立数据", tags = {"Hr_employee" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_hr/hr_employees")

    public ResponseEntity<Hr_employeeDTO> create(@RequestBody Hr_employeeDTO hr_employeedto) {
        Hr_employeeDTO dto = new Hr_employeeDTO();
        Hr_employee domain = hr_employeedto.toDO();
		hr_employeeService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "删除数据", tags = {"Hr_employee" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_hr/hr_employees/{hr_employee_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("hr_employee_id") Integer hr_employee_id) {
        Hr_employeeDTO hr_employeedto = new Hr_employeeDTO();
		Hr_employee domain = new Hr_employee();
		hr_employeedto.setId(hr_employee_id);
		domain.setId(hr_employee_id);
        Boolean rst = hr_employeeService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "更新数据", tags = {"Hr_employee" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_hr/hr_employees/{hr_employee_id}")

    public ResponseEntity<Hr_employeeDTO> update(@PathVariable("hr_employee_id") Integer hr_employee_id, @RequestBody Hr_employeeDTO hr_employeedto) {
		Hr_employee domain = hr_employeedto.toDO();
        domain.setId(hr_employee_id);
		hr_employeeService.update(domain);
		Hr_employeeDTO dto = new Hr_employeeDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批更新数据", tags = {"Hr_employee" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_hr/hr_employees/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Hr_employeeDTO> hr_employeedtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "获取数据", tags = {"Hr_employee" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_hr/hr_employees/{hr_employee_id}")
    public ResponseEntity<Hr_employeeDTO> get(@PathVariable("hr_employee_id") Integer hr_employee_id) {
        Hr_employeeDTO dto = new Hr_employeeDTO();
        Hr_employee domain = hr_employeeService.get(hr_employee_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批删除数据", tags = {"Hr_employee" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_hr/hr_employees/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Hr_employeeDTO> hr_employeedtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "批建立数据", tags = {"Hr_employee" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_hr/hr_employees/createBatch")
    public ResponseEntity<Boolean> createBatchHr_employee(@RequestBody List<Hr_employeeDTO> hr_employeedtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Hr_employee" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_hr/hr_employees/fetchdefault")
	public ResponseEntity<Page<Hr_employeeDTO>> fetchDefault(Hr_employeeSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Hr_employeeDTO> list = new ArrayList<Hr_employeeDTO>();
        
        Page<Hr_employee> domains = hr_employeeService.searchDefault(context) ;
        for(Hr_employee hr_employee : domains.getContent()){
            Hr_employeeDTO dto = new Hr_employeeDTO();
            dto.fromDO(hr_employee);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
