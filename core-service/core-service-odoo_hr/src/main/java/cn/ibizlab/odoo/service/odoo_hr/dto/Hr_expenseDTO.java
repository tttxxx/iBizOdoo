package cn.ibizlab.odoo.service.odoo_hr.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.core.odoo_hr.valuerule.anno.hr_expense.*;
import cn.ibizlab.odoo.core.odoo_hr.domain.Hr_expense;
import org.springframework.cglib.beans.BeanCopier;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 服务DTO对象[Hr_expenseDTO]
 */
public class Hr_expenseDTO implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [ATTACHMENT_NUMBER]
     *
     */
    @Hr_expenseAttachment_numberDefault(info = "默认规则")
    private Integer attachment_number;

    @JsonIgnore
    private boolean attachment_numberDirtyFlag;

    /**
     * 属性 [STATE]
     *
     */
    @Hr_expenseStateDefault(info = "默认规则")
    private String state;

    @JsonIgnore
    private boolean stateDirtyFlag;

    /**
     * 属性 [NAME]
     *
     */
    @Hr_expenseNameDefault(info = "默认规则")
    private String name;

    @JsonIgnore
    private boolean nameDirtyFlag;

    /**
     * 属性 [ACTIVITY_IDS]
     *
     */
    @Hr_expenseActivity_idsDefault(info = "默认规则")
    private String activity_ids;

    @JsonIgnore
    private boolean activity_idsDirtyFlag;

    /**
     * 属性 [ID]
     *
     */
    @Hr_expenseIdDefault(info = "默认规则")
    private Integer id;

    @JsonIgnore
    private boolean idDirtyFlag;

    /**
     * 属性 [MESSAGE_PARTNER_IDS]
     *
     */
    @Hr_expenseMessage_partner_idsDefault(info = "默认规则")
    private String message_partner_ids;

    @JsonIgnore
    private boolean message_partner_idsDirtyFlag;

    /**
     * 属性 [ACTIVITY_SUMMARY]
     *
     */
    @Hr_expenseActivity_summaryDefault(info = "默认规则")
    private String activity_summary;

    @JsonIgnore
    private boolean activity_summaryDirtyFlag;

    /**
     * 属性 [WEBSITE_MESSAGE_IDS]
     *
     */
    @Hr_expenseWebsite_message_idsDefault(info = "默认规则")
    private String website_message_ids;

    @JsonIgnore
    private boolean website_message_idsDirtyFlag;

    /**
     * 属性 [MESSAGE_NEEDACTION]
     *
     */
    @Hr_expenseMessage_needactionDefault(info = "默认规则")
    private String message_needaction;

    @JsonIgnore
    private boolean message_needactionDirtyFlag;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @Hr_expenseDisplay_nameDefault(info = "默认规则")
    private String display_name;

    @JsonIgnore
    private boolean display_nameDirtyFlag;

    /**
     * 属性 [ACTIVITY_STATE]
     *
     */
    @Hr_expenseActivity_stateDefault(info = "默认规则")
    private String activity_state;

    @JsonIgnore
    private boolean activity_stateDirtyFlag;

    /**
     * 属性 [TOTAL_AMOUNT]
     *
     */
    @Hr_expenseTotal_amountDefault(info = "默认规则")
    private Double total_amount;

    @JsonIgnore
    private boolean total_amountDirtyFlag;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @Hr_expense__last_updateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp __last_update;

    @JsonIgnore
    private boolean __last_updateDirtyFlag;

    /**
     * 属性 [ACTIVITY_DATE_DEADLINE]
     *
     */
    @Hr_expenseActivity_date_deadlineDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    private Timestamp activity_date_deadline;

    @JsonIgnore
    private boolean activity_date_deadlineDirtyFlag;

    /**
     * 属性 [MESSAGE_FOLLOWER_IDS]
     *
     */
    @Hr_expenseMessage_follower_idsDefault(info = "默认规则")
    private String message_follower_ids;

    @JsonIgnore
    private boolean message_follower_idsDirtyFlag;

    /**
     * 属性 [MESSAGE_HAS_ERROR]
     *
     */
    @Hr_expenseMessage_has_errorDefault(info = "默认规则")
    private String message_has_error;

    @JsonIgnore
    private boolean message_has_errorDirtyFlag;

    /**
     * 属性 [TAX_IDS]
     *
     */
    @Hr_expenseTax_idsDefault(info = "默认规则")
    private String tax_ids;

    @JsonIgnore
    private boolean tax_idsDirtyFlag;

    /**
     * 属性 [UNIT_AMOUNT]
     *
     */
    @Hr_expenseUnit_amountDefault(info = "默认规则")
    private Double unit_amount;

    @JsonIgnore
    private boolean unit_amountDirtyFlag;

    /**
     * 属性 [PAYMENT_MODE]
     *
     */
    @Hr_expensePayment_modeDefault(info = "默认规则")
    private String payment_mode;

    @JsonIgnore
    private boolean payment_modeDirtyFlag;

    /**
     * 属性 [MESSAGE_CHANNEL_IDS]
     *
     */
    @Hr_expenseMessage_channel_idsDefault(info = "默认规则")
    private String message_channel_ids;

    @JsonIgnore
    private boolean message_channel_idsDirtyFlag;

    /**
     * 属性 [CREATE_DATE]
     *
     */
    @Hr_expenseCreate_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp create_date;

    @JsonIgnore
    private boolean create_dateDirtyFlag;

    /**
     * 属性 [MESSAGE_ATTACHMENT_COUNT]
     *
     */
    @Hr_expenseMessage_attachment_countDefault(info = "默认规则")
    private Integer message_attachment_count;

    @JsonIgnore
    private boolean message_attachment_countDirtyFlag;

    /**
     * 属性 [MESSAGE_NEEDACTION_COUNTER]
     *
     */
    @Hr_expenseMessage_needaction_counterDefault(info = "默认规则")
    private Integer message_needaction_counter;

    @JsonIgnore
    private boolean message_needaction_counterDirtyFlag;

    /**
     * 属性 [WRITE_DATE]
     *
     */
    @Hr_expenseWrite_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp write_date;

    @JsonIgnore
    private boolean write_dateDirtyFlag;

    /**
     * 属性 [TOTAL_AMOUNT_COMPANY]
     *
     */
    @Hr_expenseTotal_amount_companyDefault(info = "默认规则")
    private Double total_amount_company;

    @JsonIgnore
    private boolean total_amount_companyDirtyFlag;

    /**
     * 属性 [IS_REFUSED]
     *
     */
    @Hr_expenseIs_refusedDefault(info = "默认规则")
    private String is_refused;

    @JsonIgnore
    private boolean is_refusedDirtyFlag;

    /**
     * 属性 [DATE]
     *
     */
    @Hr_expenseDateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    private Timestamp date;

    @JsonIgnore
    private boolean dateDirtyFlag;

    /**
     * 属性 [MESSAGE_IDS]
     *
     */
    @Hr_expenseMessage_idsDefault(info = "默认规则")
    private String message_ids;

    @JsonIgnore
    private boolean message_idsDirtyFlag;

    /**
     * 属性 [QUANTITY]
     *
     */
    @Hr_expenseQuantityDefault(info = "默认规则")
    private Double quantity;

    @JsonIgnore
    private boolean quantityDirtyFlag;

    /**
     * 属性 [MESSAGE_UNREAD]
     *
     */
    @Hr_expenseMessage_unreadDefault(info = "默认规则")
    private String message_unread;

    @JsonIgnore
    private boolean message_unreadDirtyFlag;

    /**
     * 属性 [MESSAGE_IS_FOLLOWER]
     *
     */
    @Hr_expenseMessage_is_followerDefault(info = "默认规则")
    private String message_is_follower;

    @JsonIgnore
    private boolean message_is_followerDirtyFlag;

    /**
     * 属性 [UNTAXED_AMOUNT]
     *
     */
    @Hr_expenseUntaxed_amountDefault(info = "默认规则")
    private Double untaxed_amount;

    @JsonIgnore
    private boolean untaxed_amountDirtyFlag;

    /**
     * 属性 [MESSAGE_HAS_ERROR_COUNTER]
     *
     */
    @Hr_expenseMessage_has_error_counterDefault(info = "默认规则")
    private Integer message_has_error_counter;

    @JsonIgnore
    private boolean message_has_error_counterDirtyFlag;

    /**
     * 属性 [MESSAGE_MAIN_ATTACHMENT_ID]
     *
     */
    @Hr_expenseMessage_main_attachment_idDefault(info = "默认规则")
    private Integer message_main_attachment_id;

    @JsonIgnore
    private boolean message_main_attachment_idDirtyFlag;

    /**
     * 属性 [ANALYTIC_TAG_IDS]
     *
     */
    @Hr_expenseAnalytic_tag_idsDefault(info = "默认规则")
    private String analytic_tag_ids;

    @JsonIgnore
    private boolean analytic_tag_idsDirtyFlag;

    /**
     * 属性 [DESCRIPTION]
     *
     */
    @Hr_expenseDescriptionDefault(info = "默认规则")
    private String description;

    @JsonIgnore
    private boolean descriptionDirtyFlag;

    /**
     * 属性 [REFERENCE]
     *
     */
    @Hr_expenseReferenceDefault(info = "默认规则")
    private String reference;

    @JsonIgnore
    private boolean referenceDirtyFlag;

    /**
     * 属性 [ACTIVITY_USER_ID]
     *
     */
    @Hr_expenseActivity_user_idDefault(info = "默认规则")
    private Integer activity_user_id;

    @JsonIgnore
    private boolean activity_user_idDirtyFlag;

    /**
     * 属性 [MESSAGE_UNREAD_COUNTER]
     *
     */
    @Hr_expenseMessage_unread_counterDefault(info = "默认规则")
    private Integer message_unread_counter;

    @JsonIgnore
    private boolean message_unread_counterDirtyFlag;

    /**
     * 属性 [ACTIVITY_TYPE_ID]
     *
     */
    @Hr_expenseActivity_type_idDefault(info = "默认规则")
    private Integer activity_type_id;

    @JsonIgnore
    private boolean activity_type_idDirtyFlag;

    /**
     * 属性 [SHEET_ID_TEXT]
     *
     */
    @Hr_expenseSheet_id_textDefault(info = "默认规则")
    private String sheet_id_text;

    @JsonIgnore
    private boolean sheet_id_textDirtyFlag;

    /**
     * 属性 [CURRENCY_ID_TEXT]
     *
     */
    @Hr_expenseCurrency_id_textDefault(info = "默认规则")
    private String currency_id_text;

    @JsonIgnore
    private boolean currency_id_textDirtyFlag;

    /**
     * 属性 [COMPANY_ID_TEXT]
     *
     */
    @Hr_expenseCompany_id_textDefault(info = "默认规则")
    private String company_id_text;

    @JsonIgnore
    private boolean company_id_textDirtyFlag;

    /**
     * 属性 [PRODUCT_ID_TEXT]
     *
     */
    @Hr_expenseProduct_id_textDefault(info = "默认规则")
    private String product_id_text;

    @JsonIgnore
    private boolean product_id_textDirtyFlag;

    /**
     * 属性 [CREATE_UID_TEXT]
     *
     */
    @Hr_expenseCreate_uid_textDefault(info = "默认规则")
    private String create_uid_text;

    @JsonIgnore
    private boolean create_uid_textDirtyFlag;

    /**
     * 属性 [COMPANY_CURRENCY_ID_TEXT]
     *
     */
    @Hr_expenseCompany_currency_id_textDefault(info = "默认规则")
    private String company_currency_id_text;

    @JsonIgnore
    private boolean company_currency_id_textDirtyFlag;

    /**
     * 属性 [EMPLOYEE_ID_TEXT]
     *
     */
    @Hr_expenseEmployee_id_textDefault(info = "默认规则")
    private String employee_id_text;

    @JsonIgnore
    private boolean employee_id_textDirtyFlag;

    /**
     * 属性 [PRODUCT_UOM_ID_TEXT]
     *
     */
    @Hr_expenseProduct_uom_id_textDefault(info = "默认规则")
    private String product_uom_id_text;

    @JsonIgnore
    private boolean product_uom_id_textDirtyFlag;

    /**
     * 属性 [SALE_ORDER_ID_TEXT]
     *
     */
    @Hr_expenseSale_order_id_textDefault(info = "默认规则")
    private String sale_order_id_text;

    @JsonIgnore
    private boolean sale_order_id_textDirtyFlag;

    /**
     * 属性 [ANALYTIC_ACCOUNT_ID_TEXT]
     *
     */
    @Hr_expenseAnalytic_account_id_textDefault(info = "默认规则")
    private String analytic_account_id_text;

    @JsonIgnore
    private boolean analytic_account_id_textDirtyFlag;

    /**
     * 属性 [WRITE_UID_TEXT]
     *
     */
    @Hr_expenseWrite_uid_textDefault(info = "默认规则")
    private String write_uid_text;

    @JsonIgnore
    private boolean write_uid_textDirtyFlag;

    /**
     * 属性 [ACCOUNT_ID_TEXT]
     *
     */
    @Hr_expenseAccount_id_textDefault(info = "默认规则")
    private String account_id_text;

    @JsonIgnore
    private boolean account_id_textDirtyFlag;

    /**
     * 属性 [CREATE_UID]
     *
     */
    @Hr_expenseCreate_uidDefault(info = "默认规则")
    private Integer create_uid;

    @JsonIgnore
    private boolean create_uidDirtyFlag;

    /**
     * 属性 [EMPLOYEE_ID]
     *
     */
    @Hr_expenseEmployee_idDefault(info = "默认规则")
    private Integer employee_id;

    @JsonIgnore
    private boolean employee_idDirtyFlag;

    /**
     * 属性 [SALE_ORDER_ID]
     *
     */
    @Hr_expenseSale_order_idDefault(info = "默认规则")
    private Integer sale_order_id;

    @JsonIgnore
    private boolean sale_order_idDirtyFlag;

    /**
     * 属性 [COMPANY_ID]
     *
     */
    @Hr_expenseCompany_idDefault(info = "默认规则")
    private Integer company_id;

    @JsonIgnore
    private boolean company_idDirtyFlag;

    /**
     * 属性 [CURRENCY_ID]
     *
     */
    @Hr_expenseCurrency_idDefault(info = "默认规则")
    private Integer currency_id;

    @JsonIgnore
    private boolean currency_idDirtyFlag;

    /**
     * 属性 [PRODUCT_ID]
     *
     */
    @Hr_expenseProduct_idDefault(info = "默认规则")
    private Integer product_id;

    @JsonIgnore
    private boolean product_idDirtyFlag;

    /**
     * 属性 [PRODUCT_UOM_ID]
     *
     */
    @Hr_expenseProduct_uom_idDefault(info = "默认规则")
    private Integer product_uom_id;

    @JsonIgnore
    private boolean product_uom_idDirtyFlag;

    /**
     * 属性 [SHEET_ID]
     *
     */
    @Hr_expenseSheet_idDefault(info = "默认规则")
    private Integer sheet_id;

    @JsonIgnore
    private boolean sheet_idDirtyFlag;

    /**
     * 属性 [WRITE_UID]
     *
     */
    @Hr_expenseWrite_uidDefault(info = "默认规则")
    private Integer write_uid;

    @JsonIgnore
    private boolean write_uidDirtyFlag;

    /**
     * 属性 [ACCOUNT_ID]
     *
     */
    @Hr_expenseAccount_idDefault(info = "默认规则")
    private Integer account_id;

    @JsonIgnore
    private boolean account_idDirtyFlag;

    /**
     * 属性 [COMPANY_CURRENCY_ID]
     *
     */
    @Hr_expenseCompany_currency_idDefault(info = "默认规则")
    private Integer company_currency_id;

    @JsonIgnore
    private boolean company_currency_idDirtyFlag;

    /**
     * 属性 [ANALYTIC_ACCOUNT_ID]
     *
     */
    @Hr_expenseAnalytic_account_idDefault(info = "默认规则")
    private Integer analytic_account_id;

    @JsonIgnore
    private boolean analytic_account_idDirtyFlag;


    /**
     * 获取 [ATTACHMENT_NUMBER]
     */
    @JsonProperty("attachment_number")
    public Integer getAttachment_number(){
        return attachment_number ;
    }

    /**
     * 设置 [ATTACHMENT_NUMBER]
     */
    @JsonProperty("attachment_number")
    public void setAttachment_number(Integer  attachment_number){
        this.attachment_number = attachment_number ;
        this.attachment_numberDirtyFlag = true ;
    }

    /**
     * 获取 [ATTACHMENT_NUMBER]脏标记
     */
    @JsonIgnore
    public boolean getAttachment_numberDirtyFlag(){
        return attachment_numberDirtyFlag ;
    }

    /**
     * 获取 [STATE]
     */
    @JsonProperty("state")
    public String getState(){
        return state ;
    }

    /**
     * 设置 [STATE]
     */
    @JsonProperty("state")
    public void setState(String  state){
        this.state = state ;
        this.stateDirtyFlag = true ;
    }

    /**
     * 获取 [STATE]脏标记
     */
    @JsonIgnore
    public boolean getStateDirtyFlag(){
        return stateDirtyFlag ;
    }

    /**
     * 获取 [NAME]
     */
    @JsonProperty("name")
    public String getName(){
        return name ;
    }

    /**
     * 设置 [NAME]
     */
    @JsonProperty("name")
    public void setName(String  name){
        this.name = name ;
        this.nameDirtyFlag = true ;
    }

    /**
     * 获取 [NAME]脏标记
     */
    @JsonIgnore
    public boolean getNameDirtyFlag(){
        return nameDirtyFlag ;
    }

    /**
     * 获取 [ACTIVITY_IDS]
     */
    @JsonProperty("activity_ids")
    public String getActivity_ids(){
        return activity_ids ;
    }

    /**
     * 设置 [ACTIVITY_IDS]
     */
    @JsonProperty("activity_ids")
    public void setActivity_ids(String  activity_ids){
        this.activity_ids = activity_ids ;
        this.activity_idsDirtyFlag = true ;
    }

    /**
     * 获取 [ACTIVITY_IDS]脏标记
     */
    @JsonIgnore
    public boolean getActivity_idsDirtyFlag(){
        return activity_idsDirtyFlag ;
    }

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

    /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return idDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_PARTNER_IDS]
     */
    @JsonProperty("message_partner_ids")
    public String getMessage_partner_ids(){
        return message_partner_ids ;
    }

    /**
     * 设置 [MESSAGE_PARTNER_IDS]
     */
    @JsonProperty("message_partner_ids")
    public void setMessage_partner_ids(String  message_partner_ids){
        this.message_partner_ids = message_partner_ids ;
        this.message_partner_idsDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_PARTNER_IDS]脏标记
     */
    @JsonIgnore
    public boolean getMessage_partner_idsDirtyFlag(){
        return message_partner_idsDirtyFlag ;
    }

    /**
     * 获取 [ACTIVITY_SUMMARY]
     */
    @JsonProperty("activity_summary")
    public String getActivity_summary(){
        return activity_summary ;
    }

    /**
     * 设置 [ACTIVITY_SUMMARY]
     */
    @JsonProperty("activity_summary")
    public void setActivity_summary(String  activity_summary){
        this.activity_summary = activity_summary ;
        this.activity_summaryDirtyFlag = true ;
    }

    /**
     * 获取 [ACTIVITY_SUMMARY]脏标记
     */
    @JsonIgnore
    public boolean getActivity_summaryDirtyFlag(){
        return activity_summaryDirtyFlag ;
    }

    /**
     * 获取 [WEBSITE_MESSAGE_IDS]
     */
    @JsonProperty("website_message_ids")
    public String getWebsite_message_ids(){
        return website_message_ids ;
    }

    /**
     * 设置 [WEBSITE_MESSAGE_IDS]
     */
    @JsonProperty("website_message_ids")
    public void setWebsite_message_ids(String  website_message_ids){
        this.website_message_ids = website_message_ids ;
        this.website_message_idsDirtyFlag = true ;
    }

    /**
     * 获取 [WEBSITE_MESSAGE_IDS]脏标记
     */
    @JsonIgnore
    public boolean getWebsite_message_idsDirtyFlag(){
        return website_message_idsDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_NEEDACTION]
     */
    @JsonProperty("message_needaction")
    public String getMessage_needaction(){
        return message_needaction ;
    }

    /**
     * 设置 [MESSAGE_NEEDACTION]
     */
    @JsonProperty("message_needaction")
    public void setMessage_needaction(String  message_needaction){
        this.message_needaction = message_needaction ;
        this.message_needactionDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_NEEDACTION]脏标记
     */
    @JsonIgnore
    public boolean getMessage_needactionDirtyFlag(){
        return message_needactionDirtyFlag ;
    }

    /**
     * 获取 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return display_name ;
    }

    /**
     * 设置 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

    /**
     * 获取 [DISPLAY_NAME]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return display_nameDirtyFlag ;
    }

    /**
     * 获取 [ACTIVITY_STATE]
     */
    @JsonProperty("activity_state")
    public String getActivity_state(){
        return activity_state ;
    }

    /**
     * 设置 [ACTIVITY_STATE]
     */
    @JsonProperty("activity_state")
    public void setActivity_state(String  activity_state){
        this.activity_state = activity_state ;
        this.activity_stateDirtyFlag = true ;
    }

    /**
     * 获取 [ACTIVITY_STATE]脏标记
     */
    @JsonIgnore
    public boolean getActivity_stateDirtyFlag(){
        return activity_stateDirtyFlag ;
    }

    /**
     * 获取 [TOTAL_AMOUNT]
     */
    @JsonProperty("total_amount")
    public Double getTotal_amount(){
        return total_amount ;
    }

    /**
     * 设置 [TOTAL_AMOUNT]
     */
    @JsonProperty("total_amount")
    public void setTotal_amount(Double  total_amount){
        this.total_amount = total_amount ;
        this.total_amountDirtyFlag = true ;
    }

    /**
     * 获取 [TOTAL_AMOUNT]脏标记
     */
    @JsonIgnore
    public boolean getTotal_amountDirtyFlag(){
        return total_amountDirtyFlag ;
    }

    /**
     * 获取 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return __last_update ;
    }

    /**
     * 设置 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

    /**
     * 获取 [__LAST_UPDATE]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return __last_updateDirtyFlag ;
    }

    /**
     * 获取 [ACTIVITY_DATE_DEADLINE]
     */
    @JsonProperty("activity_date_deadline")
    public Timestamp getActivity_date_deadline(){
        return activity_date_deadline ;
    }

    /**
     * 设置 [ACTIVITY_DATE_DEADLINE]
     */
    @JsonProperty("activity_date_deadline")
    public void setActivity_date_deadline(Timestamp  activity_date_deadline){
        this.activity_date_deadline = activity_date_deadline ;
        this.activity_date_deadlineDirtyFlag = true ;
    }

    /**
     * 获取 [ACTIVITY_DATE_DEADLINE]脏标记
     */
    @JsonIgnore
    public boolean getActivity_date_deadlineDirtyFlag(){
        return activity_date_deadlineDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_FOLLOWER_IDS]
     */
    @JsonProperty("message_follower_ids")
    public String getMessage_follower_ids(){
        return message_follower_ids ;
    }

    /**
     * 设置 [MESSAGE_FOLLOWER_IDS]
     */
    @JsonProperty("message_follower_ids")
    public void setMessage_follower_ids(String  message_follower_ids){
        this.message_follower_ids = message_follower_ids ;
        this.message_follower_idsDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_FOLLOWER_IDS]脏标记
     */
    @JsonIgnore
    public boolean getMessage_follower_idsDirtyFlag(){
        return message_follower_idsDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_HAS_ERROR]
     */
    @JsonProperty("message_has_error")
    public String getMessage_has_error(){
        return message_has_error ;
    }

    /**
     * 设置 [MESSAGE_HAS_ERROR]
     */
    @JsonProperty("message_has_error")
    public void setMessage_has_error(String  message_has_error){
        this.message_has_error = message_has_error ;
        this.message_has_errorDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_HAS_ERROR]脏标记
     */
    @JsonIgnore
    public boolean getMessage_has_errorDirtyFlag(){
        return message_has_errorDirtyFlag ;
    }

    /**
     * 获取 [TAX_IDS]
     */
    @JsonProperty("tax_ids")
    public String getTax_ids(){
        return tax_ids ;
    }

    /**
     * 设置 [TAX_IDS]
     */
    @JsonProperty("tax_ids")
    public void setTax_ids(String  tax_ids){
        this.tax_ids = tax_ids ;
        this.tax_idsDirtyFlag = true ;
    }

    /**
     * 获取 [TAX_IDS]脏标记
     */
    @JsonIgnore
    public boolean getTax_idsDirtyFlag(){
        return tax_idsDirtyFlag ;
    }

    /**
     * 获取 [UNIT_AMOUNT]
     */
    @JsonProperty("unit_amount")
    public Double getUnit_amount(){
        return unit_amount ;
    }

    /**
     * 设置 [UNIT_AMOUNT]
     */
    @JsonProperty("unit_amount")
    public void setUnit_amount(Double  unit_amount){
        this.unit_amount = unit_amount ;
        this.unit_amountDirtyFlag = true ;
    }

    /**
     * 获取 [UNIT_AMOUNT]脏标记
     */
    @JsonIgnore
    public boolean getUnit_amountDirtyFlag(){
        return unit_amountDirtyFlag ;
    }

    /**
     * 获取 [PAYMENT_MODE]
     */
    @JsonProperty("payment_mode")
    public String getPayment_mode(){
        return payment_mode ;
    }

    /**
     * 设置 [PAYMENT_MODE]
     */
    @JsonProperty("payment_mode")
    public void setPayment_mode(String  payment_mode){
        this.payment_mode = payment_mode ;
        this.payment_modeDirtyFlag = true ;
    }

    /**
     * 获取 [PAYMENT_MODE]脏标记
     */
    @JsonIgnore
    public boolean getPayment_modeDirtyFlag(){
        return payment_modeDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_CHANNEL_IDS]
     */
    @JsonProperty("message_channel_ids")
    public String getMessage_channel_ids(){
        return message_channel_ids ;
    }

    /**
     * 设置 [MESSAGE_CHANNEL_IDS]
     */
    @JsonProperty("message_channel_ids")
    public void setMessage_channel_ids(String  message_channel_ids){
        this.message_channel_ids = message_channel_ids ;
        this.message_channel_idsDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_CHANNEL_IDS]脏标记
     */
    @JsonIgnore
    public boolean getMessage_channel_idsDirtyFlag(){
        return message_channel_idsDirtyFlag ;
    }

    /**
     * 获取 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return create_date ;
    }

    /**
     * 设置 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return create_dateDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_ATTACHMENT_COUNT]
     */
    @JsonProperty("message_attachment_count")
    public Integer getMessage_attachment_count(){
        return message_attachment_count ;
    }

    /**
     * 设置 [MESSAGE_ATTACHMENT_COUNT]
     */
    @JsonProperty("message_attachment_count")
    public void setMessage_attachment_count(Integer  message_attachment_count){
        this.message_attachment_count = message_attachment_count ;
        this.message_attachment_countDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_ATTACHMENT_COUNT]脏标记
     */
    @JsonIgnore
    public boolean getMessage_attachment_countDirtyFlag(){
        return message_attachment_countDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_NEEDACTION_COUNTER]
     */
    @JsonProperty("message_needaction_counter")
    public Integer getMessage_needaction_counter(){
        return message_needaction_counter ;
    }

    /**
     * 设置 [MESSAGE_NEEDACTION_COUNTER]
     */
    @JsonProperty("message_needaction_counter")
    public void setMessage_needaction_counter(Integer  message_needaction_counter){
        this.message_needaction_counter = message_needaction_counter ;
        this.message_needaction_counterDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_NEEDACTION_COUNTER]脏标记
     */
    @JsonIgnore
    public boolean getMessage_needaction_counterDirtyFlag(){
        return message_needaction_counterDirtyFlag ;
    }

    /**
     * 获取 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return write_date ;
    }

    /**
     * 设置 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return write_dateDirtyFlag ;
    }

    /**
     * 获取 [TOTAL_AMOUNT_COMPANY]
     */
    @JsonProperty("total_amount_company")
    public Double getTotal_amount_company(){
        return total_amount_company ;
    }

    /**
     * 设置 [TOTAL_AMOUNT_COMPANY]
     */
    @JsonProperty("total_amount_company")
    public void setTotal_amount_company(Double  total_amount_company){
        this.total_amount_company = total_amount_company ;
        this.total_amount_companyDirtyFlag = true ;
    }

    /**
     * 获取 [TOTAL_AMOUNT_COMPANY]脏标记
     */
    @JsonIgnore
    public boolean getTotal_amount_companyDirtyFlag(){
        return total_amount_companyDirtyFlag ;
    }

    /**
     * 获取 [IS_REFUSED]
     */
    @JsonProperty("is_refused")
    public String getIs_refused(){
        return is_refused ;
    }

    /**
     * 设置 [IS_REFUSED]
     */
    @JsonProperty("is_refused")
    public void setIs_refused(String  is_refused){
        this.is_refused = is_refused ;
        this.is_refusedDirtyFlag = true ;
    }

    /**
     * 获取 [IS_REFUSED]脏标记
     */
    @JsonIgnore
    public boolean getIs_refusedDirtyFlag(){
        return is_refusedDirtyFlag ;
    }

    /**
     * 获取 [DATE]
     */
    @JsonProperty("date")
    public Timestamp getDate(){
        return date ;
    }

    /**
     * 设置 [DATE]
     */
    @JsonProperty("date")
    public void setDate(Timestamp  date){
        this.date = date ;
        this.dateDirtyFlag = true ;
    }

    /**
     * 获取 [DATE]脏标记
     */
    @JsonIgnore
    public boolean getDateDirtyFlag(){
        return dateDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_IDS]
     */
    @JsonProperty("message_ids")
    public String getMessage_ids(){
        return message_ids ;
    }

    /**
     * 设置 [MESSAGE_IDS]
     */
    @JsonProperty("message_ids")
    public void setMessage_ids(String  message_ids){
        this.message_ids = message_ids ;
        this.message_idsDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_IDS]脏标记
     */
    @JsonIgnore
    public boolean getMessage_idsDirtyFlag(){
        return message_idsDirtyFlag ;
    }

    /**
     * 获取 [QUANTITY]
     */
    @JsonProperty("quantity")
    public Double getQuantity(){
        return quantity ;
    }

    /**
     * 设置 [QUANTITY]
     */
    @JsonProperty("quantity")
    public void setQuantity(Double  quantity){
        this.quantity = quantity ;
        this.quantityDirtyFlag = true ;
    }

    /**
     * 获取 [QUANTITY]脏标记
     */
    @JsonIgnore
    public boolean getQuantityDirtyFlag(){
        return quantityDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_UNREAD]
     */
    @JsonProperty("message_unread")
    public String getMessage_unread(){
        return message_unread ;
    }

    /**
     * 设置 [MESSAGE_UNREAD]
     */
    @JsonProperty("message_unread")
    public void setMessage_unread(String  message_unread){
        this.message_unread = message_unread ;
        this.message_unreadDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_UNREAD]脏标记
     */
    @JsonIgnore
    public boolean getMessage_unreadDirtyFlag(){
        return message_unreadDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_IS_FOLLOWER]
     */
    @JsonProperty("message_is_follower")
    public String getMessage_is_follower(){
        return message_is_follower ;
    }

    /**
     * 设置 [MESSAGE_IS_FOLLOWER]
     */
    @JsonProperty("message_is_follower")
    public void setMessage_is_follower(String  message_is_follower){
        this.message_is_follower = message_is_follower ;
        this.message_is_followerDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_IS_FOLLOWER]脏标记
     */
    @JsonIgnore
    public boolean getMessage_is_followerDirtyFlag(){
        return message_is_followerDirtyFlag ;
    }

    /**
     * 获取 [UNTAXED_AMOUNT]
     */
    @JsonProperty("untaxed_amount")
    public Double getUntaxed_amount(){
        return untaxed_amount ;
    }

    /**
     * 设置 [UNTAXED_AMOUNT]
     */
    @JsonProperty("untaxed_amount")
    public void setUntaxed_amount(Double  untaxed_amount){
        this.untaxed_amount = untaxed_amount ;
        this.untaxed_amountDirtyFlag = true ;
    }

    /**
     * 获取 [UNTAXED_AMOUNT]脏标记
     */
    @JsonIgnore
    public boolean getUntaxed_amountDirtyFlag(){
        return untaxed_amountDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_HAS_ERROR_COUNTER]
     */
    @JsonProperty("message_has_error_counter")
    public Integer getMessage_has_error_counter(){
        return message_has_error_counter ;
    }

    /**
     * 设置 [MESSAGE_HAS_ERROR_COUNTER]
     */
    @JsonProperty("message_has_error_counter")
    public void setMessage_has_error_counter(Integer  message_has_error_counter){
        this.message_has_error_counter = message_has_error_counter ;
        this.message_has_error_counterDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_HAS_ERROR_COUNTER]脏标记
     */
    @JsonIgnore
    public boolean getMessage_has_error_counterDirtyFlag(){
        return message_has_error_counterDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_MAIN_ATTACHMENT_ID]
     */
    @JsonProperty("message_main_attachment_id")
    public Integer getMessage_main_attachment_id(){
        return message_main_attachment_id ;
    }

    /**
     * 设置 [MESSAGE_MAIN_ATTACHMENT_ID]
     */
    @JsonProperty("message_main_attachment_id")
    public void setMessage_main_attachment_id(Integer  message_main_attachment_id){
        this.message_main_attachment_id = message_main_attachment_id ;
        this.message_main_attachment_idDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_MAIN_ATTACHMENT_ID]脏标记
     */
    @JsonIgnore
    public boolean getMessage_main_attachment_idDirtyFlag(){
        return message_main_attachment_idDirtyFlag ;
    }

    /**
     * 获取 [ANALYTIC_TAG_IDS]
     */
    @JsonProperty("analytic_tag_ids")
    public String getAnalytic_tag_ids(){
        return analytic_tag_ids ;
    }

    /**
     * 设置 [ANALYTIC_TAG_IDS]
     */
    @JsonProperty("analytic_tag_ids")
    public void setAnalytic_tag_ids(String  analytic_tag_ids){
        this.analytic_tag_ids = analytic_tag_ids ;
        this.analytic_tag_idsDirtyFlag = true ;
    }

    /**
     * 获取 [ANALYTIC_TAG_IDS]脏标记
     */
    @JsonIgnore
    public boolean getAnalytic_tag_idsDirtyFlag(){
        return analytic_tag_idsDirtyFlag ;
    }

    /**
     * 获取 [DESCRIPTION]
     */
    @JsonProperty("description")
    public String getDescription(){
        return description ;
    }

    /**
     * 设置 [DESCRIPTION]
     */
    @JsonProperty("description")
    public void setDescription(String  description){
        this.description = description ;
        this.descriptionDirtyFlag = true ;
    }

    /**
     * 获取 [DESCRIPTION]脏标记
     */
    @JsonIgnore
    public boolean getDescriptionDirtyFlag(){
        return descriptionDirtyFlag ;
    }

    /**
     * 获取 [REFERENCE]
     */
    @JsonProperty("reference")
    public String getReference(){
        return reference ;
    }

    /**
     * 设置 [REFERENCE]
     */
    @JsonProperty("reference")
    public void setReference(String  reference){
        this.reference = reference ;
        this.referenceDirtyFlag = true ;
    }

    /**
     * 获取 [REFERENCE]脏标记
     */
    @JsonIgnore
    public boolean getReferenceDirtyFlag(){
        return referenceDirtyFlag ;
    }

    /**
     * 获取 [ACTIVITY_USER_ID]
     */
    @JsonProperty("activity_user_id")
    public Integer getActivity_user_id(){
        return activity_user_id ;
    }

    /**
     * 设置 [ACTIVITY_USER_ID]
     */
    @JsonProperty("activity_user_id")
    public void setActivity_user_id(Integer  activity_user_id){
        this.activity_user_id = activity_user_id ;
        this.activity_user_idDirtyFlag = true ;
    }

    /**
     * 获取 [ACTIVITY_USER_ID]脏标记
     */
    @JsonIgnore
    public boolean getActivity_user_idDirtyFlag(){
        return activity_user_idDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_UNREAD_COUNTER]
     */
    @JsonProperty("message_unread_counter")
    public Integer getMessage_unread_counter(){
        return message_unread_counter ;
    }

    /**
     * 设置 [MESSAGE_UNREAD_COUNTER]
     */
    @JsonProperty("message_unread_counter")
    public void setMessage_unread_counter(Integer  message_unread_counter){
        this.message_unread_counter = message_unread_counter ;
        this.message_unread_counterDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_UNREAD_COUNTER]脏标记
     */
    @JsonIgnore
    public boolean getMessage_unread_counterDirtyFlag(){
        return message_unread_counterDirtyFlag ;
    }

    /**
     * 获取 [ACTIVITY_TYPE_ID]
     */
    @JsonProperty("activity_type_id")
    public Integer getActivity_type_id(){
        return activity_type_id ;
    }

    /**
     * 设置 [ACTIVITY_TYPE_ID]
     */
    @JsonProperty("activity_type_id")
    public void setActivity_type_id(Integer  activity_type_id){
        this.activity_type_id = activity_type_id ;
        this.activity_type_idDirtyFlag = true ;
    }

    /**
     * 获取 [ACTIVITY_TYPE_ID]脏标记
     */
    @JsonIgnore
    public boolean getActivity_type_idDirtyFlag(){
        return activity_type_idDirtyFlag ;
    }

    /**
     * 获取 [SHEET_ID_TEXT]
     */
    @JsonProperty("sheet_id_text")
    public String getSheet_id_text(){
        return sheet_id_text ;
    }

    /**
     * 设置 [SHEET_ID_TEXT]
     */
    @JsonProperty("sheet_id_text")
    public void setSheet_id_text(String  sheet_id_text){
        this.sheet_id_text = sheet_id_text ;
        this.sheet_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [SHEET_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getSheet_id_textDirtyFlag(){
        return sheet_id_textDirtyFlag ;
    }

    /**
     * 获取 [CURRENCY_ID_TEXT]
     */
    @JsonProperty("currency_id_text")
    public String getCurrency_id_text(){
        return currency_id_text ;
    }

    /**
     * 设置 [CURRENCY_ID_TEXT]
     */
    @JsonProperty("currency_id_text")
    public void setCurrency_id_text(String  currency_id_text){
        this.currency_id_text = currency_id_text ;
        this.currency_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [CURRENCY_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getCurrency_id_textDirtyFlag(){
        return currency_id_textDirtyFlag ;
    }

    /**
     * 获取 [COMPANY_ID_TEXT]
     */
    @JsonProperty("company_id_text")
    public String getCompany_id_text(){
        return company_id_text ;
    }

    /**
     * 设置 [COMPANY_ID_TEXT]
     */
    @JsonProperty("company_id_text")
    public void setCompany_id_text(String  company_id_text){
        this.company_id_text = company_id_text ;
        this.company_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [COMPANY_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getCompany_id_textDirtyFlag(){
        return company_id_textDirtyFlag ;
    }

    /**
     * 获取 [PRODUCT_ID_TEXT]
     */
    @JsonProperty("product_id_text")
    public String getProduct_id_text(){
        return product_id_text ;
    }

    /**
     * 设置 [PRODUCT_ID_TEXT]
     */
    @JsonProperty("product_id_text")
    public void setProduct_id_text(String  product_id_text){
        this.product_id_text = product_id_text ;
        this.product_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [PRODUCT_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getProduct_id_textDirtyFlag(){
        return product_id_textDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return create_uid_text ;
    }

    /**
     * 设置 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return create_uid_textDirtyFlag ;
    }

    /**
     * 获取 [COMPANY_CURRENCY_ID_TEXT]
     */
    @JsonProperty("company_currency_id_text")
    public String getCompany_currency_id_text(){
        return company_currency_id_text ;
    }

    /**
     * 设置 [COMPANY_CURRENCY_ID_TEXT]
     */
    @JsonProperty("company_currency_id_text")
    public void setCompany_currency_id_text(String  company_currency_id_text){
        this.company_currency_id_text = company_currency_id_text ;
        this.company_currency_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [COMPANY_CURRENCY_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getCompany_currency_id_textDirtyFlag(){
        return company_currency_id_textDirtyFlag ;
    }

    /**
     * 获取 [EMPLOYEE_ID_TEXT]
     */
    @JsonProperty("employee_id_text")
    public String getEmployee_id_text(){
        return employee_id_text ;
    }

    /**
     * 设置 [EMPLOYEE_ID_TEXT]
     */
    @JsonProperty("employee_id_text")
    public void setEmployee_id_text(String  employee_id_text){
        this.employee_id_text = employee_id_text ;
        this.employee_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [EMPLOYEE_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getEmployee_id_textDirtyFlag(){
        return employee_id_textDirtyFlag ;
    }

    /**
     * 获取 [PRODUCT_UOM_ID_TEXT]
     */
    @JsonProperty("product_uom_id_text")
    public String getProduct_uom_id_text(){
        return product_uom_id_text ;
    }

    /**
     * 设置 [PRODUCT_UOM_ID_TEXT]
     */
    @JsonProperty("product_uom_id_text")
    public void setProduct_uom_id_text(String  product_uom_id_text){
        this.product_uom_id_text = product_uom_id_text ;
        this.product_uom_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [PRODUCT_UOM_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getProduct_uom_id_textDirtyFlag(){
        return product_uom_id_textDirtyFlag ;
    }

    /**
     * 获取 [SALE_ORDER_ID_TEXT]
     */
    @JsonProperty("sale_order_id_text")
    public String getSale_order_id_text(){
        return sale_order_id_text ;
    }

    /**
     * 设置 [SALE_ORDER_ID_TEXT]
     */
    @JsonProperty("sale_order_id_text")
    public void setSale_order_id_text(String  sale_order_id_text){
        this.sale_order_id_text = sale_order_id_text ;
        this.sale_order_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [SALE_ORDER_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getSale_order_id_textDirtyFlag(){
        return sale_order_id_textDirtyFlag ;
    }

    /**
     * 获取 [ANALYTIC_ACCOUNT_ID_TEXT]
     */
    @JsonProperty("analytic_account_id_text")
    public String getAnalytic_account_id_text(){
        return analytic_account_id_text ;
    }

    /**
     * 设置 [ANALYTIC_ACCOUNT_ID_TEXT]
     */
    @JsonProperty("analytic_account_id_text")
    public void setAnalytic_account_id_text(String  analytic_account_id_text){
        this.analytic_account_id_text = analytic_account_id_text ;
        this.analytic_account_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [ANALYTIC_ACCOUNT_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getAnalytic_account_id_textDirtyFlag(){
        return analytic_account_id_textDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return write_uid_text ;
    }

    /**
     * 设置 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return write_uid_textDirtyFlag ;
    }

    /**
     * 获取 [ACCOUNT_ID_TEXT]
     */
    @JsonProperty("account_id_text")
    public String getAccount_id_text(){
        return account_id_text ;
    }

    /**
     * 设置 [ACCOUNT_ID_TEXT]
     */
    @JsonProperty("account_id_text")
    public void setAccount_id_text(String  account_id_text){
        this.account_id_text = account_id_text ;
        this.account_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [ACCOUNT_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getAccount_id_textDirtyFlag(){
        return account_id_textDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return create_uid ;
    }

    /**
     * 设置 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return create_uidDirtyFlag ;
    }

    /**
     * 获取 [EMPLOYEE_ID]
     */
    @JsonProperty("employee_id")
    public Integer getEmployee_id(){
        return employee_id ;
    }

    /**
     * 设置 [EMPLOYEE_ID]
     */
    @JsonProperty("employee_id")
    public void setEmployee_id(Integer  employee_id){
        this.employee_id = employee_id ;
        this.employee_idDirtyFlag = true ;
    }

    /**
     * 获取 [EMPLOYEE_ID]脏标记
     */
    @JsonIgnore
    public boolean getEmployee_idDirtyFlag(){
        return employee_idDirtyFlag ;
    }

    /**
     * 获取 [SALE_ORDER_ID]
     */
    @JsonProperty("sale_order_id")
    public Integer getSale_order_id(){
        return sale_order_id ;
    }

    /**
     * 设置 [SALE_ORDER_ID]
     */
    @JsonProperty("sale_order_id")
    public void setSale_order_id(Integer  sale_order_id){
        this.sale_order_id = sale_order_id ;
        this.sale_order_idDirtyFlag = true ;
    }

    /**
     * 获取 [SALE_ORDER_ID]脏标记
     */
    @JsonIgnore
    public boolean getSale_order_idDirtyFlag(){
        return sale_order_idDirtyFlag ;
    }

    /**
     * 获取 [COMPANY_ID]
     */
    @JsonProperty("company_id")
    public Integer getCompany_id(){
        return company_id ;
    }

    /**
     * 设置 [COMPANY_ID]
     */
    @JsonProperty("company_id")
    public void setCompany_id(Integer  company_id){
        this.company_id = company_id ;
        this.company_idDirtyFlag = true ;
    }

    /**
     * 获取 [COMPANY_ID]脏标记
     */
    @JsonIgnore
    public boolean getCompany_idDirtyFlag(){
        return company_idDirtyFlag ;
    }

    /**
     * 获取 [CURRENCY_ID]
     */
    @JsonProperty("currency_id")
    public Integer getCurrency_id(){
        return currency_id ;
    }

    /**
     * 设置 [CURRENCY_ID]
     */
    @JsonProperty("currency_id")
    public void setCurrency_id(Integer  currency_id){
        this.currency_id = currency_id ;
        this.currency_idDirtyFlag = true ;
    }

    /**
     * 获取 [CURRENCY_ID]脏标记
     */
    @JsonIgnore
    public boolean getCurrency_idDirtyFlag(){
        return currency_idDirtyFlag ;
    }

    /**
     * 获取 [PRODUCT_ID]
     */
    @JsonProperty("product_id")
    public Integer getProduct_id(){
        return product_id ;
    }

    /**
     * 设置 [PRODUCT_ID]
     */
    @JsonProperty("product_id")
    public void setProduct_id(Integer  product_id){
        this.product_id = product_id ;
        this.product_idDirtyFlag = true ;
    }

    /**
     * 获取 [PRODUCT_ID]脏标记
     */
    @JsonIgnore
    public boolean getProduct_idDirtyFlag(){
        return product_idDirtyFlag ;
    }

    /**
     * 获取 [PRODUCT_UOM_ID]
     */
    @JsonProperty("product_uom_id")
    public Integer getProduct_uom_id(){
        return product_uom_id ;
    }

    /**
     * 设置 [PRODUCT_UOM_ID]
     */
    @JsonProperty("product_uom_id")
    public void setProduct_uom_id(Integer  product_uom_id){
        this.product_uom_id = product_uom_id ;
        this.product_uom_idDirtyFlag = true ;
    }

    /**
     * 获取 [PRODUCT_UOM_ID]脏标记
     */
    @JsonIgnore
    public boolean getProduct_uom_idDirtyFlag(){
        return product_uom_idDirtyFlag ;
    }

    /**
     * 获取 [SHEET_ID]
     */
    @JsonProperty("sheet_id")
    public Integer getSheet_id(){
        return sheet_id ;
    }

    /**
     * 设置 [SHEET_ID]
     */
    @JsonProperty("sheet_id")
    public void setSheet_id(Integer  sheet_id){
        this.sheet_id = sheet_id ;
        this.sheet_idDirtyFlag = true ;
    }

    /**
     * 获取 [SHEET_ID]脏标记
     */
    @JsonIgnore
    public boolean getSheet_idDirtyFlag(){
        return sheet_idDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return write_uid ;
    }

    /**
     * 设置 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return write_uidDirtyFlag ;
    }

    /**
     * 获取 [ACCOUNT_ID]
     */
    @JsonProperty("account_id")
    public Integer getAccount_id(){
        return account_id ;
    }

    /**
     * 设置 [ACCOUNT_ID]
     */
    @JsonProperty("account_id")
    public void setAccount_id(Integer  account_id){
        this.account_id = account_id ;
        this.account_idDirtyFlag = true ;
    }

    /**
     * 获取 [ACCOUNT_ID]脏标记
     */
    @JsonIgnore
    public boolean getAccount_idDirtyFlag(){
        return account_idDirtyFlag ;
    }

    /**
     * 获取 [COMPANY_CURRENCY_ID]
     */
    @JsonProperty("company_currency_id")
    public Integer getCompany_currency_id(){
        return company_currency_id ;
    }

    /**
     * 设置 [COMPANY_CURRENCY_ID]
     */
    @JsonProperty("company_currency_id")
    public void setCompany_currency_id(Integer  company_currency_id){
        this.company_currency_id = company_currency_id ;
        this.company_currency_idDirtyFlag = true ;
    }

    /**
     * 获取 [COMPANY_CURRENCY_ID]脏标记
     */
    @JsonIgnore
    public boolean getCompany_currency_idDirtyFlag(){
        return company_currency_idDirtyFlag ;
    }

    /**
     * 获取 [ANALYTIC_ACCOUNT_ID]
     */
    @JsonProperty("analytic_account_id")
    public Integer getAnalytic_account_id(){
        return analytic_account_id ;
    }

    /**
     * 设置 [ANALYTIC_ACCOUNT_ID]
     */
    @JsonProperty("analytic_account_id")
    public void setAnalytic_account_id(Integer  analytic_account_id){
        this.analytic_account_id = analytic_account_id ;
        this.analytic_account_idDirtyFlag = true ;
    }

    /**
     * 获取 [ANALYTIC_ACCOUNT_ID]脏标记
     */
    @JsonIgnore
    public boolean getAnalytic_account_idDirtyFlag(){
        return analytic_account_idDirtyFlag ;
    }



    public Hr_expense toDO() {
        Hr_expense srfdomain = new Hr_expense();
        if(getAttachment_numberDirtyFlag())
            srfdomain.setAttachment_number(attachment_number);
        if(getStateDirtyFlag())
            srfdomain.setState(state);
        if(getNameDirtyFlag())
            srfdomain.setName(name);
        if(getActivity_idsDirtyFlag())
            srfdomain.setActivity_ids(activity_ids);
        if(getIdDirtyFlag())
            srfdomain.setId(id);
        if(getMessage_partner_idsDirtyFlag())
            srfdomain.setMessage_partner_ids(message_partner_ids);
        if(getActivity_summaryDirtyFlag())
            srfdomain.setActivity_summary(activity_summary);
        if(getWebsite_message_idsDirtyFlag())
            srfdomain.setWebsite_message_ids(website_message_ids);
        if(getMessage_needactionDirtyFlag())
            srfdomain.setMessage_needaction(message_needaction);
        if(getDisplay_nameDirtyFlag())
            srfdomain.setDisplay_name(display_name);
        if(getActivity_stateDirtyFlag())
            srfdomain.setActivity_state(activity_state);
        if(getTotal_amountDirtyFlag())
            srfdomain.setTotal_amount(total_amount);
        if(get__last_updateDirtyFlag())
            srfdomain.set__last_update(__last_update);
        if(getActivity_date_deadlineDirtyFlag())
            srfdomain.setActivity_date_deadline(activity_date_deadline);
        if(getMessage_follower_idsDirtyFlag())
            srfdomain.setMessage_follower_ids(message_follower_ids);
        if(getMessage_has_errorDirtyFlag())
            srfdomain.setMessage_has_error(message_has_error);
        if(getTax_idsDirtyFlag())
            srfdomain.setTax_ids(tax_ids);
        if(getUnit_amountDirtyFlag())
            srfdomain.setUnit_amount(unit_amount);
        if(getPayment_modeDirtyFlag())
            srfdomain.setPayment_mode(payment_mode);
        if(getMessage_channel_idsDirtyFlag())
            srfdomain.setMessage_channel_ids(message_channel_ids);
        if(getCreate_dateDirtyFlag())
            srfdomain.setCreate_date(create_date);
        if(getMessage_attachment_countDirtyFlag())
            srfdomain.setMessage_attachment_count(message_attachment_count);
        if(getMessage_needaction_counterDirtyFlag())
            srfdomain.setMessage_needaction_counter(message_needaction_counter);
        if(getWrite_dateDirtyFlag())
            srfdomain.setWrite_date(write_date);
        if(getTotal_amount_companyDirtyFlag())
            srfdomain.setTotal_amount_company(total_amount_company);
        if(getIs_refusedDirtyFlag())
            srfdomain.setIs_refused(is_refused);
        if(getDateDirtyFlag())
            srfdomain.setDate(date);
        if(getMessage_idsDirtyFlag())
            srfdomain.setMessage_ids(message_ids);
        if(getQuantityDirtyFlag())
            srfdomain.setQuantity(quantity);
        if(getMessage_unreadDirtyFlag())
            srfdomain.setMessage_unread(message_unread);
        if(getMessage_is_followerDirtyFlag())
            srfdomain.setMessage_is_follower(message_is_follower);
        if(getUntaxed_amountDirtyFlag())
            srfdomain.setUntaxed_amount(untaxed_amount);
        if(getMessage_has_error_counterDirtyFlag())
            srfdomain.setMessage_has_error_counter(message_has_error_counter);
        if(getMessage_main_attachment_idDirtyFlag())
            srfdomain.setMessage_main_attachment_id(message_main_attachment_id);
        if(getAnalytic_tag_idsDirtyFlag())
            srfdomain.setAnalytic_tag_ids(analytic_tag_ids);
        if(getDescriptionDirtyFlag())
            srfdomain.setDescription(description);
        if(getReferenceDirtyFlag())
            srfdomain.setReference(reference);
        if(getActivity_user_idDirtyFlag())
            srfdomain.setActivity_user_id(activity_user_id);
        if(getMessage_unread_counterDirtyFlag())
            srfdomain.setMessage_unread_counter(message_unread_counter);
        if(getActivity_type_idDirtyFlag())
            srfdomain.setActivity_type_id(activity_type_id);
        if(getSheet_id_textDirtyFlag())
            srfdomain.setSheet_id_text(sheet_id_text);
        if(getCurrency_id_textDirtyFlag())
            srfdomain.setCurrency_id_text(currency_id_text);
        if(getCompany_id_textDirtyFlag())
            srfdomain.setCompany_id_text(company_id_text);
        if(getProduct_id_textDirtyFlag())
            srfdomain.setProduct_id_text(product_id_text);
        if(getCreate_uid_textDirtyFlag())
            srfdomain.setCreate_uid_text(create_uid_text);
        if(getCompany_currency_id_textDirtyFlag())
            srfdomain.setCompany_currency_id_text(company_currency_id_text);
        if(getEmployee_id_textDirtyFlag())
            srfdomain.setEmployee_id_text(employee_id_text);
        if(getProduct_uom_id_textDirtyFlag())
            srfdomain.setProduct_uom_id_text(product_uom_id_text);
        if(getSale_order_id_textDirtyFlag())
            srfdomain.setSale_order_id_text(sale_order_id_text);
        if(getAnalytic_account_id_textDirtyFlag())
            srfdomain.setAnalytic_account_id_text(analytic_account_id_text);
        if(getWrite_uid_textDirtyFlag())
            srfdomain.setWrite_uid_text(write_uid_text);
        if(getAccount_id_textDirtyFlag())
            srfdomain.setAccount_id_text(account_id_text);
        if(getCreate_uidDirtyFlag())
            srfdomain.setCreate_uid(create_uid);
        if(getEmployee_idDirtyFlag())
            srfdomain.setEmployee_id(employee_id);
        if(getSale_order_idDirtyFlag())
            srfdomain.setSale_order_id(sale_order_id);
        if(getCompany_idDirtyFlag())
            srfdomain.setCompany_id(company_id);
        if(getCurrency_idDirtyFlag())
            srfdomain.setCurrency_id(currency_id);
        if(getProduct_idDirtyFlag())
            srfdomain.setProduct_id(product_id);
        if(getProduct_uom_idDirtyFlag())
            srfdomain.setProduct_uom_id(product_uom_id);
        if(getSheet_idDirtyFlag())
            srfdomain.setSheet_id(sheet_id);
        if(getWrite_uidDirtyFlag())
            srfdomain.setWrite_uid(write_uid);
        if(getAccount_idDirtyFlag())
            srfdomain.setAccount_id(account_id);
        if(getCompany_currency_idDirtyFlag())
            srfdomain.setCompany_currency_id(company_currency_id);
        if(getAnalytic_account_idDirtyFlag())
            srfdomain.setAnalytic_account_id(analytic_account_id);

        return srfdomain;
    }

    public void fromDO(Hr_expense srfdomain) {
        if(srfdomain == null )
          return ;
        if(srfdomain.getAttachment_numberDirtyFlag())
            this.setAttachment_number(srfdomain.getAttachment_number());
        if(srfdomain.getStateDirtyFlag())
            this.setState(srfdomain.getState());
        if(srfdomain.getNameDirtyFlag())
            this.setName(srfdomain.getName());
        if(srfdomain.getActivity_idsDirtyFlag())
            this.setActivity_ids(srfdomain.getActivity_ids());
        if(srfdomain.getIdDirtyFlag())
            this.setId(srfdomain.getId());
        if(srfdomain.getMessage_partner_idsDirtyFlag())
            this.setMessage_partner_ids(srfdomain.getMessage_partner_ids());
        if(srfdomain.getActivity_summaryDirtyFlag())
            this.setActivity_summary(srfdomain.getActivity_summary());
        if(srfdomain.getWebsite_message_idsDirtyFlag())
            this.setWebsite_message_ids(srfdomain.getWebsite_message_ids());
        if(srfdomain.getMessage_needactionDirtyFlag())
            this.setMessage_needaction(srfdomain.getMessage_needaction());
        if(srfdomain.getDisplay_nameDirtyFlag())
            this.setDisplay_name(srfdomain.getDisplay_name());
        if(srfdomain.getActivity_stateDirtyFlag())
            this.setActivity_state(srfdomain.getActivity_state());
        if(srfdomain.getTotal_amountDirtyFlag())
            this.setTotal_amount(srfdomain.getTotal_amount());
        if(srfdomain.get__last_updateDirtyFlag())
            this.set__last_update(srfdomain.get__last_update());
        if(srfdomain.getActivity_date_deadlineDirtyFlag())
            this.setActivity_date_deadline(srfdomain.getActivity_date_deadline());
        if(srfdomain.getMessage_follower_idsDirtyFlag())
            this.setMessage_follower_ids(srfdomain.getMessage_follower_ids());
        if(srfdomain.getMessage_has_errorDirtyFlag())
            this.setMessage_has_error(srfdomain.getMessage_has_error());
        if(srfdomain.getTax_idsDirtyFlag())
            this.setTax_ids(srfdomain.getTax_ids());
        if(srfdomain.getUnit_amountDirtyFlag())
            this.setUnit_amount(srfdomain.getUnit_amount());
        if(srfdomain.getPayment_modeDirtyFlag())
            this.setPayment_mode(srfdomain.getPayment_mode());
        if(srfdomain.getMessage_channel_idsDirtyFlag())
            this.setMessage_channel_ids(srfdomain.getMessage_channel_ids());
        if(srfdomain.getCreate_dateDirtyFlag())
            this.setCreate_date(srfdomain.getCreate_date());
        if(srfdomain.getMessage_attachment_countDirtyFlag())
            this.setMessage_attachment_count(srfdomain.getMessage_attachment_count());
        if(srfdomain.getMessage_needaction_counterDirtyFlag())
            this.setMessage_needaction_counter(srfdomain.getMessage_needaction_counter());
        if(srfdomain.getWrite_dateDirtyFlag())
            this.setWrite_date(srfdomain.getWrite_date());
        if(srfdomain.getTotal_amount_companyDirtyFlag())
            this.setTotal_amount_company(srfdomain.getTotal_amount_company());
        if(srfdomain.getIs_refusedDirtyFlag())
            this.setIs_refused(srfdomain.getIs_refused());
        if(srfdomain.getDateDirtyFlag())
            this.setDate(srfdomain.getDate());
        if(srfdomain.getMessage_idsDirtyFlag())
            this.setMessage_ids(srfdomain.getMessage_ids());
        if(srfdomain.getQuantityDirtyFlag())
            this.setQuantity(srfdomain.getQuantity());
        if(srfdomain.getMessage_unreadDirtyFlag())
            this.setMessage_unread(srfdomain.getMessage_unread());
        if(srfdomain.getMessage_is_followerDirtyFlag())
            this.setMessage_is_follower(srfdomain.getMessage_is_follower());
        if(srfdomain.getUntaxed_amountDirtyFlag())
            this.setUntaxed_amount(srfdomain.getUntaxed_amount());
        if(srfdomain.getMessage_has_error_counterDirtyFlag())
            this.setMessage_has_error_counter(srfdomain.getMessage_has_error_counter());
        if(srfdomain.getMessage_main_attachment_idDirtyFlag())
            this.setMessage_main_attachment_id(srfdomain.getMessage_main_attachment_id());
        if(srfdomain.getAnalytic_tag_idsDirtyFlag())
            this.setAnalytic_tag_ids(srfdomain.getAnalytic_tag_ids());
        if(srfdomain.getDescriptionDirtyFlag())
            this.setDescription(srfdomain.getDescription());
        if(srfdomain.getReferenceDirtyFlag())
            this.setReference(srfdomain.getReference());
        if(srfdomain.getActivity_user_idDirtyFlag())
            this.setActivity_user_id(srfdomain.getActivity_user_id());
        if(srfdomain.getMessage_unread_counterDirtyFlag())
            this.setMessage_unread_counter(srfdomain.getMessage_unread_counter());
        if(srfdomain.getActivity_type_idDirtyFlag())
            this.setActivity_type_id(srfdomain.getActivity_type_id());
        if(srfdomain.getSheet_id_textDirtyFlag())
            this.setSheet_id_text(srfdomain.getSheet_id_text());
        if(srfdomain.getCurrency_id_textDirtyFlag())
            this.setCurrency_id_text(srfdomain.getCurrency_id_text());
        if(srfdomain.getCompany_id_textDirtyFlag())
            this.setCompany_id_text(srfdomain.getCompany_id_text());
        if(srfdomain.getProduct_id_textDirtyFlag())
            this.setProduct_id_text(srfdomain.getProduct_id_text());
        if(srfdomain.getCreate_uid_textDirtyFlag())
            this.setCreate_uid_text(srfdomain.getCreate_uid_text());
        if(srfdomain.getCompany_currency_id_textDirtyFlag())
            this.setCompany_currency_id_text(srfdomain.getCompany_currency_id_text());
        if(srfdomain.getEmployee_id_textDirtyFlag())
            this.setEmployee_id_text(srfdomain.getEmployee_id_text());
        if(srfdomain.getProduct_uom_id_textDirtyFlag())
            this.setProduct_uom_id_text(srfdomain.getProduct_uom_id_text());
        if(srfdomain.getSale_order_id_textDirtyFlag())
            this.setSale_order_id_text(srfdomain.getSale_order_id_text());
        if(srfdomain.getAnalytic_account_id_textDirtyFlag())
            this.setAnalytic_account_id_text(srfdomain.getAnalytic_account_id_text());
        if(srfdomain.getWrite_uid_textDirtyFlag())
            this.setWrite_uid_text(srfdomain.getWrite_uid_text());
        if(srfdomain.getAccount_id_textDirtyFlag())
            this.setAccount_id_text(srfdomain.getAccount_id_text());
        if(srfdomain.getCreate_uidDirtyFlag())
            this.setCreate_uid(srfdomain.getCreate_uid());
        if(srfdomain.getEmployee_idDirtyFlag())
            this.setEmployee_id(srfdomain.getEmployee_id());
        if(srfdomain.getSale_order_idDirtyFlag())
            this.setSale_order_id(srfdomain.getSale_order_id());
        if(srfdomain.getCompany_idDirtyFlag())
            this.setCompany_id(srfdomain.getCompany_id());
        if(srfdomain.getCurrency_idDirtyFlag())
            this.setCurrency_id(srfdomain.getCurrency_id());
        if(srfdomain.getProduct_idDirtyFlag())
            this.setProduct_id(srfdomain.getProduct_id());
        if(srfdomain.getProduct_uom_idDirtyFlag())
            this.setProduct_uom_id(srfdomain.getProduct_uom_id());
        if(srfdomain.getSheet_idDirtyFlag())
            this.setSheet_id(srfdomain.getSheet_id());
        if(srfdomain.getWrite_uidDirtyFlag())
            this.setWrite_uid(srfdomain.getWrite_uid());
        if(srfdomain.getAccount_idDirtyFlag())
            this.setAccount_id(srfdomain.getAccount_id());
        if(srfdomain.getCompany_currency_idDirtyFlag())
            this.setCompany_currency_id(srfdomain.getCompany_currency_id());
        if(srfdomain.getAnalytic_account_idDirtyFlag())
            this.setAnalytic_account_id(srfdomain.getAnalytic_account_id());

    }

    public List<Hr_expenseDTO> fromDOPage(List<Hr_expense> poPage)   {
        if(poPage == null)
            return null;
        List<Hr_expenseDTO> dtos=new ArrayList<Hr_expenseDTO>();
        for(Hr_expense domain : poPage) {
            Hr_expenseDTO dto = new Hr_expenseDTO();
            dto.fromDO(domain);
            dtos.add(dto);
        }
        return dtos;
    }
}

