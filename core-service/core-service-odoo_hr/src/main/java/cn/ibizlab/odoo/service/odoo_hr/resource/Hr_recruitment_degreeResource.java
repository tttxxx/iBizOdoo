package cn.ibizlab.odoo.service.odoo_hr.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_hr.dto.Hr_recruitment_degreeDTO;
import cn.ibizlab.odoo.core.odoo_hr.domain.Hr_recruitment_degree;
import cn.ibizlab.odoo.core.odoo_hr.service.IHr_recruitment_degreeService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_hr.filter.Hr_recruitment_degreeSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Hr_recruitment_degree" })
@RestController
@RequestMapping("")
public class Hr_recruitment_degreeResource {

    @Autowired
    private IHr_recruitment_degreeService hr_recruitment_degreeService;

    public IHr_recruitment_degreeService getHr_recruitment_degreeService() {
        return this.hr_recruitment_degreeService;
    }

    @ApiOperation(value = "批建立数据", tags = {"Hr_recruitment_degree" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_hr/hr_recruitment_degrees/createBatch")
    public ResponseEntity<Boolean> createBatchHr_recruitment_degree(@RequestBody List<Hr_recruitment_degreeDTO> hr_recruitment_degreedtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "批更新数据", tags = {"Hr_recruitment_degree" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_hr/hr_recruitment_degrees/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Hr_recruitment_degreeDTO> hr_recruitment_degreedtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "批删除数据", tags = {"Hr_recruitment_degree" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_hr/hr_recruitment_degrees/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Hr_recruitment_degreeDTO> hr_recruitment_degreedtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "更新数据", tags = {"Hr_recruitment_degree" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_hr/hr_recruitment_degrees/{hr_recruitment_degree_id}")

    public ResponseEntity<Hr_recruitment_degreeDTO> update(@PathVariable("hr_recruitment_degree_id") Integer hr_recruitment_degree_id, @RequestBody Hr_recruitment_degreeDTO hr_recruitment_degreedto) {
		Hr_recruitment_degree domain = hr_recruitment_degreedto.toDO();
        domain.setId(hr_recruitment_degree_id);
		hr_recruitment_degreeService.update(domain);
		Hr_recruitment_degreeDTO dto = new Hr_recruitment_degreeDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "删除数据", tags = {"Hr_recruitment_degree" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_hr/hr_recruitment_degrees/{hr_recruitment_degree_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("hr_recruitment_degree_id") Integer hr_recruitment_degree_id) {
        Hr_recruitment_degreeDTO hr_recruitment_degreedto = new Hr_recruitment_degreeDTO();
		Hr_recruitment_degree domain = new Hr_recruitment_degree();
		hr_recruitment_degreedto.setId(hr_recruitment_degree_id);
		domain.setId(hr_recruitment_degree_id);
        Boolean rst = hr_recruitment_degreeService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "获取数据", tags = {"Hr_recruitment_degree" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_hr/hr_recruitment_degrees/{hr_recruitment_degree_id}")
    public ResponseEntity<Hr_recruitment_degreeDTO> get(@PathVariable("hr_recruitment_degree_id") Integer hr_recruitment_degree_id) {
        Hr_recruitment_degreeDTO dto = new Hr_recruitment_degreeDTO();
        Hr_recruitment_degree domain = hr_recruitment_degreeService.get(hr_recruitment_degree_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "建立数据", tags = {"Hr_recruitment_degree" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_hr/hr_recruitment_degrees")

    public ResponseEntity<Hr_recruitment_degreeDTO> create(@RequestBody Hr_recruitment_degreeDTO hr_recruitment_degreedto) {
        Hr_recruitment_degreeDTO dto = new Hr_recruitment_degreeDTO();
        Hr_recruitment_degree domain = hr_recruitment_degreedto.toDO();
		hr_recruitment_degreeService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Hr_recruitment_degree" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_hr/hr_recruitment_degrees/fetchdefault")
	public ResponseEntity<Page<Hr_recruitment_degreeDTO>> fetchDefault(Hr_recruitment_degreeSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Hr_recruitment_degreeDTO> list = new ArrayList<Hr_recruitment_degreeDTO>();
        
        Page<Hr_recruitment_degree> domains = hr_recruitment_degreeService.searchDefault(context) ;
        for(Hr_recruitment_degree hr_recruitment_degree : domains.getContent()){
            Hr_recruitment_degreeDTO dto = new Hr_recruitment_degreeDTO();
            dto.fromDO(hr_recruitment_degree);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
