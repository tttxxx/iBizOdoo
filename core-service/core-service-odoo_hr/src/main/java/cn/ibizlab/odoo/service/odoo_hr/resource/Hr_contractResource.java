package cn.ibizlab.odoo.service.odoo_hr.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_hr.dto.Hr_contractDTO;
import cn.ibizlab.odoo.core.odoo_hr.domain.Hr_contract;
import cn.ibizlab.odoo.core.odoo_hr.service.IHr_contractService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_hr.filter.Hr_contractSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Hr_contract" })
@RestController
@RequestMapping("")
public class Hr_contractResource {

    @Autowired
    private IHr_contractService hr_contractService;

    public IHr_contractService getHr_contractService() {
        return this.hr_contractService;
    }

    @ApiOperation(value = "批建立数据", tags = {"Hr_contract" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_hr/hr_contracts/createBatch")
    public ResponseEntity<Boolean> createBatchHr_contract(@RequestBody List<Hr_contractDTO> hr_contractdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "批更新数据", tags = {"Hr_contract" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_hr/hr_contracts/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Hr_contractDTO> hr_contractdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "删除数据", tags = {"Hr_contract" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_hr/hr_contracts/{hr_contract_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("hr_contract_id") Integer hr_contract_id) {
        Hr_contractDTO hr_contractdto = new Hr_contractDTO();
		Hr_contract domain = new Hr_contract();
		hr_contractdto.setId(hr_contract_id);
		domain.setId(hr_contract_id);
        Boolean rst = hr_contractService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "更新数据", tags = {"Hr_contract" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_hr/hr_contracts/{hr_contract_id}")

    public ResponseEntity<Hr_contractDTO> update(@PathVariable("hr_contract_id") Integer hr_contract_id, @RequestBody Hr_contractDTO hr_contractdto) {
		Hr_contract domain = hr_contractdto.toDO();
        domain.setId(hr_contract_id);
		hr_contractService.update(domain);
		Hr_contractDTO dto = new Hr_contractDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "建立数据", tags = {"Hr_contract" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_hr/hr_contracts")

    public ResponseEntity<Hr_contractDTO> create(@RequestBody Hr_contractDTO hr_contractdto) {
        Hr_contractDTO dto = new Hr_contractDTO();
        Hr_contract domain = hr_contractdto.toDO();
		hr_contractService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取数据", tags = {"Hr_contract" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_hr/hr_contracts/{hr_contract_id}")
    public ResponseEntity<Hr_contractDTO> get(@PathVariable("hr_contract_id") Integer hr_contract_id) {
        Hr_contractDTO dto = new Hr_contractDTO();
        Hr_contract domain = hr_contractService.get(hr_contract_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批删除数据", tags = {"Hr_contract" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_hr/hr_contracts/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Hr_contractDTO> hr_contractdtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Hr_contract" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_hr/hr_contracts/fetchdefault")
	public ResponseEntity<Page<Hr_contractDTO>> fetchDefault(Hr_contractSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Hr_contractDTO> list = new ArrayList<Hr_contractDTO>();
        
        Page<Hr_contract> domains = hr_contractService.searchDefault(context) ;
        for(Hr_contract hr_contract : domains.getContent()){
            Hr_contractDTO dto = new Hr_contractDTO();
            dto.fromDO(hr_contract);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
