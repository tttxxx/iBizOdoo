package cn.ibizlab.odoo.service.odoo_hr.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.core.odoo_hr.valuerule.anno.hr_employee.*;
import cn.ibizlab.odoo.core.odoo_hr.domain.Hr_employee;
import org.springframework.cglib.beans.BeanCopier;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 服务DTO对象[Hr_employeeDTO]
 */
public class Hr_employeeDTO implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [MOBILE_PHONE]
     *
     */
    @Hr_employeeMobile_phoneDefault(info = "默认规则")
    private String mobile_phone;

    @JsonIgnore
    private boolean mobile_phoneDirtyFlag;

    /**
     * 属性 [MESSAGE_HAS_ERROR_COUNTER]
     *
     */
    @Hr_employeeMessage_has_error_counterDefault(info = "默认规则")
    private Integer message_has_error_counter;

    @JsonIgnore
    private boolean message_has_error_counterDirtyFlag;

    /**
     * 属性 [MESSAGE_NEEDACTION_COUNTER]
     *
     */
    @Hr_employeeMessage_needaction_counterDefault(info = "默认规则")
    private Integer message_needaction_counter;

    @JsonIgnore
    private boolean message_needaction_counterDirtyFlag;

    /**
     * 属性 [LEAVE_DATE_FROM]
     *
     */
    @Hr_employeeLeave_date_fromDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    private Timestamp leave_date_from;

    @JsonIgnore
    private boolean leave_date_fromDirtyFlag;

    /**
     * 属性 [MESSAGE_UNREAD]
     *
     */
    @Hr_employeeMessage_unreadDefault(info = "默认规则")
    private String message_unread;

    @JsonIgnore
    private boolean message_unreadDirtyFlag;

    /**
     * 属性 [CHILDREN]
     *
     */
    @Hr_employeeChildrenDefault(info = "默认规则")
    private Integer children;

    @JsonIgnore
    private boolean childrenDirtyFlag;

    /**
     * 属性 [IMAGE_SMALL]
     *
     */
    @Hr_employeeImage_smallDefault(info = "默认规则")
    private byte[] image_small;

    @JsonIgnore
    private boolean image_smallDirtyFlag;

    /**
     * 属性 [MESSAGE_NEEDACTION]
     *
     */
    @Hr_employeeMessage_needactionDefault(info = "默认规则")
    private String message_needaction;

    @JsonIgnore
    private boolean message_needactionDirtyFlag;

    /**
     * 属性 [PIN]
     *
     */
    @Hr_employeePinDefault(info = "默认规则")
    private String pin;

    @JsonIgnore
    private boolean pinDirtyFlag;

    /**
     * 属性 [MESSAGE_MAIN_ATTACHMENT_ID]
     *
     */
    @Hr_employeeMessage_main_attachment_idDefault(info = "默认规则")
    private Integer message_main_attachment_id;

    @JsonIgnore
    private boolean message_main_attachment_idDirtyFlag;

    /**
     * 属性 [STUDY_SCHOOL]
     *
     */
    @Hr_employeeStudy_schoolDefault(info = "默认规则")
    private String study_school;

    @JsonIgnore
    private boolean study_schoolDirtyFlag;

    /**
     * 属性 [ACTIVITY_IDS]
     *
     */
    @Hr_employeeActivity_idsDefault(info = "默认规则")
    private String activity_ids;

    @JsonIgnore
    private boolean activity_idsDirtyFlag;

    /**
     * 属性 [MARITAL]
     *
     */
    @Hr_employeeMaritalDefault(info = "默认规则")
    private String marital;

    @JsonIgnore
    private boolean maritalDirtyFlag;

    /**
     * 属性 [DIRECT_BADGE_IDS]
     *
     */
    @Hr_employeeDirect_badge_idsDefault(info = "默认规则")
    private String direct_badge_ids;

    @JsonIgnore
    private boolean direct_badge_idsDirtyFlag;

    /**
     * 属性 [IMAGE_MEDIUM]
     *
     */
    @Hr_employeeImage_mediumDefault(info = "默认规则")
    private byte[] image_medium;

    @JsonIgnore
    private boolean image_mediumDirtyFlag;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @Hr_employee__last_updateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp __last_update;

    @JsonIgnore
    private boolean __last_updateDirtyFlag;

    /**
     * 属性 [EMERGENCY_PHONE]
     *
     */
    @Hr_employeeEmergency_phoneDefault(info = "默认规则")
    private String emergency_phone;

    @JsonIgnore
    private boolean emergency_phoneDirtyFlag;

    /**
     * 属性 [ACTIVITY_DATE_DEADLINE]
     *
     */
    @Hr_employeeActivity_date_deadlineDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    private Timestamp activity_date_deadline;

    @JsonIgnore
    private boolean activity_date_deadlineDirtyFlag;

    /**
     * 属性 [KM_HOME_WORK]
     *
     */
    @Hr_employeeKm_home_workDefault(info = "默认规则")
    private Integer km_home_work;

    @JsonIgnore
    private boolean km_home_workDirtyFlag;

    /**
     * 属性 [VISA_NO]
     *
     */
    @Hr_employeeVisa_noDefault(info = "默认规则")
    private String visa_no;

    @JsonIgnore
    private boolean visa_noDirtyFlag;

    /**
     * 属性 [PLACE_OF_BIRTH]
     *
     */
    @Hr_employeePlace_of_birthDefault(info = "默认规则")
    private String place_of_birth;

    @JsonIgnore
    private boolean place_of_birthDirtyFlag;

    /**
     * 属性 [SPOUSE_COMPLETE_NAME]
     *
     */
    @Hr_employeeSpouse_complete_nameDefault(info = "默认规则")
    private String spouse_complete_name;

    @JsonIgnore
    private boolean spouse_complete_nameDirtyFlag;

    /**
     * 属性 [SINID]
     *
     */
    @Hr_employeeSinidDefault(info = "默认规则")
    private String sinid;

    @JsonIgnore
    private boolean sinidDirtyFlag;

    /**
     * 属性 [BADGE_IDS]
     *
     */
    @Hr_employeeBadge_idsDefault(info = "默认规则")
    private String badge_ids;

    @JsonIgnore
    private boolean badge_idsDirtyFlag;

    /**
     * 属性 [WORK_EMAIL]
     *
     */
    @Hr_employeeWork_emailDefault(info = "默认规则")
    private String work_email;

    @JsonIgnore
    private boolean work_emailDirtyFlag;

    /**
     * 属性 [HAS_BADGES]
     *
     */
    @Hr_employeeHas_badgesDefault(info = "默认规则")
    private String has_badges;

    @JsonIgnore
    private boolean has_badgesDirtyFlag;

    /**
     * 属性 [CURRENT_LEAVE_ID]
     *
     */
    @Hr_employeeCurrent_leave_idDefault(info = "默认规则")
    private Integer current_leave_id;

    @JsonIgnore
    private boolean current_leave_idDirtyFlag;

    /**
     * 属性 [CONTRACT_IDS]
     *
     */
    @Hr_employeeContract_idsDefault(info = "默认规则")
    private String contract_ids;

    @JsonIgnore
    private boolean contract_idsDirtyFlag;

    /**
     * 属性 [MESSAGE_IS_FOLLOWER]
     *
     */
    @Hr_employeeMessage_is_followerDefault(info = "默认规则")
    private String message_is_follower;

    @JsonIgnore
    private boolean message_is_followerDirtyFlag;

    /**
     * 属性 [MESSAGE_CHANNEL_IDS]
     *
     */
    @Hr_employeeMessage_channel_idsDefault(info = "默认规则")
    private String message_channel_ids;

    @JsonIgnore
    private boolean message_channel_idsDirtyFlag;

    /**
     * 属性 [BARCODE]
     *
     */
    @Hr_employeeBarcodeDefault(info = "默认规则")
    private String barcode;

    @JsonIgnore
    private boolean barcodeDirtyFlag;

    /**
     * 属性 [BIRTHDAY]
     *
     */
    @Hr_employeeBirthdayDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    private Timestamp birthday;

    @JsonIgnore
    private boolean birthdayDirtyFlag;

    /**
     * 属性 [CERTIFICATE]
     *
     */
    @Hr_employeeCertificateDefault(info = "默认规则")
    private String certificate;

    @JsonIgnore
    private boolean certificateDirtyFlag;

    /**
     * 属性 [ACTIVITY_USER_ID]
     *
     */
    @Hr_employeeActivity_user_idDefault(info = "默认规则")
    private Integer activity_user_id;

    @JsonIgnore
    private boolean activity_user_idDirtyFlag;

    /**
     * 属性 [STUDY_FIELD]
     *
     */
    @Hr_employeeStudy_fieldDefault(info = "默认规则")
    private String study_field;

    @JsonIgnore
    private boolean study_fieldDirtyFlag;

    /**
     * 属性 [WEBSITE_MESSAGE_IDS]
     *
     */
    @Hr_employeeWebsite_message_idsDefault(info = "默认规则")
    private String website_message_ids;

    @JsonIgnore
    private boolean website_message_idsDirtyFlag;

    /**
     * 属性 [ACTIVITY_SUMMARY]
     *
     */
    @Hr_employeeActivity_summaryDefault(info = "默认规则")
    private String activity_summary;

    @JsonIgnore
    private boolean activity_summaryDirtyFlag;

    /**
     * 属性 [ATTENDANCE_STATE]
     *
     */
    @Hr_employeeAttendance_stateDefault(info = "默认规则")
    private String attendance_state;

    @JsonIgnore
    private boolean attendance_stateDirtyFlag;

    /**
     * 属性 [CONTRACTS_COUNT]
     *
     */
    @Hr_employeeContracts_countDefault(info = "默认规则")
    private Integer contracts_count;

    @JsonIgnore
    private boolean contracts_countDirtyFlag;

    /**
     * 属性 [GENDER]
     *
     */
    @Hr_employeeGenderDefault(info = "默认规则")
    private String gender;

    @JsonIgnore
    private boolean genderDirtyFlag;

    /**
     * 属性 [IMAGE]
     *
     */
    @Hr_employeeImageDefault(info = "默认规则")
    private byte[] image;

    @JsonIgnore
    private boolean imageDirtyFlag;

    /**
     * 属性 [SPOUSE_BIRTHDATE]
     *
     */
    @Hr_employeeSpouse_birthdateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    private Timestamp spouse_birthdate;

    @JsonIgnore
    private boolean spouse_birthdateDirtyFlag;

    /**
     * 属性 [MESSAGE_UNREAD_COUNTER]
     *
     */
    @Hr_employeeMessage_unread_counterDefault(info = "默认规则")
    private Integer message_unread_counter;

    @JsonIgnore
    private boolean message_unread_counterDirtyFlag;

    /**
     * 属性 [LEAVES_COUNT]
     *
     */
    @Hr_employeeLeaves_countDefault(info = "默认规则")
    private Double leaves_count;

    @JsonIgnore
    private boolean leaves_countDirtyFlag;

    /**
     * 属性 [NOTES]
     *
     */
    @Hr_employeeNotesDefault(info = "默认规则")
    private String notes;

    @JsonIgnore
    private boolean notesDirtyFlag;

    /**
     * 属性 [ADDITIONAL_NOTE]
     *
     */
    @Hr_employeeAdditional_noteDefault(info = "默认规则")
    private String additional_note;

    @JsonIgnore
    private boolean additional_noteDirtyFlag;

    /**
     * 属性 [WRITE_DATE]
     *
     */
    @Hr_employeeWrite_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp write_date;

    @JsonIgnore
    private boolean write_dateDirtyFlag;

    /**
     * 属性 [MEDIC_EXAM]
     *
     */
    @Hr_employeeMedic_examDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    private Timestamp medic_exam;

    @JsonIgnore
    private boolean medic_examDirtyFlag;

    /**
     * 属性 [VISA_EXPIRE]
     *
     */
    @Hr_employeeVisa_expireDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    private Timestamp visa_expire;

    @JsonIgnore
    private boolean visa_expireDirtyFlag;

    /**
     * 属性 [WORK_PHONE]
     *
     */
    @Hr_employeeWork_phoneDefault(info = "默认规则")
    private String work_phone;

    @JsonIgnore
    private boolean work_phoneDirtyFlag;

    /**
     * 属性 [EMERGENCY_CONTACT]
     *
     */
    @Hr_employeeEmergency_contactDefault(info = "默认规则")
    private String emergency_contact;

    @JsonIgnore
    private boolean emergency_contactDirtyFlag;

    /**
     * 属性 [NEWLY_HIRED_EMPLOYEE]
     *
     */
    @Hr_employeeNewly_hired_employeeDefault(info = "默认规则")
    private String newly_hired_employee;

    @JsonIgnore
    private boolean newly_hired_employeeDirtyFlag;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @Hr_employeeDisplay_nameDefault(info = "默认规则")
    private String display_name;

    @JsonIgnore
    private boolean display_nameDirtyFlag;

    /**
     * 属性 [IS_ADDRESS_HOME_A_COMPANY]
     *
     */
    @Hr_employeeIs_address_home_a_companyDefault(info = "默认规则")
    private String is_address_home_a_company;

    @JsonIgnore
    private boolean is_address_home_a_companyDirtyFlag;

    /**
     * 属性 [ATTENDANCE_IDS]
     *
     */
    @Hr_employeeAttendance_idsDefault(info = "默认规则")
    private String attendance_ids;

    @JsonIgnore
    private boolean attendance_idsDirtyFlag;

    /**
     * 属性 [ACTIVITY_STATE]
     *
     */
    @Hr_employeeActivity_stateDefault(info = "默认规则")
    private String activity_state;

    @JsonIgnore
    private boolean activity_stateDirtyFlag;

    /**
     * 属性 [MESSAGE_HAS_ERROR]
     *
     */
    @Hr_employeeMessage_has_errorDefault(info = "默认规则")
    private String message_has_error;

    @JsonIgnore
    private boolean message_has_errorDirtyFlag;

    /**
     * 属性 [CURRENT_LEAVE_STATE]
     *
     */
    @Hr_employeeCurrent_leave_stateDefault(info = "默认规则")
    private String current_leave_state;

    @JsonIgnore
    private boolean current_leave_stateDirtyFlag;

    /**
     * 属性 [CHILD_IDS]
     *
     */
    @Hr_employeeChild_idsDefault(info = "默认规则")
    private String child_ids;

    @JsonIgnore
    private boolean child_idsDirtyFlag;

    /**
     * 属性 [MESSAGE_IDS]
     *
     */
    @Hr_employeeMessage_idsDefault(info = "默认规则")
    private String message_ids;

    @JsonIgnore
    private boolean message_idsDirtyFlag;

    /**
     * 属性 [COLOR]
     *
     */
    @Hr_employeeColorDefault(info = "默认规则")
    private Integer color;

    @JsonIgnore
    private boolean colorDirtyFlag;

    /**
     * 属性 [MANUAL_ATTENDANCE]
     *
     */
    @Hr_employeeManual_attendanceDefault(info = "默认规则")
    private String manual_attendance;

    @JsonIgnore
    private boolean manual_attendanceDirtyFlag;

    /**
     * 属性 [REMAINING_LEAVES]
     *
     */
    @Hr_employeeRemaining_leavesDefault(info = "默认规则")
    private Double remaining_leaves;

    @JsonIgnore
    private boolean remaining_leavesDirtyFlag;

    /**
     * 属性 [SSNID]
     *
     */
    @Hr_employeeSsnidDefault(info = "默认规则")
    private String ssnid;

    @JsonIgnore
    private boolean ssnidDirtyFlag;

    /**
     * 属性 [JOB_TITLE]
     *
     */
    @Hr_employeeJob_titleDefault(info = "默认规则")
    private String job_title;

    @JsonIgnore
    private boolean job_titleDirtyFlag;

    /**
     * 属性 [ACTIVITY_TYPE_ID]
     *
     */
    @Hr_employeeActivity_type_idDefault(info = "默认规则")
    private Integer activity_type_id;

    @JsonIgnore
    private boolean activity_type_idDirtyFlag;

    /**
     * 属性 [IS_ABSENT_TOTAY]
     *
     */
    @Hr_employeeIs_absent_totayDefault(info = "默认规则")
    private String is_absent_totay;

    @JsonIgnore
    private boolean is_absent_totayDirtyFlag;

    /**
     * 属性 [MESSAGE_PARTNER_IDS]
     *
     */
    @Hr_employeeMessage_partner_idsDefault(info = "默认规则")
    private String message_partner_ids;

    @JsonIgnore
    private boolean message_partner_idsDirtyFlag;

    /**
     * 属性 [PASSPORT_ID]
     *
     */
    @Hr_employeePassport_idDefault(info = "默认规则")
    private String passport_id;

    @JsonIgnore
    private boolean passport_idDirtyFlag;

    /**
     * 属性 [GOAL_IDS]
     *
     */
    @Hr_employeeGoal_idsDefault(info = "默认规则")
    private String goal_ids;

    @JsonIgnore
    private boolean goal_idsDirtyFlag;

    /**
     * 属性 [LEAVE_DATE_TO]
     *
     */
    @Hr_employeeLeave_date_toDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    private Timestamp leave_date_to;

    @JsonIgnore
    private boolean leave_date_toDirtyFlag;

    /**
     * 属性 [MANAGER]
     *
     */
    @Hr_employeeManagerDefault(info = "默认规则")
    private String manager;

    @JsonIgnore
    private boolean managerDirtyFlag;

    /**
     * 属性 [WORK_LOCATION]
     *
     */
    @Hr_employeeWork_locationDefault(info = "默认规则")
    private String work_location;

    @JsonIgnore
    private boolean work_locationDirtyFlag;

    /**
     * 属性 [MESSAGE_ATTACHMENT_COUNT]
     *
     */
    @Hr_employeeMessage_attachment_countDefault(info = "默认规则")
    private Integer message_attachment_count;

    @JsonIgnore
    private boolean message_attachment_countDirtyFlag;

    /**
     * 属性 [CONTRACT_ID]
     *
     */
    @Hr_employeeContract_idDefault(info = "默认规则")
    private Integer contract_id;

    @JsonIgnore
    private boolean contract_idDirtyFlag;

    /**
     * 属性 [GOOGLE_DRIVE_LINK]
     *
     */
    @Hr_employeeGoogle_drive_linkDefault(info = "默认规则")
    private String google_drive_link;

    @JsonIgnore
    private boolean google_drive_linkDirtyFlag;

    /**
     * 属性 [PERMIT_NO]
     *
     */
    @Hr_employeePermit_noDefault(info = "默认规则")
    private String permit_no;

    @JsonIgnore
    private boolean permit_noDirtyFlag;

    /**
     * 属性 [MESSAGE_FOLLOWER_IDS]
     *
     */
    @Hr_employeeMessage_follower_idsDefault(info = "默认规则")
    private String message_follower_ids;

    @JsonIgnore
    private boolean message_follower_idsDirtyFlag;

    /**
     * 属性 [ID]
     *
     */
    @Hr_employeeIdDefault(info = "默认规则")
    private Integer id;

    @JsonIgnore
    private boolean idDirtyFlag;

    /**
     * 属性 [IDENTIFICATION_ID]
     *
     */
    @Hr_employeeIdentification_idDefault(info = "默认规则")
    private String identification_id;

    @JsonIgnore
    private boolean identification_idDirtyFlag;

    /**
     * 属性 [VEHICLE]
     *
     */
    @Hr_employeeVehicleDefault(info = "默认规则")
    private String vehicle;

    @JsonIgnore
    private boolean vehicleDirtyFlag;

    /**
     * 属性 [CATEGORY_IDS]
     *
     */
    @Hr_employeeCategory_idsDefault(info = "默认规则")
    private String category_ids;

    @JsonIgnore
    private boolean category_idsDirtyFlag;

    /**
     * 属性 [SHOW_LEAVES]
     *
     */
    @Hr_employeeShow_leavesDefault(info = "默认规则")
    private String show_leaves;

    @JsonIgnore
    private boolean show_leavesDirtyFlag;

    /**
     * 属性 [CREATE_DATE]
     *
     */
    @Hr_employeeCreate_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp create_date;

    @JsonIgnore
    private boolean create_dateDirtyFlag;

    /**
     * 属性 [PARENT_ID_TEXT]
     *
     */
    @Hr_employeeParent_id_textDefault(info = "默认规则")
    private String parent_id_text;

    @JsonIgnore
    private boolean parent_id_textDirtyFlag;

    /**
     * 属性 [WRITE_UID_TEXT]
     *
     */
    @Hr_employeeWrite_uid_textDefault(info = "默认规则")
    private String write_uid_text;

    @JsonIgnore
    private boolean write_uid_textDirtyFlag;

    /**
     * 属性 [NAME]
     *
     */
    @Hr_employeeNameDefault(info = "默认规则")
    private String name;

    @JsonIgnore
    private boolean nameDirtyFlag;

    /**
     * 属性 [CREATE_UID_TEXT]
     *
     */
    @Hr_employeeCreate_uid_textDefault(info = "默认规则")
    private String create_uid_text;

    @JsonIgnore
    private boolean create_uid_textDirtyFlag;

    /**
     * 属性 [ADDRESS_HOME_ID_TEXT]
     *
     */
    @Hr_employeeAddress_home_id_textDefault(info = "默认规则")
    private String address_home_id_text;

    @JsonIgnore
    private boolean address_home_id_textDirtyFlag;

    /**
     * 属性 [TZ]
     *
     */
    @Hr_employeeTzDefault(info = "默认规则")
    private String tz;

    @JsonIgnore
    private boolean tzDirtyFlag;

    /**
     * 属性 [RESOURCE_CALENDAR_ID_TEXT]
     *
     */
    @Hr_employeeResource_calendar_id_textDefault(info = "默认规则")
    private String resource_calendar_id_text;

    @JsonIgnore
    private boolean resource_calendar_id_textDirtyFlag;

    /**
     * 属性 [USER_ID_TEXT]
     *
     */
    @Hr_employeeUser_id_textDefault(info = "默认规则")
    private String user_id_text;

    @JsonIgnore
    private boolean user_id_textDirtyFlag;

    /**
     * 属性 [DEPARTMENT_ID_TEXT]
     *
     */
    @Hr_employeeDepartment_id_textDefault(info = "默认规则")
    private String department_id_text;

    @JsonIgnore
    private boolean department_id_textDirtyFlag;

    /**
     * 属性 [COUNTRY_OF_BIRTH_TEXT]
     *
     */
    @Hr_employeeCountry_of_birth_textDefault(info = "默认规则")
    private String country_of_birth_text;

    @JsonIgnore
    private boolean country_of_birth_textDirtyFlag;

    /**
     * 属性 [EXPENSE_MANAGER_ID_TEXT]
     *
     */
    @Hr_employeeExpense_manager_id_textDefault(info = "默认规则")
    private String expense_manager_id_text;

    @JsonIgnore
    private boolean expense_manager_id_textDirtyFlag;

    /**
     * 属性 [JOB_ID_TEXT]
     *
     */
    @Hr_employeeJob_id_textDefault(info = "默认规则")
    private String job_id_text;

    @JsonIgnore
    private boolean job_id_textDirtyFlag;

    /**
     * 属性 [ADDRESS_ID_TEXT]
     *
     */
    @Hr_employeeAddress_id_textDefault(info = "默认规则")
    private String address_id_text;

    @JsonIgnore
    private boolean address_id_textDirtyFlag;

    /**
     * 属性 [COMPANY_ID_TEXT]
     *
     */
    @Hr_employeeCompany_id_textDefault(info = "默认规则")
    private String company_id_text;

    @JsonIgnore
    private boolean company_id_textDirtyFlag;

    /**
     * 属性 [ACTIVE]
     *
     */
    @Hr_employeeActiveDefault(info = "默认规则")
    private String active;

    @JsonIgnore
    private boolean activeDirtyFlag;

    /**
     * 属性 [COACH_ID_TEXT]
     *
     */
    @Hr_employeeCoach_id_textDefault(info = "默认规则")
    private String coach_id_text;

    @JsonIgnore
    private boolean coach_id_textDirtyFlag;

    /**
     * 属性 [COUNTRY_ID_TEXT]
     *
     */
    @Hr_employeeCountry_id_textDefault(info = "默认规则")
    private String country_id_text;

    @JsonIgnore
    private boolean country_id_textDirtyFlag;

    /**
     * 属性 [ADDRESS_HOME_ID]
     *
     */
    @Hr_employeeAddress_home_idDefault(info = "默认规则")
    private Integer address_home_id;

    @JsonIgnore
    private boolean address_home_idDirtyFlag;

    /**
     * 属性 [EXPENSE_MANAGER_ID]
     *
     */
    @Hr_employeeExpense_manager_idDefault(info = "默认规则")
    private Integer expense_manager_id;

    @JsonIgnore
    private boolean expense_manager_idDirtyFlag;

    /**
     * 属性 [BANK_ACCOUNT_ID]
     *
     */
    @Hr_employeeBank_account_idDefault(info = "默认规则")
    private Integer bank_account_id;

    @JsonIgnore
    private boolean bank_account_idDirtyFlag;

    /**
     * 属性 [WRITE_UID]
     *
     */
    @Hr_employeeWrite_uidDefault(info = "默认规则")
    private Integer write_uid;

    @JsonIgnore
    private boolean write_uidDirtyFlag;

    /**
     * 属性 [CREATE_UID]
     *
     */
    @Hr_employeeCreate_uidDefault(info = "默认规则")
    private Integer create_uid;

    @JsonIgnore
    private boolean create_uidDirtyFlag;

    /**
     * 属性 [COUNTRY_ID]
     *
     */
    @Hr_employeeCountry_idDefault(info = "默认规则")
    private Integer country_id;

    @JsonIgnore
    private boolean country_idDirtyFlag;

    /**
     * 属性 [COMPANY_ID]
     *
     */
    @Hr_employeeCompany_idDefault(info = "默认规则")
    private Integer company_id;

    @JsonIgnore
    private boolean company_idDirtyFlag;

    /**
     * 属性 [JOB_ID]
     *
     */
    @Hr_employeeJob_idDefault(info = "默认规则")
    private Integer job_id;

    @JsonIgnore
    private boolean job_idDirtyFlag;

    /**
     * 属性 [RESOURCE_ID]
     *
     */
    @Hr_employeeResource_idDefault(info = "默认规则")
    private Integer resource_id;

    @JsonIgnore
    private boolean resource_idDirtyFlag;

    /**
     * 属性 [USER_ID]
     *
     */
    @Hr_employeeUser_idDefault(info = "默认规则")
    private Integer user_id;

    @JsonIgnore
    private boolean user_idDirtyFlag;

    /**
     * 属性 [DEPARTMENT_ID]
     *
     */
    @Hr_employeeDepartment_idDefault(info = "默认规则")
    private Integer department_id;

    @JsonIgnore
    private boolean department_idDirtyFlag;

    /**
     * 属性 [PARENT_ID]
     *
     */
    @Hr_employeeParent_idDefault(info = "默认规则")
    private Integer parent_id;

    @JsonIgnore
    private boolean parent_idDirtyFlag;

    /**
     * 属性 [LAST_ATTENDANCE_ID]
     *
     */
    @Hr_employeeLast_attendance_idDefault(info = "默认规则")
    private Integer last_attendance_id;

    @JsonIgnore
    private boolean last_attendance_idDirtyFlag;

    /**
     * 属性 [COACH_ID]
     *
     */
    @Hr_employeeCoach_idDefault(info = "默认规则")
    private Integer coach_id;

    @JsonIgnore
    private boolean coach_idDirtyFlag;

    /**
     * 属性 [ADDRESS_ID]
     *
     */
    @Hr_employeeAddress_idDefault(info = "默认规则")
    private Integer address_id;

    @JsonIgnore
    private boolean address_idDirtyFlag;

    /**
     * 属性 [COUNTRY_OF_BIRTH]
     *
     */
    @Hr_employeeCountry_of_birthDefault(info = "默认规则")
    private Integer country_of_birth;

    @JsonIgnore
    private boolean country_of_birthDirtyFlag;

    /**
     * 属性 [RESOURCE_CALENDAR_ID]
     *
     */
    @Hr_employeeResource_calendar_idDefault(info = "默认规则")
    private Integer resource_calendar_id;

    @JsonIgnore
    private boolean resource_calendar_idDirtyFlag;


    /**
     * 获取 [MOBILE_PHONE]
     */
    @JsonProperty("mobile_phone")
    public String getMobile_phone(){
        return mobile_phone ;
    }

    /**
     * 设置 [MOBILE_PHONE]
     */
    @JsonProperty("mobile_phone")
    public void setMobile_phone(String  mobile_phone){
        this.mobile_phone = mobile_phone ;
        this.mobile_phoneDirtyFlag = true ;
    }

    /**
     * 获取 [MOBILE_PHONE]脏标记
     */
    @JsonIgnore
    public boolean getMobile_phoneDirtyFlag(){
        return mobile_phoneDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_HAS_ERROR_COUNTER]
     */
    @JsonProperty("message_has_error_counter")
    public Integer getMessage_has_error_counter(){
        return message_has_error_counter ;
    }

    /**
     * 设置 [MESSAGE_HAS_ERROR_COUNTER]
     */
    @JsonProperty("message_has_error_counter")
    public void setMessage_has_error_counter(Integer  message_has_error_counter){
        this.message_has_error_counter = message_has_error_counter ;
        this.message_has_error_counterDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_HAS_ERROR_COUNTER]脏标记
     */
    @JsonIgnore
    public boolean getMessage_has_error_counterDirtyFlag(){
        return message_has_error_counterDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_NEEDACTION_COUNTER]
     */
    @JsonProperty("message_needaction_counter")
    public Integer getMessage_needaction_counter(){
        return message_needaction_counter ;
    }

    /**
     * 设置 [MESSAGE_NEEDACTION_COUNTER]
     */
    @JsonProperty("message_needaction_counter")
    public void setMessage_needaction_counter(Integer  message_needaction_counter){
        this.message_needaction_counter = message_needaction_counter ;
        this.message_needaction_counterDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_NEEDACTION_COUNTER]脏标记
     */
    @JsonIgnore
    public boolean getMessage_needaction_counterDirtyFlag(){
        return message_needaction_counterDirtyFlag ;
    }

    /**
     * 获取 [LEAVE_DATE_FROM]
     */
    @JsonProperty("leave_date_from")
    public Timestamp getLeave_date_from(){
        return leave_date_from ;
    }

    /**
     * 设置 [LEAVE_DATE_FROM]
     */
    @JsonProperty("leave_date_from")
    public void setLeave_date_from(Timestamp  leave_date_from){
        this.leave_date_from = leave_date_from ;
        this.leave_date_fromDirtyFlag = true ;
    }

    /**
     * 获取 [LEAVE_DATE_FROM]脏标记
     */
    @JsonIgnore
    public boolean getLeave_date_fromDirtyFlag(){
        return leave_date_fromDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_UNREAD]
     */
    @JsonProperty("message_unread")
    public String getMessage_unread(){
        return message_unread ;
    }

    /**
     * 设置 [MESSAGE_UNREAD]
     */
    @JsonProperty("message_unread")
    public void setMessage_unread(String  message_unread){
        this.message_unread = message_unread ;
        this.message_unreadDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_UNREAD]脏标记
     */
    @JsonIgnore
    public boolean getMessage_unreadDirtyFlag(){
        return message_unreadDirtyFlag ;
    }

    /**
     * 获取 [CHILDREN]
     */
    @JsonProperty("children")
    public Integer getChildren(){
        return children ;
    }

    /**
     * 设置 [CHILDREN]
     */
    @JsonProperty("children")
    public void setChildren(Integer  children){
        this.children = children ;
        this.childrenDirtyFlag = true ;
    }

    /**
     * 获取 [CHILDREN]脏标记
     */
    @JsonIgnore
    public boolean getChildrenDirtyFlag(){
        return childrenDirtyFlag ;
    }

    /**
     * 获取 [IMAGE_SMALL]
     */
    @JsonProperty("image_small")
    public byte[] getImage_small(){
        return image_small ;
    }

    /**
     * 设置 [IMAGE_SMALL]
     */
    @JsonProperty("image_small")
    public void setImage_small(byte[]  image_small){
        this.image_small = image_small ;
        this.image_smallDirtyFlag = true ;
    }

    /**
     * 获取 [IMAGE_SMALL]脏标记
     */
    @JsonIgnore
    public boolean getImage_smallDirtyFlag(){
        return image_smallDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_NEEDACTION]
     */
    @JsonProperty("message_needaction")
    public String getMessage_needaction(){
        return message_needaction ;
    }

    /**
     * 设置 [MESSAGE_NEEDACTION]
     */
    @JsonProperty("message_needaction")
    public void setMessage_needaction(String  message_needaction){
        this.message_needaction = message_needaction ;
        this.message_needactionDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_NEEDACTION]脏标记
     */
    @JsonIgnore
    public boolean getMessage_needactionDirtyFlag(){
        return message_needactionDirtyFlag ;
    }

    /**
     * 获取 [PIN]
     */
    @JsonProperty("pin")
    public String getPin(){
        return pin ;
    }

    /**
     * 设置 [PIN]
     */
    @JsonProperty("pin")
    public void setPin(String  pin){
        this.pin = pin ;
        this.pinDirtyFlag = true ;
    }

    /**
     * 获取 [PIN]脏标记
     */
    @JsonIgnore
    public boolean getPinDirtyFlag(){
        return pinDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_MAIN_ATTACHMENT_ID]
     */
    @JsonProperty("message_main_attachment_id")
    public Integer getMessage_main_attachment_id(){
        return message_main_attachment_id ;
    }

    /**
     * 设置 [MESSAGE_MAIN_ATTACHMENT_ID]
     */
    @JsonProperty("message_main_attachment_id")
    public void setMessage_main_attachment_id(Integer  message_main_attachment_id){
        this.message_main_attachment_id = message_main_attachment_id ;
        this.message_main_attachment_idDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_MAIN_ATTACHMENT_ID]脏标记
     */
    @JsonIgnore
    public boolean getMessage_main_attachment_idDirtyFlag(){
        return message_main_attachment_idDirtyFlag ;
    }

    /**
     * 获取 [STUDY_SCHOOL]
     */
    @JsonProperty("study_school")
    public String getStudy_school(){
        return study_school ;
    }

    /**
     * 设置 [STUDY_SCHOOL]
     */
    @JsonProperty("study_school")
    public void setStudy_school(String  study_school){
        this.study_school = study_school ;
        this.study_schoolDirtyFlag = true ;
    }

    /**
     * 获取 [STUDY_SCHOOL]脏标记
     */
    @JsonIgnore
    public boolean getStudy_schoolDirtyFlag(){
        return study_schoolDirtyFlag ;
    }

    /**
     * 获取 [ACTIVITY_IDS]
     */
    @JsonProperty("activity_ids")
    public String getActivity_ids(){
        return activity_ids ;
    }

    /**
     * 设置 [ACTIVITY_IDS]
     */
    @JsonProperty("activity_ids")
    public void setActivity_ids(String  activity_ids){
        this.activity_ids = activity_ids ;
        this.activity_idsDirtyFlag = true ;
    }

    /**
     * 获取 [ACTIVITY_IDS]脏标记
     */
    @JsonIgnore
    public boolean getActivity_idsDirtyFlag(){
        return activity_idsDirtyFlag ;
    }

    /**
     * 获取 [MARITAL]
     */
    @JsonProperty("marital")
    public String getMarital(){
        return marital ;
    }

    /**
     * 设置 [MARITAL]
     */
    @JsonProperty("marital")
    public void setMarital(String  marital){
        this.marital = marital ;
        this.maritalDirtyFlag = true ;
    }

    /**
     * 获取 [MARITAL]脏标记
     */
    @JsonIgnore
    public boolean getMaritalDirtyFlag(){
        return maritalDirtyFlag ;
    }

    /**
     * 获取 [DIRECT_BADGE_IDS]
     */
    @JsonProperty("direct_badge_ids")
    public String getDirect_badge_ids(){
        return direct_badge_ids ;
    }

    /**
     * 设置 [DIRECT_BADGE_IDS]
     */
    @JsonProperty("direct_badge_ids")
    public void setDirect_badge_ids(String  direct_badge_ids){
        this.direct_badge_ids = direct_badge_ids ;
        this.direct_badge_idsDirtyFlag = true ;
    }

    /**
     * 获取 [DIRECT_BADGE_IDS]脏标记
     */
    @JsonIgnore
    public boolean getDirect_badge_idsDirtyFlag(){
        return direct_badge_idsDirtyFlag ;
    }

    /**
     * 获取 [IMAGE_MEDIUM]
     */
    @JsonProperty("image_medium")
    public byte[] getImage_medium(){
        return image_medium ;
    }

    /**
     * 设置 [IMAGE_MEDIUM]
     */
    @JsonProperty("image_medium")
    public void setImage_medium(byte[]  image_medium){
        this.image_medium = image_medium ;
        this.image_mediumDirtyFlag = true ;
    }

    /**
     * 获取 [IMAGE_MEDIUM]脏标记
     */
    @JsonIgnore
    public boolean getImage_mediumDirtyFlag(){
        return image_mediumDirtyFlag ;
    }

    /**
     * 获取 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return __last_update ;
    }

    /**
     * 设置 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

    /**
     * 获取 [__LAST_UPDATE]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return __last_updateDirtyFlag ;
    }

    /**
     * 获取 [EMERGENCY_PHONE]
     */
    @JsonProperty("emergency_phone")
    public String getEmergency_phone(){
        return emergency_phone ;
    }

    /**
     * 设置 [EMERGENCY_PHONE]
     */
    @JsonProperty("emergency_phone")
    public void setEmergency_phone(String  emergency_phone){
        this.emergency_phone = emergency_phone ;
        this.emergency_phoneDirtyFlag = true ;
    }

    /**
     * 获取 [EMERGENCY_PHONE]脏标记
     */
    @JsonIgnore
    public boolean getEmergency_phoneDirtyFlag(){
        return emergency_phoneDirtyFlag ;
    }

    /**
     * 获取 [ACTIVITY_DATE_DEADLINE]
     */
    @JsonProperty("activity_date_deadline")
    public Timestamp getActivity_date_deadline(){
        return activity_date_deadline ;
    }

    /**
     * 设置 [ACTIVITY_DATE_DEADLINE]
     */
    @JsonProperty("activity_date_deadline")
    public void setActivity_date_deadline(Timestamp  activity_date_deadline){
        this.activity_date_deadline = activity_date_deadline ;
        this.activity_date_deadlineDirtyFlag = true ;
    }

    /**
     * 获取 [ACTIVITY_DATE_DEADLINE]脏标记
     */
    @JsonIgnore
    public boolean getActivity_date_deadlineDirtyFlag(){
        return activity_date_deadlineDirtyFlag ;
    }

    /**
     * 获取 [KM_HOME_WORK]
     */
    @JsonProperty("km_home_work")
    public Integer getKm_home_work(){
        return km_home_work ;
    }

    /**
     * 设置 [KM_HOME_WORK]
     */
    @JsonProperty("km_home_work")
    public void setKm_home_work(Integer  km_home_work){
        this.km_home_work = km_home_work ;
        this.km_home_workDirtyFlag = true ;
    }

    /**
     * 获取 [KM_HOME_WORK]脏标记
     */
    @JsonIgnore
    public boolean getKm_home_workDirtyFlag(){
        return km_home_workDirtyFlag ;
    }

    /**
     * 获取 [VISA_NO]
     */
    @JsonProperty("visa_no")
    public String getVisa_no(){
        return visa_no ;
    }

    /**
     * 设置 [VISA_NO]
     */
    @JsonProperty("visa_no")
    public void setVisa_no(String  visa_no){
        this.visa_no = visa_no ;
        this.visa_noDirtyFlag = true ;
    }

    /**
     * 获取 [VISA_NO]脏标记
     */
    @JsonIgnore
    public boolean getVisa_noDirtyFlag(){
        return visa_noDirtyFlag ;
    }

    /**
     * 获取 [PLACE_OF_BIRTH]
     */
    @JsonProperty("place_of_birth")
    public String getPlace_of_birth(){
        return place_of_birth ;
    }

    /**
     * 设置 [PLACE_OF_BIRTH]
     */
    @JsonProperty("place_of_birth")
    public void setPlace_of_birth(String  place_of_birth){
        this.place_of_birth = place_of_birth ;
        this.place_of_birthDirtyFlag = true ;
    }

    /**
     * 获取 [PLACE_OF_BIRTH]脏标记
     */
    @JsonIgnore
    public boolean getPlace_of_birthDirtyFlag(){
        return place_of_birthDirtyFlag ;
    }

    /**
     * 获取 [SPOUSE_COMPLETE_NAME]
     */
    @JsonProperty("spouse_complete_name")
    public String getSpouse_complete_name(){
        return spouse_complete_name ;
    }

    /**
     * 设置 [SPOUSE_COMPLETE_NAME]
     */
    @JsonProperty("spouse_complete_name")
    public void setSpouse_complete_name(String  spouse_complete_name){
        this.spouse_complete_name = spouse_complete_name ;
        this.spouse_complete_nameDirtyFlag = true ;
    }

    /**
     * 获取 [SPOUSE_COMPLETE_NAME]脏标记
     */
    @JsonIgnore
    public boolean getSpouse_complete_nameDirtyFlag(){
        return spouse_complete_nameDirtyFlag ;
    }

    /**
     * 获取 [SINID]
     */
    @JsonProperty("sinid")
    public String getSinid(){
        return sinid ;
    }

    /**
     * 设置 [SINID]
     */
    @JsonProperty("sinid")
    public void setSinid(String  sinid){
        this.sinid = sinid ;
        this.sinidDirtyFlag = true ;
    }

    /**
     * 获取 [SINID]脏标记
     */
    @JsonIgnore
    public boolean getSinidDirtyFlag(){
        return sinidDirtyFlag ;
    }

    /**
     * 获取 [BADGE_IDS]
     */
    @JsonProperty("badge_ids")
    public String getBadge_ids(){
        return badge_ids ;
    }

    /**
     * 设置 [BADGE_IDS]
     */
    @JsonProperty("badge_ids")
    public void setBadge_ids(String  badge_ids){
        this.badge_ids = badge_ids ;
        this.badge_idsDirtyFlag = true ;
    }

    /**
     * 获取 [BADGE_IDS]脏标记
     */
    @JsonIgnore
    public boolean getBadge_idsDirtyFlag(){
        return badge_idsDirtyFlag ;
    }

    /**
     * 获取 [WORK_EMAIL]
     */
    @JsonProperty("work_email")
    public String getWork_email(){
        return work_email ;
    }

    /**
     * 设置 [WORK_EMAIL]
     */
    @JsonProperty("work_email")
    public void setWork_email(String  work_email){
        this.work_email = work_email ;
        this.work_emailDirtyFlag = true ;
    }

    /**
     * 获取 [WORK_EMAIL]脏标记
     */
    @JsonIgnore
    public boolean getWork_emailDirtyFlag(){
        return work_emailDirtyFlag ;
    }

    /**
     * 获取 [HAS_BADGES]
     */
    @JsonProperty("has_badges")
    public String getHas_badges(){
        return has_badges ;
    }

    /**
     * 设置 [HAS_BADGES]
     */
    @JsonProperty("has_badges")
    public void setHas_badges(String  has_badges){
        this.has_badges = has_badges ;
        this.has_badgesDirtyFlag = true ;
    }

    /**
     * 获取 [HAS_BADGES]脏标记
     */
    @JsonIgnore
    public boolean getHas_badgesDirtyFlag(){
        return has_badgesDirtyFlag ;
    }

    /**
     * 获取 [CURRENT_LEAVE_ID]
     */
    @JsonProperty("current_leave_id")
    public Integer getCurrent_leave_id(){
        return current_leave_id ;
    }

    /**
     * 设置 [CURRENT_LEAVE_ID]
     */
    @JsonProperty("current_leave_id")
    public void setCurrent_leave_id(Integer  current_leave_id){
        this.current_leave_id = current_leave_id ;
        this.current_leave_idDirtyFlag = true ;
    }

    /**
     * 获取 [CURRENT_LEAVE_ID]脏标记
     */
    @JsonIgnore
    public boolean getCurrent_leave_idDirtyFlag(){
        return current_leave_idDirtyFlag ;
    }

    /**
     * 获取 [CONTRACT_IDS]
     */
    @JsonProperty("contract_ids")
    public String getContract_ids(){
        return contract_ids ;
    }

    /**
     * 设置 [CONTRACT_IDS]
     */
    @JsonProperty("contract_ids")
    public void setContract_ids(String  contract_ids){
        this.contract_ids = contract_ids ;
        this.contract_idsDirtyFlag = true ;
    }

    /**
     * 获取 [CONTRACT_IDS]脏标记
     */
    @JsonIgnore
    public boolean getContract_idsDirtyFlag(){
        return contract_idsDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_IS_FOLLOWER]
     */
    @JsonProperty("message_is_follower")
    public String getMessage_is_follower(){
        return message_is_follower ;
    }

    /**
     * 设置 [MESSAGE_IS_FOLLOWER]
     */
    @JsonProperty("message_is_follower")
    public void setMessage_is_follower(String  message_is_follower){
        this.message_is_follower = message_is_follower ;
        this.message_is_followerDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_IS_FOLLOWER]脏标记
     */
    @JsonIgnore
    public boolean getMessage_is_followerDirtyFlag(){
        return message_is_followerDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_CHANNEL_IDS]
     */
    @JsonProperty("message_channel_ids")
    public String getMessage_channel_ids(){
        return message_channel_ids ;
    }

    /**
     * 设置 [MESSAGE_CHANNEL_IDS]
     */
    @JsonProperty("message_channel_ids")
    public void setMessage_channel_ids(String  message_channel_ids){
        this.message_channel_ids = message_channel_ids ;
        this.message_channel_idsDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_CHANNEL_IDS]脏标记
     */
    @JsonIgnore
    public boolean getMessage_channel_idsDirtyFlag(){
        return message_channel_idsDirtyFlag ;
    }

    /**
     * 获取 [BARCODE]
     */
    @JsonProperty("barcode")
    public String getBarcode(){
        return barcode ;
    }

    /**
     * 设置 [BARCODE]
     */
    @JsonProperty("barcode")
    public void setBarcode(String  barcode){
        this.barcode = barcode ;
        this.barcodeDirtyFlag = true ;
    }

    /**
     * 获取 [BARCODE]脏标记
     */
    @JsonIgnore
    public boolean getBarcodeDirtyFlag(){
        return barcodeDirtyFlag ;
    }

    /**
     * 获取 [BIRTHDAY]
     */
    @JsonProperty("birthday")
    public Timestamp getBirthday(){
        return birthday ;
    }

    /**
     * 设置 [BIRTHDAY]
     */
    @JsonProperty("birthday")
    public void setBirthday(Timestamp  birthday){
        this.birthday = birthday ;
        this.birthdayDirtyFlag = true ;
    }

    /**
     * 获取 [BIRTHDAY]脏标记
     */
    @JsonIgnore
    public boolean getBirthdayDirtyFlag(){
        return birthdayDirtyFlag ;
    }

    /**
     * 获取 [CERTIFICATE]
     */
    @JsonProperty("certificate")
    public String getCertificate(){
        return certificate ;
    }

    /**
     * 设置 [CERTIFICATE]
     */
    @JsonProperty("certificate")
    public void setCertificate(String  certificate){
        this.certificate = certificate ;
        this.certificateDirtyFlag = true ;
    }

    /**
     * 获取 [CERTIFICATE]脏标记
     */
    @JsonIgnore
    public boolean getCertificateDirtyFlag(){
        return certificateDirtyFlag ;
    }

    /**
     * 获取 [ACTIVITY_USER_ID]
     */
    @JsonProperty("activity_user_id")
    public Integer getActivity_user_id(){
        return activity_user_id ;
    }

    /**
     * 设置 [ACTIVITY_USER_ID]
     */
    @JsonProperty("activity_user_id")
    public void setActivity_user_id(Integer  activity_user_id){
        this.activity_user_id = activity_user_id ;
        this.activity_user_idDirtyFlag = true ;
    }

    /**
     * 获取 [ACTIVITY_USER_ID]脏标记
     */
    @JsonIgnore
    public boolean getActivity_user_idDirtyFlag(){
        return activity_user_idDirtyFlag ;
    }

    /**
     * 获取 [STUDY_FIELD]
     */
    @JsonProperty("study_field")
    public String getStudy_field(){
        return study_field ;
    }

    /**
     * 设置 [STUDY_FIELD]
     */
    @JsonProperty("study_field")
    public void setStudy_field(String  study_field){
        this.study_field = study_field ;
        this.study_fieldDirtyFlag = true ;
    }

    /**
     * 获取 [STUDY_FIELD]脏标记
     */
    @JsonIgnore
    public boolean getStudy_fieldDirtyFlag(){
        return study_fieldDirtyFlag ;
    }

    /**
     * 获取 [WEBSITE_MESSAGE_IDS]
     */
    @JsonProperty("website_message_ids")
    public String getWebsite_message_ids(){
        return website_message_ids ;
    }

    /**
     * 设置 [WEBSITE_MESSAGE_IDS]
     */
    @JsonProperty("website_message_ids")
    public void setWebsite_message_ids(String  website_message_ids){
        this.website_message_ids = website_message_ids ;
        this.website_message_idsDirtyFlag = true ;
    }

    /**
     * 获取 [WEBSITE_MESSAGE_IDS]脏标记
     */
    @JsonIgnore
    public boolean getWebsite_message_idsDirtyFlag(){
        return website_message_idsDirtyFlag ;
    }

    /**
     * 获取 [ACTIVITY_SUMMARY]
     */
    @JsonProperty("activity_summary")
    public String getActivity_summary(){
        return activity_summary ;
    }

    /**
     * 设置 [ACTIVITY_SUMMARY]
     */
    @JsonProperty("activity_summary")
    public void setActivity_summary(String  activity_summary){
        this.activity_summary = activity_summary ;
        this.activity_summaryDirtyFlag = true ;
    }

    /**
     * 获取 [ACTIVITY_SUMMARY]脏标记
     */
    @JsonIgnore
    public boolean getActivity_summaryDirtyFlag(){
        return activity_summaryDirtyFlag ;
    }

    /**
     * 获取 [ATTENDANCE_STATE]
     */
    @JsonProperty("attendance_state")
    public String getAttendance_state(){
        return attendance_state ;
    }

    /**
     * 设置 [ATTENDANCE_STATE]
     */
    @JsonProperty("attendance_state")
    public void setAttendance_state(String  attendance_state){
        this.attendance_state = attendance_state ;
        this.attendance_stateDirtyFlag = true ;
    }

    /**
     * 获取 [ATTENDANCE_STATE]脏标记
     */
    @JsonIgnore
    public boolean getAttendance_stateDirtyFlag(){
        return attendance_stateDirtyFlag ;
    }

    /**
     * 获取 [CONTRACTS_COUNT]
     */
    @JsonProperty("contracts_count")
    public Integer getContracts_count(){
        return contracts_count ;
    }

    /**
     * 设置 [CONTRACTS_COUNT]
     */
    @JsonProperty("contracts_count")
    public void setContracts_count(Integer  contracts_count){
        this.contracts_count = contracts_count ;
        this.contracts_countDirtyFlag = true ;
    }

    /**
     * 获取 [CONTRACTS_COUNT]脏标记
     */
    @JsonIgnore
    public boolean getContracts_countDirtyFlag(){
        return contracts_countDirtyFlag ;
    }

    /**
     * 获取 [GENDER]
     */
    @JsonProperty("gender")
    public String getGender(){
        return gender ;
    }

    /**
     * 设置 [GENDER]
     */
    @JsonProperty("gender")
    public void setGender(String  gender){
        this.gender = gender ;
        this.genderDirtyFlag = true ;
    }

    /**
     * 获取 [GENDER]脏标记
     */
    @JsonIgnore
    public boolean getGenderDirtyFlag(){
        return genderDirtyFlag ;
    }

    /**
     * 获取 [IMAGE]
     */
    @JsonProperty("image")
    public byte[] getImage(){
        return image ;
    }

    /**
     * 设置 [IMAGE]
     */
    @JsonProperty("image")
    public void setImage(byte[]  image){
        this.image = image ;
        this.imageDirtyFlag = true ;
    }

    /**
     * 获取 [IMAGE]脏标记
     */
    @JsonIgnore
    public boolean getImageDirtyFlag(){
        return imageDirtyFlag ;
    }

    /**
     * 获取 [SPOUSE_BIRTHDATE]
     */
    @JsonProperty("spouse_birthdate")
    public Timestamp getSpouse_birthdate(){
        return spouse_birthdate ;
    }

    /**
     * 设置 [SPOUSE_BIRTHDATE]
     */
    @JsonProperty("spouse_birthdate")
    public void setSpouse_birthdate(Timestamp  spouse_birthdate){
        this.spouse_birthdate = spouse_birthdate ;
        this.spouse_birthdateDirtyFlag = true ;
    }

    /**
     * 获取 [SPOUSE_BIRTHDATE]脏标记
     */
    @JsonIgnore
    public boolean getSpouse_birthdateDirtyFlag(){
        return spouse_birthdateDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_UNREAD_COUNTER]
     */
    @JsonProperty("message_unread_counter")
    public Integer getMessage_unread_counter(){
        return message_unread_counter ;
    }

    /**
     * 设置 [MESSAGE_UNREAD_COUNTER]
     */
    @JsonProperty("message_unread_counter")
    public void setMessage_unread_counter(Integer  message_unread_counter){
        this.message_unread_counter = message_unread_counter ;
        this.message_unread_counterDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_UNREAD_COUNTER]脏标记
     */
    @JsonIgnore
    public boolean getMessage_unread_counterDirtyFlag(){
        return message_unread_counterDirtyFlag ;
    }

    /**
     * 获取 [LEAVES_COUNT]
     */
    @JsonProperty("leaves_count")
    public Double getLeaves_count(){
        return leaves_count ;
    }

    /**
     * 设置 [LEAVES_COUNT]
     */
    @JsonProperty("leaves_count")
    public void setLeaves_count(Double  leaves_count){
        this.leaves_count = leaves_count ;
        this.leaves_countDirtyFlag = true ;
    }

    /**
     * 获取 [LEAVES_COUNT]脏标记
     */
    @JsonIgnore
    public boolean getLeaves_countDirtyFlag(){
        return leaves_countDirtyFlag ;
    }

    /**
     * 获取 [NOTES]
     */
    @JsonProperty("notes")
    public String getNotes(){
        return notes ;
    }

    /**
     * 设置 [NOTES]
     */
    @JsonProperty("notes")
    public void setNotes(String  notes){
        this.notes = notes ;
        this.notesDirtyFlag = true ;
    }

    /**
     * 获取 [NOTES]脏标记
     */
    @JsonIgnore
    public boolean getNotesDirtyFlag(){
        return notesDirtyFlag ;
    }

    /**
     * 获取 [ADDITIONAL_NOTE]
     */
    @JsonProperty("additional_note")
    public String getAdditional_note(){
        return additional_note ;
    }

    /**
     * 设置 [ADDITIONAL_NOTE]
     */
    @JsonProperty("additional_note")
    public void setAdditional_note(String  additional_note){
        this.additional_note = additional_note ;
        this.additional_noteDirtyFlag = true ;
    }

    /**
     * 获取 [ADDITIONAL_NOTE]脏标记
     */
    @JsonIgnore
    public boolean getAdditional_noteDirtyFlag(){
        return additional_noteDirtyFlag ;
    }

    /**
     * 获取 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return write_date ;
    }

    /**
     * 设置 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return write_dateDirtyFlag ;
    }

    /**
     * 获取 [MEDIC_EXAM]
     */
    @JsonProperty("medic_exam")
    public Timestamp getMedic_exam(){
        return medic_exam ;
    }

    /**
     * 设置 [MEDIC_EXAM]
     */
    @JsonProperty("medic_exam")
    public void setMedic_exam(Timestamp  medic_exam){
        this.medic_exam = medic_exam ;
        this.medic_examDirtyFlag = true ;
    }

    /**
     * 获取 [MEDIC_EXAM]脏标记
     */
    @JsonIgnore
    public boolean getMedic_examDirtyFlag(){
        return medic_examDirtyFlag ;
    }

    /**
     * 获取 [VISA_EXPIRE]
     */
    @JsonProperty("visa_expire")
    public Timestamp getVisa_expire(){
        return visa_expire ;
    }

    /**
     * 设置 [VISA_EXPIRE]
     */
    @JsonProperty("visa_expire")
    public void setVisa_expire(Timestamp  visa_expire){
        this.visa_expire = visa_expire ;
        this.visa_expireDirtyFlag = true ;
    }

    /**
     * 获取 [VISA_EXPIRE]脏标记
     */
    @JsonIgnore
    public boolean getVisa_expireDirtyFlag(){
        return visa_expireDirtyFlag ;
    }

    /**
     * 获取 [WORK_PHONE]
     */
    @JsonProperty("work_phone")
    public String getWork_phone(){
        return work_phone ;
    }

    /**
     * 设置 [WORK_PHONE]
     */
    @JsonProperty("work_phone")
    public void setWork_phone(String  work_phone){
        this.work_phone = work_phone ;
        this.work_phoneDirtyFlag = true ;
    }

    /**
     * 获取 [WORK_PHONE]脏标记
     */
    @JsonIgnore
    public boolean getWork_phoneDirtyFlag(){
        return work_phoneDirtyFlag ;
    }

    /**
     * 获取 [EMERGENCY_CONTACT]
     */
    @JsonProperty("emergency_contact")
    public String getEmergency_contact(){
        return emergency_contact ;
    }

    /**
     * 设置 [EMERGENCY_CONTACT]
     */
    @JsonProperty("emergency_contact")
    public void setEmergency_contact(String  emergency_contact){
        this.emergency_contact = emergency_contact ;
        this.emergency_contactDirtyFlag = true ;
    }

    /**
     * 获取 [EMERGENCY_CONTACT]脏标记
     */
    @JsonIgnore
    public boolean getEmergency_contactDirtyFlag(){
        return emergency_contactDirtyFlag ;
    }

    /**
     * 获取 [NEWLY_HIRED_EMPLOYEE]
     */
    @JsonProperty("newly_hired_employee")
    public String getNewly_hired_employee(){
        return newly_hired_employee ;
    }

    /**
     * 设置 [NEWLY_HIRED_EMPLOYEE]
     */
    @JsonProperty("newly_hired_employee")
    public void setNewly_hired_employee(String  newly_hired_employee){
        this.newly_hired_employee = newly_hired_employee ;
        this.newly_hired_employeeDirtyFlag = true ;
    }

    /**
     * 获取 [NEWLY_HIRED_EMPLOYEE]脏标记
     */
    @JsonIgnore
    public boolean getNewly_hired_employeeDirtyFlag(){
        return newly_hired_employeeDirtyFlag ;
    }

    /**
     * 获取 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return display_name ;
    }

    /**
     * 设置 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

    /**
     * 获取 [DISPLAY_NAME]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return display_nameDirtyFlag ;
    }

    /**
     * 获取 [IS_ADDRESS_HOME_A_COMPANY]
     */
    @JsonProperty("is_address_home_a_company")
    public String getIs_address_home_a_company(){
        return is_address_home_a_company ;
    }

    /**
     * 设置 [IS_ADDRESS_HOME_A_COMPANY]
     */
    @JsonProperty("is_address_home_a_company")
    public void setIs_address_home_a_company(String  is_address_home_a_company){
        this.is_address_home_a_company = is_address_home_a_company ;
        this.is_address_home_a_companyDirtyFlag = true ;
    }

    /**
     * 获取 [IS_ADDRESS_HOME_A_COMPANY]脏标记
     */
    @JsonIgnore
    public boolean getIs_address_home_a_companyDirtyFlag(){
        return is_address_home_a_companyDirtyFlag ;
    }

    /**
     * 获取 [ATTENDANCE_IDS]
     */
    @JsonProperty("attendance_ids")
    public String getAttendance_ids(){
        return attendance_ids ;
    }

    /**
     * 设置 [ATTENDANCE_IDS]
     */
    @JsonProperty("attendance_ids")
    public void setAttendance_ids(String  attendance_ids){
        this.attendance_ids = attendance_ids ;
        this.attendance_idsDirtyFlag = true ;
    }

    /**
     * 获取 [ATTENDANCE_IDS]脏标记
     */
    @JsonIgnore
    public boolean getAttendance_idsDirtyFlag(){
        return attendance_idsDirtyFlag ;
    }

    /**
     * 获取 [ACTIVITY_STATE]
     */
    @JsonProperty("activity_state")
    public String getActivity_state(){
        return activity_state ;
    }

    /**
     * 设置 [ACTIVITY_STATE]
     */
    @JsonProperty("activity_state")
    public void setActivity_state(String  activity_state){
        this.activity_state = activity_state ;
        this.activity_stateDirtyFlag = true ;
    }

    /**
     * 获取 [ACTIVITY_STATE]脏标记
     */
    @JsonIgnore
    public boolean getActivity_stateDirtyFlag(){
        return activity_stateDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_HAS_ERROR]
     */
    @JsonProperty("message_has_error")
    public String getMessage_has_error(){
        return message_has_error ;
    }

    /**
     * 设置 [MESSAGE_HAS_ERROR]
     */
    @JsonProperty("message_has_error")
    public void setMessage_has_error(String  message_has_error){
        this.message_has_error = message_has_error ;
        this.message_has_errorDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_HAS_ERROR]脏标记
     */
    @JsonIgnore
    public boolean getMessage_has_errorDirtyFlag(){
        return message_has_errorDirtyFlag ;
    }

    /**
     * 获取 [CURRENT_LEAVE_STATE]
     */
    @JsonProperty("current_leave_state")
    public String getCurrent_leave_state(){
        return current_leave_state ;
    }

    /**
     * 设置 [CURRENT_LEAVE_STATE]
     */
    @JsonProperty("current_leave_state")
    public void setCurrent_leave_state(String  current_leave_state){
        this.current_leave_state = current_leave_state ;
        this.current_leave_stateDirtyFlag = true ;
    }

    /**
     * 获取 [CURRENT_LEAVE_STATE]脏标记
     */
    @JsonIgnore
    public boolean getCurrent_leave_stateDirtyFlag(){
        return current_leave_stateDirtyFlag ;
    }

    /**
     * 获取 [CHILD_IDS]
     */
    @JsonProperty("child_ids")
    public String getChild_ids(){
        return child_ids ;
    }

    /**
     * 设置 [CHILD_IDS]
     */
    @JsonProperty("child_ids")
    public void setChild_ids(String  child_ids){
        this.child_ids = child_ids ;
        this.child_idsDirtyFlag = true ;
    }

    /**
     * 获取 [CHILD_IDS]脏标记
     */
    @JsonIgnore
    public boolean getChild_idsDirtyFlag(){
        return child_idsDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_IDS]
     */
    @JsonProperty("message_ids")
    public String getMessage_ids(){
        return message_ids ;
    }

    /**
     * 设置 [MESSAGE_IDS]
     */
    @JsonProperty("message_ids")
    public void setMessage_ids(String  message_ids){
        this.message_ids = message_ids ;
        this.message_idsDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_IDS]脏标记
     */
    @JsonIgnore
    public boolean getMessage_idsDirtyFlag(){
        return message_idsDirtyFlag ;
    }

    /**
     * 获取 [COLOR]
     */
    @JsonProperty("color")
    public Integer getColor(){
        return color ;
    }

    /**
     * 设置 [COLOR]
     */
    @JsonProperty("color")
    public void setColor(Integer  color){
        this.color = color ;
        this.colorDirtyFlag = true ;
    }

    /**
     * 获取 [COLOR]脏标记
     */
    @JsonIgnore
    public boolean getColorDirtyFlag(){
        return colorDirtyFlag ;
    }

    /**
     * 获取 [MANUAL_ATTENDANCE]
     */
    @JsonProperty("manual_attendance")
    public String getManual_attendance(){
        return manual_attendance ;
    }

    /**
     * 设置 [MANUAL_ATTENDANCE]
     */
    @JsonProperty("manual_attendance")
    public void setManual_attendance(String  manual_attendance){
        this.manual_attendance = manual_attendance ;
        this.manual_attendanceDirtyFlag = true ;
    }

    /**
     * 获取 [MANUAL_ATTENDANCE]脏标记
     */
    @JsonIgnore
    public boolean getManual_attendanceDirtyFlag(){
        return manual_attendanceDirtyFlag ;
    }

    /**
     * 获取 [REMAINING_LEAVES]
     */
    @JsonProperty("remaining_leaves")
    public Double getRemaining_leaves(){
        return remaining_leaves ;
    }

    /**
     * 设置 [REMAINING_LEAVES]
     */
    @JsonProperty("remaining_leaves")
    public void setRemaining_leaves(Double  remaining_leaves){
        this.remaining_leaves = remaining_leaves ;
        this.remaining_leavesDirtyFlag = true ;
    }

    /**
     * 获取 [REMAINING_LEAVES]脏标记
     */
    @JsonIgnore
    public boolean getRemaining_leavesDirtyFlag(){
        return remaining_leavesDirtyFlag ;
    }

    /**
     * 获取 [SSNID]
     */
    @JsonProperty("ssnid")
    public String getSsnid(){
        return ssnid ;
    }

    /**
     * 设置 [SSNID]
     */
    @JsonProperty("ssnid")
    public void setSsnid(String  ssnid){
        this.ssnid = ssnid ;
        this.ssnidDirtyFlag = true ;
    }

    /**
     * 获取 [SSNID]脏标记
     */
    @JsonIgnore
    public boolean getSsnidDirtyFlag(){
        return ssnidDirtyFlag ;
    }

    /**
     * 获取 [JOB_TITLE]
     */
    @JsonProperty("job_title")
    public String getJob_title(){
        return job_title ;
    }

    /**
     * 设置 [JOB_TITLE]
     */
    @JsonProperty("job_title")
    public void setJob_title(String  job_title){
        this.job_title = job_title ;
        this.job_titleDirtyFlag = true ;
    }

    /**
     * 获取 [JOB_TITLE]脏标记
     */
    @JsonIgnore
    public boolean getJob_titleDirtyFlag(){
        return job_titleDirtyFlag ;
    }

    /**
     * 获取 [ACTIVITY_TYPE_ID]
     */
    @JsonProperty("activity_type_id")
    public Integer getActivity_type_id(){
        return activity_type_id ;
    }

    /**
     * 设置 [ACTIVITY_TYPE_ID]
     */
    @JsonProperty("activity_type_id")
    public void setActivity_type_id(Integer  activity_type_id){
        this.activity_type_id = activity_type_id ;
        this.activity_type_idDirtyFlag = true ;
    }

    /**
     * 获取 [ACTIVITY_TYPE_ID]脏标记
     */
    @JsonIgnore
    public boolean getActivity_type_idDirtyFlag(){
        return activity_type_idDirtyFlag ;
    }

    /**
     * 获取 [IS_ABSENT_TOTAY]
     */
    @JsonProperty("is_absent_totay")
    public String getIs_absent_totay(){
        return is_absent_totay ;
    }

    /**
     * 设置 [IS_ABSENT_TOTAY]
     */
    @JsonProperty("is_absent_totay")
    public void setIs_absent_totay(String  is_absent_totay){
        this.is_absent_totay = is_absent_totay ;
        this.is_absent_totayDirtyFlag = true ;
    }

    /**
     * 获取 [IS_ABSENT_TOTAY]脏标记
     */
    @JsonIgnore
    public boolean getIs_absent_totayDirtyFlag(){
        return is_absent_totayDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_PARTNER_IDS]
     */
    @JsonProperty("message_partner_ids")
    public String getMessage_partner_ids(){
        return message_partner_ids ;
    }

    /**
     * 设置 [MESSAGE_PARTNER_IDS]
     */
    @JsonProperty("message_partner_ids")
    public void setMessage_partner_ids(String  message_partner_ids){
        this.message_partner_ids = message_partner_ids ;
        this.message_partner_idsDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_PARTNER_IDS]脏标记
     */
    @JsonIgnore
    public boolean getMessage_partner_idsDirtyFlag(){
        return message_partner_idsDirtyFlag ;
    }

    /**
     * 获取 [PASSPORT_ID]
     */
    @JsonProperty("passport_id")
    public String getPassport_id(){
        return passport_id ;
    }

    /**
     * 设置 [PASSPORT_ID]
     */
    @JsonProperty("passport_id")
    public void setPassport_id(String  passport_id){
        this.passport_id = passport_id ;
        this.passport_idDirtyFlag = true ;
    }

    /**
     * 获取 [PASSPORT_ID]脏标记
     */
    @JsonIgnore
    public boolean getPassport_idDirtyFlag(){
        return passport_idDirtyFlag ;
    }

    /**
     * 获取 [GOAL_IDS]
     */
    @JsonProperty("goal_ids")
    public String getGoal_ids(){
        return goal_ids ;
    }

    /**
     * 设置 [GOAL_IDS]
     */
    @JsonProperty("goal_ids")
    public void setGoal_ids(String  goal_ids){
        this.goal_ids = goal_ids ;
        this.goal_idsDirtyFlag = true ;
    }

    /**
     * 获取 [GOAL_IDS]脏标记
     */
    @JsonIgnore
    public boolean getGoal_idsDirtyFlag(){
        return goal_idsDirtyFlag ;
    }

    /**
     * 获取 [LEAVE_DATE_TO]
     */
    @JsonProperty("leave_date_to")
    public Timestamp getLeave_date_to(){
        return leave_date_to ;
    }

    /**
     * 设置 [LEAVE_DATE_TO]
     */
    @JsonProperty("leave_date_to")
    public void setLeave_date_to(Timestamp  leave_date_to){
        this.leave_date_to = leave_date_to ;
        this.leave_date_toDirtyFlag = true ;
    }

    /**
     * 获取 [LEAVE_DATE_TO]脏标记
     */
    @JsonIgnore
    public boolean getLeave_date_toDirtyFlag(){
        return leave_date_toDirtyFlag ;
    }

    /**
     * 获取 [MANAGER]
     */
    @JsonProperty("manager")
    public String getManager(){
        return manager ;
    }

    /**
     * 设置 [MANAGER]
     */
    @JsonProperty("manager")
    public void setManager(String  manager){
        this.manager = manager ;
        this.managerDirtyFlag = true ;
    }

    /**
     * 获取 [MANAGER]脏标记
     */
    @JsonIgnore
    public boolean getManagerDirtyFlag(){
        return managerDirtyFlag ;
    }

    /**
     * 获取 [WORK_LOCATION]
     */
    @JsonProperty("work_location")
    public String getWork_location(){
        return work_location ;
    }

    /**
     * 设置 [WORK_LOCATION]
     */
    @JsonProperty("work_location")
    public void setWork_location(String  work_location){
        this.work_location = work_location ;
        this.work_locationDirtyFlag = true ;
    }

    /**
     * 获取 [WORK_LOCATION]脏标记
     */
    @JsonIgnore
    public boolean getWork_locationDirtyFlag(){
        return work_locationDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_ATTACHMENT_COUNT]
     */
    @JsonProperty("message_attachment_count")
    public Integer getMessage_attachment_count(){
        return message_attachment_count ;
    }

    /**
     * 设置 [MESSAGE_ATTACHMENT_COUNT]
     */
    @JsonProperty("message_attachment_count")
    public void setMessage_attachment_count(Integer  message_attachment_count){
        this.message_attachment_count = message_attachment_count ;
        this.message_attachment_countDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_ATTACHMENT_COUNT]脏标记
     */
    @JsonIgnore
    public boolean getMessage_attachment_countDirtyFlag(){
        return message_attachment_countDirtyFlag ;
    }

    /**
     * 获取 [CONTRACT_ID]
     */
    @JsonProperty("contract_id")
    public Integer getContract_id(){
        return contract_id ;
    }

    /**
     * 设置 [CONTRACT_ID]
     */
    @JsonProperty("contract_id")
    public void setContract_id(Integer  contract_id){
        this.contract_id = contract_id ;
        this.contract_idDirtyFlag = true ;
    }

    /**
     * 获取 [CONTRACT_ID]脏标记
     */
    @JsonIgnore
    public boolean getContract_idDirtyFlag(){
        return contract_idDirtyFlag ;
    }

    /**
     * 获取 [GOOGLE_DRIVE_LINK]
     */
    @JsonProperty("google_drive_link")
    public String getGoogle_drive_link(){
        return google_drive_link ;
    }

    /**
     * 设置 [GOOGLE_DRIVE_LINK]
     */
    @JsonProperty("google_drive_link")
    public void setGoogle_drive_link(String  google_drive_link){
        this.google_drive_link = google_drive_link ;
        this.google_drive_linkDirtyFlag = true ;
    }

    /**
     * 获取 [GOOGLE_DRIVE_LINK]脏标记
     */
    @JsonIgnore
    public boolean getGoogle_drive_linkDirtyFlag(){
        return google_drive_linkDirtyFlag ;
    }

    /**
     * 获取 [PERMIT_NO]
     */
    @JsonProperty("permit_no")
    public String getPermit_no(){
        return permit_no ;
    }

    /**
     * 设置 [PERMIT_NO]
     */
    @JsonProperty("permit_no")
    public void setPermit_no(String  permit_no){
        this.permit_no = permit_no ;
        this.permit_noDirtyFlag = true ;
    }

    /**
     * 获取 [PERMIT_NO]脏标记
     */
    @JsonIgnore
    public boolean getPermit_noDirtyFlag(){
        return permit_noDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_FOLLOWER_IDS]
     */
    @JsonProperty("message_follower_ids")
    public String getMessage_follower_ids(){
        return message_follower_ids ;
    }

    /**
     * 设置 [MESSAGE_FOLLOWER_IDS]
     */
    @JsonProperty("message_follower_ids")
    public void setMessage_follower_ids(String  message_follower_ids){
        this.message_follower_ids = message_follower_ids ;
        this.message_follower_idsDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_FOLLOWER_IDS]脏标记
     */
    @JsonIgnore
    public boolean getMessage_follower_idsDirtyFlag(){
        return message_follower_idsDirtyFlag ;
    }

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

    /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return idDirtyFlag ;
    }

    /**
     * 获取 [IDENTIFICATION_ID]
     */
    @JsonProperty("identification_id")
    public String getIdentification_id(){
        return identification_id ;
    }

    /**
     * 设置 [IDENTIFICATION_ID]
     */
    @JsonProperty("identification_id")
    public void setIdentification_id(String  identification_id){
        this.identification_id = identification_id ;
        this.identification_idDirtyFlag = true ;
    }

    /**
     * 获取 [IDENTIFICATION_ID]脏标记
     */
    @JsonIgnore
    public boolean getIdentification_idDirtyFlag(){
        return identification_idDirtyFlag ;
    }

    /**
     * 获取 [VEHICLE]
     */
    @JsonProperty("vehicle")
    public String getVehicle(){
        return vehicle ;
    }

    /**
     * 设置 [VEHICLE]
     */
    @JsonProperty("vehicle")
    public void setVehicle(String  vehicle){
        this.vehicle = vehicle ;
        this.vehicleDirtyFlag = true ;
    }

    /**
     * 获取 [VEHICLE]脏标记
     */
    @JsonIgnore
    public boolean getVehicleDirtyFlag(){
        return vehicleDirtyFlag ;
    }

    /**
     * 获取 [CATEGORY_IDS]
     */
    @JsonProperty("category_ids")
    public String getCategory_ids(){
        return category_ids ;
    }

    /**
     * 设置 [CATEGORY_IDS]
     */
    @JsonProperty("category_ids")
    public void setCategory_ids(String  category_ids){
        this.category_ids = category_ids ;
        this.category_idsDirtyFlag = true ;
    }

    /**
     * 获取 [CATEGORY_IDS]脏标记
     */
    @JsonIgnore
    public boolean getCategory_idsDirtyFlag(){
        return category_idsDirtyFlag ;
    }

    /**
     * 获取 [SHOW_LEAVES]
     */
    @JsonProperty("show_leaves")
    public String getShow_leaves(){
        return show_leaves ;
    }

    /**
     * 设置 [SHOW_LEAVES]
     */
    @JsonProperty("show_leaves")
    public void setShow_leaves(String  show_leaves){
        this.show_leaves = show_leaves ;
        this.show_leavesDirtyFlag = true ;
    }

    /**
     * 获取 [SHOW_LEAVES]脏标记
     */
    @JsonIgnore
    public boolean getShow_leavesDirtyFlag(){
        return show_leavesDirtyFlag ;
    }

    /**
     * 获取 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return create_date ;
    }

    /**
     * 设置 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return create_dateDirtyFlag ;
    }

    /**
     * 获取 [PARENT_ID_TEXT]
     */
    @JsonProperty("parent_id_text")
    public String getParent_id_text(){
        return parent_id_text ;
    }

    /**
     * 设置 [PARENT_ID_TEXT]
     */
    @JsonProperty("parent_id_text")
    public void setParent_id_text(String  parent_id_text){
        this.parent_id_text = parent_id_text ;
        this.parent_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [PARENT_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getParent_id_textDirtyFlag(){
        return parent_id_textDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return write_uid_text ;
    }

    /**
     * 设置 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return write_uid_textDirtyFlag ;
    }

    /**
     * 获取 [NAME]
     */
    @JsonProperty("name")
    public String getName(){
        return name ;
    }

    /**
     * 设置 [NAME]
     */
    @JsonProperty("name")
    public void setName(String  name){
        this.name = name ;
        this.nameDirtyFlag = true ;
    }

    /**
     * 获取 [NAME]脏标记
     */
    @JsonIgnore
    public boolean getNameDirtyFlag(){
        return nameDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return create_uid_text ;
    }

    /**
     * 设置 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return create_uid_textDirtyFlag ;
    }

    /**
     * 获取 [ADDRESS_HOME_ID_TEXT]
     */
    @JsonProperty("address_home_id_text")
    public String getAddress_home_id_text(){
        return address_home_id_text ;
    }

    /**
     * 设置 [ADDRESS_HOME_ID_TEXT]
     */
    @JsonProperty("address_home_id_text")
    public void setAddress_home_id_text(String  address_home_id_text){
        this.address_home_id_text = address_home_id_text ;
        this.address_home_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [ADDRESS_HOME_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getAddress_home_id_textDirtyFlag(){
        return address_home_id_textDirtyFlag ;
    }

    /**
     * 获取 [TZ]
     */
    @JsonProperty("tz")
    public String getTz(){
        return tz ;
    }

    /**
     * 设置 [TZ]
     */
    @JsonProperty("tz")
    public void setTz(String  tz){
        this.tz = tz ;
        this.tzDirtyFlag = true ;
    }

    /**
     * 获取 [TZ]脏标记
     */
    @JsonIgnore
    public boolean getTzDirtyFlag(){
        return tzDirtyFlag ;
    }

    /**
     * 获取 [RESOURCE_CALENDAR_ID_TEXT]
     */
    @JsonProperty("resource_calendar_id_text")
    public String getResource_calendar_id_text(){
        return resource_calendar_id_text ;
    }

    /**
     * 设置 [RESOURCE_CALENDAR_ID_TEXT]
     */
    @JsonProperty("resource_calendar_id_text")
    public void setResource_calendar_id_text(String  resource_calendar_id_text){
        this.resource_calendar_id_text = resource_calendar_id_text ;
        this.resource_calendar_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [RESOURCE_CALENDAR_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getResource_calendar_id_textDirtyFlag(){
        return resource_calendar_id_textDirtyFlag ;
    }

    /**
     * 获取 [USER_ID_TEXT]
     */
    @JsonProperty("user_id_text")
    public String getUser_id_text(){
        return user_id_text ;
    }

    /**
     * 设置 [USER_ID_TEXT]
     */
    @JsonProperty("user_id_text")
    public void setUser_id_text(String  user_id_text){
        this.user_id_text = user_id_text ;
        this.user_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [USER_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getUser_id_textDirtyFlag(){
        return user_id_textDirtyFlag ;
    }

    /**
     * 获取 [DEPARTMENT_ID_TEXT]
     */
    @JsonProperty("department_id_text")
    public String getDepartment_id_text(){
        return department_id_text ;
    }

    /**
     * 设置 [DEPARTMENT_ID_TEXT]
     */
    @JsonProperty("department_id_text")
    public void setDepartment_id_text(String  department_id_text){
        this.department_id_text = department_id_text ;
        this.department_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [DEPARTMENT_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getDepartment_id_textDirtyFlag(){
        return department_id_textDirtyFlag ;
    }

    /**
     * 获取 [COUNTRY_OF_BIRTH_TEXT]
     */
    @JsonProperty("country_of_birth_text")
    public String getCountry_of_birth_text(){
        return country_of_birth_text ;
    }

    /**
     * 设置 [COUNTRY_OF_BIRTH_TEXT]
     */
    @JsonProperty("country_of_birth_text")
    public void setCountry_of_birth_text(String  country_of_birth_text){
        this.country_of_birth_text = country_of_birth_text ;
        this.country_of_birth_textDirtyFlag = true ;
    }

    /**
     * 获取 [COUNTRY_OF_BIRTH_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getCountry_of_birth_textDirtyFlag(){
        return country_of_birth_textDirtyFlag ;
    }

    /**
     * 获取 [EXPENSE_MANAGER_ID_TEXT]
     */
    @JsonProperty("expense_manager_id_text")
    public String getExpense_manager_id_text(){
        return expense_manager_id_text ;
    }

    /**
     * 设置 [EXPENSE_MANAGER_ID_TEXT]
     */
    @JsonProperty("expense_manager_id_text")
    public void setExpense_manager_id_text(String  expense_manager_id_text){
        this.expense_manager_id_text = expense_manager_id_text ;
        this.expense_manager_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [EXPENSE_MANAGER_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getExpense_manager_id_textDirtyFlag(){
        return expense_manager_id_textDirtyFlag ;
    }

    /**
     * 获取 [JOB_ID_TEXT]
     */
    @JsonProperty("job_id_text")
    public String getJob_id_text(){
        return job_id_text ;
    }

    /**
     * 设置 [JOB_ID_TEXT]
     */
    @JsonProperty("job_id_text")
    public void setJob_id_text(String  job_id_text){
        this.job_id_text = job_id_text ;
        this.job_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [JOB_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getJob_id_textDirtyFlag(){
        return job_id_textDirtyFlag ;
    }

    /**
     * 获取 [ADDRESS_ID_TEXT]
     */
    @JsonProperty("address_id_text")
    public String getAddress_id_text(){
        return address_id_text ;
    }

    /**
     * 设置 [ADDRESS_ID_TEXT]
     */
    @JsonProperty("address_id_text")
    public void setAddress_id_text(String  address_id_text){
        this.address_id_text = address_id_text ;
        this.address_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [ADDRESS_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getAddress_id_textDirtyFlag(){
        return address_id_textDirtyFlag ;
    }

    /**
     * 获取 [COMPANY_ID_TEXT]
     */
    @JsonProperty("company_id_text")
    public String getCompany_id_text(){
        return company_id_text ;
    }

    /**
     * 设置 [COMPANY_ID_TEXT]
     */
    @JsonProperty("company_id_text")
    public void setCompany_id_text(String  company_id_text){
        this.company_id_text = company_id_text ;
        this.company_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [COMPANY_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getCompany_id_textDirtyFlag(){
        return company_id_textDirtyFlag ;
    }

    /**
     * 获取 [ACTIVE]
     */
    @JsonProperty("active")
    public String getActive(){
        return active ;
    }

    /**
     * 设置 [ACTIVE]
     */
    @JsonProperty("active")
    public void setActive(String  active){
        this.active = active ;
        this.activeDirtyFlag = true ;
    }

    /**
     * 获取 [ACTIVE]脏标记
     */
    @JsonIgnore
    public boolean getActiveDirtyFlag(){
        return activeDirtyFlag ;
    }

    /**
     * 获取 [COACH_ID_TEXT]
     */
    @JsonProperty("coach_id_text")
    public String getCoach_id_text(){
        return coach_id_text ;
    }

    /**
     * 设置 [COACH_ID_TEXT]
     */
    @JsonProperty("coach_id_text")
    public void setCoach_id_text(String  coach_id_text){
        this.coach_id_text = coach_id_text ;
        this.coach_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [COACH_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getCoach_id_textDirtyFlag(){
        return coach_id_textDirtyFlag ;
    }

    /**
     * 获取 [COUNTRY_ID_TEXT]
     */
    @JsonProperty("country_id_text")
    public String getCountry_id_text(){
        return country_id_text ;
    }

    /**
     * 设置 [COUNTRY_ID_TEXT]
     */
    @JsonProperty("country_id_text")
    public void setCountry_id_text(String  country_id_text){
        this.country_id_text = country_id_text ;
        this.country_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [COUNTRY_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getCountry_id_textDirtyFlag(){
        return country_id_textDirtyFlag ;
    }

    /**
     * 获取 [ADDRESS_HOME_ID]
     */
    @JsonProperty("address_home_id")
    public Integer getAddress_home_id(){
        return address_home_id ;
    }

    /**
     * 设置 [ADDRESS_HOME_ID]
     */
    @JsonProperty("address_home_id")
    public void setAddress_home_id(Integer  address_home_id){
        this.address_home_id = address_home_id ;
        this.address_home_idDirtyFlag = true ;
    }

    /**
     * 获取 [ADDRESS_HOME_ID]脏标记
     */
    @JsonIgnore
    public boolean getAddress_home_idDirtyFlag(){
        return address_home_idDirtyFlag ;
    }

    /**
     * 获取 [EXPENSE_MANAGER_ID]
     */
    @JsonProperty("expense_manager_id")
    public Integer getExpense_manager_id(){
        return expense_manager_id ;
    }

    /**
     * 设置 [EXPENSE_MANAGER_ID]
     */
    @JsonProperty("expense_manager_id")
    public void setExpense_manager_id(Integer  expense_manager_id){
        this.expense_manager_id = expense_manager_id ;
        this.expense_manager_idDirtyFlag = true ;
    }

    /**
     * 获取 [EXPENSE_MANAGER_ID]脏标记
     */
    @JsonIgnore
    public boolean getExpense_manager_idDirtyFlag(){
        return expense_manager_idDirtyFlag ;
    }

    /**
     * 获取 [BANK_ACCOUNT_ID]
     */
    @JsonProperty("bank_account_id")
    public Integer getBank_account_id(){
        return bank_account_id ;
    }

    /**
     * 设置 [BANK_ACCOUNT_ID]
     */
    @JsonProperty("bank_account_id")
    public void setBank_account_id(Integer  bank_account_id){
        this.bank_account_id = bank_account_id ;
        this.bank_account_idDirtyFlag = true ;
    }

    /**
     * 获取 [BANK_ACCOUNT_ID]脏标记
     */
    @JsonIgnore
    public boolean getBank_account_idDirtyFlag(){
        return bank_account_idDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return write_uid ;
    }

    /**
     * 设置 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return write_uidDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return create_uid ;
    }

    /**
     * 设置 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return create_uidDirtyFlag ;
    }

    /**
     * 获取 [COUNTRY_ID]
     */
    @JsonProperty("country_id")
    public Integer getCountry_id(){
        return country_id ;
    }

    /**
     * 设置 [COUNTRY_ID]
     */
    @JsonProperty("country_id")
    public void setCountry_id(Integer  country_id){
        this.country_id = country_id ;
        this.country_idDirtyFlag = true ;
    }

    /**
     * 获取 [COUNTRY_ID]脏标记
     */
    @JsonIgnore
    public boolean getCountry_idDirtyFlag(){
        return country_idDirtyFlag ;
    }

    /**
     * 获取 [COMPANY_ID]
     */
    @JsonProperty("company_id")
    public Integer getCompany_id(){
        return company_id ;
    }

    /**
     * 设置 [COMPANY_ID]
     */
    @JsonProperty("company_id")
    public void setCompany_id(Integer  company_id){
        this.company_id = company_id ;
        this.company_idDirtyFlag = true ;
    }

    /**
     * 获取 [COMPANY_ID]脏标记
     */
    @JsonIgnore
    public boolean getCompany_idDirtyFlag(){
        return company_idDirtyFlag ;
    }

    /**
     * 获取 [JOB_ID]
     */
    @JsonProperty("job_id")
    public Integer getJob_id(){
        return job_id ;
    }

    /**
     * 设置 [JOB_ID]
     */
    @JsonProperty("job_id")
    public void setJob_id(Integer  job_id){
        this.job_id = job_id ;
        this.job_idDirtyFlag = true ;
    }

    /**
     * 获取 [JOB_ID]脏标记
     */
    @JsonIgnore
    public boolean getJob_idDirtyFlag(){
        return job_idDirtyFlag ;
    }

    /**
     * 获取 [RESOURCE_ID]
     */
    @JsonProperty("resource_id")
    public Integer getResource_id(){
        return resource_id ;
    }

    /**
     * 设置 [RESOURCE_ID]
     */
    @JsonProperty("resource_id")
    public void setResource_id(Integer  resource_id){
        this.resource_id = resource_id ;
        this.resource_idDirtyFlag = true ;
    }

    /**
     * 获取 [RESOURCE_ID]脏标记
     */
    @JsonIgnore
    public boolean getResource_idDirtyFlag(){
        return resource_idDirtyFlag ;
    }

    /**
     * 获取 [USER_ID]
     */
    @JsonProperty("user_id")
    public Integer getUser_id(){
        return user_id ;
    }

    /**
     * 设置 [USER_ID]
     */
    @JsonProperty("user_id")
    public void setUser_id(Integer  user_id){
        this.user_id = user_id ;
        this.user_idDirtyFlag = true ;
    }

    /**
     * 获取 [USER_ID]脏标记
     */
    @JsonIgnore
    public boolean getUser_idDirtyFlag(){
        return user_idDirtyFlag ;
    }

    /**
     * 获取 [DEPARTMENT_ID]
     */
    @JsonProperty("department_id")
    public Integer getDepartment_id(){
        return department_id ;
    }

    /**
     * 设置 [DEPARTMENT_ID]
     */
    @JsonProperty("department_id")
    public void setDepartment_id(Integer  department_id){
        this.department_id = department_id ;
        this.department_idDirtyFlag = true ;
    }

    /**
     * 获取 [DEPARTMENT_ID]脏标记
     */
    @JsonIgnore
    public boolean getDepartment_idDirtyFlag(){
        return department_idDirtyFlag ;
    }

    /**
     * 获取 [PARENT_ID]
     */
    @JsonProperty("parent_id")
    public Integer getParent_id(){
        return parent_id ;
    }

    /**
     * 设置 [PARENT_ID]
     */
    @JsonProperty("parent_id")
    public void setParent_id(Integer  parent_id){
        this.parent_id = parent_id ;
        this.parent_idDirtyFlag = true ;
    }

    /**
     * 获取 [PARENT_ID]脏标记
     */
    @JsonIgnore
    public boolean getParent_idDirtyFlag(){
        return parent_idDirtyFlag ;
    }

    /**
     * 获取 [LAST_ATTENDANCE_ID]
     */
    @JsonProperty("last_attendance_id")
    public Integer getLast_attendance_id(){
        return last_attendance_id ;
    }

    /**
     * 设置 [LAST_ATTENDANCE_ID]
     */
    @JsonProperty("last_attendance_id")
    public void setLast_attendance_id(Integer  last_attendance_id){
        this.last_attendance_id = last_attendance_id ;
        this.last_attendance_idDirtyFlag = true ;
    }

    /**
     * 获取 [LAST_ATTENDANCE_ID]脏标记
     */
    @JsonIgnore
    public boolean getLast_attendance_idDirtyFlag(){
        return last_attendance_idDirtyFlag ;
    }

    /**
     * 获取 [COACH_ID]
     */
    @JsonProperty("coach_id")
    public Integer getCoach_id(){
        return coach_id ;
    }

    /**
     * 设置 [COACH_ID]
     */
    @JsonProperty("coach_id")
    public void setCoach_id(Integer  coach_id){
        this.coach_id = coach_id ;
        this.coach_idDirtyFlag = true ;
    }

    /**
     * 获取 [COACH_ID]脏标记
     */
    @JsonIgnore
    public boolean getCoach_idDirtyFlag(){
        return coach_idDirtyFlag ;
    }

    /**
     * 获取 [ADDRESS_ID]
     */
    @JsonProperty("address_id")
    public Integer getAddress_id(){
        return address_id ;
    }

    /**
     * 设置 [ADDRESS_ID]
     */
    @JsonProperty("address_id")
    public void setAddress_id(Integer  address_id){
        this.address_id = address_id ;
        this.address_idDirtyFlag = true ;
    }

    /**
     * 获取 [ADDRESS_ID]脏标记
     */
    @JsonIgnore
    public boolean getAddress_idDirtyFlag(){
        return address_idDirtyFlag ;
    }

    /**
     * 获取 [COUNTRY_OF_BIRTH]
     */
    @JsonProperty("country_of_birth")
    public Integer getCountry_of_birth(){
        return country_of_birth ;
    }

    /**
     * 设置 [COUNTRY_OF_BIRTH]
     */
    @JsonProperty("country_of_birth")
    public void setCountry_of_birth(Integer  country_of_birth){
        this.country_of_birth = country_of_birth ;
        this.country_of_birthDirtyFlag = true ;
    }

    /**
     * 获取 [COUNTRY_OF_BIRTH]脏标记
     */
    @JsonIgnore
    public boolean getCountry_of_birthDirtyFlag(){
        return country_of_birthDirtyFlag ;
    }

    /**
     * 获取 [RESOURCE_CALENDAR_ID]
     */
    @JsonProperty("resource_calendar_id")
    public Integer getResource_calendar_id(){
        return resource_calendar_id ;
    }

    /**
     * 设置 [RESOURCE_CALENDAR_ID]
     */
    @JsonProperty("resource_calendar_id")
    public void setResource_calendar_id(Integer  resource_calendar_id){
        this.resource_calendar_id = resource_calendar_id ;
        this.resource_calendar_idDirtyFlag = true ;
    }

    /**
     * 获取 [RESOURCE_CALENDAR_ID]脏标记
     */
    @JsonIgnore
    public boolean getResource_calendar_idDirtyFlag(){
        return resource_calendar_idDirtyFlag ;
    }



    public Hr_employee toDO() {
        Hr_employee srfdomain = new Hr_employee();
        if(getMobile_phoneDirtyFlag())
            srfdomain.setMobile_phone(mobile_phone);
        if(getMessage_has_error_counterDirtyFlag())
            srfdomain.setMessage_has_error_counter(message_has_error_counter);
        if(getMessage_needaction_counterDirtyFlag())
            srfdomain.setMessage_needaction_counter(message_needaction_counter);
        if(getLeave_date_fromDirtyFlag())
            srfdomain.setLeave_date_from(leave_date_from);
        if(getMessage_unreadDirtyFlag())
            srfdomain.setMessage_unread(message_unread);
        if(getChildrenDirtyFlag())
            srfdomain.setChildren(children);
        if(getImage_smallDirtyFlag())
            srfdomain.setImage_small(image_small);
        if(getMessage_needactionDirtyFlag())
            srfdomain.setMessage_needaction(message_needaction);
        if(getPinDirtyFlag())
            srfdomain.setPin(pin);
        if(getMessage_main_attachment_idDirtyFlag())
            srfdomain.setMessage_main_attachment_id(message_main_attachment_id);
        if(getStudy_schoolDirtyFlag())
            srfdomain.setStudy_school(study_school);
        if(getActivity_idsDirtyFlag())
            srfdomain.setActivity_ids(activity_ids);
        if(getMaritalDirtyFlag())
            srfdomain.setMarital(marital);
        if(getDirect_badge_idsDirtyFlag())
            srfdomain.setDirect_badge_ids(direct_badge_ids);
        if(getImage_mediumDirtyFlag())
            srfdomain.setImage_medium(image_medium);
        if(get__last_updateDirtyFlag())
            srfdomain.set__last_update(__last_update);
        if(getEmergency_phoneDirtyFlag())
            srfdomain.setEmergency_phone(emergency_phone);
        if(getActivity_date_deadlineDirtyFlag())
            srfdomain.setActivity_date_deadline(activity_date_deadline);
        if(getKm_home_workDirtyFlag())
            srfdomain.setKm_home_work(km_home_work);
        if(getVisa_noDirtyFlag())
            srfdomain.setVisa_no(visa_no);
        if(getPlace_of_birthDirtyFlag())
            srfdomain.setPlace_of_birth(place_of_birth);
        if(getSpouse_complete_nameDirtyFlag())
            srfdomain.setSpouse_complete_name(spouse_complete_name);
        if(getSinidDirtyFlag())
            srfdomain.setSinid(sinid);
        if(getBadge_idsDirtyFlag())
            srfdomain.setBadge_ids(badge_ids);
        if(getWork_emailDirtyFlag())
            srfdomain.setWork_email(work_email);
        if(getHas_badgesDirtyFlag())
            srfdomain.setHas_badges(has_badges);
        if(getCurrent_leave_idDirtyFlag())
            srfdomain.setCurrent_leave_id(current_leave_id);
        if(getContract_idsDirtyFlag())
            srfdomain.setContract_ids(contract_ids);
        if(getMessage_is_followerDirtyFlag())
            srfdomain.setMessage_is_follower(message_is_follower);
        if(getMessage_channel_idsDirtyFlag())
            srfdomain.setMessage_channel_ids(message_channel_ids);
        if(getBarcodeDirtyFlag())
            srfdomain.setBarcode(barcode);
        if(getBirthdayDirtyFlag())
            srfdomain.setBirthday(birthday);
        if(getCertificateDirtyFlag())
            srfdomain.setCertificate(certificate);
        if(getActivity_user_idDirtyFlag())
            srfdomain.setActivity_user_id(activity_user_id);
        if(getStudy_fieldDirtyFlag())
            srfdomain.setStudy_field(study_field);
        if(getWebsite_message_idsDirtyFlag())
            srfdomain.setWebsite_message_ids(website_message_ids);
        if(getActivity_summaryDirtyFlag())
            srfdomain.setActivity_summary(activity_summary);
        if(getAttendance_stateDirtyFlag())
            srfdomain.setAttendance_state(attendance_state);
        if(getContracts_countDirtyFlag())
            srfdomain.setContracts_count(contracts_count);
        if(getGenderDirtyFlag())
            srfdomain.setGender(gender);
        if(getImageDirtyFlag())
            srfdomain.setImage(image);
        if(getSpouse_birthdateDirtyFlag())
            srfdomain.setSpouse_birthdate(spouse_birthdate);
        if(getMessage_unread_counterDirtyFlag())
            srfdomain.setMessage_unread_counter(message_unread_counter);
        if(getLeaves_countDirtyFlag())
            srfdomain.setLeaves_count(leaves_count);
        if(getNotesDirtyFlag())
            srfdomain.setNotes(notes);
        if(getAdditional_noteDirtyFlag())
            srfdomain.setAdditional_note(additional_note);
        if(getWrite_dateDirtyFlag())
            srfdomain.setWrite_date(write_date);
        if(getMedic_examDirtyFlag())
            srfdomain.setMedic_exam(medic_exam);
        if(getVisa_expireDirtyFlag())
            srfdomain.setVisa_expire(visa_expire);
        if(getWork_phoneDirtyFlag())
            srfdomain.setWork_phone(work_phone);
        if(getEmergency_contactDirtyFlag())
            srfdomain.setEmergency_contact(emergency_contact);
        if(getNewly_hired_employeeDirtyFlag())
            srfdomain.setNewly_hired_employee(newly_hired_employee);
        if(getDisplay_nameDirtyFlag())
            srfdomain.setDisplay_name(display_name);
        if(getIs_address_home_a_companyDirtyFlag())
            srfdomain.setIs_address_home_a_company(is_address_home_a_company);
        if(getAttendance_idsDirtyFlag())
            srfdomain.setAttendance_ids(attendance_ids);
        if(getActivity_stateDirtyFlag())
            srfdomain.setActivity_state(activity_state);
        if(getMessage_has_errorDirtyFlag())
            srfdomain.setMessage_has_error(message_has_error);
        if(getCurrent_leave_stateDirtyFlag())
            srfdomain.setCurrent_leave_state(current_leave_state);
        if(getChild_idsDirtyFlag())
            srfdomain.setChild_ids(child_ids);
        if(getMessage_idsDirtyFlag())
            srfdomain.setMessage_ids(message_ids);
        if(getColorDirtyFlag())
            srfdomain.setColor(color);
        if(getManual_attendanceDirtyFlag())
            srfdomain.setManual_attendance(manual_attendance);
        if(getRemaining_leavesDirtyFlag())
            srfdomain.setRemaining_leaves(remaining_leaves);
        if(getSsnidDirtyFlag())
            srfdomain.setSsnid(ssnid);
        if(getJob_titleDirtyFlag())
            srfdomain.setJob_title(job_title);
        if(getActivity_type_idDirtyFlag())
            srfdomain.setActivity_type_id(activity_type_id);
        if(getIs_absent_totayDirtyFlag())
            srfdomain.setIs_absent_totay(is_absent_totay);
        if(getMessage_partner_idsDirtyFlag())
            srfdomain.setMessage_partner_ids(message_partner_ids);
        if(getPassport_idDirtyFlag())
            srfdomain.setPassport_id(passport_id);
        if(getGoal_idsDirtyFlag())
            srfdomain.setGoal_ids(goal_ids);
        if(getLeave_date_toDirtyFlag())
            srfdomain.setLeave_date_to(leave_date_to);
        if(getManagerDirtyFlag())
            srfdomain.setManager(manager);
        if(getWork_locationDirtyFlag())
            srfdomain.setWork_location(work_location);
        if(getMessage_attachment_countDirtyFlag())
            srfdomain.setMessage_attachment_count(message_attachment_count);
        if(getContract_idDirtyFlag())
            srfdomain.setContract_id(contract_id);
        if(getGoogle_drive_linkDirtyFlag())
            srfdomain.setGoogle_drive_link(google_drive_link);
        if(getPermit_noDirtyFlag())
            srfdomain.setPermit_no(permit_no);
        if(getMessage_follower_idsDirtyFlag())
            srfdomain.setMessage_follower_ids(message_follower_ids);
        if(getIdDirtyFlag())
            srfdomain.setId(id);
        if(getIdentification_idDirtyFlag())
            srfdomain.setIdentification_id(identification_id);
        if(getVehicleDirtyFlag())
            srfdomain.setVehicle(vehicle);
        if(getCategory_idsDirtyFlag())
            srfdomain.setCategory_ids(category_ids);
        if(getShow_leavesDirtyFlag())
            srfdomain.setShow_leaves(show_leaves);
        if(getCreate_dateDirtyFlag())
            srfdomain.setCreate_date(create_date);
        if(getParent_id_textDirtyFlag())
            srfdomain.setParent_id_text(parent_id_text);
        if(getWrite_uid_textDirtyFlag())
            srfdomain.setWrite_uid_text(write_uid_text);
        if(getNameDirtyFlag())
            srfdomain.setName(name);
        if(getCreate_uid_textDirtyFlag())
            srfdomain.setCreate_uid_text(create_uid_text);
        if(getAddress_home_id_textDirtyFlag())
            srfdomain.setAddress_home_id_text(address_home_id_text);
        if(getTzDirtyFlag())
            srfdomain.setTz(tz);
        if(getResource_calendar_id_textDirtyFlag())
            srfdomain.setResource_calendar_id_text(resource_calendar_id_text);
        if(getUser_id_textDirtyFlag())
            srfdomain.setUser_id_text(user_id_text);
        if(getDepartment_id_textDirtyFlag())
            srfdomain.setDepartment_id_text(department_id_text);
        if(getCountry_of_birth_textDirtyFlag())
            srfdomain.setCountry_of_birth_text(country_of_birth_text);
        if(getExpense_manager_id_textDirtyFlag())
            srfdomain.setExpense_manager_id_text(expense_manager_id_text);
        if(getJob_id_textDirtyFlag())
            srfdomain.setJob_id_text(job_id_text);
        if(getAddress_id_textDirtyFlag())
            srfdomain.setAddress_id_text(address_id_text);
        if(getCompany_id_textDirtyFlag())
            srfdomain.setCompany_id_text(company_id_text);
        if(getActiveDirtyFlag())
            srfdomain.setActive(active);
        if(getCoach_id_textDirtyFlag())
            srfdomain.setCoach_id_text(coach_id_text);
        if(getCountry_id_textDirtyFlag())
            srfdomain.setCountry_id_text(country_id_text);
        if(getAddress_home_idDirtyFlag())
            srfdomain.setAddress_home_id(address_home_id);
        if(getExpense_manager_idDirtyFlag())
            srfdomain.setExpense_manager_id(expense_manager_id);
        if(getBank_account_idDirtyFlag())
            srfdomain.setBank_account_id(bank_account_id);
        if(getWrite_uidDirtyFlag())
            srfdomain.setWrite_uid(write_uid);
        if(getCreate_uidDirtyFlag())
            srfdomain.setCreate_uid(create_uid);
        if(getCountry_idDirtyFlag())
            srfdomain.setCountry_id(country_id);
        if(getCompany_idDirtyFlag())
            srfdomain.setCompany_id(company_id);
        if(getJob_idDirtyFlag())
            srfdomain.setJob_id(job_id);
        if(getResource_idDirtyFlag())
            srfdomain.setResource_id(resource_id);
        if(getUser_idDirtyFlag())
            srfdomain.setUser_id(user_id);
        if(getDepartment_idDirtyFlag())
            srfdomain.setDepartment_id(department_id);
        if(getParent_idDirtyFlag())
            srfdomain.setParent_id(parent_id);
        if(getLast_attendance_idDirtyFlag())
            srfdomain.setLast_attendance_id(last_attendance_id);
        if(getCoach_idDirtyFlag())
            srfdomain.setCoach_id(coach_id);
        if(getAddress_idDirtyFlag())
            srfdomain.setAddress_id(address_id);
        if(getCountry_of_birthDirtyFlag())
            srfdomain.setCountry_of_birth(country_of_birth);
        if(getResource_calendar_idDirtyFlag())
            srfdomain.setResource_calendar_id(resource_calendar_id);

        return srfdomain;
    }

    public void fromDO(Hr_employee srfdomain) {
        if(srfdomain == null )
          return ;
        if(srfdomain.getMobile_phoneDirtyFlag())
            this.setMobile_phone(srfdomain.getMobile_phone());
        if(srfdomain.getMessage_has_error_counterDirtyFlag())
            this.setMessage_has_error_counter(srfdomain.getMessage_has_error_counter());
        if(srfdomain.getMessage_needaction_counterDirtyFlag())
            this.setMessage_needaction_counter(srfdomain.getMessage_needaction_counter());
        if(srfdomain.getLeave_date_fromDirtyFlag())
            this.setLeave_date_from(srfdomain.getLeave_date_from());
        if(srfdomain.getMessage_unreadDirtyFlag())
            this.setMessage_unread(srfdomain.getMessage_unread());
        if(srfdomain.getChildrenDirtyFlag())
            this.setChildren(srfdomain.getChildren());
        if(srfdomain.getImage_smallDirtyFlag())
            this.setImage_small(srfdomain.getImage_small());
        if(srfdomain.getMessage_needactionDirtyFlag())
            this.setMessage_needaction(srfdomain.getMessage_needaction());
        if(srfdomain.getPinDirtyFlag())
            this.setPin(srfdomain.getPin());
        if(srfdomain.getMessage_main_attachment_idDirtyFlag())
            this.setMessage_main_attachment_id(srfdomain.getMessage_main_attachment_id());
        if(srfdomain.getStudy_schoolDirtyFlag())
            this.setStudy_school(srfdomain.getStudy_school());
        if(srfdomain.getActivity_idsDirtyFlag())
            this.setActivity_ids(srfdomain.getActivity_ids());
        if(srfdomain.getMaritalDirtyFlag())
            this.setMarital(srfdomain.getMarital());
        if(srfdomain.getDirect_badge_idsDirtyFlag())
            this.setDirect_badge_ids(srfdomain.getDirect_badge_ids());
        if(srfdomain.getImage_mediumDirtyFlag())
            this.setImage_medium(srfdomain.getImage_medium());
        if(srfdomain.get__last_updateDirtyFlag())
            this.set__last_update(srfdomain.get__last_update());
        if(srfdomain.getEmergency_phoneDirtyFlag())
            this.setEmergency_phone(srfdomain.getEmergency_phone());
        if(srfdomain.getActivity_date_deadlineDirtyFlag())
            this.setActivity_date_deadline(srfdomain.getActivity_date_deadline());
        if(srfdomain.getKm_home_workDirtyFlag())
            this.setKm_home_work(srfdomain.getKm_home_work());
        if(srfdomain.getVisa_noDirtyFlag())
            this.setVisa_no(srfdomain.getVisa_no());
        if(srfdomain.getPlace_of_birthDirtyFlag())
            this.setPlace_of_birth(srfdomain.getPlace_of_birth());
        if(srfdomain.getSpouse_complete_nameDirtyFlag())
            this.setSpouse_complete_name(srfdomain.getSpouse_complete_name());
        if(srfdomain.getSinidDirtyFlag())
            this.setSinid(srfdomain.getSinid());
        if(srfdomain.getBadge_idsDirtyFlag())
            this.setBadge_ids(srfdomain.getBadge_ids());
        if(srfdomain.getWork_emailDirtyFlag())
            this.setWork_email(srfdomain.getWork_email());
        if(srfdomain.getHas_badgesDirtyFlag())
            this.setHas_badges(srfdomain.getHas_badges());
        if(srfdomain.getCurrent_leave_idDirtyFlag())
            this.setCurrent_leave_id(srfdomain.getCurrent_leave_id());
        if(srfdomain.getContract_idsDirtyFlag())
            this.setContract_ids(srfdomain.getContract_ids());
        if(srfdomain.getMessage_is_followerDirtyFlag())
            this.setMessage_is_follower(srfdomain.getMessage_is_follower());
        if(srfdomain.getMessage_channel_idsDirtyFlag())
            this.setMessage_channel_ids(srfdomain.getMessage_channel_ids());
        if(srfdomain.getBarcodeDirtyFlag())
            this.setBarcode(srfdomain.getBarcode());
        if(srfdomain.getBirthdayDirtyFlag())
            this.setBirthday(srfdomain.getBirthday());
        if(srfdomain.getCertificateDirtyFlag())
            this.setCertificate(srfdomain.getCertificate());
        if(srfdomain.getActivity_user_idDirtyFlag())
            this.setActivity_user_id(srfdomain.getActivity_user_id());
        if(srfdomain.getStudy_fieldDirtyFlag())
            this.setStudy_field(srfdomain.getStudy_field());
        if(srfdomain.getWebsite_message_idsDirtyFlag())
            this.setWebsite_message_ids(srfdomain.getWebsite_message_ids());
        if(srfdomain.getActivity_summaryDirtyFlag())
            this.setActivity_summary(srfdomain.getActivity_summary());
        if(srfdomain.getAttendance_stateDirtyFlag())
            this.setAttendance_state(srfdomain.getAttendance_state());
        if(srfdomain.getContracts_countDirtyFlag())
            this.setContracts_count(srfdomain.getContracts_count());
        if(srfdomain.getGenderDirtyFlag())
            this.setGender(srfdomain.getGender());
        if(srfdomain.getImageDirtyFlag())
            this.setImage(srfdomain.getImage());
        if(srfdomain.getSpouse_birthdateDirtyFlag())
            this.setSpouse_birthdate(srfdomain.getSpouse_birthdate());
        if(srfdomain.getMessage_unread_counterDirtyFlag())
            this.setMessage_unread_counter(srfdomain.getMessage_unread_counter());
        if(srfdomain.getLeaves_countDirtyFlag())
            this.setLeaves_count(srfdomain.getLeaves_count());
        if(srfdomain.getNotesDirtyFlag())
            this.setNotes(srfdomain.getNotes());
        if(srfdomain.getAdditional_noteDirtyFlag())
            this.setAdditional_note(srfdomain.getAdditional_note());
        if(srfdomain.getWrite_dateDirtyFlag())
            this.setWrite_date(srfdomain.getWrite_date());
        if(srfdomain.getMedic_examDirtyFlag())
            this.setMedic_exam(srfdomain.getMedic_exam());
        if(srfdomain.getVisa_expireDirtyFlag())
            this.setVisa_expire(srfdomain.getVisa_expire());
        if(srfdomain.getWork_phoneDirtyFlag())
            this.setWork_phone(srfdomain.getWork_phone());
        if(srfdomain.getEmergency_contactDirtyFlag())
            this.setEmergency_contact(srfdomain.getEmergency_contact());
        if(srfdomain.getNewly_hired_employeeDirtyFlag())
            this.setNewly_hired_employee(srfdomain.getNewly_hired_employee());
        if(srfdomain.getDisplay_nameDirtyFlag())
            this.setDisplay_name(srfdomain.getDisplay_name());
        if(srfdomain.getIs_address_home_a_companyDirtyFlag())
            this.setIs_address_home_a_company(srfdomain.getIs_address_home_a_company());
        if(srfdomain.getAttendance_idsDirtyFlag())
            this.setAttendance_ids(srfdomain.getAttendance_ids());
        if(srfdomain.getActivity_stateDirtyFlag())
            this.setActivity_state(srfdomain.getActivity_state());
        if(srfdomain.getMessage_has_errorDirtyFlag())
            this.setMessage_has_error(srfdomain.getMessage_has_error());
        if(srfdomain.getCurrent_leave_stateDirtyFlag())
            this.setCurrent_leave_state(srfdomain.getCurrent_leave_state());
        if(srfdomain.getChild_idsDirtyFlag())
            this.setChild_ids(srfdomain.getChild_ids());
        if(srfdomain.getMessage_idsDirtyFlag())
            this.setMessage_ids(srfdomain.getMessage_ids());
        if(srfdomain.getColorDirtyFlag())
            this.setColor(srfdomain.getColor());
        if(srfdomain.getManual_attendanceDirtyFlag())
            this.setManual_attendance(srfdomain.getManual_attendance());
        if(srfdomain.getRemaining_leavesDirtyFlag())
            this.setRemaining_leaves(srfdomain.getRemaining_leaves());
        if(srfdomain.getSsnidDirtyFlag())
            this.setSsnid(srfdomain.getSsnid());
        if(srfdomain.getJob_titleDirtyFlag())
            this.setJob_title(srfdomain.getJob_title());
        if(srfdomain.getActivity_type_idDirtyFlag())
            this.setActivity_type_id(srfdomain.getActivity_type_id());
        if(srfdomain.getIs_absent_totayDirtyFlag())
            this.setIs_absent_totay(srfdomain.getIs_absent_totay());
        if(srfdomain.getMessage_partner_idsDirtyFlag())
            this.setMessage_partner_ids(srfdomain.getMessage_partner_ids());
        if(srfdomain.getPassport_idDirtyFlag())
            this.setPassport_id(srfdomain.getPassport_id());
        if(srfdomain.getGoal_idsDirtyFlag())
            this.setGoal_ids(srfdomain.getGoal_ids());
        if(srfdomain.getLeave_date_toDirtyFlag())
            this.setLeave_date_to(srfdomain.getLeave_date_to());
        if(srfdomain.getManagerDirtyFlag())
            this.setManager(srfdomain.getManager());
        if(srfdomain.getWork_locationDirtyFlag())
            this.setWork_location(srfdomain.getWork_location());
        if(srfdomain.getMessage_attachment_countDirtyFlag())
            this.setMessage_attachment_count(srfdomain.getMessage_attachment_count());
        if(srfdomain.getContract_idDirtyFlag())
            this.setContract_id(srfdomain.getContract_id());
        if(srfdomain.getGoogle_drive_linkDirtyFlag())
            this.setGoogle_drive_link(srfdomain.getGoogle_drive_link());
        if(srfdomain.getPermit_noDirtyFlag())
            this.setPermit_no(srfdomain.getPermit_no());
        if(srfdomain.getMessage_follower_idsDirtyFlag())
            this.setMessage_follower_ids(srfdomain.getMessage_follower_ids());
        if(srfdomain.getIdDirtyFlag())
            this.setId(srfdomain.getId());
        if(srfdomain.getIdentification_idDirtyFlag())
            this.setIdentification_id(srfdomain.getIdentification_id());
        if(srfdomain.getVehicleDirtyFlag())
            this.setVehicle(srfdomain.getVehicle());
        if(srfdomain.getCategory_idsDirtyFlag())
            this.setCategory_ids(srfdomain.getCategory_ids());
        if(srfdomain.getShow_leavesDirtyFlag())
            this.setShow_leaves(srfdomain.getShow_leaves());
        if(srfdomain.getCreate_dateDirtyFlag())
            this.setCreate_date(srfdomain.getCreate_date());
        if(srfdomain.getParent_id_textDirtyFlag())
            this.setParent_id_text(srfdomain.getParent_id_text());
        if(srfdomain.getWrite_uid_textDirtyFlag())
            this.setWrite_uid_text(srfdomain.getWrite_uid_text());
        if(srfdomain.getNameDirtyFlag())
            this.setName(srfdomain.getName());
        if(srfdomain.getCreate_uid_textDirtyFlag())
            this.setCreate_uid_text(srfdomain.getCreate_uid_text());
        if(srfdomain.getAddress_home_id_textDirtyFlag())
            this.setAddress_home_id_text(srfdomain.getAddress_home_id_text());
        if(srfdomain.getTzDirtyFlag())
            this.setTz(srfdomain.getTz());
        if(srfdomain.getResource_calendar_id_textDirtyFlag())
            this.setResource_calendar_id_text(srfdomain.getResource_calendar_id_text());
        if(srfdomain.getUser_id_textDirtyFlag())
            this.setUser_id_text(srfdomain.getUser_id_text());
        if(srfdomain.getDepartment_id_textDirtyFlag())
            this.setDepartment_id_text(srfdomain.getDepartment_id_text());
        if(srfdomain.getCountry_of_birth_textDirtyFlag())
            this.setCountry_of_birth_text(srfdomain.getCountry_of_birth_text());
        if(srfdomain.getExpense_manager_id_textDirtyFlag())
            this.setExpense_manager_id_text(srfdomain.getExpense_manager_id_text());
        if(srfdomain.getJob_id_textDirtyFlag())
            this.setJob_id_text(srfdomain.getJob_id_text());
        if(srfdomain.getAddress_id_textDirtyFlag())
            this.setAddress_id_text(srfdomain.getAddress_id_text());
        if(srfdomain.getCompany_id_textDirtyFlag())
            this.setCompany_id_text(srfdomain.getCompany_id_text());
        if(srfdomain.getActiveDirtyFlag())
            this.setActive(srfdomain.getActive());
        if(srfdomain.getCoach_id_textDirtyFlag())
            this.setCoach_id_text(srfdomain.getCoach_id_text());
        if(srfdomain.getCountry_id_textDirtyFlag())
            this.setCountry_id_text(srfdomain.getCountry_id_text());
        if(srfdomain.getAddress_home_idDirtyFlag())
            this.setAddress_home_id(srfdomain.getAddress_home_id());
        if(srfdomain.getExpense_manager_idDirtyFlag())
            this.setExpense_manager_id(srfdomain.getExpense_manager_id());
        if(srfdomain.getBank_account_idDirtyFlag())
            this.setBank_account_id(srfdomain.getBank_account_id());
        if(srfdomain.getWrite_uidDirtyFlag())
            this.setWrite_uid(srfdomain.getWrite_uid());
        if(srfdomain.getCreate_uidDirtyFlag())
            this.setCreate_uid(srfdomain.getCreate_uid());
        if(srfdomain.getCountry_idDirtyFlag())
            this.setCountry_id(srfdomain.getCountry_id());
        if(srfdomain.getCompany_idDirtyFlag())
            this.setCompany_id(srfdomain.getCompany_id());
        if(srfdomain.getJob_idDirtyFlag())
            this.setJob_id(srfdomain.getJob_id());
        if(srfdomain.getResource_idDirtyFlag())
            this.setResource_id(srfdomain.getResource_id());
        if(srfdomain.getUser_idDirtyFlag())
            this.setUser_id(srfdomain.getUser_id());
        if(srfdomain.getDepartment_idDirtyFlag())
            this.setDepartment_id(srfdomain.getDepartment_id());
        if(srfdomain.getParent_idDirtyFlag())
            this.setParent_id(srfdomain.getParent_id());
        if(srfdomain.getLast_attendance_idDirtyFlag())
            this.setLast_attendance_id(srfdomain.getLast_attendance_id());
        if(srfdomain.getCoach_idDirtyFlag())
            this.setCoach_id(srfdomain.getCoach_id());
        if(srfdomain.getAddress_idDirtyFlag())
            this.setAddress_id(srfdomain.getAddress_id());
        if(srfdomain.getCountry_of_birthDirtyFlag())
            this.setCountry_of_birth(srfdomain.getCountry_of_birth());
        if(srfdomain.getResource_calendar_idDirtyFlag())
            this.setResource_calendar_id(srfdomain.getResource_calendar_id());

    }

    public List<Hr_employeeDTO> fromDOPage(List<Hr_employee> poPage)   {
        if(poPage == null)
            return null;
        List<Hr_employeeDTO> dtos=new ArrayList<Hr_employeeDTO>();
        for(Hr_employee domain : poPage) {
            Hr_employeeDTO dto = new Hr_employeeDTO();
            dto.fromDO(domain);
            dtos.add(dto);
        }
        return dtos;
    }
}

