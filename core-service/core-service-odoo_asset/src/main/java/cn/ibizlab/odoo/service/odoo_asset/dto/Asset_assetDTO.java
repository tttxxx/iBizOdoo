package cn.ibizlab.odoo.service.odoo_asset.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.core.odoo_asset.valuerule.anno.asset_asset.*;
import cn.ibizlab.odoo.core.odoo_asset.domain.Asset_asset;
import org.springframework.cglib.beans.BeanCopier;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 服务DTO对象[Asset_assetDTO]
 */
public class Asset_assetDTO implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [POSITION]
     *
     */
    @Asset_assetPositionDefault(info = "默认规则")
    private String position;

    @JsonIgnore
    private boolean positionDirtyFlag;

    /**
     * 属性 [METER_IDS]
     *
     */
    @Asset_assetMeter_idsDefault(info = "默认规则")
    private String meter_ids;

    @JsonIgnore
    private boolean meter_idsDirtyFlag;

    /**
     * 属性 [MESSAGE_ATTACHMENT_COUNT]
     *
     */
    @Asset_assetMessage_attachment_countDefault(info = "默认规则")
    private Integer message_attachment_count;

    @JsonIgnore
    private boolean message_attachment_countDirtyFlag;

    /**
     * 属性 [START_DATE]
     *
     */
    @Asset_assetStart_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    private Timestamp start_date;

    @JsonIgnore
    private boolean start_dateDirtyFlag;

    /**
     * 属性 [MAINTENANCE_DATE]
     *
     */
    @Asset_assetMaintenance_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp maintenance_date;

    @JsonIgnore
    private boolean maintenance_dateDirtyFlag;

    /**
     * 属性 [IMAGE]
     *
     */
    @Asset_assetImageDefault(info = "默认规则")
    private byte[] image;

    @JsonIgnore
    private boolean imageDirtyFlag;

    /**
     * 属性 [PROPERTY_STOCK_ASSET]
     *
     */
    @Asset_assetProperty_stock_assetDefault(info = "默认规则")
    private Integer property_stock_asset;

    @JsonIgnore
    private boolean property_stock_assetDirtyFlag;

    /**
     * 属性 [MESSAGE_CHANNEL_IDS]
     *
     */
    @Asset_assetMessage_channel_idsDefault(info = "默认规则")
    private String message_channel_ids;

    @JsonIgnore
    private boolean message_channel_idsDirtyFlag;

    /**
     * 属性 [MODEL]
     *
     */
    @Asset_assetModelDefault(info = "默认规则")
    private String model;

    @JsonIgnore
    private boolean modelDirtyFlag;

    /**
     * 属性 [ASSET_NUMBER]
     *
     */
    @Asset_assetAsset_numberDefault(info = "默认规则")
    private String asset_number;

    @JsonIgnore
    private boolean asset_numberDirtyFlag;

    /**
     * 属性 [MESSAGE_UNREAD]
     *
     */
    @Asset_assetMessage_unreadDefault(info = "默认规则")
    private String message_unread;

    @JsonIgnore
    private boolean message_unreadDirtyFlag;

    /**
     * 属性 [MESSAGE_MAIN_ATTACHMENT_ID]
     *
     */
    @Asset_assetMessage_main_attachment_idDefault(info = "默认规则")
    private Integer message_main_attachment_id;

    @JsonIgnore
    private boolean message_main_attachment_idDirtyFlag;

    /**
     * 属性 [SERIAL]
     *
     */
    @Asset_assetSerialDefault(info = "默认规则")
    private String serial;

    @JsonIgnore
    private boolean serialDirtyFlag;

    /**
     * 属性 [MESSAGE_HAS_ERROR_COUNTER]
     *
     */
    @Asset_assetMessage_has_error_counterDefault(info = "默认规则")
    private Integer message_has_error_counter;

    @JsonIgnore
    private boolean message_has_error_counterDirtyFlag;

    /**
     * 属性 [MESSAGE_HAS_ERROR]
     *
     */
    @Asset_assetMessage_has_errorDefault(info = "默认规则")
    private String message_has_error;

    @JsonIgnore
    private boolean message_has_errorDirtyFlag;

    /**
     * 属性 [MESSAGE_PARTNER_IDS]
     *
     */
    @Asset_assetMessage_partner_idsDefault(info = "默认规则")
    private String message_partner_ids;

    @JsonIgnore
    private boolean message_partner_idsDirtyFlag;

    /**
     * 属性 [ACTIVE]
     *
     */
    @Asset_assetActiveDefault(info = "默认规则")
    private String active;

    @JsonIgnore
    private boolean activeDirtyFlag;

    /**
     * 属性 [MESSAGE_FOLLOWER_IDS]
     *
     */
    @Asset_assetMessage_follower_idsDefault(info = "默认规则")
    private String message_follower_ids;

    @JsonIgnore
    private boolean message_follower_idsDirtyFlag;

    /**
     * 属性 [WRITE_DATE]
     *
     */
    @Asset_assetWrite_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp write_date;

    @JsonIgnore
    private boolean write_dateDirtyFlag;

    /**
     * 属性 [MESSAGE_NEEDACTION_COUNTER]
     *
     */
    @Asset_assetMessage_needaction_counterDefault(info = "默认规则")
    private Integer message_needaction_counter;

    @JsonIgnore
    private boolean message_needaction_counterDirtyFlag;

    /**
     * 属性 [PURCHASE_DATE]
     *
     */
    @Asset_assetPurchase_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    private Timestamp purchase_date;

    @JsonIgnore
    private boolean purchase_dateDirtyFlag;

    /**
     * 属性 [IMAGE_MEDIUM]
     *
     */
    @Asset_assetImage_mediumDefault(info = "默认规则")
    private byte[] image_medium;

    @JsonIgnore
    private boolean image_mediumDirtyFlag;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @Asset_assetDisplay_nameDefault(info = "默认规则")
    private String display_name;

    @JsonIgnore
    private boolean display_nameDirtyFlag;

    /**
     * 属性 [ID]
     *
     */
    @Asset_assetIdDefault(info = "默认规则")
    private Integer id;

    @JsonIgnore
    private boolean idDirtyFlag;

    /**
     * 属性 [CATEGORY_IDS]
     *
     */
    @Asset_assetCategory_idsDefault(info = "默认规则")
    private String category_ids;

    @JsonIgnore
    private boolean category_idsDirtyFlag;

    /**
     * 属性 [WARRANTY_START_DATE]
     *
     */
    @Asset_assetWarranty_start_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    private Timestamp warranty_start_date;

    @JsonIgnore
    private boolean warranty_start_dateDirtyFlag;

    /**
     * 属性 [MESSAGE_UNREAD_COUNTER]
     *
     */
    @Asset_assetMessage_unread_counterDefault(info = "默认规则")
    private Integer message_unread_counter;

    @JsonIgnore
    private boolean message_unread_counterDirtyFlag;

    /**
     * 属性 [CREATE_DATE]
     *
     */
    @Asset_assetCreate_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp create_date;

    @JsonIgnore
    private boolean create_dateDirtyFlag;

    /**
     * 属性 [WARRANTY_END_DATE]
     *
     */
    @Asset_assetWarranty_end_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    private Timestamp warranty_end_date;

    @JsonIgnore
    private boolean warranty_end_dateDirtyFlag;

    /**
     * 属性 [WEBSITE_MESSAGE_IDS]
     *
     */
    @Asset_assetWebsite_message_idsDefault(info = "默认规则")
    private String website_message_ids;

    @JsonIgnore
    private boolean website_message_idsDirtyFlag;

    /**
     * 属性 [IMAGE_SMALL]
     *
     */
    @Asset_assetImage_smallDefault(info = "默认规则")
    private byte[] image_small;

    @JsonIgnore
    private boolean image_smallDirtyFlag;

    /**
     * 属性 [NAME]
     *
     */
    @Asset_assetNameDefault(info = "默认规则")
    private String name;

    @JsonIgnore
    private boolean nameDirtyFlag;

    /**
     * 属性 [CRITICALITY]
     *
     */
    @Asset_assetCriticalityDefault(info = "默认规则")
    private String criticality;

    @JsonIgnore
    private boolean criticalityDirtyFlag;

    /**
     * 属性 [MESSAGE_IDS]
     *
     */
    @Asset_assetMessage_idsDefault(info = "默认规则")
    private String message_ids;

    @JsonIgnore
    private boolean message_idsDirtyFlag;

    /**
     * 属性 [MESSAGE_NEEDACTION]
     *
     */
    @Asset_assetMessage_needactionDefault(info = "默认规则")
    private String message_needaction;

    @JsonIgnore
    private boolean message_needactionDirtyFlag;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @Asset_asset__last_updateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp __last_update;

    @JsonIgnore
    private boolean __last_updateDirtyFlag;

    /**
     * 属性 [MRO_COUNT]
     *
     */
    @Asset_assetMro_countDefault(info = "默认规则")
    private Integer mro_count;

    @JsonIgnore
    private boolean mro_countDirtyFlag;

    /**
     * 属性 [MESSAGE_IS_FOLLOWER]
     *
     */
    @Asset_assetMessage_is_followerDefault(info = "默认规则")
    private String message_is_follower;

    @JsonIgnore
    private boolean message_is_followerDirtyFlag;

    /**
     * 属性 [WAREHOUSE_STATE_ID_TEXT]
     *
     */
    @Asset_assetWarehouse_state_id_textDefault(info = "默认规则")
    private String warehouse_state_id_text;

    @JsonIgnore
    private boolean warehouse_state_id_textDirtyFlag;

    /**
     * 属性 [USER_ID_TEXT]
     *
     */
    @Asset_assetUser_id_textDefault(info = "默认规则")
    private String user_id_text;

    @JsonIgnore
    private boolean user_id_textDirtyFlag;

    /**
     * 属性 [CREATE_UID_TEXT]
     *
     */
    @Asset_assetCreate_uid_textDefault(info = "默认规则")
    private String create_uid_text;

    @JsonIgnore
    private boolean create_uid_textDirtyFlag;

    /**
     * 属性 [MAINTENANCE_STATE_COLOR]
     *
     */
    @Asset_assetMaintenance_state_colorDefault(info = "默认规则")
    private String maintenance_state_color;

    @JsonIgnore
    private boolean maintenance_state_colorDirtyFlag;

    /**
     * 属性 [ACCOUNTING_STATE_ID_TEXT]
     *
     */
    @Asset_assetAccounting_state_id_textDefault(info = "默认规则")
    private String accounting_state_id_text;

    @JsonIgnore
    private boolean accounting_state_id_textDirtyFlag;

    /**
     * 属性 [MANUFACTURE_STATE_ID_TEXT]
     *
     */
    @Asset_assetManufacture_state_id_textDefault(info = "默认规则")
    private String manufacture_state_id_text;

    @JsonIgnore
    private boolean manufacture_state_id_textDirtyFlag;

    /**
     * 属性 [FINANCE_STATE_ID_TEXT]
     *
     */
    @Asset_assetFinance_state_id_textDefault(info = "默认规则")
    private String finance_state_id_text;

    @JsonIgnore
    private boolean finance_state_id_textDirtyFlag;

    /**
     * 属性 [MAINTENANCE_STATE_ID_TEXT]
     *
     */
    @Asset_assetMaintenance_state_id_textDefault(info = "默认规则")
    private String maintenance_state_id_text;

    @JsonIgnore
    private boolean maintenance_state_id_textDirtyFlag;

    /**
     * 属性 [MANUFACTURER_ID_TEXT]
     *
     */
    @Asset_assetManufacturer_id_textDefault(info = "默认规则")
    private String manufacturer_id_text;

    @JsonIgnore
    private boolean manufacturer_id_textDirtyFlag;

    /**
     * 属性 [WRITE_UID_TEXT]
     *
     */
    @Asset_assetWrite_uid_textDefault(info = "默认规则")
    private String write_uid_text;

    @JsonIgnore
    private boolean write_uid_textDirtyFlag;

    /**
     * 属性 [VENDOR_ID_TEXT]
     *
     */
    @Asset_assetVendor_id_textDefault(info = "默认规则")
    private String vendor_id_text;

    @JsonIgnore
    private boolean vendor_id_textDirtyFlag;

    /**
     * 属性 [MANUFACTURE_STATE_ID]
     *
     */
    @Asset_assetManufacture_state_idDefault(info = "默认规则")
    private Integer manufacture_state_id;

    @JsonIgnore
    private boolean manufacture_state_idDirtyFlag;

    /**
     * 属性 [ACCOUNTING_STATE_ID]
     *
     */
    @Asset_assetAccounting_state_idDefault(info = "默认规则")
    private Integer accounting_state_id;

    @JsonIgnore
    private boolean accounting_state_idDirtyFlag;

    /**
     * 属性 [CREATE_UID]
     *
     */
    @Asset_assetCreate_uidDefault(info = "默认规则")
    private Integer create_uid;

    @JsonIgnore
    private boolean create_uidDirtyFlag;

    /**
     * 属性 [VENDOR_ID]
     *
     */
    @Asset_assetVendor_idDefault(info = "默认规则")
    private Integer vendor_id;

    @JsonIgnore
    private boolean vendor_idDirtyFlag;

    /**
     * 属性 [WAREHOUSE_STATE_ID]
     *
     */
    @Asset_assetWarehouse_state_idDefault(info = "默认规则")
    private Integer warehouse_state_id;

    @JsonIgnore
    private boolean warehouse_state_idDirtyFlag;

    /**
     * 属性 [WRITE_UID]
     *
     */
    @Asset_assetWrite_uidDefault(info = "默认规则")
    private Integer write_uid;

    @JsonIgnore
    private boolean write_uidDirtyFlag;

    /**
     * 属性 [USER_ID]
     *
     */
    @Asset_assetUser_idDefault(info = "默认规则")
    private Integer user_id;

    @JsonIgnore
    private boolean user_idDirtyFlag;

    /**
     * 属性 [FINANCE_STATE_ID]
     *
     */
    @Asset_assetFinance_state_idDefault(info = "默认规则")
    private Integer finance_state_id;

    @JsonIgnore
    private boolean finance_state_idDirtyFlag;

    /**
     * 属性 [MAINTENANCE_STATE_ID]
     *
     */
    @Asset_assetMaintenance_state_idDefault(info = "默认规则")
    private Integer maintenance_state_id;

    @JsonIgnore
    private boolean maintenance_state_idDirtyFlag;

    /**
     * 属性 [MANUFACTURER_ID]
     *
     */
    @Asset_assetManufacturer_idDefault(info = "默认规则")
    private Integer manufacturer_id;

    @JsonIgnore
    private boolean manufacturer_idDirtyFlag;


    /**
     * 获取 [POSITION]
     */
    @JsonProperty("position")
    public String getPosition(){
        return position ;
    }

    /**
     * 设置 [POSITION]
     */
    @JsonProperty("position")
    public void setPosition(String  position){
        this.position = position ;
        this.positionDirtyFlag = true ;
    }

    /**
     * 获取 [POSITION]脏标记
     */
    @JsonIgnore
    public boolean getPositionDirtyFlag(){
        return positionDirtyFlag ;
    }

    /**
     * 获取 [METER_IDS]
     */
    @JsonProperty("meter_ids")
    public String getMeter_ids(){
        return meter_ids ;
    }

    /**
     * 设置 [METER_IDS]
     */
    @JsonProperty("meter_ids")
    public void setMeter_ids(String  meter_ids){
        this.meter_ids = meter_ids ;
        this.meter_idsDirtyFlag = true ;
    }

    /**
     * 获取 [METER_IDS]脏标记
     */
    @JsonIgnore
    public boolean getMeter_idsDirtyFlag(){
        return meter_idsDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_ATTACHMENT_COUNT]
     */
    @JsonProperty("message_attachment_count")
    public Integer getMessage_attachment_count(){
        return message_attachment_count ;
    }

    /**
     * 设置 [MESSAGE_ATTACHMENT_COUNT]
     */
    @JsonProperty("message_attachment_count")
    public void setMessage_attachment_count(Integer  message_attachment_count){
        this.message_attachment_count = message_attachment_count ;
        this.message_attachment_countDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_ATTACHMENT_COUNT]脏标记
     */
    @JsonIgnore
    public boolean getMessage_attachment_countDirtyFlag(){
        return message_attachment_countDirtyFlag ;
    }

    /**
     * 获取 [START_DATE]
     */
    @JsonProperty("start_date")
    public Timestamp getStart_date(){
        return start_date ;
    }

    /**
     * 设置 [START_DATE]
     */
    @JsonProperty("start_date")
    public void setStart_date(Timestamp  start_date){
        this.start_date = start_date ;
        this.start_dateDirtyFlag = true ;
    }

    /**
     * 获取 [START_DATE]脏标记
     */
    @JsonIgnore
    public boolean getStart_dateDirtyFlag(){
        return start_dateDirtyFlag ;
    }

    /**
     * 获取 [MAINTENANCE_DATE]
     */
    @JsonProperty("maintenance_date")
    public Timestamp getMaintenance_date(){
        return maintenance_date ;
    }

    /**
     * 设置 [MAINTENANCE_DATE]
     */
    @JsonProperty("maintenance_date")
    public void setMaintenance_date(Timestamp  maintenance_date){
        this.maintenance_date = maintenance_date ;
        this.maintenance_dateDirtyFlag = true ;
    }

    /**
     * 获取 [MAINTENANCE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getMaintenance_dateDirtyFlag(){
        return maintenance_dateDirtyFlag ;
    }

    /**
     * 获取 [IMAGE]
     */
    @JsonProperty("image")
    public byte[] getImage(){
        return image ;
    }

    /**
     * 设置 [IMAGE]
     */
    @JsonProperty("image")
    public void setImage(byte[]  image){
        this.image = image ;
        this.imageDirtyFlag = true ;
    }

    /**
     * 获取 [IMAGE]脏标记
     */
    @JsonIgnore
    public boolean getImageDirtyFlag(){
        return imageDirtyFlag ;
    }

    /**
     * 获取 [PROPERTY_STOCK_ASSET]
     */
    @JsonProperty("property_stock_asset")
    public Integer getProperty_stock_asset(){
        return property_stock_asset ;
    }

    /**
     * 设置 [PROPERTY_STOCK_ASSET]
     */
    @JsonProperty("property_stock_asset")
    public void setProperty_stock_asset(Integer  property_stock_asset){
        this.property_stock_asset = property_stock_asset ;
        this.property_stock_assetDirtyFlag = true ;
    }

    /**
     * 获取 [PROPERTY_STOCK_ASSET]脏标记
     */
    @JsonIgnore
    public boolean getProperty_stock_assetDirtyFlag(){
        return property_stock_assetDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_CHANNEL_IDS]
     */
    @JsonProperty("message_channel_ids")
    public String getMessage_channel_ids(){
        return message_channel_ids ;
    }

    /**
     * 设置 [MESSAGE_CHANNEL_IDS]
     */
    @JsonProperty("message_channel_ids")
    public void setMessage_channel_ids(String  message_channel_ids){
        this.message_channel_ids = message_channel_ids ;
        this.message_channel_idsDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_CHANNEL_IDS]脏标记
     */
    @JsonIgnore
    public boolean getMessage_channel_idsDirtyFlag(){
        return message_channel_idsDirtyFlag ;
    }

    /**
     * 获取 [MODEL]
     */
    @JsonProperty("model")
    public String getModel(){
        return model ;
    }

    /**
     * 设置 [MODEL]
     */
    @JsonProperty("model")
    public void setModel(String  model){
        this.model = model ;
        this.modelDirtyFlag = true ;
    }

    /**
     * 获取 [MODEL]脏标记
     */
    @JsonIgnore
    public boolean getModelDirtyFlag(){
        return modelDirtyFlag ;
    }

    /**
     * 获取 [ASSET_NUMBER]
     */
    @JsonProperty("asset_number")
    public String getAsset_number(){
        return asset_number ;
    }

    /**
     * 设置 [ASSET_NUMBER]
     */
    @JsonProperty("asset_number")
    public void setAsset_number(String  asset_number){
        this.asset_number = asset_number ;
        this.asset_numberDirtyFlag = true ;
    }

    /**
     * 获取 [ASSET_NUMBER]脏标记
     */
    @JsonIgnore
    public boolean getAsset_numberDirtyFlag(){
        return asset_numberDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_UNREAD]
     */
    @JsonProperty("message_unread")
    public String getMessage_unread(){
        return message_unread ;
    }

    /**
     * 设置 [MESSAGE_UNREAD]
     */
    @JsonProperty("message_unread")
    public void setMessage_unread(String  message_unread){
        this.message_unread = message_unread ;
        this.message_unreadDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_UNREAD]脏标记
     */
    @JsonIgnore
    public boolean getMessage_unreadDirtyFlag(){
        return message_unreadDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_MAIN_ATTACHMENT_ID]
     */
    @JsonProperty("message_main_attachment_id")
    public Integer getMessage_main_attachment_id(){
        return message_main_attachment_id ;
    }

    /**
     * 设置 [MESSAGE_MAIN_ATTACHMENT_ID]
     */
    @JsonProperty("message_main_attachment_id")
    public void setMessage_main_attachment_id(Integer  message_main_attachment_id){
        this.message_main_attachment_id = message_main_attachment_id ;
        this.message_main_attachment_idDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_MAIN_ATTACHMENT_ID]脏标记
     */
    @JsonIgnore
    public boolean getMessage_main_attachment_idDirtyFlag(){
        return message_main_attachment_idDirtyFlag ;
    }

    /**
     * 获取 [SERIAL]
     */
    @JsonProperty("serial")
    public String getSerial(){
        return serial ;
    }

    /**
     * 设置 [SERIAL]
     */
    @JsonProperty("serial")
    public void setSerial(String  serial){
        this.serial = serial ;
        this.serialDirtyFlag = true ;
    }

    /**
     * 获取 [SERIAL]脏标记
     */
    @JsonIgnore
    public boolean getSerialDirtyFlag(){
        return serialDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_HAS_ERROR_COUNTER]
     */
    @JsonProperty("message_has_error_counter")
    public Integer getMessage_has_error_counter(){
        return message_has_error_counter ;
    }

    /**
     * 设置 [MESSAGE_HAS_ERROR_COUNTER]
     */
    @JsonProperty("message_has_error_counter")
    public void setMessage_has_error_counter(Integer  message_has_error_counter){
        this.message_has_error_counter = message_has_error_counter ;
        this.message_has_error_counterDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_HAS_ERROR_COUNTER]脏标记
     */
    @JsonIgnore
    public boolean getMessage_has_error_counterDirtyFlag(){
        return message_has_error_counterDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_HAS_ERROR]
     */
    @JsonProperty("message_has_error")
    public String getMessage_has_error(){
        return message_has_error ;
    }

    /**
     * 设置 [MESSAGE_HAS_ERROR]
     */
    @JsonProperty("message_has_error")
    public void setMessage_has_error(String  message_has_error){
        this.message_has_error = message_has_error ;
        this.message_has_errorDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_HAS_ERROR]脏标记
     */
    @JsonIgnore
    public boolean getMessage_has_errorDirtyFlag(){
        return message_has_errorDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_PARTNER_IDS]
     */
    @JsonProperty("message_partner_ids")
    public String getMessage_partner_ids(){
        return message_partner_ids ;
    }

    /**
     * 设置 [MESSAGE_PARTNER_IDS]
     */
    @JsonProperty("message_partner_ids")
    public void setMessage_partner_ids(String  message_partner_ids){
        this.message_partner_ids = message_partner_ids ;
        this.message_partner_idsDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_PARTNER_IDS]脏标记
     */
    @JsonIgnore
    public boolean getMessage_partner_idsDirtyFlag(){
        return message_partner_idsDirtyFlag ;
    }

    /**
     * 获取 [ACTIVE]
     */
    @JsonProperty("active")
    public String getActive(){
        return active ;
    }

    /**
     * 设置 [ACTIVE]
     */
    @JsonProperty("active")
    public void setActive(String  active){
        this.active = active ;
        this.activeDirtyFlag = true ;
    }

    /**
     * 获取 [ACTIVE]脏标记
     */
    @JsonIgnore
    public boolean getActiveDirtyFlag(){
        return activeDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_FOLLOWER_IDS]
     */
    @JsonProperty("message_follower_ids")
    public String getMessage_follower_ids(){
        return message_follower_ids ;
    }

    /**
     * 设置 [MESSAGE_FOLLOWER_IDS]
     */
    @JsonProperty("message_follower_ids")
    public void setMessage_follower_ids(String  message_follower_ids){
        this.message_follower_ids = message_follower_ids ;
        this.message_follower_idsDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_FOLLOWER_IDS]脏标记
     */
    @JsonIgnore
    public boolean getMessage_follower_idsDirtyFlag(){
        return message_follower_idsDirtyFlag ;
    }

    /**
     * 获取 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return write_date ;
    }

    /**
     * 设置 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return write_dateDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_NEEDACTION_COUNTER]
     */
    @JsonProperty("message_needaction_counter")
    public Integer getMessage_needaction_counter(){
        return message_needaction_counter ;
    }

    /**
     * 设置 [MESSAGE_NEEDACTION_COUNTER]
     */
    @JsonProperty("message_needaction_counter")
    public void setMessage_needaction_counter(Integer  message_needaction_counter){
        this.message_needaction_counter = message_needaction_counter ;
        this.message_needaction_counterDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_NEEDACTION_COUNTER]脏标记
     */
    @JsonIgnore
    public boolean getMessage_needaction_counterDirtyFlag(){
        return message_needaction_counterDirtyFlag ;
    }

    /**
     * 获取 [PURCHASE_DATE]
     */
    @JsonProperty("purchase_date")
    public Timestamp getPurchase_date(){
        return purchase_date ;
    }

    /**
     * 设置 [PURCHASE_DATE]
     */
    @JsonProperty("purchase_date")
    public void setPurchase_date(Timestamp  purchase_date){
        this.purchase_date = purchase_date ;
        this.purchase_dateDirtyFlag = true ;
    }

    /**
     * 获取 [PURCHASE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getPurchase_dateDirtyFlag(){
        return purchase_dateDirtyFlag ;
    }

    /**
     * 获取 [IMAGE_MEDIUM]
     */
    @JsonProperty("image_medium")
    public byte[] getImage_medium(){
        return image_medium ;
    }

    /**
     * 设置 [IMAGE_MEDIUM]
     */
    @JsonProperty("image_medium")
    public void setImage_medium(byte[]  image_medium){
        this.image_medium = image_medium ;
        this.image_mediumDirtyFlag = true ;
    }

    /**
     * 获取 [IMAGE_MEDIUM]脏标记
     */
    @JsonIgnore
    public boolean getImage_mediumDirtyFlag(){
        return image_mediumDirtyFlag ;
    }

    /**
     * 获取 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return display_name ;
    }

    /**
     * 设置 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

    /**
     * 获取 [DISPLAY_NAME]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return display_nameDirtyFlag ;
    }

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

    /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return idDirtyFlag ;
    }

    /**
     * 获取 [CATEGORY_IDS]
     */
    @JsonProperty("category_ids")
    public String getCategory_ids(){
        return category_ids ;
    }

    /**
     * 设置 [CATEGORY_IDS]
     */
    @JsonProperty("category_ids")
    public void setCategory_ids(String  category_ids){
        this.category_ids = category_ids ;
        this.category_idsDirtyFlag = true ;
    }

    /**
     * 获取 [CATEGORY_IDS]脏标记
     */
    @JsonIgnore
    public boolean getCategory_idsDirtyFlag(){
        return category_idsDirtyFlag ;
    }

    /**
     * 获取 [WARRANTY_START_DATE]
     */
    @JsonProperty("warranty_start_date")
    public Timestamp getWarranty_start_date(){
        return warranty_start_date ;
    }

    /**
     * 设置 [WARRANTY_START_DATE]
     */
    @JsonProperty("warranty_start_date")
    public void setWarranty_start_date(Timestamp  warranty_start_date){
        this.warranty_start_date = warranty_start_date ;
        this.warranty_start_dateDirtyFlag = true ;
    }

    /**
     * 获取 [WARRANTY_START_DATE]脏标记
     */
    @JsonIgnore
    public boolean getWarranty_start_dateDirtyFlag(){
        return warranty_start_dateDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_UNREAD_COUNTER]
     */
    @JsonProperty("message_unread_counter")
    public Integer getMessage_unread_counter(){
        return message_unread_counter ;
    }

    /**
     * 设置 [MESSAGE_UNREAD_COUNTER]
     */
    @JsonProperty("message_unread_counter")
    public void setMessage_unread_counter(Integer  message_unread_counter){
        this.message_unread_counter = message_unread_counter ;
        this.message_unread_counterDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_UNREAD_COUNTER]脏标记
     */
    @JsonIgnore
    public boolean getMessage_unread_counterDirtyFlag(){
        return message_unread_counterDirtyFlag ;
    }

    /**
     * 获取 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return create_date ;
    }

    /**
     * 设置 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return create_dateDirtyFlag ;
    }

    /**
     * 获取 [WARRANTY_END_DATE]
     */
    @JsonProperty("warranty_end_date")
    public Timestamp getWarranty_end_date(){
        return warranty_end_date ;
    }

    /**
     * 设置 [WARRANTY_END_DATE]
     */
    @JsonProperty("warranty_end_date")
    public void setWarranty_end_date(Timestamp  warranty_end_date){
        this.warranty_end_date = warranty_end_date ;
        this.warranty_end_dateDirtyFlag = true ;
    }

    /**
     * 获取 [WARRANTY_END_DATE]脏标记
     */
    @JsonIgnore
    public boolean getWarranty_end_dateDirtyFlag(){
        return warranty_end_dateDirtyFlag ;
    }

    /**
     * 获取 [WEBSITE_MESSAGE_IDS]
     */
    @JsonProperty("website_message_ids")
    public String getWebsite_message_ids(){
        return website_message_ids ;
    }

    /**
     * 设置 [WEBSITE_MESSAGE_IDS]
     */
    @JsonProperty("website_message_ids")
    public void setWebsite_message_ids(String  website_message_ids){
        this.website_message_ids = website_message_ids ;
        this.website_message_idsDirtyFlag = true ;
    }

    /**
     * 获取 [WEBSITE_MESSAGE_IDS]脏标记
     */
    @JsonIgnore
    public boolean getWebsite_message_idsDirtyFlag(){
        return website_message_idsDirtyFlag ;
    }

    /**
     * 获取 [IMAGE_SMALL]
     */
    @JsonProperty("image_small")
    public byte[] getImage_small(){
        return image_small ;
    }

    /**
     * 设置 [IMAGE_SMALL]
     */
    @JsonProperty("image_small")
    public void setImage_small(byte[]  image_small){
        this.image_small = image_small ;
        this.image_smallDirtyFlag = true ;
    }

    /**
     * 获取 [IMAGE_SMALL]脏标记
     */
    @JsonIgnore
    public boolean getImage_smallDirtyFlag(){
        return image_smallDirtyFlag ;
    }

    /**
     * 获取 [NAME]
     */
    @JsonProperty("name")
    public String getName(){
        return name ;
    }

    /**
     * 设置 [NAME]
     */
    @JsonProperty("name")
    public void setName(String  name){
        this.name = name ;
        this.nameDirtyFlag = true ;
    }

    /**
     * 获取 [NAME]脏标记
     */
    @JsonIgnore
    public boolean getNameDirtyFlag(){
        return nameDirtyFlag ;
    }

    /**
     * 获取 [CRITICALITY]
     */
    @JsonProperty("criticality")
    public String getCriticality(){
        return criticality ;
    }

    /**
     * 设置 [CRITICALITY]
     */
    @JsonProperty("criticality")
    public void setCriticality(String  criticality){
        this.criticality = criticality ;
        this.criticalityDirtyFlag = true ;
    }

    /**
     * 获取 [CRITICALITY]脏标记
     */
    @JsonIgnore
    public boolean getCriticalityDirtyFlag(){
        return criticalityDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_IDS]
     */
    @JsonProperty("message_ids")
    public String getMessage_ids(){
        return message_ids ;
    }

    /**
     * 设置 [MESSAGE_IDS]
     */
    @JsonProperty("message_ids")
    public void setMessage_ids(String  message_ids){
        this.message_ids = message_ids ;
        this.message_idsDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_IDS]脏标记
     */
    @JsonIgnore
    public boolean getMessage_idsDirtyFlag(){
        return message_idsDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_NEEDACTION]
     */
    @JsonProperty("message_needaction")
    public String getMessage_needaction(){
        return message_needaction ;
    }

    /**
     * 设置 [MESSAGE_NEEDACTION]
     */
    @JsonProperty("message_needaction")
    public void setMessage_needaction(String  message_needaction){
        this.message_needaction = message_needaction ;
        this.message_needactionDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_NEEDACTION]脏标记
     */
    @JsonIgnore
    public boolean getMessage_needactionDirtyFlag(){
        return message_needactionDirtyFlag ;
    }

    /**
     * 获取 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return __last_update ;
    }

    /**
     * 设置 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

    /**
     * 获取 [__LAST_UPDATE]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return __last_updateDirtyFlag ;
    }

    /**
     * 获取 [MRO_COUNT]
     */
    @JsonProperty("mro_count")
    public Integer getMro_count(){
        return mro_count ;
    }

    /**
     * 设置 [MRO_COUNT]
     */
    @JsonProperty("mro_count")
    public void setMro_count(Integer  mro_count){
        this.mro_count = mro_count ;
        this.mro_countDirtyFlag = true ;
    }

    /**
     * 获取 [MRO_COUNT]脏标记
     */
    @JsonIgnore
    public boolean getMro_countDirtyFlag(){
        return mro_countDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_IS_FOLLOWER]
     */
    @JsonProperty("message_is_follower")
    public String getMessage_is_follower(){
        return message_is_follower ;
    }

    /**
     * 设置 [MESSAGE_IS_FOLLOWER]
     */
    @JsonProperty("message_is_follower")
    public void setMessage_is_follower(String  message_is_follower){
        this.message_is_follower = message_is_follower ;
        this.message_is_followerDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_IS_FOLLOWER]脏标记
     */
    @JsonIgnore
    public boolean getMessage_is_followerDirtyFlag(){
        return message_is_followerDirtyFlag ;
    }

    /**
     * 获取 [WAREHOUSE_STATE_ID_TEXT]
     */
    @JsonProperty("warehouse_state_id_text")
    public String getWarehouse_state_id_text(){
        return warehouse_state_id_text ;
    }

    /**
     * 设置 [WAREHOUSE_STATE_ID_TEXT]
     */
    @JsonProperty("warehouse_state_id_text")
    public void setWarehouse_state_id_text(String  warehouse_state_id_text){
        this.warehouse_state_id_text = warehouse_state_id_text ;
        this.warehouse_state_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [WAREHOUSE_STATE_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getWarehouse_state_id_textDirtyFlag(){
        return warehouse_state_id_textDirtyFlag ;
    }

    /**
     * 获取 [USER_ID_TEXT]
     */
    @JsonProperty("user_id_text")
    public String getUser_id_text(){
        return user_id_text ;
    }

    /**
     * 设置 [USER_ID_TEXT]
     */
    @JsonProperty("user_id_text")
    public void setUser_id_text(String  user_id_text){
        this.user_id_text = user_id_text ;
        this.user_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [USER_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getUser_id_textDirtyFlag(){
        return user_id_textDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return create_uid_text ;
    }

    /**
     * 设置 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return create_uid_textDirtyFlag ;
    }

    /**
     * 获取 [MAINTENANCE_STATE_COLOR]
     */
    @JsonProperty("maintenance_state_color")
    public String getMaintenance_state_color(){
        return maintenance_state_color ;
    }

    /**
     * 设置 [MAINTENANCE_STATE_COLOR]
     */
    @JsonProperty("maintenance_state_color")
    public void setMaintenance_state_color(String  maintenance_state_color){
        this.maintenance_state_color = maintenance_state_color ;
        this.maintenance_state_colorDirtyFlag = true ;
    }

    /**
     * 获取 [MAINTENANCE_STATE_COLOR]脏标记
     */
    @JsonIgnore
    public boolean getMaintenance_state_colorDirtyFlag(){
        return maintenance_state_colorDirtyFlag ;
    }

    /**
     * 获取 [ACCOUNTING_STATE_ID_TEXT]
     */
    @JsonProperty("accounting_state_id_text")
    public String getAccounting_state_id_text(){
        return accounting_state_id_text ;
    }

    /**
     * 设置 [ACCOUNTING_STATE_ID_TEXT]
     */
    @JsonProperty("accounting_state_id_text")
    public void setAccounting_state_id_text(String  accounting_state_id_text){
        this.accounting_state_id_text = accounting_state_id_text ;
        this.accounting_state_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [ACCOUNTING_STATE_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getAccounting_state_id_textDirtyFlag(){
        return accounting_state_id_textDirtyFlag ;
    }

    /**
     * 获取 [MANUFACTURE_STATE_ID_TEXT]
     */
    @JsonProperty("manufacture_state_id_text")
    public String getManufacture_state_id_text(){
        return manufacture_state_id_text ;
    }

    /**
     * 设置 [MANUFACTURE_STATE_ID_TEXT]
     */
    @JsonProperty("manufacture_state_id_text")
    public void setManufacture_state_id_text(String  manufacture_state_id_text){
        this.manufacture_state_id_text = manufacture_state_id_text ;
        this.manufacture_state_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [MANUFACTURE_STATE_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getManufacture_state_id_textDirtyFlag(){
        return manufacture_state_id_textDirtyFlag ;
    }

    /**
     * 获取 [FINANCE_STATE_ID_TEXT]
     */
    @JsonProperty("finance_state_id_text")
    public String getFinance_state_id_text(){
        return finance_state_id_text ;
    }

    /**
     * 设置 [FINANCE_STATE_ID_TEXT]
     */
    @JsonProperty("finance_state_id_text")
    public void setFinance_state_id_text(String  finance_state_id_text){
        this.finance_state_id_text = finance_state_id_text ;
        this.finance_state_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [FINANCE_STATE_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getFinance_state_id_textDirtyFlag(){
        return finance_state_id_textDirtyFlag ;
    }

    /**
     * 获取 [MAINTENANCE_STATE_ID_TEXT]
     */
    @JsonProperty("maintenance_state_id_text")
    public String getMaintenance_state_id_text(){
        return maintenance_state_id_text ;
    }

    /**
     * 设置 [MAINTENANCE_STATE_ID_TEXT]
     */
    @JsonProperty("maintenance_state_id_text")
    public void setMaintenance_state_id_text(String  maintenance_state_id_text){
        this.maintenance_state_id_text = maintenance_state_id_text ;
        this.maintenance_state_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [MAINTENANCE_STATE_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getMaintenance_state_id_textDirtyFlag(){
        return maintenance_state_id_textDirtyFlag ;
    }

    /**
     * 获取 [MANUFACTURER_ID_TEXT]
     */
    @JsonProperty("manufacturer_id_text")
    public String getManufacturer_id_text(){
        return manufacturer_id_text ;
    }

    /**
     * 设置 [MANUFACTURER_ID_TEXT]
     */
    @JsonProperty("manufacturer_id_text")
    public void setManufacturer_id_text(String  manufacturer_id_text){
        this.manufacturer_id_text = manufacturer_id_text ;
        this.manufacturer_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [MANUFACTURER_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getManufacturer_id_textDirtyFlag(){
        return manufacturer_id_textDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return write_uid_text ;
    }

    /**
     * 设置 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return write_uid_textDirtyFlag ;
    }

    /**
     * 获取 [VENDOR_ID_TEXT]
     */
    @JsonProperty("vendor_id_text")
    public String getVendor_id_text(){
        return vendor_id_text ;
    }

    /**
     * 设置 [VENDOR_ID_TEXT]
     */
    @JsonProperty("vendor_id_text")
    public void setVendor_id_text(String  vendor_id_text){
        this.vendor_id_text = vendor_id_text ;
        this.vendor_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [VENDOR_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getVendor_id_textDirtyFlag(){
        return vendor_id_textDirtyFlag ;
    }

    /**
     * 获取 [MANUFACTURE_STATE_ID]
     */
    @JsonProperty("manufacture_state_id")
    public Integer getManufacture_state_id(){
        return manufacture_state_id ;
    }

    /**
     * 设置 [MANUFACTURE_STATE_ID]
     */
    @JsonProperty("manufacture_state_id")
    public void setManufacture_state_id(Integer  manufacture_state_id){
        this.manufacture_state_id = manufacture_state_id ;
        this.manufacture_state_idDirtyFlag = true ;
    }

    /**
     * 获取 [MANUFACTURE_STATE_ID]脏标记
     */
    @JsonIgnore
    public boolean getManufacture_state_idDirtyFlag(){
        return manufacture_state_idDirtyFlag ;
    }

    /**
     * 获取 [ACCOUNTING_STATE_ID]
     */
    @JsonProperty("accounting_state_id")
    public Integer getAccounting_state_id(){
        return accounting_state_id ;
    }

    /**
     * 设置 [ACCOUNTING_STATE_ID]
     */
    @JsonProperty("accounting_state_id")
    public void setAccounting_state_id(Integer  accounting_state_id){
        this.accounting_state_id = accounting_state_id ;
        this.accounting_state_idDirtyFlag = true ;
    }

    /**
     * 获取 [ACCOUNTING_STATE_ID]脏标记
     */
    @JsonIgnore
    public boolean getAccounting_state_idDirtyFlag(){
        return accounting_state_idDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return create_uid ;
    }

    /**
     * 设置 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return create_uidDirtyFlag ;
    }

    /**
     * 获取 [VENDOR_ID]
     */
    @JsonProperty("vendor_id")
    public Integer getVendor_id(){
        return vendor_id ;
    }

    /**
     * 设置 [VENDOR_ID]
     */
    @JsonProperty("vendor_id")
    public void setVendor_id(Integer  vendor_id){
        this.vendor_id = vendor_id ;
        this.vendor_idDirtyFlag = true ;
    }

    /**
     * 获取 [VENDOR_ID]脏标记
     */
    @JsonIgnore
    public boolean getVendor_idDirtyFlag(){
        return vendor_idDirtyFlag ;
    }

    /**
     * 获取 [WAREHOUSE_STATE_ID]
     */
    @JsonProperty("warehouse_state_id")
    public Integer getWarehouse_state_id(){
        return warehouse_state_id ;
    }

    /**
     * 设置 [WAREHOUSE_STATE_ID]
     */
    @JsonProperty("warehouse_state_id")
    public void setWarehouse_state_id(Integer  warehouse_state_id){
        this.warehouse_state_id = warehouse_state_id ;
        this.warehouse_state_idDirtyFlag = true ;
    }

    /**
     * 获取 [WAREHOUSE_STATE_ID]脏标记
     */
    @JsonIgnore
    public boolean getWarehouse_state_idDirtyFlag(){
        return warehouse_state_idDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return write_uid ;
    }

    /**
     * 设置 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return write_uidDirtyFlag ;
    }

    /**
     * 获取 [USER_ID]
     */
    @JsonProperty("user_id")
    public Integer getUser_id(){
        return user_id ;
    }

    /**
     * 设置 [USER_ID]
     */
    @JsonProperty("user_id")
    public void setUser_id(Integer  user_id){
        this.user_id = user_id ;
        this.user_idDirtyFlag = true ;
    }

    /**
     * 获取 [USER_ID]脏标记
     */
    @JsonIgnore
    public boolean getUser_idDirtyFlag(){
        return user_idDirtyFlag ;
    }

    /**
     * 获取 [FINANCE_STATE_ID]
     */
    @JsonProperty("finance_state_id")
    public Integer getFinance_state_id(){
        return finance_state_id ;
    }

    /**
     * 设置 [FINANCE_STATE_ID]
     */
    @JsonProperty("finance_state_id")
    public void setFinance_state_id(Integer  finance_state_id){
        this.finance_state_id = finance_state_id ;
        this.finance_state_idDirtyFlag = true ;
    }

    /**
     * 获取 [FINANCE_STATE_ID]脏标记
     */
    @JsonIgnore
    public boolean getFinance_state_idDirtyFlag(){
        return finance_state_idDirtyFlag ;
    }

    /**
     * 获取 [MAINTENANCE_STATE_ID]
     */
    @JsonProperty("maintenance_state_id")
    public Integer getMaintenance_state_id(){
        return maintenance_state_id ;
    }

    /**
     * 设置 [MAINTENANCE_STATE_ID]
     */
    @JsonProperty("maintenance_state_id")
    public void setMaintenance_state_id(Integer  maintenance_state_id){
        this.maintenance_state_id = maintenance_state_id ;
        this.maintenance_state_idDirtyFlag = true ;
    }

    /**
     * 获取 [MAINTENANCE_STATE_ID]脏标记
     */
    @JsonIgnore
    public boolean getMaintenance_state_idDirtyFlag(){
        return maintenance_state_idDirtyFlag ;
    }

    /**
     * 获取 [MANUFACTURER_ID]
     */
    @JsonProperty("manufacturer_id")
    public Integer getManufacturer_id(){
        return manufacturer_id ;
    }

    /**
     * 设置 [MANUFACTURER_ID]
     */
    @JsonProperty("manufacturer_id")
    public void setManufacturer_id(Integer  manufacturer_id){
        this.manufacturer_id = manufacturer_id ;
        this.manufacturer_idDirtyFlag = true ;
    }

    /**
     * 获取 [MANUFACTURER_ID]脏标记
     */
    @JsonIgnore
    public boolean getManufacturer_idDirtyFlag(){
        return manufacturer_idDirtyFlag ;
    }



    public Asset_asset toDO() {
        Asset_asset srfdomain = new Asset_asset();
        if(getPositionDirtyFlag())
            srfdomain.setPosition(position);
        if(getMeter_idsDirtyFlag())
            srfdomain.setMeter_ids(meter_ids);
        if(getMessage_attachment_countDirtyFlag())
            srfdomain.setMessage_attachment_count(message_attachment_count);
        if(getStart_dateDirtyFlag())
            srfdomain.setStart_date(start_date);
        if(getMaintenance_dateDirtyFlag())
            srfdomain.setMaintenance_date(maintenance_date);
        if(getImageDirtyFlag())
            srfdomain.setImage(image);
        if(getProperty_stock_assetDirtyFlag())
            srfdomain.setProperty_stock_asset(property_stock_asset);
        if(getMessage_channel_idsDirtyFlag())
            srfdomain.setMessage_channel_ids(message_channel_ids);
        if(getModelDirtyFlag())
            srfdomain.setModel(model);
        if(getAsset_numberDirtyFlag())
            srfdomain.setAsset_number(asset_number);
        if(getMessage_unreadDirtyFlag())
            srfdomain.setMessage_unread(message_unread);
        if(getMessage_main_attachment_idDirtyFlag())
            srfdomain.setMessage_main_attachment_id(message_main_attachment_id);
        if(getSerialDirtyFlag())
            srfdomain.setSerial(serial);
        if(getMessage_has_error_counterDirtyFlag())
            srfdomain.setMessage_has_error_counter(message_has_error_counter);
        if(getMessage_has_errorDirtyFlag())
            srfdomain.setMessage_has_error(message_has_error);
        if(getMessage_partner_idsDirtyFlag())
            srfdomain.setMessage_partner_ids(message_partner_ids);
        if(getActiveDirtyFlag())
            srfdomain.setActive(active);
        if(getMessage_follower_idsDirtyFlag())
            srfdomain.setMessage_follower_ids(message_follower_ids);
        if(getWrite_dateDirtyFlag())
            srfdomain.setWrite_date(write_date);
        if(getMessage_needaction_counterDirtyFlag())
            srfdomain.setMessage_needaction_counter(message_needaction_counter);
        if(getPurchase_dateDirtyFlag())
            srfdomain.setPurchase_date(purchase_date);
        if(getImage_mediumDirtyFlag())
            srfdomain.setImage_medium(image_medium);
        if(getDisplay_nameDirtyFlag())
            srfdomain.setDisplay_name(display_name);
        if(getIdDirtyFlag())
            srfdomain.setId(id);
        if(getCategory_idsDirtyFlag())
            srfdomain.setCategory_ids(category_ids);
        if(getWarranty_start_dateDirtyFlag())
            srfdomain.setWarranty_start_date(warranty_start_date);
        if(getMessage_unread_counterDirtyFlag())
            srfdomain.setMessage_unread_counter(message_unread_counter);
        if(getCreate_dateDirtyFlag())
            srfdomain.setCreate_date(create_date);
        if(getWarranty_end_dateDirtyFlag())
            srfdomain.setWarranty_end_date(warranty_end_date);
        if(getWebsite_message_idsDirtyFlag())
            srfdomain.setWebsite_message_ids(website_message_ids);
        if(getImage_smallDirtyFlag())
            srfdomain.setImage_small(image_small);
        if(getNameDirtyFlag())
            srfdomain.setName(name);
        if(getCriticalityDirtyFlag())
            srfdomain.setCriticality(criticality);
        if(getMessage_idsDirtyFlag())
            srfdomain.setMessage_ids(message_ids);
        if(getMessage_needactionDirtyFlag())
            srfdomain.setMessage_needaction(message_needaction);
        if(get__last_updateDirtyFlag())
            srfdomain.set__last_update(__last_update);
        if(getMro_countDirtyFlag())
            srfdomain.setMro_count(mro_count);
        if(getMessage_is_followerDirtyFlag())
            srfdomain.setMessage_is_follower(message_is_follower);
        if(getWarehouse_state_id_textDirtyFlag())
            srfdomain.setWarehouse_state_id_text(warehouse_state_id_text);
        if(getUser_id_textDirtyFlag())
            srfdomain.setUser_id_text(user_id_text);
        if(getCreate_uid_textDirtyFlag())
            srfdomain.setCreate_uid_text(create_uid_text);
        if(getMaintenance_state_colorDirtyFlag())
            srfdomain.setMaintenance_state_color(maintenance_state_color);
        if(getAccounting_state_id_textDirtyFlag())
            srfdomain.setAccounting_state_id_text(accounting_state_id_text);
        if(getManufacture_state_id_textDirtyFlag())
            srfdomain.setManufacture_state_id_text(manufacture_state_id_text);
        if(getFinance_state_id_textDirtyFlag())
            srfdomain.setFinance_state_id_text(finance_state_id_text);
        if(getMaintenance_state_id_textDirtyFlag())
            srfdomain.setMaintenance_state_id_text(maintenance_state_id_text);
        if(getManufacturer_id_textDirtyFlag())
            srfdomain.setManufacturer_id_text(manufacturer_id_text);
        if(getWrite_uid_textDirtyFlag())
            srfdomain.setWrite_uid_text(write_uid_text);
        if(getVendor_id_textDirtyFlag())
            srfdomain.setVendor_id_text(vendor_id_text);
        if(getManufacture_state_idDirtyFlag())
            srfdomain.setManufacture_state_id(manufacture_state_id);
        if(getAccounting_state_idDirtyFlag())
            srfdomain.setAccounting_state_id(accounting_state_id);
        if(getCreate_uidDirtyFlag())
            srfdomain.setCreate_uid(create_uid);
        if(getVendor_idDirtyFlag())
            srfdomain.setVendor_id(vendor_id);
        if(getWarehouse_state_idDirtyFlag())
            srfdomain.setWarehouse_state_id(warehouse_state_id);
        if(getWrite_uidDirtyFlag())
            srfdomain.setWrite_uid(write_uid);
        if(getUser_idDirtyFlag())
            srfdomain.setUser_id(user_id);
        if(getFinance_state_idDirtyFlag())
            srfdomain.setFinance_state_id(finance_state_id);
        if(getMaintenance_state_idDirtyFlag())
            srfdomain.setMaintenance_state_id(maintenance_state_id);
        if(getManufacturer_idDirtyFlag())
            srfdomain.setManufacturer_id(manufacturer_id);

        return srfdomain;
    }

    public void fromDO(Asset_asset srfdomain) {
        if(srfdomain == null )
          return ;
        if(srfdomain.getPositionDirtyFlag())
            this.setPosition(srfdomain.getPosition());
        if(srfdomain.getMeter_idsDirtyFlag())
            this.setMeter_ids(srfdomain.getMeter_ids());
        if(srfdomain.getMessage_attachment_countDirtyFlag())
            this.setMessage_attachment_count(srfdomain.getMessage_attachment_count());
        if(srfdomain.getStart_dateDirtyFlag())
            this.setStart_date(srfdomain.getStart_date());
        if(srfdomain.getMaintenance_dateDirtyFlag())
            this.setMaintenance_date(srfdomain.getMaintenance_date());
        if(srfdomain.getImageDirtyFlag())
            this.setImage(srfdomain.getImage());
        if(srfdomain.getProperty_stock_assetDirtyFlag())
            this.setProperty_stock_asset(srfdomain.getProperty_stock_asset());
        if(srfdomain.getMessage_channel_idsDirtyFlag())
            this.setMessage_channel_ids(srfdomain.getMessage_channel_ids());
        if(srfdomain.getModelDirtyFlag())
            this.setModel(srfdomain.getModel());
        if(srfdomain.getAsset_numberDirtyFlag())
            this.setAsset_number(srfdomain.getAsset_number());
        if(srfdomain.getMessage_unreadDirtyFlag())
            this.setMessage_unread(srfdomain.getMessage_unread());
        if(srfdomain.getMessage_main_attachment_idDirtyFlag())
            this.setMessage_main_attachment_id(srfdomain.getMessage_main_attachment_id());
        if(srfdomain.getSerialDirtyFlag())
            this.setSerial(srfdomain.getSerial());
        if(srfdomain.getMessage_has_error_counterDirtyFlag())
            this.setMessage_has_error_counter(srfdomain.getMessage_has_error_counter());
        if(srfdomain.getMessage_has_errorDirtyFlag())
            this.setMessage_has_error(srfdomain.getMessage_has_error());
        if(srfdomain.getMessage_partner_idsDirtyFlag())
            this.setMessage_partner_ids(srfdomain.getMessage_partner_ids());
        if(srfdomain.getActiveDirtyFlag())
            this.setActive(srfdomain.getActive());
        if(srfdomain.getMessage_follower_idsDirtyFlag())
            this.setMessage_follower_ids(srfdomain.getMessage_follower_ids());
        if(srfdomain.getWrite_dateDirtyFlag())
            this.setWrite_date(srfdomain.getWrite_date());
        if(srfdomain.getMessage_needaction_counterDirtyFlag())
            this.setMessage_needaction_counter(srfdomain.getMessage_needaction_counter());
        if(srfdomain.getPurchase_dateDirtyFlag())
            this.setPurchase_date(srfdomain.getPurchase_date());
        if(srfdomain.getImage_mediumDirtyFlag())
            this.setImage_medium(srfdomain.getImage_medium());
        if(srfdomain.getDisplay_nameDirtyFlag())
            this.setDisplay_name(srfdomain.getDisplay_name());
        if(srfdomain.getIdDirtyFlag())
            this.setId(srfdomain.getId());
        if(srfdomain.getCategory_idsDirtyFlag())
            this.setCategory_ids(srfdomain.getCategory_ids());
        if(srfdomain.getWarranty_start_dateDirtyFlag())
            this.setWarranty_start_date(srfdomain.getWarranty_start_date());
        if(srfdomain.getMessage_unread_counterDirtyFlag())
            this.setMessage_unread_counter(srfdomain.getMessage_unread_counter());
        if(srfdomain.getCreate_dateDirtyFlag())
            this.setCreate_date(srfdomain.getCreate_date());
        if(srfdomain.getWarranty_end_dateDirtyFlag())
            this.setWarranty_end_date(srfdomain.getWarranty_end_date());
        if(srfdomain.getWebsite_message_idsDirtyFlag())
            this.setWebsite_message_ids(srfdomain.getWebsite_message_ids());
        if(srfdomain.getImage_smallDirtyFlag())
            this.setImage_small(srfdomain.getImage_small());
        if(srfdomain.getNameDirtyFlag())
            this.setName(srfdomain.getName());
        if(srfdomain.getCriticalityDirtyFlag())
            this.setCriticality(srfdomain.getCriticality());
        if(srfdomain.getMessage_idsDirtyFlag())
            this.setMessage_ids(srfdomain.getMessage_ids());
        if(srfdomain.getMessage_needactionDirtyFlag())
            this.setMessage_needaction(srfdomain.getMessage_needaction());
        if(srfdomain.get__last_updateDirtyFlag())
            this.set__last_update(srfdomain.get__last_update());
        if(srfdomain.getMro_countDirtyFlag())
            this.setMro_count(srfdomain.getMro_count());
        if(srfdomain.getMessage_is_followerDirtyFlag())
            this.setMessage_is_follower(srfdomain.getMessage_is_follower());
        if(srfdomain.getWarehouse_state_id_textDirtyFlag())
            this.setWarehouse_state_id_text(srfdomain.getWarehouse_state_id_text());
        if(srfdomain.getUser_id_textDirtyFlag())
            this.setUser_id_text(srfdomain.getUser_id_text());
        if(srfdomain.getCreate_uid_textDirtyFlag())
            this.setCreate_uid_text(srfdomain.getCreate_uid_text());
        if(srfdomain.getMaintenance_state_colorDirtyFlag())
            this.setMaintenance_state_color(srfdomain.getMaintenance_state_color());
        if(srfdomain.getAccounting_state_id_textDirtyFlag())
            this.setAccounting_state_id_text(srfdomain.getAccounting_state_id_text());
        if(srfdomain.getManufacture_state_id_textDirtyFlag())
            this.setManufacture_state_id_text(srfdomain.getManufacture_state_id_text());
        if(srfdomain.getFinance_state_id_textDirtyFlag())
            this.setFinance_state_id_text(srfdomain.getFinance_state_id_text());
        if(srfdomain.getMaintenance_state_id_textDirtyFlag())
            this.setMaintenance_state_id_text(srfdomain.getMaintenance_state_id_text());
        if(srfdomain.getManufacturer_id_textDirtyFlag())
            this.setManufacturer_id_text(srfdomain.getManufacturer_id_text());
        if(srfdomain.getWrite_uid_textDirtyFlag())
            this.setWrite_uid_text(srfdomain.getWrite_uid_text());
        if(srfdomain.getVendor_id_textDirtyFlag())
            this.setVendor_id_text(srfdomain.getVendor_id_text());
        if(srfdomain.getManufacture_state_idDirtyFlag())
            this.setManufacture_state_id(srfdomain.getManufacture_state_id());
        if(srfdomain.getAccounting_state_idDirtyFlag())
            this.setAccounting_state_id(srfdomain.getAccounting_state_id());
        if(srfdomain.getCreate_uidDirtyFlag())
            this.setCreate_uid(srfdomain.getCreate_uid());
        if(srfdomain.getVendor_idDirtyFlag())
            this.setVendor_id(srfdomain.getVendor_id());
        if(srfdomain.getWarehouse_state_idDirtyFlag())
            this.setWarehouse_state_id(srfdomain.getWarehouse_state_id());
        if(srfdomain.getWrite_uidDirtyFlag())
            this.setWrite_uid(srfdomain.getWrite_uid());
        if(srfdomain.getUser_idDirtyFlag())
            this.setUser_id(srfdomain.getUser_id());
        if(srfdomain.getFinance_state_idDirtyFlag())
            this.setFinance_state_id(srfdomain.getFinance_state_id());
        if(srfdomain.getMaintenance_state_idDirtyFlag())
            this.setMaintenance_state_id(srfdomain.getMaintenance_state_id());
        if(srfdomain.getManufacturer_idDirtyFlag())
            this.setManufacturer_id(srfdomain.getManufacturer_id());

    }

    public List<Asset_assetDTO> fromDOPage(List<Asset_asset> poPage)   {
        if(poPage == null)
            return null;
        List<Asset_assetDTO> dtos=new ArrayList<Asset_assetDTO>();
        for(Asset_asset domain : poPage) {
            Asset_assetDTO dto = new Asset_assetDTO();
            dto.fromDO(domain);
            dtos.add(dto);
        }
        return dtos;
    }
}

