package cn.ibizlab.odoo.service.odoo_asset.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_asset.dto.Asset_categoryDTO;
import cn.ibizlab.odoo.core.odoo_asset.domain.Asset_category;
import cn.ibizlab.odoo.core.odoo_asset.service.IAsset_categoryService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_asset.filter.Asset_categorySearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Asset_category" })
@RestController
@RequestMapping("")
public class Asset_categoryResource {

    @Autowired
    private IAsset_categoryService asset_categoryService;

    public IAsset_categoryService getAsset_categoryService() {
        return this.asset_categoryService;
    }

    @ApiOperation(value = "获取数据", tags = {"Asset_category" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_asset/asset_categories/{asset_category_id}")
    public ResponseEntity<Asset_categoryDTO> get(@PathVariable("asset_category_id") Integer asset_category_id) {
        Asset_categoryDTO dto = new Asset_categoryDTO();
        Asset_category domain = asset_categoryService.get(asset_category_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批删除数据", tags = {"Asset_category" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_asset/asset_categories/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Asset_categoryDTO> asset_categorydtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "删除数据", tags = {"Asset_category" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_asset/asset_categories/{asset_category_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("asset_category_id") Integer asset_category_id) {
        Asset_categoryDTO asset_categorydto = new Asset_categoryDTO();
		Asset_category domain = new Asset_category();
		asset_categorydto.setId(asset_category_id);
		domain.setId(asset_category_id);
        Boolean rst = asset_categoryService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "批更新数据", tags = {"Asset_category" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_asset/asset_categories/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Asset_categoryDTO> asset_categorydtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "批建立数据", tags = {"Asset_category" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_asset/asset_categories/createBatch")
    public ResponseEntity<Boolean> createBatchAsset_category(@RequestBody List<Asset_categoryDTO> asset_categorydtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "建立数据", tags = {"Asset_category" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_asset/asset_categories")

    public ResponseEntity<Asset_categoryDTO> create(@RequestBody Asset_categoryDTO asset_categorydto) {
        Asset_categoryDTO dto = new Asset_categoryDTO();
        Asset_category domain = asset_categorydto.toDO();
		asset_categoryService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "更新数据", tags = {"Asset_category" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_asset/asset_categories/{asset_category_id}")

    public ResponseEntity<Asset_categoryDTO> update(@PathVariable("asset_category_id") Integer asset_category_id, @RequestBody Asset_categoryDTO asset_categorydto) {
		Asset_category domain = asset_categorydto.toDO();
        domain.setId(asset_category_id);
		asset_categoryService.update(domain);
		Asset_categoryDTO dto = new Asset_categoryDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Asset_category" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_asset/asset_categories/fetchdefault")
	public ResponseEntity<Page<Asset_categoryDTO>> fetchDefault(Asset_categorySearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Asset_categoryDTO> list = new ArrayList<Asset_categoryDTO>();
        
        Page<Asset_category> domains = asset_categoryService.searchDefault(context) ;
        for(Asset_category asset_category : domains.getContent()){
            Asset_categoryDTO dto = new Asset_categoryDTO();
            dto.fromDO(asset_category);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
