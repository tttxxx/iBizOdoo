package cn.ibizlab.odoo.service.odoo_fetchmail.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_fetchmail.dto.Fetchmail_serverDTO;
import cn.ibizlab.odoo.core.odoo_fetchmail.domain.Fetchmail_server;
import cn.ibizlab.odoo.core.odoo_fetchmail.service.IFetchmail_serverService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_fetchmail.filter.Fetchmail_serverSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Fetchmail_server" })
@RestController
@RequestMapping("")
public class Fetchmail_serverResource {

    @Autowired
    private IFetchmail_serverService fetchmail_serverService;

    public IFetchmail_serverService getFetchmail_serverService() {
        return this.fetchmail_serverService;
    }

    @ApiOperation(value = "批删除数据", tags = {"Fetchmail_server" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_fetchmail/fetchmail_servers/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Fetchmail_serverDTO> fetchmail_serverdtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "批建立数据", tags = {"Fetchmail_server" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_fetchmail/fetchmail_servers/createBatch")
    public ResponseEntity<Boolean> createBatchFetchmail_server(@RequestBody List<Fetchmail_serverDTO> fetchmail_serverdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "获取数据", tags = {"Fetchmail_server" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_fetchmail/fetchmail_servers/{fetchmail_server_id}")
    public ResponseEntity<Fetchmail_serverDTO> get(@PathVariable("fetchmail_server_id") Integer fetchmail_server_id) {
        Fetchmail_serverDTO dto = new Fetchmail_serverDTO();
        Fetchmail_server domain = fetchmail_serverService.get(fetchmail_server_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "更新数据", tags = {"Fetchmail_server" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_fetchmail/fetchmail_servers/{fetchmail_server_id}")

    public ResponseEntity<Fetchmail_serverDTO> update(@PathVariable("fetchmail_server_id") Integer fetchmail_server_id, @RequestBody Fetchmail_serverDTO fetchmail_serverdto) {
		Fetchmail_server domain = fetchmail_serverdto.toDO();
        domain.setId(fetchmail_server_id);
		fetchmail_serverService.update(domain);
		Fetchmail_serverDTO dto = new Fetchmail_serverDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "建立数据", tags = {"Fetchmail_server" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_fetchmail/fetchmail_servers")

    public ResponseEntity<Fetchmail_serverDTO> create(@RequestBody Fetchmail_serverDTO fetchmail_serverdto) {
        Fetchmail_serverDTO dto = new Fetchmail_serverDTO();
        Fetchmail_server domain = fetchmail_serverdto.toDO();
		fetchmail_serverService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "删除数据", tags = {"Fetchmail_server" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_fetchmail/fetchmail_servers/{fetchmail_server_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("fetchmail_server_id") Integer fetchmail_server_id) {
        Fetchmail_serverDTO fetchmail_serverdto = new Fetchmail_serverDTO();
		Fetchmail_server domain = new Fetchmail_server();
		fetchmail_serverdto.setId(fetchmail_server_id);
		domain.setId(fetchmail_server_id);
        Boolean rst = fetchmail_serverService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "批更新数据", tags = {"Fetchmail_server" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_fetchmail/fetchmail_servers/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Fetchmail_serverDTO> fetchmail_serverdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Fetchmail_server" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_fetchmail/fetchmail_servers/fetchdefault")
	public ResponseEntity<Page<Fetchmail_serverDTO>> fetchDefault(Fetchmail_serverSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Fetchmail_serverDTO> list = new ArrayList<Fetchmail_serverDTO>();
        
        Page<Fetchmail_server> domains = fetchmail_serverService.searchDefault(context) ;
        for(Fetchmail_server fetchmail_server : domains.getContent()){
            Fetchmail_serverDTO dto = new Fetchmail_serverDTO();
            dto.fromDO(fetchmail_server);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
