package cn.ibizlab.odoo.service.odoo_fetchmail.config;

import org.springframework.context.annotation.ComponentScan;

@ComponentScan("cn.ibizlab.odoo.service.odoo_fetchmail")
public class odoo_fetchmailRestConfiguration {

}
