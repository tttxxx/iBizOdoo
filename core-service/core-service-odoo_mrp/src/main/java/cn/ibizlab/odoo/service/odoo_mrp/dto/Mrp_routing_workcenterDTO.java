package cn.ibizlab.odoo.service.odoo_mrp.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.core.odoo_mrp.valuerule.anno.mrp_routing_workcenter.*;
import cn.ibizlab.odoo.core.odoo_mrp.domain.Mrp_routing_workcenter;
import org.springframework.cglib.beans.BeanCopier;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 服务DTO对象[Mrp_routing_workcenterDTO]
 */
public class Mrp_routing_workcenterDTO implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [TIME_MODE_BATCH]
     *
     */
    @Mrp_routing_workcenterTime_mode_batchDefault(info = "默认规则")
    private Integer time_mode_batch;

    @JsonIgnore
    private boolean time_mode_batchDirtyFlag;

    /**
     * 属性 [BATCH_SIZE]
     *
     */
    @Mrp_routing_workcenterBatch_sizeDefault(info = "默认规则")
    private Double batch_size;

    @JsonIgnore
    private boolean batch_sizeDirtyFlag;

    /**
     * 属性 [WORKORDER_IDS]
     *
     */
    @Mrp_routing_workcenterWorkorder_idsDefault(info = "默认规则")
    private String workorder_ids;

    @JsonIgnore
    private boolean workorder_idsDirtyFlag;

    /**
     * 属性 [WORKORDER_COUNT]
     *
     */
    @Mrp_routing_workcenterWorkorder_countDefault(info = "默认规则")
    private Integer workorder_count;

    @JsonIgnore
    private boolean workorder_countDirtyFlag;

    /**
     * 属性 [SEQUENCE]
     *
     */
    @Mrp_routing_workcenterSequenceDefault(info = "默认规则")
    private Integer sequence;

    @JsonIgnore
    private boolean sequenceDirtyFlag;

    /**
     * 属性 [WRITE_DATE]
     *
     */
    @Mrp_routing_workcenterWrite_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp write_date;

    @JsonIgnore
    private boolean write_dateDirtyFlag;

    /**
     * 属性 [BATCH]
     *
     */
    @Mrp_routing_workcenterBatchDefault(info = "默认规则")
    private String batch;

    @JsonIgnore
    private boolean batchDirtyFlag;

    /**
     * 属性 [TIME_CYCLE_MANUAL]
     *
     */
    @Mrp_routing_workcenterTime_cycle_manualDefault(info = "默认规则")
    private Double time_cycle_manual;

    @JsonIgnore
    private boolean time_cycle_manualDirtyFlag;

    /**
     * 属性 [ID]
     *
     */
    @Mrp_routing_workcenterIdDefault(info = "默认规则")
    private Integer id;

    @JsonIgnore
    private boolean idDirtyFlag;

    /**
     * 属性 [NOTE]
     *
     */
    @Mrp_routing_workcenterNoteDefault(info = "默认规则")
    private String note;

    @JsonIgnore
    private boolean noteDirtyFlag;

    /**
     * 属性 [TIME_MODE]
     *
     */
    @Mrp_routing_workcenterTime_modeDefault(info = "默认规则")
    private String time_mode;

    @JsonIgnore
    private boolean time_modeDirtyFlag;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @Mrp_routing_workcenterDisplay_nameDefault(info = "默认规则")
    private String display_name;

    @JsonIgnore
    private boolean display_nameDirtyFlag;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @Mrp_routing_workcenter__last_updateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp __last_update;

    @JsonIgnore
    private boolean __last_updateDirtyFlag;

    /**
     * 属性 [NAME]
     *
     */
    @Mrp_routing_workcenterNameDefault(info = "默认规则")
    private String name;

    @JsonIgnore
    private boolean nameDirtyFlag;

    /**
     * 属性 [TIME_CYCLE]
     *
     */
    @Mrp_routing_workcenterTime_cycleDefault(info = "默认规则")
    private Double time_cycle;

    @JsonIgnore
    private boolean time_cycleDirtyFlag;

    /**
     * 属性 [WORKSHEET]
     *
     */
    @Mrp_routing_workcenterWorksheetDefault(info = "默认规则")
    private byte[] worksheet;

    @JsonIgnore
    private boolean worksheetDirtyFlag;

    /**
     * 属性 [CREATE_DATE]
     *
     */
    @Mrp_routing_workcenterCreate_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp create_date;

    @JsonIgnore
    private boolean create_dateDirtyFlag;

    /**
     * 属性 [COMPANY_ID_TEXT]
     *
     */
    @Mrp_routing_workcenterCompany_id_textDefault(info = "默认规则")
    private String company_id_text;

    @JsonIgnore
    private boolean company_id_textDirtyFlag;

    /**
     * 属性 [WORKCENTER_ID_TEXT]
     *
     */
    @Mrp_routing_workcenterWorkcenter_id_textDefault(info = "默认规则")
    private String workcenter_id_text;

    @JsonIgnore
    private boolean workcenter_id_textDirtyFlag;

    /**
     * 属性 [ROUTING_ID_TEXT]
     *
     */
    @Mrp_routing_workcenterRouting_id_textDefault(info = "默认规则")
    private String routing_id_text;

    @JsonIgnore
    private boolean routing_id_textDirtyFlag;

    /**
     * 属性 [WRITE_UID_TEXT]
     *
     */
    @Mrp_routing_workcenterWrite_uid_textDefault(info = "默认规则")
    private String write_uid_text;

    @JsonIgnore
    private boolean write_uid_textDirtyFlag;

    /**
     * 属性 [CREATE_UID_TEXT]
     *
     */
    @Mrp_routing_workcenterCreate_uid_textDefault(info = "默认规则")
    private String create_uid_text;

    @JsonIgnore
    private boolean create_uid_textDirtyFlag;

    /**
     * 属性 [ROUTING_ID]
     *
     */
    @Mrp_routing_workcenterRouting_idDefault(info = "默认规则")
    private Integer routing_id;

    @JsonIgnore
    private boolean routing_idDirtyFlag;

    /**
     * 属性 [WRITE_UID]
     *
     */
    @Mrp_routing_workcenterWrite_uidDefault(info = "默认规则")
    private Integer write_uid;

    @JsonIgnore
    private boolean write_uidDirtyFlag;

    /**
     * 属性 [COMPANY_ID]
     *
     */
    @Mrp_routing_workcenterCompany_idDefault(info = "默认规则")
    private Integer company_id;

    @JsonIgnore
    private boolean company_idDirtyFlag;

    /**
     * 属性 [WORKCENTER_ID]
     *
     */
    @Mrp_routing_workcenterWorkcenter_idDefault(info = "默认规则")
    private Integer workcenter_id;

    @JsonIgnore
    private boolean workcenter_idDirtyFlag;

    /**
     * 属性 [CREATE_UID]
     *
     */
    @Mrp_routing_workcenterCreate_uidDefault(info = "默认规则")
    private Integer create_uid;

    @JsonIgnore
    private boolean create_uidDirtyFlag;


    /**
     * 获取 [TIME_MODE_BATCH]
     */
    @JsonProperty("time_mode_batch")
    public Integer getTime_mode_batch(){
        return time_mode_batch ;
    }

    /**
     * 设置 [TIME_MODE_BATCH]
     */
    @JsonProperty("time_mode_batch")
    public void setTime_mode_batch(Integer  time_mode_batch){
        this.time_mode_batch = time_mode_batch ;
        this.time_mode_batchDirtyFlag = true ;
    }

    /**
     * 获取 [TIME_MODE_BATCH]脏标记
     */
    @JsonIgnore
    public boolean getTime_mode_batchDirtyFlag(){
        return time_mode_batchDirtyFlag ;
    }

    /**
     * 获取 [BATCH_SIZE]
     */
    @JsonProperty("batch_size")
    public Double getBatch_size(){
        return batch_size ;
    }

    /**
     * 设置 [BATCH_SIZE]
     */
    @JsonProperty("batch_size")
    public void setBatch_size(Double  batch_size){
        this.batch_size = batch_size ;
        this.batch_sizeDirtyFlag = true ;
    }

    /**
     * 获取 [BATCH_SIZE]脏标记
     */
    @JsonIgnore
    public boolean getBatch_sizeDirtyFlag(){
        return batch_sizeDirtyFlag ;
    }

    /**
     * 获取 [WORKORDER_IDS]
     */
    @JsonProperty("workorder_ids")
    public String getWorkorder_ids(){
        return workorder_ids ;
    }

    /**
     * 设置 [WORKORDER_IDS]
     */
    @JsonProperty("workorder_ids")
    public void setWorkorder_ids(String  workorder_ids){
        this.workorder_ids = workorder_ids ;
        this.workorder_idsDirtyFlag = true ;
    }

    /**
     * 获取 [WORKORDER_IDS]脏标记
     */
    @JsonIgnore
    public boolean getWorkorder_idsDirtyFlag(){
        return workorder_idsDirtyFlag ;
    }

    /**
     * 获取 [WORKORDER_COUNT]
     */
    @JsonProperty("workorder_count")
    public Integer getWorkorder_count(){
        return workorder_count ;
    }

    /**
     * 设置 [WORKORDER_COUNT]
     */
    @JsonProperty("workorder_count")
    public void setWorkorder_count(Integer  workorder_count){
        this.workorder_count = workorder_count ;
        this.workorder_countDirtyFlag = true ;
    }

    /**
     * 获取 [WORKORDER_COUNT]脏标记
     */
    @JsonIgnore
    public boolean getWorkorder_countDirtyFlag(){
        return workorder_countDirtyFlag ;
    }

    /**
     * 获取 [SEQUENCE]
     */
    @JsonProperty("sequence")
    public Integer getSequence(){
        return sequence ;
    }

    /**
     * 设置 [SEQUENCE]
     */
    @JsonProperty("sequence")
    public void setSequence(Integer  sequence){
        this.sequence = sequence ;
        this.sequenceDirtyFlag = true ;
    }

    /**
     * 获取 [SEQUENCE]脏标记
     */
    @JsonIgnore
    public boolean getSequenceDirtyFlag(){
        return sequenceDirtyFlag ;
    }

    /**
     * 获取 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return write_date ;
    }

    /**
     * 设置 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return write_dateDirtyFlag ;
    }

    /**
     * 获取 [BATCH]
     */
    @JsonProperty("batch")
    public String getBatch(){
        return batch ;
    }

    /**
     * 设置 [BATCH]
     */
    @JsonProperty("batch")
    public void setBatch(String  batch){
        this.batch = batch ;
        this.batchDirtyFlag = true ;
    }

    /**
     * 获取 [BATCH]脏标记
     */
    @JsonIgnore
    public boolean getBatchDirtyFlag(){
        return batchDirtyFlag ;
    }

    /**
     * 获取 [TIME_CYCLE_MANUAL]
     */
    @JsonProperty("time_cycle_manual")
    public Double getTime_cycle_manual(){
        return time_cycle_manual ;
    }

    /**
     * 设置 [TIME_CYCLE_MANUAL]
     */
    @JsonProperty("time_cycle_manual")
    public void setTime_cycle_manual(Double  time_cycle_manual){
        this.time_cycle_manual = time_cycle_manual ;
        this.time_cycle_manualDirtyFlag = true ;
    }

    /**
     * 获取 [TIME_CYCLE_MANUAL]脏标记
     */
    @JsonIgnore
    public boolean getTime_cycle_manualDirtyFlag(){
        return time_cycle_manualDirtyFlag ;
    }

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

    /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return idDirtyFlag ;
    }

    /**
     * 获取 [NOTE]
     */
    @JsonProperty("note")
    public String getNote(){
        return note ;
    }

    /**
     * 设置 [NOTE]
     */
    @JsonProperty("note")
    public void setNote(String  note){
        this.note = note ;
        this.noteDirtyFlag = true ;
    }

    /**
     * 获取 [NOTE]脏标记
     */
    @JsonIgnore
    public boolean getNoteDirtyFlag(){
        return noteDirtyFlag ;
    }

    /**
     * 获取 [TIME_MODE]
     */
    @JsonProperty("time_mode")
    public String getTime_mode(){
        return time_mode ;
    }

    /**
     * 设置 [TIME_MODE]
     */
    @JsonProperty("time_mode")
    public void setTime_mode(String  time_mode){
        this.time_mode = time_mode ;
        this.time_modeDirtyFlag = true ;
    }

    /**
     * 获取 [TIME_MODE]脏标记
     */
    @JsonIgnore
    public boolean getTime_modeDirtyFlag(){
        return time_modeDirtyFlag ;
    }

    /**
     * 获取 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return display_name ;
    }

    /**
     * 设置 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

    /**
     * 获取 [DISPLAY_NAME]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return display_nameDirtyFlag ;
    }

    /**
     * 获取 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return __last_update ;
    }

    /**
     * 设置 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

    /**
     * 获取 [__LAST_UPDATE]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return __last_updateDirtyFlag ;
    }

    /**
     * 获取 [NAME]
     */
    @JsonProperty("name")
    public String getName(){
        return name ;
    }

    /**
     * 设置 [NAME]
     */
    @JsonProperty("name")
    public void setName(String  name){
        this.name = name ;
        this.nameDirtyFlag = true ;
    }

    /**
     * 获取 [NAME]脏标记
     */
    @JsonIgnore
    public boolean getNameDirtyFlag(){
        return nameDirtyFlag ;
    }

    /**
     * 获取 [TIME_CYCLE]
     */
    @JsonProperty("time_cycle")
    public Double getTime_cycle(){
        return time_cycle ;
    }

    /**
     * 设置 [TIME_CYCLE]
     */
    @JsonProperty("time_cycle")
    public void setTime_cycle(Double  time_cycle){
        this.time_cycle = time_cycle ;
        this.time_cycleDirtyFlag = true ;
    }

    /**
     * 获取 [TIME_CYCLE]脏标记
     */
    @JsonIgnore
    public boolean getTime_cycleDirtyFlag(){
        return time_cycleDirtyFlag ;
    }

    /**
     * 获取 [WORKSHEET]
     */
    @JsonProperty("worksheet")
    public byte[] getWorksheet(){
        return worksheet ;
    }

    /**
     * 设置 [WORKSHEET]
     */
    @JsonProperty("worksheet")
    public void setWorksheet(byte[]  worksheet){
        this.worksheet = worksheet ;
        this.worksheetDirtyFlag = true ;
    }

    /**
     * 获取 [WORKSHEET]脏标记
     */
    @JsonIgnore
    public boolean getWorksheetDirtyFlag(){
        return worksheetDirtyFlag ;
    }

    /**
     * 获取 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return create_date ;
    }

    /**
     * 设置 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return create_dateDirtyFlag ;
    }

    /**
     * 获取 [COMPANY_ID_TEXT]
     */
    @JsonProperty("company_id_text")
    public String getCompany_id_text(){
        return company_id_text ;
    }

    /**
     * 设置 [COMPANY_ID_TEXT]
     */
    @JsonProperty("company_id_text")
    public void setCompany_id_text(String  company_id_text){
        this.company_id_text = company_id_text ;
        this.company_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [COMPANY_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getCompany_id_textDirtyFlag(){
        return company_id_textDirtyFlag ;
    }

    /**
     * 获取 [WORKCENTER_ID_TEXT]
     */
    @JsonProperty("workcenter_id_text")
    public String getWorkcenter_id_text(){
        return workcenter_id_text ;
    }

    /**
     * 设置 [WORKCENTER_ID_TEXT]
     */
    @JsonProperty("workcenter_id_text")
    public void setWorkcenter_id_text(String  workcenter_id_text){
        this.workcenter_id_text = workcenter_id_text ;
        this.workcenter_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [WORKCENTER_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getWorkcenter_id_textDirtyFlag(){
        return workcenter_id_textDirtyFlag ;
    }

    /**
     * 获取 [ROUTING_ID_TEXT]
     */
    @JsonProperty("routing_id_text")
    public String getRouting_id_text(){
        return routing_id_text ;
    }

    /**
     * 设置 [ROUTING_ID_TEXT]
     */
    @JsonProperty("routing_id_text")
    public void setRouting_id_text(String  routing_id_text){
        this.routing_id_text = routing_id_text ;
        this.routing_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [ROUTING_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getRouting_id_textDirtyFlag(){
        return routing_id_textDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return write_uid_text ;
    }

    /**
     * 设置 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return write_uid_textDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return create_uid_text ;
    }

    /**
     * 设置 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return create_uid_textDirtyFlag ;
    }

    /**
     * 获取 [ROUTING_ID]
     */
    @JsonProperty("routing_id")
    public Integer getRouting_id(){
        return routing_id ;
    }

    /**
     * 设置 [ROUTING_ID]
     */
    @JsonProperty("routing_id")
    public void setRouting_id(Integer  routing_id){
        this.routing_id = routing_id ;
        this.routing_idDirtyFlag = true ;
    }

    /**
     * 获取 [ROUTING_ID]脏标记
     */
    @JsonIgnore
    public boolean getRouting_idDirtyFlag(){
        return routing_idDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return write_uid ;
    }

    /**
     * 设置 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return write_uidDirtyFlag ;
    }

    /**
     * 获取 [COMPANY_ID]
     */
    @JsonProperty("company_id")
    public Integer getCompany_id(){
        return company_id ;
    }

    /**
     * 设置 [COMPANY_ID]
     */
    @JsonProperty("company_id")
    public void setCompany_id(Integer  company_id){
        this.company_id = company_id ;
        this.company_idDirtyFlag = true ;
    }

    /**
     * 获取 [COMPANY_ID]脏标记
     */
    @JsonIgnore
    public boolean getCompany_idDirtyFlag(){
        return company_idDirtyFlag ;
    }

    /**
     * 获取 [WORKCENTER_ID]
     */
    @JsonProperty("workcenter_id")
    public Integer getWorkcenter_id(){
        return workcenter_id ;
    }

    /**
     * 设置 [WORKCENTER_ID]
     */
    @JsonProperty("workcenter_id")
    public void setWorkcenter_id(Integer  workcenter_id){
        this.workcenter_id = workcenter_id ;
        this.workcenter_idDirtyFlag = true ;
    }

    /**
     * 获取 [WORKCENTER_ID]脏标记
     */
    @JsonIgnore
    public boolean getWorkcenter_idDirtyFlag(){
        return workcenter_idDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return create_uid ;
    }

    /**
     * 设置 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return create_uidDirtyFlag ;
    }



    public Mrp_routing_workcenter toDO() {
        Mrp_routing_workcenter srfdomain = new Mrp_routing_workcenter();
        if(getTime_mode_batchDirtyFlag())
            srfdomain.setTime_mode_batch(time_mode_batch);
        if(getBatch_sizeDirtyFlag())
            srfdomain.setBatch_size(batch_size);
        if(getWorkorder_idsDirtyFlag())
            srfdomain.setWorkorder_ids(workorder_ids);
        if(getWorkorder_countDirtyFlag())
            srfdomain.setWorkorder_count(workorder_count);
        if(getSequenceDirtyFlag())
            srfdomain.setSequence(sequence);
        if(getWrite_dateDirtyFlag())
            srfdomain.setWrite_date(write_date);
        if(getBatchDirtyFlag())
            srfdomain.setBatch(batch);
        if(getTime_cycle_manualDirtyFlag())
            srfdomain.setTime_cycle_manual(time_cycle_manual);
        if(getIdDirtyFlag())
            srfdomain.setId(id);
        if(getNoteDirtyFlag())
            srfdomain.setNote(note);
        if(getTime_modeDirtyFlag())
            srfdomain.setTime_mode(time_mode);
        if(getDisplay_nameDirtyFlag())
            srfdomain.setDisplay_name(display_name);
        if(get__last_updateDirtyFlag())
            srfdomain.set__last_update(__last_update);
        if(getNameDirtyFlag())
            srfdomain.setName(name);
        if(getTime_cycleDirtyFlag())
            srfdomain.setTime_cycle(time_cycle);
        if(getWorksheetDirtyFlag())
            srfdomain.setWorksheet(worksheet);
        if(getCreate_dateDirtyFlag())
            srfdomain.setCreate_date(create_date);
        if(getCompany_id_textDirtyFlag())
            srfdomain.setCompany_id_text(company_id_text);
        if(getWorkcenter_id_textDirtyFlag())
            srfdomain.setWorkcenter_id_text(workcenter_id_text);
        if(getRouting_id_textDirtyFlag())
            srfdomain.setRouting_id_text(routing_id_text);
        if(getWrite_uid_textDirtyFlag())
            srfdomain.setWrite_uid_text(write_uid_text);
        if(getCreate_uid_textDirtyFlag())
            srfdomain.setCreate_uid_text(create_uid_text);
        if(getRouting_idDirtyFlag())
            srfdomain.setRouting_id(routing_id);
        if(getWrite_uidDirtyFlag())
            srfdomain.setWrite_uid(write_uid);
        if(getCompany_idDirtyFlag())
            srfdomain.setCompany_id(company_id);
        if(getWorkcenter_idDirtyFlag())
            srfdomain.setWorkcenter_id(workcenter_id);
        if(getCreate_uidDirtyFlag())
            srfdomain.setCreate_uid(create_uid);

        return srfdomain;
    }

    public void fromDO(Mrp_routing_workcenter srfdomain) {
        if(srfdomain == null )
          return ;
        if(srfdomain.getTime_mode_batchDirtyFlag())
            this.setTime_mode_batch(srfdomain.getTime_mode_batch());
        if(srfdomain.getBatch_sizeDirtyFlag())
            this.setBatch_size(srfdomain.getBatch_size());
        if(srfdomain.getWorkorder_idsDirtyFlag())
            this.setWorkorder_ids(srfdomain.getWorkorder_ids());
        if(srfdomain.getWorkorder_countDirtyFlag())
            this.setWorkorder_count(srfdomain.getWorkorder_count());
        if(srfdomain.getSequenceDirtyFlag())
            this.setSequence(srfdomain.getSequence());
        if(srfdomain.getWrite_dateDirtyFlag())
            this.setWrite_date(srfdomain.getWrite_date());
        if(srfdomain.getBatchDirtyFlag())
            this.setBatch(srfdomain.getBatch());
        if(srfdomain.getTime_cycle_manualDirtyFlag())
            this.setTime_cycle_manual(srfdomain.getTime_cycle_manual());
        if(srfdomain.getIdDirtyFlag())
            this.setId(srfdomain.getId());
        if(srfdomain.getNoteDirtyFlag())
            this.setNote(srfdomain.getNote());
        if(srfdomain.getTime_modeDirtyFlag())
            this.setTime_mode(srfdomain.getTime_mode());
        if(srfdomain.getDisplay_nameDirtyFlag())
            this.setDisplay_name(srfdomain.getDisplay_name());
        if(srfdomain.get__last_updateDirtyFlag())
            this.set__last_update(srfdomain.get__last_update());
        if(srfdomain.getNameDirtyFlag())
            this.setName(srfdomain.getName());
        if(srfdomain.getTime_cycleDirtyFlag())
            this.setTime_cycle(srfdomain.getTime_cycle());
        if(srfdomain.getWorksheetDirtyFlag())
            this.setWorksheet(srfdomain.getWorksheet());
        if(srfdomain.getCreate_dateDirtyFlag())
            this.setCreate_date(srfdomain.getCreate_date());
        if(srfdomain.getCompany_id_textDirtyFlag())
            this.setCompany_id_text(srfdomain.getCompany_id_text());
        if(srfdomain.getWorkcenter_id_textDirtyFlag())
            this.setWorkcenter_id_text(srfdomain.getWorkcenter_id_text());
        if(srfdomain.getRouting_id_textDirtyFlag())
            this.setRouting_id_text(srfdomain.getRouting_id_text());
        if(srfdomain.getWrite_uid_textDirtyFlag())
            this.setWrite_uid_text(srfdomain.getWrite_uid_text());
        if(srfdomain.getCreate_uid_textDirtyFlag())
            this.setCreate_uid_text(srfdomain.getCreate_uid_text());
        if(srfdomain.getRouting_idDirtyFlag())
            this.setRouting_id(srfdomain.getRouting_id());
        if(srfdomain.getWrite_uidDirtyFlag())
            this.setWrite_uid(srfdomain.getWrite_uid());
        if(srfdomain.getCompany_idDirtyFlag())
            this.setCompany_id(srfdomain.getCompany_id());
        if(srfdomain.getWorkcenter_idDirtyFlag())
            this.setWorkcenter_id(srfdomain.getWorkcenter_id());
        if(srfdomain.getCreate_uidDirtyFlag())
            this.setCreate_uid(srfdomain.getCreate_uid());

    }

    public List<Mrp_routing_workcenterDTO> fromDOPage(List<Mrp_routing_workcenter> poPage)   {
        if(poPage == null)
            return null;
        List<Mrp_routing_workcenterDTO> dtos=new ArrayList<Mrp_routing_workcenterDTO>();
        for(Mrp_routing_workcenter domain : poPage) {
            Mrp_routing_workcenterDTO dto = new Mrp_routing_workcenterDTO();
            dto.fromDO(domain);
            dtos.add(dto);
        }
        return dtos;
    }
}

