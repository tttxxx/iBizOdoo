package cn.ibizlab.odoo.service.odoo_mrp.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_mrp.dto.Mrp_workcenter_productivity_loss_typeDTO;
import cn.ibizlab.odoo.core.odoo_mrp.domain.Mrp_workcenter_productivity_loss_type;
import cn.ibizlab.odoo.core.odoo_mrp.service.IMrp_workcenter_productivity_loss_typeService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_mrp.filter.Mrp_workcenter_productivity_loss_typeSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Mrp_workcenter_productivity_loss_type" })
@RestController
@RequestMapping("")
public class Mrp_workcenter_productivity_loss_typeResource {

    @Autowired
    private IMrp_workcenter_productivity_loss_typeService mrp_workcenter_productivity_loss_typeService;

    public IMrp_workcenter_productivity_loss_typeService getMrp_workcenter_productivity_loss_typeService() {
        return this.mrp_workcenter_productivity_loss_typeService;
    }

    @ApiOperation(value = "更新数据", tags = {"Mrp_workcenter_productivity_loss_type" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_mrp/mrp_workcenter_productivity_loss_types/{mrp_workcenter_productivity_loss_type_id}")

    public ResponseEntity<Mrp_workcenter_productivity_loss_typeDTO> update(@PathVariable("mrp_workcenter_productivity_loss_type_id") Integer mrp_workcenter_productivity_loss_type_id, @RequestBody Mrp_workcenter_productivity_loss_typeDTO mrp_workcenter_productivity_loss_typedto) {
		Mrp_workcenter_productivity_loss_type domain = mrp_workcenter_productivity_loss_typedto.toDO();
        domain.setId(mrp_workcenter_productivity_loss_type_id);
		mrp_workcenter_productivity_loss_typeService.update(domain);
		Mrp_workcenter_productivity_loss_typeDTO dto = new Mrp_workcenter_productivity_loss_typeDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批更新数据", tags = {"Mrp_workcenter_productivity_loss_type" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_mrp/mrp_workcenter_productivity_loss_types/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Mrp_workcenter_productivity_loss_typeDTO> mrp_workcenter_productivity_loss_typedtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "建立数据", tags = {"Mrp_workcenter_productivity_loss_type" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_mrp/mrp_workcenter_productivity_loss_types")

    public ResponseEntity<Mrp_workcenter_productivity_loss_typeDTO> create(@RequestBody Mrp_workcenter_productivity_loss_typeDTO mrp_workcenter_productivity_loss_typedto) {
        Mrp_workcenter_productivity_loss_typeDTO dto = new Mrp_workcenter_productivity_loss_typeDTO();
        Mrp_workcenter_productivity_loss_type domain = mrp_workcenter_productivity_loss_typedto.toDO();
		mrp_workcenter_productivity_loss_typeService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批建立数据", tags = {"Mrp_workcenter_productivity_loss_type" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_mrp/mrp_workcenter_productivity_loss_types/createBatch")
    public ResponseEntity<Boolean> createBatchMrp_workcenter_productivity_loss_type(@RequestBody List<Mrp_workcenter_productivity_loss_typeDTO> mrp_workcenter_productivity_loss_typedtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "获取数据", tags = {"Mrp_workcenter_productivity_loss_type" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_mrp/mrp_workcenter_productivity_loss_types/{mrp_workcenter_productivity_loss_type_id}")
    public ResponseEntity<Mrp_workcenter_productivity_loss_typeDTO> get(@PathVariable("mrp_workcenter_productivity_loss_type_id") Integer mrp_workcenter_productivity_loss_type_id) {
        Mrp_workcenter_productivity_loss_typeDTO dto = new Mrp_workcenter_productivity_loss_typeDTO();
        Mrp_workcenter_productivity_loss_type domain = mrp_workcenter_productivity_loss_typeService.get(mrp_workcenter_productivity_loss_type_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "删除数据", tags = {"Mrp_workcenter_productivity_loss_type" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_mrp/mrp_workcenter_productivity_loss_types/{mrp_workcenter_productivity_loss_type_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("mrp_workcenter_productivity_loss_type_id") Integer mrp_workcenter_productivity_loss_type_id) {
        Mrp_workcenter_productivity_loss_typeDTO mrp_workcenter_productivity_loss_typedto = new Mrp_workcenter_productivity_loss_typeDTO();
		Mrp_workcenter_productivity_loss_type domain = new Mrp_workcenter_productivity_loss_type();
		mrp_workcenter_productivity_loss_typedto.setId(mrp_workcenter_productivity_loss_type_id);
		domain.setId(mrp_workcenter_productivity_loss_type_id);
        Boolean rst = mrp_workcenter_productivity_loss_typeService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "批删除数据", tags = {"Mrp_workcenter_productivity_loss_type" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_mrp/mrp_workcenter_productivity_loss_types/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Mrp_workcenter_productivity_loss_typeDTO> mrp_workcenter_productivity_loss_typedtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Mrp_workcenter_productivity_loss_type" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_mrp/mrp_workcenter_productivity_loss_types/fetchdefault")
	public ResponseEntity<Page<Mrp_workcenter_productivity_loss_typeDTO>> fetchDefault(Mrp_workcenter_productivity_loss_typeSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Mrp_workcenter_productivity_loss_typeDTO> list = new ArrayList<Mrp_workcenter_productivity_loss_typeDTO>();
        
        Page<Mrp_workcenter_productivity_loss_type> domains = mrp_workcenter_productivity_loss_typeService.searchDefault(context) ;
        for(Mrp_workcenter_productivity_loss_type mrp_workcenter_productivity_loss_type : domains.getContent()){
            Mrp_workcenter_productivity_loss_typeDTO dto = new Mrp_workcenter_productivity_loss_typeDTO();
            dto.fromDO(mrp_workcenter_productivity_loss_type);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
