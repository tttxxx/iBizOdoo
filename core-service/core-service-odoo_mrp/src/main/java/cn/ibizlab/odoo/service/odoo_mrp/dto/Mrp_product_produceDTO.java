package cn.ibizlab.odoo.service.odoo_mrp.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.core.odoo_mrp.valuerule.anno.mrp_product_produce.*;
import cn.ibizlab.odoo.core.odoo_mrp.domain.Mrp_product_produce;
import org.springframework.cglib.beans.BeanCopier;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 服务DTO对象[Mrp_product_produceDTO]
 */
public class Mrp_product_produceDTO implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @Mrp_product_produceDisplay_nameDefault(info = "默认规则")
    private String display_name;

    @JsonIgnore
    private boolean display_nameDirtyFlag;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @Mrp_product_produce__last_updateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp __last_update;

    @JsonIgnore
    private boolean __last_updateDirtyFlag;

    /**
     * 属性 [PRODUCT_QTY]
     *
     */
    @Mrp_product_produceProduct_qtyDefault(info = "默认规则")
    private Double product_qty;

    @JsonIgnore
    private boolean product_qtyDirtyFlag;

    /**
     * 属性 [CREATE_DATE]
     *
     */
    @Mrp_product_produceCreate_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp create_date;

    @JsonIgnore
    private boolean create_dateDirtyFlag;

    /**
     * 属性 [SERIAL]
     *
     */
    @Mrp_product_produceSerialDefault(info = "默认规则")
    private String serial;

    @JsonIgnore
    private boolean serialDirtyFlag;

    /**
     * 属性 [PRODUCE_LINE_IDS]
     *
     */
    @Mrp_product_produceProduce_line_idsDefault(info = "默认规则")
    private String produce_line_ids;

    @JsonIgnore
    private boolean produce_line_idsDirtyFlag;

    /**
     * 属性 [WRITE_DATE]
     *
     */
    @Mrp_product_produceWrite_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp write_date;

    @JsonIgnore
    private boolean write_dateDirtyFlag;

    /**
     * 属性 [ID]
     *
     */
    @Mrp_product_produceIdDefault(info = "默认规则")
    private Integer id;

    @JsonIgnore
    private boolean idDirtyFlag;

    /**
     * 属性 [PRODUCT_UOM_ID_TEXT]
     *
     */
    @Mrp_product_produceProduct_uom_id_textDefault(info = "默认规则")
    private String product_uom_id_text;

    @JsonIgnore
    private boolean product_uom_id_textDirtyFlag;

    /**
     * 属性 [CREATE_UID_TEXT]
     *
     */
    @Mrp_product_produceCreate_uid_textDefault(info = "默认规则")
    private String create_uid_text;

    @JsonIgnore
    private boolean create_uid_textDirtyFlag;

    /**
     * 属性 [PRODUCTION_ID_TEXT]
     *
     */
    @Mrp_product_produceProduction_id_textDefault(info = "默认规则")
    private String production_id_text;

    @JsonIgnore
    private boolean production_id_textDirtyFlag;

    /**
     * 属性 [PRODUCT_TRACKING]
     *
     */
    @Mrp_product_produceProduct_trackingDefault(info = "默认规则")
    private String product_tracking;

    @JsonIgnore
    private boolean product_trackingDirtyFlag;

    /**
     * 属性 [LOT_ID_TEXT]
     *
     */
    @Mrp_product_produceLot_id_textDefault(info = "默认规则")
    private String lot_id_text;

    @JsonIgnore
    private boolean lot_id_textDirtyFlag;

    /**
     * 属性 [PRODUCT_ID_TEXT]
     *
     */
    @Mrp_product_produceProduct_id_textDefault(info = "默认规则")
    private String product_id_text;

    @JsonIgnore
    private boolean product_id_textDirtyFlag;

    /**
     * 属性 [WRITE_UID_TEXT]
     *
     */
    @Mrp_product_produceWrite_uid_textDefault(info = "默认规则")
    private String write_uid_text;

    @JsonIgnore
    private boolean write_uid_textDirtyFlag;

    /**
     * 属性 [LOT_ID]
     *
     */
    @Mrp_product_produceLot_idDefault(info = "默认规则")
    private Integer lot_id;

    @JsonIgnore
    private boolean lot_idDirtyFlag;

    /**
     * 属性 [CREATE_UID]
     *
     */
    @Mrp_product_produceCreate_uidDefault(info = "默认规则")
    private Integer create_uid;

    @JsonIgnore
    private boolean create_uidDirtyFlag;

    /**
     * 属性 [PRODUCTION_ID]
     *
     */
    @Mrp_product_produceProduction_idDefault(info = "默认规则")
    private Integer production_id;

    @JsonIgnore
    private boolean production_idDirtyFlag;

    /**
     * 属性 [PRODUCT_UOM_ID]
     *
     */
    @Mrp_product_produceProduct_uom_idDefault(info = "默认规则")
    private Integer product_uom_id;

    @JsonIgnore
    private boolean product_uom_idDirtyFlag;

    /**
     * 属性 [WRITE_UID]
     *
     */
    @Mrp_product_produceWrite_uidDefault(info = "默认规则")
    private Integer write_uid;

    @JsonIgnore
    private boolean write_uidDirtyFlag;

    /**
     * 属性 [PRODUCT_ID]
     *
     */
    @Mrp_product_produceProduct_idDefault(info = "默认规则")
    private Integer product_id;

    @JsonIgnore
    private boolean product_idDirtyFlag;


    /**
     * 获取 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return display_name ;
    }

    /**
     * 设置 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

    /**
     * 获取 [DISPLAY_NAME]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return display_nameDirtyFlag ;
    }

    /**
     * 获取 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return __last_update ;
    }

    /**
     * 设置 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

    /**
     * 获取 [__LAST_UPDATE]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return __last_updateDirtyFlag ;
    }

    /**
     * 获取 [PRODUCT_QTY]
     */
    @JsonProperty("product_qty")
    public Double getProduct_qty(){
        return product_qty ;
    }

    /**
     * 设置 [PRODUCT_QTY]
     */
    @JsonProperty("product_qty")
    public void setProduct_qty(Double  product_qty){
        this.product_qty = product_qty ;
        this.product_qtyDirtyFlag = true ;
    }

    /**
     * 获取 [PRODUCT_QTY]脏标记
     */
    @JsonIgnore
    public boolean getProduct_qtyDirtyFlag(){
        return product_qtyDirtyFlag ;
    }

    /**
     * 获取 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return create_date ;
    }

    /**
     * 设置 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return create_dateDirtyFlag ;
    }

    /**
     * 获取 [SERIAL]
     */
    @JsonProperty("serial")
    public String getSerial(){
        return serial ;
    }

    /**
     * 设置 [SERIAL]
     */
    @JsonProperty("serial")
    public void setSerial(String  serial){
        this.serial = serial ;
        this.serialDirtyFlag = true ;
    }

    /**
     * 获取 [SERIAL]脏标记
     */
    @JsonIgnore
    public boolean getSerialDirtyFlag(){
        return serialDirtyFlag ;
    }

    /**
     * 获取 [PRODUCE_LINE_IDS]
     */
    @JsonProperty("produce_line_ids")
    public String getProduce_line_ids(){
        return produce_line_ids ;
    }

    /**
     * 设置 [PRODUCE_LINE_IDS]
     */
    @JsonProperty("produce_line_ids")
    public void setProduce_line_ids(String  produce_line_ids){
        this.produce_line_ids = produce_line_ids ;
        this.produce_line_idsDirtyFlag = true ;
    }

    /**
     * 获取 [PRODUCE_LINE_IDS]脏标记
     */
    @JsonIgnore
    public boolean getProduce_line_idsDirtyFlag(){
        return produce_line_idsDirtyFlag ;
    }

    /**
     * 获取 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return write_date ;
    }

    /**
     * 设置 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return write_dateDirtyFlag ;
    }

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

    /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return idDirtyFlag ;
    }

    /**
     * 获取 [PRODUCT_UOM_ID_TEXT]
     */
    @JsonProperty("product_uom_id_text")
    public String getProduct_uom_id_text(){
        return product_uom_id_text ;
    }

    /**
     * 设置 [PRODUCT_UOM_ID_TEXT]
     */
    @JsonProperty("product_uom_id_text")
    public void setProduct_uom_id_text(String  product_uom_id_text){
        this.product_uom_id_text = product_uom_id_text ;
        this.product_uom_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [PRODUCT_UOM_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getProduct_uom_id_textDirtyFlag(){
        return product_uom_id_textDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return create_uid_text ;
    }

    /**
     * 设置 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return create_uid_textDirtyFlag ;
    }

    /**
     * 获取 [PRODUCTION_ID_TEXT]
     */
    @JsonProperty("production_id_text")
    public String getProduction_id_text(){
        return production_id_text ;
    }

    /**
     * 设置 [PRODUCTION_ID_TEXT]
     */
    @JsonProperty("production_id_text")
    public void setProduction_id_text(String  production_id_text){
        this.production_id_text = production_id_text ;
        this.production_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [PRODUCTION_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getProduction_id_textDirtyFlag(){
        return production_id_textDirtyFlag ;
    }

    /**
     * 获取 [PRODUCT_TRACKING]
     */
    @JsonProperty("product_tracking")
    public String getProduct_tracking(){
        return product_tracking ;
    }

    /**
     * 设置 [PRODUCT_TRACKING]
     */
    @JsonProperty("product_tracking")
    public void setProduct_tracking(String  product_tracking){
        this.product_tracking = product_tracking ;
        this.product_trackingDirtyFlag = true ;
    }

    /**
     * 获取 [PRODUCT_TRACKING]脏标记
     */
    @JsonIgnore
    public boolean getProduct_trackingDirtyFlag(){
        return product_trackingDirtyFlag ;
    }

    /**
     * 获取 [LOT_ID_TEXT]
     */
    @JsonProperty("lot_id_text")
    public String getLot_id_text(){
        return lot_id_text ;
    }

    /**
     * 设置 [LOT_ID_TEXT]
     */
    @JsonProperty("lot_id_text")
    public void setLot_id_text(String  lot_id_text){
        this.lot_id_text = lot_id_text ;
        this.lot_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [LOT_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getLot_id_textDirtyFlag(){
        return lot_id_textDirtyFlag ;
    }

    /**
     * 获取 [PRODUCT_ID_TEXT]
     */
    @JsonProperty("product_id_text")
    public String getProduct_id_text(){
        return product_id_text ;
    }

    /**
     * 设置 [PRODUCT_ID_TEXT]
     */
    @JsonProperty("product_id_text")
    public void setProduct_id_text(String  product_id_text){
        this.product_id_text = product_id_text ;
        this.product_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [PRODUCT_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getProduct_id_textDirtyFlag(){
        return product_id_textDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return write_uid_text ;
    }

    /**
     * 设置 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return write_uid_textDirtyFlag ;
    }

    /**
     * 获取 [LOT_ID]
     */
    @JsonProperty("lot_id")
    public Integer getLot_id(){
        return lot_id ;
    }

    /**
     * 设置 [LOT_ID]
     */
    @JsonProperty("lot_id")
    public void setLot_id(Integer  lot_id){
        this.lot_id = lot_id ;
        this.lot_idDirtyFlag = true ;
    }

    /**
     * 获取 [LOT_ID]脏标记
     */
    @JsonIgnore
    public boolean getLot_idDirtyFlag(){
        return lot_idDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return create_uid ;
    }

    /**
     * 设置 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return create_uidDirtyFlag ;
    }

    /**
     * 获取 [PRODUCTION_ID]
     */
    @JsonProperty("production_id")
    public Integer getProduction_id(){
        return production_id ;
    }

    /**
     * 设置 [PRODUCTION_ID]
     */
    @JsonProperty("production_id")
    public void setProduction_id(Integer  production_id){
        this.production_id = production_id ;
        this.production_idDirtyFlag = true ;
    }

    /**
     * 获取 [PRODUCTION_ID]脏标记
     */
    @JsonIgnore
    public boolean getProduction_idDirtyFlag(){
        return production_idDirtyFlag ;
    }

    /**
     * 获取 [PRODUCT_UOM_ID]
     */
    @JsonProperty("product_uom_id")
    public Integer getProduct_uom_id(){
        return product_uom_id ;
    }

    /**
     * 设置 [PRODUCT_UOM_ID]
     */
    @JsonProperty("product_uom_id")
    public void setProduct_uom_id(Integer  product_uom_id){
        this.product_uom_id = product_uom_id ;
        this.product_uom_idDirtyFlag = true ;
    }

    /**
     * 获取 [PRODUCT_UOM_ID]脏标记
     */
    @JsonIgnore
    public boolean getProduct_uom_idDirtyFlag(){
        return product_uom_idDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return write_uid ;
    }

    /**
     * 设置 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return write_uidDirtyFlag ;
    }

    /**
     * 获取 [PRODUCT_ID]
     */
    @JsonProperty("product_id")
    public Integer getProduct_id(){
        return product_id ;
    }

    /**
     * 设置 [PRODUCT_ID]
     */
    @JsonProperty("product_id")
    public void setProduct_id(Integer  product_id){
        this.product_id = product_id ;
        this.product_idDirtyFlag = true ;
    }

    /**
     * 获取 [PRODUCT_ID]脏标记
     */
    @JsonIgnore
    public boolean getProduct_idDirtyFlag(){
        return product_idDirtyFlag ;
    }



    public Mrp_product_produce toDO() {
        Mrp_product_produce srfdomain = new Mrp_product_produce();
        if(getDisplay_nameDirtyFlag())
            srfdomain.setDisplay_name(display_name);
        if(get__last_updateDirtyFlag())
            srfdomain.set__last_update(__last_update);
        if(getProduct_qtyDirtyFlag())
            srfdomain.setProduct_qty(product_qty);
        if(getCreate_dateDirtyFlag())
            srfdomain.setCreate_date(create_date);
        if(getSerialDirtyFlag())
            srfdomain.setSerial(serial);
        if(getProduce_line_idsDirtyFlag())
            srfdomain.setProduce_line_ids(produce_line_ids);
        if(getWrite_dateDirtyFlag())
            srfdomain.setWrite_date(write_date);
        if(getIdDirtyFlag())
            srfdomain.setId(id);
        if(getProduct_uom_id_textDirtyFlag())
            srfdomain.setProduct_uom_id_text(product_uom_id_text);
        if(getCreate_uid_textDirtyFlag())
            srfdomain.setCreate_uid_text(create_uid_text);
        if(getProduction_id_textDirtyFlag())
            srfdomain.setProduction_id_text(production_id_text);
        if(getProduct_trackingDirtyFlag())
            srfdomain.setProduct_tracking(product_tracking);
        if(getLot_id_textDirtyFlag())
            srfdomain.setLot_id_text(lot_id_text);
        if(getProduct_id_textDirtyFlag())
            srfdomain.setProduct_id_text(product_id_text);
        if(getWrite_uid_textDirtyFlag())
            srfdomain.setWrite_uid_text(write_uid_text);
        if(getLot_idDirtyFlag())
            srfdomain.setLot_id(lot_id);
        if(getCreate_uidDirtyFlag())
            srfdomain.setCreate_uid(create_uid);
        if(getProduction_idDirtyFlag())
            srfdomain.setProduction_id(production_id);
        if(getProduct_uom_idDirtyFlag())
            srfdomain.setProduct_uom_id(product_uom_id);
        if(getWrite_uidDirtyFlag())
            srfdomain.setWrite_uid(write_uid);
        if(getProduct_idDirtyFlag())
            srfdomain.setProduct_id(product_id);

        return srfdomain;
    }

    public void fromDO(Mrp_product_produce srfdomain) {
        if(srfdomain == null )
          return ;
        if(srfdomain.getDisplay_nameDirtyFlag())
            this.setDisplay_name(srfdomain.getDisplay_name());
        if(srfdomain.get__last_updateDirtyFlag())
            this.set__last_update(srfdomain.get__last_update());
        if(srfdomain.getProduct_qtyDirtyFlag())
            this.setProduct_qty(srfdomain.getProduct_qty());
        if(srfdomain.getCreate_dateDirtyFlag())
            this.setCreate_date(srfdomain.getCreate_date());
        if(srfdomain.getSerialDirtyFlag())
            this.setSerial(srfdomain.getSerial());
        if(srfdomain.getProduce_line_idsDirtyFlag())
            this.setProduce_line_ids(srfdomain.getProduce_line_ids());
        if(srfdomain.getWrite_dateDirtyFlag())
            this.setWrite_date(srfdomain.getWrite_date());
        if(srfdomain.getIdDirtyFlag())
            this.setId(srfdomain.getId());
        if(srfdomain.getProduct_uom_id_textDirtyFlag())
            this.setProduct_uom_id_text(srfdomain.getProduct_uom_id_text());
        if(srfdomain.getCreate_uid_textDirtyFlag())
            this.setCreate_uid_text(srfdomain.getCreate_uid_text());
        if(srfdomain.getProduction_id_textDirtyFlag())
            this.setProduction_id_text(srfdomain.getProduction_id_text());
        if(srfdomain.getProduct_trackingDirtyFlag())
            this.setProduct_tracking(srfdomain.getProduct_tracking());
        if(srfdomain.getLot_id_textDirtyFlag())
            this.setLot_id_text(srfdomain.getLot_id_text());
        if(srfdomain.getProduct_id_textDirtyFlag())
            this.setProduct_id_text(srfdomain.getProduct_id_text());
        if(srfdomain.getWrite_uid_textDirtyFlag())
            this.setWrite_uid_text(srfdomain.getWrite_uid_text());
        if(srfdomain.getLot_idDirtyFlag())
            this.setLot_id(srfdomain.getLot_id());
        if(srfdomain.getCreate_uidDirtyFlag())
            this.setCreate_uid(srfdomain.getCreate_uid());
        if(srfdomain.getProduction_idDirtyFlag())
            this.setProduction_id(srfdomain.getProduction_id());
        if(srfdomain.getProduct_uom_idDirtyFlag())
            this.setProduct_uom_id(srfdomain.getProduct_uom_id());
        if(srfdomain.getWrite_uidDirtyFlag())
            this.setWrite_uid(srfdomain.getWrite_uid());
        if(srfdomain.getProduct_idDirtyFlag())
            this.setProduct_id(srfdomain.getProduct_id());

    }

    public List<Mrp_product_produceDTO> fromDOPage(List<Mrp_product_produce> poPage)   {
        if(poPage == null)
            return null;
        List<Mrp_product_produceDTO> dtos=new ArrayList<Mrp_product_produceDTO>();
        for(Mrp_product_produce domain : poPage) {
            Mrp_product_produceDTO dto = new Mrp_product_produceDTO();
            dto.fromDO(domain);
            dtos.add(dto);
        }
        return dtos;
    }
}

