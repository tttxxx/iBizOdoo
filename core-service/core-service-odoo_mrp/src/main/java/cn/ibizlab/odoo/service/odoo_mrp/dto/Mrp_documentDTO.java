package cn.ibizlab.odoo.service.odoo_mrp.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.core.odoo_mrp.valuerule.anno.mrp_document.*;
import cn.ibizlab.odoo.core.odoo_mrp.domain.Mrp_document;
import org.springframework.cglib.beans.BeanCopier;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 服务DTO对象[Mrp_documentDTO]
 */
public class Mrp_documentDTO implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [CREATE_DATE]
     *
     */
    @Mrp_documentCreate_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp create_date;

    @JsonIgnore
    private boolean create_dateDirtyFlag;

    /**
     * 属性 [ACTIVE]
     *
     */
    @Mrp_documentActiveDefault(info = "默认规则")
    private String active;

    @JsonIgnore
    private boolean activeDirtyFlag;

    /**
     * 属性 [RES_FIELD]
     *
     */
    @Mrp_documentRes_fieldDefault(info = "默认规则")
    private String res_field;

    @JsonIgnore
    private boolean res_fieldDirtyFlag;

    /**
     * 属性 [RES_NAME]
     *
     */
    @Mrp_documentRes_nameDefault(info = "默认规则")
    private String res_name;

    @JsonIgnore
    private boolean res_nameDirtyFlag;

    /**
     * 属性 [RES_MODEL_NAME]
     *
     */
    @Mrp_documentRes_model_nameDefault(info = "默认规则")
    private String res_model_name;

    @JsonIgnore
    private boolean res_model_nameDirtyFlag;

    /**
     * 属性 [DATAS_FNAME]
     *
     */
    @Mrp_documentDatas_fnameDefault(info = "默认规则")
    private String datas_fname;

    @JsonIgnore
    private boolean datas_fnameDirtyFlag;

    /**
     * 属性 [THEME_TEMPLATE_ID]
     *
     */
    @Mrp_documentTheme_template_idDefault(info = "默认规则")
    private Integer theme_template_id;

    @JsonIgnore
    private boolean theme_template_idDirtyFlag;

    /**
     * 属性 [MIMETYPE]
     *
     */
    @Mrp_documentMimetypeDefault(info = "默认规则")
    private String mimetype;

    @JsonIgnore
    private boolean mimetypeDirtyFlag;

    /**
     * 属性 [RES_ID]
     *
     */
    @Mrp_documentRes_idDefault(info = "默认规则")
    private Integer res_id;

    @JsonIgnore
    private boolean res_idDirtyFlag;

    /**
     * 属性 [STORE_FNAME]
     *
     */
    @Mrp_documentStore_fnameDefault(info = "默认规则")
    private String store_fname;

    @JsonIgnore
    private boolean store_fnameDirtyFlag;

    /**
     * 属性 [WRITE_DATE]
     *
     */
    @Mrp_documentWrite_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp write_date;

    @JsonIgnore
    private boolean write_dateDirtyFlag;

    /**
     * 属性 [LOCAL_URL]
     *
     */
    @Mrp_documentLocal_urlDefault(info = "默认规则")
    private String local_url;

    @JsonIgnore
    private boolean local_urlDirtyFlag;

    /**
     * 属性 [KEY]
     *
     */
    @Mrp_documentKeyDefault(info = "默认规则")
    private String key;

    @JsonIgnore
    private boolean keyDirtyFlag;

    /**
     * 属性 [IBIZPUBLIC]
     *
     */
    @Mrp_documentIbizpublicDefault(info = "默认规则")
    private String ibizpublic;

    @JsonIgnore
    private boolean ibizpublicDirtyFlag;

    /**
     * 属性 [RES_MODEL]
     *
     */
    @Mrp_documentRes_modelDefault(info = "默认规则")
    private String res_model;

    @JsonIgnore
    private boolean res_modelDirtyFlag;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @Mrp_document__last_updateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp __last_update;

    @JsonIgnore
    private boolean __last_updateDirtyFlag;

    /**
     * 属性 [THUMBNAIL]
     *
     */
    @Mrp_documentThumbnailDefault(info = "默认规则")
    private byte[] thumbnail;

    @JsonIgnore
    private boolean thumbnailDirtyFlag;

    /**
     * 属性 [URL]
     *
     */
    @Mrp_documentUrlDefault(info = "默认规则")
    private String url;

    @JsonIgnore
    private boolean urlDirtyFlag;

    /**
     * 属性 [FILE_SIZE]
     *
     */
    @Mrp_documentFile_sizeDefault(info = "默认规则")
    private Integer file_size;

    @JsonIgnore
    private boolean file_sizeDirtyFlag;

    /**
     * 属性 [ACCESS_TOKEN]
     *
     */
    @Mrp_documentAccess_tokenDefault(info = "默认规则")
    private String access_token;

    @JsonIgnore
    private boolean access_tokenDirtyFlag;

    /**
     * 属性 [COMPANY_ID]
     *
     */
    @Mrp_documentCompany_idDefault(info = "默认规则")
    private Integer company_id;

    @JsonIgnore
    private boolean company_idDirtyFlag;

    /**
     * 属性 [IR_ATTACHMENT_ID]
     *
     */
    @Mrp_documentIr_attachment_idDefault(info = "默认规则")
    private Integer ir_attachment_id;

    @JsonIgnore
    private boolean ir_attachment_idDirtyFlag;

    /**
     * 属性 [ID]
     *
     */
    @Mrp_documentIdDefault(info = "默认规则")
    private Integer id;

    @JsonIgnore
    private boolean idDirtyFlag;

    /**
     * 属性 [TYPE]
     *
     */
    @Mrp_documentTypeDefault(info = "默认规则")
    private String type;

    @JsonIgnore
    private boolean typeDirtyFlag;

    /**
     * 属性 [CHECKSUM]
     *
     */
    @Mrp_documentChecksumDefault(info = "默认规则")
    private String checksum;

    @JsonIgnore
    private boolean checksumDirtyFlag;

    /**
     * 属性 [DB_DATAS]
     *
     */
    @Mrp_documentDb_datasDefault(info = "默认规则")
    private byte[] db_datas;

    @JsonIgnore
    private boolean db_datasDirtyFlag;

    /**
     * 属性 [INDEX_CONTENT]
     *
     */
    @Mrp_documentIndex_contentDefault(info = "默认规则")
    private String index_content;

    @JsonIgnore
    private boolean index_contentDirtyFlag;

    /**
     * 属性 [DESCRIPTION]
     *
     */
    @Mrp_documentDescriptionDefault(info = "默认规则")
    private String description;

    @JsonIgnore
    private boolean descriptionDirtyFlag;

    /**
     * 属性 [NAME]
     *
     */
    @Mrp_documentNameDefault(info = "默认规则")
    private String name;

    @JsonIgnore
    private boolean nameDirtyFlag;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @Mrp_documentDisplay_nameDefault(info = "默认规则")
    private String display_name;

    @JsonIgnore
    private boolean display_nameDirtyFlag;

    /**
     * 属性 [WEBSITE_URL]
     *
     */
    @Mrp_documentWebsite_urlDefault(info = "默认规则")
    private String website_url;

    @JsonIgnore
    private boolean website_urlDirtyFlag;

    /**
     * 属性 [WEBSITE_ID]
     *
     */
    @Mrp_documentWebsite_idDefault(info = "默认规则")
    private Integer website_id;

    @JsonIgnore
    private boolean website_idDirtyFlag;

    /**
     * 属性 [DATAS]
     *
     */
    @Mrp_documentDatasDefault(info = "默认规则")
    private byte[] datas;

    @JsonIgnore
    private boolean datasDirtyFlag;

    /**
     * 属性 [PRIORITY]
     *
     */
    @Mrp_documentPriorityDefault(info = "默认规则")
    private String priority;

    @JsonIgnore
    private boolean priorityDirtyFlag;

    /**
     * 属性 [WRITE_UID_TEXT]
     *
     */
    @Mrp_documentWrite_uid_textDefault(info = "默认规则")
    private String write_uid_text;

    @JsonIgnore
    private boolean write_uid_textDirtyFlag;

    /**
     * 属性 [CREATE_UID_TEXT]
     *
     */
    @Mrp_documentCreate_uid_textDefault(info = "默认规则")
    private String create_uid_text;

    @JsonIgnore
    private boolean create_uid_textDirtyFlag;

    /**
     * 属性 [CREATE_UID]
     *
     */
    @Mrp_documentCreate_uidDefault(info = "默认规则")
    private Integer create_uid;

    @JsonIgnore
    private boolean create_uidDirtyFlag;

    /**
     * 属性 [WRITE_UID]
     *
     */
    @Mrp_documentWrite_uidDefault(info = "默认规则")
    private Integer write_uid;

    @JsonIgnore
    private boolean write_uidDirtyFlag;


    /**
     * 获取 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return create_date ;
    }

    /**
     * 设置 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return create_dateDirtyFlag ;
    }

    /**
     * 获取 [ACTIVE]
     */
    @JsonProperty("active")
    public String getActive(){
        return active ;
    }

    /**
     * 设置 [ACTIVE]
     */
    @JsonProperty("active")
    public void setActive(String  active){
        this.active = active ;
        this.activeDirtyFlag = true ;
    }

    /**
     * 获取 [ACTIVE]脏标记
     */
    @JsonIgnore
    public boolean getActiveDirtyFlag(){
        return activeDirtyFlag ;
    }

    /**
     * 获取 [RES_FIELD]
     */
    @JsonProperty("res_field")
    public String getRes_field(){
        return res_field ;
    }

    /**
     * 设置 [RES_FIELD]
     */
    @JsonProperty("res_field")
    public void setRes_field(String  res_field){
        this.res_field = res_field ;
        this.res_fieldDirtyFlag = true ;
    }

    /**
     * 获取 [RES_FIELD]脏标记
     */
    @JsonIgnore
    public boolean getRes_fieldDirtyFlag(){
        return res_fieldDirtyFlag ;
    }

    /**
     * 获取 [RES_NAME]
     */
    @JsonProperty("res_name")
    public String getRes_name(){
        return res_name ;
    }

    /**
     * 设置 [RES_NAME]
     */
    @JsonProperty("res_name")
    public void setRes_name(String  res_name){
        this.res_name = res_name ;
        this.res_nameDirtyFlag = true ;
    }

    /**
     * 获取 [RES_NAME]脏标记
     */
    @JsonIgnore
    public boolean getRes_nameDirtyFlag(){
        return res_nameDirtyFlag ;
    }

    /**
     * 获取 [RES_MODEL_NAME]
     */
    @JsonProperty("res_model_name")
    public String getRes_model_name(){
        return res_model_name ;
    }

    /**
     * 设置 [RES_MODEL_NAME]
     */
    @JsonProperty("res_model_name")
    public void setRes_model_name(String  res_model_name){
        this.res_model_name = res_model_name ;
        this.res_model_nameDirtyFlag = true ;
    }

    /**
     * 获取 [RES_MODEL_NAME]脏标记
     */
    @JsonIgnore
    public boolean getRes_model_nameDirtyFlag(){
        return res_model_nameDirtyFlag ;
    }

    /**
     * 获取 [DATAS_FNAME]
     */
    @JsonProperty("datas_fname")
    public String getDatas_fname(){
        return datas_fname ;
    }

    /**
     * 设置 [DATAS_FNAME]
     */
    @JsonProperty("datas_fname")
    public void setDatas_fname(String  datas_fname){
        this.datas_fname = datas_fname ;
        this.datas_fnameDirtyFlag = true ;
    }

    /**
     * 获取 [DATAS_FNAME]脏标记
     */
    @JsonIgnore
    public boolean getDatas_fnameDirtyFlag(){
        return datas_fnameDirtyFlag ;
    }

    /**
     * 获取 [THEME_TEMPLATE_ID]
     */
    @JsonProperty("theme_template_id")
    public Integer getTheme_template_id(){
        return theme_template_id ;
    }

    /**
     * 设置 [THEME_TEMPLATE_ID]
     */
    @JsonProperty("theme_template_id")
    public void setTheme_template_id(Integer  theme_template_id){
        this.theme_template_id = theme_template_id ;
        this.theme_template_idDirtyFlag = true ;
    }

    /**
     * 获取 [THEME_TEMPLATE_ID]脏标记
     */
    @JsonIgnore
    public boolean getTheme_template_idDirtyFlag(){
        return theme_template_idDirtyFlag ;
    }

    /**
     * 获取 [MIMETYPE]
     */
    @JsonProperty("mimetype")
    public String getMimetype(){
        return mimetype ;
    }

    /**
     * 设置 [MIMETYPE]
     */
    @JsonProperty("mimetype")
    public void setMimetype(String  mimetype){
        this.mimetype = mimetype ;
        this.mimetypeDirtyFlag = true ;
    }

    /**
     * 获取 [MIMETYPE]脏标记
     */
    @JsonIgnore
    public boolean getMimetypeDirtyFlag(){
        return mimetypeDirtyFlag ;
    }

    /**
     * 获取 [RES_ID]
     */
    @JsonProperty("res_id")
    public Integer getRes_id(){
        return res_id ;
    }

    /**
     * 设置 [RES_ID]
     */
    @JsonProperty("res_id")
    public void setRes_id(Integer  res_id){
        this.res_id = res_id ;
        this.res_idDirtyFlag = true ;
    }

    /**
     * 获取 [RES_ID]脏标记
     */
    @JsonIgnore
    public boolean getRes_idDirtyFlag(){
        return res_idDirtyFlag ;
    }

    /**
     * 获取 [STORE_FNAME]
     */
    @JsonProperty("store_fname")
    public String getStore_fname(){
        return store_fname ;
    }

    /**
     * 设置 [STORE_FNAME]
     */
    @JsonProperty("store_fname")
    public void setStore_fname(String  store_fname){
        this.store_fname = store_fname ;
        this.store_fnameDirtyFlag = true ;
    }

    /**
     * 获取 [STORE_FNAME]脏标记
     */
    @JsonIgnore
    public boolean getStore_fnameDirtyFlag(){
        return store_fnameDirtyFlag ;
    }

    /**
     * 获取 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return write_date ;
    }

    /**
     * 设置 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return write_dateDirtyFlag ;
    }

    /**
     * 获取 [LOCAL_URL]
     */
    @JsonProperty("local_url")
    public String getLocal_url(){
        return local_url ;
    }

    /**
     * 设置 [LOCAL_URL]
     */
    @JsonProperty("local_url")
    public void setLocal_url(String  local_url){
        this.local_url = local_url ;
        this.local_urlDirtyFlag = true ;
    }

    /**
     * 获取 [LOCAL_URL]脏标记
     */
    @JsonIgnore
    public boolean getLocal_urlDirtyFlag(){
        return local_urlDirtyFlag ;
    }

    /**
     * 获取 [KEY]
     */
    @JsonProperty("key")
    public String getKey(){
        return key ;
    }

    /**
     * 设置 [KEY]
     */
    @JsonProperty("key")
    public void setKey(String  key){
        this.key = key ;
        this.keyDirtyFlag = true ;
    }

    /**
     * 获取 [KEY]脏标记
     */
    @JsonIgnore
    public boolean getKeyDirtyFlag(){
        return keyDirtyFlag ;
    }

    /**
     * 获取 [IBIZPUBLIC]
     */
    @JsonProperty("ibizpublic")
    public String getIbizpublic(){
        return ibizpublic ;
    }

    /**
     * 设置 [IBIZPUBLIC]
     */
    @JsonProperty("ibizpublic")
    public void setIbizpublic(String  ibizpublic){
        this.ibizpublic = ibizpublic ;
        this.ibizpublicDirtyFlag = true ;
    }

    /**
     * 获取 [IBIZPUBLIC]脏标记
     */
    @JsonIgnore
    public boolean getIbizpublicDirtyFlag(){
        return ibizpublicDirtyFlag ;
    }

    /**
     * 获取 [RES_MODEL]
     */
    @JsonProperty("res_model")
    public String getRes_model(){
        return res_model ;
    }

    /**
     * 设置 [RES_MODEL]
     */
    @JsonProperty("res_model")
    public void setRes_model(String  res_model){
        this.res_model = res_model ;
        this.res_modelDirtyFlag = true ;
    }

    /**
     * 获取 [RES_MODEL]脏标记
     */
    @JsonIgnore
    public boolean getRes_modelDirtyFlag(){
        return res_modelDirtyFlag ;
    }

    /**
     * 获取 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return __last_update ;
    }

    /**
     * 设置 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

    /**
     * 获取 [__LAST_UPDATE]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return __last_updateDirtyFlag ;
    }

    /**
     * 获取 [THUMBNAIL]
     */
    @JsonProperty("thumbnail")
    public byte[] getThumbnail(){
        return thumbnail ;
    }

    /**
     * 设置 [THUMBNAIL]
     */
    @JsonProperty("thumbnail")
    public void setThumbnail(byte[]  thumbnail){
        this.thumbnail = thumbnail ;
        this.thumbnailDirtyFlag = true ;
    }

    /**
     * 获取 [THUMBNAIL]脏标记
     */
    @JsonIgnore
    public boolean getThumbnailDirtyFlag(){
        return thumbnailDirtyFlag ;
    }

    /**
     * 获取 [URL]
     */
    @JsonProperty("url")
    public String getUrl(){
        return url ;
    }

    /**
     * 设置 [URL]
     */
    @JsonProperty("url")
    public void setUrl(String  url){
        this.url = url ;
        this.urlDirtyFlag = true ;
    }

    /**
     * 获取 [URL]脏标记
     */
    @JsonIgnore
    public boolean getUrlDirtyFlag(){
        return urlDirtyFlag ;
    }

    /**
     * 获取 [FILE_SIZE]
     */
    @JsonProperty("file_size")
    public Integer getFile_size(){
        return file_size ;
    }

    /**
     * 设置 [FILE_SIZE]
     */
    @JsonProperty("file_size")
    public void setFile_size(Integer  file_size){
        this.file_size = file_size ;
        this.file_sizeDirtyFlag = true ;
    }

    /**
     * 获取 [FILE_SIZE]脏标记
     */
    @JsonIgnore
    public boolean getFile_sizeDirtyFlag(){
        return file_sizeDirtyFlag ;
    }

    /**
     * 获取 [ACCESS_TOKEN]
     */
    @JsonProperty("access_token")
    public String getAccess_token(){
        return access_token ;
    }

    /**
     * 设置 [ACCESS_TOKEN]
     */
    @JsonProperty("access_token")
    public void setAccess_token(String  access_token){
        this.access_token = access_token ;
        this.access_tokenDirtyFlag = true ;
    }

    /**
     * 获取 [ACCESS_TOKEN]脏标记
     */
    @JsonIgnore
    public boolean getAccess_tokenDirtyFlag(){
        return access_tokenDirtyFlag ;
    }

    /**
     * 获取 [COMPANY_ID]
     */
    @JsonProperty("company_id")
    public Integer getCompany_id(){
        return company_id ;
    }

    /**
     * 设置 [COMPANY_ID]
     */
    @JsonProperty("company_id")
    public void setCompany_id(Integer  company_id){
        this.company_id = company_id ;
        this.company_idDirtyFlag = true ;
    }

    /**
     * 获取 [COMPANY_ID]脏标记
     */
    @JsonIgnore
    public boolean getCompany_idDirtyFlag(){
        return company_idDirtyFlag ;
    }

    /**
     * 获取 [IR_ATTACHMENT_ID]
     */
    @JsonProperty("ir_attachment_id")
    public Integer getIr_attachment_id(){
        return ir_attachment_id ;
    }

    /**
     * 设置 [IR_ATTACHMENT_ID]
     */
    @JsonProperty("ir_attachment_id")
    public void setIr_attachment_id(Integer  ir_attachment_id){
        this.ir_attachment_id = ir_attachment_id ;
        this.ir_attachment_idDirtyFlag = true ;
    }

    /**
     * 获取 [IR_ATTACHMENT_ID]脏标记
     */
    @JsonIgnore
    public boolean getIr_attachment_idDirtyFlag(){
        return ir_attachment_idDirtyFlag ;
    }

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

    /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return idDirtyFlag ;
    }

    /**
     * 获取 [TYPE]
     */
    @JsonProperty("type")
    public String getType(){
        return type ;
    }

    /**
     * 设置 [TYPE]
     */
    @JsonProperty("type")
    public void setType(String  type){
        this.type = type ;
        this.typeDirtyFlag = true ;
    }

    /**
     * 获取 [TYPE]脏标记
     */
    @JsonIgnore
    public boolean getTypeDirtyFlag(){
        return typeDirtyFlag ;
    }

    /**
     * 获取 [CHECKSUM]
     */
    @JsonProperty("checksum")
    public String getChecksum(){
        return checksum ;
    }

    /**
     * 设置 [CHECKSUM]
     */
    @JsonProperty("checksum")
    public void setChecksum(String  checksum){
        this.checksum = checksum ;
        this.checksumDirtyFlag = true ;
    }

    /**
     * 获取 [CHECKSUM]脏标记
     */
    @JsonIgnore
    public boolean getChecksumDirtyFlag(){
        return checksumDirtyFlag ;
    }

    /**
     * 获取 [DB_DATAS]
     */
    @JsonProperty("db_datas")
    public byte[] getDb_datas(){
        return db_datas ;
    }

    /**
     * 设置 [DB_DATAS]
     */
    @JsonProperty("db_datas")
    public void setDb_datas(byte[]  db_datas){
        this.db_datas = db_datas ;
        this.db_datasDirtyFlag = true ;
    }

    /**
     * 获取 [DB_DATAS]脏标记
     */
    @JsonIgnore
    public boolean getDb_datasDirtyFlag(){
        return db_datasDirtyFlag ;
    }

    /**
     * 获取 [INDEX_CONTENT]
     */
    @JsonProperty("index_content")
    public String getIndex_content(){
        return index_content ;
    }

    /**
     * 设置 [INDEX_CONTENT]
     */
    @JsonProperty("index_content")
    public void setIndex_content(String  index_content){
        this.index_content = index_content ;
        this.index_contentDirtyFlag = true ;
    }

    /**
     * 获取 [INDEX_CONTENT]脏标记
     */
    @JsonIgnore
    public boolean getIndex_contentDirtyFlag(){
        return index_contentDirtyFlag ;
    }

    /**
     * 获取 [DESCRIPTION]
     */
    @JsonProperty("description")
    public String getDescription(){
        return description ;
    }

    /**
     * 设置 [DESCRIPTION]
     */
    @JsonProperty("description")
    public void setDescription(String  description){
        this.description = description ;
        this.descriptionDirtyFlag = true ;
    }

    /**
     * 获取 [DESCRIPTION]脏标记
     */
    @JsonIgnore
    public boolean getDescriptionDirtyFlag(){
        return descriptionDirtyFlag ;
    }

    /**
     * 获取 [NAME]
     */
    @JsonProperty("name")
    public String getName(){
        return name ;
    }

    /**
     * 设置 [NAME]
     */
    @JsonProperty("name")
    public void setName(String  name){
        this.name = name ;
        this.nameDirtyFlag = true ;
    }

    /**
     * 获取 [NAME]脏标记
     */
    @JsonIgnore
    public boolean getNameDirtyFlag(){
        return nameDirtyFlag ;
    }

    /**
     * 获取 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return display_name ;
    }

    /**
     * 设置 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

    /**
     * 获取 [DISPLAY_NAME]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return display_nameDirtyFlag ;
    }

    /**
     * 获取 [WEBSITE_URL]
     */
    @JsonProperty("website_url")
    public String getWebsite_url(){
        return website_url ;
    }

    /**
     * 设置 [WEBSITE_URL]
     */
    @JsonProperty("website_url")
    public void setWebsite_url(String  website_url){
        this.website_url = website_url ;
        this.website_urlDirtyFlag = true ;
    }

    /**
     * 获取 [WEBSITE_URL]脏标记
     */
    @JsonIgnore
    public boolean getWebsite_urlDirtyFlag(){
        return website_urlDirtyFlag ;
    }

    /**
     * 获取 [WEBSITE_ID]
     */
    @JsonProperty("website_id")
    public Integer getWebsite_id(){
        return website_id ;
    }

    /**
     * 设置 [WEBSITE_ID]
     */
    @JsonProperty("website_id")
    public void setWebsite_id(Integer  website_id){
        this.website_id = website_id ;
        this.website_idDirtyFlag = true ;
    }

    /**
     * 获取 [WEBSITE_ID]脏标记
     */
    @JsonIgnore
    public boolean getWebsite_idDirtyFlag(){
        return website_idDirtyFlag ;
    }

    /**
     * 获取 [DATAS]
     */
    @JsonProperty("datas")
    public byte[] getDatas(){
        return datas ;
    }

    /**
     * 设置 [DATAS]
     */
    @JsonProperty("datas")
    public void setDatas(byte[]  datas){
        this.datas = datas ;
        this.datasDirtyFlag = true ;
    }

    /**
     * 获取 [DATAS]脏标记
     */
    @JsonIgnore
    public boolean getDatasDirtyFlag(){
        return datasDirtyFlag ;
    }

    /**
     * 获取 [PRIORITY]
     */
    @JsonProperty("priority")
    public String getPriority(){
        return priority ;
    }

    /**
     * 设置 [PRIORITY]
     */
    @JsonProperty("priority")
    public void setPriority(String  priority){
        this.priority = priority ;
        this.priorityDirtyFlag = true ;
    }

    /**
     * 获取 [PRIORITY]脏标记
     */
    @JsonIgnore
    public boolean getPriorityDirtyFlag(){
        return priorityDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return write_uid_text ;
    }

    /**
     * 设置 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return write_uid_textDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return create_uid_text ;
    }

    /**
     * 设置 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return create_uid_textDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return create_uid ;
    }

    /**
     * 设置 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return create_uidDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return write_uid ;
    }

    /**
     * 设置 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return write_uidDirtyFlag ;
    }



    public Mrp_document toDO() {
        Mrp_document srfdomain = new Mrp_document();
        if(getCreate_dateDirtyFlag())
            srfdomain.setCreate_date(create_date);
        if(getActiveDirtyFlag())
            srfdomain.setActive(active);
        if(getRes_fieldDirtyFlag())
            srfdomain.setRes_field(res_field);
        if(getRes_nameDirtyFlag())
            srfdomain.setRes_name(res_name);
        if(getRes_model_nameDirtyFlag())
            srfdomain.setRes_model_name(res_model_name);
        if(getDatas_fnameDirtyFlag())
            srfdomain.setDatas_fname(datas_fname);
        if(getTheme_template_idDirtyFlag())
            srfdomain.setTheme_template_id(theme_template_id);
        if(getMimetypeDirtyFlag())
            srfdomain.setMimetype(mimetype);
        if(getRes_idDirtyFlag())
            srfdomain.setRes_id(res_id);
        if(getStore_fnameDirtyFlag())
            srfdomain.setStore_fname(store_fname);
        if(getWrite_dateDirtyFlag())
            srfdomain.setWrite_date(write_date);
        if(getLocal_urlDirtyFlag())
            srfdomain.setLocal_url(local_url);
        if(getKeyDirtyFlag())
            srfdomain.setKey(key);
        if(getIbizpublicDirtyFlag())
            srfdomain.setIbizpublic(ibizpublic);
        if(getRes_modelDirtyFlag())
            srfdomain.setRes_model(res_model);
        if(get__last_updateDirtyFlag())
            srfdomain.set__last_update(__last_update);
        if(getThumbnailDirtyFlag())
            srfdomain.setThumbnail(thumbnail);
        if(getUrlDirtyFlag())
            srfdomain.setUrl(url);
        if(getFile_sizeDirtyFlag())
            srfdomain.setFile_size(file_size);
        if(getAccess_tokenDirtyFlag())
            srfdomain.setAccess_token(access_token);
        if(getCompany_idDirtyFlag())
            srfdomain.setCompany_id(company_id);
        if(getIr_attachment_idDirtyFlag())
            srfdomain.setIr_attachment_id(ir_attachment_id);
        if(getIdDirtyFlag())
            srfdomain.setId(id);
        if(getTypeDirtyFlag())
            srfdomain.setType(type);
        if(getChecksumDirtyFlag())
            srfdomain.setChecksum(checksum);
        if(getDb_datasDirtyFlag())
            srfdomain.setDb_datas(db_datas);
        if(getIndex_contentDirtyFlag())
            srfdomain.setIndex_content(index_content);
        if(getDescriptionDirtyFlag())
            srfdomain.setDescription(description);
        if(getNameDirtyFlag())
            srfdomain.setName(name);
        if(getDisplay_nameDirtyFlag())
            srfdomain.setDisplay_name(display_name);
        if(getWebsite_urlDirtyFlag())
            srfdomain.setWebsite_url(website_url);
        if(getWebsite_idDirtyFlag())
            srfdomain.setWebsite_id(website_id);
        if(getDatasDirtyFlag())
            srfdomain.setDatas(datas);
        if(getPriorityDirtyFlag())
            srfdomain.setPriority(priority);
        if(getWrite_uid_textDirtyFlag())
            srfdomain.setWrite_uid_text(write_uid_text);
        if(getCreate_uid_textDirtyFlag())
            srfdomain.setCreate_uid_text(create_uid_text);
        if(getCreate_uidDirtyFlag())
            srfdomain.setCreate_uid(create_uid);
        if(getWrite_uidDirtyFlag())
            srfdomain.setWrite_uid(write_uid);

        return srfdomain;
    }

    public void fromDO(Mrp_document srfdomain) {
        if(srfdomain == null )
          return ;
        if(srfdomain.getCreate_dateDirtyFlag())
            this.setCreate_date(srfdomain.getCreate_date());
        if(srfdomain.getActiveDirtyFlag())
            this.setActive(srfdomain.getActive());
        if(srfdomain.getRes_fieldDirtyFlag())
            this.setRes_field(srfdomain.getRes_field());
        if(srfdomain.getRes_nameDirtyFlag())
            this.setRes_name(srfdomain.getRes_name());
        if(srfdomain.getRes_model_nameDirtyFlag())
            this.setRes_model_name(srfdomain.getRes_model_name());
        if(srfdomain.getDatas_fnameDirtyFlag())
            this.setDatas_fname(srfdomain.getDatas_fname());
        if(srfdomain.getTheme_template_idDirtyFlag())
            this.setTheme_template_id(srfdomain.getTheme_template_id());
        if(srfdomain.getMimetypeDirtyFlag())
            this.setMimetype(srfdomain.getMimetype());
        if(srfdomain.getRes_idDirtyFlag())
            this.setRes_id(srfdomain.getRes_id());
        if(srfdomain.getStore_fnameDirtyFlag())
            this.setStore_fname(srfdomain.getStore_fname());
        if(srfdomain.getWrite_dateDirtyFlag())
            this.setWrite_date(srfdomain.getWrite_date());
        if(srfdomain.getLocal_urlDirtyFlag())
            this.setLocal_url(srfdomain.getLocal_url());
        if(srfdomain.getKeyDirtyFlag())
            this.setKey(srfdomain.getKey());
        if(srfdomain.getIbizpublicDirtyFlag())
            this.setIbizpublic(srfdomain.getIbizpublic());
        if(srfdomain.getRes_modelDirtyFlag())
            this.setRes_model(srfdomain.getRes_model());
        if(srfdomain.get__last_updateDirtyFlag())
            this.set__last_update(srfdomain.get__last_update());
        if(srfdomain.getThumbnailDirtyFlag())
            this.setThumbnail(srfdomain.getThumbnail());
        if(srfdomain.getUrlDirtyFlag())
            this.setUrl(srfdomain.getUrl());
        if(srfdomain.getFile_sizeDirtyFlag())
            this.setFile_size(srfdomain.getFile_size());
        if(srfdomain.getAccess_tokenDirtyFlag())
            this.setAccess_token(srfdomain.getAccess_token());
        if(srfdomain.getCompany_idDirtyFlag())
            this.setCompany_id(srfdomain.getCompany_id());
        if(srfdomain.getIr_attachment_idDirtyFlag())
            this.setIr_attachment_id(srfdomain.getIr_attachment_id());
        if(srfdomain.getIdDirtyFlag())
            this.setId(srfdomain.getId());
        if(srfdomain.getTypeDirtyFlag())
            this.setType(srfdomain.getType());
        if(srfdomain.getChecksumDirtyFlag())
            this.setChecksum(srfdomain.getChecksum());
        if(srfdomain.getDb_datasDirtyFlag())
            this.setDb_datas(srfdomain.getDb_datas());
        if(srfdomain.getIndex_contentDirtyFlag())
            this.setIndex_content(srfdomain.getIndex_content());
        if(srfdomain.getDescriptionDirtyFlag())
            this.setDescription(srfdomain.getDescription());
        if(srfdomain.getNameDirtyFlag())
            this.setName(srfdomain.getName());
        if(srfdomain.getDisplay_nameDirtyFlag())
            this.setDisplay_name(srfdomain.getDisplay_name());
        if(srfdomain.getWebsite_urlDirtyFlag())
            this.setWebsite_url(srfdomain.getWebsite_url());
        if(srfdomain.getWebsite_idDirtyFlag())
            this.setWebsite_id(srfdomain.getWebsite_id());
        if(srfdomain.getDatasDirtyFlag())
            this.setDatas(srfdomain.getDatas());
        if(srfdomain.getPriorityDirtyFlag())
            this.setPriority(srfdomain.getPriority());
        if(srfdomain.getWrite_uid_textDirtyFlag())
            this.setWrite_uid_text(srfdomain.getWrite_uid_text());
        if(srfdomain.getCreate_uid_textDirtyFlag())
            this.setCreate_uid_text(srfdomain.getCreate_uid_text());
        if(srfdomain.getCreate_uidDirtyFlag())
            this.setCreate_uid(srfdomain.getCreate_uid());
        if(srfdomain.getWrite_uidDirtyFlag())
            this.setWrite_uid(srfdomain.getWrite_uid());

    }

    public List<Mrp_documentDTO> fromDOPage(List<Mrp_document> poPage)   {
        if(poPage == null)
            return null;
        List<Mrp_documentDTO> dtos=new ArrayList<Mrp_documentDTO>();
        for(Mrp_document domain : poPage) {
            Mrp_documentDTO dto = new Mrp_documentDTO();
            dto.fromDO(domain);
            dtos.add(dto);
        }
        return dtos;
    }
}

