package cn.ibizlab.odoo.service.odoo_mrp.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_mrp.dto.Mrp_workcenterDTO;
import cn.ibizlab.odoo.core.odoo_mrp.domain.Mrp_workcenter;
import cn.ibizlab.odoo.core.odoo_mrp.service.IMrp_workcenterService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_mrp.filter.Mrp_workcenterSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Mrp_workcenter" })
@RestController
@RequestMapping("")
public class Mrp_workcenterResource {

    @Autowired
    private IMrp_workcenterService mrp_workcenterService;

    public IMrp_workcenterService getMrp_workcenterService() {
        return this.mrp_workcenterService;
    }

    @ApiOperation(value = "批建立数据", tags = {"Mrp_workcenter" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_mrp/mrp_workcenters/createBatch")
    public ResponseEntity<Boolean> createBatchMrp_workcenter(@RequestBody List<Mrp_workcenterDTO> mrp_workcenterdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "批删除数据", tags = {"Mrp_workcenter" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_mrp/mrp_workcenters/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Mrp_workcenterDTO> mrp_workcenterdtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "建立数据", tags = {"Mrp_workcenter" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_mrp/mrp_workcenters")

    public ResponseEntity<Mrp_workcenterDTO> create(@RequestBody Mrp_workcenterDTO mrp_workcenterdto) {
        Mrp_workcenterDTO dto = new Mrp_workcenterDTO();
        Mrp_workcenter domain = mrp_workcenterdto.toDO();
		mrp_workcenterService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取数据", tags = {"Mrp_workcenter" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_mrp/mrp_workcenters/{mrp_workcenter_id}")
    public ResponseEntity<Mrp_workcenterDTO> get(@PathVariable("mrp_workcenter_id") Integer mrp_workcenter_id) {
        Mrp_workcenterDTO dto = new Mrp_workcenterDTO();
        Mrp_workcenter domain = mrp_workcenterService.get(mrp_workcenter_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批更新数据", tags = {"Mrp_workcenter" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_mrp/mrp_workcenters/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Mrp_workcenterDTO> mrp_workcenterdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "更新数据", tags = {"Mrp_workcenter" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_mrp/mrp_workcenters/{mrp_workcenter_id}")

    public ResponseEntity<Mrp_workcenterDTO> update(@PathVariable("mrp_workcenter_id") Integer mrp_workcenter_id, @RequestBody Mrp_workcenterDTO mrp_workcenterdto) {
		Mrp_workcenter domain = mrp_workcenterdto.toDO();
        domain.setId(mrp_workcenter_id);
		mrp_workcenterService.update(domain);
		Mrp_workcenterDTO dto = new Mrp_workcenterDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "删除数据", tags = {"Mrp_workcenter" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_mrp/mrp_workcenters/{mrp_workcenter_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("mrp_workcenter_id") Integer mrp_workcenter_id) {
        Mrp_workcenterDTO mrp_workcenterdto = new Mrp_workcenterDTO();
		Mrp_workcenter domain = new Mrp_workcenter();
		mrp_workcenterdto.setId(mrp_workcenter_id);
		domain.setId(mrp_workcenter_id);
        Boolean rst = mrp_workcenterService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

	@ApiOperation(value = "获取默认查询", tags = {"Mrp_workcenter" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_mrp/mrp_workcenters/fetchdefault")
	public ResponseEntity<Page<Mrp_workcenterDTO>> fetchDefault(Mrp_workcenterSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Mrp_workcenterDTO> list = new ArrayList<Mrp_workcenterDTO>();
        
        Page<Mrp_workcenter> domains = mrp_workcenterService.searchDefault(context) ;
        for(Mrp_workcenter mrp_workcenter : domains.getContent()){
            Mrp_workcenterDTO dto = new Mrp_workcenterDTO();
            dto.fromDO(mrp_workcenter);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
