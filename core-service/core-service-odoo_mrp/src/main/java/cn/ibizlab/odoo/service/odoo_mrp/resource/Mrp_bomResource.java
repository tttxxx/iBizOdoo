package cn.ibizlab.odoo.service.odoo_mrp.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_mrp.dto.Mrp_bomDTO;
import cn.ibizlab.odoo.core.odoo_mrp.domain.Mrp_bom;
import cn.ibizlab.odoo.core.odoo_mrp.service.IMrp_bomService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_mrp.filter.Mrp_bomSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Mrp_bom" })
@RestController
@RequestMapping("")
public class Mrp_bomResource {

    @Autowired
    private IMrp_bomService mrp_bomService;

    public IMrp_bomService getMrp_bomService() {
        return this.mrp_bomService;
    }

    @ApiOperation(value = "更新数据", tags = {"Mrp_bom" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_mrp/mrp_boms/{mrp_bom_id}")

    public ResponseEntity<Mrp_bomDTO> update(@PathVariable("mrp_bom_id") Integer mrp_bom_id, @RequestBody Mrp_bomDTO mrp_bomdto) {
		Mrp_bom domain = mrp_bomdto.toDO();
        domain.setId(mrp_bom_id);
		mrp_bomService.update(domain);
		Mrp_bomDTO dto = new Mrp_bomDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "删除数据", tags = {"Mrp_bom" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_mrp/mrp_boms/{mrp_bom_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("mrp_bom_id") Integer mrp_bom_id) {
        Mrp_bomDTO mrp_bomdto = new Mrp_bomDTO();
		Mrp_bom domain = new Mrp_bom();
		mrp_bomdto.setId(mrp_bom_id);
		domain.setId(mrp_bom_id);
        Boolean rst = mrp_bomService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "建立数据", tags = {"Mrp_bom" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_mrp/mrp_boms")

    public ResponseEntity<Mrp_bomDTO> create(@RequestBody Mrp_bomDTO mrp_bomdto) {
        Mrp_bomDTO dto = new Mrp_bomDTO();
        Mrp_bom domain = mrp_bomdto.toDO();
		mrp_bomService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批更新数据", tags = {"Mrp_bom" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_mrp/mrp_boms/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Mrp_bomDTO> mrp_bomdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "获取数据", tags = {"Mrp_bom" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_mrp/mrp_boms/{mrp_bom_id}")
    public ResponseEntity<Mrp_bomDTO> get(@PathVariable("mrp_bom_id") Integer mrp_bom_id) {
        Mrp_bomDTO dto = new Mrp_bomDTO();
        Mrp_bom domain = mrp_bomService.get(mrp_bom_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批建立数据", tags = {"Mrp_bom" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_mrp/mrp_boms/createBatch")
    public ResponseEntity<Boolean> createBatchMrp_bom(@RequestBody List<Mrp_bomDTO> mrp_bomdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "批删除数据", tags = {"Mrp_bom" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_mrp/mrp_boms/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Mrp_bomDTO> mrp_bomdtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Mrp_bom" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_mrp/mrp_boms/fetchdefault")
	public ResponseEntity<Page<Mrp_bomDTO>> fetchDefault(Mrp_bomSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Mrp_bomDTO> list = new ArrayList<Mrp_bomDTO>();
        
        Page<Mrp_bom> domains = mrp_bomService.searchDefault(context) ;
        for(Mrp_bom mrp_bom : domains.getContent()){
            Mrp_bomDTO dto = new Mrp_bomDTO();
            dto.fromDO(mrp_bom);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
