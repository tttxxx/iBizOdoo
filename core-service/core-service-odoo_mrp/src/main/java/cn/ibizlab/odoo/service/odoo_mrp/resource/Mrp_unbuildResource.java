package cn.ibizlab.odoo.service.odoo_mrp.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_mrp.dto.Mrp_unbuildDTO;
import cn.ibizlab.odoo.core.odoo_mrp.domain.Mrp_unbuild;
import cn.ibizlab.odoo.core.odoo_mrp.service.IMrp_unbuildService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_mrp.filter.Mrp_unbuildSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Mrp_unbuild" })
@RestController
@RequestMapping("")
public class Mrp_unbuildResource {

    @Autowired
    private IMrp_unbuildService mrp_unbuildService;

    public IMrp_unbuildService getMrp_unbuildService() {
        return this.mrp_unbuildService;
    }

    @ApiOperation(value = "更新数据", tags = {"Mrp_unbuild" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_mrp/mrp_unbuilds/{mrp_unbuild_id}")

    public ResponseEntity<Mrp_unbuildDTO> update(@PathVariable("mrp_unbuild_id") Integer mrp_unbuild_id, @RequestBody Mrp_unbuildDTO mrp_unbuilddto) {
		Mrp_unbuild domain = mrp_unbuilddto.toDO();
        domain.setId(mrp_unbuild_id);
		mrp_unbuildService.update(domain);
		Mrp_unbuildDTO dto = new Mrp_unbuildDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批删除数据", tags = {"Mrp_unbuild" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_mrp/mrp_unbuilds/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Mrp_unbuildDTO> mrp_unbuilddtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "删除数据", tags = {"Mrp_unbuild" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_mrp/mrp_unbuilds/{mrp_unbuild_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("mrp_unbuild_id") Integer mrp_unbuild_id) {
        Mrp_unbuildDTO mrp_unbuilddto = new Mrp_unbuildDTO();
		Mrp_unbuild domain = new Mrp_unbuild();
		mrp_unbuilddto.setId(mrp_unbuild_id);
		domain.setId(mrp_unbuild_id);
        Boolean rst = mrp_unbuildService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "获取数据", tags = {"Mrp_unbuild" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_mrp/mrp_unbuilds/{mrp_unbuild_id}")
    public ResponseEntity<Mrp_unbuildDTO> get(@PathVariable("mrp_unbuild_id") Integer mrp_unbuild_id) {
        Mrp_unbuildDTO dto = new Mrp_unbuildDTO();
        Mrp_unbuild domain = mrp_unbuildService.get(mrp_unbuild_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批建立数据", tags = {"Mrp_unbuild" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_mrp/mrp_unbuilds/createBatch")
    public ResponseEntity<Boolean> createBatchMrp_unbuild(@RequestBody List<Mrp_unbuildDTO> mrp_unbuilddtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "批更新数据", tags = {"Mrp_unbuild" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_mrp/mrp_unbuilds/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Mrp_unbuildDTO> mrp_unbuilddtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "建立数据", tags = {"Mrp_unbuild" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_mrp/mrp_unbuilds")

    public ResponseEntity<Mrp_unbuildDTO> create(@RequestBody Mrp_unbuildDTO mrp_unbuilddto) {
        Mrp_unbuildDTO dto = new Mrp_unbuildDTO();
        Mrp_unbuild domain = mrp_unbuilddto.toDO();
		mrp_unbuildService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Mrp_unbuild" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_mrp/mrp_unbuilds/fetchdefault")
	public ResponseEntity<Page<Mrp_unbuildDTO>> fetchDefault(Mrp_unbuildSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Mrp_unbuildDTO> list = new ArrayList<Mrp_unbuildDTO>();
        
        Page<Mrp_unbuild> domains = mrp_unbuildService.searchDefault(context) ;
        for(Mrp_unbuild mrp_unbuild : domains.getContent()){
            Mrp_unbuildDTO dto = new Mrp_unbuildDTO();
            dto.fromDO(mrp_unbuild);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
