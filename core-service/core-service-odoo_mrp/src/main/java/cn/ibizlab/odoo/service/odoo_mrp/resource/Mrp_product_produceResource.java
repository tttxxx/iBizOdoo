package cn.ibizlab.odoo.service.odoo_mrp.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_mrp.dto.Mrp_product_produceDTO;
import cn.ibizlab.odoo.core.odoo_mrp.domain.Mrp_product_produce;
import cn.ibizlab.odoo.core.odoo_mrp.service.IMrp_product_produceService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_mrp.filter.Mrp_product_produceSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Mrp_product_produce" })
@RestController
@RequestMapping("")
public class Mrp_product_produceResource {

    @Autowired
    private IMrp_product_produceService mrp_product_produceService;

    public IMrp_product_produceService getMrp_product_produceService() {
        return this.mrp_product_produceService;
    }

    @ApiOperation(value = "建立数据", tags = {"Mrp_product_produce" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_mrp/mrp_product_produces")

    public ResponseEntity<Mrp_product_produceDTO> create(@RequestBody Mrp_product_produceDTO mrp_product_producedto) {
        Mrp_product_produceDTO dto = new Mrp_product_produceDTO();
        Mrp_product_produce domain = mrp_product_producedto.toDO();
		mrp_product_produceService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批更新数据", tags = {"Mrp_product_produce" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_mrp/mrp_product_produces/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Mrp_product_produceDTO> mrp_product_producedtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "批删除数据", tags = {"Mrp_product_produce" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_mrp/mrp_product_produces/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Mrp_product_produceDTO> mrp_product_producedtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "批建立数据", tags = {"Mrp_product_produce" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_mrp/mrp_product_produces/createBatch")
    public ResponseEntity<Boolean> createBatchMrp_product_produce(@RequestBody List<Mrp_product_produceDTO> mrp_product_producedtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "更新数据", tags = {"Mrp_product_produce" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_mrp/mrp_product_produces/{mrp_product_produce_id}")

    public ResponseEntity<Mrp_product_produceDTO> update(@PathVariable("mrp_product_produce_id") Integer mrp_product_produce_id, @RequestBody Mrp_product_produceDTO mrp_product_producedto) {
		Mrp_product_produce domain = mrp_product_producedto.toDO();
        domain.setId(mrp_product_produce_id);
		mrp_product_produceService.update(domain);
		Mrp_product_produceDTO dto = new Mrp_product_produceDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "删除数据", tags = {"Mrp_product_produce" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_mrp/mrp_product_produces/{mrp_product_produce_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("mrp_product_produce_id") Integer mrp_product_produce_id) {
        Mrp_product_produceDTO mrp_product_producedto = new Mrp_product_produceDTO();
		Mrp_product_produce domain = new Mrp_product_produce();
		mrp_product_producedto.setId(mrp_product_produce_id);
		domain.setId(mrp_product_produce_id);
        Boolean rst = mrp_product_produceService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "获取数据", tags = {"Mrp_product_produce" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_mrp/mrp_product_produces/{mrp_product_produce_id}")
    public ResponseEntity<Mrp_product_produceDTO> get(@PathVariable("mrp_product_produce_id") Integer mrp_product_produce_id) {
        Mrp_product_produceDTO dto = new Mrp_product_produceDTO();
        Mrp_product_produce domain = mrp_product_produceService.get(mrp_product_produce_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Mrp_product_produce" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_mrp/mrp_product_produces/fetchdefault")
	public ResponseEntity<Page<Mrp_product_produceDTO>> fetchDefault(Mrp_product_produceSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Mrp_product_produceDTO> list = new ArrayList<Mrp_product_produceDTO>();
        
        Page<Mrp_product_produce> domains = mrp_product_produceService.searchDefault(context) ;
        for(Mrp_product_produce mrp_product_produce : domains.getContent()){
            Mrp_product_produceDTO dto = new Mrp_product_produceDTO();
            dto.fromDO(mrp_product_produce);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
