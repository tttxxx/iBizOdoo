package cn.ibizlab.odoo.service.odoo_purchase.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.core.odoo_purchase.valuerule.anno.purchase_bill_union.*;
import cn.ibizlab.odoo.core.odoo_purchase.domain.Purchase_bill_union;
import org.springframework.cglib.beans.BeanCopier;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 服务DTO对象[Purchase_bill_unionDTO]
 */
public class Purchase_bill_unionDTO implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [ID]
     *
     */
    @Purchase_bill_unionIdDefault(info = "默认规则")
    private Integer id;

    @JsonIgnore
    private boolean idDirtyFlag;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @Purchase_bill_unionDisplay_nameDefault(info = "默认规则")
    private String display_name;

    @JsonIgnore
    private boolean display_nameDirtyFlag;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @Purchase_bill_union__last_updateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp __last_update;

    @JsonIgnore
    private boolean __last_updateDirtyFlag;

    /**
     * 属性 [REFERENCE]
     *
     */
    @Purchase_bill_unionReferenceDefault(info = "默认规则")
    private String reference;

    @JsonIgnore
    private boolean referenceDirtyFlag;

    /**
     * 属性 [NAME]
     *
     */
    @Purchase_bill_unionNameDefault(info = "默认规则")
    private String name;

    @JsonIgnore
    private boolean nameDirtyFlag;

    /**
     * 属性 [AMOUNT]
     *
     */
    @Purchase_bill_unionAmountDefault(info = "默认规则")
    private Double amount;

    @JsonIgnore
    private boolean amountDirtyFlag;

    /**
     * 属性 [DATE]
     *
     */
    @Purchase_bill_unionDateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    private Timestamp date;

    @JsonIgnore
    private boolean dateDirtyFlag;

    /**
     * 属性 [PURCHASE_ORDER_ID_TEXT]
     *
     */
    @Purchase_bill_unionPurchase_order_id_textDefault(info = "默认规则")
    private String purchase_order_id_text;

    @JsonIgnore
    private boolean purchase_order_id_textDirtyFlag;

    /**
     * 属性 [CURRENCY_ID_TEXT]
     *
     */
    @Purchase_bill_unionCurrency_id_textDefault(info = "默认规则")
    private String currency_id_text;

    @JsonIgnore
    private boolean currency_id_textDirtyFlag;

    /**
     * 属性 [PARTNER_ID_TEXT]
     *
     */
    @Purchase_bill_unionPartner_id_textDefault(info = "默认规则")
    private String partner_id_text;

    @JsonIgnore
    private boolean partner_id_textDirtyFlag;

    /**
     * 属性 [VENDOR_BILL_ID_TEXT]
     *
     */
    @Purchase_bill_unionVendor_bill_id_textDefault(info = "默认规则")
    private String vendor_bill_id_text;

    @JsonIgnore
    private boolean vendor_bill_id_textDirtyFlag;

    /**
     * 属性 [COMPANY_ID_TEXT]
     *
     */
    @Purchase_bill_unionCompany_id_textDefault(info = "默认规则")
    private String company_id_text;

    @JsonIgnore
    private boolean company_id_textDirtyFlag;

    /**
     * 属性 [VENDOR_BILL_ID]
     *
     */
    @Purchase_bill_unionVendor_bill_idDefault(info = "默认规则")
    private Integer vendor_bill_id;

    @JsonIgnore
    private boolean vendor_bill_idDirtyFlag;

    /**
     * 属性 [CURRENCY_ID]
     *
     */
    @Purchase_bill_unionCurrency_idDefault(info = "默认规则")
    private Integer currency_id;

    @JsonIgnore
    private boolean currency_idDirtyFlag;

    /**
     * 属性 [PURCHASE_ORDER_ID]
     *
     */
    @Purchase_bill_unionPurchase_order_idDefault(info = "默认规则")
    private Integer purchase_order_id;

    @JsonIgnore
    private boolean purchase_order_idDirtyFlag;

    /**
     * 属性 [COMPANY_ID]
     *
     */
    @Purchase_bill_unionCompany_idDefault(info = "默认规则")
    private Integer company_id;

    @JsonIgnore
    private boolean company_idDirtyFlag;

    /**
     * 属性 [PARTNER_ID]
     *
     */
    @Purchase_bill_unionPartner_idDefault(info = "默认规则")
    private Integer partner_id;

    @JsonIgnore
    private boolean partner_idDirtyFlag;


    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

    /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return idDirtyFlag ;
    }

    /**
     * 获取 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return display_name ;
    }

    /**
     * 设置 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

    /**
     * 获取 [DISPLAY_NAME]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return display_nameDirtyFlag ;
    }

    /**
     * 获取 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return __last_update ;
    }

    /**
     * 设置 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

    /**
     * 获取 [__LAST_UPDATE]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return __last_updateDirtyFlag ;
    }

    /**
     * 获取 [REFERENCE]
     */
    @JsonProperty("reference")
    public String getReference(){
        return reference ;
    }

    /**
     * 设置 [REFERENCE]
     */
    @JsonProperty("reference")
    public void setReference(String  reference){
        this.reference = reference ;
        this.referenceDirtyFlag = true ;
    }

    /**
     * 获取 [REFERENCE]脏标记
     */
    @JsonIgnore
    public boolean getReferenceDirtyFlag(){
        return referenceDirtyFlag ;
    }

    /**
     * 获取 [NAME]
     */
    @JsonProperty("name")
    public String getName(){
        return name ;
    }

    /**
     * 设置 [NAME]
     */
    @JsonProperty("name")
    public void setName(String  name){
        this.name = name ;
        this.nameDirtyFlag = true ;
    }

    /**
     * 获取 [NAME]脏标记
     */
    @JsonIgnore
    public boolean getNameDirtyFlag(){
        return nameDirtyFlag ;
    }

    /**
     * 获取 [AMOUNT]
     */
    @JsonProperty("amount")
    public Double getAmount(){
        return amount ;
    }

    /**
     * 设置 [AMOUNT]
     */
    @JsonProperty("amount")
    public void setAmount(Double  amount){
        this.amount = amount ;
        this.amountDirtyFlag = true ;
    }

    /**
     * 获取 [AMOUNT]脏标记
     */
    @JsonIgnore
    public boolean getAmountDirtyFlag(){
        return amountDirtyFlag ;
    }

    /**
     * 获取 [DATE]
     */
    @JsonProperty("date")
    public Timestamp getDate(){
        return date ;
    }

    /**
     * 设置 [DATE]
     */
    @JsonProperty("date")
    public void setDate(Timestamp  date){
        this.date = date ;
        this.dateDirtyFlag = true ;
    }

    /**
     * 获取 [DATE]脏标记
     */
    @JsonIgnore
    public boolean getDateDirtyFlag(){
        return dateDirtyFlag ;
    }

    /**
     * 获取 [PURCHASE_ORDER_ID_TEXT]
     */
    @JsonProperty("purchase_order_id_text")
    public String getPurchase_order_id_text(){
        return purchase_order_id_text ;
    }

    /**
     * 设置 [PURCHASE_ORDER_ID_TEXT]
     */
    @JsonProperty("purchase_order_id_text")
    public void setPurchase_order_id_text(String  purchase_order_id_text){
        this.purchase_order_id_text = purchase_order_id_text ;
        this.purchase_order_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [PURCHASE_ORDER_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getPurchase_order_id_textDirtyFlag(){
        return purchase_order_id_textDirtyFlag ;
    }

    /**
     * 获取 [CURRENCY_ID_TEXT]
     */
    @JsonProperty("currency_id_text")
    public String getCurrency_id_text(){
        return currency_id_text ;
    }

    /**
     * 设置 [CURRENCY_ID_TEXT]
     */
    @JsonProperty("currency_id_text")
    public void setCurrency_id_text(String  currency_id_text){
        this.currency_id_text = currency_id_text ;
        this.currency_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [CURRENCY_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getCurrency_id_textDirtyFlag(){
        return currency_id_textDirtyFlag ;
    }

    /**
     * 获取 [PARTNER_ID_TEXT]
     */
    @JsonProperty("partner_id_text")
    public String getPartner_id_text(){
        return partner_id_text ;
    }

    /**
     * 设置 [PARTNER_ID_TEXT]
     */
    @JsonProperty("partner_id_text")
    public void setPartner_id_text(String  partner_id_text){
        this.partner_id_text = partner_id_text ;
        this.partner_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [PARTNER_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getPartner_id_textDirtyFlag(){
        return partner_id_textDirtyFlag ;
    }

    /**
     * 获取 [VENDOR_BILL_ID_TEXT]
     */
    @JsonProperty("vendor_bill_id_text")
    public String getVendor_bill_id_text(){
        return vendor_bill_id_text ;
    }

    /**
     * 设置 [VENDOR_BILL_ID_TEXT]
     */
    @JsonProperty("vendor_bill_id_text")
    public void setVendor_bill_id_text(String  vendor_bill_id_text){
        this.vendor_bill_id_text = vendor_bill_id_text ;
        this.vendor_bill_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [VENDOR_BILL_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getVendor_bill_id_textDirtyFlag(){
        return vendor_bill_id_textDirtyFlag ;
    }

    /**
     * 获取 [COMPANY_ID_TEXT]
     */
    @JsonProperty("company_id_text")
    public String getCompany_id_text(){
        return company_id_text ;
    }

    /**
     * 设置 [COMPANY_ID_TEXT]
     */
    @JsonProperty("company_id_text")
    public void setCompany_id_text(String  company_id_text){
        this.company_id_text = company_id_text ;
        this.company_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [COMPANY_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getCompany_id_textDirtyFlag(){
        return company_id_textDirtyFlag ;
    }

    /**
     * 获取 [VENDOR_BILL_ID]
     */
    @JsonProperty("vendor_bill_id")
    public Integer getVendor_bill_id(){
        return vendor_bill_id ;
    }

    /**
     * 设置 [VENDOR_BILL_ID]
     */
    @JsonProperty("vendor_bill_id")
    public void setVendor_bill_id(Integer  vendor_bill_id){
        this.vendor_bill_id = vendor_bill_id ;
        this.vendor_bill_idDirtyFlag = true ;
    }

    /**
     * 获取 [VENDOR_BILL_ID]脏标记
     */
    @JsonIgnore
    public boolean getVendor_bill_idDirtyFlag(){
        return vendor_bill_idDirtyFlag ;
    }

    /**
     * 获取 [CURRENCY_ID]
     */
    @JsonProperty("currency_id")
    public Integer getCurrency_id(){
        return currency_id ;
    }

    /**
     * 设置 [CURRENCY_ID]
     */
    @JsonProperty("currency_id")
    public void setCurrency_id(Integer  currency_id){
        this.currency_id = currency_id ;
        this.currency_idDirtyFlag = true ;
    }

    /**
     * 获取 [CURRENCY_ID]脏标记
     */
    @JsonIgnore
    public boolean getCurrency_idDirtyFlag(){
        return currency_idDirtyFlag ;
    }

    /**
     * 获取 [PURCHASE_ORDER_ID]
     */
    @JsonProperty("purchase_order_id")
    public Integer getPurchase_order_id(){
        return purchase_order_id ;
    }

    /**
     * 设置 [PURCHASE_ORDER_ID]
     */
    @JsonProperty("purchase_order_id")
    public void setPurchase_order_id(Integer  purchase_order_id){
        this.purchase_order_id = purchase_order_id ;
        this.purchase_order_idDirtyFlag = true ;
    }

    /**
     * 获取 [PURCHASE_ORDER_ID]脏标记
     */
    @JsonIgnore
    public boolean getPurchase_order_idDirtyFlag(){
        return purchase_order_idDirtyFlag ;
    }

    /**
     * 获取 [COMPANY_ID]
     */
    @JsonProperty("company_id")
    public Integer getCompany_id(){
        return company_id ;
    }

    /**
     * 设置 [COMPANY_ID]
     */
    @JsonProperty("company_id")
    public void setCompany_id(Integer  company_id){
        this.company_id = company_id ;
        this.company_idDirtyFlag = true ;
    }

    /**
     * 获取 [COMPANY_ID]脏标记
     */
    @JsonIgnore
    public boolean getCompany_idDirtyFlag(){
        return company_idDirtyFlag ;
    }

    /**
     * 获取 [PARTNER_ID]
     */
    @JsonProperty("partner_id")
    public Integer getPartner_id(){
        return partner_id ;
    }

    /**
     * 设置 [PARTNER_ID]
     */
    @JsonProperty("partner_id")
    public void setPartner_id(Integer  partner_id){
        this.partner_id = partner_id ;
        this.partner_idDirtyFlag = true ;
    }

    /**
     * 获取 [PARTNER_ID]脏标记
     */
    @JsonIgnore
    public boolean getPartner_idDirtyFlag(){
        return partner_idDirtyFlag ;
    }



    public Purchase_bill_union toDO() {
        Purchase_bill_union srfdomain = new Purchase_bill_union();
        if(getIdDirtyFlag())
            srfdomain.setId(id);
        if(getDisplay_nameDirtyFlag())
            srfdomain.setDisplay_name(display_name);
        if(get__last_updateDirtyFlag())
            srfdomain.set__last_update(__last_update);
        if(getReferenceDirtyFlag())
            srfdomain.setReference(reference);
        if(getNameDirtyFlag())
            srfdomain.setName(name);
        if(getAmountDirtyFlag())
            srfdomain.setAmount(amount);
        if(getDateDirtyFlag())
            srfdomain.setDate(date);
        if(getPurchase_order_id_textDirtyFlag())
            srfdomain.setPurchase_order_id_text(purchase_order_id_text);
        if(getCurrency_id_textDirtyFlag())
            srfdomain.setCurrency_id_text(currency_id_text);
        if(getPartner_id_textDirtyFlag())
            srfdomain.setPartner_id_text(partner_id_text);
        if(getVendor_bill_id_textDirtyFlag())
            srfdomain.setVendor_bill_id_text(vendor_bill_id_text);
        if(getCompany_id_textDirtyFlag())
            srfdomain.setCompany_id_text(company_id_text);
        if(getVendor_bill_idDirtyFlag())
            srfdomain.setVendor_bill_id(vendor_bill_id);
        if(getCurrency_idDirtyFlag())
            srfdomain.setCurrency_id(currency_id);
        if(getPurchase_order_idDirtyFlag())
            srfdomain.setPurchase_order_id(purchase_order_id);
        if(getCompany_idDirtyFlag())
            srfdomain.setCompany_id(company_id);
        if(getPartner_idDirtyFlag())
            srfdomain.setPartner_id(partner_id);

        return srfdomain;
    }

    public void fromDO(Purchase_bill_union srfdomain) {
        if(srfdomain == null )
          return ;
        if(srfdomain.getIdDirtyFlag())
            this.setId(srfdomain.getId());
        if(srfdomain.getDisplay_nameDirtyFlag())
            this.setDisplay_name(srfdomain.getDisplay_name());
        if(srfdomain.get__last_updateDirtyFlag())
            this.set__last_update(srfdomain.get__last_update());
        if(srfdomain.getReferenceDirtyFlag())
            this.setReference(srfdomain.getReference());
        if(srfdomain.getNameDirtyFlag())
            this.setName(srfdomain.getName());
        if(srfdomain.getAmountDirtyFlag())
            this.setAmount(srfdomain.getAmount());
        if(srfdomain.getDateDirtyFlag())
            this.setDate(srfdomain.getDate());
        if(srfdomain.getPurchase_order_id_textDirtyFlag())
            this.setPurchase_order_id_text(srfdomain.getPurchase_order_id_text());
        if(srfdomain.getCurrency_id_textDirtyFlag())
            this.setCurrency_id_text(srfdomain.getCurrency_id_text());
        if(srfdomain.getPartner_id_textDirtyFlag())
            this.setPartner_id_text(srfdomain.getPartner_id_text());
        if(srfdomain.getVendor_bill_id_textDirtyFlag())
            this.setVendor_bill_id_text(srfdomain.getVendor_bill_id_text());
        if(srfdomain.getCompany_id_textDirtyFlag())
            this.setCompany_id_text(srfdomain.getCompany_id_text());
        if(srfdomain.getVendor_bill_idDirtyFlag())
            this.setVendor_bill_id(srfdomain.getVendor_bill_id());
        if(srfdomain.getCurrency_idDirtyFlag())
            this.setCurrency_id(srfdomain.getCurrency_id());
        if(srfdomain.getPurchase_order_idDirtyFlag())
            this.setPurchase_order_id(srfdomain.getPurchase_order_id());
        if(srfdomain.getCompany_idDirtyFlag())
            this.setCompany_id(srfdomain.getCompany_id());
        if(srfdomain.getPartner_idDirtyFlag())
            this.setPartner_id(srfdomain.getPartner_id());

    }

    public List<Purchase_bill_unionDTO> fromDOPage(List<Purchase_bill_union> poPage)   {
        if(poPage == null)
            return null;
        List<Purchase_bill_unionDTO> dtos=new ArrayList<Purchase_bill_unionDTO>();
        for(Purchase_bill_union domain : poPage) {
            Purchase_bill_unionDTO dto = new Purchase_bill_unionDTO();
            dto.fromDO(domain);
            dtos.add(dto);
        }
        return dtos;
    }
}

