package cn.ibizlab.odoo.service.odoo_purchase.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_purchase.dto.Purchase_reportDTO;
import cn.ibizlab.odoo.core.odoo_purchase.domain.Purchase_report;
import cn.ibizlab.odoo.core.odoo_purchase.service.IPurchase_reportService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_purchase.filter.Purchase_reportSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Purchase_report" })
@RestController
@RequestMapping("")
public class Purchase_reportResource {

    @Autowired
    private IPurchase_reportService purchase_reportService;

    public IPurchase_reportService getPurchase_reportService() {
        return this.purchase_reportService;
    }

    @ApiOperation(value = "更新数据", tags = {"Purchase_report" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_purchase/purchase_reports/{purchase_report_id}")

    public ResponseEntity<Purchase_reportDTO> update(@PathVariable("purchase_report_id") Integer purchase_report_id, @RequestBody Purchase_reportDTO purchase_reportdto) {
		Purchase_report domain = purchase_reportdto.toDO();
        domain.setId(purchase_report_id);
		purchase_reportService.update(domain);
		Purchase_reportDTO dto = new Purchase_reportDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "建立数据", tags = {"Purchase_report" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_purchase/purchase_reports")

    public ResponseEntity<Purchase_reportDTO> create(@RequestBody Purchase_reportDTO purchase_reportdto) {
        Purchase_reportDTO dto = new Purchase_reportDTO();
        Purchase_report domain = purchase_reportdto.toDO();
		purchase_reportService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "删除数据", tags = {"Purchase_report" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_purchase/purchase_reports/{purchase_report_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("purchase_report_id") Integer purchase_report_id) {
        Purchase_reportDTO purchase_reportdto = new Purchase_reportDTO();
		Purchase_report domain = new Purchase_report();
		purchase_reportdto.setId(purchase_report_id);
		domain.setId(purchase_report_id);
        Boolean rst = purchase_reportService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "获取数据", tags = {"Purchase_report" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_purchase/purchase_reports/{purchase_report_id}")
    public ResponseEntity<Purchase_reportDTO> get(@PathVariable("purchase_report_id") Integer purchase_report_id) {
        Purchase_reportDTO dto = new Purchase_reportDTO();
        Purchase_report domain = purchase_reportService.get(purchase_report_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批建立数据", tags = {"Purchase_report" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_purchase/purchase_reports/createBatch")
    public ResponseEntity<Boolean> createBatchPurchase_report(@RequestBody List<Purchase_reportDTO> purchase_reportdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "批更新数据", tags = {"Purchase_report" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_purchase/purchase_reports/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Purchase_reportDTO> purchase_reportdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "批删除数据", tags = {"Purchase_report" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_purchase/purchase_reports/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Purchase_reportDTO> purchase_reportdtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Purchase_report" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_purchase/purchase_reports/fetchdefault")
	public ResponseEntity<Page<Purchase_reportDTO>> fetchDefault(Purchase_reportSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Purchase_reportDTO> list = new ArrayList<Purchase_reportDTO>();
        
        Page<Purchase_report> domains = purchase_reportService.searchDefault(context) ;
        for(Purchase_report purchase_report : domains.getContent()){
            Purchase_reportDTO dto = new Purchase_reportDTO();
            dto.fromDO(purchase_report);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
