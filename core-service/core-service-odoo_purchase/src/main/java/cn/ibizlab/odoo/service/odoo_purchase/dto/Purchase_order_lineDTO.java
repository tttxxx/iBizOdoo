package cn.ibizlab.odoo.service.odoo_purchase.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.core.odoo_purchase.valuerule.anno.purchase_order_line.*;
import cn.ibizlab.odoo.core.odoo_purchase.domain.Purchase_order_line;
import org.springframework.cglib.beans.BeanCopier;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 服务DTO对象[Purchase_order_lineDTO]
 */
public class Purchase_order_lineDTO implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [PRICE_UNIT]
     *
     */
    @Purchase_order_linePrice_unitDefault(info = "默认规则")
    private Double price_unit;

    @JsonIgnore
    private boolean price_unitDirtyFlag;

    /**
     * 属性 [QTY_RECEIVED]
     *
     */
    @Purchase_order_lineQty_receivedDefault(info = "默认规则")
    private Double qty_received;

    @JsonIgnore
    private boolean qty_receivedDirtyFlag;

    /**
     * 属性 [PRICE_TOTAL]
     *
     */
    @Purchase_order_linePrice_totalDefault(info = "默认规则")
    private Double price_total;

    @JsonIgnore
    private boolean price_totalDirtyFlag;

    /**
     * 属性 [NAME]
     *
     */
    @Purchase_order_lineNameDefault(info = "默认规则")
    private String name;

    @JsonIgnore
    private boolean nameDirtyFlag;

    /**
     * 属性 [PRODUCT_QTY]
     *
     */
    @Purchase_order_lineProduct_qtyDefault(info = "默认规则")
    private Double product_qty;

    @JsonIgnore
    private boolean product_qtyDirtyFlag;

    /**
     * 属性 [PRICE_TAX]
     *
     */
    @Purchase_order_linePrice_taxDefault(info = "默认规则")
    private Double price_tax;

    @JsonIgnore
    private boolean price_taxDirtyFlag;

    /**
     * 属性 [MOVE_IDS]
     *
     */
    @Purchase_order_lineMove_idsDefault(info = "默认规则")
    private String move_ids;

    @JsonIgnore
    private boolean move_idsDirtyFlag;

    /**
     * 属性 [PRICE_SUBTOTAL]
     *
     */
    @Purchase_order_linePrice_subtotalDefault(info = "默认规则")
    private Double price_subtotal;

    @JsonIgnore
    private boolean price_subtotalDirtyFlag;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @Purchase_order_line__last_updateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp __last_update;

    @JsonIgnore
    private boolean __last_updateDirtyFlag;

    /**
     * 属性 [SEQUENCE]
     *
     */
    @Purchase_order_lineSequenceDefault(info = "默认规则")
    private Integer sequence;

    @JsonIgnore
    private boolean sequenceDirtyFlag;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @Purchase_order_lineDisplay_nameDefault(info = "默认规则")
    private String display_name;

    @JsonIgnore
    private boolean display_nameDirtyFlag;

    /**
     * 属性 [PRODUCT_UOM_QTY]
     *
     */
    @Purchase_order_lineProduct_uom_qtyDefault(info = "默认规则")
    private Double product_uom_qty;

    @JsonIgnore
    private boolean product_uom_qtyDirtyFlag;

    /**
     * 属性 [TAXES_ID]
     *
     */
    @Purchase_order_lineTaxes_idDefault(info = "默认规则")
    private String taxes_id;

    @JsonIgnore
    private boolean taxes_idDirtyFlag;

    /**
     * 属性 [INVOICE_LINES]
     *
     */
    @Purchase_order_lineInvoice_linesDefault(info = "默认规则")
    private String invoice_lines;

    @JsonIgnore
    private boolean invoice_linesDirtyFlag;

    /**
     * 属性 [CREATE_DATE]
     *
     */
    @Purchase_order_lineCreate_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp create_date;

    @JsonIgnore
    private boolean create_dateDirtyFlag;

    /**
     * 属性 [WRITE_DATE]
     *
     */
    @Purchase_order_lineWrite_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp write_date;

    @JsonIgnore
    private boolean write_dateDirtyFlag;

    /**
     * 属性 [DATE_PLANNED]
     *
     */
    @Purchase_order_lineDate_plannedDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp date_planned;

    @JsonIgnore
    private boolean date_plannedDirtyFlag;

    /**
     * 属性 [QTY_INVOICED]
     *
     */
    @Purchase_order_lineQty_invoicedDefault(info = "默认规则")
    private Double qty_invoiced;

    @JsonIgnore
    private boolean qty_invoicedDirtyFlag;

    /**
     * 属性 [ANALYTIC_TAG_IDS]
     *
     */
    @Purchase_order_lineAnalytic_tag_idsDefault(info = "默认规则")
    private String analytic_tag_ids;

    @JsonIgnore
    private boolean analytic_tag_idsDirtyFlag;

    /**
     * 属性 [MOVE_DEST_IDS]
     *
     */
    @Purchase_order_lineMove_dest_idsDefault(info = "默认规则")
    private String move_dest_ids;

    @JsonIgnore
    private boolean move_dest_idsDirtyFlag;

    /**
     * 属性 [ID]
     *
     */
    @Purchase_order_lineIdDefault(info = "默认规则")
    private Integer id;

    @JsonIgnore
    private boolean idDirtyFlag;

    /**
     * 属性 [PARTNER_ID_TEXT]
     *
     */
    @Purchase_order_linePartner_id_textDefault(info = "默认规则")
    private String partner_id_text;

    @JsonIgnore
    private boolean partner_id_textDirtyFlag;

    /**
     * 属性 [CREATE_UID_TEXT]
     *
     */
    @Purchase_order_lineCreate_uid_textDefault(info = "默认规则")
    private String create_uid_text;

    @JsonIgnore
    private boolean create_uid_textDirtyFlag;

    /**
     * 属性 [ORDERPOINT_ID_TEXT]
     *
     */
    @Purchase_order_lineOrderpoint_id_textDefault(info = "默认规则")
    private String orderpoint_id_text;

    @JsonIgnore
    private boolean orderpoint_id_textDirtyFlag;

    /**
     * 属性 [SALE_ORDER_ID_TEXT]
     *
     */
    @Purchase_order_lineSale_order_id_textDefault(info = "默认规则")
    private String sale_order_id_text;

    @JsonIgnore
    private boolean sale_order_id_textDirtyFlag;

    /**
     * 属性 [PRODUCT_ID_TEXT]
     *
     */
    @Purchase_order_lineProduct_id_textDefault(info = "默认规则")
    private String product_id_text;

    @JsonIgnore
    private boolean product_id_textDirtyFlag;

    /**
     * 属性 [PRODUCT_TYPE]
     *
     */
    @Purchase_order_lineProduct_typeDefault(info = "默认规则")
    private String product_type;

    @JsonIgnore
    private boolean product_typeDirtyFlag;

    /**
     * 属性 [WRITE_UID_TEXT]
     *
     */
    @Purchase_order_lineWrite_uid_textDefault(info = "默认规则")
    private String write_uid_text;

    @JsonIgnore
    private boolean write_uid_textDirtyFlag;

    /**
     * 属性 [COMPANY_ID_TEXT]
     *
     */
    @Purchase_order_lineCompany_id_textDefault(info = "默认规则")
    private String company_id_text;

    @JsonIgnore
    private boolean company_id_textDirtyFlag;

    /**
     * 属性 [STATE]
     *
     */
    @Purchase_order_lineStateDefault(info = "默认规则")
    private String state;

    @JsonIgnore
    private boolean stateDirtyFlag;

    /**
     * 属性 [ACCOUNT_ANALYTIC_ID_TEXT]
     *
     */
    @Purchase_order_lineAccount_analytic_id_textDefault(info = "默认规则")
    private String account_analytic_id_text;

    @JsonIgnore
    private boolean account_analytic_id_textDirtyFlag;

    /**
     * 属性 [PRODUCT_IMAGE]
     *
     */
    @Purchase_order_lineProduct_imageDefault(info = "默认规则")
    private byte[] product_image;

    @JsonIgnore
    private boolean product_imageDirtyFlag;

    /**
     * 属性 [PRODUCT_UOM_TEXT]
     *
     */
    @Purchase_order_lineProduct_uom_textDefault(info = "默认规则")
    private String product_uom_text;

    @JsonIgnore
    private boolean product_uom_textDirtyFlag;

    /**
     * 属性 [ORDER_ID_TEXT]
     *
     */
    @Purchase_order_lineOrder_id_textDefault(info = "默认规则")
    private String order_id_text;

    @JsonIgnore
    private boolean order_id_textDirtyFlag;

    /**
     * 属性 [CURRENCY_ID_TEXT]
     *
     */
    @Purchase_order_lineCurrency_id_textDefault(info = "默认规则")
    private String currency_id_text;

    @JsonIgnore
    private boolean currency_id_textDirtyFlag;

    /**
     * 属性 [SALE_LINE_ID_TEXT]
     *
     */
    @Purchase_order_lineSale_line_id_textDefault(info = "默认规则")
    private String sale_line_id_text;

    @JsonIgnore
    private boolean sale_line_id_textDirtyFlag;

    /**
     * 属性 [DATE_ORDER]
     *
     */
    @Purchase_order_lineDate_orderDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp date_order;

    @JsonIgnore
    private boolean date_orderDirtyFlag;

    /**
     * 属性 [COMPANY_ID]
     *
     */
    @Purchase_order_lineCompany_idDefault(info = "默认规则")
    private Integer company_id;

    @JsonIgnore
    private boolean company_idDirtyFlag;

    /**
     * 属性 [ACCOUNT_ANALYTIC_ID]
     *
     */
    @Purchase_order_lineAccount_analytic_idDefault(info = "默认规则")
    private Integer account_analytic_id;

    @JsonIgnore
    private boolean account_analytic_idDirtyFlag;

    /**
     * 属性 [CURRENCY_ID]
     *
     */
    @Purchase_order_lineCurrency_idDefault(info = "默认规则")
    private Integer currency_id;

    @JsonIgnore
    private boolean currency_idDirtyFlag;

    /**
     * 属性 [SALE_ORDER_ID]
     *
     */
    @Purchase_order_lineSale_order_idDefault(info = "默认规则")
    private Integer sale_order_id;

    @JsonIgnore
    private boolean sale_order_idDirtyFlag;

    /**
     * 属性 [PRODUCT_UOM]
     *
     */
    @Purchase_order_lineProduct_uomDefault(info = "默认规则")
    private Integer product_uom;

    @JsonIgnore
    private boolean product_uomDirtyFlag;

    /**
     * 属性 [SALE_LINE_ID]
     *
     */
    @Purchase_order_lineSale_line_idDefault(info = "默认规则")
    private Integer sale_line_id;

    @JsonIgnore
    private boolean sale_line_idDirtyFlag;

    /**
     * 属性 [ORDER_ID]
     *
     */
    @Purchase_order_lineOrder_idDefault(info = "默认规则")
    private Integer order_id;

    @JsonIgnore
    private boolean order_idDirtyFlag;

    /**
     * 属性 [ORDERPOINT_ID]
     *
     */
    @Purchase_order_lineOrderpoint_idDefault(info = "默认规则")
    private Integer orderpoint_id;

    @JsonIgnore
    private boolean orderpoint_idDirtyFlag;

    /**
     * 属性 [CREATE_UID]
     *
     */
    @Purchase_order_lineCreate_uidDefault(info = "默认规则")
    private Integer create_uid;

    @JsonIgnore
    private boolean create_uidDirtyFlag;

    /**
     * 属性 [WRITE_UID]
     *
     */
    @Purchase_order_lineWrite_uidDefault(info = "默认规则")
    private Integer write_uid;

    @JsonIgnore
    private boolean write_uidDirtyFlag;

    /**
     * 属性 [PRODUCT_ID]
     *
     */
    @Purchase_order_lineProduct_idDefault(info = "默认规则")
    private Integer product_id;

    @JsonIgnore
    private boolean product_idDirtyFlag;

    /**
     * 属性 [PARTNER_ID]
     *
     */
    @Purchase_order_linePartner_idDefault(info = "默认规则")
    private Integer partner_id;

    @JsonIgnore
    private boolean partner_idDirtyFlag;


    /**
     * 获取 [PRICE_UNIT]
     */
    @JsonProperty("price_unit")
    public Double getPrice_unit(){
        return price_unit ;
    }

    /**
     * 设置 [PRICE_UNIT]
     */
    @JsonProperty("price_unit")
    public void setPrice_unit(Double  price_unit){
        this.price_unit = price_unit ;
        this.price_unitDirtyFlag = true ;
    }

    /**
     * 获取 [PRICE_UNIT]脏标记
     */
    @JsonIgnore
    public boolean getPrice_unitDirtyFlag(){
        return price_unitDirtyFlag ;
    }

    /**
     * 获取 [QTY_RECEIVED]
     */
    @JsonProperty("qty_received")
    public Double getQty_received(){
        return qty_received ;
    }

    /**
     * 设置 [QTY_RECEIVED]
     */
    @JsonProperty("qty_received")
    public void setQty_received(Double  qty_received){
        this.qty_received = qty_received ;
        this.qty_receivedDirtyFlag = true ;
    }

    /**
     * 获取 [QTY_RECEIVED]脏标记
     */
    @JsonIgnore
    public boolean getQty_receivedDirtyFlag(){
        return qty_receivedDirtyFlag ;
    }

    /**
     * 获取 [PRICE_TOTAL]
     */
    @JsonProperty("price_total")
    public Double getPrice_total(){
        return price_total ;
    }

    /**
     * 设置 [PRICE_TOTAL]
     */
    @JsonProperty("price_total")
    public void setPrice_total(Double  price_total){
        this.price_total = price_total ;
        this.price_totalDirtyFlag = true ;
    }

    /**
     * 获取 [PRICE_TOTAL]脏标记
     */
    @JsonIgnore
    public boolean getPrice_totalDirtyFlag(){
        return price_totalDirtyFlag ;
    }

    /**
     * 获取 [NAME]
     */
    @JsonProperty("name")
    public String getName(){
        return name ;
    }

    /**
     * 设置 [NAME]
     */
    @JsonProperty("name")
    public void setName(String  name){
        this.name = name ;
        this.nameDirtyFlag = true ;
    }

    /**
     * 获取 [NAME]脏标记
     */
    @JsonIgnore
    public boolean getNameDirtyFlag(){
        return nameDirtyFlag ;
    }

    /**
     * 获取 [PRODUCT_QTY]
     */
    @JsonProperty("product_qty")
    public Double getProduct_qty(){
        return product_qty ;
    }

    /**
     * 设置 [PRODUCT_QTY]
     */
    @JsonProperty("product_qty")
    public void setProduct_qty(Double  product_qty){
        this.product_qty = product_qty ;
        this.product_qtyDirtyFlag = true ;
    }

    /**
     * 获取 [PRODUCT_QTY]脏标记
     */
    @JsonIgnore
    public boolean getProduct_qtyDirtyFlag(){
        return product_qtyDirtyFlag ;
    }

    /**
     * 获取 [PRICE_TAX]
     */
    @JsonProperty("price_tax")
    public Double getPrice_tax(){
        return price_tax ;
    }

    /**
     * 设置 [PRICE_TAX]
     */
    @JsonProperty("price_tax")
    public void setPrice_tax(Double  price_tax){
        this.price_tax = price_tax ;
        this.price_taxDirtyFlag = true ;
    }

    /**
     * 获取 [PRICE_TAX]脏标记
     */
    @JsonIgnore
    public boolean getPrice_taxDirtyFlag(){
        return price_taxDirtyFlag ;
    }

    /**
     * 获取 [MOVE_IDS]
     */
    @JsonProperty("move_ids")
    public String getMove_ids(){
        return move_ids ;
    }

    /**
     * 设置 [MOVE_IDS]
     */
    @JsonProperty("move_ids")
    public void setMove_ids(String  move_ids){
        this.move_ids = move_ids ;
        this.move_idsDirtyFlag = true ;
    }

    /**
     * 获取 [MOVE_IDS]脏标记
     */
    @JsonIgnore
    public boolean getMove_idsDirtyFlag(){
        return move_idsDirtyFlag ;
    }

    /**
     * 获取 [PRICE_SUBTOTAL]
     */
    @JsonProperty("price_subtotal")
    public Double getPrice_subtotal(){
        return price_subtotal ;
    }

    /**
     * 设置 [PRICE_SUBTOTAL]
     */
    @JsonProperty("price_subtotal")
    public void setPrice_subtotal(Double  price_subtotal){
        this.price_subtotal = price_subtotal ;
        this.price_subtotalDirtyFlag = true ;
    }

    /**
     * 获取 [PRICE_SUBTOTAL]脏标记
     */
    @JsonIgnore
    public boolean getPrice_subtotalDirtyFlag(){
        return price_subtotalDirtyFlag ;
    }

    /**
     * 获取 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return __last_update ;
    }

    /**
     * 设置 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

    /**
     * 获取 [__LAST_UPDATE]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return __last_updateDirtyFlag ;
    }

    /**
     * 获取 [SEQUENCE]
     */
    @JsonProperty("sequence")
    public Integer getSequence(){
        return sequence ;
    }

    /**
     * 设置 [SEQUENCE]
     */
    @JsonProperty("sequence")
    public void setSequence(Integer  sequence){
        this.sequence = sequence ;
        this.sequenceDirtyFlag = true ;
    }

    /**
     * 获取 [SEQUENCE]脏标记
     */
    @JsonIgnore
    public boolean getSequenceDirtyFlag(){
        return sequenceDirtyFlag ;
    }

    /**
     * 获取 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return display_name ;
    }

    /**
     * 设置 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

    /**
     * 获取 [DISPLAY_NAME]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return display_nameDirtyFlag ;
    }

    /**
     * 获取 [PRODUCT_UOM_QTY]
     */
    @JsonProperty("product_uom_qty")
    public Double getProduct_uom_qty(){
        return product_uom_qty ;
    }

    /**
     * 设置 [PRODUCT_UOM_QTY]
     */
    @JsonProperty("product_uom_qty")
    public void setProduct_uom_qty(Double  product_uom_qty){
        this.product_uom_qty = product_uom_qty ;
        this.product_uom_qtyDirtyFlag = true ;
    }

    /**
     * 获取 [PRODUCT_UOM_QTY]脏标记
     */
    @JsonIgnore
    public boolean getProduct_uom_qtyDirtyFlag(){
        return product_uom_qtyDirtyFlag ;
    }

    /**
     * 获取 [TAXES_ID]
     */
    @JsonProperty("taxes_id")
    public String getTaxes_id(){
        return taxes_id ;
    }

    /**
     * 设置 [TAXES_ID]
     */
    @JsonProperty("taxes_id")
    public void setTaxes_id(String  taxes_id){
        this.taxes_id = taxes_id ;
        this.taxes_idDirtyFlag = true ;
    }

    /**
     * 获取 [TAXES_ID]脏标记
     */
    @JsonIgnore
    public boolean getTaxes_idDirtyFlag(){
        return taxes_idDirtyFlag ;
    }

    /**
     * 获取 [INVOICE_LINES]
     */
    @JsonProperty("invoice_lines")
    public String getInvoice_lines(){
        return invoice_lines ;
    }

    /**
     * 设置 [INVOICE_LINES]
     */
    @JsonProperty("invoice_lines")
    public void setInvoice_lines(String  invoice_lines){
        this.invoice_lines = invoice_lines ;
        this.invoice_linesDirtyFlag = true ;
    }

    /**
     * 获取 [INVOICE_LINES]脏标记
     */
    @JsonIgnore
    public boolean getInvoice_linesDirtyFlag(){
        return invoice_linesDirtyFlag ;
    }

    /**
     * 获取 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return create_date ;
    }

    /**
     * 设置 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return create_dateDirtyFlag ;
    }

    /**
     * 获取 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return write_date ;
    }

    /**
     * 设置 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return write_dateDirtyFlag ;
    }

    /**
     * 获取 [DATE_PLANNED]
     */
    @JsonProperty("date_planned")
    public Timestamp getDate_planned(){
        return date_planned ;
    }

    /**
     * 设置 [DATE_PLANNED]
     */
    @JsonProperty("date_planned")
    public void setDate_planned(Timestamp  date_planned){
        this.date_planned = date_planned ;
        this.date_plannedDirtyFlag = true ;
    }

    /**
     * 获取 [DATE_PLANNED]脏标记
     */
    @JsonIgnore
    public boolean getDate_plannedDirtyFlag(){
        return date_plannedDirtyFlag ;
    }

    /**
     * 获取 [QTY_INVOICED]
     */
    @JsonProperty("qty_invoiced")
    public Double getQty_invoiced(){
        return qty_invoiced ;
    }

    /**
     * 设置 [QTY_INVOICED]
     */
    @JsonProperty("qty_invoiced")
    public void setQty_invoiced(Double  qty_invoiced){
        this.qty_invoiced = qty_invoiced ;
        this.qty_invoicedDirtyFlag = true ;
    }

    /**
     * 获取 [QTY_INVOICED]脏标记
     */
    @JsonIgnore
    public boolean getQty_invoicedDirtyFlag(){
        return qty_invoicedDirtyFlag ;
    }

    /**
     * 获取 [ANALYTIC_TAG_IDS]
     */
    @JsonProperty("analytic_tag_ids")
    public String getAnalytic_tag_ids(){
        return analytic_tag_ids ;
    }

    /**
     * 设置 [ANALYTIC_TAG_IDS]
     */
    @JsonProperty("analytic_tag_ids")
    public void setAnalytic_tag_ids(String  analytic_tag_ids){
        this.analytic_tag_ids = analytic_tag_ids ;
        this.analytic_tag_idsDirtyFlag = true ;
    }

    /**
     * 获取 [ANALYTIC_TAG_IDS]脏标记
     */
    @JsonIgnore
    public boolean getAnalytic_tag_idsDirtyFlag(){
        return analytic_tag_idsDirtyFlag ;
    }

    /**
     * 获取 [MOVE_DEST_IDS]
     */
    @JsonProperty("move_dest_ids")
    public String getMove_dest_ids(){
        return move_dest_ids ;
    }

    /**
     * 设置 [MOVE_DEST_IDS]
     */
    @JsonProperty("move_dest_ids")
    public void setMove_dest_ids(String  move_dest_ids){
        this.move_dest_ids = move_dest_ids ;
        this.move_dest_idsDirtyFlag = true ;
    }

    /**
     * 获取 [MOVE_DEST_IDS]脏标记
     */
    @JsonIgnore
    public boolean getMove_dest_idsDirtyFlag(){
        return move_dest_idsDirtyFlag ;
    }

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

    /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return idDirtyFlag ;
    }

    /**
     * 获取 [PARTNER_ID_TEXT]
     */
    @JsonProperty("partner_id_text")
    public String getPartner_id_text(){
        return partner_id_text ;
    }

    /**
     * 设置 [PARTNER_ID_TEXT]
     */
    @JsonProperty("partner_id_text")
    public void setPartner_id_text(String  partner_id_text){
        this.partner_id_text = partner_id_text ;
        this.partner_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [PARTNER_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getPartner_id_textDirtyFlag(){
        return partner_id_textDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return create_uid_text ;
    }

    /**
     * 设置 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return create_uid_textDirtyFlag ;
    }

    /**
     * 获取 [ORDERPOINT_ID_TEXT]
     */
    @JsonProperty("orderpoint_id_text")
    public String getOrderpoint_id_text(){
        return orderpoint_id_text ;
    }

    /**
     * 设置 [ORDERPOINT_ID_TEXT]
     */
    @JsonProperty("orderpoint_id_text")
    public void setOrderpoint_id_text(String  orderpoint_id_text){
        this.orderpoint_id_text = orderpoint_id_text ;
        this.orderpoint_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [ORDERPOINT_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getOrderpoint_id_textDirtyFlag(){
        return orderpoint_id_textDirtyFlag ;
    }

    /**
     * 获取 [SALE_ORDER_ID_TEXT]
     */
    @JsonProperty("sale_order_id_text")
    public String getSale_order_id_text(){
        return sale_order_id_text ;
    }

    /**
     * 设置 [SALE_ORDER_ID_TEXT]
     */
    @JsonProperty("sale_order_id_text")
    public void setSale_order_id_text(String  sale_order_id_text){
        this.sale_order_id_text = sale_order_id_text ;
        this.sale_order_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [SALE_ORDER_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getSale_order_id_textDirtyFlag(){
        return sale_order_id_textDirtyFlag ;
    }

    /**
     * 获取 [PRODUCT_ID_TEXT]
     */
    @JsonProperty("product_id_text")
    public String getProduct_id_text(){
        return product_id_text ;
    }

    /**
     * 设置 [PRODUCT_ID_TEXT]
     */
    @JsonProperty("product_id_text")
    public void setProduct_id_text(String  product_id_text){
        this.product_id_text = product_id_text ;
        this.product_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [PRODUCT_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getProduct_id_textDirtyFlag(){
        return product_id_textDirtyFlag ;
    }

    /**
     * 获取 [PRODUCT_TYPE]
     */
    @JsonProperty("product_type")
    public String getProduct_type(){
        return product_type ;
    }

    /**
     * 设置 [PRODUCT_TYPE]
     */
    @JsonProperty("product_type")
    public void setProduct_type(String  product_type){
        this.product_type = product_type ;
        this.product_typeDirtyFlag = true ;
    }

    /**
     * 获取 [PRODUCT_TYPE]脏标记
     */
    @JsonIgnore
    public boolean getProduct_typeDirtyFlag(){
        return product_typeDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return write_uid_text ;
    }

    /**
     * 设置 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return write_uid_textDirtyFlag ;
    }

    /**
     * 获取 [COMPANY_ID_TEXT]
     */
    @JsonProperty("company_id_text")
    public String getCompany_id_text(){
        return company_id_text ;
    }

    /**
     * 设置 [COMPANY_ID_TEXT]
     */
    @JsonProperty("company_id_text")
    public void setCompany_id_text(String  company_id_text){
        this.company_id_text = company_id_text ;
        this.company_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [COMPANY_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getCompany_id_textDirtyFlag(){
        return company_id_textDirtyFlag ;
    }

    /**
     * 获取 [STATE]
     */
    @JsonProperty("state")
    public String getState(){
        return state ;
    }

    /**
     * 设置 [STATE]
     */
    @JsonProperty("state")
    public void setState(String  state){
        this.state = state ;
        this.stateDirtyFlag = true ;
    }

    /**
     * 获取 [STATE]脏标记
     */
    @JsonIgnore
    public boolean getStateDirtyFlag(){
        return stateDirtyFlag ;
    }

    /**
     * 获取 [ACCOUNT_ANALYTIC_ID_TEXT]
     */
    @JsonProperty("account_analytic_id_text")
    public String getAccount_analytic_id_text(){
        return account_analytic_id_text ;
    }

    /**
     * 设置 [ACCOUNT_ANALYTIC_ID_TEXT]
     */
    @JsonProperty("account_analytic_id_text")
    public void setAccount_analytic_id_text(String  account_analytic_id_text){
        this.account_analytic_id_text = account_analytic_id_text ;
        this.account_analytic_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [ACCOUNT_ANALYTIC_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getAccount_analytic_id_textDirtyFlag(){
        return account_analytic_id_textDirtyFlag ;
    }

    /**
     * 获取 [PRODUCT_IMAGE]
     */
    @JsonProperty("product_image")
    public byte[] getProduct_image(){
        return product_image ;
    }

    /**
     * 设置 [PRODUCT_IMAGE]
     */
    @JsonProperty("product_image")
    public void setProduct_image(byte[]  product_image){
        this.product_image = product_image ;
        this.product_imageDirtyFlag = true ;
    }

    /**
     * 获取 [PRODUCT_IMAGE]脏标记
     */
    @JsonIgnore
    public boolean getProduct_imageDirtyFlag(){
        return product_imageDirtyFlag ;
    }

    /**
     * 获取 [PRODUCT_UOM_TEXT]
     */
    @JsonProperty("product_uom_text")
    public String getProduct_uom_text(){
        return product_uom_text ;
    }

    /**
     * 设置 [PRODUCT_UOM_TEXT]
     */
    @JsonProperty("product_uom_text")
    public void setProduct_uom_text(String  product_uom_text){
        this.product_uom_text = product_uom_text ;
        this.product_uom_textDirtyFlag = true ;
    }

    /**
     * 获取 [PRODUCT_UOM_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getProduct_uom_textDirtyFlag(){
        return product_uom_textDirtyFlag ;
    }

    /**
     * 获取 [ORDER_ID_TEXT]
     */
    @JsonProperty("order_id_text")
    public String getOrder_id_text(){
        return order_id_text ;
    }

    /**
     * 设置 [ORDER_ID_TEXT]
     */
    @JsonProperty("order_id_text")
    public void setOrder_id_text(String  order_id_text){
        this.order_id_text = order_id_text ;
        this.order_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [ORDER_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getOrder_id_textDirtyFlag(){
        return order_id_textDirtyFlag ;
    }

    /**
     * 获取 [CURRENCY_ID_TEXT]
     */
    @JsonProperty("currency_id_text")
    public String getCurrency_id_text(){
        return currency_id_text ;
    }

    /**
     * 设置 [CURRENCY_ID_TEXT]
     */
    @JsonProperty("currency_id_text")
    public void setCurrency_id_text(String  currency_id_text){
        this.currency_id_text = currency_id_text ;
        this.currency_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [CURRENCY_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getCurrency_id_textDirtyFlag(){
        return currency_id_textDirtyFlag ;
    }

    /**
     * 获取 [SALE_LINE_ID_TEXT]
     */
    @JsonProperty("sale_line_id_text")
    public String getSale_line_id_text(){
        return sale_line_id_text ;
    }

    /**
     * 设置 [SALE_LINE_ID_TEXT]
     */
    @JsonProperty("sale_line_id_text")
    public void setSale_line_id_text(String  sale_line_id_text){
        this.sale_line_id_text = sale_line_id_text ;
        this.sale_line_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [SALE_LINE_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getSale_line_id_textDirtyFlag(){
        return sale_line_id_textDirtyFlag ;
    }

    /**
     * 获取 [DATE_ORDER]
     */
    @JsonProperty("date_order")
    public Timestamp getDate_order(){
        return date_order ;
    }

    /**
     * 设置 [DATE_ORDER]
     */
    @JsonProperty("date_order")
    public void setDate_order(Timestamp  date_order){
        this.date_order = date_order ;
        this.date_orderDirtyFlag = true ;
    }

    /**
     * 获取 [DATE_ORDER]脏标记
     */
    @JsonIgnore
    public boolean getDate_orderDirtyFlag(){
        return date_orderDirtyFlag ;
    }

    /**
     * 获取 [COMPANY_ID]
     */
    @JsonProperty("company_id")
    public Integer getCompany_id(){
        return company_id ;
    }

    /**
     * 设置 [COMPANY_ID]
     */
    @JsonProperty("company_id")
    public void setCompany_id(Integer  company_id){
        this.company_id = company_id ;
        this.company_idDirtyFlag = true ;
    }

    /**
     * 获取 [COMPANY_ID]脏标记
     */
    @JsonIgnore
    public boolean getCompany_idDirtyFlag(){
        return company_idDirtyFlag ;
    }

    /**
     * 获取 [ACCOUNT_ANALYTIC_ID]
     */
    @JsonProperty("account_analytic_id")
    public Integer getAccount_analytic_id(){
        return account_analytic_id ;
    }

    /**
     * 设置 [ACCOUNT_ANALYTIC_ID]
     */
    @JsonProperty("account_analytic_id")
    public void setAccount_analytic_id(Integer  account_analytic_id){
        this.account_analytic_id = account_analytic_id ;
        this.account_analytic_idDirtyFlag = true ;
    }

    /**
     * 获取 [ACCOUNT_ANALYTIC_ID]脏标记
     */
    @JsonIgnore
    public boolean getAccount_analytic_idDirtyFlag(){
        return account_analytic_idDirtyFlag ;
    }

    /**
     * 获取 [CURRENCY_ID]
     */
    @JsonProperty("currency_id")
    public Integer getCurrency_id(){
        return currency_id ;
    }

    /**
     * 设置 [CURRENCY_ID]
     */
    @JsonProperty("currency_id")
    public void setCurrency_id(Integer  currency_id){
        this.currency_id = currency_id ;
        this.currency_idDirtyFlag = true ;
    }

    /**
     * 获取 [CURRENCY_ID]脏标记
     */
    @JsonIgnore
    public boolean getCurrency_idDirtyFlag(){
        return currency_idDirtyFlag ;
    }

    /**
     * 获取 [SALE_ORDER_ID]
     */
    @JsonProperty("sale_order_id")
    public Integer getSale_order_id(){
        return sale_order_id ;
    }

    /**
     * 设置 [SALE_ORDER_ID]
     */
    @JsonProperty("sale_order_id")
    public void setSale_order_id(Integer  sale_order_id){
        this.sale_order_id = sale_order_id ;
        this.sale_order_idDirtyFlag = true ;
    }

    /**
     * 获取 [SALE_ORDER_ID]脏标记
     */
    @JsonIgnore
    public boolean getSale_order_idDirtyFlag(){
        return sale_order_idDirtyFlag ;
    }

    /**
     * 获取 [PRODUCT_UOM]
     */
    @JsonProperty("product_uom")
    public Integer getProduct_uom(){
        return product_uom ;
    }

    /**
     * 设置 [PRODUCT_UOM]
     */
    @JsonProperty("product_uom")
    public void setProduct_uom(Integer  product_uom){
        this.product_uom = product_uom ;
        this.product_uomDirtyFlag = true ;
    }

    /**
     * 获取 [PRODUCT_UOM]脏标记
     */
    @JsonIgnore
    public boolean getProduct_uomDirtyFlag(){
        return product_uomDirtyFlag ;
    }

    /**
     * 获取 [SALE_LINE_ID]
     */
    @JsonProperty("sale_line_id")
    public Integer getSale_line_id(){
        return sale_line_id ;
    }

    /**
     * 设置 [SALE_LINE_ID]
     */
    @JsonProperty("sale_line_id")
    public void setSale_line_id(Integer  sale_line_id){
        this.sale_line_id = sale_line_id ;
        this.sale_line_idDirtyFlag = true ;
    }

    /**
     * 获取 [SALE_LINE_ID]脏标记
     */
    @JsonIgnore
    public boolean getSale_line_idDirtyFlag(){
        return sale_line_idDirtyFlag ;
    }

    /**
     * 获取 [ORDER_ID]
     */
    @JsonProperty("order_id")
    public Integer getOrder_id(){
        return order_id ;
    }

    /**
     * 设置 [ORDER_ID]
     */
    @JsonProperty("order_id")
    public void setOrder_id(Integer  order_id){
        this.order_id = order_id ;
        this.order_idDirtyFlag = true ;
    }

    /**
     * 获取 [ORDER_ID]脏标记
     */
    @JsonIgnore
    public boolean getOrder_idDirtyFlag(){
        return order_idDirtyFlag ;
    }

    /**
     * 获取 [ORDERPOINT_ID]
     */
    @JsonProperty("orderpoint_id")
    public Integer getOrderpoint_id(){
        return orderpoint_id ;
    }

    /**
     * 设置 [ORDERPOINT_ID]
     */
    @JsonProperty("orderpoint_id")
    public void setOrderpoint_id(Integer  orderpoint_id){
        this.orderpoint_id = orderpoint_id ;
        this.orderpoint_idDirtyFlag = true ;
    }

    /**
     * 获取 [ORDERPOINT_ID]脏标记
     */
    @JsonIgnore
    public boolean getOrderpoint_idDirtyFlag(){
        return orderpoint_idDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return create_uid ;
    }

    /**
     * 设置 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return create_uidDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return write_uid ;
    }

    /**
     * 设置 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return write_uidDirtyFlag ;
    }

    /**
     * 获取 [PRODUCT_ID]
     */
    @JsonProperty("product_id")
    public Integer getProduct_id(){
        return product_id ;
    }

    /**
     * 设置 [PRODUCT_ID]
     */
    @JsonProperty("product_id")
    public void setProduct_id(Integer  product_id){
        this.product_id = product_id ;
        this.product_idDirtyFlag = true ;
    }

    /**
     * 获取 [PRODUCT_ID]脏标记
     */
    @JsonIgnore
    public boolean getProduct_idDirtyFlag(){
        return product_idDirtyFlag ;
    }

    /**
     * 获取 [PARTNER_ID]
     */
    @JsonProperty("partner_id")
    public Integer getPartner_id(){
        return partner_id ;
    }

    /**
     * 设置 [PARTNER_ID]
     */
    @JsonProperty("partner_id")
    public void setPartner_id(Integer  partner_id){
        this.partner_id = partner_id ;
        this.partner_idDirtyFlag = true ;
    }

    /**
     * 获取 [PARTNER_ID]脏标记
     */
    @JsonIgnore
    public boolean getPartner_idDirtyFlag(){
        return partner_idDirtyFlag ;
    }



    public Purchase_order_line toDO() {
        Purchase_order_line srfdomain = new Purchase_order_line();
        if(getPrice_unitDirtyFlag())
            srfdomain.setPrice_unit(price_unit);
        if(getQty_receivedDirtyFlag())
            srfdomain.setQty_received(qty_received);
        if(getPrice_totalDirtyFlag())
            srfdomain.setPrice_total(price_total);
        if(getNameDirtyFlag())
            srfdomain.setName(name);
        if(getProduct_qtyDirtyFlag())
            srfdomain.setProduct_qty(product_qty);
        if(getPrice_taxDirtyFlag())
            srfdomain.setPrice_tax(price_tax);
        if(getMove_idsDirtyFlag())
            srfdomain.setMove_ids(move_ids);
        if(getPrice_subtotalDirtyFlag())
            srfdomain.setPrice_subtotal(price_subtotal);
        if(get__last_updateDirtyFlag())
            srfdomain.set__last_update(__last_update);
        if(getSequenceDirtyFlag())
            srfdomain.setSequence(sequence);
        if(getDisplay_nameDirtyFlag())
            srfdomain.setDisplay_name(display_name);
        if(getProduct_uom_qtyDirtyFlag())
            srfdomain.setProduct_uom_qty(product_uom_qty);
        if(getTaxes_idDirtyFlag())
            srfdomain.setTaxes_id(taxes_id);
        if(getInvoice_linesDirtyFlag())
            srfdomain.setInvoice_lines(invoice_lines);
        if(getCreate_dateDirtyFlag())
            srfdomain.setCreate_date(create_date);
        if(getWrite_dateDirtyFlag())
            srfdomain.setWrite_date(write_date);
        if(getDate_plannedDirtyFlag())
            srfdomain.setDate_planned(date_planned);
        if(getQty_invoicedDirtyFlag())
            srfdomain.setQty_invoiced(qty_invoiced);
        if(getAnalytic_tag_idsDirtyFlag())
            srfdomain.setAnalytic_tag_ids(analytic_tag_ids);
        if(getMove_dest_idsDirtyFlag())
            srfdomain.setMove_dest_ids(move_dest_ids);
        if(getIdDirtyFlag())
            srfdomain.setId(id);
        if(getPartner_id_textDirtyFlag())
            srfdomain.setPartner_id_text(partner_id_text);
        if(getCreate_uid_textDirtyFlag())
            srfdomain.setCreate_uid_text(create_uid_text);
        if(getOrderpoint_id_textDirtyFlag())
            srfdomain.setOrderpoint_id_text(orderpoint_id_text);
        if(getSale_order_id_textDirtyFlag())
            srfdomain.setSale_order_id_text(sale_order_id_text);
        if(getProduct_id_textDirtyFlag())
            srfdomain.setProduct_id_text(product_id_text);
        if(getProduct_typeDirtyFlag())
            srfdomain.setProduct_type(product_type);
        if(getWrite_uid_textDirtyFlag())
            srfdomain.setWrite_uid_text(write_uid_text);
        if(getCompany_id_textDirtyFlag())
            srfdomain.setCompany_id_text(company_id_text);
        if(getStateDirtyFlag())
            srfdomain.setState(state);
        if(getAccount_analytic_id_textDirtyFlag())
            srfdomain.setAccount_analytic_id_text(account_analytic_id_text);
        if(getProduct_imageDirtyFlag())
            srfdomain.setProduct_image(product_image);
        if(getProduct_uom_textDirtyFlag())
            srfdomain.setProduct_uom_text(product_uom_text);
        if(getOrder_id_textDirtyFlag())
            srfdomain.setOrder_id_text(order_id_text);
        if(getCurrency_id_textDirtyFlag())
            srfdomain.setCurrency_id_text(currency_id_text);
        if(getSale_line_id_textDirtyFlag())
            srfdomain.setSale_line_id_text(sale_line_id_text);
        if(getDate_orderDirtyFlag())
            srfdomain.setDate_order(date_order);
        if(getCompany_idDirtyFlag())
            srfdomain.setCompany_id(company_id);
        if(getAccount_analytic_idDirtyFlag())
            srfdomain.setAccount_analytic_id(account_analytic_id);
        if(getCurrency_idDirtyFlag())
            srfdomain.setCurrency_id(currency_id);
        if(getSale_order_idDirtyFlag())
            srfdomain.setSale_order_id(sale_order_id);
        if(getProduct_uomDirtyFlag())
            srfdomain.setProduct_uom(product_uom);
        if(getSale_line_idDirtyFlag())
            srfdomain.setSale_line_id(sale_line_id);
        if(getOrder_idDirtyFlag())
            srfdomain.setOrder_id(order_id);
        if(getOrderpoint_idDirtyFlag())
            srfdomain.setOrderpoint_id(orderpoint_id);
        if(getCreate_uidDirtyFlag())
            srfdomain.setCreate_uid(create_uid);
        if(getWrite_uidDirtyFlag())
            srfdomain.setWrite_uid(write_uid);
        if(getProduct_idDirtyFlag())
            srfdomain.setProduct_id(product_id);
        if(getPartner_idDirtyFlag())
            srfdomain.setPartner_id(partner_id);

        return srfdomain;
    }

    public void fromDO(Purchase_order_line srfdomain) {
        if(srfdomain == null )
          return ;
        if(srfdomain.getPrice_unitDirtyFlag())
            this.setPrice_unit(srfdomain.getPrice_unit());
        if(srfdomain.getQty_receivedDirtyFlag())
            this.setQty_received(srfdomain.getQty_received());
        if(srfdomain.getPrice_totalDirtyFlag())
            this.setPrice_total(srfdomain.getPrice_total());
        if(srfdomain.getNameDirtyFlag())
            this.setName(srfdomain.getName());
        if(srfdomain.getProduct_qtyDirtyFlag())
            this.setProduct_qty(srfdomain.getProduct_qty());
        if(srfdomain.getPrice_taxDirtyFlag())
            this.setPrice_tax(srfdomain.getPrice_tax());
        if(srfdomain.getMove_idsDirtyFlag())
            this.setMove_ids(srfdomain.getMove_ids());
        if(srfdomain.getPrice_subtotalDirtyFlag())
            this.setPrice_subtotal(srfdomain.getPrice_subtotal());
        if(srfdomain.get__last_updateDirtyFlag())
            this.set__last_update(srfdomain.get__last_update());
        if(srfdomain.getSequenceDirtyFlag())
            this.setSequence(srfdomain.getSequence());
        if(srfdomain.getDisplay_nameDirtyFlag())
            this.setDisplay_name(srfdomain.getDisplay_name());
        if(srfdomain.getProduct_uom_qtyDirtyFlag())
            this.setProduct_uom_qty(srfdomain.getProduct_uom_qty());
        if(srfdomain.getTaxes_idDirtyFlag())
            this.setTaxes_id(srfdomain.getTaxes_id());
        if(srfdomain.getInvoice_linesDirtyFlag())
            this.setInvoice_lines(srfdomain.getInvoice_lines());
        if(srfdomain.getCreate_dateDirtyFlag())
            this.setCreate_date(srfdomain.getCreate_date());
        if(srfdomain.getWrite_dateDirtyFlag())
            this.setWrite_date(srfdomain.getWrite_date());
        if(srfdomain.getDate_plannedDirtyFlag())
            this.setDate_planned(srfdomain.getDate_planned());
        if(srfdomain.getQty_invoicedDirtyFlag())
            this.setQty_invoiced(srfdomain.getQty_invoiced());
        if(srfdomain.getAnalytic_tag_idsDirtyFlag())
            this.setAnalytic_tag_ids(srfdomain.getAnalytic_tag_ids());
        if(srfdomain.getMove_dest_idsDirtyFlag())
            this.setMove_dest_ids(srfdomain.getMove_dest_ids());
        if(srfdomain.getIdDirtyFlag())
            this.setId(srfdomain.getId());
        if(srfdomain.getPartner_id_textDirtyFlag())
            this.setPartner_id_text(srfdomain.getPartner_id_text());
        if(srfdomain.getCreate_uid_textDirtyFlag())
            this.setCreate_uid_text(srfdomain.getCreate_uid_text());
        if(srfdomain.getOrderpoint_id_textDirtyFlag())
            this.setOrderpoint_id_text(srfdomain.getOrderpoint_id_text());
        if(srfdomain.getSale_order_id_textDirtyFlag())
            this.setSale_order_id_text(srfdomain.getSale_order_id_text());
        if(srfdomain.getProduct_id_textDirtyFlag())
            this.setProduct_id_text(srfdomain.getProduct_id_text());
        if(srfdomain.getProduct_typeDirtyFlag())
            this.setProduct_type(srfdomain.getProduct_type());
        if(srfdomain.getWrite_uid_textDirtyFlag())
            this.setWrite_uid_text(srfdomain.getWrite_uid_text());
        if(srfdomain.getCompany_id_textDirtyFlag())
            this.setCompany_id_text(srfdomain.getCompany_id_text());
        if(srfdomain.getStateDirtyFlag())
            this.setState(srfdomain.getState());
        if(srfdomain.getAccount_analytic_id_textDirtyFlag())
            this.setAccount_analytic_id_text(srfdomain.getAccount_analytic_id_text());
        if(srfdomain.getProduct_imageDirtyFlag())
            this.setProduct_image(srfdomain.getProduct_image());
        if(srfdomain.getProduct_uom_textDirtyFlag())
            this.setProduct_uom_text(srfdomain.getProduct_uom_text());
        if(srfdomain.getOrder_id_textDirtyFlag())
            this.setOrder_id_text(srfdomain.getOrder_id_text());
        if(srfdomain.getCurrency_id_textDirtyFlag())
            this.setCurrency_id_text(srfdomain.getCurrency_id_text());
        if(srfdomain.getSale_line_id_textDirtyFlag())
            this.setSale_line_id_text(srfdomain.getSale_line_id_text());
        if(srfdomain.getDate_orderDirtyFlag())
            this.setDate_order(srfdomain.getDate_order());
        if(srfdomain.getCompany_idDirtyFlag())
            this.setCompany_id(srfdomain.getCompany_id());
        if(srfdomain.getAccount_analytic_idDirtyFlag())
            this.setAccount_analytic_id(srfdomain.getAccount_analytic_id());
        if(srfdomain.getCurrency_idDirtyFlag())
            this.setCurrency_id(srfdomain.getCurrency_id());
        if(srfdomain.getSale_order_idDirtyFlag())
            this.setSale_order_id(srfdomain.getSale_order_id());
        if(srfdomain.getProduct_uomDirtyFlag())
            this.setProduct_uom(srfdomain.getProduct_uom());
        if(srfdomain.getSale_line_idDirtyFlag())
            this.setSale_line_id(srfdomain.getSale_line_id());
        if(srfdomain.getOrder_idDirtyFlag())
            this.setOrder_id(srfdomain.getOrder_id());
        if(srfdomain.getOrderpoint_idDirtyFlag())
            this.setOrderpoint_id(srfdomain.getOrderpoint_id());
        if(srfdomain.getCreate_uidDirtyFlag())
            this.setCreate_uid(srfdomain.getCreate_uid());
        if(srfdomain.getWrite_uidDirtyFlag())
            this.setWrite_uid(srfdomain.getWrite_uid());
        if(srfdomain.getProduct_idDirtyFlag())
            this.setProduct_id(srfdomain.getProduct_id());
        if(srfdomain.getPartner_idDirtyFlag())
            this.setPartner_id(srfdomain.getPartner_id());

    }

    public List<Purchase_order_lineDTO> fromDOPage(List<Purchase_order_line> poPage)   {
        if(poPage == null)
            return null;
        List<Purchase_order_lineDTO> dtos=new ArrayList<Purchase_order_lineDTO>();
        for(Purchase_order_line domain : poPage) {
            Purchase_order_lineDTO dto = new Purchase_order_lineDTO();
            dto.fromDO(domain);
            dtos.add(dto);
        }
        return dtos;
    }
}

