package cn.ibizlab.odoo.service.odoo_purchase.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import lombok.Data;

@ConfigurationProperties(prefix = "service.odoo.purchase")
@Data
public class odoo_purchaseServiceProperties {

	private boolean enabled;

	private boolean auth;


}