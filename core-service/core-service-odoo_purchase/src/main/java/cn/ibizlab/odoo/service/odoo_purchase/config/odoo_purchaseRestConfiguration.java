package cn.ibizlab.odoo.service.odoo_purchase.config;

import org.springframework.context.annotation.ComponentScan;

@ComponentScan("cn.ibizlab.odoo.service.odoo_purchase")
public class odoo_purchaseRestConfiguration {

}
