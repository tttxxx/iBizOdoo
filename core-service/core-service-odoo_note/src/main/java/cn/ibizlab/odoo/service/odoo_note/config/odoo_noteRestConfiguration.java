package cn.ibizlab.odoo.service.odoo_note.config;

import org.springframework.context.annotation.ComponentScan;

@ComponentScan("cn.ibizlab.odoo.service.odoo_note")
public class odoo_noteRestConfiguration {

}
