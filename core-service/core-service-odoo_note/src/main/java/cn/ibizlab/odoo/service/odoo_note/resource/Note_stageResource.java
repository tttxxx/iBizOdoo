package cn.ibizlab.odoo.service.odoo_note.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_note.dto.Note_stageDTO;
import cn.ibizlab.odoo.core.odoo_note.domain.Note_stage;
import cn.ibizlab.odoo.core.odoo_note.service.INote_stageService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_note.filter.Note_stageSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Note_stage" })
@RestController
@RequestMapping("")
public class Note_stageResource {

    @Autowired
    private INote_stageService note_stageService;

    public INote_stageService getNote_stageService() {
        return this.note_stageService;
    }

    @ApiOperation(value = "删除数据", tags = {"Note_stage" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_note/note_stages/{note_stage_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("note_stage_id") Integer note_stage_id) {
        Note_stageDTO note_stagedto = new Note_stageDTO();
		Note_stage domain = new Note_stage();
		note_stagedto.setId(note_stage_id);
		domain.setId(note_stage_id);
        Boolean rst = note_stageService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "建立数据", tags = {"Note_stage" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_note/note_stages")

    public ResponseEntity<Note_stageDTO> create(@RequestBody Note_stageDTO note_stagedto) {
        Note_stageDTO dto = new Note_stageDTO();
        Note_stage domain = note_stagedto.toDO();
		note_stageService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "更新数据", tags = {"Note_stage" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_note/note_stages/{note_stage_id}")

    public ResponseEntity<Note_stageDTO> update(@PathVariable("note_stage_id") Integer note_stage_id, @RequestBody Note_stageDTO note_stagedto) {
		Note_stage domain = note_stagedto.toDO();
        domain.setId(note_stage_id);
		note_stageService.update(domain);
		Note_stageDTO dto = new Note_stageDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取数据", tags = {"Note_stage" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_note/note_stages/{note_stage_id}")
    public ResponseEntity<Note_stageDTO> get(@PathVariable("note_stage_id") Integer note_stage_id) {
        Note_stageDTO dto = new Note_stageDTO();
        Note_stage domain = note_stageService.get(note_stage_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批建立数据", tags = {"Note_stage" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_note/note_stages/createBatch")
    public ResponseEntity<Boolean> createBatchNote_stage(@RequestBody List<Note_stageDTO> note_stagedtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "批更新数据", tags = {"Note_stage" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_note/note_stages/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Note_stageDTO> note_stagedtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "批删除数据", tags = {"Note_stage" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_note/note_stages/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Note_stageDTO> note_stagedtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Note_stage" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_note/note_stages/fetchdefault")
	public ResponseEntity<Page<Note_stageDTO>> fetchDefault(Note_stageSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Note_stageDTO> list = new ArrayList<Note_stageDTO>();
        
        Page<Note_stage> domains = note_stageService.searchDefault(context) ;
        for(Note_stage note_stage : domains.getContent()){
            Note_stageDTO dto = new Note_stageDTO();
            dto.fromDO(note_stage);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
