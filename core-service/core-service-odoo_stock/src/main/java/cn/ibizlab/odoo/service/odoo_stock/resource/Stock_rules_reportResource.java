package cn.ibizlab.odoo.service.odoo_stock.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_stock.dto.Stock_rules_reportDTO;
import cn.ibizlab.odoo.core.odoo_stock.domain.Stock_rules_report;
import cn.ibizlab.odoo.core.odoo_stock.service.IStock_rules_reportService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_stock.filter.Stock_rules_reportSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Stock_rules_report" })
@RestController
@RequestMapping("")
public class Stock_rules_reportResource {

    @Autowired
    private IStock_rules_reportService stock_rules_reportService;

    public IStock_rules_reportService getStock_rules_reportService() {
        return this.stock_rules_reportService;
    }

    @ApiOperation(value = "批删除数据", tags = {"Stock_rules_report" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_stock/stock_rules_reports/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Stock_rules_reportDTO> stock_rules_reportdtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "建立数据", tags = {"Stock_rules_report" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_stock/stock_rules_reports")

    public ResponseEntity<Stock_rules_reportDTO> create(@RequestBody Stock_rules_reportDTO stock_rules_reportdto) {
        Stock_rules_reportDTO dto = new Stock_rules_reportDTO();
        Stock_rules_report domain = stock_rules_reportdto.toDO();
		stock_rules_reportService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批建立数据", tags = {"Stock_rules_report" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_stock/stock_rules_reports/createBatch")
    public ResponseEntity<Boolean> createBatchStock_rules_report(@RequestBody List<Stock_rules_reportDTO> stock_rules_reportdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "获取数据", tags = {"Stock_rules_report" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_stock/stock_rules_reports/{stock_rules_report_id}")
    public ResponseEntity<Stock_rules_reportDTO> get(@PathVariable("stock_rules_report_id") Integer stock_rules_report_id) {
        Stock_rules_reportDTO dto = new Stock_rules_reportDTO();
        Stock_rules_report domain = stock_rules_reportService.get(stock_rules_report_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "删除数据", tags = {"Stock_rules_report" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_stock/stock_rules_reports/{stock_rules_report_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("stock_rules_report_id") Integer stock_rules_report_id) {
        Stock_rules_reportDTO stock_rules_reportdto = new Stock_rules_reportDTO();
		Stock_rules_report domain = new Stock_rules_report();
		stock_rules_reportdto.setId(stock_rules_report_id);
		domain.setId(stock_rules_report_id);
        Boolean rst = stock_rules_reportService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "更新数据", tags = {"Stock_rules_report" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_stock/stock_rules_reports/{stock_rules_report_id}")

    public ResponseEntity<Stock_rules_reportDTO> update(@PathVariable("stock_rules_report_id") Integer stock_rules_report_id, @RequestBody Stock_rules_reportDTO stock_rules_reportdto) {
		Stock_rules_report domain = stock_rules_reportdto.toDO();
        domain.setId(stock_rules_report_id);
		stock_rules_reportService.update(domain);
		Stock_rules_reportDTO dto = new Stock_rules_reportDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批更新数据", tags = {"Stock_rules_report" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_stock/stock_rules_reports/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Stock_rules_reportDTO> stock_rules_reportdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Stock_rules_report" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_stock/stock_rules_reports/fetchdefault")
	public ResponseEntity<Page<Stock_rules_reportDTO>> fetchDefault(Stock_rules_reportSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Stock_rules_reportDTO> list = new ArrayList<Stock_rules_reportDTO>();
        
        Page<Stock_rules_report> domains = stock_rules_reportService.searchDefault(context) ;
        for(Stock_rules_report stock_rules_report : domains.getContent()){
            Stock_rules_reportDTO dto = new Stock_rules_reportDTO();
            dto.fromDO(stock_rules_report);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
