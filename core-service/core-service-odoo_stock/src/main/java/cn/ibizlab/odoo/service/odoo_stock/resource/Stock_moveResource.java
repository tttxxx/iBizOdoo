package cn.ibizlab.odoo.service.odoo_stock.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_stock.dto.Stock_moveDTO;
import cn.ibizlab.odoo.core.odoo_stock.domain.Stock_move;
import cn.ibizlab.odoo.core.odoo_stock.service.IStock_moveService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_stock.filter.Stock_moveSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Stock_move" })
@RestController
@RequestMapping("")
public class Stock_moveResource {

    @Autowired
    private IStock_moveService stock_moveService;

    public IStock_moveService getStock_moveService() {
        return this.stock_moveService;
    }

    @ApiOperation(value = "批建立数据", tags = {"Stock_move" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_stock/stock_moves/createBatch")
    public ResponseEntity<Boolean> createBatchStock_move(@RequestBody List<Stock_moveDTO> stock_movedtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "获取数据", tags = {"Stock_move" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_stock/stock_moves/{stock_move_id}")
    public ResponseEntity<Stock_moveDTO> get(@PathVariable("stock_move_id") Integer stock_move_id) {
        Stock_moveDTO dto = new Stock_moveDTO();
        Stock_move domain = stock_moveService.get(stock_move_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "更新数据", tags = {"Stock_move" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_stock/stock_moves/{stock_move_id}")

    public ResponseEntity<Stock_moveDTO> update(@PathVariable("stock_move_id") Integer stock_move_id, @RequestBody Stock_moveDTO stock_movedto) {
		Stock_move domain = stock_movedto.toDO();
        domain.setId(stock_move_id);
		stock_moveService.update(domain);
		Stock_moveDTO dto = new Stock_moveDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "建立数据", tags = {"Stock_move" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_stock/stock_moves")

    public ResponseEntity<Stock_moveDTO> create(@RequestBody Stock_moveDTO stock_movedto) {
        Stock_moveDTO dto = new Stock_moveDTO();
        Stock_move domain = stock_movedto.toDO();
		stock_moveService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批删除数据", tags = {"Stock_move" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_stock/stock_moves/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Stock_moveDTO> stock_movedtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "删除数据", tags = {"Stock_move" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_stock/stock_moves/{stock_move_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("stock_move_id") Integer stock_move_id) {
        Stock_moveDTO stock_movedto = new Stock_moveDTO();
		Stock_move domain = new Stock_move();
		stock_movedto.setId(stock_move_id);
		domain.setId(stock_move_id);
        Boolean rst = stock_moveService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "批更新数据", tags = {"Stock_move" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_stock/stock_moves/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Stock_moveDTO> stock_movedtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Stock_move" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_stock/stock_moves/fetchdefault")
	public ResponseEntity<Page<Stock_moveDTO>> fetchDefault(Stock_moveSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Stock_moveDTO> list = new ArrayList<Stock_moveDTO>();
        
        Page<Stock_move> domains = stock_moveService.searchDefault(context) ;
        for(Stock_move stock_move : domains.getContent()){
            Stock_moveDTO dto = new Stock_moveDTO();
            dto.fromDO(stock_move);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
