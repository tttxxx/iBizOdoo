package cn.ibizlab.odoo.service.odoo_stock.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_stock.dto.Stock_pickingDTO;
import cn.ibizlab.odoo.core.odoo_stock.domain.Stock_picking;
import cn.ibizlab.odoo.core.odoo_stock.service.IStock_pickingService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_stock.filter.Stock_pickingSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Stock_picking" })
@RestController
@RequestMapping("")
public class Stock_pickingResource {

    @Autowired
    private IStock_pickingService stock_pickingService;

    public IStock_pickingService getStock_pickingService() {
        return this.stock_pickingService;
    }

    @ApiOperation(value = "获取数据", tags = {"Stock_picking" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_stock/stock_pickings/{stock_picking_id}")
    public ResponseEntity<Stock_pickingDTO> get(@PathVariable("stock_picking_id") Integer stock_picking_id) {
        Stock_pickingDTO dto = new Stock_pickingDTO();
        Stock_picking domain = stock_pickingService.get(stock_picking_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "删除数据", tags = {"Stock_picking" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_stock/stock_pickings/{stock_picking_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("stock_picking_id") Integer stock_picking_id) {
        Stock_pickingDTO stock_pickingdto = new Stock_pickingDTO();
		Stock_picking domain = new Stock_picking();
		stock_pickingdto.setId(stock_picking_id);
		domain.setId(stock_picking_id);
        Boolean rst = stock_pickingService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "批更新数据", tags = {"Stock_picking" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_stock/stock_pickings/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Stock_pickingDTO> stock_pickingdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "更新数据", tags = {"Stock_picking" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_stock/stock_pickings/{stock_picking_id}")

    public ResponseEntity<Stock_pickingDTO> update(@PathVariable("stock_picking_id") Integer stock_picking_id, @RequestBody Stock_pickingDTO stock_pickingdto) {
		Stock_picking domain = stock_pickingdto.toDO();
        domain.setId(stock_picking_id);
		stock_pickingService.update(domain);
		Stock_pickingDTO dto = new Stock_pickingDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批建立数据", tags = {"Stock_picking" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_stock/stock_pickings/createBatch")
    public ResponseEntity<Boolean> createBatchStock_picking(@RequestBody List<Stock_pickingDTO> stock_pickingdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "批删除数据", tags = {"Stock_picking" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_stock/stock_pickings/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Stock_pickingDTO> stock_pickingdtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "建立数据", tags = {"Stock_picking" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_stock/stock_pickings")

    public ResponseEntity<Stock_pickingDTO> create(@RequestBody Stock_pickingDTO stock_pickingdto) {
        Stock_pickingDTO dto = new Stock_pickingDTO();
        Stock_picking domain = stock_pickingdto.toDO();
		stock_pickingService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Stock_picking" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_stock/stock_pickings/fetchdefault")
	public ResponseEntity<Page<Stock_pickingDTO>> fetchDefault(Stock_pickingSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Stock_pickingDTO> list = new ArrayList<Stock_pickingDTO>();
        
        Page<Stock_picking> domains = stock_pickingService.searchDefault(context) ;
        for(Stock_picking stock_picking : domains.getContent()){
            Stock_pickingDTO dto = new Stock_pickingDTO();
            dto.fromDO(stock_picking);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
