package cn.ibizlab.odoo.service.odoo_stock.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_stock.dto.Stock_immediate_transferDTO;
import cn.ibizlab.odoo.core.odoo_stock.domain.Stock_immediate_transfer;
import cn.ibizlab.odoo.core.odoo_stock.service.IStock_immediate_transferService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_stock.filter.Stock_immediate_transferSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Stock_immediate_transfer" })
@RestController
@RequestMapping("")
public class Stock_immediate_transferResource {

    @Autowired
    private IStock_immediate_transferService stock_immediate_transferService;

    public IStock_immediate_transferService getStock_immediate_transferService() {
        return this.stock_immediate_transferService;
    }

    @ApiOperation(value = "更新数据", tags = {"Stock_immediate_transfer" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_stock/stock_immediate_transfers/{stock_immediate_transfer_id}")

    public ResponseEntity<Stock_immediate_transferDTO> update(@PathVariable("stock_immediate_transfer_id") Integer stock_immediate_transfer_id, @RequestBody Stock_immediate_transferDTO stock_immediate_transferdto) {
		Stock_immediate_transfer domain = stock_immediate_transferdto.toDO();
        domain.setId(stock_immediate_transfer_id);
		stock_immediate_transferService.update(domain);
		Stock_immediate_transferDTO dto = new Stock_immediate_transferDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取数据", tags = {"Stock_immediate_transfer" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_stock/stock_immediate_transfers/{stock_immediate_transfer_id}")
    public ResponseEntity<Stock_immediate_transferDTO> get(@PathVariable("stock_immediate_transfer_id") Integer stock_immediate_transfer_id) {
        Stock_immediate_transferDTO dto = new Stock_immediate_transferDTO();
        Stock_immediate_transfer domain = stock_immediate_transferService.get(stock_immediate_transfer_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批更新数据", tags = {"Stock_immediate_transfer" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_stock/stock_immediate_transfers/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Stock_immediate_transferDTO> stock_immediate_transferdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "批删除数据", tags = {"Stock_immediate_transfer" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_stock/stock_immediate_transfers/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Stock_immediate_transferDTO> stock_immediate_transferdtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "建立数据", tags = {"Stock_immediate_transfer" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_stock/stock_immediate_transfers")

    public ResponseEntity<Stock_immediate_transferDTO> create(@RequestBody Stock_immediate_transferDTO stock_immediate_transferdto) {
        Stock_immediate_transferDTO dto = new Stock_immediate_transferDTO();
        Stock_immediate_transfer domain = stock_immediate_transferdto.toDO();
		stock_immediate_transferService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批建立数据", tags = {"Stock_immediate_transfer" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_stock/stock_immediate_transfers/createBatch")
    public ResponseEntity<Boolean> createBatchStock_immediate_transfer(@RequestBody List<Stock_immediate_transferDTO> stock_immediate_transferdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "删除数据", tags = {"Stock_immediate_transfer" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_stock/stock_immediate_transfers/{stock_immediate_transfer_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("stock_immediate_transfer_id") Integer stock_immediate_transfer_id) {
        Stock_immediate_transferDTO stock_immediate_transferdto = new Stock_immediate_transferDTO();
		Stock_immediate_transfer domain = new Stock_immediate_transfer();
		stock_immediate_transferdto.setId(stock_immediate_transfer_id);
		domain.setId(stock_immediate_transfer_id);
        Boolean rst = stock_immediate_transferService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

	@ApiOperation(value = "获取默认查询", tags = {"Stock_immediate_transfer" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_stock/stock_immediate_transfers/fetchdefault")
	public ResponseEntity<Page<Stock_immediate_transferDTO>> fetchDefault(Stock_immediate_transferSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Stock_immediate_transferDTO> list = new ArrayList<Stock_immediate_transferDTO>();
        
        Page<Stock_immediate_transfer> domains = stock_immediate_transferService.searchDefault(context) ;
        for(Stock_immediate_transfer stock_immediate_transfer : domains.getContent()){
            Stock_immediate_transferDTO dto = new Stock_immediate_transferDTO();
            dto.fromDO(stock_immediate_transfer);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
