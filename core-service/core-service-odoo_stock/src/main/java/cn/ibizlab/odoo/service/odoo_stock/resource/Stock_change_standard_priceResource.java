package cn.ibizlab.odoo.service.odoo_stock.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_stock.dto.Stock_change_standard_priceDTO;
import cn.ibizlab.odoo.core.odoo_stock.domain.Stock_change_standard_price;
import cn.ibizlab.odoo.core.odoo_stock.service.IStock_change_standard_priceService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_stock.filter.Stock_change_standard_priceSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Stock_change_standard_price" })
@RestController
@RequestMapping("")
public class Stock_change_standard_priceResource {

    @Autowired
    private IStock_change_standard_priceService stock_change_standard_priceService;

    public IStock_change_standard_priceService getStock_change_standard_priceService() {
        return this.stock_change_standard_priceService;
    }

    @ApiOperation(value = "批删除数据", tags = {"Stock_change_standard_price" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_stock/stock_change_standard_prices/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Stock_change_standard_priceDTO> stock_change_standard_pricedtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "批更新数据", tags = {"Stock_change_standard_price" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_stock/stock_change_standard_prices/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Stock_change_standard_priceDTO> stock_change_standard_pricedtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "批建立数据", tags = {"Stock_change_standard_price" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_stock/stock_change_standard_prices/createBatch")
    public ResponseEntity<Boolean> createBatchStock_change_standard_price(@RequestBody List<Stock_change_standard_priceDTO> stock_change_standard_pricedtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "删除数据", tags = {"Stock_change_standard_price" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_stock/stock_change_standard_prices/{stock_change_standard_price_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("stock_change_standard_price_id") Integer stock_change_standard_price_id) {
        Stock_change_standard_priceDTO stock_change_standard_pricedto = new Stock_change_standard_priceDTO();
		Stock_change_standard_price domain = new Stock_change_standard_price();
		stock_change_standard_pricedto.setId(stock_change_standard_price_id);
		domain.setId(stock_change_standard_price_id);
        Boolean rst = stock_change_standard_priceService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "获取数据", tags = {"Stock_change_standard_price" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_stock/stock_change_standard_prices/{stock_change_standard_price_id}")
    public ResponseEntity<Stock_change_standard_priceDTO> get(@PathVariable("stock_change_standard_price_id") Integer stock_change_standard_price_id) {
        Stock_change_standard_priceDTO dto = new Stock_change_standard_priceDTO();
        Stock_change_standard_price domain = stock_change_standard_priceService.get(stock_change_standard_price_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "建立数据", tags = {"Stock_change_standard_price" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_stock/stock_change_standard_prices")

    public ResponseEntity<Stock_change_standard_priceDTO> create(@RequestBody Stock_change_standard_priceDTO stock_change_standard_pricedto) {
        Stock_change_standard_priceDTO dto = new Stock_change_standard_priceDTO();
        Stock_change_standard_price domain = stock_change_standard_pricedto.toDO();
		stock_change_standard_priceService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "更新数据", tags = {"Stock_change_standard_price" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_stock/stock_change_standard_prices/{stock_change_standard_price_id}")

    public ResponseEntity<Stock_change_standard_priceDTO> update(@PathVariable("stock_change_standard_price_id") Integer stock_change_standard_price_id, @RequestBody Stock_change_standard_priceDTO stock_change_standard_pricedto) {
		Stock_change_standard_price domain = stock_change_standard_pricedto.toDO();
        domain.setId(stock_change_standard_price_id);
		stock_change_standard_priceService.update(domain);
		Stock_change_standard_priceDTO dto = new Stock_change_standard_priceDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Stock_change_standard_price" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_stock/stock_change_standard_prices/fetchdefault")
	public ResponseEntity<Page<Stock_change_standard_priceDTO>> fetchDefault(Stock_change_standard_priceSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Stock_change_standard_priceDTO> list = new ArrayList<Stock_change_standard_priceDTO>();
        
        Page<Stock_change_standard_price> domains = stock_change_standard_priceService.searchDefault(context) ;
        for(Stock_change_standard_price stock_change_standard_price : domains.getContent()){
            Stock_change_standard_priceDTO dto = new Stock_change_standard_priceDTO();
            dto.fromDO(stock_change_standard_price);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
