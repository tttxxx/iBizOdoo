package cn.ibizlab.odoo.service.odoo_stock.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.core.odoo_stock.valuerule.anno.stock_location.*;
import cn.ibizlab.odoo.core.odoo_stock.domain.Stock_location;
import org.springframework.cglib.beans.BeanCopier;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 服务DTO对象[Stock_locationDTO]
 */
public class Stock_locationDTO implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [POSX]
     *
     */
    @Stock_locationPosxDefault(info = "默认规则")
    private Integer posx;

    @JsonIgnore
    private boolean posxDirtyFlag;

    /**
     * 属性 [POSY]
     *
     */
    @Stock_locationPosyDefault(info = "默认规则")
    private Integer posy;

    @JsonIgnore
    private boolean posyDirtyFlag;

    /**
     * 属性 [ACTIVE]
     *
     */
    @Stock_locationActiveDefault(info = "默认规则")
    private String active;

    @JsonIgnore
    private boolean activeDirtyFlag;

    /**
     * 属性 [CREATE_DATE]
     *
     */
    @Stock_locationCreate_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp create_date;

    @JsonIgnore
    private boolean create_dateDirtyFlag;

    /**
     * 属性 [BARCODE]
     *
     */
    @Stock_locationBarcodeDefault(info = "默认规则")
    private String barcode;

    @JsonIgnore
    private boolean barcodeDirtyFlag;

    /**
     * 属性 [WRITE_DATE]
     *
     */
    @Stock_locationWrite_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp write_date;

    @JsonIgnore
    private boolean write_dateDirtyFlag;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @Stock_locationDisplay_nameDefault(info = "默认规则")
    private String display_name;

    @JsonIgnore
    private boolean display_nameDirtyFlag;

    /**
     * 属性 [POSZ]
     *
     */
    @Stock_locationPoszDefault(info = "默认规则")
    private Integer posz;

    @JsonIgnore
    private boolean poszDirtyFlag;

    /**
     * 属性 [COMPLETE_NAME]
     *
     */
    @Stock_locationComplete_nameDefault(info = "默认规则")
    private String complete_name;

    @JsonIgnore
    private boolean complete_nameDirtyFlag;

    /**
     * 属性 [SCRAP_LOCATION]
     *
     */
    @Stock_locationScrap_locationDefault(info = "默认规则")
    private String scrap_location;

    @JsonIgnore
    private boolean scrap_locationDirtyFlag;

    /**
     * 属性 [RETURN_LOCATION]
     *
     */
    @Stock_locationReturn_locationDefault(info = "默认规则")
    private String return_location;

    @JsonIgnore
    private boolean return_locationDirtyFlag;

    /**
     * 属性 [NAME]
     *
     */
    @Stock_locationNameDefault(info = "默认规则")
    private String name;

    @JsonIgnore
    private boolean nameDirtyFlag;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @Stock_location__last_updateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp __last_update;

    @JsonIgnore
    private boolean __last_updateDirtyFlag;

    /**
     * 属性 [PARENT_PATH]
     *
     */
    @Stock_locationParent_pathDefault(info = "默认规则")
    private String parent_path;

    @JsonIgnore
    private boolean parent_pathDirtyFlag;

    /**
     * 属性 [QUANT_IDS]
     *
     */
    @Stock_locationQuant_idsDefault(info = "默认规则")
    private String quant_ids;

    @JsonIgnore
    private boolean quant_idsDirtyFlag;

    /**
     * 属性 [USAGE]
     *
     */
    @Stock_locationUsageDefault(info = "默认规则")
    private String usage;

    @JsonIgnore
    private boolean usageDirtyFlag;

    /**
     * 属性 [COMMENT]
     *
     */
    @Stock_locationCommentDefault(info = "默认规则")
    private String comment;

    @JsonIgnore
    private boolean commentDirtyFlag;

    /**
     * 属性 [ID]
     *
     */
    @Stock_locationIdDefault(info = "默认规则")
    private Integer id;

    @JsonIgnore
    private boolean idDirtyFlag;

    /**
     * 属性 [CHILD_IDS]
     *
     */
    @Stock_locationChild_idsDefault(info = "默认规则")
    private String child_ids;

    @JsonIgnore
    private boolean child_idsDirtyFlag;

    /**
     * 属性 [LOCATION_ID_TEXT]
     *
     */
    @Stock_locationLocation_id_textDefault(info = "默认规则")
    private String location_id_text;

    @JsonIgnore
    private boolean location_id_textDirtyFlag;

    /**
     * 属性 [COMPANY_ID_TEXT]
     *
     */
    @Stock_locationCompany_id_textDefault(info = "默认规则")
    private String company_id_text;

    @JsonIgnore
    private boolean company_id_textDirtyFlag;

    /**
     * 属性 [CREATE_UID_TEXT]
     *
     */
    @Stock_locationCreate_uid_textDefault(info = "默认规则")
    private String create_uid_text;

    @JsonIgnore
    private boolean create_uid_textDirtyFlag;

    /**
     * 属性 [VALUATION_OUT_ACCOUNT_ID_TEXT]
     *
     */
    @Stock_locationValuation_out_account_id_textDefault(info = "默认规则")
    private String valuation_out_account_id_text;

    @JsonIgnore
    private boolean valuation_out_account_id_textDirtyFlag;

    /**
     * 属性 [PARTNER_ID_TEXT]
     *
     */
    @Stock_locationPartner_id_textDefault(info = "默认规则")
    private String partner_id_text;

    @JsonIgnore
    private boolean partner_id_textDirtyFlag;

    /**
     * 属性 [REMOVAL_STRATEGY_ID_TEXT]
     *
     */
    @Stock_locationRemoval_strategy_id_textDefault(info = "默认规则")
    private String removal_strategy_id_text;

    @JsonIgnore
    private boolean removal_strategy_id_textDirtyFlag;

    /**
     * 属性 [PUTAWAY_STRATEGY_ID_TEXT]
     *
     */
    @Stock_locationPutaway_strategy_id_textDefault(info = "默认规则")
    private String putaway_strategy_id_text;

    @JsonIgnore
    private boolean putaway_strategy_id_textDirtyFlag;

    /**
     * 属性 [WRITE_UID_TEXT]
     *
     */
    @Stock_locationWrite_uid_textDefault(info = "默认规则")
    private String write_uid_text;

    @JsonIgnore
    private boolean write_uid_textDirtyFlag;

    /**
     * 属性 [VALUATION_IN_ACCOUNT_ID_TEXT]
     *
     */
    @Stock_locationValuation_in_account_id_textDefault(info = "默认规则")
    private String valuation_in_account_id_text;

    @JsonIgnore
    private boolean valuation_in_account_id_textDirtyFlag;

    /**
     * 属性 [WRITE_UID]
     *
     */
    @Stock_locationWrite_uidDefault(info = "默认规则")
    private Integer write_uid;

    @JsonIgnore
    private boolean write_uidDirtyFlag;

    /**
     * 属性 [CREATE_UID]
     *
     */
    @Stock_locationCreate_uidDefault(info = "默认规则")
    private Integer create_uid;

    @JsonIgnore
    private boolean create_uidDirtyFlag;

    /**
     * 属性 [VALUATION_IN_ACCOUNT_ID]
     *
     */
    @Stock_locationValuation_in_account_idDefault(info = "默认规则")
    private Integer valuation_in_account_id;

    @JsonIgnore
    private boolean valuation_in_account_idDirtyFlag;

    /**
     * 属性 [COMPANY_ID]
     *
     */
    @Stock_locationCompany_idDefault(info = "默认规则")
    private Integer company_id;

    @JsonIgnore
    private boolean company_idDirtyFlag;

    /**
     * 属性 [LOCATION_ID]
     *
     */
    @Stock_locationLocation_idDefault(info = "默认规则")
    private Integer location_id;

    @JsonIgnore
    private boolean location_idDirtyFlag;

    /**
     * 属性 [VALUATION_OUT_ACCOUNT_ID]
     *
     */
    @Stock_locationValuation_out_account_idDefault(info = "默认规则")
    private Integer valuation_out_account_id;

    @JsonIgnore
    private boolean valuation_out_account_idDirtyFlag;

    /**
     * 属性 [PARTNER_ID]
     *
     */
    @Stock_locationPartner_idDefault(info = "默认规则")
    private Integer partner_id;

    @JsonIgnore
    private boolean partner_idDirtyFlag;

    /**
     * 属性 [PUTAWAY_STRATEGY_ID]
     *
     */
    @Stock_locationPutaway_strategy_idDefault(info = "默认规则")
    private Integer putaway_strategy_id;

    @JsonIgnore
    private boolean putaway_strategy_idDirtyFlag;

    /**
     * 属性 [REMOVAL_STRATEGY_ID]
     *
     */
    @Stock_locationRemoval_strategy_idDefault(info = "默认规则")
    private Integer removal_strategy_id;

    @JsonIgnore
    private boolean removal_strategy_idDirtyFlag;


    /**
     * 获取 [POSX]
     */
    @JsonProperty("posx")
    public Integer getPosx(){
        return posx ;
    }

    /**
     * 设置 [POSX]
     */
    @JsonProperty("posx")
    public void setPosx(Integer  posx){
        this.posx = posx ;
        this.posxDirtyFlag = true ;
    }

    /**
     * 获取 [POSX]脏标记
     */
    @JsonIgnore
    public boolean getPosxDirtyFlag(){
        return posxDirtyFlag ;
    }

    /**
     * 获取 [POSY]
     */
    @JsonProperty("posy")
    public Integer getPosy(){
        return posy ;
    }

    /**
     * 设置 [POSY]
     */
    @JsonProperty("posy")
    public void setPosy(Integer  posy){
        this.posy = posy ;
        this.posyDirtyFlag = true ;
    }

    /**
     * 获取 [POSY]脏标记
     */
    @JsonIgnore
    public boolean getPosyDirtyFlag(){
        return posyDirtyFlag ;
    }

    /**
     * 获取 [ACTIVE]
     */
    @JsonProperty("active")
    public String getActive(){
        return active ;
    }

    /**
     * 设置 [ACTIVE]
     */
    @JsonProperty("active")
    public void setActive(String  active){
        this.active = active ;
        this.activeDirtyFlag = true ;
    }

    /**
     * 获取 [ACTIVE]脏标记
     */
    @JsonIgnore
    public boolean getActiveDirtyFlag(){
        return activeDirtyFlag ;
    }

    /**
     * 获取 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return create_date ;
    }

    /**
     * 设置 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return create_dateDirtyFlag ;
    }

    /**
     * 获取 [BARCODE]
     */
    @JsonProperty("barcode")
    public String getBarcode(){
        return barcode ;
    }

    /**
     * 设置 [BARCODE]
     */
    @JsonProperty("barcode")
    public void setBarcode(String  barcode){
        this.barcode = barcode ;
        this.barcodeDirtyFlag = true ;
    }

    /**
     * 获取 [BARCODE]脏标记
     */
    @JsonIgnore
    public boolean getBarcodeDirtyFlag(){
        return barcodeDirtyFlag ;
    }

    /**
     * 获取 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return write_date ;
    }

    /**
     * 设置 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return write_dateDirtyFlag ;
    }

    /**
     * 获取 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return display_name ;
    }

    /**
     * 设置 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

    /**
     * 获取 [DISPLAY_NAME]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return display_nameDirtyFlag ;
    }

    /**
     * 获取 [POSZ]
     */
    @JsonProperty("posz")
    public Integer getPosz(){
        return posz ;
    }

    /**
     * 设置 [POSZ]
     */
    @JsonProperty("posz")
    public void setPosz(Integer  posz){
        this.posz = posz ;
        this.poszDirtyFlag = true ;
    }

    /**
     * 获取 [POSZ]脏标记
     */
    @JsonIgnore
    public boolean getPoszDirtyFlag(){
        return poszDirtyFlag ;
    }

    /**
     * 获取 [COMPLETE_NAME]
     */
    @JsonProperty("complete_name")
    public String getComplete_name(){
        return complete_name ;
    }

    /**
     * 设置 [COMPLETE_NAME]
     */
    @JsonProperty("complete_name")
    public void setComplete_name(String  complete_name){
        this.complete_name = complete_name ;
        this.complete_nameDirtyFlag = true ;
    }

    /**
     * 获取 [COMPLETE_NAME]脏标记
     */
    @JsonIgnore
    public boolean getComplete_nameDirtyFlag(){
        return complete_nameDirtyFlag ;
    }

    /**
     * 获取 [SCRAP_LOCATION]
     */
    @JsonProperty("scrap_location")
    public String getScrap_location(){
        return scrap_location ;
    }

    /**
     * 设置 [SCRAP_LOCATION]
     */
    @JsonProperty("scrap_location")
    public void setScrap_location(String  scrap_location){
        this.scrap_location = scrap_location ;
        this.scrap_locationDirtyFlag = true ;
    }

    /**
     * 获取 [SCRAP_LOCATION]脏标记
     */
    @JsonIgnore
    public boolean getScrap_locationDirtyFlag(){
        return scrap_locationDirtyFlag ;
    }

    /**
     * 获取 [RETURN_LOCATION]
     */
    @JsonProperty("return_location")
    public String getReturn_location(){
        return return_location ;
    }

    /**
     * 设置 [RETURN_LOCATION]
     */
    @JsonProperty("return_location")
    public void setReturn_location(String  return_location){
        this.return_location = return_location ;
        this.return_locationDirtyFlag = true ;
    }

    /**
     * 获取 [RETURN_LOCATION]脏标记
     */
    @JsonIgnore
    public boolean getReturn_locationDirtyFlag(){
        return return_locationDirtyFlag ;
    }

    /**
     * 获取 [NAME]
     */
    @JsonProperty("name")
    public String getName(){
        return name ;
    }

    /**
     * 设置 [NAME]
     */
    @JsonProperty("name")
    public void setName(String  name){
        this.name = name ;
        this.nameDirtyFlag = true ;
    }

    /**
     * 获取 [NAME]脏标记
     */
    @JsonIgnore
    public boolean getNameDirtyFlag(){
        return nameDirtyFlag ;
    }

    /**
     * 获取 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return __last_update ;
    }

    /**
     * 设置 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

    /**
     * 获取 [__LAST_UPDATE]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return __last_updateDirtyFlag ;
    }

    /**
     * 获取 [PARENT_PATH]
     */
    @JsonProperty("parent_path")
    public String getParent_path(){
        return parent_path ;
    }

    /**
     * 设置 [PARENT_PATH]
     */
    @JsonProperty("parent_path")
    public void setParent_path(String  parent_path){
        this.parent_path = parent_path ;
        this.parent_pathDirtyFlag = true ;
    }

    /**
     * 获取 [PARENT_PATH]脏标记
     */
    @JsonIgnore
    public boolean getParent_pathDirtyFlag(){
        return parent_pathDirtyFlag ;
    }

    /**
     * 获取 [QUANT_IDS]
     */
    @JsonProperty("quant_ids")
    public String getQuant_ids(){
        return quant_ids ;
    }

    /**
     * 设置 [QUANT_IDS]
     */
    @JsonProperty("quant_ids")
    public void setQuant_ids(String  quant_ids){
        this.quant_ids = quant_ids ;
        this.quant_idsDirtyFlag = true ;
    }

    /**
     * 获取 [QUANT_IDS]脏标记
     */
    @JsonIgnore
    public boolean getQuant_idsDirtyFlag(){
        return quant_idsDirtyFlag ;
    }

    /**
     * 获取 [USAGE]
     */
    @JsonProperty("usage")
    public String getUsage(){
        return usage ;
    }

    /**
     * 设置 [USAGE]
     */
    @JsonProperty("usage")
    public void setUsage(String  usage){
        this.usage = usage ;
        this.usageDirtyFlag = true ;
    }

    /**
     * 获取 [USAGE]脏标记
     */
    @JsonIgnore
    public boolean getUsageDirtyFlag(){
        return usageDirtyFlag ;
    }

    /**
     * 获取 [COMMENT]
     */
    @JsonProperty("comment")
    public String getComment(){
        return comment ;
    }

    /**
     * 设置 [COMMENT]
     */
    @JsonProperty("comment")
    public void setComment(String  comment){
        this.comment = comment ;
        this.commentDirtyFlag = true ;
    }

    /**
     * 获取 [COMMENT]脏标记
     */
    @JsonIgnore
    public boolean getCommentDirtyFlag(){
        return commentDirtyFlag ;
    }

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

    /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return idDirtyFlag ;
    }

    /**
     * 获取 [CHILD_IDS]
     */
    @JsonProperty("child_ids")
    public String getChild_ids(){
        return child_ids ;
    }

    /**
     * 设置 [CHILD_IDS]
     */
    @JsonProperty("child_ids")
    public void setChild_ids(String  child_ids){
        this.child_ids = child_ids ;
        this.child_idsDirtyFlag = true ;
    }

    /**
     * 获取 [CHILD_IDS]脏标记
     */
    @JsonIgnore
    public boolean getChild_idsDirtyFlag(){
        return child_idsDirtyFlag ;
    }

    /**
     * 获取 [LOCATION_ID_TEXT]
     */
    @JsonProperty("location_id_text")
    public String getLocation_id_text(){
        return location_id_text ;
    }

    /**
     * 设置 [LOCATION_ID_TEXT]
     */
    @JsonProperty("location_id_text")
    public void setLocation_id_text(String  location_id_text){
        this.location_id_text = location_id_text ;
        this.location_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [LOCATION_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getLocation_id_textDirtyFlag(){
        return location_id_textDirtyFlag ;
    }

    /**
     * 获取 [COMPANY_ID_TEXT]
     */
    @JsonProperty("company_id_text")
    public String getCompany_id_text(){
        return company_id_text ;
    }

    /**
     * 设置 [COMPANY_ID_TEXT]
     */
    @JsonProperty("company_id_text")
    public void setCompany_id_text(String  company_id_text){
        this.company_id_text = company_id_text ;
        this.company_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [COMPANY_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getCompany_id_textDirtyFlag(){
        return company_id_textDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return create_uid_text ;
    }

    /**
     * 设置 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return create_uid_textDirtyFlag ;
    }

    /**
     * 获取 [VALUATION_OUT_ACCOUNT_ID_TEXT]
     */
    @JsonProperty("valuation_out_account_id_text")
    public String getValuation_out_account_id_text(){
        return valuation_out_account_id_text ;
    }

    /**
     * 设置 [VALUATION_OUT_ACCOUNT_ID_TEXT]
     */
    @JsonProperty("valuation_out_account_id_text")
    public void setValuation_out_account_id_text(String  valuation_out_account_id_text){
        this.valuation_out_account_id_text = valuation_out_account_id_text ;
        this.valuation_out_account_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [VALUATION_OUT_ACCOUNT_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getValuation_out_account_id_textDirtyFlag(){
        return valuation_out_account_id_textDirtyFlag ;
    }

    /**
     * 获取 [PARTNER_ID_TEXT]
     */
    @JsonProperty("partner_id_text")
    public String getPartner_id_text(){
        return partner_id_text ;
    }

    /**
     * 设置 [PARTNER_ID_TEXT]
     */
    @JsonProperty("partner_id_text")
    public void setPartner_id_text(String  partner_id_text){
        this.partner_id_text = partner_id_text ;
        this.partner_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [PARTNER_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getPartner_id_textDirtyFlag(){
        return partner_id_textDirtyFlag ;
    }

    /**
     * 获取 [REMOVAL_STRATEGY_ID_TEXT]
     */
    @JsonProperty("removal_strategy_id_text")
    public String getRemoval_strategy_id_text(){
        return removal_strategy_id_text ;
    }

    /**
     * 设置 [REMOVAL_STRATEGY_ID_TEXT]
     */
    @JsonProperty("removal_strategy_id_text")
    public void setRemoval_strategy_id_text(String  removal_strategy_id_text){
        this.removal_strategy_id_text = removal_strategy_id_text ;
        this.removal_strategy_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [REMOVAL_STRATEGY_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getRemoval_strategy_id_textDirtyFlag(){
        return removal_strategy_id_textDirtyFlag ;
    }

    /**
     * 获取 [PUTAWAY_STRATEGY_ID_TEXT]
     */
    @JsonProperty("putaway_strategy_id_text")
    public String getPutaway_strategy_id_text(){
        return putaway_strategy_id_text ;
    }

    /**
     * 设置 [PUTAWAY_STRATEGY_ID_TEXT]
     */
    @JsonProperty("putaway_strategy_id_text")
    public void setPutaway_strategy_id_text(String  putaway_strategy_id_text){
        this.putaway_strategy_id_text = putaway_strategy_id_text ;
        this.putaway_strategy_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [PUTAWAY_STRATEGY_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getPutaway_strategy_id_textDirtyFlag(){
        return putaway_strategy_id_textDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return write_uid_text ;
    }

    /**
     * 设置 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return write_uid_textDirtyFlag ;
    }

    /**
     * 获取 [VALUATION_IN_ACCOUNT_ID_TEXT]
     */
    @JsonProperty("valuation_in_account_id_text")
    public String getValuation_in_account_id_text(){
        return valuation_in_account_id_text ;
    }

    /**
     * 设置 [VALUATION_IN_ACCOUNT_ID_TEXT]
     */
    @JsonProperty("valuation_in_account_id_text")
    public void setValuation_in_account_id_text(String  valuation_in_account_id_text){
        this.valuation_in_account_id_text = valuation_in_account_id_text ;
        this.valuation_in_account_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [VALUATION_IN_ACCOUNT_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getValuation_in_account_id_textDirtyFlag(){
        return valuation_in_account_id_textDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return write_uid ;
    }

    /**
     * 设置 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return write_uidDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return create_uid ;
    }

    /**
     * 设置 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return create_uidDirtyFlag ;
    }

    /**
     * 获取 [VALUATION_IN_ACCOUNT_ID]
     */
    @JsonProperty("valuation_in_account_id")
    public Integer getValuation_in_account_id(){
        return valuation_in_account_id ;
    }

    /**
     * 设置 [VALUATION_IN_ACCOUNT_ID]
     */
    @JsonProperty("valuation_in_account_id")
    public void setValuation_in_account_id(Integer  valuation_in_account_id){
        this.valuation_in_account_id = valuation_in_account_id ;
        this.valuation_in_account_idDirtyFlag = true ;
    }

    /**
     * 获取 [VALUATION_IN_ACCOUNT_ID]脏标记
     */
    @JsonIgnore
    public boolean getValuation_in_account_idDirtyFlag(){
        return valuation_in_account_idDirtyFlag ;
    }

    /**
     * 获取 [COMPANY_ID]
     */
    @JsonProperty("company_id")
    public Integer getCompany_id(){
        return company_id ;
    }

    /**
     * 设置 [COMPANY_ID]
     */
    @JsonProperty("company_id")
    public void setCompany_id(Integer  company_id){
        this.company_id = company_id ;
        this.company_idDirtyFlag = true ;
    }

    /**
     * 获取 [COMPANY_ID]脏标记
     */
    @JsonIgnore
    public boolean getCompany_idDirtyFlag(){
        return company_idDirtyFlag ;
    }

    /**
     * 获取 [LOCATION_ID]
     */
    @JsonProperty("location_id")
    public Integer getLocation_id(){
        return location_id ;
    }

    /**
     * 设置 [LOCATION_ID]
     */
    @JsonProperty("location_id")
    public void setLocation_id(Integer  location_id){
        this.location_id = location_id ;
        this.location_idDirtyFlag = true ;
    }

    /**
     * 获取 [LOCATION_ID]脏标记
     */
    @JsonIgnore
    public boolean getLocation_idDirtyFlag(){
        return location_idDirtyFlag ;
    }

    /**
     * 获取 [VALUATION_OUT_ACCOUNT_ID]
     */
    @JsonProperty("valuation_out_account_id")
    public Integer getValuation_out_account_id(){
        return valuation_out_account_id ;
    }

    /**
     * 设置 [VALUATION_OUT_ACCOUNT_ID]
     */
    @JsonProperty("valuation_out_account_id")
    public void setValuation_out_account_id(Integer  valuation_out_account_id){
        this.valuation_out_account_id = valuation_out_account_id ;
        this.valuation_out_account_idDirtyFlag = true ;
    }

    /**
     * 获取 [VALUATION_OUT_ACCOUNT_ID]脏标记
     */
    @JsonIgnore
    public boolean getValuation_out_account_idDirtyFlag(){
        return valuation_out_account_idDirtyFlag ;
    }

    /**
     * 获取 [PARTNER_ID]
     */
    @JsonProperty("partner_id")
    public Integer getPartner_id(){
        return partner_id ;
    }

    /**
     * 设置 [PARTNER_ID]
     */
    @JsonProperty("partner_id")
    public void setPartner_id(Integer  partner_id){
        this.partner_id = partner_id ;
        this.partner_idDirtyFlag = true ;
    }

    /**
     * 获取 [PARTNER_ID]脏标记
     */
    @JsonIgnore
    public boolean getPartner_idDirtyFlag(){
        return partner_idDirtyFlag ;
    }

    /**
     * 获取 [PUTAWAY_STRATEGY_ID]
     */
    @JsonProperty("putaway_strategy_id")
    public Integer getPutaway_strategy_id(){
        return putaway_strategy_id ;
    }

    /**
     * 设置 [PUTAWAY_STRATEGY_ID]
     */
    @JsonProperty("putaway_strategy_id")
    public void setPutaway_strategy_id(Integer  putaway_strategy_id){
        this.putaway_strategy_id = putaway_strategy_id ;
        this.putaway_strategy_idDirtyFlag = true ;
    }

    /**
     * 获取 [PUTAWAY_STRATEGY_ID]脏标记
     */
    @JsonIgnore
    public boolean getPutaway_strategy_idDirtyFlag(){
        return putaway_strategy_idDirtyFlag ;
    }

    /**
     * 获取 [REMOVAL_STRATEGY_ID]
     */
    @JsonProperty("removal_strategy_id")
    public Integer getRemoval_strategy_id(){
        return removal_strategy_id ;
    }

    /**
     * 设置 [REMOVAL_STRATEGY_ID]
     */
    @JsonProperty("removal_strategy_id")
    public void setRemoval_strategy_id(Integer  removal_strategy_id){
        this.removal_strategy_id = removal_strategy_id ;
        this.removal_strategy_idDirtyFlag = true ;
    }

    /**
     * 获取 [REMOVAL_STRATEGY_ID]脏标记
     */
    @JsonIgnore
    public boolean getRemoval_strategy_idDirtyFlag(){
        return removal_strategy_idDirtyFlag ;
    }



    public Stock_location toDO() {
        Stock_location srfdomain = new Stock_location();
        if(getPosxDirtyFlag())
            srfdomain.setPosx(posx);
        if(getPosyDirtyFlag())
            srfdomain.setPosy(posy);
        if(getActiveDirtyFlag())
            srfdomain.setActive(active);
        if(getCreate_dateDirtyFlag())
            srfdomain.setCreate_date(create_date);
        if(getBarcodeDirtyFlag())
            srfdomain.setBarcode(barcode);
        if(getWrite_dateDirtyFlag())
            srfdomain.setWrite_date(write_date);
        if(getDisplay_nameDirtyFlag())
            srfdomain.setDisplay_name(display_name);
        if(getPoszDirtyFlag())
            srfdomain.setPosz(posz);
        if(getComplete_nameDirtyFlag())
            srfdomain.setComplete_name(complete_name);
        if(getScrap_locationDirtyFlag())
            srfdomain.setScrap_location(scrap_location);
        if(getReturn_locationDirtyFlag())
            srfdomain.setReturn_location(return_location);
        if(getNameDirtyFlag())
            srfdomain.setName(name);
        if(get__last_updateDirtyFlag())
            srfdomain.set__last_update(__last_update);
        if(getParent_pathDirtyFlag())
            srfdomain.setParent_path(parent_path);
        if(getQuant_idsDirtyFlag())
            srfdomain.setQuant_ids(quant_ids);
        if(getUsageDirtyFlag())
            srfdomain.setUsage(usage);
        if(getCommentDirtyFlag())
            srfdomain.setComment(comment);
        if(getIdDirtyFlag())
            srfdomain.setId(id);
        if(getChild_idsDirtyFlag())
            srfdomain.setChild_ids(child_ids);
        if(getLocation_id_textDirtyFlag())
            srfdomain.setLocation_id_text(location_id_text);
        if(getCompany_id_textDirtyFlag())
            srfdomain.setCompany_id_text(company_id_text);
        if(getCreate_uid_textDirtyFlag())
            srfdomain.setCreate_uid_text(create_uid_text);
        if(getValuation_out_account_id_textDirtyFlag())
            srfdomain.setValuation_out_account_id_text(valuation_out_account_id_text);
        if(getPartner_id_textDirtyFlag())
            srfdomain.setPartner_id_text(partner_id_text);
        if(getRemoval_strategy_id_textDirtyFlag())
            srfdomain.setRemoval_strategy_id_text(removal_strategy_id_text);
        if(getPutaway_strategy_id_textDirtyFlag())
            srfdomain.setPutaway_strategy_id_text(putaway_strategy_id_text);
        if(getWrite_uid_textDirtyFlag())
            srfdomain.setWrite_uid_text(write_uid_text);
        if(getValuation_in_account_id_textDirtyFlag())
            srfdomain.setValuation_in_account_id_text(valuation_in_account_id_text);
        if(getWrite_uidDirtyFlag())
            srfdomain.setWrite_uid(write_uid);
        if(getCreate_uidDirtyFlag())
            srfdomain.setCreate_uid(create_uid);
        if(getValuation_in_account_idDirtyFlag())
            srfdomain.setValuation_in_account_id(valuation_in_account_id);
        if(getCompany_idDirtyFlag())
            srfdomain.setCompany_id(company_id);
        if(getLocation_idDirtyFlag())
            srfdomain.setLocation_id(location_id);
        if(getValuation_out_account_idDirtyFlag())
            srfdomain.setValuation_out_account_id(valuation_out_account_id);
        if(getPartner_idDirtyFlag())
            srfdomain.setPartner_id(partner_id);
        if(getPutaway_strategy_idDirtyFlag())
            srfdomain.setPutaway_strategy_id(putaway_strategy_id);
        if(getRemoval_strategy_idDirtyFlag())
            srfdomain.setRemoval_strategy_id(removal_strategy_id);

        return srfdomain;
    }

    public void fromDO(Stock_location srfdomain) {
        if(srfdomain == null )
          return ;
        if(srfdomain.getPosxDirtyFlag())
            this.setPosx(srfdomain.getPosx());
        if(srfdomain.getPosyDirtyFlag())
            this.setPosy(srfdomain.getPosy());
        if(srfdomain.getActiveDirtyFlag())
            this.setActive(srfdomain.getActive());
        if(srfdomain.getCreate_dateDirtyFlag())
            this.setCreate_date(srfdomain.getCreate_date());
        if(srfdomain.getBarcodeDirtyFlag())
            this.setBarcode(srfdomain.getBarcode());
        if(srfdomain.getWrite_dateDirtyFlag())
            this.setWrite_date(srfdomain.getWrite_date());
        if(srfdomain.getDisplay_nameDirtyFlag())
            this.setDisplay_name(srfdomain.getDisplay_name());
        if(srfdomain.getPoszDirtyFlag())
            this.setPosz(srfdomain.getPosz());
        if(srfdomain.getComplete_nameDirtyFlag())
            this.setComplete_name(srfdomain.getComplete_name());
        if(srfdomain.getScrap_locationDirtyFlag())
            this.setScrap_location(srfdomain.getScrap_location());
        if(srfdomain.getReturn_locationDirtyFlag())
            this.setReturn_location(srfdomain.getReturn_location());
        if(srfdomain.getNameDirtyFlag())
            this.setName(srfdomain.getName());
        if(srfdomain.get__last_updateDirtyFlag())
            this.set__last_update(srfdomain.get__last_update());
        if(srfdomain.getParent_pathDirtyFlag())
            this.setParent_path(srfdomain.getParent_path());
        if(srfdomain.getQuant_idsDirtyFlag())
            this.setQuant_ids(srfdomain.getQuant_ids());
        if(srfdomain.getUsageDirtyFlag())
            this.setUsage(srfdomain.getUsage());
        if(srfdomain.getCommentDirtyFlag())
            this.setComment(srfdomain.getComment());
        if(srfdomain.getIdDirtyFlag())
            this.setId(srfdomain.getId());
        if(srfdomain.getChild_idsDirtyFlag())
            this.setChild_ids(srfdomain.getChild_ids());
        if(srfdomain.getLocation_id_textDirtyFlag())
            this.setLocation_id_text(srfdomain.getLocation_id_text());
        if(srfdomain.getCompany_id_textDirtyFlag())
            this.setCompany_id_text(srfdomain.getCompany_id_text());
        if(srfdomain.getCreate_uid_textDirtyFlag())
            this.setCreate_uid_text(srfdomain.getCreate_uid_text());
        if(srfdomain.getValuation_out_account_id_textDirtyFlag())
            this.setValuation_out_account_id_text(srfdomain.getValuation_out_account_id_text());
        if(srfdomain.getPartner_id_textDirtyFlag())
            this.setPartner_id_text(srfdomain.getPartner_id_text());
        if(srfdomain.getRemoval_strategy_id_textDirtyFlag())
            this.setRemoval_strategy_id_text(srfdomain.getRemoval_strategy_id_text());
        if(srfdomain.getPutaway_strategy_id_textDirtyFlag())
            this.setPutaway_strategy_id_text(srfdomain.getPutaway_strategy_id_text());
        if(srfdomain.getWrite_uid_textDirtyFlag())
            this.setWrite_uid_text(srfdomain.getWrite_uid_text());
        if(srfdomain.getValuation_in_account_id_textDirtyFlag())
            this.setValuation_in_account_id_text(srfdomain.getValuation_in_account_id_text());
        if(srfdomain.getWrite_uidDirtyFlag())
            this.setWrite_uid(srfdomain.getWrite_uid());
        if(srfdomain.getCreate_uidDirtyFlag())
            this.setCreate_uid(srfdomain.getCreate_uid());
        if(srfdomain.getValuation_in_account_idDirtyFlag())
            this.setValuation_in_account_id(srfdomain.getValuation_in_account_id());
        if(srfdomain.getCompany_idDirtyFlag())
            this.setCompany_id(srfdomain.getCompany_id());
        if(srfdomain.getLocation_idDirtyFlag())
            this.setLocation_id(srfdomain.getLocation_id());
        if(srfdomain.getValuation_out_account_idDirtyFlag())
            this.setValuation_out_account_id(srfdomain.getValuation_out_account_id());
        if(srfdomain.getPartner_idDirtyFlag())
            this.setPartner_id(srfdomain.getPartner_id());
        if(srfdomain.getPutaway_strategy_idDirtyFlag())
            this.setPutaway_strategy_id(srfdomain.getPutaway_strategy_id());
        if(srfdomain.getRemoval_strategy_idDirtyFlag())
            this.setRemoval_strategy_id(srfdomain.getRemoval_strategy_id());

    }

    public List<Stock_locationDTO> fromDOPage(List<Stock_location> poPage)   {
        if(poPage == null)
            return null;
        List<Stock_locationDTO> dtos=new ArrayList<Stock_locationDTO>();
        for(Stock_location domain : poPage) {
            Stock_locationDTO dto = new Stock_locationDTO();
            dto.fromDO(domain);
            dtos.add(dto);
        }
        return dtos;
    }
}

