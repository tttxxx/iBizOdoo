package cn.ibizlab.odoo.service.odoo_stock.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_stock.dto.Stock_track_lineDTO;
import cn.ibizlab.odoo.core.odoo_stock.domain.Stock_track_line;
import cn.ibizlab.odoo.core.odoo_stock.service.IStock_track_lineService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_stock.filter.Stock_track_lineSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Stock_track_line" })
@RestController
@RequestMapping("")
public class Stock_track_lineResource {

    @Autowired
    private IStock_track_lineService stock_track_lineService;

    public IStock_track_lineService getStock_track_lineService() {
        return this.stock_track_lineService;
    }

    @ApiOperation(value = "批更新数据", tags = {"Stock_track_line" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_stock/stock_track_lines/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Stock_track_lineDTO> stock_track_linedtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "获取数据", tags = {"Stock_track_line" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_stock/stock_track_lines/{stock_track_line_id}")
    public ResponseEntity<Stock_track_lineDTO> get(@PathVariable("stock_track_line_id") Integer stock_track_line_id) {
        Stock_track_lineDTO dto = new Stock_track_lineDTO();
        Stock_track_line domain = stock_track_lineService.get(stock_track_line_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "建立数据", tags = {"Stock_track_line" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_stock/stock_track_lines")

    public ResponseEntity<Stock_track_lineDTO> create(@RequestBody Stock_track_lineDTO stock_track_linedto) {
        Stock_track_lineDTO dto = new Stock_track_lineDTO();
        Stock_track_line domain = stock_track_linedto.toDO();
		stock_track_lineService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批建立数据", tags = {"Stock_track_line" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_stock/stock_track_lines/createBatch")
    public ResponseEntity<Boolean> createBatchStock_track_line(@RequestBody List<Stock_track_lineDTO> stock_track_linedtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "删除数据", tags = {"Stock_track_line" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_stock/stock_track_lines/{stock_track_line_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("stock_track_line_id") Integer stock_track_line_id) {
        Stock_track_lineDTO stock_track_linedto = new Stock_track_lineDTO();
		Stock_track_line domain = new Stock_track_line();
		stock_track_linedto.setId(stock_track_line_id);
		domain.setId(stock_track_line_id);
        Boolean rst = stock_track_lineService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "批删除数据", tags = {"Stock_track_line" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_stock/stock_track_lines/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Stock_track_lineDTO> stock_track_linedtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "更新数据", tags = {"Stock_track_line" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_stock/stock_track_lines/{stock_track_line_id}")

    public ResponseEntity<Stock_track_lineDTO> update(@PathVariable("stock_track_line_id") Integer stock_track_line_id, @RequestBody Stock_track_lineDTO stock_track_linedto) {
		Stock_track_line domain = stock_track_linedto.toDO();
        domain.setId(stock_track_line_id);
		stock_track_lineService.update(domain);
		Stock_track_lineDTO dto = new Stock_track_lineDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Stock_track_line" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_stock/stock_track_lines/fetchdefault")
	public ResponseEntity<Page<Stock_track_lineDTO>> fetchDefault(Stock_track_lineSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Stock_track_lineDTO> list = new ArrayList<Stock_track_lineDTO>();
        
        Page<Stock_track_line> domains = stock_track_lineService.searchDefault(context) ;
        for(Stock_track_line stock_track_line : domains.getContent()){
            Stock_track_lineDTO dto = new Stock_track_lineDTO();
            dto.fromDO(stock_track_line);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
