package cn.ibizlab.odoo.service.odoo_stock.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_stock.dto.Stock_warehouseDTO;
import cn.ibizlab.odoo.core.odoo_stock.domain.Stock_warehouse;
import cn.ibizlab.odoo.core.odoo_stock.service.IStock_warehouseService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_stock.filter.Stock_warehouseSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Stock_warehouse" })
@RestController
@RequestMapping("")
public class Stock_warehouseResource {

    @Autowired
    private IStock_warehouseService stock_warehouseService;

    public IStock_warehouseService getStock_warehouseService() {
        return this.stock_warehouseService;
    }

    @ApiOperation(value = "获取数据", tags = {"Stock_warehouse" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_stock/stock_warehouses/{stock_warehouse_id}")
    public ResponseEntity<Stock_warehouseDTO> get(@PathVariable("stock_warehouse_id") Integer stock_warehouse_id) {
        Stock_warehouseDTO dto = new Stock_warehouseDTO();
        Stock_warehouse domain = stock_warehouseService.get(stock_warehouse_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批删除数据", tags = {"Stock_warehouse" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_stock/stock_warehouses/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Stock_warehouseDTO> stock_warehousedtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "建立数据", tags = {"Stock_warehouse" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_stock/stock_warehouses")

    public ResponseEntity<Stock_warehouseDTO> create(@RequestBody Stock_warehouseDTO stock_warehousedto) {
        Stock_warehouseDTO dto = new Stock_warehouseDTO();
        Stock_warehouse domain = stock_warehousedto.toDO();
		stock_warehouseService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "更新数据", tags = {"Stock_warehouse" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_stock/stock_warehouses/{stock_warehouse_id}")

    public ResponseEntity<Stock_warehouseDTO> update(@PathVariable("stock_warehouse_id") Integer stock_warehouse_id, @RequestBody Stock_warehouseDTO stock_warehousedto) {
		Stock_warehouse domain = stock_warehousedto.toDO();
        domain.setId(stock_warehouse_id);
		stock_warehouseService.update(domain);
		Stock_warehouseDTO dto = new Stock_warehouseDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批建立数据", tags = {"Stock_warehouse" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_stock/stock_warehouses/createBatch")
    public ResponseEntity<Boolean> createBatchStock_warehouse(@RequestBody List<Stock_warehouseDTO> stock_warehousedtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "批更新数据", tags = {"Stock_warehouse" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_stock/stock_warehouses/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Stock_warehouseDTO> stock_warehousedtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "删除数据", tags = {"Stock_warehouse" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_stock/stock_warehouses/{stock_warehouse_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("stock_warehouse_id") Integer stock_warehouse_id) {
        Stock_warehouseDTO stock_warehousedto = new Stock_warehouseDTO();
		Stock_warehouse domain = new Stock_warehouse();
		stock_warehousedto.setId(stock_warehouse_id);
		domain.setId(stock_warehouse_id);
        Boolean rst = stock_warehouseService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

	@ApiOperation(value = "获取默认查询", tags = {"Stock_warehouse" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_stock/stock_warehouses/fetchdefault")
	public ResponseEntity<Page<Stock_warehouseDTO>> fetchDefault(Stock_warehouseSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Stock_warehouseDTO> list = new ArrayList<Stock_warehouseDTO>();
        
        Page<Stock_warehouse> domains = stock_warehouseService.searchDefault(context) ;
        for(Stock_warehouse stock_warehouse : domains.getContent()){
            Stock_warehouseDTO dto = new Stock_warehouseDTO();
            dto.fromDO(stock_warehouse);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
