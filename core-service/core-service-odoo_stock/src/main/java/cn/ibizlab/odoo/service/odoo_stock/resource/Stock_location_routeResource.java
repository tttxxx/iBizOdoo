package cn.ibizlab.odoo.service.odoo_stock.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_stock.dto.Stock_location_routeDTO;
import cn.ibizlab.odoo.core.odoo_stock.domain.Stock_location_route;
import cn.ibizlab.odoo.core.odoo_stock.service.IStock_location_routeService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_stock.filter.Stock_location_routeSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Stock_location_route" })
@RestController
@RequestMapping("")
public class Stock_location_routeResource {

    @Autowired
    private IStock_location_routeService stock_location_routeService;

    public IStock_location_routeService getStock_location_routeService() {
        return this.stock_location_routeService;
    }

    @ApiOperation(value = "批删除数据", tags = {"Stock_location_route" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_stock/stock_location_routes/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Stock_location_routeDTO> stock_location_routedtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "建立数据", tags = {"Stock_location_route" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_stock/stock_location_routes")

    public ResponseEntity<Stock_location_routeDTO> create(@RequestBody Stock_location_routeDTO stock_location_routedto) {
        Stock_location_routeDTO dto = new Stock_location_routeDTO();
        Stock_location_route domain = stock_location_routedto.toDO();
		stock_location_routeService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "删除数据", tags = {"Stock_location_route" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_stock/stock_location_routes/{stock_location_route_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("stock_location_route_id") Integer stock_location_route_id) {
        Stock_location_routeDTO stock_location_routedto = new Stock_location_routeDTO();
		Stock_location_route domain = new Stock_location_route();
		stock_location_routedto.setId(stock_location_route_id);
		domain.setId(stock_location_route_id);
        Boolean rst = stock_location_routeService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "批建立数据", tags = {"Stock_location_route" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_stock/stock_location_routes/createBatch")
    public ResponseEntity<Boolean> createBatchStock_location_route(@RequestBody List<Stock_location_routeDTO> stock_location_routedtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "获取数据", tags = {"Stock_location_route" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_stock/stock_location_routes/{stock_location_route_id}")
    public ResponseEntity<Stock_location_routeDTO> get(@PathVariable("stock_location_route_id") Integer stock_location_route_id) {
        Stock_location_routeDTO dto = new Stock_location_routeDTO();
        Stock_location_route domain = stock_location_routeService.get(stock_location_route_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批更新数据", tags = {"Stock_location_route" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_stock/stock_location_routes/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Stock_location_routeDTO> stock_location_routedtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "更新数据", tags = {"Stock_location_route" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_stock/stock_location_routes/{stock_location_route_id}")

    public ResponseEntity<Stock_location_routeDTO> update(@PathVariable("stock_location_route_id") Integer stock_location_route_id, @RequestBody Stock_location_routeDTO stock_location_routedto) {
		Stock_location_route domain = stock_location_routedto.toDO();
        domain.setId(stock_location_route_id);
		stock_location_routeService.update(domain);
		Stock_location_routeDTO dto = new Stock_location_routeDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Stock_location_route" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_stock/stock_location_routes/fetchdefault")
	public ResponseEntity<Page<Stock_location_routeDTO>> fetchDefault(Stock_location_routeSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Stock_location_routeDTO> list = new ArrayList<Stock_location_routeDTO>();
        
        Page<Stock_location_route> domains = stock_location_routeService.searchDefault(context) ;
        for(Stock_location_route stock_location_route : domains.getContent()){
            Stock_location_routeDTO dto = new Stock_location_routeDTO();
            dto.fromDO(stock_location_route);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
