package cn.ibizlab.odoo.service.odoo_stock.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_stock.dto.Stock_warn_insufficient_qty_scrapDTO;
import cn.ibizlab.odoo.core.odoo_stock.domain.Stock_warn_insufficient_qty_scrap;
import cn.ibizlab.odoo.core.odoo_stock.service.IStock_warn_insufficient_qty_scrapService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_stock.filter.Stock_warn_insufficient_qty_scrapSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Stock_warn_insufficient_qty_scrap" })
@RestController
@RequestMapping("")
public class Stock_warn_insufficient_qty_scrapResource {

    @Autowired
    private IStock_warn_insufficient_qty_scrapService stock_warn_insufficient_qty_scrapService;

    public IStock_warn_insufficient_qty_scrapService getStock_warn_insufficient_qty_scrapService() {
        return this.stock_warn_insufficient_qty_scrapService;
    }

    @ApiOperation(value = "删除数据", tags = {"Stock_warn_insufficient_qty_scrap" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_stock/stock_warn_insufficient_qty_scraps/{stock_warn_insufficient_qty_scrap_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("stock_warn_insufficient_qty_scrap_id") Integer stock_warn_insufficient_qty_scrap_id) {
        Stock_warn_insufficient_qty_scrapDTO stock_warn_insufficient_qty_scrapdto = new Stock_warn_insufficient_qty_scrapDTO();
		Stock_warn_insufficient_qty_scrap domain = new Stock_warn_insufficient_qty_scrap();
		stock_warn_insufficient_qty_scrapdto.setId(stock_warn_insufficient_qty_scrap_id);
		domain.setId(stock_warn_insufficient_qty_scrap_id);
        Boolean rst = stock_warn_insufficient_qty_scrapService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "更新数据", tags = {"Stock_warn_insufficient_qty_scrap" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_stock/stock_warn_insufficient_qty_scraps/{stock_warn_insufficient_qty_scrap_id}")

    public ResponseEntity<Stock_warn_insufficient_qty_scrapDTO> update(@PathVariable("stock_warn_insufficient_qty_scrap_id") Integer stock_warn_insufficient_qty_scrap_id, @RequestBody Stock_warn_insufficient_qty_scrapDTO stock_warn_insufficient_qty_scrapdto) {
		Stock_warn_insufficient_qty_scrap domain = stock_warn_insufficient_qty_scrapdto.toDO();
        domain.setId(stock_warn_insufficient_qty_scrap_id);
		stock_warn_insufficient_qty_scrapService.update(domain);
		Stock_warn_insufficient_qty_scrapDTO dto = new Stock_warn_insufficient_qty_scrapDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批删除数据", tags = {"Stock_warn_insufficient_qty_scrap" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_stock/stock_warn_insufficient_qty_scraps/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Stock_warn_insufficient_qty_scrapDTO> stock_warn_insufficient_qty_scrapdtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "建立数据", tags = {"Stock_warn_insufficient_qty_scrap" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_stock/stock_warn_insufficient_qty_scraps")

    public ResponseEntity<Stock_warn_insufficient_qty_scrapDTO> create(@RequestBody Stock_warn_insufficient_qty_scrapDTO stock_warn_insufficient_qty_scrapdto) {
        Stock_warn_insufficient_qty_scrapDTO dto = new Stock_warn_insufficient_qty_scrapDTO();
        Stock_warn_insufficient_qty_scrap domain = stock_warn_insufficient_qty_scrapdto.toDO();
		stock_warn_insufficient_qty_scrapService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批更新数据", tags = {"Stock_warn_insufficient_qty_scrap" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_stock/stock_warn_insufficient_qty_scraps/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Stock_warn_insufficient_qty_scrapDTO> stock_warn_insufficient_qty_scrapdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "批建立数据", tags = {"Stock_warn_insufficient_qty_scrap" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_stock/stock_warn_insufficient_qty_scraps/createBatch")
    public ResponseEntity<Boolean> createBatchStock_warn_insufficient_qty_scrap(@RequestBody List<Stock_warn_insufficient_qty_scrapDTO> stock_warn_insufficient_qty_scrapdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "获取数据", tags = {"Stock_warn_insufficient_qty_scrap" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_stock/stock_warn_insufficient_qty_scraps/{stock_warn_insufficient_qty_scrap_id}")
    public ResponseEntity<Stock_warn_insufficient_qty_scrapDTO> get(@PathVariable("stock_warn_insufficient_qty_scrap_id") Integer stock_warn_insufficient_qty_scrap_id) {
        Stock_warn_insufficient_qty_scrapDTO dto = new Stock_warn_insufficient_qty_scrapDTO();
        Stock_warn_insufficient_qty_scrap domain = stock_warn_insufficient_qty_scrapService.get(stock_warn_insufficient_qty_scrap_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Stock_warn_insufficient_qty_scrap" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_stock/stock_warn_insufficient_qty_scraps/fetchdefault")
	public ResponseEntity<Page<Stock_warn_insufficient_qty_scrapDTO>> fetchDefault(Stock_warn_insufficient_qty_scrapSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Stock_warn_insufficient_qty_scrapDTO> list = new ArrayList<Stock_warn_insufficient_qty_scrapDTO>();
        
        Page<Stock_warn_insufficient_qty_scrap> domains = stock_warn_insufficient_qty_scrapService.searchDefault(context) ;
        for(Stock_warn_insufficient_qty_scrap stock_warn_insufficient_qty_scrap : domains.getContent()){
            Stock_warn_insufficient_qty_scrapDTO dto = new Stock_warn_insufficient_qty_scrapDTO();
            dto.fromDO(stock_warn_insufficient_qty_scrap);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
