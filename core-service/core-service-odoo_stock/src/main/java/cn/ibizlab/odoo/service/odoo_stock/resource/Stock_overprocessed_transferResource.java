package cn.ibizlab.odoo.service.odoo_stock.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_stock.dto.Stock_overprocessed_transferDTO;
import cn.ibizlab.odoo.core.odoo_stock.domain.Stock_overprocessed_transfer;
import cn.ibizlab.odoo.core.odoo_stock.service.IStock_overprocessed_transferService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_stock.filter.Stock_overprocessed_transferSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Stock_overprocessed_transfer" })
@RestController
@RequestMapping("")
public class Stock_overprocessed_transferResource {

    @Autowired
    private IStock_overprocessed_transferService stock_overprocessed_transferService;

    public IStock_overprocessed_transferService getStock_overprocessed_transferService() {
        return this.stock_overprocessed_transferService;
    }

    @ApiOperation(value = "批建立数据", tags = {"Stock_overprocessed_transfer" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_stock/stock_overprocessed_transfers/createBatch")
    public ResponseEntity<Boolean> createBatchStock_overprocessed_transfer(@RequestBody List<Stock_overprocessed_transferDTO> stock_overprocessed_transferdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "建立数据", tags = {"Stock_overprocessed_transfer" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_stock/stock_overprocessed_transfers")

    public ResponseEntity<Stock_overprocessed_transferDTO> create(@RequestBody Stock_overprocessed_transferDTO stock_overprocessed_transferdto) {
        Stock_overprocessed_transferDTO dto = new Stock_overprocessed_transferDTO();
        Stock_overprocessed_transfer domain = stock_overprocessed_transferdto.toDO();
		stock_overprocessed_transferService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批删除数据", tags = {"Stock_overprocessed_transfer" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_stock/stock_overprocessed_transfers/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Stock_overprocessed_transferDTO> stock_overprocessed_transferdtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "获取数据", tags = {"Stock_overprocessed_transfer" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_stock/stock_overprocessed_transfers/{stock_overprocessed_transfer_id}")
    public ResponseEntity<Stock_overprocessed_transferDTO> get(@PathVariable("stock_overprocessed_transfer_id") Integer stock_overprocessed_transfer_id) {
        Stock_overprocessed_transferDTO dto = new Stock_overprocessed_transferDTO();
        Stock_overprocessed_transfer domain = stock_overprocessed_transferService.get(stock_overprocessed_transfer_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批更新数据", tags = {"Stock_overprocessed_transfer" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_stock/stock_overprocessed_transfers/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Stock_overprocessed_transferDTO> stock_overprocessed_transferdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "更新数据", tags = {"Stock_overprocessed_transfer" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_stock/stock_overprocessed_transfers/{stock_overprocessed_transfer_id}")

    public ResponseEntity<Stock_overprocessed_transferDTO> update(@PathVariable("stock_overprocessed_transfer_id") Integer stock_overprocessed_transfer_id, @RequestBody Stock_overprocessed_transferDTO stock_overprocessed_transferdto) {
		Stock_overprocessed_transfer domain = stock_overprocessed_transferdto.toDO();
        domain.setId(stock_overprocessed_transfer_id);
		stock_overprocessed_transferService.update(domain);
		Stock_overprocessed_transferDTO dto = new Stock_overprocessed_transferDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "删除数据", tags = {"Stock_overprocessed_transfer" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_stock/stock_overprocessed_transfers/{stock_overprocessed_transfer_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("stock_overprocessed_transfer_id") Integer stock_overprocessed_transfer_id) {
        Stock_overprocessed_transferDTO stock_overprocessed_transferdto = new Stock_overprocessed_transferDTO();
		Stock_overprocessed_transfer domain = new Stock_overprocessed_transfer();
		stock_overprocessed_transferdto.setId(stock_overprocessed_transfer_id);
		domain.setId(stock_overprocessed_transfer_id);
        Boolean rst = stock_overprocessed_transferService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

	@ApiOperation(value = "获取默认查询", tags = {"Stock_overprocessed_transfer" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_stock/stock_overprocessed_transfers/fetchdefault")
	public ResponseEntity<Page<Stock_overprocessed_transferDTO>> fetchDefault(Stock_overprocessed_transferSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Stock_overprocessed_transferDTO> list = new ArrayList<Stock_overprocessed_transferDTO>();
        
        Page<Stock_overprocessed_transfer> domains = stock_overprocessed_transferService.searchDefault(context) ;
        for(Stock_overprocessed_transfer stock_overprocessed_transfer : domains.getContent()){
            Stock_overprocessed_transferDTO dto = new Stock_overprocessed_transferDTO();
            dto.fromDO(stock_overprocessed_transfer);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
