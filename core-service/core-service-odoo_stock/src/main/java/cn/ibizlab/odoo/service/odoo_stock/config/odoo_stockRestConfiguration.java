package cn.ibizlab.odoo.service.odoo_stock.config;

import org.springframework.context.annotation.ComponentScan;

@ComponentScan("cn.ibizlab.odoo.service.odoo_stock")
public class odoo_stockRestConfiguration {

}
