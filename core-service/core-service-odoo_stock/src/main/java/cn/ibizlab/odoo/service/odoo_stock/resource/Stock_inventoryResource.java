package cn.ibizlab.odoo.service.odoo_stock.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_stock.dto.Stock_inventoryDTO;
import cn.ibizlab.odoo.core.odoo_stock.domain.Stock_inventory;
import cn.ibizlab.odoo.core.odoo_stock.service.IStock_inventoryService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_stock.filter.Stock_inventorySearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Stock_inventory" })
@RestController
@RequestMapping("")
public class Stock_inventoryResource {

    @Autowired
    private IStock_inventoryService stock_inventoryService;

    public IStock_inventoryService getStock_inventoryService() {
        return this.stock_inventoryService;
    }

    @ApiOperation(value = "建立数据", tags = {"Stock_inventory" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_stock/stock_inventories")

    public ResponseEntity<Stock_inventoryDTO> create(@RequestBody Stock_inventoryDTO stock_inventorydto) {
        Stock_inventoryDTO dto = new Stock_inventoryDTO();
        Stock_inventory domain = stock_inventorydto.toDO();
		stock_inventoryService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取数据", tags = {"Stock_inventory" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_stock/stock_inventories/{stock_inventory_id}")
    public ResponseEntity<Stock_inventoryDTO> get(@PathVariable("stock_inventory_id") Integer stock_inventory_id) {
        Stock_inventoryDTO dto = new Stock_inventoryDTO();
        Stock_inventory domain = stock_inventoryService.get(stock_inventory_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "更新数据", tags = {"Stock_inventory" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_stock/stock_inventories/{stock_inventory_id}")

    public ResponseEntity<Stock_inventoryDTO> update(@PathVariable("stock_inventory_id") Integer stock_inventory_id, @RequestBody Stock_inventoryDTO stock_inventorydto) {
		Stock_inventory domain = stock_inventorydto.toDO();
        domain.setId(stock_inventory_id);
		stock_inventoryService.update(domain);
		Stock_inventoryDTO dto = new Stock_inventoryDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批建立数据", tags = {"Stock_inventory" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_stock/stock_inventories/createBatch")
    public ResponseEntity<Boolean> createBatchStock_inventory(@RequestBody List<Stock_inventoryDTO> stock_inventorydtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "批删除数据", tags = {"Stock_inventory" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_stock/stock_inventories/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Stock_inventoryDTO> stock_inventorydtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "删除数据", tags = {"Stock_inventory" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_stock/stock_inventories/{stock_inventory_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("stock_inventory_id") Integer stock_inventory_id) {
        Stock_inventoryDTO stock_inventorydto = new Stock_inventoryDTO();
		Stock_inventory domain = new Stock_inventory();
		stock_inventorydto.setId(stock_inventory_id);
		domain.setId(stock_inventory_id);
        Boolean rst = stock_inventoryService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "批更新数据", tags = {"Stock_inventory" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_stock/stock_inventories/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Stock_inventoryDTO> stock_inventorydtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Stock_inventory" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_stock/stock_inventories/fetchdefault")
	public ResponseEntity<Page<Stock_inventoryDTO>> fetchDefault(Stock_inventorySearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Stock_inventoryDTO> list = new ArrayList<Stock_inventoryDTO>();
        
        Page<Stock_inventory> domains = stock_inventoryService.searchDefault(context) ;
        for(Stock_inventory stock_inventory : domains.getContent()){
            Stock_inventoryDTO dto = new Stock_inventoryDTO();
            dto.fromDO(stock_inventory);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
