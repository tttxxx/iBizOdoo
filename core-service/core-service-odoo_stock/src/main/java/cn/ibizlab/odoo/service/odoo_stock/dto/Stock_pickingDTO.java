package cn.ibizlab.odoo.service.odoo_stock.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.core.odoo_stock.valuerule.anno.stock_picking.*;
import cn.ibizlab.odoo.core.odoo_stock.domain.Stock_picking;
import org.springframework.cglib.beans.BeanCopier;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 服务DTO对象[Stock_pickingDTO]
 */
public class Stock_pickingDTO implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [PACKAGE_LEVEL_IDS]
     *
     */
    @Stock_pickingPackage_level_idsDefault(info = "默认规则")
    private String package_level_ids;

    @JsonIgnore
    private boolean package_level_idsDirtyFlag;

    /**
     * 属性 [NOTE]
     *
     */
    @Stock_pickingNoteDefault(info = "默认规则")
    private String note;

    @JsonIgnore
    private boolean noteDirtyFlag;

    /**
     * 属性 [WEBSITE_ID]
     *
     */
    @Stock_pickingWebsite_idDefault(info = "默认规则")
    private Integer website_id;

    @JsonIgnore
    private boolean website_idDirtyFlag;

    /**
     * 属性 [ACTIVITY_TYPE_ID]
     *
     */
    @Stock_pickingActivity_type_idDefault(info = "默认规则")
    private Integer activity_type_id;

    @JsonIgnore
    private boolean activity_type_idDirtyFlag;

    /**
     * 属性 [PRINTED]
     *
     */
    @Stock_pickingPrintedDefault(info = "默认规则")
    private String printed;

    @JsonIgnore
    private boolean printedDirtyFlag;

    /**
     * 属性 [MESSAGE_NEEDACTION_COUNTER]
     *
     */
    @Stock_pickingMessage_needaction_counterDefault(info = "默认规则")
    private Integer message_needaction_counter;

    @JsonIgnore
    private boolean message_needaction_counterDirtyFlag;

    /**
     * 属性 [PRODUCT_ID]
     *
     */
    @Stock_pickingProduct_idDefault(info = "默认规则")
    private Integer product_id;

    @JsonIgnore
    private boolean product_idDirtyFlag;

    /**
     * 属性 [MESSAGE_PARTNER_IDS]
     *
     */
    @Stock_pickingMessage_partner_idsDefault(info = "默认规则")
    private String message_partner_ids;

    @JsonIgnore
    private boolean message_partner_idsDirtyFlag;

    /**
     * 属性 [MESSAGE_UNREAD_COUNTER]
     *
     */
    @Stock_pickingMessage_unread_counterDefault(info = "默认规则")
    private Integer message_unread_counter;

    @JsonIgnore
    private boolean message_unread_counterDirtyFlag;

    /**
     * 属性 [MOVE_TYPE]
     *
     */
    @Stock_pickingMove_typeDefault(info = "默认规则")
    private String move_type;

    @JsonIgnore
    private boolean move_typeDirtyFlag;

    /**
     * 属性 [BACKORDER_IDS]
     *
     */
    @Stock_pickingBackorder_idsDefault(info = "默认规则")
    private String backorder_ids;

    @JsonIgnore
    private boolean backorder_idsDirtyFlag;

    /**
     * 属性 [MESSAGE_NEEDACTION]
     *
     */
    @Stock_pickingMessage_needactionDefault(info = "默认规则")
    private String message_needaction;

    @JsonIgnore
    private boolean message_needactionDirtyFlag;

    /**
     * 属性 [MESSAGE_IDS]
     *
     */
    @Stock_pickingMessage_idsDefault(info = "默认规则")
    private String message_ids;

    @JsonIgnore
    private boolean message_idsDirtyFlag;

    /**
     * 属性 [HAS_PACKAGES]
     *
     */
    @Stock_pickingHas_packagesDefault(info = "默认规则")
    private String has_packages;

    @JsonIgnore
    private boolean has_packagesDirtyFlag;

    /**
     * 属性 [ORIGIN]
     *
     */
    @Stock_pickingOriginDefault(info = "默认规则")
    private String origin;

    @JsonIgnore
    private boolean originDirtyFlag;

    /**
     * 属性 [IS_LOCKED]
     *
     */
    @Stock_pickingIs_lockedDefault(info = "默认规则")
    private String is_locked;

    @JsonIgnore
    private boolean is_lockedDirtyFlag;

    /**
     * 属性 [SHOW_OPERATIONS]
     *
     */
    @Stock_pickingShow_operationsDefault(info = "默认规则")
    private String show_operations;

    @JsonIgnore
    private boolean show_operationsDirtyFlag;

    /**
     * 属性 [GROUP_ID]
     *
     */
    @Stock_pickingGroup_idDefault(info = "默认规则")
    private Integer group_id;

    @JsonIgnore
    private boolean group_idDirtyFlag;

    /**
     * 属性 [MESSAGE_HAS_ERROR_COUNTER]
     *
     */
    @Stock_pickingMessage_has_error_counterDefault(info = "默认规则")
    private Integer message_has_error_counter;

    @JsonIgnore
    private boolean message_has_error_counterDirtyFlag;

    /**
     * 属性 [MOVE_LINE_IDS_WITHOUT_PACKAGE]
     *
     */
    @Stock_pickingMove_line_ids_without_packageDefault(info = "默认规则")
    private String move_line_ids_without_package;

    @JsonIgnore
    private boolean move_line_ids_without_packageDirtyFlag;

    /**
     * 属性 [ACTIVITY_DATE_DEADLINE]
     *
     */
    @Stock_pickingActivity_date_deadlineDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    private Timestamp activity_date_deadline;

    @JsonIgnore
    private boolean activity_date_deadlineDirtyFlag;

    /**
     * 属性 [ACTIVITY_USER_ID]
     *
     */
    @Stock_pickingActivity_user_idDefault(info = "默认规则")
    private Integer activity_user_id;

    @JsonIgnore
    private boolean activity_user_idDirtyFlag;

    /**
     * 属性 [ACTIVITY_STATE]
     *
     */
    @Stock_pickingActivity_stateDefault(info = "默认规则")
    private String activity_state;

    @JsonIgnore
    private boolean activity_stateDirtyFlag;

    /**
     * 属性 [ACTIVITY_IDS]
     *
     */
    @Stock_pickingActivity_idsDefault(info = "默认规则")
    private String activity_ids;

    @JsonIgnore
    private boolean activity_idsDirtyFlag;

    /**
     * 属性 [ID]
     *
     */
    @Stock_pickingIdDefault(info = "默认规则")
    private Integer id;

    @JsonIgnore
    private boolean idDirtyFlag;

    /**
     * 属性 [ACTIVITY_SUMMARY]
     *
     */
    @Stock_pickingActivity_summaryDefault(info = "默认规则")
    private String activity_summary;

    @JsonIgnore
    private boolean activity_summaryDirtyFlag;

    /**
     * 属性 [MESSAGE_HAS_ERROR]
     *
     */
    @Stock_pickingMessage_has_errorDefault(info = "默认规则")
    private String message_has_error;

    @JsonIgnore
    private boolean message_has_errorDirtyFlag;

    /**
     * 属性 [MOVE_LINE_IDS]
     *
     */
    @Stock_pickingMove_line_idsDefault(info = "默认规则")
    private String move_line_ids;

    @JsonIgnore
    private boolean move_line_idsDirtyFlag;

    /**
     * 属性 [PACKAGE_LEVEL_IDS_DETAILS]
     *
     */
    @Stock_pickingPackage_level_ids_detailsDefault(info = "默认规则")
    private String package_level_ids_details;

    @JsonIgnore
    private boolean package_level_ids_detailsDirtyFlag;

    /**
     * 属性 [MESSAGE_UNREAD]
     *
     */
    @Stock_pickingMessage_unreadDefault(info = "默认规则")
    private String message_unread;

    @JsonIgnore
    private boolean message_unreadDirtyFlag;

    /**
     * 属性 [DATE]
     *
     */
    @Stock_pickingDateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp date;

    @JsonIgnore
    private boolean dateDirtyFlag;

    /**
     * 属性 [DATE_DONE]
     *
     */
    @Stock_pickingDate_doneDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp date_done;

    @JsonIgnore
    private boolean date_doneDirtyFlag;

    /**
     * 属性 [HAS_TRACKING]
     *
     */
    @Stock_pickingHas_trackingDefault(info = "默认规则")
    private String has_tracking;

    @JsonIgnore
    private boolean has_trackingDirtyFlag;

    /**
     * 属性 [MESSAGE_MAIN_ATTACHMENT_ID]
     *
     */
    @Stock_pickingMessage_main_attachment_idDefault(info = "默认规则")
    private Integer message_main_attachment_id;

    @JsonIgnore
    private boolean message_main_attachment_idDirtyFlag;

    /**
     * 属性 [STATE]
     *
     */
    @Stock_pickingStateDefault(info = "默认规则")
    private String state;

    @JsonIgnore
    private boolean stateDirtyFlag;

    /**
     * 属性 [IMMEDIATE_TRANSFER]
     *
     */
    @Stock_pickingImmediate_transferDefault(info = "默认规则")
    private String immediate_transfer;

    @JsonIgnore
    private boolean immediate_transferDirtyFlag;

    /**
     * 属性 [HAS_SCRAP_MOVE]
     *
     */
    @Stock_pickingHas_scrap_moveDefault(info = "默认规则")
    private String has_scrap_move;

    @JsonIgnore
    private boolean has_scrap_moveDirtyFlag;

    /**
     * 属性 [MOVE_IDS_WITHOUT_PACKAGE]
     *
     */
    @Stock_pickingMove_ids_without_packageDefault(info = "默认规则")
    private String move_ids_without_package;

    @JsonIgnore
    private boolean move_ids_without_packageDirtyFlag;

    /**
     * 属性 [CREATE_DATE]
     *
     */
    @Stock_pickingCreate_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp create_date;

    @JsonIgnore
    private boolean create_dateDirtyFlag;

    /**
     * 属性 [SHOW_VALIDATE]
     *
     */
    @Stock_pickingShow_validateDefault(info = "默认规则")
    private String show_validate;

    @JsonIgnore
    private boolean show_validateDirtyFlag;

    /**
     * 属性 [NAME]
     *
     */
    @Stock_pickingNameDefault(info = "默认规则")
    private String name;

    @JsonIgnore
    private boolean nameDirtyFlag;

    /**
     * 属性 [MESSAGE_CHANNEL_IDS]
     *
     */
    @Stock_pickingMessage_channel_idsDefault(info = "默认规则")
    private String message_channel_ids;

    @JsonIgnore
    private boolean message_channel_idsDirtyFlag;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @Stock_pickingDisplay_nameDefault(info = "默认规则")
    private String display_name;

    @JsonIgnore
    private boolean display_nameDirtyFlag;

    /**
     * 属性 [PRIORITY]
     *
     */
    @Stock_pickingPriorityDefault(info = "默认规则")
    private String priority;

    @JsonIgnore
    private boolean priorityDirtyFlag;

    /**
     * 属性 [MOVE_LINES]
     *
     */
    @Stock_pickingMove_linesDefault(info = "默认规则")
    private String move_lines;

    @JsonIgnore
    private boolean move_linesDirtyFlag;

    /**
     * 属性 [PURCHASE_ID]
     *
     */
    @Stock_pickingPurchase_idDefault(info = "默认规则")
    private Integer purchase_id;

    @JsonIgnore
    private boolean purchase_idDirtyFlag;

    /**
     * 属性 [WRITE_DATE]
     *
     */
    @Stock_pickingWrite_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp write_date;

    @JsonIgnore
    private boolean write_dateDirtyFlag;

    /**
     * 属性 [WEBSITE_MESSAGE_IDS]
     *
     */
    @Stock_pickingWebsite_message_idsDefault(info = "默认规则")
    private String website_message_ids;

    @JsonIgnore
    private boolean website_message_idsDirtyFlag;

    /**
     * 属性 [SHOW_MARK_AS_TODO]
     *
     */
    @Stock_pickingShow_mark_as_todoDefault(info = "默认规则")
    private String show_mark_as_todo;

    @JsonIgnore
    private boolean show_mark_as_todoDirtyFlag;

    /**
     * 属性 [MESSAGE_FOLLOWER_IDS]
     *
     */
    @Stock_pickingMessage_follower_idsDefault(info = "默认规则")
    private String message_follower_ids;

    @JsonIgnore
    private boolean message_follower_idsDirtyFlag;

    /**
     * 属性 [MOVE_LINE_EXIST]
     *
     */
    @Stock_pickingMove_line_existDefault(info = "默认规则")
    private String move_line_exist;

    @JsonIgnore
    private boolean move_line_existDirtyFlag;

    /**
     * 属性 [MESSAGE_ATTACHMENT_COUNT]
     *
     */
    @Stock_pickingMessage_attachment_countDefault(info = "默认规则")
    private Integer message_attachment_count;

    @JsonIgnore
    private boolean message_attachment_countDirtyFlag;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @Stock_picking__last_updateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp __last_update;

    @JsonIgnore
    private boolean __last_updateDirtyFlag;

    /**
     * 属性 [SCHEDULED_DATE]
     *
     */
    @Stock_pickingScheduled_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp scheduled_date;

    @JsonIgnore
    private boolean scheduled_dateDirtyFlag;

    /**
     * 属性 [MESSAGE_IS_FOLLOWER]
     *
     */
    @Stock_pickingMessage_is_followerDefault(info = "默认规则")
    private String message_is_follower;

    @JsonIgnore
    private boolean message_is_followerDirtyFlag;

    /**
     * 属性 [SHOW_CHECK_AVAILABILITY]
     *
     */
    @Stock_pickingShow_check_availabilityDefault(info = "默认规则")
    private String show_check_availability;

    @JsonIgnore
    private boolean show_check_availabilityDirtyFlag;

    /**
     * 属性 [SHOW_LOTS_TEXT]
     *
     */
    @Stock_pickingShow_lots_textDefault(info = "默认规则")
    private String show_lots_text;

    @JsonIgnore
    private boolean show_lots_textDirtyFlag;

    /**
     * 属性 [WRITE_UID_TEXT]
     *
     */
    @Stock_pickingWrite_uid_textDefault(info = "默认规则")
    private String write_uid_text;

    @JsonIgnore
    private boolean write_uid_textDirtyFlag;

    /**
     * 属性 [BACKORDER_ID_TEXT]
     *
     */
    @Stock_pickingBackorder_id_textDefault(info = "默认规则")
    private String backorder_id_text;

    @JsonIgnore
    private boolean backorder_id_textDirtyFlag;

    /**
     * 属性 [PICKING_TYPE_ID_TEXT]
     *
     */
    @Stock_pickingPicking_type_id_textDefault(info = "默认规则")
    private String picking_type_id_text;

    @JsonIgnore
    private boolean picking_type_id_textDirtyFlag;

    /**
     * 属性 [PARTNER_ID_TEXT]
     *
     */
    @Stock_pickingPartner_id_textDefault(info = "默认规则")
    private String partner_id_text;

    @JsonIgnore
    private boolean partner_id_textDirtyFlag;

    /**
     * 属性 [CREATE_UID_TEXT]
     *
     */
    @Stock_pickingCreate_uid_textDefault(info = "默认规则")
    private String create_uid_text;

    @JsonIgnore
    private boolean create_uid_textDirtyFlag;

    /**
     * 属性 [LOCATION_ID_TEXT]
     *
     */
    @Stock_pickingLocation_id_textDefault(info = "默认规则")
    private String location_id_text;

    @JsonIgnore
    private boolean location_id_textDirtyFlag;

    /**
     * 属性 [LOCATION_DEST_ID_TEXT]
     *
     */
    @Stock_pickingLocation_dest_id_textDefault(info = "默认规则")
    private String location_dest_id_text;

    @JsonIgnore
    private boolean location_dest_id_textDirtyFlag;

    /**
     * 属性 [PICKING_TYPE_CODE]
     *
     */
    @Stock_pickingPicking_type_codeDefault(info = "默认规则")
    private String picking_type_code;

    @JsonIgnore
    private boolean picking_type_codeDirtyFlag;

    /**
     * 属性 [PICKING_TYPE_ENTIRE_PACKS]
     *
     */
    @Stock_pickingPicking_type_entire_packsDefault(info = "默认规则")
    private String picking_type_entire_packs;

    @JsonIgnore
    private boolean picking_type_entire_packsDirtyFlag;

    /**
     * 属性 [OWNER_ID_TEXT]
     *
     */
    @Stock_pickingOwner_id_textDefault(info = "默认规则")
    private String owner_id_text;

    @JsonIgnore
    private boolean owner_id_textDirtyFlag;

    /**
     * 属性 [SALE_ID_TEXT]
     *
     */
    @Stock_pickingSale_id_textDefault(info = "默认规则")
    private String sale_id_text;

    @JsonIgnore
    private boolean sale_id_textDirtyFlag;

    /**
     * 属性 [COMPANY_ID_TEXT]
     *
     */
    @Stock_pickingCompany_id_textDefault(info = "默认规则")
    private String company_id_text;

    @JsonIgnore
    private boolean company_id_textDirtyFlag;

    /**
     * 属性 [PICKING_TYPE_ID]
     *
     */
    @Stock_pickingPicking_type_idDefault(info = "默认规则")
    private Integer picking_type_id;

    @JsonIgnore
    private boolean picking_type_idDirtyFlag;

    /**
     * 属性 [SALE_ID]
     *
     */
    @Stock_pickingSale_idDefault(info = "默认规则")
    private Integer sale_id;

    @JsonIgnore
    private boolean sale_idDirtyFlag;

    /**
     * 属性 [LOCATION_DEST_ID]
     *
     */
    @Stock_pickingLocation_dest_idDefault(info = "默认规则")
    private Integer location_dest_id;

    @JsonIgnore
    private boolean location_dest_idDirtyFlag;

    /**
     * 属性 [CREATE_UID]
     *
     */
    @Stock_pickingCreate_uidDefault(info = "默认规则")
    private Integer create_uid;

    @JsonIgnore
    private boolean create_uidDirtyFlag;

    /**
     * 属性 [PARTNER_ID]
     *
     */
    @Stock_pickingPartner_idDefault(info = "默认规则")
    private Integer partner_id;

    @JsonIgnore
    private boolean partner_idDirtyFlag;

    /**
     * 属性 [OWNER_ID]
     *
     */
    @Stock_pickingOwner_idDefault(info = "默认规则")
    private Integer owner_id;

    @JsonIgnore
    private boolean owner_idDirtyFlag;

    /**
     * 属性 [WRITE_UID]
     *
     */
    @Stock_pickingWrite_uidDefault(info = "默认规则")
    private Integer write_uid;

    @JsonIgnore
    private boolean write_uidDirtyFlag;

    /**
     * 属性 [COMPANY_ID]
     *
     */
    @Stock_pickingCompany_idDefault(info = "默认规则")
    private Integer company_id;

    @JsonIgnore
    private boolean company_idDirtyFlag;

    /**
     * 属性 [LOCATION_ID]
     *
     */
    @Stock_pickingLocation_idDefault(info = "默认规则")
    private Integer location_id;

    @JsonIgnore
    private boolean location_idDirtyFlag;

    /**
     * 属性 [BACKORDER_ID]
     *
     */
    @Stock_pickingBackorder_idDefault(info = "默认规则")
    private Integer backorder_id;

    @JsonIgnore
    private boolean backorder_idDirtyFlag;


    /**
     * 获取 [PACKAGE_LEVEL_IDS]
     */
    @JsonProperty("package_level_ids")
    public String getPackage_level_ids(){
        return package_level_ids ;
    }

    /**
     * 设置 [PACKAGE_LEVEL_IDS]
     */
    @JsonProperty("package_level_ids")
    public void setPackage_level_ids(String  package_level_ids){
        this.package_level_ids = package_level_ids ;
        this.package_level_idsDirtyFlag = true ;
    }

    /**
     * 获取 [PACKAGE_LEVEL_IDS]脏标记
     */
    @JsonIgnore
    public boolean getPackage_level_idsDirtyFlag(){
        return package_level_idsDirtyFlag ;
    }

    /**
     * 获取 [NOTE]
     */
    @JsonProperty("note")
    public String getNote(){
        return note ;
    }

    /**
     * 设置 [NOTE]
     */
    @JsonProperty("note")
    public void setNote(String  note){
        this.note = note ;
        this.noteDirtyFlag = true ;
    }

    /**
     * 获取 [NOTE]脏标记
     */
    @JsonIgnore
    public boolean getNoteDirtyFlag(){
        return noteDirtyFlag ;
    }

    /**
     * 获取 [WEBSITE_ID]
     */
    @JsonProperty("website_id")
    public Integer getWebsite_id(){
        return website_id ;
    }

    /**
     * 设置 [WEBSITE_ID]
     */
    @JsonProperty("website_id")
    public void setWebsite_id(Integer  website_id){
        this.website_id = website_id ;
        this.website_idDirtyFlag = true ;
    }

    /**
     * 获取 [WEBSITE_ID]脏标记
     */
    @JsonIgnore
    public boolean getWebsite_idDirtyFlag(){
        return website_idDirtyFlag ;
    }

    /**
     * 获取 [ACTIVITY_TYPE_ID]
     */
    @JsonProperty("activity_type_id")
    public Integer getActivity_type_id(){
        return activity_type_id ;
    }

    /**
     * 设置 [ACTIVITY_TYPE_ID]
     */
    @JsonProperty("activity_type_id")
    public void setActivity_type_id(Integer  activity_type_id){
        this.activity_type_id = activity_type_id ;
        this.activity_type_idDirtyFlag = true ;
    }

    /**
     * 获取 [ACTIVITY_TYPE_ID]脏标记
     */
    @JsonIgnore
    public boolean getActivity_type_idDirtyFlag(){
        return activity_type_idDirtyFlag ;
    }

    /**
     * 获取 [PRINTED]
     */
    @JsonProperty("printed")
    public String getPrinted(){
        return printed ;
    }

    /**
     * 设置 [PRINTED]
     */
    @JsonProperty("printed")
    public void setPrinted(String  printed){
        this.printed = printed ;
        this.printedDirtyFlag = true ;
    }

    /**
     * 获取 [PRINTED]脏标记
     */
    @JsonIgnore
    public boolean getPrintedDirtyFlag(){
        return printedDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_NEEDACTION_COUNTER]
     */
    @JsonProperty("message_needaction_counter")
    public Integer getMessage_needaction_counter(){
        return message_needaction_counter ;
    }

    /**
     * 设置 [MESSAGE_NEEDACTION_COUNTER]
     */
    @JsonProperty("message_needaction_counter")
    public void setMessage_needaction_counter(Integer  message_needaction_counter){
        this.message_needaction_counter = message_needaction_counter ;
        this.message_needaction_counterDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_NEEDACTION_COUNTER]脏标记
     */
    @JsonIgnore
    public boolean getMessage_needaction_counterDirtyFlag(){
        return message_needaction_counterDirtyFlag ;
    }

    /**
     * 获取 [PRODUCT_ID]
     */
    @JsonProperty("product_id")
    public Integer getProduct_id(){
        return product_id ;
    }

    /**
     * 设置 [PRODUCT_ID]
     */
    @JsonProperty("product_id")
    public void setProduct_id(Integer  product_id){
        this.product_id = product_id ;
        this.product_idDirtyFlag = true ;
    }

    /**
     * 获取 [PRODUCT_ID]脏标记
     */
    @JsonIgnore
    public boolean getProduct_idDirtyFlag(){
        return product_idDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_PARTNER_IDS]
     */
    @JsonProperty("message_partner_ids")
    public String getMessage_partner_ids(){
        return message_partner_ids ;
    }

    /**
     * 设置 [MESSAGE_PARTNER_IDS]
     */
    @JsonProperty("message_partner_ids")
    public void setMessage_partner_ids(String  message_partner_ids){
        this.message_partner_ids = message_partner_ids ;
        this.message_partner_idsDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_PARTNER_IDS]脏标记
     */
    @JsonIgnore
    public boolean getMessage_partner_idsDirtyFlag(){
        return message_partner_idsDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_UNREAD_COUNTER]
     */
    @JsonProperty("message_unread_counter")
    public Integer getMessage_unread_counter(){
        return message_unread_counter ;
    }

    /**
     * 设置 [MESSAGE_UNREAD_COUNTER]
     */
    @JsonProperty("message_unread_counter")
    public void setMessage_unread_counter(Integer  message_unread_counter){
        this.message_unread_counter = message_unread_counter ;
        this.message_unread_counterDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_UNREAD_COUNTER]脏标记
     */
    @JsonIgnore
    public boolean getMessage_unread_counterDirtyFlag(){
        return message_unread_counterDirtyFlag ;
    }

    /**
     * 获取 [MOVE_TYPE]
     */
    @JsonProperty("move_type")
    public String getMove_type(){
        return move_type ;
    }

    /**
     * 设置 [MOVE_TYPE]
     */
    @JsonProperty("move_type")
    public void setMove_type(String  move_type){
        this.move_type = move_type ;
        this.move_typeDirtyFlag = true ;
    }

    /**
     * 获取 [MOVE_TYPE]脏标记
     */
    @JsonIgnore
    public boolean getMove_typeDirtyFlag(){
        return move_typeDirtyFlag ;
    }

    /**
     * 获取 [BACKORDER_IDS]
     */
    @JsonProperty("backorder_ids")
    public String getBackorder_ids(){
        return backorder_ids ;
    }

    /**
     * 设置 [BACKORDER_IDS]
     */
    @JsonProperty("backorder_ids")
    public void setBackorder_ids(String  backorder_ids){
        this.backorder_ids = backorder_ids ;
        this.backorder_idsDirtyFlag = true ;
    }

    /**
     * 获取 [BACKORDER_IDS]脏标记
     */
    @JsonIgnore
    public boolean getBackorder_idsDirtyFlag(){
        return backorder_idsDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_NEEDACTION]
     */
    @JsonProperty("message_needaction")
    public String getMessage_needaction(){
        return message_needaction ;
    }

    /**
     * 设置 [MESSAGE_NEEDACTION]
     */
    @JsonProperty("message_needaction")
    public void setMessage_needaction(String  message_needaction){
        this.message_needaction = message_needaction ;
        this.message_needactionDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_NEEDACTION]脏标记
     */
    @JsonIgnore
    public boolean getMessage_needactionDirtyFlag(){
        return message_needactionDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_IDS]
     */
    @JsonProperty("message_ids")
    public String getMessage_ids(){
        return message_ids ;
    }

    /**
     * 设置 [MESSAGE_IDS]
     */
    @JsonProperty("message_ids")
    public void setMessage_ids(String  message_ids){
        this.message_ids = message_ids ;
        this.message_idsDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_IDS]脏标记
     */
    @JsonIgnore
    public boolean getMessage_idsDirtyFlag(){
        return message_idsDirtyFlag ;
    }

    /**
     * 获取 [HAS_PACKAGES]
     */
    @JsonProperty("has_packages")
    public String getHas_packages(){
        return has_packages ;
    }

    /**
     * 设置 [HAS_PACKAGES]
     */
    @JsonProperty("has_packages")
    public void setHas_packages(String  has_packages){
        this.has_packages = has_packages ;
        this.has_packagesDirtyFlag = true ;
    }

    /**
     * 获取 [HAS_PACKAGES]脏标记
     */
    @JsonIgnore
    public boolean getHas_packagesDirtyFlag(){
        return has_packagesDirtyFlag ;
    }

    /**
     * 获取 [ORIGIN]
     */
    @JsonProperty("origin")
    public String getOrigin(){
        return origin ;
    }

    /**
     * 设置 [ORIGIN]
     */
    @JsonProperty("origin")
    public void setOrigin(String  origin){
        this.origin = origin ;
        this.originDirtyFlag = true ;
    }

    /**
     * 获取 [ORIGIN]脏标记
     */
    @JsonIgnore
    public boolean getOriginDirtyFlag(){
        return originDirtyFlag ;
    }

    /**
     * 获取 [IS_LOCKED]
     */
    @JsonProperty("is_locked")
    public String getIs_locked(){
        return is_locked ;
    }

    /**
     * 设置 [IS_LOCKED]
     */
    @JsonProperty("is_locked")
    public void setIs_locked(String  is_locked){
        this.is_locked = is_locked ;
        this.is_lockedDirtyFlag = true ;
    }

    /**
     * 获取 [IS_LOCKED]脏标记
     */
    @JsonIgnore
    public boolean getIs_lockedDirtyFlag(){
        return is_lockedDirtyFlag ;
    }

    /**
     * 获取 [SHOW_OPERATIONS]
     */
    @JsonProperty("show_operations")
    public String getShow_operations(){
        return show_operations ;
    }

    /**
     * 设置 [SHOW_OPERATIONS]
     */
    @JsonProperty("show_operations")
    public void setShow_operations(String  show_operations){
        this.show_operations = show_operations ;
        this.show_operationsDirtyFlag = true ;
    }

    /**
     * 获取 [SHOW_OPERATIONS]脏标记
     */
    @JsonIgnore
    public boolean getShow_operationsDirtyFlag(){
        return show_operationsDirtyFlag ;
    }

    /**
     * 获取 [GROUP_ID]
     */
    @JsonProperty("group_id")
    public Integer getGroup_id(){
        return group_id ;
    }

    /**
     * 设置 [GROUP_ID]
     */
    @JsonProperty("group_id")
    public void setGroup_id(Integer  group_id){
        this.group_id = group_id ;
        this.group_idDirtyFlag = true ;
    }

    /**
     * 获取 [GROUP_ID]脏标记
     */
    @JsonIgnore
    public boolean getGroup_idDirtyFlag(){
        return group_idDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_HAS_ERROR_COUNTER]
     */
    @JsonProperty("message_has_error_counter")
    public Integer getMessage_has_error_counter(){
        return message_has_error_counter ;
    }

    /**
     * 设置 [MESSAGE_HAS_ERROR_COUNTER]
     */
    @JsonProperty("message_has_error_counter")
    public void setMessage_has_error_counter(Integer  message_has_error_counter){
        this.message_has_error_counter = message_has_error_counter ;
        this.message_has_error_counterDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_HAS_ERROR_COUNTER]脏标记
     */
    @JsonIgnore
    public boolean getMessage_has_error_counterDirtyFlag(){
        return message_has_error_counterDirtyFlag ;
    }

    /**
     * 获取 [MOVE_LINE_IDS_WITHOUT_PACKAGE]
     */
    @JsonProperty("move_line_ids_without_package")
    public String getMove_line_ids_without_package(){
        return move_line_ids_without_package ;
    }

    /**
     * 设置 [MOVE_LINE_IDS_WITHOUT_PACKAGE]
     */
    @JsonProperty("move_line_ids_without_package")
    public void setMove_line_ids_without_package(String  move_line_ids_without_package){
        this.move_line_ids_without_package = move_line_ids_without_package ;
        this.move_line_ids_without_packageDirtyFlag = true ;
    }

    /**
     * 获取 [MOVE_LINE_IDS_WITHOUT_PACKAGE]脏标记
     */
    @JsonIgnore
    public boolean getMove_line_ids_without_packageDirtyFlag(){
        return move_line_ids_without_packageDirtyFlag ;
    }

    /**
     * 获取 [ACTIVITY_DATE_DEADLINE]
     */
    @JsonProperty("activity_date_deadline")
    public Timestamp getActivity_date_deadline(){
        return activity_date_deadline ;
    }

    /**
     * 设置 [ACTIVITY_DATE_DEADLINE]
     */
    @JsonProperty("activity_date_deadline")
    public void setActivity_date_deadline(Timestamp  activity_date_deadline){
        this.activity_date_deadline = activity_date_deadline ;
        this.activity_date_deadlineDirtyFlag = true ;
    }

    /**
     * 获取 [ACTIVITY_DATE_DEADLINE]脏标记
     */
    @JsonIgnore
    public boolean getActivity_date_deadlineDirtyFlag(){
        return activity_date_deadlineDirtyFlag ;
    }

    /**
     * 获取 [ACTIVITY_USER_ID]
     */
    @JsonProperty("activity_user_id")
    public Integer getActivity_user_id(){
        return activity_user_id ;
    }

    /**
     * 设置 [ACTIVITY_USER_ID]
     */
    @JsonProperty("activity_user_id")
    public void setActivity_user_id(Integer  activity_user_id){
        this.activity_user_id = activity_user_id ;
        this.activity_user_idDirtyFlag = true ;
    }

    /**
     * 获取 [ACTIVITY_USER_ID]脏标记
     */
    @JsonIgnore
    public boolean getActivity_user_idDirtyFlag(){
        return activity_user_idDirtyFlag ;
    }

    /**
     * 获取 [ACTIVITY_STATE]
     */
    @JsonProperty("activity_state")
    public String getActivity_state(){
        return activity_state ;
    }

    /**
     * 设置 [ACTIVITY_STATE]
     */
    @JsonProperty("activity_state")
    public void setActivity_state(String  activity_state){
        this.activity_state = activity_state ;
        this.activity_stateDirtyFlag = true ;
    }

    /**
     * 获取 [ACTIVITY_STATE]脏标记
     */
    @JsonIgnore
    public boolean getActivity_stateDirtyFlag(){
        return activity_stateDirtyFlag ;
    }

    /**
     * 获取 [ACTIVITY_IDS]
     */
    @JsonProperty("activity_ids")
    public String getActivity_ids(){
        return activity_ids ;
    }

    /**
     * 设置 [ACTIVITY_IDS]
     */
    @JsonProperty("activity_ids")
    public void setActivity_ids(String  activity_ids){
        this.activity_ids = activity_ids ;
        this.activity_idsDirtyFlag = true ;
    }

    /**
     * 获取 [ACTIVITY_IDS]脏标记
     */
    @JsonIgnore
    public boolean getActivity_idsDirtyFlag(){
        return activity_idsDirtyFlag ;
    }

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

    /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return idDirtyFlag ;
    }

    /**
     * 获取 [ACTIVITY_SUMMARY]
     */
    @JsonProperty("activity_summary")
    public String getActivity_summary(){
        return activity_summary ;
    }

    /**
     * 设置 [ACTIVITY_SUMMARY]
     */
    @JsonProperty("activity_summary")
    public void setActivity_summary(String  activity_summary){
        this.activity_summary = activity_summary ;
        this.activity_summaryDirtyFlag = true ;
    }

    /**
     * 获取 [ACTIVITY_SUMMARY]脏标记
     */
    @JsonIgnore
    public boolean getActivity_summaryDirtyFlag(){
        return activity_summaryDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_HAS_ERROR]
     */
    @JsonProperty("message_has_error")
    public String getMessage_has_error(){
        return message_has_error ;
    }

    /**
     * 设置 [MESSAGE_HAS_ERROR]
     */
    @JsonProperty("message_has_error")
    public void setMessage_has_error(String  message_has_error){
        this.message_has_error = message_has_error ;
        this.message_has_errorDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_HAS_ERROR]脏标记
     */
    @JsonIgnore
    public boolean getMessage_has_errorDirtyFlag(){
        return message_has_errorDirtyFlag ;
    }

    /**
     * 获取 [MOVE_LINE_IDS]
     */
    @JsonProperty("move_line_ids")
    public String getMove_line_ids(){
        return move_line_ids ;
    }

    /**
     * 设置 [MOVE_LINE_IDS]
     */
    @JsonProperty("move_line_ids")
    public void setMove_line_ids(String  move_line_ids){
        this.move_line_ids = move_line_ids ;
        this.move_line_idsDirtyFlag = true ;
    }

    /**
     * 获取 [MOVE_LINE_IDS]脏标记
     */
    @JsonIgnore
    public boolean getMove_line_idsDirtyFlag(){
        return move_line_idsDirtyFlag ;
    }

    /**
     * 获取 [PACKAGE_LEVEL_IDS_DETAILS]
     */
    @JsonProperty("package_level_ids_details")
    public String getPackage_level_ids_details(){
        return package_level_ids_details ;
    }

    /**
     * 设置 [PACKAGE_LEVEL_IDS_DETAILS]
     */
    @JsonProperty("package_level_ids_details")
    public void setPackage_level_ids_details(String  package_level_ids_details){
        this.package_level_ids_details = package_level_ids_details ;
        this.package_level_ids_detailsDirtyFlag = true ;
    }

    /**
     * 获取 [PACKAGE_LEVEL_IDS_DETAILS]脏标记
     */
    @JsonIgnore
    public boolean getPackage_level_ids_detailsDirtyFlag(){
        return package_level_ids_detailsDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_UNREAD]
     */
    @JsonProperty("message_unread")
    public String getMessage_unread(){
        return message_unread ;
    }

    /**
     * 设置 [MESSAGE_UNREAD]
     */
    @JsonProperty("message_unread")
    public void setMessage_unread(String  message_unread){
        this.message_unread = message_unread ;
        this.message_unreadDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_UNREAD]脏标记
     */
    @JsonIgnore
    public boolean getMessage_unreadDirtyFlag(){
        return message_unreadDirtyFlag ;
    }

    /**
     * 获取 [DATE]
     */
    @JsonProperty("date")
    public Timestamp getDate(){
        return date ;
    }

    /**
     * 设置 [DATE]
     */
    @JsonProperty("date")
    public void setDate(Timestamp  date){
        this.date = date ;
        this.dateDirtyFlag = true ;
    }

    /**
     * 获取 [DATE]脏标记
     */
    @JsonIgnore
    public boolean getDateDirtyFlag(){
        return dateDirtyFlag ;
    }

    /**
     * 获取 [DATE_DONE]
     */
    @JsonProperty("date_done")
    public Timestamp getDate_done(){
        return date_done ;
    }

    /**
     * 设置 [DATE_DONE]
     */
    @JsonProperty("date_done")
    public void setDate_done(Timestamp  date_done){
        this.date_done = date_done ;
        this.date_doneDirtyFlag = true ;
    }

    /**
     * 获取 [DATE_DONE]脏标记
     */
    @JsonIgnore
    public boolean getDate_doneDirtyFlag(){
        return date_doneDirtyFlag ;
    }

    /**
     * 获取 [HAS_TRACKING]
     */
    @JsonProperty("has_tracking")
    public String getHas_tracking(){
        return has_tracking ;
    }

    /**
     * 设置 [HAS_TRACKING]
     */
    @JsonProperty("has_tracking")
    public void setHas_tracking(String  has_tracking){
        this.has_tracking = has_tracking ;
        this.has_trackingDirtyFlag = true ;
    }

    /**
     * 获取 [HAS_TRACKING]脏标记
     */
    @JsonIgnore
    public boolean getHas_trackingDirtyFlag(){
        return has_trackingDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_MAIN_ATTACHMENT_ID]
     */
    @JsonProperty("message_main_attachment_id")
    public Integer getMessage_main_attachment_id(){
        return message_main_attachment_id ;
    }

    /**
     * 设置 [MESSAGE_MAIN_ATTACHMENT_ID]
     */
    @JsonProperty("message_main_attachment_id")
    public void setMessage_main_attachment_id(Integer  message_main_attachment_id){
        this.message_main_attachment_id = message_main_attachment_id ;
        this.message_main_attachment_idDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_MAIN_ATTACHMENT_ID]脏标记
     */
    @JsonIgnore
    public boolean getMessage_main_attachment_idDirtyFlag(){
        return message_main_attachment_idDirtyFlag ;
    }

    /**
     * 获取 [STATE]
     */
    @JsonProperty("state")
    public String getState(){
        return state ;
    }

    /**
     * 设置 [STATE]
     */
    @JsonProperty("state")
    public void setState(String  state){
        this.state = state ;
        this.stateDirtyFlag = true ;
    }

    /**
     * 获取 [STATE]脏标记
     */
    @JsonIgnore
    public boolean getStateDirtyFlag(){
        return stateDirtyFlag ;
    }

    /**
     * 获取 [IMMEDIATE_TRANSFER]
     */
    @JsonProperty("immediate_transfer")
    public String getImmediate_transfer(){
        return immediate_transfer ;
    }

    /**
     * 设置 [IMMEDIATE_TRANSFER]
     */
    @JsonProperty("immediate_transfer")
    public void setImmediate_transfer(String  immediate_transfer){
        this.immediate_transfer = immediate_transfer ;
        this.immediate_transferDirtyFlag = true ;
    }

    /**
     * 获取 [IMMEDIATE_TRANSFER]脏标记
     */
    @JsonIgnore
    public boolean getImmediate_transferDirtyFlag(){
        return immediate_transferDirtyFlag ;
    }

    /**
     * 获取 [HAS_SCRAP_MOVE]
     */
    @JsonProperty("has_scrap_move")
    public String getHas_scrap_move(){
        return has_scrap_move ;
    }

    /**
     * 设置 [HAS_SCRAP_MOVE]
     */
    @JsonProperty("has_scrap_move")
    public void setHas_scrap_move(String  has_scrap_move){
        this.has_scrap_move = has_scrap_move ;
        this.has_scrap_moveDirtyFlag = true ;
    }

    /**
     * 获取 [HAS_SCRAP_MOVE]脏标记
     */
    @JsonIgnore
    public boolean getHas_scrap_moveDirtyFlag(){
        return has_scrap_moveDirtyFlag ;
    }

    /**
     * 获取 [MOVE_IDS_WITHOUT_PACKAGE]
     */
    @JsonProperty("move_ids_without_package")
    public String getMove_ids_without_package(){
        return move_ids_without_package ;
    }

    /**
     * 设置 [MOVE_IDS_WITHOUT_PACKAGE]
     */
    @JsonProperty("move_ids_without_package")
    public void setMove_ids_without_package(String  move_ids_without_package){
        this.move_ids_without_package = move_ids_without_package ;
        this.move_ids_without_packageDirtyFlag = true ;
    }

    /**
     * 获取 [MOVE_IDS_WITHOUT_PACKAGE]脏标记
     */
    @JsonIgnore
    public boolean getMove_ids_without_packageDirtyFlag(){
        return move_ids_without_packageDirtyFlag ;
    }

    /**
     * 获取 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return create_date ;
    }

    /**
     * 设置 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return create_dateDirtyFlag ;
    }

    /**
     * 获取 [SHOW_VALIDATE]
     */
    @JsonProperty("show_validate")
    public String getShow_validate(){
        return show_validate ;
    }

    /**
     * 设置 [SHOW_VALIDATE]
     */
    @JsonProperty("show_validate")
    public void setShow_validate(String  show_validate){
        this.show_validate = show_validate ;
        this.show_validateDirtyFlag = true ;
    }

    /**
     * 获取 [SHOW_VALIDATE]脏标记
     */
    @JsonIgnore
    public boolean getShow_validateDirtyFlag(){
        return show_validateDirtyFlag ;
    }

    /**
     * 获取 [NAME]
     */
    @JsonProperty("name")
    public String getName(){
        return name ;
    }

    /**
     * 设置 [NAME]
     */
    @JsonProperty("name")
    public void setName(String  name){
        this.name = name ;
        this.nameDirtyFlag = true ;
    }

    /**
     * 获取 [NAME]脏标记
     */
    @JsonIgnore
    public boolean getNameDirtyFlag(){
        return nameDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_CHANNEL_IDS]
     */
    @JsonProperty("message_channel_ids")
    public String getMessage_channel_ids(){
        return message_channel_ids ;
    }

    /**
     * 设置 [MESSAGE_CHANNEL_IDS]
     */
    @JsonProperty("message_channel_ids")
    public void setMessage_channel_ids(String  message_channel_ids){
        this.message_channel_ids = message_channel_ids ;
        this.message_channel_idsDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_CHANNEL_IDS]脏标记
     */
    @JsonIgnore
    public boolean getMessage_channel_idsDirtyFlag(){
        return message_channel_idsDirtyFlag ;
    }

    /**
     * 获取 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return display_name ;
    }

    /**
     * 设置 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

    /**
     * 获取 [DISPLAY_NAME]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return display_nameDirtyFlag ;
    }

    /**
     * 获取 [PRIORITY]
     */
    @JsonProperty("priority")
    public String getPriority(){
        return priority ;
    }

    /**
     * 设置 [PRIORITY]
     */
    @JsonProperty("priority")
    public void setPriority(String  priority){
        this.priority = priority ;
        this.priorityDirtyFlag = true ;
    }

    /**
     * 获取 [PRIORITY]脏标记
     */
    @JsonIgnore
    public boolean getPriorityDirtyFlag(){
        return priorityDirtyFlag ;
    }

    /**
     * 获取 [MOVE_LINES]
     */
    @JsonProperty("move_lines")
    public String getMove_lines(){
        return move_lines ;
    }

    /**
     * 设置 [MOVE_LINES]
     */
    @JsonProperty("move_lines")
    public void setMove_lines(String  move_lines){
        this.move_lines = move_lines ;
        this.move_linesDirtyFlag = true ;
    }

    /**
     * 获取 [MOVE_LINES]脏标记
     */
    @JsonIgnore
    public boolean getMove_linesDirtyFlag(){
        return move_linesDirtyFlag ;
    }

    /**
     * 获取 [PURCHASE_ID]
     */
    @JsonProperty("purchase_id")
    public Integer getPurchase_id(){
        return purchase_id ;
    }

    /**
     * 设置 [PURCHASE_ID]
     */
    @JsonProperty("purchase_id")
    public void setPurchase_id(Integer  purchase_id){
        this.purchase_id = purchase_id ;
        this.purchase_idDirtyFlag = true ;
    }

    /**
     * 获取 [PURCHASE_ID]脏标记
     */
    @JsonIgnore
    public boolean getPurchase_idDirtyFlag(){
        return purchase_idDirtyFlag ;
    }

    /**
     * 获取 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return write_date ;
    }

    /**
     * 设置 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return write_dateDirtyFlag ;
    }

    /**
     * 获取 [WEBSITE_MESSAGE_IDS]
     */
    @JsonProperty("website_message_ids")
    public String getWebsite_message_ids(){
        return website_message_ids ;
    }

    /**
     * 设置 [WEBSITE_MESSAGE_IDS]
     */
    @JsonProperty("website_message_ids")
    public void setWebsite_message_ids(String  website_message_ids){
        this.website_message_ids = website_message_ids ;
        this.website_message_idsDirtyFlag = true ;
    }

    /**
     * 获取 [WEBSITE_MESSAGE_IDS]脏标记
     */
    @JsonIgnore
    public boolean getWebsite_message_idsDirtyFlag(){
        return website_message_idsDirtyFlag ;
    }

    /**
     * 获取 [SHOW_MARK_AS_TODO]
     */
    @JsonProperty("show_mark_as_todo")
    public String getShow_mark_as_todo(){
        return show_mark_as_todo ;
    }

    /**
     * 设置 [SHOW_MARK_AS_TODO]
     */
    @JsonProperty("show_mark_as_todo")
    public void setShow_mark_as_todo(String  show_mark_as_todo){
        this.show_mark_as_todo = show_mark_as_todo ;
        this.show_mark_as_todoDirtyFlag = true ;
    }

    /**
     * 获取 [SHOW_MARK_AS_TODO]脏标记
     */
    @JsonIgnore
    public boolean getShow_mark_as_todoDirtyFlag(){
        return show_mark_as_todoDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_FOLLOWER_IDS]
     */
    @JsonProperty("message_follower_ids")
    public String getMessage_follower_ids(){
        return message_follower_ids ;
    }

    /**
     * 设置 [MESSAGE_FOLLOWER_IDS]
     */
    @JsonProperty("message_follower_ids")
    public void setMessage_follower_ids(String  message_follower_ids){
        this.message_follower_ids = message_follower_ids ;
        this.message_follower_idsDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_FOLLOWER_IDS]脏标记
     */
    @JsonIgnore
    public boolean getMessage_follower_idsDirtyFlag(){
        return message_follower_idsDirtyFlag ;
    }

    /**
     * 获取 [MOVE_LINE_EXIST]
     */
    @JsonProperty("move_line_exist")
    public String getMove_line_exist(){
        return move_line_exist ;
    }

    /**
     * 设置 [MOVE_LINE_EXIST]
     */
    @JsonProperty("move_line_exist")
    public void setMove_line_exist(String  move_line_exist){
        this.move_line_exist = move_line_exist ;
        this.move_line_existDirtyFlag = true ;
    }

    /**
     * 获取 [MOVE_LINE_EXIST]脏标记
     */
    @JsonIgnore
    public boolean getMove_line_existDirtyFlag(){
        return move_line_existDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_ATTACHMENT_COUNT]
     */
    @JsonProperty("message_attachment_count")
    public Integer getMessage_attachment_count(){
        return message_attachment_count ;
    }

    /**
     * 设置 [MESSAGE_ATTACHMENT_COUNT]
     */
    @JsonProperty("message_attachment_count")
    public void setMessage_attachment_count(Integer  message_attachment_count){
        this.message_attachment_count = message_attachment_count ;
        this.message_attachment_countDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_ATTACHMENT_COUNT]脏标记
     */
    @JsonIgnore
    public boolean getMessage_attachment_countDirtyFlag(){
        return message_attachment_countDirtyFlag ;
    }

    /**
     * 获取 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return __last_update ;
    }

    /**
     * 设置 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

    /**
     * 获取 [__LAST_UPDATE]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return __last_updateDirtyFlag ;
    }

    /**
     * 获取 [SCHEDULED_DATE]
     */
    @JsonProperty("scheduled_date")
    public Timestamp getScheduled_date(){
        return scheduled_date ;
    }

    /**
     * 设置 [SCHEDULED_DATE]
     */
    @JsonProperty("scheduled_date")
    public void setScheduled_date(Timestamp  scheduled_date){
        this.scheduled_date = scheduled_date ;
        this.scheduled_dateDirtyFlag = true ;
    }

    /**
     * 获取 [SCHEDULED_DATE]脏标记
     */
    @JsonIgnore
    public boolean getScheduled_dateDirtyFlag(){
        return scheduled_dateDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_IS_FOLLOWER]
     */
    @JsonProperty("message_is_follower")
    public String getMessage_is_follower(){
        return message_is_follower ;
    }

    /**
     * 设置 [MESSAGE_IS_FOLLOWER]
     */
    @JsonProperty("message_is_follower")
    public void setMessage_is_follower(String  message_is_follower){
        this.message_is_follower = message_is_follower ;
        this.message_is_followerDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_IS_FOLLOWER]脏标记
     */
    @JsonIgnore
    public boolean getMessage_is_followerDirtyFlag(){
        return message_is_followerDirtyFlag ;
    }

    /**
     * 获取 [SHOW_CHECK_AVAILABILITY]
     */
    @JsonProperty("show_check_availability")
    public String getShow_check_availability(){
        return show_check_availability ;
    }

    /**
     * 设置 [SHOW_CHECK_AVAILABILITY]
     */
    @JsonProperty("show_check_availability")
    public void setShow_check_availability(String  show_check_availability){
        this.show_check_availability = show_check_availability ;
        this.show_check_availabilityDirtyFlag = true ;
    }

    /**
     * 获取 [SHOW_CHECK_AVAILABILITY]脏标记
     */
    @JsonIgnore
    public boolean getShow_check_availabilityDirtyFlag(){
        return show_check_availabilityDirtyFlag ;
    }

    /**
     * 获取 [SHOW_LOTS_TEXT]
     */
    @JsonProperty("show_lots_text")
    public String getShow_lots_text(){
        return show_lots_text ;
    }

    /**
     * 设置 [SHOW_LOTS_TEXT]
     */
    @JsonProperty("show_lots_text")
    public void setShow_lots_text(String  show_lots_text){
        this.show_lots_text = show_lots_text ;
        this.show_lots_textDirtyFlag = true ;
    }

    /**
     * 获取 [SHOW_LOTS_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getShow_lots_textDirtyFlag(){
        return show_lots_textDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return write_uid_text ;
    }

    /**
     * 设置 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return write_uid_textDirtyFlag ;
    }

    /**
     * 获取 [BACKORDER_ID_TEXT]
     */
    @JsonProperty("backorder_id_text")
    public String getBackorder_id_text(){
        return backorder_id_text ;
    }

    /**
     * 设置 [BACKORDER_ID_TEXT]
     */
    @JsonProperty("backorder_id_text")
    public void setBackorder_id_text(String  backorder_id_text){
        this.backorder_id_text = backorder_id_text ;
        this.backorder_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [BACKORDER_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getBackorder_id_textDirtyFlag(){
        return backorder_id_textDirtyFlag ;
    }

    /**
     * 获取 [PICKING_TYPE_ID_TEXT]
     */
    @JsonProperty("picking_type_id_text")
    public String getPicking_type_id_text(){
        return picking_type_id_text ;
    }

    /**
     * 设置 [PICKING_TYPE_ID_TEXT]
     */
    @JsonProperty("picking_type_id_text")
    public void setPicking_type_id_text(String  picking_type_id_text){
        this.picking_type_id_text = picking_type_id_text ;
        this.picking_type_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [PICKING_TYPE_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getPicking_type_id_textDirtyFlag(){
        return picking_type_id_textDirtyFlag ;
    }

    /**
     * 获取 [PARTNER_ID_TEXT]
     */
    @JsonProperty("partner_id_text")
    public String getPartner_id_text(){
        return partner_id_text ;
    }

    /**
     * 设置 [PARTNER_ID_TEXT]
     */
    @JsonProperty("partner_id_text")
    public void setPartner_id_text(String  partner_id_text){
        this.partner_id_text = partner_id_text ;
        this.partner_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [PARTNER_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getPartner_id_textDirtyFlag(){
        return partner_id_textDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return create_uid_text ;
    }

    /**
     * 设置 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return create_uid_textDirtyFlag ;
    }

    /**
     * 获取 [LOCATION_ID_TEXT]
     */
    @JsonProperty("location_id_text")
    public String getLocation_id_text(){
        return location_id_text ;
    }

    /**
     * 设置 [LOCATION_ID_TEXT]
     */
    @JsonProperty("location_id_text")
    public void setLocation_id_text(String  location_id_text){
        this.location_id_text = location_id_text ;
        this.location_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [LOCATION_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getLocation_id_textDirtyFlag(){
        return location_id_textDirtyFlag ;
    }

    /**
     * 获取 [LOCATION_DEST_ID_TEXT]
     */
    @JsonProperty("location_dest_id_text")
    public String getLocation_dest_id_text(){
        return location_dest_id_text ;
    }

    /**
     * 设置 [LOCATION_DEST_ID_TEXT]
     */
    @JsonProperty("location_dest_id_text")
    public void setLocation_dest_id_text(String  location_dest_id_text){
        this.location_dest_id_text = location_dest_id_text ;
        this.location_dest_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [LOCATION_DEST_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getLocation_dest_id_textDirtyFlag(){
        return location_dest_id_textDirtyFlag ;
    }

    /**
     * 获取 [PICKING_TYPE_CODE]
     */
    @JsonProperty("picking_type_code")
    public String getPicking_type_code(){
        return picking_type_code ;
    }

    /**
     * 设置 [PICKING_TYPE_CODE]
     */
    @JsonProperty("picking_type_code")
    public void setPicking_type_code(String  picking_type_code){
        this.picking_type_code = picking_type_code ;
        this.picking_type_codeDirtyFlag = true ;
    }

    /**
     * 获取 [PICKING_TYPE_CODE]脏标记
     */
    @JsonIgnore
    public boolean getPicking_type_codeDirtyFlag(){
        return picking_type_codeDirtyFlag ;
    }

    /**
     * 获取 [PICKING_TYPE_ENTIRE_PACKS]
     */
    @JsonProperty("picking_type_entire_packs")
    public String getPicking_type_entire_packs(){
        return picking_type_entire_packs ;
    }

    /**
     * 设置 [PICKING_TYPE_ENTIRE_PACKS]
     */
    @JsonProperty("picking_type_entire_packs")
    public void setPicking_type_entire_packs(String  picking_type_entire_packs){
        this.picking_type_entire_packs = picking_type_entire_packs ;
        this.picking_type_entire_packsDirtyFlag = true ;
    }

    /**
     * 获取 [PICKING_TYPE_ENTIRE_PACKS]脏标记
     */
    @JsonIgnore
    public boolean getPicking_type_entire_packsDirtyFlag(){
        return picking_type_entire_packsDirtyFlag ;
    }

    /**
     * 获取 [OWNER_ID_TEXT]
     */
    @JsonProperty("owner_id_text")
    public String getOwner_id_text(){
        return owner_id_text ;
    }

    /**
     * 设置 [OWNER_ID_TEXT]
     */
    @JsonProperty("owner_id_text")
    public void setOwner_id_text(String  owner_id_text){
        this.owner_id_text = owner_id_text ;
        this.owner_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [OWNER_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getOwner_id_textDirtyFlag(){
        return owner_id_textDirtyFlag ;
    }

    /**
     * 获取 [SALE_ID_TEXT]
     */
    @JsonProperty("sale_id_text")
    public String getSale_id_text(){
        return sale_id_text ;
    }

    /**
     * 设置 [SALE_ID_TEXT]
     */
    @JsonProperty("sale_id_text")
    public void setSale_id_text(String  sale_id_text){
        this.sale_id_text = sale_id_text ;
        this.sale_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [SALE_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getSale_id_textDirtyFlag(){
        return sale_id_textDirtyFlag ;
    }

    /**
     * 获取 [COMPANY_ID_TEXT]
     */
    @JsonProperty("company_id_text")
    public String getCompany_id_text(){
        return company_id_text ;
    }

    /**
     * 设置 [COMPANY_ID_TEXT]
     */
    @JsonProperty("company_id_text")
    public void setCompany_id_text(String  company_id_text){
        this.company_id_text = company_id_text ;
        this.company_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [COMPANY_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getCompany_id_textDirtyFlag(){
        return company_id_textDirtyFlag ;
    }

    /**
     * 获取 [PICKING_TYPE_ID]
     */
    @JsonProperty("picking_type_id")
    public Integer getPicking_type_id(){
        return picking_type_id ;
    }

    /**
     * 设置 [PICKING_TYPE_ID]
     */
    @JsonProperty("picking_type_id")
    public void setPicking_type_id(Integer  picking_type_id){
        this.picking_type_id = picking_type_id ;
        this.picking_type_idDirtyFlag = true ;
    }

    /**
     * 获取 [PICKING_TYPE_ID]脏标记
     */
    @JsonIgnore
    public boolean getPicking_type_idDirtyFlag(){
        return picking_type_idDirtyFlag ;
    }

    /**
     * 获取 [SALE_ID]
     */
    @JsonProperty("sale_id")
    public Integer getSale_id(){
        return sale_id ;
    }

    /**
     * 设置 [SALE_ID]
     */
    @JsonProperty("sale_id")
    public void setSale_id(Integer  sale_id){
        this.sale_id = sale_id ;
        this.sale_idDirtyFlag = true ;
    }

    /**
     * 获取 [SALE_ID]脏标记
     */
    @JsonIgnore
    public boolean getSale_idDirtyFlag(){
        return sale_idDirtyFlag ;
    }

    /**
     * 获取 [LOCATION_DEST_ID]
     */
    @JsonProperty("location_dest_id")
    public Integer getLocation_dest_id(){
        return location_dest_id ;
    }

    /**
     * 设置 [LOCATION_DEST_ID]
     */
    @JsonProperty("location_dest_id")
    public void setLocation_dest_id(Integer  location_dest_id){
        this.location_dest_id = location_dest_id ;
        this.location_dest_idDirtyFlag = true ;
    }

    /**
     * 获取 [LOCATION_DEST_ID]脏标记
     */
    @JsonIgnore
    public boolean getLocation_dest_idDirtyFlag(){
        return location_dest_idDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return create_uid ;
    }

    /**
     * 设置 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return create_uidDirtyFlag ;
    }

    /**
     * 获取 [PARTNER_ID]
     */
    @JsonProperty("partner_id")
    public Integer getPartner_id(){
        return partner_id ;
    }

    /**
     * 设置 [PARTNER_ID]
     */
    @JsonProperty("partner_id")
    public void setPartner_id(Integer  partner_id){
        this.partner_id = partner_id ;
        this.partner_idDirtyFlag = true ;
    }

    /**
     * 获取 [PARTNER_ID]脏标记
     */
    @JsonIgnore
    public boolean getPartner_idDirtyFlag(){
        return partner_idDirtyFlag ;
    }

    /**
     * 获取 [OWNER_ID]
     */
    @JsonProperty("owner_id")
    public Integer getOwner_id(){
        return owner_id ;
    }

    /**
     * 设置 [OWNER_ID]
     */
    @JsonProperty("owner_id")
    public void setOwner_id(Integer  owner_id){
        this.owner_id = owner_id ;
        this.owner_idDirtyFlag = true ;
    }

    /**
     * 获取 [OWNER_ID]脏标记
     */
    @JsonIgnore
    public boolean getOwner_idDirtyFlag(){
        return owner_idDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return write_uid ;
    }

    /**
     * 设置 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return write_uidDirtyFlag ;
    }

    /**
     * 获取 [COMPANY_ID]
     */
    @JsonProperty("company_id")
    public Integer getCompany_id(){
        return company_id ;
    }

    /**
     * 设置 [COMPANY_ID]
     */
    @JsonProperty("company_id")
    public void setCompany_id(Integer  company_id){
        this.company_id = company_id ;
        this.company_idDirtyFlag = true ;
    }

    /**
     * 获取 [COMPANY_ID]脏标记
     */
    @JsonIgnore
    public boolean getCompany_idDirtyFlag(){
        return company_idDirtyFlag ;
    }

    /**
     * 获取 [LOCATION_ID]
     */
    @JsonProperty("location_id")
    public Integer getLocation_id(){
        return location_id ;
    }

    /**
     * 设置 [LOCATION_ID]
     */
    @JsonProperty("location_id")
    public void setLocation_id(Integer  location_id){
        this.location_id = location_id ;
        this.location_idDirtyFlag = true ;
    }

    /**
     * 获取 [LOCATION_ID]脏标记
     */
    @JsonIgnore
    public boolean getLocation_idDirtyFlag(){
        return location_idDirtyFlag ;
    }

    /**
     * 获取 [BACKORDER_ID]
     */
    @JsonProperty("backorder_id")
    public Integer getBackorder_id(){
        return backorder_id ;
    }

    /**
     * 设置 [BACKORDER_ID]
     */
    @JsonProperty("backorder_id")
    public void setBackorder_id(Integer  backorder_id){
        this.backorder_id = backorder_id ;
        this.backorder_idDirtyFlag = true ;
    }

    /**
     * 获取 [BACKORDER_ID]脏标记
     */
    @JsonIgnore
    public boolean getBackorder_idDirtyFlag(){
        return backorder_idDirtyFlag ;
    }



    public Stock_picking toDO() {
        Stock_picking srfdomain = new Stock_picking();
        if(getPackage_level_idsDirtyFlag())
            srfdomain.setPackage_level_ids(package_level_ids);
        if(getNoteDirtyFlag())
            srfdomain.setNote(note);
        if(getWebsite_idDirtyFlag())
            srfdomain.setWebsite_id(website_id);
        if(getActivity_type_idDirtyFlag())
            srfdomain.setActivity_type_id(activity_type_id);
        if(getPrintedDirtyFlag())
            srfdomain.setPrinted(printed);
        if(getMessage_needaction_counterDirtyFlag())
            srfdomain.setMessage_needaction_counter(message_needaction_counter);
        if(getProduct_idDirtyFlag())
            srfdomain.setProduct_id(product_id);
        if(getMessage_partner_idsDirtyFlag())
            srfdomain.setMessage_partner_ids(message_partner_ids);
        if(getMessage_unread_counterDirtyFlag())
            srfdomain.setMessage_unread_counter(message_unread_counter);
        if(getMove_typeDirtyFlag())
            srfdomain.setMove_type(move_type);
        if(getBackorder_idsDirtyFlag())
            srfdomain.setBackorder_ids(backorder_ids);
        if(getMessage_needactionDirtyFlag())
            srfdomain.setMessage_needaction(message_needaction);
        if(getMessage_idsDirtyFlag())
            srfdomain.setMessage_ids(message_ids);
        if(getHas_packagesDirtyFlag())
            srfdomain.setHas_packages(has_packages);
        if(getOriginDirtyFlag())
            srfdomain.setOrigin(origin);
        if(getIs_lockedDirtyFlag())
            srfdomain.setIs_locked(is_locked);
        if(getShow_operationsDirtyFlag())
            srfdomain.setShow_operations(show_operations);
        if(getGroup_idDirtyFlag())
            srfdomain.setGroup_id(group_id);
        if(getMessage_has_error_counterDirtyFlag())
            srfdomain.setMessage_has_error_counter(message_has_error_counter);
        if(getMove_line_ids_without_packageDirtyFlag())
            srfdomain.setMove_line_ids_without_package(move_line_ids_without_package);
        if(getActivity_date_deadlineDirtyFlag())
            srfdomain.setActivity_date_deadline(activity_date_deadline);
        if(getActivity_user_idDirtyFlag())
            srfdomain.setActivity_user_id(activity_user_id);
        if(getActivity_stateDirtyFlag())
            srfdomain.setActivity_state(activity_state);
        if(getActivity_idsDirtyFlag())
            srfdomain.setActivity_ids(activity_ids);
        if(getIdDirtyFlag())
            srfdomain.setId(id);
        if(getActivity_summaryDirtyFlag())
            srfdomain.setActivity_summary(activity_summary);
        if(getMessage_has_errorDirtyFlag())
            srfdomain.setMessage_has_error(message_has_error);
        if(getMove_line_idsDirtyFlag())
            srfdomain.setMove_line_ids(move_line_ids);
        if(getPackage_level_ids_detailsDirtyFlag())
            srfdomain.setPackage_level_ids_details(package_level_ids_details);
        if(getMessage_unreadDirtyFlag())
            srfdomain.setMessage_unread(message_unread);
        if(getDateDirtyFlag())
            srfdomain.setDate(date);
        if(getDate_doneDirtyFlag())
            srfdomain.setDate_done(date_done);
        if(getHas_trackingDirtyFlag())
            srfdomain.setHas_tracking(has_tracking);
        if(getMessage_main_attachment_idDirtyFlag())
            srfdomain.setMessage_main_attachment_id(message_main_attachment_id);
        if(getStateDirtyFlag())
            srfdomain.setState(state);
        if(getImmediate_transferDirtyFlag())
            srfdomain.setImmediate_transfer(immediate_transfer);
        if(getHas_scrap_moveDirtyFlag())
            srfdomain.setHas_scrap_move(has_scrap_move);
        if(getMove_ids_without_packageDirtyFlag())
            srfdomain.setMove_ids_without_package(move_ids_without_package);
        if(getCreate_dateDirtyFlag())
            srfdomain.setCreate_date(create_date);
        if(getShow_validateDirtyFlag())
            srfdomain.setShow_validate(show_validate);
        if(getNameDirtyFlag())
            srfdomain.setName(name);
        if(getMessage_channel_idsDirtyFlag())
            srfdomain.setMessage_channel_ids(message_channel_ids);
        if(getDisplay_nameDirtyFlag())
            srfdomain.setDisplay_name(display_name);
        if(getPriorityDirtyFlag())
            srfdomain.setPriority(priority);
        if(getMove_linesDirtyFlag())
            srfdomain.setMove_lines(move_lines);
        if(getPurchase_idDirtyFlag())
            srfdomain.setPurchase_id(purchase_id);
        if(getWrite_dateDirtyFlag())
            srfdomain.setWrite_date(write_date);
        if(getWebsite_message_idsDirtyFlag())
            srfdomain.setWebsite_message_ids(website_message_ids);
        if(getShow_mark_as_todoDirtyFlag())
            srfdomain.setShow_mark_as_todo(show_mark_as_todo);
        if(getMessage_follower_idsDirtyFlag())
            srfdomain.setMessage_follower_ids(message_follower_ids);
        if(getMove_line_existDirtyFlag())
            srfdomain.setMove_line_exist(move_line_exist);
        if(getMessage_attachment_countDirtyFlag())
            srfdomain.setMessage_attachment_count(message_attachment_count);
        if(get__last_updateDirtyFlag())
            srfdomain.set__last_update(__last_update);
        if(getScheduled_dateDirtyFlag())
            srfdomain.setScheduled_date(scheduled_date);
        if(getMessage_is_followerDirtyFlag())
            srfdomain.setMessage_is_follower(message_is_follower);
        if(getShow_check_availabilityDirtyFlag())
            srfdomain.setShow_check_availability(show_check_availability);
        if(getShow_lots_textDirtyFlag())
            srfdomain.setShow_lots_text(show_lots_text);
        if(getWrite_uid_textDirtyFlag())
            srfdomain.setWrite_uid_text(write_uid_text);
        if(getBackorder_id_textDirtyFlag())
            srfdomain.setBackorder_id_text(backorder_id_text);
        if(getPicking_type_id_textDirtyFlag())
            srfdomain.setPicking_type_id_text(picking_type_id_text);
        if(getPartner_id_textDirtyFlag())
            srfdomain.setPartner_id_text(partner_id_text);
        if(getCreate_uid_textDirtyFlag())
            srfdomain.setCreate_uid_text(create_uid_text);
        if(getLocation_id_textDirtyFlag())
            srfdomain.setLocation_id_text(location_id_text);
        if(getLocation_dest_id_textDirtyFlag())
            srfdomain.setLocation_dest_id_text(location_dest_id_text);
        if(getPicking_type_codeDirtyFlag())
            srfdomain.setPicking_type_code(picking_type_code);
        if(getPicking_type_entire_packsDirtyFlag())
            srfdomain.setPicking_type_entire_packs(picking_type_entire_packs);
        if(getOwner_id_textDirtyFlag())
            srfdomain.setOwner_id_text(owner_id_text);
        if(getSale_id_textDirtyFlag())
            srfdomain.setSale_id_text(sale_id_text);
        if(getCompany_id_textDirtyFlag())
            srfdomain.setCompany_id_text(company_id_text);
        if(getPicking_type_idDirtyFlag())
            srfdomain.setPicking_type_id(picking_type_id);
        if(getSale_idDirtyFlag())
            srfdomain.setSale_id(sale_id);
        if(getLocation_dest_idDirtyFlag())
            srfdomain.setLocation_dest_id(location_dest_id);
        if(getCreate_uidDirtyFlag())
            srfdomain.setCreate_uid(create_uid);
        if(getPartner_idDirtyFlag())
            srfdomain.setPartner_id(partner_id);
        if(getOwner_idDirtyFlag())
            srfdomain.setOwner_id(owner_id);
        if(getWrite_uidDirtyFlag())
            srfdomain.setWrite_uid(write_uid);
        if(getCompany_idDirtyFlag())
            srfdomain.setCompany_id(company_id);
        if(getLocation_idDirtyFlag())
            srfdomain.setLocation_id(location_id);
        if(getBackorder_idDirtyFlag())
            srfdomain.setBackorder_id(backorder_id);

        return srfdomain;
    }

    public void fromDO(Stock_picking srfdomain) {
        if(srfdomain == null )
          return ;
        if(srfdomain.getPackage_level_idsDirtyFlag())
            this.setPackage_level_ids(srfdomain.getPackage_level_ids());
        if(srfdomain.getNoteDirtyFlag())
            this.setNote(srfdomain.getNote());
        if(srfdomain.getWebsite_idDirtyFlag())
            this.setWebsite_id(srfdomain.getWebsite_id());
        if(srfdomain.getActivity_type_idDirtyFlag())
            this.setActivity_type_id(srfdomain.getActivity_type_id());
        if(srfdomain.getPrintedDirtyFlag())
            this.setPrinted(srfdomain.getPrinted());
        if(srfdomain.getMessage_needaction_counterDirtyFlag())
            this.setMessage_needaction_counter(srfdomain.getMessage_needaction_counter());
        if(srfdomain.getProduct_idDirtyFlag())
            this.setProduct_id(srfdomain.getProduct_id());
        if(srfdomain.getMessage_partner_idsDirtyFlag())
            this.setMessage_partner_ids(srfdomain.getMessage_partner_ids());
        if(srfdomain.getMessage_unread_counterDirtyFlag())
            this.setMessage_unread_counter(srfdomain.getMessage_unread_counter());
        if(srfdomain.getMove_typeDirtyFlag())
            this.setMove_type(srfdomain.getMove_type());
        if(srfdomain.getBackorder_idsDirtyFlag())
            this.setBackorder_ids(srfdomain.getBackorder_ids());
        if(srfdomain.getMessage_needactionDirtyFlag())
            this.setMessage_needaction(srfdomain.getMessage_needaction());
        if(srfdomain.getMessage_idsDirtyFlag())
            this.setMessage_ids(srfdomain.getMessage_ids());
        if(srfdomain.getHas_packagesDirtyFlag())
            this.setHas_packages(srfdomain.getHas_packages());
        if(srfdomain.getOriginDirtyFlag())
            this.setOrigin(srfdomain.getOrigin());
        if(srfdomain.getIs_lockedDirtyFlag())
            this.setIs_locked(srfdomain.getIs_locked());
        if(srfdomain.getShow_operationsDirtyFlag())
            this.setShow_operations(srfdomain.getShow_operations());
        if(srfdomain.getGroup_idDirtyFlag())
            this.setGroup_id(srfdomain.getGroup_id());
        if(srfdomain.getMessage_has_error_counterDirtyFlag())
            this.setMessage_has_error_counter(srfdomain.getMessage_has_error_counter());
        if(srfdomain.getMove_line_ids_without_packageDirtyFlag())
            this.setMove_line_ids_without_package(srfdomain.getMove_line_ids_without_package());
        if(srfdomain.getActivity_date_deadlineDirtyFlag())
            this.setActivity_date_deadline(srfdomain.getActivity_date_deadline());
        if(srfdomain.getActivity_user_idDirtyFlag())
            this.setActivity_user_id(srfdomain.getActivity_user_id());
        if(srfdomain.getActivity_stateDirtyFlag())
            this.setActivity_state(srfdomain.getActivity_state());
        if(srfdomain.getActivity_idsDirtyFlag())
            this.setActivity_ids(srfdomain.getActivity_ids());
        if(srfdomain.getIdDirtyFlag())
            this.setId(srfdomain.getId());
        if(srfdomain.getActivity_summaryDirtyFlag())
            this.setActivity_summary(srfdomain.getActivity_summary());
        if(srfdomain.getMessage_has_errorDirtyFlag())
            this.setMessage_has_error(srfdomain.getMessage_has_error());
        if(srfdomain.getMove_line_idsDirtyFlag())
            this.setMove_line_ids(srfdomain.getMove_line_ids());
        if(srfdomain.getPackage_level_ids_detailsDirtyFlag())
            this.setPackage_level_ids_details(srfdomain.getPackage_level_ids_details());
        if(srfdomain.getMessage_unreadDirtyFlag())
            this.setMessage_unread(srfdomain.getMessage_unread());
        if(srfdomain.getDateDirtyFlag())
            this.setDate(srfdomain.getDate());
        if(srfdomain.getDate_doneDirtyFlag())
            this.setDate_done(srfdomain.getDate_done());
        if(srfdomain.getHas_trackingDirtyFlag())
            this.setHas_tracking(srfdomain.getHas_tracking());
        if(srfdomain.getMessage_main_attachment_idDirtyFlag())
            this.setMessage_main_attachment_id(srfdomain.getMessage_main_attachment_id());
        if(srfdomain.getStateDirtyFlag())
            this.setState(srfdomain.getState());
        if(srfdomain.getImmediate_transferDirtyFlag())
            this.setImmediate_transfer(srfdomain.getImmediate_transfer());
        if(srfdomain.getHas_scrap_moveDirtyFlag())
            this.setHas_scrap_move(srfdomain.getHas_scrap_move());
        if(srfdomain.getMove_ids_without_packageDirtyFlag())
            this.setMove_ids_without_package(srfdomain.getMove_ids_without_package());
        if(srfdomain.getCreate_dateDirtyFlag())
            this.setCreate_date(srfdomain.getCreate_date());
        if(srfdomain.getShow_validateDirtyFlag())
            this.setShow_validate(srfdomain.getShow_validate());
        if(srfdomain.getNameDirtyFlag())
            this.setName(srfdomain.getName());
        if(srfdomain.getMessage_channel_idsDirtyFlag())
            this.setMessage_channel_ids(srfdomain.getMessage_channel_ids());
        if(srfdomain.getDisplay_nameDirtyFlag())
            this.setDisplay_name(srfdomain.getDisplay_name());
        if(srfdomain.getPriorityDirtyFlag())
            this.setPriority(srfdomain.getPriority());
        if(srfdomain.getMove_linesDirtyFlag())
            this.setMove_lines(srfdomain.getMove_lines());
        if(srfdomain.getPurchase_idDirtyFlag())
            this.setPurchase_id(srfdomain.getPurchase_id());
        if(srfdomain.getWrite_dateDirtyFlag())
            this.setWrite_date(srfdomain.getWrite_date());
        if(srfdomain.getWebsite_message_idsDirtyFlag())
            this.setWebsite_message_ids(srfdomain.getWebsite_message_ids());
        if(srfdomain.getShow_mark_as_todoDirtyFlag())
            this.setShow_mark_as_todo(srfdomain.getShow_mark_as_todo());
        if(srfdomain.getMessage_follower_idsDirtyFlag())
            this.setMessage_follower_ids(srfdomain.getMessage_follower_ids());
        if(srfdomain.getMove_line_existDirtyFlag())
            this.setMove_line_exist(srfdomain.getMove_line_exist());
        if(srfdomain.getMessage_attachment_countDirtyFlag())
            this.setMessage_attachment_count(srfdomain.getMessage_attachment_count());
        if(srfdomain.get__last_updateDirtyFlag())
            this.set__last_update(srfdomain.get__last_update());
        if(srfdomain.getScheduled_dateDirtyFlag())
            this.setScheduled_date(srfdomain.getScheduled_date());
        if(srfdomain.getMessage_is_followerDirtyFlag())
            this.setMessage_is_follower(srfdomain.getMessage_is_follower());
        if(srfdomain.getShow_check_availabilityDirtyFlag())
            this.setShow_check_availability(srfdomain.getShow_check_availability());
        if(srfdomain.getShow_lots_textDirtyFlag())
            this.setShow_lots_text(srfdomain.getShow_lots_text());
        if(srfdomain.getWrite_uid_textDirtyFlag())
            this.setWrite_uid_text(srfdomain.getWrite_uid_text());
        if(srfdomain.getBackorder_id_textDirtyFlag())
            this.setBackorder_id_text(srfdomain.getBackorder_id_text());
        if(srfdomain.getPicking_type_id_textDirtyFlag())
            this.setPicking_type_id_text(srfdomain.getPicking_type_id_text());
        if(srfdomain.getPartner_id_textDirtyFlag())
            this.setPartner_id_text(srfdomain.getPartner_id_text());
        if(srfdomain.getCreate_uid_textDirtyFlag())
            this.setCreate_uid_text(srfdomain.getCreate_uid_text());
        if(srfdomain.getLocation_id_textDirtyFlag())
            this.setLocation_id_text(srfdomain.getLocation_id_text());
        if(srfdomain.getLocation_dest_id_textDirtyFlag())
            this.setLocation_dest_id_text(srfdomain.getLocation_dest_id_text());
        if(srfdomain.getPicking_type_codeDirtyFlag())
            this.setPicking_type_code(srfdomain.getPicking_type_code());
        if(srfdomain.getPicking_type_entire_packsDirtyFlag())
            this.setPicking_type_entire_packs(srfdomain.getPicking_type_entire_packs());
        if(srfdomain.getOwner_id_textDirtyFlag())
            this.setOwner_id_text(srfdomain.getOwner_id_text());
        if(srfdomain.getSale_id_textDirtyFlag())
            this.setSale_id_text(srfdomain.getSale_id_text());
        if(srfdomain.getCompany_id_textDirtyFlag())
            this.setCompany_id_text(srfdomain.getCompany_id_text());
        if(srfdomain.getPicking_type_idDirtyFlag())
            this.setPicking_type_id(srfdomain.getPicking_type_id());
        if(srfdomain.getSale_idDirtyFlag())
            this.setSale_id(srfdomain.getSale_id());
        if(srfdomain.getLocation_dest_idDirtyFlag())
            this.setLocation_dest_id(srfdomain.getLocation_dest_id());
        if(srfdomain.getCreate_uidDirtyFlag())
            this.setCreate_uid(srfdomain.getCreate_uid());
        if(srfdomain.getPartner_idDirtyFlag())
            this.setPartner_id(srfdomain.getPartner_id());
        if(srfdomain.getOwner_idDirtyFlag())
            this.setOwner_id(srfdomain.getOwner_id());
        if(srfdomain.getWrite_uidDirtyFlag())
            this.setWrite_uid(srfdomain.getWrite_uid());
        if(srfdomain.getCompany_idDirtyFlag())
            this.setCompany_id(srfdomain.getCompany_id());
        if(srfdomain.getLocation_idDirtyFlag())
            this.setLocation_id(srfdomain.getLocation_id());
        if(srfdomain.getBackorder_idDirtyFlag())
            this.setBackorder_id(srfdomain.getBackorder_id());

    }

    public List<Stock_pickingDTO> fromDOPage(List<Stock_picking> poPage)   {
        if(poPage == null)
            return null;
        List<Stock_pickingDTO> dtos=new ArrayList<Stock_pickingDTO>();
        for(Stock_picking domain : poPage) {
            Stock_pickingDTO dto = new Stock_pickingDTO();
            dto.fromDO(domain);
            dtos.add(dto);
        }
        return dtos;
    }
}

