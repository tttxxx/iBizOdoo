package cn.ibizlab.odoo.service.odoo_stock.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_stock.dto.Stock_warn_insufficient_qty_unbuildDTO;
import cn.ibizlab.odoo.core.odoo_stock.domain.Stock_warn_insufficient_qty_unbuild;
import cn.ibizlab.odoo.core.odoo_stock.service.IStock_warn_insufficient_qty_unbuildService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_stock.filter.Stock_warn_insufficient_qty_unbuildSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Stock_warn_insufficient_qty_unbuild" })
@RestController
@RequestMapping("")
public class Stock_warn_insufficient_qty_unbuildResource {

    @Autowired
    private IStock_warn_insufficient_qty_unbuildService stock_warn_insufficient_qty_unbuildService;

    public IStock_warn_insufficient_qty_unbuildService getStock_warn_insufficient_qty_unbuildService() {
        return this.stock_warn_insufficient_qty_unbuildService;
    }

    @ApiOperation(value = "更新数据", tags = {"Stock_warn_insufficient_qty_unbuild" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_stock/stock_warn_insufficient_qty_unbuilds/{stock_warn_insufficient_qty_unbuild_id}")

    public ResponseEntity<Stock_warn_insufficient_qty_unbuildDTO> update(@PathVariable("stock_warn_insufficient_qty_unbuild_id") Integer stock_warn_insufficient_qty_unbuild_id, @RequestBody Stock_warn_insufficient_qty_unbuildDTO stock_warn_insufficient_qty_unbuilddto) {
		Stock_warn_insufficient_qty_unbuild domain = stock_warn_insufficient_qty_unbuilddto.toDO();
        domain.setId(stock_warn_insufficient_qty_unbuild_id);
		stock_warn_insufficient_qty_unbuildService.update(domain);
		Stock_warn_insufficient_qty_unbuildDTO dto = new Stock_warn_insufficient_qty_unbuildDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批更新数据", tags = {"Stock_warn_insufficient_qty_unbuild" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_stock/stock_warn_insufficient_qty_unbuilds/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Stock_warn_insufficient_qty_unbuildDTO> stock_warn_insufficient_qty_unbuilddtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "批删除数据", tags = {"Stock_warn_insufficient_qty_unbuild" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_stock/stock_warn_insufficient_qty_unbuilds/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Stock_warn_insufficient_qty_unbuildDTO> stock_warn_insufficient_qty_unbuilddtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "建立数据", tags = {"Stock_warn_insufficient_qty_unbuild" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_stock/stock_warn_insufficient_qty_unbuilds")

    public ResponseEntity<Stock_warn_insufficient_qty_unbuildDTO> create(@RequestBody Stock_warn_insufficient_qty_unbuildDTO stock_warn_insufficient_qty_unbuilddto) {
        Stock_warn_insufficient_qty_unbuildDTO dto = new Stock_warn_insufficient_qty_unbuildDTO();
        Stock_warn_insufficient_qty_unbuild domain = stock_warn_insufficient_qty_unbuilddto.toDO();
		stock_warn_insufficient_qty_unbuildService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取数据", tags = {"Stock_warn_insufficient_qty_unbuild" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_stock/stock_warn_insufficient_qty_unbuilds/{stock_warn_insufficient_qty_unbuild_id}")
    public ResponseEntity<Stock_warn_insufficient_qty_unbuildDTO> get(@PathVariable("stock_warn_insufficient_qty_unbuild_id") Integer stock_warn_insufficient_qty_unbuild_id) {
        Stock_warn_insufficient_qty_unbuildDTO dto = new Stock_warn_insufficient_qty_unbuildDTO();
        Stock_warn_insufficient_qty_unbuild domain = stock_warn_insufficient_qty_unbuildService.get(stock_warn_insufficient_qty_unbuild_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "删除数据", tags = {"Stock_warn_insufficient_qty_unbuild" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_stock/stock_warn_insufficient_qty_unbuilds/{stock_warn_insufficient_qty_unbuild_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("stock_warn_insufficient_qty_unbuild_id") Integer stock_warn_insufficient_qty_unbuild_id) {
        Stock_warn_insufficient_qty_unbuildDTO stock_warn_insufficient_qty_unbuilddto = new Stock_warn_insufficient_qty_unbuildDTO();
		Stock_warn_insufficient_qty_unbuild domain = new Stock_warn_insufficient_qty_unbuild();
		stock_warn_insufficient_qty_unbuilddto.setId(stock_warn_insufficient_qty_unbuild_id);
		domain.setId(stock_warn_insufficient_qty_unbuild_id);
        Boolean rst = stock_warn_insufficient_qty_unbuildService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "批建立数据", tags = {"Stock_warn_insufficient_qty_unbuild" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_stock/stock_warn_insufficient_qty_unbuilds/createBatch")
    public ResponseEntity<Boolean> createBatchStock_warn_insufficient_qty_unbuild(@RequestBody List<Stock_warn_insufficient_qty_unbuildDTO> stock_warn_insufficient_qty_unbuilddtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Stock_warn_insufficient_qty_unbuild" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_stock/stock_warn_insufficient_qty_unbuilds/fetchdefault")
	public ResponseEntity<Page<Stock_warn_insufficient_qty_unbuildDTO>> fetchDefault(Stock_warn_insufficient_qty_unbuildSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Stock_warn_insufficient_qty_unbuildDTO> list = new ArrayList<Stock_warn_insufficient_qty_unbuildDTO>();
        
        Page<Stock_warn_insufficient_qty_unbuild> domains = stock_warn_insufficient_qty_unbuildService.searchDefault(context) ;
        for(Stock_warn_insufficient_qty_unbuild stock_warn_insufficient_qty_unbuild : domains.getContent()){
            Stock_warn_insufficient_qty_unbuildDTO dto = new Stock_warn_insufficient_qty_unbuildDTO();
            dto.fromDO(stock_warn_insufficient_qty_unbuild);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
