package cn.ibizlab.odoo.service.odoo_stock.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.core.odoo_stock.valuerule.anno.stock_warehouse_orderpoint.*;
import cn.ibizlab.odoo.core.odoo_stock.domain.Stock_warehouse_orderpoint;
import org.springframework.cglib.beans.BeanCopier;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 服务DTO对象[Stock_warehouse_orderpointDTO]
 */
public class Stock_warehouse_orderpointDTO implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [ACTIVE]
     *
     */
    @Stock_warehouse_orderpointActiveDefault(info = "默认规则")
    private String active;

    @JsonIgnore
    private boolean activeDirtyFlag;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @Stock_warehouse_orderpoint__last_updateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp __last_update;

    @JsonIgnore
    private boolean __last_updateDirtyFlag;

    /**
     * 属性 [LEAD_DAYS]
     *
     */
    @Stock_warehouse_orderpointLead_daysDefault(info = "默认规则")
    private Integer lead_days;

    @JsonIgnore
    private boolean lead_daysDirtyFlag;

    /**
     * 属性 [WRITE_DATE]
     *
     */
    @Stock_warehouse_orderpointWrite_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp write_date;

    @JsonIgnore
    private boolean write_dateDirtyFlag;

    /**
     * 属性 [LEAD_TYPE]
     *
     */
    @Stock_warehouse_orderpointLead_typeDefault(info = "默认规则")
    private String lead_type;

    @JsonIgnore
    private boolean lead_typeDirtyFlag;

    /**
     * 属性 [GROUP_ID]
     *
     */
    @Stock_warehouse_orderpointGroup_idDefault(info = "默认规则")
    private Integer group_id;

    @JsonIgnore
    private boolean group_idDirtyFlag;

    /**
     * 属性 [PRODUCT_MAX_QTY]
     *
     */
    @Stock_warehouse_orderpointProduct_max_qtyDefault(info = "默认规则")
    private Double product_max_qty;

    @JsonIgnore
    private boolean product_max_qtyDirtyFlag;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @Stock_warehouse_orderpointDisplay_nameDefault(info = "默认规则")
    private String display_name;

    @JsonIgnore
    private boolean display_nameDirtyFlag;

    /**
     * 属性 [QTY_MULTIPLE]
     *
     */
    @Stock_warehouse_orderpointQty_multipleDefault(info = "默认规则")
    private Double qty_multiple;

    @JsonIgnore
    private boolean qty_multipleDirtyFlag;

    /**
     * 属性 [PRODUCT_MIN_QTY]
     *
     */
    @Stock_warehouse_orderpointProduct_min_qtyDefault(info = "默认规则")
    private Double product_min_qty;

    @JsonIgnore
    private boolean product_min_qtyDirtyFlag;

    /**
     * 属性 [ID]
     *
     */
    @Stock_warehouse_orderpointIdDefault(info = "默认规则")
    private Integer id;

    @JsonIgnore
    private boolean idDirtyFlag;

    /**
     * 属性 [NAME]
     *
     */
    @Stock_warehouse_orderpointNameDefault(info = "默认规则")
    private String name;

    @JsonIgnore
    private boolean nameDirtyFlag;

    /**
     * 属性 [CREATE_DATE]
     *
     */
    @Stock_warehouse_orderpointCreate_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp create_date;

    @JsonIgnore
    private boolean create_dateDirtyFlag;

    /**
     * 属性 [WAREHOUSE_ID_TEXT]
     *
     */
    @Stock_warehouse_orderpointWarehouse_id_textDefault(info = "默认规则")
    private String warehouse_id_text;

    @JsonIgnore
    private boolean warehouse_id_textDirtyFlag;

    /**
     * 属性 [PRODUCT_ID_TEXT]
     *
     */
    @Stock_warehouse_orderpointProduct_id_textDefault(info = "默认规则")
    private String product_id_text;

    @JsonIgnore
    private boolean product_id_textDirtyFlag;

    /**
     * 属性 [WRITE_UID_TEXT]
     *
     */
    @Stock_warehouse_orderpointWrite_uid_textDefault(info = "默认规则")
    private String write_uid_text;

    @JsonIgnore
    private boolean write_uid_textDirtyFlag;

    /**
     * 属性 [PRODUCT_UOM]
     *
     */
    @Stock_warehouse_orderpointProduct_uomDefault(info = "默认规则")
    private Integer product_uom;

    @JsonIgnore
    private boolean product_uomDirtyFlag;

    /**
     * 属性 [COMPANY_ID_TEXT]
     *
     */
    @Stock_warehouse_orderpointCompany_id_textDefault(info = "默认规则")
    private String company_id_text;

    @JsonIgnore
    private boolean company_id_textDirtyFlag;

    /**
     * 属性 [CREATE_UID_TEXT]
     *
     */
    @Stock_warehouse_orderpointCreate_uid_textDefault(info = "默认规则")
    private String create_uid_text;

    @JsonIgnore
    private boolean create_uid_textDirtyFlag;

    /**
     * 属性 [LOCATION_ID_TEXT]
     *
     */
    @Stock_warehouse_orderpointLocation_id_textDefault(info = "默认规则")
    private String location_id_text;

    @JsonIgnore
    private boolean location_id_textDirtyFlag;

    /**
     * 属性 [LOCATION_ID]
     *
     */
    @Stock_warehouse_orderpointLocation_idDefault(info = "默认规则")
    private Integer location_id;

    @JsonIgnore
    private boolean location_idDirtyFlag;

    /**
     * 属性 [WRITE_UID]
     *
     */
    @Stock_warehouse_orderpointWrite_uidDefault(info = "默认规则")
    private Integer write_uid;

    @JsonIgnore
    private boolean write_uidDirtyFlag;

    /**
     * 属性 [WAREHOUSE_ID]
     *
     */
    @Stock_warehouse_orderpointWarehouse_idDefault(info = "默认规则")
    private Integer warehouse_id;

    @JsonIgnore
    private boolean warehouse_idDirtyFlag;

    /**
     * 属性 [PRODUCT_ID]
     *
     */
    @Stock_warehouse_orderpointProduct_idDefault(info = "默认规则")
    private Integer product_id;

    @JsonIgnore
    private boolean product_idDirtyFlag;

    /**
     * 属性 [CREATE_UID]
     *
     */
    @Stock_warehouse_orderpointCreate_uidDefault(info = "默认规则")
    private Integer create_uid;

    @JsonIgnore
    private boolean create_uidDirtyFlag;

    /**
     * 属性 [COMPANY_ID]
     *
     */
    @Stock_warehouse_orderpointCompany_idDefault(info = "默认规则")
    private Integer company_id;

    @JsonIgnore
    private boolean company_idDirtyFlag;


    /**
     * 获取 [ACTIVE]
     */
    @JsonProperty("active")
    public String getActive(){
        return active ;
    }

    /**
     * 设置 [ACTIVE]
     */
    @JsonProperty("active")
    public void setActive(String  active){
        this.active = active ;
        this.activeDirtyFlag = true ;
    }

    /**
     * 获取 [ACTIVE]脏标记
     */
    @JsonIgnore
    public boolean getActiveDirtyFlag(){
        return activeDirtyFlag ;
    }

    /**
     * 获取 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return __last_update ;
    }

    /**
     * 设置 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

    /**
     * 获取 [__LAST_UPDATE]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return __last_updateDirtyFlag ;
    }

    /**
     * 获取 [LEAD_DAYS]
     */
    @JsonProperty("lead_days")
    public Integer getLead_days(){
        return lead_days ;
    }

    /**
     * 设置 [LEAD_DAYS]
     */
    @JsonProperty("lead_days")
    public void setLead_days(Integer  lead_days){
        this.lead_days = lead_days ;
        this.lead_daysDirtyFlag = true ;
    }

    /**
     * 获取 [LEAD_DAYS]脏标记
     */
    @JsonIgnore
    public boolean getLead_daysDirtyFlag(){
        return lead_daysDirtyFlag ;
    }

    /**
     * 获取 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return write_date ;
    }

    /**
     * 设置 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return write_dateDirtyFlag ;
    }

    /**
     * 获取 [LEAD_TYPE]
     */
    @JsonProperty("lead_type")
    public String getLead_type(){
        return lead_type ;
    }

    /**
     * 设置 [LEAD_TYPE]
     */
    @JsonProperty("lead_type")
    public void setLead_type(String  lead_type){
        this.lead_type = lead_type ;
        this.lead_typeDirtyFlag = true ;
    }

    /**
     * 获取 [LEAD_TYPE]脏标记
     */
    @JsonIgnore
    public boolean getLead_typeDirtyFlag(){
        return lead_typeDirtyFlag ;
    }

    /**
     * 获取 [GROUP_ID]
     */
    @JsonProperty("group_id")
    public Integer getGroup_id(){
        return group_id ;
    }

    /**
     * 设置 [GROUP_ID]
     */
    @JsonProperty("group_id")
    public void setGroup_id(Integer  group_id){
        this.group_id = group_id ;
        this.group_idDirtyFlag = true ;
    }

    /**
     * 获取 [GROUP_ID]脏标记
     */
    @JsonIgnore
    public boolean getGroup_idDirtyFlag(){
        return group_idDirtyFlag ;
    }

    /**
     * 获取 [PRODUCT_MAX_QTY]
     */
    @JsonProperty("product_max_qty")
    public Double getProduct_max_qty(){
        return product_max_qty ;
    }

    /**
     * 设置 [PRODUCT_MAX_QTY]
     */
    @JsonProperty("product_max_qty")
    public void setProduct_max_qty(Double  product_max_qty){
        this.product_max_qty = product_max_qty ;
        this.product_max_qtyDirtyFlag = true ;
    }

    /**
     * 获取 [PRODUCT_MAX_QTY]脏标记
     */
    @JsonIgnore
    public boolean getProduct_max_qtyDirtyFlag(){
        return product_max_qtyDirtyFlag ;
    }

    /**
     * 获取 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return display_name ;
    }

    /**
     * 设置 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

    /**
     * 获取 [DISPLAY_NAME]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return display_nameDirtyFlag ;
    }

    /**
     * 获取 [QTY_MULTIPLE]
     */
    @JsonProperty("qty_multiple")
    public Double getQty_multiple(){
        return qty_multiple ;
    }

    /**
     * 设置 [QTY_MULTIPLE]
     */
    @JsonProperty("qty_multiple")
    public void setQty_multiple(Double  qty_multiple){
        this.qty_multiple = qty_multiple ;
        this.qty_multipleDirtyFlag = true ;
    }

    /**
     * 获取 [QTY_MULTIPLE]脏标记
     */
    @JsonIgnore
    public boolean getQty_multipleDirtyFlag(){
        return qty_multipleDirtyFlag ;
    }

    /**
     * 获取 [PRODUCT_MIN_QTY]
     */
    @JsonProperty("product_min_qty")
    public Double getProduct_min_qty(){
        return product_min_qty ;
    }

    /**
     * 设置 [PRODUCT_MIN_QTY]
     */
    @JsonProperty("product_min_qty")
    public void setProduct_min_qty(Double  product_min_qty){
        this.product_min_qty = product_min_qty ;
        this.product_min_qtyDirtyFlag = true ;
    }

    /**
     * 获取 [PRODUCT_MIN_QTY]脏标记
     */
    @JsonIgnore
    public boolean getProduct_min_qtyDirtyFlag(){
        return product_min_qtyDirtyFlag ;
    }

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

    /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return idDirtyFlag ;
    }

    /**
     * 获取 [NAME]
     */
    @JsonProperty("name")
    public String getName(){
        return name ;
    }

    /**
     * 设置 [NAME]
     */
    @JsonProperty("name")
    public void setName(String  name){
        this.name = name ;
        this.nameDirtyFlag = true ;
    }

    /**
     * 获取 [NAME]脏标记
     */
    @JsonIgnore
    public boolean getNameDirtyFlag(){
        return nameDirtyFlag ;
    }

    /**
     * 获取 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return create_date ;
    }

    /**
     * 设置 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return create_dateDirtyFlag ;
    }

    /**
     * 获取 [WAREHOUSE_ID_TEXT]
     */
    @JsonProperty("warehouse_id_text")
    public String getWarehouse_id_text(){
        return warehouse_id_text ;
    }

    /**
     * 设置 [WAREHOUSE_ID_TEXT]
     */
    @JsonProperty("warehouse_id_text")
    public void setWarehouse_id_text(String  warehouse_id_text){
        this.warehouse_id_text = warehouse_id_text ;
        this.warehouse_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [WAREHOUSE_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getWarehouse_id_textDirtyFlag(){
        return warehouse_id_textDirtyFlag ;
    }

    /**
     * 获取 [PRODUCT_ID_TEXT]
     */
    @JsonProperty("product_id_text")
    public String getProduct_id_text(){
        return product_id_text ;
    }

    /**
     * 设置 [PRODUCT_ID_TEXT]
     */
    @JsonProperty("product_id_text")
    public void setProduct_id_text(String  product_id_text){
        this.product_id_text = product_id_text ;
        this.product_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [PRODUCT_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getProduct_id_textDirtyFlag(){
        return product_id_textDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return write_uid_text ;
    }

    /**
     * 设置 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return write_uid_textDirtyFlag ;
    }

    /**
     * 获取 [PRODUCT_UOM]
     */
    @JsonProperty("product_uom")
    public Integer getProduct_uom(){
        return product_uom ;
    }

    /**
     * 设置 [PRODUCT_UOM]
     */
    @JsonProperty("product_uom")
    public void setProduct_uom(Integer  product_uom){
        this.product_uom = product_uom ;
        this.product_uomDirtyFlag = true ;
    }

    /**
     * 获取 [PRODUCT_UOM]脏标记
     */
    @JsonIgnore
    public boolean getProduct_uomDirtyFlag(){
        return product_uomDirtyFlag ;
    }

    /**
     * 获取 [COMPANY_ID_TEXT]
     */
    @JsonProperty("company_id_text")
    public String getCompany_id_text(){
        return company_id_text ;
    }

    /**
     * 设置 [COMPANY_ID_TEXT]
     */
    @JsonProperty("company_id_text")
    public void setCompany_id_text(String  company_id_text){
        this.company_id_text = company_id_text ;
        this.company_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [COMPANY_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getCompany_id_textDirtyFlag(){
        return company_id_textDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return create_uid_text ;
    }

    /**
     * 设置 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return create_uid_textDirtyFlag ;
    }

    /**
     * 获取 [LOCATION_ID_TEXT]
     */
    @JsonProperty("location_id_text")
    public String getLocation_id_text(){
        return location_id_text ;
    }

    /**
     * 设置 [LOCATION_ID_TEXT]
     */
    @JsonProperty("location_id_text")
    public void setLocation_id_text(String  location_id_text){
        this.location_id_text = location_id_text ;
        this.location_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [LOCATION_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getLocation_id_textDirtyFlag(){
        return location_id_textDirtyFlag ;
    }

    /**
     * 获取 [LOCATION_ID]
     */
    @JsonProperty("location_id")
    public Integer getLocation_id(){
        return location_id ;
    }

    /**
     * 设置 [LOCATION_ID]
     */
    @JsonProperty("location_id")
    public void setLocation_id(Integer  location_id){
        this.location_id = location_id ;
        this.location_idDirtyFlag = true ;
    }

    /**
     * 获取 [LOCATION_ID]脏标记
     */
    @JsonIgnore
    public boolean getLocation_idDirtyFlag(){
        return location_idDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return write_uid ;
    }

    /**
     * 设置 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return write_uidDirtyFlag ;
    }

    /**
     * 获取 [WAREHOUSE_ID]
     */
    @JsonProperty("warehouse_id")
    public Integer getWarehouse_id(){
        return warehouse_id ;
    }

    /**
     * 设置 [WAREHOUSE_ID]
     */
    @JsonProperty("warehouse_id")
    public void setWarehouse_id(Integer  warehouse_id){
        this.warehouse_id = warehouse_id ;
        this.warehouse_idDirtyFlag = true ;
    }

    /**
     * 获取 [WAREHOUSE_ID]脏标记
     */
    @JsonIgnore
    public boolean getWarehouse_idDirtyFlag(){
        return warehouse_idDirtyFlag ;
    }

    /**
     * 获取 [PRODUCT_ID]
     */
    @JsonProperty("product_id")
    public Integer getProduct_id(){
        return product_id ;
    }

    /**
     * 设置 [PRODUCT_ID]
     */
    @JsonProperty("product_id")
    public void setProduct_id(Integer  product_id){
        this.product_id = product_id ;
        this.product_idDirtyFlag = true ;
    }

    /**
     * 获取 [PRODUCT_ID]脏标记
     */
    @JsonIgnore
    public boolean getProduct_idDirtyFlag(){
        return product_idDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return create_uid ;
    }

    /**
     * 设置 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return create_uidDirtyFlag ;
    }

    /**
     * 获取 [COMPANY_ID]
     */
    @JsonProperty("company_id")
    public Integer getCompany_id(){
        return company_id ;
    }

    /**
     * 设置 [COMPANY_ID]
     */
    @JsonProperty("company_id")
    public void setCompany_id(Integer  company_id){
        this.company_id = company_id ;
        this.company_idDirtyFlag = true ;
    }

    /**
     * 获取 [COMPANY_ID]脏标记
     */
    @JsonIgnore
    public boolean getCompany_idDirtyFlag(){
        return company_idDirtyFlag ;
    }



    public Stock_warehouse_orderpoint toDO() {
        Stock_warehouse_orderpoint srfdomain = new Stock_warehouse_orderpoint();
        if(getActiveDirtyFlag())
            srfdomain.setActive(active);
        if(get__last_updateDirtyFlag())
            srfdomain.set__last_update(__last_update);
        if(getLead_daysDirtyFlag())
            srfdomain.setLead_days(lead_days);
        if(getWrite_dateDirtyFlag())
            srfdomain.setWrite_date(write_date);
        if(getLead_typeDirtyFlag())
            srfdomain.setLead_type(lead_type);
        if(getGroup_idDirtyFlag())
            srfdomain.setGroup_id(group_id);
        if(getProduct_max_qtyDirtyFlag())
            srfdomain.setProduct_max_qty(product_max_qty);
        if(getDisplay_nameDirtyFlag())
            srfdomain.setDisplay_name(display_name);
        if(getQty_multipleDirtyFlag())
            srfdomain.setQty_multiple(qty_multiple);
        if(getProduct_min_qtyDirtyFlag())
            srfdomain.setProduct_min_qty(product_min_qty);
        if(getIdDirtyFlag())
            srfdomain.setId(id);
        if(getNameDirtyFlag())
            srfdomain.setName(name);
        if(getCreate_dateDirtyFlag())
            srfdomain.setCreate_date(create_date);
        if(getWarehouse_id_textDirtyFlag())
            srfdomain.setWarehouse_id_text(warehouse_id_text);
        if(getProduct_id_textDirtyFlag())
            srfdomain.setProduct_id_text(product_id_text);
        if(getWrite_uid_textDirtyFlag())
            srfdomain.setWrite_uid_text(write_uid_text);
        if(getProduct_uomDirtyFlag())
            srfdomain.setProduct_uom(product_uom);
        if(getCompany_id_textDirtyFlag())
            srfdomain.setCompany_id_text(company_id_text);
        if(getCreate_uid_textDirtyFlag())
            srfdomain.setCreate_uid_text(create_uid_text);
        if(getLocation_id_textDirtyFlag())
            srfdomain.setLocation_id_text(location_id_text);
        if(getLocation_idDirtyFlag())
            srfdomain.setLocation_id(location_id);
        if(getWrite_uidDirtyFlag())
            srfdomain.setWrite_uid(write_uid);
        if(getWarehouse_idDirtyFlag())
            srfdomain.setWarehouse_id(warehouse_id);
        if(getProduct_idDirtyFlag())
            srfdomain.setProduct_id(product_id);
        if(getCreate_uidDirtyFlag())
            srfdomain.setCreate_uid(create_uid);
        if(getCompany_idDirtyFlag())
            srfdomain.setCompany_id(company_id);

        return srfdomain;
    }

    public void fromDO(Stock_warehouse_orderpoint srfdomain) {
        if(srfdomain == null )
          return ;
        if(srfdomain.getActiveDirtyFlag())
            this.setActive(srfdomain.getActive());
        if(srfdomain.get__last_updateDirtyFlag())
            this.set__last_update(srfdomain.get__last_update());
        if(srfdomain.getLead_daysDirtyFlag())
            this.setLead_days(srfdomain.getLead_days());
        if(srfdomain.getWrite_dateDirtyFlag())
            this.setWrite_date(srfdomain.getWrite_date());
        if(srfdomain.getLead_typeDirtyFlag())
            this.setLead_type(srfdomain.getLead_type());
        if(srfdomain.getGroup_idDirtyFlag())
            this.setGroup_id(srfdomain.getGroup_id());
        if(srfdomain.getProduct_max_qtyDirtyFlag())
            this.setProduct_max_qty(srfdomain.getProduct_max_qty());
        if(srfdomain.getDisplay_nameDirtyFlag())
            this.setDisplay_name(srfdomain.getDisplay_name());
        if(srfdomain.getQty_multipleDirtyFlag())
            this.setQty_multiple(srfdomain.getQty_multiple());
        if(srfdomain.getProduct_min_qtyDirtyFlag())
            this.setProduct_min_qty(srfdomain.getProduct_min_qty());
        if(srfdomain.getIdDirtyFlag())
            this.setId(srfdomain.getId());
        if(srfdomain.getNameDirtyFlag())
            this.setName(srfdomain.getName());
        if(srfdomain.getCreate_dateDirtyFlag())
            this.setCreate_date(srfdomain.getCreate_date());
        if(srfdomain.getWarehouse_id_textDirtyFlag())
            this.setWarehouse_id_text(srfdomain.getWarehouse_id_text());
        if(srfdomain.getProduct_id_textDirtyFlag())
            this.setProduct_id_text(srfdomain.getProduct_id_text());
        if(srfdomain.getWrite_uid_textDirtyFlag())
            this.setWrite_uid_text(srfdomain.getWrite_uid_text());
        if(srfdomain.getProduct_uomDirtyFlag())
            this.setProduct_uom(srfdomain.getProduct_uom());
        if(srfdomain.getCompany_id_textDirtyFlag())
            this.setCompany_id_text(srfdomain.getCompany_id_text());
        if(srfdomain.getCreate_uid_textDirtyFlag())
            this.setCreate_uid_text(srfdomain.getCreate_uid_text());
        if(srfdomain.getLocation_id_textDirtyFlag())
            this.setLocation_id_text(srfdomain.getLocation_id_text());
        if(srfdomain.getLocation_idDirtyFlag())
            this.setLocation_id(srfdomain.getLocation_id());
        if(srfdomain.getWrite_uidDirtyFlag())
            this.setWrite_uid(srfdomain.getWrite_uid());
        if(srfdomain.getWarehouse_idDirtyFlag())
            this.setWarehouse_id(srfdomain.getWarehouse_id());
        if(srfdomain.getProduct_idDirtyFlag())
            this.setProduct_id(srfdomain.getProduct_id());
        if(srfdomain.getCreate_uidDirtyFlag())
            this.setCreate_uid(srfdomain.getCreate_uid());
        if(srfdomain.getCompany_idDirtyFlag())
            this.setCompany_id(srfdomain.getCompany_id());

    }

    public List<Stock_warehouse_orderpointDTO> fromDOPage(List<Stock_warehouse_orderpoint> poPage)   {
        if(poPage == null)
            return null;
        List<Stock_warehouse_orderpointDTO> dtos=new ArrayList<Stock_warehouse_orderpointDTO>();
        for(Stock_warehouse_orderpoint domain : poPage) {
            Stock_warehouse_orderpointDTO dto = new Stock_warehouse_orderpointDTO();
            dto.fromDO(domain);
            dtos.add(dto);
        }
        return dtos;
    }
}

