package cn.ibizlab.odoo.service.odoo_stock.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.core.odoo_stock.valuerule.anno.stock_location_route.*;
import cn.ibizlab.odoo.core.odoo_stock.domain.Stock_location_route;
import org.springframework.cglib.beans.BeanCopier;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 服务DTO对象[Stock_location_routeDTO]
 */
public class Stock_location_routeDTO implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [SALE_SELECTABLE]
     *
     */
    @Stock_location_routeSale_selectableDefault(info = "默认规则")
    private String sale_selectable;

    @JsonIgnore
    private boolean sale_selectableDirtyFlag;

    /**
     * 属性 [WRITE_DATE]
     *
     */
    @Stock_location_routeWrite_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp write_date;

    @JsonIgnore
    private boolean write_dateDirtyFlag;

    /**
     * 属性 [PRODUCT_IDS]
     *
     */
    @Stock_location_routeProduct_idsDefault(info = "默认规则")
    private String product_ids;

    @JsonIgnore
    private boolean product_idsDirtyFlag;

    /**
     * 属性 [ID]
     *
     */
    @Stock_location_routeIdDefault(info = "默认规则")
    private Integer id;

    @JsonIgnore
    private boolean idDirtyFlag;

    /**
     * 属性 [CREATE_DATE]
     *
     */
    @Stock_location_routeCreate_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp create_date;

    @JsonIgnore
    private boolean create_dateDirtyFlag;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @Stock_location_routeDisplay_nameDefault(info = "默认规则")
    private String display_name;

    @JsonIgnore
    private boolean display_nameDirtyFlag;

    /**
     * 属性 [NAME]
     *
     */
    @Stock_location_routeNameDefault(info = "默认规则")
    private String name;

    @JsonIgnore
    private boolean nameDirtyFlag;

    /**
     * 属性 [CATEG_IDS]
     *
     */
    @Stock_location_routeCateg_idsDefault(info = "默认规则")
    private String categ_ids;

    @JsonIgnore
    private boolean categ_idsDirtyFlag;

    /**
     * 属性 [WAREHOUSE_SELECTABLE]
     *
     */
    @Stock_location_routeWarehouse_selectableDefault(info = "默认规则")
    private String warehouse_selectable;

    @JsonIgnore
    private boolean warehouse_selectableDirtyFlag;

    /**
     * 属性 [SEQUENCE]
     *
     */
    @Stock_location_routeSequenceDefault(info = "默认规则")
    private Integer sequence;

    @JsonIgnore
    private boolean sequenceDirtyFlag;

    /**
     * 属性 [RULE_IDS]
     *
     */
    @Stock_location_routeRule_idsDefault(info = "默认规则")
    private String rule_ids;

    @JsonIgnore
    private boolean rule_idsDirtyFlag;

    /**
     * 属性 [PRODUCT_SELECTABLE]
     *
     */
    @Stock_location_routeProduct_selectableDefault(info = "默认规则")
    private String product_selectable;

    @JsonIgnore
    private boolean product_selectableDirtyFlag;

    /**
     * 属性 [ACTIVE]
     *
     */
    @Stock_location_routeActiveDefault(info = "默认规则")
    private String active;

    @JsonIgnore
    private boolean activeDirtyFlag;

    /**
     * 属性 [PRODUCT_CATEG_SELECTABLE]
     *
     */
    @Stock_location_routeProduct_categ_selectableDefault(info = "默认规则")
    private String product_categ_selectable;

    @JsonIgnore
    private boolean product_categ_selectableDirtyFlag;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @Stock_location_route__last_updateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp __last_update;

    @JsonIgnore
    private boolean __last_updateDirtyFlag;

    /**
     * 属性 [WAREHOUSE_IDS]
     *
     */
    @Stock_location_routeWarehouse_idsDefault(info = "默认规则")
    private String warehouse_ids;

    @JsonIgnore
    private boolean warehouse_idsDirtyFlag;

    /**
     * 属性 [CREATE_UID_TEXT]
     *
     */
    @Stock_location_routeCreate_uid_textDefault(info = "默认规则")
    private String create_uid_text;

    @JsonIgnore
    private boolean create_uid_textDirtyFlag;

    /**
     * 属性 [SUPPLIED_WH_ID_TEXT]
     *
     */
    @Stock_location_routeSupplied_wh_id_textDefault(info = "默认规则")
    private String supplied_wh_id_text;

    @JsonIgnore
    private boolean supplied_wh_id_textDirtyFlag;

    /**
     * 属性 [WRITE_UID_TEXT]
     *
     */
    @Stock_location_routeWrite_uid_textDefault(info = "默认规则")
    private String write_uid_text;

    @JsonIgnore
    private boolean write_uid_textDirtyFlag;

    /**
     * 属性 [SUPPLIER_WH_ID_TEXT]
     *
     */
    @Stock_location_routeSupplier_wh_id_textDefault(info = "默认规则")
    private String supplier_wh_id_text;

    @JsonIgnore
    private boolean supplier_wh_id_textDirtyFlag;

    /**
     * 属性 [COMPANY_ID_TEXT]
     *
     */
    @Stock_location_routeCompany_id_textDefault(info = "默认规则")
    private String company_id_text;

    @JsonIgnore
    private boolean company_id_textDirtyFlag;

    /**
     * 属性 [COMPANY_ID]
     *
     */
    @Stock_location_routeCompany_idDefault(info = "默认规则")
    private Integer company_id;

    @JsonIgnore
    private boolean company_idDirtyFlag;

    /**
     * 属性 [CREATE_UID]
     *
     */
    @Stock_location_routeCreate_uidDefault(info = "默认规则")
    private Integer create_uid;

    @JsonIgnore
    private boolean create_uidDirtyFlag;

    /**
     * 属性 [WRITE_UID]
     *
     */
    @Stock_location_routeWrite_uidDefault(info = "默认规则")
    private Integer write_uid;

    @JsonIgnore
    private boolean write_uidDirtyFlag;

    /**
     * 属性 [SUPPLIER_WH_ID]
     *
     */
    @Stock_location_routeSupplier_wh_idDefault(info = "默认规则")
    private Integer supplier_wh_id;

    @JsonIgnore
    private boolean supplier_wh_idDirtyFlag;

    /**
     * 属性 [SUPPLIED_WH_ID]
     *
     */
    @Stock_location_routeSupplied_wh_idDefault(info = "默认规则")
    private Integer supplied_wh_id;

    @JsonIgnore
    private boolean supplied_wh_idDirtyFlag;


    /**
     * 获取 [SALE_SELECTABLE]
     */
    @JsonProperty("sale_selectable")
    public String getSale_selectable(){
        return sale_selectable ;
    }

    /**
     * 设置 [SALE_SELECTABLE]
     */
    @JsonProperty("sale_selectable")
    public void setSale_selectable(String  sale_selectable){
        this.sale_selectable = sale_selectable ;
        this.sale_selectableDirtyFlag = true ;
    }

    /**
     * 获取 [SALE_SELECTABLE]脏标记
     */
    @JsonIgnore
    public boolean getSale_selectableDirtyFlag(){
        return sale_selectableDirtyFlag ;
    }

    /**
     * 获取 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return write_date ;
    }

    /**
     * 设置 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return write_dateDirtyFlag ;
    }

    /**
     * 获取 [PRODUCT_IDS]
     */
    @JsonProperty("product_ids")
    public String getProduct_ids(){
        return product_ids ;
    }

    /**
     * 设置 [PRODUCT_IDS]
     */
    @JsonProperty("product_ids")
    public void setProduct_ids(String  product_ids){
        this.product_ids = product_ids ;
        this.product_idsDirtyFlag = true ;
    }

    /**
     * 获取 [PRODUCT_IDS]脏标记
     */
    @JsonIgnore
    public boolean getProduct_idsDirtyFlag(){
        return product_idsDirtyFlag ;
    }

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

    /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return idDirtyFlag ;
    }

    /**
     * 获取 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return create_date ;
    }

    /**
     * 设置 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return create_dateDirtyFlag ;
    }

    /**
     * 获取 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return display_name ;
    }

    /**
     * 设置 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

    /**
     * 获取 [DISPLAY_NAME]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return display_nameDirtyFlag ;
    }

    /**
     * 获取 [NAME]
     */
    @JsonProperty("name")
    public String getName(){
        return name ;
    }

    /**
     * 设置 [NAME]
     */
    @JsonProperty("name")
    public void setName(String  name){
        this.name = name ;
        this.nameDirtyFlag = true ;
    }

    /**
     * 获取 [NAME]脏标记
     */
    @JsonIgnore
    public boolean getNameDirtyFlag(){
        return nameDirtyFlag ;
    }

    /**
     * 获取 [CATEG_IDS]
     */
    @JsonProperty("categ_ids")
    public String getCateg_ids(){
        return categ_ids ;
    }

    /**
     * 设置 [CATEG_IDS]
     */
    @JsonProperty("categ_ids")
    public void setCateg_ids(String  categ_ids){
        this.categ_ids = categ_ids ;
        this.categ_idsDirtyFlag = true ;
    }

    /**
     * 获取 [CATEG_IDS]脏标记
     */
    @JsonIgnore
    public boolean getCateg_idsDirtyFlag(){
        return categ_idsDirtyFlag ;
    }

    /**
     * 获取 [WAREHOUSE_SELECTABLE]
     */
    @JsonProperty("warehouse_selectable")
    public String getWarehouse_selectable(){
        return warehouse_selectable ;
    }

    /**
     * 设置 [WAREHOUSE_SELECTABLE]
     */
    @JsonProperty("warehouse_selectable")
    public void setWarehouse_selectable(String  warehouse_selectable){
        this.warehouse_selectable = warehouse_selectable ;
        this.warehouse_selectableDirtyFlag = true ;
    }

    /**
     * 获取 [WAREHOUSE_SELECTABLE]脏标记
     */
    @JsonIgnore
    public boolean getWarehouse_selectableDirtyFlag(){
        return warehouse_selectableDirtyFlag ;
    }

    /**
     * 获取 [SEQUENCE]
     */
    @JsonProperty("sequence")
    public Integer getSequence(){
        return sequence ;
    }

    /**
     * 设置 [SEQUENCE]
     */
    @JsonProperty("sequence")
    public void setSequence(Integer  sequence){
        this.sequence = sequence ;
        this.sequenceDirtyFlag = true ;
    }

    /**
     * 获取 [SEQUENCE]脏标记
     */
    @JsonIgnore
    public boolean getSequenceDirtyFlag(){
        return sequenceDirtyFlag ;
    }

    /**
     * 获取 [RULE_IDS]
     */
    @JsonProperty("rule_ids")
    public String getRule_ids(){
        return rule_ids ;
    }

    /**
     * 设置 [RULE_IDS]
     */
    @JsonProperty("rule_ids")
    public void setRule_ids(String  rule_ids){
        this.rule_ids = rule_ids ;
        this.rule_idsDirtyFlag = true ;
    }

    /**
     * 获取 [RULE_IDS]脏标记
     */
    @JsonIgnore
    public boolean getRule_idsDirtyFlag(){
        return rule_idsDirtyFlag ;
    }

    /**
     * 获取 [PRODUCT_SELECTABLE]
     */
    @JsonProperty("product_selectable")
    public String getProduct_selectable(){
        return product_selectable ;
    }

    /**
     * 设置 [PRODUCT_SELECTABLE]
     */
    @JsonProperty("product_selectable")
    public void setProduct_selectable(String  product_selectable){
        this.product_selectable = product_selectable ;
        this.product_selectableDirtyFlag = true ;
    }

    /**
     * 获取 [PRODUCT_SELECTABLE]脏标记
     */
    @JsonIgnore
    public boolean getProduct_selectableDirtyFlag(){
        return product_selectableDirtyFlag ;
    }

    /**
     * 获取 [ACTIVE]
     */
    @JsonProperty("active")
    public String getActive(){
        return active ;
    }

    /**
     * 设置 [ACTIVE]
     */
    @JsonProperty("active")
    public void setActive(String  active){
        this.active = active ;
        this.activeDirtyFlag = true ;
    }

    /**
     * 获取 [ACTIVE]脏标记
     */
    @JsonIgnore
    public boolean getActiveDirtyFlag(){
        return activeDirtyFlag ;
    }

    /**
     * 获取 [PRODUCT_CATEG_SELECTABLE]
     */
    @JsonProperty("product_categ_selectable")
    public String getProduct_categ_selectable(){
        return product_categ_selectable ;
    }

    /**
     * 设置 [PRODUCT_CATEG_SELECTABLE]
     */
    @JsonProperty("product_categ_selectable")
    public void setProduct_categ_selectable(String  product_categ_selectable){
        this.product_categ_selectable = product_categ_selectable ;
        this.product_categ_selectableDirtyFlag = true ;
    }

    /**
     * 获取 [PRODUCT_CATEG_SELECTABLE]脏标记
     */
    @JsonIgnore
    public boolean getProduct_categ_selectableDirtyFlag(){
        return product_categ_selectableDirtyFlag ;
    }

    /**
     * 获取 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return __last_update ;
    }

    /**
     * 设置 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

    /**
     * 获取 [__LAST_UPDATE]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return __last_updateDirtyFlag ;
    }

    /**
     * 获取 [WAREHOUSE_IDS]
     */
    @JsonProperty("warehouse_ids")
    public String getWarehouse_ids(){
        return warehouse_ids ;
    }

    /**
     * 设置 [WAREHOUSE_IDS]
     */
    @JsonProperty("warehouse_ids")
    public void setWarehouse_ids(String  warehouse_ids){
        this.warehouse_ids = warehouse_ids ;
        this.warehouse_idsDirtyFlag = true ;
    }

    /**
     * 获取 [WAREHOUSE_IDS]脏标记
     */
    @JsonIgnore
    public boolean getWarehouse_idsDirtyFlag(){
        return warehouse_idsDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return create_uid_text ;
    }

    /**
     * 设置 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return create_uid_textDirtyFlag ;
    }

    /**
     * 获取 [SUPPLIED_WH_ID_TEXT]
     */
    @JsonProperty("supplied_wh_id_text")
    public String getSupplied_wh_id_text(){
        return supplied_wh_id_text ;
    }

    /**
     * 设置 [SUPPLIED_WH_ID_TEXT]
     */
    @JsonProperty("supplied_wh_id_text")
    public void setSupplied_wh_id_text(String  supplied_wh_id_text){
        this.supplied_wh_id_text = supplied_wh_id_text ;
        this.supplied_wh_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [SUPPLIED_WH_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getSupplied_wh_id_textDirtyFlag(){
        return supplied_wh_id_textDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return write_uid_text ;
    }

    /**
     * 设置 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return write_uid_textDirtyFlag ;
    }

    /**
     * 获取 [SUPPLIER_WH_ID_TEXT]
     */
    @JsonProperty("supplier_wh_id_text")
    public String getSupplier_wh_id_text(){
        return supplier_wh_id_text ;
    }

    /**
     * 设置 [SUPPLIER_WH_ID_TEXT]
     */
    @JsonProperty("supplier_wh_id_text")
    public void setSupplier_wh_id_text(String  supplier_wh_id_text){
        this.supplier_wh_id_text = supplier_wh_id_text ;
        this.supplier_wh_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [SUPPLIER_WH_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getSupplier_wh_id_textDirtyFlag(){
        return supplier_wh_id_textDirtyFlag ;
    }

    /**
     * 获取 [COMPANY_ID_TEXT]
     */
    @JsonProperty("company_id_text")
    public String getCompany_id_text(){
        return company_id_text ;
    }

    /**
     * 设置 [COMPANY_ID_TEXT]
     */
    @JsonProperty("company_id_text")
    public void setCompany_id_text(String  company_id_text){
        this.company_id_text = company_id_text ;
        this.company_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [COMPANY_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getCompany_id_textDirtyFlag(){
        return company_id_textDirtyFlag ;
    }

    /**
     * 获取 [COMPANY_ID]
     */
    @JsonProperty("company_id")
    public Integer getCompany_id(){
        return company_id ;
    }

    /**
     * 设置 [COMPANY_ID]
     */
    @JsonProperty("company_id")
    public void setCompany_id(Integer  company_id){
        this.company_id = company_id ;
        this.company_idDirtyFlag = true ;
    }

    /**
     * 获取 [COMPANY_ID]脏标记
     */
    @JsonIgnore
    public boolean getCompany_idDirtyFlag(){
        return company_idDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return create_uid ;
    }

    /**
     * 设置 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return create_uidDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return write_uid ;
    }

    /**
     * 设置 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return write_uidDirtyFlag ;
    }

    /**
     * 获取 [SUPPLIER_WH_ID]
     */
    @JsonProperty("supplier_wh_id")
    public Integer getSupplier_wh_id(){
        return supplier_wh_id ;
    }

    /**
     * 设置 [SUPPLIER_WH_ID]
     */
    @JsonProperty("supplier_wh_id")
    public void setSupplier_wh_id(Integer  supplier_wh_id){
        this.supplier_wh_id = supplier_wh_id ;
        this.supplier_wh_idDirtyFlag = true ;
    }

    /**
     * 获取 [SUPPLIER_WH_ID]脏标记
     */
    @JsonIgnore
    public boolean getSupplier_wh_idDirtyFlag(){
        return supplier_wh_idDirtyFlag ;
    }

    /**
     * 获取 [SUPPLIED_WH_ID]
     */
    @JsonProperty("supplied_wh_id")
    public Integer getSupplied_wh_id(){
        return supplied_wh_id ;
    }

    /**
     * 设置 [SUPPLIED_WH_ID]
     */
    @JsonProperty("supplied_wh_id")
    public void setSupplied_wh_id(Integer  supplied_wh_id){
        this.supplied_wh_id = supplied_wh_id ;
        this.supplied_wh_idDirtyFlag = true ;
    }

    /**
     * 获取 [SUPPLIED_WH_ID]脏标记
     */
    @JsonIgnore
    public boolean getSupplied_wh_idDirtyFlag(){
        return supplied_wh_idDirtyFlag ;
    }



    public Stock_location_route toDO() {
        Stock_location_route srfdomain = new Stock_location_route();
        if(getSale_selectableDirtyFlag())
            srfdomain.setSale_selectable(sale_selectable);
        if(getWrite_dateDirtyFlag())
            srfdomain.setWrite_date(write_date);
        if(getProduct_idsDirtyFlag())
            srfdomain.setProduct_ids(product_ids);
        if(getIdDirtyFlag())
            srfdomain.setId(id);
        if(getCreate_dateDirtyFlag())
            srfdomain.setCreate_date(create_date);
        if(getDisplay_nameDirtyFlag())
            srfdomain.setDisplay_name(display_name);
        if(getNameDirtyFlag())
            srfdomain.setName(name);
        if(getCateg_idsDirtyFlag())
            srfdomain.setCateg_ids(categ_ids);
        if(getWarehouse_selectableDirtyFlag())
            srfdomain.setWarehouse_selectable(warehouse_selectable);
        if(getSequenceDirtyFlag())
            srfdomain.setSequence(sequence);
        if(getRule_idsDirtyFlag())
            srfdomain.setRule_ids(rule_ids);
        if(getProduct_selectableDirtyFlag())
            srfdomain.setProduct_selectable(product_selectable);
        if(getActiveDirtyFlag())
            srfdomain.setActive(active);
        if(getProduct_categ_selectableDirtyFlag())
            srfdomain.setProduct_categ_selectable(product_categ_selectable);
        if(get__last_updateDirtyFlag())
            srfdomain.set__last_update(__last_update);
        if(getWarehouse_idsDirtyFlag())
            srfdomain.setWarehouse_ids(warehouse_ids);
        if(getCreate_uid_textDirtyFlag())
            srfdomain.setCreate_uid_text(create_uid_text);
        if(getSupplied_wh_id_textDirtyFlag())
            srfdomain.setSupplied_wh_id_text(supplied_wh_id_text);
        if(getWrite_uid_textDirtyFlag())
            srfdomain.setWrite_uid_text(write_uid_text);
        if(getSupplier_wh_id_textDirtyFlag())
            srfdomain.setSupplier_wh_id_text(supplier_wh_id_text);
        if(getCompany_id_textDirtyFlag())
            srfdomain.setCompany_id_text(company_id_text);
        if(getCompany_idDirtyFlag())
            srfdomain.setCompany_id(company_id);
        if(getCreate_uidDirtyFlag())
            srfdomain.setCreate_uid(create_uid);
        if(getWrite_uidDirtyFlag())
            srfdomain.setWrite_uid(write_uid);
        if(getSupplier_wh_idDirtyFlag())
            srfdomain.setSupplier_wh_id(supplier_wh_id);
        if(getSupplied_wh_idDirtyFlag())
            srfdomain.setSupplied_wh_id(supplied_wh_id);

        return srfdomain;
    }

    public void fromDO(Stock_location_route srfdomain) {
        if(srfdomain == null )
          return ;
        if(srfdomain.getSale_selectableDirtyFlag())
            this.setSale_selectable(srfdomain.getSale_selectable());
        if(srfdomain.getWrite_dateDirtyFlag())
            this.setWrite_date(srfdomain.getWrite_date());
        if(srfdomain.getProduct_idsDirtyFlag())
            this.setProduct_ids(srfdomain.getProduct_ids());
        if(srfdomain.getIdDirtyFlag())
            this.setId(srfdomain.getId());
        if(srfdomain.getCreate_dateDirtyFlag())
            this.setCreate_date(srfdomain.getCreate_date());
        if(srfdomain.getDisplay_nameDirtyFlag())
            this.setDisplay_name(srfdomain.getDisplay_name());
        if(srfdomain.getNameDirtyFlag())
            this.setName(srfdomain.getName());
        if(srfdomain.getCateg_idsDirtyFlag())
            this.setCateg_ids(srfdomain.getCateg_ids());
        if(srfdomain.getWarehouse_selectableDirtyFlag())
            this.setWarehouse_selectable(srfdomain.getWarehouse_selectable());
        if(srfdomain.getSequenceDirtyFlag())
            this.setSequence(srfdomain.getSequence());
        if(srfdomain.getRule_idsDirtyFlag())
            this.setRule_ids(srfdomain.getRule_ids());
        if(srfdomain.getProduct_selectableDirtyFlag())
            this.setProduct_selectable(srfdomain.getProduct_selectable());
        if(srfdomain.getActiveDirtyFlag())
            this.setActive(srfdomain.getActive());
        if(srfdomain.getProduct_categ_selectableDirtyFlag())
            this.setProduct_categ_selectable(srfdomain.getProduct_categ_selectable());
        if(srfdomain.get__last_updateDirtyFlag())
            this.set__last_update(srfdomain.get__last_update());
        if(srfdomain.getWarehouse_idsDirtyFlag())
            this.setWarehouse_ids(srfdomain.getWarehouse_ids());
        if(srfdomain.getCreate_uid_textDirtyFlag())
            this.setCreate_uid_text(srfdomain.getCreate_uid_text());
        if(srfdomain.getSupplied_wh_id_textDirtyFlag())
            this.setSupplied_wh_id_text(srfdomain.getSupplied_wh_id_text());
        if(srfdomain.getWrite_uid_textDirtyFlag())
            this.setWrite_uid_text(srfdomain.getWrite_uid_text());
        if(srfdomain.getSupplier_wh_id_textDirtyFlag())
            this.setSupplier_wh_id_text(srfdomain.getSupplier_wh_id_text());
        if(srfdomain.getCompany_id_textDirtyFlag())
            this.setCompany_id_text(srfdomain.getCompany_id_text());
        if(srfdomain.getCompany_idDirtyFlag())
            this.setCompany_id(srfdomain.getCompany_id());
        if(srfdomain.getCreate_uidDirtyFlag())
            this.setCreate_uid(srfdomain.getCreate_uid());
        if(srfdomain.getWrite_uidDirtyFlag())
            this.setWrite_uid(srfdomain.getWrite_uid());
        if(srfdomain.getSupplier_wh_idDirtyFlag())
            this.setSupplier_wh_id(srfdomain.getSupplier_wh_id());
        if(srfdomain.getSupplied_wh_idDirtyFlag())
            this.setSupplied_wh_id(srfdomain.getSupplied_wh_id());

    }

    public List<Stock_location_routeDTO> fromDOPage(List<Stock_location_route> poPage)   {
        if(poPage == null)
            return null;
        List<Stock_location_routeDTO> dtos=new ArrayList<Stock_location_routeDTO>();
        for(Stock_location_route domain : poPage) {
            Stock_location_routeDTO dto = new Stock_location_routeDTO();
            dto.fromDO(domain);
            dtos.add(dto);
        }
        return dtos;
    }
}

