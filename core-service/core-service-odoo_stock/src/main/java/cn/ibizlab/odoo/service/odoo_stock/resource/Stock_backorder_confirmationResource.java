package cn.ibizlab.odoo.service.odoo_stock.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_stock.dto.Stock_backorder_confirmationDTO;
import cn.ibizlab.odoo.core.odoo_stock.domain.Stock_backorder_confirmation;
import cn.ibizlab.odoo.core.odoo_stock.service.IStock_backorder_confirmationService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_stock.filter.Stock_backorder_confirmationSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Stock_backorder_confirmation" })
@RestController
@RequestMapping("")
public class Stock_backorder_confirmationResource {

    @Autowired
    private IStock_backorder_confirmationService stock_backorder_confirmationService;

    public IStock_backorder_confirmationService getStock_backorder_confirmationService() {
        return this.stock_backorder_confirmationService;
    }

    @ApiOperation(value = "删除数据", tags = {"Stock_backorder_confirmation" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_stock/stock_backorder_confirmations/{stock_backorder_confirmation_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("stock_backorder_confirmation_id") Integer stock_backorder_confirmation_id) {
        Stock_backorder_confirmationDTO stock_backorder_confirmationdto = new Stock_backorder_confirmationDTO();
		Stock_backorder_confirmation domain = new Stock_backorder_confirmation();
		stock_backorder_confirmationdto.setId(stock_backorder_confirmation_id);
		domain.setId(stock_backorder_confirmation_id);
        Boolean rst = stock_backorder_confirmationService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "批更新数据", tags = {"Stock_backorder_confirmation" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_stock/stock_backorder_confirmations/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Stock_backorder_confirmationDTO> stock_backorder_confirmationdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "获取数据", tags = {"Stock_backorder_confirmation" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_stock/stock_backorder_confirmations/{stock_backorder_confirmation_id}")
    public ResponseEntity<Stock_backorder_confirmationDTO> get(@PathVariable("stock_backorder_confirmation_id") Integer stock_backorder_confirmation_id) {
        Stock_backorder_confirmationDTO dto = new Stock_backorder_confirmationDTO();
        Stock_backorder_confirmation domain = stock_backorder_confirmationService.get(stock_backorder_confirmation_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "更新数据", tags = {"Stock_backorder_confirmation" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_stock/stock_backorder_confirmations/{stock_backorder_confirmation_id}")

    public ResponseEntity<Stock_backorder_confirmationDTO> update(@PathVariable("stock_backorder_confirmation_id") Integer stock_backorder_confirmation_id, @RequestBody Stock_backorder_confirmationDTO stock_backorder_confirmationdto) {
		Stock_backorder_confirmation domain = stock_backorder_confirmationdto.toDO();
        domain.setId(stock_backorder_confirmation_id);
		stock_backorder_confirmationService.update(domain);
		Stock_backorder_confirmationDTO dto = new Stock_backorder_confirmationDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批删除数据", tags = {"Stock_backorder_confirmation" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_stock/stock_backorder_confirmations/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Stock_backorder_confirmationDTO> stock_backorder_confirmationdtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "批建立数据", tags = {"Stock_backorder_confirmation" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_stock/stock_backorder_confirmations/createBatch")
    public ResponseEntity<Boolean> createBatchStock_backorder_confirmation(@RequestBody List<Stock_backorder_confirmationDTO> stock_backorder_confirmationdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "建立数据", tags = {"Stock_backorder_confirmation" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_stock/stock_backorder_confirmations")

    public ResponseEntity<Stock_backorder_confirmationDTO> create(@RequestBody Stock_backorder_confirmationDTO stock_backorder_confirmationdto) {
        Stock_backorder_confirmationDTO dto = new Stock_backorder_confirmationDTO();
        Stock_backorder_confirmation domain = stock_backorder_confirmationdto.toDO();
		stock_backorder_confirmationService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Stock_backorder_confirmation" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_stock/stock_backorder_confirmations/fetchdefault")
	public ResponseEntity<Page<Stock_backorder_confirmationDTO>> fetchDefault(Stock_backorder_confirmationSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Stock_backorder_confirmationDTO> list = new ArrayList<Stock_backorder_confirmationDTO>();
        
        Page<Stock_backorder_confirmation> domains = stock_backorder_confirmationService.searchDefault(context) ;
        for(Stock_backorder_confirmation stock_backorder_confirmation : domains.getContent()){
            Stock_backorder_confirmationDTO dto = new Stock_backorder_confirmationDTO();
            dto.fromDO(stock_backorder_confirmation);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
