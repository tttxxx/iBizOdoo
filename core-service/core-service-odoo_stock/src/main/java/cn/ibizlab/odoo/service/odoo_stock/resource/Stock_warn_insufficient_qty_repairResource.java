package cn.ibizlab.odoo.service.odoo_stock.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_stock.dto.Stock_warn_insufficient_qty_repairDTO;
import cn.ibizlab.odoo.core.odoo_stock.domain.Stock_warn_insufficient_qty_repair;
import cn.ibizlab.odoo.core.odoo_stock.service.IStock_warn_insufficient_qty_repairService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_stock.filter.Stock_warn_insufficient_qty_repairSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Stock_warn_insufficient_qty_repair" })
@RestController
@RequestMapping("")
public class Stock_warn_insufficient_qty_repairResource {

    @Autowired
    private IStock_warn_insufficient_qty_repairService stock_warn_insufficient_qty_repairService;

    public IStock_warn_insufficient_qty_repairService getStock_warn_insufficient_qty_repairService() {
        return this.stock_warn_insufficient_qty_repairService;
    }

    @ApiOperation(value = "删除数据", tags = {"Stock_warn_insufficient_qty_repair" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_stock/stock_warn_insufficient_qty_repairs/{stock_warn_insufficient_qty_repair_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("stock_warn_insufficient_qty_repair_id") Integer stock_warn_insufficient_qty_repair_id) {
        Stock_warn_insufficient_qty_repairDTO stock_warn_insufficient_qty_repairdto = new Stock_warn_insufficient_qty_repairDTO();
		Stock_warn_insufficient_qty_repair domain = new Stock_warn_insufficient_qty_repair();
		stock_warn_insufficient_qty_repairdto.setId(stock_warn_insufficient_qty_repair_id);
		domain.setId(stock_warn_insufficient_qty_repair_id);
        Boolean rst = stock_warn_insufficient_qty_repairService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "获取数据", tags = {"Stock_warn_insufficient_qty_repair" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_stock/stock_warn_insufficient_qty_repairs/{stock_warn_insufficient_qty_repair_id}")
    public ResponseEntity<Stock_warn_insufficient_qty_repairDTO> get(@PathVariable("stock_warn_insufficient_qty_repair_id") Integer stock_warn_insufficient_qty_repair_id) {
        Stock_warn_insufficient_qty_repairDTO dto = new Stock_warn_insufficient_qty_repairDTO();
        Stock_warn_insufficient_qty_repair domain = stock_warn_insufficient_qty_repairService.get(stock_warn_insufficient_qty_repair_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批更新数据", tags = {"Stock_warn_insufficient_qty_repair" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_stock/stock_warn_insufficient_qty_repairs/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Stock_warn_insufficient_qty_repairDTO> stock_warn_insufficient_qty_repairdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "批建立数据", tags = {"Stock_warn_insufficient_qty_repair" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_stock/stock_warn_insufficient_qty_repairs/createBatch")
    public ResponseEntity<Boolean> createBatchStock_warn_insufficient_qty_repair(@RequestBody List<Stock_warn_insufficient_qty_repairDTO> stock_warn_insufficient_qty_repairdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "批删除数据", tags = {"Stock_warn_insufficient_qty_repair" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_stock/stock_warn_insufficient_qty_repairs/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Stock_warn_insufficient_qty_repairDTO> stock_warn_insufficient_qty_repairdtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "建立数据", tags = {"Stock_warn_insufficient_qty_repair" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_stock/stock_warn_insufficient_qty_repairs")

    public ResponseEntity<Stock_warn_insufficient_qty_repairDTO> create(@RequestBody Stock_warn_insufficient_qty_repairDTO stock_warn_insufficient_qty_repairdto) {
        Stock_warn_insufficient_qty_repairDTO dto = new Stock_warn_insufficient_qty_repairDTO();
        Stock_warn_insufficient_qty_repair domain = stock_warn_insufficient_qty_repairdto.toDO();
		stock_warn_insufficient_qty_repairService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "更新数据", tags = {"Stock_warn_insufficient_qty_repair" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_stock/stock_warn_insufficient_qty_repairs/{stock_warn_insufficient_qty_repair_id}")

    public ResponseEntity<Stock_warn_insufficient_qty_repairDTO> update(@PathVariable("stock_warn_insufficient_qty_repair_id") Integer stock_warn_insufficient_qty_repair_id, @RequestBody Stock_warn_insufficient_qty_repairDTO stock_warn_insufficient_qty_repairdto) {
		Stock_warn_insufficient_qty_repair domain = stock_warn_insufficient_qty_repairdto.toDO();
        domain.setId(stock_warn_insufficient_qty_repair_id);
		stock_warn_insufficient_qty_repairService.update(domain);
		Stock_warn_insufficient_qty_repairDTO dto = new Stock_warn_insufficient_qty_repairDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Stock_warn_insufficient_qty_repair" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_stock/stock_warn_insufficient_qty_repairs/fetchdefault")
	public ResponseEntity<Page<Stock_warn_insufficient_qty_repairDTO>> fetchDefault(Stock_warn_insufficient_qty_repairSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Stock_warn_insufficient_qty_repairDTO> list = new ArrayList<Stock_warn_insufficient_qty_repairDTO>();
        
        Page<Stock_warn_insufficient_qty_repair> domains = stock_warn_insufficient_qty_repairService.searchDefault(context) ;
        for(Stock_warn_insufficient_qty_repair stock_warn_insufficient_qty_repair : domains.getContent()){
            Stock_warn_insufficient_qty_repairDTO dto = new Stock_warn_insufficient_qty_repairDTO();
            dto.fromDO(stock_warn_insufficient_qty_repair);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
