package cn.ibizlab.odoo.service.odoo_stock.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_stock.dto.Stock_package_levelDTO;
import cn.ibizlab.odoo.core.odoo_stock.domain.Stock_package_level;
import cn.ibizlab.odoo.core.odoo_stock.service.IStock_package_levelService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_stock.filter.Stock_package_levelSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Stock_package_level" })
@RestController
@RequestMapping("")
public class Stock_package_levelResource {

    @Autowired
    private IStock_package_levelService stock_package_levelService;

    public IStock_package_levelService getStock_package_levelService() {
        return this.stock_package_levelService;
    }

    @ApiOperation(value = "批删除数据", tags = {"Stock_package_level" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_stock/stock_package_levels/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Stock_package_levelDTO> stock_package_leveldtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "删除数据", tags = {"Stock_package_level" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_stock/stock_package_levels/{stock_package_level_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("stock_package_level_id") Integer stock_package_level_id) {
        Stock_package_levelDTO stock_package_leveldto = new Stock_package_levelDTO();
		Stock_package_level domain = new Stock_package_level();
		stock_package_leveldto.setId(stock_package_level_id);
		domain.setId(stock_package_level_id);
        Boolean rst = stock_package_levelService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "批更新数据", tags = {"Stock_package_level" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_stock/stock_package_levels/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Stock_package_levelDTO> stock_package_leveldtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "获取数据", tags = {"Stock_package_level" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_stock/stock_package_levels/{stock_package_level_id}")
    public ResponseEntity<Stock_package_levelDTO> get(@PathVariable("stock_package_level_id") Integer stock_package_level_id) {
        Stock_package_levelDTO dto = new Stock_package_levelDTO();
        Stock_package_level domain = stock_package_levelService.get(stock_package_level_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "建立数据", tags = {"Stock_package_level" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_stock/stock_package_levels")

    public ResponseEntity<Stock_package_levelDTO> create(@RequestBody Stock_package_levelDTO stock_package_leveldto) {
        Stock_package_levelDTO dto = new Stock_package_levelDTO();
        Stock_package_level domain = stock_package_leveldto.toDO();
		stock_package_levelService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批建立数据", tags = {"Stock_package_level" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_stock/stock_package_levels/createBatch")
    public ResponseEntity<Boolean> createBatchStock_package_level(@RequestBody List<Stock_package_levelDTO> stock_package_leveldtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "更新数据", tags = {"Stock_package_level" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_stock/stock_package_levels/{stock_package_level_id}")

    public ResponseEntity<Stock_package_levelDTO> update(@PathVariable("stock_package_level_id") Integer stock_package_level_id, @RequestBody Stock_package_levelDTO stock_package_leveldto) {
		Stock_package_level domain = stock_package_leveldto.toDO();
        domain.setId(stock_package_level_id);
		stock_package_levelService.update(domain);
		Stock_package_levelDTO dto = new Stock_package_levelDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Stock_package_level" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_stock/stock_package_levels/fetchdefault")
	public ResponseEntity<Page<Stock_package_levelDTO>> fetchDefault(Stock_package_levelSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Stock_package_levelDTO> list = new ArrayList<Stock_package_levelDTO>();
        
        Page<Stock_package_level> domains = stock_package_levelService.searchDefault(context) ;
        for(Stock_package_level stock_package_level : domains.getContent()){
            Stock_package_levelDTO dto = new Stock_package_levelDTO();
            dto.fromDO(stock_package_level);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
