package cn.ibizlab.odoo.service.odoo_stock.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.core.odoo_stock.valuerule.anno.stock_warehouse.*;
import cn.ibizlab.odoo.core.odoo_stock.domain.Stock_warehouse;
import org.springframework.cglib.beans.BeanCopier;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 服务DTO对象[Stock_warehouseDTO]
 */
public class Stock_warehouseDTO implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [RESUPPLY_ROUTE_IDS]
     *
     */
    @Stock_warehouseResupply_route_idsDefault(info = "默认规则")
    private String resupply_route_ids;

    @JsonIgnore
    private boolean resupply_route_idsDirtyFlag;

    /**
     * 属性 [ACTIVE]
     *
     */
    @Stock_warehouseActiveDefault(info = "默认规则")
    private String active;

    @JsonIgnore
    private boolean activeDirtyFlag;

    /**
     * 属性 [RESUPPLY_WH_IDS]
     *
     */
    @Stock_warehouseResupply_wh_idsDefault(info = "默认规则")
    private String resupply_wh_ids;

    @JsonIgnore
    private boolean resupply_wh_idsDirtyFlag;

    /**
     * 属性 [WRITE_DATE]
     *
     */
    @Stock_warehouseWrite_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp write_date;

    @JsonIgnore
    private boolean write_dateDirtyFlag;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @Stock_warehouse__last_updateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp __last_update;

    @JsonIgnore
    private boolean __last_updateDirtyFlag;

    /**
     * 属性 [NAME]
     *
     */
    @Stock_warehouseNameDefault(info = "默认规则")
    private String name;

    @JsonIgnore
    private boolean nameDirtyFlag;

    /**
     * 属性 [DELIVERY_STEPS]
     *
     */
    @Stock_warehouseDelivery_stepsDefault(info = "默认规则")
    private String delivery_steps;

    @JsonIgnore
    private boolean delivery_stepsDirtyFlag;

    /**
     * 属性 [WAREHOUSE_COUNT]
     *
     */
    @Stock_warehouseWarehouse_countDefault(info = "默认规则")
    private Integer warehouse_count;

    @JsonIgnore
    private boolean warehouse_countDirtyFlag;

    /**
     * 属性 [MANUFACTURE_TO_RESUPPLY]
     *
     */
    @Stock_warehouseManufacture_to_resupplyDefault(info = "默认规则")
    private String manufacture_to_resupply;

    @JsonIgnore
    private boolean manufacture_to_resupplyDirtyFlag;

    /**
     * 属性 [CREATE_DATE]
     *
     */
    @Stock_warehouseCreate_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp create_date;

    @JsonIgnore
    private boolean create_dateDirtyFlag;

    /**
     * 属性 [ROUTE_IDS]
     *
     */
    @Stock_warehouseRoute_idsDefault(info = "默认规则")
    private String route_ids;

    @JsonIgnore
    private boolean route_idsDirtyFlag;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @Stock_warehouseDisplay_nameDefault(info = "默认规则")
    private String display_name;

    @JsonIgnore
    private boolean display_nameDirtyFlag;

    /**
     * 属性 [RECEPTION_STEPS]
     *
     */
    @Stock_warehouseReception_stepsDefault(info = "默认规则")
    private String reception_steps;

    @JsonIgnore
    private boolean reception_stepsDirtyFlag;

    /**
     * 属性 [BUY_TO_RESUPPLY]
     *
     */
    @Stock_warehouseBuy_to_resupplyDefault(info = "默认规则")
    private String buy_to_resupply;

    @JsonIgnore
    private boolean buy_to_resupplyDirtyFlag;

    /**
     * 属性 [ID]
     *
     */
    @Stock_warehouseIdDefault(info = "默认规则")
    private Integer id;

    @JsonIgnore
    private boolean idDirtyFlag;

    /**
     * 属性 [CODE]
     *
     */
    @Stock_warehouseCodeDefault(info = "默认规则")
    private String code;

    @JsonIgnore
    private boolean codeDirtyFlag;

    /**
     * 属性 [MANUFACTURE_STEPS]
     *
     */
    @Stock_warehouseManufacture_stepsDefault(info = "默认规则")
    private String manufacture_steps;

    @JsonIgnore
    private boolean manufacture_stepsDirtyFlag;

    /**
     * 属性 [SHOW_RESUPPLY]
     *
     */
    @Stock_warehouseShow_resupplyDefault(info = "默认规则")
    private String show_resupply;

    @JsonIgnore
    private boolean show_resupplyDirtyFlag;

    /**
     * 属性 [VIEW_LOCATION_ID_TEXT]
     *
     */
    @Stock_warehouseView_location_id_textDefault(info = "默认规则")
    private String view_location_id_text;

    @JsonIgnore
    private boolean view_location_id_textDirtyFlag;

    /**
     * 属性 [WH_INPUT_STOCK_LOC_ID_TEXT]
     *
     */
    @Stock_warehouseWh_input_stock_loc_id_textDefault(info = "默认规则")
    private String wh_input_stock_loc_id_text;

    @JsonIgnore
    private boolean wh_input_stock_loc_id_textDirtyFlag;

    /**
     * 属性 [SAM_LOC_ID_TEXT]
     *
     */
    @Stock_warehouseSam_loc_id_textDefault(info = "默认规则")
    private String sam_loc_id_text;

    @JsonIgnore
    private boolean sam_loc_id_textDirtyFlag;

    /**
     * 属性 [CROSSDOCK_ROUTE_ID_TEXT]
     *
     */
    @Stock_warehouseCrossdock_route_id_textDefault(info = "默认规则")
    private String crossdock_route_id_text;

    @JsonIgnore
    private boolean crossdock_route_id_textDirtyFlag;

    /**
     * 属性 [PBM_ROUTE_ID_TEXT]
     *
     */
    @Stock_warehousePbm_route_id_textDefault(info = "默认规则")
    private String pbm_route_id_text;

    @JsonIgnore
    private boolean pbm_route_id_textDirtyFlag;

    /**
     * 属性 [WRITE_UID_TEXT]
     *
     */
    @Stock_warehouseWrite_uid_textDefault(info = "默认规则")
    private String write_uid_text;

    @JsonIgnore
    private boolean write_uid_textDirtyFlag;

    /**
     * 属性 [PBM_TYPE_ID_TEXT]
     *
     */
    @Stock_warehousePbm_type_id_textDefault(info = "默认规则")
    private String pbm_type_id_text;

    @JsonIgnore
    private boolean pbm_type_id_textDirtyFlag;

    /**
     * 属性 [PICK_TYPE_ID_TEXT]
     *
     */
    @Stock_warehousePick_type_id_textDefault(info = "默认规则")
    private String pick_type_id_text;

    @JsonIgnore
    private boolean pick_type_id_textDirtyFlag;

    /**
     * 属性 [INT_TYPE_ID_TEXT]
     *
     */
    @Stock_warehouseInt_type_id_textDefault(info = "默认规则")
    private String int_type_id_text;

    @JsonIgnore
    private boolean int_type_id_textDirtyFlag;

    /**
     * 属性 [MANU_TYPE_ID_TEXT]
     *
     */
    @Stock_warehouseManu_type_id_textDefault(info = "默认规则")
    private String manu_type_id_text;

    @JsonIgnore
    private boolean manu_type_id_textDirtyFlag;

    /**
     * 属性 [BUY_PULL_ID_TEXT]
     *
     */
    @Stock_warehouseBuy_pull_id_textDefault(info = "默认规则")
    private String buy_pull_id_text;

    @JsonIgnore
    private boolean buy_pull_id_textDirtyFlag;

    /**
     * 属性 [WH_PACK_STOCK_LOC_ID_TEXT]
     *
     */
    @Stock_warehouseWh_pack_stock_loc_id_textDefault(info = "默认规则")
    private String wh_pack_stock_loc_id_text;

    @JsonIgnore
    private boolean wh_pack_stock_loc_id_textDirtyFlag;

    /**
     * 属性 [MANUFACTURE_PULL_ID_TEXT]
     *
     */
    @Stock_warehouseManufacture_pull_id_textDefault(info = "默认规则")
    private String manufacture_pull_id_text;

    @JsonIgnore
    private boolean manufacture_pull_id_textDirtyFlag;

    /**
     * 属性 [RECEPTION_ROUTE_ID_TEXT]
     *
     */
    @Stock_warehouseReception_route_id_textDefault(info = "默认规则")
    private String reception_route_id_text;

    @JsonIgnore
    private boolean reception_route_id_textDirtyFlag;

    /**
     * 属性 [DELIVERY_ROUTE_ID_TEXT]
     *
     */
    @Stock_warehouseDelivery_route_id_textDefault(info = "默认规则")
    private String delivery_route_id_text;

    @JsonIgnore
    private boolean delivery_route_id_textDirtyFlag;

    /**
     * 属性 [SAM_TYPE_ID_TEXT]
     *
     */
    @Stock_warehouseSam_type_id_textDefault(info = "默认规则")
    private String sam_type_id_text;

    @JsonIgnore
    private boolean sam_type_id_textDirtyFlag;

    /**
     * 属性 [PARTNER_ID_TEXT]
     *
     */
    @Stock_warehousePartner_id_textDefault(info = "默认规则")
    private String partner_id_text;

    @JsonIgnore
    private boolean partner_id_textDirtyFlag;

    /**
     * 属性 [OUT_TYPE_ID_TEXT]
     *
     */
    @Stock_warehouseOut_type_id_textDefault(info = "默认规则")
    private String out_type_id_text;

    @JsonIgnore
    private boolean out_type_id_textDirtyFlag;

    /**
     * 属性 [MTO_PULL_ID_TEXT]
     *
     */
    @Stock_warehouseMto_pull_id_textDefault(info = "默认规则")
    private String mto_pull_id_text;

    @JsonIgnore
    private boolean mto_pull_id_textDirtyFlag;

    /**
     * 属性 [CREATE_UID_TEXT]
     *
     */
    @Stock_warehouseCreate_uid_textDefault(info = "默认规则")
    private String create_uid_text;

    @JsonIgnore
    private boolean create_uid_textDirtyFlag;

    /**
     * 属性 [LOT_STOCK_ID_TEXT]
     *
     */
    @Stock_warehouseLot_stock_id_textDefault(info = "默认规则")
    private String lot_stock_id_text;

    @JsonIgnore
    private boolean lot_stock_id_textDirtyFlag;

    /**
     * 属性 [PBM_MTO_PULL_ID_TEXT]
     *
     */
    @Stock_warehousePbm_mto_pull_id_textDefault(info = "默认规则")
    private String pbm_mto_pull_id_text;

    @JsonIgnore
    private boolean pbm_mto_pull_id_textDirtyFlag;

    /**
     * 属性 [COMPANY_ID_TEXT]
     *
     */
    @Stock_warehouseCompany_id_textDefault(info = "默认规则")
    private String company_id_text;

    @JsonIgnore
    private boolean company_id_textDirtyFlag;

    /**
     * 属性 [PBM_LOC_ID_TEXT]
     *
     */
    @Stock_warehousePbm_loc_id_textDefault(info = "默认规则")
    private String pbm_loc_id_text;

    @JsonIgnore
    private boolean pbm_loc_id_textDirtyFlag;

    /**
     * 属性 [PACK_TYPE_ID_TEXT]
     *
     */
    @Stock_warehousePack_type_id_textDefault(info = "默认规则")
    private String pack_type_id_text;

    @JsonIgnore
    private boolean pack_type_id_textDirtyFlag;

    /**
     * 属性 [SAM_RULE_ID_TEXT]
     *
     */
    @Stock_warehouseSam_rule_id_textDefault(info = "默认规则")
    private String sam_rule_id_text;

    @JsonIgnore
    private boolean sam_rule_id_textDirtyFlag;

    /**
     * 属性 [IN_TYPE_ID_TEXT]
     *
     */
    @Stock_warehouseIn_type_id_textDefault(info = "默认规则")
    private String in_type_id_text;

    @JsonIgnore
    private boolean in_type_id_textDirtyFlag;

    /**
     * 属性 [WH_OUTPUT_STOCK_LOC_ID_TEXT]
     *
     */
    @Stock_warehouseWh_output_stock_loc_id_textDefault(info = "默认规则")
    private String wh_output_stock_loc_id_text;

    @JsonIgnore
    private boolean wh_output_stock_loc_id_textDirtyFlag;

    /**
     * 属性 [WH_QC_STOCK_LOC_ID_TEXT]
     *
     */
    @Stock_warehouseWh_qc_stock_loc_id_textDefault(info = "默认规则")
    private String wh_qc_stock_loc_id_text;

    @JsonIgnore
    private boolean wh_qc_stock_loc_id_textDirtyFlag;

    /**
     * 属性 [WH_OUTPUT_STOCK_LOC_ID]
     *
     */
    @Stock_warehouseWh_output_stock_loc_idDefault(info = "默认规则")
    private Integer wh_output_stock_loc_id;

    @JsonIgnore
    private boolean wh_output_stock_loc_idDirtyFlag;

    /**
     * 属性 [WRITE_UID]
     *
     */
    @Stock_warehouseWrite_uidDefault(info = "默认规则")
    private Integer write_uid;

    @JsonIgnore
    private boolean write_uidDirtyFlag;

    /**
     * 属性 [PBM_LOC_ID]
     *
     */
    @Stock_warehousePbm_loc_idDefault(info = "默认规则")
    private Integer pbm_loc_id;

    @JsonIgnore
    private boolean pbm_loc_idDirtyFlag;

    /**
     * 属性 [VIEW_LOCATION_ID]
     *
     */
    @Stock_warehouseView_location_idDefault(info = "默认规则")
    private Integer view_location_id;

    @JsonIgnore
    private boolean view_location_idDirtyFlag;

    /**
     * 属性 [OUT_TYPE_ID]
     *
     */
    @Stock_warehouseOut_type_idDefault(info = "默认规则")
    private Integer out_type_id;

    @JsonIgnore
    private boolean out_type_idDirtyFlag;

    /**
     * 属性 [MANU_TYPE_ID]
     *
     */
    @Stock_warehouseManu_type_idDefault(info = "默认规则")
    private Integer manu_type_id;

    @JsonIgnore
    private boolean manu_type_idDirtyFlag;

    /**
     * 属性 [PBM_MTO_PULL_ID]
     *
     */
    @Stock_warehousePbm_mto_pull_idDefault(info = "默认规则")
    private Integer pbm_mto_pull_id;

    @JsonIgnore
    private boolean pbm_mto_pull_idDirtyFlag;

    /**
     * 属性 [MTO_PULL_ID]
     *
     */
    @Stock_warehouseMto_pull_idDefault(info = "默认规则")
    private Integer mto_pull_id;

    @JsonIgnore
    private boolean mto_pull_idDirtyFlag;

    /**
     * 属性 [SAM_TYPE_ID]
     *
     */
    @Stock_warehouseSam_type_idDefault(info = "默认规则")
    private Integer sam_type_id;

    @JsonIgnore
    private boolean sam_type_idDirtyFlag;

    /**
     * 属性 [MANUFACTURE_PULL_ID]
     *
     */
    @Stock_warehouseManufacture_pull_idDefault(info = "默认规则")
    private Integer manufacture_pull_id;

    @JsonIgnore
    private boolean manufacture_pull_idDirtyFlag;

    /**
     * 属性 [SAM_LOC_ID]
     *
     */
    @Stock_warehouseSam_loc_idDefault(info = "默认规则")
    private Integer sam_loc_id;

    @JsonIgnore
    private boolean sam_loc_idDirtyFlag;

    /**
     * 属性 [BUY_PULL_ID]
     *
     */
    @Stock_warehouseBuy_pull_idDefault(info = "默认规则")
    private Integer buy_pull_id;

    @JsonIgnore
    private boolean buy_pull_idDirtyFlag;

    /**
     * 属性 [INT_TYPE_ID]
     *
     */
    @Stock_warehouseInt_type_idDefault(info = "默认规则")
    private Integer int_type_id;

    @JsonIgnore
    private boolean int_type_idDirtyFlag;

    /**
     * 属性 [WH_QC_STOCK_LOC_ID]
     *
     */
    @Stock_warehouseWh_qc_stock_loc_idDefault(info = "默认规则")
    private Integer wh_qc_stock_loc_id;

    @JsonIgnore
    private boolean wh_qc_stock_loc_idDirtyFlag;

    /**
     * 属性 [IN_TYPE_ID]
     *
     */
    @Stock_warehouseIn_type_idDefault(info = "默认规则")
    private Integer in_type_id;

    @JsonIgnore
    private boolean in_type_idDirtyFlag;

    /**
     * 属性 [PBM_ROUTE_ID]
     *
     */
    @Stock_warehousePbm_route_idDefault(info = "默认规则")
    private Integer pbm_route_id;

    @JsonIgnore
    private boolean pbm_route_idDirtyFlag;

    /**
     * 属性 [PICK_TYPE_ID]
     *
     */
    @Stock_warehousePick_type_idDefault(info = "默认规则")
    private Integer pick_type_id;

    @JsonIgnore
    private boolean pick_type_idDirtyFlag;

    /**
     * 属性 [CREATE_UID]
     *
     */
    @Stock_warehouseCreate_uidDefault(info = "默认规则")
    private Integer create_uid;

    @JsonIgnore
    private boolean create_uidDirtyFlag;

    /**
     * 属性 [PARTNER_ID]
     *
     */
    @Stock_warehousePartner_idDefault(info = "默认规则")
    private Integer partner_id;

    @JsonIgnore
    private boolean partner_idDirtyFlag;

    /**
     * 属性 [COMPANY_ID]
     *
     */
    @Stock_warehouseCompany_idDefault(info = "默认规则")
    private Integer company_id;

    @JsonIgnore
    private boolean company_idDirtyFlag;

    /**
     * 属性 [RECEPTION_ROUTE_ID]
     *
     */
    @Stock_warehouseReception_route_idDefault(info = "默认规则")
    private Integer reception_route_id;

    @JsonIgnore
    private boolean reception_route_idDirtyFlag;

    /**
     * 属性 [PACK_TYPE_ID]
     *
     */
    @Stock_warehousePack_type_idDefault(info = "默认规则")
    private Integer pack_type_id;

    @JsonIgnore
    private boolean pack_type_idDirtyFlag;

    /**
     * 属性 [PBM_TYPE_ID]
     *
     */
    @Stock_warehousePbm_type_idDefault(info = "默认规则")
    private Integer pbm_type_id;

    @JsonIgnore
    private boolean pbm_type_idDirtyFlag;

    /**
     * 属性 [DELIVERY_ROUTE_ID]
     *
     */
    @Stock_warehouseDelivery_route_idDefault(info = "默认规则")
    private Integer delivery_route_id;

    @JsonIgnore
    private boolean delivery_route_idDirtyFlag;

    /**
     * 属性 [WH_INPUT_STOCK_LOC_ID]
     *
     */
    @Stock_warehouseWh_input_stock_loc_idDefault(info = "默认规则")
    private Integer wh_input_stock_loc_id;

    @JsonIgnore
    private boolean wh_input_stock_loc_idDirtyFlag;

    /**
     * 属性 [LOT_STOCK_ID]
     *
     */
    @Stock_warehouseLot_stock_idDefault(info = "默认规则")
    private Integer lot_stock_id;

    @JsonIgnore
    private boolean lot_stock_idDirtyFlag;

    /**
     * 属性 [SAM_RULE_ID]
     *
     */
    @Stock_warehouseSam_rule_idDefault(info = "默认规则")
    private Integer sam_rule_id;

    @JsonIgnore
    private boolean sam_rule_idDirtyFlag;

    /**
     * 属性 [CROSSDOCK_ROUTE_ID]
     *
     */
    @Stock_warehouseCrossdock_route_idDefault(info = "默认规则")
    private Integer crossdock_route_id;

    @JsonIgnore
    private boolean crossdock_route_idDirtyFlag;

    /**
     * 属性 [WH_PACK_STOCK_LOC_ID]
     *
     */
    @Stock_warehouseWh_pack_stock_loc_idDefault(info = "默认规则")
    private Integer wh_pack_stock_loc_id;

    @JsonIgnore
    private boolean wh_pack_stock_loc_idDirtyFlag;


    /**
     * 获取 [RESUPPLY_ROUTE_IDS]
     */
    @JsonProperty("resupply_route_ids")
    public String getResupply_route_ids(){
        return resupply_route_ids ;
    }

    /**
     * 设置 [RESUPPLY_ROUTE_IDS]
     */
    @JsonProperty("resupply_route_ids")
    public void setResupply_route_ids(String  resupply_route_ids){
        this.resupply_route_ids = resupply_route_ids ;
        this.resupply_route_idsDirtyFlag = true ;
    }

    /**
     * 获取 [RESUPPLY_ROUTE_IDS]脏标记
     */
    @JsonIgnore
    public boolean getResupply_route_idsDirtyFlag(){
        return resupply_route_idsDirtyFlag ;
    }

    /**
     * 获取 [ACTIVE]
     */
    @JsonProperty("active")
    public String getActive(){
        return active ;
    }

    /**
     * 设置 [ACTIVE]
     */
    @JsonProperty("active")
    public void setActive(String  active){
        this.active = active ;
        this.activeDirtyFlag = true ;
    }

    /**
     * 获取 [ACTIVE]脏标记
     */
    @JsonIgnore
    public boolean getActiveDirtyFlag(){
        return activeDirtyFlag ;
    }

    /**
     * 获取 [RESUPPLY_WH_IDS]
     */
    @JsonProperty("resupply_wh_ids")
    public String getResupply_wh_ids(){
        return resupply_wh_ids ;
    }

    /**
     * 设置 [RESUPPLY_WH_IDS]
     */
    @JsonProperty("resupply_wh_ids")
    public void setResupply_wh_ids(String  resupply_wh_ids){
        this.resupply_wh_ids = resupply_wh_ids ;
        this.resupply_wh_idsDirtyFlag = true ;
    }

    /**
     * 获取 [RESUPPLY_WH_IDS]脏标记
     */
    @JsonIgnore
    public boolean getResupply_wh_idsDirtyFlag(){
        return resupply_wh_idsDirtyFlag ;
    }

    /**
     * 获取 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return write_date ;
    }

    /**
     * 设置 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return write_dateDirtyFlag ;
    }

    /**
     * 获取 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return __last_update ;
    }

    /**
     * 设置 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

    /**
     * 获取 [__LAST_UPDATE]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return __last_updateDirtyFlag ;
    }

    /**
     * 获取 [NAME]
     */
    @JsonProperty("name")
    public String getName(){
        return name ;
    }

    /**
     * 设置 [NAME]
     */
    @JsonProperty("name")
    public void setName(String  name){
        this.name = name ;
        this.nameDirtyFlag = true ;
    }

    /**
     * 获取 [NAME]脏标记
     */
    @JsonIgnore
    public boolean getNameDirtyFlag(){
        return nameDirtyFlag ;
    }

    /**
     * 获取 [DELIVERY_STEPS]
     */
    @JsonProperty("delivery_steps")
    public String getDelivery_steps(){
        return delivery_steps ;
    }

    /**
     * 设置 [DELIVERY_STEPS]
     */
    @JsonProperty("delivery_steps")
    public void setDelivery_steps(String  delivery_steps){
        this.delivery_steps = delivery_steps ;
        this.delivery_stepsDirtyFlag = true ;
    }

    /**
     * 获取 [DELIVERY_STEPS]脏标记
     */
    @JsonIgnore
    public boolean getDelivery_stepsDirtyFlag(){
        return delivery_stepsDirtyFlag ;
    }

    /**
     * 获取 [WAREHOUSE_COUNT]
     */
    @JsonProperty("warehouse_count")
    public Integer getWarehouse_count(){
        return warehouse_count ;
    }

    /**
     * 设置 [WAREHOUSE_COUNT]
     */
    @JsonProperty("warehouse_count")
    public void setWarehouse_count(Integer  warehouse_count){
        this.warehouse_count = warehouse_count ;
        this.warehouse_countDirtyFlag = true ;
    }

    /**
     * 获取 [WAREHOUSE_COUNT]脏标记
     */
    @JsonIgnore
    public boolean getWarehouse_countDirtyFlag(){
        return warehouse_countDirtyFlag ;
    }

    /**
     * 获取 [MANUFACTURE_TO_RESUPPLY]
     */
    @JsonProperty("manufacture_to_resupply")
    public String getManufacture_to_resupply(){
        return manufacture_to_resupply ;
    }

    /**
     * 设置 [MANUFACTURE_TO_RESUPPLY]
     */
    @JsonProperty("manufacture_to_resupply")
    public void setManufacture_to_resupply(String  manufacture_to_resupply){
        this.manufacture_to_resupply = manufacture_to_resupply ;
        this.manufacture_to_resupplyDirtyFlag = true ;
    }

    /**
     * 获取 [MANUFACTURE_TO_RESUPPLY]脏标记
     */
    @JsonIgnore
    public boolean getManufacture_to_resupplyDirtyFlag(){
        return manufacture_to_resupplyDirtyFlag ;
    }

    /**
     * 获取 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return create_date ;
    }

    /**
     * 设置 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return create_dateDirtyFlag ;
    }

    /**
     * 获取 [ROUTE_IDS]
     */
    @JsonProperty("route_ids")
    public String getRoute_ids(){
        return route_ids ;
    }

    /**
     * 设置 [ROUTE_IDS]
     */
    @JsonProperty("route_ids")
    public void setRoute_ids(String  route_ids){
        this.route_ids = route_ids ;
        this.route_idsDirtyFlag = true ;
    }

    /**
     * 获取 [ROUTE_IDS]脏标记
     */
    @JsonIgnore
    public boolean getRoute_idsDirtyFlag(){
        return route_idsDirtyFlag ;
    }

    /**
     * 获取 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return display_name ;
    }

    /**
     * 设置 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

    /**
     * 获取 [DISPLAY_NAME]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return display_nameDirtyFlag ;
    }

    /**
     * 获取 [RECEPTION_STEPS]
     */
    @JsonProperty("reception_steps")
    public String getReception_steps(){
        return reception_steps ;
    }

    /**
     * 设置 [RECEPTION_STEPS]
     */
    @JsonProperty("reception_steps")
    public void setReception_steps(String  reception_steps){
        this.reception_steps = reception_steps ;
        this.reception_stepsDirtyFlag = true ;
    }

    /**
     * 获取 [RECEPTION_STEPS]脏标记
     */
    @JsonIgnore
    public boolean getReception_stepsDirtyFlag(){
        return reception_stepsDirtyFlag ;
    }

    /**
     * 获取 [BUY_TO_RESUPPLY]
     */
    @JsonProperty("buy_to_resupply")
    public String getBuy_to_resupply(){
        return buy_to_resupply ;
    }

    /**
     * 设置 [BUY_TO_RESUPPLY]
     */
    @JsonProperty("buy_to_resupply")
    public void setBuy_to_resupply(String  buy_to_resupply){
        this.buy_to_resupply = buy_to_resupply ;
        this.buy_to_resupplyDirtyFlag = true ;
    }

    /**
     * 获取 [BUY_TO_RESUPPLY]脏标记
     */
    @JsonIgnore
    public boolean getBuy_to_resupplyDirtyFlag(){
        return buy_to_resupplyDirtyFlag ;
    }

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

    /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return idDirtyFlag ;
    }

    /**
     * 获取 [CODE]
     */
    @JsonProperty("code")
    public String getCode(){
        return code ;
    }

    /**
     * 设置 [CODE]
     */
    @JsonProperty("code")
    public void setCode(String  code){
        this.code = code ;
        this.codeDirtyFlag = true ;
    }

    /**
     * 获取 [CODE]脏标记
     */
    @JsonIgnore
    public boolean getCodeDirtyFlag(){
        return codeDirtyFlag ;
    }

    /**
     * 获取 [MANUFACTURE_STEPS]
     */
    @JsonProperty("manufacture_steps")
    public String getManufacture_steps(){
        return manufacture_steps ;
    }

    /**
     * 设置 [MANUFACTURE_STEPS]
     */
    @JsonProperty("manufacture_steps")
    public void setManufacture_steps(String  manufacture_steps){
        this.manufacture_steps = manufacture_steps ;
        this.manufacture_stepsDirtyFlag = true ;
    }

    /**
     * 获取 [MANUFACTURE_STEPS]脏标记
     */
    @JsonIgnore
    public boolean getManufacture_stepsDirtyFlag(){
        return manufacture_stepsDirtyFlag ;
    }

    /**
     * 获取 [SHOW_RESUPPLY]
     */
    @JsonProperty("show_resupply")
    public String getShow_resupply(){
        return show_resupply ;
    }

    /**
     * 设置 [SHOW_RESUPPLY]
     */
    @JsonProperty("show_resupply")
    public void setShow_resupply(String  show_resupply){
        this.show_resupply = show_resupply ;
        this.show_resupplyDirtyFlag = true ;
    }

    /**
     * 获取 [SHOW_RESUPPLY]脏标记
     */
    @JsonIgnore
    public boolean getShow_resupplyDirtyFlag(){
        return show_resupplyDirtyFlag ;
    }

    /**
     * 获取 [VIEW_LOCATION_ID_TEXT]
     */
    @JsonProperty("view_location_id_text")
    public String getView_location_id_text(){
        return view_location_id_text ;
    }

    /**
     * 设置 [VIEW_LOCATION_ID_TEXT]
     */
    @JsonProperty("view_location_id_text")
    public void setView_location_id_text(String  view_location_id_text){
        this.view_location_id_text = view_location_id_text ;
        this.view_location_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [VIEW_LOCATION_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getView_location_id_textDirtyFlag(){
        return view_location_id_textDirtyFlag ;
    }

    /**
     * 获取 [WH_INPUT_STOCK_LOC_ID_TEXT]
     */
    @JsonProperty("wh_input_stock_loc_id_text")
    public String getWh_input_stock_loc_id_text(){
        return wh_input_stock_loc_id_text ;
    }

    /**
     * 设置 [WH_INPUT_STOCK_LOC_ID_TEXT]
     */
    @JsonProperty("wh_input_stock_loc_id_text")
    public void setWh_input_stock_loc_id_text(String  wh_input_stock_loc_id_text){
        this.wh_input_stock_loc_id_text = wh_input_stock_loc_id_text ;
        this.wh_input_stock_loc_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [WH_INPUT_STOCK_LOC_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getWh_input_stock_loc_id_textDirtyFlag(){
        return wh_input_stock_loc_id_textDirtyFlag ;
    }

    /**
     * 获取 [SAM_LOC_ID_TEXT]
     */
    @JsonProperty("sam_loc_id_text")
    public String getSam_loc_id_text(){
        return sam_loc_id_text ;
    }

    /**
     * 设置 [SAM_LOC_ID_TEXT]
     */
    @JsonProperty("sam_loc_id_text")
    public void setSam_loc_id_text(String  sam_loc_id_text){
        this.sam_loc_id_text = sam_loc_id_text ;
        this.sam_loc_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [SAM_LOC_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getSam_loc_id_textDirtyFlag(){
        return sam_loc_id_textDirtyFlag ;
    }

    /**
     * 获取 [CROSSDOCK_ROUTE_ID_TEXT]
     */
    @JsonProperty("crossdock_route_id_text")
    public String getCrossdock_route_id_text(){
        return crossdock_route_id_text ;
    }

    /**
     * 设置 [CROSSDOCK_ROUTE_ID_TEXT]
     */
    @JsonProperty("crossdock_route_id_text")
    public void setCrossdock_route_id_text(String  crossdock_route_id_text){
        this.crossdock_route_id_text = crossdock_route_id_text ;
        this.crossdock_route_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [CROSSDOCK_ROUTE_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getCrossdock_route_id_textDirtyFlag(){
        return crossdock_route_id_textDirtyFlag ;
    }

    /**
     * 获取 [PBM_ROUTE_ID_TEXT]
     */
    @JsonProperty("pbm_route_id_text")
    public String getPbm_route_id_text(){
        return pbm_route_id_text ;
    }

    /**
     * 设置 [PBM_ROUTE_ID_TEXT]
     */
    @JsonProperty("pbm_route_id_text")
    public void setPbm_route_id_text(String  pbm_route_id_text){
        this.pbm_route_id_text = pbm_route_id_text ;
        this.pbm_route_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [PBM_ROUTE_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getPbm_route_id_textDirtyFlag(){
        return pbm_route_id_textDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return write_uid_text ;
    }

    /**
     * 设置 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return write_uid_textDirtyFlag ;
    }

    /**
     * 获取 [PBM_TYPE_ID_TEXT]
     */
    @JsonProperty("pbm_type_id_text")
    public String getPbm_type_id_text(){
        return pbm_type_id_text ;
    }

    /**
     * 设置 [PBM_TYPE_ID_TEXT]
     */
    @JsonProperty("pbm_type_id_text")
    public void setPbm_type_id_text(String  pbm_type_id_text){
        this.pbm_type_id_text = pbm_type_id_text ;
        this.pbm_type_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [PBM_TYPE_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getPbm_type_id_textDirtyFlag(){
        return pbm_type_id_textDirtyFlag ;
    }

    /**
     * 获取 [PICK_TYPE_ID_TEXT]
     */
    @JsonProperty("pick_type_id_text")
    public String getPick_type_id_text(){
        return pick_type_id_text ;
    }

    /**
     * 设置 [PICK_TYPE_ID_TEXT]
     */
    @JsonProperty("pick_type_id_text")
    public void setPick_type_id_text(String  pick_type_id_text){
        this.pick_type_id_text = pick_type_id_text ;
        this.pick_type_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [PICK_TYPE_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getPick_type_id_textDirtyFlag(){
        return pick_type_id_textDirtyFlag ;
    }

    /**
     * 获取 [INT_TYPE_ID_TEXT]
     */
    @JsonProperty("int_type_id_text")
    public String getInt_type_id_text(){
        return int_type_id_text ;
    }

    /**
     * 设置 [INT_TYPE_ID_TEXT]
     */
    @JsonProperty("int_type_id_text")
    public void setInt_type_id_text(String  int_type_id_text){
        this.int_type_id_text = int_type_id_text ;
        this.int_type_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [INT_TYPE_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getInt_type_id_textDirtyFlag(){
        return int_type_id_textDirtyFlag ;
    }

    /**
     * 获取 [MANU_TYPE_ID_TEXT]
     */
    @JsonProperty("manu_type_id_text")
    public String getManu_type_id_text(){
        return manu_type_id_text ;
    }

    /**
     * 设置 [MANU_TYPE_ID_TEXT]
     */
    @JsonProperty("manu_type_id_text")
    public void setManu_type_id_text(String  manu_type_id_text){
        this.manu_type_id_text = manu_type_id_text ;
        this.manu_type_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [MANU_TYPE_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getManu_type_id_textDirtyFlag(){
        return manu_type_id_textDirtyFlag ;
    }

    /**
     * 获取 [BUY_PULL_ID_TEXT]
     */
    @JsonProperty("buy_pull_id_text")
    public String getBuy_pull_id_text(){
        return buy_pull_id_text ;
    }

    /**
     * 设置 [BUY_PULL_ID_TEXT]
     */
    @JsonProperty("buy_pull_id_text")
    public void setBuy_pull_id_text(String  buy_pull_id_text){
        this.buy_pull_id_text = buy_pull_id_text ;
        this.buy_pull_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [BUY_PULL_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getBuy_pull_id_textDirtyFlag(){
        return buy_pull_id_textDirtyFlag ;
    }

    /**
     * 获取 [WH_PACK_STOCK_LOC_ID_TEXT]
     */
    @JsonProperty("wh_pack_stock_loc_id_text")
    public String getWh_pack_stock_loc_id_text(){
        return wh_pack_stock_loc_id_text ;
    }

    /**
     * 设置 [WH_PACK_STOCK_LOC_ID_TEXT]
     */
    @JsonProperty("wh_pack_stock_loc_id_text")
    public void setWh_pack_stock_loc_id_text(String  wh_pack_stock_loc_id_text){
        this.wh_pack_stock_loc_id_text = wh_pack_stock_loc_id_text ;
        this.wh_pack_stock_loc_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [WH_PACK_STOCK_LOC_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getWh_pack_stock_loc_id_textDirtyFlag(){
        return wh_pack_stock_loc_id_textDirtyFlag ;
    }

    /**
     * 获取 [MANUFACTURE_PULL_ID_TEXT]
     */
    @JsonProperty("manufacture_pull_id_text")
    public String getManufacture_pull_id_text(){
        return manufacture_pull_id_text ;
    }

    /**
     * 设置 [MANUFACTURE_PULL_ID_TEXT]
     */
    @JsonProperty("manufacture_pull_id_text")
    public void setManufacture_pull_id_text(String  manufacture_pull_id_text){
        this.manufacture_pull_id_text = manufacture_pull_id_text ;
        this.manufacture_pull_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [MANUFACTURE_PULL_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getManufacture_pull_id_textDirtyFlag(){
        return manufacture_pull_id_textDirtyFlag ;
    }

    /**
     * 获取 [RECEPTION_ROUTE_ID_TEXT]
     */
    @JsonProperty("reception_route_id_text")
    public String getReception_route_id_text(){
        return reception_route_id_text ;
    }

    /**
     * 设置 [RECEPTION_ROUTE_ID_TEXT]
     */
    @JsonProperty("reception_route_id_text")
    public void setReception_route_id_text(String  reception_route_id_text){
        this.reception_route_id_text = reception_route_id_text ;
        this.reception_route_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [RECEPTION_ROUTE_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getReception_route_id_textDirtyFlag(){
        return reception_route_id_textDirtyFlag ;
    }

    /**
     * 获取 [DELIVERY_ROUTE_ID_TEXT]
     */
    @JsonProperty("delivery_route_id_text")
    public String getDelivery_route_id_text(){
        return delivery_route_id_text ;
    }

    /**
     * 设置 [DELIVERY_ROUTE_ID_TEXT]
     */
    @JsonProperty("delivery_route_id_text")
    public void setDelivery_route_id_text(String  delivery_route_id_text){
        this.delivery_route_id_text = delivery_route_id_text ;
        this.delivery_route_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [DELIVERY_ROUTE_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getDelivery_route_id_textDirtyFlag(){
        return delivery_route_id_textDirtyFlag ;
    }

    /**
     * 获取 [SAM_TYPE_ID_TEXT]
     */
    @JsonProperty("sam_type_id_text")
    public String getSam_type_id_text(){
        return sam_type_id_text ;
    }

    /**
     * 设置 [SAM_TYPE_ID_TEXT]
     */
    @JsonProperty("sam_type_id_text")
    public void setSam_type_id_text(String  sam_type_id_text){
        this.sam_type_id_text = sam_type_id_text ;
        this.sam_type_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [SAM_TYPE_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getSam_type_id_textDirtyFlag(){
        return sam_type_id_textDirtyFlag ;
    }

    /**
     * 获取 [PARTNER_ID_TEXT]
     */
    @JsonProperty("partner_id_text")
    public String getPartner_id_text(){
        return partner_id_text ;
    }

    /**
     * 设置 [PARTNER_ID_TEXT]
     */
    @JsonProperty("partner_id_text")
    public void setPartner_id_text(String  partner_id_text){
        this.partner_id_text = partner_id_text ;
        this.partner_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [PARTNER_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getPartner_id_textDirtyFlag(){
        return partner_id_textDirtyFlag ;
    }

    /**
     * 获取 [OUT_TYPE_ID_TEXT]
     */
    @JsonProperty("out_type_id_text")
    public String getOut_type_id_text(){
        return out_type_id_text ;
    }

    /**
     * 设置 [OUT_TYPE_ID_TEXT]
     */
    @JsonProperty("out_type_id_text")
    public void setOut_type_id_text(String  out_type_id_text){
        this.out_type_id_text = out_type_id_text ;
        this.out_type_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [OUT_TYPE_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getOut_type_id_textDirtyFlag(){
        return out_type_id_textDirtyFlag ;
    }

    /**
     * 获取 [MTO_PULL_ID_TEXT]
     */
    @JsonProperty("mto_pull_id_text")
    public String getMto_pull_id_text(){
        return mto_pull_id_text ;
    }

    /**
     * 设置 [MTO_PULL_ID_TEXT]
     */
    @JsonProperty("mto_pull_id_text")
    public void setMto_pull_id_text(String  mto_pull_id_text){
        this.mto_pull_id_text = mto_pull_id_text ;
        this.mto_pull_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [MTO_PULL_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getMto_pull_id_textDirtyFlag(){
        return mto_pull_id_textDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return create_uid_text ;
    }

    /**
     * 设置 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return create_uid_textDirtyFlag ;
    }

    /**
     * 获取 [LOT_STOCK_ID_TEXT]
     */
    @JsonProperty("lot_stock_id_text")
    public String getLot_stock_id_text(){
        return lot_stock_id_text ;
    }

    /**
     * 设置 [LOT_STOCK_ID_TEXT]
     */
    @JsonProperty("lot_stock_id_text")
    public void setLot_stock_id_text(String  lot_stock_id_text){
        this.lot_stock_id_text = lot_stock_id_text ;
        this.lot_stock_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [LOT_STOCK_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getLot_stock_id_textDirtyFlag(){
        return lot_stock_id_textDirtyFlag ;
    }

    /**
     * 获取 [PBM_MTO_PULL_ID_TEXT]
     */
    @JsonProperty("pbm_mto_pull_id_text")
    public String getPbm_mto_pull_id_text(){
        return pbm_mto_pull_id_text ;
    }

    /**
     * 设置 [PBM_MTO_PULL_ID_TEXT]
     */
    @JsonProperty("pbm_mto_pull_id_text")
    public void setPbm_mto_pull_id_text(String  pbm_mto_pull_id_text){
        this.pbm_mto_pull_id_text = pbm_mto_pull_id_text ;
        this.pbm_mto_pull_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [PBM_MTO_PULL_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getPbm_mto_pull_id_textDirtyFlag(){
        return pbm_mto_pull_id_textDirtyFlag ;
    }

    /**
     * 获取 [COMPANY_ID_TEXT]
     */
    @JsonProperty("company_id_text")
    public String getCompany_id_text(){
        return company_id_text ;
    }

    /**
     * 设置 [COMPANY_ID_TEXT]
     */
    @JsonProperty("company_id_text")
    public void setCompany_id_text(String  company_id_text){
        this.company_id_text = company_id_text ;
        this.company_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [COMPANY_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getCompany_id_textDirtyFlag(){
        return company_id_textDirtyFlag ;
    }

    /**
     * 获取 [PBM_LOC_ID_TEXT]
     */
    @JsonProperty("pbm_loc_id_text")
    public String getPbm_loc_id_text(){
        return pbm_loc_id_text ;
    }

    /**
     * 设置 [PBM_LOC_ID_TEXT]
     */
    @JsonProperty("pbm_loc_id_text")
    public void setPbm_loc_id_text(String  pbm_loc_id_text){
        this.pbm_loc_id_text = pbm_loc_id_text ;
        this.pbm_loc_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [PBM_LOC_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getPbm_loc_id_textDirtyFlag(){
        return pbm_loc_id_textDirtyFlag ;
    }

    /**
     * 获取 [PACK_TYPE_ID_TEXT]
     */
    @JsonProperty("pack_type_id_text")
    public String getPack_type_id_text(){
        return pack_type_id_text ;
    }

    /**
     * 设置 [PACK_TYPE_ID_TEXT]
     */
    @JsonProperty("pack_type_id_text")
    public void setPack_type_id_text(String  pack_type_id_text){
        this.pack_type_id_text = pack_type_id_text ;
        this.pack_type_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [PACK_TYPE_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getPack_type_id_textDirtyFlag(){
        return pack_type_id_textDirtyFlag ;
    }

    /**
     * 获取 [SAM_RULE_ID_TEXT]
     */
    @JsonProperty("sam_rule_id_text")
    public String getSam_rule_id_text(){
        return sam_rule_id_text ;
    }

    /**
     * 设置 [SAM_RULE_ID_TEXT]
     */
    @JsonProperty("sam_rule_id_text")
    public void setSam_rule_id_text(String  sam_rule_id_text){
        this.sam_rule_id_text = sam_rule_id_text ;
        this.sam_rule_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [SAM_RULE_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getSam_rule_id_textDirtyFlag(){
        return sam_rule_id_textDirtyFlag ;
    }

    /**
     * 获取 [IN_TYPE_ID_TEXT]
     */
    @JsonProperty("in_type_id_text")
    public String getIn_type_id_text(){
        return in_type_id_text ;
    }

    /**
     * 设置 [IN_TYPE_ID_TEXT]
     */
    @JsonProperty("in_type_id_text")
    public void setIn_type_id_text(String  in_type_id_text){
        this.in_type_id_text = in_type_id_text ;
        this.in_type_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [IN_TYPE_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getIn_type_id_textDirtyFlag(){
        return in_type_id_textDirtyFlag ;
    }

    /**
     * 获取 [WH_OUTPUT_STOCK_LOC_ID_TEXT]
     */
    @JsonProperty("wh_output_stock_loc_id_text")
    public String getWh_output_stock_loc_id_text(){
        return wh_output_stock_loc_id_text ;
    }

    /**
     * 设置 [WH_OUTPUT_STOCK_LOC_ID_TEXT]
     */
    @JsonProperty("wh_output_stock_loc_id_text")
    public void setWh_output_stock_loc_id_text(String  wh_output_stock_loc_id_text){
        this.wh_output_stock_loc_id_text = wh_output_stock_loc_id_text ;
        this.wh_output_stock_loc_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [WH_OUTPUT_STOCK_LOC_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getWh_output_stock_loc_id_textDirtyFlag(){
        return wh_output_stock_loc_id_textDirtyFlag ;
    }

    /**
     * 获取 [WH_QC_STOCK_LOC_ID_TEXT]
     */
    @JsonProperty("wh_qc_stock_loc_id_text")
    public String getWh_qc_stock_loc_id_text(){
        return wh_qc_stock_loc_id_text ;
    }

    /**
     * 设置 [WH_QC_STOCK_LOC_ID_TEXT]
     */
    @JsonProperty("wh_qc_stock_loc_id_text")
    public void setWh_qc_stock_loc_id_text(String  wh_qc_stock_loc_id_text){
        this.wh_qc_stock_loc_id_text = wh_qc_stock_loc_id_text ;
        this.wh_qc_stock_loc_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [WH_QC_STOCK_LOC_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getWh_qc_stock_loc_id_textDirtyFlag(){
        return wh_qc_stock_loc_id_textDirtyFlag ;
    }

    /**
     * 获取 [WH_OUTPUT_STOCK_LOC_ID]
     */
    @JsonProperty("wh_output_stock_loc_id")
    public Integer getWh_output_stock_loc_id(){
        return wh_output_stock_loc_id ;
    }

    /**
     * 设置 [WH_OUTPUT_STOCK_LOC_ID]
     */
    @JsonProperty("wh_output_stock_loc_id")
    public void setWh_output_stock_loc_id(Integer  wh_output_stock_loc_id){
        this.wh_output_stock_loc_id = wh_output_stock_loc_id ;
        this.wh_output_stock_loc_idDirtyFlag = true ;
    }

    /**
     * 获取 [WH_OUTPUT_STOCK_LOC_ID]脏标记
     */
    @JsonIgnore
    public boolean getWh_output_stock_loc_idDirtyFlag(){
        return wh_output_stock_loc_idDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return write_uid ;
    }

    /**
     * 设置 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return write_uidDirtyFlag ;
    }

    /**
     * 获取 [PBM_LOC_ID]
     */
    @JsonProperty("pbm_loc_id")
    public Integer getPbm_loc_id(){
        return pbm_loc_id ;
    }

    /**
     * 设置 [PBM_LOC_ID]
     */
    @JsonProperty("pbm_loc_id")
    public void setPbm_loc_id(Integer  pbm_loc_id){
        this.pbm_loc_id = pbm_loc_id ;
        this.pbm_loc_idDirtyFlag = true ;
    }

    /**
     * 获取 [PBM_LOC_ID]脏标记
     */
    @JsonIgnore
    public boolean getPbm_loc_idDirtyFlag(){
        return pbm_loc_idDirtyFlag ;
    }

    /**
     * 获取 [VIEW_LOCATION_ID]
     */
    @JsonProperty("view_location_id")
    public Integer getView_location_id(){
        return view_location_id ;
    }

    /**
     * 设置 [VIEW_LOCATION_ID]
     */
    @JsonProperty("view_location_id")
    public void setView_location_id(Integer  view_location_id){
        this.view_location_id = view_location_id ;
        this.view_location_idDirtyFlag = true ;
    }

    /**
     * 获取 [VIEW_LOCATION_ID]脏标记
     */
    @JsonIgnore
    public boolean getView_location_idDirtyFlag(){
        return view_location_idDirtyFlag ;
    }

    /**
     * 获取 [OUT_TYPE_ID]
     */
    @JsonProperty("out_type_id")
    public Integer getOut_type_id(){
        return out_type_id ;
    }

    /**
     * 设置 [OUT_TYPE_ID]
     */
    @JsonProperty("out_type_id")
    public void setOut_type_id(Integer  out_type_id){
        this.out_type_id = out_type_id ;
        this.out_type_idDirtyFlag = true ;
    }

    /**
     * 获取 [OUT_TYPE_ID]脏标记
     */
    @JsonIgnore
    public boolean getOut_type_idDirtyFlag(){
        return out_type_idDirtyFlag ;
    }

    /**
     * 获取 [MANU_TYPE_ID]
     */
    @JsonProperty("manu_type_id")
    public Integer getManu_type_id(){
        return manu_type_id ;
    }

    /**
     * 设置 [MANU_TYPE_ID]
     */
    @JsonProperty("manu_type_id")
    public void setManu_type_id(Integer  manu_type_id){
        this.manu_type_id = manu_type_id ;
        this.manu_type_idDirtyFlag = true ;
    }

    /**
     * 获取 [MANU_TYPE_ID]脏标记
     */
    @JsonIgnore
    public boolean getManu_type_idDirtyFlag(){
        return manu_type_idDirtyFlag ;
    }

    /**
     * 获取 [PBM_MTO_PULL_ID]
     */
    @JsonProperty("pbm_mto_pull_id")
    public Integer getPbm_mto_pull_id(){
        return pbm_mto_pull_id ;
    }

    /**
     * 设置 [PBM_MTO_PULL_ID]
     */
    @JsonProperty("pbm_mto_pull_id")
    public void setPbm_mto_pull_id(Integer  pbm_mto_pull_id){
        this.pbm_mto_pull_id = pbm_mto_pull_id ;
        this.pbm_mto_pull_idDirtyFlag = true ;
    }

    /**
     * 获取 [PBM_MTO_PULL_ID]脏标记
     */
    @JsonIgnore
    public boolean getPbm_mto_pull_idDirtyFlag(){
        return pbm_mto_pull_idDirtyFlag ;
    }

    /**
     * 获取 [MTO_PULL_ID]
     */
    @JsonProperty("mto_pull_id")
    public Integer getMto_pull_id(){
        return mto_pull_id ;
    }

    /**
     * 设置 [MTO_PULL_ID]
     */
    @JsonProperty("mto_pull_id")
    public void setMto_pull_id(Integer  mto_pull_id){
        this.mto_pull_id = mto_pull_id ;
        this.mto_pull_idDirtyFlag = true ;
    }

    /**
     * 获取 [MTO_PULL_ID]脏标记
     */
    @JsonIgnore
    public boolean getMto_pull_idDirtyFlag(){
        return mto_pull_idDirtyFlag ;
    }

    /**
     * 获取 [SAM_TYPE_ID]
     */
    @JsonProperty("sam_type_id")
    public Integer getSam_type_id(){
        return sam_type_id ;
    }

    /**
     * 设置 [SAM_TYPE_ID]
     */
    @JsonProperty("sam_type_id")
    public void setSam_type_id(Integer  sam_type_id){
        this.sam_type_id = sam_type_id ;
        this.sam_type_idDirtyFlag = true ;
    }

    /**
     * 获取 [SAM_TYPE_ID]脏标记
     */
    @JsonIgnore
    public boolean getSam_type_idDirtyFlag(){
        return sam_type_idDirtyFlag ;
    }

    /**
     * 获取 [MANUFACTURE_PULL_ID]
     */
    @JsonProperty("manufacture_pull_id")
    public Integer getManufacture_pull_id(){
        return manufacture_pull_id ;
    }

    /**
     * 设置 [MANUFACTURE_PULL_ID]
     */
    @JsonProperty("manufacture_pull_id")
    public void setManufacture_pull_id(Integer  manufacture_pull_id){
        this.manufacture_pull_id = manufacture_pull_id ;
        this.manufacture_pull_idDirtyFlag = true ;
    }

    /**
     * 获取 [MANUFACTURE_PULL_ID]脏标记
     */
    @JsonIgnore
    public boolean getManufacture_pull_idDirtyFlag(){
        return manufacture_pull_idDirtyFlag ;
    }

    /**
     * 获取 [SAM_LOC_ID]
     */
    @JsonProperty("sam_loc_id")
    public Integer getSam_loc_id(){
        return sam_loc_id ;
    }

    /**
     * 设置 [SAM_LOC_ID]
     */
    @JsonProperty("sam_loc_id")
    public void setSam_loc_id(Integer  sam_loc_id){
        this.sam_loc_id = sam_loc_id ;
        this.sam_loc_idDirtyFlag = true ;
    }

    /**
     * 获取 [SAM_LOC_ID]脏标记
     */
    @JsonIgnore
    public boolean getSam_loc_idDirtyFlag(){
        return sam_loc_idDirtyFlag ;
    }

    /**
     * 获取 [BUY_PULL_ID]
     */
    @JsonProperty("buy_pull_id")
    public Integer getBuy_pull_id(){
        return buy_pull_id ;
    }

    /**
     * 设置 [BUY_PULL_ID]
     */
    @JsonProperty("buy_pull_id")
    public void setBuy_pull_id(Integer  buy_pull_id){
        this.buy_pull_id = buy_pull_id ;
        this.buy_pull_idDirtyFlag = true ;
    }

    /**
     * 获取 [BUY_PULL_ID]脏标记
     */
    @JsonIgnore
    public boolean getBuy_pull_idDirtyFlag(){
        return buy_pull_idDirtyFlag ;
    }

    /**
     * 获取 [INT_TYPE_ID]
     */
    @JsonProperty("int_type_id")
    public Integer getInt_type_id(){
        return int_type_id ;
    }

    /**
     * 设置 [INT_TYPE_ID]
     */
    @JsonProperty("int_type_id")
    public void setInt_type_id(Integer  int_type_id){
        this.int_type_id = int_type_id ;
        this.int_type_idDirtyFlag = true ;
    }

    /**
     * 获取 [INT_TYPE_ID]脏标记
     */
    @JsonIgnore
    public boolean getInt_type_idDirtyFlag(){
        return int_type_idDirtyFlag ;
    }

    /**
     * 获取 [WH_QC_STOCK_LOC_ID]
     */
    @JsonProperty("wh_qc_stock_loc_id")
    public Integer getWh_qc_stock_loc_id(){
        return wh_qc_stock_loc_id ;
    }

    /**
     * 设置 [WH_QC_STOCK_LOC_ID]
     */
    @JsonProperty("wh_qc_stock_loc_id")
    public void setWh_qc_stock_loc_id(Integer  wh_qc_stock_loc_id){
        this.wh_qc_stock_loc_id = wh_qc_stock_loc_id ;
        this.wh_qc_stock_loc_idDirtyFlag = true ;
    }

    /**
     * 获取 [WH_QC_STOCK_LOC_ID]脏标记
     */
    @JsonIgnore
    public boolean getWh_qc_stock_loc_idDirtyFlag(){
        return wh_qc_stock_loc_idDirtyFlag ;
    }

    /**
     * 获取 [IN_TYPE_ID]
     */
    @JsonProperty("in_type_id")
    public Integer getIn_type_id(){
        return in_type_id ;
    }

    /**
     * 设置 [IN_TYPE_ID]
     */
    @JsonProperty("in_type_id")
    public void setIn_type_id(Integer  in_type_id){
        this.in_type_id = in_type_id ;
        this.in_type_idDirtyFlag = true ;
    }

    /**
     * 获取 [IN_TYPE_ID]脏标记
     */
    @JsonIgnore
    public boolean getIn_type_idDirtyFlag(){
        return in_type_idDirtyFlag ;
    }

    /**
     * 获取 [PBM_ROUTE_ID]
     */
    @JsonProperty("pbm_route_id")
    public Integer getPbm_route_id(){
        return pbm_route_id ;
    }

    /**
     * 设置 [PBM_ROUTE_ID]
     */
    @JsonProperty("pbm_route_id")
    public void setPbm_route_id(Integer  pbm_route_id){
        this.pbm_route_id = pbm_route_id ;
        this.pbm_route_idDirtyFlag = true ;
    }

    /**
     * 获取 [PBM_ROUTE_ID]脏标记
     */
    @JsonIgnore
    public boolean getPbm_route_idDirtyFlag(){
        return pbm_route_idDirtyFlag ;
    }

    /**
     * 获取 [PICK_TYPE_ID]
     */
    @JsonProperty("pick_type_id")
    public Integer getPick_type_id(){
        return pick_type_id ;
    }

    /**
     * 设置 [PICK_TYPE_ID]
     */
    @JsonProperty("pick_type_id")
    public void setPick_type_id(Integer  pick_type_id){
        this.pick_type_id = pick_type_id ;
        this.pick_type_idDirtyFlag = true ;
    }

    /**
     * 获取 [PICK_TYPE_ID]脏标记
     */
    @JsonIgnore
    public boolean getPick_type_idDirtyFlag(){
        return pick_type_idDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return create_uid ;
    }

    /**
     * 设置 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return create_uidDirtyFlag ;
    }

    /**
     * 获取 [PARTNER_ID]
     */
    @JsonProperty("partner_id")
    public Integer getPartner_id(){
        return partner_id ;
    }

    /**
     * 设置 [PARTNER_ID]
     */
    @JsonProperty("partner_id")
    public void setPartner_id(Integer  partner_id){
        this.partner_id = partner_id ;
        this.partner_idDirtyFlag = true ;
    }

    /**
     * 获取 [PARTNER_ID]脏标记
     */
    @JsonIgnore
    public boolean getPartner_idDirtyFlag(){
        return partner_idDirtyFlag ;
    }

    /**
     * 获取 [COMPANY_ID]
     */
    @JsonProperty("company_id")
    public Integer getCompany_id(){
        return company_id ;
    }

    /**
     * 设置 [COMPANY_ID]
     */
    @JsonProperty("company_id")
    public void setCompany_id(Integer  company_id){
        this.company_id = company_id ;
        this.company_idDirtyFlag = true ;
    }

    /**
     * 获取 [COMPANY_ID]脏标记
     */
    @JsonIgnore
    public boolean getCompany_idDirtyFlag(){
        return company_idDirtyFlag ;
    }

    /**
     * 获取 [RECEPTION_ROUTE_ID]
     */
    @JsonProperty("reception_route_id")
    public Integer getReception_route_id(){
        return reception_route_id ;
    }

    /**
     * 设置 [RECEPTION_ROUTE_ID]
     */
    @JsonProperty("reception_route_id")
    public void setReception_route_id(Integer  reception_route_id){
        this.reception_route_id = reception_route_id ;
        this.reception_route_idDirtyFlag = true ;
    }

    /**
     * 获取 [RECEPTION_ROUTE_ID]脏标记
     */
    @JsonIgnore
    public boolean getReception_route_idDirtyFlag(){
        return reception_route_idDirtyFlag ;
    }

    /**
     * 获取 [PACK_TYPE_ID]
     */
    @JsonProperty("pack_type_id")
    public Integer getPack_type_id(){
        return pack_type_id ;
    }

    /**
     * 设置 [PACK_TYPE_ID]
     */
    @JsonProperty("pack_type_id")
    public void setPack_type_id(Integer  pack_type_id){
        this.pack_type_id = pack_type_id ;
        this.pack_type_idDirtyFlag = true ;
    }

    /**
     * 获取 [PACK_TYPE_ID]脏标记
     */
    @JsonIgnore
    public boolean getPack_type_idDirtyFlag(){
        return pack_type_idDirtyFlag ;
    }

    /**
     * 获取 [PBM_TYPE_ID]
     */
    @JsonProperty("pbm_type_id")
    public Integer getPbm_type_id(){
        return pbm_type_id ;
    }

    /**
     * 设置 [PBM_TYPE_ID]
     */
    @JsonProperty("pbm_type_id")
    public void setPbm_type_id(Integer  pbm_type_id){
        this.pbm_type_id = pbm_type_id ;
        this.pbm_type_idDirtyFlag = true ;
    }

    /**
     * 获取 [PBM_TYPE_ID]脏标记
     */
    @JsonIgnore
    public boolean getPbm_type_idDirtyFlag(){
        return pbm_type_idDirtyFlag ;
    }

    /**
     * 获取 [DELIVERY_ROUTE_ID]
     */
    @JsonProperty("delivery_route_id")
    public Integer getDelivery_route_id(){
        return delivery_route_id ;
    }

    /**
     * 设置 [DELIVERY_ROUTE_ID]
     */
    @JsonProperty("delivery_route_id")
    public void setDelivery_route_id(Integer  delivery_route_id){
        this.delivery_route_id = delivery_route_id ;
        this.delivery_route_idDirtyFlag = true ;
    }

    /**
     * 获取 [DELIVERY_ROUTE_ID]脏标记
     */
    @JsonIgnore
    public boolean getDelivery_route_idDirtyFlag(){
        return delivery_route_idDirtyFlag ;
    }

    /**
     * 获取 [WH_INPUT_STOCK_LOC_ID]
     */
    @JsonProperty("wh_input_stock_loc_id")
    public Integer getWh_input_stock_loc_id(){
        return wh_input_stock_loc_id ;
    }

    /**
     * 设置 [WH_INPUT_STOCK_LOC_ID]
     */
    @JsonProperty("wh_input_stock_loc_id")
    public void setWh_input_stock_loc_id(Integer  wh_input_stock_loc_id){
        this.wh_input_stock_loc_id = wh_input_stock_loc_id ;
        this.wh_input_stock_loc_idDirtyFlag = true ;
    }

    /**
     * 获取 [WH_INPUT_STOCK_LOC_ID]脏标记
     */
    @JsonIgnore
    public boolean getWh_input_stock_loc_idDirtyFlag(){
        return wh_input_stock_loc_idDirtyFlag ;
    }

    /**
     * 获取 [LOT_STOCK_ID]
     */
    @JsonProperty("lot_stock_id")
    public Integer getLot_stock_id(){
        return lot_stock_id ;
    }

    /**
     * 设置 [LOT_STOCK_ID]
     */
    @JsonProperty("lot_stock_id")
    public void setLot_stock_id(Integer  lot_stock_id){
        this.lot_stock_id = lot_stock_id ;
        this.lot_stock_idDirtyFlag = true ;
    }

    /**
     * 获取 [LOT_STOCK_ID]脏标记
     */
    @JsonIgnore
    public boolean getLot_stock_idDirtyFlag(){
        return lot_stock_idDirtyFlag ;
    }

    /**
     * 获取 [SAM_RULE_ID]
     */
    @JsonProperty("sam_rule_id")
    public Integer getSam_rule_id(){
        return sam_rule_id ;
    }

    /**
     * 设置 [SAM_RULE_ID]
     */
    @JsonProperty("sam_rule_id")
    public void setSam_rule_id(Integer  sam_rule_id){
        this.sam_rule_id = sam_rule_id ;
        this.sam_rule_idDirtyFlag = true ;
    }

    /**
     * 获取 [SAM_RULE_ID]脏标记
     */
    @JsonIgnore
    public boolean getSam_rule_idDirtyFlag(){
        return sam_rule_idDirtyFlag ;
    }

    /**
     * 获取 [CROSSDOCK_ROUTE_ID]
     */
    @JsonProperty("crossdock_route_id")
    public Integer getCrossdock_route_id(){
        return crossdock_route_id ;
    }

    /**
     * 设置 [CROSSDOCK_ROUTE_ID]
     */
    @JsonProperty("crossdock_route_id")
    public void setCrossdock_route_id(Integer  crossdock_route_id){
        this.crossdock_route_id = crossdock_route_id ;
        this.crossdock_route_idDirtyFlag = true ;
    }

    /**
     * 获取 [CROSSDOCK_ROUTE_ID]脏标记
     */
    @JsonIgnore
    public boolean getCrossdock_route_idDirtyFlag(){
        return crossdock_route_idDirtyFlag ;
    }

    /**
     * 获取 [WH_PACK_STOCK_LOC_ID]
     */
    @JsonProperty("wh_pack_stock_loc_id")
    public Integer getWh_pack_stock_loc_id(){
        return wh_pack_stock_loc_id ;
    }

    /**
     * 设置 [WH_PACK_STOCK_LOC_ID]
     */
    @JsonProperty("wh_pack_stock_loc_id")
    public void setWh_pack_stock_loc_id(Integer  wh_pack_stock_loc_id){
        this.wh_pack_stock_loc_id = wh_pack_stock_loc_id ;
        this.wh_pack_stock_loc_idDirtyFlag = true ;
    }

    /**
     * 获取 [WH_PACK_STOCK_LOC_ID]脏标记
     */
    @JsonIgnore
    public boolean getWh_pack_stock_loc_idDirtyFlag(){
        return wh_pack_stock_loc_idDirtyFlag ;
    }



    public Stock_warehouse toDO() {
        Stock_warehouse srfdomain = new Stock_warehouse();
        if(getResupply_route_idsDirtyFlag())
            srfdomain.setResupply_route_ids(resupply_route_ids);
        if(getActiveDirtyFlag())
            srfdomain.setActive(active);
        if(getResupply_wh_idsDirtyFlag())
            srfdomain.setResupply_wh_ids(resupply_wh_ids);
        if(getWrite_dateDirtyFlag())
            srfdomain.setWrite_date(write_date);
        if(get__last_updateDirtyFlag())
            srfdomain.set__last_update(__last_update);
        if(getNameDirtyFlag())
            srfdomain.setName(name);
        if(getDelivery_stepsDirtyFlag())
            srfdomain.setDelivery_steps(delivery_steps);
        if(getWarehouse_countDirtyFlag())
            srfdomain.setWarehouse_count(warehouse_count);
        if(getManufacture_to_resupplyDirtyFlag())
            srfdomain.setManufacture_to_resupply(manufacture_to_resupply);
        if(getCreate_dateDirtyFlag())
            srfdomain.setCreate_date(create_date);
        if(getRoute_idsDirtyFlag())
            srfdomain.setRoute_ids(route_ids);
        if(getDisplay_nameDirtyFlag())
            srfdomain.setDisplay_name(display_name);
        if(getReception_stepsDirtyFlag())
            srfdomain.setReception_steps(reception_steps);
        if(getBuy_to_resupplyDirtyFlag())
            srfdomain.setBuy_to_resupply(buy_to_resupply);
        if(getIdDirtyFlag())
            srfdomain.setId(id);
        if(getCodeDirtyFlag())
            srfdomain.setCode(code);
        if(getManufacture_stepsDirtyFlag())
            srfdomain.setManufacture_steps(manufacture_steps);
        if(getShow_resupplyDirtyFlag())
            srfdomain.setShow_resupply(show_resupply);
        if(getView_location_id_textDirtyFlag())
            srfdomain.setView_location_id_text(view_location_id_text);
        if(getWh_input_stock_loc_id_textDirtyFlag())
            srfdomain.setWh_input_stock_loc_id_text(wh_input_stock_loc_id_text);
        if(getSam_loc_id_textDirtyFlag())
            srfdomain.setSam_loc_id_text(sam_loc_id_text);
        if(getCrossdock_route_id_textDirtyFlag())
            srfdomain.setCrossdock_route_id_text(crossdock_route_id_text);
        if(getPbm_route_id_textDirtyFlag())
            srfdomain.setPbm_route_id_text(pbm_route_id_text);
        if(getWrite_uid_textDirtyFlag())
            srfdomain.setWrite_uid_text(write_uid_text);
        if(getPbm_type_id_textDirtyFlag())
            srfdomain.setPbm_type_id_text(pbm_type_id_text);
        if(getPick_type_id_textDirtyFlag())
            srfdomain.setPick_type_id_text(pick_type_id_text);
        if(getInt_type_id_textDirtyFlag())
            srfdomain.setInt_type_id_text(int_type_id_text);
        if(getManu_type_id_textDirtyFlag())
            srfdomain.setManu_type_id_text(manu_type_id_text);
        if(getBuy_pull_id_textDirtyFlag())
            srfdomain.setBuy_pull_id_text(buy_pull_id_text);
        if(getWh_pack_stock_loc_id_textDirtyFlag())
            srfdomain.setWh_pack_stock_loc_id_text(wh_pack_stock_loc_id_text);
        if(getManufacture_pull_id_textDirtyFlag())
            srfdomain.setManufacture_pull_id_text(manufacture_pull_id_text);
        if(getReception_route_id_textDirtyFlag())
            srfdomain.setReception_route_id_text(reception_route_id_text);
        if(getDelivery_route_id_textDirtyFlag())
            srfdomain.setDelivery_route_id_text(delivery_route_id_text);
        if(getSam_type_id_textDirtyFlag())
            srfdomain.setSam_type_id_text(sam_type_id_text);
        if(getPartner_id_textDirtyFlag())
            srfdomain.setPartner_id_text(partner_id_text);
        if(getOut_type_id_textDirtyFlag())
            srfdomain.setOut_type_id_text(out_type_id_text);
        if(getMto_pull_id_textDirtyFlag())
            srfdomain.setMto_pull_id_text(mto_pull_id_text);
        if(getCreate_uid_textDirtyFlag())
            srfdomain.setCreate_uid_text(create_uid_text);
        if(getLot_stock_id_textDirtyFlag())
            srfdomain.setLot_stock_id_text(lot_stock_id_text);
        if(getPbm_mto_pull_id_textDirtyFlag())
            srfdomain.setPbm_mto_pull_id_text(pbm_mto_pull_id_text);
        if(getCompany_id_textDirtyFlag())
            srfdomain.setCompany_id_text(company_id_text);
        if(getPbm_loc_id_textDirtyFlag())
            srfdomain.setPbm_loc_id_text(pbm_loc_id_text);
        if(getPack_type_id_textDirtyFlag())
            srfdomain.setPack_type_id_text(pack_type_id_text);
        if(getSam_rule_id_textDirtyFlag())
            srfdomain.setSam_rule_id_text(sam_rule_id_text);
        if(getIn_type_id_textDirtyFlag())
            srfdomain.setIn_type_id_text(in_type_id_text);
        if(getWh_output_stock_loc_id_textDirtyFlag())
            srfdomain.setWh_output_stock_loc_id_text(wh_output_stock_loc_id_text);
        if(getWh_qc_stock_loc_id_textDirtyFlag())
            srfdomain.setWh_qc_stock_loc_id_text(wh_qc_stock_loc_id_text);
        if(getWh_output_stock_loc_idDirtyFlag())
            srfdomain.setWh_output_stock_loc_id(wh_output_stock_loc_id);
        if(getWrite_uidDirtyFlag())
            srfdomain.setWrite_uid(write_uid);
        if(getPbm_loc_idDirtyFlag())
            srfdomain.setPbm_loc_id(pbm_loc_id);
        if(getView_location_idDirtyFlag())
            srfdomain.setView_location_id(view_location_id);
        if(getOut_type_idDirtyFlag())
            srfdomain.setOut_type_id(out_type_id);
        if(getManu_type_idDirtyFlag())
            srfdomain.setManu_type_id(manu_type_id);
        if(getPbm_mto_pull_idDirtyFlag())
            srfdomain.setPbm_mto_pull_id(pbm_mto_pull_id);
        if(getMto_pull_idDirtyFlag())
            srfdomain.setMto_pull_id(mto_pull_id);
        if(getSam_type_idDirtyFlag())
            srfdomain.setSam_type_id(sam_type_id);
        if(getManufacture_pull_idDirtyFlag())
            srfdomain.setManufacture_pull_id(manufacture_pull_id);
        if(getSam_loc_idDirtyFlag())
            srfdomain.setSam_loc_id(sam_loc_id);
        if(getBuy_pull_idDirtyFlag())
            srfdomain.setBuy_pull_id(buy_pull_id);
        if(getInt_type_idDirtyFlag())
            srfdomain.setInt_type_id(int_type_id);
        if(getWh_qc_stock_loc_idDirtyFlag())
            srfdomain.setWh_qc_stock_loc_id(wh_qc_stock_loc_id);
        if(getIn_type_idDirtyFlag())
            srfdomain.setIn_type_id(in_type_id);
        if(getPbm_route_idDirtyFlag())
            srfdomain.setPbm_route_id(pbm_route_id);
        if(getPick_type_idDirtyFlag())
            srfdomain.setPick_type_id(pick_type_id);
        if(getCreate_uidDirtyFlag())
            srfdomain.setCreate_uid(create_uid);
        if(getPartner_idDirtyFlag())
            srfdomain.setPartner_id(partner_id);
        if(getCompany_idDirtyFlag())
            srfdomain.setCompany_id(company_id);
        if(getReception_route_idDirtyFlag())
            srfdomain.setReception_route_id(reception_route_id);
        if(getPack_type_idDirtyFlag())
            srfdomain.setPack_type_id(pack_type_id);
        if(getPbm_type_idDirtyFlag())
            srfdomain.setPbm_type_id(pbm_type_id);
        if(getDelivery_route_idDirtyFlag())
            srfdomain.setDelivery_route_id(delivery_route_id);
        if(getWh_input_stock_loc_idDirtyFlag())
            srfdomain.setWh_input_stock_loc_id(wh_input_stock_loc_id);
        if(getLot_stock_idDirtyFlag())
            srfdomain.setLot_stock_id(lot_stock_id);
        if(getSam_rule_idDirtyFlag())
            srfdomain.setSam_rule_id(sam_rule_id);
        if(getCrossdock_route_idDirtyFlag())
            srfdomain.setCrossdock_route_id(crossdock_route_id);
        if(getWh_pack_stock_loc_idDirtyFlag())
            srfdomain.setWh_pack_stock_loc_id(wh_pack_stock_loc_id);

        return srfdomain;
    }

    public void fromDO(Stock_warehouse srfdomain) {
        if(srfdomain == null )
          return ;
        if(srfdomain.getResupply_route_idsDirtyFlag())
            this.setResupply_route_ids(srfdomain.getResupply_route_ids());
        if(srfdomain.getActiveDirtyFlag())
            this.setActive(srfdomain.getActive());
        if(srfdomain.getResupply_wh_idsDirtyFlag())
            this.setResupply_wh_ids(srfdomain.getResupply_wh_ids());
        if(srfdomain.getWrite_dateDirtyFlag())
            this.setWrite_date(srfdomain.getWrite_date());
        if(srfdomain.get__last_updateDirtyFlag())
            this.set__last_update(srfdomain.get__last_update());
        if(srfdomain.getNameDirtyFlag())
            this.setName(srfdomain.getName());
        if(srfdomain.getDelivery_stepsDirtyFlag())
            this.setDelivery_steps(srfdomain.getDelivery_steps());
        if(srfdomain.getWarehouse_countDirtyFlag())
            this.setWarehouse_count(srfdomain.getWarehouse_count());
        if(srfdomain.getManufacture_to_resupplyDirtyFlag())
            this.setManufacture_to_resupply(srfdomain.getManufacture_to_resupply());
        if(srfdomain.getCreate_dateDirtyFlag())
            this.setCreate_date(srfdomain.getCreate_date());
        if(srfdomain.getRoute_idsDirtyFlag())
            this.setRoute_ids(srfdomain.getRoute_ids());
        if(srfdomain.getDisplay_nameDirtyFlag())
            this.setDisplay_name(srfdomain.getDisplay_name());
        if(srfdomain.getReception_stepsDirtyFlag())
            this.setReception_steps(srfdomain.getReception_steps());
        if(srfdomain.getBuy_to_resupplyDirtyFlag())
            this.setBuy_to_resupply(srfdomain.getBuy_to_resupply());
        if(srfdomain.getIdDirtyFlag())
            this.setId(srfdomain.getId());
        if(srfdomain.getCodeDirtyFlag())
            this.setCode(srfdomain.getCode());
        if(srfdomain.getManufacture_stepsDirtyFlag())
            this.setManufacture_steps(srfdomain.getManufacture_steps());
        if(srfdomain.getShow_resupplyDirtyFlag())
            this.setShow_resupply(srfdomain.getShow_resupply());
        if(srfdomain.getView_location_id_textDirtyFlag())
            this.setView_location_id_text(srfdomain.getView_location_id_text());
        if(srfdomain.getWh_input_stock_loc_id_textDirtyFlag())
            this.setWh_input_stock_loc_id_text(srfdomain.getWh_input_stock_loc_id_text());
        if(srfdomain.getSam_loc_id_textDirtyFlag())
            this.setSam_loc_id_text(srfdomain.getSam_loc_id_text());
        if(srfdomain.getCrossdock_route_id_textDirtyFlag())
            this.setCrossdock_route_id_text(srfdomain.getCrossdock_route_id_text());
        if(srfdomain.getPbm_route_id_textDirtyFlag())
            this.setPbm_route_id_text(srfdomain.getPbm_route_id_text());
        if(srfdomain.getWrite_uid_textDirtyFlag())
            this.setWrite_uid_text(srfdomain.getWrite_uid_text());
        if(srfdomain.getPbm_type_id_textDirtyFlag())
            this.setPbm_type_id_text(srfdomain.getPbm_type_id_text());
        if(srfdomain.getPick_type_id_textDirtyFlag())
            this.setPick_type_id_text(srfdomain.getPick_type_id_text());
        if(srfdomain.getInt_type_id_textDirtyFlag())
            this.setInt_type_id_text(srfdomain.getInt_type_id_text());
        if(srfdomain.getManu_type_id_textDirtyFlag())
            this.setManu_type_id_text(srfdomain.getManu_type_id_text());
        if(srfdomain.getBuy_pull_id_textDirtyFlag())
            this.setBuy_pull_id_text(srfdomain.getBuy_pull_id_text());
        if(srfdomain.getWh_pack_stock_loc_id_textDirtyFlag())
            this.setWh_pack_stock_loc_id_text(srfdomain.getWh_pack_stock_loc_id_text());
        if(srfdomain.getManufacture_pull_id_textDirtyFlag())
            this.setManufacture_pull_id_text(srfdomain.getManufacture_pull_id_text());
        if(srfdomain.getReception_route_id_textDirtyFlag())
            this.setReception_route_id_text(srfdomain.getReception_route_id_text());
        if(srfdomain.getDelivery_route_id_textDirtyFlag())
            this.setDelivery_route_id_text(srfdomain.getDelivery_route_id_text());
        if(srfdomain.getSam_type_id_textDirtyFlag())
            this.setSam_type_id_text(srfdomain.getSam_type_id_text());
        if(srfdomain.getPartner_id_textDirtyFlag())
            this.setPartner_id_text(srfdomain.getPartner_id_text());
        if(srfdomain.getOut_type_id_textDirtyFlag())
            this.setOut_type_id_text(srfdomain.getOut_type_id_text());
        if(srfdomain.getMto_pull_id_textDirtyFlag())
            this.setMto_pull_id_text(srfdomain.getMto_pull_id_text());
        if(srfdomain.getCreate_uid_textDirtyFlag())
            this.setCreate_uid_text(srfdomain.getCreate_uid_text());
        if(srfdomain.getLot_stock_id_textDirtyFlag())
            this.setLot_stock_id_text(srfdomain.getLot_stock_id_text());
        if(srfdomain.getPbm_mto_pull_id_textDirtyFlag())
            this.setPbm_mto_pull_id_text(srfdomain.getPbm_mto_pull_id_text());
        if(srfdomain.getCompany_id_textDirtyFlag())
            this.setCompany_id_text(srfdomain.getCompany_id_text());
        if(srfdomain.getPbm_loc_id_textDirtyFlag())
            this.setPbm_loc_id_text(srfdomain.getPbm_loc_id_text());
        if(srfdomain.getPack_type_id_textDirtyFlag())
            this.setPack_type_id_text(srfdomain.getPack_type_id_text());
        if(srfdomain.getSam_rule_id_textDirtyFlag())
            this.setSam_rule_id_text(srfdomain.getSam_rule_id_text());
        if(srfdomain.getIn_type_id_textDirtyFlag())
            this.setIn_type_id_text(srfdomain.getIn_type_id_text());
        if(srfdomain.getWh_output_stock_loc_id_textDirtyFlag())
            this.setWh_output_stock_loc_id_text(srfdomain.getWh_output_stock_loc_id_text());
        if(srfdomain.getWh_qc_stock_loc_id_textDirtyFlag())
            this.setWh_qc_stock_loc_id_text(srfdomain.getWh_qc_stock_loc_id_text());
        if(srfdomain.getWh_output_stock_loc_idDirtyFlag())
            this.setWh_output_stock_loc_id(srfdomain.getWh_output_stock_loc_id());
        if(srfdomain.getWrite_uidDirtyFlag())
            this.setWrite_uid(srfdomain.getWrite_uid());
        if(srfdomain.getPbm_loc_idDirtyFlag())
            this.setPbm_loc_id(srfdomain.getPbm_loc_id());
        if(srfdomain.getView_location_idDirtyFlag())
            this.setView_location_id(srfdomain.getView_location_id());
        if(srfdomain.getOut_type_idDirtyFlag())
            this.setOut_type_id(srfdomain.getOut_type_id());
        if(srfdomain.getManu_type_idDirtyFlag())
            this.setManu_type_id(srfdomain.getManu_type_id());
        if(srfdomain.getPbm_mto_pull_idDirtyFlag())
            this.setPbm_mto_pull_id(srfdomain.getPbm_mto_pull_id());
        if(srfdomain.getMto_pull_idDirtyFlag())
            this.setMto_pull_id(srfdomain.getMto_pull_id());
        if(srfdomain.getSam_type_idDirtyFlag())
            this.setSam_type_id(srfdomain.getSam_type_id());
        if(srfdomain.getManufacture_pull_idDirtyFlag())
            this.setManufacture_pull_id(srfdomain.getManufacture_pull_id());
        if(srfdomain.getSam_loc_idDirtyFlag())
            this.setSam_loc_id(srfdomain.getSam_loc_id());
        if(srfdomain.getBuy_pull_idDirtyFlag())
            this.setBuy_pull_id(srfdomain.getBuy_pull_id());
        if(srfdomain.getInt_type_idDirtyFlag())
            this.setInt_type_id(srfdomain.getInt_type_id());
        if(srfdomain.getWh_qc_stock_loc_idDirtyFlag())
            this.setWh_qc_stock_loc_id(srfdomain.getWh_qc_stock_loc_id());
        if(srfdomain.getIn_type_idDirtyFlag())
            this.setIn_type_id(srfdomain.getIn_type_id());
        if(srfdomain.getPbm_route_idDirtyFlag())
            this.setPbm_route_id(srfdomain.getPbm_route_id());
        if(srfdomain.getPick_type_idDirtyFlag())
            this.setPick_type_id(srfdomain.getPick_type_id());
        if(srfdomain.getCreate_uidDirtyFlag())
            this.setCreate_uid(srfdomain.getCreate_uid());
        if(srfdomain.getPartner_idDirtyFlag())
            this.setPartner_id(srfdomain.getPartner_id());
        if(srfdomain.getCompany_idDirtyFlag())
            this.setCompany_id(srfdomain.getCompany_id());
        if(srfdomain.getReception_route_idDirtyFlag())
            this.setReception_route_id(srfdomain.getReception_route_id());
        if(srfdomain.getPack_type_idDirtyFlag())
            this.setPack_type_id(srfdomain.getPack_type_id());
        if(srfdomain.getPbm_type_idDirtyFlag())
            this.setPbm_type_id(srfdomain.getPbm_type_id());
        if(srfdomain.getDelivery_route_idDirtyFlag())
            this.setDelivery_route_id(srfdomain.getDelivery_route_id());
        if(srfdomain.getWh_input_stock_loc_idDirtyFlag())
            this.setWh_input_stock_loc_id(srfdomain.getWh_input_stock_loc_id());
        if(srfdomain.getLot_stock_idDirtyFlag())
            this.setLot_stock_id(srfdomain.getLot_stock_id());
        if(srfdomain.getSam_rule_idDirtyFlag())
            this.setSam_rule_id(srfdomain.getSam_rule_id());
        if(srfdomain.getCrossdock_route_idDirtyFlag())
            this.setCrossdock_route_id(srfdomain.getCrossdock_route_id());
        if(srfdomain.getWh_pack_stock_loc_idDirtyFlag())
            this.setWh_pack_stock_loc_id(srfdomain.getWh_pack_stock_loc_id());

    }

    public List<Stock_warehouseDTO> fromDOPage(List<Stock_warehouse> poPage)   {
        if(poPage == null)
            return null;
        List<Stock_warehouseDTO> dtos=new ArrayList<Stock_warehouseDTO>();
        for(Stock_warehouse domain : poPage) {
            Stock_warehouseDTO dto = new Stock_warehouseDTO();
            dto.fromDO(domain);
            dtos.add(dto);
        }
        return dtos;
    }
}

