package cn.ibizlab.odoo.service.odoo_stock.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_stock.dto.Stock_warn_insufficient_qtyDTO;
import cn.ibizlab.odoo.core.odoo_stock.domain.Stock_warn_insufficient_qty;
import cn.ibizlab.odoo.core.odoo_stock.service.IStock_warn_insufficient_qtyService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_stock.filter.Stock_warn_insufficient_qtySearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Stock_warn_insufficient_qty" })
@RestController
@RequestMapping("")
public class Stock_warn_insufficient_qtyResource {

    @Autowired
    private IStock_warn_insufficient_qtyService stock_warn_insufficient_qtyService;

    public IStock_warn_insufficient_qtyService getStock_warn_insufficient_qtyService() {
        return this.stock_warn_insufficient_qtyService;
    }

    @ApiOperation(value = "更新数据", tags = {"Stock_warn_insufficient_qty" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_stock/stock_warn_insufficient_qties/{stock_warn_insufficient_qty_id}")

    public ResponseEntity<Stock_warn_insufficient_qtyDTO> update(@PathVariable("stock_warn_insufficient_qty_id") Integer stock_warn_insufficient_qty_id, @RequestBody Stock_warn_insufficient_qtyDTO stock_warn_insufficient_qtydto) {
		Stock_warn_insufficient_qty domain = stock_warn_insufficient_qtydto.toDO();
        domain.setId(stock_warn_insufficient_qty_id);
		stock_warn_insufficient_qtyService.update(domain);
		Stock_warn_insufficient_qtyDTO dto = new Stock_warn_insufficient_qtyDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取数据", tags = {"Stock_warn_insufficient_qty" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_stock/stock_warn_insufficient_qties/{stock_warn_insufficient_qty_id}")
    public ResponseEntity<Stock_warn_insufficient_qtyDTO> get(@PathVariable("stock_warn_insufficient_qty_id") Integer stock_warn_insufficient_qty_id) {
        Stock_warn_insufficient_qtyDTO dto = new Stock_warn_insufficient_qtyDTO();
        Stock_warn_insufficient_qty domain = stock_warn_insufficient_qtyService.get(stock_warn_insufficient_qty_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "删除数据", tags = {"Stock_warn_insufficient_qty" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_stock/stock_warn_insufficient_qties/{stock_warn_insufficient_qty_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("stock_warn_insufficient_qty_id") Integer stock_warn_insufficient_qty_id) {
        Stock_warn_insufficient_qtyDTO stock_warn_insufficient_qtydto = new Stock_warn_insufficient_qtyDTO();
		Stock_warn_insufficient_qty domain = new Stock_warn_insufficient_qty();
		stock_warn_insufficient_qtydto.setId(stock_warn_insufficient_qty_id);
		domain.setId(stock_warn_insufficient_qty_id);
        Boolean rst = stock_warn_insufficient_qtyService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "批建立数据", tags = {"Stock_warn_insufficient_qty" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_stock/stock_warn_insufficient_qties/createBatch")
    public ResponseEntity<Boolean> createBatchStock_warn_insufficient_qty(@RequestBody List<Stock_warn_insufficient_qtyDTO> stock_warn_insufficient_qtydtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "批更新数据", tags = {"Stock_warn_insufficient_qty" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_stock/stock_warn_insufficient_qties/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Stock_warn_insufficient_qtyDTO> stock_warn_insufficient_qtydtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "批删除数据", tags = {"Stock_warn_insufficient_qty" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_stock/stock_warn_insufficient_qties/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Stock_warn_insufficient_qtyDTO> stock_warn_insufficient_qtydtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "建立数据", tags = {"Stock_warn_insufficient_qty" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_stock/stock_warn_insufficient_qties")

    public ResponseEntity<Stock_warn_insufficient_qtyDTO> create(@RequestBody Stock_warn_insufficient_qtyDTO stock_warn_insufficient_qtydto) {
        Stock_warn_insufficient_qtyDTO dto = new Stock_warn_insufficient_qtyDTO();
        Stock_warn_insufficient_qty domain = stock_warn_insufficient_qtydto.toDO();
		stock_warn_insufficient_qtyService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Stock_warn_insufficient_qty" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_stock/stock_warn_insufficient_qties/fetchdefault")
	public ResponseEntity<Page<Stock_warn_insufficient_qtyDTO>> fetchDefault(Stock_warn_insufficient_qtySearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Stock_warn_insufficient_qtyDTO> list = new ArrayList<Stock_warn_insufficient_qtyDTO>();
        
        Page<Stock_warn_insufficient_qty> domains = stock_warn_insufficient_qtyService.searchDefault(context) ;
        for(Stock_warn_insufficient_qty stock_warn_insufficient_qty : domains.getContent()){
            Stock_warn_insufficient_qtyDTO dto = new Stock_warn_insufficient_qtyDTO();
            dto.fromDO(stock_warn_insufficient_qty);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
