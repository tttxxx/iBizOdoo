package cn.ibizlab.odoo.service.odoo_stock.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_stock.dto.Stock_quant_packageDTO;
import cn.ibizlab.odoo.core.odoo_stock.domain.Stock_quant_package;
import cn.ibizlab.odoo.core.odoo_stock.service.IStock_quant_packageService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_stock.filter.Stock_quant_packageSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Stock_quant_package" })
@RestController
@RequestMapping("")
public class Stock_quant_packageResource {

    @Autowired
    private IStock_quant_packageService stock_quant_packageService;

    public IStock_quant_packageService getStock_quant_packageService() {
        return this.stock_quant_packageService;
    }

    @ApiOperation(value = "更新数据", tags = {"Stock_quant_package" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_stock/stock_quant_packages/{stock_quant_package_id}")

    public ResponseEntity<Stock_quant_packageDTO> update(@PathVariable("stock_quant_package_id") Integer stock_quant_package_id, @RequestBody Stock_quant_packageDTO stock_quant_packagedto) {
		Stock_quant_package domain = stock_quant_packagedto.toDO();
        domain.setId(stock_quant_package_id);
		stock_quant_packageService.update(domain);
		Stock_quant_packageDTO dto = new Stock_quant_packageDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "删除数据", tags = {"Stock_quant_package" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_stock/stock_quant_packages/{stock_quant_package_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("stock_quant_package_id") Integer stock_quant_package_id) {
        Stock_quant_packageDTO stock_quant_packagedto = new Stock_quant_packageDTO();
		Stock_quant_package domain = new Stock_quant_package();
		stock_quant_packagedto.setId(stock_quant_package_id);
		domain.setId(stock_quant_package_id);
        Boolean rst = stock_quant_packageService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "批删除数据", tags = {"Stock_quant_package" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_stock/stock_quant_packages/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Stock_quant_packageDTO> stock_quant_packagedtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "建立数据", tags = {"Stock_quant_package" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_stock/stock_quant_packages")

    public ResponseEntity<Stock_quant_packageDTO> create(@RequestBody Stock_quant_packageDTO stock_quant_packagedto) {
        Stock_quant_packageDTO dto = new Stock_quant_packageDTO();
        Stock_quant_package domain = stock_quant_packagedto.toDO();
		stock_quant_packageService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批更新数据", tags = {"Stock_quant_package" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_stock/stock_quant_packages/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Stock_quant_packageDTO> stock_quant_packagedtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "获取数据", tags = {"Stock_quant_package" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_stock/stock_quant_packages/{stock_quant_package_id}")
    public ResponseEntity<Stock_quant_packageDTO> get(@PathVariable("stock_quant_package_id") Integer stock_quant_package_id) {
        Stock_quant_packageDTO dto = new Stock_quant_packageDTO();
        Stock_quant_package domain = stock_quant_packageService.get(stock_quant_package_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批建立数据", tags = {"Stock_quant_package" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_stock/stock_quant_packages/createBatch")
    public ResponseEntity<Boolean> createBatchStock_quant_package(@RequestBody List<Stock_quant_packageDTO> stock_quant_packagedtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Stock_quant_package" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_stock/stock_quant_packages/fetchdefault")
	public ResponseEntity<Page<Stock_quant_packageDTO>> fetchDefault(Stock_quant_packageSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Stock_quant_packageDTO> list = new ArrayList<Stock_quant_packageDTO>();
        
        Page<Stock_quant_package> domains = stock_quant_packageService.searchDefault(context) ;
        for(Stock_quant_package stock_quant_package : domains.getContent()){
            Stock_quant_packageDTO dto = new Stock_quant_packageDTO();
            dto.fromDO(stock_quant_package);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
