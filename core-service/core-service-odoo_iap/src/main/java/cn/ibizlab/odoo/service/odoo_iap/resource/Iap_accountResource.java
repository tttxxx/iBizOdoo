package cn.ibizlab.odoo.service.odoo_iap.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_iap.dto.Iap_accountDTO;
import cn.ibizlab.odoo.core.odoo_iap.domain.Iap_account;
import cn.ibizlab.odoo.core.odoo_iap.service.IIap_accountService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_iap.filter.Iap_accountSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Iap_account" })
@RestController
@RequestMapping("")
public class Iap_accountResource {

    @Autowired
    private IIap_accountService iap_accountService;

    public IIap_accountService getIap_accountService() {
        return this.iap_accountService;
    }

    @ApiOperation(value = "删除数据", tags = {"Iap_account" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_iap/iap_accounts/{iap_account_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("iap_account_id") Integer iap_account_id) {
        Iap_accountDTO iap_accountdto = new Iap_accountDTO();
		Iap_account domain = new Iap_account();
		iap_accountdto.setId(iap_account_id);
		domain.setId(iap_account_id);
        Boolean rst = iap_accountService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "获取数据", tags = {"Iap_account" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_iap/iap_accounts/{iap_account_id}")
    public ResponseEntity<Iap_accountDTO> get(@PathVariable("iap_account_id") Integer iap_account_id) {
        Iap_accountDTO dto = new Iap_accountDTO();
        Iap_account domain = iap_accountService.get(iap_account_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批删除数据", tags = {"Iap_account" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_iap/iap_accounts/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Iap_accountDTO> iap_accountdtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "批建立数据", tags = {"Iap_account" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_iap/iap_accounts/createBatch")
    public ResponseEntity<Boolean> createBatchIap_account(@RequestBody List<Iap_accountDTO> iap_accountdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "更新数据", tags = {"Iap_account" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_iap/iap_accounts/{iap_account_id}")

    public ResponseEntity<Iap_accountDTO> update(@PathVariable("iap_account_id") Integer iap_account_id, @RequestBody Iap_accountDTO iap_accountdto) {
		Iap_account domain = iap_accountdto.toDO();
        domain.setId(iap_account_id);
		iap_accountService.update(domain);
		Iap_accountDTO dto = new Iap_accountDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批更新数据", tags = {"Iap_account" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_iap/iap_accounts/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Iap_accountDTO> iap_accountdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "建立数据", tags = {"Iap_account" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_iap/iap_accounts")

    public ResponseEntity<Iap_accountDTO> create(@RequestBody Iap_accountDTO iap_accountdto) {
        Iap_accountDTO dto = new Iap_accountDTO();
        Iap_account domain = iap_accountdto.toDO();
		iap_accountService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Iap_account" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_iap/iap_accounts/fetchdefault")
	public ResponseEntity<Page<Iap_accountDTO>> fetchDefault(Iap_accountSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Iap_accountDTO> list = new ArrayList<Iap_accountDTO>();
        
        Page<Iap_account> domains = iap_accountService.searchDefault(context) ;
        for(Iap_account iap_account : domains.getContent()){
            Iap_accountDTO dto = new Iap_accountDTO();
            dto.fromDO(iap_account);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
