package cn.ibizlab.odoo.service.odoo_project.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import lombok.Data;

@ConfigurationProperties(prefix = "service.odoo.project")
@Data
public class odoo_projectServiceProperties {

	private boolean enabled;

	private boolean auth;


}