package cn.ibizlab.odoo.service.odoo_project.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.core.odoo_project.valuerule.anno.project_task.*;
import cn.ibizlab.odoo.core.odoo_project.domain.Project_task;
import org.springframework.cglib.beans.BeanCopier;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 服务DTO对象[Project_taskDTO]
 */
public class Project_taskDTO implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [DATE_START]
     *
     */
    @Project_taskDate_startDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp date_start;

    @JsonIgnore
    private boolean date_startDirtyFlag;

    /**
     * 属性 [ACTIVITY_STATE]
     *
     */
    @Project_taskActivity_stateDefault(info = "默认规则")
    private String activity_state;

    @JsonIgnore
    private boolean activity_stateDirtyFlag;

    /**
     * 属性 [RATING_COUNT]
     *
     */
    @Project_taskRating_countDefault(info = "默认规则")
    private Integer rating_count;

    @JsonIgnore
    private boolean rating_countDirtyFlag;

    /**
     * 属性 [EMAIL_FROM]
     *
     */
    @Project_taskEmail_fromDefault(info = "默认规则")
    private String email_from;

    @JsonIgnore
    private boolean email_fromDirtyFlag;

    /**
     * 属性 [ATTACHMENT_IDS]
     *
     */
    @Project_taskAttachment_idsDefault(info = "默认规则")
    private String attachment_ids;

    @JsonIgnore
    private boolean attachment_idsDirtyFlag;

    /**
     * 属性 [DATE_DEADLINE]
     *
     */
    @Project_taskDate_deadlineDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    private Timestamp date_deadline;

    @JsonIgnore
    private boolean date_deadlineDirtyFlag;

    /**
     * 属性 [KANBAN_STATE_LABEL]
     *
     */
    @Project_taskKanban_state_labelDefault(info = "默认规则")
    private String kanban_state_label;

    @JsonIgnore
    private boolean kanban_state_labelDirtyFlag;

    /**
     * 属性 [ACCESS_WARNING]
     *
     */
    @Project_taskAccess_warningDefault(info = "默认规则")
    private String access_warning;

    @JsonIgnore
    private boolean access_warningDirtyFlag;

    /**
     * 属性 [RATING_IDS]
     *
     */
    @Project_taskRating_idsDefault(info = "默认规则")
    private String rating_ids;

    @JsonIgnore
    private boolean rating_idsDirtyFlag;

    /**
     * 属性 [RATING_LAST_VALUE]
     *
     */
    @Project_taskRating_last_valueDefault(info = "默认规则")
    private Double rating_last_value;

    @JsonIgnore
    private boolean rating_last_valueDirtyFlag;

    /**
     * 属性 [DATE_ASSIGN]
     *
     */
    @Project_taskDate_assignDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp date_assign;

    @JsonIgnore
    private boolean date_assignDirtyFlag;

    /**
     * 属性 [ID]
     *
     */
    @Project_taskIdDefault(info = "默认规则")
    private Integer id;

    @JsonIgnore
    private boolean idDirtyFlag;

    /**
     * 属性 [MESSAGE_IDS]
     *
     */
    @Project_taskMessage_idsDefault(info = "默认规则")
    private String message_ids;

    @JsonIgnore
    private boolean message_idsDirtyFlag;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @Project_task__last_updateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp __last_update;

    @JsonIgnore
    private boolean __last_updateDirtyFlag;

    /**
     * 属性 [MESSAGE_UNREAD]
     *
     */
    @Project_taskMessage_unreadDefault(info = "默认规则")
    private String message_unread;

    @JsonIgnore
    private boolean message_unreadDirtyFlag;

    /**
     * 属性 [RATING_LAST_FEEDBACK]
     *
     */
    @Project_taskRating_last_feedbackDefault(info = "默认规则")
    private String rating_last_feedback;

    @JsonIgnore
    private boolean rating_last_feedbackDirtyFlag;

    /**
     * 属性 [ACTIVITY_DATE_DEADLINE]
     *
     */
    @Project_taskActivity_date_deadlineDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    private Timestamp activity_date_deadline;

    @JsonIgnore
    private boolean activity_date_deadlineDirtyFlag;

    /**
     * 属性 [ACCESS_TOKEN]
     *
     */
    @Project_taskAccess_tokenDefault(info = "默认规则")
    private String access_token;

    @JsonIgnore
    private boolean access_tokenDirtyFlag;

    /**
     * 属性 [MESSAGE_FOLLOWER_IDS]
     *
     */
    @Project_taskMessage_follower_idsDefault(info = "默认规则")
    private String message_follower_ids;

    @JsonIgnore
    private boolean message_follower_idsDirtyFlag;

    /**
     * 属性 [SUBTASK_COUNT]
     *
     */
    @Project_taskSubtask_countDefault(info = "默认规则")
    private Integer subtask_count;

    @JsonIgnore
    private boolean subtask_countDirtyFlag;

    /**
     * 属性 [WORKING_HOURS_OPEN]
     *
     */
    @Project_taskWorking_hours_openDefault(info = "默认规则")
    private Double working_hours_open;

    @JsonIgnore
    private boolean working_hours_openDirtyFlag;

    /**
     * 属性 [WORKING_HOURS_CLOSE]
     *
     */
    @Project_taskWorking_hours_closeDefault(info = "默认规则")
    private Double working_hours_close;

    @JsonIgnore
    private boolean working_hours_closeDirtyFlag;

    /**
     * 属性 [DESCRIPTION]
     *
     */
    @Project_taskDescriptionDefault(info = "默认规则")
    private String description;

    @JsonIgnore
    private boolean descriptionDirtyFlag;

    /**
     * 属性 [RATING_LAST_IMAGE]
     *
     */
    @Project_taskRating_last_imageDefault(info = "默认规则")
    private byte[] rating_last_image;

    @JsonIgnore
    private boolean rating_last_imageDirtyFlag;

    /**
     * 属性 [WRITE_DATE]
     *
     */
    @Project_taskWrite_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp write_date;

    @JsonIgnore
    private boolean write_dateDirtyFlag;

    /**
     * 属性 [DISPLAYED_IMAGE_ID]
     *
     */
    @Project_taskDisplayed_image_idDefault(info = "默认规则")
    private Integer displayed_image_id;

    @JsonIgnore
    private boolean displayed_image_idDirtyFlag;

    /**
     * 属性 [TAG_IDS]
     *
     */
    @Project_taskTag_idsDefault(info = "默认规则")
    private String tag_ids;

    @JsonIgnore
    private boolean tag_idsDirtyFlag;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @Project_taskDisplay_nameDefault(info = "默认规则")
    private String display_name;

    @JsonIgnore
    private boolean display_nameDirtyFlag;

    /**
     * 属性 [ACTIVITY_TYPE_ID]
     *
     */
    @Project_taskActivity_type_idDefault(info = "默认规则")
    private Integer activity_type_id;

    @JsonIgnore
    private boolean activity_type_idDirtyFlag;

    /**
     * 属性 [MESSAGE_CHANNEL_IDS]
     *
     */
    @Project_taskMessage_channel_idsDefault(info = "默认规则")
    private String message_channel_ids;

    @JsonIgnore
    private boolean message_channel_idsDirtyFlag;

    /**
     * 属性 [ACTIVITY_IDS]
     *
     */
    @Project_taskActivity_idsDefault(info = "默认规则")
    private String activity_ids;

    @JsonIgnore
    private boolean activity_idsDirtyFlag;

    /**
     * 属性 [KANBAN_STATE]
     *
     */
    @Project_taskKanban_stateDefault(info = "默认规则")
    private String kanban_state;

    @JsonIgnore
    private boolean kanban_stateDirtyFlag;

    /**
     * 属性 [SEQUENCE]
     *
     */
    @Project_taskSequenceDefault(info = "默认规则")
    private Integer sequence;

    @JsonIgnore
    private boolean sequenceDirtyFlag;

    /**
     * 属性 [NOTES]
     *
     */
    @Project_taskNotesDefault(info = "默认规则")
    private String notes;

    @JsonIgnore
    private boolean notesDirtyFlag;

    /**
     * 属性 [ACTIVITY_SUMMARY]
     *
     */
    @Project_taskActivity_summaryDefault(info = "默认规则")
    private String activity_summary;

    @JsonIgnore
    private boolean activity_summaryDirtyFlag;

    /**
     * 属性 [MESSAGE_IS_FOLLOWER]
     *
     */
    @Project_taskMessage_is_followerDefault(info = "默认规则")
    private String message_is_follower;

    @JsonIgnore
    private boolean message_is_followerDirtyFlag;

    /**
     * 属性 [MESSAGE_NEEDACTION_COUNTER]
     *
     */
    @Project_taskMessage_needaction_counterDefault(info = "默认规则")
    private Integer message_needaction_counter;

    @JsonIgnore
    private boolean message_needaction_counterDirtyFlag;

    /**
     * 属性 [MESSAGE_NEEDACTION]
     *
     */
    @Project_taskMessage_needactionDefault(info = "默认规则")
    private String message_needaction;

    @JsonIgnore
    private boolean message_needactionDirtyFlag;

    /**
     * 属性 [DATE_LAST_STAGE_UPDATE]
     *
     */
    @Project_taskDate_last_stage_updateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp date_last_stage_update;

    @JsonIgnore
    private boolean date_last_stage_updateDirtyFlag;

    /**
     * 属性 [MESSAGE_HAS_ERROR_COUNTER]
     *
     */
    @Project_taskMessage_has_error_counterDefault(info = "默认规则")
    private Integer message_has_error_counter;

    @JsonIgnore
    private boolean message_has_error_counterDirtyFlag;

    /**
     * 属性 [MESSAGE_ATTACHMENT_COUNT]
     *
     */
    @Project_taskMessage_attachment_countDefault(info = "默认规则")
    private Integer message_attachment_count;

    @JsonIgnore
    private boolean message_attachment_countDirtyFlag;

    /**
     * 属性 [WORKING_DAYS_OPEN]
     *
     */
    @Project_taskWorking_days_openDefault(info = "默认规则")
    private Double working_days_open;

    @JsonIgnore
    private boolean working_days_openDirtyFlag;

    /**
     * 属性 [ACTIVITY_USER_ID]
     *
     */
    @Project_taskActivity_user_idDefault(info = "默认规则")
    private Integer activity_user_id;

    @JsonIgnore
    private boolean activity_user_idDirtyFlag;

    /**
     * 属性 [WORKING_DAYS_CLOSE]
     *
     */
    @Project_taskWorking_days_closeDefault(info = "默认规则")
    private Double working_days_close;

    @JsonIgnore
    private boolean working_days_closeDirtyFlag;

    /**
     * 属性 [ACTIVE]
     *
     */
    @Project_taskActiveDefault(info = "默认规则")
    private String active;

    @JsonIgnore
    private boolean activeDirtyFlag;

    /**
     * 属性 [MESSAGE_MAIN_ATTACHMENT_ID]
     *
     */
    @Project_taskMessage_main_attachment_idDefault(info = "默认规则")
    private Integer message_main_attachment_id;

    @JsonIgnore
    private boolean message_main_attachment_idDirtyFlag;

    /**
     * 属性 [MESSAGE_UNREAD_COUNTER]
     *
     */
    @Project_taskMessage_unread_counterDefault(info = "默认规则")
    private Integer message_unread_counter;

    @JsonIgnore
    private boolean message_unread_counterDirtyFlag;

    /**
     * 属性 [EMAIL_CC]
     *
     */
    @Project_taskEmail_ccDefault(info = "默认规则")
    private String email_cc;

    @JsonIgnore
    private boolean email_ccDirtyFlag;

    /**
     * 属性 [DATE_END]
     *
     */
    @Project_taskDate_endDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp date_end;

    @JsonIgnore
    private boolean date_endDirtyFlag;

    /**
     * 属性 [CHILD_IDS]
     *
     */
    @Project_taskChild_idsDefault(info = "默认规则")
    private String child_ids;

    @JsonIgnore
    private boolean child_idsDirtyFlag;

    /**
     * 属性 [PLANNED_HOURS]
     *
     */
    @Project_taskPlanned_hoursDefault(info = "默认规则")
    private Double planned_hours;

    @JsonIgnore
    private boolean planned_hoursDirtyFlag;

    /**
     * 属性 [WEBSITE_MESSAGE_IDS]
     *
     */
    @Project_taskWebsite_message_idsDefault(info = "默认规则")
    private String website_message_ids;

    @JsonIgnore
    private boolean website_message_idsDirtyFlag;

    /**
     * 属性 [PRIORITY]
     *
     */
    @Project_taskPriorityDefault(info = "默认规则")
    private String priority;

    @JsonIgnore
    private boolean priorityDirtyFlag;

    /**
     * 属性 [MESSAGE_PARTNER_IDS]
     *
     */
    @Project_taskMessage_partner_idsDefault(info = "默认规则")
    private String message_partner_ids;

    @JsonIgnore
    private boolean message_partner_idsDirtyFlag;

    /**
     * 属性 [MESSAGE_HAS_ERROR]
     *
     */
    @Project_taskMessage_has_errorDefault(info = "默认规则")
    private String message_has_error;

    @JsonIgnore
    private boolean message_has_errorDirtyFlag;

    /**
     * 属性 [COLOR]
     *
     */
    @Project_taskColorDefault(info = "默认规则")
    private Integer color;

    @JsonIgnore
    private boolean colorDirtyFlag;

    /**
     * 属性 [CREATE_DATE]
     *
     */
    @Project_taskCreate_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp create_date;

    @JsonIgnore
    private boolean create_dateDirtyFlag;

    /**
     * 属性 [ACCESS_URL]
     *
     */
    @Project_taskAccess_urlDefault(info = "默认规则")
    private String access_url;

    @JsonIgnore
    private boolean access_urlDirtyFlag;

    /**
     * 属性 [NAME]
     *
     */
    @Project_taskNameDefault(info = "默认规则")
    private String name;

    @JsonIgnore
    private boolean nameDirtyFlag;

    /**
     * 属性 [SUBTASK_PLANNED_HOURS]
     *
     */
    @Project_taskSubtask_planned_hoursDefault(info = "默认规则")
    private Double subtask_planned_hours;

    @JsonIgnore
    private boolean subtask_planned_hoursDirtyFlag;

    /**
     * 属性 [LEGEND_DONE]
     *
     */
    @Project_taskLegend_doneDefault(info = "默认规则")
    private String legend_done;

    @JsonIgnore
    private boolean legend_doneDirtyFlag;

    /**
     * 属性 [LEGEND_BLOCKED]
     *
     */
    @Project_taskLegend_blockedDefault(info = "默认规则")
    private String legend_blocked;

    @JsonIgnore
    private boolean legend_blockedDirtyFlag;

    /**
     * 属性 [COMPANY_ID_TEXT]
     *
     */
    @Project_taskCompany_id_textDefault(info = "默认规则")
    private String company_id_text;

    @JsonIgnore
    private boolean company_id_textDirtyFlag;

    /**
     * 属性 [PARTNER_ID_TEXT]
     *
     */
    @Project_taskPartner_id_textDefault(info = "默认规则")
    private String partner_id_text;

    @JsonIgnore
    private boolean partner_id_textDirtyFlag;

    /**
     * 属性 [MANAGER_ID]
     *
     */
    @Project_taskManager_idDefault(info = "默认规则")
    private Integer manager_id;

    @JsonIgnore
    private boolean manager_idDirtyFlag;

    /**
     * 属性 [PROJECT_ID_TEXT]
     *
     */
    @Project_taskProject_id_textDefault(info = "默认规则")
    private String project_id_text;

    @JsonIgnore
    private boolean project_id_textDirtyFlag;

    /**
     * 属性 [PARENT_ID_TEXT]
     *
     */
    @Project_taskParent_id_textDefault(info = "默认规则")
    private String parent_id_text;

    @JsonIgnore
    private boolean parent_id_textDirtyFlag;

    /**
     * 属性 [USER_ID_TEXT]
     *
     */
    @Project_taskUser_id_textDefault(info = "默认规则")
    private String user_id_text;

    @JsonIgnore
    private boolean user_id_textDirtyFlag;

    /**
     * 属性 [CREATE_UID_TEXT]
     *
     */
    @Project_taskCreate_uid_textDefault(info = "默认规则")
    private String create_uid_text;

    @JsonIgnore
    private boolean create_uid_textDirtyFlag;

    /**
     * 属性 [USER_EMAIL]
     *
     */
    @Project_taskUser_emailDefault(info = "默认规则")
    private String user_email;

    @JsonIgnore
    private boolean user_emailDirtyFlag;

    /**
     * 属性 [SUBTASK_PROJECT_ID]
     *
     */
    @Project_taskSubtask_project_idDefault(info = "默认规则")
    private Integer subtask_project_id;

    @JsonIgnore
    private boolean subtask_project_idDirtyFlag;

    /**
     * 属性 [WRITE_UID_TEXT]
     *
     */
    @Project_taskWrite_uid_textDefault(info = "默认规则")
    private String write_uid_text;

    @JsonIgnore
    private boolean write_uid_textDirtyFlag;

    /**
     * 属性 [STAGE_ID_TEXT]
     *
     */
    @Project_taskStage_id_textDefault(info = "默认规则")
    private String stage_id_text;

    @JsonIgnore
    private boolean stage_id_textDirtyFlag;

    /**
     * 属性 [LEGEND_NORMAL]
     *
     */
    @Project_taskLegend_normalDefault(info = "默认规则")
    private String legend_normal;

    @JsonIgnore
    private boolean legend_normalDirtyFlag;

    /**
     * 属性 [CREATE_UID]
     *
     */
    @Project_taskCreate_uidDefault(info = "默认规则")
    private Integer create_uid;

    @JsonIgnore
    private boolean create_uidDirtyFlag;

    /**
     * 属性 [USER_ID]
     *
     */
    @Project_taskUser_idDefault(info = "默认规则")
    private Integer user_id;

    @JsonIgnore
    private boolean user_idDirtyFlag;

    /**
     * 属性 [PROJECT_ID]
     *
     */
    @Project_taskProject_idDefault(info = "默认规则")
    private Integer project_id;

    @JsonIgnore
    private boolean project_idDirtyFlag;

    /**
     * 属性 [COMPANY_ID]
     *
     */
    @Project_taskCompany_idDefault(info = "默认规则")
    private Integer company_id;

    @JsonIgnore
    private boolean company_idDirtyFlag;

    /**
     * 属性 [PARTNER_ID]
     *
     */
    @Project_taskPartner_idDefault(info = "默认规则")
    private Integer partner_id;

    @JsonIgnore
    private boolean partner_idDirtyFlag;

    /**
     * 属性 [WRITE_UID]
     *
     */
    @Project_taskWrite_uidDefault(info = "默认规则")
    private Integer write_uid;

    @JsonIgnore
    private boolean write_uidDirtyFlag;

    /**
     * 属性 [PARENT_ID]
     *
     */
    @Project_taskParent_idDefault(info = "默认规则")
    private Integer parent_id;

    @JsonIgnore
    private boolean parent_idDirtyFlag;

    /**
     * 属性 [STAGE_ID]
     *
     */
    @Project_taskStage_idDefault(info = "默认规则")
    private Integer stage_id;

    @JsonIgnore
    private boolean stage_idDirtyFlag;


    /**
     * 获取 [DATE_START]
     */
    @JsonProperty("date_start")
    public Timestamp getDate_start(){
        return date_start ;
    }

    /**
     * 设置 [DATE_START]
     */
    @JsonProperty("date_start")
    public void setDate_start(Timestamp  date_start){
        this.date_start = date_start ;
        this.date_startDirtyFlag = true ;
    }

    /**
     * 获取 [DATE_START]脏标记
     */
    @JsonIgnore
    public boolean getDate_startDirtyFlag(){
        return date_startDirtyFlag ;
    }

    /**
     * 获取 [ACTIVITY_STATE]
     */
    @JsonProperty("activity_state")
    public String getActivity_state(){
        return activity_state ;
    }

    /**
     * 设置 [ACTIVITY_STATE]
     */
    @JsonProperty("activity_state")
    public void setActivity_state(String  activity_state){
        this.activity_state = activity_state ;
        this.activity_stateDirtyFlag = true ;
    }

    /**
     * 获取 [ACTIVITY_STATE]脏标记
     */
    @JsonIgnore
    public boolean getActivity_stateDirtyFlag(){
        return activity_stateDirtyFlag ;
    }

    /**
     * 获取 [RATING_COUNT]
     */
    @JsonProperty("rating_count")
    public Integer getRating_count(){
        return rating_count ;
    }

    /**
     * 设置 [RATING_COUNT]
     */
    @JsonProperty("rating_count")
    public void setRating_count(Integer  rating_count){
        this.rating_count = rating_count ;
        this.rating_countDirtyFlag = true ;
    }

    /**
     * 获取 [RATING_COUNT]脏标记
     */
    @JsonIgnore
    public boolean getRating_countDirtyFlag(){
        return rating_countDirtyFlag ;
    }

    /**
     * 获取 [EMAIL_FROM]
     */
    @JsonProperty("email_from")
    public String getEmail_from(){
        return email_from ;
    }

    /**
     * 设置 [EMAIL_FROM]
     */
    @JsonProperty("email_from")
    public void setEmail_from(String  email_from){
        this.email_from = email_from ;
        this.email_fromDirtyFlag = true ;
    }

    /**
     * 获取 [EMAIL_FROM]脏标记
     */
    @JsonIgnore
    public boolean getEmail_fromDirtyFlag(){
        return email_fromDirtyFlag ;
    }

    /**
     * 获取 [ATTACHMENT_IDS]
     */
    @JsonProperty("attachment_ids")
    public String getAttachment_ids(){
        return attachment_ids ;
    }

    /**
     * 设置 [ATTACHMENT_IDS]
     */
    @JsonProperty("attachment_ids")
    public void setAttachment_ids(String  attachment_ids){
        this.attachment_ids = attachment_ids ;
        this.attachment_idsDirtyFlag = true ;
    }

    /**
     * 获取 [ATTACHMENT_IDS]脏标记
     */
    @JsonIgnore
    public boolean getAttachment_idsDirtyFlag(){
        return attachment_idsDirtyFlag ;
    }

    /**
     * 获取 [DATE_DEADLINE]
     */
    @JsonProperty("date_deadline")
    public Timestamp getDate_deadline(){
        return date_deadline ;
    }

    /**
     * 设置 [DATE_DEADLINE]
     */
    @JsonProperty("date_deadline")
    public void setDate_deadline(Timestamp  date_deadline){
        this.date_deadline = date_deadline ;
        this.date_deadlineDirtyFlag = true ;
    }

    /**
     * 获取 [DATE_DEADLINE]脏标记
     */
    @JsonIgnore
    public boolean getDate_deadlineDirtyFlag(){
        return date_deadlineDirtyFlag ;
    }

    /**
     * 获取 [KANBAN_STATE_LABEL]
     */
    @JsonProperty("kanban_state_label")
    public String getKanban_state_label(){
        return kanban_state_label ;
    }

    /**
     * 设置 [KANBAN_STATE_LABEL]
     */
    @JsonProperty("kanban_state_label")
    public void setKanban_state_label(String  kanban_state_label){
        this.kanban_state_label = kanban_state_label ;
        this.kanban_state_labelDirtyFlag = true ;
    }

    /**
     * 获取 [KANBAN_STATE_LABEL]脏标记
     */
    @JsonIgnore
    public boolean getKanban_state_labelDirtyFlag(){
        return kanban_state_labelDirtyFlag ;
    }

    /**
     * 获取 [ACCESS_WARNING]
     */
    @JsonProperty("access_warning")
    public String getAccess_warning(){
        return access_warning ;
    }

    /**
     * 设置 [ACCESS_WARNING]
     */
    @JsonProperty("access_warning")
    public void setAccess_warning(String  access_warning){
        this.access_warning = access_warning ;
        this.access_warningDirtyFlag = true ;
    }

    /**
     * 获取 [ACCESS_WARNING]脏标记
     */
    @JsonIgnore
    public boolean getAccess_warningDirtyFlag(){
        return access_warningDirtyFlag ;
    }

    /**
     * 获取 [RATING_IDS]
     */
    @JsonProperty("rating_ids")
    public String getRating_ids(){
        return rating_ids ;
    }

    /**
     * 设置 [RATING_IDS]
     */
    @JsonProperty("rating_ids")
    public void setRating_ids(String  rating_ids){
        this.rating_ids = rating_ids ;
        this.rating_idsDirtyFlag = true ;
    }

    /**
     * 获取 [RATING_IDS]脏标记
     */
    @JsonIgnore
    public boolean getRating_idsDirtyFlag(){
        return rating_idsDirtyFlag ;
    }

    /**
     * 获取 [RATING_LAST_VALUE]
     */
    @JsonProperty("rating_last_value")
    public Double getRating_last_value(){
        return rating_last_value ;
    }

    /**
     * 设置 [RATING_LAST_VALUE]
     */
    @JsonProperty("rating_last_value")
    public void setRating_last_value(Double  rating_last_value){
        this.rating_last_value = rating_last_value ;
        this.rating_last_valueDirtyFlag = true ;
    }

    /**
     * 获取 [RATING_LAST_VALUE]脏标记
     */
    @JsonIgnore
    public boolean getRating_last_valueDirtyFlag(){
        return rating_last_valueDirtyFlag ;
    }

    /**
     * 获取 [DATE_ASSIGN]
     */
    @JsonProperty("date_assign")
    public Timestamp getDate_assign(){
        return date_assign ;
    }

    /**
     * 设置 [DATE_ASSIGN]
     */
    @JsonProperty("date_assign")
    public void setDate_assign(Timestamp  date_assign){
        this.date_assign = date_assign ;
        this.date_assignDirtyFlag = true ;
    }

    /**
     * 获取 [DATE_ASSIGN]脏标记
     */
    @JsonIgnore
    public boolean getDate_assignDirtyFlag(){
        return date_assignDirtyFlag ;
    }

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

    /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return idDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_IDS]
     */
    @JsonProperty("message_ids")
    public String getMessage_ids(){
        return message_ids ;
    }

    /**
     * 设置 [MESSAGE_IDS]
     */
    @JsonProperty("message_ids")
    public void setMessage_ids(String  message_ids){
        this.message_ids = message_ids ;
        this.message_idsDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_IDS]脏标记
     */
    @JsonIgnore
    public boolean getMessage_idsDirtyFlag(){
        return message_idsDirtyFlag ;
    }

    /**
     * 获取 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return __last_update ;
    }

    /**
     * 设置 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

    /**
     * 获取 [__LAST_UPDATE]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return __last_updateDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_UNREAD]
     */
    @JsonProperty("message_unread")
    public String getMessage_unread(){
        return message_unread ;
    }

    /**
     * 设置 [MESSAGE_UNREAD]
     */
    @JsonProperty("message_unread")
    public void setMessage_unread(String  message_unread){
        this.message_unread = message_unread ;
        this.message_unreadDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_UNREAD]脏标记
     */
    @JsonIgnore
    public boolean getMessage_unreadDirtyFlag(){
        return message_unreadDirtyFlag ;
    }

    /**
     * 获取 [RATING_LAST_FEEDBACK]
     */
    @JsonProperty("rating_last_feedback")
    public String getRating_last_feedback(){
        return rating_last_feedback ;
    }

    /**
     * 设置 [RATING_LAST_FEEDBACK]
     */
    @JsonProperty("rating_last_feedback")
    public void setRating_last_feedback(String  rating_last_feedback){
        this.rating_last_feedback = rating_last_feedback ;
        this.rating_last_feedbackDirtyFlag = true ;
    }

    /**
     * 获取 [RATING_LAST_FEEDBACK]脏标记
     */
    @JsonIgnore
    public boolean getRating_last_feedbackDirtyFlag(){
        return rating_last_feedbackDirtyFlag ;
    }

    /**
     * 获取 [ACTIVITY_DATE_DEADLINE]
     */
    @JsonProperty("activity_date_deadline")
    public Timestamp getActivity_date_deadline(){
        return activity_date_deadline ;
    }

    /**
     * 设置 [ACTIVITY_DATE_DEADLINE]
     */
    @JsonProperty("activity_date_deadline")
    public void setActivity_date_deadline(Timestamp  activity_date_deadline){
        this.activity_date_deadline = activity_date_deadline ;
        this.activity_date_deadlineDirtyFlag = true ;
    }

    /**
     * 获取 [ACTIVITY_DATE_DEADLINE]脏标记
     */
    @JsonIgnore
    public boolean getActivity_date_deadlineDirtyFlag(){
        return activity_date_deadlineDirtyFlag ;
    }

    /**
     * 获取 [ACCESS_TOKEN]
     */
    @JsonProperty("access_token")
    public String getAccess_token(){
        return access_token ;
    }

    /**
     * 设置 [ACCESS_TOKEN]
     */
    @JsonProperty("access_token")
    public void setAccess_token(String  access_token){
        this.access_token = access_token ;
        this.access_tokenDirtyFlag = true ;
    }

    /**
     * 获取 [ACCESS_TOKEN]脏标记
     */
    @JsonIgnore
    public boolean getAccess_tokenDirtyFlag(){
        return access_tokenDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_FOLLOWER_IDS]
     */
    @JsonProperty("message_follower_ids")
    public String getMessage_follower_ids(){
        return message_follower_ids ;
    }

    /**
     * 设置 [MESSAGE_FOLLOWER_IDS]
     */
    @JsonProperty("message_follower_ids")
    public void setMessage_follower_ids(String  message_follower_ids){
        this.message_follower_ids = message_follower_ids ;
        this.message_follower_idsDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_FOLLOWER_IDS]脏标记
     */
    @JsonIgnore
    public boolean getMessage_follower_idsDirtyFlag(){
        return message_follower_idsDirtyFlag ;
    }

    /**
     * 获取 [SUBTASK_COUNT]
     */
    @JsonProperty("subtask_count")
    public Integer getSubtask_count(){
        return subtask_count ;
    }

    /**
     * 设置 [SUBTASK_COUNT]
     */
    @JsonProperty("subtask_count")
    public void setSubtask_count(Integer  subtask_count){
        this.subtask_count = subtask_count ;
        this.subtask_countDirtyFlag = true ;
    }

    /**
     * 获取 [SUBTASK_COUNT]脏标记
     */
    @JsonIgnore
    public boolean getSubtask_countDirtyFlag(){
        return subtask_countDirtyFlag ;
    }

    /**
     * 获取 [WORKING_HOURS_OPEN]
     */
    @JsonProperty("working_hours_open")
    public Double getWorking_hours_open(){
        return working_hours_open ;
    }

    /**
     * 设置 [WORKING_HOURS_OPEN]
     */
    @JsonProperty("working_hours_open")
    public void setWorking_hours_open(Double  working_hours_open){
        this.working_hours_open = working_hours_open ;
        this.working_hours_openDirtyFlag = true ;
    }

    /**
     * 获取 [WORKING_HOURS_OPEN]脏标记
     */
    @JsonIgnore
    public boolean getWorking_hours_openDirtyFlag(){
        return working_hours_openDirtyFlag ;
    }

    /**
     * 获取 [WORKING_HOURS_CLOSE]
     */
    @JsonProperty("working_hours_close")
    public Double getWorking_hours_close(){
        return working_hours_close ;
    }

    /**
     * 设置 [WORKING_HOURS_CLOSE]
     */
    @JsonProperty("working_hours_close")
    public void setWorking_hours_close(Double  working_hours_close){
        this.working_hours_close = working_hours_close ;
        this.working_hours_closeDirtyFlag = true ;
    }

    /**
     * 获取 [WORKING_HOURS_CLOSE]脏标记
     */
    @JsonIgnore
    public boolean getWorking_hours_closeDirtyFlag(){
        return working_hours_closeDirtyFlag ;
    }

    /**
     * 获取 [DESCRIPTION]
     */
    @JsonProperty("description")
    public String getDescription(){
        return description ;
    }

    /**
     * 设置 [DESCRIPTION]
     */
    @JsonProperty("description")
    public void setDescription(String  description){
        this.description = description ;
        this.descriptionDirtyFlag = true ;
    }

    /**
     * 获取 [DESCRIPTION]脏标记
     */
    @JsonIgnore
    public boolean getDescriptionDirtyFlag(){
        return descriptionDirtyFlag ;
    }

    /**
     * 获取 [RATING_LAST_IMAGE]
     */
    @JsonProperty("rating_last_image")
    public byte[] getRating_last_image(){
        return rating_last_image ;
    }

    /**
     * 设置 [RATING_LAST_IMAGE]
     */
    @JsonProperty("rating_last_image")
    public void setRating_last_image(byte[]  rating_last_image){
        this.rating_last_image = rating_last_image ;
        this.rating_last_imageDirtyFlag = true ;
    }

    /**
     * 获取 [RATING_LAST_IMAGE]脏标记
     */
    @JsonIgnore
    public boolean getRating_last_imageDirtyFlag(){
        return rating_last_imageDirtyFlag ;
    }

    /**
     * 获取 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return write_date ;
    }

    /**
     * 设置 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return write_dateDirtyFlag ;
    }

    /**
     * 获取 [DISPLAYED_IMAGE_ID]
     */
    @JsonProperty("displayed_image_id")
    public Integer getDisplayed_image_id(){
        return displayed_image_id ;
    }

    /**
     * 设置 [DISPLAYED_IMAGE_ID]
     */
    @JsonProperty("displayed_image_id")
    public void setDisplayed_image_id(Integer  displayed_image_id){
        this.displayed_image_id = displayed_image_id ;
        this.displayed_image_idDirtyFlag = true ;
    }

    /**
     * 获取 [DISPLAYED_IMAGE_ID]脏标记
     */
    @JsonIgnore
    public boolean getDisplayed_image_idDirtyFlag(){
        return displayed_image_idDirtyFlag ;
    }

    /**
     * 获取 [TAG_IDS]
     */
    @JsonProperty("tag_ids")
    public String getTag_ids(){
        return tag_ids ;
    }

    /**
     * 设置 [TAG_IDS]
     */
    @JsonProperty("tag_ids")
    public void setTag_ids(String  tag_ids){
        this.tag_ids = tag_ids ;
        this.tag_idsDirtyFlag = true ;
    }

    /**
     * 获取 [TAG_IDS]脏标记
     */
    @JsonIgnore
    public boolean getTag_idsDirtyFlag(){
        return tag_idsDirtyFlag ;
    }

    /**
     * 获取 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return display_name ;
    }

    /**
     * 设置 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

    /**
     * 获取 [DISPLAY_NAME]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return display_nameDirtyFlag ;
    }

    /**
     * 获取 [ACTIVITY_TYPE_ID]
     */
    @JsonProperty("activity_type_id")
    public Integer getActivity_type_id(){
        return activity_type_id ;
    }

    /**
     * 设置 [ACTIVITY_TYPE_ID]
     */
    @JsonProperty("activity_type_id")
    public void setActivity_type_id(Integer  activity_type_id){
        this.activity_type_id = activity_type_id ;
        this.activity_type_idDirtyFlag = true ;
    }

    /**
     * 获取 [ACTIVITY_TYPE_ID]脏标记
     */
    @JsonIgnore
    public boolean getActivity_type_idDirtyFlag(){
        return activity_type_idDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_CHANNEL_IDS]
     */
    @JsonProperty("message_channel_ids")
    public String getMessage_channel_ids(){
        return message_channel_ids ;
    }

    /**
     * 设置 [MESSAGE_CHANNEL_IDS]
     */
    @JsonProperty("message_channel_ids")
    public void setMessage_channel_ids(String  message_channel_ids){
        this.message_channel_ids = message_channel_ids ;
        this.message_channel_idsDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_CHANNEL_IDS]脏标记
     */
    @JsonIgnore
    public boolean getMessage_channel_idsDirtyFlag(){
        return message_channel_idsDirtyFlag ;
    }

    /**
     * 获取 [ACTIVITY_IDS]
     */
    @JsonProperty("activity_ids")
    public String getActivity_ids(){
        return activity_ids ;
    }

    /**
     * 设置 [ACTIVITY_IDS]
     */
    @JsonProperty("activity_ids")
    public void setActivity_ids(String  activity_ids){
        this.activity_ids = activity_ids ;
        this.activity_idsDirtyFlag = true ;
    }

    /**
     * 获取 [ACTIVITY_IDS]脏标记
     */
    @JsonIgnore
    public boolean getActivity_idsDirtyFlag(){
        return activity_idsDirtyFlag ;
    }

    /**
     * 获取 [KANBAN_STATE]
     */
    @JsonProperty("kanban_state")
    public String getKanban_state(){
        return kanban_state ;
    }

    /**
     * 设置 [KANBAN_STATE]
     */
    @JsonProperty("kanban_state")
    public void setKanban_state(String  kanban_state){
        this.kanban_state = kanban_state ;
        this.kanban_stateDirtyFlag = true ;
    }

    /**
     * 获取 [KANBAN_STATE]脏标记
     */
    @JsonIgnore
    public boolean getKanban_stateDirtyFlag(){
        return kanban_stateDirtyFlag ;
    }

    /**
     * 获取 [SEQUENCE]
     */
    @JsonProperty("sequence")
    public Integer getSequence(){
        return sequence ;
    }

    /**
     * 设置 [SEQUENCE]
     */
    @JsonProperty("sequence")
    public void setSequence(Integer  sequence){
        this.sequence = sequence ;
        this.sequenceDirtyFlag = true ;
    }

    /**
     * 获取 [SEQUENCE]脏标记
     */
    @JsonIgnore
    public boolean getSequenceDirtyFlag(){
        return sequenceDirtyFlag ;
    }

    /**
     * 获取 [NOTES]
     */
    @JsonProperty("notes")
    public String getNotes(){
        return notes ;
    }

    /**
     * 设置 [NOTES]
     */
    @JsonProperty("notes")
    public void setNotes(String  notes){
        this.notes = notes ;
        this.notesDirtyFlag = true ;
    }

    /**
     * 获取 [NOTES]脏标记
     */
    @JsonIgnore
    public boolean getNotesDirtyFlag(){
        return notesDirtyFlag ;
    }

    /**
     * 获取 [ACTIVITY_SUMMARY]
     */
    @JsonProperty("activity_summary")
    public String getActivity_summary(){
        return activity_summary ;
    }

    /**
     * 设置 [ACTIVITY_SUMMARY]
     */
    @JsonProperty("activity_summary")
    public void setActivity_summary(String  activity_summary){
        this.activity_summary = activity_summary ;
        this.activity_summaryDirtyFlag = true ;
    }

    /**
     * 获取 [ACTIVITY_SUMMARY]脏标记
     */
    @JsonIgnore
    public boolean getActivity_summaryDirtyFlag(){
        return activity_summaryDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_IS_FOLLOWER]
     */
    @JsonProperty("message_is_follower")
    public String getMessage_is_follower(){
        return message_is_follower ;
    }

    /**
     * 设置 [MESSAGE_IS_FOLLOWER]
     */
    @JsonProperty("message_is_follower")
    public void setMessage_is_follower(String  message_is_follower){
        this.message_is_follower = message_is_follower ;
        this.message_is_followerDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_IS_FOLLOWER]脏标记
     */
    @JsonIgnore
    public boolean getMessage_is_followerDirtyFlag(){
        return message_is_followerDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_NEEDACTION_COUNTER]
     */
    @JsonProperty("message_needaction_counter")
    public Integer getMessage_needaction_counter(){
        return message_needaction_counter ;
    }

    /**
     * 设置 [MESSAGE_NEEDACTION_COUNTER]
     */
    @JsonProperty("message_needaction_counter")
    public void setMessage_needaction_counter(Integer  message_needaction_counter){
        this.message_needaction_counter = message_needaction_counter ;
        this.message_needaction_counterDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_NEEDACTION_COUNTER]脏标记
     */
    @JsonIgnore
    public boolean getMessage_needaction_counterDirtyFlag(){
        return message_needaction_counterDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_NEEDACTION]
     */
    @JsonProperty("message_needaction")
    public String getMessage_needaction(){
        return message_needaction ;
    }

    /**
     * 设置 [MESSAGE_NEEDACTION]
     */
    @JsonProperty("message_needaction")
    public void setMessage_needaction(String  message_needaction){
        this.message_needaction = message_needaction ;
        this.message_needactionDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_NEEDACTION]脏标记
     */
    @JsonIgnore
    public boolean getMessage_needactionDirtyFlag(){
        return message_needactionDirtyFlag ;
    }

    /**
     * 获取 [DATE_LAST_STAGE_UPDATE]
     */
    @JsonProperty("date_last_stage_update")
    public Timestamp getDate_last_stage_update(){
        return date_last_stage_update ;
    }

    /**
     * 设置 [DATE_LAST_STAGE_UPDATE]
     */
    @JsonProperty("date_last_stage_update")
    public void setDate_last_stage_update(Timestamp  date_last_stage_update){
        this.date_last_stage_update = date_last_stage_update ;
        this.date_last_stage_updateDirtyFlag = true ;
    }

    /**
     * 获取 [DATE_LAST_STAGE_UPDATE]脏标记
     */
    @JsonIgnore
    public boolean getDate_last_stage_updateDirtyFlag(){
        return date_last_stage_updateDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_HAS_ERROR_COUNTER]
     */
    @JsonProperty("message_has_error_counter")
    public Integer getMessage_has_error_counter(){
        return message_has_error_counter ;
    }

    /**
     * 设置 [MESSAGE_HAS_ERROR_COUNTER]
     */
    @JsonProperty("message_has_error_counter")
    public void setMessage_has_error_counter(Integer  message_has_error_counter){
        this.message_has_error_counter = message_has_error_counter ;
        this.message_has_error_counterDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_HAS_ERROR_COUNTER]脏标记
     */
    @JsonIgnore
    public boolean getMessage_has_error_counterDirtyFlag(){
        return message_has_error_counterDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_ATTACHMENT_COUNT]
     */
    @JsonProperty("message_attachment_count")
    public Integer getMessage_attachment_count(){
        return message_attachment_count ;
    }

    /**
     * 设置 [MESSAGE_ATTACHMENT_COUNT]
     */
    @JsonProperty("message_attachment_count")
    public void setMessage_attachment_count(Integer  message_attachment_count){
        this.message_attachment_count = message_attachment_count ;
        this.message_attachment_countDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_ATTACHMENT_COUNT]脏标记
     */
    @JsonIgnore
    public boolean getMessage_attachment_countDirtyFlag(){
        return message_attachment_countDirtyFlag ;
    }

    /**
     * 获取 [WORKING_DAYS_OPEN]
     */
    @JsonProperty("working_days_open")
    public Double getWorking_days_open(){
        return working_days_open ;
    }

    /**
     * 设置 [WORKING_DAYS_OPEN]
     */
    @JsonProperty("working_days_open")
    public void setWorking_days_open(Double  working_days_open){
        this.working_days_open = working_days_open ;
        this.working_days_openDirtyFlag = true ;
    }

    /**
     * 获取 [WORKING_DAYS_OPEN]脏标记
     */
    @JsonIgnore
    public boolean getWorking_days_openDirtyFlag(){
        return working_days_openDirtyFlag ;
    }

    /**
     * 获取 [ACTIVITY_USER_ID]
     */
    @JsonProperty("activity_user_id")
    public Integer getActivity_user_id(){
        return activity_user_id ;
    }

    /**
     * 设置 [ACTIVITY_USER_ID]
     */
    @JsonProperty("activity_user_id")
    public void setActivity_user_id(Integer  activity_user_id){
        this.activity_user_id = activity_user_id ;
        this.activity_user_idDirtyFlag = true ;
    }

    /**
     * 获取 [ACTIVITY_USER_ID]脏标记
     */
    @JsonIgnore
    public boolean getActivity_user_idDirtyFlag(){
        return activity_user_idDirtyFlag ;
    }

    /**
     * 获取 [WORKING_DAYS_CLOSE]
     */
    @JsonProperty("working_days_close")
    public Double getWorking_days_close(){
        return working_days_close ;
    }

    /**
     * 设置 [WORKING_DAYS_CLOSE]
     */
    @JsonProperty("working_days_close")
    public void setWorking_days_close(Double  working_days_close){
        this.working_days_close = working_days_close ;
        this.working_days_closeDirtyFlag = true ;
    }

    /**
     * 获取 [WORKING_DAYS_CLOSE]脏标记
     */
    @JsonIgnore
    public boolean getWorking_days_closeDirtyFlag(){
        return working_days_closeDirtyFlag ;
    }

    /**
     * 获取 [ACTIVE]
     */
    @JsonProperty("active")
    public String getActive(){
        return active ;
    }

    /**
     * 设置 [ACTIVE]
     */
    @JsonProperty("active")
    public void setActive(String  active){
        this.active = active ;
        this.activeDirtyFlag = true ;
    }

    /**
     * 获取 [ACTIVE]脏标记
     */
    @JsonIgnore
    public boolean getActiveDirtyFlag(){
        return activeDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_MAIN_ATTACHMENT_ID]
     */
    @JsonProperty("message_main_attachment_id")
    public Integer getMessage_main_attachment_id(){
        return message_main_attachment_id ;
    }

    /**
     * 设置 [MESSAGE_MAIN_ATTACHMENT_ID]
     */
    @JsonProperty("message_main_attachment_id")
    public void setMessage_main_attachment_id(Integer  message_main_attachment_id){
        this.message_main_attachment_id = message_main_attachment_id ;
        this.message_main_attachment_idDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_MAIN_ATTACHMENT_ID]脏标记
     */
    @JsonIgnore
    public boolean getMessage_main_attachment_idDirtyFlag(){
        return message_main_attachment_idDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_UNREAD_COUNTER]
     */
    @JsonProperty("message_unread_counter")
    public Integer getMessage_unread_counter(){
        return message_unread_counter ;
    }

    /**
     * 设置 [MESSAGE_UNREAD_COUNTER]
     */
    @JsonProperty("message_unread_counter")
    public void setMessage_unread_counter(Integer  message_unread_counter){
        this.message_unread_counter = message_unread_counter ;
        this.message_unread_counterDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_UNREAD_COUNTER]脏标记
     */
    @JsonIgnore
    public boolean getMessage_unread_counterDirtyFlag(){
        return message_unread_counterDirtyFlag ;
    }

    /**
     * 获取 [EMAIL_CC]
     */
    @JsonProperty("email_cc")
    public String getEmail_cc(){
        return email_cc ;
    }

    /**
     * 设置 [EMAIL_CC]
     */
    @JsonProperty("email_cc")
    public void setEmail_cc(String  email_cc){
        this.email_cc = email_cc ;
        this.email_ccDirtyFlag = true ;
    }

    /**
     * 获取 [EMAIL_CC]脏标记
     */
    @JsonIgnore
    public boolean getEmail_ccDirtyFlag(){
        return email_ccDirtyFlag ;
    }

    /**
     * 获取 [DATE_END]
     */
    @JsonProperty("date_end")
    public Timestamp getDate_end(){
        return date_end ;
    }

    /**
     * 设置 [DATE_END]
     */
    @JsonProperty("date_end")
    public void setDate_end(Timestamp  date_end){
        this.date_end = date_end ;
        this.date_endDirtyFlag = true ;
    }

    /**
     * 获取 [DATE_END]脏标记
     */
    @JsonIgnore
    public boolean getDate_endDirtyFlag(){
        return date_endDirtyFlag ;
    }

    /**
     * 获取 [CHILD_IDS]
     */
    @JsonProperty("child_ids")
    public String getChild_ids(){
        return child_ids ;
    }

    /**
     * 设置 [CHILD_IDS]
     */
    @JsonProperty("child_ids")
    public void setChild_ids(String  child_ids){
        this.child_ids = child_ids ;
        this.child_idsDirtyFlag = true ;
    }

    /**
     * 获取 [CHILD_IDS]脏标记
     */
    @JsonIgnore
    public boolean getChild_idsDirtyFlag(){
        return child_idsDirtyFlag ;
    }

    /**
     * 获取 [PLANNED_HOURS]
     */
    @JsonProperty("planned_hours")
    public Double getPlanned_hours(){
        return planned_hours ;
    }

    /**
     * 设置 [PLANNED_HOURS]
     */
    @JsonProperty("planned_hours")
    public void setPlanned_hours(Double  planned_hours){
        this.planned_hours = planned_hours ;
        this.planned_hoursDirtyFlag = true ;
    }

    /**
     * 获取 [PLANNED_HOURS]脏标记
     */
    @JsonIgnore
    public boolean getPlanned_hoursDirtyFlag(){
        return planned_hoursDirtyFlag ;
    }

    /**
     * 获取 [WEBSITE_MESSAGE_IDS]
     */
    @JsonProperty("website_message_ids")
    public String getWebsite_message_ids(){
        return website_message_ids ;
    }

    /**
     * 设置 [WEBSITE_MESSAGE_IDS]
     */
    @JsonProperty("website_message_ids")
    public void setWebsite_message_ids(String  website_message_ids){
        this.website_message_ids = website_message_ids ;
        this.website_message_idsDirtyFlag = true ;
    }

    /**
     * 获取 [WEBSITE_MESSAGE_IDS]脏标记
     */
    @JsonIgnore
    public boolean getWebsite_message_idsDirtyFlag(){
        return website_message_idsDirtyFlag ;
    }

    /**
     * 获取 [PRIORITY]
     */
    @JsonProperty("priority")
    public String getPriority(){
        return priority ;
    }

    /**
     * 设置 [PRIORITY]
     */
    @JsonProperty("priority")
    public void setPriority(String  priority){
        this.priority = priority ;
        this.priorityDirtyFlag = true ;
    }

    /**
     * 获取 [PRIORITY]脏标记
     */
    @JsonIgnore
    public boolean getPriorityDirtyFlag(){
        return priorityDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_PARTNER_IDS]
     */
    @JsonProperty("message_partner_ids")
    public String getMessage_partner_ids(){
        return message_partner_ids ;
    }

    /**
     * 设置 [MESSAGE_PARTNER_IDS]
     */
    @JsonProperty("message_partner_ids")
    public void setMessage_partner_ids(String  message_partner_ids){
        this.message_partner_ids = message_partner_ids ;
        this.message_partner_idsDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_PARTNER_IDS]脏标记
     */
    @JsonIgnore
    public boolean getMessage_partner_idsDirtyFlag(){
        return message_partner_idsDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_HAS_ERROR]
     */
    @JsonProperty("message_has_error")
    public String getMessage_has_error(){
        return message_has_error ;
    }

    /**
     * 设置 [MESSAGE_HAS_ERROR]
     */
    @JsonProperty("message_has_error")
    public void setMessage_has_error(String  message_has_error){
        this.message_has_error = message_has_error ;
        this.message_has_errorDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_HAS_ERROR]脏标记
     */
    @JsonIgnore
    public boolean getMessage_has_errorDirtyFlag(){
        return message_has_errorDirtyFlag ;
    }

    /**
     * 获取 [COLOR]
     */
    @JsonProperty("color")
    public Integer getColor(){
        return color ;
    }

    /**
     * 设置 [COLOR]
     */
    @JsonProperty("color")
    public void setColor(Integer  color){
        this.color = color ;
        this.colorDirtyFlag = true ;
    }

    /**
     * 获取 [COLOR]脏标记
     */
    @JsonIgnore
    public boolean getColorDirtyFlag(){
        return colorDirtyFlag ;
    }

    /**
     * 获取 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return create_date ;
    }

    /**
     * 设置 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return create_dateDirtyFlag ;
    }

    /**
     * 获取 [ACCESS_URL]
     */
    @JsonProperty("access_url")
    public String getAccess_url(){
        return access_url ;
    }

    /**
     * 设置 [ACCESS_URL]
     */
    @JsonProperty("access_url")
    public void setAccess_url(String  access_url){
        this.access_url = access_url ;
        this.access_urlDirtyFlag = true ;
    }

    /**
     * 获取 [ACCESS_URL]脏标记
     */
    @JsonIgnore
    public boolean getAccess_urlDirtyFlag(){
        return access_urlDirtyFlag ;
    }

    /**
     * 获取 [NAME]
     */
    @JsonProperty("name")
    public String getName(){
        return name ;
    }

    /**
     * 设置 [NAME]
     */
    @JsonProperty("name")
    public void setName(String  name){
        this.name = name ;
        this.nameDirtyFlag = true ;
    }

    /**
     * 获取 [NAME]脏标记
     */
    @JsonIgnore
    public boolean getNameDirtyFlag(){
        return nameDirtyFlag ;
    }

    /**
     * 获取 [SUBTASK_PLANNED_HOURS]
     */
    @JsonProperty("subtask_planned_hours")
    public Double getSubtask_planned_hours(){
        return subtask_planned_hours ;
    }

    /**
     * 设置 [SUBTASK_PLANNED_HOURS]
     */
    @JsonProperty("subtask_planned_hours")
    public void setSubtask_planned_hours(Double  subtask_planned_hours){
        this.subtask_planned_hours = subtask_planned_hours ;
        this.subtask_planned_hoursDirtyFlag = true ;
    }

    /**
     * 获取 [SUBTASK_PLANNED_HOURS]脏标记
     */
    @JsonIgnore
    public boolean getSubtask_planned_hoursDirtyFlag(){
        return subtask_planned_hoursDirtyFlag ;
    }

    /**
     * 获取 [LEGEND_DONE]
     */
    @JsonProperty("legend_done")
    public String getLegend_done(){
        return legend_done ;
    }

    /**
     * 设置 [LEGEND_DONE]
     */
    @JsonProperty("legend_done")
    public void setLegend_done(String  legend_done){
        this.legend_done = legend_done ;
        this.legend_doneDirtyFlag = true ;
    }

    /**
     * 获取 [LEGEND_DONE]脏标记
     */
    @JsonIgnore
    public boolean getLegend_doneDirtyFlag(){
        return legend_doneDirtyFlag ;
    }

    /**
     * 获取 [LEGEND_BLOCKED]
     */
    @JsonProperty("legend_blocked")
    public String getLegend_blocked(){
        return legend_blocked ;
    }

    /**
     * 设置 [LEGEND_BLOCKED]
     */
    @JsonProperty("legend_blocked")
    public void setLegend_blocked(String  legend_blocked){
        this.legend_blocked = legend_blocked ;
        this.legend_blockedDirtyFlag = true ;
    }

    /**
     * 获取 [LEGEND_BLOCKED]脏标记
     */
    @JsonIgnore
    public boolean getLegend_blockedDirtyFlag(){
        return legend_blockedDirtyFlag ;
    }

    /**
     * 获取 [COMPANY_ID_TEXT]
     */
    @JsonProperty("company_id_text")
    public String getCompany_id_text(){
        return company_id_text ;
    }

    /**
     * 设置 [COMPANY_ID_TEXT]
     */
    @JsonProperty("company_id_text")
    public void setCompany_id_text(String  company_id_text){
        this.company_id_text = company_id_text ;
        this.company_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [COMPANY_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getCompany_id_textDirtyFlag(){
        return company_id_textDirtyFlag ;
    }

    /**
     * 获取 [PARTNER_ID_TEXT]
     */
    @JsonProperty("partner_id_text")
    public String getPartner_id_text(){
        return partner_id_text ;
    }

    /**
     * 设置 [PARTNER_ID_TEXT]
     */
    @JsonProperty("partner_id_text")
    public void setPartner_id_text(String  partner_id_text){
        this.partner_id_text = partner_id_text ;
        this.partner_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [PARTNER_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getPartner_id_textDirtyFlag(){
        return partner_id_textDirtyFlag ;
    }

    /**
     * 获取 [MANAGER_ID]
     */
    @JsonProperty("manager_id")
    public Integer getManager_id(){
        return manager_id ;
    }

    /**
     * 设置 [MANAGER_ID]
     */
    @JsonProperty("manager_id")
    public void setManager_id(Integer  manager_id){
        this.manager_id = manager_id ;
        this.manager_idDirtyFlag = true ;
    }

    /**
     * 获取 [MANAGER_ID]脏标记
     */
    @JsonIgnore
    public boolean getManager_idDirtyFlag(){
        return manager_idDirtyFlag ;
    }

    /**
     * 获取 [PROJECT_ID_TEXT]
     */
    @JsonProperty("project_id_text")
    public String getProject_id_text(){
        return project_id_text ;
    }

    /**
     * 设置 [PROJECT_ID_TEXT]
     */
    @JsonProperty("project_id_text")
    public void setProject_id_text(String  project_id_text){
        this.project_id_text = project_id_text ;
        this.project_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [PROJECT_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getProject_id_textDirtyFlag(){
        return project_id_textDirtyFlag ;
    }

    /**
     * 获取 [PARENT_ID_TEXT]
     */
    @JsonProperty("parent_id_text")
    public String getParent_id_text(){
        return parent_id_text ;
    }

    /**
     * 设置 [PARENT_ID_TEXT]
     */
    @JsonProperty("parent_id_text")
    public void setParent_id_text(String  parent_id_text){
        this.parent_id_text = parent_id_text ;
        this.parent_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [PARENT_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getParent_id_textDirtyFlag(){
        return parent_id_textDirtyFlag ;
    }

    /**
     * 获取 [USER_ID_TEXT]
     */
    @JsonProperty("user_id_text")
    public String getUser_id_text(){
        return user_id_text ;
    }

    /**
     * 设置 [USER_ID_TEXT]
     */
    @JsonProperty("user_id_text")
    public void setUser_id_text(String  user_id_text){
        this.user_id_text = user_id_text ;
        this.user_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [USER_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getUser_id_textDirtyFlag(){
        return user_id_textDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return create_uid_text ;
    }

    /**
     * 设置 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return create_uid_textDirtyFlag ;
    }

    /**
     * 获取 [USER_EMAIL]
     */
    @JsonProperty("user_email")
    public String getUser_email(){
        return user_email ;
    }

    /**
     * 设置 [USER_EMAIL]
     */
    @JsonProperty("user_email")
    public void setUser_email(String  user_email){
        this.user_email = user_email ;
        this.user_emailDirtyFlag = true ;
    }

    /**
     * 获取 [USER_EMAIL]脏标记
     */
    @JsonIgnore
    public boolean getUser_emailDirtyFlag(){
        return user_emailDirtyFlag ;
    }

    /**
     * 获取 [SUBTASK_PROJECT_ID]
     */
    @JsonProperty("subtask_project_id")
    public Integer getSubtask_project_id(){
        return subtask_project_id ;
    }

    /**
     * 设置 [SUBTASK_PROJECT_ID]
     */
    @JsonProperty("subtask_project_id")
    public void setSubtask_project_id(Integer  subtask_project_id){
        this.subtask_project_id = subtask_project_id ;
        this.subtask_project_idDirtyFlag = true ;
    }

    /**
     * 获取 [SUBTASK_PROJECT_ID]脏标记
     */
    @JsonIgnore
    public boolean getSubtask_project_idDirtyFlag(){
        return subtask_project_idDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return write_uid_text ;
    }

    /**
     * 设置 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return write_uid_textDirtyFlag ;
    }

    /**
     * 获取 [STAGE_ID_TEXT]
     */
    @JsonProperty("stage_id_text")
    public String getStage_id_text(){
        return stage_id_text ;
    }

    /**
     * 设置 [STAGE_ID_TEXT]
     */
    @JsonProperty("stage_id_text")
    public void setStage_id_text(String  stage_id_text){
        this.stage_id_text = stage_id_text ;
        this.stage_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [STAGE_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getStage_id_textDirtyFlag(){
        return stage_id_textDirtyFlag ;
    }

    /**
     * 获取 [LEGEND_NORMAL]
     */
    @JsonProperty("legend_normal")
    public String getLegend_normal(){
        return legend_normal ;
    }

    /**
     * 设置 [LEGEND_NORMAL]
     */
    @JsonProperty("legend_normal")
    public void setLegend_normal(String  legend_normal){
        this.legend_normal = legend_normal ;
        this.legend_normalDirtyFlag = true ;
    }

    /**
     * 获取 [LEGEND_NORMAL]脏标记
     */
    @JsonIgnore
    public boolean getLegend_normalDirtyFlag(){
        return legend_normalDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return create_uid ;
    }

    /**
     * 设置 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return create_uidDirtyFlag ;
    }

    /**
     * 获取 [USER_ID]
     */
    @JsonProperty("user_id")
    public Integer getUser_id(){
        return user_id ;
    }

    /**
     * 设置 [USER_ID]
     */
    @JsonProperty("user_id")
    public void setUser_id(Integer  user_id){
        this.user_id = user_id ;
        this.user_idDirtyFlag = true ;
    }

    /**
     * 获取 [USER_ID]脏标记
     */
    @JsonIgnore
    public boolean getUser_idDirtyFlag(){
        return user_idDirtyFlag ;
    }

    /**
     * 获取 [PROJECT_ID]
     */
    @JsonProperty("project_id")
    public Integer getProject_id(){
        return project_id ;
    }

    /**
     * 设置 [PROJECT_ID]
     */
    @JsonProperty("project_id")
    public void setProject_id(Integer  project_id){
        this.project_id = project_id ;
        this.project_idDirtyFlag = true ;
    }

    /**
     * 获取 [PROJECT_ID]脏标记
     */
    @JsonIgnore
    public boolean getProject_idDirtyFlag(){
        return project_idDirtyFlag ;
    }

    /**
     * 获取 [COMPANY_ID]
     */
    @JsonProperty("company_id")
    public Integer getCompany_id(){
        return company_id ;
    }

    /**
     * 设置 [COMPANY_ID]
     */
    @JsonProperty("company_id")
    public void setCompany_id(Integer  company_id){
        this.company_id = company_id ;
        this.company_idDirtyFlag = true ;
    }

    /**
     * 获取 [COMPANY_ID]脏标记
     */
    @JsonIgnore
    public boolean getCompany_idDirtyFlag(){
        return company_idDirtyFlag ;
    }

    /**
     * 获取 [PARTNER_ID]
     */
    @JsonProperty("partner_id")
    public Integer getPartner_id(){
        return partner_id ;
    }

    /**
     * 设置 [PARTNER_ID]
     */
    @JsonProperty("partner_id")
    public void setPartner_id(Integer  partner_id){
        this.partner_id = partner_id ;
        this.partner_idDirtyFlag = true ;
    }

    /**
     * 获取 [PARTNER_ID]脏标记
     */
    @JsonIgnore
    public boolean getPartner_idDirtyFlag(){
        return partner_idDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return write_uid ;
    }

    /**
     * 设置 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return write_uidDirtyFlag ;
    }

    /**
     * 获取 [PARENT_ID]
     */
    @JsonProperty("parent_id")
    public Integer getParent_id(){
        return parent_id ;
    }

    /**
     * 设置 [PARENT_ID]
     */
    @JsonProperty("parent_id")
    public void setParent_id(Integer  parent_id){
        this.parent_id = parent_id ;
        this.parent_idDirtyFlag = true ;
    }

    /**
     * 获取 [PARENT_ID]脏标记
     */
    @JsonIgnore
    public boolean getParent_idDirtyFlag(){
        return parent_idDirtyFlag ;
    }

    /**
     * 获取 [STAGE_ID]
     */
    @JsonProperty("stage_id")
    public Integer getStage_id(){
        return stage_id ;
    }

    /**
     * 设置 [STAGE_ID]
     */
    @JsonProperty("stage_id")
    public void setStage_id(Integer  stage_id){
        this.stage_id = stage_id ;
        this.stage_idDirtyFlag = true ;
    }

    /**
     * 获取 [STAGE_ID]脏标记
     */
    @JsonIgnore
    public boolean getStage_idDirtyFlag(){
        return stage_idDirtyFlag ;
    }



    public Project_task toDO() {
        Project_task srfdomain = new Project_task();
        if(getDate_startDirtyFlag())
            srfdomain.setDate_start(date_start);
        if(getActivity_stateDirtyFlag())
            srfdomain.setActivity_state(activity_state);
        if(getRating_countDirtyFlag())
            srfdomain.setRating_count(rating_count);
        if(getEmail_fromDirtyFlag())
            srfdomain.setEmail_from(email_from);
        if(getAttachment_idsDirtyFlag())
            srfdomain.setAttachment_ids(attachment_ids);
        if(getDate_deadlineDirtyFlag())
            srfdomain.setDate_deadline(date_deadline);
        if(getKanban_state_labelDirtyFlag())
            srfdomain.setKanban_state_label(kanban_state_label);
        if(getAccess_warningDirtyFlag())
            srfdomain.setAccess_warning(access_warning);
        if(getRating_idsDirtyFlag())
            srfdomain.setRating_ids(rating_ids);
        if(getRating_last_valueDirtyFlag())
            srfdomain.setRating_last_value(rating_last_value);
        if(getDate_assignDirtyFlag())
            srfdomain.setDate_assign(date_assign);
        if(getIdDirtyFlag())
            srfdomain.setId(id);
        if(getMessage_idsDirtyFlag())
            srfdomain.setMessage_ids(message_ids);
        if(get__last_updateDirtyFlag())
            srfdomain.set__last_update(__last_update);
        if(getMessage_unreadDirtyFlag())
            srfdomain.setMessage_unread(message_unread);
        if(getRating_last_feedbackDirtyFlag())
            srfdomain.setRating_last_feedback(rating_last_feedback);
        if(getActivity_date_deadlineDirtyFlag())
            srfdomain.setActivity_date_deadline(activity_date_deadline);
        if(getAccess_tokenDirtyFlag())
            srfdomain.setAccess_token(access_token);
        if(getMessage_follower_idsDirtyFlag())
            srfdomain.setMessage_follower_ids(message_follower_ids);
        if(getSubtask_countDirtyFlag())
            srfdomain.setSubtask_count(subtask_count);
        if(getWorking_hours_openDirtyFlag())
            srfdomain.setWorking_hours_open(working_hours_open);
        if(getWorking_hours_closeDirtyFlag())
            srfdomain.setWorking_hours_close(working_hours_close);
        if(getDescriptionDirtyFlag())
            srfdomain.setDescription(description);
        if(getRating_last_imageDirtyFlag())
            srfdomain.setRating_last_image(rating_last_image);
        if(getWrite_dateDirtyFlag())
            srfdomain.setWrite_date(write_date);
        if(getDisplayed_image_idDirtyFlag())
            srfdomain.setDisplayed_image_id(displayed_image_id);
        if(getTag_idsDirtyFlag())
            srfdomain.setTag_ids(tag_ids);
        if(getDisplay_nameDirtyFlag())
            srfdomain.setDisplay_name(display_name);
        if(getActivity_type_idDirtyFlag())
            srfdomain.setActivity_type_id(activity_type_id);
        if(getMessage_channel_idsDirtyFlag())
            srfdomain.setMessage_channel_ids(message_channel_ids);
        if(getActivity_idsDirtyFlag())
            srfdomain.setActivity_ids(activity_ids);
        if(getKanban_stateDirtyFlag())
            srfdomain.setKanban_state(kanban_state);
        if(getSequenceDirtyFlag())
            srfdomain.setSequence(sequence);
        if(getNotesDirtyFlag())
            srfdomain.setNotes(notes);
        if(getActivity_summaryDirtyFlag())
            srfdomain.setActivity_summary(activity_summary);
        if(getMessage_is_followerDirtyFlag())
            srfdomain.setMessage_is_follower(message_is_follower);
        if(getMessage_needaction_counterDirtyFlag())
            srfdomain.setMessage_needaction_counter(message_needaction_counter);
        if(getMessage_needactionDirtyFlag())
            srfdomain.setMessage_needaction(message_needaction);
        if(getDate_last_stage_updateDirtyFlag())
            srfdomain.setDate_last_stage_update(date_last_stage_update);
        if(getMessage_has_error_counterDirtyFlag())
            srfdomain.setMessage_has_error_counter(message_has_error_counter);
        if(getMessage_attachment_countDirtyFlag())
            srfdomain.setMessage_attachment_count(message_attachment_count);
        if(getWorking_days_openDirtyFlag())
            srfdomain.setWorking_days_open(working_days_open);
        if(getActivity_user_idDirtyFlag())
            srfdomain.setActivity_user_id(activity_user_id);
        if(getWorking_days_closeDirtyFlag())
            srfdomain.setWorking_days_close(working_days_close);
        if(getActiveDirtyFlag())
            srfdomain.setActive(active);
        if(getMessage_main_attachment_idDirtyFlag())
            srfdomain.setMessage_main_attachment_id(message_main_attachment_id);
        if(getMessage_unread_counterDirtyFlag())
            srfdomain.setMessage_unread_counter(message_unread_counter);
        if(getEmail_ccDirtyFlag())
            srfdomain.setEmail_cc(email_cc);
        if(getDate_endDirtyFlag())
            srfdomain.setDate_end(date_end);
        if(getChild_idsDirtyFlag())
            srfdomain.setChild_ids(child_ids);
        if(getPlanned_hoursDirtyFlag())
            srfdomain.setPlanned_hours(planned_hours);
        if(getWebsite_message_idsDirtyFlag())
            srfdomain.setWebsite_message_ids(website_message_ids);
        if(getPriorityDirtyFlag())
            srfdomain.setPriority(priority);
        if(getMessage_partner_idsDirtyFlag())
            srfdomain.setMessage_partner_ids(message_partner_ids);
        if(getMessage_has_errorDirtyFlag())
            srfdomain.setMessage_has_error(message_has_error);
        if(getColorDirtyFlag())
            srfdomain.setColor(color);
        if(getCreate_dateDirtyFlag())
            srfdomain.setCreate_date(create_date);
        if(getAccess_urlDirtyFlag())
            srfdomain.setAccess_url(access_url);
        if(getNameDirtyFlag())
            srfdomain.setName(name);
        if(getSubtask_planned_hoursDirtyFlag())
            srfdomain.setSubtask_planned_hours(subtask_planned_hours);
        if(getLegend_doneDirtyFlag())
            srfdomain.setLegend_done(legend_done);
        if(getLegend_blockedDirtyFlag())
            srfdomain.setLegend_blocked(legend_blocked);
        if(getCompany_id_textDirtyFlag())
            srfdomain.setCompany_id_text(company_id_text);
        if(getPartner_id_textDirtyFlag())
            srfdomain.setPartner_id_text(partner_id_text);
        if(getManager_idDirtyFlag())
            srfdomain.setManager_id(manager_id);
        if(getProject_id_textDirtyFlag())
            srfdomain.setProject_id_text(project_id_text);
        if(getParent_id_textDirtyFlag())
            srfdomain.setParent_id_text(parent_id_text);
        if(getUser_id_textDirtyFlag())
            srfdomain.setUser_id_text(user_id_text);
        if(getCreate_uid_textDirtyFlag())
            srfdomain.setCreate_uid_text(create_uid_text);
        if(getUser_emailDirtyFlag())
            srfdomain.setUser_email(user_email);
        if(getSubtask_project_idDirtyFlag())
            srfdomain.setSubtask_project_id(subtask_project_id);
        if(getWrite_uid_textDirtyFlag())
            srfdomain.setWrite_uid_text(write_uid_text);
        if(getStage_id_textDirtyFlag())
            srfdomain.setStage_id_text(stage_id_text);
        if(getLegend_normalDirtyFlag())
            srfdomain.setLegend_normal(legend_normal);
        if(getCreate_uidDirtyFlag())
            srfdomain.setCreate_uid(create_uid);
        if(getUser_idDirtyFlag())
            srfdomain.setUser_id(user_id);
        if(getProject_idDirtyFlag())
            srfdomain.setProject_id(project_id);
        if(getCompany_idDirtyFlag())
            srfdomain.setCompany_id(company_id);
        if(getPartner_idDirtyFlag())
            srfdomain.setPartner_id(partner_id);
        if(getWrite_uidDirtyFlag())
            srfdomain.setWrite_uid(write_uid);
        if(getParent_idDirtyFlag())
            srfdomain.setParent_id(parent_id);
        if(getStage_idDirtyFlag())
            srfdomain.setStage_id(stage_id);

        return srfdomain;
    }

    public void fromDO(Project_task srfdomain) {
        if(srfdomain == null )
          return ;
        if(srfdomain.getDate_startDirtyFlag())
            this.setDate_start(srfdomain.getDate_start());
        if(srfdomain.getActivity_stateDirtyFlag())
            this.setActivity_state(srfdomain.getActivity_state());
        if(srfdomain.getRating_countDirtyFlag())
            this.setRating_count(srfdomain.getRating_count());
        if(srfdomain.getEmail_fromDirtyFlag())
            this.setEmail_from(srfdomain.getEmail_from());
        if(srfdomain.getAttachment_idsDirtyFlag())
            this.setAttachment_ids(srfdomain.getAttachment_ids());
        if(srfdomain.getDate_deadlineDirtyFlag())
            this.setDate_deadline(srfdomain.getDate_deadline());
        if(srfdomain.getKanban_state_labelDirtyFlag())
            this.setKanban_state_label(srfdomain.getKanban_state_label());
        if(srfdomain.getAccess_warningDirtyFlag())
            this.setAccess_warning(srfdomain.getAccess_warning());
        if(srfdomain.getRating_idsDirtyFlag())
            this.setRating_ids(srfdomain.getRating_ids());
        if(srfdomain.getRating_last_valueDirtyFlag())
            this.setRating_last_value(srfdomain.getRating_last_value());
        if(srfdomain.getDate_assignDirtyFlag())
            this.setDate_assign(srfdomain.getDate_assign());
        if(srfdomain.getIdDirtyFlag())
            this.setId(srfdomain.getId());
        if(srfdomain.getMessage_idsDirtyFlag())
            this.setMessage_ids(srfdomain.getMessage_ids());
        if(srfdomain.get__last_updateDirtyFlag())
            this.set__last_update(srfdomain.get__last_update());
        if(srfdomain.getMessage_unreadDirtyFlag())
            this.setMessage_unread(srfdomain.getMessage_unread());
        if(srfdomain.getRating_last_feedbackDirtyFlag())
            this.setRating_last_feedback(srfdomain.getRating_last_feedback());
        if(srfdomain.getActivity_date_deadlineDirtyFlag())
            this.setActivity_date_deadline(srfdomain.getActivity_date_deadline());
        if(srfdomain.getAccess_tokenDirtyFlag())
            this.setAccess_token(srfdomain.getAccess_token());
        if(srfdomain.getMessage_follower_idsDirtyFlag())
            this.setMessage_follower_ids(srfdomain.getMessage_follower_ids());
        if(srfdomain.getSubtask_countDirtyFlag())
            this.setSubtask_count(srfdomain.getSubtask_count());
        if(srfdomain.getWorking_hours_openDirtyFlag())
            this.setWorking_hours_open(srfdomain.getWorking_hours_open());
        if(srfdomain.getWorking_hours_closeDirtyFlag())
            this.setWorking_hours_close(srfdomain.getWorking_hours_close());
        if(srfdomain.getDescriptionDirtyFlag())
            this.setDescription(srfdomain.getDescription());
        if(srfdomain.getRating_last_imageDirtyFlag())
            this.setRating_last_image(srfdomain.getRating_last_image());
        if(srfdomain.getWrite_dateDirtyFlag())
            this.setWrite_date(srfdomain.getWrite_date());
        if(srfdomain.getDisplayed_image_idDirtyFlag())
            this.setDisplayed_image_id(srfdomain.getDisplayed_image_id());
        if(srfdomain.getTag_idsDirtyFlag())
            this.setTag_ids(srfdomain.getTag_ids());
        if(srfdomain.getDisplay_nameDirtyFlag())
            this.setDisplay_name(srfdomain.getDisplay_name());
        if(srfdomain.getActivity_type_idDirtyFlag())
            this.setActivity_type_id(srfdomain.getActivity_type_id());
        if(srfdomain.getMessage_channel_idsDirtyFlag())
            this.setMessage_channel_ids(srfdomain.getMessage_channel_ids());
        if(srfdomain.getActivity_idsDirtyFlag())
            this.setActivity_ids(srfdomain.getActivity_ids());
        if(srfdomain.getKanban_stateDirtyFlag())
            this.setKanban_state(srfdomain.getKanban_state());
        if(srfdomain.getSequenceDirtyFlag())
            this.setSequence(srfdomain.getSequence());
        if(srfdomain.getNotesDirtyFlag())
            this.setNotes(srfdomain.getNotes());
        if(srfdomain.getActivity_summaryDirtyFlag())
            this.setActivity_summary(srfdomain.getActivity_summary());
        if(srfdomain.getMessage_is_followerDirtyFlag())
            this.setMessage_is_follower(srfdomain.getMessage_is_follower());
        if(srfdomain.getMessage_needaction_counterDirtyFlag())
            this.setMessage_needaction_counter(srfdomain.getMessage_needaction_counter());
        if(srfdomain.getMessage_needactionDirtyFlag())
            this.setMessage_needaction(srfdomain.getMessage_needaction());
        if(srfdomain.getDate_last_stage_updateDirtyFlag())
            this.setDate_last_stage_update(srfdomain.getDate_last_stage_update());
        if(srfdomain.getMessage_has_error_counterDirtyFlag())
            this.setMessage_has_error_counter(srfdomain.getMessage_has_error_counter());
        if(srfdomain.getMessage_attachment_countDirtyFlag())
            this.setMessage_attachment_count(srfdomain.getMessage_attachment_count());
        if(srfdomain.getWorking_days_openDirtyFlag())
            this.setWorking_days_open(srfdomain.getWorking_days_open());
        if(srfdomain.getActivity_user_idDirtyFlag())
            this.setActivity_user_id(srfdomain.getActivity_user_id());
        if(srfdomain.getWorking_days_closeDirtyFlag())
            this.setWorking_days_close(srfdomain.getWorking_days_close());
        if(srfdomain.getActiveDirtyFlag())
            this.setActive(srfdomain.getActive());
        if(srfdomain.getMessage_main_attachment_idDirtyFlag())
            this.setMessage_main_attachment_id(srfdomain.getMessage_main_attachment_id());
        if(srfdomain.getMessage_unread_counterDirtyFlag())
            this.setMessage_unread_counter(srfdomain.getMessage_unread_counter());
        if(srfdomain.getEmail_ccDirtyFlag())
            this.setEmail_cc(srfdomain.getEmail_cc());
        if(srfdomain.getDate_endDirtyFlag())
            this.setDate_end(srfdomain.getDate_end());
        if(srfdomain.getChild_idsDirtyFlag())
            this.setChild_ids(srfdomain.getChild_ids());
        if(srfdomain.getPlanned_hoursDirtyFlag())
            this.setPlanned_hours(srfdomain.getPlanned_hours());
        if(srfdomain.getWebsite_message_idsDirtyFlag())
            this.setWebsite_message_ids(srfdomain.getWebsite_message_ids());
        if(srfdomain.getPriorityDirtyFlag())
            this.setPriority(srfdomain.getPriority());
        if(srfdomain.getMessage_partner_idsDirtyFlag())
            this.setMessage_partner_ids(srfdomain.getMessage_partner_ids());
        if(srfdomain.getMessage_has_errorDirtyFlag())
            this.setMessage_has_error(srfdomain.getMessage_has_error());
        if(srfdomain.getColorDirtyFlag())
            this.setColor(srfdomain.getColor());
        if(srfdomain.getCreate_dateDirtyFlag())
            this.setCreate_date(srfdomain.getCreate_date());
        if(srfdomain.getAccess_urlDirtyFlag())
            this.setAccess_url(srfdomain.getAccess_url());
        if(srfdomain.getNameDirtyFlag())
            this.setName(srfdomain.getName());
        if(srfdomain.getSubtask_planned_hoursDirtyFlag())
            this.setSubtask_planned_hours(srfdomain.getSubtask_planned_hours());
        if(srfdomain.getLegend_doneDirtyFlag())
            this.setLegend_done(srfdomain.getLegend_done());
        if(srfdomain.getLegend_blockedDirtyFlag())
            this.setLegend_blocked(srfdomain.getLegend_blocked());
        if(srfdomain.getCompany_id_textDirtyFlag())
            this.setCompany_id_text(srfdomain.getCompany_id_text());
        if(srfdomain.getPartner_id_textDirtyFlag())
            this.setPartner_id_text(srfdomain.getPartner_id_text());
        if(srfdomain.getManager_idDirtyFlag())
            this.setManager_id(srfdomain.getManager_id());
        if(srfdomain.getProject_id_textDirtyFlag())
            this.setProject_id_text(srfdomain.getProject_id_text());
        if(srfdomain.getParent_id_textDirtyFlag())
            this.setParent_id_text(srfdomain.getParent_id_text());
        if(srfdomain.getUser_id_textDirtyFlag())
            this.setUser_id_text(srfdomain.getUser_id_text());
        if(srfdomain.getCreate_uid_textDirtyFlag())
            this.setCreate_uid_text(srfdomain.getCreate_uid_text());
        if(srfdomain.getUser_emailDirtyFlag())
            this.setUser_email(srfdomain.getUser_email());
        if(srfdomain.getSubtask_project_idDirtyFlag())
            this.setSubtask_project_id(srfdomain.getSubtask_project_id());
        if(srfdomain.getWrite_uid_textDirtyFlag())
            this.setWrite_uid_text(srfdomain.getWrite_uid_text());
        if(srfdomain.getStage_id_textDirtyFlag())
            this.setStage_id_text(srfdomain.getStage_id_text());
        if(srfdomain.getLegend_normalDirtyFlag())
            this.setLegend_normal(srfdomain.getLegend_normal());
        if(srfdomain.getCreate_uidDirtyFlag())
            this.setCreate_uid(srfdomain.getCreate_uid());
        if(srfdomain.getUser_idDirtyFlag())
            this.setUser_id(srfdomain.getUser_id());
        if(srfdomain.getProject_idDirtyFlag())
            this.setProject_id(srfdomain.getProject_id());
        if(srfdomain.getCompany_idDirtyFlag())
            this.setCompany_id(srfdomain.getCompany_id());
        if(srfdomain.getPartner_idDirtyFlag())
            this.setPartner_id(srfdomain.getPartner_id());
        if(srfdomain.getWrite_uidDirtyFlag())
            this.setWrite_uid(srfdomain.getWrite_uid());
        if(srfdomain.getParent_idDirtyFlag())
            this.setParent_id(srfdomain.getParent_id());
        if(srfdomain.getStage_idDirtyFlag())
            this.setStage_id(srfdomain.getStage_id());

    }

    public List<Project_taskDTO> fromDOPage(List<Project_task> poPage)   {
        if(poPage == null)
            return null;
        List<Project_taskDTO> dtos=new ArrayList<Project_taskDTO>();
        for(Project_task domain : poPage) {
            Project_taskDTO dto = new Project_taskDTO();
            dto.fromDO(domain);
            dtos.add(dto);
        }
        return dtos;
    }
}

