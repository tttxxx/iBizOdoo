package cn.ibizlab.odoo.service.odoo_project.config;

import org.springframework.context.annotation.ComponentScan;

@ComponentScan("cn.ibizlab.odoo.service.odoo_project")
public class odoo_projectRestConfiguration {

}
