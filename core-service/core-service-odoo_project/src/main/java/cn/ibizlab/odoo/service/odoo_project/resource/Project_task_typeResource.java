package cn.ibizlab.odoo.service.odoo_project.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_project.dto.Project_task_typeDTO;
import cn.ibizlab.odoo.core.odoo_project.domain.Project_task_type;
import cn.ibizlab.odoo.core.odoo_project.service.IProject_task_typeService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_project.filter.Project_task_typeSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Project_task_type" })
@RestController
@RequestMapping("")
public class Project_task_typeResource {

    @Autowired
    private IProject_task_typeService project_task_typeService;

    public IProject_task_typeService getProject_task_typeService() {
        return this.project_task_typeService;
    }

    @ApiOperation(value = "获取数据", tags = {"Project_task_type" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_project/project_task_types/{project_task_type_id}")
    public ResponseEntity<Project_task_typeDTO> get(@PathVariable("project_task_type_id") Integer project_task_type_id) {
        Project_task_typeDTO dto = new Project_task_typeDTO();
        Project_task_type domain = project_task_typeService.get(project_task_type_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "建立数据", tags = {"Project_task_type" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_project/project_task_types")

    public ResponseEntity<Project_task_typeDTO> create(@RequestBody Project_task_typeDTO project_task_typedto) {
        Project_task_typeDTO dto = new Project_task_typeDTO();
        Project_task_type domain = project_task_typedto.toDO();
		project_task_typeService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "删除数据", tags = {"Project_task_type" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_project/project_task_types/{project_task_type_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("project_task_type_id") Integer project_task_type_id) {
        Project_task_typeDTO project_task_typedto = new Project_task_typeDTO();
		Project_task_type domain = new Project_task_type();
		project_task_typedto.setId(project_task_type_id);
		domain.setId(project_task_type_id);
        Boolean rst = project_task_typeService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "更新数据", tags = {"Project_task_type" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_project/project_task_types/{project_task_type_id}")

    public ResponseEntity<Project_task_typeDTO> update(@PathVariable("project_task_type_id") Integer project_task_type_id, @RequestBody Project_task_typeDTO project_task_typedto) {
		Project_task_type domain = project_task_typedto.toDO();
        domain.setId(project_task_type_id);
		project_task_typeService.update(domain);
		Project_task_typeDTO dto = new Project_task_typeDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批建立数据", tags = {"Project_task_type" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_project/project_task_types/createBatch")
    public ResponseEntity<Boolean> createBatchProject_task_type(@RequestBody List<Project_task_typeDTO> project_task_typedtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "批删除数据", tags = {"Project_task_type" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_project/project_task_types/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Project_task_typeDTO> project_task_typedtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "批更新数据", tags = {"Project_task_type" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_project/project_task_types/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Project_task_typeDTO> project_task_typedtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Project_task_type" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_project/project_task_types/fetchdefault")
	public ResponseEntity<Page<Project_task_typeDTO>> fetchDefault(Project_task_typeSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Project_task_typeDTO> list = new ArrayList<Project_task_typeDTO>();
        
        Page<Project_task_type> domains = project_task_typeService.searchDefault(context) ;
        for(Project_task_type project_task_type : domains.getContent()){
            Project_task_typeDTO dto = new Project_task_typeDTO();
            dto.fromDO(project_task_type);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
