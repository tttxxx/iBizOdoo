package cn.ibizlab.odoo.service.odoo_project.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.core.odoo_project.valuerule.anno.project_project.*;
import cn.ibizlab.odoo.core.odoo_project.domain.Project_project;
import org.springframework.cglib.beans.BeanCopier;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 服务DTO对象[Project_projectDTO]
 */
public class Project_projectDTO implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [ACCESS_WARNING]
     *
     */
    @Project_projectAccess_warningDefault(info = "默认规则")
    private String access_warning;

    @JsonIgnore
    private boolean access_warningDirtyFlag;

    /**
     * 属性 [MESSAGE_ATTACHMENT_COUNT]
     *
     */
    @Project_projectMessage_attachment_countDefault(info = "默认规则")
    private Integer message_attachment_count;

    @JsonIgnore
    private boolean message_attachment_countDirtyFlag;

    /**
     * 属性 [DATE]
     *
     */
    @Project_projectDateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    private Timestamp date;

    @JsonIgnore
    private boolean dateDirtyFlag;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @Project_projectDisplay_nameDefault(info = "默认规则")
    private String display_name;

    @JsonIgnore
    private boolean display_nameDirtyFlag;

    /**
     * 属性 [TASKS]
     *
     */
    @Project_projectTasksDefault(info = "默认规则")
    private String tasks;

    @JsonIgnore
    private boolean tasksDirtyFlag;

    /**
     * 属性 [CREATE_DATE]
     *
     */
    @Project_projectCreate_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp create_date;

    @JsonIgnore
    private boolean create_dateDirtyFlag;

    /**
     * 属性 [PERCENTAGE_SATISFACTION_TASK]
     *
     */
    @Project_projectPercentage_satisfaction_taskDefault(info = "默认规则")
    private Integer percentage_satisfaction_task;

    @JsonIgnore
    private boolean percentage_satisfaction_taskDirtyFlag;

    /**
     * 属性 [ACCESS_URL]
     *
     */
    @Project_projectAccess_urlDefault(info = "默认规则")
    private String access_url;

    @JsonIgnore
    private boolean access_urlDirtyFlag;

    /**
     * 属性 [TYPE_IDS]
     *
     */
    @Project_projectType_idsDefault(info = "默认规则")
    private String type_ids;

    @JsonIgnore
    private boolean type_idsDirtyFlag;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @Project_project__last_updateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp __last_update;

    @JsonIgnore
    private boolean __last_updateDirtyFlag;

    /**
     * 属性 [ACCESS_TOKEN]
     *
     */
    @Project_projectAccess_tokenDefault(info = "默认规则")
    private String access_token;

    @JsonIgnore
    private boolean access_tokenDirtyFlag;

    /**
     * 属性 [MESSAGE_IS_FOLLOWER]
     *
     */
    @Project_projectMessage_is_followerDefault(info = "默认规则")
    private String message_is_follower;

    @JsonIgnore
    private boolean message_is_followerDirtyFlag;

    /**
     * 属性 [PRIVACY_VISIBILITY]
     *
     */
    @Project_projectPrivacy_visibilityDefault(info = "默认规则")
    private String privacy_visibility;

    @JsonIgnore
    private boolean privacy_visibilityDirtyFlag;

    /**
     * 属性 [MESSAGE_FOLLOWER_IDS]
     *
     */
    @Project_projectMessage_follower_idsDefault(info = "默认规则")
    private String message_follower_ids;

    @JsonIgnore
    private boolean message_follower_idsDirtyFlag;

    /**
     * 属性 [NAME]
     *
     */
    @Project_projectNameDefault(info = "默认规则")
    private String name;

    @JsonIgnore
    private boolean nameDirtyFlag;

    /**
     * 属性 [RATING_STATUS_PERIOD]
     *
     */
    @Project_projectRating_status_periodDefault(info = "默认规则")
    private String rating_status_period;

    @JsonIgnore
    private boolean rating_status_periodDirtyFlag;

    /**
     * 属性 [ID]
     *
     */
    @Project_projectIdDefault(info = "默认规则")
    private Integer id;

    @JsonIgnore
    private boolean idDirtyFlag;

    /**
     * 属性 [PERCENTAGE_SATISFACTION_PROJECT]
     *
     */
    @Project_projectPercentage_satisfaction_projectDefault(info = "默认规则")
    private Integer percentage_satisfaction_project;

    @JsonIgnore
    private boolean percentage_satisfaction_projectDirtyFlag;

    /**
     * 属性 [DOC_COUNT]
     *
     */
    @Project_projectDoc_countDefault(info = "默认规则")
    private Integer doc_count;

    @JsonIgnore
    private boolean doc_countDirtyFlag;

    /**
     * 属性 [FAVORITE_USER_IDS]
     *
     */
    @Project_projectFavorite_user_idsDefault(info = "默认规则")
    private String favorite_user_ids;

    @JsonIgnore
    private boolean favorite_user_idsDirtyFlag;

    /**
     * 属性 [COLOR]
     *
     */
    @Project_projectColorDefault(info = "默认规则")
    private Integer color;

    @JsonIgnore
    private boolean colorDirtyFlag;

    /**
     * 属性 [WEBSITE_MESSAGE_IDS]
     *
     */
    @Project_projectWebsite_message_idsDefault(info = "默认规则")
    private String website_message_ids;

    @JsonIgnore
    private boolean website_message_idsDirtyFlag;

    /**
     * 属性 [TASK_COUNT]
     *
     */
    @Project_projectTask_countDefault(info = "默认规则")
    private Integer task_count;

    @JsonIgnore
    private boolean task_countDirtyFlag;

    /**
     * 属性 [IS_FAVORITE]
     *
     */
    @Project_projectIs_favoriteDefault(info = "默认规则")
    private String is_favorite;

    @JsonIgnore
    private boolean is_favoriteDirtyFlag;

    /**
     * 属性 [PORTAL_SHOW_RATING]
     *
     */
    @Project_projectPortal_show_ratingDefault(info = "默认规则")
    private String portal_show_rating;

    @JsonIgnore
    private boolean portal_show_ratingDirtyFlag;

    /**
     * 属性 [RATING_REQUEST_DEADLINE]
     *
     */
    @Project_projectRating_request_deadlineDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp rating_request_deadline;

    @JsonIgnore
    private boolean rating_request_deadlineDirtyFlag;

    /**
     * 属性 [MESSAGE_IDS]
     *
     */
    @Project_projectMessage_idsDefault(info = "默认规则")
    private String message_ids;

    @JsonIgnore
    private boolean message_idsDirtyFlag;

    /**
     * 属性 [MESSAGE_MAIN_ATTACHMENT_ID]
     *
     */
    @Project_projectMessage_main_attachment_idDefault(info = "默认规则")
    private Integer message_main_attachment_id;

    @JsonIgnore
    private boolean message_main_attachment_idDirtyFlag;

    /**
     * 属性 [MESSAGE_NEEDACTION_COUNTER]
     *
     */
    @Project_projectMessage_needaction_counterDefault(info = "默认规则")
    private Integer message_needaction_counter;

    @JsonIgnore
    private boolean message_needaction_counterDirtyFlag;

    /**
     * 属性 [MESSAGE_PARTNER_IDS]
     *
     */
    @Project_projectMessage_partner_idsDefault(info = "默认规则")
    private String message_partner_ids;

    @JsonIgnore
    private boolean message_partner_idsDirtyFlag;

    /**
     * 属性 [ACTIVE]
     *
     */
    @Project_projectActiveDefault(info = "默认规则")
    private String active;

    @JsonIgnore
    private boolean activeDirtyFlag;

    /**
     * 属性 [MESSAGE_CHANNEL_IDS]
     *
     */
    @Project_projectMessage_channel_idsDefault(info = "默认规则")
    private String message_channel_ids;

    @JsonIgnore
    private boolean message_channel_idsDirtyFlag;

    /**
     * 属性 [MESSAGE_NEEDACTION]
     *
     */
    @Project_projectMessage_needactionDefault(info = "默认规则")
    private String message_needaction;

    @JsonIgnore
    private boolean message_needactionDirtyFlag;

    /**
     * 属性 [MESSAGE_UNREAD_COUNTER]
     *
     */
    @Project_projectMessage_unread_counterDefault(info = "默认规则")
    private Integer message_unread_counter;

    @JsonIgnore
    private boolean message_unread_counterDirtyFlag;

    /**
     * 属性 [SEQUENCE]
     *
     */
    @Project_projectSequenceDefault(info = "默认规则")
    private Integer sequence;

    @JsonIgnore
    private boolean sequenceDirtyFlag;

    /**
     * 属性 [MESSAGE_HAS_ERROR]
     *
     */
    @Project_projectMessage_has_errorDefault(info = "默认规则")
    private String message_has_error;

    @JsonIgnore
    private boolean message_has_errorDirtyFlag;

    /**
     * 属性 [WRITE_DATE]
     *
     */
    @Project_projectWrite_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp write_date;

    @JsonIgnore
    private boolean write_dateDirtyFlag;

    /**
     * 属性 [MESSAGE_UNREAD]
     *
     */
    @Project_projectMessage_unreadDefault(info = "默认规则")
    private String message_unread;

    @JsonIgnore
    private boolean message_unreadDirtyFlag;

    /**
     * 属性 [RATING_STATUS]
     *
     */
    @Project_projectRating_statusDefault(info = "默认规则")
    private String rating_status;

    @JsonIgnore
    private boolean rating_statusDirtyFlag;

    /**
     * 属性 [LABEL_TASKS]
     *
     */
    @Project_projectLabel_tasksDefault(info = "默认规则")
    private String label_tasks;

    @JsonIgnore
    private boolean label_tasksDirtyFlag;

    /**
     * 属性 [TASK_IDS]
     *
     */
    @Project_projectTask_idsDefault(info = "默认规则")
    private String task_ids;

    @JsonIgnore
    private boolean task_idsDirtyFlag;

    /**
     * 属性 [MESSAGE_HAS_ERROR_COUNTER]
     *
     */
    @Project_projectMessage_has_error_counterDefault(info = "默认规则")
    private Integer message_has_error_counter;

    @JsonIgnore
    private boolean message_has_error_counterDirtyFlag;

    /**
     * 属性 [DATE_START]
     *
     */
    @Project_projectDate_startDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    private Timestamp date_start;

    @JsonIgnore
    private boolean date_startDirtyFlag;

    /**
     * 属性 [PARTNER_ID_TEXT]
     *
     */
    @Project_projectPartner_id_textDefault(info = "默认规则")
    private String partner_id_text;

    @JsonIgnore
    private boolean partner_id_textDirtyFlag;

    /**
     * 属性 [COMPANY_ID_TEXT]
     *
     */
    @Project_projectCompany_id_textDefault(info = "默认规则")
    private String company_id_text;

    @JsonIgnore
    private boolean company_id_textDirtyFlag;

    /**
     * 属性 [CURRENCY_ID]
     *
     */
    @Project_projectCurrency_idDefault(info = "默认规则")
    private Integer currency_id;

    @JsonIgnore
    private boolean currency_idDirtyFlag;

    /**
     * 属性 [ALIAS_PARENT_THREAD_ID]
     *
     */
    @Project_projectAlias_parent_thread_idDefault(info = "默认规则")
    private Integer alias_parent_thread_id;

    @JsonIgnore
    private boolean alias_parent_thread_idDirtyFlag;

    /**
     * 属性 [ALIAS_FORCE_THREAD_ID]
     *
     */
    @Project_projectAlias_force_thread_idDefault(info = "默认规则")
    private Integer alias_force_thread_id;

    @JsonIgnore
    private boolean alias_force_thread_idDirtyFlag;

    /**
     * 属性 [WRITE_UID_TEXT]
     *
     */
    @Project_projectWrite_uid_textDefault(info = "默认规则")
    private String write_uid_text;

    @JsonIgnore
    private boolean write_uid_textDirtyFlag;

    /**
     * 属性 [ALIAS_DOMAIN]
     *
     */
    @Project_projectAlias_domainDefault(info = "默认规则")
    private String alias_domain;

    @JsonIgnore
    private boolean alias_domainDirtyFlag;

    /**
     * 属性 [ALIAS_CONTACT]
     *
     */
    @Project_projectAlias_contactDefault(info = "默认规则")
    private String alias_contact;

    @JsonIgnore
    private boolean alias_contactDirtyFlag;

    /**
     * 属性 [ALIAS_PARENT_MODEL_ID]
     *
     */
    @Project_projectAlias_parent_model_idDefault(info = "默认规则")
    private Integer alias_parent_model_id;

    @JsonIgnore
    private boolean alias_parent_model_idDirtyFlag;

    /**
     * 属性 [ALIAS_MODEL_ID]
     *
     */
    @Project_projectAlias_model_idDefault(info = "默认规则")
    private Integer alias_model_id;

    @JsonIgnore
    private boolean alias_model_idDirtyFlag;

    /**
     * 属性 [CREATE_UID_TEXT]
     *
     */
    @Project_projectCreate_uid_textDefault(info = "默认规则")
    private String create_uid_text;

    @JsonIgnore
    private boolean create_uid_textDirtyFlag;

    /**
     * 属性 [ALIAS_USER_ID]
     *
     */
    @Project_projectAlias_user_idDefault(info = "默认规则")
    private Integer alias_user_id;

    @JsonIgnore
    private boolean alias_user_idDirtyFlag;

    /**
     * 属性 [ALIAS_DEFAULTS]
     *
     */
    @Project_projectAlias_defaultsDefault(info = "默认规则")
    private String alias_defaults;

    @JsonIgnore
    private boolean alias_defaultsDirtyFlag;

    /**
     * 属性 [RESOURCE_CALENDAR_ID_TEXT]
     *
     */
    @Project_projectResource_calendar_id_textDefault(info = "默认规则")
    private String resource_calendar_id_text;

    @JsonIgnore
    private boolean resource_calendar_id_textDirtyFlag;

    /**
     * 属性 [SUBTASK_PROJECT_ID_TEXT]
     *
     */
    @Project_projectSubtask_project_id_textDefault(info = "默认规则")
    private String subtask_project_id_text;

    @JsonIgnore
    private boolean subtask_project_id_textDirtyFlag;

    /**
     * 属性 [ALIAS_NAME]
     *
     */
    @Project_projectAlias_nameDefault(info = "默认规则")
    private String alias_name;

    @JsonIgnore
    private boolean alias_nameDirtyFlag;

    /**
     * 属性 [USER_ID_TEXT]
     *
     */
    @Project_projectUser_id_textDefault(info = "默认规则")
    private String user_id_text;

    @JsonIgnore
    private boolean user_id_textDirtyFlag;

    /**
     * 属性 [WRITE_UID]
     *
     */
    @Project_projectWrite_uidDefault(info = "默认规则")
    private Integer write_uid;

    @JsonIgnore
    private boolean write_uidDirtyFlag;

    /**
     * 属性 [USER_ID]
     *
     */
    @Project_projectUser_idDefault(info = "默认规则")
    private Integer user_id;

    @JsonIgnore
    private boolean user_idDirtyFlag;

    /**
     * 属性 [PARTNER_ID]
     *
     */
    @Project_projectPartner_idDefault(info = "默认规则")
    private Integer partner_id;

    @JsonIgnore
    private boolean partner_idDirtyFlag;

    /**
     * 属性 [COMPANY_ID]
     *
     */
    @Project_projectCompany_idDefault(info = "默认规则")
    private Integer company_id;

    @JsonIgnore
    private boolean company_idDirtyFlag;

    /**
     * 属性 [SUBTASK_PROJECT_ID]
     *
     */
    @Project_projectSubtask_project_idDefault(info = "默认规则")
    private Integer subtask_project_id;

    @JsonIgnore
    private boolean subtask_project_idDirtyFlag;

    /**
     * 属性 [CREATE_UID]
     *
     */
    @Project_projectCreate_uidDefault(info = "默认规则")
    private Integer create_uid;

    @JsonIgnore
    private boolean create_uidDirtyFlag;

    /**
     * 属性 [RESOURCE_CALENDAR_ID]
     *
     */
    @Project_projectResource_calendar_idDefault(info = "默认规则")
    private Integer resource_calendar_id;

    @JsonIgnore
    private boolean resource_calendar_idDirtyFlag;

    /**
     * 属性 [ALIAS_ID]
     *
     */
    @Project_projectAlias_idDefault(info = "默认规则")
    private Integer alias_id;

    @JsonIgnore
    private boolean alias_idDirtyFlag;


    /**
     * 获取 [ACCESS_WARNING]
     */
    @JsonProperty("access_warning")
    public String getAccess_warning(){
        return access_warning ;
    }

    /**
     * 设置 [ACCESS_WARNING]
     */
    @JsonProperty("access_warning")
    public void setAccess_warning(String  access_warning){
        this.access_warning = access_warning ;
        this.access_warningDirtyFlag = true ;
    }

    /**
     * 获取 [ACCESS_WARNING]脏标记
     */
    @JsonIgnore
    public boolean getAccess_warningDirtyFlag(){
        return access_warningDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_ATTACHMENT_COUNT]
     */
    @JsonProperty("message_attachment_count")
    public Integer getMessage_attachment_count(){
        return message_attachment_count ;
    }

    /**
     * 设置 [MESSAGE_ATTACHMENT_COUNT]
     */
    @JsonProperty("message_attachment_count")
    public void setMessage_attachment_count(Integer  message_attachment_count){
        this.message_attachment_count = message_attachment_count ;
        this.message_attachment_countDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_ATTACHMENT_COUNT]脏标记
     */
    @JsonIgnore
    public boolean getMessage_attachment_countDirtyFlag(){
        return message_attachment_countDirtyFlag ;
    }

    /**
     * 获取 [DATE]
     */
    @JsonProperty("date")
    public Timestamp getDate(){
        return date ;
    }

    /**
     * 设置 [DATE]
     */
    @JsonProperty("date")
    public void setDate(Timestamp  date){
        this.date = date ;
        this.dateDirtyFlag = true ;
    }

    /**
     * 获取 [DATE]脏标记
     */
    @JsonIgnore
    public boolean getDateDirtyFlag(){
        return dateDirtyFlag ;
    }

    /**
     * 获取 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return display_name ;
    }

    /**
     * 设置 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

    /**
     * 获取 [DISPLAY_NAME]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return display_nameDirtyFlag ;
    }

    /**
     * 获取 [TASKS]
     */
    @JsonProperty("tasks")
    public String getTasks(){
        return tasks ;
    }

    /**
     * 设置 [TASKS]
     */
    @JsonProperty("tasks")
    public void setTasks(String  tasks){
        this.tasks = tasks ;
        this.tasksDirtyFlag = true ;
    }

    /**
     * 获取 [TASKS]脏标记
     */
    @JsonIgnore
    public boolean getTasksDirtyFlag(){
        return tasksDirtyFlag ;
    }

    /**
     * 获取 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return create_date ;
    }

    /**
     * 设置 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return create_dateDirtyFlag ;
    }

    /**
     * 获取 [PERCENTAGE_SATISFACTION_TASK]
     */
    @JsonProperty("percentage_satisfaction_task")
    public Integer getPercentage_satisfaction_task(){
        return percentage_satisfaction_task ;
    }

    /**
     * 设置 [PERCENTAGE_SATISFACTION_TASK]
     */
    @JsonProperty("percentage_satisfaction_task")
    public void setPercentage_satisfaction_task(Integer  percentage_satisfaction_task){
        this.percentage_satisfaction_task = percentage_satisfaction_task ;
        this.percentage_satisfaction_taskDirtyFlag = true ;
    }

    /**
     * 获取 [PERCENTAGE_SATISFACTION_TASK]脏标记
     */
    @JsonIgnore
    public boolean getPercentage_satisfaction_taskDirtyFlag(){
        return percentage_satisfaction_taskDirtyFlag ;
    }

    /**
     * 获取 [ACCESS_URL]
     */
    @JsonProperty("access_url")
    public String getAccess_url(){
        return access_url ;
    }

    /**
     * 设置 [ACCESS_URL]
     */
    @JsonProperty("access_url")
    public void setAccess_url(String  access_url){
        this.access_url = access_url ;
        this.access_urlDirtyFlag = true ;
    }

    /**
     * 获取 [ACCESS_URL]脏标记
     */
    @JsonIgnore
    public boolean getAccess_urlDirtyFlag(){
        return access_urlDirtyFlag ;
    }

    /**
     * 获取 [TYPE_IDS]
     */
    @JsonProperty("type_ids")
    public String getType_ids(){
        return type_ids ;
    }

    /**
     * 设置 [TYPE_IDS]
     */
    @JsonProperty("type_ids")
    public void setType_ids(String  type_ids){
        this.type_ids = type_ids ;
        this.type_idsDirtyFlag = true ;
    }

    /**
     * 获取 [TYPE_IDS]脏标记
     */
    @JsonIgnore
    public boolean getType_idsDirtyFlag(){
        return type_idsDirtyFlag ;
    }

    /**
     * 获取 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return __last_update ;
    }

    /**
     * 设置 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

    /**
     * 获取 [__LAST_UPDATE]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return __last_updateDirtyFlag ;
    }

    /**
     * 获取 [ACCESS_TOKEN]
     */
    @JsonProperty("access_token")
    public String getAccess_token(){
        return access_token ;
    }

    /**
     * 设置 [ACCESS_TOKEN]
     */
    @JsonProperty("access_token")
    public void setAccess_token(String  access_token){
        this.access_token = access_token ;
        this.access_tokenDirtyFlag = true ;
    }

    /**
     * 获取 [ACCESS_TOKEN]脏标记
     */
    @JsonIgnore
    public boolean getAccess_tokenDirtyFlag(){
        return access_tokenDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_IS_FOLLOWER]
     */
    @JsonProperty("message_is_follower")
    public String getMessage_is_follower(){
        return message_is_follower ;
    }

    /**
     * 设置 [MESSAGE_IS_FOLLOWER]
     */
    @JsonProperty("message_is_follower")
    public void setMessage_is_follower(String  message_is_follower){
        this.message_is_follower = message_is_follower ;
        this.message_is_followerDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_IS_FOLLOWER]脏标记
     */
    @JsonIgnore
    public boolean getMessage_is_followerDirtyFlag(){
        return message_is_followerDirtyFlag ;
    }

    /**
     * 获取 [PRIVACY_VISIBILITY]
     */
    @JsonProperty("privacy_visibility")
    public String getPrivacy_visibility(){
        return privacy_visibility ;
    }

    /**
     * 设置 [PRIVACY_VISIBILITY]
     */
    @JsonProperty("privacy_visibility")
    public void setPrivacy_visibility(String  privacy_visibility){
        this.privacy_visibility = privacy_visibility ;
        this.privacy_visibilityDirtyFlag = true ;
    }

    /**
     * 获取 [PRIVACY_VISIBILITY]脏标记
     */
    @JsonIgnore
    public boolean getPrivacy_visibilityDirtyFlag(){
        return privacy_visibilityDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_FOLLOWER_IDS]
     */
    @JsonProperty("message_follower_ids")
    public String getMessage_follower_ids(){
        return message_follower_ids ;
    }

    /**
     * 设置 [MESSAGE_FOLLOWER_IDS]
     */
    @JsonProperty("message_follower_ids")
    public void setMessage_follower_ids(String  message_follower_ids){
        this.message_follower_ids = message_follower_ids ;
        this.message_follower_idsDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_FOLLOWER_IDS]脏标记
     */
    @JsonIgnore
    public boolean getMessage_follower_idsDirtyFlag(){
        return message_follower_idsDirtyFlag ;
    }

    /**
     * 获取 [NAME]
     */
    @JsonProperty("name")
    public String getName(){
        return name ;
    }

    /**
     * 设置 [NAME]
     */
    @JsonProperty("name")
    public void setName(String  name){
        this.name = name ;
        this.nameDirtyFlag = true ;
    }

    /**
     * 获取 [NAME]脏标记
     */
    @JsonIgnore
    public boolean getNameDirtyFlag(){
        return nameDirtyFlag ;
    }

    /**
     * 获取 [RATING_STATUS_PERIOD]
     */
    @JsonProperty("rating_status_period")
    public String getRating_status_period(){
        return rating_status_period ;
    }

    /**
     * 设置 [RATING_STATUS_PERIOD]
     */
    @JsonProperty("rating_status_period")
    public void setRating_status_period(String  rating_status_period){
        this.rating_status_period = rating_status_period ;
        this.rating_status_periodDirtyFlag = true ;
    }

    /**
     * 获取 [RATING_STATUS_PERIOD]脏标记
     */
    @JsonIgnore
    public boolean getRating_status_periodDirtyFlag(){
        return rating_status_periodDirtyFlag ;
    }

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

    /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return idDirtyFlag ;
    }

    /**
     * 获取 [PERCENTAGE_SATISFACTION_PROJECT]
     */
    @JsonProperty("percentage_satisfaction_project")
    public Integer getPercentage_satisfaction_project(){
        return percentage_satisfaction_project ;
    }

    /**
     * 设置 [PERCENTAGE_SATISFACTION_PROJECT]
     */
    @JsonProperty("percentage_satisfaction_project")
    public void setPercentage_satisfaction_project(Integer  percentage_satisfaction_project){
        this.percentage_satisfaction_project = percentage_satisfaction_project ;
        this.percentage_satisfaction_projectDirtyFlag = true ;
    }

    /**
     * 获取 [PERCENTAGE_SATISFACTION_PROJECT]脏标记
     */
    @JsonIgnore
    public boolean getPercentage_satisfaction_projectDirtyFlag(){
        return percentage_satisfaction_projectDirtyFlag ;
    }

    /**
     * 获取 [DOC_COUNT]
     */
    @JsonProperty("doc_count")
    public Integer getDoc_count(){
        return doc_count ;
    }

    /**
     * 设置 [DOC_COUNT]
     */
    @JsonProperty("doc_count")
    public void setDoc_count(Integer  doc_count){
        this.doc_count = doc_count ;
        this.doc_countDirtyFlag = true ;
    }

    /**
     * 获取 [DOC_COUNT]脏标记
     */
    @JsonIgnore
    public boolean getDoc_countDirtyFlag(){
        return doc_countDirtyFlag ;
    }

    /**
     * 获取 [FAVORITE_USER_IDS]
     */
    @JsonProperty("favorite_user_ids")
    public String getFavorite_user_ids(){
        return favorite_user_ids ;
    }

    /**
     * 设置 [FAVORITE_USER_IDS]
     */
    @JsonProperty("favorite_user_ids")
    public void setFavorite_user_ids(String  favorite_user_ids){
        this.favorite_user_ids = favorite_user_ids ;
        this.favorite_user_idsDirtyFlag = true ;
    }

    /**
     * 获取 [FAVORITE_USER_IDS]脏标记
     */
    @JsonIgnore
    public boolean getFavorite_user_idsDirtyFlag(){
        return favorite_user_idsDirtyFlag ;
    }

    /**
     * 获取 [COLOR]
     */
    @JsonProperty("color")
    public Integer getColor(){
        return color ;
    }

    /**
     * 设置 [COLOR]
     */
    @JsonProperty("color")
    public void setColor(Integer  color){
        this.color = color ;
        this.colorDirtyFlag = true ;
    }

    /**
     * 获取 [COLOR]脏标记
     */
    @JsonIgnore
    public boolean getColorDirtyFlag(){
        return colorDirtyFlag ;
    }

    /**
     * 获取 [WEBSITE_MESSAGE_IDS]
     */
    @JsonProperty("website_message_ids")
    public String getWebsite_message_ids(){
        return website_message_ids ;
    }

    /**
     * 设置 [WEBSITE_MESSAGE_IDS]
     */
    @JsonProperty("website_message_ids")
    public void setWebsite_message_ids(String  website_message_ids){
        this.website_message_ids = website_message_ids ;
        this.website_message_idsDirtyFlag = true ;
    }

    /**
     * 获取 [WEBSITE_MESSAGE_IDS]脏标记
     */
    @JsonIgnore
    public boolean getWebsite_message_idsDirtyFlag(){
        return website_message_idsDirtyFlag ;
    }

    /**
     * 获取 [TASK_COUNT]
     */
    @JsonProperty("task_count")
    public Integer getTask_count(){
        return task_count ;
    }

    /**
     * 设置 [TASK_COUNT]
     */
    @JsonProperty("task_count")
    public void setTask_count(Integer  task_count){
        this.task_count = task_count ;
        this.task_countDirtyFlag = true ;
    }

    /**
     * 获取 [TASK_COUNT]脏标记
     */
    @JsonIgnore
    public boolean getTask_countDirtyFlag(){
        return task_countDirtyFlag ;
    }

    /**
     * 获取 [IS_FAVORITE]
     */
    @JsonProperty("is_favorite")
    public String getIs_favorite(){
        return is_favorite ;
    }

    /**
     * 设置 [IS_FAVORITE]
     */
    @JsonProperty("is_favorite")
    public void setIs_favorite(String  is_favorite){
        this.is_favorite = is_favorite ;
        this.is_favoriteDirtyFlag = true ;
    }

    /**
     * 获取 [IS_FAVORITE]脏标记
     */
    @JsonIgnore
    public boolean getIs_favoriteDirtyFlag(){
        return is_favoriteDirtyFlag ;
    }

    /**
     * 获取 [PORTAL_SHOW_RATING]
     */
    @JsonProperty("portal_show_rating")
    public String getPortal_show_rating(){
        return portal_show_rating ;
    }

    /**
     * 设置 [PORTAL_SHOW_RATING]
     */
    @JsonProperty("portal_show_rating")
    public void setPortal_show_rating(String  portal_show_rating){
        this.portal_show_rating = portal_show_rating ;
        this.portal_show_ratingDirtyFlag = true ;
    }

    /**
     * 获取 [PORTAL_SHOW_RATING]脏标记
     */
    @JsonIgnore
    public boolean getPortal_show_ratingDirtyFlag(){
        return portal_show_ratingDirtyFlag ;
    }

    /**
     * 获取 [RATING_REQUEST_DEADLINE]
     */
    @JsonProperty("rating_request_deadline")
    public Timestamp getRating_request_deadline(){
        return rating_request_deadline ;
    }

    /**
     * 设置 [RATING_REQUEST_DEADLINE]
     */
    @JsonProperty("rating_request_deadline")
    public void setRating_request_deadline(Timestamp  rating_request_deadline){
        this.rating_request_deadline = rating_request_deadline ;
        this.rating_request_deadlineDirtyFlag = true ;
    }

    /**
     * 获取 [RATING_REQUEST_DEADLINE]脏标记
     */
    @JsonIgnore
    public boolean getRating_request_deadlineDirtyFlag(){
        return rating_request_deadlineDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_IDS]
     */
    @JsonProperty("message_ids")
    public String getMessage_ids(){
        return message_ids ;
    }

    /**
     * 设置 [MESSAGE_IDS]
     */
    @JsonProperty("message_ids")
    public void setMessage_ids(String  message_ids){
        this.message_ids = message_ids ;
        this.message_idsDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_IDS]脏标记
     */
    @JsonIgnore
    public boolean getMessage_idsDirtyFlag(){
        return message_idsDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_MAIN_ATTACHMENT_ID]
     */
    @JsonProperty("message_main_attachment_id")
    public Integer getMessage_main_attachment_id(){
        return message_main_attachment_id ;
    }

    /**
     * 设置 [MESSAGE_MAIN_ATTACHMENT_ID]
     */
    @JsonProperty("message_main_attachment_id")
    public void setMessage_main_attachment_id(Integer  message_main_attachment_id){
        this.message_main_attachment_id = message_main_attachment_id ;
        this.message_main_attachment_idDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_MAIN_ATTACHMENT_ID]脏标记
     */
    @JsonIgnore
    public boolean getMessage_main_attachment_idDirtyFlag(){
        return message_main_attachment_idDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_NEEDACTION_COUNTER]
     */
    @JsonProperty("message_needaction_counter")
    public Integer getMessage_needaction_counter(){
        return message_needaction_counter ;
    }

    /**
     * 设置 [MESSAGE_NEEDACTION_COUNTER]
     */
    @JsonProperty("message_needaction_counter")
    public void setMessage_needaction_counter(Integer  message_needaction_counter){
        this.message_needaction_counter = message_needaction_counter ;
        this.message_needaction_counterDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_NEEDACTION_COUNTER]脏标记
     */
    @JsonIgnore
    public boolean getMessage_needaction_counterDirtyFlag(){
        return message_needaction_counterDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_PARTNER_IDS]
     */
    @JsonProperty("message_partner_ids")
    public String getMessage_partner_ids(){
        return message_partner_ids ;
    }

    /**
     * 设置 [MESSAGE_PARTNER_IDS]
     */
    @JsonProperty("message_partner_ids")
    public void setMessage_partner_ids(String  message_partner_ids){
        this.message_partner_ids = message_partner_ids ;
        this.message_partner_idsDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_PARTNER_IDS]脏标记
     */
    @JsonIgnore
    public boolean getMessage_partner_idsDirtyFlag(){
        return message_partner_idsDirtyFlag ;
    }

    /**
     * 获取 [ACTIVE]
     */
    @JsonProperty("active")
    public String getActive(){
        return active ;
    }

    /**
     * 设置 [ACTIVE]
     */
    @JsonProperty("active")
    public void setActive(String  active){
        this.active = active ;
        this.activeDirtyFlag = true ;
    }

    /**
     * 获取 [ACTIVE]脏标记
     */
    @JsonIgnore
    public boolean getActiveDirtyFlag(){
        return activeDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_CHANNEL_IDS]
     */
    @JsonProperty("message_channel_ids")
    public String getMessage_channel_ids(){
        return message_channel_ids ;
    }

    /**
     * 设置 [MESSAGE_CHANNEL_IDS]
     */
    @JsonProperty("message_channel_ids")
    public void setMessage_channel_ids(String  message_channel_ids){
        this.message_channel_ids = message_channel_ids ;
        this.message_channel_idsDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_CHANNEL_IDS]脏标记
     */
    @JsonIgnore
    public boolean getMessage_channel_idsDirtyFlag(){
        return message_channel_idsDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_NEEDACTION]
     */
    @JsonProperty("message_needaction")
    public String getMessage_needaction(){
        return message_needaction ;
    }

    /**
     * 设置 [MESSAGE_NEEDACTION]
     */
    @JsonProperty("message_needaction")
    public void setMessage_needaction(String  message_needaction){
        this.message_needaction = message_needaction ;
        this.message_needactionDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_NEEDACTION]脏标记
     */
    @JsonIgnore
    public boolean getMessage_needactionDirtyFlag(){
        return message_needactionDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_UNREAD_COUNTER]
     */
    @JsonProperty("message_unread_counter")
    public Integer getMessage_unread_counter(){
        return message_unread_counter ;
    }

    /**
     * 设置 [MESSAGE_UNREAD_COUNTER]
     */
    @JsonProperty("message_unread_counter")
    public void setMessage_unread_counter(Integer  message_unread_counter){
        this.message_unread_counter = message_unread_counter ;
        this.message_unread_counterDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_UNREAD_COUNTER]脏标记
     */
    @JsonIgnore
    public boolean getMessage_unread_counterDirtyFlag(){
        return message_unread_counterDirtyFlag ;
    }

    /**
     * 获取 [SEQUENCE]
     */
    @JsonProperty("sequence")
    public Integer getSequence(){
        return sequence ;
    }

    /**
     * 设置 [SEQUENCE]
     */
    @JsonProperty("sequence")
    public void setSequence(Integer  sequence){
        this.sequence = sequence ;
        this.sequenceDirtyFlag = true ;
    }

    /**
     * 获取 [SEQUENCE]脏标记
     */
    @JsonIgnore
    public boolean getSequenceDirtyFlag(){
        return sequenceDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_HAS_ERROR]
     */
    @JsonProperty("message_has_error")
    public String getMessage_has_error(){
        return message_has_error ;
    }

    /**
     * 设置 [MESSAGE_HAS_ERROR]
     */
    @JsonProperty("message_has_error")
    public void setMessage_has_error(String  message_has_error){
        this.message_has_error = message_has_error ;
        this.message_has_errorDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_HAS_ERROR]脏标记
     */
    @JsonIgnore
    public boolean getMessage_has_errorDirtyFlag(){
        return message_has_errorDirtyFlag ;
    }

    /**
     * 获取 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return write_date ;
    }

    /**
     * 设置 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return write_dateDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_UNREAD]
     */
    @JsonProperty("message_unread")
    public String getMessage_unread(){
        return message_unread ;
    }

    /**
     * 设置 [MESSAGE_UNREAD]
     */
    @JsonProperty("message_unread")
    public void setMessage_unread(String  message_unread){
        this.message_unread = message_unread ;
        this.message_unreadDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_UNREAD]脏标记
     */
    @JsonIgnore
    public boolean getMessage_unreadDirtyFlag(){
        return message_unreadDirtyFlag ;
    }

    /**
     * 获取 [RATING_STATUS]
     */
    @JsonProperty("rating_status")
    public String getRating_status(){
        return rating_status ;
    }

    /**
     * 设置 [RATING_STATUS]
     */
    @JsonProperty("rating_status")
    public void setRating_status(String  rating_status){
        this.rating_status = rating_status ;
        this.rating_statusDirtyFlag = true ;
    }

    /**
     * 获取 [RATING_STATUS]脏标记
     */
    @JsonIgnore
    public boolean getRating_statusDirtyFlag(){
        return rating_statusDirtyFlag ;
    }

    /**
     * 获取 [LABEL_TASKS]
     */
    @JsonProperty("label_tasks")
    public String getLabel_tasks(){
        return label_tasks ;
    }

    /**
     * 设置 [LABEL_TASKS]
     */
    @JsonProperty("label_tasks")
    public void setLabel_tasks(String  label_tasks){
        this.label_tasks = label_tasks ;
        this.label_tasksDirtyFlag = true ;
    }

    /**
     * 获取 [LABEL_TASKS]脏标记
     */
    @JsonIgnore
    public boolean getLabel_tasksDirtyFlag(){
        return label_tasksDirtyFlag ;
    }

    /**
     * 获取 [TASK_IDS]
     */
    @JsonProperty("task_ids")
    public String getTask_ids(){
        return task_ids ;
    }

    /**
     * 设置 [TASK_IDS]
     */
    @JsonProperty("task_ids")
    public void setTask_ids(String  task_ids){
        this.task_ids = task_ids ;
        this.task_idsDirtyFlag = true ;
    }

    /**
     * 获取 [TASK_IDS]脏标记
     */
    @JsonIgnore
    public boolean getTask_idsDirtyFlag(){
        return task_idsDirtyFlag ;
    }

    /**
     * 获取 [MESSAGE_HAS_ERROR_COUNTER]
     */
    @JsonProperty("message_has_error_counter")
    public Integer getMessage_has_error_counter(){
        return message_has_error_counter ;
    }

    /**
     * 设置 [MESSAGE_HAS_ERROR_COUNTER]
     */
    @JsonProperty("message_has_error_counter")
    public void setMessage_has_error_counter(Integer  message_has_error_counter){
        this.message_has_error_counter = message_has_error_counter ;
        this.message_has_error_counterDirtyFlag = true ;
    }

    /**
     * 获取 [MESSAGE_HAS_ERROR_COUNTER]脏标记
     */
    @JsonIgnore
    public boolean getMessage_has_error_counterDirtyFlag(){
        return message_has_error_counterDirtyFlag ;
    }

    /**
     * 获取 [DATE_START]
     */
    @JsonProperty("date_start")
    public Timestamp getDate_start(){
        return date_start ;
    }

    /**
     * 设置 [DATE_START]
     */
    @JsonProperty("date_start")
    public void setDate_start(Timestamp  date_start){
        this.date_start = date_start ;
        this.date_startDirtyFlag = true ;
    }

    /**
     * 获取 [DATE_START]脏标记
     */
    @JsonIgnore
    public boolean getDate_startDirtyFlag(){
        return date_startDirtyFlag ;
    }

    /**
     * 获取 [PARTNER_ID_TEXT]
     */
    @JsonProperty("partner_id_text")
    public String getPartner_id_text(){
        return partner_id_text ;
    }

    /**
     * 设置 [PARTNER_ID_TEXT]
     */
    @JsonProperty("partner_id_text")
    public void setPartner_id_text(String  partner_id_text){
        this.partner_id_text = partner_id_text ;
        this.partner_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [PARTNER_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getPartner_id_textDirtyFlag(){
        return partner_id_textDirtyFlag ;
    }

    /**
     * 获取 [COMPANY_ID_TEXT]
     */
    @JsonProperty("company_id_text")
    public String getCompany_id_text(){
        return company_id_text ;
    }

    /**
     * 设置 [COMPANY_ID_TEXT]
     */
    @JsonProperty("company_id_text")
    public void setCompany_id_text(String  company_id_text){
        this.company_id_text = company_id_text ;
        this.company_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [COMPANY_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getCompany_id_textDirtyFlag(){
        return company_id_textDirtyFlag ;
    }

    /**
     * 获取 [CURRENCY_ID]
     */
    @JsonProperty("currency_id")
    public Integer getCurrency_id(){
        return currency_id ;
    }

    /**
     * 设置 [CURRENCY_ID]
     */
    @JsonProperty("currency_id")
    public void setCurrency_id(Integer  currency_id){
        this.currency_id = currency_id ;
        this.currency_idDirtyFlag = true ;
    }

    /**
     * 获取 [CURRENCY_ID]脏标记
     */
    @JsonIgnore
    public boolean getCurrency_idDirtyFlag(){
        return currency_idDirtyFlag ;
    }

    /**
     * 获取 [ALIAS_PARENT_THREAD_ID]
     */
    @JsonProperty("alias_parent_thread_id")
    public Integer getAlias_parent_thread_id(){
        return alias_parent_thread_id ;
    }

    /**
     * 设置 [ALIAS_PARENT_THREAD_ID]
     */
    @JsonProperty("alias_parent_thread_id")
    public void setAlias_parent_thread_id(Integer  alias_parent_thread_id){
        this.alias_parent_thread_id = alias_parent_thread_id ;
        this.alias_parent_thread_idDirtyFlag = true ;
    }

    /**
     * 获取 [ALIAS_PARENT_THREAD_ID]脏标记
     */
    @JsonIgnore
    public boolean getAlias_parent_thread_idDirtyFlag(){
        return alias_parent_thread_idDirtyFlag ;
    }

    /**
     * 获取 [ALIAS_FORCE_THREAD_ID]
     */
    @JsonProperty("alias_force_thread_id")
    public Integer getAlias_force_thread_id(){
        return alias_force_thread_id ;
    }

    /**
     * 设置 [ALIAS_FORCE_THREAD_ID]
     */
    @JsonProperty("alias_force_thread_id")
    public void setAlias_force_thread_id(Integer  alias_force_thread_id){
        this.alias_force_thread_id = alias_force_thread_id ;
        this.alias_force_thread_idDirtyFlag = true ;
    }

    /**
     * 获取 [ALIAS_FORCE_THREAD_ID]脏标记
     */
    @JsonIgnore
    public boolean getAlias_force_thread_idDirtyFlag(){
        return alias_force_thread_idDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return write_uid_text ;
    }

    /**
     * 设置 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return write_uid_textDirtyFlag ;
    }

    /**
     * 获取 [ALIAS_DOMAIN]
     */
    @JsonProperty("alias_domain")
    public String getAlias_domain(){
        return alias_domain ;
    }

    /**
     * 设置 [ALIAS_DOMAIN]
     */
    @JsonProperty("alias_domain")
    public void setAlias_domain(String  alias_domain){
        this.alias_domain = alias_domain ;
        this.alias_domainDirtyFlag = true ;
    }

    /**
     * 获取 [ALIAS_DOMAIN]脏标记
     */
    @JsonIgnore
    public boolean getAlias_domainDirtyFlag(){
        return alias_domainDirtyFlag ;
    }

    /**
     * 获取 [ALIAS_CONTACT]
     */
    @JsonProperty("alias_contact")
    public String getAlias_contact(){
        return alias_contact ;
    }

    /**
     * 设置 [ALIAS_CONTACT]
     */
    @JsonProperty("alias_contact")
    public void setAlias_contact(String  alias_contact){
        this.alias_contact = alias_contact ;
        this.alias_contactDirtyFlag = true ;
    }

    /**
     * 获取 [ALIAS_CONTACT]脏标记
     */
    @JsonIgnore
    public boolean getAlias_contactDirtyFlag(){
        return alias_contactDirtyFlag ;
    }

    /**
     * 获取 [ALIAS_PARENT_MODEL_ID]
     */
    @JsonProperty("alias_parent_model_id")
    public Integer getAlias_parent_model_id(){
        return alias_parent_model_id ;
    }

    /**
     * 设置 [ALIAS_PARENT_MODEL_ID]
     */
    @JsonProperty("alias_parent_model_id")
    public void setAlias_parent_model_id(Integer  alias_parent_model_id){
        this.alias_parent_model_id = alias_parent_model_id ;
        this.alias_parent_model_idDirtyFlag = true ;
    }

    /**
     * 获取 [ALIAS_PARENT_MODEL_ID]脏标记
     */
    @JsonIgnore
    public boolean getAlias_parent_model_idDirtyFlag(){
        return alias_parent_model_idDirtyFlag ;
    }

    /**
     * 获取 [ALIAS_MODEL_ID]
     */
    @JsonProperty("alias_model_id")
    public Integer getAlias_model_id(){
        return alias_model_id ;
    }

    /**
     * 设置 [ALIAS_MODEL_ID]
     */
    @JsonProperty("alias_model_id")
    public void setAlias_model_id(Integer  alias_model_id){
        this.alias_model_id = alias_model_id ;
        this.alias_model_idDirtyFlag = true ;
    }

    /**
     * 获取 [ALIAS_MODEL_ID]脏标记
     */
    @JsonIgnore
    public boolean getAlias_model_idDirtyFlag(){
        return alias_model_idDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return create_uid_text ;
    }

    /**
     * 设置 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return create_uid_textDirtyFlag ;
    }

    /**
     * 获取 [ALIAS_USER_ID]
     */
    @JsonProperty("alias_user_id")
    public Integer getAlias_user_id(){
        return alias_user_id ;
    }

    /**
     * 设置 [ALIAS_USER_ID]
     */
    @JsonProperty("alias_user_id")
    public void setAlias_user_id(Integer  alias_user_id){
        this.alias_user_id = alias_user_id ;
        this.alias_user_idDirtyFlag = true ;
    }

    /**
     * 获取 [ALIAS_USER_ID]脏标记
     */
    @JsonIgnore
    public boolean getAlias_user_idDirtyFlag(){
        return alias_user_idDirtyFlag ;
    }

    /**
     * 获取 [ALIAS_DEFAULTS]
     */
    @JsonProperty("alias_defaults")
    public String getAlias_defaults(){
        return alias_defaults ;
    }

    /**
     * 设置 [ALIAS_DEFAULTS]
     */
    @JsonProperty("alias_defaults")
    public void setAlias_defaults(String  alias_defaults){
        this.alias_defaults = alias_defaults ;
        this.alias_defaultsDirtyFlag = true ;
    }

    /**
     * 获取 [ALIAS_DEFAULTS]脏标记
     */
    @JsonIgnore
    public boolean getAlias_defaultsDirtyFlag(){
        return alias_defaultsDirtyFlag ;
    }

    /**
     * 获取 [RESOURCE_CALENDAR_ID_TEXT]
     */
    @JsonProperty("resource_calendar_id_text")
    public String getResource_calendar_id_text(){
        return resource_calendar_id_text ;
    }

    /**
     * 设置 [RESOURCE_CALENDAR_ID_TEXT]
     */
    @JsonProperty("resource_calendar_id_text")
    public void setResource_calendar_id_text(String  resource_calendar_id_text){
        this.resource_calendar_id_text = resource_calendar_id_text ;
        this.resource_calendar_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [RESOURCE_CALENDAR_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getResource_calendar_id_textDirtyFlag(){
        return resource_calendar_id_textDirtyFlag ;
    }

    /**
     * 获取 [SUBTASK_PROJECT_ID_TEXT]
     */
    @JsonProperty("subtask_project_id_text")
    public String getSubtask_project_id_text(){
        return subtask_project_id_text ;
    }

    /**
     * 设置 [SUBTASK_PROJECT_ID_TEXT]
     */
    @JsonProperty("subtask_project_id_text")
    public void setSubtask_project_id_text(String  subtask_project_id_text){
        this.subtask_project_id_text = subtask_project_id_text ;
        this.subtask_project_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [SUBTASK_PROJECT_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getSubtask_project_id_textDirtyFlag(){
        return subtask_project_id_textDirtyFlag ;
    }

    /**
     * 获取 [ALIAS_NAME]
     */
    @JsonProperty("alias_name")
    public String getAlias_name(){
        return alias_name ;
    }

    /**
     * 设置 [ALIAS_NAME]
     */
    @JsonProperty("alias_name")
    public void setAlias_name(String  alias_name){
        this.alias_name = alias_name ;
        this.alias_nameDirtyFlag = true ;
    }

    /**
     * 获取 [ALIAS_NAME]脏标记
     */
    @JsonIgnore
    public boolean getAlias_nameDirtyFlag(){
        return alias_nameDirtyFlag ;
    }

    /**
     * 获取 [USER_ID_TEXT]
     */
    @JsonProperty("user_id_text")
    public String getUser_id_text(){
        return user_id_text ;
    }

    /**
     * 设置 [USER_ID_TEXT]
     */
    @JsonProperty("user_id_text")
    public void setUser_id_text(String  user_id_text){
        this.user_id_text = user_id_text ;
        this.user_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [USER_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getUser_id_textDirtyFlag(){
        return user_id_textDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return write_uid ;
    }

    /**
     * 设置 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return write_uidDirtyFlag ;
    }

    /**
     * 获取 [USER_ID]
     */
    @JsonProperty("user_id")
    public Integer getUser_id(){
        return user_id ;
    }

    /**
     * 设置 [USER_ID]
     */
    @JsonProperty("user_id")
    public void setUser_id(Integer  user_id){
        this.user_id = user_id ;
        this.user_idDirtyFlag = true ;
    }

    /**
     * 获取 [USER_ID]脏标记
     */
    @JsonIgnore
    public boolean getUser_idDirtyFlag(){
        return user_idDirtyFlag ;
    }

    /**
     * 获取 [PARTNER_ID]
     */
    @JsonProperty("partner_id")
    public Integer getPartner_id(){
        return partner_id ;
    }

    /**
     * 设置 [PARTNER_ID]
     */
    @JsonProperty("partner_id")
    public void setPartner_id(Integer  partner_id){
        this.partner_id = partner_id ;
        this.partner_idDirtyFlag = true ;
    }

    /**
     * 获取 [PARTNER_ID]脏标记
     */
    @JsonIgnore
    public boolean getPartner_idDirtyFlag(){
        return partner_idDirtyFlag ;
    }

    /**
     * 获取 [COMPANY_ID]
     */
    @JsonProperty("company_id")
    public Integer getCompany_id(){
        return company_id ;
    }

    /**
     * 设置 [COMPANY_ID]
     */
    @JsonProperty("company_id")
    public void setCompany_id(Integer  company_id){
        this.company_id = company_id ;
        this.company_idDirtyFlag = true ;
    }

    /**
     * 获取 [COMPANY_ID]脏标记
     */
    @JsonIgnore
    public boolean getCompany_idDirtyFlag(){
        return company_idDirtyFlag ;
    }

    /**
     * 获取 [SUBTASK_PROJECT_ID]
     */
    @JsonProperty("subtask_project_id")
    public Integer getSubtask_project_id(){
        return subtask_project_id ;
    }

    /**
     * 设置 [SUBTASK_PROJECT_ID]
     */
    @JsonProperty("subtask_project_id")
    public void setSubtask_project_id(Integer  subtask_project_id){
        this.subtask_project_id = subtask_project_id ;
        this.subtask_project_idDirtyFlag = true ;
    }

    /**
     * 获取 [SUBTASK_PROJECT_ID]脏标记
     */
    @JsonIgnore
    public boolean getSubtask_project_idDirtyFlag(){
        return subtask_project_idDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return create_uid ;
    }

    /**
     * 设置 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return create_uidDirtyFlag ;
    }

    /**
     * 获取 [RESOURCE_CALENDAR_ID]
     */
    @JsonProperty("resource_calendar_id")
    public Integer getResource_calendar_id(){
        return resource_calendar_id ;
    }

    /**
     * 设置 [RESOURCE_CALENDAR_ID]
     */
    @JsonProperty("resource_calendar_id")
    public void setResource_calendar_id(Integer  resource_calendar_id){
        this.resource_calendar_id = resource_calendar_id ;
        this.resource_calendar_idDirtyFlag = true ;
    }

    /**
     * 获取 [RESOURCE_CALENDAR_ID]脏标记
     */
    @JsonIgnore
    public boolean getResource_calendar_idDirtyFlag(){
        return resource_calendar_idDirtyFlag ;
    }

    /**
     * 获取 [ALIAS_ID]
     */
    @JsonProperty("alias_id")
    public Integer getAlias_id(){
        return alias_id ;
    }

    /**
     * 设置 [ALIAS_ID]
     */
    @JsonProperty("alias_id")
    public void setAlias_id(Integer  alias_id){
        this.alias_id = alias_id ;
        this.alias_idDirtyFlag = true ;
    }

    /**
     * 获取 [ALIAS_ID]脏标记
     */
    @JsonIgnore
    public boolean getAlias_idDirtyFlag(){
        return alias_idDirtyFlag ;
    }



    public Project_project toDO() {
        Project_project srfdomain = new Project_project();
        if(getAccess_warningDirtyFlag())
            srfdomain.setAccess_warning(access_warning);
        if(getMessage_attachment_countDirtyFlag())
            srfdomain.setMessage_attachment_count(message_attachment_count);
        if(getDateDirtyFlag())
            srfdomain.setDate(date);
        if(getDisplay_nameDirtyFlag())
            srfdomain.setDisplay_name(display_name);
        if(getTasksDirtyFlag())
            srfdomain.setTasks(tasks);
        if(getCreate_dateDirtyFlag())
            srfdomain.setCreate_date(create_date);
        if(getPercentage_satisfaction_taskDirtyFlag())
            srfdomain.setPercentage_satisfaction_task(percentage_satisfaction_task);
        if(getAccess_urlDirtyFlag())
            srfdomain.setAccess_url(access_url);
        if(getType_idsDirtyFlag())
            srfdomain.setType_ids(type_ids);
        if(get__last_updateDirtyFlag())
            srfdomain.set__last_update(__last_update);
        if(getAccess_tokenDirtyFlag())
            srfdomain.setAccess_token(access_token);
        if(getMessage_is_followerDirtyFlag())
            srfdomain.setMessage_is_follower(message_is_follower);
        if(getPrivacy_visibilityDirtyFlag())
            srfdomain.setPrivacy_visibility(privacy_visibility);
        if(getMessage_follower_idsDirtyFlag())
            srfdomain.setMessage_follower_ids(message_follower_ids);
        if(getNameDirtyFlag())
            srfdomain.setName(name);
        if(getRating_status_periodDirtyFlag())
            srfdomain.setRating_status_period(rating_status_period);
        if(getIdDirtyFlag())
            srfdomain.setId(id);
        if(getPercentage_satisfaction_projectDirtyFlag())
            srfdomain.setPercentage_satisfaction_project(percentage_satisfaction_project);
        if(getDoc_countDirtyFlag())
            srfdomain.setDoc_count(doc_count);
        if(getFavorite_user_idsDirtyFlag())
            srfdomain.setFavorite_user_ids(favorite_user_ids);
        if(getColorDirtyFlag())
            srfdomain.setColor(color);
        if(getWebsite_message_idsDirtyFlag())
            srfdomain.setWebsite_message_ids(website_message_ids);
        if(getTask_countDirtyFlag())
            srfdomain.setTask_count(task_count);
        if(getIs_favoriteDirtyFlag())
            srfdomain.setIs_favorite(is_favorite);
        if(getPortal_show_ratingDirtyFlag())
            srfdomain.setPortal_show_rating(portal_show_rating);
        if(getRating_request_deadlineDirtyFlag())
            srfdomain.setRating_request_deadline(rating_request_deadline);
        if(getMessage_idsDirtyFlag())
            srfdomain.setMessage_ids(message_ids);
        if(getMessage_main_attachment_idDirtyFlag())
            srfdomain.setMessage_main_attachment_id(message_main_attachment_id);
        if(getMessage_needaction_counterDirtyFlag())
            srfdomain.setMessage_needaction_counter(message_needaction_counter);
        if(getMessage_partner_idsDirtyFlag())
            srfdomain.setMessage_partner_ids(message_partner_ids);
        if(getActiveDirtyFlag())
            srfdomain.setActive(active);
        if(getMessage_channel_idsDirtyFlag())
            srfdomain.setMessage_channel_ids(message_channel_ids);
        if(getMessage_needactionDirtyFlag())
            srfdomain.setMessage_needaction(message_needaction);
        if(getMessage_unread_counterDirtyFlag())
            srfdomain.setMessage_unread_counter(message_unread_counter);
        if(getSequenceDirtyFlag())
            srfdomain.setSequence(sequence);
        if(getMessage_has_errorDirtyFlag())
            srfdomain.setMessage_has_error(message_has_error);
        if(getWrite_dateDirtyFlag())
            srfdomain.setWrite_date(write_date);
        if(getMessage_unreadDirtyFlag())
            srfdomain.setMessage_unread(message_unread);
        if(getRating_statusDirtyFlag())
            srfdomain.setRating_status(rating_status);
        if(getLabel_tasksDirtyFlag())
            srfdomain.setLabel_tasks(label_tasks);
        if(getTask_idsDirtyFlag())
            srfdomain.setTask_ids(task_ids);
        if(getMessage_has_error_counterDirtyFlag())
            srfdomain.setMessage_has_error_counter(message_has_error_counter);
        if(getDate_startDirtyFlag())
            srfdomain.setDate_start(date_start);
        if(getPartner_id_textDirtyFlag())
            srfdomain.setPartner_id_text(partner_id_text);
        if(getCompany_id_textDirtyFlag())
            srfdomain.setCompany_id_text(company_id_text);
        if(getCurrency_idDirtyFlag())
            srfdomain.setCurrency_id(currency_id);
        if(getAlias_parent_thread_idDirtyFlag())
            srfdomain.setAlias_parent_thread_id(alias_parent_thread_id);
        if(getAlias_force_thread_idDirtyFlag())
            srfdomain.setAlias_force_thread_id(alias_force_thread_id);
        if(getWrite_uid_textDirtyFlag())
            srfdomain.setWrite_uid_text(write_uid_text);
        if(getAlias_domainDirtyFlag())
            srfdomain.setAlias_domain(alias_domain);
        if(getAlias_contactDirtyFlag())
            srfdomain.setAlias_contact(alias_contact);
        if(getAlias_parent_model_idDirtyFlag())
            srfdomain.setAlias_parent_model_id(alias_parent_model_id);
        if(getAlias_model_idDirtyFlag())
            srfdomain.setAlias_model_id(alias_model_id);
        if(getCreate_uid_textDirtyFlag())
            srfdomain.setCreate_uid_text(create_uid_text);
        if(getAlias_user_idDirtyFlag())
            srfdomain.setAlias_user_id(alias_user_id);
        if(getAlias_defaultsDirtyFlag())
            srfdomain.setAlias_defaults(alias_defaults);
        if(getResource_calendar_id_textDirtyFlag())
            srfdomain.setResource_calendar_id_text(resource_calendar_id_text);
        if(getSubtask_project_id_textDirtyFlag())
            srfdomain.setSubtask_project_id_text(subtask_project_id_text);
        if(getAlias_nameDirtyFlag())
            srfdomain.setAlias_name(alias_name);
        if(getUser_id_textDirtyFlag())
            srfdomain.setUser_id_text(user_id_text);
        if(getWrite_uidDirtyFlag())
            srfdomain.setWrite_uid(write_uid);
        if(getUser_idDirtyFlag())
            srfdomain.setUser_id(user_id);
        if(getPartner_idDirtyFlag())
            srfdomain.setPartner_id(partner_id);
        if(getCompany_idDirtyFlag())
            srfdomain.setCompany_id(company_id);
        if(getSubtask_project_idDirtyFlag())
            srfdomain.setSubtask_project_id(subtask_project_id);
        if(getCreate_uidDirtyFlag())
            srfdomain.setCreate_uid(create_uid);
        if(getResource_calendar_idDirtyFlag())
            srfdomain.setResource_calendar_id(resource_calendar_id);
        if(getAlias_idDirtyFlag())
            srfdomain.setAlias_id(alias_id);

        return srfdomain;
    }

    public void fromDO(Project_project srfdomain) {
        if(srfdomain == null )
          return ;
        if(srfdomain.getAccess_warningDirtyFlag())
            this.setAccess_warning(srfdomain.getAccess_warning());
        if(srfdomain.getMessage_attachment_countDirtyFlag())
            this.setMessage_attachment_count(srfdomain.getMessage_attachment_count());
        if(srfdomain.getDateDirtyFlag())
            this.setDate(srfdomain.getDate());
        if(srfdomain.getDisplay_nameDirtyFlag())
            this.setDisplay_name(srfdomain.getDisplay_name());
        if(srfdomain.getTasksDirtyFlag())
            this.setTasks(srfdomain.getTasks());
        if(srfdomain.getCreate_dateDirtyFlag())
            this.setCreate_date(srfdomain.getCreate_date());
        if(srfdomain.getPercentage_satisfaction_taskDirtyFlag())
            this.setPercentage_satisfaction_task(srfdomain.getPercentage_satisfaction_task());
        if(srfdomain.getAccess_urlDirtyFlag())
            this.setAccess_url(srfdomain.getAccess_url());
        if(srfdomain.getType_idsDirtyFlag())
            this.setType_ids(srfdomain.getType_ids());
        if(srfdomain.get__last_updateDirtyFlag())
            this.set__last_update(srfdomain.get__last_update());
        if(srfdomain.getAccess_tokenDirtyFlag())
            this.setAccess_token(srfdomain.getAccess_token());
        if(srfdomain.getMessage_is_followerDirtyFlag())
            this.setMessage_is_follower(srfdomain.getMessage_is_follower());
        if(srfdomain.getPrivacy_visibilityDirtyFlag())
            this.setPrivacy_visibility(srfdomain.getPrivacy_visibility());
        if(srfdomain.getMessage_follower_idsDirtyFlag())
            this.setMessage_follower_ids(srfdomain.getMessage_follower_ids());
        if(srfdomain.getNameDirtyFlag())
            this.setName(srfdomain.getName());
        if(srfdomain.getRating_status_periodDirtyFlag())
            this.setRating_status_period(srfdomain.getRating_status_period());
        if(srfdomain.getIdDirtyFlag())
            this.setId(srfdomain.getId());
        if(srfdomain.getPercentage_satisfaction_projectDirtyFlag())
            this.setPercentage_satisfaction_project(srfdomain.getPercentage_satisfaction_project());
        if(srfdomain.getDoc_countDirtyFlag())
            this.setDoc_count(srfdomain.getDoc_count());
        if(srfdomain.getFavorite_user_idsDirtyFlag())
            this.setFavorite_user_ids(srfdomain.getFavorite_user_ids());
        if(srfdomain.getColorDirtyFlag())
            this.setColor(srfdomain.getColor());
        if(srfdomain.getWebsite_message_idsDirtyFlag())
            this.setWebsite_message_ids(srfdomain.getWebsite_message_ids());
        if(srfdomain.getTask_countDirtyFlag())
            this.setTask_count(srfdomain.getTask_count());
        if(srfdomain.getIs_favoriteDirtyFlag())
            this.setIs_favorite(srfdomain.getIs_favorite());
        if(srfdomain.getPortal_show_ratingDirtyFlag())
            this.setPortal_show_rating(srfdomain.getPortal_show_rating());
        if(srfdomain.getRating_request_deadlineDirtyFlag())
            this.setRating_request_deadline(srfdomain.getRating_request_deadline());
        if(srfdomain.getMessage_idsDirtyFlag())
            this.setMessage_ids(srfdomain.getMessage_ids());
        if(srfdomain.getMessage_main_attachment_idDirtyFlag())
            this.setMessage_main_attachment_id(srfdomain.getMessage_main_attachment_id());
        if(srfdomain.getMessage_needaction_counterDirtyFlag())
            this.setMessage_needaction_counter(srfdomain.getMessage_needaction_counter());
        if(srfdomain.getMessage_partner_idsDirtyFlag())
            this.setMessage_partner_ids(srfdomain.getMessage_partner_ids());
        if(srfdomain.getActiveDirtyFlag())
            this.setActive(srfdomain.getActive());
        if(srfdomain.getMessage_channel_idsDirtyFlag())
            this.setMessage_channel_ids(srfdomain.getMessage_channel_ids());
        if(srfdomain.getMessage_needactionDirtyFlag())
            this.setMessage_needaction(srfdomain.getMessage_needaction());
        if(srfdomain.getMessage_unread_counterDirtyFlag())
            this.setMessage_unread_counter(srfdomain.getMessage_unread_counter());
        if(srfdomain.getSequenceDirtyFlag())
            this.setSequence(srfdomain.getSequence());
        if(srfdomain.getMessage_has_errorDirtyFlag())
            this.setMessage_has_error(srfdomain.getMessage_has_error());
        if(srfdomain.getWrite_dateDirtyFlag())
            this.setWrite_date(srfdomain.getWrite_date());
        if(srfdomain.getMessage_unreadDirtyFlag())
            this.setMessage_unread(srfdomain.getMessage_unread());
        if(srfdomain.getRating_statusDirtyFlag())
            this.setRating_status(srfdomain.getRating_status());
        if(srfdomain.getLabel_tasksDirtyFlag())
            this.setLabel_tasks(srfdomain.getLabel_tasks());
        if(srfdomain.getTask_idsDirtyFlag())
            this.setTask_ids(srfdomain.getTask_ids());
        if(srfdomain.getMessage_has_error_counterDirtyFlag())
            this.setMessage_has_error_counter(srfdomain.getMessage_has_error_counter());
        if(srfdomain.getDate_startDirtyFlag())
            this.setDate_start(srfdomain.getDate_start());
        if(srfdomain.getPartner_id_textDirtyFlag())
            this.setPartner_id_text(srfdomain.getPartner_id_text());
        if(srfdomain.getCompany_id_textDirtyFlag())
            this.setCompany_id_text(srfdomain.getCompany_id_text());
        if(srfdomain.getCurrency_idDirtyFlag())
            this.setCurrency_id(srfdomain.getCurrency_id());
        if(srfdomain.getAlias_parent_thread_idDirtyFlag())
            this.setAlias_parent_thread_id(srfdomain.getAlias_parent_thread_id());
        if(srfdomain.getAlias_force_thread_idDirtyFlag())
            this.setAlias_force_thread_id(srfdomain.getAlias_force_thread_id());
        if(srfdomain.getWrite_uid_textDirtyFlag())
            this.setWrite_uid_text(srfdomain.getWrite_uid_text());
        if(srfdomain.getAlias_domainDirtyFlag())
            this.setAlias_domain(srfdomain.getAlias_domain());
        if(srfdomain.getAlias_contactDirtyFlag())
            this.setAlias_contact(srfdomain.getAlias_contact());
        if(srfdomain.getAlias_parent_model_idDirtyFlag())
            this.setAlias_parent_model_id(srfdomain.getAlias_parent_model_id());
        if(srfdomain.getAlias_model_idDirtyFlag())
            this.setAlias_model_id(srfdomain.getAlias_model_id());
        if(srfdomain.getCreate_uid_textDirtyFlag())
            this.setCreate_uid_text(srfdomain.getCreate_uid_text());
        if(srfdomain.getAlias_user_idDirtyFlag())
            this.setAlias_user_id(srfdomain.getAlias_user_id());
        if(srfdomain.getAlias_defaultsDirtyFlag())
            this.setAlias_defaults(srfdomain.getAlias_defaults());
        if(srfdomain.getResource_calendar_id_textDirtyFlag())
            this.setResource_calendar_id_text(srfdomain.getResource_calendar_id_text());
        if(srfdomain.getSubtask_project_id_textDirtyFlag())
            this.setSubtask_project_id_text(srfdomain.getSubtask_project_id_text());
        if(srfdomain.getAlias_nameDirtyFlag())
            this.setAlias_name(srfdomain.getAlias_name());
        if(srfdomain.getUser_id_textDirtyFlag())
            this.setUser_id_text(srfdomain.getUser_id_text());
        if(srfdomain.getWrite_uidDirtyFlag())
            this.setWrite_uid(srfdomain.getWrite_uid());
        if(srfdomain.getUser_idDirtyFlag())
            this.setUser_id(srfdomain.getUser_id());
        if(srfdomain.getPartner_idDirtyFlag())
            this.setPartner_id(srfdomain.getPartner_id());
        if(srfdomain.getCompany_idDirtyFlag())
            this.setCompany_id(srfdomain.getCompany_id());
        if(srfdomain.getSubtask_project_idDirtyFlag())
            this.setSubtask_project_id(srfdomain.getSubtask_project_id());
        if(srfdomain.getCreate_uidDirtyFlag())
            this.setCreate_uid(srfdomain.getCreate_uid());
        if(srfdomain.getResource_calendar_idDirtyFlag())
            this.setResource_calendar_id(srfdomain.getResource_calendar_id());
        if(srfdomain.getAlias_idDirtyFlag())
            this.setAlias_id(srfdomain.getAlias_id());

    }

    public List<Project_projectDTO> fromDOPage(List<Project_project> poPage)   {
        if(poPage == null)
            return null;
        List<Project_projectDTO> dtos=new ArrayList<Project_projectDTO>();
        for(Project_project domain : poPage) {
            Project_projectDTO dto = new Project_projectDTO();
            dto.fromDO(domain);
            dtos.add(dto);
        }
        return dtos;
    }
}

