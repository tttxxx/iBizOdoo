package cn.ibizlab.odoo.service.odoo_project.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_project.dto.Project_taskDTO;
import cn.ibizlab.odoo.core.odoo_project.domain.Project_task;
import cn.ibizlab.odoo.core.odoo_project.service.IProject_taskService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_project.filter.Project_taskSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Project_task" })
@RestController
@RequestMapping("")
public class Project_taskResource {

    @Autowired
    private IProject_taskService project_taskService;

    public IProject_taskService getProject_taskService() {
        return this.project_taskService;
    }

    @ApiOperation(value = "更新数据", tags = {"Project_task" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_project/project_tasks/{project_task_id}")

    public ResponseEntity<Project_taskDTO> update(@PathVariable("project_task_id") Integer project_task_id, @RequestBody Project_taskDTO project_taskdto) {
		Project_task domain = project_taskdto.toDO();
        domain.setId(project_task_id);
		project_taskService.update(domain);
		Project_taskDTO dto = new Project_taskDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取数据", tags = {"Project_task" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_project/project_tasks/{project_task_id}")
    public ResponseEntity<Project_taskDTO> get(@PathVariable("project_task_id") Integer project_task_id) {
        Project_taskDTO dto = new Project_taskDTO();
        Project_task domain = project_taskService.get(project_task_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批建立数据", tags = {"Project_task" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_project/project_tasks/createBatch")
    public ResponseEntity<Boolean> createBatchProject_task(@RequestBody List<Project_taskDTO> project_taskdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "删除数据", tags = {"Project_task" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_project/project_tasks/{project_task_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("project_task_id") Integer project_task_id) {
        Project_taskDTO project_taskdto = new Project_taskDTO();
		Project_task domain = new Project_task();
		project_taskdto.setId(project_task_id);
		domain.setId(project_task_id);
        Boolean rst = project_taskService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "建立数据", tags = {"Project_task" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_project/project_tasks")

    public ResponseEntity<Project_taskDTO> create(@RequestBody Project_taskDTO project_taskdto) {
        Project_taskDTO dto = new Project_taskDTO();
        Project_task domain = project_taskdto.toDO();
		project_taskService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批更新数据", tags = {"Project_task" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_project/project_tasks/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Project_taskDTO> project_taskdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "批删除数据", tags = {"Project_task" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_project/project_tasks/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Project_taskDTO> project_taskdtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Project_task" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_project/project_tasks/fetchdefault")
	public ResponseEntity<Page<Project_taskDTO>> fetchDefault(Project_taskSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Project_taskDTO> list = new ArrayList<Project_taskDTO>();
        
        Page<Project_task> domains = project_taskService.searchDefault(context) ;
        for(Project_task project_task : domains.getContent()){
            Project_taskDTO dto = new Project_taskDTO();
            dto.fromDO(project_task);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
