package cn.ibizlab.odoo.service.odoo_project.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_project.dto.Project_tagsDTO;
import cn.ibizlab.odoo.core.odoo_project.domain.Project_tags;
import cn.ibizlab.odoo.core.odoo_project.service.IProject_tagsService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_project.filter.Project_tagsSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Project_tags" })
@RestController
@RequestMapping("")
public class Project_tagsResource {

    @Autowired
    private IProject_tagsService project_tagsService;

    public IProject_tagsService getProject_tagsService() {
        return this.project_tagsService;
    }

    @ApiOperation(value = "获取数据", tags = {"Project_tags" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_project/project_tags/{project_tags_id}")
    public ResponseEntity<Project_tagsDTO> get(@PathVariable("project_tags_id") Integer project_tags_id) {
        Project_tagsDTO dto = new Project_tagsDTO();
        Project_tags domain = project_tagsService.get(project_tags_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批建立数据", tags = {"Project_tags" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_project/project_tags/createBatch")
    public ResponseEntity<Boolean> createBatchProject_tags(@RequestBody List<Project_tagsDTO> project_tagsdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "删除数据", tags = {"Project_tags" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_project/project_tags/{project_tags_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("project_tags_id") Integer project_tags_id) {
        Project_tagsDTO project_tagsdto = new Project_tagsDTO();
		Project_tags domain = new Project_tags();
		project_tagsdto.setId(project_tags_id);
		domain.setId(project_tags_id);
        Boolean rst = project_tagsService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "建立数据", tags = {"Project_tags" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_project/project_tags")

    public ResponseEntity<Project_tagsDTO> create(@RequestBody Project_tagsDTO project_tagsdto) {
        Project_tagsDTO dto = new Project_tagsDTO();
        Project_tags domain = project_tagsdto.toDO();
		project_tagsService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批更新数据", tags = {"Project_tags" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_project/project_tags/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Project_tagsDTO> project_tagsdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "更新数据", tags = {"Project_tags" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_project/project_tags/{project_tags_id}")

    public ResponseEntity<Project_tagsDTO> update(@PathVariable("project_tags_id") Integer project_tags_id, @RequestBody Project_tagsDTO project_tagsdto) {
		Project_tags domain = project_tagsdto.toDO();
        domain.setId(project_tags_id);
		project_tagsService.update(domain);
		Project_tagsDTO dto = new Project_tagsDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批删除数据", tags = {"Project_tags" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_project/project_tags/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Project_tagsDTO> project_tagsdtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Project_tags" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_project/project_tags/fetchdefault")
	public ResponseEntity<Page<Project_tagsDTO>> fetchDefault(Project_tagsSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Project_tagsDTO> list = new ArrayList<Project_tagsDTO>();
        
        Page<Project_tags> domains = project_tagsService.searchDefault(context) ;
        for(Project_tags project_tags : domains.getContent()){
            Project_tagsDTO dto = new Project_tagsDTO();
            dto.fromDO(project_tags);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
