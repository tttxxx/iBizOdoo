package cn.ibizlab.odoo.service.odoo_rating.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_rating.dto.Rating_ratingDTO;
import cn.ibizlab.odoo.core.odoo_rating.domain.Rating_rating;
import cn.ibizlab.odoo.core.odoo_rating.service.IRating_ratingService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_rating.filter.Rating_ratingSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Rating_rating" })
@RestController
@RequestMapping("")
public class Rating_ratingResource {

    @Autowired
    private IRating_ratingService rating_ratingService;

    public IRating_ratingService getRating_ratingService() {
        return this.rating_ratingService;
    }

    @ApiOperation(value = "更新数据", tags = {"Rating_rating" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_rating/rating_ratings/{rating_rating_id}")

    public ResponseEntity<Rating_ratingDTO> update(@PathVariable("rating_rating_id") Integer rating_rating_id, @RequestBody Rating_ratingDTO rating_ratingdto) {
		Rating_rating domain = rating_ratingdto.toDO();
        domain.setId(rating_rating_id);
		rating_ratingService.update(domain);
		Rating_ratingDTO dto = new Rating_ratingDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批删除数据", tags = {"Rating_rating" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_rating/rating_ratings/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Rating_ratingDTO> rating_ratingdtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "批更新数据", tags = {"Rating_rating" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_rating/rating_ratings/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Rating_ratingDTO> rating_ratingdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "建立数据", tags = {"Rating_rating" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_rating/rating_ratings")

    public ResponseEntity<Rating_ratingDTO> create(@RequestBody Rating_ratingDTO rating_ratingdto) {
        Rating_ratingDTO dto = new Rating_ratingDTO();
        Rating_rating domain = rating_ratingdto.toDO();
		rating_ratingService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批建立数据", tags = {"Rating_rating" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_rating/rating_ratings/createBatch")
    public ResponseEntity<Boolean> createBatchRating_rating(@RequestBody List<Rating_ratingDTO> rating_ratingdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "获取数据", tags = {"Rating_rating" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_rating/rating_ratings/{rating_rating_id}")
    public ResponseEntity<Rating_ratingDTO> get(@PathVariable("rating_rating_id") Integer rating_rating_id) {
        Rating_ratingDTO dto = new Rating_ratingDTO();
        Rating_rating domain = rating_ratingService.get(rating_rating_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "删除数据", tags = {"Rating_rating" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_rating/rating_ratings/{rating_rating_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("rating_rating_id") Integer rating_rating_id) {
        Rating_ratingDTO rating_ratingdto = new Rating_ratingDTO();
		Rating_rating domain = new Rating_rating();
		rating_ratingdto.setId(rating_rating_id);
		domain.setId(rating_rating_id);
        Boolean rst = rating_ratingService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

	@ApiOperation(value = "获取默认查询", tags = {"Rating_rating" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_rating/rating_ratings/fetchdefault")
	public ResponseEntity<Page<Rating_ratingDTO>> fetchDefault(Rating_ratingSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Rating_ratingDTO> list = new ArrayList<Rating_ratingDTO>();
        
        Page<Rating_rating> domains = rating_ratingService.searchDefault(context) ;
        for(Rating_rating rating_rating : domains.getContent()){
            Rating_ratingDTO dto = new Rating_ratingDTO();
            dto.fromDO(rating_rating);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
