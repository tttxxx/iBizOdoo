package cn.ibizlab.odoo.service.odoo_resource.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_resource.dto.Resource_resourceDTO;
import cn.ibizlab.odoo.core.odoo_resource.domain.Resource_resource;
import cn.ibizlab.odoo.core.odoo_resource.service.IResource_resourceService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_resource.filter.Resource_resourceSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Resource_resource" })
@RestController
@RequestMapping("")
public class Resource_resourceResource {

    @Autowired
    private IResource_resourceService resource_resourceService;

    public IResource_resourceService getResource_resourceService() {
        return this.resource_resourceService;
    }

    @ApiOperation(value = "批更新数据", tags = {"Resource_resource" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_resource/resource_resources/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Resource_resourceDTO> resource_resourcedtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "批删除数据", tags = {"Resource_resource" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_resource/resource_resources/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Resource_resourceDTO> resource_resourcedtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "更新数据", tags = {"Resource_resource" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_resource/resource_resources/{resource_resource_id}")

    public ResponseEntity<Resource_resourceDTO> update(@PathVariable("resource_resource_id") Integer resource_resource_id, @RequestBody Resource_resourceDTO resource_resourcedto) {
		Resource_resource domain = resource_resourcedto.toDO();
        domain.setId(resource_resource_id);
		resource_resourceService.update(domain);
		Resource_resourceDTO dto = new Resource_resourceDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取数据", tags = {"Resource_resource" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_resource/resource_resources/{resource_resource_id}")
    public ResponseEntity<Resource_resourceDTO> get(@PathVariable("resource_resource_id") Integer resource_resource_id) {
        Resource_resourceDTO dto = new Resource_resourceDTO();
        Resource_resource domain = resource_resourceService.get(resource_resource_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "建立数据", tags = {"Resource_resource" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_resource/resource_resources")

    public ResponseEntity<Resource_resourceDTO> create(@RequestBody Resource_resourceDTO resource_resourcedto) {
        Resource_resourceDTO dto = new Resource_resourceDTO();
        Resource_resource domain = resource_resourcedto.toDO();
		resource_resourceService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批建立数据", tags = {"Resource_resource" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_resource/resource_resources/createBatch")
    public ResponseEntity<Boolean> createBatchResource_resource(@RequestBody List<Resource_resourceDTO> resource_resourcedtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "删除数据", tags = {"Resource_resource" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_resource/resource_resources/{resource_resource_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("resource_resource_id") Integer resource_resource_id) {
        Resource_resourceDTO resource_resourcedto = new Resource_resourceDTO();
		Resource_resource domain = new Resource_resource();
		resource_resourcedto.setId(resource_resource_id);
		domain.setId(resource_resource_id);
        Boolean rst = resource_resourceService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

	@ApiOperation(value = "获取默认查询", tags = {"Resource_resource" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_resource/resource_resources/fetchdefault")
	public ResponseEntity<Page<Resource_resourceDTO>> fetchDefault(Resource_resourceSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Resource_resourceDTO> list = new ArrayList<Resource_resourceDTO>();
        
        Page<Resource_resource> domains = resource_resourceService.searchDefault(context) ;
        for(Resource_resource resource_resource : domains.getContent()){
            Resource_resourceDTO dto = new Resource_resourceDTO();
            dto.fromDO(resource_resource);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
