package cn.ibizlab.odoo.service.odoo_resource.config;

import org.springframework.context.annotation.ComponentScan;

@ComponentScan("cn.ibizlab.odoo.service.odoo_resource")
public class odoo_resourceRestConfiguration {

}
