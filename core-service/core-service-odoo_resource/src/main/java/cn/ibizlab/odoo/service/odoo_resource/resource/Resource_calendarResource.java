package cn.ibizlab.odoo.service.odoo_resource.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_resource.dto.Resource_calendarDTO;
import cn.ibizlab.odoo.core.odoo_resource.domain.Resource_calendar;
import cn.ibizlab.odoo.core.odoo_resource.service.IResource_calendarService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_resource.filter.Resource_calendarSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Resource_calendar" })
@RestController
@RequestMapping("")
public class Resource_calendarResource {

    @Autowired
    private IResource_calendarService resource_calendarService;

    public IResource_calendarService getResource_calendarService() {
        return this.resource_calendarService;
    }

    @ApiOperation(value = "建立数据", tags = {"Resource_calendar" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_resource/resource_calendars")

    public ResponseEntity<Resource_calendarDTO> create(@RequestBody Resource_calendarDTO resource_calendardto) {
        Resource_calendarDTO dto = new Resource_calendarDTO();
        Resource_calendar domain = resource_calendardto.toDO();
		resource_calendarService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批更新数据", tags = {"Resource_calendar" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_resource/resource_calendars/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Resource_calendarDTO> resource_calendardtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "获取数据", tags = {"Resource_calendar" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_resource/resource_calendars/{resource_calendar_id}")
    public ResponseEntity<Resource_calendarDTO> get(@PathVariable("resource_calendar_id") Integer resource_calendar_id) {
        Resource_calendarDTO dto = new Resource_calendarDTO();
        Resource_calendar domain = resource_calendarService.get(resource_calendar_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "删除数据", tags = {"Resource_calendar" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_resource/resource_calendars/{resource_calendar_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("resource_calendar_id") Integer resource_calendar_id) {
        Resource_calendarDTO resource_calendardto = new Resource_calendarDTO();
		Resource_calendar domain = new Resource_calendar();
		resource_calendardto.setId(resource_calendar_id);
		domain.setId(resource_calendar_id);
        Boolean rst = resource_calendarService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "更新数据", tags = {"Resource_calendar" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_resource/resource_calendars/{resource_calendar_id}")

    public ResponseEntity<Resource_calendarDTO> update(@PathVariable("resource_calendar_id") Integer resource_calendar_id, @RequestBody Resource_calendarDTO resource_calendardto) {
		Resource_calendar domain = resource_calendardto.toDO();
        domain.setId(resource_calendar_id);
		resource_calendarService.update(domain);
		Resource_calendarDTO dto = new Resource_calendarDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批建立数据", tags = {"Resource_calendar" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_resource/resource_calendars/createBatch")
    public ResponseEntity<Boolean> createBatchResource_calendar(@RequestBody List<Resource_calendarDTO> resource_calendardtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "批删除数据", tags = {"Resource_calendar" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_resource/resource_calendars/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Resource_calendarDTO> resource_calendardtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Resource_calendar" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_resource/resource_calendars/fetchdefault")
	public ResponseEntity<Page<Resource_calendarDTO>> fetchDefault(Resource_calendarSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Resource_calendarDTO> list = new ArrayList<Resource_calendarDTO>();
        
        Page<Resource_calendar> domains = resource_calendarService.searchDefault(context) ;
        for(Resource_calendar resource_calendar : domains.getContent()){
            Resource_calendarDTO dto = new Resource_calendarDTO();
            dto.fromDO(resource_calendar);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
