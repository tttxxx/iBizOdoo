package cn.ibizlab.odoo.service.odoo_resource.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.core.odoo_resource.valuerule.anno.resource_calendar_attendance.*;
import cn.ibizlab.odoo.core.odoo_resource.domain.Resource_calendar_attendance;
import org.springframework.cglib.beans.BeanCopier;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 服务DTO对象[Resource_calendar_attendanceDTO]
 */
public class Resource_calendar_attendanceDTO implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [DAYOFWEEK]
     *
     */
    @Resource_calendar_attendanceDayofweekDefault(info = "默认规则")
    private String dayofweek;

    @JsonIgnore
    private boolean dayofweekDirtyFlag;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @Resource_calendar_attendance__last_updateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp __last_update;

    @JsonIgnore
    private boolean __last_updateDirtyFlag;

    /**
     * 属性 [HOUR_TO]
     *
     */
    @Resource_calendar_attendanceHour_toDefault(info = "默认规则")
    private Double hour_to;

    @JsonIgnore
    private boolean hour_toDirtyFlag;

    /**
     * 属性 [DAY_PERIOD]
     *
     */
    @Resource_calendar_attendanceDay_periodDefault(info = "默认规则")
    private String day_period;

    @JsonIgnore
    private boolean day_periodDirtyFlag;

    /**
     * 属性 [HOUR_FROM]
     *
     */
    @Resource_calendar_attendanceHour_fromDefault(info = "默认规则")
    private Double hour_from;

    @JsonIgnore
    private boolean hour_fromDirtyFlag;

    /**
     * 属性 [CREATE_DATE]
     *
     */
    @Resource_calendar_attendanceCreate_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp create_date;

    @JsonIgnore
    private boolean create_dateDirtyFlag;

    /**
     * 属性 [NAME]
     *
     */
    @Resource_calendar_attendanceNameDefault(info = "默认规则")
    private String name;

    @JsonIgnore
    private boolean nameDirtyFlag;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @Resource_calendar_attendanceDisplay_nameDefault(info = "默认规则")
    private String display_name;

    @JsonIgnore
    private boolean display_nameDirtyFlag;

    /**
     * 属性 [WRITE_DATE]
     *
     */
    @Resource_calendar_attendanceWrite_dateDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    private Timestamp write_date;

    @JsonIgnore
    private boolean write_dateDirtyFlag;

    /**
     * 属性 [ID]
     *
     */
    @Resource_calendar_attendanceIdDefault(info = "默认规则")
    private Integer id;

    @JsonIgnore
    private boolean idDirtyFlag;

    /**
     * 属性 [DATE_TO]
     *
     */
    @Resource_calendar_attendanceDate_toDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    private Timestamp date_to;

    @JsonIgnore
    private boolean date_toDirtyFlag;

    /**
     * 属性 [DATE_FROM]
     *
     */
    @Resource_calendar_attendanceDate_fromDefault(info = "默认规则")
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    private Timestamp date_from;

    @JsonIgnore
    private boolean date_fromDirtyFlag;

    /**
     * 属性 [CREATE_UID_TEXT]
     *
     */
    @Resource_calendar_attendanceCreate_uid_textDefault(info = "默认规则")
    private String create_uid_text;

    @JsonIgnore
    private boolean create_uid_textDirtyFlag;

    /**
     * 属性 [CALENDAR_ID_TEXT]
     *
     */
    @Resource_calendar_attendanceCalendar_id_textDefault(info = "默认规则")
    private String calendar_id_text;

    @JsonIgnore
    private boolean calendar_id_textDirtyFlag;

    /**
     * 属性 [WRITE_UID_TEXT]
     *
     */
    @Resource_calendar_attendanceWrite_uid_textDefault(info = "默认规则")
    private String write_uid_text;

    @JsonIgnore
    private boolean write_uid_textDirtyFlag;

    /**
     * 属性 [CALENDAR_ID]
     *
     */
    @Resource_calendar_attendanceCalendar_idDefault(info = "默认规则")
    private Integer calendar_id;

    @JsonIgnore
    private boolean calendar_idDirtyFlag;

    /**
     * 属性 [WRITE_UID]
     *
     */
    @Resource_calendar_attendanceWrite_uidDefault(info = "默认规则")
    private Integer write_uid;

    @JsonIgnore
    private boolean write_uidDirtyFlag;

    /**
     * 属性 [CREATE_UID]
     *
     */
    @Resource_calendar_attendanceCreate_uidDefault(info = "默认规则")
    private Integer create_uid;

    @JsonIgnore
    private boolean create_uidDirtyFlag;


    /**
     * 获取 [DAYOFWEEK]
     */
    @JsonProperty("dayofweek")
    public String getDayofweek(){
        return dayofweek ;
    }

    /**
     * 设置 [DAYOFWEEK]
     */
    @JsonProperty("dayofweek")
    public void setDayofweek(String  dayofweek){
        this.dayofweek = dayofweek ;
        this.dayofweekDirtyFlag = true ;
    }

    /**
     * 获取 [DAYOFWEEK]脏标记
     */
    @JsonIgnore
    public boolean getDayofweekDirtyFlag(){
        return dayofweekDirtyFlag ;
    }

    /**
     * 获取 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return __last_update ;
    }

    /**
     * 设置 [__LAST_UPDATE]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

    /**
     * 获取 [__LAST_UPDATE]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return __last_updateDirtyFlag ;
    }

    /**
     * 获取 [HOUR_TO]
     */
    @JsonProperty("hour_to")
    public Double getHour_to(){
        return hour_to ;
    }

    /**
     * 设置 [HOUR_TO]
     */
    @JsonProperty("hour_to")
    public void setHour_to(Double  hour_to){
        this.hour_to = hour_to ;
        this.hour_toDirtyFlag = true ;
    }

    /**
     * 获取 [HOUR_TO]脏标记
     */
    @JsonIgnore
    public boolean getHour_toDirtyFlag(){
        return hour_toDirtyFlag ;
    }

    /**
     * 获取 [DAY_PERIOD]
     */
    @JsonProperty("day_period")
    public String getDay_period(){
        return day_period ;
    }

    /**
     * 设置 [DAY_PERIOD]
     */
    @JsonProperty("day_period")
    public void setDay_period(String  day_period){
        this.day_period = day_period ;
        this.day_periodDirtyFlag = true ;
    }

    /**
     * 获取 [DAY_PERIOD]脏标记
     */
    @JsonIgnore
    public boolean getDay_periodDirtyFlag(){
        return day_periodDirtyFlag ;
    }

    /**
     * 获取 [HOUR_FROM]
     */
    @JsonProperty("hour_from")
    public Double getHour_from(){
        return hour_from ;
    }

    /**
     * 设置 [HOUR_FROM]
     */
    @JsonProperty("hour_from")
    public void setHour_from(Double  hour_from){
        this.hour_from = hour_from ;
        this.hour_fromDirtyFlag = true ;
    }

    /**
     * 获取 [HOUR_FROM]脏标记
     */
    @JsonIgnore
    public boolean getHour_fromDirtyFlag(){
        return hour_fromDirtyFlag ;
    }

    /**
     * 获取 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return create_date ;
    }

    /**
     * 设置 [CREATE_DATE]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return create_dateDirtyFlag ;
    }

    /**
     * 获取 [NAME]
     */
    @JsonProperty("name")
    public String getName(){
        return name ;
    }

    /**
     * 设置 [NAME]
     */
    @JsonProperty("name")
    public void setName(String  name){
        this.name = name ;
        this.nameDirtyFlag = true ;
    }

    /**
     * 获取 [NAME]脏标记
     */
    @JsonIgnore
    public boolean getNameDirtyFlag(){
        return nameDirtyFlag ;
    }

    /**
     * 获取 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return display_name ;
    }

    /**
     * 设置 [DISPLAY_NAME]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

    /**
     * 获取 [DISPLAY_NAME]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return display_nameDirtyFlag ;
    }

    /**
     * 获取 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return write_date ;
    }

    /**
     * 设置 [WRITE_DATE]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_DATE]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return write_dateDirtyFlag ;
    }

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

    /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return idDirtyFlag ;
    }

    /**
     * 获取 [DATE_TO]
     */
    @JsonProperty("date_to")
    public Timestamp getDate_to(){
        return date_to ;
    }

    /**
     * 设置 [DATE_TO]
     */
    @JsonProperty("date_to")
    public void setDate_to(Timestamp  date_to){
        this.date_to = date_to ;
        this.date_toDirtyFlag = true ;
    }

    /**
     * 获取 [DATE_TO]脏标记
     */
    @JsonIgnore
    public boolean getDate_toDirtyFlag(){
        return date_toDirtyFlag ;
    }

    /**
     * 获取 [DATE_FROM]
     */
    @JsonProperty("date_from")
    public Timestamp getDate_from(){
        return date_from ;
    }

    /**
     * 设置 [DATE_FROM]
     */
    @JsonProperty("date_from")
    public void setDate_from(Timestamp  date_from){
        this.date_from = date_from ;
        this.date_fromDirtyFlag = true ;
    }

    /**
     * 获取 [DATE_FROM]脏标记
     */
    @JsonIgnore
    public boolean getDate_fromDirtyFlag(){
        return date_fromDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return create_uid_text ;
    }

    /**
     * 设置 [CREATE_UID_TEXT]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return create_uid_textDirtyFlag ;
    }

    /**
     * 获取 [CALENDAR_ID_TEXT]
     */
    @JsonProperty("calendar_id_text")
    public String getCalendar_id_text(){
        return calendar_id_text ;
    }

    /**
     * 设置 [CALENDAR_ID_TEXT]
     */
    @JsonProperty("calendar_id_text")
    public void setCalendar_id_text(String  calendar_id_text){
        this.calendar_id_text = calendar_id_text ;
        this.calendar_id_textDirtyFlag = true ;
    }

    /**
     * 获取 [CALENDAR_ID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getCalendar_id_textDirtyFlag(){
        return calendar_id_textDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return write_uid_text ;
    }

    /**
     * 设置 [WRITE_UID_TEXT]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID_TEXT]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return write_uid_textDirtyFlag ;
    }

    /**
     * 获取 [CALENDAR_ID]
     */
    @JsonProperty("calendar_id")
    public Integer getCalendar_id(){
        return calendar_id ;
    }

    /**
     * 设置 [CALENDAR_ID]
     */
    @JsonProperty("calendar_id")
    public void setCalendar_id(Integer  calendar_id){
        this.calendar_id = calendar_id ;
        this.calendar_idDirtyFlag = true ;
    }

    /**
     * 获取 [CALENDAR_ID]脏标记
     */
    @JsonIgnore
    public boolean getCalendar_idDirtyFlag(){
        return calendar_idDirtyFlag ;
    }

    /**
     * 获取 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return write_uid ;
    }

    /**
     * 设置 [WRITE_UID]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

    /**
     * 获取 [WRITE_UID]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return write_uidDirtyFlag ;
    }

    /**
     * 获取 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return create_uid ;
    }

    /**
     * 设置 [CREATE_UID]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

    /**
     * 获取 [CREATE_UID]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return create_uidDirtyFlag ;
    }



    public Resource_calendar_attendance toDO() {
        Resource_calendar_attendance srfdomain = new Resource_calendar_attendance();
        if(getDayofweekDirtyFlag())
            srfdomain.setDayofweek(dayofweek);
        if(get__last_updateDirtyFlag())
            srfdomain.set__last_update(__last_update);
        if(getHour_toDirtyFlag())
            srfdomain.setHour_to(hour_to);
        if(getDay_periodDirtyFlag())
            srfdomain.setDay_period(day_period);
        if(getHour_fromDirtyFlag())
            srfdomain.setHour_from(hour_from);
        if(getCreate_dateDirtyFlag())
            srfdomain.setCreate_date(create_date);
        if(getNameDirtyFlag())
            srfdomain.setName(name);
        if(getDisplay_nameDirtyFlag())
            srfdomain.setDisplay_name(display_name);
        if(getWrite_dateDirtyFlag())
            srfdomain.setWrite_date(write_date);
        if(getIdDirtyFlag())
            srfdomain.setId(id);
        if(getDate_toDirtyFlag())
            srfdomain.setDate_to(date_to);
        if(getDate_fromDirtyFlag())
            srfdomain.setDate_from(date_from);
        if(getCreate_uid_textDirtyFlag())
            srfdomain.setCreate_uid_text(create_uid_text);
        if(getCalendar_id_textDirtyFlag())
            srfdomain.setCalendar_id_text(calendar_id_text);
        if(getWrite_uid_textDirtyFlag())
            srfdomain.setWrite_uid_text(write_uid_text);
        if(getCalendar_idDirtyFlag())
            srfdomain.setCalendar_id(calendar_id);
        if(getWrite_uidDirtyFlag())
            srfdomain.setWrite_uid(write_uid);
        if(getCreate_uidDirtyFlag())
            srfdomain.setCreate_uid(create_uid);

        return srfdomain;
    }

    public void fromDO(Resource_calendar_attendance srfdomain) {
        if(srfdomain == null )
          return ;
        if(srfdomain.getDayofweekDirtyFlag())
            this.setDayofweek(srfdomain.getDayofweek());
        if(srfdomain.get__last_updateDirtyFlag())
            this.set__last_update(srfdomain.get__last_update());
        if(srfdomain.getHour_toDirtyFlag())
            this.setHour_to(srfdomain.getHour_to());
        if(srfdomain.getDay_periodDirtyFlag())
            this.setDay_period(srfdomain.getDay_period());
        if(srfdomain.getHour_fromDirtyFlag())
            this.setHour_from(srfdomain.getHour_from());
        if(srfdomain.getCreate_dateDirtyFlag())
            this.setCreate_date(srfdomain.getCreate_date());
        if(srfdomain.getNameDirtyFlag())
            this.setName(srfdomain.getName());
        if(srfdomain.getDisplay_nameDirtyFlag())
            this.setDisplay_name(srfdomain.getDisplay_name());
        if(srfdomain.getWrite_dateDirtyFlag())
            this.setWrite_date(srfdomain.getWrite_date());
        if(srfdomain.getIdDirtyFlag())
            this.setId(srfdomain.getId());
        if(srfdomain.getDate_toDirtyFlag())
            this.setDate_to(srfdomain.getDate_to());
        if(srfdomain.getDate_fromDirtyFlag())
            this.setDate_from(srfdomain.getDate_from());
        if(srfdomain.getCreate_uid_textDirtyFlag())
            this.setCreate_uid_text(srfdomain.getCreate_uid_text());
        if(srfdomain.getCalendar_id_textDirtyFlag())
            this.setCalendar_id_text(srfdomain.getCalendar_id_text());
        if(srfdomain.getWrite_uid_textDirtyFlag())
            this.setWrite_uid_text(srfdomain.getWrite_uid_text());
        if(srfdomain.getCalendar_idDirtyFlag())
            this.setCalendar_id(srfdomain.getCalendar_id());
        if(srfdomain.getWrite_uidDirtyFlag())
            this.setWrite_uid(srfdomain.getWrite_uid());
        if(srfdomain.getCreate_uidDirtyFlag())
            this.setCreate_uid(srfdomain.getCreate_uid());

    }

    public List<Resource_calendar_attendanceDTO> fromDOPage(List<Resource_calendar_attendance> poPage)   {
        if(poPage == null)
            return null;
        List<Resource_calendar_attendanceDTO> dtos=new ArrayList<Resource_calendar_attendanceDTO>();
        for(Resource_calendar_attendance domain : poPage) {
            Resource_calendar_attendanceDTO dto = new Resource_calendar_attendanceDTO();
            dto.fromDO(domain);
            dtos.add(dto);
        }
        return dtos;
    }
}

