package cn.ibizlab.odoo.service.odoo_crm.resource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.service.odoo_crm.dto.Crm_lead2opportunity_partnerDTO;
import cn.ibizlab.odoo.core.odoo_crm.domain.Crm_lead2opportunity_partner;
import cn.ibizlab.odoo.core.odoo_crm.service.ICrm_lead2opportunity_partnerService;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.core.odoo_crm.filter.Crm_lead2opportunity_partnerSearchContext;
import cn.ibizlab.odoo.util.log.IBIZLog;

@Slf4j
@IBIZLog
@Api(tags = {"Crm_lead2opportunity_partner" })
@RestController
@RequestMapping("")
public class Crm_lead2opportunity_partnerResource {

    @Autowired
    private ICrm_lead2opportunity_partnerService crm_lead2opportunity_partnerService;

    public ICrm_lead2opportunity_partnerService getCrm_lead2opportunity_partnerService() {
        return this.crm_lead2opportunity_partnerService;
    }

    @ApiOperation(value = "建立数据", tags = {"Crm_lead2opportunity_partner" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_crm/crm_lead2opportunity_partners")

    public ResponseEntity<Crm_lead2opportunity_partnerDTO> create(@RequestBody Crm_lead2opportunity_partnerDTO crm_lead2opportunity_partnerdto) {
        Crm_lead2opportunity_partnerDTO dto = new Crm_lead2opportunity_partnerDTO();
        Crm_lead2opportunity_partner domain = crm_lead2opportunity_partnerdto.toDO();
		crm_lead2opportunity_partnerService.create(domain);
        dto.fromDO(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "删除数据", tags = {"Crm_lead2opportunity_partner" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_crm/crm_lead2opportunity_partners/{crm_lead2opportunity_partner_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("crm_lead2opportunity_partner_id") Integer crm_lead2opportunity_partner_id) {
        Crm_lead2opportunity_partnerDTO crm_lead2opportunity_partnerdto = new Crm_lead2opportunity_partnerDTO();
		Crm_lead2opportunity_partner domain = new Crm_lead2opportunity_partner();
		crm_lead2opportunity_partnerdto.setId(crm_lead2opportunity_partner_id);
		domain.setId(crm_lead2opportunity_partner_id);
        Boolean rst = crm_lead2opportunity_partnerService.remove(domain.getId());
        if(rst){
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}else{
			return ResponseEntity.status(HttpStatus.OK).body(rst);
		}
    }

    @ApiOperation(value = "获取数据", tags = {"Crm_lead2opportunity_partner" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/odoo_crm/crm_lead2opportunity_partners/{crm_lead2opportunity_partner_id}")
    public ResponseEntity<Crm_lead2opportunity_partnerDTO> get(@PathVariable("crm_lead2opportunity_partner_id") Integer crm_lead2opportunity_partner_id) {
        Crm_lead2opportunity_partnerDTO dto = new Crm_lead2opportunity_partnerDTO();
        Crm_lead2opportunity_partner domain = crm_lead2opportunity_partnerService.get(crm_lead2opportunity_partner_id);
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "更新数据", tags = {"Crm_lead2opportunity_partner" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_crm/crm_lead2opportunity_partners/{crm_lead2opportunity_partner_id}")

    public ResponseEntity<Crm_lead2opportunity_partnerDTO> update(@PathVariable("crm_lead2opportunity_partner_id") Integer crm_lead2opportunity_partner_id, @RequestBody Crm_lead2opportunity_partnerDTO crm_lead2opportunity_partnerdto) {
		Crm_lead2opportunity_partner domain = crm_lead2opportunity_partnerdto.toDO();
        domain.setId(crm_lead2opportunity_partner_id);
		crm_lead2opportunity_partnerService.update(domain);
		Crm_lead2opportunity_partnerDTO dto = new Crm_lead2opportunity_partnerDTO();
		dto.fromDO(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "批删除数据", tags = {"Crm_lead2opportunity_partner" },  notes = "批删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/odoo_crm/crm_lead2opportunity_partners/removebatch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Crm_lead2opportunity_partnerDTO> crm_lead2opportunity_partnerdtos) {

        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "批建立数据", tags = {"Crm_lead2opportunity_partner" },  notes = "批建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/odoo_crm/crm_lead2opportunity_partners/createBatch")
    public ResponseEntity<Boolean> createBatchCrm_lead2opportunity_partner(@RequestBody List<Crm_lead2opportunity_partnerDTO> crm_lead2opportunity_partnerdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @ApiOperation(value = "批更新数据", tags = {"Crm_lead2opportunity_partner" },  notes = "批更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/odoo_crm/crm_lead2opportunity_partners/updateBatch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Crm_lead2opportunity_partnerDTO> crm_lead2opportunity_partnerdtos) {
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

	@ApiOperation(value = "获取默认查询", tags = {"Crm_lead2opportunity_partner" } ,notes = "获取默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/odoo_crm/crm_lead2opportunity_partners/fetchdefault")
	public ResponseEntity<Page<Crm_lead2opportunity_partnerDTO>> fetchDefault(Crm_lead2opportunity_partnerSearchContext context,Pageable pageable ,ServletRequest request) {
        context.setPageable(pageable);
        List<Crm_lead2opportunity_partnerDTO> list = new ArrayList<Crm_lead2opportunity_partnerDTO>();
        
        Page<Crm_lead2opportunity_partner> domains = crm_lead2opportunity_partnerService.searchDefault(context) ;
        for(Crm_lead2opportunity_partner crm_lead2opportunity_partner : domains.getContent()){
            Crm_lead2opportunity_partnerDTO dto = new Crm_lead2opportunity_partnerDTO();
            dto.fromDO(crm_lead2opportunity_partner);
            list.add(dto);
        }
	    return ResponseEntity.status(HttpStatus.OK).body(new PageImpl(list,context.getPageable(),domains.getTotalElements()));
	}


}
