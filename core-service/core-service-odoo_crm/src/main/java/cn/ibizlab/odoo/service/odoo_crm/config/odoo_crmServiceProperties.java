package cn.ibizlab.odoo.service.odoo_crm.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import lombok.Data;

@ConfigurationProperties(prefix = "service.odoo.crm")
@Data
public class odoo_crmServiceProperties {

	private boolean enabled;

	private boolean auth;


}