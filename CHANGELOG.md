### 2020-4-9 模型变更日志

| 序号 | 应用类别 | 说明 |
| ------ | ------ | ------ |
|1|	全局|	Sale_Order与Sale_Order_Line的实体关系变更为嵌套关系|
|2|	全局|	服务接口[ODOO标准服务]销售新增Sale_Order与Sale_Order_Line的接口关系|
|3|	全局|	服务接口[ODOO标准服务]接口实体Sale_Order_Line切换为从实体模式|
|4|	全局|	外部接口[ODOO接口]销售新增Sale_Order与Sale_Order_Line的接口关系|

### 2020-3-30 模型变更日志

* 初始化系统模型，提供Odoo服务接口相关业务模型