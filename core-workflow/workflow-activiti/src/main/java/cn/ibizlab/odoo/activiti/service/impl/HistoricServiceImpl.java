package cn.ibizlab.odoo.activiti.service.impl;

import cn.ibizlab.odoo.activiti.service.IHistoricService;
import org.activiti.engine.HistoryService;
import org.activiti.engine.history.HistoricActivityInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class HistoricServiceImpl implements IHistoricService {

	@Autowired
	HistoryService historyService;

	/**
	 * 
	 * 功能描述:查询流程历史步骤
	 *
	 * @see [相关类/方法](可选)
	 * @since [产品/模块版本](可选)
	 */
	public List<HistoricActivityInstance> historicWFStep(String instanceId) {

		List<HistoricActivityInstance> historicTasks = null;
		try {
			historicTasks= historyService.createHistoricActivityInstanceQuery()
					.processInstanceId(instanceId).list();
			if (historicTasks != null && historicTasks.size() > 0) {
				for (HistoricActivityInstance hai : historicTasks) {
					System.out.println(hai.getId());
					System.out.println("步骤ID：" + hai.getActivityId());
					System.out.println("步骤名称：" + hai.getActivityName());
					System.out.println("执行人：" + hai.getAssignee());
					System.out.println("*****************************************************************************");
				}
			}
		} catch (Exception e) {
			return historicTasks;
		}
		return historicTasks;
	}
}
