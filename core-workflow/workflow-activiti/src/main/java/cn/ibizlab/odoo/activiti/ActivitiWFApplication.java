package cn.ibizlab.odoo.activiti;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;

@SpringBootApplication
public class ActivitiWFApplication {

    public static void main(String[] args) {
        SpringApplicationBuilder builder=new SpringApplicationBuilder(ActivitiWFApplication.class);
        builder.run(args);
    }
}
