package cn.ibizlab.odoo.activiti.util;

import cn.ibizlab.odoo.activiti.entity.TaskInfo;
import org.activiti.engine.task.Task;
import org.springframework.stereotype.Component;

@Component
public class TaskUtil {

    public TaskInfo covert2Task(Task task){

        TaskInfo taskInfo=new TaskInfo();
        taskInfo.setId(task.getId());
        taskInfo.setName(task.getName());
        taskInfo.setAssignee(task.getAssignee());
        taskInfo.setCreateTime(task.getCreateTime());
        taskInfo.setExecutionId(task.getExecutionId());
        taskInfo.setProcessDefinitionId(task.getProcessDefinitionId());
        taskInfo.setProcessInstanceId(task.getProcessInstanceId());

        return taskInfo;
    }

}
