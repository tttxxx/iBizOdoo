package cn.ibizlab.odoo.activiti.resource;

import com.alibaba.fastjson.JSONObject;
import cn.ibizlab.odoo.activiti.entity.TaskInfo;
import cn.ibizlab.odoo.activiti.service.ITaskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import java.util.Map;


/**
 * 当前运行流程待办任务服务对象
 */
@RestController
@RequestMapping("/task")
public class TaskResource{

	@Autowired
	ITaskService taskService;

	/**
	 * 完成任务
	 * @param params taskId：任务标识 、variables：流程参数
	 * @return
	 */
	@RequestMapping(value = "/complete")
	public String complete(@RequestBody JSONObject params) {

		String taskId=params.getString("taskid");

		Map<String, Object> variables=params.getJSONObject("variables");

		if (StringUtils.isEmpty(taskId)) {
			return "taskId error";
		}
		taskService.complete(taskId,variables);

		return "success";
	}


	/**
	 * 获取流程实例当前所处步骤
	 * @param instanceId
	 * @return
	 */
	@RequestMapping(value = "/getcurrenttask")
	public TaskInfo getCurentTask(@RequestParam("instanceid") String instanceId) {

		TaskInfo taskInfo=null;
		if (StringUtils.isEmpty(instanceId)) {
			return taskInfo;
		}
		taskInfo =taskService.getCurentTask(instanceId);

		return taskInfo;
	}
}
