package cn.ibizlab.odoo.activiti.service;

import java.util.Map;

public interface IProcessService {

    /**
     * 部署工作流
     * @param resourceFilePath
     * @return
     */
    String deployWF(String resourceFilePath);

    /**
     * 移除部署工作流
     * @param deployId
     * @return
     */
    String deDeployWF(String deployId);

    /**
     * 启动工作流，建立流程实例
     * @param processId
     * @param variables
     * @return
     */
    String startWF(String processId, Map variables);

    /**
     * 删除工作流实例
     * @param instanceId
     * @return
     */
    String deleteWF(String instanceId);
}
