package cn.ibizlab.odoo.activiti.service;


import cn.ibizlab.odoo.activiti.entity.TaskInfo;
import com.alibaba.fastjson.JSONObject;
import org.activiti.engine.history.HistoricActivityInstance;
import java.util.List;

public interface IWFCoreService {

    /**
     * 工作流启动
     */
    String wfStart(JSONObject et);

    /**
     * 工作流提交处理
     */
    String wfSubmit(JSONObject et);

    /**
     * 工作流关闭
     */

     void wfClose(JSONObject et);

    /**
     * 工作流跳转
     */
     void wfGoto(JSONObject et);

    /**
     * 工作流重新启动
     */
     void wfRestart(JSONObject et);

    /**
     * 工作流撤回
     */
     void wfRollback(JSONObject et);

    /**
     * 工作流退回
     */
     void wfSendBack(JSONObject et);

    /**
     * 工作流重新分配（处理人）
     */
     void wfReassign(JSONObject et);

    /**
     * 工作流标记为已读
     */
     void wfMarkRead(JSONObject et);

     /**
      * 工作流部署
     */
     String wfDeploy(JSONObject et);

     /**
      * 获取流程当前步骤
     */
     TaskInfo getWFCurStep(JSONObject et);

     /**
      * 获取流程历史步骤
     */
     List<HistoricActivityInstance> getWFHisStep(JSONObject et);
}
