package cn.ibizlab.odoo.activiti.resource;

import cn.ibizlab.odoo.activiti.service.IHistoricService;
import org.activiti.engine.history.HistoricActivityInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * 历史流程信息服务对象
 */
@RestController
@RequestMapping("/history")
public class HistoricResource {

	@Autowired
	IHistoricService historicService;

	/**
	 * 查询历史流程步骤
	 * @param instanceId
	 * @return
	 */
	@RequestMapping(value = "/wfstep")
	@ResponseBody
	public List<HistoricActivityInstance> historicWFStep(@RequestParam("instanceid") String instanceId) {

		List<HistoricActivityInstance> historicTasks=null;
		if (StringUtils.isEmpty(instanceId)) {
			return historicTasks;
		}
		historicTasks=historicService.historicWFStep(instanceId);

		return historicTasks;
	}

}
