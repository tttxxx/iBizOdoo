package cn.ibizlab.odoo.client.odoo_gamification.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Igamification_goal_wizard;
import cn.ibizlab.odoo.core.client.service.Igamification_goal_wizardClientService;
import cn.ibizlab.odoo.client.odoo_gamification.model.gamification_goal_wizardImpl;
import cn.ibizlab.odoo.client.odoo_gamification.odooclient.Igamification_goal_wizardOdooClient;
import cn.ibizlab.odoo.client.odoo_gamification.odooclient.impl.gamification_goal_wizardOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[gamification_goal_wizard] 服务对象接口
 */
@Service
public class gamification_goal_wizardClientServiceImpl implements Igamification_goal_wizardClientService {
    @Autowired
    private  Igamification_goal_wizardOdooClient  gamification_goal_wizardOdooClient;

    public Igamification_goal_wizard createModel() {		
		return new gamification_goal_wizardImpl();
	}


        public void removeBatch(List<Igamification_goal_wizard> gamification_goal_wizards){
            
        }
        
        public void update(Igamification_goal_wizard gamification_goal_wizard){
this.gamification_goal_wizardOdooClient.update(gamification_goal_wizard) ;
        }
        
        public void create(Igamification_goal_wizard gamification_goal_wizard){
this.gamification_goal_wizardOdooClient.create(gamification_goal_wizard) ;
        }
        
        public void get(Igamification_goal_wizard gamification_goal_wizard){
            this.gamification_goal_wizardOdooClient.get(gamification_goal_wizard) ;
        }
        
        public void createBatch(List<Igamification_goal_wizard> gamification_goal_wizards){
            
        }
        
        public void updateBatch(List<Igamification_goal_wizard> gamification_goal_wizards){
            
        }
        
        public Page<Igamification_goal_wizard> search(SearchContext context){
            return this.gamification_goal_wizardOdooClient.search(context) ;
        }
        
        public void remove(Igamification_goal_wizard gamification_goal_wizard){
this.gamification_goal_wizardOdooClient.remove(gamification_goal_wizard) ;
        }
        
        public Page<Igamification_goal_wizard> select(SearchContext context){
            return null ;
        }
        

}

