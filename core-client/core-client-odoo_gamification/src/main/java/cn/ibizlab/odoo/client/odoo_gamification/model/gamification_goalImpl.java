package cn.ibizlab.odoo.client.odoo_gamification.model;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.math.BigInteger;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.core.client.model.Igamification_goal;
import cn.ibizlab.odoo.util.helper.OdooClientHelper;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang.ArrayUtils;
import org.springframework.util.StringUtils;

/**
 * 接口实体[gamification_goal] 对象
 */
public class gamification_goalImpl implements Igamification_goal,Serializable{

    /**
     * 挑战
     */
    public Integer challenge_id;

    @JsonIgnore
    public boolean challenge_idDirtyFlag;
    
    /**
     * 挑战
     */
    public String challenge_id_text;

    @JsonIgnore
    public boolean challenge_id_textDirtyFlag;
    
    /**
     * 关闭的目标
     */
    public String closed;

    @JsonIgnore
    public boolean closedDirtyFlag;
    
    /**
     * 完整性
     */
    public Double completeness;

    @JsonIgnore
    public boolean completenessDirtyFlag;
    
    /**
     * 计算模式
     */
    public String computation_mode;

    @JsonIgnore
    public boolean computation_modeDirtyFlag;
    
    /**
     * 创建时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp create_date;

    @JsonIgnore
    public boolean create_dateDirtyFlag;
    
    /**
     * 创建人
     */
    public Integer create_uid;

    @JsonIgnore
    public boolean create_uidDirtyFlag;
    
    /**
     * 创建人
     */
    public String create_uid_text;

    @JsonIgnore
    public boolean create_uid_textDirtyFlag;
    
    /**
     * 当前值
     */
    public Double current;

    @JsonIgnore
    public boolean currentDirtyFlag;
    
    /**
     * 目标绩效
     */
    public String definition_condition;

    @JsonIgnore
    public boolean definition_conditionDirtyFlag;
    
    /**
     * 定义说明
     */
    public String definition_description;

    @JsonIgnore
    public boolean definition_descriptionDirtyFlag;
    
    /**
     * 显示为
     */
    public String definition_display;

    @JsonIgnore
    public boolean definition_displayDirtyFlag;
    
    /**
     * 目标定义
     */
    public Integer definition_id;

    @JsonIgnore
    public boolean definition_idDirtyFlag;
    
    /**
     * 目标定义
     */
    public String definition_id_text;

    @JsonIgnore
    public boolean definition_id_textDirtyFlag;
    
    /**
     * 后缀
     */
    public String definition_suffix;

    @JsonIgnore
    public boolean definition_suffixDirtyFlag;
    
    /**
     * 显示名称
     */
    public String display_name;

    @JsonIgnore
    public boolean display_nameDirtyFlag;
    
    /**
     * 结束日期
     */
    public Timestamp end_date;

    @JsonIgnore
    public boolean end_dateDirtyFlag;
    
    /**
     * ID
     */
    public Integer id;

    @JsonIgnore
    public boolean idDirtyFlag;
    
    /**
     * 最近更新
     */
    public Timestamp last_update;

    @JsonIgnore
    public boolean last_updateDirtyFlag;
    
    /**
     * 挑战行
     */
    public Integer line_id;

    @JsonIgnore
    public boolean line_idDirtyFlag;
    
    /**
     * 挑战行
     */
    public String line_id_text;

    @JsonIgnore
    public boolean line_id_textDirtyFlag;
    
    /**
     * 提醒延迟
     */
    public Integer remind_update_delay;

    @JsonIgnore
    public boolean remind_update_delayDirtyFlag;
    
    /**
     * 开始日期
     */
    public Timestamp start_date;

    @JsonIgnore
    public boolean start_dateDirtyFlag;
    
    /**
     * 状态
     */
    public String state;

    @JsonIgnore
    public boolean stateDirtyFlag;
    
    /**
     * 达到
     */
    public Double target_goal;

    @JsonIgnore
    public boolean target_goalDirtyFlag;
    
    /**
     * 更新
     */
    public String to_update;

    @JsonIgnore
    public boolean to_updateDirtyFlag;
    
    /**
     * 用户
     */
    public Integer user_id;

    @JsonIgnore
    public boolean user_idDirtyFlag;
    
    /**
     * 用户
     */
    public String user_id_text;

    @JsonIgnore
    public boolean user_id_textDirtyFlag;
    
    /**
     * 最后更新时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp write_date;

    @JsonIgnore
    public boolean write_dateDirtyFlag;
    
    /**
     * 最后更新人
     */
    public Integer write_uid;

    @JsonIgnore
    public boolean write_uidDirtyFlag;
    
    /**
     * 最后更新人
     */
    public String write_uid_text;

    @JsonIgnore
    public boolean write_uid_textDirtyFlag;
    
    /**
     * 最后修改日
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp __last_update;

    @JsonIgnore
    public boolean __last_updateDirtyFlag;
    
    /**
     * 获取 [挑战]
     */
    @JsonProperty("challenge_id")
    public Integer getChallenge_id(){
        return this.challenge_id ;
    }

    /**
     * 设置 [挑战]
     */
    @JsonProperty("challenge_id")
    public void setChallenge_id(Integer  challenge_id){
        this.challenge_id = challenge_id ;
        this.challenge_idDirtyFlag = true ;
    }

     /**
     * 获取 [挑战]脏标记
     */
    @JsonIgnore
    public boolean getChallenge_idDirtyFlag(){
        return this.challenge_idDirtyFlag ;
    }   

    /**
     * 获取 [挑战]
     */
    @JsonProperty("challenge_id_text")
    public String getChallenge_id_text(){
        return this.challenge_id_text ;
    }

    /**
     * 设置 [挑战]
     */
    @JsonProperty("challenge_id_text")
    public void setChallenge_id_text(String  challenge_id_text){
        this.challenge_id_text = challenge_id_text ;
        this.challenge_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [挑战]脏标记
     */
    @JsonIgnore
    public boolean getChallenge_id_textDirtyFlag(){
        return this.challenge_id_textDirtyFlag ;
    }   

    /**
     * 获取 [关闭的目标]
     */
    @JsonProperty("closed")
    public String getClosed(){
        return this.closed ;
    }

    /**
     * 设置 [关闭的目标]
     */
    @JsonProperty("closed")
    public void setClosed(String  closed){
        this.closed = closed ;
        this.closedDirtyFlag = true ;
    }

     /**
     * 获取 [关闭的目标]脏标记
     */
    @JsonIgnore
    public boolean getClosedDirtyFlag(){
        return this.closedDirtyFlag ;
    }   

    /**
     * 获取 [完整性]
     */
    @JsonProperty("completeness")
    public Double getCompleteness(){
        return this.completeness ;
    }

    /**
     * 设置 [完整性]
     */
    @JsonProperty("completeness")
    public void setCompleteness(Double  completeness){
        this.completeness = completeness ;
        this.completenessDirtyFlag = true ;
    }

     /**
     * 获取 [完整性]脏标记
     */
    @JsonIgnore
    public boolean getCompletenessDirtyFlag(){
        return this.completenessDirtyFlag ;
    }   

    /**
     * 获取 [计算模式]
     */
    @JsonProperty("computation_mode")
    public String getComputation_mode(){
        return this.computation_mode ;
    }

    /**
     * 设置 [计算模式]
     */
    @JsonProperty("computation_mode")
    public void setComputation_mode(String  computation_mode){
        this.computation_mode = computation_mode ;
        this.computation_modeDirtyFlag = true ;
    }

     /**
     * 获取 [计算模式]脏标记
     */
    @JsonIgnore
    public boolean getComputation_modeDirtyFlag(){
        return this.computation_modeDirtyFlag ;
    }   

    /**
     * 获取 [创建时间]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return this.create_date ;
    }

    /**
     * 设置 [创建时间]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

     /**
     * 获取 [创建时间]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return this.create_dateDirtyFlag ;
    }   

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return this.create_uid ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

     /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return this.create_uidDirtyFlag ;
    }   

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return this.create_uid_text ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

     /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return this.create_uid_textDirtyFlag ;
    }   

    /**
     * 获取 [当前值]
     */
    @JsonProperty("current")
    public Double getCurrent(){
        return this.current ;
    }

    /**
     * 设置 [当前值]
     */
    @JsonProperty("current")
    public void setCurrent(Double  current){
        this.current = current ;
        this.currentDirtyFlag = true ;
    }

     /**
     * 获取 [当前值]脏标记
     */
    @JsonIgnore
    public boolean getCurrentDirtyFlag(){
        return this.currentDirtyFlag ;
    }   

    /**
     * 获取 [目标绩效]
     */
    @JsonProperty("definition_condition")
    public String getDefinition_condition(){
        return this.definition_condition ;
    }

    /**
     * 设置 [目标绩效]
     */
    @JsonProperty("definition_condition")
    public void setDefinition_condition(String  definition_condition){
        this.definition_condition = definition_condition ;
        this.definition_conditionDirtyFlag = true ;
    }

     /**
     * 获取 [目标绩效]脏标记
     */
    @JsonIgnore
    public boolean getDefinition_conditionDirtyFlag(){
        return this.definition_conditionDirtyFlag ;
    }   

    /**
     * 获取 [定义说明]
     */
    @JsonProperty("definition_description")
    public String getDefinition_description(){
        return this.definition_description ;
    }

    /**
     * 设置 [定义说明]
     */
    @JsonProperty("definition_description")
    public void setDefinition_description(String  definition_description){
        this.definition_description = definition_description ;
        this.definition_descriptionDirtyFlag = true ;
    }

     /**
     * 获取 [定义说明]脏标记
     */
    @JsonIgnore
    public boolean getDefinition_descriptionDirtyFlag(){
        return this.definition_descriptionDirtyFlag ;
    }   

    /**
     * 获取 [显示为]
     */
    @JsonProperty("definition_display")
    public String getDefinition_display(){
        return this.definition_display ;
    }

    /**
     * 设置 [显示为]
     */
    @JsonProperty("definition_display")
    public void setDefinition_display(String  definition_display){
        this.definition_display = definition_display ;
        this.definition_displayDirtyFlag = true ;
    }

     /**
     * 获取 [显示为]脏标记
     */
    @JsonIgnore
    public boolean getDefinition_displayDirtyFlag(){
        return this.definition_displayDirtyFlag ;
    }   

    /**
     * 获取 [目标定义]
     */
    @JsonProperty("definition_id")
    public Integer getDefinition_id(){
        return this.definition_id ;
    }

    /**
     * 设置 [目标定义]
     */
    @JsonProperty("definition_id")
    public void setDefinition_id(Integer  definition_id){
        this.definition_id = definition_id ;
        this.definition_idDirtyFlag = true ;
    }

     /**
     * 获取 [目标定义]脏标记
     */
    @JsonIgnore
    public boolean getDefinition_idDirtyFlag(){
        return this.definition_idDirtyFlag ;
    }   

    /**
     * 获取 [目标定义]
     */
    @JsonProperty("definition_id_text")
    public String getDefinition_id_text(){
        return this.definition_id_text ;
    }

    /**
     * 设置 [目标定义]
     */
    @JsonProperty("definition_id_text")
    public void setDefinition_id_text(String  definition_id_text){
        this.definition_id_text = definition_id_text ;
        this.definition_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [目标定义]脏标记
     */
    @JsonIgnore
    public boolean getDefinition_id_textDirtyFlag(){
        return this.definition_id_textDirtyFlag ;
    }   

    /**
     * 获取 [后缀]
     */
    @JsonProperty("definition_suffix")
    public String getDefinition_suffix(){
        return this.definition_suffix ;
    }

    /**
     * 设置 [后缀]
     */
    @JsonProperty("definition_suffix")
    public void setDefinition_suffix(String  definition_suffix){
        this.definition_suffix = definition_suffix ;
        this.definition_suffixDirtyFlag = true ;
    }

     /**
     * 获取 [后缀]脏标记
     */
    @JsonIgnore
    public boolean getDefinition_suffixDirtyFlag(){
        return this.definition_suffixDirtyFlag ;
    }   

    /**
     * 获取 [显示名称]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return this.display_name ;
    }

    /**
     * 设置 [显示名称]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

     /**
     * 获取 [显示名称]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return this.display_nameDirtyFlag ;
    }   

    /**
     * 获取 [结束日期]
     */
    @JsonProperty("end_date")
    public Timestamp getEnd_date(){
        return this.end_date ;
    }

    /**
     * 设置 [结束日期]
     */
    @JsonProperty("end_date")
    public void setEnd_date(Timestamp  end_date){
        this.end_date = end_date ;
        this.end_dateDirtyFlag = true ;
    }

     /**
     * 获取 [结束日期]脏标记
     */
    @JsonIgnore
    public boolean getEnd_dateDirtyFlag(){
        return this.end_dateDirtyFlag ;
    }   

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return this.id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

     /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return this.idDirtyFlag ;
    }   

    /**
     * 获取 [最近更新]
     */
    @JsonProperty("last_update")
    public Timestamp getLast_update(){
        return this.last_update ;
    }

    /**
     * 设置 [最近更新]
     */
    @JsonProperty("last_update")
    public void setLast_update(Timestamp  last_update){
        this.last_update = last_update ;
        this.last_updateDirtyFlag = true ;
    }

     /**
     * 获取 [最近更新]脏标记
     */
    @JsonIgnore
    public boolean getLast_updateDirtyFlag(){
        return this.last_updateDirtyFlag ;
    }   

    /**
     * 获取 [挑战行]
     */
    @JsonProperty("line_id")
    public Integer getLine_id(){
        return this.line_id ;
    }

    /**
     * 设置 [挑战行]
     */
    @JsonProperty("line_id")
    public void setLine_id(Integer  line_id){
        this.line_id = line_id ;
        this.line_idDirtyFlag = true ;
    }

     /**
     * 获取 [挑战行]脏标记
     */
    @JsonIgnore
    public boolean getLine_idDirtyFlag(){
        return this.line_idDirtyFlag ;
    }   

    /**
     * 获取 [挑战行]
     */
    @JsonProperty("line_id_text")
    public String getLine_id_text(){
        return this.line_id_text ;
    }

    /**
     * 设置 [挑战行]
     */
    @JsonProperty("line_id_text")
    public void setLine_id_text(String  line_id_text){
        this.line_id_text = line_id_text ;
        this.line_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [挑战行]脏标记
     */
    @JsonIgnore
    public boolean getLine_id_textDirtyFlag(){
        return this.line_id_textDirtyFlag ;
    }   

    /**
     * 获取 [提醒延迟]
     */
    @JsonProperty("remind_update_delay")
    public Integer getRemind_update_delay(){
        return this.remind_update_delay ;
    }

    /**
     * 设置 [提醒延迟]
     */
    @JsonProperty("remind_update_delay")
    public void setRemind_update_delay(Integer  remind_update_delay){
        this.remind_update_delay = remind_update_delay ;
        this.remind_update_delayDirtyFlag = true ;
    }

     /**
     * 获取 [提醒延迟]脏标记
     */
    @JsonIgnore
    public boolean getRemind_update_delayDirtyFlag(){
        return this.remind_update_delayDirtyFlag ;
    }   

    /**
     * 获取 [开始日期]
     */
    @JsonProperty("start_date")
    public Timestamp getStart_date(){
        return this.start_date ;
    }

    /**
     * 设置 [开始日期]
     */
    @JsonProperty("start_date")
    public void setStart_date(Timestamp  start_date){
        this.start_date = start_date ;
        this.start_dateDirtyFlag = true ;
    }

     /**
     * 获取 [开始日期]脏标记
     */
    @JsonIgnore
    public boolean getStart_dateDirtyFlag(){
        return this.start_dateDirtyFlag ;
    }   

    /**
     * 获取 [状态]
     */
    @JsonProperty("state")
    public String getState(){
        return this.state ;
    }

    /**
     * 设置 [状态]
     */
    @JsonProperty("state")
    public void setState(String  state){
        this.state = state ;
        this.stateDirtyFlag = true ;
    }

     /**
     * 获取 [状态]脏标记
     */
    @JsonIgnore
    public boolean getStateDirtyFlag(){
        return this.stateDirtyFlag ;
    }   

    /**
     * 获取 [达到]
     */
    @JsonProperty("target_goal")
    public Double getTarget_goal(){
        return this.target_goal ;
    }

    /**
     * 设置 [达到]
     */
    @JsonProperty("target_goal")
    public void setTarget_goal(Double  target_goal){
        this.target_goal = target_goal ;
        this.target_goalDirtyFlag = true ;
    }

     /**
     * 获取 [达到]脏标记
     */
    @JsonIgnore
    public boolean getTarget_goalDirtyFlag(){
        return this.target_goalDirtyFlag ;
    }   

    /**
     * 获取 [更新]
     */
    @JsonProperty("to_update")
    public String getTo_update(){
        return this.to_update ;
    }

    /**
     * 设置 [更新]
     */
    @JsonProperty("to_update")
    public void setTo_update(String  to_update){
        this.to_update = to_update ;
        this.to_updateDirtyFlag = true ;
    }

     /**
     * 获取 [更新]脏标记
     */
    @JsonIgnore
    public boolean getTo_updateDirtyFlag(){
        return this.to_updateDirtyFlag ;
    }   

    /**
     * 获取 [用户]
     */
    @JsonProperty("user_id")
    public Integer getUser_id(){
        return this.user_id ;
    }

    /**
     * 设置 [用户]
     */
    @JsonProperty("user_id")
    public void setUser_id(Integer  user_id){
        this.user_id = user_id ;
        this.user_idDirtyFlag = true ;
    }

     /**
     * 获取 [用户]脏标记
     */
    @JsonIgnore
    public boolean getUser_idDirtyFlag(){
        return this.user_idDirtyFlag ;
    }   

    /**
     * 获取 [用户]
     */
    @JsonProperty("user_id_text")
    public String getUser_id_text(){
        return this.user_id_text ;
    }

    /**
     * 设置 [用户]
     */
    @JsonProperty("user_id_text")
    public void setUser_id_text(String  user_id_text){
        this.user_id_text = user_id_text ;
        this.user_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [用户]脏标记
     */
    @JsonIgnore
    public boolean getUser_id_textDirtyFlag(){
        return this.user_id_textDirtyFlag ;
    }   

    /**
     * 获取 [最后更新时间]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return this.write_date ;
    }

    /**
     * 设置 [最后更新时间]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新时间]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return this.write_dateDirtyFlag ;
    }   

    /**
     * 获取 [最后更新人]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return this.write_uid ;
    }

    /**
     * 设置 [最后更新人]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新人]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return this.write_uidDirtyFlag ;
    }   

    /**
     * 获取 [最后更新人]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return this.write_uid_text ;
    }

    /**
     * 设置 [最后更新人]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新人]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return this.write_uid_textDirtyFlag ;
    }   

    /**
     * 获取 [最后修改日]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return this.__last_update ;
    }

    /**
     * 设置 [最后修改日]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

     /**
     * 获取 [最后修改日]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return this.__last_updateDirtyFlag ;
    }   



    public void fromMap(Map<String, Object> map) throws Exception {
		if(!(map.get("challenge_id") instanceof Boolean)&& map.get("challenge_id")!=null){
			Object[] objs = (Object[])map.get("challenge_id");
			if(objs.length > 0){
				this.setChallenge_id((Integer)objs[0]);
			}
		}
		if(!(map.get("challenge_id") instanceof Boolean)&& map.get("challenge_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("challenge_id");
			if(objs.length > 1){
				this.setChallenge_id_text((String)objs[1]);
			}
		}
		if(map.get("closed") instanceof Boolean){
			this.setClosed(((Boolean)map.get("closed"))? "true" : "false");
		}
		if(!(map.get("completeness") instanceof Boolean)&& map.get("completeness")!=null){
			this.setCompleteness((Double)map.get("completeness"));
		}
		if(!(map.get("computation_mode") instanceof Boolean)&& map.get("computation_mode")!=null){
			this.setComputation_mode((String)map.get("computation_mode"));
		}
		if(!(map.get("create_date") instanceof Boolean)&& map.get("create_date")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse((String)map.get("create_date"));
   			this.setCreate_date(new Timestamp(parse.getTime()));
		}
		if(!(map.get("create_uid") instanceof Boolean)&& map.get("create_uid")!=null){
			Object[] objs = (Object[])map.get("create_uid");
			if(objs.length > 0){
				this.setCreate_uid((Integer)objs[0]);
			}
		}
		if(!(map.get("create_uid") instanceof Boolean)&& map.get("create_uid")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("create_uid");
			if(objs.length > 1){
				this.setCreate_uid_text((String)objs[1]);
			}
		}
		if(!(map.get("current") instanceof Boolean)&& map.get("current")!=null){
			this.setCurrent((Double)map.get("current"));
		}
		if(!(map.get("definition_condition") instanceof Boolean)&& map.get("definition_condition")!=null){
			this.setDefinition_condition((String)map.get("definition_condition"));
		}
		if(!(map.get("definition_description") instanceof Boolean)&& map.get("definition_description")!=null){
			this.setDefinition_description((String)map.get("definition_description"));
		}
		if(!(map.get("definition_display") instanceof Boolean)&& map.get("definition_display")!=null){
			this.setDefinition_display((String)map.get("definition_display"));
		}
		if(!(map.get("definition_id") instanceof Boolean)&& map.get("definition_id")!=null){
			Object[] objs = (Object[])map.get("definition_id");
			if(objs.length > 0){
				this.setDefinition_id((Integer)objs[0]);
			}
		}
		if(!(map.get("definition_id") instanceof Boolean)&& map.get("definition_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("definition_id");
			if(objs.length > 1){
				this.setDefinition_id_text((String)objs[1]);
			}
		}
		if(!(map.get("definition_suffix") instanceof Boolean)&& map.get("definition_suffix")!=null){
			this.setDefinition_suffix((String)map.get("definition_suffix"));
		}
		if(!(map.get("display_name") instanceof Boolean)&& map.get("display_name")!=null){
			this.setDisplay_name((String)map.get("display_name"));
		}
		if(!(map.get("end_date") instanceof Boolean)&& map.get("end_date")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd").parse((String)map.get("end_date"));
   			this.setEnd_date(new Timestamp(parse.getTime()));
		}
		if(!(map.get("id") instanceof Boolean)&& map.get("id")!=null){
			this.setId((Integer)map.get("id"));
		}
		if(!(map.get("last_update") instanceof Boolean)&& map.get("last_update")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd").parse((String)map.get("last_update"));
   			this.setLast_update(new Timestamp(parse.getTime()));
		}
		if(!(map.get("line_id") instanceof Boolean)&& map.get("line_id")!=null){
			Object[] objs = (Object[])map.get("line_id");
			if(objs.length > 0){
				this.setLine_id((Integer)objs[0]);
			}
		}
		if(!(map.get("line_id") instanceof Boolean)&& map.get("line_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("line_id");
			if(objs.length > 1){
				this.setLine_id_text((String)objs[1]);
			}
		}
		if(!(map.get("remind_update_delay") instanceof Boolean)&& map.get("remind_update_delay")!=null){
			this.setRemind_update_delay((Integer)map.get("remind_update_delay"));
		}
		if(!(map.get("start_date") instanceof Boolean)&& map.get("start_date")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd").parse((String)map.get("start_date"));
   			this.setStart_date(new Timestamp(parse.getTime()));
		}
		if(!(map.get("state") instanceof Boolean)&& map.get("state")!=null){
			this.setState((String)map.get("state"));
		}
		if(!(map.get("target_goal") instanceof Boolean)&& map.get("target_goal")!=null){
			this.setTarget_goal((Double)map.get("target_goal"));
		}
		if(map.get("to_update") instanceof Boolean){
			this.setTo_update(((Boolean)map.get("to_update"))? "true" : "false");
		}
		if(!(map.get("user_id") instanceof Boolean)&& map.get("user_id")!=null){
			Object[] objs = (Object[])map.get("user_id");
			if(objs.length > 0){
				this.setUser_id((Integer)objs[0]);
			}
		}
		if(!(map.get("user_id") instanceof Boolean)&& map.get("user_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("user_id");
			if(objs.length > 1){
				this.setUser_id_text((String)objs[1]);
			}
		}
		if(!(map.get("write_date") instanceof Boolean)&& map.get("write_date")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse((String)map.get("write_date"));
   			this.setWrite_date(new Timestamp(parse.getTime()));
		}
		if(!(map.get("write_uid") instanceof Boolean)&& map.get("write_uid")!=null){
			Object[] objs = (Object[])map.get("write_uid");
			if(objs.length > 0){
				this.setWrite_uid((Integer)objs[0]);
			}
		}
		if(!(map.get("write_uid") instanceof Boolean)&& map.get("write_uid")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("write_uid");
			if(objs.length > 1){
				this.setWrite_uid_text((String)objs[1]);
			}
		}
		if(!(map.get("__last_update") instanceof Boolean)&& map.get("__last_update")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse((String)map.get("__last_update"));
   			this.set__last_update(new Timestamp(parse.getTime()));
		}
	}

	public Map<String, Object> toMap() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		if(this.getChallenge_id()!=null&&this.getChallenge_idDirtyFlag()){
			map.put("challenge_id",this.getChallenge_id());
		}else if(this.getChallenge_idDirtyFlag()){
			map.put("challenge_id",false);
		}
		if(this.getChallenge_id_text()!=null&&this.getChallenge_id_textDirtyFlag()){
			//忽略文本外键challenge_id_text
		}else if(this.getChallenge_id_textDirtyFlag()){
			map.put("challenge_id",false);
		}
		if(this.getClosed()!=null&&this.getClosedDirtyFlag()){
			map.put("closed",Boolean.parseBoolean(this.getClosed()));		
		}		if(this.getCompleteness()!=null&&this.getCompletenessDirtyFlag()){
			map.put("completeness",this.getCompleteness());
		}else if(this.getCompletenessDirtyFlag()){
			map.put("completeness",false);
		}
		if(this.getComputation_mode()!=null&&this.getComputation_modeDirtyFlag()){
			map.put("computation_mode",this.getComputation_mode());
		}else if(this.getComputation_modeDirtyFlag()){
			map.put("computation_mode",false);
		}
		if(this.getCreate_date()!=null&&this.getCreate_dateDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String datetimeStr = sdf.format(this.getCreate_date());
			map.put("create_date",datetimeStr);
		}else if(this.getCreate_dateDirtyFlag()){
			map.put("create_date",false);
		}
		if(this.getCreate_uid()!=null&&this.getCreate_uidDirtyFlag()){
			map.put("create_uid",this.getCreate_uid());
		}else if(this.getCreate_uidDirtyFlag()){
			map.put("create_uid",false);
		}
		if(this.getCreate_uid_text()!=null&&this.getCreate_uid_textDirtyFlag()){
			//忽略文本外键create_uid_text
		}else if(this.getCreate_uid_textDirtyFlag()){
			map.put("create_uid",false);
		}
		if(this.getCurrent()!=null&&this.getCurrentDirtyFlag()){
			map.put("current",this.getCurrent());
		}else if(this.getCurrentDirtyFlag()){
			map.put("current",false);
		}
		if(this.getDefinition_condition()!=null&&this.getDefinition_conditionDirtyFlag()){
			map.put("definition_condition",this.getDefinition_condition());
		}else if(this.getDefinition_conditionDirtyFlag()){
			map.put("definition_condition",false);
		}
		if(this.getDefinition_description()!=null&&this.getDefinition_descriptionDirtyFlag()){
			map.put("definition_description",this.getDefinition_description());
		}else if(this.getDefinition_descriptionDirtyFlag()){
			map.put("definition_description",false);
		}
		if(this.getDefinition_display()!=null&&this.getDefinition_displayDirtyFlag()){
			map.put("definition_display",this.getDefinition_display());
		}else if(this.getDefinition_displayDirtyFlag()){
			map.put("definition_display",false);
		}
		if(this.getDefinition_id()!=null&&this.getDefinition_idDirtyFlag()){
			map.put("definition_id",this.getDefinition_id());
		}else if(this.getDefinition_idDirtyFlag()){
			map.put("definition_id",false);
		}
		if(this.getDefinition_id_text()!=null&&this.getDefinition_id_textDirtyFlag()){
			//忽略文本外键definition_id_text
		}else if(this.getDefinition_id_textDirtyFlag()){
			map.put("definition_id",false);
		}
		if(this.getDefinition_suffix()!=null&&this.getDefinition_suffixDirtyFlag()){
			map.put("definition_suffix",this.getDefinition_suffix());
		}else if(this.getDefinition_suffixDirtyFlag()){
			map.put("definition_suffix",false);
		}
		if(this.getDisplay_name()!=null&&this.getDisplay_nameDirtyFlag()){
			map.put("display_name",this.getDisplay_name());
		}else if(this.getDisplay_nameDirtyFlag()){
			map.put("display_name",false);
		}
		if(this.getEnd_date()!=null&&this.getEnd_dateDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			String datetimeStr = sdf.format(this.getEnd_date());
			map.put("end_date",datetimeStr);
		}else if(this.getEnd_dateDirtyFlag()){
			map.put("end_date",false);
		}
		if(this.getId()!=null&&this.getIdDirtyFlag()){
			map.put("id",this.getId());
		}else if(this.getIdDirtyFlag()){
			map.put("id",false);
		}
		if(this.getLast_update()!=null&&this.getLast_updateDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			String datetimeStr = sdf.format(this.getLast_update());
			map.put("last_update",datetimeStr);
		}else if(this.getLast_updateDirtyFlag()){
			map.put("last_update",false);
		}
		if(this.getLine_id()!=null&&this.getLine_idDirtyFlag()){
			map.put("line_id",this.getLine_id());
		}else if(this.getLine_idDirtyFlag()){
			map.put("line_id",false);
		}
		if(this.getLine_id_text()!=null&&this.getLine_id_textDirtyFlag()){
			//忽略文本外键line_id_text
		}else if(this.getLine_id_textDirtyFlag()){
			map.put("line_id",false);
		}
		if(this.getRemind_update_delay()!=null&&this.getRemind_update_delayDirtyFlag()){
			map.put("remind_update_delay",this.getRemind_update_delay());
		}else if(this.getRemind_update_delayDirtyFlag()){
			map.put("remind_update_delay",false);
		}
		if(this.getStart_date()!=null&&this.getStart_dateDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			String datetimeStr = sdf.format(this.getStart_date());
			map.put("start_date",datetimeStr);
		}else if(this.getStart_dateDirtyFlag()){
			map.put("start_date",false);
		}
		if(this.getState()!=null&&this.getStateDirtyFlag()){
			map.put("state",this.getState());
		}else if(this.getStateDirtyFlag()){
			map.put("state",false);
		}
		if(this.getTarget_goal()!=null&&this.getTarget_goalDirtyFlag()){
			map.put("target_goal",this.getTarget_goal());
		}else if(this.getTarget_goalDirtyFlag()){
			map.put("target_goal",false);
		}
		if(this.getTo_update()!=null&&this.getTo_updateDirtyFlag()){
			map.put("to_update",Boolean.parseBoolean(this.getTo_update()));		
		}		if(this.getUser_id()!=null&&this.getUser_idDirtyFlag()){
			map.put("user_id",this.getUser_id());
		}else if(this.getUser_idDirtyFlag()){
			map.put("user_id",false);
		}
		if(this.getUser_id_text()!=null&&this.getUser_id_textDirtyFlag()){
			//忽略文本外键user_id_text
		}else if(this.getUser_id_textDirtyFlag()){
			map.put("user_id",false);
		}
		if(this.getWrite_date()!=null&&this.getWrite_dateDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String datetimeStr = sdf.format(this.getWrite_date());
			map.put("write_date",datetimeStr);
		}else if(this.getWrite_dateDirtyFlag()){
			map.put("write_date",false);
		}
		if(this.getWrite_uid()!=null&&this.getWrite_uidDirtyFlag()){
			map.put("write_uid",this.getWrite_uid());
		}else if(this.getWrite_uidDirtyFlag()){
			map.put("write_uid",false);
		}
		if(this.getWrite_uid_text()!=null&&this.getWrite_uid_textDirtyFlag()){
			//忽略文本外键write_uid_text
		}else if(this.getWrite_uid_textDirtyFlag()){
			map.put("write_uid",false);
		}
		if(this.get__last_update()!=null&&this.get__last_updateDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String datetimeStr = sdf.format(this.get__last_update());
			map.put("__last_update",datetimeStr);
		}else if(this.get__last_updateDirtyFlag()){
			map.put("__last_update",false);
		}
		return map;
	}

}
