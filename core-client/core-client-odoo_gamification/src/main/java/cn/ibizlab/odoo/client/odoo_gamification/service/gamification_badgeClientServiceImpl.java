package cn.ibizlab.odoo.client.odoo_gamification.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Igamification_badge;
import cn.ibizlab.odoo.core.client.service.Igamification_badgeClientService;
import cn.ibizlab.odoo.client.odoo_gamification.model.gamification_badgeImpl;
import cn.ibizlab.odoo.client.odoo_gamification.odooclient.Igamification_badgeOdooClient;
import cn.ibizlab.odoo.client.odoo_gamification.odooclient.impl.gamification_badgeOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[gamification_badge] 服务对象接口
 */
@Service
public class gamification_badgeClientServiceImpl implements Igamification_badgeClientService {
    @Autowired
    private  Igamification_badgeOdooClient  gamification_badgeOdooClient;

    public Igamification_badge createModel() {		
		return new gamification_badgeImpl();
	}


        public void remove(Igamification_badge gamification_badge){
this.gamification_badgeOdooClient.remove(gamification_badge) ;
        }
        
        public void get(Igamification_badge gamification_badge){
            this.gamification_badgeOdooClient.get(gamification_badge) ;
        }
        
        public void removeBatch(List<Igamification_badge> gamification_badges){
            
        }
        
        public Page<Igamification_badge> search(SearchContext context){
            return this.gamification_badgeOdooClient.search(context) ;
        }
        
        public void createBatch(List<Igamification_badge> gamification_badges){
            
        }
        
        public void updateBatch(List<Igamification_badge> gamification_badges){
            
        }
        
        public void update(Igamification_badge gamification_badge){
this.gamification_badgeOdooClient.update(gamification_badge) ;
        }
        
        public void create(Igamification_badge gamification_badge){
this.gamification_badgeOdooClient.create(gamification_badge) ;
        }
        
        public Page<Igamification_badge> select(SearchContext context){
            return null ;
        }
        

}

