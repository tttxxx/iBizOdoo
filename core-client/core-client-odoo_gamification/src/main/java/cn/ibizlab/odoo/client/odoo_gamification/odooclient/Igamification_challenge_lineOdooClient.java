package cn.ibizlab.odoo.client.odoo_gamification.odooclient;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.core.client.model.Igamification_challenge_line;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[gamification_challenge_line] 服务对象客户端接口
 */
public interface Igamification_challenge_lineOdooClient {
    
        public void create(Igamification_challenge_line gamification_challenge_line);

        public void update(Igamification_challenge_line gamification_challenge_line);

        public Page<Igamification_challenge_line> search(SearchContext context);

        public void updateBatch(Igamification_challenge_line gamification_challenge_line);

        public void removeBatch(Igamification_challenge_line gamification_challenge_line);

        public void get(Igamification_challenge_line gamification_challenge_line);

        public void createBatch(Igamification_challenge_line gamification_challenge_line);

        public void remove(Igamification_challenge_line gamification_challenge_line);

        public List<Igamification_challenge_line> select();


}