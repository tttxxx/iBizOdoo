package cn.ibizlab.odoo.client.odoo_gamification.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import lombok.Data;

@ConfigurationProperties(prefix = "client.odoo.gamification")
@Data
public class odoo_gamificationClientProperties {

	private String tokenUrl ;

	private String clientId ;

	private String clientSecret ;

}
