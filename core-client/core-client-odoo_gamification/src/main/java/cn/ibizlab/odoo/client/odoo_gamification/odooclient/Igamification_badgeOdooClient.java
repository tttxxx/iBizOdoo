package cn.ibizlab.odoo.client.odoo_gamification.odooclient;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.core.client.model.Igamification_badge;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[gamification_badge] 服务对象客户端接口
 */
public interface Igamification_badgeOdooClient {
    
        public void remove(Igamification_badge gamification_badge);

        public void get(Igamification_badge gamification_badge);

        public void removeBatch(Igamification_badge gamification_badge);

        public Page<Igamification_badge> search(SearchContext context);

        public void createBatch(Igamification_badge gamification_badge);

        public void updateBatch(Igamification_badge gamification_badge);

        public void update(Igamification_badge gamification_badge);

        public void create(Igamification_badge gamification_badge);

        public List<Igamification_badge> select();


}