package cn.ibizlab.odoo.client.odoo_gamification.odooclient;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.core.client.model.Igamification_badge_user;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[gamification_badge_user] 服务对象客户端接口
 */
public interface Igamification_badge_userOdooClient {
    
        public void createBatch(Igamification_badge_user gamification_badge_user);

        public void get(Igamification_badge_user gamification_badge_user);

        public void updateBatch(Igamification_badge_user gamification_badge_user);

        public void update(Igamification_badge_user gamification_badge_user);

        public void removeBatch(Igamification_badge_user gamification_badge_user);

        public void create(Igamification_badge_user gamification_badge_user);

        public Page<Igamification_badge_user> search(SearchContext context);

        public void remove(Igamification_badge_user gamification_badge_user);

        public List<Igamification_badge_user> select();


}