package cn.ibizlab.odoo.client.odoo_gamification.odooclient;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.core.client.model.Igamification_goal_definition;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[gamification_goal_definition] 服务对象客户端接口
 */
public interface Igamification_goal_definitionOdooClient {
    
        public void updateBatch(Igamification_goal_definition gamification_goal_definition);

        public void update(Igamification_goal_definition gamification_goal_definition);

        public void get(Igamification_goal_definition gamification_goal_definition);

        public Page<Igamification_goal_definition> search(SearchContext context);

        public void removeBatch(Igamification_goal_definition gamification_goal_definition);

        public void create(Igamification_goal_definition gamification_goal_definition);

        public void remove(Igamification_goal_definition gamification_goal_definition);

        public void createBatch(Igamification_goal_definition gamification_goal_definition);

        public List<Igamification_goal_definition> select();


}