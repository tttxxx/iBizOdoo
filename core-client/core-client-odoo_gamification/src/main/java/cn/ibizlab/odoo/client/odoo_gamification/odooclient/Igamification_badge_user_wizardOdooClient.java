package cn.ibizlab.odoo.client.odoo_gamification.odooclient;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.core.client.model.Igamification_badge_user_wizard;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[gamification_badge_user_wizard] 服务对象客户端接口
 */
public interface Igamification_badge_user_wizardOdooClient {
    
        public void updateBatch(Igamification_badge_user_wizard gamification_badge_user_wizard);

        public void removeBatch(Igamification_badge_user_wizard gamification_badge_user_wizard);

        public void get(Igamification_badge_user_wizard gamification_badge_user_wizard);

        public void remove(Igamification_badge_user_wizard gamification_badge_user_wizard);

        public void create(Igamification_badge_user_wizard gamification_badge_user_wizard);

        public Page<Igamification_badge_user_wizard> search(SearchContext context);

        public void update(Igamification_badge_user_wizard gamification_badge_user_wizard);

        public void createBatch(Igamification_badge_user_wizard gamification_badge_user_wizard);

        public List<Igamification_badge_user_wizard> select();


}