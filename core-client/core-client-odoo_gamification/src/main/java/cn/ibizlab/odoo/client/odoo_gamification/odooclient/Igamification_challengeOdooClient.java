package cn.ibizlab.odoo.client.odoo_gamification.odooclient;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.core.client.model.Igamification_challenge;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[gamification_challenge] 服务对象客户端接口
 */
public interface Igamification_challengeOdooClient {
    
        public void create(Igamification_challenge gamification_challenge);

        public void createBatch(Igamification_challenge gamification_challenge);

        public Page<Igamification_challenge> search(SearchContext context);

        public void update(Igamification_challenge gamification_challenge);

        public void get(Igamification_challenge gamification_challenge);

        public void remove(Igamification_challenge gamification_challenge);

        public void updateBatch(Igamification_challenge gamification_challenge);

        public void removeBatch(Igamification_challenge gamification_challenge);

        public List<Igamification_challenge> select();


}