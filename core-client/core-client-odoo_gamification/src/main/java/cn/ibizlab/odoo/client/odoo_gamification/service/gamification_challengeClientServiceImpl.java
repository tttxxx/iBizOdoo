package cn.ibizlab.odoo.client.odoo_gamification.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Igamification_challenge;
import cn.ibizlab.odoo.core.client.service.Igamification_challengeClientService;
import cn.ibizlab.odoo.client.odoo_gamification.model.gamification_challengeImpl;
import cn.ibizlab.odoo.client.odoo_gamification.odooclient.Igamification_challengeOdooClient;
import cn.ibizlab.odoo.client.odoo_gamification.odooclient.impl.gamification_challengeOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[gamification_challenge] 服务对象接口
 */
@Service
public class gamification_challengeClientServiceImpl implements Igamification_challengeClientService {
    @Autowired
    private  Igamification_challengeOdooClient  gamification_challengeOdooClient;

    public Igamification_challenge createModel() {		
		return new gamification_challengeImpl();
	}


        public void create(Igamification_challenge gamification_challenge){
this.gamification_challengeOdooClient.create(gamification_challenge) ;
        }
        
        public void createBatch(List<Igamification_challenge> gamification_challenges){
            
        }
        
        public Page<Igamification_challenge> search(SearchContext context){
            return this.gamification_challengeOdooClient.search(context) ;
        }
        
        public void update(Igamification_challenge gamification_challenge){
this.gamification_challengeOdooClient.update(gamification_challenge) ;
        }
        
        public void get(Igamification_challenge gamification_challenge){
            this.gamification_challengeOdooClient.get(gamification_challenge) ;
        }
        
        public void remove(Igamification_challenge gamification_challenge){
this.gamification_challengeOdooClient.remove(gamification_challenge) ;
        }
        
        public void updateBatch(List<Igamification_challenge> gamification_challenges){
            
        }
        
        public void removeBatch(List<Igamification_challenge> gamification_challenges){
            
        }
        
        public Page<Igamification_challenge> select(SearchContext context){
            return null ;
        }
        

}

