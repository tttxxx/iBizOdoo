package cn.ibizlab.odoo.client.odoo_gamification.model;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.math.BigInteger;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.core.client.model.Igamification_challenge_line;
import cn.ibizlab.odoo.util.helper.OdooClientHelper;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang.ArrayUtils;
import org.springframework.util.StringUtils;

/**
 * 接口实体[gamification_challenge_line] 对象
 */
public class gamification_challenge_lineImpl implements Igamification_challenge_line,Serializable{

    /**
     * 挑战
     */
    public Integer challenge_id;

    @JsonIgnore
    public boolean challenge_idDirtyFlag;
    
    /**
     * 挑战
     */
    public String challenge_id_text;

    @JsonIgnore
    public boolean challenge_id_textDirtyFlag;
    
    /**
     * 目标绩效
     */
    public String condition;

    @JsonIgnore
    public boolean conditionDirtyFlag;
    
    /**
     * 创建时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp create_date;

    @JsonIgnore
    public boolean create_dateDirtyFlag;
    
    /**
     * 创建人
     */
    public Integer create_uid;

    @JsonIgnore
    public boolean create_uidDirtyFlag;
    
    /**
     * 创建人
     */
    public String create_uid_text;

    @JsonIgnore
    public boolean create_uid_textDirtyFlag;
    
    /**
     * 后缀
     */
    public String definition_full_suffix;

    @JsonIgnore
    public boolean definition_full_suffixDirtyFlag;
    
    /**
     * 目标定义
     */
    public Integer definition_id;

    @JsonIgnore
    public boolean definition_idDirtyFlag;
    
    /**
     * 货币
     */
    public String definition_monetary;

    @JsonIgnore
    public boolean definition_monetaryDirtyFlag;
    
    /**
     * 单位
     */
    public String definition_suffix;

    @JsonIgnore
    public boolean definition_suffixDirtyFlag;
    
    /**
     * 显示名称
     */
    public String display_name;

    @JsonIgnore
    public boolean display_nameDirtyFlag;
    
    /**
     * ID
     */
    public Integer id;

    @JsonIgnore
    public boolean idDirtyFlag;
    
    /**
     * 名称
     */
    public String name;

    @JsonIgnore
    public boolean nameDirtyFlag;
    
    /**
     * 序号
     */
    public Integer sequence;

    @JsonIgnore
    public boolean sequenceDirtyFlag;
    
    /**
     * 要达到的目标值
     */
    public Double target_goal;

    @JsonIgnore
    public boolean target_goalDirtyFlag;
    
    /**
     * 最后更新时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp write_date;

    @JsonIgnore
    public boolean write_dateDirtyFlag;
    
    /**
     * 最后更新人
     */
    public Integer write_uid;

    @JsonIgnore
    public boolean write_uidDirtyFlag;
    
    /**
     * 最后更新人
     */
    public String write_uid_text;

    @JsonIgnore
    public boolean write_uid_textDirtyFlag;
    
    /**
     * 最后修改日
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp __last_update;

    @JsonIgnore
    public boolean __last_updateDirtyFlag;
    
    /**
     * 获取 [挑战]
     */
    @JsonProperty("challenge_id")
    public Integer getChallenge_id(){
        return this.challenge_id ;
    }

    /**
     * 设置 [挑战]
     */
    @JsonProperty("challenge_id")
    public void setChallenge_id(Integer  challenge_id){
        this.challenge_id = challenge_id ;
        this.challenge_idDirtyFlag = true ;
    }

     /**
     * 获取 [挑战]脏标记
     */
    @JsonIgnore
    public boolean getChallenge_idDirtyFlag(){
        return this.challenge_idDirtyFlag ;
    }   

    /**
     * 获取 [挑战]
     */
    @JsonProperty("challenge_id_text")
    public String getChallenge_id_text(){
        return this.challenge_id_text ;
    }

    /**
     * 设置 [挑战]
     */
    @JsonProperty("challenge_id_text")
    public void setChallenge_id_text(String  challenge_id_text){
        this.challenge_id_text = challenge_id_text ;
        this.challenge_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [挑战]脏标记
     */
    @JsonIgnore
    public boolean getChallenge_id_textDirtyFlag(){
        return this.challenge_id_textDirtyFlag ;
    }   

    /**
     * 获取 [目标绩效]
     */
    @JsonProperty("condition")
    public String getCondition(){
        return this.condition ;
    }

    /**
     * 设置 [目标绩效]
     */
    @JsonProperty("condition")
    public void setCondition(String  condition){
        this.condition = condition ;
        this.conditionDirtyFlag = true ;
    }

     /**
     * 获取 [目标绩效]脏标记
     */
    @JsonIgnore
    public boolean getConditionDirtyFlag(){
        return this.conditionDirtyFlag ;
    }   

    /**
     * 获取 [创建时间]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return this.create_date ;
    }

    /**
     * 设置 [创建时间]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

     /**
     * 获取 [创建时间]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return this.create_dateDirtyFlag ;
    }   

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return this.create_uid ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

     /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return this.create_uidDirtyFlag ;
    }   

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return this.create_uid_text ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

     /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return this.create_uid_textDirtyFlag ;
    }   

    /**
     * 获取 [后缀]
     */
    @JsonProperty("definition_full_suffix")
    public String getDefinition_full_suffix(){
        return this.definition_full_suffix ;
    }

    /**
     * 设置 [后缀]
     */
    @JsonProperty("definition_full_suffix")
    public void setDefinition_full_suffix(String  definition_full_suffix){
        this.definition_full_suffix = definition_full_suffix ;
        this.definition_full_suffixDirtyFlag = true ;
    }

     /**
     * 获取 [后缀]脏标记
     */
    @JsonIgnore
    public boolean getDefinition_full_suffixDirtyFlag(){
        return this.definition_full_suffixDirtyFlag ;
    }   

    /**
     * 获取 [目标定义]
     */
    @JsonProperty("definition_id")
    public Integer getDefinition_id(){
        return this.definition_id ;
    }

    /**
     * 设置 [目标定义]
     */
    @JsonProperty("definition_id")
    public void setDefinition_id(Integer  definition_id){
        this.definition_id = definition_id ;
        this.definition_idDirtyFlag = true ;
    }

     /**
     * 获取 [目标定义]脏标记
     */
    @JsonIgnore
    public boolean getDefinition_idDirtyFlag(){
        return this.definition_idDirtyFlag ;
    }   

    /**
     * 获取 [货币]
     */
    @JsonProperty("definition_monetary")
    public String getDefinition_monetary(){
        return this.definition_monetary ;
    }

    /**
     * 设置 [货币]
     */
    @JsonProperty("definition_monetary")
    public void setDefinition_monetary(String  definition_monetary){
        this.definition_monetary = definition_monetary ;
        this.definition_monetaryDirtyFlag = true ;
    }

     /**
     * 获取 [货币]脏标记
     */
    @JsonIgnore
    public boolean getDefinition_monetaryDirtyFlag(){
        return this.definition_monetaryDirtyFlag ;
    }   

    /**
     * 获取 [单位]
     */
    @JsonProperty("definition_suffix")
    public String getDefinition_suffix(){
        return this.definition_suffix ;
    }

    /**
     * 设置 [单位]
     */
    @JsonProperty("definition_suffix")
    public void setDefinition_suffix(String  definition_suffix){
        this.definition_suffix = definition_suffix ;
        this.definition_suffixDirtyFlag = true ;
    }

     /**
     * 获取 [单位]脏标记
     */
    @JsonIgnore
    public boolean getDefinition_suffixDirtyFlag(){
        return this.definition_suffixDirtyFlag ;
    }   

    /**
     * 获取 [显示名称]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return this.display_name ;
    }

    /**
     * 设置 [显示名称]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

     /**
     * 获取 [显示名称]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return this.display_nameDirtyFlag ;
    }   

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return this.id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

     /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return this.idDirtyFlag ;
    }   

    /**
     * 获取 [名称]
     */
    @JsonProperty("name")
    public String getName(){
        return this.name ;
    }

    /**
     * 设置 [名称]
     */
    @JsonProperty("name")
    public void setName(String  name){
        this.name = name ;
        this.nameDirtyFlag = true ;
    }

     /**
     * 获取 [名称]脏标记
     */
    @JsonIgnore
    public boolean getNameDirtyFlag(){
        return this.nameDirtyFlag ;
    }   

    /**
     * 获取 [序号]
     */
    @JsonProperty("sequence")
    public Integer getSequence(){
        return this.sequence ;
    }

    /**
     * 设置 [序号]
     */
    @JsonProperty("sequence")
    public void setSequence(Integer  sequence){
        this.sequence = sequence ;
        this.sequenceDirtyFlag = true ;
    }

     /**
     * 获取 [序号]脏标记
     */
    @JsonIgnore
    public boolean getSequenceDirtyFlag(){
        return this.sequenceDirtyFlag ;
    }   

    /**
     * 获取 [要达到的目标值]
     */
    @JsonProperty("target_goal")
    public Double getTarget_goal(){
        return this.target_goal ;
    }

    /**
     * 设置 [要达到的目标值]
     */
    @JsonProperty("target_goal")
    public void setTarget_goal(Double  target_goal){
        this.target_goal = target_goal ;
        this.target_goalDirtyFlag = true ;
    }

     /**
     * 获取 [要达到的目标值]脏标记
     */
    @JsonIgnore
    public boolean getTarget_goalDirtyFlag(){
        return this.target_goalDirtyFlag ;
    }   

    /**
     * 获取 [最后更新时间]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return this.write_date ;
    }

    /**
     * 设置 [最后更新时间]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新时间]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return this.write_dateDirtyFlag ;
    }   

    /**
     * 获取 [最后更新人]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return this.write_uid ;
    }

    /**
     * 设置 [最后更新人]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新人]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return this.write_uidDirtyFlag ;
    }   

    /**
     * 获取 [最后更新人]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return this.write_uid_text ;
    }

    /**
     * 设置 [最后更新人]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新人]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return this.write_uid_textDirtyFlag ;
    }   

    /**
     * 获取 [最后修改日]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return this.__last_update ;
    }

    /**
     * 设置 [最后修改日]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

     /**
     * 获取 [最后修改日]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return this.__last_updateDirtyFlag ;
    }   



    public void fromMap(Map<String, Object> map) throws Exception {
		if(!(map.get("challenge_id") instanceof Boolean)&& map.get("challenge_id")!=null){
			Object[] objs = (Object[])map.get("challenge_id");
			if(objs.length > 0){
				this.setChallenge_id((Integer)objs[0]);
			}
		}
		if(!(map.get("challenge_id") instanceof Boolean)&& map.get("challenge_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("challenge_id");
			if(objs.length > 1){
				this.setChallenge_id_text((String)objs[1]);
			}
		}
		if(!(map.get("condition") instanceof Boolean)&& map.get("condition")!=null){
			this.setCondition((String)map.get("condition"));
		}
		if(!(map.get("create_date") instanceof Boolean)&& map.get("create_date")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse((String)map.get("create_date"));
   			this.setCreate_date(new Timestamp(parse.getTime()));
		}
		if(!(map.get("create_uid") instanceof Boolean)&& map.get("create_uid")!=null){
			Object[] objs = (Object[])map.get("create_uid");
			if(objs.length > 0){
				this.setCreate_uid((Integer)objs[0]);
			}
		}
		if(!(map.get("create_uid") instanceof Boolean)&& map.get("create_uid")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("create_uid");
			if(objs.length > 1){
				this.setCreate_uid_text((String)objs[1]);
			}
		}
		if(!(map.get("definition_full_suffix") instanceof Boolean)&& map.get("definition_full_suffix")!=null){
			this.setDefinition_full_suffix((String)map.get("definition_full_suffix"));
		}
		if(!(map.get("definition_id") instanceof Boolean)&& map.get("definition_id")!=null){
			Object[] objs = (Object[])map.get("definition_id");
			if(objs.length > 0){
				this.setDefinition_id((Integer)objs[0]);
			}
		}
		if(map.get("definition_monetary") instanceof Boolean){
			this.setDefinition_monetary(((Boolean)map.get("definition_monetary"))? "true" : "false");
		}
		if(!(map.get("definition_suffix") instanceof Boolean)&& map.get("definition_suffix")!=null){
			this.setDefinition_suffix((String)map.get("definition_suffix"));
		}
		if(!(map.get("display_name") instanceof Boolean)&& map.get("display_name")!=null){
			this.setDisplay_name((String)map.get("display_name"));
		}
		if(!(map.get("id") instanceof Boolean)&& map.get("id")!=null){
			this.setId((Integer)map.get("id"));
		}
		if(!(map.get("name") instanceof Boolean)&& map.get("name")!=null){
			this.setName((String)map.get("name"));
		}
		if(!(map.get("sequence") instanceof Boolean)&& map.get("sequence")!=null){
			this.setSequence((Integer)map.get("sequence"));
		}
		if(!(map.get("target_goal") instanceof Boolean)&& map.get("target_goal")!=null){
			this.setTarget_goal((Double)map.get("target_goal"));
		}
		if(!(map.get("write_date") instanceof Boolean)&& map.get("write_date")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse((String)map.get("write_date"));
   			this.setWrite_date(new Timestamp(parse.getTime()));
		}
		if(!(map.get("write_uid") instanceof Boolean)&& map.get("write_uid")!=null){
			Object[] objs = (Object[])map.get("write_uid");
			if(objs.length > 0){
				this.setWrite_uid((Integer)objs[0]);
			}
		}
		if(!(map.get("write_uid") instanceof Boolean)&& map.get("write_uid")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("write_uid");
			if(objs.length > 1){
				this.setWrite_uid_text((String)objs[1]);
			}
		}
		if(!(map.get("__last_update") instanceof Boolean)&& map.get("__last_update")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse((String)map.get("__last_update"));
   			this.set__last_update(new Timestamp(parse.getTime()));
		}
	}

	public Map<String, Object> toMap() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		if(this.getChallenge_id()!=null&&this.getChallenge_idDirtyFlag()){
			map.put("challenge_id",this.getChallenge_id());
		}else if(this.getChallenge_idDirtyFlag()){
			map.put("challenge_id",false);
		}
		if(this.getChallenge_id_text()!=null&&this.getChallenge_id_textDirtyFlag()){
			//忽略文本外键challenge_id_text
		}else if(this.getChallenge_id_textDirtyFlag()){
			map.put("challenge_id",false);
		}
		if(this.getCondition()!=null&&this.getConditionDirtyFlag()){
			map.put("condition",this.getCondition());
		}else if(this.getConditionDirtyFlag()){
			map.put("condition",false);
		}
		if(this.getCreate_date()!=null&&this.getCreate_dateDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String datetimeStr = sdf.format(this.getCreate_date());
			map.put("create_date",datetimeStr);
		}else if(this.getCreate_dateDirtyFlag()){
			map.put("create_date",false);
		}
		if(this.getCreate_uid()!=null&&this.getCreate_uidDirtyFlag()){
			map.put("create_uid",this.getCreate_uid());
		}else if(this.getCreate_uidDirtyFlag()){
			map.put("create_uid",false);
		}
		if(this.getCreate_uid_text()!=null&&this.getCreate_uid_textDirtyFlag()){
			//忽略文本外键create_uid_text
		}else if(this.getCreate_uid_textDirtyFlag()){
			map.put("create_uid",false);
		}
		if(this.getDefinition_full_suffix()!=null&&this.getDefinition_full_suffixDirtyFlag()){
			map.put("definition_full_suffix",this.getDefinition_full_suffix());
		}else if(this.getDefinition_full_suffixDirtyFlag()){
			map.put("definition_full_suffix",false);
		}
		if(this.getDefinition_id()!=null&&this.getDefinition_idDirtyFlag()){
			map.put("definition_id",this.getDefinition_id());
		}else if(this.getDefinition_idDirtyFlag()){
			map.put("definition_id",false);
		}
		if(this.getDefinition_monetary()!=null&&this.getDefinition_monetaryDirtyFlag()){
			map.put("definition_monetary",Boolean.parseBoolean(this.getDefinition_monetary()));		
		}		if(this.getDefinition_suffix()!=null&&this.getDefinition_suffixDirtyFlag()){
			map.put("definition_suffix",this.getDefinition_suffix());
		}else if(this.getDefinition_suffixDirtyFlag()){
			map.put("definition_suffix",false);
		}
		if(this.getDisplay_name()!=null&&this.getDisplay_nameDirtyFlag()){
			map.put("display_name",this.getDisplay_name());
		}else if(this.getDisplay_nameDirtyFlag()){
			map.put("display_name",false);
		}
		if(this.getId()!=null&&this.getIdDirtyFlag()){
			map.put("id",this.getId());
		}else if(this.getIdDirtyFlag()){
			map.put("id",false);
		}
		if(this.getName()!=null&&this.getNameDirtyFlag()){
			map.put("name",this.getName());
		}else if(this.getNameDirtyFlag()){
			map.put("name",false);
		}
		if(this.getSequence()!=null&&this.getSequenceDirtyFlag()){
			map.put("sequence",this.getSequence());
		}else if(this.getSequenceDirtyFlag()){
			map.put("sequence",false);
		}
		if(this.getTarget_goal()!=null&&this.getTarget_goalDirtyFlag()){
			map.put("target_goal",this.getTarget_goal());
		}else if(this.getTarget_goalDirtyFlag()){
			map.put("target_goal",false);
		}
		if(this.getWrite_date()!=null&&this.getWrite_dateDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String datetimeStr = sdf.format(this.getWrite_date());
			map.put("write_date",datetimeStr);
		}else if(this.getWrite_dateDirtyFlag()){
			map.put("write_date",false);
		}
		if(this.getWrite_uid()!=null&&this.getWrite_uidDirtyFlag()){
			map.put("write_uid",this.getWrite_uid());
		}else if(this.getWrite_uidDirtyFlag()){
			map.put("write_uid",false);
		}
		if(this.getWrite_uid_text()!=null&&this.getWrite_uid_textDirtyFlag()){
			//忽略文本外键write_uid_text
		}else if(this.getWrite_uid_textDirtyFlag()){
			map.put("write_uid",false);
		}
		if(this.get__last_update()!=null&&this.get__last_updateDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String datetimeStr = sdf.format(this.get__last_update());
			map.put("__last_update",datetimeStr);
		}else if(this.get__last_updateDirtyFlag()){
			map.put("__last_update",false);
		}
		return map;
	}

}
