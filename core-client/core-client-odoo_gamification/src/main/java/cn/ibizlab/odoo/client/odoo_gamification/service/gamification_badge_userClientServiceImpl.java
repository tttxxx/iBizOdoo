package cn.ibizlab.odoo.client.odoo_gamification.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Igamification_badge_user;
import cn.ibizlab.odoo.core.client.service.Igamification_badge_userClientService;
import cn.ibizlab.odoo.client.odoo_gamification.model.gamification_badge_userImpl;
import cn.ibizlab.odoo.client.odoo_gamification.odooclient.Igamification_badge_userOdooClient;
import cn.ibizlab.odoo.client.odoo_gamification.odooclient.impl.gamification_badge_userOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[gamification_badge_user] 服务对象接口
 */
@Service
public class gamification_badge_userClientServiceImpl implements Igamification_badge_userClientService {
    @Autowired
    private  Igamification_badge_userOdooClient  gamification_badge_userOdooClient;

    public Igamification_badge_user createModel() {		
		return new gamification_badge_userImpl();
	}


        public void createBatch(List<Igamification_badge_user> gamification_badge_users){
            
        }
        
        public void get(Igamification_badge_user gamification_badge_user){
            this.gamification_badge_userOdooClient.get(gamification_badge_user) ;
        }
        
        public void updateBatch(List<Igamification_badge_user> gamification_badge_users){
            
        }
        
        public void update(Igamification_badge_user gamification_badge_user){
this.gamification_badge_userOdooClient.update(gamification_badge_user) ;
        }
        
        public void removeBatch(List<Igamification_badge_user> gamification_badge_users){
            
        }
        
        public void create(Igamification_badge_user gamification_badge_user){
this.gamification_badge_userOdooClient.create(gamification_badge_user) ;
        }
        
        public Page<Igamification_badge_user> search(SearchContext context){
            return this.gamification_badge_userOdooClient.search(context) ;
        }
        
        public void remove(Igamification_badge_user gamification_badge_user){
this.gamification_badge_userOdooClient.remove(gamification_badge_user) ;
        }
        
        public Page<Igamification_badge_user> select(SearchContext context){
            return null ;
        }
        

}

