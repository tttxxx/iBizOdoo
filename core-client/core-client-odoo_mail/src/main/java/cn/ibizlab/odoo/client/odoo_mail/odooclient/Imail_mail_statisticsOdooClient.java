package cn.ibizlab.odoo.client.odoo_mail.odooclient;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.core.client.model.Imail_mail_statistics;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[mail_mail_statistics] 服务对象客户端接口
 */
public interface Imail_mail_statisticsOdooClient {
    
        public void remove(Imail_mail_statistics mail_mail_statistics);

        public Page<Imail_mail_statistics> search(SearchContext context);

        public void updateBatch(Imail_mail_statistics mail_mail_statistics);

        public void create(Imail_mail_statistics mail_mail_statistics);

        public void removeBatch(Imail_mail_statistics mail_mail_statistics);

        public void createBatch(Imail_mail_statistics mail_mail_statistics);

        public void update(Imail_mail_statistics mail_mail_statistics);

        public void get(Imail_mail_statistics mail_mail_statistics);

        public List<Imail_mail_statistics> select();


}