package cn.ibizlab.odoo.client.odoo_mail.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Imail_mass_mailing;
import cn.ibizlab.odoo.core.client.service.Imail_mass_mailingClientService;
import cn.ibizlab.odoo.client.odoo_mail.model.mail_mass_mailingImpl;
import cn.ibizlab.odoo.client.odoo_mail.odooclient.Imail_mass_mailingOdooClient;
import cn.ibizlab.odoo.client.odoo_mail.odooclient.impl.mail_mass_mailingOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[mail_mass_mailing] 服务对象接口
 */
@Service
public class mail_mass_mailingClientServiceImpl implements Imail_mass_mailingClientService {
    @Autowired
    private  Imail_mass_mailingOdooClient  mail_mass_mailingOdooClient;

    public Imail_mass_mailing createModel() {		
		return new mail_mass_mailingImpl();
	}


        public Page<Imail_mass_mailing> search(SearchContext context){
            return this.mail_mass_mailingOdooClient.search(context) ;
        }
        
        public void removeBatch(List<Imail_mass_mailing> mail_mass_mailings){
            
        }
        
        public void create(Imail_mass_mailing mail_mass_mailing){
this.mail_mass_mailingOdooClient.create(mail_mass_mailing) ;
        }
        
        public void remove(Imail_mass_mailing mail_mass_mailing){
this.mail_mass_mailingOdooClient.remove(mail_mass_mailing) ;
        }
        
        public void updateBatch(List<Imail_mass_mailing> mail_mass_mailings){
            
        }
        
        public void update(Imail_mass_mailing mail_mass_mailing){
this.mail_mass_mailingOdooClient.update(mail_mass_mailing) ;
        }
        
        public void createBatch(List<Imail_mass_mailing> mail_mass_mailings){
            
        }
        
        public void get(Imail_mass_mailing mail_mass_mailing){
            this.mail_mass_mailingOdooClient.get(mail_mass_mailing) ;
        }
        
        public Page<Imail_mass_mailing> select(SearchContext context){
            return null ;
        }
        

}

