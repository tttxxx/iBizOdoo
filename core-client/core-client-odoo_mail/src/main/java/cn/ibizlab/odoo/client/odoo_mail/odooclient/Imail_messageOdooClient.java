package cn.ibizlab.odoo.client.odoo_mail.odooclient;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.core.client.model.Imail_message;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[mail_message] 服务对象客户端接口
 */
public interface Imail_messageOdooClient {
    
        public void remove(Imail_message mail_message);

        public void updateBatch(Imail_message mail_message);

        public void create(Imail_message mail_message);

        public void createBatch(Imail_message mail_message);

        public void get(Imail_message mail_message);

        public void removeBatch(Imail_message mail_message);

        public Page<Imail_message> search(SearchContext context);

        public void update(Imail_message mail_message);

        public List<Imail_message> select();


}