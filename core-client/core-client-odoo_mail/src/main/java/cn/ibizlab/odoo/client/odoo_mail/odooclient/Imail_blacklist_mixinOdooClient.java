package cn.ibizlab.odoo.client.odoo_mail.odooclient;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.core.client.model.Imail_blacklist_mixin;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[mail_blacklist_mixin] 服务对象客户端接口
 */
public interface Imail_blacklist_mixinOdooClient {
    
        public void create(Imail_blacklist_mixin mail_blacklist_mixin);

        public void createBatch(Imail_blacklist_mixin mail_blacklist_mixin);

        public Page<Imail_blacklist_mixin> search(SearchContext context);

        public void updateBatch(Imail_blacklist_mixin mail_blacklist_mixin);

        public void remove(Imail_blacklist_mixin mail_blacklist_mixin);

        public void removeBatch(Imail_blacklist_mixin mail_blacklist_mixin);

        public void update(Imail_blacklist_mixin mail_blacklist_mixin);

        public void get(Imail_blacklist_mixin mail_blacklist_mixin);

        public List<Imail_blacklist_mixin> select();


}