package cn.ibizlab.odoo.client.odoo_mail.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Imail_activity_type;
import cn.ibizlab.odoo.core.client.service.Imail_activity_typeClientService;
import cn.ibizlab.odoo.client.odoo_mail.model.mail_activity_typeImpl;
import cn.ibizlab.odoo.client.odoo_mail.odooclient.Imail_activity_typeOdooClient;
import cn.ibizlab.odoo.client.odoo_mail.odooclient.impl.mail_activity_typeOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[mail_activity_type] 服务对象接口
 */
@Service
public class mail_activity_typeClientServiceImpl implements Imail_activity_typeClientService {
    @Autowired
    private  Imail_activity_typeOdooClient  mail_activity_typeOdooClient;

    public Imail_activity_type createModel() {		
		return new mail_activity_typeImpl();
	}


        public void update(Imail_activity_type mail_activity_type){
this.mail_activity_typeOdooClient.update(mail_activity_type) ;
        }
        
        public void create(Imail_activity_type mail_activity_type){
this.mail_activity_typeOdooClient.create(mail_activity_type) ;
        }
        
        public void get(Imail_activity_type mail_activity_type){
            this.mail_activity_typeOdooClient.get(mail_activity_type) ;
        }
        
        public void updateBatch(List<Imail_activity_type> mail_activity_types){
            
        }
        
        public void removeBatch(List<Imail_activity_type> mail_activity_types){
            
        }
        
        public Page<Imail_activity_type> search(SearchContext context){
            return this.mail_activity_typeOdooClient.search(context) ;
        }
        
        public void remove(Imail_activity_type mail_activity_type){
this.mail_activity_typeOdooClient.remove(mail_activity_type) ;
        }
        
        public void createBatch(List<Imail_activity_type> mail_activity_types){
            
        }
        
        public Page<Imail_activity_type> select(SearchContext context){
            return null ;
        }
        

}

