package cn.ibizlab.odoo.client.odoo_mail.odooclient;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.core.client.model.Imail_bot;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[mail_bot] 服务对象客户端接口
 */
public interface Imail_botOdooClient {
    
        public void update(Imail_bot mail_bot);

        public void create(Imail_bot mail_bot);

        public void createBatch(Imail_bot mail_bot);

        public void get(Imail_bot mail_bot);

        public void removeBatch(Imail_bot mail_bot);

        public void remove(Imail_bot mail_bot);

        public void updateBatch(Imail_bot mail_bot);

        public Page<Imail_bot> search(SearchContext context);

        public List<Imail_bot> select();


}