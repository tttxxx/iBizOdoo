package cn.ibizlab.odoo.client.odoo_mail.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Imail_resend_message;
import cn.ibizlab.odoo.core.client.service.Imail_resend_messageClientService;
import cn.ibizlab.odoo.client.odoo_mail.model.mail_resend_messageImpl;
import cn.ibizlab.odoo.client.odoo_mail.odooclient.Imail_resend_messageOdooClient;
import cn.ibizlab.odoo.client.odoo_mail.odooclient.impl.mail_resend_messageOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[mail_resend_message] 服务对象接口
 */
@Service
public class mail_resend_messageClientServiceImpl implements Imail_resend_messageClientService {
    @Autowired
    private  Imail_resend_messageOdooClient  mail_resend_messageOdooClient;

    public Imail_resend_message createModel() {		
		return new mail_resend_messageImpl();
	}


        public void updateBatch(List<Imail_resend_message> mail_resend_messages){
            
        }
        
        public void update(Imail_resend_message mail_resend_message){
this.mail_resend_messageOdooClient.update(mail_resend_message) ;
        }
        
        public void remove(Imail_resend_message mail_resend_message){
this.mail_resend_messageOdooClient.remove(mail_resend_message) ;
        }
        
        public void createBatch(List<Imail_resend_message> mail_resend_messages){
            
        }
        
        public void get(Imail_resend_message mail_resend_message){
            this.mail_resend_messageOdooClient.get(mail_resend_message) ;
        }
        
        public void removeBatch(List<Imail_resend_message> mail_resend_messages){
            
        }
        
        public Page<Imail_resend_message> search(SearchContext context){
            return this.mail_resend_messageOdooClient.search(context) ;
        }
        
        public void create(Imail_resend_message mail_resend_message){
this.mail_resend_messageOdooClient.create(mail_resend_message) ;
        }
        
        public Page<Imail_resend_message> select(SearchContext context){
            return null ;
        }
        

}

