package cn.ibizlab.odoo.client.odoo_mail.odooclient;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.core.client.model.Imail_message_subtype;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[mail_message_subtype] 服务对象客户端接口
 */
public interface Imail_message_subtypeOdooClient {
    
        public Page<Imail_message_subtype> search(SearchContext context);

        public void updateBatch(Imail_message_subtype mail_message_subtype);

        public void get(Imail_message_subtype mail_message_subtype);

        public void update(Imail_message_subtype mail_message_subtype);

        public void remove(Imail_message_subtype mail_message_subtype);

        public void removeBatch(Imail_message_subtype mail_message_subtype);

        public void create(Imail_message_subtype mail_message_subtype);

        public void createBatch(Imail_message_subtype mail_message_subtype);

        public List<Imail_message_subtype> select();


}