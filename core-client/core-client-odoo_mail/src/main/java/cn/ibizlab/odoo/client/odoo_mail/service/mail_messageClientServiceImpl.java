package cn.ibizlab.odoo.client.odoo_mail.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Imail_message;
import cn.ibizlab.odoo.core.client.service.Imail_messageClientService;
import cn.ibizlab.odoo.client.odoo_mail.model.mail_messageImpl;
import cn.ibizlab.odoo.client.odoo_mail.odooclient.Imail_messageOdooClient;
import cn.ibizlab.odoo.client.odoo_mail.odooclient.impl.mail_messageOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[mail_message] 服务对象接口
 */
@Service
public class mail_messageClientServiceImpl implements Imail_messageClientService {
    @Autowired
    private  Imail_messageOdooClient  mail_messageOdooClient;

    public Imail_message createModel() {		
		return new mail_messageImpl();
	}


        public void remove(Imail_message mail_message){
this.mail_messageOdooClient.remove(mail_message) ;
        }
        
        public void updateBatch(List<Imail_message> mail_messages){
            
        }
        
        public void create(Imail_message mail_message){
this.mail_messageOdooClient.create(mail_message) ;
        }
        
        public void createBatch(List<Imail_message> mail_messages){
            
        }
        
        public void get(Imail_message mail_message){
            this.mail_messageOdooClient.get(mail_message) ;
        }
        
        public void removeBatch(List<Imail_message> mail_messages){
            
        }
        
        public Page<Imail_message> search(SearchContext context){
            return this.mail_messageOdooClient.search(context) ;
        }
        
        public void update(Imail_message mail_message){
this.mail_messageOdooClient.update(mail_message) ;
        }
        
        public Page<Imail_message> select(SearchContext context){
            return null ;
        }
        

}

