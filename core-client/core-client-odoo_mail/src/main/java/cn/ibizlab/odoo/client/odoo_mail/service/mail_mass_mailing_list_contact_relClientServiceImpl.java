package cn.ibizlab.odoo.client.odoo_mail.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Imail_mass_mailing_list_contact_rel;
import cn.ibizlab.odoo.core.client.service.Imail_mass_mailing_list_contact_relClientService;
import cn.ibizlab.odoo.client.odoo_mail.model.mail_mass_mailing_list_contact_relImpl;
import cn.ibizlab.odoo.client.odoo_mail.odooclient.Imail_mass_mailing_list_contact_relOdooClient;
import cn.ibizlab.odoo.client.odoo_mail.odooclient.impl.mail_mass_mailing_list_contact_relOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[mail_mass_mailing_list_contact_rel] 服务对象接口
 */
@Service
public class mail_mass_mailing_list_contact_relClientServiceImpl implements Imail_mass_mailing_list_contact_relClientService {
    @Autowired
    private  Imail_mass_mailing_list_contact_relOdooClient  mail_mass_mailing_list_contact_relOdooClient;

    public Imail_mass_mailing_list_contact_rel createModel() {		
		return new mail_mass_mailing_list_contact_relImpl();
	}


        public Page<Imail_mass_mailing_list_contact_rel> search(SearchContext context){
            return this.mail_mass_mailing_list_contact_relOdooClient.search(context) ;
        }
        
        public void create(Imail_mass_mailing_list_contact_rel mail_mass_mailing_list_contact_rel){
this.mail_mass_mailing_list_contact_relOdooClient.create(mail_mass_mailing_list_contact_rel) ;
        }
        
        public void remove(Imail_mass_mailing_list_contact_rel mail_mass_mailing_list_contact_rel){
this.mail_mass_mailing_list_contact_relOdooClient.remove(mail_mass_mailing_list_contact_rel) ;
        }
        
        public void removeBatch(List<Imail_mass_mailing_list_contact_rel> mail_mass_mailing_list_contact_rels){
            
        }
        
        public void createBatch(List<Imail_mass_mailing_list_contact_rel> mail_mass_mailing_list_contact_rels){
            
        }
        
        public void update(Imail_mass_mailing_list_contact_rel mail_mass_mailing_list_contact_rel){
this.mail_mass_mailing_list_contact_relOdooClient.update(mail_mass_mailing_list_contact_rel) ;
        }
        
        public void updateBatch(List<Imail_mass_mailing_list_contact_rel> mail_mass_mailing_list_contact_rels){
            
        }
        
        public void get(Imail_mass_mailing_list_contact_rel mail_mass_mailing_list_contact_rel){
            this.mail_mass_mailing_list_contact_relOdooClient.get(mail_mass_mailing_list_contact_rel) ;
        }
        
        public Page<Imail_mass_mailing_list_contact_rel> select(SearchContext context){
            return null ;
        }
        

}

