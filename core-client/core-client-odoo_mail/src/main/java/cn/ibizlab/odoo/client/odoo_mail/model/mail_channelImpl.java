package cn.ibizlab.odoo.client.odoo_mail.model;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.math.BigInteger;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.core.client.model.Imail_channel;
import cn.ibizlab.odoo.util.helper.OdooClientHelper;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang.ArrayUtils;
import org.springframework.util.StringUtils;

/**
 * 接口实体[mail_channel] 对象
 */
public class mail_channelImpl implements Imail_channel,Serializable{

    /**
     * 安全联系人别名
     */
    public String alias_contact;

    @JsonIgnore
    public boolean alias_contactDirtyFlag;
    
    /**
     * 默认值
     */
    public String alias_defaults;

    @JsonIgnore
    public boolean alias_defaultsDirtyFlag;
    
    /**
     * 网域别名
     */
    public String alias_domain;

    @JsonIgnore
    public boolean alias_domainDirtyFlag;
    
    /**
     * 记录线索ID
     */
    public Integer alias_force_thread_id;

    @JsonIgnore
    public boolean alias_force_thread_idDirtyFlag;
    
    /**
     * 别名
     */
    public Integer alias_id;

    @JsonIgnore
    public boolean alias_idDirtyFlag;
    
    /**
     * 模型别名
     */
    public Integer alias_model_id;

    @JsonIgnore
    public boolean alias_model_idDirtyFlag;
    
    /**
     * 别名
     */
    public String alias_name;

    @JsonIgnore
    public boolean alias_nameDirtyFlag;
    
    /**
     * 上级模型
     */
    public Integer alias_parent_model_id;

    @JsonIgnore
    public boolean alias_parent_model_idDirtyFlag;
    
    /**
     * 上级记录ID
     */
    public Integer alias_parent_thread_id;

    @JsonIgnore
    public boolean alias_parent_thread_idDirtyFlag;
    
    /**
     * 所有者
     */
    public Integer alias_user_id;

    @JsonIgnore
    public boolean alias_user_idDirtyFlag;
    
    /**
     * 匿名用户姓名
     */
    public String anonymous_name;

    @JsonIgnore
    public boolean anonymous_nameDirtyFlag;
    
    /**
     * 最近一次查阅
     */
    public String channel_last_seen_partner_ids;

    @JsonIgnore
    public boolean channel_last_seen_partner_idsDirtyFlag;
    
    /**
     * 渠道消息
     */
    public String channel_message_ids;

    @JsonIgnore
    public boolean channel_message_idsDirtyFlag;
    
    /**
     * 监听器
     */
    public String channel_partner_ids;

    @JsonIgnore
    public boolean channel_partner_idsDirtyFlag;
    
    /**
     * 渠道类型
     */
    public String channel_type;

    @JsonIgnore
    public boolean channel_typeDirtyFlag;
    
    /**
     * 创建时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp create_date;

    @JsonIgnore
    public boolean create_dateDirtyFlag;
    
    /**
     * 创建人
     */
    public Integer create_uid;

    @JsonIgnore
    public boolean create_uidDirtyFlag;
    
    /**
     * 创建人
     */
    public String create_uid_text;

    @JsonIgnore
    public boolean create_uid_textDirtyFlag;
    
    /**
     * 说明
     */
    public String description;

    @JsonIgnore
    public boolean descriptionDirtyFlag;
    
    /**
     * 显示名称
     */
    public String display_name;

    @JsonIgnore
    public boolean display_nameDirtyFlag;
    
    /**
     * 以邮件形式发送
     */
    public String email_send;

    @JsonIgnore
    public boolean email_sendDirtyFlag;
    
    /**
     * 自动订阅
     */
    public String group_ids;

    @JsonIgnore
    public boolean group_idsDirtyFlag;
    
    /**
     * 经授权的群组
     */
    public Integer group_public_id;

    @JsonIgnore
    public boolean group_public_idDirtyFlag;
    
    /**
     * 经授权的群组
     */
    public String group_public_id_text;

    @JsonIgnore
    public boolean group_public_id_textDirtyFlag;
    
    /**
     * 隐私
     */
    public String ibizpublic;

    @JsonIgnore
    public boolean ibizpublicDirtyFlag;
    
    /**
     * ID
     */
    public Integer id;

    @JsonIgnore
    public boolean idDirtyFlag;
    
    /**
     * 照片
     */
    public byte[] image;

    @JsonIgnore
    public boolean imageDirtyFlag;
    
    /**
     * 中等尺寸照片
     */
    public byte[] image_medium;

    @JsonIgnore
    public boolean image_mediumDirtyFlag;
    
    /**
     * 小尺寸照片
     */
    public byte[] image_small;

    @JsonIgnore
    public boolean image_smallDirtyFlag;
    
    /**
     * 是聊天
     */
    public String is_chat;

    @JsonIgnore
    public boolean is_chatDirtyFlag;
    
    /**
     * 成员
     */
    public String is_member;

    @JsonIgnore
    public boolean is_memberDirtyFlag;
    
    /**
     * 管理员
     */
    public String is_moderator;

    @JsonIgnore
    public boolean is_moderatorDirtyFlag;
    
    /**
     * 已订阅
     */
    public String is_subscribed;

    @JsonIgnore
    public boolean is_subscribedDirtyFlag;
    
    /**
     * 渠道
     */
    public Integer livechat_channel_id;

    @JsonIgnore
    public boolean livechat_channel_idDirtyFlag;
    
    /**
     * 渠道
     */
    public String livechat_channel_id_text;

    @JsonIgnore
    public boolean livechat_channel_id_textDirtyFlag;
    
    /**
     * 附件数量
     */
    public Integer message_attachment_count;

    @JsonIgnore
    public boolean message_attachment_countDirtyFlag;
    
    /**
     * 关注者(渠道)
     */
    public String message_channel_ids;

    @JsonIgnore
    public boolean message_channel_idsDirtyFlag;
    
    /**
     * 关注者
     */
    public String message_follower_ids;

    @JsonIgnore
    public boolean message_follower_idsDirtyFlag;
    
    /**
     * 消息递送错误
     */
    public String message_has_error;

    @JsonIgnore
    public boolean message_has_errorDirtyFlag;
    
    /**
     * 错误个数
     */
    public Integer message_has_error_counter;

    @JsonIgnore
    public boolean message_has_error_counterDirtyFlag;
    
    /**
     * 消息
     */
    public String message_ids;

    @JsonIgnore
    public boolean message_idsDirtyFlag;
    
    /**
     * 关注者
     */
    public String message_is_follower;

    @JsonIgnore
    public boolean message_is_followerDirtyFlag;
    
    /**
     * 附件
     */
    public Integer message_main_attachment_id;

    @JsonIgnore
    public boolean message_main_attachment_idDirtyFlag;
    
    /**
     * 前置操作
     */
    public String message_needaction;

    @JsonIgnore
    public boolean message_needactionDirtyFlag;
    
    /**
     * 操作次数
     */
    public Integer message_needaction_counter;

    @JsonIgnore
    public boolean message_needaction_counterDirtyFlag;
    
    /**
     * 关注者(业务伙伴)
     */
    public String message_partner_ids;

    @JsonIgnore
    public boolean message_partner_idsDirtyFlag;
    
    /**
     * 未读消息
     */
    public String message_unread;

    @JsonIgnore
    public boolean message_unreadDirtyFlag;
    
    /**
     * 未读消息计数器
     */
    public Integer message_unread_counter;

    @JsonIgnore
    public boolean message_unread_counterDirtyFlag;
    
    /**
     * 管理频道
     */
    public String moderation;

    @JsonIgnore
    public boolean moderationDirtyFlag;
    
    /**
     * 管理EMail账户
     */
    public Integer moderation_count;

    @JsonIgnore
    public boolean moderation_countDirtyFlag;
    
    /**
     * 向新用户发送订阅指南
     */
    public String moderation_guidelines;

    @JsonIgnore
    public boolean moderation_guidelinesDirtyFlag;
    
    /**
     * 方针
     */
    public String moderation_guidelines_msg;

    @JsonIgnore
    public boolean moderation_guidelines_msgDirtyFlag;
    
    /**
     * 管理EMail
     */
    public String moderation_ids;

    @JsonIgnore
    public boolean moderation_idsDirtyFlag;
    
    /**
     * 自动通知
     */
    public String moderation_notify;

    @JsonIgnore
    public boolean moderation_notifyDirtyFlag;
    
    /**
     * 通知消息
     */
    public String moderation_notify_msg;

    @JsonIgnore
    public boolean moderation_notify_msgDirtyFlag;
    
    /**
     * 管理员
     */
    public String moderator_ids;

    @JsonIgnore
    public boolean moderator_idsDirtyFlag;
    
    /**
     * 名称
     */
    public String name;

    @JsonIgnore
    public boolean nameDirtyFlag;
    
    /**
     * 评级数
     */
    public Integer rating_count;

    @JsonIgnore
    public boolean rating_countDirtyFlag;
    
    /**
     * 评级
     */
    public String rating_ids;

    @JsonIgnore
    public boolean rating_idsDirtyFlag;
    
    /**
     * 最新反馈评级
     */
    public String rating_last_feedback;

    @JsonIgnore
    public boolean rating_last_feedbackDirtyFlag;
    
    /**
     * 最新图像评级
     */
    public byte[] rating_last_image;

    @JsonIgnore
    public boolean rating_last_imageDirtyFlag;
    
    /**
     * 最新值评级
     */
    public Double rating_last_value;

    @JsonIgnore
    public boolean rating_last_valueDirtyFlag;
    
    /**
     * 人力资源部门
     */
    public String subscription_department_ids;

    @JsonIgnore
    public boolean subscription_department_idsDirtyFlag;
    
    /**
     * UUID
     */
    public String uuid;

    @JsonIgnore
    public boolean uuidDirtyFlag;
    
    /**
     * 网站消息
     */
    public String website_message_ids;

    @JsonIgnore
    public boolean website_message_idsDirtyFlag;
    
    /**
     * 最后更新时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp write_date;

    @JsonIgnore
    public boolean write_dateDirtyFlag;
    
    /**
     * 最后更新者
     */
    public Integer write_uid;

    @JsonIgnore
    public boolean write_uidDirtyFlag;
    
    /**
     * 最后更新者
     */
    public String write_uid_text;

    @JsonIgnore
    public boolean write_uid_textDirtyFlag;
    
    /**
     * 最后修改日
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp __last_update;

    @JsonIgnore
    public boolean __last_updateDirtyFlag;
    
    /**
     * 获取 [安全联系人别名]
     */
    @JsonProperty("alias_contact")
    public String getAlias_contact(){
        return this.alias_contact ;
    }

    /**
     * 设置 [安全联系人别名]
     */
    @JsonProperty("alias_contact")
    public void setAlias_contact(String  alias_contact){
        this.alias_contact = alias_contact ;
        this.alias_contactDirtyFlag = true ;
    }

     /**
     * 获取 [安全联系人别名]脏标记
     */
    @JsonIgnore
    public boolean getAlias_contactDirtyFlag(){
        return this.alias_contactDirtyFlag ;
    }   

    /**
     * 获取 [默认值]
     */
    @JsonProperty("alias_defaults")
    public String getAlias_defaults(){
        return this.alias_defaults ;
    }

    /**
     * 设置 [默认值]
     */
    @JsonProperty("alias_defaults")
    public void setAlias_defaults(String  alias_defaults){
        this.alias_defaults = alias_defaults ;
        this.alias_defaultsDirtyFlag = true ;
    }

     /**
     * 获取 [默认值]脏标记
     */
    @JsonIgnore
    public boolean getAlias_defaultsDirtyFlag(){
        return this.alias_defaultsDirtyFlag ;
    }   

    /**
     * 获取 [网域别名]
     */
    @JsonProperty("alias_domain")
    public String getAlias_domain(){
        return this.alias_domain ;
    }

    /**
     * 设置 [网域别名]
     */
    @JsonProperty("alias_domain")
    public void setAlias_domain(String  alias_domain){
        this.alias_domain = alias_domain ;
        this.alias_domainDirtyFlag = true ;
    }

     /**
     * 获取 [网域别名]脏标记
     */
    @JsonIgnore
    public boolean getAlias_domainDirtyFlag(){
        return this.alias_domainDirtyFlag ;
    }   

    /**
     * 获取 [记录线索ID]
     */
    @JsonProperty("alias_force_thread_id")
    public Integer getAlias_force_thread_id(){
        return this.alias_force_thread_id ;
    }

    /**
     * 设置 [记录线索ID]
     */
    @JsonProperty("alias_force_thread_id")
    public void setAlias_force_thread_id(Integer  alias_force_thread_id){
        this.alias_force_thread_id = alias_force_thread_id ;
        this.alias_force_thread_idDirtyFlag = true ;
    }

     /**
     * 获取 [记录线索ID]脏标记
     */
    @JsonIgnore
    public boolean getAlias_force_thread_idDirtyFlag(){
        return this.alias_force_thread_idDirtyFlag ;
    }   

    /**
     * 获取 [别名]
     */
    @JsonProperty("alias_id")
    public Integer getAlias_id(){
        return this.alias_id ;
    }

    /**
     * 设置 [别名]
     */
    @JsonProperty("alias_id")
    public void setAlias_id(Integer  alias_id){
        this.alias_id = alias_id ;
        this.alias_idDirtyFlag = true ;
    }

     /**
     * 获取 [别名]脏标记
     */
    @JsonIgnore
    public boolean getAlias_idDirtyFlag(){
        return this.alias_idDirtyFlag ;
    }   

    /**
     * 获取 [模型别名]
     */
    @JsonProperty("alias_model_id")
    public Integer getAlias_model_id(){
        return this.alias_model_id ;
    }

    /**
     * 设置 [模型别名]
     */
    @JsonProperty("alias_model_id")
    public void setAlias_model_id(Integer  alias_model_id){
        this.alias_model_id = alias_model_id ;
        this.alias_model_idDirtyFlag = true ;
    }

     /**
     * 获取 [模型别名]脏标记
     */
    @JsonIgnore
    public boolean getAlias_model_idDirtyFlag(){
        return this.alias_model_idDirtyFlag ;
    }   

    /**
     * 获取 [别名]
     */
    @JsonProperty("alias_name")
    public String getAlias_name(){
        return this.alias_name ;
    }

    /**
     * 设置 [别名]
     */
    @JsonProperty("alias_name")
    public void setAlias_name(String  alias_name){
        this.alias_name = alias_name ;
        this.alias_nameDirtyFlag = true ;
    }

     /**
     * 获取 [别名]脏标记
     */
    @JsonIgnore
    public boolean getAlias_nameDirtyFlag(){
        return this.alias_nameDirtyFlag ;
    }   

    /**
     * 获取 [上级模型]
     */
    @JsonProperty("alias_parent_model_id")
    public Integer getAlias_parent_model_id(){
        return this.alias_parent_model_id ;
    }

    /**
     * 设置 [上级模型]
     */
    @JsonProperty("alias_parent_model_id")
    public void setAlias_parent_model_id(Integer  alias_parent_model_id){
        this.alias_parent_model_id = alias_parent_model_id ;
        this.alias_parent_model_idDirtyFlag = true ;
    }

     /**
     * 获取 [上级模型]脏标记
     */
    @JsonIgnore
    public boolean getAlias_parent_model_idDirtyFlag(){
        return this.alias_parent_model_idDirtyFlag ;
    }   

    /**
     * 获取 [上级记录ID]
     */
    @JsonProperty("alias_parent_thread_id")
    public Integer getAlias_parent_thread_id(){
        return this.alias_parent_thread_id ;
    }

    /**
     * 设置 [上级记录ID]
     */
    @JsonProperty("alias_parent_thread_id")
    public void setAlias_parent_thread_id(Integer  alias_parent_thread_id){
        this.alias_parent_thread_id = alias_parent_thread_id ;
        this.alias_parent_thread_idDirtyFlag = true ;
    }

     /**
     * 获取 [上级记录ID]脏标记
     */
    @JsonIgnore
    public boolean getAlias_parent_thread_idDirtyFlag(){
        return this.alias_parent_thread_idDirtyFlag ;
    }   

    /**
     * 获取 [所有者]
     */
    @JsonProperty("alias_user_id")
    public Integer getAlias_user_id(){
        return this.alias_user_id ;
    }

    /**
     * 设置 [所有者]
     */
    @JsonProperty("alias_user_id")
    public void setAlias_user_id(Integer  alias_user_id){
        this.alias_user_id = alias_user_id ;
        this.alias_user_idDirtyFlag = true ;
    }

     /**
     * 获取 [所有者]脏标记
     */
    @JsonIgnore
    public boolean getAlias_user_idDirtyFlag(){
        return this.alias_user_idDirtyFlag ;
    }   

    /**
     * 获取 [匿名用户姓名]
     */
    @JsonProperty("anonymous_name")
    public String getAnonymous_name(){
        return this.anonymous_name ;
    }

    /**
     * 设置 [匿名用户姓名]
     */
    @JsonProperty("anonymous_name")
    public void setAnonymous_name(String  anonymous_name){
        this.anonymous_name = anonymous_name ;
        this.anonymous_nameDirtyFlag = true ;
    }

     /**
     * 获取 [匿名用户姓名]脏标记
     */
    @JsonIgnore
    public boolean getAnonymous_nameDirtyFlag(){
        return this.anonymous_nameDirtyFlag ;
    }   

    /**
     * 获取 [最近一次查阅]
     */
    @JsonProperty("channel_last_seen_partner_ids")
    public String getChannel_last_seen_partner_ids(){
        return this.channel_last_seen_partner_ids ;
    }

    /**
     * 设置 [最近一次查阅]
     */
    @JsonProperty("channel_last_seen_partner_ids")
    public void setChannel_last_seen_partner_ids(String  channel_last_seen_partner_ids){
        this.channel_last_seen_partner_ids = channel_last_seen_partner_ids ;
        this.channel_last_seen_partner_idsDirtyFlag = true ;
    }

     /**
     * 获取 [最近一次查阅]脏标记
     */
    @JsonIgnore
    public boolean getChannel_last_seen_partner_idsDirtyFlag(){
        return this.channel_last_seen_partner_idsDirtyFlag ;
    }   

    /**
     * 获取 [渠道消息]
     */
    @JsonProperty("channel_message_ids")
    public String getChannel_message_ids(){
        return this.channel_message_ids ;
    }

    /**
     * 设置 [渠道消息]
     */
    @JsonProperty("channel_message_ids")
    public void setChannel_message_ids(String  channel_message_ids){
        this.channel_message_ids = channel_message_ids ;
        this.channel_message_idsDirtyFlag = true ;
    }

     /**
     * 获取 [渠道消息]脏标记
     */
    @JsonIgnore
    public boolean getChannel_message_idsDirtyFlag(){
        return this.channel_message_idsDirtyFlag ;
    }   

    /**
     * 获取 [监听器]
     */
    @JsonProperty("channel_partner_ids")
    public String getChannel_partner_ids(){
        return this.channel_partner_ids ;
    }

    /**
     * 设置 [监听器]
     */
    @JsonProperty("channel_partner_ids")
    public void setChannel_partner_ids(String  channel_partner_ids){
        this.channel_partner_ids = channel_partner_ids ;
        this.channel_partner_idsDirtyFlag = true ;
    }

     /**
     * 获取 [监听器]脏标记
     */
    @JsonIgnore
    public boolean getChannel_partner_idsDirtyFlag(){
        return this.channel_partner_idsDirtyFlag ;
    }   

    /**
     * 获取 [渠道类型]
     */
    @JsonProperty("channel_type")
    public String getChannel_type(){
        return this.channel_type ;
    }

    /**
     * 设置 [渠道类型]
     */
    @JsonProperty("channel_type")
    public void setChannel_type(String  channel_type){
        this.channel_type = channel_type ;
        this.channel_typeDirtyFlag = true ;
    }

     /**
     * 获取 [渠道类型]脏标记
     */
    @JsonIgnore
    public boolean getChannel_typeDirtyFlag(){
        return this.channel_typeDirtyFlag ;
    }   

    /**
     * 获取 [创建时间]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return this.create_date ;
    }

    /**
     * 设置 [创建时间]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

     /**
     * 获取 [创建时间]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return this.create_dateDirtyFlag ;
    }   

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return this.create_uid ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

     /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return this.create_uidDirtyFlag ;
    }   

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return this.create_uid_text ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

     /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return this.create_uid_textDirtyFlag ;
    }   

    /**
     * 获取 [说明]
     */
    @JsonProperty("description")
    public String getDescription(){
        return this.description ;
    }

    /**
     * 设置 [说明]
     */
    @JsonProperty("description")
    public void setDescription(String  description){
        this.description = description ;
        this.descriptionDirtyFlag = true ;
    }

     /**
     * 获取 [说明]脏标记
     */
    @JsonIgnore
    public boolean getDescriptionDirtyFlag(){
        return this.descriptionDirtyFlag ;
    }   

    /**
     * 获取 [显示名称]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return this.display_name ;
    }

    /**
     * 设置 [显示名称]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

     /**
     * 获取 [显示名称]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return this.display_nameDirtyFlag ;
    }   

    /**
     * 获取 [以邮件形式发送]
     */
    @JsonProperty("email_send")
    public String getEmail_send(){
        return this.email_send ;
    }

    /**
     * 设置 [以邮件形式发送]
     */
    @JsonProperty("email_send")
    public void setEmail_send(String  email_send){
        this.email_send = email_send ;
        this.email_sendDirtyFlag = true ;
    }

     /**
     * 获取 [以邮件形式发送]脏标记
     */
    @JsonIgnore
    public boolean getEmail_sendDirtyFlag(){
        return this.email_sendDirtyFlag ;
    }   

    /**
     * 获取 [自动订阅]
     */
    @JsonProperty("group_ids")
    public String getGroup_ids(){
        return this.group_ids ;
    }

    /**
     * 设置 [自动订阅]
     */
    @JsonProperty("group_ids")
    public void setGroup_ids(String  group_ids){
        this.group_ids = group_ids ;
        this.group_idsDirtyFlag = true ;
    }

     /**
     * 获取 [自动订阅]脏标记
     */
    @JsonIgnore
    public boolean getGroup_idsDirtyFlag(){
        return this.group_idsDirtyFlag ;
    }   

    /**
     * 获取 [经授权的群组]
     */
    @JsonProperty("group_public_id")
    public Integer getGroup_public_id(){
        return this.group_public_id ;
    }

    /**
     * 设置 [经授权的群组]
     */
    @JsonProperty("group_public_id")
    public void setGroup_public_id(Integer  group_public_id){
        this.group_public_id = group_public_id ;
        this.group_public_idDirtyFlag = true ;
    }

     /**
     * 获取 [经授权的群组]脏标记
     */
    @JsonIgnore
    public boolean getGroup_public_idDirtyFlag(){
        return this.group_public_idDirtyFlag ;
    }   

    /**
     * 获取 [经授权的群组]
     */
    @JsonProperty("group_public_id_text")
    public String getGroup_public_id_text(){
        return this.group_public_id_text ;
    }

    /**
     * 设置 [经授权的群组]
     */
    @JsonProperty("group_public_id_text")
    public void setGroup_public_id_text(String  group_public_id_text){
        this.group_public_id_text = group_public_id_text ;
        this.group_public_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [经授权的群组]脏标记
     */
    @JsonIgnore
    public boolean getGroup_public_id_textDirtyFlag(){
        return this.group_public_id_textDirtyFlag ;
    }   

    /**
     * 获取 [隐私]
     */
    @JsonProperty("ibizpublic")
    public String getIbizpublic(){
        return this.ibizpublic ;
    }

    /**
     * 设置 [隐私]
     */
    @JsonProperty("ibizpublic")
    public void setIbizpublic(String  ibizpublic){
        this.ibizpublic = ibizpublic ;
        this.ibizpublicDirtyFlag = true ;
    }

     /**
     * 获取 [隐私]脏标记
     */
    @JsonIgnore
    public boolean getIbizpublicDirtyFlag(){
        return this.ibizpublicDirtyFlag ;
    }   

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return this.id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

     /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return this.idDirtyFlag ;
    }   

    /**
     * 获取 [照片]
     */
    @JsonProperty("image")
    public byte[] getImage(){
        return this.image ;
    }

    /**
     * 设置 [照片]
     */
    @JsonProperty("image")
    public void setImage(byte[]  image){
        this.image = image ;
        this.imageDirtyFlag = true ;
    }

     /**
     * 获取 [照片]脏标记
     */
    @JsonIgnore
    public boolean getImageDirtyFlag(){
        return this.imageDirtyFlag ;
    }   

    /**
     * 获取 [中等尺寸照片]
     */
    @JsonProperty("image_medium")
    public byte[] getImage_medium(){
        return this.image_medium ;
    }

    /**
     * 设置 [中等尺寸照片]
     */
    @JsonProperty("image_medium")
    public void setImage_medium(byte[]  image_medium){
        this.image_medium = image_medium ;
        this.image_mediumDirtyFlag = true ;
    }

     /**
     * 获取 [中等尺寸照片]脏标记
     */
    @JsonIgnore
    public boolean getImage_mediumDirtyFlag(){
        return this.image_mediumDirtyFlag ;
    }   

    /**
     * 获取 [小尺寸照片]
     */
    @JsonProperty("image_small")
    public byte[] getImage_small(){
        return this.image_small ;
    }

    /**
     * 设置 [小尺寸照片]
     */
    @JsonProperty("image_small")
    public void setImage_small(byte[]  image_small){
        this.image_small = image_small ;
        this.image_smallDirtyFlag = true ;
    }

     /**
     * 获取 [小尺寸照片]脏标记
     */
    @JsonIgnore
    public boolean getImage_smallDirtyFlag(){
        return this.image_smallDirtyFlag ;
    }   

    /**
     * 获取 [是聊天]
     */
    @JsonProperty("is_chat")
    public String getIs_chat(){
        return this.is_chat ;
    }

    /**
     * 设置 [是聊天]
     */
    @JsonProperty("is_chat")
    public void setIs_chat(String  is_chat){
        this.is_chat = is_chat ;
        this.is_chatDirtyFlag = true ;
    }

     /**
     * 获取 [是聊天]脏标记
     */
    @JsonIgnore
    public boolean getIs_chatDirtyFlag(){
        return this.is_chatDirtyFlag ;
    }   

    /**
     * 获取 [成员]
     */
    @JsonProperty("is_member")
    public String getIs_member(){
        return this.is_member ;
    }

    /**
     * 设置 [成员]
     */
    @JsonProperty("is_member")
    public void setIs_member(String  is_member){
        this.is_member = is_member ;
        this.is_memberDirtyFlag = true ;
    }

     /**
     * 获取 [成员]脏标记
     */
    @JsonIgnore
    public boolean getIs_memberDirtyFlag(){
        return this.is_memberDirtyFlag ;
    }   

    /**
     * 获取 [管理员]
     */
    @JsonProperty("is_moderator")
    public String getIs_moderator(){
        return this.is_moderator ;
    }

    /**
     * 设置 [管理员]
     */
    @JsonProperty("is_moderator")
    public void setIs_moderator(String  is_moderator){
        this.is_moderator = is_moderator ;
        this.is_moderatorDirtyFlag = true ;
    }

     /**
     * 获取 [管理员]脏标记
     */
    @JsonIgnore
    public boolean getIs_moderatorDirtyFlag(){
        return this.is_moderatorDirtyFlag ;
    }   

    /**
     * 获取 [已订阅]
     */
    @JsonProperty("is_subscribed")
    public String getIs_subscribed(){
        return this.is_subscribed ;
    }

    /**
     * 设置 [已订阅]
     */
    @JsonProperty("is_subscribed")
    public void setIs_subscribed(String  is_subscribed){
        this.is_subscribed = is_subscribed ;
        this.is_subscribedDirtyFlag = true ;
    }

     /**
     * 获取 [已订阅]脏标记
     */
    @JsonIgnore
    public boolean getIs_subscribedDirtyFlag(){
        return this.is_subscribedDirtyFlag ;
    }   

    /**
     * 获取 [渠道]
     */
    @JsonProperty("livechat_channel_id")
    public Integer getLivechat_channel_id(){
        return this.livechat_channel_id ;
    }

    /**
     * 设置 [渠道]
     */
    @JsonProperty("livechat_channel_id")
    public void setLivechat_channel_id(Integer  livechat_channel_id){
        this.livechat_channel_id = livechat_channel_id ;
        this.livechat_channel_idDirtyFlag = true ;
    }

     /**
     * 获取 [渠道]脏标记
     */
    @JsonIgnore
    public boolean getLivechat_channel_idDirtyFlag(){
        return this.livechat_channel_idDirtyFlag ;
    }   

    /**
     * 获取 [渠道]
     */
    @JsonProperty("livechat_channel_id_text")
    public String getLivechat_channel_id_text(){
        return this.livechat_channel_id_text ;
    }

    /**
     * 设置 [渠道]
     */
    @JsonProperty("livechat_channel_id_text")
    public void setLivechat_channel_id_text(String  livechat_channel_id_text){
        this.livechat_channel_id_text = livechat_channel_id_text ;
        this.livechat_channel_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [渠道]脏标记
     */
    @JsonIgnore
    public boolean getLivechat_channel_id_textDirtyFlag(){
        return this.livechat_channel_id_textDirtyFlag ;
    }   

    /**
     * 获取 [附件数量]
     */
    @JsonProperty("message_attachment_count")
    public Integer getMessage_attachment_count(){
        return this.message_attachment_count ;
    }

    /**
     * 设置 [附件数量]
     */
    @JsonProperty("message_attachment_count")
    public void setMessage_attachment_count(Integer  message_attachment_count){
        this.message_attachment_count = message_attachment_count ;
        this.message_attachment_countDirtyFlag = true ;
    }

     /**
     * 获取 [附件数量]脏标记
     */
    @JsonIgnore
    public boolean getMessage_attachment_countDirtyFlag(){
        return this.message_attachment_countDirtyFlag ;
    }   

    /**
     * 获取 [关注者(渠道)]
     */
    @JsonProperty("message_channel_ids")
    public String getMessage_channel_ids(){
        return this.message_channel_ids ;
    }

    /**
     * 设置 [关注者(渠道)]
     */
    @JsonProperty("message_channel_ids")
    public void setMessage_channel_ids(String  message_channel_ids){
        this.message_channel_ids = message_channel_ids ;
        this.message_channel_idsDirtyFlag = true ;
    }

     /**
     * 获取 [关注者(渠道)]脏标记
     */
    @JsonIgnore
    public boolean getMessage_channel_idsDirtyFlag(){
        return this.message_channel_idsDirtyFlag ;
    }   

    /**
     * 获取 [关注者]
     */
    @JsonProperty("message_follower_ids")
    public String getMessage_follower_ids(){
        return this.message_follower_ids ;
    }

    /**
     * 设置 [关注者]
     */
    @JsonProperty("message_follower_ids")
    public void setMessage_follower_ids(String  message_follower_ids){
        this.message_follower_ids = message_follower_ids ;
        this.message_follower_idsDirtyFlag = true ;
    }

     /**
     * 获取 [关注者]脏标记
     */
    @JsonIgnore
    public boolean getMessage_follower_idsDirtyFlag(){
        return this.message_follower_idsDirtyFlag ;
    }   

    /**
     * 获取 [消息递送错误]
     */
    @JsonProperty("message_has_error")
    public String getMessage_has_error(){
        return this.message_has_error ;
    }

    /**
     * 设置 [消息递送错误]
     */
    @JsonProperty("message_has_error")
    public void setMessage_has_error(String  message_has_error){
        this.message_has_error = message_has_error ;
        this.message_has_errorDirtyFlag = true ;
    }

     /**
     * 获取 [消息递送错误]脏标记
     */
    @JsonIgnore
    public boolean getMessage_has_errorDirtyFlag(){
        return this.message_has_errorDirtyFlag ;
    }   

    /**
     * 获取 [错误个数]
     */
    @JsonProperty("message_has_error_counter")
    public Integer getMessage_has_error_counter(){
        return this.message_has_error_counter ;
    }

    /**
     * 设置 [错误个数]
     */
    @JsonProperty("message_has_error_counter")
    public void setMessage_has_error_counter(Integer  message_has_error_counter){
        this.message_has_error_counter = message_has_error_counter ;
        this.message_has_error_counterDirtyFlag = true ;
    }

     /**
     * 获取 [错误个数]脏标记
     */
    @JsonIgnore
    public boolean getMessage_has_error_counterDirtyFlag(){
        return this.message_has_error_counterDirtyFlag ;
    }   

    /**
     * 获取 [消息]
     */
    @JsonProperty("message_ids")
    public String getMessage_ids(){
        return this.message_ids ;
    }

    /**
     * 设置 [消息]
     */
    @JsonProperty("message_ids")
    public void setMessage_ids(String  message_ids){
        this.message_ids = message_ids ;
        this.message_idsDirtyFlag = true ;
    }

     /**
     * 获取 [消息]脏标记
     */
    @JsonIgnore
    public boolean getMessage_idsDirtyFlag(){
        return this.message_idsDirtyFlag ;
    }   

    /**
     * 获取 [关注者]
     */
    @JsonProperty("message_is_follower")
    public String getMessage_is_follower(){
        return this.message_is_follower ;
    }

    /**
     * 设置 [关注者]
     */
    @JsonProperty("message_is_follower")
    public void setMessage_is_follower(String  message_is_follower){
        this.message_is_follower = message_is_follower ;
        this.message_is_followerDirtyFlag = true ;
    }

     /**
     * 获取 [关注者]脏标记
     */
    @JsonIgnore
    public boolean getMessage_is_followerDirtyFlag(){
        return this.message_is_followerDirtyFlag ;
    }   

    /**
     * 获取 [附件]
     */
    @JsonProperty("message_main_attachment_id")
    public Integer getMessage_main_attachment_id(){
        return this.message_main_attachment_id ;
    }

    /**
     * 设置 [附件]
     */
    @JsonProperty("message_main_attachment_id")
    public void setMessage_main_attachment_id(Integer  message_main_attachment_id){
        this.message_main_attachment_id = message_main_attachment_id ;
        this.message_main_attachment_idDirtyFlag = true ;
    }

     /**
     * 获取 [附件]脏标记
     */
    @JsonIgnore
    public boolean getMessage_main_attachment_idDirtyFlag(){
        return this.message_main_attachment_idDirtyFlag ;
    }   

    /**
     * 获取 [前置操作]
     */
    @JsonProperty("message_needaction")
    public String getMessage_needaction(){
        return this.message_needaction ;
    }

    /**
     * 设置 [前置操作]
     */
    @JsonProperty("message_needaction")
    public void setMessage_needaction(String  message_needaction){
        this.message_needaction = message_needaction ;
        this.message_needactionDirtyFlag = true ;
    }

     /**
     * 获取 [前置操作]脏标记
     */
    @JsonIgnore
    public boolean getMessage_needactionDirtyFlag(){
        return this.message_needactionDirtyFlag ;
    }   

    /**
     * 获取 [操作次数]
     */
    @JsonProperty("message_needaction_counter")
    public Integer getMessage_needaction_counter(){
        return this.message_needaction_counter ;
    }

    /**
     * 设置 [操作次数]
     */
    @JsonProperty("message_needaction_counter")
    public void setMessage_needaction_counter(Integer  message_needaction_counter){
        this.message_needaction_counter = message_needaction_counter ;
        this.message_needaction_counterDirtyFlag = true ;
    }

     /**
     * 获取 [操作次数]脏标记
     */
    @JsonIgnore
    public boolean getMessage_needaction_counterDirtyFlag(){
        return this.message_needaction_counterDirtyFlag ;
    }   

    /**
     * 获取 [关注者(业务伙伴)]
     */
    @JsonProperty("message_partner_ids")
    public String getMessage_partner_ids(){
        return this.message_partner_ids ;
    }

    /**
     * 设置 [关注者(业务伙伴)]
     */
    @JsonProperty("message_partner_ids")
    public void setMessage_partner_ids(String  message_partner_ids){
        this.message_partner_ids = message_partner_ids ;
        this.message_partner_idsDirtyFlag = true ;
    }

     /**
     * 获取 [关注者(业务伙伴)]脏标记
     */
    @JsonIgnore
    public boolean getMessage_partner_idsDirtyFlag(){
        return this.message_partner_idsDirtyFlag ;
    }   

    /**
     * 获取 [未读消息]
     */
    @JsonProperty("message_unread")
    public String getMessage_unread(){
        return this.message_unread ;
    }

    /**
     * 设置 [未读消息]
     */
    @JsonProperty("message_unread")
    public void setMessage_unread(String  message_unread){
        this.message_unread = message_unread ;
        this.message_unreadDirtyFlag = true ;
    }

     /**
     * 获取 [未读消息]脏标记
     */
    @JsonIgnore
    public boolean getMessage_unreadDirtyFlag(){
        return this.message_unreadDirtyFlag ;
    }   

    /**
     * 获取 [未读消息计数器]
     */
    @JsonProperty("message_unread_counter")
    public Integer getMessage_unread_counter(){
        return this.message_unread_counter ;
    }

    /**
     * 设置 [未读消息计数器]
     */
    @JsonProperty("message_unread_counter")
    public void setMessage_unread_counter(Integer  message_unread_counter){
        this.message_unread_counter = message_unread_counter ;
        this.message_unread_counterDirtyFlag = true ;
    }

     /**
     * 获取 [未读消息计数器]脏标记
     */
    @JsonIgnore
    public boolean getMessage_unread_counterDirtyFlag(){
        return this.message_unread_counterDirtyFlag ;
    }   

    /**
     * 获取 [管理频道]
     */
    @JsonProperty("moderation")
    public String getModeration(){
        return this.moderation ;
    }

    /**
     * 设置 [管理频道]
     */
    @JsonProperty("moderation")
    public void setModeration(String  moderation){
        this.moderation = moderation ;
        this.moderationDirtyFlag = true ;
    }

     /**
     * 获取 [管理频道]脏标记
     */
    @JsonIgnore
    public boolean getModerationDirtyFlag(){
        return this.moderationDirtyFlag ;
    }   

    /**
     * 获取 [管理EMail账户]
     */
    @JsonProperty("moderation_count")
    public Integer getModeration_count(){
        return this.moderation_count ;
    }

    /**
     * 设置 [管理EMail账户]
     */
    @JsonProperty("moderation_count")
    public void setModeration_count(Integer  moderation_count){
        this.moderation_count = moderation_count ;
        this.moderation_countDirtyFlag = true ;
    }

     /**
     * 获取 [管理EMail账户]脏标记
     */
    @JsonIgnore
    public boolean getModeration_countDirtyFlag(){
        return this.moderation_countDirtyFlag ;
    }   

    /**
     * 获取 [向新用户发送订阅指南]
     */
    @JsonProperty("moderation_guidelines")
    public String getModeration_guidelines(){
        return this.moderation_guidelines ;
    }

    /**
     * 设置 [向新用户发送订阅指南]
     */
    @JsonProperty("moderation_guidelines")
    public void setModeration_guidelines(String  moderation_guidelines){
        this.moderation_guidelines = moderation_guidelines ;
        this.moderation_guidelinesDirtyFlag = true ;
    }

     /**
     * 获取 [向新用户发送订阅指南]脏标记
     */
    @JsonIgnore
    public boolean getModeration_guidelinesDirtyFlag(){
        return this.moderation_guidelinesDirtyFlag ;
    }   

    /**
     * 获取 [方针]
     */
    @JsonProperty("moderation_guidelines_msg")
    public String getModeration_guidelines_msg(){
        return this.moderation_guidelines_msg ;
    }

    /**
     * 设置 [方针]
     */
    @JsonProperty("moderation_guidelines_msg")
    public void setModeration_guidelines_msg(String  moderation_guidelines_msg){
        this.moderation_guidelines_msg = moderation_guidelines_msg ;
        this.moderation_guidelines_msgDirtyFlag = true ;
    }

     /**
     * 获取 [方针]脏标记
     */
    @JsonIgnore
    public boolean getModeration_guidelines_msgDirtyFlag(){
        return this.moderation_guidelines_msgDirtyFlag ;
    }   

    /**
     * 获取 [管理EMail]
     */
    @JsonProperty("moderation_ids")
    public String getModeration_ids(){
        return this.moderation_ids ;
    }

    /**
     * 设置 [管理EMail]
     */
    @JsonProperty("moderation_ids")
    public void setModeration_ids(String  moderation_ids){
        this.moderation_ids = moderation_ids ;
        this.moderation_idsDirtyFlag = true ;
    }

     /**
     * 获取 [管理EMail]脏标记
     */
    @JsonIgnore
    public boolean getModeration_idsDirtyFlag(){
        return this.moderation_idsDirtyFlag ;
    }   

    /**
     * 获取 [自动通知]
     */
    @JsonProperty("moderation_notify")
    public String getModeration_notify(){
        return this.moderation_notify ;
    }

    /**
     * 设置 [自动通知]
     */
    @JsonProperty("moderation_notify")
    public void setModeration_notify(String  moderation_notify){
        this.moderation_notify = moderation_notify ;
        this.moderation_notifyDirtyFlag = true ;
    }

     /**
     * 获取 [自动通知]脏标记
     */
    @JsonIgnore
    public boolean getModeration_notifyDirtyFlag(){
        return this.moderation_notifyDirtyFlag ;
    }   

    /**
     * 获取 [通知消息]
     */
    @JsonProperty("moderation_notify_msg")
    public String getModeration_notify_msg(){
        return this.moderation_notify_msg ;
    }

    /**
     * 设置 [通知消息]
     */
    @JsonProperty("moderation_notify_msg")
    public void setModeration_notify_msg(String  moderation_notify_msg){
        this.moderation_notify_msg = moderation_notify_msg ;
        this.moderation_notify_msgDirtyFlag = true ;
    }

     /**
     * 获取 [通知消息]脏标记
     */
    @JsonIgnore
    public boolean getModeration_notify_msgDirtyFlag(){
        return this.moderation_notify_msgDirtyFlag ;
    }   

    /**
     * 获取 [管理员]
     */
    @JsonProperty("moderator_ids")
    public String getModerator_ids(){
        return this.moderator_ids ;
    }

    /**
     * 设置 [管理员]
     */
    @JsonProperty("moderator_ids")
    public void setModerator_ids(String  moderator_ids){
        this.moderator_ids = moderator_ids ;
        this.moderator_idsDirtyFlag = true ;
    }

     /**
     * 获取 [管理员]脏标记
     */
    @JsonIgnore
    public boolean getModerator_idsDirtyFlag(){
        return this.moderator_idsDirtyFlag ;
    }   

    /**
     * 获取 [名称]
     */
    @JsonProperty("name")
    public String getName(){
        return this.name ;
    }

    /**
     * 设置 [名称]
     */
    @JsonProperty("name")
    public void setName(String  name){
        this.name = name ;
        this.nameDirtyFlag = true ;
    }

     /**
     * 获取 [名称]脏标记
     */
    @JsonIgnore
    public boolean getNameDirtyFlag(){
        return this.nameDirtyFlag ;
    }   

    /**
     * 获取 [评级数]
     */
    @JsonProperty("rating_count")
    public Integer getRating_count(){
        return this.rating_count ;
    }

    /**
     * 设置 [评级数]
     */
    @JsonProperty("rating_count")
    public void setRating_count(Integer  rating_count){
        this.rating_count = rating_count ;
        this.rating_countDirtyFlag = true ;
    }

     /**
     * 获取 [评级数]脏标记
     */
    @JsonIgnore
    public boolean getRating_countDirtyFlag(){
        return this.rating_countDirtyFlag ;
    }   

    /**
     * 获取 [评级]
     */
    @JsonProperty("rating_ids")
    public String getRating_ids(){
        return this.rating_ids ;
    }

    /**
     * 设置 [评级]
     */
    @JsonProperty("rating_ids")
    public void setRating_ids(String  rating_ids){
        this.rating_ids = rating_ids ;
        this.rating_idsDirtyFlag = true ;
    }

     /**
     * 获取 [评级]脏标记
     */
    @JsonIgnore
    public boolean getRating_idsDirtyFlag(){
        return this.rating_idsDirtyFlag ;
    }   

    /**
     * 获取 [最新反馈评级]
     */
    @JsonProperty("rating_last_feedback")
    public String getRating_last_feedback(){
        return this.rating_last_feedback ;
    }

    /**
     * 设置 [最新反馈评级]
     */
    @JsonProperty("rating_last_feedback")
    public void setRating_last_feedback(String  rating_last_feedback){
        this.rating_last_feedback = rating_last_feedback ;
        this.rating_last_feedbackDirtyFlag = true ;
    }

     /**
     * 获取 [最新反馈评级]脏标记
     */
    @JsonIgnore
    public boolean getRating_last_feedbackDirtyFlag(){
        return this.rating_last_feedbackDirtyFlag ;
    }   

    /**
     * 获取 [最新图像评级]
     */
    @JsonProperty("rating_last_image")
    public byte[] getRating_last_image(){
        return this.rating_last_image ;
    }

    /**
     * 设置 [最新图像评级]
     */
    @JsonProperty("rating_last_image")
    public void setRating_last_image(byte[]  rating_last_image){
        this.rating_last_image = rating_last_image ;
        this.rating_last_imageDirtyFlag = true ;
    }

     /**
     * 获取 [最新图像评级]脏标记
     */
    @JsonIgnore
    public boolean getRating_last_imageDirtyFlag(){
        return this.rating_last_imageDirtyFlag ;
    }   

    /**
     * 获取 [最新值评级]
     */
    @JsonProperty("rating_last_value")
    public Double getRating_last_value(){
        return this.rating_last_value ;
    }

    /**
     * 设置 [最新值评级]
     */
    @JsonProperty("rating_last_value")
    public void setRating_last_value(Double  rating_last_value){
        this.rating_last_value = rating_last_value ;
        this.rating_last_valueDirtyFlag = true ;
    }

     /**
     * 获取 [最新值评级]脏标记
     */
    @JsonIgnore
    public boolean getRating_last_valueDirtyFlag(){
        return this.rating_last_valueDirtyFlag ;
    }   

    /**
     * 获取 [人力资源部门]
     */
    @JsonProperty("subscription_department_ids")
    public String getSubscription_department_ids(){
        return this.subscription_department_ids ;
    }

    /**
     * 设置 [人力资源部门]
     */
    @JsonProperty("subscription_department_ids")
    public void setSubscription_department_ids(String  subscription_department_ids){
        this.subscription_department_ids = subscription_department_ids ;
        this.subscription_department_idsDirtyFlag = true ;
    }

     /**
     * 获取 [人力资源部门]脏标记
     */
    @JsonIgnore
    public boolean getSubscription_department_idsDirtyFlag(){
        return this.subscription_department_idsDirtyFlag ;
    }   

    /**
     * 获取 [UUID]
     */
    @JsonProperty("uuid")
    public String getUuid(){
        return this.uuid ;
    }

    /**
     * 设置 [UUID]
     */
    @JsonProperty("uuid")
    public void setUuid(String  uuid){
        this.uuid = uuid ;
        this.uuidDirtyFlag = true ;
    }

     /**
     * 获取 [UUID]脏标记
     */
    @JsonIgnore
    public boolean getUuidDirtyFlag(){
        return this.uuidDirtyFlag ;
    }   

    /**
     * 获取 [网站消息]
     */
    @JsonProperty("website_message_ids")
    public String getWebsite_message_ids(){
        return this.website_message_ids ;
    }

    /**
     * 设置 [网站消息]
     */
    @JsonProperty("website_message_ids")
    public void setWebsite_message_ids(String  website_message_ids){
        this.website_message_ids = website_message_ids ;
        this.website_message_idsDirtyFlag = true ;
    }

     /**
     * 获取 [网站消息]脏标记
     */
    @JsonIgnore
    public boolean getWebsite_message_idsDirtyFlag(){
        return this.website_message_idsDirtyFlag ;
    }   

    /**
     * 获取 [最后更新时间]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return this.write_date ;
    }

    /**
     * 设置 [最后更新时间]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新时间]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return this.write_dateDirtyFlag ;
    }   

    /**
     * 获取 [最后更新者]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return this.write_uid ;
    }

    /**
     * 设置 [最后更新者]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新者]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return this.write_uidDirtyFlag ;
    }   

    /**
     * 获取 [最后更新者]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return this.write_uid_text ;
    }

    /**
     * 设置 [最后更新者]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新者]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return this.write_uid_textDirtyFlag ;
    }   

    /**
     * 获取 [最后修改日]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return this.__last_update ;
    }

    /**
     * 设置 [最后修改日]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

     /**
     * 获取 [最后修改日]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return this.__last_updateDirtyFlag ;
    }   



    public void fromMap(Map<String, Object> map) throws Exception {
		if(!(map.get("alias_contact") instanceof Boolean)&& map.get("alias_contact")!=null){
			this.setAlias_contact((String)map.get("alias_contact"));
		}
		if(!(map.get("alias_defaults") instanceof Boolean)&& map.get("alias_defaults")!=null){
			this.setAlias_defaults((String)map.get("alias_defaults"));
		}
		if(!(map.get("alias_domain") instanceof Boolean)&& map.get("alias_domain")!=null){
			this.setAlias_domain((String)map.get("alias_domain"));
		}
		if(!(map.get("alias_force_thread_id") instanceof Boolean)&& map.get("alias_force_thread_id")!=null){
			this.setAlias_force_thread_id((Integer)map.get("alias_force_thread_id"));
		}
		if(!(map.get("alias_id") instanceof Boolean)&& map.get("alias_id")!=null){
			Object[] objs = (Object[])map.get("alias_id");
			if(objs.length > 0){
				this.setAlias_id((Integer)objs[0]);
			}
		}
		if(!(map.get("alias_model_id") instanceof Boolean)&& map.get("alias_model_id")!=null){
			Object[] objs = (Object[])map.get("alias_model_id");
			if(objs.length > 0){
				this.setAlias_model_id((Integer)objs[0]);
			}
		}
		if(!(map.get("alias_name") instanceof Boolean)&& map.get("alias_name")!=null){
			this.setAlias_name((String)map.get("alias_name"));
		}
		if(!(map.get("alias_parent_model_id") instanceof Boolean)&& map.get("alias_parent_model_id")!=null){
			Object[] objs = (Object[])map.get("alias_parent_model_id");
			if(objs.length > 0){
				this.setAlias_parent_model_id((Integer)objs[0]);
			}
		}
		if(!(map.get("alias_parent_thread_id") instanceof Boolean)&& map.get("alias_parent_thread_id")!=null){
			this.setAlias_parent_thread_id((Integer)map.get("alias_parent_thread_id"));
		}
		if(!(map.get("alias_user_id") instanceof Boolean)&& map.get("alias_user_id")!=null){
			Object[] objs = (Object[])map.get("alias_user_id");
			if(objs.length > 0){
				this.setAlias_user_id((Integer)objs[0]);
			}
		}
		if(!(map.get("anonymous_name") instanceof Boolean)&& map.get("anonymous_name")!=null){
			this.setAnonymous_name((String)map.get("anonymous_name"));
		}
		if(!(map.get("channel_last_seen_partner_ids") instanceof Boolean)&& map.get("channel_last_seen_partner_ids")!=null){
			Object[] objs = (Object[])map.get("channel_last_seen_partner_ids");
			if(objs.length > 0){
				Integer[] channel_last_seen_partner_ids = Arrays.copyOfRange(objs,0,objs.length,Integer[].class);
				this.setChannel_last_seen_partner_ids(Arrays.toString(channel_last_seen_partner_ids));
			}
		}
		if(!(map.get("channel_message_ids") instanceof Boolean)&& map.get("channel_message_ids")!=null){
			Object[] objs = (Object[])map.get("channel_message_ids");
			if(objs.length > 0){
				Integer[] channel_message_ids = Arrays.copyOfRange(objs,0,objs.length,Integer[].class);
				this.setChannel_message_ids(Arrays.toString(channel_message_ids));
			}
		}
		if(!(map.get("channel_partner_ids") instanceof Boolean)&& map.get("channel_partner_ids")!=null){
			Object[] objs = (Object[])map.get("channel_partner_ids");
			if(objs.length > 0){
				Integer[] channel_partner_ids = Arrays.copyOfRange(objs,0,objs.length,Integer[].class);
				this.setChannel_partner_ids(Arrays.toString(channel_partner_ids));
			}
		}
		if(!(map.get("channel_type") instanceof Boolean)&& map.get("channel_type")!=null){
			this.setChannel_type((String)map.get("channel_type"));
		}
		if(!(map.get("create_date") instanceof Boolean)&& map.get("create_date")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse((String)map.get("create_date"));
   			this.setCreate_date(new Timestamp(parse.getTime()));
		}
		if(!(map.get("create_uid") instanceof Boolean)&& map.get("create_uid")!=null){
			Object[] objs = (Object[])map.get("create_uid");
			if(objs.length > 0){
				this.setCreate_uid((Integer)objs[0]);
			}
		}
		if(!(map.get("create_uid") instanceof Boolean)&& map.get("create_uid")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("create_uid");
			if(objs.length > 1){
				this.setCreate_uid_text((String)objs[1]);
			}
		}
		if(!(map.get("description") instanceof Boolean)&& map.get("description")!=null){
			this.setDescription((String)map.get("description"));
		}
		if(!(map.get("display_name") instanceof Boolean)&& map.get("display_name")!=null){
			this.setDisplay_name((String)map.get("display_name"));
		}
		if(map.get("email_send") instanceof Boolean){
			this.setEmail_send(((Boolean)map.get("email_send"))? "true" : "false");
		}
		if(!(map.get("group_ids") instanceof Boolean)&& map.get("group_ids")!=null){
			Object[] objs = (Object[])map.get("group_ids");
			if(objs.length > 0){
				Integer[] group_ids = Arrays.copyOfRange(objs,0,objs.length,Integer[].class);
				this.setGroup_ids(Arrays.toString(group_ids));
			}
		}
		if(!(map.get("group_public_id") instanceof Boolean)&& map.get("group_public_id")!=null){
			Object[] objs = (Object[])map.get("group_public_id");
			if(objs.length > 0){
				this.setGroup_public_id((Integer)objs[0]);
			}
		}
		if(!(map.get("group_public_id") instanceof Boolean)&& map.get("group_public_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("group_public_id");
			if(objs.length > 1){
				this.setGroup_public_id_text((String)objs[1]);
			}
		}
		if(!(map.get("public") instanceof Boolean)&& map.get("public")!=null){
			this.setIbizpublic((String)map.get("public"));
		}
		if(!(map.get("id") instanceof Boolean)&& map.get("id")!=null){
			this.setId((Integer)map.get("id"));
		}
		if(!(map.get("image") instanceof Boolean)&& map.get("image")!=null){
			//暂时忽略
			//this.setImage(((String)map.get("image")).getBytes("UTF-8"));
		}
		if(!(map.get("image_medium") instanceof Boolean)&& map.get("image_medium")!=null){
			//暂时忽略
			//this.setImage_medium(((String)map.get("image_medium")).getBytes("UTF-8"));
		}
		if(!(map.get("image_small") instanceof Boolean)&& map.get("image_small")!=null){
			//暂时忽略
			//this.setImage_small(((String)map.get("image_small")).getBytes("UTF-8"));
		}
		if(map.get("is_chat") instanceof Boolean){
			this.setIs_chat(((Boolean)map.get("is_chat"))? "true" : "false");
		}
		if(map.get("is_member") instanceof Boolean){
			this.setIs_member(((Boolean)map.get("is_member"))? "true" : "false");
		}
		if(map.get("is_moderator") instanceof Boolean){
			this.setIs_moderator(((Boolean)map.get("is_moderator"))? "true" : "false");
		}
		if(map.get("is_subscribed") instanceof Boolean){
			this.setIs_subscribed(((Boolean)map.get("is_subscribed"))? "true" : "false");
		}
		if(!(map.get("livechat_channel_id") instanceof Boolean)&& map.get("livechat_channel_id")!=null){
			Object[] objs = (Object[])map.get("livechat_channel_id");
			if(objs.length > 0){
				this.setLivechat_channel_id((Integer)objs[0]);
			}
		}
		if(!(map.get("livechat_channel_id") instanceof Boolean)&& map.get("livechat_channel_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("livechat_channel_id");
			if(objs.length > 1){
				this.setLivechat_channel_id_text((String)objs[1]);
			}
		}
		if(!(map.get("message_attachment_count") instanceof Boolean)&& map.get("message_attachment_count")!=null){
			this.setMessage_attachment_count((Integer)map.get("message_attachment_count"));
		}
		if(!(map.get("message_channel_ids") instanceof Boolean)&& map.get("message_channel_ids")!=null){
			Object[] objs = (Object[])map.get("message_channel_ids");
			if(objs.length > 0){
				Integer[] message_channel_ids = Arrays.copyOfRange(objs,0,objs.length,Integer[].class);
				this.setMessage_channel_ids(Arrays.toString(message_channel_ids));
			}
		}
		if(!(map.get("message_follower_ids") instanceof Boolean)&& map.get("message_follower_ids")!=null){
			Object[] objs = (Object[])map.get("message_follower_ids");
			if(objs.length > 0){
				Integer[] message_follower_ids = Arrays.copyOfRange(objs,0,objs.length,Integer[].class);
				this.setMessage_follower_ids(Arrays.toString(message_follower_ids));
			}
		}
		if(map.get("message_has_error") instanceof Boolean){
			this.setMessage_has_error(((Boolean)map.get("message_has_error"))? "true" : "false");
		}
		if(!(map.get("message_has_error_counter") instanceof Boolean)&& map.get("message_has_error_counter")!=null){
			this.setMessage_has_error_counter((Integer)map.get("message_has_error_counter"));
		}
		if(!(map.get("message_ids") instanceof Boolean)&& map.get("message_ids")!=null){
			Object[] objs = (Object[])map.get("message_ids");
			if(objs.length > 0){
				Integer[] message_ids = Arrays.copyOfRange(objs,0,objs.length,Integer[].class);
				this.setMessage_ids(Arrays.toString(message_ids));
			}
		}
		if(map.get("message_is_follower") instanceof Boolean){
			this.setMessage_is_follower(((Boolean)map.get("message_is_follower"))? "true" : "false");
		}
		if(!(map.get("message_main_attachment_id") instanceof Boolean)&& map.get("message_main_attachment_id")!=null){
			Object[] objs = (Object[])map.get("message_main_attachment_id");
			if(objs.length > 0){
				this.setMessage_main_attachment_id((Integer)objs[0]);
			}
		}
		if(map.get("message_needaction") instanceof Boolean){
			this.setMessage_needaction(((Boolean)map.get("message_needaction"))? "true" : "false");
		}
		if(!(map.get("message_needaction_counter") instanceof Boolean)&& map.get("message_needaction_counter")!=null){
			this.setMessage_needaction_counter((Integer)map.get("message_needaction_counter"));
		}
		if(!(map.get("message_partner_ids") instanceof Boolean)&& map.get("message_partner_ids")!=null){
			Object[] objs = (Object[])map.get("message_partner_ids");
			if(objs.length > 0){
				Integer[] message_partner_ids = Arrays.copyOfRange(objs,0,objs.length,Integer[].class);
				this.setMessage_partner_ids(Arrays.toString(message_partner_ids));
			}
		}
		if(map.get("message_unread") instanceof Boolean){
			this.setMessage_unread(((Boolean)map.get("message_unread"))? "true" : "false");
		}
		if(!(map.get("message_unread_counter") instanceof Boolean)&& map.get("message_unread_counter")!=null){
			this.setMessage_unread_counter((Integer)map.get("message_unread_counter"));
		}
		if(map.get("moderation") instanceof Boolean){
			this.setModeration(((Boolean)map.get("moderation"))? "true" : "false");
		}
		if(!(map.get("moderation_count") instanceof Boolean)&& map.get("moderation_count")!=null){
			this.setModeration_count((Integer)map.get("moderation_count"));
		}
		if(map.get("moderation_guidelines") instanceof Boolean){
			this.setModeration_guidelines(((Boolean)map.get("moderation_guidelines"))? "true" : "false");
		}
		if(!(map.get("moderation_guidelines_msg") instanceof Boolean)&& map.get("moderation_guidelines_msg")!=null){
			this.setModeration_guidelines_msg((String)map.get("moderation_guidelines_msg"));
		}
		if(!(map.get("moderation_ids") instanceof Boolean)&& map.get("moderation_ids")!=null){
			Object[] objs = (Object[])map.get("moderation_ids");
			if(objs.length > 0){
				Integer[] moderation_ids = Arrays.copyOfRange(objs,0,objs.length,Integer[].class);
				this.setModeration_ids(Arrays.toString(moderation_ids));
			}
		}
		if(map.get("moderation_notify") instanceof Boolean){
			this.setModeration_notify(((Boolean)map.get("moderation_notify"))? "true" : "false");
		}
		if(!(map.get("moderation_notify_msg") instanceof Boolean)&& map.get("moderation_notify_msg")!=null){
			this.setModeration_notify_msg((String)map.get("moderation_notify_msg"));
		}
		if(!(map.get("moderator_ids") instanceof Boolean)&& map.get("moderator_ids")!=null){
			Object[] objs = (Object[])map.get("moderator_ids");
			if(objs.length > 0){
				Integer[] moderator_ids = Arrays.copyOfRange(objs,0,objs.length,Integer[].class);
				this.setModerator_ids(Arrays.toString(moderator_ids));
			}
		}
		if(!(map.get("name") instanceof Boolean)&& map.get("name")!=null){
			this.setName((String)map.get("name"));
		}
		if(!(map.get("rating_count") instanceof Boolean)&& map.get("rating_count")!=null){
			this.setRating_count((Integer)map.get("rating_count"));
		}
		if(!(map.get("rating_ids") instanceof Boolean)&& map.get("rating_ids")!=null){
			Object[] objs = (Object[])map.get("rating_ids");
			if(objs.length > 0){
				Integer[] rating_ids = Arrays.copyOfRange(objs,0,objs.length,Integer[].class);
				this.setRating_ids(Arrays.toString(rating_ids));
			}
		}
		if(!(map.get("rating_last_feedback") instanceof Boolean)&& map.get("rating_last_feedback")!=null){
			this.setRating_last_feedback((String)map.get("rating_last_feedback"));
		}
		if(!(map.get("rating_last_image") instanceof Boolean)&& map.get("rating_last_image")!=null){
			//暂时忽略
			//this.setRating_last_image(((String)map.get("rating_last_image")).getBytes("UTF-8"));
		}
		if(!(map.get("rating_last_value") instanceof Boolean)&& map.get("rating_last_value")!=null){
			this.setRating_last_value((Double)map.get("rating_last_value"));
		}
		if(!(map.get("subscription_department_ids") instanceof Boolean)&& map.get("subscription_department_ids")!=null){
			Object[] objs = (Object[])map.get("subscription_department_ids");
			if(objs.length > 0){
				Integer[] subscription_department_ids = Arrays.copyOfRange(objs,0,objs.length,Integer[].class);
				this.setSubscription_department_ids(Arrays.toString(subscription_department_ids));
			}
		}
		if(!(map.get("uuid") instanceof Boolean)&& map.get("uuid")!=null){
			this.setUuid((String)map.get("uuid"));
		}
		if(!(map.get("website_message_ids") instanceof Boolean)&& map.get("website_message_ids")!=null){
			Object[] objs = (Object[])map.get("website_message_ids");
			if(objs.length > 0){
				Integer[] website_message_ids = Arrays.copyOfRange(objs,0,objs.length,Integer[].class);
				this.setWebsite_message_ids(Arrays.toString(website_message_ids));
			}
		}
		if(!(map.get("write_date") instanceof Boolean)&& map.get("write_date")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse((String)map.get("write_date"));
   			this.setWrite_date(new Timestamp(parse.getTime()));
		}
		if(!(map.get("write_uid") instanceof Boolean)&& map.get("write_uid")!=null){
			Object[] objs = (Object[])map.get("write_uid");
			if(objs.length > 0){
				this.setWrite_uid((Integer)objs[0]);
			}
		}
		if(!(map.get("write_uid") instanceof Boolean)&& map.get("write_uid")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("write_uid");
			if(objs.length > 1){
				this.setWrite_uid_text((String)objs[1]);
			}
		}
		if(!(map.get("__last_update") instanceof Boolean)&& map.get("__last_update")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse((String)map.get("__last_update"));
   			this.set__last_update(new Timestamp(parse.getTime()));
		}
	}

	public Map<String, Object> toMap() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		if(this.getAlias_contact()!=null&&this.getAlias_contactDirtyFlag()){
			map.put("alias_contact",this.getAlias_contact());
		}else if(this.getAlias_contactDirtyFlag()){
			map.put("alias_contact",false);
		}
		if(this.getAlias_defaults()!=null&&this.getAlias_defaultsDirtyFlag()){
			map.put("alias_defaults",this.getAlias_defaults());
		}else if(this.getAlias_defaultsDirtyFlag()){
			map.put("alias_defaults",false);
		}
		if(this.getAlias_domain()!=null&&this.getAlias_domainDirtyFlag()){
			map.put("alias_domain",this.getAlias_domain());
		}else if(this.getAlias_domainDirtyFlag()){
			map.put("alias_domain",false);
		}
		if(this.getAlias_force_thread_id()!=null&&this.getAlias_force_thread_idDirtyFlag()){
			map.put("alias_force_thread_id",this.getAlias_force_thread_id());
		}else if(this.getAlias_force_thread_idDirtyFlag()){
			map.put("alias_force_thread_id",false);
		}
		if(this.getAlias_id()!=null&&this.getAlias_idDirtyFlag()){
			map.put("alias_id",this.getAlias_id());
		}else if(this.getAlias_idDirtyFlag()){
			map.put("alias_id",false);
		}
		if(this.getAlias_model_id()!=null&&this.getAlias_model_idDirtyFlag()){
			map.put("alias_model_id",this.getAlias_model_id());
		}else if(this.getAlias_model_idDirtyFlag()){
			map.put("alias_model_id",false);
		}
		if(this.getAlias_name()!=null&&this.getAlias_nameDirtyFlag()){
			map.put("alias_name",this.getAlias_name());
		}else if(this.getAlias_nameDirtyFlag()){
			map.put("alias_name",false);
		}
		if(this.getAlias_parent_model_id()!=null&&this.getAlias_parent_model_idDirtyFlag()){
			map.put("alias_parent_model_id",this.getAlias_parent_model_id());
		}else if(this.getAlias_parent_model_idDirtyFlag()){
			map.put("alias_parent_model_id",false);
		}
		if(this.getAlias_parent_thread_id()!=null&&this.getAlias_parent_thread_idDirtyFlag()){
			map.put("alias_parent_thread_id",this.getAlias_parent_thread_id());
		}else if(this.getAlias_parent_thread_idDirtyFlag()){
			map.put("alias_parent_thread_id",false);
		}
		if(this.getAlias_user_id()!=null&&this.getAlias_user_idDirtyFlag()){
			map.put("alias_user_id",this.getAlias_user_id());
		}else if(this.getAlias_user_idDirtyFlag()){
			map.put("alias_user_id",false);
		}
		if(this.getAnonymous_name()!=null&&this.getAnonymous_nameDirtyFlag()){
			map.put("anonymous_name",this.getAnonymous_name());
		}else if(this.getAnonymous_nameDirtyFlag()){
			map.put("anonymous_name",false);
		}
		if(this.getChannel_last_seen_partner_ids()!=null&&this.getChannel_last_seen_partner_idsDirtyFlag()){
			map.put("channel_last_seen_partner_ids",this.getChannel_last_seen_partner_ids());
		}else if(this.getChannel_last_seen_partner_idsDirtyFlag()){
			map.put("channel_last_seen_partner_ids",false);
		}
		if(this.getChannel_message_ids()!=null&&this.getChannel_message_idsDirtyFlag()){
			map.put("channel_message_ids",this.getChannel_message_ids());
		}else if(this.getChannel_message_idsDirtyFlag()){
			map.put("channel_message_ids",false);
		}
		if(this.getChannel_partner_ids()!=null&&this.getChannel_partner_idsDirtyFlag()){
			map.put("channel_partner_ids",this.getChannel_partner_ids());
		}else if(this.getChannel_partner_idsDirtyFlag()){
			map.put("channel_partner_ids",false);
		}
		if(this.getChannel_type()!=null&&this.getChannel_typeDirtyFlag()){
			map.put("channel_type",this.getChannel_type());
		}else if(this.getChannel_typeDirtyFlag()){
			map.put("channel_type",false);
		}
		if(this.getCreate_date()!=null&&this.getCreate_dateDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String datetimeStr = sdf.format(this.getCreate_date());
			map.put("create_date",datetimeStr);
		}else if(this.getCreate_dateDirtyFlag()){
			map.put("create_date",false);
		}
		if(this.getCreate_uid()!=null&&this.getCreate_uidDirtyFlag()){
			map.put("create_uid",this.getCreate_uid());
		}else if(this.getCreate_uidDirtyFlag()){
			map.put("create_uid",false);
		}
		if(this.getCreate_uid_text()!=null&&this.getCreate_uid_textDirtyFlag()){
			//忽略文本外键create_uid_text
		}else if(this.getCreate_uid_textDirtyFlag()){
			map.put("create_uid",false);
		}
		if(this.getDescription()!=null&&this.getDescriptionDirtyFlag()){
			map.put("description",this.getDescription());
		}else if(this.getDescriptionDirtyFlag()){
			map.put("description",false);
		}
		if(this.getDisplay_name()!=null&&this.getDisplay_nameDirtyFlag()){
			map.put("display_name",this.getDisplay_name());
		}else if(this.getDisplay_nameDirtyFlag()){
			map.put("display_name",false);
		}
		if(this.getEmail_send()!=null&&this.getEmail_sendDirtyFlag()){
			map.put("email_send",Boolean.parseBoolean(this.getEmail_send()));		
		}		if(this.getGroup_ids()!=null&&this.getGroup_idsDirtyFlag()){
			map.put("group_ids",this.getGroup_ids());
		}else if(this.getGroup_idsDirtyFlag()){
			map.put("group_ids",false);
		}
		if(this.getGroup_public_id()!=null&&this.getGroup_public_idDirtyFlag()){
			map.put("group_public_id",this.getGroup_public_id());
		}else if(this.getGroup_public_idDirtyFlag()){
			map.put("group_public_id",false);
		}
		if(this.getGroup_public_id_text()!=null&&this.getGroup_public_id_textDirtyFlag()){
			//忽略文本外键group_public_id_text
		}else if(this.getGroup_public_id_textDirtyFlag()){
			map.put("group_public_id",false);
		}
		if(this.getIbizpublic()!=null&&this.getIbizpublicDirtyFlag()){
			map.put("public",this.getIbizpublic());
		}else if(this.getIbizpublicDirtyFlag()){
			map.put("public",false);
		}
		if(this.getId()!=null&&this.getIdDirtyFlag()){
			map.put("id",this.getId());
		}else if(this.getIdDirtyFlag()){
			map.put("id",false);
		}
		if(this.getImage()!=null&&this.getImageDirtyFlag()){
			//暂不支持binary类型image
		}else if(this.getImageDirtyFlag()){
			map.put("image",false);
		}
		if(this.getImage_medium()!=null&&this.getImage_mediumDirtyFlag()){
			//暂不支持binary类型image_medium
		}else if(this.getImage_mediumDirtyFlag()){
			map.put("image_medium",false);
		}
		if(this.getImage_small()!=null&&this.getImage_smallDirtyFlag()){
			//暂不支持binary类型image_small
		}else if(this.getImage_smallDirtyFlag()){
			map.put("image_small",false);
		}
		if(this.getIs_chat()!=null&&this.getIs_chatDirtyFlag()){
			map.put("is_chat",Boolean.parseBoolean(this.getIs_chat()));		
		}		if(this.getIs_member()!=null&&this.getIs_memberDirtyFlag()){
			map.put("is_member",Boolean.parseBoolean(this.getIs_member()));		
		}		if(this.getIs_moderator()!=null&&this.getIs_moderatorDirtyFlag()){
			map.put("is_moderator",Boolean.parseBoolean(this.getIs_moderator()));		
		}		if(this.getIs_subscribed()!=null&&this.getIs_subscribedDirtyFlag()){
			map.put("is_subscribed",Boolean.parseBoolean(this.getIs_subscribed()));		
		}		if(this.getLivechat_channel_id()!=null&&this.getLivechat_channel_idDirtyFlag()){
			map.put("livechat_channel_id",this.getLivechat_channel_id());
		}else if(this.getLivechat_channel_idDirtyFlag()){
			map.put("livechat_channel_id",false);
		}
		if(this.getLivechat_channel_id_text()!=null&&this.getLivechat_channel_id_textDirtyFlag()){
			//忽略文本外键livechat_channel_id_text
		}else if(this.getLivechat_channel_id_textDirtyFlag()){
			map.put("livechat_channel_id",false);
		}
		if(this.getMessage_attachment_count()!=null&&this.getMessage_attachment_countDirtyFlag()){
			map.put("message_attachment_count",this.getMessage_attachment_count());
		}else if(this.getMessage_attachment_countDirtyFlag()){
			map.put("message_attachment_count",false);
		}
		if(this.getMessage_channel_ids()!=null&&this.getMessage_channel_idsDirtyFlag()){
			map.put("message_channel_ids",this.getMessage_channel_ids());
		}else if(this.getMessage_channel_idsDirtyFlag()){
			map.put("message_channel_ids",false);
		}
		if(this.getMessage_follower_ids()!=null&&this.getMessage_follower_idsDirtyFlag()){
			map.put("message_follower_ids",this.getMessage_follower_ids());
		}else if(this.getMessage_follower_idsDirtyFlag()){
			map.put("message_follower_ids",false);
		}
		if(this.getMessage_has_error()!=null&&this.getMessage_has_errorDirtyFlag()){
			map.put("message_has_error",Boolean.parseBoolean(this.getMessage_has_error()));		
		}		if(this.getMessage_has_error_counter()!=null&&this.getMessage_has_error_counterDirtyFlag()){
			map.put("message_has_error_counter",this.getMessage_has_error_counter());
		}else if(this.getMessage_has_error_counterDirtyFlag()){
			map.put("message_has_error_counter",false);
		}
		if(this.getMessage_ids()!=null&&this.getMessage_idsDirtyFlag()){
			map.put("message_ids",this.getMessage_ids());
		}else if(this.getMessage_idsDirtyFlag()){
			map.put("message_ids",false);
		}
		if(this.getMessage_is_follower()!=null&&this.getMessage_is_followerDirtyFlag()){
			map.put("message_is_follower",Boolean.parseBoolean(this.getMessage_is_follower()));		
		}		if(this.getMessage_main_attachment_id()!=null&&this.getMessage_main_attachment_idDirtyFlag()){
			map.put("message_main_attachment_id",this.getMessage_main_attachment_id());
		}else if(this.getMessage_main_attachment_idDirtyFlag()){
			map.put("message_main_attachment_id",false);
		}
		if(this.getMessage_needaction()!=null&&this.getMessage_needactionDirtyFlag()){
			map.put("message_needaction",Boolean.parseBoolean(this.getMessage_needaction()));		
		}		if(this.getMessage_needaction_counter()!=null&&this.getMessage_needaction_counterDirtyFlag()){
			map.put("message_needaction_counter",this.getMessage_needaction_counter());
		}else if(this.getMessage_needaction_counterDirtyFlag()){
			map.put("message_needaction_counter",false);
		}
		if(this.getMessage_partner_ids()!=null&&this.getMessage_partner_idsDirtyFlag()){
			map.put("message_partner_ids",this.getMessage_partner_ids());
		}else if(this.getMessage_partner_idsDirtyFlag()){
			map.put("message_partner_ids",false);
		}
		if(this.getMessage_unread()!=null&&this.getMessage_unreadDirtyFlag()){
			map.put("message_unread",Boolean.parseBoolean(this.getMessage_unread()));		
		}		if(this.getMessage_unread_counter()!=null&&this.getMessage_unread_counterDirtyFlag()){
			map.put("message_unread_counter",this.getMessage_unread_counter());
		}else if(this.getMessage_unread_counterDirtyFlag()){
			map.put("message_unread_counter",false);
		}
		if(this.getModeration()!=null&&this.getModerationDirtyFlag()){
			map.put("moderation",Boolean.parseBoolean(this.getModeration()));		
		}		if(this.getModeration_count()!=null&&this.getModeration_countDirtyFlag()){
			map.put("moderation_count",this.getModeration_count());
		}else if(this.getModeration_countDirtyFlag()){
			map.put("moderation_count",false);
		}
		if(this.getModeration_guidelines()!=null&&this.getModeration_guidelinesDirtyFlag()){
			map.put("moderation_guidelines",Boolean.parseBoolean(this.getModeration_guidelines()));		
		}		if(this.getModeration_guidelines_msg()!=null&&this.getModeration_guidelines_msgDirtyFlag()){
			map.put("moderation_guidelines_msg",this.getModeration_guidelines_msg());
		}else if(this.getModeration_guidelines_msgDirtyFlag()){
			map.put("moderation_guidelines_msg",false);
		}
		if(this.getModeration_ids()!=null&&this.getModeration_idsDirtyFlag()){
			map.put("moderation_ids",this.getModeration_ids());
		}else if(this.getModeration_idsDirtyFlag()){
			map.put("moderation_ids",false);
		}
		if(this.getModeration_notify()!=null&&this.getModeration_notifyDirtyFlag()){
			map.put("moderation_notify",Boolean.parseBoolean(this.getModeration_notify()));		
		}		if(this.getModeration_notify_msg()!=null&&this.getModeration_notify_msgDirtyFlag()){
			map.put("moderation_notify_msg",this.getModeration_notify_msg());
		}else if(this.getModeration_notify_msgDirtyFlag()){
			map.put("moderation_notify_msg",false);
		}
		if(this.getModerator_ids()!=null&&this.getModerator_idsDirtyFlag()){
			map.put("moderator_ids",this.getModerator_ids());
		}else if(this.getModerator_idsDirtyFlag()){
			map.put("moderator_ids",false);
		}
		if(this.getName()!=null&&this.getNameDirtyFlag()){
			map.put("name",this.getName());
		}else if(this.getNameDirtyFlag()){
			map.put("name",false);
		}
		if(this.getRating_count()!=null&&this.getRating_countDirtyFlag()){
			map.put("rating_count",this.getRating_count());
		}else if(this.getRating_countDirtyFlag()){
			map.put("rating_count",false);
		}
		if(this.getRating_ids()!=null&&this.getRating_idsDirtyFlag()){
			map.put("rating_ids",this.getRating_ids());
		}else if(this.getRating_idsDirtyFlag()){
			map.put("rating_ids",false);
		}
		if(this.getRating_last_feedback()!=null&&this.getRating_last_feedbackDirtyFlag()){
			map.put("rating_last_feedback",this.getRating_last_feedback());
		}else if(this.getRating_last_feedbackDirtyFlag()){
			map.put("rating_last_feedback",false);
		}
		if(this.getRating_last_image()!=null&&this.getRating_last_imageDirtyFlag()){
			//暂不支持binary类型rating_last_image
		}else if(this.getRating_last_imageDirtyFlag()){
			map.put("rating_last_image",false);
		}
		if(this.getRating_last_value()!=null&&this.getRating_last_valueDirtyFlag()){
			map.put("rating_last_value",this.getRating_last_value());
		}else if(this.getRating_last_valueDirtyFlag()){
			map.put("rating_last_value",false);
		}
		if(this.getSubscription_department_ids()!=null&&this.getSubscription_department_idsDirtyFlag()){
			map.put("subscription_department_ids",this.getSubscription_department_ids());
		}else if(this.getSubscription_department_idsDirtyFlag()){
			map.put("subscription_department_ids",false);
		}
		if(this.getUuid()!=null&&this.getUuidDirtyFlag()){
			map.put("uuid",this.getUuid());
		}else if(this.getUuidDirtyFlag()){
			map.put("uuid",false);
		}
		if(this.getWebsite_message_ids()!=null&&this.getWebsite_message_idsDirtyFlag()){
			map.put("website_message_ids",this.getWebsite_message_ids());
		}else if(this.getWebsite_message_idsDirtyFlag()){
			map.put("website_message_ids",false);
		}
		if(this.getWrite_date()!=null&&this.getWrite_dateDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String datetimeStr = sdf.format(this.getWrite_date());
			map.put("write_date",datetimeStr);
		}else if(this.getWrite_dateDirtyFlag()){
			map.put("write_date",false);
		}
		if(this.getWrite_uid()!=null&&this.getWrite_uidDirtyFlag()){
			map.put("write_uid",this.getWrite_uid());
		}else if(this.getWrite_uidDirtyFlag()){
			map.put("write_uid",false);
		}
		if(this.getWrite_uid_text()!=null&&this.getWrite_uid_textDirtyFlag()){
			//忽略文本外键write_uid_text
		}else if(this.getWrite_uid_textDirtyFlag()){
			map.put("write_uid",false);
		}
		if(this.get__last_update()!=null&&this.get__last_updateDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String datetimeStr = sdf.format(this.get__last_update());
			map.put("__last_update",datetimeStr);
		}else if(this.get__last_updateDirtyFlag()){
			map.put("__last_update",false);
		}
		return map;
	}

}
