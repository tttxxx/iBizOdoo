package cn.ibizlab.odoo.client.odoo_mail.odooclient;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.core.client.model.Imail_alias_mixin;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[mail_alias_mixin] 服务对象客户端接口
 */
public interface Imail_alias_mixinOdooClient {
    
        public void updateBatch(Imail_alias_mixin mail_alias_mixin);

        public void remove(Imail_alias_mixin mail_alias_mixin);

        public void removeBatch(Imail_alias_mixin mail_alias_mixin);

        public void update(Imail_alias_mixin mail_alias_mixin);

        public void create(Imail_alias_mixin mail_alias_mixin);

        public void get(Imail_alias_mixin mail_alias_mixin);

        public Page<Imail_alias_mixin> search(SearchContext context);

        public void createBatch(Imail_alias_mixin mail_alias_mixin);

        public List<Imail_alias_mixin> select();


}