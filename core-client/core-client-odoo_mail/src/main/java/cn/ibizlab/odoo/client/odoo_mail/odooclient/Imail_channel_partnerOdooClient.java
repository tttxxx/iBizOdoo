package cn.ibizlab.odoo.client.odoo_mail.odooclient;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.core.client.model.Imail_channel_partner;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[mail_channel_partner] 服务对象客户端接口
 */
public interface Imail_channel_partnerOdooClient {
    
        public void updateBatch(Imail_channel_partner mail_channel_partner);

        public void removeBatch(Imail_channel_partner mail_channel_partner);

        public void createBatch(Imail_channel_partner mail_channel_partner);

        public void create(Imail_channel_partner mail_channel_partner);

        public Page<Imail_channel_partner> search(SearchContext context);

        public void update(Imail_channel_partner mail_channel_partner);

        public void remove(Imail_channel_partner mail_channel_partner);

        public void get(Imail_channel_partner mail_channel_partner);

        public List<Imail_channel_partner> select();


}