package cn.ibizlab.odoo.client.odoo_mail.odooclient;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.core.client.model.Imail_notification;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[mail_notification] 服务对象客户端接口
 */
public interface Imail_notificationOdooClient {
    
        public void createBatch(Imail_notification mail_notification);

        public void updateBatch(Imail_notification mail_notification);

        public void create(Imail_notification mail_notification);

        public void remove(Imail_notification mail_notification);

        public Page<Imail_notification> search(SearchContext context);

        public void get(Imail_notification mail_notification);

        public void update(Imail_notification mail_notification);

        public void removeBatch(Imail_notification mail_notification);

        public List<Imail_notification> select();


}