package cn.ibizlab.odoo.client.odoo_mail.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Imail_alias_mixin;
import cn.ibizlab.odoo.core.client.service.Imail_alias_mixinClientService;
import cn.ibizlab.odoo.client.odoo_mail.model.mail_alias_mixinImpl;
import cn.ibizlab.odoo.client.odoo_mail.odooclient.Imail_alias_mixinOdooClient;
import cn.ibizlab.odoo.client.odoo_mail.odooclient.impl.mail_alias_mixinOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[mail_alias_mixin] 服务对象接口
 */
@Service
public class mail_alias_mixinClientServiceImpl implements Imail_alias_mixinClientService {
    @Autowired
    private  Imail_alias_mixinOdooClient  mail_alias_mixinOdooClient;

    public Imail_alias_mixin createModel() {		
		return new mail_alias_mixinImpl();
	}


        public void updateBatch(List<Imail_alias_mixin> mail_alias_mixins){
            
        }
        
        public void remove(Imail_alias_mixin mail_alias_mixin){
this.mail_alias_mixinOdooClient.remove(mail_alias_mixin) ;
        }
        
        public void removeBatch(List<Imail_alias_mixin> mail_alias_mixins){
            
        }
        
        public void update(Imail_alias_mixin mail_alias_mixin){
this.mail_alias_mixinOdooClient.update(mail_alias_mixin) ;
        }
        
        public void create(Imail_alias_mixin mail_alias_mixin){
this.mail_alias_mixinOdooClient.create(mail_alias_mixin) ;
        }
        
        public void get(Imail_alias_mixin mail_alias_mixin){
            this.mail_alias_mixinOdooClient.get(mail_alias_mixin) ;
        }
        
        public Page<Imail_alias_mixin> search(SearchContext context){
            return this.mail_alias_mixinOdooClient.search(context) ;
        }
        
        public void createBatch(List<Imail_alias_mixin> mail_alias_mixins){
            
        }
        
        public Page<Imail_alias_mixin> select(SearchContext context){
            return null ;
        }
        

}

