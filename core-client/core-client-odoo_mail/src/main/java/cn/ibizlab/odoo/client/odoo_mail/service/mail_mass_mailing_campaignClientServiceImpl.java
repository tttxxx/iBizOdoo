package cn.ibizlab.odoo.client.odoo_mail.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Imail_mass_mailing_campaign;
import cn.ibizlab.odoo.core.client.service.Imail_mass_mailing_campaignClientService;
import cn.ibizlab.odoo.client.odoo_mail.model.mail_mass_mailing_campaignImpl;
import cn.ibizlab.odoo.client.odoo_mail.odooclient.Imail_mass_mailing_campaignOdooClient;
import cn.ibizlab.odoo.client.odoo_mail.odooclient.impl.mail_mass_mailing_campaignOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[mail_mass_mailing_campaign] 服务对象接口
 */
@Service
public class mail_mass_mailing_campaignClientServiceImpl implements Imail_mass_mailing_campaignClientService {
    @Autowired
    private  Imail_mass_mailing_campaignOdooClient  mail_mass_mailing_campaignOdooClient;

    public Imail_mass_mailing_campaign createModel() {		
		return new mail_mass_mailing_campaignImpl();
	}


        public void update(Imail_mass_mailing_campaign mail_mass_mailing_campaign){
this.mail_mass_mailing_campaignOdooClient.update(mail_mass_mailing_campaign) ;
        }
        
        public void remove(Imail_mass_mailing_campaign mail_mass_mailing_campaign){
this.mail_mass_mailing_campaignOdooClient.remove(mail_mass_mailing_campaign) ;
        }
        
        public void createBatch(List<Imail_mass_mailing_campaign> mail_mass_mailing_campaigns){
            
        }
        
        public void updateBatch(List<Imail_mass_mailing_campaign> mail_mass_mailing_campaigns){
            
        }
        
        public void get(Imail_mass_mailing_campaign mail_mass_mailing_campaign){
            this.mail_mass_mailing_campaignOdooClient.get(mail_mass_mailing_campaign) ;
        }
        
        public Page<Imail_mass_mailing_campaign> search(SearchContext context){
            return this.mail_mass_mailing_campaignOdooClient.search(context) ;
        }
        
        public void create(Imail_mass_mailing_campaign mail_mass_mailing_campaign){
this.mail_mass_mailing_campaignOdooClient.create(mail_mass_mailing_campaign) ;
        }
        
        public void removeBatch(List<Imail_mass_mailing_campaign> mail_mass_mailing_campaigns){
            
        }
        
        public Page<Imail_mass_mailing_campaign> select(SearchContext context){
            return null ;
        }
        

}

