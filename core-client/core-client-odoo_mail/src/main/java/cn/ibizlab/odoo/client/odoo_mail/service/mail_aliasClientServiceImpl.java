package cn.ibizlab.odoo.client.odoo_mail.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Imail_alias;
import cn.ibizlab.odoo.core.client.service.Imail_aliasClientService;
import cn.ibizlab.odoo.client.odoo_mail.model.mail_aliasImpl;
import cn.ibizlab.odoo.client.odoo_mail.odooclient.Imail_aliasOdooClient;
import cn.ibizlab.odoo.client.odoo_mail.odooclient.impl.mail_aliasOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[mail_alias] 服务对象接口
 */
@Service
public class mail_aliasClientServiceImpl implements Imail_aliasClientService {
    @Autowired
    private  Imail_aliasOdooClient  mail_aliasOdooClient;

    public Imail_alias createModel() {		
		return new mail_aliasImpl();
	}


        public void removeBatch(List<Imail_alias> mail_aliases){
            
        }
        
        public void update(Imail_alias mail_alias){
this.mail_aliasOdooClient.update(mail_alias) ;
        }
        
        public void updateBatch(List<Imail_alias> mail_aliases){
            
        }
        
        public void get(Imail_alias mail_alias){
            this.mail_aliasOdooClient.get(mail_alias) ;
        }
        
        public void remove(Imail_alias mail_alias){
this.mail_aliasOdooClient.remove(mail_alias) ;
        }
        
        public void create(Imail_alias mail_alias){
this.mail_aliasOdooClient.create(mail_alias) ;
        }
        
        public void createBatch(List<Imail_alias> mail_aliases){
            
        }
        
        public Page<Imail_alias> search(SearchContext context){
            return this.mail_aliasOdooClient.search(context) ;
        }
        
        public Page<Imail_alias> select(SearchContext context){
            return null ;
        }
        

}

