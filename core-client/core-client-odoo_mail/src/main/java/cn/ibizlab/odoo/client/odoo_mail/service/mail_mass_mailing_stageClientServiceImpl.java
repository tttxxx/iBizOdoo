package cn.ibizlab.odoo.client.odoo_mail.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Imail_mass_mailing_stage;
import cn.ibizlab.odoo.core.client.service.Imail_mass_mailing_stageClientService;
import cn.ibizlab.odoo.client.odoo_mail.model.mail_mass_mailing_stageImpl;
import cn.ibizlab.odoo.client.odoo_mail.odooclient.Imail_mass_mailing_stageOdooClient;
import cn.ibizlab.odoo.client.odoo_mail.odooclient.impl.mail_mass_mailing_stageOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[mail_mass_mailing_stage] 服务对象接口
 */
@Service
public class mail_mass_mailing_stageClientServiceImpl implements Imail_mass_mailing_stageClientService {
    @Autowired
    private  Imail_mass_mailing_stageOdooClient  mail_mass_mailing_stageOdooClient;

    public Imail_mass_mailing_stage createModel() {		
		return new mail_mass_mailing_stageImpl();
	}


        public void create(Imail_mass_mailing_stage mail_mass_mailing_stage){
this.mail_mass_mailing_stageOdooClient.create(mail_mass_mailing_stage) ;
        }
        
        public void updateBatch(List<Imail_mass_mailing_stage> mail_mass_mailing_stages){
            
        }
        
        public void createBatch(List<Imail_mass_mailing_stage> mail_mass_mailing_stages){
            
        }
        
        public void removeBatch(List<Imail_mass_mailing_stage> mail_mass_mailing_stages){
            
        }
        
        public void update(Imail_mass_mailing_stage mail_mass_mailing_stage){
this.mail_mass_mailing_stageOdooClient.update(mail_mass_mailing_stage) ;
        }
        
        public void remove(Imail_mass_mailing_stage mail_mass_mailing_stage){
this.mail_mass_mailing_stageOdooClient.remove(mail_mass_mailing_stage) ;
        }
        
        public void get(Imail_mass_mailing_stage mail_mass_mailing_stage){
            this.mail_mass_mailing_stageOdooClient.get(mail_mass_mailing_stage) ;
        }
        
        public Page<Imail_mass_mailing_stage> search(SearchContext context){
            return this.mail_mass_mailing_stageOdooClient.search(context) ;
        }
        
        public Page<Imail_mass_mailing_stage> select(SearchContext context){
            return null ;
        }
        

}

