package cn.ibizlab.odoo.client.odoo_mail.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Imail_resend_partner;
import cn.ibizlab.odoo.core.client.service.Imail_resend_partnerClientService;
import cn.ibizlab.odoo.client.odoo_mail.model.mail_resend_partnerImpl;
import cn.ibizlab.odoo.client.odoo_mail.odooclient.Imail_resend_partnerOdooClient;
import cn.ibizlab.odoo.client.odoo_mail.odooclient.impl.mail_resend_partnerOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[mail_resend_partner] 服务对象接口
 */
@Service
public class mail_resend_partnerClientServiceImpl implements Imail_resend_partnerClientService {
    @Autowired
    private  Imail_resend_partnerOdooClient  mail_resend_partnerOdooClient;

    public Imail_resend_partner createModel() {		
		return new mail_resend_partnerImpl();
	}


        public void get(Imail_resend_partner mail_resend_partner){
            this.mail_resend_partnerOdooClient.get(mail_resend_partner) ;
        }
        
        public void update(Imail_resend_partner mail_resend_partner){
this.mail_resend_partnerOdooClient.update(mail_resend_partner) ;
        }
        
        public void updateBatch(List<Imail_resend_partner> mail_resend_partners){
            
        }
        
        public void removeBatch(List<Imail_resend_partner> mail_resend_partners){
            
        }
        
        public void create(Imail_resend_partner mail_resend_partner){
this.mail_resend_partnerOdooClient.create(mail_resend_partner) ;
        }
        
        public void createBatch(List<Imail_resend_partner> mail_resend_partners){
            
        }
        
        public void remove(Imail_resend_partner mail_resend_partner){
this.mail_resend_partnerOdooClient.remove(mail_resend_partner) ;
        }
        
        public Page<Imail_resend_partner> search(SearchContext context){
            return this.mail_resend_partnerOdooClient.search(context) ;
        }
        
        public Page<Imail_resend_partner> select(SearchContext context){
            return null ;
        }
        

}

