package cn.ibizlab.odoo.client.odoo_mail.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Imail_compose_message;
import cn.ibizlab.odoo.core.client.service.Imail_compose_messageClientService;
import cn.ibizlab.odoo.client.odoo_mail.model.mail_compose_messageImpl;
import cn.ibizlab.odoo.client.odoo_mail.odooclient.Imail_compose_messageOdooClient;
import cn.ibizlab.odoo.client.odoo_mail.odooclient.impl.mail_compose_messageOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[mail_compose_message] 服务对象接口
 */
@Service
public class mail_compose_messageClientServiceImpl implements Imail_compose_messageClientService {
    @Autowired
    private  Imail_compose_messageOdooClient  mail_compose_messageOdooClient;

    public Imail_compose_message createModel() {		
		return new mail_compose_messageImpl();
	}


        public void remove(Imail_compose_message mail_compose_message){
this.mail_compose_messageOdooClient.remove(mail_compose_message) ;
        }
        
        public Page<Imail_compose_message> search(SearchContext context){
            return this.mail_compose_messageOdooClient.search(context) ;
        }
        
        public void update(Imail_compose_message mail_compose_message){
this.mail_compose_messageOdooClient.update(mail_compose_message) ;
        }
        
        public void removeBatch(List<Imail_compose_message> mail_compose_messages){
            
        }
        
        public void updateBatch(List<Imail_compose_message> mail_compose_messages){
            
        }
        
        public void create(Imail_compose_message mail_compose_message){
this.mail_compose_messageOdooClient.create(mail_compose_message) ;
        }
        
        public void createBatch(List<Imail_compose_message> mail_compose_messages){
            
        }
        
        public void get(Imail_compose_message mail_compose_message){
            this.mail_compose_messageOdooClient.get(mail_compose_message) ;
        }
        
        public Page<Imail_compose_message> select(SearchContext context){
            return null ;
        }
        

}

