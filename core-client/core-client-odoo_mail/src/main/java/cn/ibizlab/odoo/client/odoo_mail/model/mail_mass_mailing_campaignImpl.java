package cn.ibizlab.odoo.client.odoo_mail.model;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.math.BigInteger;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.core.client.model.Imail_mass_mailing_campaign;
import cn.ibizlab.odoo.util.helper.OdooClientHelper;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang.ArrayUtils;
import org.springframework.util.StringUtils;

/**
 * 接口实体[mail_mass_mailing_campaign] 对象
 */
public class mail_mass_mailing_campaignImpl implements Imail_mass_mailing_campaign,Serializable{

    /**
     * 被退回
     */
    public Integer bounced;

    @JsonIgnore
    public boolean bouncedDirtyFlag;
    
    /**
     * 被退回的比率
     */
    public Integer bounced_ratio;

    @JsonIgnore
    public boolean bounced_ratioDirtyFlag;
    
    /**
     * 运动_ ID
     */
    public Integer campaign_id;

    @JsonIgnore
    public boolean campaign_idDirtyFlag;
    
    /**
     * 点击数
     */
    public Integer clicks_ratio;

    @JsonIgnore
    public boolean clicks_ratioDirtyFlag;
    
    /**
     * 颜色索引
     */
    public Integer color;

    @JsonIgnore
    public boolean colorDirtyFlag;
    
    /**
     * 创建时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp create_date;

    @JsonIgnore
    public boolean create_dateDirtyFlag;
    
    /**
     * 创建人
     */
    public Integer create_uid;

    @JsonIgnore
    public boolean create_uidDirtyFlag;
    
    /**
     * 创建人
     */
    public String create_uid_text;

    @JsonIgnore
    public boolean create_uid_textDirtyFlag;
    
    /**
     * 已送货
     */
    public Integer delivered;

    @JsonIgnore
    public boolean deliveredDirtyFlag;
    
    /**
     * 显示名称
     */
    public String display_name;

    @JsonIgnore
    public boolean display_nameDirtyFlag;
    
    /**
     * 失败的
     */
    public Integer failed;

    @JsonIgnore
    public boolean failedDirtyFlag;
    
    /**
     * ID
     */
    public Integer id;

    @JsonIgnore
    public boolean idDirtyFlag;
    
    /**
     * 忽略
     */
    public Integer ignored;

    @JsonIgnore
    public boolean ignoredDirtyFlag;
    
    /**
     * 群发邮件
     */
    public String mass_mailing_ids;

    @JsonIgnore
    public boolean mass_mailing_idsDirtyFlag;
    
    /**
     * 媒体
     */
    public Integer medium_id;

    @JsonIgnore
    public boolean medium_idDirtyFlag;
    
    /**
     * 媒体
     */
    public String medium_id_text;

    @JsonIgnore
    public boolean medium_id_textDirtyFlag;
    
    /**
     * 营销名称
     */
    public String name;

    @JsonIgnore
    public boolean nameDirtyFlag;
    
    /**
     * 已开启
     */
    public Integer opened;

    @JsonIgnore
    public boolean openedDirtyFlag;
    
    /**
     * 打开比例
     */
    public Integer opened_ratio;

    @JsonIgnore
    public boolean opened_ratioDirtyFlag;
    
    /**
     * 已接收比例
     */
    public Integer received_ratio;

    @JsonIgnore
    public boolean received_ratioDirtyFlag;
    
    /**
     * 已回复
     */
    public Integer replied;

    @JsonIgnore
    public boolean repliedDirtyFlag;
    
    /**
     * 回复比例
     */
    public Integer replied_ratio;

    @JsonIgnore
    public boolean replied_ratioDirtyFlag;
    
    /**
     * 安排
     */
    public Integer scheduled;

    @JsonIgnore
    public boolean scheduledDirtyFlag;
    
    /**
     * 发送邮件
     */
    public Integer sent;

    @JsonIgnore
    public boolean sentDirtyFlag;
    
    /**
     * 来源
     */
    public Integer source_id;

    @JsonIgnore
    public boolean source_idDirtyFlag;
    
    /**
     * 来源
     */
    public String source_id_text;

    @JsonIgnore
    public boolean source_id_textDirtyFlag;
    
    /**
     * 阶段
     */
    public Integer stage_id;

    @JsonIgnore
    public boolean stage_idDirtyFlag;
    
    /**
     * 阶段
     */
    public String stage_id_text;

    @JsonIgnore
    public boolean stage_id_textDirtyFlag;
    
    /**
     * 标签
     */
    public String tag_ids;

    @JsonIgnore
    public boolean tag_idsDirtyFlag;
    
    /**
     * 总计
     */
    public Integer total;

    @JsonIgnore
    public boolean totalDirtyFlag;
    
    /**
     * 邮件
     */
    public Integer total_mailings;

    @JsonIgnore
    public boolean total_mailingsDirtyFlag;
    
    /**
     * 支持 A/B 测试
     */
    public String unique_ab_testing;

    @JsonIgnore
    public boolean unique_ab_testingDirtyFlag;
    
    /**
     * 负责人
     */
    public Integer user_id;

    @JsonIgnore
    public boolean user_idDirtyFlag;
    
    /**
     * 负责人
     */
    public String user_id_text;

    @JsonIgnore
    public boolean user_id_textDirtyFlag;
    
    /**
     * 最后更新时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp write_date;

    @JsonIgnore
    public boolean write_dateDirtyFlag;
    
    /**
     * 最后更新者
     */
    public Integer write_uid;

    @JsonIgnore
    public boolean write_uidDirtyFlag;
    
    /**
     * 最后更新者
     */
    public String write_uid_text;

    @JsonIgnore
    public boolean write_uid_textDirtyFlag;
    
    /**
     * 最后修改日
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp __last_update;

    @JsonIgnore
    public boolean __last_updateDirtyFlag;
    
    /**
     * 获取 [被退回]
     */
    @JsonProperty("bounced")
    public Integer getBounced(){
        return this.bounced ;
    }

    /**
     * 设置 [被退回]
     */
    @JsonProperty("bounced")
    public void setBounced(Integer  bounced){
        this.bounced = bounced ;
        this.bouncedDirtyFlag = true ;
    }

     /**
     * 获取 [被退回]脏标记
     */
    @JsonIgnore
    public boolean getBouncedDirtyFlag(){
        return this.bouncedDirtyFlag ;
    }   

    /**
     * 获取 [被退回的比率]
     */
    @JsonProperty("bounced_ratio")
    public Integer getBounced_ratio(){
        return this.bounced_ratio ;
    }

    /**
     * 设置 [被退回的比率]
     */
    @JsonProperty("bounced_ratio")
    public void setBounced_ratio(Integer  bounced_ratio){
        this.bounced_ratio = bounced_ratio ;
        this.bounced_ratioDirtyFlag = true ;
    }

     /**
     * 获取 [被退回的比率]脏标记
     */
    @JsonIgnore
    public boolean getBounced_ratioDirtyFlag(){
        return this.bounced_ratioDirtyFlag ;
    }   

    /**
     * 获取 [运动_ ID]
     */
    @JsonProperty("campaign_id")
    public Integer getCampaign_id(){
        return this.campaign_id ;
    }

    /**
     * 设置 [运动_ ID]
     */
    @JsonProperty("campaign_id")
    public void setCampaign_id(Integer  campaign_id){
        this.campaign_id = campaign_id ;
        this.campaign_idDirtyFlag = true ;
    }

     /**
     * 获取 [运动_ ID]脏标记
     */
    @JsonIgnore
    public boolean getCampaign_idDirtyFlag(){
        return this.campaign_idDirtyFlag ;
    }   

    /**
     * 获取 [点击数]
     */
    @JsonProperty("clicks_ratio")
    public Integer getClicks_ratio(){
        return this.clicks_ratio ;
    }

    /**
     * 设置 [点击数]
     */
    @JsonProperty("clicks_ratio")
    public void setClicks_ratio(Integer  clicks_ratio){
        this.clicks_ratio = clicks_ratio ;
        this.clicks_ratioDirtyFlag = true ;
    }

     /**
     * 获取 [点击数]脏标记
     */
    @JsonIgnore
    public boolean getClicks_ratioDirtyFlag(){
        return this.clicks_ratioDirtyFlag ;
    }   

    /**
     * 获取 [颜色索引]
     */
    @JsonProperty("color")
    public Integer getColor(){
        return this.color ;
    }

    /**
     * 设置 [颜色索引]
     */
    @JsonProperty("color")
    public void setColor(Integer  color){
        this.color = color ;
        this.colorDirtyFlag = true ;
    }

     /**
     * 获取 [颜色索引]脏标记
     */
    @JsonIgnore
    public boolean getColorDirtyFlag(){
        return this.colorDirtyFlag ;
    }   

    /**
     * 获取 [创建时间]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return this.create_date ;
    }

    /**
     * 设置 [创建时间]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

     /**
     * 获取 [创建时间]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return this.create_dateDirtyFlag ;
    }   

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return this.create_uid ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

     /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return this.create_uidDirtyFlag ;
    }   

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return this.create_uid_text ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

     /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return this.create_uid_textDirtyFlag ;
    }   

    /**
     * 获取 [已送货]
     */
    @JsonProperty("delivered")
    public Integer getDelivered(){
        return this.delivered ;
    }

    /**
     * 设置 [已送货]
     */
    @JsonProperty("delivered")
    public void setDelivered(Integer  delivered){
        this.delivered = delivered ;
        this.deliveredDirtyFlag = true ;
    }

     /**
     * 获取 [已送货]脏标记
     */
    @JsonIgnore
    public boolean getDeliveredDirtyFlag(){
        return this.deliveredDirtyFlag ;
    }   

    /**
     * 获取 [显示名称]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return this.display_name ;
    }

    /**
     * 设置 [显示名称]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

     /**
     * 获取 [显示名称]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return this.display_nameDirtyFlag ;
    }   

    /**
     * 获取 [失败的]
     */
    @JsonProperty("failed")
    public Integer getFailed(){
        return this.failed ;
    }

    /**
     * 设置 [失败的]
     */
    @JsonProperty("failed")
    public void setFailed(Integer  failed){
        this.failed = failed ;
        this.failedDirtyFlag = true ;
    }

     /**
     * 获取 [失败的]脏标记
     */
    @JsonIgnore
    public boolean getFailedDirtyFlag(){
        return this.failedDirtyFlag ;
    }   

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return this.id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

     /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return this.idDirtyFlag ;
    }   

    /**
     * 获取 [忽略]
     */
    @JsonProperty("ignored")
    public Integer getIgnored(){
        return this.ignored ;
    }

    /**
     * 设置 [忽略]
     */
    @JsonProperty("ignored")
    public void setIgnored(Integer  ignored){
        this.ignored = ignored ;
        this.ignoredDirtyFlag = true ;
    }

     /**
     * 获取 [忽略]脏标记
     */
    @JsonIgnore
    public boolean getIgnoredDirtyFlag(){
        return this.ignoredDirtyFlag ;
    }   

    /**
     * 获取 [群发邮件]
     */
    @JsonProperty("mass_mailing_ids")
    public String getMass_mailing_ids(){
        return this.mass_mailing_ids ;
    }

    /**
     * 设置 [群发邮件]
     */
    @JsonProperty("mass_mailing_ids")
    public void setMass_mailing_ids(String  mass_mailing_ids){
        this.mass_mailing_ids = mass_mailing_ids ;
        this.mass_mailing_idsDirtyFlag = true ;
    }

     /**
     * 获取 [群发邮件]脏标记
     */
    @JsonIgnore
    public boolean getMass_mailing_idsDirtyFlag(){
        return this.mass_mailing_idsDirtyFlag ;
    }   

    /**
     * 获取 [媒体]
     */
    @JsonProperty("medium_id")
    public Integer getMedium_id(){
        return this.medium_id ;
    }

    /**
     * 设置 [媒体]
     */
    @JsonProperty("medium_id")
    public void setMedium_id(Integer  medium_id){
        this.medium_id = medium_id ;
        this.medium_idDirtyFlag = true ;
    }

     /**
     * 获取 [媒体]脏标记
     */
    @JsonIgnore
    public boolean getMedium_idDirtyFlag(){
        return this.medium_idDirtyFlag ;
    }   

    /**
     * 获取 [媒体]
     */
    @JsonProperty("medium_id_text")
    public String getMedium_id_text(){
        return this.medium_id_text ;
    }

    /**
     * 设置 [媒体]
     */
    @JsonProperty("medium_id_text")
    public void setMedium_id_text(String  medium_id_text){
        this.medium_id_text = medium_id_text ;
        this.medium_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [媒体]脏标记
     */
    @JsonIgnore
    public boolean getMedium_id_textDirtyFlag(){
        return this.medium_id_textDirtyFlag ;
    }   

    /**
     * 获取 [营销名称]
     */
    @JsonProperty("name")
    public String getName(){
        return this.name ;
    }

    /**
     * 设置 [营销名称]
     */
    @JsonProperty("name")
    public void setName(String  name){
        this.name = name ;
        this.nameDirtyFlag = true ;
    }

     /**
     * 获取 [营销名称]脏标记
     */
    @JsonIgnore
    public boolean getNameDirtyFlag(){
        return this.nameDirtyFlag ;
    }   

    /**
     * 获取 [已开启]
     */
    @JsonProperty("opened")
    public Integer getOpened(){
        return this.opened ;
    }

    /**
     * 设置 [已开启]
     */
    @JsonProperty("opened")
    public void setOpened(Integer  opened){
        this.opened = opened ;
        this.openedDirtyFlag = true ;
    }

     /**
     * 获取 [已开启]脏标记
     */
    @JsonIgnore
    public boolean getOpenedDirtyFlag(){
        return this.openedDirtyFlag ;
    }   

    /**
     * 获取 [打开比例]
     */
    @JsonProperty("opened_ratio")
    public Integer getOpened_ratio(){
        return this.opened_ratio ;
    }

    /**
     * 设置 [打开比例]
     */
    @JsonProperty("opened_ratio")
    public void setOpened_ratio(Integer  opened_ratio){
        this.opened_ratio = opened_ratio ;
        this.opened_ratioDirtyFlag = true ;
    }

     /**
     * 获取 [打开比例]脏标记
     */
    @JsonIgnore
    public boolean getOpened_ratioDirtyFlag(){
        return this.opened_ratioDirtyFlag ;
    }   

    /**
     * 获取 [已接收比例]
     */
    @JsonProperty("received_ratio")
    public Integer getReceived_ratio(){
        return this.received_ratio ;
    }

    /**
     * 设置 [已接收比例]
     */
    @JsonProperty("received_ratio")
    public void setReceived_ratio(Integer  received_ratio){
        this.received_ratio = received_ratio ;
        this.received_ratioDirtyFlag = true ;
    }

     /**
     * 获取 [已接收比例]脏标记
     */
    @JsonIgnore
    public boolean getReceived_ratioDirtyFlag(){
        return this.received_ratioDirtyFlag ;
    }   

    /**
     * 获取 [已回复]
     */
    @JsonProperty("replied")
    public Integer getReplied(){
        return this.replied ;
    }

    /**
     * 设置 [已回复]
     */
    @JsonProperty("replied")
    public void setReplied(Integer  replied){
        this.replied = replied ;
        this.repliedDirtyFlag = true ;
    }

     /**
     * 获取 [已回复]脏标记
     */
    @JsonIgnore
    public boolean getRepliedDirtyFlag(){
        return this.repliedDirtyFlag ;
    }   

    /**
     * 获取 [回复比例]
     */
    @JsonProperty("replied_ratio")
    public Integer getReplied_ratio(){
        return this.replied_ratio ;
    }

    /**
     * 设置 [回复比例]
     */
    @JsonProperty("replied_ratio")
    public void setReplied_ratio(Integer  replied_ratio){
        this.replied_ratio = replied_ratio ;
        this.replied_ratioDirtyFlag = true ;
    }

     /**
     * 获取 [回复比例]脏标记
     */
    @JsonIgnore
    public boolean getReplied_ratioDirtyFlag(){
        return this.replied_ratioDirtyFlag ;
    }   

    /**
     * 获取 [安排]
     */
    @JsonProperty("scheduled")
    public Integer getScheduled(){
        return this.scheduled ;
    }

    /**
     * 设置 [安排]
     */
    @JsonProperty("scheduled")
    public void setScheduled(Integer  scheduled){
        this.scheduled = scheduled ;
        this.scheduledDirtyFlag = true ;
    }

     /**
     * 获取 [安排]脏标记
     */
    @JsonIgnore
    public boolean getScheduledDirtyFlag(){
        return this.scheduledDirtyFlag ;
    }   

    /**
     * 获取 [发送邮件]
     */
    @JsonProperty("sent")
    public Integer getSent(){
        return this.sent ;
    }

    /**
     * 设置 [发送邮件]
     */
    @JsonProperty("sent")
    public void setSent(Integer  sent){
        this.sent = sent ;
        this.sentDirtyFlag = true ;
    }

     /**
     * 获取 [发送邮件]脏标记
     */
    @JsonIgnore
    public boolean getSentDirtyFlag(){
        return this.sentDirtyFlag ;
    }   

    /**
     * 获取 [来源]
     */
    @JsonProperty("source_id")
    public Integer getSource_id(){
        return this.source_id ;
    }

    /**
     * 设置 [来源]
     */
    @JsonProperty("source_id")
    public void setSource_id(Integer  source_id){
        this.source_id = source_id ;
        this.source_idDirtyFlag = true ;
    }

     /**
     * 获取 [来源]脏标记
     */
    @JsonIgnore
    public boolean getSource_idDirtyFlag(){
        return this.source_idDirtyFlag ;
    }   

    /**
     * 获取 [来源]
     */
    @JsonProperty("source_id_text")
    public String getSource_id_text(){
        return this.source_id_text ;
    }

    /**
     * 设置 [来源]
     */
    @JsonProperty("source_id_text")
    public void setSource_id_text(String  source_id_text){
        this.source_id_text = source_id_text ;
        this.source_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [来源]脏标记
     */
    @JsonIgnore
    public boolean getSource_id_textDirtyFlag(){
        return this.source_id_textDirtyFlag ;
    }   

    /**
     * 获取 [阶段]
     */
    @JsonProperty("stage_id")
    public Integer getStage_id(){
        return this.stage_id ;
    }

    /**
     * 设置 [阶段]
     */
    @JsonProperty("stage_id")
    public void setStage_id(Integer  stage_id){
        this.stage_id = stage_id ;
        this.stage_idDirtyFlag = true ;
    }

     /**
     * 获取 [阶段]脏标记
     */
    @JsonIgnore
    public boolean getStage_idDirtyFlag(){
        return this.stage_idDirtyFlag ;
    }   

    /**
     * 获取 [阶段]
     */
    @JsonProperty("stage_id_text")
    public String getStage_id_text(){
        return this.stage_id_text ;
    }

    /**
     * 设置 [阶段]
     */
    @JsonProperty("stage_id_text")
    public void setStage_id_text(String  stage_id_text){
        this.stage_id_text = stage_id_text ;
        this.stage_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [阶段]脏标记
     */
    @JsonIgnore
    public boolean getStage_id_textDirtyFlag(){
        return this.stage_id_textDirtyFlag ;
    }   

    /**
     * 获取 [标签]
     */
    @JsonProperty("tag_ids")
    public String getTag_ids(){
        return this.tag_ids ;
    }

    /**
     * 设置 [标签]
     */
    @JsonProperty("tag_ids")
    public void setTag_ids(String  tag_ids){
        this.tag_ids = tag_ids ;
        this.tag_idsDirtyFlag = true ;
    }

     /**
     * 获取 [标签]脏标记
     */
    @JsonIgnore
    public boolean getTag_idsDirtyFlag(){
        return this.tag_idsDirtyFlag ;
    }   

    /**
     * 获取 [总计]
     */
    @JsonProperty("total")
    public Integer getTotal(){
        return this.total ;
    }

    /**
     * 设置 [总计]
     */
    @JsonProperty("total")
    public void setTotal(Integer  total){
        this.total = total ;
        this.totalDirtyFlag = true ;
    }

     /**
     * 获取 [总计]脏标记
     */
    @JsonIgnore
    public boolean getTotalDirtyFlag(){
        return this.totalDirtyFlag ;
    }   

    /**
     * 获取 [邮件]
     */
    @JsonProperty("total_mailings")
    public Integer getTotal_mailings(){
        return this.total_mailings ;
    }

    /**
     * 设置 [邮件]
     */
    @JsonProperty("total_mailings")
    public void setTotal_mailings(Integer  total_mailings){
        this.total_mailings = total_mailings ;
        this.total_mailingsDirtyFlag = true ;
    }

     /**
     * 获取 [邮件]脏标记
     */
    @JsonIgnore
    public boolean getTotal_mailingsDirtyFlag(){
        return this.total_mailingsDirtyFlag ;
    }   

    /**
     * 获取 [支持 A/B 测试]
     */
    @JsonProperty("unique_ab_testing")
    public String getUnique_ab_testing(){
        return this.unique_ab_testing ;
    }

    /**
     * 设置 [支持 A/B 测试]
     */
    @JsonProperty("unique_ab_testing")
    public void setUnique_ab_testing(String  unique_ab_testing){
        this.unique_ab_testing = unique_ab_testing ;
        this.unique_ab_testingDirtyFlag = true ;
    }

     /**
     * 获取 [支持 A/B 测试]脏标记
     */
    @JsonIgnore
    public boolean getUnique_ab_testingDirtyFlag(){
        return this.unique_ab_testingDirtyFlag ;
    }   

    /**
     * 获取 [负责人]
     */
    @JsonProperty("user_id")
    public Integer getUser_id(){
        return this.user_id ;
    }

    /**
     * 设置 [负责人]
     */
    @JsonProperty("user_id")
    public void setUser_id(Integer  user_id){
        this.user_id = user_id ;
        this.user_idDirtyFlag = true ;
    }

     /**
     * 获取 [负责人]脏标记
     */
    @JsonIgnore
    public boolean getUser_idDirtyFlag(){
        return this.user_idDirtyFlag ;
    }   

    /**
     * 获取 [负责人]
     */
    @JsonProperty("user_id_text")
    public String getUser_id_text(){
        return this.user_id_text ;
    }

    /**
     * 设置 [负责人]
     */
    @JsonProperty("user_id_text")
    public void setUser_id_text(String  user_id_text){
        this.user_id_text = user_id_text ;
        this.user_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [负责人]脏标记
     */
    @JsonIgnore
    public boolean getUser_id_textDirtyFlag(){
        return this.user_id_textDirtyFlag ;
    }   

    /**
     * 获取 [最后更新时间]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return this.write_date ;
    }

    /**
     * 设置 [最后更新时间]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新时间]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return this.write_dateDirtyFlag ;
    }   

    /**
     * 获取 [最后更新者]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return this.write_uid ;
    }

    /**
     * 设置 [最后更新者]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新者]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return this.write_uidDirtyFlag ;
    }   

    /**
     * 获取 [最后更新者]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return this.write_uid_text ;
    }

    /**
     * 设置 [最后更新者]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新者]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return this.write_uid_textDirtyFlag ;
    }   

    /**
     * 获取 [最后修改日]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return this.__last_update ;
    }

    /**
     * 设置 [最后修改日]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

     /**
     * 获取 [最后修改日]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return this.__last_updateDirtyFlag ;
    }   



    public void fromMap(Map<String, Object> map) throws Exception {
		if(!(map.get("bounced") instanceof Boolean)&& map.get("bounced")!=null){
			this.setBounced((Integer)map.get("bounced"));
		}
		if(!(map.get("bounced_ratio") instanceof Boolean)&& map.get("bounced_ratio")!=null){
			this.setBounced_ratio((Integer)map.get("bounced_ratio"));
		}
		if(!(map.get("campaign_id") instanceof Boolean)&& map.get("campaign_id")!=null){
			Object[] objs = (Object[])map.get("campaign_id");
			if(objs.length > 0){
				this.setCampaign_id((Integer)objs[0]);
			}
		}
		if(!(map.get("clicks_ratio") instanceof Boolean)&& map.get("clicks_ratio")!=null){
			this.setClicks_ratio((Integer)map.get("clicks_ratio"));
		}
		if(!(map.get("color") instanceof Boolean)&& map.get("color")!=null){
			this.setColor((Integer)map.get("color"));
		}
		if(!(map.get("create_date") instanceof Boolean)&& map.get("create_date")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse((String)map.get("create_date"));
   			this.setCreate_date(new Timestamp(parse.getTime()));
		}
		if(!(map.get("create_uid") instanceof Boolean)&& map.get("create_uid")!=null){
			Object[] objs = (Object[])map.get("create_uid");
			if(objs.length > 0){
				this.setCreate_uid((Integer)objs[0]);
			}
		}
		if(!(map.get("create_uid") instanceof Boolean)&& map.get("create_uid")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("create_uid");
			if(objs.length > 1){
				this.setCreate_uid_text((String)objs[1]);
			}
		}
		if(!(map.get("delivered") instanceof Boolean)&& map.get("delivered")!=null){
			this.setDelivered((Integer)map.get("delivered"));
		}
		if(!(map.get("display_name") instanceof Boolean)&& map.get("display_name")!=null){
			this.setDisplay_name((String)map.get("display_name"));
		}
		if(!(map.get("failed") instanceof Boolean)&& map.get("failed")!=null){
			this.setFailed((Integer)map.get("failed"));
		}
		if(!(map.get("id") instanceof Boolean)&& map.get("id")!=null){
			this.setId((Integer)map.get("id"));
		}
		if(!(map.get("ignored") instanceof Boolean)&& map.get("ignored")!=null){
			this.setIgnored((Integer)map.get("ignored"));
		}
		if(!(map.get("mass_mailing_ids") instanceof Boolean)&& map.get("mass_mailing_ids")!=null){
			Object[] objs = (Object[])map.get("mass_mailing_ids");
			if(objs.length > 0){
				Integer[] mass_mailing_ids = Arrays.copyOfRange(objs,0,objs.length,Integer[].class);
				this.setMass_mailing_ids(Arrays.toString(mass_mailing_ids));
			}
		}
		if(!(map.get("medium_id") instanceof Boolean)&& map.get("medium_id")!=null){
			Object[] objs = (Object[])map.get("medium_id");
			if(objs.length > 0){
				this.setMedium_id((Integer)objs[0]);
			}
		}
		if(!(map.get("medium_id") instanceof Boolean)&& map.get("medium_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("medium_id");
			if(objs.length > 1){
				this.setMedium_id_text((String)objs[1]);
			}
		}
		if(!(map.get("name") instanceof Boolean)&& map.get("name")!=null){
			this.setName((String)map.get("name"));
		}
		if(!(map.get("opened") instanceof Boolean)&& map.get("opened")!=null){
			this.setOpened((Integer)map.get("opened"));
		}
		if(!(map.get("opened_ratio") instanceof Boolean)&& map.get("opened_ratio")!=null){
			this.setOpened_ratio((Integer)map.get("opened_ratio"));
		}
		if(!(map.get("received_ratio") instanceof Boolean)&& map.get("received_ratio")!=null){
			this.setReceived_ratio((Integer)map.get("received_ratio"));
		}
		if(!(map.get("replied") instanceof Boolean)&& map.get("replied")!=null){
			this.setReplied((Integer)map.get("replied"));
		}
		if(!(map.get("replied_ratio") instanceof Boolean)&& map.get("replied_ratio")!=null){
			this.setReplied_ratio((Integer)map.get("replied_ratio"));
		}
		if(!(map.get("scheduled") instanceof Boolean)&& map.get("scheduled")!=null){
			this.setScheduled((Integer)map.get("scheduled"));
		}
		if(!(map.get("sent") instanceof Boolean)&& map.get("sent")!=null){
			this.setSent((Integer)map.get("sent"));
		}
		if(!(map.get("source_id") instanceof Boolean)&& map.get("source_id")!=null){
			Object[] objs = (Object[])map.get("source_id");
			if(objs.length > 0){
				this.setSource_id((Integer)objs[0]);
			}
		}
		if(!(map.get("source_id") instanceof Boolean)&& map.get("source_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("source_id");
			if(objs.length > 1){
				this.setSource_id_text((String)objs[1]);
			}
		}
		if(!(map.get("stage_id") instanceof Boolean)&& map.get("stage_id")!=null){
			Object[] objs = (Object[])map.get("stage_id");
			if(objs.length > 0){
				this.setStage_id((Integer)objs[0]);
			}
		}
		if(!(map.get("stage_id") instanceof Boolean)&& map.get("stage_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("stage_id");
			if(objs.length > 1){
				this.setStage_id_text((String)objs[1]);
			}
		}
		if(!(map.get("tag_ids") instanceof Boolean)&& map.get("tag_ids")!=null){
			Object[] objs = (Object[])map.get("tag_ids");
			if(objs.length > 0){
				Integer[] tag_ids = Arrays.copyOfRange(objs,0,objs.length,Integer[].class);
				this.setTag_ids(Arrays.toString(tag_ids));
			}
		}
		if(!(map.get("total") instanceof Boolean)&& map.get("total")!=null){
			this.setTotal((Integer)map.get("total"));
		}
		if(!(map.get("total_mailings") instanceof Boolean)&& map.get("total_mailings")!=null){
			this.setTotal_mailings((Integer)map.get("total_mailings"));
		}
		if(map.get("unique_ab_testing") instanceof Boolean){
			this.setUnique_ab_testing(((Boolean)map.get("unique_ab_testing"))? "true" : "false");
		}
		if(!(map.get("user_id") instanceof Boolean)&& map.get("user_id")!=null){
			Object[] objs = (Object[])map.get("user_id");
			if(objs.length > 0){
				this.setUser_id((Integer)objs[0]);
			}
		}
		if(!(map.get("user_id") instanceof Boolean)&& map.get("user_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("user_id");
			if(objs.length > 1){
				this.setUser_id_text((String)objs[1]);
			}
		}
		if(!(map.get("write_date") instanceof Boolean)&& map.get("write_date")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse((String)map.get("write_date"));
   			this.setWrite_date(new Timestamp(parse.getTime()));
		}
		if(!(map.get("write_uid") instanceof Boolean)&& map.get("write_uid")!=null){
			Object[] objs = (Object[])map.get("write_uid");
			if(objs.length > 0){
				this.setWrite_uid((Integer)objs[0]);
			}
		}
		if(!(map.get("write_uid") instanceof Boolean)&& map.get("write_uid")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("write_uid");
			if(objs.length > 1){
				this.setWrite_uid_text((String)objs[1]);
			}
		}
		if(!(map.get("__last_update") instanceof Boolean)&& map.get("__last_update")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse((String)map.get("__last_update"));
   			this.set__last_update(new Timestamp(parse.getTime()));
		}
	}

	public Map<String, Object> toMap() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		if(this.getBounced()!=null&&this.getBouncedDirtyFlag()){
			map.put("bounced",this.getBounced());
		}else if(this.getBouncedDirtyFlag()){
			map.put("bounced",false);
		}
		if(this.getBounced_ratio()!=null&&this.getBounced_ratioDirtyFlag()){
			map.put("bounced_ratio",this.getBounced_ratio());
		}else if(this.getBounced_ratioDirtyFlag()){
			map.put("bounced_ratio",false);
		}
		if(this.getCampaign_id()!=null&&this.getCampaign_idDirtyFlag()){
			map.put("campaign_id",this.getCampaign_id());
		}else if(this.getCampaign_idDirtyFlag()){
			map.put("campaign_id",false);
		}
		if(this.getClicks_ratio()!=null&&this.getClicks_ratioDirtyFlag()){
			map.put("clicks_ratio",this.getClicks_ratio());
		}else if(this.getClicks_ratioDirtyFlag()){
			map.put("clicks_ratio",false);
		}
		if(this.getColor()!=null&&this.getColorDirtyFlag()){
			map.put("color",this.getColor());
		}else if(this.getColorDirtyFlag()){
			map.put("color",false);
		}
		if(this.getCreate_date()!=null&&this.getCreate_dateDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String datetimeStr = sdf.format(this.getCreate_date());
			map.put("create_date",datetimeStr);
		}else if(this.getCreate_dateDirtyFlag()){
			map.put("create_date",false);
		}
		if(this.getCreate_uid()!=null&&this.getCreate_uidDirtyFlag()){
			map.put("create_uid",this.getCreate_uid());
		}else if(this.getCreate_uidDirtyFlag()){
			map.put("create_uid",false);
		}
		if(this.getCreate_uid_text()!=null&&this.getCreate_uid_textDirtyFlag()){
			//忽略文本外键create_uid_text
		}else if(this.getCreate_uid_textDirtyFlag()){
			map.put("create_uid",false);
		}
		if(this.getDelivered()!=null&&this.getDeliveredDirtyFlag()){
			map.put("delivered",this.getDelivered());
		}else if(this.getDeliveredDirtyFlag()){
			map.put("delivered",false);
		}
		if(this.getDisplay_name()!=null&&this.getDisplay_nameDirtyFlag()){
			map.put("display_name",this.getDisplay_name());
		}else if(this.getDisplay_nameDirtyFlag()){
			map.put("display_name",false);
		}
		if(this.getFailed()!=null&&this.getFailedDirtyFlag()){
			map.put("failed",this.getFailed());
		}else if(this.getFailedDirtyFlag()){
			map.put("failed",false);
		}
		if(this.getId()!=null&&this.getIdDirtyFlag()){
			map.put("id",this.getId());
		}else if(this.getIdDirtyFlag()){
			map.put("id",false);
		}
		if(this.getIgnored()!=null&&this.getIgnoredDirtyFlag()){
			map.put("ignored",this.getIgnored());
		}else if(this.getIgnoredDirtyFlag()){
			map.put("ignored",false);
		}
		if(this.getMass_mailing_ids()!=null&&this.getMass_mailing_idsDirtyFlag()){
			map.put("mass_mailing_ids",this.getMass_mailing_ids());
		}else if(this.getMass_mailing_idsDirtyFlag()){
			map.put("mass_mailing_ids",false);
		}
		if(this.getMedium_id()!=null&&this.getMedium_idDirtyFlag()){
			map.put("medium_id",this.getMedium_id());
		}else if(this.getMedium_idDirtyFlag()){
			map.put("medium_id",false);
		}
		if(this.getMedium_id_text()!=null&&this.getMedium_id_textDirtyFlag()){
			//忽略文本外键medium_id_text
		}else if(this.getMedium_id_textDirtyFlag()){
			map.put("medium_id",false);
		}
		if(this.getName()!=null&&this.getNameDirtyFlag()){
			map.put("name",this.getName());
		}else if(this.getNameDirtyFlag()){
			map.put("name",false);
		}
		if(this.getOpened()!=null&&this.getOpenedDirtyFlag()){
			map.put("opened",this.getOpened());
		}else if(this.getOpenedDirtyFlag()){
			map.put("opened",false);
		}
		if(this.getOpened_ratio()!=null&&this.getOpened_ratioDirtyFlag()){
			map.put("opened_ratio",this.getOpened_ratio());
		}else if(this.getOpened_ratioDirtyFlag()){
			map.put("opened_ratio",false);
		}
		if(this.getReceived_ratio()!=null&&this.getReceived_ratioDirtyFlag()){
			map.put("received_ratio",this.getReceived_ratio());
		}else if(this.getReceived_ratioDirtyFlag()){
			map.put("received_ratio",false);
		}
		if(this.getReplied()!=null&&this.getRepliedDirtyFlag()){
			map.put("replied",this.getReplied());
		}else if(this.getRepliedDirtyFlag()){
			map.put("replied",false);
		}
		if(this.getReplied_ratio()!=null&&this.getReplied_ratioDirtyFlag()){
			map.put("replied_ratio",this.getReplied_ratio());
		}else if(this.getReplied_ratioDirtyFlag()){
			map.put("replied_ratio",false);
		}
		if(this.getScheduled()!=null&&this.getScheduledDirtyFlag()){
			map.put("scheduled",this.getScheduled());
		}else if(this.getScheduledDirtyFlag()){
			map.put("scheduled",false);
		}
		if(this.getSent()!=null&&this.getSentDirtyFlag()){
			map.put("sent",this.getSent());
		}else if(this.getSentDirtyFlag()){
			map.put("sent",false);
		}
		if(this.getSource_id()!=null&&this.getSource_idDirtyFlag()){
			map.put("source_id",this.getSource_id());
		}else if(this.getSource_idDirtyFlag()){
			map.put("source_id",false);
		}
		if(this.getSource_id_text()!=null&&this.getSource_id_textDirtyFlag()){
			//忽略文本外键source_id_text
		}else if(this.getSource_id_textDirtyFlag()){
			map.put("source_id",false);
		}
		if(this.getStage_id()!=null&&this.getStage_idDirtyFlag()){
			map.put("stage_id",this.getStage_id());
		}else if(this.getStage_idDirtyFlag()){
			map.put("stage_id",false);
		}
		if(this.getStage_id_text()!=null&&this.getStage_id_textDirtyFlag()){
			//忽略文本外键stage_id_text
		}else if(this.getStage_id_textDirtyFlag()){
			map.put("stage_id",false);
		}
		if(this.getTag_ids()!=null&&this.getTag_idsDirtyFlag()){
			map.put("tag_ids",this.getTag_ids());
		}else if(this.getTag_idsDirtyFlag()){
			map.put("tag_ids",false);
		}
		if(this.getTotal()!=null&&this.getTotalDirtyFlag()){
			map.put("total",this.getTotal());
		}else if(this.getTotalDirtyFlag()){
			map.put("total",false);
		}
		if(this.getTotal_mailings()!=null&&this.getTotal_mailingsDirtyFlag()){
			map.put("total_mailings",this.getTotal_mailings());
		}else if(this.getTotal_mailingsDirtyFlag()){
			map.put("total_mailings",false);
		}
		if(this.getUnique_ab_testing()!=null&&this.getUnique_ab_testingDirtyFlag()){
			map.put("unique_ab_testing",Boolean.parseBoolean(this.getUnique_ab_testing()));		
		}		if(this.getUser_id()!=null&&this.getUser_idDirtyFlag()){
			map.put("user_id",this.getUser_id());
		}else if(this.getUser_idDirtyFlag()){
			map.put("user_id",false);
		}
		if(this.getUser_id_text()!=null&&this.getUser_id_textDirtyFlag()){
			//忽略文本外键user_id_text
		}else if(this.getUser_id_textDirtyFlag()){
			map.put("user_id",false);
		}
		if(this.getWrite_date()!=null&&this.getWrite_dateDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String datetimeStr = sdf.format(this.getWrite_date());
			map.put("write_date",datetimeStr);
		}else if(this.getWrite_dateDirtyFlag()){
			map.put("write_date",false);
		}
		if(this.getWrite_uid()!=null&&this.getWrite_uidDirtyFlag()){
			map.put("write_uid",this.getWrite_uid());
		}else if(this.getWrite_uidDirtyFlag()){
			map.put("write_uid",false);
		}
		if(this.getWrite_uid_text()!=null&&this.getWrite_uid_textDirtyFlag()){
			//忽略文本外键write_uid_text
		}else if(this.getWrite_uid_textDirtyFlag()){
			map.put("write_uid",false);
		}
		if(this.get__last_update()!=null&&this.get__last_updateDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String datetimeStr = sdf.format(this.get__last_update());
			map.put("__last_update",datetimeStr);
		}else if(this.get__last_updateDirtyFlag()){
			map.put("__last_update",false);
		}
		return map;
	}

}
