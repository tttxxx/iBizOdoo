package cn.ibizlab.odoo.client.odoo_mail.odooclient;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.core.client.model.Imail_resend_message;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[mail_resend_message] 服务对象客户端接口
 */
public interface Imail_resend_messageOdooClient {
    
        public void updateBatch(Imail_resend_message mail_resend_message);

        public void update(Imail_resend_message mail_resend_message);

        public void remove(Imail_resend_message mail_resend_message);

        public void createBatch(Imail_resend_message mail_resend_message);

        public void get(Imail_resend_message mail_resend_message);

        public void removeBatch(Imail_resend_message mail_resend_message);

        public Page<Imail_resend_message> search(SearchContext context);

        public void create(Imail_resend_message mail_resend_message);

        public List<Imail_resend_message> select();


}