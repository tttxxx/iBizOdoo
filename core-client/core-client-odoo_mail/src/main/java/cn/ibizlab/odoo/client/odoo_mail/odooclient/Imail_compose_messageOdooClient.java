package cn.ibizlab.odoo.client.odoo_mail.odooclient;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.core.client.model.Imail_compose_message;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[mail_compose_message] 服务对象客户端接口
 */
public interface Imail_compose_messageOdooClient {
    
        public void remove(Imail_compose_message mail_compose_message);

        public Page<Imail_compose_message> search(SearchContext context);

        public void update(Imail_compose_message mail_compose_message);

        public void removeBatch(Imail_compose_message mail_compose_message);

        public void updateBatch(Imail_compose_message mail_compose_message);

        public void create(Imail_compose_message mail_compose_message);

        public void createBatch(Imail_compose_message mail_compose_message);

        public void get(Imail_compose_message mail_compose_message);

        public List<Imail_compose_message> select();


}