package cn.ibizlab.odoo.client.odoo_mail.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Imail_channel;
import cn.ibizlab.odoo.core.client.service.Imail_channelClientService;
import cn.ibizlab.odoo.client.odoo_mail.model.mail_channelImpl;
import cn.ibizlab.odoo.client.odoo_mail.odooclient.Imail_channelOdooClient;
import cn.ibizlab.odoo.client.odoo_mail.odooclient.impl.mail_channelOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[mail_channel] 服务对象接口
 */
@Service
public class mail_channelClientServiceImpl implements Imail_channelClientService {
    @Autowired
    private  Imail_channelOdooClient  mail_channelOdooClient;

    public Imail_channel createModel() {		
		return new mail_channelImpl();
	}


        public void updateBatch(List<Imail_channel> mail_channels){
            
        }
        
        public void create(Imail_channel mail_channel){
this.mail_channelOdooClient.create(mail_channel) ;
        }
        
        public Page<Imail_channel> search(SearchContext context){
            return this.mail_channelOdooClient.search(context) ;
        }
        
        public void get(Imail_channel mail_channel){
            this.mail_channelOdooClient.get(mail_channel) ;
        }
        
        public void removeBatch(List<Imail_channel> mail_channels){
            
        }
        
        public void createBatch(List<Imail_channel> mail_channels){
            
        }
        
        public void update(Imail_channel mail_channel){
this.mail_channelOdooClient.update(mail_channel) ;
        }
        
        public void remove(Imail_channel mail_channel){
this.mail_channelOdooClient.remove(mail_channel) ;
        }
        
        public Page<Imail_channel> select(SearchContext context){
            return null ;
        }
        

}

