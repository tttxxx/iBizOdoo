package cn.ibizlab.odoo.client.odoo_mail.odooclient;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.core.client.model.Imail_followers;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[mail_followers] 服务对象客户端接口
 */
public interface Imail_followersOdooClient {
    
        public void createBatch(Imail_followers mail_followers);

        public void update(Imail_followers mail_followers);

        public void get(Imail_followers mail_followers);

        public void updateBatch(Imail_followers mail_followers);

        public void removeBatch(Imail_followers mail_followers);

        public void remove(Imail_followers mail_followers);

        public Page<Imail_followers> search(SearchContext context);

        public void create(Imail_followers mail_followers);

        public List<Imail_followers> select();


}