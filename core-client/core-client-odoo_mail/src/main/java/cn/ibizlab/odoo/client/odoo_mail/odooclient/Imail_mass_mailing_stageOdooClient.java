package cn.ibizlab.odoo.client.odoo_mail.odooclient;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.core.client.model.Imail_mass_mailing_stage;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[mail_mass_mailing_stage] 服务对象客户端接口
 */
public interface Imail_mass_mailing_stageOdooClient {
    
        public void create(Imail_mass_mailing_stage mail_mass_mailing_stage);

        public void updateBatch(Imail_mass_mailing_stage mail_mass_mailing_stage);

        public void createBatch(Imail_mass_mailing_stage mail_mass_mailing_stage);

        public void removeBatch(Imail_mass_mailing_stage mail_mass_mailing_stage);

        public void update(Imail_mass_mailing_stage mail_mass_mailing_stage);

        public void remove(Imail_mass_mailing_stage mail_mass_mailing_stage);

        public void get(Imail_mass_mailing_stage mail_mass_mailing_stage);

        public Page<Imail_mass_mailing_stage> search(SearchContext context);

        public List<Imail_mass_mailing_stage> select();


}