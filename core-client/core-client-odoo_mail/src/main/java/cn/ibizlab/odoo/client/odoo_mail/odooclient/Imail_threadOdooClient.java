package cn.ibizlab.odoo.client.odoo_mail.odooclient;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.core.client.model.Imail_thread;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[mail_thread] 服务对象客户端接口
 */
public interface Imail_threadOdooClient {
    
        public void updateBatch(Imail_thread mail_thread);

        public Page<Imail_thread> search(SearchContext context);

        public void update(Imail_thread mail_thread);

        public void get(Imail_thread mail_thread);

        public void remove(Imail_thread mail_thread);

        public void createBatch(Imail_thread mail_thread);

        public void removeBatch(Imail_thread mail_thread);

        public void create(Imail_thread mail_thread);

        public List<Imail_thread> select();


}