package cn.ibizlab.odoo.client.odoo_mail.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Imail_bot;
import cn.ibizlab.odoo.core.client.service.Imail_botClientService;
import cn.ibizlab.odoo.client.odoo_mail.model.mail_botImpl;
import cn.ibizlab.odoo.client.odoo_mail.odooclient.Imail_botOdooClient;
import cn.ibizlab.odoo.client.odoo_mail.odooclient.impl.mail_botOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[mail_bot] 服务对象接口
 */
@Service
public class mail_botClientServiceImpl implements Imail_botClientService {
    @Autowired
    private  Imail_botOdooClient  mail_botOdooClient;

    public Imail_bot createModel() {		
		return new mail_botImpl();
	}


        public void update(Imail_bot mail_bot){
this.mail_botOdooClient.update(mail_bot) ;
        }
        
        public void create(Imail_bot mail_bot){
this.mail_botOdooClient.create(mail_bot) ;
        }
        
        public void createBatch(List<Imail_bot> mail_bots){
            
        }
        
        public void get(Imail_bot mail_bot){
            this.mail_botOdooClient.get(mail_bot) ;
        }
        
        public void removeBatch(List<Imail_bot> mail_bots){
            
        }
        
        public void remove(Imail_bot mail_bot){
this.mail_botOdooClient.remove(mail_bot) ;
        }
        
        public void updateBatch(List<Imail_bot> mail_bots){
            
        }
        
        public Page<Imail_bot> search(SearchContext context){
            return this.mail_botOdooClient.search(context) ;
        }
        
        public Page<Imail_bot> select(SearchContext context){
            return null ;
        }
        

}

