package cn.ibizlab.odoo.client.odoo_mail.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Imail_message_subtype;
import cn.ibizlab.odoo.core.client.service.Imail_message_subtypeClientService;
import cn.ibizlab.odoo.client.odoo_mail.model.mail_message_subtypeImpl;
import cn.ibizlab.odoo.client.odoo_mail.odooclient.Imail_message_subtypeOdooClient;
import cn.ibizlab.odoo.client.odoo_mail.odooclient.impl.mail_message_subtypeOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[mail_message_subtype] 服务对象接口
 */
@Service
public class mail_message_subtypeClientServiceImpl implements Imail_message_subtypeClientService {
    @Autowired
    private  Imail_message_subtypeOdooClient  mail_message_subtypeOdooClient;

    public Imail_message_subtype createModel() {		
		return new mail_message_subtypeImpl();
	}


        public Page<Imail_message_subtype> search(SearchContext context){
            return this.mail_message_subtypeOdooClient.search(context) ;
        }
        
        public void updateBatch(List<Imail_message_subtype> mail_message_subtypes){
            
        }
        
        public void get(Imail_message_subtype mail_message_subtype){
            this.mail_message_subtypeOdooClient.get(mail_message_subtype) ;
        }
        
        public void update(Imail_message_subtype mail_message_subtype){
this.mail_message_subtypeOdooClient.update(mail_message_subtype) ;
        }
        
        public void remove(Imail_message_subtype mail_message_subtype){
this.mail_message_subtypeOdooClient.remove(mail_message_subtype) ;
        }
        
        public void removeBatch(List<Imail_message_subtype> mail_message_subtypes){
            
        }
        
        public void create(Imail_message_subtype mail_message_subtype){
this.mail_message_subtypeOdooClient.create(mail_message_subtype) ;
        }
        
        public void createBatch(List<Imail_message_subtype> mail_message_subtypes){
            
        }
        
        public Page<Imail_message_subtype> select(SearchContext context){
            return null ;
        }
        

}

