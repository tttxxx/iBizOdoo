package cn.ibizlab.odoo.client.odoo_mail.odooclient;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.core.client.model.Imail_activity;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[mail_activity] 服务对象客户端接口
 */
public interface Imail_activityOdooClient {
    
        public Page<Imail_activity> search(SearchContext context);

        public void removeBatch(Imail_activity mail_activity);

        public void get(Imail_activity mail_activity);

        public void remove(Imail_activity mail_activity);

        public void createBatch(Imail_activity mail_activity);

        public void update(Imail_activity mail_activity);

        public void create(Imail_activity mail_activity);

        public void updateBatch(Imail_activity mail_activity);

        public List<Imail_activity> select();


}