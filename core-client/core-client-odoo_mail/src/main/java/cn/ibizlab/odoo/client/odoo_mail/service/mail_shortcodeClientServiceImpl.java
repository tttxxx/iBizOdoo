package cn.ibizlab.odoo.client.odoo_mail.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Imail_shortcode;
import cn.ibizlab.odoo.core.client.service.Imail_shortcodeClientService;
import cn.ibizlab.odoo.client.odoo_mail.model.mail_shortcodeImpl;
import cn.ibizlab.odoo.client.odoo_mail.odooclient.Imail_shortcodeOdooClient;
import cn.ibizlab.odoo.client.odoo_mail.odooclient.impl.mail_shortcodeOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[mail_shortcode] 服务对象接口
 */
@Service
public class mail_shortcodeClientServiceImpl implements Imail_shortcodeClientService {
    @Autowired
    private  Imail_shortcodeOdooClient  mail_shortcodeOdooClient;

    public Imail_shortcode createModel() {		
		return new mail_shortcodeImpl();
	}


        public void updateBatch(List<Imail_shortcode> mail_shortcodes){
            
        }
        
        public void get(Imail_shortcode mail_shortcode){
            this.mail_shortcodeOdooClient.get(mail_shortcode) ;
        }
        
        public void createBatch(List<Imail_shortcode> mail_shortcodes){
            
        }
        
        public void create(Imail_shortcode mail_shortcode){
this.mail_shortcodeOdooClient.create(mail_shortcode) ;
        }
        
        public void removeBatch(List<Imail_shortcode> mail_shortcodes){
            
        }
        
        public void update(Imail_shortcode mail_shortcode){
this.mail_shortcodeOdooClient.update(mail_shortcode) ;
        }
        
        public void remove(Imail_shortcode mail_shortcode){
this.mail_shortcodeOdooClient.remove(mail_shortcode) ;
        }
        
        public Page<Imail_shortcode> search(SearchContext context){
            return this.mail_shortcodeOdooClient.search(context) ;
        }
        
        public Page<Imail_shortcode> select(SearchContext context){
            return null ;
        }
        

}

