package cn.ibizlab.odoo.client.odoo_mail.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Imail_mail;
import cn.ibizlab.odoo.client.odoo_mail.model.mail_mailImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[mail_mail] 服务对象接口
 */
public interface mail_mailFeignClient {

    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_mail/mail_mails/removebatch")
    public mail_mailImpl removeBatch(@RequestBody List<mail_mailImpl> mail_mails);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_mail/mail_mails/updatebatch")
    public mail_mailImpl updateBatch(@RequestBody List<mail_mailImpl> mail_mails);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_mail/mail_mails/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_mail/mail_mails/{id}")
    public mail_mailImpl update(@PathVariable("id") Integer id,@RequestBody mail_mailImpl mail_mail);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_mail/mail_mails/createbatch")
    public mail_mailImpl createBatch(@RequestBody List<mail_mailImpl> mail_mails);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_mail/mail_mails/{id}")
    public mail_mailImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_mail/mail_mails")
    public mail_mailImpl create(@RequestBody mail_mailImpl mail_mail);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_mail/mail_mails/search")
    public Page<mail_mailImpl> search(SearchContext context);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_mail/mail_mails/select")
    public Page<mail_mailImpl> select();



}
