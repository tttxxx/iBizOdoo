package cn.ibizlab.odoo.client.odoo_mail.odooclient;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.core.client.model.Imail_shortcode;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[mail_shortcode] 服务对象客户端接口
 */
public interface Imail_shortcodeOdooClient {
    
        public void updateBatch(Imail_shortcode mail_shortcode);

        public void get(Imail_shortcode mail_shortcode);

        public void createBatch(Imail_shortcode mail_shortcode);

        public void create(Imail_shortcode mail_shortcode);

        public void removeBatch(Imail_shortcode mail_shortcode);

        public void update(Imail_shortcode mail_shortcode);

        public void remove(Imail_shortcode mail_shortcode);

        public Page<Imail_shortcode> search(SearchContext context);

        public List<Imail_shortcode> select();


}