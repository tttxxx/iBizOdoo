package cn.ibizlab.odoo.client.odoo_mail.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Imail_followers;
import cn.ibizlab.odoo.core.client.service.Imail_followersClientService;
import cn.ibizlab.odoo.client.odoo_mail.model.mail_followersImpl;
import cn.ibizlab.odoo.client.odoo_mail.odooclient.Imail_followersOdooClient;
import cn.ibizlab.odoo.client.odoo_mail.odooclient.impl.mail_followersOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[mail_followers] 服务对象接口
 */
@Service
public class mail_followersClientServiceImpl implements Imail_followersClientService {
    @Autowired
    private  Imail_followersOdooClient  mail_followersOdooClient;

    public Imail_followers createModel() {		
		return new mail_followersImpl();
	}


        public void createBatch(List<Imail_followers> mail_followers){
            
        }
        
        public void update(Imail_followers mail_followers){
this.mail_followersOdooClient.update(mail_followers) ;
        }
        
        public void get(Imail_followers mail_followers){
            this.mail_followersOdooClient.get(mail_followers) ;
        }
        
        public void updateBatch(List<Imail_followers> mail_followers){
            
        }
        
        public void removeBatch(List<Imail_followers> mail_followers){
            
        }
        
        public void remove(Imail_followers mail_followers){
this.mail_followersOdooClient.remove(mail_followers) ;
        }
        
        public Page<Imail_followers> search(SearchContext context){
            return this.mail_followersOdooClient.search(context) ;
        }
        
        public void create(Imail_followers mail_followers){
this.mail_followersOdooClient.create(mail_followers) ;
        }
        
        public Page<Imail_followers> select(SearchContext context){
            return null ;
        }
        

}

