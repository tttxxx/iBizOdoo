package cn.ibizlab.odoo.client.odoo_mail.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Imail_mass_mailing_list;
import cn.ibizlab.odoo.client.odoo_mail.model.mail_mass_mailing_listImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[mail_mass_mailing_list] 服务对象接口
 */
public interface mail_mass_mailing_listFeignClient {

    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_mail/mail_mass_mailing_lists/{id}")
    public mail_mass_mailing_listImpl update(@PathVariable("id") Integer id,@RequestBody mail_mass_mailing_listImpl mail_mass_mailing_list);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_mail/mail_mass_mailing_lists/search")
    public Page<mail_mass_mailing_listImpl> search(SearchContext context);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_mail/mail_mass_mailing_lists")
    public mail_mass_mailing_listImpl create(@RequestBody mail_mass_mailing_listImpl mail_mass_mailing_list);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_mail/mail_mass_mailing_lists/createbatch")
    public mail_mass_mailing_listImpl createBatch(@RequestBody List<mail_mass_mailing_listImpl> mail_mass_mailing_lists);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_mail/mail_mass_mailing_lists/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_mail/mail_mass_mailing_lists/updatebatch")
    public mail_mass_mailing_listImpl updateBatch(@RequestBody List<mail_mass_mailing_listImpl> mail_mass_mailing_lists);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_mail/mail_mass_mailing_lists/removebatch")
    public mail_mass_mailing_listImpl removeBatch(@RequestBody List<mail_mass_mailing_listImpl> mail_mass_mailing_lists);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_mail/mail_mass_mailing_lists/{id}")
    public mail_mass_mailing_listImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_mail/mail_mass_mailing_lists/select")
    public Page<mail_mass_mailing_listImpl> select();



}
