package cn.ibizlab.odoo.client.odoo_mail.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Imail_mass_mailing_test;
import cn.ibizlab.odoo.core.client.service.Imail_mass_mailing_testClientService;
import cn.ibizlab.odoo.client.odoo_mail.model.mail_mass_mailing_testImpl;
import cn.ibizlab.odoo.client.odoo_mail.odooclient.Imail_mass_mailing_testOdooClient;
import cn.ibizlab.odoo.client.odoo_mail.odooclient.impl.mail_mass_mailing_testOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[mail_mass_mailing_test] 服务对象接口
 */
@Service
public class mail_mass_mailing_testClientServiceImpl implements Imail_mass_mailing_testClientService {
    @Autowired
    private  Imail_mass_mailing_testOdooClient  mail_mass_mailing_testOdooClient;

    public Imail_mass_mailing_test createModel() {		
		return new mail_mass_mailing_testImpl();
	}


        public void update(Imail_mass_mailing_test mail_mass_mailing_test){
this.mail_mass_mailing_testOdooClient.update(mail_mass_mailing_test) ;
        }
        
        public void create(Imail_mass_mailing_test mail_mass_mailing_test){
this.mail_mass_mailing_testOdooClient.create(mail_mass_mailing_test) ;
        }
        
        public Page<Imail_mass_mailing_test> search(SearchContext context){
            return this.mail_mass_mailing_testOdooClient.search(context) ;
        }
        
        public void createBatch(List<Imail_mass_mailing_test> mail_mass_mailing_tests){
            
        }
        
        public void removeBatch(List<Imail_mass_mailing_test> mail_mass_mailing_tests){
            
        }
        
        public void updateBatch(List<Imail_mass_mailing_test> mail_mass_mailing_tests){
            
        }
        
        public void get(Imail_mass_mailing_test mail_mass_mailing_test){
            this.mail_mass_mailing_testOdooClient.get(mail_mass_mailing_test) ;
        }
        
        public void remove(Imail_mass_mailing_test mail_mass_mailing_test){
this.mail_mass_mailing_testOdooClient.remove(mail_mass_mailing_test) ;
        }
        
        public Page<Imail_mass_mailing_test> select(SearchContext context){
            return null ;
        }
        

}

