package cn.ibizlab.odoo.client.odoo_mail.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Imail_alias;
import cn.ibizlab.odoo.client.odoo_mail.model.mail_aliasImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[mail_alias] 服务对象接口
 */
public interface mail_aliasFeignClient {

    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_mail/mail_aliases/removebatch")
    public mail_aliasImpl removeBatch(@RequestBody List<mail_aliasImpl> mail_aliases);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_mail/mail_aliases/{id}")
    public mail_aliasImpl update(@PathVariable("id") Integer id,@RequestBody mail_aliasImpl mail_alias);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_mail/mail_aliases/updatebatch")
    public mail_aliasImpl updateBatch(@RequestBody List<mail_aliasImpl> mail_aliases);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_mail/mail_aliases/{id}")
    public mail_aliasImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_mail/mail_aliases/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_mail/mail_aliases")
    public mail_aliasImpl create(@RequestBody mail_aliasImpl mail_alias);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_mail/mail_aliases/createbatch")
    public mail_aliasImpl createBatch(@RequestBody List<mail_aliasImpl> mail_aliases);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_mail/mail_aliases/search")
    public Page<mail_aliasImpl> search(SearchContext context);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_mail/mail_aliases/select")
    public Page<mail_aliasImpl> select();



}
