package cn.ibizlab.odoo.client.odoo_snailmail.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Isnailmail_letter;
import cn.ibizlab.odoo.client.odoo_snailmail.model.snailmail_letterImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[snailmail_letter] 服务对象接口
 */
public interface snailmail_letterFeignClient {

    @RequestMapping(method = RequestMethod.POST, value = "/odoo_snailmail/snailmail_letters")
    public snailmail_letterImpl create(@RequestBody snailmail_letterImpl snailmail_letter);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_snailmail/snailmail_letters/{id}")
    public snailmail_letterImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_snailmail/snailmail_letters/{id}")
    public snailmail_letterImpl update(@PathVariable("id") Integer id,@RequestBody snailmail_letterImpl snailmail_letter);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_snailmail/snailmail_letters/search")
    public Page<snailmail_letterImpl> search(SearchContext context);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_snailmail/snailmail_letters/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_snailmail/snailmail_letters/removebatch")
    public snailmail_letterImpl removeBatch(@RequestBody List<snailmail_letterImpl> snailmail_letters);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_snailmail/snailmail_letters/createbatch")
    public snailmail_letterImpl createBatch(@RequestBody List<snailmail_letterImpl> snailmail_letters);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_snailmail/snailmail_letters/updatebatch")
    public snailmail_letterImpl updateBatch(@RequestBody List<snailmail_letterImpl> snailmail_letters);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_snailmail/snailmail_letters/select")
    public Page<snailmail_letterImpl> select();



}
