package cn.ibizlab.odoo.client.odoo_snailmail.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Isnailmail_letter;
import cn.ibizlab.odoo.core.client.service.Isnailmail_letterClientService;
import cn.ibizlab.odoo.client.odoo_snailmail.model.snailmail_letterImpl;
import cn.ibizlab.odoo.client.odoo_snailmail.odooclient.Isnailmail_letterOdooClient;
import cn.ibizlab.odoo.client.odoo_snailmail.odooclient.impl.snailmail_letterOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[snailmail_letter] 服务对象接口
 */
@Service
public class snailmail_letterClientServiceImpl implements Isnailmail_letterClientService {
    @Autowired
    private  Isnailmail_letterOdooClient  snailmail_letterOdooClient;

    public Isnailmail_letter createModel() {		
		return new snailmail_letterImpl();
	}


        public void create(Isnailmail_letter snailmail_letter){
this.snailmail_letterOdooClient.create(snailmail_letter) ;
        }
        
        public void get(Isnailmail_letter snailmail_letter){
            this.snailmail_letterOdooClient.get(snailmail_letter) ;
        }
        
        public void update(Isnailmail_letter snailmail_letter){
this.snailmail_letterOdooClient.update(snailmail_letter) ;
        }
        
        public Page<Isnailmail_letter> search(SearchContext context){
            return this.snailmail_letterOdooClient.search(context) ;
        }
        
        public void remove(Isnailmail_letter snailmail_letter){
this.snailmail_letterOdooClient.remove(snailmail_letter) ;
        }
        
        public void removeBatch(List<Isnailmail_letter> snailmail_letters){
            
        }
        
        public void createBatch(List<Isnailmail_letter> snailmail_letters){
            
        }
        
        public void updateBatch(List<Isnailmail_letter> snailmail_letters){
            
        }
        
        public Page<Isnailmail_letter> select(SearchContext context){
            return null ;
        }
        

}

