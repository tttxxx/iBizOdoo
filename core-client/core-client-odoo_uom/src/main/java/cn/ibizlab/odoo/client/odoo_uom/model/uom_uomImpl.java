package cn.ibizlab.odoo.client.odoo_uom.model;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.math.BigInteger;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.core.client.model.Iuom_uom;
import cn.ibizlab.odoo.util.helper.OdooClientHelper;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang.ArrayUtils;
import org.springframework.util.StringUtils;

/**
 * 接口实体[uom_uom] 对象
 */
public class uom_uomImpl implements Iuom_uom,Serializable{

    /**
     * 有效
     */
    public String active;

    @JsonIgnore
    public boolean activeDirtyFlag;
    
    /**
     * 类别
     */
    public Integer category_id;

    @JsonIgnore
    public boolean category_idDirtyFlag;
    
    /**
     * 类别
     */
    public String category_id_text;

    @JsonIgnore
    public boolean category_id_textDirtyFlag;
    
    /**
     * 创建时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp create_date;

    @JsonIgnore
    public boolean create_dateDirtyFlag;
    
    /**
     * 创建人
     */
    public Integer create_uid;

    @JsonIgnore
    public boolean create_uidDirtyFlag;
    
    /**
     * 创建人
     */
    public String create_uid_text;

    @JsonIgnore
    public boolean create_uid_textDirtyFlag;
    
    /**
     * 显示名称
     */
    public String display_name;

    @JsonIgnore
    public boolean display_nameDirtyFlag;
    
    /**
     * 比例
     */
    public Double factor;

    @JsonIgnore
    public boolean factorDirtyFlag;
    
    /**
     * 更大比率
     */
    public Double factor_inv;

    @JsonIgnore
    public boolean factor_invDirtyFlag;
    
    /**
     * ID
     */
    public Integer id;

    @JsonIgnore
    public boolean idDirtyFlag;
    
    /**
     * 分组销售点中的产品
     */
    public String is_pos_groupable;

    @JsonIgnore
    public boolean is_pos_groupableDirtyFlag;
    
    /**
     * 计量单位的类别
     */
    public String measure_type;

    @JsonIgnore
    public boolean measure_typeDirtyFlag;
    
    /**
     * 单位
     */
    public String name;

    @JsonIgnore
    public boolean nameDirtyFlag;
    
    /**
     * 舍入精度
     */
    public Double rounding;

    @JsonIgnore
    public boolean roundingDirtyFlag;
    
    /**
     * 类型
     */
    public String uom_type;

    @JsonIgnore
    public boolean uom_typeDirtyFlag;
    
    /**
     * 最后更新时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp write_date;

    @JsonIgnore
    public boolean write_dateDirtyFlag;
    
    /**
     * 最后更新者
     */
    public Integer write_uid;

    @JsonIgnore
    public boolean write_uidDirtyFlag;
    
    /**
     * 最后更新者
     */
    public String write_uid_text;

    @JsonIgnore
    public boolean write_uid_textDirtyFlag;
    
    /**
     * 最后修改日
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp __last_update;

    @JsonIgnore
    public boolean __last_updateDirtyFlag;
    
    /**
     * 获取 [有效]
     */
    @JsonProperty("active")
    public String getActive(){
        return this.active ;
    }

    /**
     * 设置 [有效]
     */
    @JsonProperty("active")
    public void setActive(String  active){
        this.active = active ;
        this.activeDirtyFlag = true ;
    }

     /**
     * 获取 [有效]脏标记
     */
    @JsonIgnore
    public boolean getActiveDirtyFlag(){
        return this.activeDirtyFlag ;
    }   

    /**
     * 获取 [类别]
     */
    @JsonProperty("category_id")
    public Integer getCategory_id(){
        return this.category_id ;
    }

    /**
     * 设置 [类别]
     */
    @JsonProperty("category_id")
    public void setCategory_id(Integer  category_id){
        this.category_id = category_id ;
        this.category_idDirtyFlag = true ;
    }

     /**
     * 获取 [类别]脏标记
     */
    @JsonIgnore
    public boolean getCategory_idDirtyFlag(){
        return this.category_idDirtyFlag ;
    }   

    /**
     * 获取 [类别]
     */
    @JsonProperty("category_id_text")
    public String getCategory_id_text(){
        return this.category_id_text ;
    }

    /**
     * 设置 [类别]
     */
    @JsonProperty("category_id_text")
    public void setCategory_id_text(String  category_id_text){
        this.category_id_text = category_id_text ;
        this.category_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [类别]脏标记
     */
    @JsonIgnore
    public boolean getCategory_id_textDirtyFlag(){
        return this.category_id_textDirtyFlag ;
    }   

    /**
     * 获取 [创建时间]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return this.create_date ;
    }

    /**
     * 设置 [创建时间]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

     /**
     * 获取 [创建时间]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return this.create_dateDirtyFlag ;
    }   

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return this.create_uid ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

     /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return this.create_uidDirtyFlag ;
    }   

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return this.create_uid_text ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

     /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return this.create_uid_textDirtyFlag ;
    }   

    /**
     * 获取 [显示名称]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return this.display_name ;
    }

    /**
     * 设置 [显示名称]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

     /**
     * 获取 [显示名称]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return this.display_nameDirtyFlag ;
    }   

    /**
     * 获取 [比例]
     */
    @JsonProperty("factor")
    public Double getFactor(){
        return this.factor ;
    }

    /**
     * 设置 [比例]
     */
    @JsonProperty("factor")
    public void setFactor(Double  factor){
        this.factor = factor ;
        this.factorDirtyFlag = true ;
    }

     /**
     * 获取 [比例]脏标记
     */
    @JsonIgnore
    public boolean getFactorDirtyFlag(){
        return this.factorDirtyFlag ;
    }   

    /**
     * 获取 [更大比率]
     */
    @JsonProperty("factor_inv")
    public Double getFactor_inv(){
        return this.factor_inv ;
    }

    /**
     * 设置 [更大比率]
     */
    @JsonProperty("factor_inv")
    public void setFactor_inv(Double  factor_inv){
        this.factor_inv = factor_inv ;
        this.factor_invDirtyFlag = true ;
    }

     /**
     * 获取 [更大比率]脏标记
     */
    @JsonIgnore
    public boolean getFactor_invDirtyFlag(){
        return this.factor_invDirtyFlag ;
    }   

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return this.id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

     /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return this.idDirtyFlag ;
    }   

    /**
     * 获取 [分组销售点中的产品]
     */
    @JsonProperty("is_pos_groupable")
    public String getIs_pos_groupable(){
        return this.is_pos_groupable ;
    }

    /**
     * 设置 [分组销售点中的产品]
     */
    @JsonProperty("is_pos_groupable")
    public void setIs_pos_groupable(String  is_pos_groupable){
        this.is_pos_groupable = is_pos_groupable ;
        this.is_pos_groupableDirtyFlag = true ;
    }

     /**
     * 获取 [分组销售点中的产品]脏标记
     */
    @JsonIgnore
    public boolean getIs_pos_groupableDirtyFlag(){
        return this.is_pos_groupableDirtyFlag ;
    }   

    /**
     * 获取 [计量单位的类别]
     */
    @JsonProperty("measure_type")
    public String getMeasure_type(){
        return this.measure_type ;
    }

    /**
     * 设置 [计量单位的类别]
     */
    @JsonProperty("measure_type")
    public void setMeasure_type(String  measure_type){
        this.measure_type = measure_type ;
        this.measure_typeDirtyFlag = true ;
    }

     /**
     * 获取 [计量单位的类别]脏标记
     */
    @JsonIgnore
    public boolean getMeasure_typeDirtyFlag(){
        return this.measure_typeDirtyFlag ;
    }   

    /**
     * 获取 [单位]
     */
    @JsonProperty("name")
    public String getName(){
        return this.name ;
    }

    /**
     * 设置 [单位]
     */
    @JsonProperty("name")
    public void setName(String  name){
        this.name = name ;
        this.nameDirtyFlag = true ;
    }

     /**
     * 获取 [单位]脏标记
     */
    @JsonIgnore
    public boolean getNameDirtyFlag(){
        return this.nameDirtyFlag ;
    }   

    /**
     * 获取 [舍入精度]
     */
    @JsonProperty("rounding")
    public Double getRounding(){
        return this.rounding ;
    }

    /**
     * 设置 [舍入精度]
     */
    @JsonProperty("rounding")
    public void setRounding(Double  rounding){
        this.rounding = rounding ;
        this.roundingDirtyFlag = true ;
    }

     /**
     * 获取 [舍入精度]脏标记
     */
    @JsonIgnore
    public boolean getRoundingDirtyFlag(){
        return this.roundingDirtyFlag ;
    }   

    /**
     * 获取 [类型]
     */
    @JsonProperty("uom_type")
    public String getUom_type(){
        return this.uom_type ;
    }

    /**
     * 设置 [类型]
     */
    @JsonProperty("uom_type")
    public void setUom_type(String  uom_type){
        this.uom_type = uom_type ;
        this.uom_typeDirtyFlag = true ;
    }

     /**
     * 获取 [类型]脏标记
     */
    @JsonIgnore
    public boolean getUom_typeDirtyFlag(){
        return this.uom_typeDirtyFlag ;
    }   

    /**
     * 获取 [最后更新时间]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return this.write_date ;
    }

    /**
     * 设置 [最后更新时间]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新时间]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return this.write_dateDirtyFlag ;
    }   

    /**
     * 获取 [最后更新者]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return this.write_uid ;
    }

    /**
     * 设置 [最后更新者]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新者]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return this.write_uidDirtyFlag ;
    }   

    /**
     * 获取 [最后更新者]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return this.write_uid_text ;
    }

    /**
     * 设置 [最后更新者]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新者]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return this.write_uid_textDirtyFlag ;
    }   

    /**
     * 获取 [最后修改日]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return this.__last_update ;
    }

    /**
     * 设置 [最后修改日]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

     /**
     * 获取 [最后修改日]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return this.__last_updateDirtyFlag ;
    }   



    public void fromMap(Map<String, Object> map) throws Exception {
		if(map.get("active") instanceof Boolean){
			this.setActive(((Boolean)map.get("active"))? "true" : "false");
		}
		if(!(map.get("category_id") instanceof Boolean)&& map.get("category_id")!=null){
			Object[] objs = (Object[])map.get("category_id");
			if(objs.length > 0){
				this.setCategory_id((Integer)objs[0]);
			}
		}
		if(!(map.get("category_id") instanceof Boolean)&& map.get("category_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("category_id");
			if(objs.length > 1){
				this.setCategory_id_text((String)objs[1]);
			}
		}
		if(!(map.get("create_date") instanceof Boolean)&& map.get("create_date")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse((String)map.get("create_date"));
   			this.setCreate_date(new Timestamp(parse.getTime()));
		}
		if(!(map.get("create_uid") instanceof Boolean)&& map.get("create_uid")!=null){
			Object[] objs = (Object[])map.get("create_uid");
			if(objs.length > 0){
				this.setCreate_uid((Integer)objs[0]);
			}
		}
		if(!(map.get("create_uid") instanceof Boolean)&& map.get("create_uid")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("create_uid");
			if(objs.length > 1){
				this.setCreate_uid_text((String)objs[1]);
			}
		}
		if(!(map.get("display_name") instanceof Boolean)&& map.get("display_name")!=null){
			this.setDisplay_name((String)map.get("display_name"));
		}
		if(!(map.get("factor") instanceof Boolean)&& map.get("factor")!=null){
			this.setFactor((Double)map.get("factor"));
		}
		if(!(map.get("factor_inv") instanceof Boolean)&& map.get("factor_inv")!=null){
			this.setFactor_inv((Double)map.get("factor_inv"));
		}
		if(!(map.get("id") instanceof Boolean)&& map.get("id")!=null){
			this.setId((Integer)map.get("id"));
		}
		if(map.get("is_pos_groupable") instanceof Boolean){
			this.setIs_pos_groupable(((Boolean)map.get("is_pos_groupable"))? "true" : "false");
		}
		if(!(map.get("measure_type") instanceof Boolean)&& map.get("measure_type")!=null){
			this.setMeasure_type((String)map.get("measure_type"));
		}
		if(!(map.get("name") instanceof Boolean)&& map.get("name")!=null){
			this.setName((String)map.get("name"));
		}
		if(!(map.get("rounding") instanceof Boolean)&& map.get("rounding")!=null){
			this.setRounding((Double)map.get("rounding"));
		}
		if(!(map.get("uom_type") instanceof Boolean)&& map.get("uom_type")!=null){
			this.setUom_type((String)map.get("uom_type"));
		}
		if(!(map.get("write_date") instanceof Boolean)&& map.get("write_date")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse((String)map.get("write_date"));
   			this.setWrite_date(new Timestamp(parse.getTime()));
		}
		if(!(map.get("write_uid") instanceof Boolean)&& map.get("write_uid")!=null){
			Object[] objs = (Object[])map.get("write_uid");
			if(objs.length > 0){
				this.setWrite_uid((Integer)objs[0]);
			}
		}
		if(!(map.get("write_uid") instanceof Boolean)&& map.get("write_uid")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("write_uid");
			if(objs.length > 1){
				this.setWrite_uid_text((String)objs[1]);
			}
		}
		if(!(map.get("__last_update") instanceof Boolean)&& map.get("__last_update")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse((String)map.get("__last_update"));
   			this.set__last_update(new Timestamp(parse.getTime()));
		}
	}

	public Map<String, Object> toMap() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		if(this.getActive()!=null&&this.getActiveDirtyFlag()){
			map.put("active",Boolean.parseBoolean(this.getActive()));		
		}		if(this.getCategory_id()!=null&&this.getCategory_idDirtyFlag()){
			map.put("category_id",this.getCategory_id());
		}else if(this.getCategory_idDirtyFlag()){
			map.put("category_id",false);
		}
		if(this.getCategory_id_text()!=null&&this.getCategory_id_textDirtyFlag()){
			//忽略文本外键category_id_text
		}else if(this.getCategory_id_textDirtyFlag()){
			map.put("category_id",false);
		}
		if(this.getCreate_date()!=null&&this.getCreate_dateDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String datetimeStr = sdf.format(this.getCreate_date());
			map.put("create_date",datetimeStr);
		}else if(this.getCreate_dateDirtyFlag()){
			map.put("create_date",false);
		}
		if(this.getCreate_uid()!=null&&this.getCreate_uidDirtyFlag()){
			map.put("create_uid",this.getCreate_uid());
		}else if(this.getCreate_uidDirtyFlag()){
			map.put("create_uid",false);
		}
		if(this.getCreate_uid_text()!=null&&this.getCreate_uid_textDirtyFlag()){
			//忽略文本外键create_uid_text
		}else if(this.getCreate_uid_textDirtyFlag()){
			map.put("create_uid",false);
		}
		if(this.getDisplay_name()!=null&&this.getDisplay_nameDirtyFlag()){
			map.put("display_name",this.getDisplay_name());
		}else if(this.getDisplay_nameDirtyFlag()){
			map.put("display_name",false);
		}
		if(this.getFactor()!=null&&this.getFactorDirtyFlag()){
			map.put("factor",this.getFactor());
		}else if(this.getFactorDirtyFlag()){
			map.put("factor",false);
		}
		if(this.getFactor_inv()!=null&&this.getFactor_invDirtyFlag()){
			map.put("factor_inv",this.getFactor_inv());
		}else if(this.getFactor_invDirtyFlag()){
			map.put("factor_inv",false);
		}
		if(this.getId()!=null&&this.getIdDirtyFlag()){
			map.put("id",this.getId());
		}else if(this.getIdDirtyFlag()){
			map.put("id",false);
		}
		if(this.getIs_pos_groupable()!=null&&this.getIs_pos_groupableDirtyFlag()){
			map.put("is_pos_groupable",Boolean.parseBoolean(this.getIs_pos_groupable()));		
		}		if(this.getMeasure_type()!=null&&this.getMeasure_typeDirtyFlag()){
			map.put("measure_type",this.getMeasure_type());
		}else if(this.getMeasure_typeDirtyFlag()){
			map.put("measure_type",false);
		}
		if(this.getName()!=null&&this.getNameDirtyFlag()){
			map.put("name",this.getName());
		}else if(this.getNameDirtyFlag()){
			map.put("name",false);
		}
		if(this.getRounding()!=null&&this.getRoundingDirtyFlag()){
			map.put("rounding",this.getRounding());
		}else if(this.getRoundingDirtyFlag()){
			map.put("rounding",false);
		}
		if(this.getUom_type()!=null&&this.getUom_typeDirtyFlag()){
			map.put("uom_type",this.getUom_type());
		}else if(this.getUom_typeDirtyFlag()){
			map.put("uom_type",false);
		}
		if(this.getWrite_date()!=null&&this.getWrite_dateDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String datetimeStr = sdf.format(this.getWrite_date());
			map.put("write_date",datetimeStr);
		}else if(this.getWrite_dateDirtyFlag()){
			map.put("write_date",false);
		}
		if(this.getWrite_uid()!=null&&this.getWrite_uidDirtyFlag()){
			map.put("write_uid",this.getWrite_uid());
		}else if(this.getWrite_uidDirtyFlag()){
			map.put("write_uid",false);
		}
		if(this.getWrite_uid_text()!=null&&this.getWrite_uid_textDirtyFlag()){
			//忽略文本外键write_uid_text
		}else if(this.getWrite_uid_textDirtyFlag()){
			map.put("write_uid",false);
		}
		if(this.get__last_update()!=null&&this.get__last_updateDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String datetimeStr = sdf.format(this.get__last_update());
			map.put("__last_update",datetimeStr);
		}else if(this.get__last_updateDirtyFlag()){
			map.put("__last_update",false);
		}
		return map;
	}

}
