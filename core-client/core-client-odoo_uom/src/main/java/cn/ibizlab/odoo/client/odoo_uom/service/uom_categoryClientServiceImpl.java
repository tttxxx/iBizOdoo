package cn.ibizlab.odoo.client.odoo_uom.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Iuom_category;
import cn.ibizlab.odoo.core.client.service.Iuom_categoryClientService;
import cn.ibizlab.odoo.client.odoo_uom.model.uom_categoryImpl;
import cn.ibizlab.odoo.client.odoo_uom.odooclient.Iuom_categoryOdooClient;
import cn.ibizlab.odoo.client.odoo_uom.odooclient.impl.uom_categoryOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[uom_category] 服务对象接口
 */
@Service
public class uom_categoryClientServiceImpl implements Iuom_categoryClientService {
    @Autowired
    private  Iuom_categoryOdooClient  uom_categoryOdooClient;

    public Iuom_category createModel() {		
		return new uom_categoryImpl();
	}


        public void createBatch(List<Iuom_category> uom_categories){
            
        }
        
        public void get(Iuom_category uom_category){
            this.uom_categoryOdooClient.get(uom_category) ;
        }
        
        public void updateBatch(List<Iuom_category> uom_categories){
            
        }
        
        public void remove(Iuom_category uom_category){
this.uom_categoryOdooClient.remove(uom_category) ;
        }
        
        public void create(Iuom_category uom_category){
this.uom_categoryOdooClient.create(uom_category) ;
        }
        
        public Page<Iuom_category> search(SearchContext context){
            return this.uom_categoryOdooClient.search(context) ;
        }
        
        public void removeBatch(List<Iuom_category> uom_categories){
            
        }
        
        public void update(Iuom_category uom_category){
this.uom_categoryOdooClient.update(uom_category) ;
        }
        
        public Page<Iuom_category> select(SearchContext context){
            return null ;
        }
        

}

