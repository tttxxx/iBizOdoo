package cn.ibizlab.odoo.client.odoo_account.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Iaccount_account;
import cn.ibizlab.odoo.core.client.service.Iaccount_accountClientService;
import cn.ibizlab.odoo.client.odoo_account.model.account_accountImpl;
import cn.ibizlab.odoo.client.odoo_account.odooclient.Iaccount_accountOdooClient;
import cn.ibizlab.odoo.client.odoo_account.odooclient.impl.account_accountOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[account_account] 服务对象接口
 */
@Service
public class account_accountClientServiceImpl implements Iaccount_accountClientService {
    @Autowired
    private  Iaccount_accountOdooClient  account_accountOdooClient;

    public Iaccount_account createModel() {		
		return new account_accountImpl();
	}


        public void remove(Iaccount_account account_account){
this.account_accountOdooClient.remove(account_account) ;
        }
        
        public Page<Iaccount_account> search(SearchContext context){
            return this.account_accountOdooClient.search(context) ;
        }
        
        public void createBatch(List<Iaccount_account> account_accounts){
            
        }
        
        public void get(Iaccount_account account_account){
            this.account_accountOdooClient.get(account_account) ;
        }
        
        public void update(Iaccount_account account_account){
this.account_accountOdooClient.update(account_account) ;
        }
        
        public void updateBatch(List<Iaccount_account> account_accounts){
            
        }
        
        public void create(Iaccount_account account_account){
this.account_accountOdooClient.create(account_account) ;
        }
        
        public void removeBatch(List<Iaccount_account> account_accounts){
            
        }
        
        public Page<Iaccount_account> select(SearchContext context){
            return null ;
        }
        

}

