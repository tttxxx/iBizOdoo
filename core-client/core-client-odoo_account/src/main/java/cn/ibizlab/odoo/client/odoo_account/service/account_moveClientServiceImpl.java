package cn.ibizlab.odoo.client.odoo_account.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Iaccount_move;
import cn.ibizlab.odoo.core.client.service.Iaccount_moveClientService;
import cn.ibizlab.odoo.client.odoo_account.model.account_moveImpl;
import cn.ibizlab.odoo.client.odoo_account.odooclient.Iaccount_moveOdooClient;
import cn.ibizlab.odoo.client.odoo_account.odooclient.impl.account_moveOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[account_move] 服务对象接口
 */
@Service
public class account_moveClientServiceImpl implements Iaccount_moveClientService {
    @Autowired
    private  Iaccount_moveOdooClient  account_moveOdooClient;

    public Iaccount_move createModel() {		
		return new account_moveImpl();
	}


        public void updateBatch(List<Iaccount_move> account_moves){
            
        }
        
        public void update(Iaccount_move account_move){
this.account_moveOdooClient.update(account_move) ;
        }
        
        public Page<Iaccount_move> search(SearchContext context){
            return this.account_moveOdooClient.search(context) ;
        }
        
        public void get(Iaccount_move account_move){
            this.account_moveOdooClient.get(account_move) ;
        }
        
        public void create(Iaccount_move account_move){
this.account_moveOdooClient.create(account_move) ;
        }
        
        public void remove(Iaccount_move account_move){
this.account_moveOdooClient.remove(account_move) ;
        }
        
        public void removeBatch(List<Iaccount_move> account_moves){
            
        }
        
        public void createBatch(List<Iaccount_move> account_moves){
            
        }
        
        public Page<Iaccount_move> select(SearchContext context){
            return null ;
        }
        

}

