package cn.ibizlab.odoo.client.odoo_account.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Iaccount_account_type;
import cn.ibizlab.odoo.core.client.service.Iaccount_account_typeClientService;
import cn.ibizlab.odoo.client.odoo_account.model.account_account_typeImpl;
import cn.ibizlab.odoo.client.odoo_account.odooclient.Iaccount_account_typeOdooClient;
import cn.ibizlab.odoo.client.odoo_account.odooclient.impl.account_account_typeOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[account_account_type] 服务对象接口
 */
@Service
public class account_account_typeClientServiceImpl implements Iaccount_account_typeClientService {
    @Autowired
    private  Iaccount_account_typeOdooClient  account_account_typeOdooClient;

    public Iaccount_account_type createModel() {		
		return new account_account_typeImpl();
	}


        public void createBatch(List<Iaccount_account_type> account_account_types){
            
        }
        
        public void removeBatch(List<Iaccount_account_type> account_account_types){
            
        }
        
        public void create(Iaccount_account_type account_account_type){
this.account_account_typeOdooClient.create(account_account_type) ;
        }
        
        public Page<Iaccount_account_type> search(SearchContext context){
            return this.account_account_typeOdooClient.search(context) ;
        }
        
        public void updateBatch(List<Iaccount_account_type> account_account_types){
            
        }
        
        public void remove(Iaccount_account_type account_account_type){
this.account_account_typeOdooClient.remove(account_account_type) ;
        }
        
        public void update(Iaccount_account_type account_account_type){
this.account_account_typeOdooClient.update(account_account_type) ;
        }
        
        public void get(Iaccount_account_type account_account_type){
            this.account_account_typeOdooClient.get(account_account_type) ;
        }
        
        public Page<Iaccount_account_type> select(SearchContext context){
            return null ;
        }
        

}

