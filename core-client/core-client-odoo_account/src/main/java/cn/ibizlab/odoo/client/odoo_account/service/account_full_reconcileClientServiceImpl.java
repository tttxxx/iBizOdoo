package cn.ibizlab.odoo.client.odoo_account.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Iaccount_full_reconcile;
import cn.ibizlab.odoo.core.client.service.Iaccount_full_reconcileClientService;
import cn.ibizlab.odoo.client.odoo_account.model.account_full_reconcileImpl;
import cn.ibizlab.odoo.client.odoo_account.odooclient.Iaccount_full_reconcileOdooClient;
import cn.ibizlab.odoo.client.odoo_account.odooclient.impl.account_full_reconcileOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[account_full_reconcile] 服务对象接口
 */
@Service
public class account_full_reconcileClientServiceImpl implements Iaccount_full_reconcileClientService {
    @Autowired
    private  Iaccount_full_reconcileOdooClient  account_full_reconcileOdooClient;

    public Iaccount_full_reconcile createModel() {		
		return new account_full_reconcileImpl();
	}


        public void create(Iaccount_full_reconcile account_full_reconcile){
this.account_full_reconcileOdooClient.create(account_full_reconcile) ;
        }
        
        public void get(Iaccount_full_reconcile account_full_reconcile){
            this.account_full_reconcileOdooClient.get(account_full_reconcile) ;
        }
        
        public void removeBatch(List<Iaccount_full_reconcile> account_full_reconciles){
            
        }
        
        public void remove(Iaccount_full_reconcile account_full_reconcile){
this.account_full_reconcileOdooClient.remove(account_full_reconcile) ;
        }
        
        public Page<Iaccount_full_reconcile> search(SearchContext context){
            return this.account_full_reconcileOdooClient.search(context) ;
        }
        
        public void updateBatch(List<Iaccount_full_reconcile> account_full_reconciles){
            
        }
        
        public void update(Iaccount_full_reconcile account_full_reconcile){
this.account_full_reconcileOdooClient.update(account_full_reconcile) ;
        }
        
        public void createBatch(List<Iaccount_full_reconcile> account_full_reconciles){
            
        }
        
        public Page<Iaccount_full_reconcile> select(SearchContext context){
            return null ;
        }
        

}

