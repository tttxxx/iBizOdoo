package cn.ibizlab.odoo.client.odoo_account.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Iaccount_fiscal_position_account;
import cn.ibizlab.odoo.core.client.service.Iaccount_fiscal_position_accountClientService;
import cn.ibizlab.odoo.client.odoo_account.model.account_fiscal_position_accountImpl;
import cn.ibizlab.odoo.client.odoo_account.odooclient.Iaccount_fiscal_position_accountOdooClient;
import cn.ibizlab.odoo.client.odoo_account.odooclient.impl.account_fiscal_position_accountOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[account_fiscal_position_account] 服务对象接口
 */
@Service
public class account_fiscal_position_accountClientServiceImpl implements Iaccount_fiscal_position_accountClientService {
    @Autowired
    private  Iaccount_fiscal_position_accountOdooClient  account_fiscal_position_accountOdooClient;

    public Iaccount_fiscal_position_account createModel() {		
		return new account_fiscal_position_accountImpl();
	}


        public void updateBatch(List<Iaccount_fiscal_position_account> account_fiscal_position_accounts){
            
        }
        
        public void create(Iaccount_fiscal_position_account account_fiscal_position_account){
this.account_fiscal_position_accountOdooClient.create(account_fiscal_position_account) ;
        }
        
        public void createBatch(List<Iaccount_fiscal_position_account> account_fiscal_position_accounts){
            
        }
        
        public void update(Iaccount_fiscal_position_account account_fiscal_position_account){
this.account_fiscal_position_accountOdooClient.update(account_fiscal_position_account) ;
        }
        
        public void get(Iaccount_fiscal_position_account account_fiscal_position_account){
            this.account_fiscal_position_accountOdooClient.get(account_fiscal_position_account) ;
        }
        
        public void remove(Iaccount_fiscal_position_account account_fiscal_position_account){
this.account_fiscal_position_accountOdooClient.remove(account_fiscal_position_account) ;
        }
        
        public Page<Iaccount_fiscal_position_account> search(SearchContext context){
            return this.account_fiscal_position_accountOdooClient.search(context) ;
        }
        
        public void removeBatch(List<Iaccount_fiscal_position_account> account_fiscal_position_accounts){
            
        }
        
        public Page<Iaccount_fiscal_position_account> select(SearchContext context){
            return null ;
        }
        

}

