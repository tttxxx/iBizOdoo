package cn.ibizlab.odoo.client.odoo_account.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Iaccount_invoice_refund;
import cn.ibizlab.odoo.core.client.service.Iaccount_invoice_refundClientService;
import cn.ibizlab.odoo.client.odoo_account.model.account_invoice_refundImpl;
import cn.ibizlab.odoo.client.odoo_account.odooclient.Iaccount_invoice_refundOdooClient;
import cn.ibizlab.odoo.client.odoo_account.odooclient.impl.account_invoice_refundOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[account_invoice_refund] 服务对象接口
 */
@Service
public class account_invoice_refundClientServiceImpl implements Iaccount_invoice_refundClientService {
    @Autowired
    private  Iaccount_invoice_refundOdooClient  account_invoice_refundOdooClient;

    public Iaccount_invoice_refund createModel() {		
		return new account_invoice_refundImpl();
	}


        public void updateBatch(List<Iaccount_invoice_refund> account_invoice_refunds){
            
        }
        
        public void get(Iaccount_invoice_refund account_invoice_refund){
            this.account_invoice_refundOdooClient.get(account_invoice_refund) ;
        }
        
        public void removeBatch(List<Iaccount_invoice_refund> account_invoice_refunds){
            
        }
        
        public void remove(Iaccount_invoice_refund account_invoice_refund){
this.account_invoice_refundOdooClient.remove(account_invoice_refund) ;
        }
        
        public Page<Iaccount_invoice_refund> search(SearchContext context){
            return this.account_invoice_refundOdooClient.search(context) ;
        }
        
        public void update(Iaccount_invoice_refund account_invoice_refund){
this.account_invoice_refundOdooClient.update(account_invoice_refund) ;
        }
        
        public void create(Iaccount_invoice_refund account_invoice_refund){
this.account_invoice_refundOdooClient.create(account_invoice_refund) ;
        }
        
        public void createBatch(List<Iaccount_invoice_refund> account_invoice_refunds){
            
        }
        
        public Page<Iaccount_invoice_refund> select(SearchContext context){
            return null ;
        }
        

}

