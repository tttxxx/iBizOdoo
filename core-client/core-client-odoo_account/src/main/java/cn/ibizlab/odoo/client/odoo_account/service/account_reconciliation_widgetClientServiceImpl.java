package cn.ibizlab.odoo.client.odoo_account.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Iaccount_reconciliation_widget;
import cn.ibizlab.odoo.core.client.service.Iaccount_reconciliation_widgetClientService;
import cn.ibizlab.odoo.client.odoo_account.model.account_reconciliation_widgetImpl;
import cn.ibizlab.odoo.client.odoo_account.odooclient.Iaccount_reconciliation_widgetOdooClient;
import cn.ibizlab.odoo.client.odoo_account.odooclient.impl.account_reconciliation_widgetOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[account_reconciliation_widget] 服务对象接口
 */
@Service
public class account_reconciliation_widgetClientServiceImpl implements Iaccount_reconciliation_widgetClientService {
    @Autowired
    private  Iaccount_reconciliation_widgetOdooClient  account_reconciliation_widgetOdooClient;

    public Iaccount_reconciliation_widget createModel() {		
		return new account_reconciliation_widgetImpl();
	}


        public void removeBatch(List<Iaccount_reconciliation_widget> account_reconciliation_widgets){
            
        }
        
        public void update(Iaccount_reconciliation_widget account_reconciliation_widget){
this.account_reconciliation_widgetOdooClient.update(account_reconciliation_widget) ;
        }
        
        public Page<Iaccount_reconciliation_widget> search(SearchContext context){
            return this.account_reconciliation_widgetOdooClient.search(context) ;
        }
        
        public void createBatch(List<Iaccount_reconciliation_widget> account_reconciliation_widgets){
            
        }
        
        public void get(Iaccount_reconciliation_widget account_reconciliation_widget){
            this.account_reconciliation_widgetOdooClient.get(account_reconciliation_widget) ;
        }
        
        public void create(Iaccount_reconciliation_widget account_reconciliation_widget){
this.account_reconciliation_widgetOdooClient.create(account_reconciliation_widget) ;
        }
        
        public void remove(Iaccount_reconciliation_widget account_reconciliation_widget){
this.account_reconciliation_widgetOdooClient.remove(account_reconciliation_widget) ;
        }
        
        public void updateBatch(List<Iaccount_reconciliation_widget> account_reconciliation_widgets){
            
        }
        
        public Page<Iaccount_reconciliation_widget> select(SearchContext context){
            return null ;
        }
        

}

