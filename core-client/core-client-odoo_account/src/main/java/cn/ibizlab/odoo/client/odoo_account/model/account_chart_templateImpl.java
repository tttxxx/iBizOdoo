package cn.ibizlab.odoo.client.odoo_account.model;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.math.BigInteger;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.core.client.model.Iaccount_chart_template;
import cn.ibizlab.odoo.util.helper.OdooClientHelper;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang.ArrayUtils;
import org.springframework.util.StringUtils;

/**
 * 接口实体[account_chart_template] 对象
 */
public class account_chart_templateImpl implements Iaccount_chart_template,Serializable{

    /**
     * 关联的科目模板
     */
    public String account_ids;

    @JsonIgnore
    public boolean account_idsDirtyFlag;
    
    /**
     * 银行科目的前缀
     */
    public String bank_account_code_prefix;

    @JsonIgnore
    public boolean bank_account_code_prefixDirtyFlag;
    
    /**
     * 主现金科目的前缀
     */
    public String cash_account_code_prefix;

    @JsonIgnore
    public boolean cash_account_code_prefixDirtyFlag;
    
    /**
     * # 数字
     */
    public Integer code_digits;

    @JsonIgnore
    public boolean code_digitsDirtyFlag;
    
    /**
     * 税率完整集合
     */
    public String complete_tax_set;

    @JsonIgnore
    public boolean complete_tax_setDirtyFlag;
    
    /**
     * 创建时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp create_date;

    @JsonIgnore
    public boolean create_dateDirtyFlag;
    
    /**
     * 创建人
     */
    public Integer create_uid;

    @JsonIgnore
    public boolean create_uidDirtyFlag;
    
    /**
     * 创建人
     */
    public String create_uid_text;

    @JsonIgnore
    public boolean create_uid_textDirtyFlag;
    
    /**
     * 币种
     */
    public Integer currency_id;

    @JsonIgnore
    public boolean currency_idDirtyFlag;
    
    /**
     * 币种
     */
    public String currency_id_text;

    @JsonIgnore
    public boolean currency_id_textDirtyFlag;
    
    /**
     * 显示名称
     */
    public String display_name;

    @JsonIgnore
    public boolean display_nameDirtyFlag;
    
    /**
     * 汇率损失科目
     */
    public Integer expense_currency_exchange_account_id;

    @JsonIgnore
    public boolean expense_currency_exchange_account_idDirtyFlag;
    
    /**
     * 汇率损失科目
     */
    public String expense_currency_exchange_account_id_text;

    @JsonIgnore
    public boolean expense_currency_exchange_account_id_textDirtyFlag;
    
    /**
     * ID
     */
    public Integer id;

    @JsonIgnore
    public boolean idDirtyFlag;
    
    /**
     * 汇率增益科目
     */
    public Integer income_currency_exchange_account_id;

    @JsonIgnore
    public boolean income_currency_exchange_account_idDirtyFlag;
    
    /**
     * 汇率增益科目
     */
    public String income_currency_exchange_account_id_text;

    @JsonIgnore
    public boolean income_currency_exchange_account_id_textDirtyFlag;
    
    /**
     * 名称
     */
    public String name;

    @JsonIgnore
    public boolean nameDirtyFlag;
    
    /**
     * 上级表模板
     */
    public Integer parent_id;

    @JsonIgnore
    public boolean parent_idDirtyFlag;
    
    /**
     * 上级表模板
     */
    public String parent_id_text;

    @JsonIgnore
    public boolean parent_id_textDirtyFlag;
    
    /**
     * 费用科目的类别
     */
    public Integer property_account_expense_categ_id;

    @JsonIgnore
    public boolean property_account_expense_categ_idDirtyFlag;
    
    /**
     * 费用科目的类别
     */
    public String property_account_expense_categ_id_text;

    @JsonIgnore
    public boolean property_account_expense_categ_id_textDirtyFlag;
    
    /**
     * 产品模板的费用科目
     */
    public Integer property_account_expense_id;

    @JsonIgnore
    public boolean property_account_expense_idDirtyFlag;
    
    /**
     * 产品模板的费用科目
     */
    public String property_account_expense_id_text;

    @JsonIgnore
    public boolean property_account_expense_id_textDirtyFlag;
    
    /**
     * 收入科目的类别
     */
    public Integer property_account_income_categ_id;

    @JsonIgnore
    public boolean property_account_income_categ_idDirtyFlag;
    
    /**
     * 收入科目的类别
     */
    public String property_account_income_categ_id_text;

    @JsonIgnore
    public boolean property_account_income_categ_id_textDirtyFlag;
    
    /**
     * 产品模板的收入科目
     */
    public Integer property_account_income_id;

    @JsonIgnore
    public boolean property_account_income_idDirtyFlag;
    
    /**
     * 产品模板的收入科目
     */
    public String property_account_income_id_text;

    @JsonIgnore
    public boolean property_account_income_id_textDirtyFlag;
    
    /**
     * 应付科目
     */
    public Integer property_account_payable_id;

    @JsonIgnore
    public boolean property_account_payable_idDirtyFlag;
    
    /**
     * 应付科目
     */
    public String property_account_payable_id_text;

    @JsonIgnore
    public boolean property_account_payable_id_textDirtyFlag;
    
    /**
     * 应收科目
     */
    public Integer property_account_receivable_id;

    @JsonIgnore
    public boolean property_account_receivable_idDirtyFlag;
    
    /**
     * 应收科目
     */
    public String property_account_receivable_id_text;

    @JsonIgnore
    public boolean property_account_receivable_id_textDirtyFlag;
    
    /**
     * 库存计价的入库科目
     */
    public Integer property_stock_account_input_categ_id;

    @JsonIgnore
    public boolean property_stock_account_input_categ_idDirtyFlag;
    
    /**
     * 库存计价的入库科目
     */
    public String property_stock_account_input_categ_id_text;

    @JsonIgnore
    public boolean property_stock_account_input_categ_id_textDirtyFlag;
    
    /**
     * 库存计价的出货科目
     */
    public Integer property_stock_account_output_categ_id;

    @JsonIgnore
    public boolean property_stock_account_output_categ_idDirtyFlag;
    
    /**
     * 库存计价的出货科目
     */
    public String property_stock_account_output_categ_id_text;

    @JsonIgnore
    public boolean property_stock_account_output_categ_id_textDirtyFlag;
    
    /**
     * 库存计价的科目模板
     */
    public Integer property_stock_valuation_account_id;

    @JsonIgnore
    public boolean property_stock_valuation_account_idDirtyFlag;
    
    /**
     * 库存计价的科目模板
     */
    public String property_stock_valuation_account_id_text;

    @JsonIgnore
    public boolean property_stock_valuation_account_id_textDirtyFlag;
    
    /**
     * 口语
     */
    public String spoken_languages;

    @JsonIgnore
    public boolean spoken_languagesDirtyFlag;
    
    /**
     * 税模板列表
     */
    public String tax_template_ids;

    @JsonIgnore
    public boolean tax_template_idsDirtyFlag;
    
    /**
     * 主转账帐户的前缀
     */
    public String transfer_account_code_prefix;

    @JsonIgnore
    public boolean transfer_account_code_prefixDirtyFlag;
    
    /**
     * 使用anglo-saxon会计
     */
    public String use_anglo_saxon;

    @JsonIgnore
    public boolean use_anglo_saxonDirtyFlag;
    
    /**
     * 可显示？
     */
    public String visible;

    @JsonIgnore
    public boolean visibleDirtyFlag;
    
    /**
     * 最后更新时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp write_date;

    @JsonIgnore
    public boolean write_dateDirtyFlag;
    
    /**
     * 最后更新人
     */
    public Integer write_uid;

    @JsonIgnore
    public boolean write_uidDirtyFlag;
    
    /**
     * 最后更新人
     */
    public String write_uid_text;

    @JsonIgnore
    public boolean write_uid_textDirtyFlag;
    
    /**
     * 最后修改日
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp __last_update;

    @JsonIgnore
    public boolean __last_updateDirtyFlag;
    
    /**
     * 获取 [关联的科目模板]
     */
    @JsonProperty("account_ids")
    public String getAccount_ids(){
        return this.account_ids ;
    }

    /**
     * 设置 [关联的科目模板]
     */
    @JsonProperty("account_ids")
    public void setAccount_ids(String  account_ids){
        this.account_ids = account_ids ;
        this.account_idsDirtyFlag = true ;
    }

     /**
     * 获取 [关联的科目模板]脏标记
     */
    @JsonIgnore
    public boolean getAccount_idsDirtyFlag(){
        return this.account_idsDirtyFlag ;
    }   

    /**
     * 获取 [银行科目的前缀]
     */
    @JsonProperty("bank_account_code_prefix")
    public String getBank_account_code_prefix(){
        return this.bank_account_code_prefix ;
    }

    /**
     * 设置 [银行科目的前缀]
     */
    @JsonProperty("bank_account_code_prefix")
    public void setBank_account_code_prefix(String  bank_account_code_prefix){
        this.bank_account_code_prefix = bank_account_code_prefix ;
        this.bank_account_code_prefixDirtyFlag = true ;
    }

     /**
     * 获取 [银行科目的前缀]脏标记
     */
    @JsonIgnore
    public boolean getBank_account_code_prefixDirtyFlag(){
        return this.bank_account_code_prefixDirtyFlag ;
    }   

    /**
     * 获取 [主现金科目的前缀]
     */
    @JsonProperty("cash_account_code_prefix")
    public String getCash_account_code_prefix(){
        return this.cash_account_code_prefix ;
    }

    /**
     * 设置 [主现金科目的前缀]
     */
    @JsonProperty("cash_account_code_prefix")
    public void setCash_account_code_prefix(String  cash_account_code_prefix){
        this.cash_account_code_prefix = cash_account_code_prefix ;
        this.cash_account_code_prefixDirtyFlag = true ;
    }

     /**
     * 获取 [主现金科目的前缀]脏标记
     */
    @JsonIgnore
    public boolean getCash_account_code_prefixDirtyFlag(){
        return this.cash_account_code_prefixDirtyFlag ;
    }   

    /**
     * 获取 [# 数字]
     */
    @JsonProperty("code_digits")
    public Integer getCode_digits(){
        return this.code_digits ;
    }

    /**
     * 设置 [# 数字]
     */
    @JsonProperty("code_digits")
    public void setCode_digits(Integer  code_digits){
        this.code_digits = code_digits ;
        this.code_digitsDirtyFlag = true ;
    }

     /**
     * 获取 [# 数字]脏标记
     */
    @JsonIgnore
    public boolean getCode_digitsDirtyFlag(){
        return this.code_digitsDirtyFlag ;
    }   

    /**
     * 获取 [税率完整集合]
     */
    @JsonProperty("complete_tax_set")
    public String getComplete_tax_set(){
        return this.complete_tax_set ;
    }

    /**
     * 设置 [税率完整集合]
     */
    @JsonProperty("complete_tax_set")
    public void setComplete_tax_set(String  complete_tax_set){
        this.complete_tax_set = complete_tax_set ;
        this.complete_tax_setDirtyFlag = true ;
    }

     /**
     * 获取 [税率完整集合]脏标记
     */
    @JsonIgnore
    public boolean getComplete_tax_setDirtyFlag(){
        return this.complete_tax_setDirtyFlag ;
    }   

    /**
     * 获取 [创建时间]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return this.create_date ;
    }

    /**
     * 设置 [创建时间]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

     /**
     * 获取 [创建时间]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return this.create_dateDirtyFlag ;
    }   

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return this.create_uid ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

     /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return this.create_uidDirtyFlag ;
    }   

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return this.create_uid_text ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

     /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return this.create_uid_textDirtyFlag ;
    }   

    /**
     * 获取 [币种]
     */
    @JsonProperty("currency_id")
    public Integer getCurrency_id(){
        return this.currency_id ;
    }

    /**
     * 设置 [币种]
     */
    @JsonProperty("currency_id")
    public void setCurrency_id(Integer  currency_id){
        this.currency_id = currency_id ;
        this.currency_idDirtyFlag = true ;
    }

     /**
     * 获取 [币种]脏标记
     */
    @JsonIgnore
    public boolean getCurrency_idDirtyFlag(){
        return this.currency_idDirtyFlag ;
    }   

    /**
     * 获取 [币种]
     */
    @JsonProperty("currency_id_text")
    public String getCurrency_id_text(){
        return this.currency_id_text ;
    }

    /**
     * 设置 [币种]
     */
    @JsonProperty("currency_id_text")
    public void setCurrency_id_text(String  currency_id_text){
        this.currency_id_text = currency_id_text ;
        this.currency_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [币种]脏标记
     */
    @JsonIgnore
    public boolean getCurrency_id_textDirtyFlag(){
        return this.currency_id_textDirtyFlag ;
    }   

    /**
     * 获取 [显示名称]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return this.display_name ;
    }

    /**
     * 设置 [显示名称]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

     /**
     * 获取 [显示名称]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return this.display_nameDirtyFlag ;
    }   

    /**
     * 获取 [汇率损失科目]
     */
    @JsonProperty("expense_currency_exchange_account_id")
    public Integer getExpense_currency_exchange_account_id(){
        return this.expense_currency_exchange_account_id ;
    }

    /**
     * 设置 [汇率损失科目]
     */
    @JsonProperty("expense_currency_exchange_account_id")
    public void setExpense_currency_exchange_account_id(Integer  expense_currency_exchange_account_id){
        this.expense_currency_exchange_account_id = expense_currency_exchange_account_id ;
        this.expense_currency_exchange_account_idDirtyFlag = true ;
    }

     /**
     * 获取 [汇率损失科目]脏标记
     */
    @JsonIgnore
    public boolean getExpense_currency_exchange_account_idDirtyFlag(){
        return this.expense_currency_exchange_account_idDirtyFlag ;
    }   

    /**
     * 获取 [汇率损失科目]
     */
    @JsonProperty("expense_currency_exchange_account_id_text")
    public String getExpense_currency_exchange_account_id_text(){
        return this.expense_currency_exchange_account_id_text ;
    }

    /**
     * 设置 [汇率损失科目]
     */
    @JsonProperty("expense_currency_exchange_account_id_text")
    public void setExpense_currency_exchange_account_id_text(String  expense_currency_exchange_account_id_text){
        this.expense_currency_exchange_account_id_text = expense_currency_exchange_account_id_text ;
        this.expense_currency_exchange_account_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [汇率损失科目]脏标记
     */
    @JsonIgnore
    public boolean getExpense_currency_exchange_account_id_textDirtyFlag(){
        return this.expense_currency_exchange_account_id_textDirtyFlag ;
    }   

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return this.id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

     /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return this.idDirtyFlag ;
    }   

    /**
     * 获取 [汇率增益科目]
     */
    @JsonProperty("income_currency_exchange_account_id")
    public Integer getIncome_currency_exchange_account_id(){
        return this.income_currency_exchange_account_id ;
    }

    /**
     * 设置 [汇率增益科目]
     */
    @JsonProperty("income_currency_exchange_account_id")
    public void setIncome_currency_exchange_account_id(Integer  income_currency_exchange_account_id){
        this.income_currency_exchange_account_id = income_currency_exchange_account_id ;
        this.income_currency_exchange_account_idDirtyFlag = true ;
    }

     /**
     * 获取 [汇率增益科目]脏标记
     */
    @JsonIgnore
    public boolean getIncome_currency_exchange_account_idDirtyFlag(){
        return this.income_currency_exchange_account_idDirtyFlag ;
    }   

    /**
     * 获取 [汇率增益科目]
     */
    @JsonProperty("income_currency_exchange_account_id_text")
    public String getIncome_currency_exchange_account_id_text(){
        return this.income_currency_exchange_account_id_text ;
    }

    /**
     * 设置 [汇率增益科目]
     */
    @JsonProperty("income_currency_exchange_account_id_text")
    public void setIncome_currency_exchange_account_id_text(String  income_currency_exchange_account_id_text){
        this.income_currency_exchange_account_id_text = income_currency_exchange_account_id_text ;
        this.income_currency_exchange_account_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [汇率增益科目]脏标记
     */
    @JsonIgnore
    public boolean getIncome_currency_exchange_account_id_textDirtyFlag(){
        return this.income_currency_exchange_account_id_textDirtyFlag ;
    }   

    /**
     * 获取 [名称]
     */
    @JsonProperty("name")
    public String getName(){
        return this.name ;
    }

    /**
     * 设置 [名称]
     */
    @JsonProperty("name")
    public void setName(String  name){
        this.name = name ;
        this.nameDirtyFlag = true ;
    }

     /**
     * 获取 [名称]脏标记
     */
    @JsonIgnore
    public boolean getNameDirtyFlag(){
        return this.nameDirtyFlag ;
    }   

    /**
     * 获取 [上级表模板]
     */
    @JsonProperty("parent_id")
    public Integer getParent_id(){
        return this.parent_id ;
    }

    /**
     * 设置 [上级表模板]
     */
    @JsonProperty("parent_id")
    public void setParent_id(Integer  parent_id){
        this.parent_id = parent_id ;
        this.parent_idDirtyFlag = true ;
    }

     /**
     * 获取 [上级表模板]脏标记
     */
    @JsonIgnore
    public boolean getParent_idDirtyFlag(){
        return this.parent_idDirtyFlag ;
    }   

    /**
     * 获取 [上级表模板]
     */
    @JsonProperty("parent_id_text")
    public String getParent_id_text(){
        return this.parent_id_text ;
    }

    /**
     * 设置 [上级表模板]
     */
    @JsonProperty("parent_id_text")
    public void setParent_id_text(String  parent_id_text){
        this.parent_id_text = parent_id_text ;
        this.parent_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [上级表模板]脏标记
     */
    @JsonIgnore
    public boolean getParent_id_textDirtyFlag(){
        return this.parent_id_textDirtyFlag ;
    }   

    /**
     * 获取 [费用科目的类别]
     */
    @JsonProperty("property_account_expense_categ_id")
    public Integer getProperty_account_expense_categ_id(){
        return this.property_account_expense_categ_id ;
    }

    /**
     * 设置 [费用科目的类别]
     */
    @JsonProperty("property_account_expense_categ_id")
    public void setProperty_account_expense_categ_id(Integer  property_account_expense_categ_id){
        this.property_account_expense_categ_id = property_account_expense_categ_id ;
        this.property_account_expense_categ_idDirtyFlag = true ;
    }

     /**
     * 获取 [费用科目的类别]脏标记
     */
    @JsonIgnore
    public boolean getProperty_account_expense_categ_idDirtyFlag(){
        return this.property_account_expense_categ_idDirtyFlag ;
    }   

    /**
     * 获取 [费用科目的类别]
     */
    @JsonProperty("property_account_expense_categ_id_text")
    public String getProperty_account_expense_categ_id_text(){
        return this.property_account_expense_categ_id_text ;
    }

    /**
     * 设置 [费用科目的类别]
     */
    @JsonProperty("property_account_expense_categ_id_text")
    public void setProperty_account_expense_categ_id_text(String  property_account_expense_categ_id_text){
        this.property_account_expense_categ_id_text = property_account_expense_categ_id_text ;
        this.property_account_expense_categ_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [费用科目的类别]脏标记
     */
    @JsonIgnore
    public boolean getProperty_account_expense_categ_id_textDirtyFlag(){
        return this.property_account_expense_categ_id_textDirtyFlag ;
    }   

    /**
     * 获取 [产品模板的费用科目]
     */
    @JsonProperty("property_account_expense_id")
    public Integer getProperty_account_expense_id(){
        return this.property_account_expense_id ;
    }

    /**
     * 设置 [产品模板的费用科目]
     */
    @JsonProperty("property_account_expense_id")
    public void setProperty_account_expense_id(Integer  property_account_expense_id){
        this.property_account_expense_id = property_account_expense_id ;
        this.property_account_expense_idDirtyFlag = true ;
    }

     /**
     * 获取 [产品模板的费用科目]脏标记
     */
    @JsonIgnore
    public boolean getProperty_account_expense_idDirtyFlag(){
        return this.property_account_expense_idDirtyFlag ;
    }   

    /**
     * 获取 [产品模板的费用科目]
     */
    @JsonProperty("property_account_expense_id_text")
    public String getProperty_account_expense_id_text(){
        return this.property_account_expense_id_text ;
    }

    /**
     * 设置 [产品模板的费用科目]
     */
    @JsonProperty("property_account_expense_id_text")
    public void setProperty_account_expense_id_text(String  property_account_expense_id_text){
        this.property_account_expense_id_text = property_account_expense_id_text ;
        this.property_account_expense_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [产品模板的费用科目]脏标记
     */
    @JsonIgnore
    public boolean getProperty_account_expense_id_textDirtyFlag(){
        return this.property_account_expense_id_textDirtyFlag ;
    }   

    /**
     * 获取 [收入科目的类别]
     */
    @JsonProperty("property_account_income_categ_id")
    public Integer getProperty_account_income_categ_id(){
        return this.property_account_income_categ_id ;
    }

    /**
     * 设置 [收入科目的类别]
     */
    @JsonProperty("property_account_income_categ_id")
    public void setProperty_account_income_categ_id(Integer  property_account_income_categ_id){
        this.property_account_income_categ_id = property_account_income_categ_id ;
        this.property_account_income_categ_idDirtyFlag = true ;
    }

     /**
     * 获取 [收入科目的类别]脏标记
     */
    @JsonIgnore
    public boolean getProperty_account_income_categ_idDirtyFlag(){
        return this.property_account_income_categ_idDirtyFlag ;
    }   

    /**
     * 获取 [收入科目的类别]
     */
    @JsonProperty("property_account_income_categ_id_text")
    public String getProperty_account_income_categ_id_text(){
        return this.property_account_income_categ_id_text ;
    }

    /**
     * 设置 [收入科目的类别]
     */
    @JsonProperty("property_account_income_categ_id_text")
    public void setProperty_account_income_categ_id_text(String  property_account_income_categ_id_text){
        this.property_account_income_categ_id_text = property_account_income_categ_id_text ;
        this.property_account_income_categ_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [收入科目的类别]脏标记
     */
    @JsonIgnore
    public boolean getProperty_account_income_categ_id_textDirtyFlag(){
        return this.property_account_income_categ_id_textDirtyFlag ;
    }   

    /**
     * 获取 [产品模板的收入科目]
     */
    @JsonProperty("property_account_income_id")
    public Integer getProperty_account_income_id(){
        return this.property_account_income_id ;
    }

    /**
     * 设置 [产品模板的收入科目]
     */
    @JsonProperty("property_account_income_id")
    public void setProperty_account_income_id(Integer  property_account_income_id){
        this.property_account_income_id = property_account_income_id ;
        this.property_account_income_idDirtyFlag = true ;
    }

     /**
     * 获取 [产品模板的收入科目]脏标记
     */
    @JsonIgnore
    public boolean getProperty_account_income_idDirtyFlag(){
        return this.property_account_income_idDirtyFlag ;
    }   

    /**
     * 获取 [产品模板的收入科目]
     */
    @JsonProperty("property_account_income_id_text")
    public String getProperty_account_income_id_text(){
        return this.property_account_income_id_text ;
    }

    /**
     * 设置 [产品模板的收入科目]
     */
    @JsonProperty("property_account_income_id_text")
    public void setProperty_account_income_id_text(String  property_account_income_id_text){
        this.property_account_income_id_text = property_account_income_id_text ;
        this.property_account_income_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [产品模板的收入科目]脏标记
     */
    @JsonIgnore
    public boolean getProperty_account_income_id_textDirtyFlag(){
        return this.property_account_income_id_textDirtyFlag ;
    }   

    /**
     * 获取 [应付科目]
     */
    @JsonProperty("property_account_payable_id")
    public Integer getProperty_account_payable_id(){
        return this.property_account_payable_id ;
    }

    /**
     * 设置 [应付科目]
     */
    @JsonProperty("property_account_payable_id")
    public void setProperty_account_payable_id(Integer  property_account_payable_id){
        this.property_account_payable_id = property_account_payable_id ;
        this.property_account_payable_idDirtyFlag = true ;
    }

     /**
     * 获取 [应付科目]脏标记
     */
    @JsonIgnore
    public boolean getProperty_account_payable_idDirtyFlag(){
        return this.property_account_payable_idDirtyFlag ;
    }   

    /**
     * 获取 [应付科目]
     */
    @JsonProperty("property_account_payable_id_text")
    public String getProperty_account_payable_id_text(){
        return this.property_account_payable_id_text ;
    }

    /**
     * 设置 [应付科目]
     */
    @JsonProperty("property_account_payable_id_text")
    public void setProperty_account_payable_id_text(String  property_account_payable_id_text){
        this.property_account_payable_id_text = property_account_payable_id_text ;
        this.property_account_payable_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [应付科目]脏标记
     */
    @JsonIgnore
    public boolean getProperty_account_payable_id_textDirtyFlag(){
        return this.property_account_payable_id_textDirtyFlag ;
    }   

    /**
     * 获取 [应收科目]
     */
    @JsonProperty("property_account_receivable_id")
    public Integer getProperty_account_receivable_id(){
        return this.property_account_receivable_id ;
    }

    /**
     * 设置 [应收科目]
     */
    @JsonProperty("property_account_receivable_id")
    public void setProperty_account_receivable_id(Integer  property_account_receivable_id){
        this.property_account_receivable_id = property_account_receivable_id ;
        this.property_account_receivable_idDirtyFlag = true ;
    }

     /**
     * 获取 [应收科目]脏标记
     */
    @JsonIgnore
    public boolean getProperty_account_receivable_idDirtyFlag(){
        return this.property_account_receivable_idDirtyFlag ;
    }   

    /**
     * 获取 [应收科目]
     */
    @JsonProperty("property_account_receivable_id_text")
    public String getProperty_account_receivable_id_text(){
        return this.property_account_receivable_id_text ;
    }

    /**
     * 设置 [应收科目]
     */
    @JsonProperty("property_account_receivable_id_text")
    public void setProperty_account_receivable_id_text(String  property_account_receivable_id_text){
        this.property_account_receivable_id_text = property_account_receivable_id_text ;
        this.property_account_receivable_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [应收科目]脏标记
     */
    @JsonIgnore
    public boolean getProperty_account_receivable_id_textDirtyFlag(){
        return this.property_account_receivable_id_textDirtyFlag ;
    }   

    /**
     * 获取 [库存计价的入库科目]
     */
    @JsonProperty("property_stock_account_input_categ_id")
    public Integer getProperty_stock_account_input_categ_id(){
        return this.property_stock_account_input_categ_id ;
    }

    /**
     * 设置 [库存计价的入库科目]
     */
    @JsonProperty("property_stock_account_input_categ_id")
    public void setProperty_stock_account_input_categ_id(Integer  property_stock_account_input_categ_id){
        this.property_stock_account_input_categ_id = property_stock_account_input_categ_id ;
        this.property_stock_account_input_categ_idDirtyFlag = true ;
    }

     /**
     * 获取 [库存计价的入库科目]脏标记
     */
    @JsonIgnore
    public boolean getProperty_stock_account_input_categ_idDirtyFlag(){
        return this.property_stock_account_input_categ_idDirtyFlag ;
    }   

    /**
     * 获取 [库存计价的入库科目]
     */
    @JsonProperty("property_stock_account_input_categ_id_text")
    public String getProperty_stock_account_input_categ_id_text(){
        return this.property_stock_account_input_categ_id_text ;
    }

    /**
     * 设置 [库存计价的入库科目]
     */
    @JsonProperty("property_stock_account_input_categ_id_text")
    public void setProperty_stock_account_input_categ_id_text(String  property_stock_account_input_categ_id_text){
        this.property_stock_account_input_categ_id_text = property_stock_account_input_categ_id_text ;
        this.property_stock_account_input_categ_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [库存计价的入库科目]脏标记
     */
    @JsonIgnore
    public boolean getProperty_stock_account_input_categ_id_textDirtyFlag(){
        return this.property_stock_account_input_categ_id_textDirtyFlag ;
    }   

    /**
     * 获取 [库存计价的出货科目]
     */
    @JsonProperty("property_stock_account_output_categ_id")
    public Integer getProperty_stock_account_output_categ_id(){
        return this.property_stock_account_output_categ_id ;
    }

    /**
     * 设置 [库存计价的出货科目]
     */
    @JsonProperty("property_stock_account_output_categ_id")
    public void setProperty_stock_account_output_categ_id(Integer  property_stock_account_output_categ_id){
        this.property_stock_account_output_categ_id = property_stock_account_output_categ_id ;
        this.property_stock_account_output_categ_idDirtyFlag = true ;
    }

     /**
     * 获取 [库存计价的出货科目]脏标记
     */
    @JsonIgnore
    public boolean getProperty_stock_account_output_categ_idDirtyFlag(){
        return this.property_stock_account_output_categ_idDirtyFlag ;
    }   

    /**
     * 获取 [库存计价的出货科目]
     */
    @JsonProperty("property_stock_account_output_categ_id_text")
    public String getProperty_stock_account_output_categ_id_text(){
        return this.property_stock_account_output_categ_id_text ;
    }

    /**
     * 设置 [库存计价的出货科目]
     */
    @JsonProperty("property_stock_account_output_categ_id_text")
    public void setProperty_stock_account_output_categ_id_text(String  property_stock_account_output_categ_id_text){
        this.property_stock_account_output_categ_id_text = property_stock_account_output_categ_id_text ;
        this.property_stock_account_output_categ_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [库存计价的出货科目]脏标记
     */
    @JsonIgnore
    public boolean getProperty_stock_account_output_categ_id_textDirtyFlag(){
        return this.property_stock_account_output_categ_id_textDirtyFlag ;
    }   

    /**
     * 获取 [库存计价的科目模板]
     */
    @JsonProperty("property_stock_valuation_account_id")
    public Integer getProperty_stock_valuation_account_id(){
        return this.property_stock_valuation_account_id ;
    }

    /**
     * 设置 [库存计价的科目模板]
     */
    @JsonProperty("property_stock_valuation_account_id")
    public void setProperty_stock_valuation_account_id(Integer  property_stock_valuation_account_id){
        this.property_stock_valuation_account_id = property_stock_valuation_account_id ;
        this.property_stock_valuation_account_idDirtyFlag = true ;
    }

     /**
     * 获取 [库存计价的科目模板]脏标记
     */
    @JsonIgnore
    public boolean getProperty_stock_valuation_account_idDirtyFlag(){
        return this.property_stock_valuation_account_idDirtyFlag ;
    }   

    /**
     * 获取 [库存计价的科目模板]
     */
    @JsonProperty("property_stock_valuation_account_id_text")
    public String getProperty_stock_valuation_account_id_text(){
        return this.property_stock_valuation_account_id_text ;
    }

    /**
     * 设置 [库存计价的科目模板]
     */
    @JsonProperty("property_stock_valuation_account_id_text")
    public void setProperty_stock_valuation_account_id_text(String  property_stock_valuation_account_id_text){
        this.property_stock_valuation_account_id_text = property_stock_valuation_account_id_text ;
        this.property_stock_valuation_account_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [库存计价的科目模板]脏标记
     */
    @JsonIgnore
    public boolean getProperty_stock_valuation_account_id_textDirtyFlag(){
        return this.property_stock_valuation_account_id_textDirtyFlag ;
    }   

    /**
     * 获取 [口语]
     */
    @JsonProperty("spoken_languages")
    public String getSpoken_languages(){
        return this.spoken_languages ;
    }

    /**
     * 设置 [口语]
     */
    @JsonProperty("spoken_languages")
    public void setSpoken_languages(String  spoken_languages){
        this.spoken_languages = spoken_languages ;
        this.spoken_languagesDirtyFlag = true ;
    }

     /**
     * 获取 [口语]脏标记
     */
    @JsonIgnore
    public boolean getSpoken_languagesDirtyFlag(){
        return this.spoken_languagesDirtyFlag ;
    }   

    /**
     * 获取 [税模板列表]
     */
    @JsonProperty("tax_template_ids")
    public String getTax_template_ids(){
        return this.tax_template_ids ;
    }

    /**
     * 设置 [税模板列表]
     */
    @JsonProperty("tax_template_ids")
    public void setTax_template_ids(String  tax_template_ids){
        this.tax_template_ids = tax_template_ids ;
        this.tax_template_idsDirtyFlag = true ;
    }

     /**
     * 获取 [税模板列表]脏标记
     */
    @JsonIgnore
    public boolean getTax_template_idsDirtyFlag(){
        return this.tax_template_idsDirtyFlag ;
    }   

    /**
     * 获取 [主转账帐户的前缀]
     */
    @JsonProperty("transfer_account_code_prefix")
    public String getTransfer_account_code_prefix(){
        return this.transfer_account_code_prefix ;
    }

    /**
     * 设置 [主转账帐户的前缀]
     */
    @JsonProperty("transfer_account_code_prefix")
    public void setTransfer_account_code_prefix(String  transfer_account_code_prefix){
        this.transfer_account_code_prefix = transfer_account_code_prefix ;
        this.transfer_account_code_prefixDirtyFlag = true ;
    }

     /**
     * 获取 [主转账帐户的前缀]脏标记
     */
    @JsonIgnore
    public boolean getTransfer_account_code_prefixDirtyFlag(){
        return this.transfer_account_code_prefixDirtyFlag ;
    }   

    /**
     * 获取 [使用anglo-saxon会计]
     */
    @JsonProperty("use_anglo_saxon")
    public String getUse_anglo_saxon(){
        return this.use_anglo_saxon ;
    }

    /**
     * 设置 [使用anglo-saxon会计]
     */
    @JsonProperty("use_anglo_saxon")
    public void setUse_anglo_saxon(String  use_anglo_saxon){
        this.use_anglo_saxon = use_anglo_saxon ;
        this.use_anglo_saxonDirtyFlag = true ;
    }

     /**
     * 获取 [使用anglo-saxon会计]脏标记
     */
    @JsonIgnore
    public boolean getUse_anglo_saxonDirtyFlag(){
        return this.use_anglo_saxonDirtyFlag ;
    }   

    /**
     * 获取 [可显示？]
     */
    @JsonProperty("visible")
    public String getVisible(){
        return this.visible ;
    }

    /**
     * 设置 [可显示？]
     */
    @JsonProperty("visible")
    public void setVisible(String  visible){
        this.visible = visible ;
        this.visibleDirtyFlag = true ;
    }

     /**
     * 获取 [可显示？]脏标记
     */
    @JsonIgnore
    public boolean getVisibleDirtyFlag(){
        return this.visibleDirtyFlag ;
    }   

    /**
     * 获取 [最后更新时间]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return this.write_date ;
    }

    /**
     * 设置 [最后更新时间]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新时间]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return this.write_dateDirtyFlag ;
    }   

    /**
     * 获取 [最后更新人]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return this.write_uid ;
    }

    /**
     * 设置 [最后更新人]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新人]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return this.write_uidDirtyFlag ;
    }   

    /**
     * 获取 [最后更新人]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return this.write_uid_text ;
    }

    /**
     * 设置 [最后更新人]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新人]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return this.write_uid_textDirtyFlag ;
    }   

    /**
     * 获取 [最后修改日]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return this.__last_update ;
    }

    /**
     * 设置 [最后修改日]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

     /**
     * 获取 [最后修改日]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return this.__last_updateDirtyFlag ;
    }   



    public void fromMap(Map<String, Object> map) throws Exception {
		if(!(map.get("account_ids") instanceof Boolean)&& map.get("account_ids")!=null){
			Object[] objs = (Object[])map.get("account_ids");
			if(objs.length > 0){
				Integer[] account_ids = Arrays.copyOfRange(objs,0,objs.length,Integer[].class);
				this.setAccount_ids(Arrays.toString(account_ids));
			}
		}
		if(!(map.get("bank_account_code_prefix") instanceof Boolean)&& map.get("bank_account_code_prefix")!=null){
			this.setBank_account_code_prefix((String)map.get("bank_account_code_prefix"));
		}
		if(!(map.get("cash_account_code_prefix") instanceof Boolean)&& map.get("cash_account_code_prefix")!=null){
			this.setCash_account_code_prefix((String)map.get("cash_account_code_prefix"));
		}
		if(!(map.get("code_digits") instanceof Boolean)&& map.get("code_digits")!=null){
			this.setCode_digits((Integer)map.get("code_digits"));
		}
		if(map.get("complete_tax_set") instanceof Boolean){
			this.setComplete_tax_set(((Boolean)map.get("complete_tax_set"))? "true" : "false");
		}
		if(!(map.get("create_date") instanceof Boolean)&& map.get("create_date")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse((String)map.get("create_date"));
   			this.setCreate_date(new Timestamp(parse.getTime()));
		}
		if(!(map.get("create_uid") instanceof Boolean)&& map.get("create_uid")!=null){
			Object[] objs = (Object[])map.get("create_uid");
			if(objs.length > 0){
				this.setCreate_uid((Integer)objs[0]);
			}
		}
		if(!(map.get("create_uid") instanceof Boolean)&& map.get("create_uid")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("create_uid");
			if(objs.length > 1){
				this.setCreate_uid_text((String)objs[1]);
			}
		}
		if(!(map.get("currency_id") instanceof Boolean)&& map.get("currency_id")!=null){
			Object[] objs = (Object[])map.get("currency_id");
			if(objs.length > 0){
				this.setCurrency_id((Integer)objs[0]);
			}
		}
		if(!(map.get("currency_id") instanceof Boolean)&& map.get("currency_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("currency_id");
			if(objs.length > 1){
				this.setCurrency_id_text((String)objs[1]);
			}
		}
		if(!(map.get("display_name") instanceof Boolean)&& map.get("display_name")!=null){
			this.setDisplay_name((String)map.get("display_name"));
		}
		if(!(map.get("expense_currency_exchange_account_id") instanceof Boolean)&& map.get("expense_currency_exchange_account_id")!=null){
			Object[] objs = (Object[])map.get("expense_currency_exchange_account_id");
			if(objs.length > 0){
				this.setExpense_currency_exchange_account_id((Integer)objs[0]);
			}
		}
		if(!(map.get("expense_currency_exchange_account_id") instanceof Boolean)&& map.get("expense_currency_exchange_account_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("expense_currency_exchange_account_id");
			if(objs.length > 1){
				this.setExpense_currency_exchange_account_id_text((String)objs[1]);
			}
		}
		if(!(map.get("id") instanceof Boolean)&& map.get("id")!=null){
			this.setId((Integer)map.get("id"));
		}
		if(!(map.get("income_currency_exchange_account_id") instanceof Boolean)&& map.get("income_currency_exchange_account_id")!=null){
			Object[] objs = (Object[])map.get("income_currency_exchange_account_id");
			if(objs.length > 0){
				this.setIncome_currency_exchange_account_id((Integer)objs[0]);
			}
		}
		if(!(map.get("income_currency_exchange_account_id") instanceof Boolean)&& map.get("income_currency_exchange_account_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("income_currency_exchange_account_id");
			if(objs.length > 1){
				this.setIncome_currency_exchange_account_id_text((String)objs[1]);
			}
		}
		if(!(map.get("name") instanceof Boolean)&& map.get("name")!=null){
			this.setName((String)map.get("name"));
		}
		if(!(map.get("parent_id") instanceof Boolean)&& map.get("parent_id")!=null){
			Object[] objs = (Object[])map.get("parent_id");
			if(objs.length > 0){
				this.setParent_id((Integer)objs[0]);
			}
		}
		if(!(map.get("parent_id") instanceof Boolean)&& map.get("parent_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("parent_id");
			if(objs.length > 1){
				this.setParent_id_text((String)objs[1]);
			}
		}
		if(!(map.get("property_account_expense_categ_id") instanceof Boolean)&& map.get("property_account_expense_categ_id")!=null){
			Object[] objs = (Object[])map.get("property_account_expense_categ_id");
			if(objs.length > 0){
				this.setProperty_account_expense_categ_id((Integer)objs[0]);
			}
		}
		if(!(map.get("property_account_expense_categ_id") instanceof Boolean)&& map.get("property_account_expense_categ_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("property_account_expense_categ_id");
			if(objs.length > 1){
				this.setProperty_account_expense_categ_id_text((String)objs[1]);
			}
		}
		if(!(map.get("property_account_expense_id") instanceof Boolean)&& map.get("property_account_expense_id")!=null){
			Object[] objs = (Object[])map.get("property_account_expense_id");
			if(objs.length > 0){
				this.setProperty_account_expense_id((Integer)objs[0]);
			}
		}
		if(!(map.get("property_account_expense_id") instanceof Boolean)&& map.get("property_account_expense_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("property_account_expense_id");
			if(objs.length > 1){
				this.setProperty_account_expense_id_text((String)objs[1]);
			}
		}
		if(!(map.get("property_account_income_categ_id") instanceof Boolean)&& map.get("property_account_income_categ_id")!=null){
			Object[] objs = (Object[])map.get("property_account_income_categ_id");
			if(objs.length > 0){
				this.setProperty_account_income_categ_id((Integer)objs[0]);
			}
		}
		if(!(map.get("property_account_income_categ_id") instanceof Boolean)&& map.get("property_account_income_categ_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("property_account_income_categ_id");
			if(objs.length > 1){
				this.setProperty_account_income_categ_id_text((String)objs[1]);
			}
		}
		if(!(map.get("property_account_income_id") instanceof Boolean)&& map.get("property_account_income_id")!=null){
			Object[] objs = (Object[])map.get("property_account_income_id");
			if(objs.length > 0){
				this.setProperty_account_income_id((Integer)objs[0]);
			}
		}
		if(!(map.get("property_account_income_id") instanceof Boolean)&& map.get("property_account_income_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("property_account_income_id");
			if(objs.length > 1){
				this.setProperty_account_income_id_text((String)objs[1]);
			}
		}
		if(!(map.get("property_account_payable_id") instanceof Boolean)&& map.get("property_account_payable_id")!=null){
			Object[] objs = (Object[])map.get("property_account_payable_id");
			if(objs.length > 0){
				this.setProperty_account_payable_id((Integer)objs[0]);
			}
		}
		if(!(map.get("property_account_payable_id") instanceof Boolean)&& map.get("property_account_payable_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("property_account_payable_id");
			if(objs.length > 1){
				this.setProperty_account_payable_id_text((String)objs[1]);
			}
		}
		if(!(map.get("property_account_receivable_id") instanceof Boolean)&& map.get("property_account_receivable_id")!=null){
			Object[] objs = (Object[])map.get("property_account_receivable_id");
			if(objs.length > 0){
				this.setProperty_account_receivable_id((Integer)objs[0]);
			}
		}
		if(!(map.get("property_account_receivable_id") instanceof Boolean)&& map.get("property_account_receivable_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("property_account_receivable_id");
			if(objs.length > 1){
				this.setProperty_account_receivable_id_text((String)objs[1]);
			}
		}
		if(!(map.get("property_stock_account_input_categ_id") instanceof Boolean)&& map.get("property_stock_account_input_categ_id")!=null){
			Object[] objs = (Object[])map.get("property_stock_account_input_categ_id");
			if(objs.length > 0){
				this.setProperty_stock_account_input_categ_id((Integer)objs[0]);
			}
		}
		if(!(map.get("property_stock_account_input_categ_id") instanceof Boolean)&& map.get("property_stock_account_input_categ_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("property_stock_account_input_categ_id");
			if(objs.length > 1){
				this.setProperty_stock_account_input_categ_id_text((String)objs[1]);
			}
		}
		if(!(map.get("property_stock_account_output_categ_id") instanceof Boolean)&& map.get("property_stock_account_output_categ_id")!=null){
			Object[] objs = (Object[])map.get("property_stock_account_output_categ_id");
			if(objs.length > 0){
				this.setProperty_stock_account_output_categ_id((Integer)objs[0]);
			}
		}
		if(!(map.get("property_stock_account_output_categ_id") instanceof Boolean)&& map.get("property_stock_account_output_categ_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("property_stock_account_output_categ_id");
			if(objs.length > 1){
				this.setProperty_stock_account_output_categ_id_text((String)objs[1]);
			}
		}
		if(!(map.get("property_stock_valuation_account_id") instanceof Boolean)&& map.get("property_stock_valuation_account_id")!=null){
			Object[] objs = (Object[])map.get("property_stock_valuation_account_id");
			if(objs.length > 0){
				this.setProperty_stock_valuation_account_id((Integer)objs[0]);
			}
		}
		if(!(map.get("property_stock_valuation_account_id") instanceof Boolean)&& map.get("property_stock_valuation_account_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("property_stock_valuation_account_id");
			if(objs.length > 1){
				this.setProperty_stock_valuation_account_id_text((String)objs[1]);
			}
		}
		if(!(map.get("spoken_languages") instanceof Boolean)&& map.get("spoken_languages")!=null){
			this.setSpoken_languages((String)map.get("spoken_languages"));
		}
		if(!(map.get("tax_template_ids") instanceof Boolean)&& map.get("tax_template_ids")!=null){
			Object[] objs = (Object[])map.get("tax_template_ids");
			if(objs.length > 0){
				Integer[] tax_template_ids = Arrays.copyOfRange(objs,0,objs.length,Integer[].class);
				this.setTax_template_ids(Arrays.toString(tax_template_ids));
			}
		}
		if(!(map.get("transfer_account_code_prefix") instanceof Boolean)&& map.get("transfer_account_code_prefix")!=null){
			this.setTransfer_account_code_prefix((String)map.get("transfer_account_code_prefix"));
		}
		if(map.get("use_anglo_saxon") instanceof Boolean){
			this.setUse_anglo_saxon(((Boolean)map.get("use_anglo_saxon"))? "true" : "false");
		}
		if(map.get("visible") instanceof Boolean){
			this.setVisible(((Boolean)map.get("visible"))? "true" : "false");
		}
		if(!(map.get("write_date") instanceof Boolean)&& map.get("write_date")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse((String)map.get("write_date"));
   			this.setWrite_date(new Timestamp(parse.getTime()));
		}
		if(!(map.get("write_uid") instanceof Boolean)&& map.get("write_uid")!=null){
			Object[] objs = (Object[])map.get("write_uid");
			if(objs.length > 0){
				this.setWrite_uid((Integer)objs[0]);
			}
		}
		if(!(map.get("write_uid") instanceof Boolean)&& map.get("write_uid")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("write_uid");
			if(objs.length > 1){
				this.setWrite_uid_text((String)objs[1]);
			}
		}
		if(!(map.get("__last_update") instanceof Boolean)&& map.get("__last_update")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse((String)map.get("__last_update"));
   			this.set__last_update(new Timestamp(parse.getTime()));
		}
	}

	public Map<String, Object> toMap() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		if(this.getAccount_ids()!=null&&this.getAccount_idsDirtyFlag()){
			map.put("account_ids",this.getAccount_ids());
		}else if(this.getAccount_idsDirtyFlag()){
			map.put("account_ids",false);
		}
		if(this.getBank_account_code_prefix()!=null&&this.getBank_account_code_prefixDirtyFlag()){
			map.put("bank_account_code_prefix",this.getBank_account_code_prefix());
		}else if(this.getBank_account_code_prefixDirtyFlag()){
			map.put("bank_account_code_prefix",false);
		}
		if(this.getCash_account_code_prefix()!=null&&this.getCash_account_code_prefixDirtyFlag()){
			map.put("cash_account_code_prefix",this.getCash_account_code_prefix());
		}else if(this.getCash_account_code_prefixDirtyFlag()){
			map.put("cash_account_code_prefix",false);
		}
		if(this.getCode_digits()!=null&&this.getCode_digitsDirtyFlag()){
			map.put("code_digits",this.getCode_digits());
		}else if(this.getCode_digitsDirtyFlag()){
			map.put("code_digits",false);
		}
		if(this.getComplete_tax_set()!=null&&this.getComplete_tax_setDirtyFlag()){
			map.put("complete_tax_set",Boolean.parseBoolean(this.getComplete_tax_set()));		
		}		if(this.getCreate_date()!=null&&this.getCreate_dateDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String datetimeStr = sdf.format(this.getCreate_date());
			map.put("create_date",datetimeStr);
		}else if(this.getCreate_dateDirtyFlag()){
			map.put("create_date",false);
		}
		if(this.getCreate_uid()!=null&&this.getCreate_uidDirtyFlag()){
			map.put("create_uid",this.getCreate_uid());
		}else if(this.getCreate_uidDirtyFlag()){
			map.put("create_uid",false);
		}
		if(this.getCreate_uid_text()!=null&&this.getCreate_uid_textDirtyFlag()){
			//忽略文本外键create_uid_text
		}else if(this.getCreate_uid_textDirtyFlag()){
			map.put("create_uid",false);
		}
		if(this.getCurrency_id()!=null&&this.getCurrency_idDirtyFlag()){
			map.put("currency_id",this.getCurrency_id());
		}else if(this.getCurrency_idDirtyFlag()){
			map.put("currency_id",false);
		}
		if(this.getCurrency_id_text()!=null&&this.getCurrency_id_textDirtyFlag()){
			//忽略文本外键currency_id_text
		}else if(this.getCurrency_id_textDirtyFlag()){
			map.put("currency_id",false);
		}
		if(this.getDisplay_name()!=null&&this.getDisplay_nameDirtyFlag()){
			map.put("display_name",this.getDisplay_name());
		}else if(this.getDisplay_nameDirtyFlag()){
			map.put("display_name",false);
		}
		if(this.getExpense_currency_exchange_account_id()!=null&&this.getExpense_currency_exchange_account_idDirtyFlag()){
			map.put("expense_currency_exchange_account_id",this.getExpense_currency_exchange_account_id());
		}else if(this.getExpense_currency_exchange_account_idDirtyFlag()){
			map.put("expense_currency_exchange_account_id",false);
		}
		if(this.getExpense_currency_exchange_account_id_text()!=null&&this.getExpense_currency_exchange_account_id_textDirtyFlag()){
			//忽略文本外键expense_currency_exchange_account_id_text
		}else if(this.getExpense_currency_exchange_account_id_textDirtyFlag()){
			map.put("expense_currency_exchange_account_id",false);
		}
		if(this.getId()!=null&&this.getIdDirtyFlag()){
			map.put("id",this.getId());
		}else if(this.getIdDirtyFlag()){
			map.put("id",false);
		}
		if(this.getIncome_currency_exchange_account_id()!=null&&this.getIncome_currency_exchange_account_idDirtyFlag()){
			map.put("income_currency_exchange_account_id",this.getIncome_currency_exchange_account_id());
		}else if(this.getIncome_currency_exchange_account_idDirtyFlag()){
			map.put("income_currency_exchange_account_id",false);
		}
		if(this.getIncome_currency_exchange_account_id_text()!=null&&this.getIncome_currency_exchange_account_id_textDirtyFlag()){
			//忽略文本外键income_currency_exchange_account_id_text
		}else if(this.getIncome_currency_exchange_account_id_textDirtyFlag()){
			map.put("income_currency_exchange_account_id",false);
		}
		if(this.getName()!=null&&this.getNameDirtyFlag()){
			map.put("name",this.getName());
		}else if(this.getNameDirtyFlag()){
			map.put("name",false);
		}
		if(this.getParent_id()!=null&&this.getParent_idDirtyFlag()){
			map.put("parent_id",this.getParent_id());
		}else if(this.getParent_idDirtyFlag()){
			map.put("parent_id",false);
		}
		if(this.getParent_id_text()!=null&&this.getParent_id_textDirtyFlag()){
			//忽略文本外键parent_id_text
		}else if(this.getParent_id_textDirtyFlag()){
			map.put("parent_id",false);
		}
		if(this.getProperty_account_expense_categ_id()!=null&&this.getProperty_account_expense_categ_idDirtyFlag()){
			map.put("property_account_expense_categ_id",this.getProperty_account_expense_categ_id());
		}else if(this.getProperty_account_expense_categ_idDirtyFlag()){
			map.put("property_account_expense_categ_id",false);
		}
		if(this.getProperty_account_expense_categ_id_text()!=null&&this.getProperty_account_expense_categ_id_textDirtyFlag()){
			//忽略文本外键property_account_expense_categ_id_text
		}else if(this.getProperty_account_expense_categ_id_textDirtyFlag()){
			map.put("property_account_expense_categ_id",false);
		}
		if(this.getProperty_account_expense_id()!=null&&this.getProperty_account_expense_idDirtyFlag()){
			map.put("property_account_expense_id",this.getProperty_account_expense_id());
		}else if(this.getProperty_account_expense_idDirtyFlag()){
			map.put("property_account_expense_id",false);
		}
		if(this.getProperty_account_expense_id_text()!=null&&this.getProperty_account_expense_id_textDirtyFlag()){
			//忽略文本外键property_account_expense_id_text
		}else if(this.getProperty_account_expense_id_textDirtyFlag()){
			map.put("property_account_expense_id",false);
		}
		if(this.getProperty_account_income_categ_id()!=null&&this.getProperty_account_income_categ_idDirtyFlag()){
			map.put("property_account_income_categ_id",this.getProperty_account_income_categ_id());
		}else if(this.getProperty_account_income_categ_idDirtyFlag()){
			map.put("property_account_income_categ_id",false);
		}
		if(this.getProperty_account_income_categ_id_text()!=null&&this.getProperty_account_income_categ_id_textDirtyFlag()){
			//忽略文本外键property_account_income_categ_id_text
		}else if(this.getProperty_account_income_categ_id_textDirtyFlag()){
			map.put("property_account_income_categ_id",false);
		}
		if(this.getProperty_account_income_id()!=null&&this.getProperty_account_income_idDirtyFlag()){
			map.put("property_account_income_id",this.getProperty_account_income_id());
		}else if(this.getProperty_account_income_idDirtyFlag()){
			map.put("property_account_income_id",false);
		}
		if(this.getProperty_account_income_id_text()!=null&&this.getProperty_account_income_id_textDirtyFlag()){
			//忽略文本外键property_account_income_id_text
		}else if(this.getProperty_account_income_id_textDirtyFlag()){
			map.put("property_account_income_id",false);
		}
		if(this.getProperty_account_payable_id()!=null&&this.getProperty_account_payable_idDirtyFlag()){
			map.put("property_account_payable_id",this.getProperty_account_payable_id());
		}else if(this.getProperty_account_payable_idDirtyFlag()){
			map.put("property_account_payable_id",false);
		}
		if(this.getProperty_account_payable_id_text()!=null&&this.getProperty_account_payable_id_textDirtyFlag()){
			//忽略文本外键property_account_payable_id_text
		}else if(this.getProperty_account_payable_id_textDirtyFlag()){
			map.put("property_account_payable_id",false);
		}
		if(this.getProperty_account_receivable_id()!=null&&this.getProperty_account_receivable_idDirtyFlag()){
			map.put("property_account_receivable_id",this.getProperty_account_receivable_id());
		}else if(this.getProperty_account_receivable_idDirtyFlag()){
			map.put("property_account_receivable_id",false);
		}
		if(this.getProperty_account_receivable_id_text()!=null&&this.getProperty_account_receivable_id_textDirtyFlag()){
			//忽略文本外键property_account_receivable_id_text
		}else if(this.getProperty_account_receivable_id_textDirtyFlag()){
			map.put("property_account_receivable_id",false);
		}
		if(this.getProperty_stock_account_input_categ_id()!=null&&this.getProperty_stock_account_input_categ_idDirtyFlag()){
			map.put("property_stock_account_input_categ_id",this.getProperty_stock_account_input_categ_id());
		}else if(this.getProperty_stock_account_input_categ_idDirtyFlag()){
			map.put("property_stock_account_input_categ_id",false);
		}
		if(this.getProperty_stock_account_input_categ_id_text()!=null&&this.getProperty_stock_account_input_categ_id_textDirtyFlag()){
			//忽略文本外键property_stock_account_input_categ_id_text
		}else if(this.getProperty_stock_account_input_categ_id_textDirtyFlag()){
			map.put("property_stock_account_input_categ_id",false);
		}
		if(this.getProperty_stock_account_output_categ_id()!=null&&this.getProperty_stock_account_output_categ_idDirtyFlag()){
			map.put("property_stock_account_output_categ_id",this.getProperty_stock_account_output_categ_id());
		}else if(this.getProperty_stock_account_output_categ_idDirtyFlag()){
			map.put("property_stock_account_output_categ_id",false);
		}
		if(this.getProperty_stock_account_output_categ_id_text()!=null&&this.getProperty_stock_account_output_categ_id_textDirtyFlag()){
			//忽略文本外键property_stock_account_output_categ_id_text
		}else if(this.getProperty_stock_account_output_categ_id_textDirtyFlag()){
			map.put("property_stock_account_output_categ_id",false);
		}
		if(this.getProperty_stock_valuation_account_id()!=null&&this.getProperty_stock_valuation_account_idDirtyFlag()){
			map.put("property_stock_valuation_account_id",this.getProperty_stock_valuation_account_id());
		}else if(this.getProperty_stock_valuation_account_idDirtyFlag()){
			map.put("property_stock_valuation_account_id",false);
		}
		if(this.getProperty_stock_valuation_account_id_text()!=null&&this.getProperty_stock_valuation_account_id_textDirtyFlag()){
			//忽略文本外键property_stock_valuation_account_id_text
		}else if(this.getProperty_stock_valuation_account_id_textDirtyFlag()){
			map.put("property_stock_valuation_account_id",false);
		}
		if(this.getSpoken_languages()!=null&&this.getSpoken_languagesDirtyFlag()){
			map.put("spoken_languages",this.getSpoken_languages());
		}else if(this.getSpoken_languagesDirtyFlag()){
			map.put("spoken_languages",false);
		}
		if(this.getTax_template_ids()!=null&&this.getTax_template_idsDirtyFlag()){
			map.put("tax_template_ids",this.getTax_template_ids());
		}else if(this.getTax_template_idsDirtyFlag()){
			map.put("tax_template_ids",false);
		}
		if(this.getTransfer_account_code_prefix()!=null&&this.getTransfer_account_code_prefixDirtyFlag()){
			map.put("transfer_account_code_prefix",this.getTransfer_account_code_prefix());
		}else if(this.getTransfer_account_code_prefixDirtyFlag()){
			map.put("transfer_account_code_prefix",false);
		}
		if(this.getUse_anglo_saxon()!=null&&this.getUse_anglo_saxonDirtyFlag()){
			map.put("use_anglo_saxon",Boolean.parseBoolean(this.getUse_anglo_saxon()));		
		}		if(this.getVisible()!=null&&this.getVisibleDirtyFlag()){
			map.put("visible",Boolean.parseBoolean(this.getVisible()));		
		}		if(this.getWrite_date()!=null&&this.getWrite_dateDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String datetimeStr = sdf.format(this.getWrite_date());
			map.put("write_date",datetimeStr);
		}else if(this.getWrite_dateDirtyFlag()){
			map.put("write_date",false);
		}
		if(this.getWrite_uid()!=null&&this.getWrite_uidDirtyFlag()){
			map.put("write_uid",this.getWrite_uid());
		}else if(this.getWrite_uidDirtyFlag()){
			map.put("write_uid",false);
		}
		if(this.getWrite_uid_text()!=null&&this.getWrite_uid_textDirtyFlag()){
			//忽略文本外键write_uid_text
		}else if(this.getWrite_uid_textDirtyFlag()){
			map.put("write_uid",false);
		}
		if(this.get__last_update()!=null&&this.get__last_updateDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String datetimeStr = sdf.format(this.get__last_update());
			map.put("__last_update",datetimeStr);
		}else if(this.get__last_updateDirtyFlag()){
			map.put("__last_update",false);
		}
		return map;
	}

}
