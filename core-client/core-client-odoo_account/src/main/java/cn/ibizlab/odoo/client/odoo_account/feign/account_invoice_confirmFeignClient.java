package cn.ibizlab.odoo.client.odoo_account.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Iaccount_invoice_confirm;
import cn.ibizlab.odoo.client.odoo_account.model.account_invoice_confirmImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[account_invoice_confirm] 服务对象接口
 */
public interface account_invoice_confirmFeignClient {

    @RequestMapping(method = RequestMethod.GET, value = "/odoo_account/account_invoice_confirms/search")
    public Page<account_invoice_confirmImpl> search(SearchContext context);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_account/account_invoice_confirms")
    public account_invoice_confirmImpl create(@RequestBody account_invoice_confirmImpl account_invoice_confirm);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_account/account_invoice_confirms/updatebatch")
    public account_invoice_confirmImpl updateBatch(@RequestBody List<account_invoice_confirmImpl> account_invoice_confirms);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_account/account_invoice_confirms/removebatch")
    public account_invoice_confirmImpl removeBatch(@RequestBody List<account_invoice_confirmImpl> account_invoice_confirms);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_account/account_invoice_confirms/{id}")
    public account_invoice_confirmImpl update(@PathVariable("id") Integer id,@RequestBody account_invoice_confirmImpl account_invoice_confirm);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_account/account_invoice_confirms/createbatch")
    public account_invoice_confirmImpl createBatch(@RequestBody List<account_invoice_confirmImpl> account_invoice_confirms);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_account/account_invoice_confirms/{id}")
    public account_invoice_confirmImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_account/account_invoice_confirms/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_account/account_invoice_confirms/select")
    public Page<account_invoice_confirmImpl> select();



}
