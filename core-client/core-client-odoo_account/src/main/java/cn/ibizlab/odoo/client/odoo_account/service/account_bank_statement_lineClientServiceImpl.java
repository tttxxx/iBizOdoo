package cn.ibizlab.odoo.client.odoo_account.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Iaccount_bank_statement_line;
import cn.ibizlab.odoo.core.client.service.Iaccount_bank_statement_lineClientService;
import cn.ibizlab.odoo.client.odoo_account.model.account_bank_statement_lineImpl;
import cn.ibizlab.odoo.client.odoo_account.odooclient.Iaccount_bank_statement_lineOdooClient;
import cn.ibizlab.odoo.client.odoo_account.odooclient.impl.account_bank_statement_lineOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[account_bank_statement_line] 服务对象接口
 */
@Service
public class account_bank_statement_lineClientServiceImpl implements Iaccount_bank_statement_lineClientService {
    @Autowired
    private  Iaccount_bank_statement_lineOdooClient  account_bank_statement_lineOdooClient;

    public Iaccount_bank_statement_line createModel() {		
		return new account_bank_statement_lineImpl();
	}


        public void remove(Iaccount_bank_statement_line account_bank_statement_line){
this.account_bank_statement_lineOdooClient.remove(account_bank_statement_line) ;
        }
        
        public void updateBatch(List<Iaccount_bank_statement_line> account_bank_statement_lines){
            
        }
        
        public void createBatch(List<Iaccount_bank_statement_line> account_bank_statement_lines){
            
        }
        
        public void get(Iaccount_bank_statement_line account_bank_statement_line){
            this.account_bank_statement_lineOdooClient.get(account_bank_statement_line) ;
        }
        
        public void removeBatch(List<Iaccount_bank_statement_line> account_bank_statement_lines){
            
        }
        
        public Page<Iaccount_bank_statement_line> search(SearchContext context){
            return this.account_bank_statement_lineOdooClient.search(context) ;
        }
        
        public void update(Iaccount_bank_statement_line account_bank_statement_line){
this.account_bank_statement_lineOdooClient.update(account_bank_statement_line) ;
        }
        
        public void create(Iaccount_bank_statement_line account_bank_statement_line){
this.account_bank_statement_lineOdooClient.create(account_bank_statement_line) ;
        }
        
        public Page<Iaccount_bank_statement_line> select(SearchContext context){
            return null ;
        }
        

}

