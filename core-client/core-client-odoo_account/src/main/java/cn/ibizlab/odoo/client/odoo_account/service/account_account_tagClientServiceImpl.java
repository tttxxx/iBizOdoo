package cn.ibizlab.odoo.client.odoo_account.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Iaccount_account_tag;
import cn.ibizlab.odoo.core.client.service.Iaccount_account_tagClientService;
import cn.ibizlab.odoo.client.odoo_account.model.account_account_tagImpl;
import cn.ibizlab.odoo.client.odoo_account.odooclient.Iaccount_account_tagOdooClient;
import cn.ibizlab.odoo.client.odoo_account.odooclient.impl.account_account_tagOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[account_account_tag] 服务对象接口
 */
@Service
public class account_account_tagClientServiceImpl implements Iaccount_account_tagClientService {
    @Autowired
    private  Iaccount_account_tagOdooClient  account_account_tagOdooClient;

    public Iaccount_account_tag createModel() {		
		return new account_account_tagImpl();
	}


        public void removeBatch(List<Iaccount_account_tag> account_account_tags){
            
        }
        
        public void create(Iaccount_account_tag account_account_tag){
this.account_account_tagOdooClient.create(account_account_tag) ;
        }
        
        public void updateBatch(List<Iaccount_account_tag> account_account_tags){
            
        }
        
        public void createBatch(List<Iaccount_account_tag> account_account_tags){
            
        }
        
        public Page<Iaccount_account_tag> search(SearchContext context){
            return this.account_account_tagOdooClient.search(context) ;
        }
        
        public void update(Iaccount_account_tag account_account_tag){
this.account_account_tagOdooClient.update(account_account_tag) ;
        }
        
        public void remove(Iaccount_account_tag account_account_tag){
this.account_account_tagOdooClient.remove(account_account_tag) ;
        }
        
        public void get(Iaccount_account_tag account_account_tag){
            this.account_account_tagOdooClient.get(account_account_tag) ;
        }
        
        public Page<Iaccount_account_tag> select(SearchContext context){
            return null ;
        }
        

}

