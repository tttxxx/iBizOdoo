package cn.ibizlab.odoo.client.odoo_account.model;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.math.BigInteger;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.core.client.model.Iaccount_fiscal_position_template;
import cn.ibizlab.odoo.util.helper.OdooClientHelper;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang.ArrayUtils;
import org.springframework.util.StringUtils;

/**
 * 接口实体[account_fiscal_position_template] 对象
 */
public class account_fiscal_position_templateImpl implements Iaccount_fiscal_position_template,Serializable{

    /**
     * 科目映射
     */
    public String account_ids;

    @JsonIgnore
    public boolean account_idsDirtyFlag;
    
    /**
     * 自动检测
     */
    public String auto_apply;

    @JsonIgnore
    public boolean auto_applyDirtyFlag;
    
    /**
     * 表模板
     */
    public Integer chart_template_id;

    @JsonIgnore
    public boolean chart_template_idDirtyFlag;
    
    /**
     * 表模板
     */
    public String chart_template_id_text;

    @JsonIgnore
    public boolean chart_template_id_textDirtyFlag;
    
    /**
     * 国家群组
     */
    public Integer country_group_id;

    @JsonIgnore
    public boolean country_group_idDirtyFlag;
    
    /**
     * 国家群组
     */
    public String country_group_id_text;

    @JsonIgnore
    public boolean country_group_id_textDirtyFlag;
    
    /**
     * 国家
     */
    public Integer country_id;

    @JsonIgnore
    public boolean country_idDirtyFlag;
    
    /**
     * 国家
     */
    public String country_id_text;

    @JsonIgnore
    public boolean country_id_textDirtyFlag;
    
    /**
     * 创建时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp create_date;

    @JsonIgnore
    public boolean create_dateDirtyFlag;
    
    /**
     * 创建人
     */
    public Integer create_uid;

    @JsonIgnore
    public boolean create_uidDirtyFlag;
    
    /**
     * 创建人
     */
    public String create_uid_text;

    @JsonIgnore
    public boolean create_uid_textDirtyFlag;
    
    /**
     * 显示名称
     */
    public String display_name;

    @JsonIgnore
    public boolean display_nameDirtyFlag;
    
    /**
     * ID
     */
    public Integer id;

    @JsonIgnore
    public boolean idDirtyFlag;
    
    /**
     * 税科目调整模版
     */
    public String name;

    @JsonIgnore
    public boolean nameDirtyFlag;
    
    /**
     * 备注
     */
    public String note;

    @JsonIgnore
    public boolean noteDirtyFlag;
    
    /**
     * 序号
     */
    public Integer sequence;

    @JsonIgnore
    public boolean sequenceDirtyFlag;
    
    /**
     * 联邦政府
     */
    public String state_ids;

    @JsonIgnore
    public boolean state_idsDirtyFlag;
    
    /**
     * 税映射
     */
    public String tax_ids;

    @JsonIgnore
    public boolean tax_idsDirtyFlag;
    
    /**
     * VAT必须
     */
    public String vat_required;

    @JsonIgnore
    public boolean vat_requiredDirtyFlag;
    
    /**
     * 最后更新时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp write_date;

    @JsonIgnore
    public boolean write_dateDirtyFlag;
    
    /**
     * 最后更新人
     */
    public Integer write_uid;

    @JsonIgnore
    public boolean write_uidDirtyFlag;
    
    /**
     * 最后更新人
     */
    public String write_uid_text;

    @JsonIgnore
    public boolean write_uid_textDirtyFlag;
    
    /**
     * 邮编范围从
     */
    public Integer zip_from;

    @JsonIgnore
    public boolean zip_fromDirtyFlag;
    
    /**
     * 邮编范围到
     */
    public Integer zip_to;

    @JsonIgnore
    public boolean zip_toDirtyFlag;
    
    /**
     * 最后修改日
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp __last_update;

    @JsonIgnore
    public boolean __last_updateDirtyFlag;
    
    /**
     * 获取 [科目映射]
     */
    @JsonProperty("account_ids")
    public String getAccount_ids(){
        return this.account_ids ;
    }

    /**
     * 设置 [科目映射]
     */
    @JsonProperty("account_ids")
    public void setAccount_ids(String  account_ids){
        this.account_ids = account_ids ;
        this.account_idsDirtyFlag = true ;
    }

     /**
     * 获取 [科目映射]脏标记
     */
    @JsonIgnore
    public boolean getAccount_idsDirtyFlag(){
        return this.account_idsDirtyFlag ;
    }   

    /**
     * 获取 [自动检测]
     */
    @JsonProperty("auto_apply")
    public String getAuto_apply(){
        return this.auto_apply ;
    }

    /**
     * 设置 [自动检测]
     */
    @JsonProperty("auto_apply")
    public void setAuto_apply(String  auto_apply){
        this.auto_apply = auto_apply ;
        this.auto_applyDirtyFlag = true ;
    }

     /**
     * 获取 [自动检测]脏标记
     */
    @JsonIgnore
    public boolean getAuto_applyDirtyFlag(){
        return this.auto_applyDirtyFlag ;
    }   

    /**
     * 获取 [表模板]
     */
    @JsonProperty("chart_template_id")
    public Integer getChart_template_id(){
        return this.chart_template_id ;
    }

    /**
     * 设置 [表模板]
     */
    @JsonProperty("chart_template_id")
    public void setChart_template_id(Integer  chart_template_id){
        this.chart_template_id = chart_template_id ;
        this.chart_template_idDirtyFlag = true ;
    }

     /**
     * 获取 [表模板]脏标记
     */
    @JsonIgnore
    public boolean getChart_template_idDirtyFlag(){
        return this.chart_template_idDirtyFlag ;
    }   

    /**
     * 获取 [表模板]
     */
    @JsonProperty("chart_template_id_text")
    public String getChart_template_id_text(){
        return this.chart_template_id_text ;
    }

    /**
     * 设置 [表模板]
     */
    @JsonProperty("chart_template_id_text")
    public void setChart_template_id_text(String  chart_template_id_text){
        this.chart_template_id_text = chart_template_id_text ;
        this.chart_template_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [表模板]脏标记
     */
    @JsonIgnore
    public boolean getChart_template_id_textDirtyFlag(){
        return this.chart_template_id_textDirtyFlag ;
    }   

    /**
     * 获取 [国家群组]
     */
    @JsonProperty("country_group_id")
    public Integer getCountry_group_id(){
        return this.country_group_id ;
    }

    /**
     * 设置 [国家群组]
     */
    @JsonProperty("country_group_id")
    public void setCountry_group_id(Integer  country_group_id){
        this.country_group_id = country_group_id ;
        this.country_group_idDirtyFlag = true ;
    }

     /**
     * 获取 [国家群组]脏标记
     */
    @JsonIgnore
    public boolean getCountry_group_idDirtyFlag(){
        return this.country_group_idDirtyFlag ;
    }   

    /**
     * 获取 [国家群组]
     */
    @JsonProperty("country_group_id_text")
    public String getCountry_group_id_text(){
        return this.country_group_id_text ;
    }

    /**
     * 设置 [国家群组]
     */
    @JsonProperty("country_group_id_text")
    public void setCountry_group_id_text(String  country_group_id_text){
        this.country_group_id_text = country_group_id_text ;
        this.country_group_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [国家群组]脏标记
     */
    @JsonIgnore
    public boolean getCountry_group_id_textDirtyFlag(){
        return this.country_group_id_textDirtyFlag ;
    }   

    /**
     * 获取 [国家]
     */
    @JsonProperty("country_id")
    public Integer getCountry_id(){
        return this.country_id ;
    }

    /**
     * 设置 [国家]
     */
    @JsonProperty("country_id")
    public void setCountry_id(Integer  country_id){
        this.country_id = country_id ;
        this.country_idDirtyFlag = true ;
    }

     /**
     * 获取 [国家]脏标记
     */
    @JsonIgnore
    public boolean getCountry_idDirtyFlag(){
        return this.country_idDirtyFlag ;
    }   

    /**
     * 获取 [国家]
     */
    @JsonProperty("country_id_text")
    public String getCountry_id_text(){
        return this.country_id_text ;
    }

    /**
     * 设置 [国家]
     */
    @JsonProperty("country_id_text")
    public void setCountry_id_text(String  country_id_text){
        this.country_id_text = country_id_text ;
        this.country_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [国家]脏标记
     */
    @JsonIgnore
    public boolean getCountry_id_textDirtyFlag(){
        return this.country_id_textDirtyFlag ;
    }   

    /**
     * 获取 [创建时间]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return this.create_date ;
    }

    /**
     * 设置 [创建时间]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

     /**
     * 获取 [创建时间]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return this.create_dateDirtyFlag ;
    }   

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return this.create_uid ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

     /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return this.create_uidDirtyFlag ;
    }   

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return this.create_uid_text ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

     /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return this.create_uid_textDirtyFlag ;
    }   

    /**
     * 获取 [显示名称]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return this.display_name ;
    }

    /**
     * 设置 [显示名称]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

     /**
     * 获取 [显示名称]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return this.display_nameDirtyFlag ;
    }   

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return this.id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

     /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return this.idDirtyFlag ;
    }   

    /**
     * 获取 [税科目调整模版]
     */
    @JsonProperty("name")
    public String getName(){
        return this.name ;
    }

    /**
     * 设置 [税科目调整模版]
     */
    @JsonProperty("name")
    public void setName(String  name){
        this.name = name ;
        this.nameDirtyFlag = true ;
    }

     /**
     * 获取 [税科目调整模版]脏标记
     */
    @JsonIgnore
    public boolean getNameDirtyFlag(){
        return this.nameDirtyFlag ;
    }   

    /**
     * 获取 [备注]
     */
    @JsonProperty("note")
    public String getNote(){
        return this.note ;
    }

    /**
     * 设置 [备注]
     */
    @JsonProperty("note")
    public void setNote(String  note){
        this.note = note ;
        this.noteDirtyFlag = true ;
    }

     /**
     * 获取 [备注]脏标记
     */
    @JsonIgnore
    public boolean getNoteDirtyFlag(){
        return this.noteDirtyFlag ;
    }   

    /**
     * 获取 [序号]
     */
    @JsonProperty("sequence")
    public Integer getSequence(){
        return this.sequence ;
    }

    /**
     * 设置 [序号]
     */
    @JsonProperty("sequence")
    public void setSequence(Integer  sequence){
        this.sequence = sequence ;
        this.sequenceDirtyFlag = true ;
    }

     /**
     * 获取 [序号]脏标记
     */
    @JsonIgnore
    public boolean getSequenceDirtyFlag(){
        return this.sequenceDirtyFlag ;
    }   

    /**
     * 获取 [联邦政府]
     */
    @JsonProperty("state_ids")
    public String getState_ids(){
        return this.state_ids ;
    }

    /**
     * 设置 [联邦政府]
     */
    @JsonProperty("state_ids")
    public void setState_ids(String  state_ids){
        this.state_ids = state_ids ;
        this.state_idsDirtyFlag = true ;
    }

     /**
     * 获取 [联邦政府]脏标记
     */
    @JsonIgnore
    public boolean getState_idsDirtyFlag(){
        return this.state_idsDirtyFlag ;
    }   

    /**
     * 获取 [税映射]
     */
    @JsonProperty("tax_ids")
    public String getTax_ids(){
        return this.tax_ids ;
    }

    /**
     * 设置 [税映射]
     */
    @JsonProperty("tax_ids")
    public void setTax_ids(String  tax_ids){
        this.tax_ids = tax_ids ;
        this.tax_idsDirtyFlag = true ;
    }

     /**
     * 获取 [税映射]脏标记
     */
    @JsonIgnore
    public boolean getTax_idsDirtyFlag(){
        return this.tax_idsDirtyFlag ;
    }   

    /**
     * 获取 [VAT必须]
     */
    @JsonProperty("vat_required")
    public String getVat_required(){
        return this.vat_required ;
    }

    /**
     * 设置 [VAT必须]
     */
    @JsonProperty("vat_required")
    public void setVat_required(String  vat_required){
        this.vat_required = vat_required ;
        this.vat_requiredDirtyFlag = true ;
    }

     /**
     * 获取 [VAT必须]脏标记
     */
    @JsonIgnore
    public boolean getVat_requiredDirtyFlag(){
        return this.vat_requiredDirtyFlag ;
    }   

    /**
     * 获取 [最后更新时间]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return this.write_date ;
    }

    /**
     * 设置 [最后更新时间]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新时间]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return this.write_dateDirtyFlag ;
    }   

    /**
     * 获取 [最后更新人]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return this.write_uid ;
    }

    /**
     * 设置 [最后更新人]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新人]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return this.write_uidDirtyFlag ;
    }   

    /**
     * 获取 [最后更新人]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return this.write_uid_text ;
    }

    /**
     * 设置 [最后更新人]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新人]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return this.write_uid_textDirtyFlag ;
    }   

    /**
     * 获取 [邮编范围从]
     */
    @JsonProperty("zip_from")
    public Integer getZip_from(){
        return this.zip_from ;
    }

    /**
     * 设置 [邮编范围从]
     */
    @JsonProperty("zip_from")
    public void setZip_from(Integer  zip_from){
        this.zip_from = zip_from ;
        this.zip_fromDirtyFlag = true ;
    }

     /**
     * 获取 [邮编范围从]脏标记
     */
    @JsonIgnore
    public boolean getZip_fromDirtyFlag(){
        return this.zip_fromDirtyFlag ;
    }   

    /**
     * 获取 [邮编范围到]
     */
    @JsonProperty("zip_to")
    public Integer getZip_to(){
        return this.zip_to ;
    }

    /**
     * 设置 [邮编范围到]
     */
    @JsonProperty("zip_to")
    public void setZip_to(Integer  zip_to){
        this.zip_to = zip_to ;
        this.zip_toDirtyFlag = true ;
    }

     /**
     * 获取 [邮编范围到]脏标记
     */
    @JsonIgnore
    public boolean getZip_toDirtyFlag(){
        return this.zip_toDirtyFlag ;
    }   

    /**
     * 获取 [最后修改日]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return this.__last_update ;
    }

    /**
     * 设置 [最后修改日]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

     /**
     * 获取 [最后修改日]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return this.__last_updateDirtyFlag ;
    }   



    public void fromMap(Map<String, Object> map) throws Exception {
		if(!(map.get("account_ids") instanceof Boolean)&& map.get("account_ids")!=null){
			Object[] objs = (Object[])map.get("account_ids");
			if(objs.length > 0){
				Integer[] account_ids = Arrays.copyOfRange(objs,0,objs.length,Integer[].class);
				this.setAccount_ids(Arrays.toString(account_ids));
			}
		}
		if(map.get("auto_apply") instanceof Boolean){
			this.setAuto_apply(((Boolean)map.get("auto_apply"))? "true" : "false");
		}
		if(!(map.get("chart_template_id") instanceof Boolean)&& map.get("chart_template_id")!=null){
			Object[] objs = (Object[])map.get("chart_template_id");
			if(objs.length > 0){
				this.setChart_template_id((Integer)objs[0]);
			}
		}
		if(!(map.get("chart_template_id") instanceof Boolean)&& map.get("chart_template_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("chart_template_id");
			if(objs.length > 1){
				this.setChart_template_id_text((String)objs[1]);
			}
		}
		if(!(map.get("country_group_id") instanceof Boolean)&& map.get("country_group_id")!=null){
			Object[] objs = (Object[])map.get("country_group_id");
			if(objs.length > 0){
				this.setCountry_group_id((Integer)objs[0]);
			}
		}
		if(!(map.get("country_group_id") instanceof Boolean)&& map.get("country_group_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("country_group_id");
			if(objs.length > 1){
				this.setCountry_group_id_text((String)objs[1]);
			}
		}
		if(!(map.get("country_id") instanceof Boolean)&& map.get("country_id")!=null){
			Object[] objs = (Object[])map.get("country_id");
			if(objs.length > 0){
				this.setCountry_id((Integer)objs[0]);
			}
		}
		if(!(map.get("country_id") instanceof Boolean)&& map.get("country_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("country_id");
			if(objs.length > 1){
				this.setCountry_id_text((String)objs[1]);
			}
		}
		if(!(map.get("create_date") instanceof Boolean)&& map.get("create_date")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse((String)map.get("create_date"));
   			this.setCreate_date(new Timestamp(parse.getTime()));
		}
		if(!(map.get("create_uid") instanceof Boolean)&& map.get("create_uid")!=null){
			Object[] objs = (Object[])map.get("create_uid");
			if(objs.length > 0){
				this.setCreate_uid((Integer)objs[0]);
			}
		}
		if(!(map.get("create_uid") instanceof Boolean)&& map.get("create_uid")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("create_uid");
			if(objs.length > 1){
				this.setCreate_uid_text((String)objs[1]);
			}
		}
		if(!(map.get("display_name") instanceof Boolean)&& map.get("display_name")!=null){
			this.setDisplay_name((String)map.get("display_name"));
		}
		if(!(map.get("id") instanceof Boolean)&& map.get("id")!=null){
			this.setId((Integer)map.get("id"));
		}
		if(!(map.get("name") instanceof Boolean)&& map.get("name")!=null){
			this.setName((String)map.get("name"));
		}
		if(!(map.get("note") instanceof Boolean)&& map.get("note")!=null){
			this.setNote((String)map.get("note"));
		}
		if(!(map.get("sequence") instanceof Boolean)&& map.get("sequence")!=null){
			this.setSequence((Integer)map.get("sequence"));
		}
		if(!(map.get("state_ids") instanceof Boolean)&& map.get("state_ids")!=null){
			Object[] objs = (Object[])map.get("state_ids");
			if(objs.length > 0){
				Integer[] state_ids = Arrays.copyOfRange(objs,0,objs.length,Integer[].class);
				this.setState_ids(Arrays.toString(state_ids));
			}
		}
		if(!(map.get("tax_ids") instanceof Boolean)&& map.get("tax_ids")!=null){
			Object[] objs = (Object[])map.get("tax_ids");
			if(objs.length > 0){
				Integer[] tax_ids = Arrays.copyOfRange(objs,0,objs.length,Integer[].class);
				this.setTax_ids(Arrays.toString(tax_ids));
			}
		}
		if(map.get("vat_required") instanceof Boolean){
			this.setVat_required(((Boolean)map.get("vat_required"))? "true" : "false");
		}
		if(!(map.get("write_date") instanceof Boolean)&& map.get("write_date")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse((String)map.get("write_date"));
   			this.setWrite_date(new Timestamp(parse.getTime()));
		}
		if(!(map.get("write_uid") instanceof Boolean)&& map.get("write_uid")!=null){
			Object[] objs = (Object[])map.get("write_uid");
			if(objs.length > 0){
				this.setWrite_uid((Integer)objs[0]);
			}
		}
		if(!(map.get("write_uid") instanceof Boolean)&& map.get("write_uid")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("write_uid");
			if(objs.length > 1){
				this.setWrite_uid_text((String)objs[1]);
			}
		}
		if(!(map.get("zip_from") instanceof Boolean)&& map.get("zip_from")!=null){
			this.setZip_from((Integer)map.get("zip_from"));
		}
		if(!(map.get("zip_to") instanceof Boolean)&& map.get("zip_to")!=null){
			this.setZip_to((Integer)map.get("zip_to"));
		}
		if(!(map.get("__last_update") instanceof Boolean)&& map.get("__last_update")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse((String)map.get("__last_update"));
   			this.set__last_update(new Timestamp(parse.getTime()));
		}
	}

	public Map<String, Object> toMap() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		if(this.getAccount_ids()!=null&&this.getAccount_idsDirtyFlag()){
			map.put("account_ids",this.getAccount_ids());
		}else if(this.getAccount_idsDirtyFlag()){
			map.put("account_ids",false);
		}
		if(this.getAuto_apply()!=null&&this.getAuto_applyDirtyFlag()){
			map.put("auto_apply",Boolean.parseBoolean(this.getAuto_apply()));		
		}		if(this.getChart_template_id()!=null&&this.getChart_template_idDirtyFlag()){
			map.put("chart_template_id",this.getChart_template_id());
		}else if(this.getChart_template_idDirtyFlag()){
			map.put("chart_template_id",false);
		}
		if(this.getChart_template_id_text()!=null&&this.getChart_template_id_textDirtyFlag()){
			//忽略文本外键chart_template_id_text
		}else if(this.getChart_template_id_textDirtyFlag()){
			map.put("chart_template_id",false);
		}
		if(this.getCountry_group_id()!=null&&this.getCountry_group_idDirtyFlag()){
			map.put("country_group_id",this.getCountry_group_id());
		}else if(this.getCountry_group_idDirtyFlag()){
			map.put("country_group_id",false);
		}
		if(this.getCountry_group_id_text()!=null&&this.getCountry_group_id_textDirtyFlag()){
			//忽略文本外键country_group_id_text
		}else if(this.getCountry_group_id_textDirtyFlag()){
			map.put("country_group_id",false);
		}
		if(this.getCountry_id()!=null&&this.getCountry_idDirtyFlag()){
			map.put("country_id",this.getCountry_id());
		}else if(this.getCountry_idDirtyFlag()){
			map.put("country_id",false);
		}
		if(this.getCountry_id_text()!=null&&this.getCountry_id_textDirtyFlag()){
			//忽略文本外键country_id_text
		}else if(this.getCountry_id_textDirtyFlag()){
			map.put("country_id",false);
		}
		if(this.getCreate_date()!=null&&this.getCreate_dateDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String datetimeStr = sdf.format(this.getCreate_date());
			map.put("create_date",datetimeStr);
		}else if(this.getCreate_dateDirtyFlag()){
			map.put("create_date",false);
		}
		if(this.getCreate_uid()!=null&&this.getCreate_uidDirtyFlag()){
			map.put("create_uid",this.getCreate_uid());
		}else if(this.getCreate_uidDirtyFlag()){
			map.put("create_uid",false);
		}
		if(this.getCreate_uid_text()!=null&&this.getCreate_uid_textDirtyFlag()){
			//忽略文本外键create_uid_text
		}else if(this.getCreate_uid_textDirtyFlag()){
			map.put("create_uid",false);
		}
		if(this.getDisplay_name()!=null&&this.getDisplay_nameDirtyFlag()){
			map.put("display_name",this.getDisplay_name());
		}else if(this.getDisplay_nameDirtyFlag()){
			map.put("display_name",false);
		}
		if(this.getId()!=null&&this.getIdDirtyFlag()){
			map.put("id",this.getId());
		}else if(this.getIdDirtyFlag()){
			map.put("id",false);
		}
		if(this.getName()!=null&&this.getNameDirtyFlag()){
			map.put("name",this.getName());
		}else if(this.getNameDirtyFlag()){
			map.put("name",false);
		}
		if(this.getNote()!=null&&this.getNoteDirtyFlag()){
			map.put("note",this.getNote());
		}else if(this.getNoteDirtyFlag()){
			map.put("note",false);
		}
		if(this.getSequence()!=null&&this.getSequenceDirtyFlag()){
			map.put("sequence",this.getSequence());
		}else if(this.getSequenceDirtyFlag()){
			map.put("sequence",false);
		}
		if(this.getState_ids()!=null&&this.getState_idsDirtyFlag()){
			map.put("state_ids",this.getState_ids());
		}else if(this.getState_idsDirtyFlag()){
			map.put("state_ids",false);
		}
		if(this.getTax_ids()!=null&&this.getTax_idsDirtyFlag()){
			map.put("tax_ids",this.getTax_ids());
		}else if(this.getTax_idsDirtyFlag()){
			map.put("tax_ids",false);
		}
		if(this.getVat_required()!=null&&this.getVat_requiredDirtyFlag()){
			map.put("vat_required",Boolean.parseBoolean(this.getVat_required()));		
		}		if(this.getWrite_date()!=null&&this.getWrite_dateDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String datetimeStr = sdf.format(this.getWrite_date());
			map.put("write_date",datetimeStr);
		}else if(this.getWrite_dateDirtyFlag()){
			map.put("write_date",false);
		}
		if(this.getWrite_uid()!=null&&this.getWrite_uidDirtyFlag()){
			map.put("write_uid",this.getWrite_uid());
		}else if(this.getWrite_uidDirtyFlag()){
			map.put("write_uid",false);
		}
		if(this.getWrite_uid_text()!=null&&this.getWrite_uid_textDirtyFlag()){
			//忽略文本外键write_uid_text
		}else if(this.getWrite_uid_textDirtyFlag()){
			map.put("write_uid",false);
		}
		if(this.getZip_from()!=null&&this.getZip_fromDirtyFlag()){
			map.put("zip_from",this.getZip_from());
		}else if(this.getZip_fromDirtyFlag()){
			map.put("zip_from",false);
		}
		if(this.getZip_to()!=null&&this.getZip_toDirtyFlag()){
			map.put("zip_to",this.getZip_to());
		}else if(this.getZip_toDirtyFlag()){
			map.put("zip_to",false);
		}
		if(this.get__last_update()!=null&&this.get__last_updateDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String datetimeStr = sdf.format(this.get__last_update());
			map.put("__last_update",datetimeStr);
		}else if(this.get__last_updateDirtyFlag()){
			map.put("__last_update",false);
		}
		return map;
	}

}
