package cn.ibizlab.odoo.client.odoo_account.odooclient;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.core.client.model.Iaccount_payment;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[account_payment] 服务对象客户端接口
 */
public interface Iaccount_paymentOdooClient {
    
        public Page<Iaccount_payment> search(SearchContext context);

        public void updateBatch(Iaccount_payment account_payment);

        public void create(Iaccount_payment account_payment);

        public void update(Iaccount_payment account_payment);

        public void remove(Iaccount_payment account_payment);

        public void createBatch(Iaccount_payment account_payment);

        public void removeBatch(Iaccount_payment account_payment);

        public void get(Iaccount_payment account_payment);

        public List<Iaccount_payment> select();


}