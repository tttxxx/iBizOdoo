package cn.ibizlab.odoo.client.odoo_account.odooclient;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.core.client.model.Iaccount_financial_year_op;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[account_financial_year_op] 服务对象客户端接口
 */
public interface Iaccount_financial_year_opOdooClient {
    
        public void remove(Iaccount_financial_year_op account_financial_year_op);

        public void removeBatch(Iaccount_financial_year_op account_financial_year_op);

        public void updateBatch(Iaccount_financial_year_op account_financial_year_op);

        public void createBatch(Iaccount_financial_year_op account_financial_year_op);

        public Page<Iaccount_financial_year_op> search(SearchContext context);

        public void get(Iaccount_financial_year_op account_financial_year_op);

        public void update(Iaccount_financial_year_op account_financial_year_op);

        public void create(Iaccount_financial_year_op account_financial_year_op);

        public List<Iaccount_financial_year_op> select();


}