package cn.ibizlab.odoo.client.odoo_account.odooclient;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.core.client.model.Iaccount_print_journal;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[account_print_journal] 服务对象客户端接口
 */
public interface Iaccount_print_journalOdooClient {
    
        public void create(Iaccount_print_journal account_print_journal);

        public void createBatch(Iaccount_print_journal account_print_journal);

        public void update(Iaccount_print_journal account_print_journal);

        public void remove(Iaccount_print_journal account_print_journal);

        public void removeBatch(Iaccount_print_journal account_print_journal);

        public Page<Iaccount_print_journal> search(SearchContext context);

        public void get(Iaccount_print_journal account_print_journal);

        public void updateBatch(Iaccount_print_journal account_print_journal);

        public List<Iaccount_print_journal> select();


}