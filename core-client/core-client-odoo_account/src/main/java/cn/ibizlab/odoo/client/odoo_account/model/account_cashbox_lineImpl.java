package cn.ibizlab.odoo.client.odoo_account.model;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.math.BigInteger;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.core.client.model.Iaccount_cashbox_line;
import cn.ibizlab.odoo.util.helper.OdooClientHelper;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang.ArrayUtils;
import org.springframework.util.StringUtils;

/**
 * 接口实体[account_cashbox_line] 对象
 */
public class account_cashbox_lineImpl implements Iaccount_cashbox_line,Serializable{

    /**
     * 钱箱
     */
    public Integer cashbox_id;

    @JsonIgnore
    public boolean cashbox_idDirtyFlag;
    
    /**
     * 硬币／账单　价值
     */
    public Double coin_value;

    @JsonIgnore
    public boolean coin_valueDirtyFlag;
    
    /**
     * 创建时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp create_date;

    @JsonIgnore
    public boolean create_dateDirtyFlag;
    
    /**
     * 创建人
     */
    public Integer create_uid;

    @JsonIgnore
    public boolean create_uidDirtyFlag;
    
    /**
     * 创建人
     */
    public String create_uid_text;

    @JsonIgnore
    public boolean create_uid_textDirtyFlag;
    
    /**
     * 在开业或结束这个销售点的余额时，默认情况下使用这个钱箱行
     */
    public Integer default_pos_id;

    @JsonIgnore
    public boolean default_pos_idDirtyFlag;
    
    /**
     * 显示名称
     */
    public String display_name;

    @JsonIgnore
    public boolean display_nameDirtyFlag;
    
    /**
     * ID
     */
    public Integer id;

    @JsonIgnore
    public boolean idDirtyFlag;
    
    /**
     * 货币和账单编号
     */
    public Integer number;

    @JsonIgnore
    public boolean numberDirtyFlag;
    
    /**
     * 小计
     */
    public Double subtotal;

    @JsonIgnore
    public boolean subtotalDirtyFlag;
    
    /**
     * 最后更新时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp write_date;

    @JsonIgnore
    public boolean write_dateDirtyFlag;
    
    /**
     * 最后更新人
     */
    public Integer write_uid;

    @JsonIgnore
    public boolean write_uidDirtyFlag;
    
    /**
     * 最后更新人
     */
    public String write_uid_text;

    @JsonIgnore
    public boolean write_uid_textDirtyFlag;
    
    /**
     * 最后修改日
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp __last_update;

    @JsonIgnore
    public boolean __last_updateDirtyFlag;
    
    /**
     * 获取 [钱箱]
     */
    @JsonProperty("cashbox_id")
    public Integer getCashbox_id(){
        return this.cashbox_id ;
    }

    /**
     * 设置 [钱箱]
     */
    @JsonProperty("cashbox_id")
    public void setCashbox_id(Integer  cashbox_id){
        this.cashbox_id = cashbox_id ;
        this.cashbox_idDirtyFlag = true ;
    }

     /**
     * 获取 [钱箱]脏标记
     */
    @JsonIgnore
    public boolean getCashbox_idDirtyFlag(){
        return this.cashbox_idDirtyFlag ;
    }   

    /**
     * 获取 [硬币／账单　价值]
     */
    @JsonProperty("coin_value")
    public Double getCoin_value(){
        return this.coin_value ;
    }

    /**
     * 设置 [硬币／账单　价值]
     */
    @JsonProperty("coin_value")
    public void setCoin_value(Double  coin_value){
        this.coin_value = coin_value ;
        this.coin_valueDirtyFlag = true ;
    }

     /**
     * 获取 [硬币／账单　价值]脏标记
     */
    @JsonIgnore
    public boolean getCoin_valueDirtyFlag(){
        return this.coin_valueDirtyFlag ;
    }   

    /**
     * 获取 [创建时间]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return this.create_date ;
    }

    /**
     * 设置 [创建时间]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

     /**
     * 获取 [创建时间]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return this.create_dateDirtyFlag ;
    }   

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return this.create_uid ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

     /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return this.create_uidDirtyFlag ;
    }   

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return this.create_uid_text ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

     /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return this.create_uid_textDirtyFlag ;
    }   

    /**
     * 获取 [在开业或结束这个销售点的余额时，默认情况下使用这个钱箱行]
     */
    @JsonProperty("default_pos_id")
    public Integer getDefault_pos_id(){
        return this.default_pos_id ;
    }

    /**
     * 设置 [在开业或结束这个销售点的余额时，默认情况下使用这个钱箱行]
     */
    @JsonProperty("default_pos_id")
    public void setDefault_pos_id(Integer  default_pos_id){
        this.default_pos_id = default_pos_id ;
        this.default_pos_idDirtyFlag = true ;
    }

     /**
     * 获取 [在开业或结束这个销售点的余额时，默认情况下使用这个钱箱行]脏标记
     */
    @JsonIgnore
    public boolean getDefault_pos_idDirtyFlag(){
        return this.default_pos_idDirtyFlag ;
    }   

    /**
     * 获取 [显示名称]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return this.display_name ;
    }

    /**
     * 设置 [显示名称]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

     /**
     * 获取 [显示名称]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return this.display_nameDirtyFlag ;
    }   

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return this.id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

     /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return this.idDirtyFlag ;
    }   

    /**
     * 获取 [货币和账单编号]
     */
    @JsonProperty("number")
    public Integer getNumber(){
        return this.number ;
    }

    /**
     * 设置 [货币和账单编号]
     */
    @JsonProperty("number")
    public void setNumber(Integer  number){
        this.number = number ;
        this.numberDirtyFlag = true ;
    }

     /**
     * 获取 [货币和账单编号]脏标记
     */
    @JsonIgnore
    public boolean getNumberDirtyFlag(){
        return this.numberDirtyFlag ;
    }   

    /**
     * 获取 [小计]
     */
    @JsonProperty("subtotal")
    public Double getSubtotal(){
        return this.subtotal ;
    }

    /**
     * 设置 [小计]
     */
    @JsonProperty("subtotal")
    public void setSubtotal(Double  subtotal){
        this.subtotal = subtotal ;
        this.subtotalDirtyFlag = true ;
    }

     /**
     * 获取 [小计]脏标记
     */
    @JsonIgnore
    public boolean getSubtotalDirtyFlag(){
        return this.subtotalDirtyFlag ;
    }   

    /**
     * 获取 [最后更新时间]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return this.write_date ;
    }

    /**
     * 设置 [最后更新时间]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新时间]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return this.write_dateDirtyFlag ;
    }   

    /**
     * 获取 [最后更新人]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return this.write_uid ;
    }

    /**
     * 设置 [最后更新人]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新人]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return this.write_uidDirtyFlag ;
    }   

    /**
     * 获取 [最后更新人]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return this.write_uid_text ;
    }

    /**
     * 设置 [最后更新人]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新人]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return this.write_uid_textDirtyFlag ;
    }   

    /**
     * 获取 [最后修改日]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return this.__last_update ;
    }

    /**
     * 设置 [最后修改日]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

     /**
     * 获取 [最后修改日]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return this.__last_updateDirtyFlag ;
    }   



    public void fromMap(Map<String, Object> map) throws Exception {
		if(!(map.get("cashbox_id") instanceof Boolean)&& map.get("cashbox_id")!=null){
			Object[] objs = (Object[])map.get("cashbox_id");
			if(objs.length > 0){
				this.setCashbox_id((Integer)objs[0]);
			}
		}
		if(!(map.get("coin_value") instanceof Boolean)&& map.get("coin_value")!=null){
			this.setCoin_value((Double)map.get("coin_value"));
		}
		if(!(map.get("create_date") instanceof Boolean)&& map.get("create_date")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse((String)map.get("create_date"));
   			this.setCreate_date(new Timestamp(parse.getTime()));
		}
		if(!(map.get("create_uid") instanceof Boolean)&& map.get("create_uid")!=null){
			Object[] objs = (Object[])map.get("create_uid");
			if(objs.length > 0){
				this.setCreate_uid((Integer)objs[0]);
			}
		}
		if(!(map.get("create_uid") instanceof Boolean)&& map.get("create_uid")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("create_uid");
			if(objs.length > 1){
				this.setCreate_uid_text((String)objs[1]);
			}
		}
		if(!(map.get("default_pos_id") instanceof Boolean)&& map.get("default_pos_id")!=null){
			Object[] objs = (Object[])map.get("default_pos_id");
			if(objs.length > 0){
				this.setDefault_pos_id((Integer)objs[0]);
			}
		}
		if(!(map.get("display_name") instanceof Boolean)&& map.get("display_name")!=null){
			this.setDisplay_name((String)map.get("display_name"));
		}
		if(!(map.get("id") instanceof Boolean)&& map.get("id")!=null){
			this.setId((Integer)map.get("id"));
		}
		if(!(map.get("number") instanceof Boolean)&& map.get("number")!=null){
			this.setNumber((Integer)map.get("number"));
		}
		if(!(map.get("subtotal") instanceof Boolean)&& map.get("subtotal")!=null){
			this.setSubtotal((Double)map.get("subtotal"));
		}
		if(!(map.get("write_date") instanceof Boolean)&& map.get("write_date")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse((String)map.get("write_date"));
   			this.setWrite_date(new Timestamp(parse.getTime()));
		}
		if(!(map.get("write_uid") instanceof Boolean)&& map.get("write_uid")!=null){
			Object[] objs = (Object[])map.get("write_uid");
			if(objs.length > 0){
				this.setWrite_uid((Integer)objs[0]);
			}
		}
		if(!(map.get("write_uid") instanceof Boolean)&& map.get("write_uid")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("write_uid");
			if(objs.length > 1){
				this.setWrite_uid_text((String)objs[1]);
			}
		}
		if(!(map.get("__last_update") instanceof Boolean)&& map.get("__last_update")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse((String)map.get("__last_update"));
   			this.set__last_update(new Timestamp(parse.getTime()));
		}
	}

	public Map<String, Object> toMap() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		if(this.getCashbox_id()!=null&&this.getCashbox_idDirtyFlag()){
			map.put("cashbox_id",this.getCashbox_id());
		}else if(this.getCashbox_idDirtyFlag()){
			map.put("cashbox_id",false);
		}
		if(this.getCoin_value()!=null&&this.getCoin_valueDirtyFlag()){
			map.put("coin_value",this.getCoin_value());
		}else if(this.getCoin_valueDirtyFlag()){
			map.put("coin_value",false);
		}
		if(this.getCreate_date()!=null&&this.getCreate_dateDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String datetimeStr = sdf.format(this.getCreate_date());
			map.put("create_date",datetimeStr);
		}else if(this.getCreate_dateDirtyFlag()){
			map.put("create_date",false);
		}
		if(this.getCreate_uid()!=null&&this.getCreate_uidDirtyFlag()){
			map.put("create_uid",this.getCreate_uid());
		}else if(this.getCreate_uidDirtyFlag()){
			map.put("create_uid",false);
		}
		if(this.getCreate_uid_text()!=null&&this.getCreate_uid_textDirtyFlag()){
			//忽略文本外键create_uid_text
		}else if(this.getCreate_uid_textDirtyFlag()){
			map.put("create_uid",false);
		}
		if(this.getDefault_pos_id()!=null&&this.getDefault_pos_idDirtyFlag()){
			map.put("default_pos_id",this.getDefault_pos_id());
		}else if(this.getDefault_pos_idDirtyFlag()){
			map.put("default_pos_id",false);
		}
		if(this.getDisplay_name()!=null&&this.getDisplay_nameDirtyFlag()){
			map.put("display_name",this.getDisplay_name());
		}else if(this.getDisplay_nameDirtyFlag()){
			map.put("display_name",false);
		}
		if(this.getId()!=null&&this.getIdDirtyFlag()){
			map.put("id",this.getId());
		}else if(this.getIdDirtyFlag()){
			map.put("id",false);
		}
		if(this.getNumber()!=null&&this.getNumberDirtyFlag()){
			map.put("number",this.getNumber());
		}else if(this.getNumberDirtyFlag()){
			map.put("number",false);
		}
		if(this.getSubtotal()!=null&&this.getSubtotalDirtyFlag()){
			map.put("subtotal",this.getSubtotal());
		}else if(this.getSubtotalDirtyFlag()){
			map.put("subtotal",false);
		}
		if(this.getWrite_date()!=null&&this.getWrite_dateDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String datetimeStr = sdf.format(this.getWrite_date());
			map.put("write_date",datetimeStr);
		}else if(this.getWrite_dateDirtyFlag()){
			map.put("write_date",false);
		}
		if(this.getWrite_uid()!=null&&this.getWrite_uidDirtyFlag()){
			map.put("write_uid",this.getWrite_uid());
		}else if(this.getWrite_uidDirtyFlag()){
			map.put("write_uid",false);
		}
		if(this.getWrite_uid_text()!=null&&this.getWrite_uid_textDirtyFlag()){
			//忽略文本外键write_uid_text
		}else if(this.getWrite_uid_textDirtyFlag()){
			map.put("write_uid",false);
		}
		if(this.get__last_update()!=null&&this.get__last_updateDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String datetimeStr = sdf.format(this.get__last_update());
			map.put("__last_update",datetimeStr);
		}else if(this.get__last_updateDirtyFlag()){
			map.put("__last_update",false);
		}
		return map;
	}

}
