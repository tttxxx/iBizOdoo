package cn.ibizlab.odoo.client.odoo_account.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Iaccount_reconcile_model;
import cn.ibizlab.odoo.core.client.service.Iaccount_reconcile_modelClientService;
import cn.ibizlab.odoo.client.odoo_account.model.account_reconcile_modelImpl;
import cn.ibizlab.odoo.client.odoo_account.odooclient.Iaccount_reconcile_modelOdooClient;
import cn.ibizlab.odoo.client.odoo_account.odooclient.impl.account_reconcile_modelOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[account_reconcile_model] 服务对象接口
 */
@Service
public class account_reconcile_modelClientServiceImpl implements Iaccount_reconcile_modelClientService {
    @Autowired
    private  Iaccount_reconcile_modelOdooClient  account_reconcile_modelOdooClient;

    public Iaccount_reconcile_model createModel() {		
		return new account_reconcile_modelImpl();
	}


        public void remove(Iaccount_reconcile_model account_reconcile_model){
this.account_reconcile_modelOdooClient.remove(account_reconcile_model) ;
        }
        
        public void create(Iaccount_reconcile_model account_reconcile_model){
this.account_reconcile_modelOdooClient.create(account_reconcile_model) ;
        }
        
        public void update(Iaccount_reconcile_model account_reconcile_model){
this.account_reconcile_modelOdooClient.update(account_reconcile_model) ;
        }
        
        public void updateBatch(List<Iaccount_reconcile_model> account_reconcile_models){
            
        }
        
        public void createBatch(List<Iaccount_reconcile_model> account_reconcile_models){
            
        }
        
        public Page<Iaccount_reconcile_model> search(SearchContext context){
            return this.account_reconcile_modelOdooClient.search(context) ;
        }
        
        public void get(Iaccount_reconcile_model account_reconcile_model){
            this.account_reconcile_modelOdooClient.get(account_reconcile_model) ;
        }
        
        public void removeBatch(List<Iaccount_reconcile_model> account_reconcile_models){
            
        }
        
        public Page<Iaccount_reconcile_model> select(SearchContext context){
            return null ;
        }
        

}

