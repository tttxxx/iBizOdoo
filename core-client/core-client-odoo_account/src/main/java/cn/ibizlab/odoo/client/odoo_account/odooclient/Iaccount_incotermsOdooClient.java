package cn.ibizlab.odoo.client.odoo_account.odooclient;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.core.client.model.Iaccount_incoterms;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[account_incoterms] 服务对象客户端接口
 */
public interface Iaccount_incotermsOdooClient {
    
        public void removeBatch(Iaccount_incoterms account_incoterms);

        public Page<Iaccount_incoterms> search(SearchContext context);

        public void update(Iaccount_incoterms account_incoterms);

        public void create(Iaccount_incoterms account_incoterms);

        public void remove(Iaccount_incoterms account_incoterms);

        public void get(Iaccount_incoterms account_incoterms);

        public void createBatch(Iaccount_incoterms account_incoterms);

        public void updateBatch(Iaccount_incoterms account_incoterms);

        public List<Iaccount_incoterms> select();


}