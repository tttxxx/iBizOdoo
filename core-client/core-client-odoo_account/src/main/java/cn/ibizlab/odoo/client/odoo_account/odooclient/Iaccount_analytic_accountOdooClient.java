package cn.ibizlab.odoo.client.odoo_account.odooclient;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.core.client.model.Iaccount_analytic_account;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[account_analytic_account] 服务对象客户端接口
 */
public interface Iaccount_analytic_accountOdooClient {
    
        public void updateBatch(Iaccount_analytic_account account_analytic_account);

        public Page<Iaccount_analytic_account> search(SearchContext context);

        public void create(Iaccount_analytic_account account_analytic_account);

        public void remove(Iaccount_analytic_account account_analytic_account);

        public void removeBatch(Iaccount_analytic_account account_analytic_account);

        public void createBatch(Iaccount_analytic_account account_analytic_account);

        public void get(Iaccount_analytic_account account_analytic_account);

        public void update(Iaccount_analytic_account account_analytic_account);

        public List<Iaccount_analytic_account> select();


}