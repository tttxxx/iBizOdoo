package cn.ibizlab.odoo.client.odoo_account.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Iaccount_invoice_import_wizard;
import cn.ibizlab.odoo.core.client.service.Iaccount_invoice_import_wizardClientService;
import cn.ibizlab.odoo.client.odoo_account.model.account_invoice_import_wizardImpl;
import cn.ibizlab.odoo.client.odoo_account.odooclient.Iaccount_invoice_import_wizardOdooClient;
import cn.ibizlab.odoo.client.odoo_account.odooclient.impl.account_invoice_import_wizardOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[account_invoice_import_wizard] 服务对象接口
 */
@Service
public class account_invoice_import_wizardClientServiceImpl implements Iaccount_invoice_import_wizardClientService {
    @Autowired
    private  Iaccount_invoice_import_wizardOdooClient  account_invoice_import_wizardOdooClient;

    public Iaccount_invoice_import_wizard createModel() {		
		return new account_invoice_import_wizardImpl();
	}


        public void updateBatch(List<Iaccount_invoice_import_wizard> account_invoice_import_wizards){
            
        }
        
        public void remove(Iaccount_invoice_import_wizard account_invoice_import_wizard){
this.account_invoice_import_wizardOdooClient.remove(account_invoice_import_wizard) ;
        }
        
        public void create(Iaccount_invoice_import_wizard account_invoice_import_wizard){
this.account_invoice_import_wizardOdooClient.create(account_invoice_import_wizard) ;
        }
        
        public void removeBatch(List<Iaccount_invoice_import_wizard> account_invoice_import_wizards){
            
        }
        
        public void createBatch(List<Iaccount_invoice_import_wizard> account_invoice_import_wizards){
            
        }
        
        public void get(Iaccount_invoice_import_wizard account_invoice_import_wizard){
            this.account_invoice_import_wizardOdooClient.get(account_invoice_import_wizard) ;
        }
        
        public void update(Iaccount_invoice_import_wizard account_invoice_import_wizard){
this.account_invoice_import_wizardOdooClient.update(account_invoice_import_wizard) ;
        }
        
        public Page<Iaccount_invoice_import_wizard> search(SearchContext context){
            return this.account_invoice_import_wizardOdooClient.search(context) ;
        }
        
        public Page<Iaccount_invoice_import_wizard> select(SearchContext context){
            return null ;
        }
        

}

