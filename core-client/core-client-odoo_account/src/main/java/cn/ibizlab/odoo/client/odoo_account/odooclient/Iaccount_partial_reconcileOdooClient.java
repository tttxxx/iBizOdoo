package cn.ibizlab.odoo.client.odoo_account.odooclient;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.core.client.model.Iaccount_partial_reconcile;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[account_partial_reconcile] 服务对象客户端接口
 */
public interface Iaccount_partial_reconcileOdooClient {
    
        public void get(Iaccount_partial_reconcile account_partial_reconcile);

        public Page<Iaccount_partial_reconcile> search(SearchContext context);

        public void remove(Iaccount_partial_reconcile account_partial_reconcile);

        public void update(Iaccount_partial_reconcile account_partial_reconcile);

        public void updateBatch(Iaccount_partial_reconcile account_partial_reconcile);

        public void create(Iaccount_partial_reconcile account_partial_reconcile);

        public void removeBatch(Iaccount_partial_reconcile account_partial_reconcile);

        public void createBatch(Iaccount_partial_reconcile account_partial_reconcile);

        public List<Iaccount_partial_reconcile> select();


}