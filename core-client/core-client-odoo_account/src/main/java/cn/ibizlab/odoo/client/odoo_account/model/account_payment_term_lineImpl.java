package cn.ibizlab.odoo.client.odoo_account.model;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.math.BigInteger;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.core.client.model.Iaccount_payment_term_line;
import cn.ibizlab.odoo.util.helper.OdooClientHelper;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang.ArrayUtils;
import org.springframework.util.StringUtils;

/**
 * 接口实体[account_payment_term_line] 对象
 */
public class account_payment_term_lineImpl implements Iaccount_payment_term_line,Serializable{

    /**
     * 创建时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp create_date;

    @JsonIgnore
    public boolean create_dateDirtyFlag;
    
    /**
     * 创建人
     */
    public Integer create_uid;

    @JsonIgnore
    public boolean create_uidDirtyFlag;
    
    /**
     * 创建人
     */
    public String create_uid_text;

    @JsonIgnore
    public boolean create_uid_textDirtyFlag;
    
    /**
     * 天数
     */
    public Integer days;

    @JsonIgnore
    public boolean daysDirtyFlag;
    
    /**
     * 日期
     */
    public Integer day_of_the_month;

    @JsonIgnore
    public boolean day_of_the_monthDirtyFlag;
    
    /**
     * 显示名称
     */
    public String display_name;

    @JsonIgnore
    public boolean display_nameDirtyFlag;
    
    /**
     * ID
     */
    public Integer id;

    @JsonIgnore
    public boolean idDirtyFlag;
    
    /**
     * 选项
     */
    public String option;

    @JsonIgnore
    public boolean optionDirtyFlag;
    
    /**
     * 付款条款
     */
    public Integer payment_id;

    @JsonIgnore
    public boolean payment_idDirtyFlag;
    
    /**
     * 付款条款
     */
    public String payment_id_text;

    @JsonIgnore
    public boolean payment_id_textDirtyFlag;
    
    /**
     * 序号
     */
    public Integer sequence;

    @JsonIgnore
    public boolean sequenceDirtyFlag;
    
    /**
     * 类型
     */
    public String value;

    @JsonIgnore
    public boolean valueDirtyFlag;
    
    /**
     * 值
     */
    public Double value_amount;

    @JsonIgnore
    public boolean value_amountDirtyFlag;
    
    /**
     * 最后更新时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp write_date;

    @JsonIgnore
    public boolean write_dateDirtyFlag;
    
    /**
     * 最后更新人
     */
    public Integer write_uid;

    @JsonIgnore
    public boolean write_uidDirtyFlag;
    
    /**
     * 最后更新人
     */
    public String write_uid_text;

    @JsonIgnore
    public boolean write_uid_textDirtyFlag;
    
    /**
     * 最后修改日
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp __last_update;

    @JsonIgnore
    public boolean __last_updateDirtyFlag;
    
    /**
     * 获取 [创建时间]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return this.create_date ;
    }

    /**
     * 设置 [创建时间]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

     /**
     * 获取 [创建时间]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return this.create_dateDirtyFlag ;
    }   

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return this.create_uid ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

     /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return this.create_uidDirtyFlag ;
    }   

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return this.create_uid_text ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

     /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return this.create_uid_textDirtyFlag ;
    }   

    /**
     * 获取 [天数]
     */
    @JsonProperty("days")
    public Integer getDays(){
        return this.days ;
    }

    /**
     * 设置 [天数]
     */
    @JsonProperty("days")
    public void setDays(Integer  days){
        this.days = days ;
        this.daysDirtyFlag = true ;
    }

     /**
     * 获取 [天数]脏标记
     */
    @JsonIgnore
    public boolean getDaysDirtyFlag(){
        return this.daysDirtyFlag ;
    }   

    /**
     * 获取 [日期]
     */
    @JsonProperty("day_of_the_month")
    public Integer getDay_of_the_month(){
        return this.day_of_the_month ;
    }

    /**
     * 设置 [日期]
     */
    @JsonProperty("day_of_the_month")
    public void setDay_of_the_month(Integer  day_of_the_month){
        this.day_of_the_month = day_of_the_month ;
        this.day_of_the_monthDirtyFlag = true ;
    }

     /**
     * 获取 [日期]脏标记
     */
    @JsonIgnore
    public boolean getDay_of_the_monthDirtyFlag(){
        return this.day_of_the_monthDirtyFlag ;
    }   

    /**
     * 获取 [显示名称]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return this.display_name ;
    }

    /**
     * 设置 [显示名称]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

     /**
     * 获取 [显示名称]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return this.display_nameDirtyFlag ;
    }   

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return this.id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

     /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return this.idDirtyFlag ;
    }   

    /**
     * 获取 [选项]
     */
    @JsonProperty("option")
    public String getOption(){
        return this.option ;
    }

    /**
     * 设置 [选项]
     */
    @JsonProperty("option")
    public void setOption(String  option){
        this.option = option ;
        this.optionDirtyFlag = true ;
    }

     /**
     * 获取 [选项]脏标记
     */
    @JsonIgnore
    public boolean getOptionDirtyFlag(){
        return this.optionDirtyFlag ;
    }   

    /**
     * 获取 [付款条款]
     */
    @JsonProperty("payment_id")
    public Integer getPayment_id(){
        return this.payment_id ;
    }

    /**
     * 设置 [付款条款]
     */
    @JsonProperty("payment_id")
    public void setPayment_id(Integer  payment_id){
        this.payment_id = payment_id ;
        this.payment_idDirtyFlag = true ;
    }

     /**
     * 获取 [付款条款]脏标记
     */
    @JsonIgnore
    public boolean getPayment_idDirtyFlag(){
        return this.payment_idDirtyFlag ;
    }   

    /**
     * 获取 [付款条款]
     */
    @JsonProperty("payment_id_text")
    public String getPayment_id_text(){
        return this.payment_id_text ;
    }

    /**
     * 设置 [付款条款]
     */
    @JsonProperty("payment_id_text")
    public void setPayment_id_text(String  payment_id_text){
        this.payment_id_text = payment_id_text ;
        this.payment_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [付款条款]脏标记
     */
    @JsonIgnore
    public boolean getPayment_id_textDirtyFlag(){
        return this.payment_id_textDirtyFlag ;
    }   

    /**
     * 获取 [序号]
     */
    @JsonProperty("sequence")
    public Integer getSequence(){
        return this.sequence ;
    }

    /**
     * 设置 [序号]
     */
    @JsonProperty("sequence")
    public void setSequence(Integer  sequence){
        this.sequence = sequence ;
        this.sequenceDirtyFlag = true ;
    }

     /**
     * 获取 [序号]脏标记
     */
    @JsonIgnore
    public boolean getSequenceDirtyFlag(){
        return this.sequenceDirtyFlag ;
    }   

    /**
     * 获取 [类型]
     */
    @JsonProperty("value")
    public String getValue(){
        return this.value ;
    }

    /**
     * 设置 [类型]
     */
    @JsonProperty("value")
    public void setValue(String  value){
        this.value = value ;
        this.valueDirtyFlag = true ;
    }

     /**
     * 获取 [类型]脏标记
     */
    @JsonIgnore
    public boolean getValueDirtyFlag(){
        return this.valueDirtyFlag ;
    }   

    /**
     * 获取 [值]
     */
    @JsonProperty("value_amount")
    public Double getValue_amount(){
        return this.value_amount ;
    }

    /**
     * 设置 [值]
     */
    @JsonProperty("value_amount")
    public void setValue_amount(Double  value_amount){
        this.value_amount = value_amount ;
        this.value_amountDirtyFlag = true ;
    }

     /**
     * 获取 [值]脏标记
     */
    @JsonIgnore
    public boolean getValue_amountDirtyFlag(){
        return this.value_amountDirtyFlag ;
    }   

    /**
     * 获取 [最后更新时间]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return this.write_date ;
    }

    /**
     * 设置 [最后更新时间]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新时间]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return this.write_dateDirtyFlag ;
    }   

    /**
     * 获取 [最后更新人]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return this.write_uid ;
    }

    /**
     * 设置 [最后更新人]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新人]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return this.write_uidDirtyFlag ;
    }   

    /**
     * 获取 [最后更新人]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return this.write_uid_text ;
    }

    /**
     * 设置 [最后更新人]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新人]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return this.write_uid_textDirtyFlag ;
    }   

    /**
     * 获取 [最后修改日]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return this.__last_update ;
    }

    /**
     * 设置 [最后修改日]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

     /**
     * 获取 [最后修改日]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return this.__last_updateDirtyFlag ;
    }   



    public void fromMap(Map<String, Object> map) throws Exception {
		if(!(map.get("create_date") instanceof Boolean)&& map.get("create_date")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse((String)map.get("create_date"));
   			this.setCreate_date(new Timestamp(parse.getTime()));
		}
		if(!(map.get("create_uid") instanceof Boolean)&& map.get("create_uid")!=null){
			Object[] objs = (Object[])map.get("create_uid");
			if(objs.length > 0){
				this.setCreate_uid((Integer)objs[0]);
			}
		}
		if(!(map.get("create_uid") instanceof Boolean)&& map.get("create_uid")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("create_uid");
			if(objs.length > 1){
				this.setCreate_uid_text((String)objs[1]);
			}
		}
		if(!(map.get("days") instanceof Boolean)&& map.get("days")!=null){
			this.setDays((Integer)map.get("days"));
		}
		if(!(map.get("day_of_the_month") instanceof Boolean)&& map.get("day_of_the_month")!=null){
			this.setDay_of_the_month((Integer)map.get("day_of_the_month"));
		}
		if(!(map.get("display_name") instanceof Boolean)&& map.get("display_name")!=null){
			this.setDisplay_name((String)map.get("display_name"));
		}
		if(!(map.get("id") instanceof Boolean)&& map.get("id")!=null){
			this.setId((Integer)map.get("id"));
		}
		if(!(map.get("option") instanceof Boolean)&& map.get("option")!=null){
			this.setOption((String)map.get("option"));
		}
		if(!(map.get("payment_id") instanceof Boolean)&& map.get("payment_id")!=null){
			Object[] objs = (Object[])map.get("payment_id");
			if(objs.length > 0){
				this.setPayment_id((Integer)objs[0]);
			}
		}
		if(!(map.get("payment_id") instanceof Boolean)&& map.get("payment_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("payment_id");
			if(objs.length > 1){
				this.setPayment_id_text((String)objs[1]);
			}
		}
		if(!(map.get("sequence") instanceof Boolean)&& map.get("sequence")!=null){
			this.setSequence((Integer)map.get("sequence"));
		}
		if(!(map.get("value") instanceof Boolean)&& map.get("value")!=null){
			this.setValue((String)map.get("value"));
		}
		if(!(map.get("value_amount") instanceof Boolean)&& map.get("value_amount")!=null){
			this.setValue_amount((Double)map.get("value_amount"));
		}
		if(!(map.get("write_date") instanceof Boolean)&& map.get("write_date")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse((String)map.get("write_date"));
   			this.setWrite_date(new Timestamp(parse.getTime()));
		}
		if(!(map.get("write_uid") instanceof Boolean)&& map.get("write_uid")!=null){
			Object[] objs = (Object[])map.get("write_uid");
			if(objs.length > 0){
				this.setWrite_uid((Integer)objs[0]);
			}
		}
		if(!(map.get("write_uid") instanceof Boolean)&& map.get("write_uid")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("write_uid");
			if(objs.length > 1){
				this.setWrite_uid_text((String)objs[1]);
			}
		}
		if(!(map.get("__last_update") instanceof Boolean)&& map.get("__last_update")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse((String)map.get("__last_update"));
   			this.set__last_update(new Timestamp(parse.getTime()));
		}
	}

	public Map<String, Object> toMap() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		if(this.getCreate_date()!=null&&this.getCreate_dateDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String datetimeStr = sdf.format(this.getCreate_date());
			map.put("create_date",datetimeStr);
		}else if(this.getCreate_dateDirtyFlag()){
			map.put("create_date",false);
		}
		if(this.getCreate_uid()!=null&&this.getCreate_uidDirtyFlag()){
			map.put("create_uid",this.getCreate_uid());
		}else if(this.getCreate_uidDirtyFlag()){
			map.put("create_uid",false);
		}
		if(this.getCreate_uid_text()!=null&&this.getCreate_uid_textDirtyFlag()){
			//忽略文本外键create_uid_text
		}else if(this.getCreate_uid_textDirtyFlag()){
			map.put("create_uid",false);
		}
		if(this.getDays()!=null&&this.getDaysDirtyFlag()){
			map.put("days",this.getDays());
		}else if(this.getDaysDirtyFlag()){
			map.put("days",false);
		}
		if(this.getDay_of_the_month()!=null&&this.getDay_of_the_monthDirtyFlag()){
			map.put("day_of_the_month",this.getDay_of_the_month());
		}else if(this.getDay_of_the_monthDirtyFlag()){
			map.put("day_of_the_month",false);
		}
		if(this.getDisplay_name()!=null&&this.getDisplay_nameDirtyFlag()){
			map.put("display_name",this.getDisplay_name());
		}else if(this.getDisplay_nameDirtyFlag()){
			map.put("display_name",false);
		}
		if(this.getId()!=null&&this.getIdDirtyFlag()){
			map.put("id",this.getId());
		}else if(this.getIdDirtyFlag()){
			map.put("id",false);
		}
		if(this.getOption()!=null&&this.getOptionDirtyFlag()){
			map.put("option",this.getOption());
		}else if(this.getOptionDirtyFlag()){
			map.put("option",false);
		}
		if(this.getPayment_id()!=null&&this.getPayment_idDirtyFlag()){
			map.put("payment_id",this.getPayment_id());
		}else if(this.getPayment_idDirtyFlag()){
			map.put("payment_id",false);
		}
		if(this.getPayment_id_text()!=null&&this.getPayment_id_textDirtyFlag()){
			//忽略文本外键payment_id_text
		}else if(this.getPayment_id_textDirtyFlag()){
			map.put("payment_id",false);
		}
		if(this.getSequence()!=null&&this.getSequenceDirtyFlag()){
			map.put("sequence",this.getSequence());
		}else if(this.getSequenceDirtyFlag()){
			map.put("sequence",false);
		}
		if(this.getValue()!=null&&this.getValueDirtyFlag()){
			map.put("value",this.getValue());
		}else if(this.getValueDirtyFlag()){
			map.put("value",false);
		}
		if(this.getValue_amount()!=null&&this.getValue_amountDirtyFlag()){
			map.put("value_amount",this.getValue_amount());
		}else if(this.getValue_amountDirtyFlag()){
			map.put("value_amount",false);
		}
		if(this.getWrite_date()!=null&&this.getWrite_dateDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String datetimeStr = sdf.format(this.getWrite_date());
			map.put("write_date",datetimeStr);
		}else if(this.getWrite_dateDirtyFlag()){
			map.put("write_date",false);
		}
		if(this.getWrite_uid()!=null&&this.getWrite_uidDirtyFlag()){
			map.put("write_uid",this.getWrite_uid());
		}else if(this.getWrite_uidDirtyFlag()){
			map.put("write_uid",false);
		}
		if(this.getWrite_uid_text()!=null&&this.getWrite_uid_textDirtyFlag()){
			//忽略文本外键write_uid_text
		}else if(this.getWrite_uid_textDirtyFlag()){
			map.put("write_uid",false);
		}
		if(this.get__last_update()!=null&&this.get__last_updateDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String datetimeStr = sdf.format(this.get__last_update());
			map.put("__last_update",datetimeStr);
		}else if(this.get__last_updateDirtyFlag()){
			map.put("__last_update",false);
		}
		return map;
	}

}
