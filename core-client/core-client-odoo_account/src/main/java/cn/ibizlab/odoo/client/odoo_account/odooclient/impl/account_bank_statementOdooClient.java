package cn.ibizlab.odoo.client.odoo_account.odooclient.impl;

import java.beans.BeanInfo;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.lang.reflect.Method;
import java.text.SimpleDateFormat;
import java.text.DateFormat;
import java.sql.Timestamp;
import java.security.Principal;
import java.net.URL;
import java.util.*;

import cn.ibizlab.odoo.client.odoo_account.model.account_bank_statementImpl;
import cn.ibizlab.odoo.client.odoo_account.odooclient.Iaccount_bank_statementOdooClient;
import cn.ibizlab.odoo.core.client.model.Iaccount_bank_statement;
import cn.ibizlab.odoo.util.SearchContext;
import cn.ibizlab.odoo.util.helper.OdooSearchContextHelper;
import cn.ibizlab.odoo.util.helper.OdooClientHelper;
import cn.ibizlab.odoo.util.config.OdooClientProperties;

import com.google.common.collect.Lists;
import org.apache.commons.beanutils.BeanUtils;
import org.apache.xmlrpc.client.XmlRpcClient;
import org.apache.xmlrpc.client.XmlRpcClientConfigImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanMap;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.PageImpl;

@Component
public class account_bank_statementOdooClient implements Iaccount_bank_statementOdooClient {

	@Autowired
	OdooClientProperties odooClientProperties;

	public static String URL ;
	public static String DB ;
	public static Integer USERID  ;
	public static String PASS ;

	public static XmlRpcClient client ;

	public account_bank_statementOdooClient(){
		
	}

	public void initClient () {
		if(client == null) {
			URL = odooClientProperties.getUrl();
			DB = odooClientProperties.getDb();
			USERID = odooClientProperties.getUserId();
			PASS = odooClientProperties.getPass();
			XmlRpcClientConfigImpl config = new XmlRpcClientConfigImpl();
			config.setEnabledForExtensions(true);
			client = new XmlRpcClient();
			try{
				config.setServerURL(new URL(String.format("%s/xmlrpc/2/object", URL)));
			}catch(Exception e){
				System.out.println(e.getMessage());
			}
			client.setConfig(config);
		}
	}

	public void remove(Iaccount_bank_statement account_bank_statement){
		initClient();
		try{
			Integer id = account_bank_statement.getId();
			client.execute("execute_kw", Arrays.asList(
				DB, USERID, PASS,
				"account.bank.statement", "unlink",
				Arrays.asList(Arrays.asList(id))));
		}catch(Exception e){
			System.out.println(e.getMessage());
		}
	}

    public Page<Iaccount_bank_statement> search(SearchContext context) {
		initClient();
		List<Iaccount_bank_statement> account_bank_statementList = new ArrayList<>();
		long totalnum = 0;
		try{
			totalnum = (Integer)client.execute(
				"execute_kw", Arrays.asList(
   						DB, USERID, PASS, "account.bank.statement", "search_count",
    					OdooSearchContextHelper.getConditions(context)
			));
			if(totalnum<1)
				return new PageImpl(account_bank_statementList,context.getPageable(),totalnum);
			List<Object> maps = Arrays.asList((Object[]) client.execute(	
				"execute_kw", Arrays.asList(
						DB, USERID, PASS, "account.bank.statement", "search_read",
						OdooSearchContextHelper.getConditions(context),
						new HashMap() {
							{				
								put("order",OdooSearchContextHelper.getSorts(context));	
								put("offset",(int)context.getPageable().getOffset());
								put("limit", context.getPageable().getPageSize());
							}
			})));
			account_bank_statementList = mapsToObjects(maps);
		}catch(Exception e)
		{
			System.out.println(e.getMessage());
		}
		
		return new PageImpl(account_bank_statementList,context.getPageable(),totalnum);
	}
	public void createBatch(Iaccount_bank_statement account_bank_statement){
		initClient();
		try{
		}catch(Exception e){
			System.out.println(e.getMessage());
		}
	}

	public void get(Iaccount_bank_statement account_bank_statement){
		initClient();
		try{
		 	List<Object> maps = Arrays.asList((Object[]) client.execute(
				"execute_kw", Arrays.asList(DB, USERID, PASS, "account.bank.statement", "search_read",
					Arrays.asList(Arrays.asList(
								 Arrays.asList("id", "=", account_bank_statement.getId())
					)),
					new HashMap() {
						{
								put("limit",1);
						}
			})));
			if (maps != null && maps.size() > 0) {
				account_bank_statement.fromMap((Map<String, Object>)maps.get(0));
			}
		}catch(Exception e){
			System.out.println(e.getMessage());
		}
	}

	public void removeBatch(Iaccount_bank_statement account_bank_statement){
		initClient();
		try{
		}catch(Exception e){
			System.out.println(e.getMessage());
		}
	}

	public void create(Iaccount_bank_statement account_bank_statement){
		initClient();
		try{
			Map map = account_bank_statement.toMap();
			Integer id = (Integer) client.execute(
				"execute_kw", Arrays.asList(DB, USERID, PASS, "account.bank.statement", "create",Arrays.asList(map)));
			List<Object> maps = Arrays.asList((Object[]) client.execute(
				"execute_kw", Arrays.asList(DB, USERID, PASS, "account.bank.statement", "search_read",
					Arrays.asList(Arrays.asList(
								 Arrays.asList("id", "=", id)
					)),
					new HashMap() {
						{
								put("limit",1);
						}
			})));
			if (maps != null && maps.size() > 0) {
				account_bank_statement.fromMap((Map<String, Object>)maps.get(0));
			}
		}catch(Exception e){
			System.out.println(e.getMessage());
		}
	}

	public void update(Iaccount_bank_statement account_bank_statement){
		initClient();
		try{
			Integer id =account_bank_statement.getId();
			Map map = account_bank_statement.toMap();
			client.execute(
				"execute_kw", Arrays.asList(DB, USERID, PASS, "account.bank.statement", "write",Arrays.asList(Arrays.asList(id),map)));
			List<Object> maps = Arrays.asList((Object[]) client.execute(
				"execute_kw", Arrays.asList(DB, USERID, PASS, "account.bank.statement", "search_read",
					Arrays.asList(Arrays.asList(
								 Arrays.asList("id", "=", id)
					)),
					new HashMap() {
						{
								put("limit",1);
						}
			})));
			if (maps != null && maps.size() > 0) {
				account_bank_statement.fromMap((Map<String, Object>)maps.get(0));
			}
		}catch(Exception e){
			System.out.println(e.getMessage());
		}
	}

	public void updateBatch(Iaccount_bank_statement account_bank_statement){
		initClient();
		try{
		}catch(Exception e){
			System.out.println(e.getMessage());
		}
	}

    public List<Iaccount_bank_statement> select() {
			return null;
	}




	private List<Iaccount_bank_statement> mapsToObjects(List<Object> maps) throws Exception {
		List<Iaccount_bank_statement> list = Lists.newArrayList();
		if (maps != null && maps.size() > 0) {
			Map<String, Object> map = null;
			for (int i = 0,size = maps.size(); i < size; i++) {
				map = (Map<String, Object>)maps.get(i);
				Iaccount_bank_statement account_bank_statement = new account_bank_statementImpl();
				account_bank_statement.fromMap(map);
				list.add(account_bank_statement);
			}
		}
		return list;
	}

	public void preAction (Principal principal,OdooClientHelper.CurOdooUser curUser) {
		String curUserName = principal.getName();
		int curUserId = 0;
		String dyncPassWord = OdooClientHelper.getCurUserdynaPass(curUserName,PASS);
		try{
			final XmlRpcClientConfigImpl common_config = new XmlRpcClientConfigImpl();
			common_config.setServerURL(
				new URL(String.format("%s/xmlrpc/2/common", URL)));
			//获取用户id
			curUserId = (int)client.execute(
				common_config, "authenticate", Arrays.asList(
						DB, curUserName, dyncPassWord, new HashMap()));
		}catch(Exception e){
			System.out.println("用户校验失败，可能无用户或密码错误"+ e.getMessage());
		}

		if(curUserId == 0) {
			try {
				//修改动态密码
				client.execute("execute_kw", Arrays.asList(
						DB, USERID, PASS,
						"res.users", "write",
						Arrays.asList(Arrays.asList(6),
							new HashMap() {{
								put("name", curUserName);
								put("new_password", dyncPassWord);
							}}
						)
				));
			} catch (Exception e) {
				System.out.println("用户密码重置，可能无用户" + e.getMessage());
			}
		}
		curUser.setUserId(curUserId);
		curUser.setPass(dyncPassWord);
	}

}
