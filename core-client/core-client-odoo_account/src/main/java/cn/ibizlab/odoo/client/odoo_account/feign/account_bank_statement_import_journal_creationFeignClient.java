package cn.ibizlab.odoo.client.odoo_account.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Iaccount_bank_statement_import_journal_creation;
import cn.ibizlab.odoo.client.odoo_account.model.account_bank_statement_import_journal_creationImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[account_bank_statement_import_journal_creation] 服务对象接口
 */
public interface account_bank_statement_import_journal_creationFeignClient {

    @RequestMapping(method = RequestMethod.POST, value = "/odoo_account/account_bank_statement_import_journal_creations")
    public account_bank_statement_import_journal_creationImpl create(@RequestBody account_bank_statement_import_journal_creationImpl account_bank_statement_import_journal_creation);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_account/account_bank_statement_import_journal_creations/{id}")
    public account_bank_statement_import_journal_creationImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_account/account_bank_statement_import_journal_creations/updatebatch")
    public account_bank_statement_import_journal_creationImpl updateBatch(@RequestBody List<account_bank_statement_import_journal_creationImpl> account_bank_statement_import_journal_creations);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_account/account_bank_statement_import_journal_creations/removebatch")
    public account_bank_statement_import_journal_creationImpl removeBatch(@RequestBody List<account_bank_statement_import_journal_creationImpl> account_bank_statement_import_journal_creations);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_account/account_bank_statement_import_journal_creations/createbatch")
    public account_bank_statement_import_journal_creationImpl createBatch(@RequestBody List<account_bank_statement_import_journal_creationImpl> account_bank_statement_import_journal_creations);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_account/account_bank_statement_import_journal_creations/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_account/account_bank_statement_import_journal_creations/{id}")
    public account_bank_statement_import_journal_creationImpl update(@PathVariable("id") Integer id,@RequestBody account_bank_statement_import_journal_creationImpl account_bank_statement_import_journal_creation);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_account/account_bank_statement_import_journal_creations/search")
    public Page<account_bank_statement_import_journal_creationImpl> search(SearchContext context);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_account/account_bank_statement_import_journal_creations/select")
    public Page<account_bank_statement_import_journal_creationImpl> select();



}
