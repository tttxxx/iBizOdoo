package cn.ibizlab.odoo.client.odoo_account.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Iaccount_fiscal_position;
import cn.ibizlab.odoo.core.client.service.Iaccount_fiscal_positionClientService;
import cn.ibizlab.odoo.client.odoo_account.model.account_fiscal_positionImpl;
import cn.ibizlab.odoo.client.odoo_account.odooclient.Iaccount_fiscal_positionOdooClient;
import cn.ibizlab.odoo.client.odoo_account.odooclient.impl.account_fiscal_positionOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[account_fiscal_position] 服务对象接口
 */
@Service
public class account_fiscal_positionClientServiceImpl implements Iaccount_fiscal_positionClientService {
    @Autowired
    private  Iaccount_fiscal_positionOdooClient  account_fiscal_positionOdooClient;

    public Iaccount_fiscal_position createModel() {		
		return new account_fiscal_positionImpl();
	}


        public void update(Iaccount_fiscal_position account_fiscal_position){
this.account_fiscal_positionOdooClient.update(account_fiscal_position) ;
        }
        
        public void removeBatch(List<Iaccount_fiscal_position> account_fiscal_positions){
            
        }
        
        public Page<Iaccount_fiscal_position> search(SearchContext context){
            return this.account_fiscal_positionOdooClient.search(context) ;
        }
        
        public void remove(Iaccount_fiscal_position account_fiscal_position){
this.account_fiscal_positionOdooClient.remove(account_fiscal_position) ;
        }
        
        public void get(Iaccount_fiscal_position account_fiscal_position){
            this.account_fiscal_positionOdooClient.get(account_fiscal_position) ;
        }
        
        public void updateBatch(List<Iaccount_fiscal_position> account_fiscal_positions){
            
        }
        
        public void create(Iaccount_fiscal_position account_fiscal_position){
this.account_fiscal_positionOdooClient.create(account_fiscal_position) ;
        }
        
        public void createBatch(List<Iaccount_fiscal_position> account_fiscal_positions){
            
        }
        
        public Page<Iaccount_fiscal_position> select(SearchContext context){
            return null ;
        }
        

}

