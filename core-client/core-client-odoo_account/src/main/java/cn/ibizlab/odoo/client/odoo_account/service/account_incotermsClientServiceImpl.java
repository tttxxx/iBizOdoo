package cn.ibizlab.odoo.client.odoo_account.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Iaccount_incoterms;
import cn.ibizlab.odoo.core.client.service.Iaccount_incotermsClientService;
import cn.ibizlab.odoo.client.odoo_account.model.account_incotermsImpl;
import cn.ibizlab.odoo.client.odoo_account.odooclient.Iaccount_incotermsOdooClient;
import cn.ibizlab.odoo.client.odoo_account.odooclient.impl.account_incotermsOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[account_incoterms] 服务对象接口
 */
@Service
public class account_incotermsClientServiceImpl implements Iaccount_incotermsClientService {
    @Autowired
    private  Iaccount_incotermsOdooClient  account_incotermsOdooClient;

    public Iaccount_incoterms createModel() {		
		return new account_incotermsImpl();
	}


        public void removeBatch(List<Iaccount_incoterms> account_incoterms){
            
        }
        
        public Page<Iaccount_incoterms> search(SearchContext context){
            return this.account_incotermsOdooClient.search(context) ;
        }
        
        public void update(Iaccount_incoterms account_incoterms){
this.account_incotermsOdooClient.update(account_incoterms) ;
        }
        
        public void create(Iaccount_incoterms account_incoterms){
this.account_incotermsOdooClient.create(account_incoterms) ;
        }
        
        public void remove(Iaccount_incoterms account_incoterms){
this.account_incotermsOdooClient.remove(account_incoterms) ;
        }
        
        public void get(Iaccount_incoterms account_incoterms){
            this.account_incotermsOdooClient.get(account_incoterms) ;
        }
        
        public void createBatch(List<Iaccount_incoterms> account_incoterms){
            
        }
        
        public void updateBatch(List<Iaccount_incoterms> account_incoterms){
            
        }
        
        public Page<Iaccount_incoterms> select(SearchContext context){
            return null ;
        }
        

}

