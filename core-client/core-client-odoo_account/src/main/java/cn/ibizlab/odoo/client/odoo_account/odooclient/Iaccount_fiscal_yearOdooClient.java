package cn.ibizlab.odoo.client.odoo_account.odooclient;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.core.client.model.Iaccount_fiscal_year;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[account_fiscal_year] 服务对象客户端接口
 */
public interface Iaccount_fiscal_yearOdooClient {
    
        public void removeBatch(Iaccount_fiscal_year account_fiscal_year);

        public void updateBatch(Iaccount_fiscal_year account_fiscal_year);

        public void get(Iaccount_fiscal_year account_fiscal_year);

        public void remove(Iaccount_fiscal_year account_fiscal_year);

        public void createBatch(Iaccount_fiscal_year account_fiscal_year);

        public void update(Iaccount_fiscal_year account_fiscal_year);

        public Page<Iaccount_fiscal_year> search(SearchContext context);

        public void create(Iaccount_fiscal_year account_fiscal_year);

        public List<Iaccount_fiscal_year> select();


}