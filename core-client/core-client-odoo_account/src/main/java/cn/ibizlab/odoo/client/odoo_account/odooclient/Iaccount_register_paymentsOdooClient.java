package cn.ibizlab.odoo.client.odoo_account.odooclient;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.core.client.model.Iaccount_register_payments;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[account_register_payments] 服务对象客户端接口
 */
public interface Iaccount_register_paymentsOdooClient {
    
        public void create(Iaccount_register_payments account_register_payments);

        public void createBatch(Iaccount_register_payments account_register_payments);

        public void removeBatch(Iaccount_register_payments account_register_payments);

        public void updateBatch(Iaccount_register_payments account_register_payments);

        public void remove(Iaccount_register_payments account_register_payments);

        public void update(Iaccount_register_payments account_register_payments);

        public void get(Iaccount_register_payments account_register_payments);

        public Page<Iaccount_register_payments> search(SearchContext context);

        public List<Iaccount_register_payments> select();


}