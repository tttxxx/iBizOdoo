package cn.ibizlab.odoo.client.odoo_account.odooclient;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.core.client.model.Iaccount_move_reversal;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[account_move_reversal] 服务对象客户端接口
 */
public interface Iaccount_move_reversalOdooClient {
    
        public void createBatch(Iaccount_move_reversal account_move_reversal);

        public void update(Iaccount_move_reversal account_move_reversal);

        public void removeBatch(Iaccount_move_reversal account_move_reversal);

        public void create(Iaccount_move_reversal account_move_reversal);

        public void updateBatch(Iaccount_move_reversal account_move_reversal);

        public void get(Iaccount_move_reversal account_move_reversal);

        public Page<Iaccount_move_reversal> search(SearchContext context);

        public void remove(Iaccount_move_reversal account_move_reversal);

        public List<Iaccount_move_reversal> select();


}