package cn.ibizlab.odoo.client.odoo_account.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Iaccount_analytic_account;
import cn.ibizlab.odoo.core.client.service.Iaccount_analytic_accountClientService;
import cn.ibizlab.odoo.client.odoo_account.model.account_analytic_accountImpl;
import cn.ibizlab.odoo.client.odoo_account.odooclient.Iaccount_analytic_accountOdooClient;
import cn.ibizlab.odoo.client.odoo_account.odooclient.impl.account_analytic_accountOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[account_analytic_account] 服务对象接口
 */
@Service
public class account_analytic_accountClientServiceImpl implements Iaccount_analytic_accountClientService {
    @Autowired
    private  Iaccount_analytic_accountOdooClient  account_analytic_accountOdooClient;

    public Iaccount_analytic_account createModel() {		
		return new account_analytic_accountImpl();
	}


        public void updateBatch(List<Iaccount_analytic_account> account_analytic_accounts){
            
        }
        
        public Page<Iaccount_analytic_account> search(SearchContext context){
            return this.account_analytic_accountOdooClient.search(context) ;
        }
        
        public void create(Iaccount_analytic_account account_analytic_account){
this.account_analytic_accountOdooClient.create(account_analytic_account) ;
        }
        
        public void remove(Iaccount_analytic_account account_analytic_account){
this.account_analytic_accountOdooClient.remove(account_analytic_account) ;
        }
        
        public void removeBatch(List<Iaccount_analytic_account> account_analytic_accounts){
            
        }
        
        public void createBatch(List<Iaccount_analytic_account> account_analytic_accounts){
            
        }
        
        public void get(Iaccount_analytic_account account_analytic_account){
            this.account_analytic_accountOdooClient.get(account_analytic_account) ;
        }
        
        public void update(Iaccount_analytic_account account_analytic_account){
this.account_analytic_accountOdooClient.update(account_analytic_account) ;
        }
        
        public Page<Iaccount_analytic_account> select(SearchContext context){
            return null ;
        }
        

}

