package cn.ibizlab.odoo.client.odoo_account.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Iaccount_journal;
import cn.ibizlab.odoo.core.client.service.Iaccount_journalClientService;
import cn.ibizlab.odoo.client.odoo_account.model.account_journalImpl;
import cn.ibizlab.odoo.client.odoo_account.odooclient.Iaccount_journalOdooClient;
import cn.ibizlab.odoo.client.odoo_account.odooclient.impl.account_journalOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[account_journal] 服务对象接口
 */
@Service
public class account_journalClientServiceImpl implements Iaccount_journalClientService {
    @Autowired
    private  Iaccount_journalOdooClient  account_journalOdooClient;

    public Iaccount_journal createModel() {		
		return new account_journalImpl();
	}


        public void updateBatch(List<Iaccount_journal> account_journals){
            
        }
        
        public void get(Iaccount_journal account_journal){
            this.account_journalOdooClient.get(account_journal) ;
        }
        
        public void create(Iaccount_journal account_journal){
this.account_journalOdooClient.create(account_journal) ;
        }
        
        public void createBatch(List<Iaccount_journal> account_journals){
            
        }
        
        public Page<Iaccount_journal> search(SearchContext context){
            return this.account_journalOdooClient.search(context) ;
        }
        
        public void update(Iaccount_journal account_journal){
this.account_journalOdooClient.update(account_journal) ;
        }
        
        public void remove(Iaccount_journal account_journal){
this.account_journalOdooClient.remove(account_journal) ;
        }
        
        public void removeBatch(List<Iaccount_journal> account_journals){
            
        }
        
        public Page<Iaccount_journal> select(SearchContext context){
            return null ;
        }
        

}

