package cn.ibizlab.odoo.client.odoo_account.odooclient;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.core.client.model.Iaccount_unreconcile;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[account_unreconcile] 服务对象客户端接口
 */
public interface Iaccount_unreconcileOdooClient {
    
        public Page<Iaccount_unreconcile> search(SearchContext context);

        public void get(Iaccount_unreconcile account_unreconcile);

        public void createBatch(Iaccount_unreconcile account_unreconcile);

        public void removeBatch(Iaccount_unreconcile account_unreconcile);

        public void create(Iaccount_unreconcile account_unreconcile);

        public void updateBatch(Iaccount_unreconcile account_unreconcile);

        public void update(Iaccount_unreconcile account_unreconcile);

        public void remove(Iaccount_unreconcile account_unreconcile);

        public List<Iaccount_unreconcile> select();


}