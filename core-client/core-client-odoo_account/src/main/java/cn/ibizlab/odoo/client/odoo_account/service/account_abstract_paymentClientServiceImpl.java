package cn.ibizlab.odoo.client.odoo_account.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Iaccount_abstract_payment;
import cn.ibizlab.odoo.core.client.service.Iaccount_abstract_paymentClientService;
import cn.ibizlab.odoo.client.odoo_account.model.account_abstract_paymentImpl;
import cn.ibizlab.odoo.client.odoo_account.odooclient.Iaccount_abstract_paymentOdooClient;
import cn.ibizlab.odoo.client.odoo_account.odooclient.impl.account_abstract_paymentOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[account_abstract_payment] 服务对象接口
 */
@Service
public class account_abstract_paymentClientServiceImpl implements Iaccount_abstract_paymentClientService {
    @Autowired
    private  Iaccount_abstract_paymentOdooClient  account_abstract_paymentOdooClient;

    public Iaccount_abstract_payment createModel() {		
		return new account_abstract_paymentImpl();
	}


        public void create(Iaccount_abstract_payment account_abstract_payment){
this.account_abstract_paymentOdooClient.create(account_abstract_payment) ;
        }
        
        public void removeBatch(List<Iaccount_abstract_payment> account_abstract_payments){
            
        }
        
        public void get(Iaccount_abstract_payment account_abstract_payment){
            this.account_abstract_paymentOdooClient.get(account_abstract_payment) ;
        }
        
        public void remove(Iaccount_abstract_payment account_abstract_payment){
this.account_abstract_paymentOdooClient.remove(account_abstract_payment) ;
        }
        
        public Page<Iaccount_abstract_payment> search(SearchContext context){
            return this.account_abstract_paymentOdooClient.search(context) ;
        }
        
        public void updateBatch(List<Iaccount_abstract_payment> account_abstract_payments){
            
        }
        
        public void update(Iaccount_abstract_payment account_abstract_payment){
this.account_abstract_paymentOdooClient.update(account_abstract_payment) ;
        }
        
        public void createBatch(List<Iaccount_abstract_payment> account_abstract_payments){
            
        }
        
        public Page<Iaccount_abstract_payment> select(SearchContext context){
            return null ;
        }
        

}

