package cn.ibizlab.odoo.client.odoo_account.odooclient;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.core.client.model.Iaccount_invoice_report;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[account_invoice_report] 服务对象客户端接口
 */
public interface Iaccount_invoice_reportOdooClient {
    
        public void createBatch(Iaccount_invoice_report account_invoice_report);

        public void removeBatch(Iaccount_invoice_report account_invoice_report);

        public void updateBatch(Iaccount_invoice_report account_invoice_report);

        public void remove(Iaccount_invoice_report account_invoice_report);

        public void get(Iaccount_invoice_report account_invoice_report);

        public void create(Iaccount_invoice_report account_invoice_report);

        public void update(Iaccount_invoice_report account_invoice_report);

        public Page<Iaccount_invoice_report> search(SearchContext context);

        public List<Iaccount_invoice_report> select();


}