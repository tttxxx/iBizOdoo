package cn.ibizlab.odoo.client.odoo_account.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Iaccount_invoice_tax;
import cn.ibizlab.odoo.core.client.service.Iaccount_invoice_taxClientService;
import cn.ibizlab.odoo.client.odoo_account.model.account_invoice_taxImpl;
import cn.ibizlab.odoo.client.odoo_account.odooclient.Iaccount_invoice_taxOdooClient;
import cn.ibizlab.odoo.client.odoo_account.odooclient.impl.account_invoice_taxOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[account_invoice_tax] 服务对象接口
 */
@Service
public class account_invoice_taxClientServiceImpl implements Iaccount_invoice_taxClientService {
    @Autowired
    private  Iaccount_invoice_taxOdooClient  account_invoice_taxOdooClient;

    public Iaccount_invoice_tax createModel() {		
		return new account_invoice_taxImpl();
	}


        public Page<Iaccount_invoice_tax> search(SearchContext context){
            return this.account_invoice_taxOdooClient.search(context) ;
        }
        
        public void updateBatch(List<Iaccount_invoice_tax> account_invoice_taxes){
            
        }
        
        public void removeBatch(List<Iaccount_invoice_tax> account_invoice_taxes){
            
        }
        
        public void createBatch(List<Iaccount_invoice_tax> account_invoice_taxes){
            
        }
        
        public void update(Iaccount_invoice_tax account_invoice_tax){
this.account_invoice_taxOdooClient.update(account_invoice_tax) ;
        }
        
        public void get(Iaccount_invoice_tax account_invoice_tax){
            this.account_invoice_taxOdooClient.get(account_invoice_tax) ;
        }
        
        public void remove(Iaccount_invoice_tax account_invoice_tax){
this.account_invoice_taxOdooClient.remove(account_invoice_tax) ;
        }
        
        public void create(Iaccount_invoice_tax account_invoice_tax){
this.account_invoice_taxOdooClient.create(account_invoice_tax) ;
        }
        
        public Page<Iaccount_invoice_tax> select(SearchContext context){
            return null ;
        }
        

}

