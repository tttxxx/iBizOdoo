package cn.ibizlab.odoo.client.odoo_account.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Iaccount_tax_template;
import cn.ibizlab.odoo.core.client.service.Iaccount_tax_templateClientService;
import cn.ibizlab.odoo.client.odoo_account.model.account_tax_templateImpl;
import cn.ibizlab.odoo.client.odoo_account.odooclient.Iaccount_tax_templateOdooClient;
import cn.ibizlab.odoo.client.odoo_account.odooclient.impl.account_tax_templateOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[account_tax_template] 服务对象接口
 */
@Service
public class account_tax_templateClientServiceImpl implements Iaccount_tax_templateClientService {
    @Autowired
    private  Iaccount_tax_templateOdooClient  account_tax_templateOdooClient;

    public Iaccount_tax_template createModel() {		
		return new account_tax_templateImpl();
	}


        public void update(Iaccount_tax_template account_tax_template){
this.account_tax_templateOdooClient.update(account_tax_template) ;
        }
        
        public void create(Iaccount_tax_template account_tax_template){
this.account_tax_templateOdooClient.create(account_tax_template) ;
        }
        
        public void get(Iaccount_tax_template account_tax_template){
            this.account_tax_templateOdooClient.get(account_tax_template) ;
        }
        
        public Page<Iaccount_tax_template> search(SearchContext context){
            return this.account_tax_templateOdooClient.search(context) ;
        }
        
        public void removeBatch(List<Iaccount_tax_template> account_tax_templates){
            
        }
        
        public void updateBatch(List<Iaccount_tax_template> account_tax_templates){
            
        }
        
        public void remove(Iaccount_tax_template account_tax_template){
this.account_tax_templateOdooClient.remove(account_tax_template) ;
        }
        
        public void createBatch(List<Iaccount_tax_template> account_tax_templates){
            
        }
        
        public Page<Iaccount_tax_template> select(SearchContext context){
            return null ;
        }
        

}

