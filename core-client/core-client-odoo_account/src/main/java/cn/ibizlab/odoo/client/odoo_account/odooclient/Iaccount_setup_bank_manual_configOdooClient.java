package cn.ibizlab.odoo.client.odoo_account.odooclient;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.core.client.model.Iaccount_setup_bank_manual_config;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[account_setup_bank_manual_config] 服务对象客户端接口
 */
public interface Iaccount_setup_bank_manual_configOdooClient {
    
        public void update(Iaccount_setup_bank_manual_config account_setup_bank_manual_config);

        public void create(Iaccount_setup_bank_manual_config account_setup_bank_manual_config);

        public void get(Iaccount_setup_bank_manual_config account_setup_bank_manual_config);

        public void updateBatch(Iaccount_setup_bank_manual_config account_setup_bank_manual_config);

        public void remove(Iaccount_setup_bank_manual_config account_setup_bank_manual_config);

        public void removeBatch(Iaccount_setup_bank_manual_config account_setup_bank_manual_config);

        public void createBatch(Iaccount_setup_bank_manual_config account_setup_bank_manual_config);

        public Page<Iaccount_setup_bank_manual_config> search(SearchContext context);

        public List<Iaccount_setup_bank_manual_config> select();


}