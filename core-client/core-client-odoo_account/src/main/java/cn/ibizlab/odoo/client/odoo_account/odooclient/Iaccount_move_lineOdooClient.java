package cn.ibizlab.odoo.client.odoo_account.odooclient;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.core.client.model.Iaccount_move_line;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[account_move_line] 服务对象客户端接口
 */
public interface Iaccount_move_lineOdooClient {
    
        public Page<Iaccount_move_line> search(SearchContext context);

        public void update(Iaccount_move_line account_move_line);

        public void createBatch(Iaccount_move_line account_move_line);

        public void create(Iaccount_move_line account_move_line);

        public void remove(Iaccount_move_line account_move_line);

        public void get(Iaccount_move_line account_move_line);

        public void updateBatch(Iaccount_move_line account_move_line);

        public void removeBatch(Iaccount_move_line account_move_line);

        public List<Iaccount_move_line> select();


}