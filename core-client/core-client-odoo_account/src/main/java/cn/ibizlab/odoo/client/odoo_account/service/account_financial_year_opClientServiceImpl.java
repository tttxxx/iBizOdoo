package cn.ibizlab.odoo.client.odoo_account.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Iaccount_financial_year_op;
import cn.ibizlab.odoo.core.client.service.Iaccount_financial_year_opClientService;
import cn.ibizlab.odoo.client.odoo_account.model.account_financial_year_opImpl;
import cn.ibizlab.odoo.client.odoo_account.odooclient.Iaccount_financial_year_opOdooClient;
import cn.ibizlab.odoo.client.odoo_account.odooclient.impl.account_financial_year_opOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[account_financial_year_op] 服务对象接口
 */
@Service
public class account_financial_year_opClientServiceImpl implements Iaccount_financial_year_opClientService {
    @Autowired
    private  Iaccount_financial_year_opOdooClient  account_financial_year_opOdooClient;

    public Iaccount_financial_year_op createModel() {		
		return new account_financial_year_opImpl();
	}


        public void remove(Iaccount_financial_year_op account_financial_year_op){
this.account_financial_year_opOdooClient.remove(account_financial_year_op) ;
        }
        
        public void removeBatch(List<Iaccount_financial_year_op> account_financial_year_ops){
            
        }
        
        public void updateBatch(List<Iaccount_financial_year_op> account_financial_year_ops){
            
        }
        
        public void createBatch(List<Iaccount_financial_year_op> account_financial_year_ops){
            
        }
        
        public Page<Iaccount_financial_year_op> search(SearchContext context){
            return this.account_financial_year_opOdooClient.search(context) ;
        }
        
        public void get(Iaccount_financial_year_op account_financial_year_op){
            this.account_financial_year_opOdooClient.get(account_financial_year_op) ;
        }
        
        public void update(Iaccount_financial_year_op account_financial_year_op){
this.account_financial_year_opOdooClient.update(account_financial_year_op) ;
        }
        
        public void create(Iaccount_financial_year_op account_financial_year_op){
this.account_financial_year_opOdooClient.create(account_financial_year_op) ;
        }
        
        public Page<Iaccount_financial_year_op> select(SearchContext context){
            return null ;
        }
        

}

