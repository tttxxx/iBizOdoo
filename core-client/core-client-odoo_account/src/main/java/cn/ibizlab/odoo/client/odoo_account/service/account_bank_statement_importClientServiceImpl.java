package cn.ibizlab.odoo.client.odoo_account.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Iaccount_bank_statement_import;
import cn.ibizlab.odoo.core.client.service.Iaccount_bank_statement_importClientService;
import cn.ibizlab.odoo.client.odoo_account.model.account_bank_statement_importImpl;
import cn.ibizlab.odoo.client.odoo_account.odooclient.Iaccount_bank_statement_importOdooClient;
import cn.ibizlab.odoo.client.odoo_account.odooclient.impl.account_bank_statement_importOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[account_bank_statement_import] 服务对象接口
 */
@Service
public class account_bank_statement_importClientServiceImpl implements Iaccount_bank_statement_importClientService {
    @Autowired
    private  Iaccount_bank_statement_importOdooClient  account_bank_statement_importOdooClient;

    public Iaccount_bank_statement_import createModel() {		
		return new account_bank_statement_importImpl();
	}


        public void update(Iaccount_bank_statement_import account_bank_statement_import){
this.account_bank_statement_importOdooClient.update(account_bank_statement_import) ;
        }
        
        public void get(Iaccount_bank_statement_import account_bank_statement_import){
            this.account_bank_statement_importOdooClient.get(account_bank_statement_import) ;
        }
        
        public void create(Iaccount_bank_statement_import account_bank_statement_import){
this.account_bank_statement_importOdooClient.create(account_bank_statement_import) ;
        }
        
        public void createBatch(List<Iaccount_bank_statement_import> account_bank_statement_imports){
            
        }
        
        public void updateBatch(List<Iaccount_bank_statement_import> account_bank_statement_imports){
            
        }
        
        public void remove(Iaccount_bank_statement_import account_bank_statement_import){
this.account_bank_statement_importOdooClient.remove(account_bank_statement_import) ;
        }
        
        public void removeBatch(List<Iaccount_bank_statement_import> account_bank_statement_imports){
            
        }
        
        public Page<Iaccount_bank_statement_import> search(SearchContext context){
            return this.account_bank_statement_importOdooClient.search(context) ;
        }
        
        public Page<Iaccount_bank_statement_import> select(SearchContext context){
            return null ;
        }
        

}

