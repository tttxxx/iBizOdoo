package cn.ibizlab.odoo.client.odoo_account.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Iaccount_reconcile_model;
import cn.ibizlab.odoo.client.odoo_account.model.account_reconcile_modelImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[account_reconcile_model] 服务对象接口
 */
public interface account_reconcile_modelFeignClient {

    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_account/account_reconcile_models/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_account/account_reconcile_models")
    public account_reconcile_modelImpl create(@RequestBody account_reconcile_modelImpl account_reconcile_model);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_account/account_reconcile_models/{id}")
    public account_reconcile_modelImpl update(@PathVariable("id") Integer id,@RequestBody account_reconcile_modelImpl account_reconcile_model);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_account/account_reconcile_models/updatebatch")
    public account_reconcile_modelImpl updateBatch(@RequestBody List<account_reconcile_modelImpl> account_reconcile_models);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_account/account_reconcile_models/createbatch")
    public account_reconcile_modelImpl createBatch(@RequestBody List<account_reconcile_modelImpl> account_reconcile_models);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_account/account_reconcile_models/search")
    public Page<account_reconcile_modelImpl> search(SearchContext context);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_account/account_reconcile_models/{id}")
    public account_reconcile_modelImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_account/account_reconcile_models/removebatch")
    public account_reconcile_modelImpl removeBatch(@RequestBody List<account_reconcile_modelImpl> account_reconcile_models);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_account/account_reconcile_models/select")
    public Page<account_reconcile_modelImpl> select();



}
