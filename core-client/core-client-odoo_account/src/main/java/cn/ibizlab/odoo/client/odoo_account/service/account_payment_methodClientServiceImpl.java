package cn.ibizlab.odoo.client.odoo_account.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Iaccount_payment_method;
import cn.ibizlab.odoo.core.client.service.Iaccount_payment_methodClientService;
import cn.ibizlab.odoo.client.odoo_account.model.account_payment_methodImpl;
import cn.ibizlab.odoo.client.odoo_account.odooclient.Iaccount_payment_methodOdooClient;
import cn.ibizlab.odoo.client.odoo_account.odooclient.impl.account_payment_methodOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[account_payment_method] 服务对象接口
 */
@Service
public class account_payment_methodClientServiceImpl implements Iaccount_payment_methodClientService {
    @Autowired
    private  Iaccount_payment_methodOdooClient  account_payment_methodOdooClient;

    public Iaccount_payment_method createModel() {		
		return new account_payment_methodImpl();
	}


        public void get(Iaccount_payment_method account_payment_method){
            this.account_payment_methodOdooClient.get(account_payment_method) ;
        }
        
        public void updateBatch(List<Iaccount_payment_method> account_payment_methods){
            
        }
        
        public void remove(Iaccount_payment_method account_payment_method){
this.account_payment_methodOdooClient.remove(account_payment_method) ;
        }
        
        public void create(Iaccount_payment_method account_payment_method){
this.account_payment_methodOdooClient.create(account_payment_method) ;
        }
        
        public void removeBatch(List<Iaccount_payment_method> account_payment_methods){
            
        }
        
        public Page<Iaccount_payment_method> search(SearchContext context){
            return this.account_payment_methodOdooClient.search(context) ;
        }
        
        public void update(Iaccount_payment_method account_payment_method){
this.account_payment_methodOdooClient.update(account_payment_method) ;
        }
        
        public void createBatch(List<Iaccount_payment_method> account_payment_methods){
            
        }
        
        public Page<Iaccount_payment_method> select(SearchContext context){
            return null ;
        }
        

}

