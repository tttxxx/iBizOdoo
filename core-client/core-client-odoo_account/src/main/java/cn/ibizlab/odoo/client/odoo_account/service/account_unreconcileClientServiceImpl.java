package cn.ibizlab.odoo.client.odoo_account.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Iaccount_unreconcile;
import cn.ibizlab.odoo.core.client.service.Iaccount_unreconcileClientService;
import cn.ibizlab.odoo.client.odoo_account.model.account_unreconcileImpl;
import cn.ibizlab.odoo.client.odoo_account.odooclient.Iaccount_unreconcileOdooClient;
import cn.ibizlab.odoo.client.odoo_account.odooclient.impl.account_unreconcileOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[account_unreconcile] 服务对象接口
 */
@Service
public class account_unreconcileClientServiceImpl implements Iaccount_unreconcileClientService {
    @Autowired
    private  Iaccount_unreconcileOdooClient  account_unreconcileOdooClient;

    public Iaccount_unreconcile createModel() {		
		return new account_unreconcileImpl();
	}


        public Page<Iaccount_unreconcile> search(SearchContext context){
            return this.account_unreconcileOdooClient.search(context) ;
        }
        
        public void get(Iaccount_unreconcile account_unreconcile){
            this.account_unreconcileOdooClient.get(account_unreconcile) ;
        }
        
        public void createBatch(List<Iaccount_unreconcile> account_unreconciles){
            
        }
        
        public void removeBatch(List<Iaccount_unreconcile> account_unreconciles){
            
        }
        
        public void create(Iaccount_unreconcile account_unreconcile){
this.account_unreconcileOdooClient.create(account_unreconcile) ;
        }
        
        public void updateBatch(List<Iaccount_unreconcile> account_unreconciles){
            
        }
        
        public void update(Iaccount_unreconcile account_unreconcile){
this.account_unreconcileOdooClient.update(account_unreconcile) ;
        }
        
        public void remove(Iaccount_unreconcile account_unreconcile){
this.account_unreconcileOdooClient.remove(account_unreconcile) ;
        }
        
        public Page<Iaccount_unreconcile> select(SearchContext context){
            return null ;
        }
        

}

