package cn.ibizlab.odoo.client.odoo_account.odooclient;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.core.client.model.Iaccount_chart_template;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[account_chart_template] 服务对象客户端接口
 */
public interface Iaccount_chart_templateOdooClient {
    
        public void updateBatch(Iaccount_chart_template account_chart_template);

        public void create(Iaccount_chart_template account_chart_template);

        public Page<Iaccount_chart_template> search(SearchContext context);

        public void get(Iaccount_chart_template account_chart_template);

        public void removeBatch(Iaccount_chart_template account_chart_template);

        public void remove(Iaccount_chart_template account_chart_template);

        public void update(Iaccount_chart_template account_chart_template);

        public void createBatch(Iaccount_chart_template account_chart_template);

        public List<Iaccount_chart_template> select();


}