package cn.ibizlab.odoo.client.odoo_account.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Iaccount_chart_template;
import cn.ibizlab.odoo.core.client.service.Iaccount_chart_templateClientService;
import cn.ibizlab.odoo.client.odoo_account.model.account_chart_templateImpl;
import cn.ibizlab.odoo.client.odoo_account.odooclient.Iaccount_chart_templateOdooClient;
import cn.ibizlab.odoo.client.odoo_account.odooclient.impl.account_chart_templateOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[account_chart_template] 服务对象接口
 */
@Service
public class account_chart_templateClientServiceImpl implements Iaccount_chart_templateClientService {
    @Autowired
    private  Iaccount_chart_templateOdooClient  account_chart_templateOdooClient;

    public Iaccount_chart_template createModel() {		
		return new account_chart_templateImpl();
	}


        public void updateBatch(List<Iaccount_chart_template> account_chart_templates){
            
        }
        
        public void create(Iaccount_chart_template account_chart_template){
this.account_chart_templateOdooClient.create(account_chart_template) ;
        }
        
        public Page<Iaccount_chart_template> search(SearchContext context){
            return this.account_chart_templateOdooClient.search(context) ;
        }
        
        public void get(Iaccount_chart_template account_chart_template){
            this.account_chart_templateOdooClient.get(account_chart_template) ;
        }
        
        public void removeBatch(List<Iaccount_chart_template> account_chart_templates){
            
        }
        
        public void remove(Iaccount_chart_template account_chart_template){
this.account_chart_templateOdooClient.remove(account_chart_template) ;
        }
        
        public void update(Iaccount_chart_template account_chart_template){
this.account_chart_templateOdooClient.update(account_chart_template) ;
        }
        
        public void createBatch(List<Iaccount_chart_template> account_chart_templates){
            
        }
        
        public Page<Iaccount_chart_template> select(SearchContext context){
            return null ;
        }
        

}

