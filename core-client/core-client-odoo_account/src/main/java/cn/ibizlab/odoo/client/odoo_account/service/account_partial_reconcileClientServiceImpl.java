package cn.ibizlab.odoo.client.odoo_account.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Iaccount_partial_reconcile;
import cn.ibizlab.odoo.core.client.service.Iaccount_partial_reconcileClientService;
import cn.ibizlab.odoo.client.odoo_account.model.account_partial_reconcileImpl;
import cn.ibizlab.odoo.client.odoo_account.odooclient.Iaccount_partial_reconcileOdooClient;
import cn.ibizlab.odoo.client.odoo_account.odooclient.impl.account_partial_reconcileOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[account_partial_reconcile] 服务对象接口
 */
@Service
public class account_partial_reconcileClientServiceImpl implements Iaccount_partial_reconcileClientService {
    @Autowired
    private  Iaccount_partial_reconcileOdooClient  account_partial_reconcileOdooClient;

    public Iaccount_partial_reconcile createModel() {		
		return new account_partial_reconcileImpl();
	}


        public void get(Iaccount_partial_reconcile account_partial_reconcile){
            this.account_partial_reconcileOdooClient.get(account_partial_reconcile) ;
        }
        
        public Page<Iaccount_partial_reconcile> search(SearchContext context){
            return this.account_partial_reconcileOdooClient.search(context) ;
        }
        
        public void remove(Iaccount_partial_reconcile account_partial_reconcile){
this.account_partial_reconcileOdooClient.remove(account_partial_reconcile) ;
        }
        
        public void update(Iaccount_partial_reconcile account_partial_reconcile){
this.account_partial_reconcileOdooClient.update(account_partial_reconcile) ;
        }
        
        public void updateBatch(List<Iaccount_partial_reconcile> account_partial_reconciles){
            
        }
        
        public void create(Iaccount_partial_reconcile account_partial_reconcile){
this.account_partial_reconcileOdooClient.create(account_partial_reconcile) ;
        }
        
        public void removeBatch(List<Iaccount_partial_reconcile> account_partial_reconciles){
            
        }
        
        public void createBatch(List<Iaccount_partial_reconcile> account_partial_reconciles){
            
        }
        
        public Page<Iaccount_partial_reconcile> select(SearchContext context){
            return null ;
        }
        

}

