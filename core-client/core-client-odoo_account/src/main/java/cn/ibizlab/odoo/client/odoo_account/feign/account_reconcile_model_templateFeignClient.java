package cn.ibizlab.odoo.client.odoo_account.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Iaccount_reconcile_model_template;
import cn.ibizlab.odoo.client.odoo_account.model.account_reconcile_model_templateImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[account_reconcile_model_template] 服务对象接口
 */
public interface account_reconcile_model_templateFeignClient {

    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_account/account_reconcile_model_templates/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_account/account_reconcile_model_templates")
    public account_reconcile_model_templateImpl create(@RequestBody account_reconcile_model_templateImpl account_reconcile_model_template);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_account/account_reconcile_model_templates/search")
    public Page<account_reconcile_model_templateImpl> search(SearchContext context);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_account/account_reconcile_model_templates/{id}")
    public account_reconcile_model_templateImpl update(@PathVariable("id") Integer id,@RequestBody account_reconcile_model_templateImpl account_reconcile_model_template);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_account/account_reconcile_model_templates/updatebatch")
    public account_reconcile_model_templateImpl updateBatch(@RequestBody List<account_reconcile_model_templateImpl> account_reconcile_model_templates);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_account/account_reconcile_model_templates/{id}")
    public account_reconcile_model_templateImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_account/account_reconcile_model_templates/removebatch")
    public account_reconcile_model_templateImpl removeBatch(@RequestBody List<account_reconcile_model_templateImpl> account_reconcile_model_templates);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_account/account_reconcile_model_templates/createbatch")
    public account_reconcile_model_templateImpl createBatch(@RequestBody List<account_reconcile_model_templateImpl> account_reconcile_model_templates);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_account/account_reconcile_model_templates/select")
    public Page<account_reconcile_model_templateImpl> select();



}
