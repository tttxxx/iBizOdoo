package cn.ibizlab.odoo.client.odoo_account.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Iaccount_tax;
import cn.ibizlab.odoo.core.client.service.Iaccount_taxClientService;
import cn.ibizlab.odoo.client.odoo_account.model.account_taxImpl;
import cn.ibizlab.odoo.client.odoo_account.odooclient.Iaccount_taxOdooClient;
import cn.ibizlab.odoo.client.odoo_account.odooclient.impl.account_taxOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[account_tax] 服务对象接口
 */
@Service
public class account_taxClientServiceImpl implements Iaccount_taxClientService {
    @Autowired
    private  Iaccount_taxOdooClient  account_taxOdooClient;

    public Iaccount_tax createModel() {		
		return new account_taxImpl();
	}


        public void get(Iaccount_tax account_tax){
            this.account_taxOdooClient.get(account_tax) ;
        }
        
        public void create(Iaccount_tax account_tax){
this.account_taxOdooClient.create(account_tax) ;
        }
        
        public void createBatch(List<Iaccount_tax> account_taxes){
            
        }
        
        public void update(Iaccount_tax account_tax){
this.account_taxOdooClient.update(account_tax) ;
        }
        
        public void updateBatch(List<Iaccount_tax> account_taxes){
            
        }
        
        public Page<Iaccount_tax> search(SearchContext context){
            return this.account_taxOdooClient.search(context) ;
        }
        
        public void removeBatch(List<Iaccount_tax> account_taxes){
            
        }
        
        public void remove(Iaccount_tax account_tax){
this.account_taxOdooClient.remove(account_tax) ;
        }
        
        public Page<Iaccount_tax> select(SearchContext context){
            return null ;
        }
        

}

