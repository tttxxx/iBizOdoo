package cn.ibizlab.odoo.client.odoo_account.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Iaccount_group;
import cn.ibizlab.odoo.core.client.service.Iaccount_groupClientService;
import cn.ibizlab.odoo.client.odoo_account.model.account_groupImpl;
import cn.ibizlab.odoo.client.odoo_account.odooclient.Iaccount_groupOdooClient;
import cn.ibizlab.odoo.client.odoo_account.odooclient.impl.account_groupOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[account_group] 服务对象接口
 */
@Service
public class account_groupClientServiceImpl implements Iaccount_groupClientService {
    @Autowired
    private  Iaccount_groupOdooClient  account_groupOdooClient;

    public Iaccount_group createModel() {		
		return new account_groupImpl();
	}


        public void removeBatch(List<Iaccount_group> account_groups){
            
        }
        
        public Page<Iaccount_group> search(SearchContext context){
            return this.account_groupOdooClient.search(context) ;
        }
        
        public void update(Iaccount_group account_group){
this.account_groupOdooClient.update(account_group) ;
        }
        
        public void create(Iaccount_group account_group){
this.account_groupOdooClient.create(account_group) ;
        }
        
        public void createBatch(List<Iaccount_group> account_groups){
            
        }
        
        public void get(Iaccount_group account_group){
            this.account_groupOdooClient.get(account_group) ;
        }
        
        public void remove(Iaccount_group account_group){
this.account_groupOdooClient.remove(account_group) ;
        }
        
        public void updateBatch(List<Iaccount_group> account_groups){
            
        }
        
        public Page<Iaccount_group> select(SearchContext context){
            return null ;
        }
        

}

