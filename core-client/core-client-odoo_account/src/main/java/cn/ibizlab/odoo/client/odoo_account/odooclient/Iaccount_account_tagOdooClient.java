package cn.ibizlab.odoo.client.odoo_account.odooclient;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.core.client.model.Iaccount_account_tag;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[account_account_tag] 服务对象客户端接口
 */
public interface Iaccount_account_tagOdooClient {
    
        public void removeBatch(Iaccount_account_tag account_account_tag);

        public void create(Iaccount_account_tag account_account_tag);

        public void updateBatch(Iaccount_account_tag account_account_tag);

        public void createBatch(Iaccount_account_tag account_account_tag);

        public Page<Iaccount_account_tag> search(SearchContext context);

        public void update(Iaccount_account_tag account_account_tag);

        public void remove(Iaccount_account_tag account_account_tag);

        public void get(Iaccount_account_tag account_account_tag);

        public List<Iaccount_account_tag> select();


}