package cn.ibizlab.odoo.client.odoo_account.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Iaccount_common_journal_report;
import cn.ibizlab.odoo.core.client.service.Iaccount_common_journal_reportClientService;
import cn.ibizlab.odoo.client.odoo_account.model.account_common_journal_reportImpl;
import cn.ibizlab.odoo.client.odoo_account.odooclient.Iaccount_common_journal_reportOdooClient;
import cn.ibizlab.odoo.client.odoo_account.odooclient.impl.account_common_journal_reportOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[account_common_journal_report] 服务对象接口
 */
@Service
public class account_common_journal_reportClientServiceImpl implements Iaccount_common_journal_reportClientService {
    @Autowired
    private  Iaccount_common_journal_reportOdooClient  account_common_journal_reportOdooClient;

    public Iaccount_common_journal_report createModel() {		
		return new account_common_journal_reportImpl();
	}


        public Page<Iaccount_common_journal_report> search(SearchContext context){
            return this.account_common_journal_reportOdooClient.search(context) ;
        }
        
        public void createBatch(List<Iaccount_common_journal_report> account_common_journal_reports){
            
        }
        
        public void remove(Iaccount_common_journal_report account_common_journal_report){
this.account_common_journal_reportOdooClient.remove(account_common_journal_report) ;
        }
        
        public void get(Iaccount_common_journal_report account_common_journal_report){
            this.account_common_journal_reportOdooClient.get(account_common_journal_report) ;
        }
        
        public void removeBatch(List<Iaccount_common_journal_report> account_common_journal_reports){
            
        }
        
        public void create(Iaccount_common_journal_report account_common_journal_report){
this.account_common_journal_reportOdooClient.create(account_common_journal_report) ;
        }
        
        public void updateBatch(List<Iaccount_common_journal_report> account_common_journal_reports){
            
        }
        
        public void update(Iaccount_common_journal_report account_common_journal_report){
this.account_common_journal_reportOdooClient.update(account_common_journal_report) ;
        }
        
        public Page<Iaccount_common_journal_report> select(SearchContext context){
            return null ;
        }
        

}

