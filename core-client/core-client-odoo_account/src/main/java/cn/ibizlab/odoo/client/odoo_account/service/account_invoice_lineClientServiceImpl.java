package cn.ibizlab.odoo.client.odoo_account.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Iaccount_invoice_line;
import cn.ibizlab.odoo.core.client.service.Iaccount_invoice_lineClientService;
import cn.ibizlab.odoo.client.odoo_account.model.account_invoice_lineImpl;
import cn.ibizlab.odoo.client.odoo_account.odooclient.Iaccount_invoice_lineOdooClient;
import cn.ibizlab.odoo.client.odoo_account.odooclient.impl.account_invoice_lineOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[account_invoice_line] 服务对象接口
 */
@Service
public class account_invoice_lineClientServiceImpl implements Iaccount_invoice_lineClientService {
    @Autowired
    private  Iaccount_invoice_lineOdooClient  account_invoice_lineOdooClient;

    public Iaccount_invoice_line createModel() {		
		return new account_invoice_lineImpl();
	}


        public void get(Iaccount_invoice_line account_invoice_line){
            this.account_invoice_lineOdooClient.get(account_invoice_line) ;
        }
        
        public void create(Iaccount_invoice_line account_invoice_line){
this.account_invoice_lineOdooClient.create(account_invoice_line) ;
        }
        
        public void createBatch(List<Iaccount_invoice_line> account_invoice_lines){
            
        }
        
        public void update(Iaccount_invoice_line account_invoice_line){
this.account_invoice_lineOdooClient.update(account_invoice_line) ;
        }
        
        public void remove(Iaccount_invoice_line account_invoice_line){
this.account_invoice_lineOdooClient.remove(account_invoice_line) ;
        }
        
        public void updateBatch(List<Iaccount_invoice_line> account_invoice_lines){
            
        }
        
        public Page<Iaccount_invoice_line> search(SearchContext context){
            return this.account_invoice_lineOdooClient.search(context) ;
        }
        
        public void removeBatch(List<Iaccount_invoice_line> account_invoice_lines){
            
        }
        
        public Page<Iaccount_invoice_line> select(SearchContext context){
            return null ;
        }
        

}

