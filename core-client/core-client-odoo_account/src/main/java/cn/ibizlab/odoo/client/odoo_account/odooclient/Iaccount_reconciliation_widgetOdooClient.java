package cn.ibizlab.odoo.client.odoo_account.odooclient;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.core.client.model.Iaccount_reconciliation_widget;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[account_reconciliation_widget] 服务对象客户端接口
 */
public interface Iaccount_reconciliation_widgetOdooClient {
    
        public void removeBatch(Iaccount_reconciliation_widget account_reconciliation_widget);

        public void update(Iaccount_reconciliation_widget account_reconciliation_widget);

        public Page<Iaccount_reconciliation_widget> search(SearchContext context);

        public void createBatch(Iaccount_reconciliation_widget account_reconciliation_widget);

        public void get(Iaccount_reconciliation_widget account_reconciliation_widget);

        public void create(Iaccount_reconciliation_widget account_reconciliation_widget);

        public void remove(Iaccount_reconciliation_widget account_reconciliation_widget);

        public void updateBatch(Iaccount_reconciliation_widget account_reconciliation_widget);

        public List<Iaccount_reconciliation_widget> select();


}