package cn.ibizlab.odoo.client.odoo_account.odooclient;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.core.client.model.Iaccount_analytic_tag;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[account_analytic_tag] 服务对象客户端接口
 */
public interface Iaccount_analytic_tagOdooClient {
    
        public void createBatch(Iaccount_analytic_tag account_analytic_tag);

        public void updateBatch(Iaccount_analytic_tag account_analytic_tag);

        public void remove(Iaccount_analytic_tag account_analytic_tag);

        public void update(Iaccount_analytic_tag account_analytic_tag);

        public void get(Iaccount_analytic_tag account_analytic_tag);

        public void create(Iaccount_analytic_tag account_analytic_tag);

        public Page<Iaccount_analytic_tag> search(SearchContext context);

        public void removeBatch(Iaccount_analytic_tag account_analytic_tag);

        public List<Iaccount_analytic_tag> select();


}