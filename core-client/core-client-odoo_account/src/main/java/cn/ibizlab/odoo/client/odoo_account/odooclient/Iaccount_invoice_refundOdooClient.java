package cn.ibizlab.odoo.client.odoo_account.odooclient;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.core.client.model.Iaccount_invoice_refund;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[account_invoice_refund] 服务对象客户端接口
 */
public interface Iaccount_invoice_refundOdooClient {
    
        public void updateBatch(Iaccount_invoice_refund account_invoice_refund);

        public void get(Iaccount_invoice_refund account_invoice_refund);

        public void removeBatch(Iaccount_invoice_refund account_invoice_refund);

        public void remove(Iaccount_invoice_refund account_invoice_refund);

        public Page<Iaccount_invoice_refund> search(SearchContext context);

        public void update(Iaccount_invoice_refund account_invoice_refund);

        public void create(Iaccount_invoice_refund account_invoice_refund);

        public void createBatch(Iaccount_invoice_refund account_invoice_refund);

        public List<Iaccount_invoice_refund> select();


}