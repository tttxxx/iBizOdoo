package cn.ibizlab.odoo.client.odoo_account.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Iaccount_bank_statement_closebalance;
import cn.ibizlab.odoo.core.client.service.Iaccount_bank_statement_closebalanceClientService;
import cn.ibizlab.odoo.client.odoo_account.model.account_bank_statement_closebalanceImpl;
import cn.ibizlab.odoo.client.odoo_account.odooclient.Iaccount_bank_statement_closebalanceOdooClient;
import cn.ibizlab.odoo.client.odoo_account.odooclient.impl.account_bank_statement_closebalanceOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[account_bank_statement_closebalance] 服务对象接口
 */
@Service
public class account_bank_statement_closebalanceClientServiceImpl implements Iaccount_bank_statement_closebalanceClientService {
    @Autowired
    private  Iaccount_bank_statement_closebalanceOdooClient  account_bank_statement_closebalanceOdooClient;

    public Iaccount_bank_statement_closebalance createModel() {		
		return new account_bank_statement_closebalanceImpl();
	}


        public void update(Iaccount_bank_statement_closebalance account_bank_statement_closebalance){
this.account_bank_statement_closebalanceOdooClient.update(account_bank_statement_closebalance) ;
        }
        
        public void remove(Iaccount_bank_statement_closebalance account_bank_statement_closebalance){
this.account_bank_statement_closebalanceOdooClient.remove(account_bank_statement_closebalance) ;
        }
        
        public void get(Iaccount_bank_statement_closebalance account_bank_statement_closebalance){
            this.account_bank_statement_closebalanceOdooClient.get(account_bank_statement_closebalance) ;
        }
        
        public void create(Iaccount_bank_statement_closebalance account_bank_statement_closebalance){
this.account_bank_statement_closebalanceOdooClient.create(account_bank_statement_closebalance) ;
        }
        
        public void createBatch(List<Iaccount_bank_statement_closebalance> account_bank_statement_closebalances){
            
        }
        
        public Page<Iaccount_bank_statement_closebalance> search(SearchContext context){
            return this.account_bank_statement_closebalanceOdooClient.search(context) ;
        }
        
        public void removeBatch(List<Iaccount_bank_statement_closebalance> account_bank_statement_closebalances){
            
        }
        
        public void updateBatch(List<Iaccount_bank_statement_closebalance> account_bank_statement_closebalances){
            
        }
        
        public Page<Iaccount_bank_statement_closebalance> select(SearchContext context){
            return null ;
        }
        

}

