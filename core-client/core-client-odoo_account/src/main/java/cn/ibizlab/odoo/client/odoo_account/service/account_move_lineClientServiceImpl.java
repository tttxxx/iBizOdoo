package cn.ibizlab.odoo.client.odoo_account.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Iaccount_move_line;
import cn.ibizlab.odoo.core.client.service.Iaccount_move_lineClientService;
import cn.ibizlab.odoo.client.odoo_account.model.account_move_lineImpl;
import cn.ibizlab.odoo.client.odoo_account.odooclient.Iaccount_move_lineOdooClient;
import cn.ibizlab.odoo.client.odoo_account.odooclient.impl.account_move_lineOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[account_move_line] 服务对象接口
 */
@Service
public class account_move_lineClientServiceImpl implements Iaccount_move_lineClientService {
    @Autowired
    private  Iaccount_move_lineOdooClient  account_move_lineOdooClient;

    public Iaccount_move_line createModel() {		
		return new account_move_lineImpl();
	}


        public Page<Iaccount_move_line> search(SearchContext context){
            return this.account_move_lineOdooClient.search(context) ;
        }
        
        public void update(Iaccount_move_line account_move_line){
this.account_move_lineOdooClient.update(account_move_line) ;
        }
        
        public void createBatch(List<Iaccount_move_line> account_move_lines){
            
        }
        
        public void create(Iaccount_move_line account_move_line){
this.account_move_lineOdooClient.create(account_move_line) ;
        }
        
        public void remove(Iaccount_move_line account_move_line){
this.account_move_lineOdooClient.remove(account_move_line) ;
        }
        
        public void get(Iaccount_move_line account_move_line){
            this.account_move_lineOdooClient.get(account_move_line) ;
        }
        
        public void updateBatch(List<Iaccount_move_line> account_move_lines){
            
        }
        
        public void removeBatch(List<Iaccount_move_line> account_move_lines){
            
        }
        
        public Page<Iaccount_move_line> select(SearchContext context){
            return null ;
        }
        

}

