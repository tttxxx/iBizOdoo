package cn.ibizlab.odoo.client.odoo_project.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Iproject_task;
import cn.ibizlab.odoo.core.client.service.Iproject_taskClientService;
import cn.ibizlab.odoo.client.odoo_project.model.project_taskImpl;
import cn.ibizlab.odoo.client.odoo_project.odooclient.Iproject_taskOdooClient;
import cn.ibizlab.odoo.client.odoo_project.odooclient.impl.project_taskOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[project_task] 服务对象接口
 */
@Service
public class project_taskClientServiceImpl implements Iproject_taskClientService {
    @Autowired
    private  Iproject_taskOdooClient  project_taskOdooClient;

    public Iproject_task createModel() {		
		return new project_taskImpl();
	}


        public void updateBatch(List<Iproject_task> project_tasks){
            
        }
        
        public void createBatch(List<Iproject_task> project_tasks){
            
        }
        
        public void create(Iproject_task project_task){
this.project_taskOdooClient.create(project_task) ;
        }
        
        public void remove(Iproject_task project_task){
this.project_taskOdooClient.remove(project_task) ;
        }
        
        public void get(Iproject_task project_task){
            this.project_taskOdooClient.get(project_task) ;
        }
        
        public void update(Iproject_task project_task){
this.project_taskOdooClient.update(project_task) ;
        }
        
        public void removeBatch(List<Iproject_task> project_tasks){
            
        }
        
        public Page<Iproject_task> search(SearchContext context){
            return this.project_taskOdooClient.search(context) ;
        }
        
        public Page<Iproject_task> select(SearchContext context){
            return null ;
        }
        

}

