package cn.ibizlab.odoo.client.odoo_project.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Iproject_task_type;
import cn.ibizlab.odoo.client.odoo_project.model.project_task_typeImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[project_task_type] 服务对象接口
 */
public interface project_task_typeFeignClient {

    @RequestMapping(method = RequestMethod.POST, value = "/odoo_project/project_task_types/createbatch")
    public project_task_typeImpl createBatch(@RequestBody List<project_task_typeImpl> project_task_types);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_project/project_task_types/{id}")
    public project_task_typeImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_project/project_task_types/search")
    public Page<project_task_typeImpl> search(SearchContext context);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_project/project_task_types/{id}")
    public project_task_typeImpl update(@PathVariable("id") Integer id,@RequestBody project_task_typeImpl project_task_type);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_project/project_task_types/removebatch")
    public project_task_typeImpl removeBatch(@RequestBody List<project_task_typeImpl> project_task_types);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_project/project_task_types")
    public project_task_typeImpl create(@RequestBody project_task_typeImpl project_task_type);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_project/project_task_types/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_project/project_task_types/updatebatch")
    public project_task_typeImpl updateBatch(@RequestBody List<project_task_typeImpl> project_task_types);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_project/project_task_types/select")
    public Page<project_task_typeImpl> select();



}
