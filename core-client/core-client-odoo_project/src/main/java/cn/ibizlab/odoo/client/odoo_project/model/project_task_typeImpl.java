package cn.ibizlab.odoo.client.odoo_project.model;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.math.BigInteger;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.core.client.model.Iproject_task_type;
import cn.ibizlab.odoo.util.helper.OdooClientHelper;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang.ArrayUtils;
import org.springframework.util.StringUtils;

/**
 * 接口实体[project_task_type] 对象
 */
public class project_task_typeImpl implements Iproject_task_type,Serializable{

    /**
     * 自动看板状态
     */
    public String auto_validation_kanban_state;

    @JsonIgnore
    public boolean auto_validation_kanban_stateDirtyFlag;
    
    /**
     * 创建时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp create_date;

    @JsonIgnore
    public boolean create_dateDirtyFlag;
    
    /**
     * 创建人
     */
    public Integer create_uid;

    @JsonIgnore
    public boolean create_uidDirtyFlag;
    
    /**
     * 创建人
     */
    public String create_uid_text;

    @JsonIgnore
    public boolean create_uid_textDirtyFlag;
    
    /**
     * 说明
     */
    public String description;

    @JsonIgnore
    public boolean descriptionDirtyFlag;
    
    /**
     * 显示名称
     */
    public String display_name;

    @JsonIgnore
    public boolean display_nameDirtyFlag;
    
    /**
     * 集中到看板中
     */
    public String fold;

    @JsonIgnore
    public boolean foldDirtyFlag;
    
    /**
     * ID
     */
    public Integer id;

    @JsonIgnore
    public boolean idDirtyFlag;
    
    /**
     * 红色的看板标签
     */
    public String legend_blocked;

    @JsonIgnore
    public boolean legend_blockedDirtyFlag;
    
    /**
     * 绿色看板标签
     */
    public String legend_done;

    @JsonIgnore
    public boolean legend_doneDirtyFlag;
    
    /**
     * 灰色看板标签
     */
    public String legend_normal;

    @JsonIgnore
    public boolean legend_normalDirtyFlag;
    
    /**
     * 星标解释
     */
    public String legend_priority;

    @JsonIgnore
    public boolean legend_priorityDirtyFlag;
    
    /**
     * EMail模板
     */
    public Integer mail_template_id;

    @JsonIgnore
    public boolean mail_template_idDirtyFlag;
    
    /**
     * EMail模板
     */
    public String mail_template_id_text;

    @JsonIgnore
    public boolean mail_template_id_textDirtyFlag;
    
    /**
     * 阶段名称
     */
    public String name;

    @JsonIgnore
    public boolean nameDirtyFlag;
    
    /**
     * 项目
     */
    public String project_ids;

    @JsonIgnore
    public boolean project_idsDirtyFlag;
    
    /**
     * 点评邮件模板
     */
    public Integer rating_template_id;

    @JsonIgnore
    public boolean rating_template_idDirtyFlag;
    
    /**
     * 点评邮件模板
     */
    public String rating_template_id_text;

    @JsonIgnore
    public boolean rating_template_id_textDirtyFlag;
    
    /**
     * 序号
     */
    public Integer sequence;

    @JsonIgnore
    public boolean sequenceDirtyFlag;
    
    /**
     * 最后更新时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp write_date;

    @JsonIgnore
    public boolean write_dateDirtyFlag;
    
    /**
     * 最后更新人
     */
    public Integer write_uid;

    @JsonIgnore
    public boolean write_uidDirtyFlag;
    
    /**
     * 最后更新人
     */
    public String write_uid_text;

    @JsonIgnore
    public boolean write_uid_textDirtyFlag;
    
    /**
     * 最后修改日
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp __last_update;

    @JsonIgnore
    public boolean __last_updateDirtyFlag;
    
    /**
     * 获取 [自动看板状态]
     */
    @JsonProperty("auto_validation_kanban_state")
    public String getAuto_validation_kanban_state(){
        return this.auto_validation_kanban_state ;
    }

    /**
     * 设置 [自动看板状态]
     */
    @JsonProperty("auto_validation_kanban_state")
    public void setAuto_validation_kanban_state(String  auto_validation_kanban_state){
        this.auto_validation_kanban_state = auto_validation_kanban_state ;
        this.auto_validation_kanban_stateDirtyFlag = true ;
    }

     /**
     * 获取 [自动看板状态]脏标记
     */
    @JsonIgnore
    public boolean getAuto_validation_kanban_stateDirtyFlag(){
        return this.auto_validation_kanban_stateDirtyFlag ;
    }   

    /**
     * 获取 [创建时间]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return this.create_date ;
    }

    /**
     * 设置 [创建时间]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

     /**
     * 获取 [创建时间]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return this.create_dateDirtyFlag ;
    }   

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return this.create_uid ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

     /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return this.create_uidDirtyFlag ;
    }   

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return this.create_uid_text ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

     /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return this.create_uid_textDirtyFlag ;
    }   

    /**
     * 获取 [说明]
     */
    @JsonProperty("description")
    public String getDescription(){
        return this.description ;
    }

    /**
     * 设置 [说明]
     */
    @JsonProperty("description")
    public void setDescription(String  description){
        this.description = description ;
        this.descriptionDirtyFlag = true ;
    }

     /**
     * 获取 [说明]脏标记
     */
    @JsonIgnore
    public boolean getDescriptionDirtyFlag(){
        return this.descriptionDirtyFlag ;
    }   

    /**
     * 获取 [显示名称]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return this.display_name ;
    }

    /**
     * 设置 [显示名称]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

     /**
     * 获取 [显示名称]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return this.display_nameDirtyFlag ;
    }   

    /**
     * 获取 [集中到看板中]
     */
    @JsonProperty("fold")
    public String getFold(){
        return this.fold ;
    }

    /**
     * 设置 [集中到看板中]
     */
    @JsonProperty("fold")
    public void setFold(String  fold){
        this.fold = fold ;
        this.foldDirtyFlag = true ;
    }

     /**
     * 获取 [集中到看板中]脏标记
     */
    @JsonIgnore
    public boolean getFoldDirtyFlag(){
        return this.foldDirtyFlag ;
    }   

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return this.id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

     /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return this.idDirtyFlag ;
    }   

    /**
     * 获取 [红色的看板标签]
     */
    @JsonProperty("legend_blocked")
    public String getLegend_blocked(){
        return this.legend_blocked ;
    }

    /**
     * 设置 [红色的看板标签]
     */
    @JsonProperty("legend_blocked")
    public void setLegend_blocked(String  legend_blocked){
        this.legend_blocked = legend_blocked ;
        this.legend_blockedDirtyFlag = true ;
    }

     /**
     * 获取 [红色的看板标签]脏标记
     */
    @JsonIgnore
    public boolean getLegend_blockedDirtyFlag(){
        return this.legend_blockedDirtyFlag ;
    }   

    /**
     * 获取 [绿色看板标签]
     */
    @JsonProperty("legend_done")
    public String getLegend_done(){
        return this.legend_done ;
    }

    /**
     * 设置 [绿色看板标签]
     */
    @JsonProperty("legend_done")
    public void setLegend_done(String  legend_done){
        this.legend_done = legend_done ;
        this.legend_doneDirtyFlag = true ;
    }

     /**
     * 获取 [绿色看板标签]脏标记
     */
    @JsonIgnore
    public boolean getLegend_doneDirtyFlag(){
        return this.legend_doneDirtyFlag ;
    }   

    /**
     * 获取 [灰色看板标签]
     */
    @JsonProperty("legend_normal")
    public String getLegend_normal(){
        return this.legend_normal ;
    }

    /**
     * 设置 [灰色看板标签]
     */
    @JsonProperty("legend_normal")
    public void setLegend_normal(String  legend_normal){
        this.legend_normal = legend_normal ;
        this.legend_normalDirtyFlag = true ;
    }

     /**
     * 获取 [灰色看板标签]脏标记
     */
    @JsonIgnore
    public boolean getLegend_normalDirtyFlag(){
        return this.legend_normalDirtyFlag ;
    }   

    /**
     * 获取 [星标解释]
     */
    @JsonProperty("legend_priority")
    public String getLegend_priority(){
        return this.legend_priority ;
    }

    /**
     * 设置 [星标解释]
     */
    @JsonProperty("legend_priority")
    public void setLegend_priority(String  legend_priority){
        this.legend_priority = legend_priority ;
        this.legend_priorityDirtyFlag = true ;
    }

     /**
     * 获取 [星标解释]脏标记
     */
    @JsonIgnore
    public boolean getLegend_priorityDirtyFlag(){
        return this.legend_priorityDirtyFlag ;
    }   

    /**
     * 获取 [EMail模板]
     */
    @JsonProperty("mail_template_id")
    public Integer getMail_template_id(){
        return this.mail_template_id ;
    }

    /**
     * 设置 [EMail模板]
     */
    @JsonProperty("mail_template_id")
    public void setMail_template_id(Integer  mail_template_id){
        this.mail_template_id = mail_template_id ;
        this.mail_template_idDirtyFlag = true ;
    }

     /**
     * 获取 [EMail模板]脏标记
     */
    @JsonIgnore
    public boolean getMail_template_idDirtyFlag(){
        return this.mail_template_idDirtyFlag ;
    }   

    /**
     * 获取 [EMail模板]
     */
    @JsonProperty("mail_template_id_text")
    public String getMail_template_id_text(){
        return this.mail_template_id_text ;
    }

    /**
     * 设置 [EMail模板]
     */
    @JsonProperty("mail_template_id_text")
    public void setMail_template_id_text(String  mail_template_id_text){
        this.mail_template_id_text = mail_template_id_text ;
        this.mail_template_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [EMail模板]脏标记
     */
    @JsonIgnore
    public boolean getMail_template_id_textDirtyFlag(){
        return this.mail_template_id_textDirtyFlag ;
    }   

    /**
     * 获取 [阶段名称]
     */
    @JsonProperty("name")
    public String getName(){
        return this.name ;
    }

    /**
     * 设置 [阶段名称]
     */
    @JsonProperty("name")
    public void setName(String  name){
        this.name = name ;
        this.nameDirtyFlag = true ;
    }

     /**
     * 获取 [阶段名称]脏标记
     */
    @JsonIgnore
    public boolean getNameDirtyFlag(){
        return this.nameDirtyFlag ;
    }   

    /**
     * 获取 [项目]
     */
    @JsonProperty("project_ids")
    public String getProject_ids(){
        return this.project_ids ;
    }

    /**
     * 设置 [项目]
     */
    @JsonProperty("project_ids")
    public void setProject_ids(String  project_ids){
        this.project_ids = project_ids ;
        this.project_idsDirtyFlag = true ;
    }

     /**
     * 获取 [项目]脏标记
     */
    @JsonIgnore
    public boolean getProject_idsDirtyFlag(){
        return this.project_idsDirtyFlag ;
    }   

    /**
     * 获取 [点评邮件模板]
     */
    @JsonProperty("rating_template_id")
    public Integer getRating_template_id(){
        return this.rating_template_id ;
    }

    /**
     * 设置 [点评邮件模板]
     */
    @JsonProperty("rating_template_id")
    public void setRating_template_id(Integer  rating_template_id){
        this.rating_template_id = rating_template_id ;
        this.rating_template_idDirtyFlag = true ;
    }

     /**
     * 获取 [点评邮件模板]脏标记
     */
    @JsonIgnore
    public boolean getRating_template_idDirtyFlag(){
        return this.rating_template_idDirtyFlag ;
    }   

    /**
     * 获取 [点评邮件模板]
     */
    @JsonProperty("rating_template_id_text")
    public String getRating_template_id_text(){
        return this.rating_template_id_text ;
    }

    /**
     * 设置 [点评邮件模板]
     */
    @JsonProperty("rating_template_id_text")
    public void setRating_template_id_text(String  rating_template_id_text){
        this.rating_template_id_text = rating_template_id_text ;
        this.rating_template_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [点评邮件模板]脏标记
     */
    @JsonIgnore
    public boolean getRating_template_id_textDirtyFlag(){
        return this.rating_template_id_textDirtyFlag ;
    }   

    /**
     * 获取 [序号]
     */
    @JsonProperty("sequence")
    public Integer getSequence(){
        return this.sequence ;
    }

    /**
     * 设置 [序号]
     */
    @JsonProperty("sequence")
    public void setSequence(Integer  sequence){
        this.sequence = sequence ;
        this.sequenceDirtyFlag = true ;
    }

     /**
     * 获取 [序号]脏标记
     */
    @JsonIgnore
    public boolean getSequenceDirtyFlag(){
        return this.sequenceDirtyFlag ;
    }   

    /**
     * 获取 [最后更新时间]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return this.write_date ;
    }

    /**
     * 设置 [最后更新时间]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新时间]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return this.write_dateDirtyFlag ;
    }   

    /**
     * 获取 [最后更新人]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return this.write_uid ;
    }

    /**
     * 设置 [最后更新人]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新人]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return this.write_uidDirtyFlag ;
    }   

    /**
     * 获取 [最后更新人]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return this.write_uid_text ;
    }

    /**
     * 设置 [最后更新人]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新人]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return this.write_uid_textDirtyFlag ;
    }   

    /**
     * 获取 [最后修改日]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return this.__last_update ;
    }

    /**
     * 设置 [最后修改日]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

     /**
     * 获取 [最后修改日]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return this.__last_updateDirtyFlag ;
    }   



    public void fromMap(Map<String, Object> map) throws Exception {
		if(map.get("auto_validation_kanban_state") instanceof Boolean){
			this.setAuto_validation_kanban_state(((Boolean)map.get("auto_validation_kanban_state"))? "true" : "false");
		}
		if(!(map.get("create_date") instanceof Boolean)&& map.get("create_date")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse((String)map.get("create_date"));
   			this.setCreate_date(new Timestamp(parse.getTime()));
		}
		if(!(map.get("create_uid") instanceof Boolean)&& map.get("create_uid")!=null){
			Object[] objs = (Object[])map.get("create_uid");
			if(objs.length > 0){
				this.setCreate_uid((Integer)objs[0]);
			}
		}
		if(!(map.get("create_uid") instanceof Boolean)&& map.get("create_uid")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("create_uid");
			if(objs.length > 1){
				this.setCreate_uid_text((String)objs[1]);
			}
		}
		if(!(map.get("description") instanceof Boolean)&& map.get("description")!=null){
			this.setDescription((String)map.get("description"));
		}
		if(!(map.get("display_name") instanceof Boolean)&& map.get("display_name")!=null){
			this.setDisplay_name((String)map.get("display_name"));
		}
		if(map.get("fold") instanceof Boolean){
			this.setFold(((Boolean)map.get("fold"))? "true" : "false");
		}
		if(!(map.get("id") instanceof Boolean)&& map.get("id")!=null){
			this.setId((Integer)map.get("id"));
		}
		if(!(map.get("legend_blocked") instanceof Boolean)&& map.get("legend_blocked")!=null){
			this.setLegend_blocked((String)map.get("legend_blocked"));
		}
		if(!(map.get("legend_done") instanceof Boolean)&& map.get("legend_done")!=null){
			this.setLegend_done((String)map.get("legend_done"));
		}
		if(!(map.get("legend_normal") instanceof Boolean)&& map.get("legend_normal")!=null){
			this.setLegend_normal((String)map.get("legend_normal"));
		}
		if(!(map.get("legend_priority") instanceof Boolean)&& map.get("legend_priority")!=null){
			this.setLegend_priority((String)map.get("legend_priority"));
		}
		if(!(map.get("mail_template_id") instanceof Boolean)&& map.get("mail_template_id")!=null){
			Object[] objs = (Object[])map.get("mail_template_id");
			if(objs.length > 0){
				this.setMail_template_id((Integer)objs[0]);
			}
		}
		if(!(map.get("mail_template_id") instanceof Boolean)&& map.get("mail_template_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("mail_template_id");
			if(objs.length > 1){
				this.setMail_template_id_text((String)objs[1]);
			}
		}
		if(!(map.get("name") instanceof Boolean)&& map.get("name")!=null){
			this.setName((String)map.get("name"));
		}
		if(!(map.get("project_ids") instanceof Boolean)&& map.get("project_ids")!=null){
			Object[] objs = (Object[])map.get("project_ids");
			if(objs.length > 0){
				Integer[] project_ids = Arrays.copyOfRange(objs,0,objs.length,Integer[].class);
				this.setProject_ids(Arrays.toString(project_ids));
			}
		}
		if(!(map.get("rating_template_id") instanceof Boolean)&& map.get("rating_template_id")!=null){
			Object[] objs = (Object[])map.get("rating_template_id");
			if(objs.length > 0){
				this.setRating_template_id((Integer)objs[0]);
			}
		}
		if(!(map.get("rating_template_id") instanceof Boolean)&& map.get("rating_template_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("rating_template_id");
			if(objs.length > 1){
				this.setRating_template_id_text((String)objs[1]);
			}
		}
		if(!(map.get("sequence") instanceof Boolean)&& map.get("sequence")!=null){
			this.setSequence((Integer)map.get("sequence"));
		}
		if(!(map.get("write_date") instanceof Boolean)&& map.get("write_date")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse((String)map.get("write_date"));
   			this.setWrite_date(new Timestamp(parse.getTime()));
		}
		if(!(map.get("write_uid") instanceof Boolean)&& map.get("write_uid")!=null){
			Object[] objs = (Object[])map.get("write_uid");
			if(objs.length > 0){
				this.setWrite_uid((Integer)objs[0]);
			}
		}
		if(!(map.get("write_uid") instanceof Boolean)&& map.get("write_uid")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("write_uid");
			if(objs.length > 1){
				this.setWrite_uid_text((String)objs[1]);
			}
		}
		if(!(map.get("__last_update") instanceof Boolean)&& map.get("__last_update")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse((String)map.get("__last_update"));
   			this.set__last_update(new Timestamp(parse.getTime()));
		}
	}

	public Map<String, Object> toMap() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		if(this.getAuto_validation_kanban_state()!=null&&this.getAuto_validation_kanban_stateDirtyFlag()){
			map.put("auto_validation_kanban_state",Boolean.parseBoolean(this.getAuto_validation_kanban_state()));		
		}		if(this.getCreate_date()!=null&&this.getCreate_dateDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String datetimeStr = sdf.format(this.getCreate_date());
			map.put("create_date",datetimeStr);
		}else if(this.getCreate_dateDirtyFlag()){
			map.put("create_date",false);
		}
		if(this.getCreate_uid()!=null&&this.getCreate_uidDirtyFlag()){
			map.put("create_uid",this.getCreate_uid());
		}else if(this.getCreate_uidDirtyFlag()){
			map.put("create_uid",false);
		}
		if(this.getCreate_uid_text()!=null&&this.getCreate_uid_textDirtyFlag()){
			//忽略文本外键create_uid_text
		}else if(this.getCreate_uid_textDirtyFlag()){
			map.put("create_uid",false);
		}
		if(this.getDescription()!=null&&this.getDescriptionDirtyFlag()){
			map.put("description",this.getDescription());
		}else if(this.getDescriptionDirtyFlag()){
			map.put("description",false);
		}
		if(this.getDisplay_name()!=null&&this.getDisplay_nameDirtyFlag()){
			map.put("display_name",this.getDisplay_name());
		}else if(this.getDisplay_nameDirtyFlag()){
			map.put("display_name",false);
		}
		if(this.getFold()!=null&&this.getFoldDirtyFlag()){
			map.put("fold",Boolean.parseBoolean(this.getFold()));		
		}		if(this.getId()!=null&&this.getIdDirtyFlag()){
			map.put("id",this.getId());
		}else if(this.getIdDirtyFlag()){
			map.put("id",false);
		}
		if(this.getLegend_blocked()!=null&&this.getLegend_blockedDirtyFlag()){
			map.put("legend_blocked",this.getLegend_blocked());
		}else if(this.getLegend_blockedDirtyFlag()){
			map.put("legend_blocked",false);
		}
		if(this.getLegend_done()!=null&&this.getLegend_doneDirtyFlag()){
			map.put("legend_done",this.getLegend_done());
		}else if(this.getLegend_doneDirtyFlag()){
			map.put("legend_done",false);
		}
		if(this.getLegend_normal()!=null&&this.getLegend_normalDirtyFlag()){
			map.put("legend_normal",this.getLegend_normal());
		}else if(this.getLegend_normalDirtyFlag()){
			map.put("legend_normal",false);
		}
		if(this.getLegend_priority()!=null&&this.getLegend_priorityDirtyFlag()){
			map.put("legend_priority",this.getLegend_priority());
		}else if(this.getLegend_priorityDirtyFlag()){
			map.put("legend_priority",false);
		}
		if(this.getMail_template_id()!=null&&this.getMail_template_idDirtyFlag()){
			map.put("mail_template_id",this.getMail_template_id());
		}else if(this.getMail_template_idDirtyFlag()){
			map.put("mail_template_id",false);
		}
		if(this.getMail_template_id_text()!=null&&this.getMail_template_id_textDirtyFlag()){
			//忽略文本外键mail_template_id_text
		}else if(this.getMail_template_id_textDirtyFlag()){
			map.put("mail_template_id",false);
		}
		if(this.getName()!=null&&this.getNameDirtyFlag()){
			map.put("name",this.getName());
		}else if(this.getNameDirtyFlag()){
			map.put("name",false);
		}
		if(this.getProject_ids()!=null&&this.getProject_idsDirtyFlag()){
			map.put("project_ids",this.getProject_ids());
		}else if(this.getProject_idsDirtyFlag()){
			map.put("project_ids",false);
		}
		if(this.getRating_template_id()!=null&&this.getRating_template_idDirtyFlag()){
			map.put("rating_template_id",this.getRating_template_id());
		}else if(this.getRating_template_idDirtyFlag()){
			map.put("rating_template_id",false);
		}
		if(this.getRating_template_id_text()!=null&&this.getRating_template_id_textDirtyFlag()){
			//忽略文本外键rating_template_id_text
		}else if(this.getRating_template_id_textDirtyFlag()){
			map.put("rating_template_id",false);
		}
		if(this.getSequence()!=null&&this.getSequenceDirtyFlag()){
			map.put("sequence",this.getSequence());
		}else if(this.getSequenceDirtyFlag()){
			map.put("sequence",false);
		}
		if(this.getWrite_date()!=null&&this.getWrite_dateDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String datetimeStr = sdf.format(this.getWrite_date());
			map.put("write_date",datetimeStr);
		}else if(this.getWrite_dateDirtyFlag()){
			map.put("write_date",false);
		}
		if(this.getWrite_uid()!=null&&this.getWrite_uidDirtyFlag()){
			map.put("write_uid",this.getWrite_uid());
		}else if(this.getWrite_uidDirtyFlag()){
			map.put("write_uid",false);
		}
		if(this.getWrite_uid_text()!=null&&this.getWrite_uid_textDirtyFlag()){
			//忽略文本外键write_uid_text
		}else if(this.getWrite_uid_textDirtyFlag()){
			map.put("write_uid",false);
		}
		if(this.get__last_update()!=null&&this.get__last_updateDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String datetimeStr = sdf.format(this.get__last_update());
			map.put("__last_update",datetimeStr);
		}else if(this.get__last_updateDirtyFlag()){
			map.put("__last_update",false);
		}
		return map;
	}

}
