package cn.ibizlab.odoo.client.odoo_project.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Iproject_task_type;
import cn.ibizlab.odoo.core.client.service.Iproject_task_typeClientService;
import cn.ibizlab.odoo.client.odoo_project.model.project_task_typeImpl;
import cn.ibizlab.odoo.client.odoo_project.odooclient.Iproject_task_typeOdooClient;
import cn.ibizlab.odoo.client.odoo_project.odooclient.impl.project_task_typeOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[project_task_type] 服务对象接口
 */
@Service
public class project_task_typeClientServiceImpl implements Iproject_task_typeClientService {
    @Autowired
    private  Iproject_task_typeOdooClient  project_task_typeOdooClient;

    public Iproject_task_type createModel() {		
		return new project_task_typeImpl();
	}


        public void createBatch(List<Iproject_task_type> project_task_types){
            
        }
        
        public void get(Iproject_task_type project_task_type){
            this.project_task_typeOdooClient.get(project_task_type) ;
        }
        
        public Page<Iproject_task_type> search(SearchContext context){
            return this.project_task_typeOdooClient.search(context) ;
        }
        
        public void update(Iproject_task_type project_task_type){
this.project_task_typeOdooClient.update(project_task_type) ;
        }
        
        public void removeBatch(List<Iproject_task_type> project_task_types){
            
        }
        
        public void create(Iproject_task_type project_task_type){
this.project_task_typeOdooClient.create(project_task_type) ;
        }
        
        public void remove(Iproject_task_type project_task_type){
this.project_task_typeOdooClient.remove(project_task_type) ;
        }
        
        public void updateBatch(List<Iproject_task_type> project_task_types){
            
        }
        
        public Page<Iproject_task_type> select(SearchContext context){
            return null ;
        }
        

}

