package cn.ibizlab.odoo.client.odoo_project.odooclient;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.core.client.model.Iproject_task;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[project_task] 服务对象客户端接口
 */
public interface Iproject_taskOdooClient {
    
        public void updateBatch(Iproject_task project_task);

        public void createBatch(Iproject_task project_task);

        public void create(Iproject_task project_task);

        public void remove(Iproject_task project_task);

        public void get(Iproject_task project_task);

        public void update(Iproject_task project_task);

        public void removeBatch(Iproject_task project_task);

        public Page<Iproject_task> search(SearchContext context);

        public List<Iproject_task> select();


}