package cn.ibizlab.odoo.client.odoo_project.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import lombok.Data;

@ConfigurationProperties(prefix = "client.odoo.project")
@Data
public class odoo_projectClientProperties {

	private String tokenUrl ;

	private String clientId ;

	private String clientSecret ;

}
