package cn.ibizlab.odoo.client.odoo_project.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Iproject_tags;
import cn.ibizlab.odoo.core.client.service.Iproject_tagsClientService;
import cn.ibizlab.odoo.client.odoo_project.model.project_tagsImpl;
import cn.ibizlab.odoo.client.odoo_project.odooclient.Iproject_tagsOdooClient;
import cn.ibizlab.odoo.client.odoo_project.odooclient.impl.project_tagsOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[project_tags] 服务对象接口
 */
@Service
public class project_tagsClientServiceImpl implements Iproject_tagsClientService {
    @Autowired
    private  Iproject_tagsOdooClient  project_tagsOdooClient;

    public Iproject_tags createModel() {		
		return new project_tagsImpl();
	}


        public Page<Iproject_tags> search(SearchContext context){
            return this.project_tagsOdooClient.search(context) ;
        }
        
        public void remove(Iproject_tags project_tags){
this.project_tagsOdooClient.remove(project_tags) ;
        }
        
        public void update(Iproject_tags project_tags){
this.project_tagsOdooClient.update(project_tags) ;
        }
        
        public void removeBatch(List<Iproject_tags> project_tags){
            
        }
        
        public void create(Iproject_tags project_tags){
this.project_tagsOdooClient.create(project_tags) ;
        }
        
        public void createBatch(List<Iproject_tags> project_tags){
            
        }
        
        public void get(Iproject_tags project_tags){
            this.project_tagsOdooClient.get(project_tags) ;
        }
        
        public void updateBatch(List<Iproject_tags> project_tags){
            
        }
        
        public Page<Iproject_tags> select(SearchContext context){
            return null ;
        }
        

}

