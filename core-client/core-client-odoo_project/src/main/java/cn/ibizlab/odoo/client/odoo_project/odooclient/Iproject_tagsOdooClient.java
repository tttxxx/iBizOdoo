package cn.ibizlab.odoo.client.odoo_project.odooclient;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.core.client.model.Iproject_tags;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[project_tags] 服务对象客户端接口
 */
public interface Iproject_tagsOdooClient {
    
        public Page<Iproject_tags> search(SearchContext context);

        public void remove(Iproject_tags project_tags);

        public void update(Iproject_tags project_tags);

        public void removeBatch(Iproject_tags project_tags);

        public void create(Iproject_tags project_tags);

        public void createBatch(Iproject_tags project_tags);

        public void get(Iproject_tags project_tags);

        public void updateBatch(Iproject_tags project_tags);

        public List<Iproject_tags> select();


}