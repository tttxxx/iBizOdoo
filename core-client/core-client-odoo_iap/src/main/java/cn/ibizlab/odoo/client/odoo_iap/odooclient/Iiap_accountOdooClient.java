package cn.ibizlab.odoo.client.odoo_iap.odooclient;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.core.client.model.Iiap_account;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[iap_account] 服务对象客户端接口
 */
public interface Iiap_accountOdooClient {
    
        public void createBatch(Iiap_account iap_account);

        public void create(Iiap_account iap_account);

        public void removeBatch(Iiap_account iap_account);

        public void updateBatch(Iiap_account iap_account);

        public void remove(Iiap_account iap_account);

        public void update(Iiap_account iap_account);

        public Page<Iiap_account> search(SearchContext context);

        public void get(Iiap_account iap_account);

        public List<Iiap_account> select();


}