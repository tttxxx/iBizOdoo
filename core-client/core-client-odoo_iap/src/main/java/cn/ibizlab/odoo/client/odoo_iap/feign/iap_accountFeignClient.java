package cn.ibizlab.odoo.client.odoo_iap.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Iiap_account;
import cn.ibizlab.odoo.client.odoo_iap.model.iap_accountImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[iap_account] 服务对象接口
 */
public interface iap_accountFeignClient {

    @RequestMapping(method = RequestMethod.POST, value = "/odoo_iap/iap_accounts/createbatch")
    public iap_accountImpl createBatch(@RequestBody List<iap_accountImpl> iap_accounts);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_iap/iap_accounts")
    public iap_accountImpl create(@RequestBody iap_accountImpl iap_account);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_iap/iap_accounts/removebatch")
    public iap_accountImpl removeBatch(@RequestBody List<iap_accountImpl> iap_accounts);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_iap/iap_accounts/updatebatch")
    public iap_accountImpl updateBatch(@RequestBody List<iap_accountImpl> iap_accounts);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_iap/iap_accounts/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_iap/iap_accounts/{id}")
    public iap_accountImpl update(@PathVariable("id") Integer id,@RequestBody iap_accountImpl iap_account);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_iap/iap_accounts/search")
    public Page<iap_accountImpl> search(SearchContext context);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_iap/iap_accounts/{id}")
    public iap_accountImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_iap/iap_accounts/select")
    public Page<iap_accountImpl> select();



}
