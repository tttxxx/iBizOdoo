package cn.ibizlab.odoo.client.odoo_asset.odooclient;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.core.client.model.Iasset_state;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[asset_state] 服务对象客户端接口
 */
public interface Iasset_stateOdooClient {
    
        public void updateBatch(Iasset_state asset_state);

        public void get(Iasset_state asset_state);

        public void create(Iasset_state asset_state);

        public void removeBatch(Iasset_state asset_state);

        public void remove(Iasset_state asset_state);

        public void createBatch(Iasset_state asset_state);

        public Page<Iasset_state> search(SearchContext context);

        public void update(Iasset_state asset_state);

        public List<Iasset_state> select();


}