package cn.ibizlab.odoo.client.odoo_asset.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Iasset_category;
import cn.ibizlab.odoo.client.odoo_asset.model.asset_categoryImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[asset_category] 服务对象接口
 */
public interface asset_categoryFeignClient {

    @RequestMapping(method = RequestMethod.POST, value = "/odoo_asset/asset_categories")
    public asset_categoryImpl create(@RequestBody asset_categoryImpl asset_category);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_asset/asset_categories/{id}")
    public asset_categoryImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_asset/asset_categories/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_asset/asset_categories/search")
    public Page<asset_categoryImpl> search(SearchContext context);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_asset/asset_categories/removebatch")
    public asset_categoryImpl removeBatch(@RequestBody List<asset_categoryImpl> asset_categories);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_asset/asset_categories/createbatch")
    public asset_categoryImpl createBatch(@RequestBody List<asset_categoryImpl> asset_categories);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_asset/asset_categories/{id}")
    public asset_categoryImpl update(@PathVariable("id") Integer id,@RequestBody asset_categoryImpl asset_category);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_asset/asset_categories/updatebatch")
    public asset_categoryImpl updateBatch(@RequestBody List<asset_categoryImpl> asset_categories);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_asset/asset_categories/select")
    public Page<asset_categoryImpl> select();



}
