package cn.ibizlab.odoo.client.odoo_asset.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Iasset_category;
import cn.ibizlab.odoo.core.client.service.Iasset_categoryClientService;
import cn.ibizlab.odoo.client.odoo_asset.model.asset_categoryImpl;
import cn.ibizlab.odoo.client.odoo_asset.odooclient.Iasset_categoryOdooClient;
import cn.ibizlab.odoo.client.odoo_asset.odooclient.impl.asset_categoryOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[asset_category] 服务对象接口
 */
@Service
public class asset_categoryClientServiceImpl implements Iasset_categoryClientService {
    @Autowired
    private  Iasset_categoryOdooClient  asset_categoryOdooClient;

    public Iasset_category createModel() {		
		return new asset_categoryImpl();
	}


        public void create(Iasset_category asset_category){
this.asset_categoryOdooClient.create(asset_category) ;
        }
        
        public void get(Iasset_category asset_category){
            this.asset_categoryOdooClient.get(asset_category) ;
        }
        
        public void remove(Iasset_category asset_category){
this.asset_categoryOdooClient.remove(asset_category) ;
        }
        
        public Page<Iasset_category> search(SearchContext context){
            return this.asset_categoryOdooClient.search(context) ;
        }
        
        public void removeBatch(List<Iasset_category> asset_categories){
            
        }
        
        public void createBatch(List<Iasset_category> asset_categories){
            
        }
        
        public void update(Iasset_category asset_category){
this.asset_categoryOdooClient.update(asset_category) ;
        }
        
        public void updateBatch(List<Iasset_category> asset_categories){
            
        }
        
        public Page<Iasset_category> select(SearchContext context){
            return null ;
        }
        

}

