package cn.ibizlab.odoo.client.odoo_lunch.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Ilunch_order_line_lucky;
import cn.ibizlab.odoo.core.client.service.Ilunch_order_line_luckyClientService;
import cn.ibizlab.odoo.client.odoo_lunch.model.lunch_order_line_luckyImpl;
import cn.ibizlab.odoo.client.odoo_lunch.odooclient.Ilunch_order_line_luckyOdooClient;
import cn.ibizlab.odoo.client.odoo_lunch.odooclient.impl.lunch_order_line_luckyOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[lunch_order_line_lucky] 服务对象接口
 */
@Service
public class lunch_order_line_luckyClientServiceImpl implements Ilunch_order_line_luckyClientService {
    @Autowired
    private  Ilunch_order_line_luckyOdooClient  lunch_order_line_luckyOdooClient;

    public Ilunch_order_line_lucky createModel() {		
		return new lunch_order_line_luckyImpl();
	}


        public Page<Ilunch_order_line_lucky> search(SearchContext context){
            return this.lunch_order_line_luckyOdooClient.search(context) ;
        }
        
        public void get(Ilunch_order_line_lucky lunch_order_line_lucky){
            this.lunch_order_line_luckyOdooClient.get(lunch_order_line_lucky) ;
        }
        
        public void createBatch(List<Ilunch_order_line_lucky> lunch_order_line_luckies){
            
        }
        
        public void updateBatch(List<Ilunch_order_line_lucky> lunch_order_line_luckies){
            
        }
        
        public void create(Ilunch_order_line_lucky lunch_order_line_lucky){
this.lunch_order_line_luckyOdooClient.create(lunch_order_line_lucky) ;
        }
        
        public void update(Ilunch_order_line_lucky lunch_order_line_lucky){
this.lunch_order_line_luckyOdooClient.update(lunch_order_line_lucky) ;
        }
        
        public void remove(Ilunch_order_line_lucky lunch_order_line_lucky){
this.lunch_order_line_luckyOdooClient.remove(lunch_order_line_lucky) ;
        }
        
        public void removeBatch(List<Ilunch_order_line_lucky> lunch_order_line_luckies){
            
        }
        
        public Page<Ilunch_order_line_lucky> select(SearchContext context){
            return null ;
        }
        

}

