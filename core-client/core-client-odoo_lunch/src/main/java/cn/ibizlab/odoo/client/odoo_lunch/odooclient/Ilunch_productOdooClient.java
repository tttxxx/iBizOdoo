package cn.ibizlab.odoo.client.odoo_lunch.odooclient;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.core.client.model.Ilunch_product;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[lunch_product] 服务对象客户端接口
 */
public interface Ilunch_productOdooClient {
    
        public void create(Ilunch_product lunch_product);

        public void get(Ilunch_product lunch_product);

        public void createBatch(Ilunch_product lunch_product);

        public void updateBatch(Ilunch_product lunch_product);

        public Page<Ilunch_product> search(SearchContext context);

        public void removeBatch(Ilunch_product lunch_product);

        public void update(Ilunch_product lunch_product);

        public void remove(Ilunch_product lunch_product);

        public List<Ilunch_product> select();


}