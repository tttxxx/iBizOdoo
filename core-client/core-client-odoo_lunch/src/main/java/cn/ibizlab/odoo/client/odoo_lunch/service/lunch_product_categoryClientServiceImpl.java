package cn.ibizlab.odoo.client.odoo_lunch.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Ilunch_product_category;
import cn.ibizlab.odoo.core.client.service.Ilunch_product_categoryClientService;
import cn.ibizlab.odoo.client.odoo_lunch.model.lunch_product_categoryImpl;
import cn.ibizlab.odoo.client.odoo_lunch.odooclient.Ilunch_product_categoryOdooClient;
import cn.ibizlab.odoo.client.odoo_lunch.odooclient.impl.lunch_product_categoryOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[lunch_product_category] 服务对象接口
 */
@Service
public class lunch_product_categoryClientServiceImpl implements Ilunch_product_categoryClientService {
    @Autowired
    private  Ilunch_product_categoryOdooClient  lunch_product_categoryOdooClient;

    public Ilunch_product_category createModel() {		
		return new lunch_product_categoryImpl();
	}


        public void create(Ilunch_product_category lunch_product_category){
this.lunch_product_categoryOdooClient.create(lunch_product_category) ;
        }
        
        public void remove(Ilunch_product_category lunch_product_category){
this.lunch_product_categoryOdooClient.remove(lunch_product_category) ;
        }
        
        public void updateBatch(List<Ilunch_product_category> lunch_product_categories){
            
        }
        
        public void update(Ilunch_product_category lunch_product_category){
this.lunch_product_categoryOdooClient.update(lunch_product_category) ;
        }
        
        public void get(Ilunch_product_category lunch_product_category){
            this.lunch_product_categoryOdooClient.get(lunch_product_category) ;
        }
        
        public void createBatch(List<Ilunch_product_category> lunch_product_categories){
            
        }
        
        public void removeBatch(List<Ilunch_product_category> lunch_product_categories){
            
        }
        
        public Page<Ilunch_product_category> search(SearchContext context){
            return this.lunch_product_categoryOdooClient.search(context) ;
        }
        
        public Page<Ilunch_product_category> select(SearchContext context){
            return null ;
        }
        

}

