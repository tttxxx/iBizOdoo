package cn.ibizlab.odoo.client.odoo_lunch.odooclient;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.core.client.model.Ilunch_alert;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[lunch_alert] 服务对象客户端接口
 */
public interface Ilunch_alertOdooClient {
    
        public void removeBatch(Ilunch_alert lunch_alert);

        public void createBatch(Ilunch_alert lunch_alert);

        public Page<Ilunch_alert> search(SearchContext context);

        public void remove(Ilunch_alert lunch_alert);

        public void get(Ilunch_alert lunch_alert);

        public void updateBatch(Ilunch_alert lunch_alert);

        public void update(Ilunch_alert lunch_alert);

        public void create(Ilunch_alert lunch_alert);

        public List<Ilunch_alert> select();


}