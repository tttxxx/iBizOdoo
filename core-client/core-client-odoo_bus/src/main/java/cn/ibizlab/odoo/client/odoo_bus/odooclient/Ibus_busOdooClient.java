package cn.ibizlab.odoo.client.odoo_bus.odooclient;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.core.client.model.Ibus_bus;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[bus_bus] 服务对象客户端接口
 */
public interface Ibus_busOdooClient {
    
        public void remove(Ibus_bus bus_bus);

        public void update(Ibus_bus bus_bus);

        public Page<Ibus_bus> search(SearchContext context);

        public void create(Ibus_bus bus_bus);

        public void createBatch(Ibus_bus bus_bus);

        public void get(Ibus_bus bus_bus);

        public void updateBatch(Ibus_bus bus_bus);

        public void removeBatch(Ibus_bus bus_bus);

        public List<Ibus_bus> select();


}