package cn.ibizlab.odoo.client.odoo_sale.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Isale_advance_payment_inv;
import cn.ibizlab.odoo.core.client.service.Isale_advance_payment_invClientService;
import cn.ibizlab.odoo.client.odoo_sale.model.sale_advance_payment_invImpl;
import cn.ibizlab.odoo.client.odoo_sale.odooclient.Isale_advance_payment_invOdooClient;
import cn.ibizlab.odoo.client.odoo_sale.odooclient.impl.sale_advance_payment_invOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[sale_advance_payment_inv] 服务对象接口
 */
@Service
public class sale_advance_payment_invClientServiceImpl implements Isale_advance_payment_invClientService {
    @Autowired
    private  Isale_advance_payment_invOdooClient  sale_advance_payment_invOdooClient;

    public Isale_advance_payment_inv createModel() {		
		return new sale_advance_payment_invImpl();
	}


        public void create(Isale_advance_payment_inv sale_advance_payment_inv){
this.sale_advance_payment_invOdooClient.create(sale_advance_payment_inv) ;
        }
        
        public void removeBatch(List<Isale_advance_payment_inv> sale_advance_payment_invs){
            
        }
        
        public void updateBatch(List<Isale_advance_payment_inv> sale_advance_payment_invs){
            
        }
        
        public void get(Isale_advance_payment_inv sale_advance_payment_inv){
            this.sale_advance_payment_invOdooClient.get(sale_advance_payment_inv) ;
        }
        
        public void remove(Isale_advance_payment_inv sale_advance_payment_inv){
this.sale_advance_payment_invOdooClient.remove(sale_advance_payment_inv) ;
        }
        
        public void update(Isale_advance_payment_inv sale_advance_payment_inv){
this.sale_advance_payment_invOdooClient.update(sale_advance_payment_inv) ;
        }
        
        public Page<Isale_advance_payment_inv> search(SearchContext context){
            return this.sale_advance_payment_invOdooClient.search(context) ;
        }
        
        public void createBatch(List<Isale_advance_payment_inv> sale_advance_payment_invs){
            
        }
        
        public Page<Isale_advance_payment_inv> select(SearchContext context){
            return null ;
        }
        

}

