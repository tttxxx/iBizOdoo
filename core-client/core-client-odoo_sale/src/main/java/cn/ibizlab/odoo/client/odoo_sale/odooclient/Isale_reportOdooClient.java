package cn.ibizlab.odoo.client.odoo_sale.odooclient;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.core.client.model.Isale_report;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[sale_report] 服务对象客户端接口
 */
public interface Isale_reportOdooClient {
    
        public Page<Isale_report> search(SearchContext context);

        public void get(Isale_report sale_report);

        public void removeBatch(Isale_report sale_report);

        public void update(Isale_report sale_report);

        public void remove(Isale_report sale_report);

        public void create(Isale_report sale_report);

        public void createBatch(Isale_report sale_report);

        public void updateBatch(Isale_report sale_report);

        public List<Isale_report> select();


}