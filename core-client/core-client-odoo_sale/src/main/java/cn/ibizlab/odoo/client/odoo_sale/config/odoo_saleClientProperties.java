package cn.ibizlab.odoo.client.odoo_sale.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import lombok.Data;

@ConfigurationProperties(prefix = "client.odoo.sale")
@Data
public class odoo_saleClientProperties {

	private String tokenUrl ;

	private String clientId ;

	private String clientSecret ;

}
