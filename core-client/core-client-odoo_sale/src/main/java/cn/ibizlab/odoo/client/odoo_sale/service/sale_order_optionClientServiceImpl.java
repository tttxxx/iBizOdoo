package cn.ibizlab.odoo.client.odoo_sale.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Isale_order_option;
import cn.ibizlab.odoo.core.client.service.Isale_order_optionClientService;
import cn.ibizlab.odoo.client.odoo_sale.model.sale_order_optionImpl;
import cn.ibizlab.odoo.client.odoo_sale.odooclient.Isale_order_optionOdooClient;
import cn.ibizlab.odoo.client.odoo_sale.odooclient.impl.sale_order_optionOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[sale_order_option] 服务对象接口
 */
@Service
public class sale_order_optionClientServiceImpl implements Isale_order_optionClientService {
    @Autowired
    private  Isale_order_optionOdooClient  sale_order_optionOdooClient;

    public Isale_order_option createModel() {		
		return new sale_order_optionImpl();
	}


        public void remove(Isale_order_option sale_order_option){
this.sale_order_optionOdooClient.remove(sale_order_option) ;
        }
        
        public void createBatch(List<Isale_order_option> sale_order_options){
            
        }
        
        public void removeBatch(List<Isale_order_option> sale_order_options){
            
        }
        
        public void update(Isale_order_option sale_order_option){
this.sale_order_optionOdooClient.update(sale_order_option) ;
        }
        
        public void updateBatch(List<Isale_order_option> sale_order_options){
            
        }
        
        public void get(Isale_order_option sale_order_option){
            this.sale_order_optionOdooClient.get(sale_order_option) ;
        }
        
        public Page<Isale_order_option> search(SearchContext context){
            return this.sale_order_optionOdooClient.search(context) ;
        }
        
        public void create(Isale_order_option sale_order_option){
this.sale_order_optionOdooClient.create(sale_order_option) ;
        }
        
        public Page<Isale_order_option> select(SearchContext context){
            return null ;
        }
        

}

