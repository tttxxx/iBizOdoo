package cn.ibizlab.odoo.client.odoo_sale.odooclient;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.core.client.model.Isale_product_configurator;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[sale_product_configurator] 服务对象客户端接口
 */
public interface Isale_product_configuratorOdooClient {
    
        public void create(Isale_product_configurator sale_product_configurator);

        public void createBatch(Isale_product_configurator sale_product_configurator);

        public void removeBatch(Isale_product_configurator sale_product_configurator);

        public Page<Isale_product_configurator> search(SearchContext context);

        public void get(Isale_product_configurator sale_product_configurator);

        public void update(Isale_product_configurator sale_product_configurator);

        public void updateBatch(Isale_product_configurator sale_product_configurator);

        public void remove(Isale_product_configurator sale_product_configurator);

        public List<Isale_product_configurator> select();


}