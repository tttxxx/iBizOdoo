package cn.ibizlab.odoo.client.odoo_sale.odooclient;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.core.client.model.Isale_order_template;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[sale_order_template] 服务对象客户端接口
 */
public interface Isale_order_templateOdooClient {
    
        public Page<Isale_order_template> search(SearchContext context);

        public void create(Isale_order_template sale_order_template);

        public void remove(Isale_order_template sale_order_template);

        public void updateBatch(Isale_order_template sale_order_template);

        public void update(Isale_order_template sale_order_template);

        public void get(Isale_order_template sale_order_template);

        public void removeBatch(Isale_order_template sale_order_template);

        public void createBatch(Isale_order_template sale_order_template);

        public List<Isale_order_template> select();


}