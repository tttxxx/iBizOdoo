package cn.ibizlab.odoo.client.odoo_sale.odooclient;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.core.client.model.Isale_advance_payment_inv;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[sale_advance_payment_inv] 服务对象客户端接口
 */
public interface Isale_advance_payment_invOdooClient {
    
        public void create(Isale_advance_payment_inv sale_advance_payment_inv);

        public void removeBatch(Isale_advance_payment_inv sale_advance_payment_inv);

        public void updateBatch(Isale_advance_payment_inv sale_advance_payment_inv);

        public void get(Isale_advance_payment_inv sale_advance_payment_inv);

        public void remove(Isale_advance_payment_inv sale_advance_payment_inv);

        public void update(Isale_advance_payment_inv sale_advance_payment_inv);

        public Page<Isale_advance_payment_inv> search(SearchContext context);

        public void createBatch(Isale_advance_payment_inv sale_advance_payment_inv);

        public List<Isale_advance_payment_inv> select();


}