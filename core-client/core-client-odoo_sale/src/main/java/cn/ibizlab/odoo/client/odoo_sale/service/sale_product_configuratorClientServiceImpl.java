package cn.ibizlab.odoo.client.odoo_sale.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Isale_product_configurator;
import cn.ibizlab.odoo.core.client.service.Isale_product_configuratorClientService;
import cn.ibizlab.odoo.client.odoo_sale.model.sale_product_configuratorImpl;
import cn.ibizlab.odoo.client.odoo_sale.odooclient.Isale_product_configuratorOdooClient;
import cn.ibizlab.odoo.client.odoo_sale.odooclient.impl.sale_product_configuratorOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[sale_product_configurator] 服务对象接口
 */
@Service
public class sale_product_configuratorClientServiceImpl implements Isale_product_configuratorClientService {
    @Autowired
    private  Isale_product_configuratorOdooClient  sale_product_configuratorOdooClient;

    public Isale_product_configurator createModel() {		
		return new sale_product_configuratorImpl();
	}


        public void create(Isale_product_configurator sale_product_configurator){
this.sale_product_configuratorOdooClient.create(sale_product_configurator) ;
        }
        
        public void createBatch(List<Isale_product_configurator> sale_product_configurators){
            
        }
        
        public void removeBatch(List<Isale_product_configurator> sale_product_configurators){
            
        }
        
        public Page<Isale_product_configurator> search(SearchContext context){
            return this.sale_product_configuratorOdooClient.search(context) ;
        }
        
        public void get(Isale_product_configurator sale_product_configurator){
            this.sale_product_configuratorOdooClient.get(sale_product_configurator) ;
        }
        
        public void update(Isale_product_configurator sale_product_configurator){
this.sale_product_configuratorOdooClient.update(sale_product_configurator) ;
        }
        
        public void updateBatch(List<Isale_product_configurator> sale_product_configurators){
            
        }
        
        public void remove(Isale_product_configurator sale_product_configurator){
this.sale_product_configuratorOdooClient.remove(sale_product_configurator) ;
        }
        
        public Page<Isale_product_configurator> select(SearchContext context){
            return null ;
        }
        

}

