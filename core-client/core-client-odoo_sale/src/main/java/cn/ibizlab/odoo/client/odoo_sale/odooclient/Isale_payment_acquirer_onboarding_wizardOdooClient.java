package cn.ibizlab.odoo.client.odoo_sale.odooclient;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.core.client.model.Isale_payment_acquirer_onboarding_wizard;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[sale_payment_acquirer_onboarding_wizard] 服务对象客户端接口
 */
public interface Isale_payment_acquirer_onboarding_wizardOdooClient {
    
        public void update(Isale_payment_acquirer_onboarding_wizard sale_payment_acquirer_onboarding_wizard);

        public void createBatch(Isale_payment_acquirer_onboarding_wizard sale_payment_acquirer_onboarding_wizard);

        public void remove(Isale_payment_acquirer_onboarding_wizard sale_payment_acquirer_onboarding_wizard);

        public void get(Isale_payment_acquirer_onboarding_wizard sale_payment_acquirer_onboarding_wizard);

        public void removeBatch(Isale_payment_acquirer_onboarding_wizard sale_payment_acquirer_onboarding_wizard);

        public Page<Isale_payment_acquirer_onboarding_wizard> search(SearchContext context);

        public void create(Isale_payment_acquirer_onboarding_wizard sale_payment_acquirer_onboarding_wizard);

        public void updateBatch(Isale_payment_acquirer_onboarding_wizard sale_payment_acquirer_onboarding_wizard);

        public List<Isale_payment_acquirer_onboarding_wizard> select();


}