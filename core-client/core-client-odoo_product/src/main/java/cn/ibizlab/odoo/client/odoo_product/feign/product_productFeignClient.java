package cn.ibizlab.odoo.client.odoo_product.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Iproduct_product;
import cn.ibizlab.odoo.client.odoo_product.model.product_productImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[product_product] 服务对象接口
 */
public interface product_productFeignClient {

    @RequestMapping(method = RequestMethod.POST, value = "/odoo_product/product_products")
    public product_productImpl create(@RequestBody product_productImpl product_product);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_product/product_products/{id}")
    public product_productImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_product/product_products/search")
    public Page<product_productImpl> search(SearchContext context);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_product/product_products/removebatch")
    public product_productImpl removeBatch(@RequestBody List<product_productImpl> product_products);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_product/product_products/createbatch")
    public product_productImpl createBatch(@RequestBody List<product_productImpl> product_products);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_product/product_products/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_product/product_products/updatebatch")
    public product_productImpl updateBatch(@RequestBody List<product_productImpl> product_products);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_product/product_products/{id}")
    public product_productImpl update(@PathVariable("id") Integer id,@RequestBody product_productImpl product_product);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_product/product_products/select")
    public Page<product_productImpl> select();



}
