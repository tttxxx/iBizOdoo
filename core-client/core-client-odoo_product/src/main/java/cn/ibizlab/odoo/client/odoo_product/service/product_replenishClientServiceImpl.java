package cn.ibizlab.odoo.client.odoo_product.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Iproduct_replenish;
import cn.ibizlab.odoo.core.client.service.Iproduct_replenishClientService;
import cn.ibizlab.odoo.client.odoo_product.model.product_replenishImpl;
import cn.ibizlab.odoo.client.odoo_product.odooclient.Iproduct_replenishOdooClient;
import cn.ibizlab.odoo.client.odoo_product.odooclient.impl.product_replenishOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[product_replenish] 服务对象接口
 */
@Service
public class product_replenishClientServiceImpl implements Iproduct_replenishClientService {
    @Autowired
    private  Iproduct_replenishOdooClient  product_replenishOdooClient;

    public Iproduct_replenish createModel() {		
		return new product_replenishImpl();
	}


        public void removeBatch(List<Iproduct_replenish> product_replenishes){
            
        }
        
        public Page<Iproduct_replenish> search(SearchContext context){
            return this.product_replenishOdooClient.search(context) ;
        }
        
        public void createBatch(List<Iproduct_replenish> product_replenishes){
            
        }
        
        public void updateBatch(List<Iproduct_replenish> product_replenishes){
            
        }
        
        public void remove(Iproduct_replenish product_replenish){
this.product_replenishOdooClient.remove(product_replenish) ;
        }
        
        public void update(Iproduct_replenish product_replenish){
this.product_replenishOdooClient.update(product_replenish) ;
        }
        
        public void get(Iproduct_replenish product_replenish){
            this.product_replenishOdooClient.get(product_replenish) ;
        }
        
        public void create(Iproduct_replenish product_replenish){
this.product_replenishOdooClient.create(product_replenish) ;
        }
        
        public Page<Iproduct_replenish> select(SearchContext context){
            return null ;
        }
        

}

