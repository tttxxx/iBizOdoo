package cn.ibizlab.odoo.client.odoo_product.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Iproduct_attribute_custom_value;
import cn.ibizlab.odoo.core.client.service.Iproduct_attribute_custom_valueClientService;
import cn.ibizlab.odoo.client.odoo_product.model.product_attribute_custom_valueImpl;
import cn.ibizlab.odoo.client.odoo_product.odooclient.Iproduct_attribute_custom_valueOdooClient;
import cn.ibizlab.odoo.client.odoo_product.odooclient.impl.product_attribute_custom_valueOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[product_attribute_custom_value] 服务对象接口
 */
@Service
public class product_attribute_custom_valueClientServiceImpl implements Iproduct_attribute_custom_valueClientService {
    @Autowired
    private  Iproduct_attribute_custom_valueOdooClient  product_attribute_custom_valueOdooClient;

    public Iproduct_attribute_custom_value createModel() {		
		return new product_attribute_custom_valueImpl();
	}


        public void get(Iproduct_attribute_custom_value product_attribute_custom_value){
            this.product_attribute_custom_valueOdooClient.get(product_attribute_custom_value) ;
        }
        
        public Page<Iproduct_attribute_custom_value> search(SearchContext context){
            return this.product_attribute_custom_valueOdooClient.search(context) ;
        }
        
        public void createBatch(List<Iproduct_attribute_custom_value> product_attribute_custom_values){
            
        }
        
        public void removeBatch(List<Iproduct_attribute_custom_value> product_attribute_custom_values){
            
        }
        
        public void create(Iproduct_attribute_custom_value product_attribute_custom_value){
this.product_attribute_custom_valueOdooClient.create(product_attribute_custom_value) ;
        }
        
        public void updateBatch(List<Iproduct_attribute_custom_value> product_attribute_custom_values){
            
        }
        
        public void update(Iproduct_attribute_custom_value product_attribute_custom_value){
this.product_attribute_custom_valueOdooClient.update(product_attribute_custom_value) ;
        }
        
        public void remove(Iproduct_attribute_custom_value product_attribute_custom_value){
this.product_attribute_custom_valueOdooClient.remove(product_attribute_custom_value) ;
        }
        
        public Page<Iproduct_attribute_custom_value> select(SearchContext context){
            return null ;
        }
        

}

