package cn.ibizlab.odoo.client.odoo_product.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Iproduct_template;
import cn.ibizlab.odoo.core.client.service.Iproduct_templateClientService;
import cn.ibizlab.odoo.client.odoo_product.model.product_templateImpl;
import cn.ibizlab.odoo.client.odoo_product.odooclient.Iproduct_templateOdooClient;
import cn.ibizlab.odoo.client.odoo_product.odooclient.impl.product_templateOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[product_template] 服务对象接口
 */
@Service
public class product_templateClientServiceImpl implements Iproduct_templateClientService {
    @Autowired
    private  Iproduct_templateOdooClient  product_templateOdooClient;

    public Iproduct_template createModel() {		
		return new product_templateImpl();
	}


        public void remove(Iproduct_template product_template){
this.product_templateOdooClient.remove(product_template) ;
        }
        
        public void create(Iproduct_template product_template){
this.product_templateOdooClient.create(product_template) ;
        }
        
        public void update(Iproduct_template product_template){
this.product_templateOdooClient.update(product_template) ;
        }
        
        public void removeBatch(List<Iproduct_template> product_templates){
            
        }
        
        public void updateBatch(List<Iproduct_template> product_templates){
            
        }
        
        public void get(Iproduct_template product_template){
            this.product_templateOdooClient.get(product_template) ;
        }
        
        public Page<Iproduct_template> search(SearchContext context){
            return this.product_templateOdooClient.search(context) ;
        }
        
        public void createBatch(List<Iproduct_template> product_templates){
            
        }
        
        public Page<Iproduct_template> select(SearchContext context){
            return null ;
        }
        

}

