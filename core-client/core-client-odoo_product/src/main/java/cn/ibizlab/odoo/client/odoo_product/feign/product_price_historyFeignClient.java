package cn.ibizlab.odoo.client.odoo_product.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Iproduct_price_history;
import cn.ibizlab.odoo.client.odoo_product.model.product_price_historyImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[product_price_history] 服务对象接口
 */
public interface product_price_historyFeignClient {

    @RequestMapping(method = RequestMethod.GET, value = "/odoo_product/product_price_histories/search")
    public Page<product_price_historyImpl> search(SearchContext context);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_product/product_price_histories/updatebatch")
    public product_price_historyImpl updateBatch(@RequestBody List<product_price_historyImpl> product_price_histories);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_product/product_price_histories/createbatch")
    public product_price_historyImpl createBatch(@RequestBody List<product_price_historyImpl> product_price_histories);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_product/product_price_histories/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_product/product_price_histories")
    public product_price_historyImpl create(@RequestBody product_price_historyImpl product_price_history);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_product/product_price_histories/{id}")
    public product_price_historyImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_product/product_price_histories/{id}")
    public product_price_historyImpl update(@PathVariable("id") Integer id,@RequestBody product_price_historyImpl product_price_history);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_product/product_price_histories/removebatch")
    public product_price_historyImpl removeBatch(@RequestBody List<product_price_historyImpl> product_price_histories);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_product/product_price_histories/select")
    public Page<product_price_historyImpl> select();



}
