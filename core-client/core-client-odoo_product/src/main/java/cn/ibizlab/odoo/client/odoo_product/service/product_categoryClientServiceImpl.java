package cn.ibizlab.odoo.client.odoo_product.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Iproduct_category;
import cn.ibizlab.odoo.core.client.service.Iproduct_categoryClientService;
import cn.ibizlab.odoo.client.odoo_product.model.product_categoryImpl;
import cn.ibizlab.odoo.client.odoo_product.odooclient.Iproduct_categoryOdooClient;
import cn.ibizlab.odoo.client.odoo_product.odooclient.impl.product_categoryOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[product_category] 服务对象接口
 */
@Service
public class product_categoryClientServiceImpl implements Iproduct_categoryClientService {
    @Autowired
    private  Iproduct_categoryOdooClient  product_categoryOdooClient;

    public Iproduct_category createModel() {		
		return new product_categoryImpl();
	}


        public void update(Iproduct_category product_category){
this.product_categoryOdooClient.update(product_category) ;
        }
        
        public void removeBatch(List<Iproduct_category> product_categories){
            
        }
        
        public void create(Iproduct_category product_category){
this.product_categoryOdooClient.create(product_category) ;
        }
        
        public void createBatch(List<Iproduct_category> product_categories){
            
        }
        
        public void remove(Iproduct_category product_category){
this.product_categoryOdooClient.remove(product_category) ;
        }
        
        public void get(Iproduct_category product_category){
            this.product_categoryOdooClient.get(product_category) ;
        }
        
        public Page<Iproduct_category> search(SearchContext context){
            return this.product_categoryOdooClient.search(context) ;
        }
        
        public void updateBatch(List<Iproduct_category> product_categories){
            
        }
        
        public Page<Iproduct_category> select(SearchContext context){
            return null ;
        }
        

}

