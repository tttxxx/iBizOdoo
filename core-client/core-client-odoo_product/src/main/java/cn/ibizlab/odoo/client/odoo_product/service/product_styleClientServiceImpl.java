package cn.ibizlab.odoo.client.odoo_product.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Iproduct_style;
import cn.ibizlab.odoo.core.client.service.Iproduct_styleClientService;
import cn.ibizlab.odoo.client.odoo_product.model.product_styleImpl;
import cn.ibizlab.odoo.client.odoo_product.odooclient.Iproduct_styleOdooClient;
import cn.ibizlab.odoo.client.odoo_product.odooclient.impl.product_styleOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[product_style] 服务对象接口
 */
@Service
public class product_styleClientServiceImpl implements Iproduct_styleClientService {
    @Autowired
    private  Iproduct_styleOdooClient  product_styleOdooClient;

    public Iproduct_style createModel() {		
		return new product_styleImpl();
	}


        public Page<Iproduct_style> search(SearchContext context){
            return this.product_styleOdooClient.search(context) ;
        }
        
        public void update(Iproduct_style product_style){
this.product_styleOdooClient.update(product_style) ;
        }
        
        public void createBatch(List<Iproduct_style> product_styles){
            
        }
        
        public void create(Iproduct_style product_style){
this.product_styleOdooClient.create(product_style) ;
        }
        
        public void remove(Iproduct_style product_style){
this.product_styleOdooClient.remove(product_style) ;
        }
        
        public void removeBatch(List<Iproduct_style> product_styles){
            
        }
        
        public void get(Iproduct_style product_style){
            this.product_styleOdooClient.get(product_style) ;
        }
        
        public void updateBatch(List<Iproduct_style> product_styles){
            
        }
        
        public Page<Iproduct_style> select(SearchContext context){
            return null ;
        }
        

}

