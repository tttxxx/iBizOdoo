package cn.ibizlab.odoo.client.odoo_product.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Iproduct_putaway;
import cn.ibizlab.odoo.core.client.service.Iproduct_putawayClientService;
import cn.ibizlab.odoo.client.odoo_product.model.product_putawayImpl;
import cn.ibizlab.odoo.client.odoo_product.odooclient.Iproduct_putawayOdooClient;
import cn.ibizlab.odoo.client.odoo_product.odooclient.impl.product_putawayOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[product_putaway] 服务对象接口
 */
@Service
public class product_putawayClientServiceImpl implements Iproduct_putawayClientService {
    @Autowired
    private  Iproduct_putawayOdooClient  product_putawayOdooClient;

    public Iproduct_putaway createModel() {		
		return new product_putawayImpl();
	}


        public void createBatch(List<Iproduct_putaway> product_putaways){
            
        }
        
        public void create(Iproduct_putaway product_putaway){
this.product_putawayOdooClient.create(product_putaway) ;
        }
        
        public void update(Iproduct_putaway product_putaway){
this.product_putawayOdooClient.update(product_putaway) ;
        }
        
        public Page<Iproduct_putaway> search(SearchContext context){
            return this.product_putawayOdooClient.search(context) ;
        }
        
        public void removeBatch(List<Iproduct_putaway> product_putaways){
            
        }
        
        public void remove(Iproduct_putaway product_putaway){
this.product_putawayOdooClient.remove(product_putaway) ;
        }
        
        public void updateBatch(List<Iproduct_putaway> product_putaways){
            
        }
        
        public void get(Iproduct_putaway product_putaway){
            this.product_putawayOdooClient.get(product_putaway) ;
        }
        
        public Page<Iproduct_putaway> select(SearchContext context){
            return null ;
        }
        

}

