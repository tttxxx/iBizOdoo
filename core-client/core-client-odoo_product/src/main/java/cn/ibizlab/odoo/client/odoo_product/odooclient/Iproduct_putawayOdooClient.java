package cn.ibizlab.odoo.client.odoo_product.odooclient;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.core.client.model.Iproduct_putaway;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[product_putaway] 服务对象客户端接口
 */
public interface Iproduct_putawayOdooClient {
    
        public void createBatch(Iproduct_putaway product_putaway);

        public void create(Iproduct_putaway product_putaway);

        public void update(Iproduct_putaway product_putaway);

        public Page<Iproduct_putaway> search(SearchContext context);

        public void removeBatch(Iproduct_putaway product_putaway);

        public void remove(Iproduct_putaway product_putaway);

        public void updateBatch(Iproduct_putaway product_putaway);

        public void get(Iproduct_putaway product_putaway);

        public List<Iproduct_putaway> select();


}