package cn.ibizlab.odoo.client.odoo_product.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Iproduct_image;
import cn.ibizlab.odoo.core.client.service.Iproduct_imageClientService;
import cn.ibizlab.odoo.client.odoo_product.model.product_imageImpl;
import cn.ibizlab.odoo.client.odoo_product.odooclient.Iproduct_imageOdooClient;
import cn.ibizlab.odoo.client.odoo_product.odooclient.impl.product_imageOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[product_image] 服务对象接口
 */
@Service
public class product_imageClientServiceImpl implements Iproduct_imageClientService {
    @Autowired
    private  Iproduct_imageOdooClient  product_imageOdooClient;

    public Iproduct_image createModel() {		
		return new product_imageImpl();
	}


        public void create(Iproduct_image product_image){
this.product_imageOdooClient.create(product_image) ;
        }
        
        public void remove(Iproduct_image product_image){
this.product_imageOdooClient.remove(product_image) ;
        }
        
        public void update(Iproduct_image product_image){
this.product_imageOdooClient.update(product_image) ;
        }
        
        public Page<Iproduct_image> search(SearchContext context){
            return this.product_imageOdooClient.search(context) ;
        }
        
        public void updateBatch(List<Iproduct_image> product_images){
            
        }
        
        public void removeBatch(List<Iproduct_image> product_images){
            
        }
        
        public void get(Iproduct_image product_image){
            this.product_imageOdooClient.get(product_image) ;
        }
        
        public void createBatch(List<Iproduct_image> product_images){
            
        }
        
        public Page<Iproduct_image> select(SearchContext context){
            return null ;
        }
        

}

