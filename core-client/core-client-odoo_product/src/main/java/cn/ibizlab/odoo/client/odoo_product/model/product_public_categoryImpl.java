package cn.ibizlab.odoo.client.odoo_product.model;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.math.BigInteger;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.core.client.model.Iproduct_public_category;
import cn.ibizlab.odoo.util.helper.OdooClientHelper;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang.ArrayUtils;
import org.springframework.util.StringUtils;

/**
 * 接口实体[product_public_category] 对象
 */
public class product_public_categoryImpl implements Iproduct_public_category,Serializable{

    /**
     * 儿童类
     */
    public String child_id;

    @JsonIgnore
    public boolean child_idDirtyFlag;
    
    /**
     * 创建时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp create_date;

    @JsonIgnore
    public boolean create_dateDirtyFlag;
    
    /**
     * 创建人
     */
    public Integer create_uid;

    @JsonIgnore
    public boolean create_uidDirtyFlag;
    
    /**
     * 创建人
     */
    public String create_uid_text;

    @JsonIgnore
    public boolean create_uid_textDirtyFlag;
    
    /**
     * 显示名称
     */
    public String display_name;

    @JsonIgnore
    public boolean display_nameDirtyFlag;
    
    /**
     * ID
     */
    public Integer id;

    @JsonIgnore
    public boolean idDirtyFlag;
    
    /**
     * 图像
     */
    public byte[] image;

    @JsonIgnore
    public boolean imageDirtyFlag;
    
    /**
     * 中等尺寸图像
     */
    public byte[] image_medium;

    @JsonIgnore
    public boolean image_mediumDirtyFlag;
    
    /**
     * 小尺寸图像
     */
    public byte[] image_small;

    @JsonIgnore
    public boolean image_smallDirtyFlag;
    
    /**
     * SEO优化
     */
    public String is_seo_optimized;

    @JsonIgnore
    public boolean is_seo_optimizedDirtyFlag;
    
    /**
     * 名称
     */
    public String name;

    @JsonIgnore
    public boolean nameDirtyFlag;
    
    /**
     * 上级类别
     */
    public Integer parent_id;

    @JsonIgnore
    public boolean parent_idDirtyFlag;
    
    /**
     * 上级类别
     */
    public String parent_id_text;

    @JsonIgnore
    public boolean parent_id_textDirtyFlag;
    
    /**
     * 序号
     */
    public Integer sequence;

    @JsonIgnore
    public boolean sequenceDirtyFlag;
    
    /**
     * 网站
     */
    public Integer website_id;

    @JsonIgnore
    public boolean website_idDirtyFlag;
    
    /**
     * 网站元说明
     */
    public String website_meta_description;

    @JsonIgnore
    public boolean website_meta_descriptionDirtyFlag;
    
    /**
     * 网站meta关键词
     */
    public String website_meta_keywords;

    @JsonIgnore
    public boolean website_meta_keywordsDirtyFlag;
    
    /**
     * 网站opengraph图像
     */
    public String website_meta_og_img;

    @JsonIgnore
    public boolean website_meta_og_imgDirtyFlag;
    
    /**
     * 网站meta标题
     */
    public String website_meta_title;

    @JsonIgnore
    public boolean website_meta_titleDirtyFlag;
    
    /**
     * 最后更新时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp write_date;

    @JsonIgnore
    public boolean write_dateDirtyFlag;
    
    /**
     * 最后更新人
     */
    public Integer write_uid;

    @JsonIgnore
    public boolean write_uidDirtyFlag;
    
    /**
     * 最后更新人
     */
    public String write_uid_text;

    @JsonIgnore
    public boolean write_uid_textDirtyFlag;
    
    /**
     * 最后修改日
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp __last_update;

    @JsonIgnore
    public boolean __last_updateDirtyFlag;
    
    /**
     * 获取 [儿童类]
     */
    @JsonProperty("child_id")
    public String getChild_id(){
        return this.child_id ;
    }

    /**
     * 设置 [儿童类]
     */
    @JsonProperty("child_id")
    public void setChild_id(String  child_id){
        this.child_id = child_id ;
        this.child_idDirtyFlag = true ;
    }

     /**
     * 获取 [儿童类]脏标记
     */
    @JsonIgnore
    public boolean getChild_idDirtyFlag(){
        return this.child_idDirtyFlag ;
    }   

    /**
     * 获取 [创建时间]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return this.create_date ;
    }

    /**
     * 设置 [创建时间]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

     /**
     * 获取 [创建时间]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return this.create_dateDirtyFlag ;
    }   

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return this.create_uid ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

     /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return this.create_uidDirtyFlag ;
    }   

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return this.create_uid_text ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

     /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return this.create_uid_textDirtyFlag ;
    }   

    /**
     * 获取 [显示名称]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return this.display_name ;
    }

    /**
     * 设置 [显示名称]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

     /**
     * 获取 [显示名称]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return this.display_nameDirtyFlag ;
    }   

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return this.id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

     /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return this.idDirtyFlag ;
    }   

    /**
     * 获取 [图像]
     */
    @JsonProperty("image")
    public byte[] getImage(){
        return this.image ;
    }

    /**
     * 设置 [图像]
     */
    @JsonProperty("image")
    public void setImage(byte[]  image){
        this.image = image ;
        this.imageDirtyFlag = true ;
    }

     /**
     * 获取 [图像]脏标记
     */
    @JsonIgnore
    public boolean getImageDirtyFlag(){
        return this.imageDirtyFlag ;
    }   

    /**
     * 获取 [中等尺寸图像]
     */
    @JsonProperty("image_medium")
    public byte[] getImage_medium(){
        return this.image_medium ;
    }

    /**
     * 设置 [中等尺寸图像]
     */
    @JsonProperty("image_medium")
    public void setImage_medium(byte[]  image_medium){
        this.image_medium = image_medium ;
        this.image_mediumDirtyFlag = true ;
    }

     /**
     * 获取 [中等尺寸图像]脏标记
     */
    @JsonIgnore
    public boolean getImage_mediumDirtyFlag(){
        return this.image_mediumDirtyFlag ;
    }   

    /**
     * 获取 [小尺寸图像]
     */
    @JsonProperty("image_small")
    public byte[] getImage_small(){
        return this.image_small ;
    }

    /**
     * 设置 [小尺寸图像]
     */
    @JsonProperty("image_small")
    public void setImage_small(byte[]  image_small){
        this.image_small = image_small ;
        this.image_smallDirtyFlag = true ;
    }

     /**
     * 获取 [小尺寸图像]脏标记
     */
    @JsonIgnore
    public boolean getImage_smallDirtyFlag(){
        return this.image_smallDirtyFlag ;
    }   

    /**
     * 获取 [SEO优化]
     */
    @JsonProperty("is_seo_optimized")
    public String getIs_seo_optimized(){
        return this.is_seo_optimized ;
    }

    /**
     * 设置 [SEO优化]
     */
    @JsonProperty("is_seo_optimized")
    public void setIs_seo_optimized(String  is_seo_optimized){
        this.is_seo_optimized = is_seo_optimized ;
        this.is_seo_optimizedDirtyFlag = true ;
    }

     /**
     * 获取 [SEO优化]脏标记
     */
    @JsonIgnore
    public boolean getIs_seo_optimizedDirtyFlag(){
        return this.is_seo_optimizedDirtyFlag ;
    }   

    /**
     * 获取 [名称]
     */
    @JsonProperty("name")
    public String getName(){
        return this.name ;
    }

    /**
     * 设置 [名称]
     */
    @JsonProperty("name")
    public void setName(String  name){
        this.name = name ;
        this.nameDirtyFlag = true ;
    }

     /**
     * 获取 [名称]脏标记
     */
    @JsonIgnore
    public boolean getNameDirtyFlag(){
        return this.nameDirtyFlag ;
    }   

    /**
     * 获取 [上级类别]
     */
    @JsonProperty("parent_id")
    public Integer getParent_id(){
        return this.parent_id ;
    }

    /**
     * 设置 [上级类别]
     */
    @JsonProperty("parent_id")
    public void setParent_id(Integer  parent_id){
        this.parent_id = parent_id ;
        this.parent_idDirtyFlag = true ;
    }

     /**
     * 获取 [上级类别]脏标记
     */
    @JsonIgnore
    public boolean getParent_idDirtyFlag(){
        return this.parent_idDirtyFlag ;
    }   

    /**
     * 获取 [上级类别]
     */
    @JsonProperty("parent_id_text")
    public String getParent_id_text(){
        return this.parent_id_text ;
    }

    /**
     * 设置 [上级类别]
     */
    @JsonProperty("parent_id_text")
    public void setParent_id_text(String  parent_id_text){
        this.parent_id_text = parent_id_text ;
        this.parent_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [上级类别]脏标记
     */
    @JsonIgnore
    public boolean getParent_id_textDirtyFlag(){
        return this.parent_id_textDirtyFlag ;
    }   

    /**
     * 获取 [序号]
     */
    @JsonProperty("sequence")
    public Integer getSequence(){
        return this.sequence ;
    }

    /**
     * 设置 [序号]
     */
    @JsonProperty("sequence")
    public void setSequence(Integer  sequence){
        this.sequence = sequence ;
        this.sequenceDirtyFlag = true ;
    }

     /**
     * 获取 [序号]脏标记
     */
    @JsonIgnore
    public boolean getSequenceDirtyFlag(){
        return this.sequenceDirtyFlag ;
    }   

    /**
     * 获取 [网站]
     */
    @JsonProperty("website_id")
    public Integer getWebsite_id(){
        return this.website_id ;
    }

    /**
     * 设置 [网站]
     */
    @JsonProperty("website_id")
    public void setWebsite_id(Integer  website_id){
        this.website_id = website_id ;
        this.website_idDirtyFlag = true ;
    }

     /**
     * 获取 [网站]脏标记
     */
    @JsonIgnore
    public boolean getWebsite_idDirtyFlag(){
        return this.website_idDirtyFlag ;
    }   

    /**
     * 获取 [网站元说明]
     */
    @JsonProperty("website_meta_description")
    public String getWebsite_meta_description(){
        return this.website_meta_description ;
    }

    /**
     * 设置 [网站元说明]
     */
    @JsonProperty("website_meta_description")
    public void setWebsite_meta_description(String  website_meta_description){
        this.website_meta_description = website_meta_description ;
        this.website_meta_descriptionDirtyFlag = true ;
    }

     /**
     * 获取 [网站元说明]脏标记
     */
    @JsonIgnore
    public boolean getWebsite_meta_descriptionDirtyFlag(){
        return this.website_meta_descriptionDirtyFlag ;
    }   

    /**
     * 获取 [网站meta关键词]
     */
    @JsonProperty("website_meta_keywords")
    public String getWebsite_meta_keywords(){
        return this.website_meta_keywords ;
    }

    /**
     * 设置 [网站meta关键词]
     */
    @JsonProperty("website_meta_keywords")
    public void setWebsite_meta_keywords(String  website_meta_keywords){
        this.website_meta_keywords = website_meta_keywords ;
        this.website_meta_keywordsDirtyFlag = true ;
    }

     /**
     * 获取 [网站meta关键词]脏标记
     */
    @JsonIgnore
    public boolean getWebsite_meta_keywordsDirtyFlag(){
        return this.website_meta_keywordsDirtyFlag ;
    }   

    /**
     * 获取 [网站opengraph图像]
     */
    @JsonProperty("website_meta_og_img")
    public String getWebsite_meta_og_img(){
        return this.website_meta_og_img ;
    }

    /**
     * 设置 [网站opengraph图像]
     */
    @JsonProperty("website_meta_og_img")
    public void setWebsite_meta_og_img(String  website_meta_og_img){
        this.website_meta_og_img = website_meta_og_img ;
        this.website_meta_og_imgDirtyFlag = true ;
    }

     /**
     * 获取 [网站opengraph图像]脏标记
     */
    @JsonIgnore
    public boolean getWebsite_meta_og_imgDirtyFlag(){
        return this.website_meta_og_imgDirtyFlag ;
    }   

    /**
     * 获取 [网站meta标题]
     */
    @JsonProperty("website_meta_title")
    public String getWebsite_meta_title(){
        return this.website_meta_title ;
    }

    /**
     * 设置 [网站meta标题]
     */
    @JsonProperty("website_meta_title")
    public void setWebsite_meta_title(String  website_meta_title){
        this.website_meta_title = website_meta_title ;
        this.website_meta_titleDirtyFlag = true ;
    }

     /**
     * 获取 [网站meta标题]脏标记
     */
    @JsonIgnore
    public boolean getWebsite_meta_titleDirtyFlag(){
        return this.website_meta_titleDirtyFlag ;
    }   

    /**
     * 获取 [最后更新时间]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return this.write_date ;
    }

    /**
     * 设置 [最后更新时间]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新时间]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return this.write_dateDirtyFlag ;
    }   

    /**
     * 获取 [最后更新人]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return this.write_uid ;
    }

    /**
     * 设置 [最后更新人]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新人]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return this.write_uidDirtyFlag ;
    }   

    /**
     * 获取 [最后更新人]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return this.write_uid_text ;
    }

    /**
     * 设置 [最后更新人]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新人]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return this.write_uid_textDirtyFlag ;
    }   

    /**
     * 获取 [最后修改日]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return this.__last_update ;
    }

    /**
     * 设置 [最后修改日]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

     /**
     * 获取 [最后修改日]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return this.__last_updateDirtyFlag ;
    }   



    public void fromMap(Map<String, Object> map) throws Exception {
		if(!(map.get("child_id") instanceof Boolean)&& map.get("child_id")!=null){
			Object[] objs = (Object[])map.get("child_id");
			if(objs.length > 0){
				Integer[] child_id = Arrays.copyOfRange(objs,0,objs.length,Integer[].class);
				this.setChild_id(Arrays.toString(child_id));
			}
		}
		if(!(map.get("create_date") instanceof Boolean)&& map.get("create_date")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse((String)map.get("create_date"));
   			this.setCreate_date(new Timestamp(parse.getTime()));
		}
		if(!(map.get("create_uid") instanceof Boolean)&& map.get("create_uid")!=null){
			Object[] objs = (Object[])map.get("create_uid");
			if(objs.length > 0){
				this.setCreate_uid((Integer)objs[0]);
			}
		}
		if(!(map.get("create_uid") instanceof Boolean)&& map.get("create_uid")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("create_uid");
			if(objs.length > 1){
				this.setCreate_uid_text((String)objs[1]);
			}
		}
		if(!(map.get("display_name") instanceof Boolean)&& map.get("display_name")!=null){
			this.setDisplay_name((String)map.get("display_name"));
		}
		if(!(map.get("id") instanceof Boolean)&& map.get("id")!=null){
			this.setId((Integer)map.get("id"));
		}
		if(!(map.get("image") instanceof Boolean)&& map.get("image")!=null){
			//暂时忽略
			//this.setImage(((String)map.get("image")).getBytes("UTF-8"));
		}
		if(!(map.get("image_medium") instanceof Boolean)&& map.get("image_medium")!=null){
			//暂时忽略
			//this.setImage_medium(((String)map.get("image_medium")).getBytes("UTF-8"));
		}
		if(!(map.get("image_small") instanceof Boolean)&& map.get("image_small")!=null){
			//暂时忽略
			//this.setImage_small(((String)map.get("image_small")).getBytes("UTF-8"));
		}
		if(map.get("is_seo_optimized") instanceof Boolean){
			this.setIs_seo_optimized(((Boolean)map.get("is_seo_optimized"))? "true" : "false");
		}
		if(!(map.get("name") instanceof Boolean)&& map.get("name")!=null){
			this.setName((String)map.get("name"));
		}
		if(!(map.get("parent_id") instanceof Boolean)&& map.get("parent_id")!=null){
			Object[] objs = (Object[])map.get("parent_id");
			if(objs.length > 0){
				this.setParent_id((Integer)objs[0]);
			}
		}
		if(!(map.get("parent_id") instanceof Boolean)&& map.get("parent_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("parent_id");
			if(objs.length > 1){
				this.setParent_id_text((String)objs[1]);
			}
		}
		if(!(map.get("sequence") instanceof Boolean)&& map.get("sequence")!=null){
			this.setSequence((Integer)map.get("sequence"));
		}
		if(!(map.get("website_id") instanceof Boolean)&& map.get("website_id")!=null){
			Object[] objs = (Object[])map.get("website_id");
			if(objs.length > 0){
				this.setWebsite_id((Integer)objs[0]);
			}
		}
		if(!(map.get("website_meta_description") instanceof Boolean)&& map.get("website_meta_description")!=null){
			this.setWebsite_meta_description((String)map.get("website_meta_description"));
		}
		if(!(map.get("website_meta_keywords") instanceof Boolean)&& map.get("website_meta_keywords")!=null){
			this.setWebsite_meta_keywords((String)map.get("website_meta_keywords"));
		}
		if(!(map.get("website_meta_og_img") instanceof Boolean)&& map.get("website_meta_og_img")!=null){
			this.setWebsite_meta_og_img((String)map.get("website_meta_og_img"));
		}
		if(!(map.get("website_meta_title") instanceof Boolean)&& map.get("website_meta_title")!=null){
			this.setWebsite_meta_title((String)map.get("website_meta_title"));
		}
		if(!(map.get("write_date") instanceof Boolean)&& map.get("write_date")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse((String)map.get("write_date"));
   			this.setWrite_date(new Timestamp(parse.getTime()));
		}
		if(!(map.get("write_uid") instanceof Boolean)&& map.get("write_uid")!=null){
			Object[] objs = (Object[])map.get("write_uid");
			if(objs.length > 0){
				this.setWrite_uid((Integer)objs[0]);
			}
		}
		if(!(map.get("write_uid") instanceof Boolean)&& map.get("write_uid")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("write_uid");
			if(objs.length > 1){
				this.setWrite_uid_text((String)objs[1]);
			}
		}
		if(!(map.get("__last_update") instanceof Boolean)&& map.get("__last_update")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse((String)map.get("__last_update"));
   			this.set__last_update(new Timestamp(parse.getTime()));
		}
	}

	public Map<String, Object> toMap() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		if(this.getChild_id()!=null&&this.getChild_idDirtyFlag()){
			map.put("child_id",this.getChild_id());
		}else if(this.getChild_idDirtyFlag()){
			map.put("child_id",false);
		}
		if(this.getCreate_date()!=null&&this.getCreate_dateDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String datetimeStr = sdf.format(this.getCreate_date());
			map.put("create_date",datetimeStr);
		}else if(this.getCreate_dateDirtyFlag()){
			map.put("create_date",false);
		}
		if(this.getCreate_uid()!=null&&this.getCreate_uidDirtyFlag()){
			map.put("create_uid",this.getCreate_uid());
		}else if(this.getCreate_uidDirtyFlag()){
			map.put("create_uid",false);
		}
		if(this.getCreate_uid_text()!=null&&this.getCreate_uid_textDirtyFlag()){
			//忽略文本外键create_uid_text
		}else if(this.getCreate_uid_textDirtyFlag()){
			map.put("create_uid",false);
		}
		if(this.getDisplay_name()!=null&&this.getDisplay_nameDirtyFlag()){
			map.put("display_name",this.getDisplay_name());
		}else if(this.getDisplay_nameDirtyFlag()){
			map.put("display_name",false);
		}
		if(this.getId()!=null&&this.getIdDirtyFlag()){
			map.put("id",this.getId());
		}else if(this.getIdDirtyFlag()){
			map.put("id",false);
		}
		if(this.getImage()!=null&&this.getImageDirtyFlag()){
			//暂不支持binary类型image
		}else if(this.getImageDirtyFlag()){
			map.put("image",false);
		}
		if(this.getImage_medium()!=null&&this.getImage_mediumDirtyFlag()){
			//暂不支持binary类型image_medium
		}else if(this.getImage_mediumDirtyFlag()){
			map.put("image_medium",false);
		}
		if(this.getImage_small()!=null&&this.getImage_smallDirtyFlag()){
			//暂不支持binary类型image_small
		}else if(this.getImage_smallDirtyFlag()){
			map.put("image_small",false);
		}
		if(this.getIs_seo_optimized()!=null&&this.getIs_seo_optimizedDirtyFlag()){
			map.put("is_seo_optimized",Boolean.parseBoolean(this.getIs_seo_optimized()));		
		}		if(this.getName()!=null&&this.getNameDirtyFlag()){
			map.put("name",this.getName());
		}else if(this.getNameDirtyFlag()){
			map.put("name",false);
		}
		if(this.getParent_id()!=null&&this.getParent_idDirtyFlag()){
			map.put("parent_id",this.getParent_id());
		}else if(this.getParent_idDirtyFlag()){
			map.put("parent_id",false);
		}
		if(this.getParent_id_text()!=null&&this.getParent_id_textDirtyFlag()){
			//忽略文本外键parent_id_text
		}else if(this.getParent_id_textDirtyFlag()){
			map.put("parent_id",false);
		}
		if(this.getSequence()!=null&&this.getSequenceDirtyFlag()){
			map.put("sequence",this.getSequence());
		}else if(this.getSequenceDirtyFlag()){
			map.put("sequence",false);
		}
		if(this.getWebsite_id()!=null&&this.getWebsite_idDirtyFlag()){
			map.put("website_id",this.getWebsite_id());
		}else if(this.getWebsite_idDirtyFlag()){
			map.put("website_id",false);
		}
		if(this.getWebsite_meta_description()!=null&&this.getWebsite_meta_descriptionDirtyFlag()){
			map.put("website_meta_description",this.getWebsite_meta_description());
		}else if(this.getWebsite_meta_descriptionDirtyFlag()){
			map.put("website_meta_description",false);
		}
		if(this.getWebsite_meta_keywords()!=null&&this.getWebsite_meta_keywordsDirtyFlag()){
			map.put("website_meta_keywords",this.getWebsite_meta_keywords());
		}else if(this.getWebsite_meta_keywordsDirtyFlag()){
			map.put("website_meta_keywords",false);
		}
		if(this.getWebsite_meta_og_img()!=null&&this.getWebsite_meta_og_imgDirtyFlag()){
			map.put("website_meta_og_img",this.getWebsite_meta_og_img());
		}else if(this.getWebsite_meta_og_imgDirtyFlag()){
			map.put("website_meta_og_img",false);
		}
		if(this.getWebsite_meta_title()!=null&&this.getWebsite_meta_titleDirtyFlag()){
			map.put("website_meta_title",this.getWebsite_meta_title());
		}else if(this.getWebsite_meta_titleDirtyFlag()){
			map.put("website_meta_title",false);
		}
		if(this.getWrite_date()!=null&&this.getWrite_dateDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String datetimeStr = sdf.format(this.getWrite_date());
			map.put("write_date",datetimeStr);
		}else if(this.getWrite_dateDirtyFlag()){
			map.put("write_date",false);
		}
		if(this.getWrite_uid()!=null&&this.getWrite_uidDirtyFlag()){
			map.put("write_uid",this.getWrite_uid());
		}else if(this.getWrite_uidDirtyFlag()){
			map.put("write_uid",false);
		}
		if(this.getWrite_uid_text()!=null&&this.getWrite_uid_textDirtyFlag()){
			//忽略文本外键write_uid_text
		}else if(this.getWrite_uid_textDirtyFlag()){
			map.put("write_uid",false);
		}
		if(this.get__last_update()!=null&&this.get__last_updateDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String datetimeStr = sdf.format(this.get__last_update());
			map.put("__last_update",datetimeStr);
		}else if(this.get__last_updateDirtyFlag()){
			map.put("__last_update",false);
		}
		return map;
	}

}
