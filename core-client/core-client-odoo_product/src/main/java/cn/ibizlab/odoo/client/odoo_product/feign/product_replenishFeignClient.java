package cn.ibizlab.odoo.client.odoo_product.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Iproduct_replenish;
import cn.ibizlab.odoo.client.odoo_product.model.product_replenishImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[product_replenish] 服务对象接口
 */
public interface product_replenishFeignClient {

    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_product/product_replenishes/removebatch")
    public product_replenishImpl removeBatch(@RequestBody List<product_replenishImpl> product_replenishes);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_product/product_replenishes/search")
    public Page<product_replenishImpl> search(SearchContext context);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_product/product_replenishes/createbatch")
    public product_replenishImpl createBatch(@RequestBody List<product_replenishImpl> product_replenishes);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_product/product_replenishes/updatebatch")
    public product_replenishImpl updateBatch(@RequestBody List<product_replenishImpl> product_replenishes);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_product/product_replenishes/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_product/product_replenishes/{id}")
    public product_replenishImpl update(@PathVariable("id") Integer id,@RequestBody product_replenishImpl product_replenish);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_product/product_replenishes/{id}")
    public product_replenishImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_product/product_replenishes")
    public product_replenishImpl create(@RequestBody product_replenishImpl product_replenish);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_product/product_replenishes/select")
    public Page<product_replenishImpl> select();



}
