package cn.ibizlab.odoo.client.odoo_product.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Iproduct_removal;
import cn.ibizlab.odoo.client.odoo_product.model.product_removalImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[product_removal] 服务对象接口
 */
public interface product_removalFeignClient {

    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_product/product_removals/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_product/product_removals/removebatch")
    public product_removalImpl removeBatch(@RequestBody List<product_removalImpl> product_removals);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_product/product_removals/createbatch")
    public product_removalImpl createBatch(@RequestBody List<product_removalImpl> product_removals);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_product/product_removals/search")
    public Page<product_removalImpl> search(SearchContext context);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_product/product_removals")
    public product_removalImpl create(@RequestBody product_removalImpl product_removal);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_product/product_removals/updatebatch")
    public product_removalImpl updateBatch(@RequestBody List<product_removalImpl> product_removals);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_product/product_removals/{id}")
    public product_removalImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_product/product_removals/{id}")
    public product_removalImpl update(@PathVariable("id") Integer id,@RequestBody product_removalImpl product_removal);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_product/product_removals/select")
    public Page<product_removalImpl> select();



}
