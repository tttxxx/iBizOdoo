package cn.ibizlab.odoo.client.odoo_product.odooclient;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.core.client.model.Iproduct_packaging;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[product_packaging] 服务对象客户端接口
 */
public interface Iproduct_packagingOdooClient {
    
        public void updateBatch(Iproduct_packaging product_packaging);

        public Page<Iproduct_packaging> search(SearchContext context);

        public void createBatch(Iproduct_packaging product_packaging);

        public void get(Iproduct_packaging product_packaging);

        public void removeBatch(Iproduct_packaging product_packaging);

        public void create(Iproduct_packaging product_packaging);

        public void remove(Iproduct_packaging product_packaging);

        public void update(Iproduct_packaging product_packaging);

        public List<Iproduct_packaging> select();


}