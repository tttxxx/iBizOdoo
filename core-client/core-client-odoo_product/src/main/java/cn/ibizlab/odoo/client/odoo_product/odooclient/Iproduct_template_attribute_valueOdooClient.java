package cn.ibizlab.odoo.client.odoo_product.odooclient;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.core.client.model.Iproduct_template_attribute_value;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[product_template_attribute_value] 服务对象客户端接口
 */
public interface Iproduct_template_attribute_valueOdooClient {
    
        public void updateBatch(Iproduct_template_attribute_value product_template_attribute_value);

        public void createBatch(Iproduct_template_attribute_value product_template_attribute_value);

        public void get(Iproduct_template_attribute_value product_template_attribute_value);

        public void removeBatch(Iproduct_template_attribute_value product_template_attribute_value);

        public void create(Iproduct_template_attribute_value product_template_attribute_value);

        public Page<Iproduct_template_attribute_value> search(SearchContext context);

        public void remove(Iproduct_template_attribute_value product_template_attribute_value);

        public void update(Iproduct_template_attribute_value product_template_attribute_value);

        public List<Iproduct_template_attribute_value> select();


}