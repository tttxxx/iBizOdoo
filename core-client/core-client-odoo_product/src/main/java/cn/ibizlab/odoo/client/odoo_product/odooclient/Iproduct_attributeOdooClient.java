package cn.ibizlab.odoo.client.odoo_product.odooclient;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.core.client.model.Iproduct_attribute;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[product_attribute] 服务对象客户端接口
 */
public interface Iproduct_attributeOdooClient {
    
        public void get(Iproduct_attribute product_attribute);

        public void updateBatch(Iproduct_attribute product_attribute);

        public void remove(Iproduct_attribute product_attribute);

        public void update(Iproduct_attribute product_attribute);

        public Page<Iproduct_attribute> search(SearchContext context);

        public void createBatch(Iproduct_attribute product_attribute);

        public void create(Iproduct_attribute product_attribute);

        public void removeBatch(Iproduct_attribute product_attribute);

        public List<Iproduct_attribute> select();


}