package cn.ibizlab.odoo.client.odoo_product.odooclient;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.core.client.model.Iproduct_template_attribute_exclusion;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[product_template_attribute_exclusion] 服务对象客户端接口
 */
public interface Iproduct_template_attribute_exclusionOdooClient {
    
        public void updateBatch(Iproduct_template_attribute_exclusion product_template_attribute_exclusion);

        public void get(Iproduct_template_attribute_exclusion product_template_attribute_exclusion);

        public void create(Iproduct_template_attribute_exclusion product_template_attribute_exclusion);

        public void update(Iproduct_template_attribute_exclusion product_template_attribute_exclusion);

        public Page<Iproduct_template_attribute_exclusion> search(SearchContext context);

        public void remove(Iproduct_template_attribute_exclusion product_template_attribute_exclusion);

        public void removeBatch(Iproduct_template_attribute_exclusion product_template_attribute_exclusion);

        public void createBatch(Iproduct_template_attribute_exclusion product_template_attribute_exclusion);

        public List<Iproduct_template_attribute_exclusion> select();


}