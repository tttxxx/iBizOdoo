package cn.ibizlab.odoo.client.odoo_product.odooclient;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.core.client.model.Iproduct_pricelist;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[product_pricelist] 服务对象客户端接口
 */
public interface Iproduct_pricelistOdooClient {
    
        public void remove(Iproduct_pricelist product_pricelist);

        public void get(Iproduct_pricelist product_pricelist);

        public Page<Iproduct_pricelist> search(SearchContext context);

        public void createBatch(Iproduct_pricelist product_pricelist);

        public void updateBatch(Iproduct_pricelist product_pricelist);

        public void removeBatch(Iproduct_pricelist product_pricelist);

        public void create(Iproduct_pricelist product_pricelist);

        public void update(Iproduct_pricelist product_pricelist);

        public List<Iproduct_pricelist> select();


}