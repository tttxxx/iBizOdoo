package cn.ibizlab.odoo.client.odoo_product.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Iproduct_price_list;
import cn.ibizlab.odoo.core.client.service.Iproduct_price_listClientService;
import cn.ibizlab.odoo.client.odoo_product.model.product_price_listImpl;
import cn.ibizlab.odoo.client.odoo_product.odooclient.Iproduct_price_listOdooClient;
import cn.ibizlab.odoo.client.odoo_product.odooclient.impl.product_price_listOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[product_price_list] 服务对象接口
 */
@Service
public class product_price_listClientServiceImpl implements Iproduct_price_listClientService {
    @Autowired
    private  Iproduct_price_listOdooClient  product_price_listOdooClient;

    public Iproduct_price_list createModel() {		
		return new product_price_listImpl();
	}


        public void createBatch(List<Iproduct_price_list> product_price_lists){
            
        }
        
        public void create(Iproduct_price_list product_price_list){
this.product_price_listOdooClient.create(product_price_list) ;
        }
        
        public void updateBatch(List<Iproduct_price_list> product_price_lists){
            
        }
        
        public void get(Iproduct_price_list product_price_list){
            this.product_price_listOdooClient.get(product_price_list) ;
        }
        
        public void update(Iproduct_price_list product_price_list){
this.product_price_listOdooClient.update(product_price_list) ;
        }
        
        public Page<Iproduct_price_list> search(SearchContext context){
            return this.product_price_listOdooClient.search(context) ;
        }
        
        public void remove(Iproduct_price_list product_price_list){
this.product_price_listOdooClient.remove(product_price_list) ;
        }
        
        public void removeBatch(List<Iproduct_price_list> product_price_lists){
            
        }
        
        public Page<Iproduct_price_list> select(SearchContext context){
            return null ;
        }
        

}

