package cn.ibizlab.odoo.client.odoo_product.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Iproduct_pricelist_item;
import cn.ibizlab.odoo.core.client.service.Iproduct_pricelist_itemClientService;
import cn.ibizlab.odoo.client.odoo_product.model.product_pricelist_itemImpl;
import cn.ibizlab.odoo.client.odoo_product.odooclient.Iproduct_pricelist_itemOdooClient;
import cn.ibizlab.odoo.client.odoo_product.odooclient.impl.product_pricelist_itemOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[product_pricelist_item] 服务对象接口
 */
@Service
public class product_pricelist_itemClientServiceImpl implements Iproduct_pricelist_itemClientService {
    @Autowired
    private  Iproduct_pricelist_itemOdooClient  product_pricelist_itemOdooClient;

    public Iproduct_pricelist_item createModel() {		
		return new product_pricelist_itemImpl();
	}


        public void removeBatch(List<Iproduct_pricelist_item> product_pricelist_items){
            
        }
        
        public void remove(Iproduct_pricelist_item product_pricelist_item){
this.product_pricelist_itemOdooClient.remove(product_pricelist_item) ;
        }
        
        public void update(Iproduct_pricelist_item product_pricelist_item){
this.product_pricelist_itemOdooClient.update(product_pricelist_item) ;
        }
        
        public void get(Iproduct_pricelist_item product_pricelist_item){
            this.product_pricelist_itemOdooClient.get(product_pricelist_item) ;
        }
        
        public void create(Iproduct_pricelist_item product_pricelist_item){
this.product_pricelist_itemOdooClient.create(product_pricelist_item) ;
        }
        
        public void updateBatch(List<Iproduct_pricelist_item> product_pricelist_items){
            
        }
        
        public Page<Iproduct_pricelist_item> search(SearchContext context){
            return this.product_pricelist_itemOdooClient.search(context) ;
        }
        
        public void createBatch(List<Iproduct_pricelist_item> product_pricelist_items){
            
        }
        
        public Page<Iproduct_pricelist_item> select(SearchContext context){
            return null ;
        }
        

}

