package cn.ibizlab.odoo.client.odoo_website.odooclient;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.core.client.model.Iwebsite_seo_metadata;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[website_seo_metadata] 服务对象客户端接口
 */
public interface Iwebsite_seo_metadataOdooClient {
    
        public Page<Iwebsite_seo_metadata> search(SearchContext context);

        public void createBatch(Iwebsite_seo_metadata website_seo_metadata);

        public void create(Iwebsite_seo_metadata website_seo_metadata);

        public void get(Iwebsite_seo_metadata website_seo_metadata);

        public void updateBatch(Iwebsite_seo_metadata website_seo_metadata);

        public void removeBatch(Iwebsite_seo_metadata website_seo_metadata);

        public void update(Iwebsite_seo_metadata website_seo_metadata);

        public void remove(Iwebsite_seo_metadata website_seo_metadata);

        public List<Iwebsite_seo_metadata> select();


}