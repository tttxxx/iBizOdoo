package cn.ibizlab.odoo.client.odoo_website.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Iwebsite_multi_mixin;
import cn.ibizlab.odoo.client.odoo_website.model.website_multi_mixinImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[website_multi_mixin] 服务对象接口
 */
public interface website_multi_mixinFeignClient {

    @RequestMapping(method = RequestMethod.GET, value = "/odoo_website/website_multi_mixins/{id}")
    public website_multi_mixinImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_website/website_multi_mixins")
    public website_multi_mixinImpl create(@RequestBody website_multi_mixinImpl website_multi_mixin);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_website/website_multi_mixins/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_website/website_multi_mixins/createbatch")
    public website_multi_mixinImpl createBatch(@RequestBody List<website_multi_mixinImpl> website_multi_mixins);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_website/website_multi_mixins/search")
    public Page<website_multi_mixinImpl> search(SearchContext context);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_website/website_multi_mixins/updatebatch")
    public website_multi_mixinImpl updateBatch(@RequestBody List<website_multi_mixinImpl> website_multi_mixins);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_website/website_multi_mixins/{id}")
    public website_multi_mixinImpl update(@PathVariable("id") Integer id,@RequestBody website_multi_mixinImpl website_multi_mixin);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_website/website_multi_mixins/removebatch")
    public website_multi_mixinImpl removeBatch(@RequestBody List<website_multi_mixinImpl> website_multi_mixins);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_website/website_multi_mixins/select")
    public Page<website_multi_mixinImpl> select();



}
