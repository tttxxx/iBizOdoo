package cn.ibizlab.odoo.client.odoo_website.odooclient;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.core.client.model.Iwebsite_sale_payment_acquirer_onboarding_wizard;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[website_sale_payment_acquirer_onboarding_wizard] 服务对象客户端接口
 */
public interface Iwebsite_sale_payment_acquirer_onboarding_wizardOdooClient {
    
        public void create(Iwebsite_sale_payment_acquirer_onboarding_wizard website_sale_payment_acquirer_onboarding_wizard);

        public void updateBatch(Iwebsite_sale_payment_acquirer_onboarding_wizard website_sale_payment_acquirer_onboarding_wizard);

        public void removeBatch(Iwebsite_sale_payment_acquirer_onboarding_wizard website_sale_payment_acquirer_onboarding_wizard);

        public void get(Iwebsite_sale_payment_acquirer_onboarding_wizard website_sale_payment_acquirer_onboarding_wizard);

        public void update(Iwebsite_sale_payment_acquirer_onboarding_wizard website_sale_payment_acquirer_onboarding_wizard);

        public void remove(Iwebsite_sale_payment_acquirer_onboarding_wizard website_sale_payment_acquirer_onboarding_wizard);

        public Page<Iwebsite_sale_payment_acquirer_onboarding_wizard> search(SearchContext context);

        public void createBatch(Iwebsite_sale_payment_acquirer_onboarding_wizard website_sale_payment_acquirer_onboarding_wizard);

        public List<Iwebsite_sale_payment_acquirer_onboarding_wizard> select();


}