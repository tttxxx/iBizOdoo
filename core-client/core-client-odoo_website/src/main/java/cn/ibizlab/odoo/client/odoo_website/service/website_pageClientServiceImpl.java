package cn.ibizlab.odoo.client.odoo_website.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Iwebsite_page;
import cn.ibizlab.odoo.core.client.service.Iwebsite_pageClientService;
import cn.ibizlab.odoo.client.odoo_website.model.website_pageImpl;
import cn.ibizlab.odoo.client.odoo_website.odooclient.Iwebsite_pageOdooClient;
import cn.ibizlab.odoo.client.odoo_website.odooclient.impl.website_pageOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[website_page] 服务对象接口
 */
@Service
public class website_pageClientServiceImpl implements Iwebsite_pageClientService {
    @Autowired
    private  Iwebsite_pageOdooClient  website_pageOdooClient;

    public Iwebsite_page createModel() {		
		return new website_pageImpl();
	}


        public void updateBatch(List<Iwebsite_page> website_pages){
            
        }
        
        public void update(Iwebsite_page website_page){
this.website_pageOdooClient.update(website_page) ;
        }
        
        public void get(Iwebsite_page website_page){
            this.website_pageOdooClient.get(website_page) ;
        }
        
        public void create(Iwebsite_page website_page){
this.website_pageOdooClient.create(website_page) ;
        }
        
        public void createBatch(List<Iwebsite_page> website_pages){
            
        }
        
        public void removeBatch(List<Iwebsite_page> website_pages){
            
        }
        
        public void remove(Iwebsite_page website_page){
this.website_pageOdooClient.remove(website_page) ;
        }
        
        public Page<Iwebsite_page> search(SearchContext context){
            return this.website_pageOdooClient.search(context) ;
        }
        
        public Page<Iwebsite_page> select(SearchContext context){
            return null ;
        }
        

}

