package cn.ibizlab.odoo.client.odoo_website.odooclient;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.core.client.model.Iwebsite_menu;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[website_menu] 服务对象客户端接口
 */
public interface Iwebsite_menuOdooClient {
    
        public void get(Iwebsite_menu website_menu);

        public void removeBatch(Iwebsite_menu website_menu);

        public void updateBatch(Iwebsite_menu website_menu);

        public void create(Iwebsite_menu website_menu);

        public void remove(Iwebsite_menu website_menu);

        public void update(Iwebsite_menu website_menu);

        public Page<Iwebsite_menu> search(SearchContext context);

        public void createBatch(Iwebsite_menu website_menu);

        public List<Iwebsite_menu> select();


}