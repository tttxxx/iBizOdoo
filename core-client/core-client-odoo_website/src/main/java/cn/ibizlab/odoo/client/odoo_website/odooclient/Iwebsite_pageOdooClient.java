package cn.ibizlab.odoo.client.odoo_website.odooclient;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.core.client.model.Iwebsite_page;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[website_page] 服务对象客户端接口
 */
public interface Iwebsite_pageOdooClient {
    
        public void updateBatch(Iwebsite_page website_page);

        public void update(Iwebsite_page website_page);

        public void get(Iwebsite_page website_page);

        public void create(Iwebsite_page website_page);

        public void createBatch(Iwebsite_page website_page);

        public void removeBatch(Iwebsite_page website_page);

        public void remove(Iwebsite_page website_page);

        public Page<Iwebsite_page> search(SearchContext context);

        public List<Iwebsite_page> select();


}