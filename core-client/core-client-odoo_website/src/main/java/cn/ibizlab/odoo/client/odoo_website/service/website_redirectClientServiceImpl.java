package cn.ibizlab.odoo.client.odoo_website.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Iwebsite_redirect;
import cn.ibizlab.odoo.core.client.service.Iwebsite_redirectClientService;
import cn.ibizlab.odoo.client.odoo_website.model.website_redirectImpl;
import cn.ibizlab.odoo.client.odoo_website.odooclient.Iwebsite_redirectOdooClient;
import cn.ibizlab.odoo.client.odoo_website.odooclient.impl.website_redirectOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[website_redirect] 服务对象接口
 */
@Service
public class website_redirectClientServiceImpl implements Iwebsite_redirectClientService {
    @Autowired
    private  Iwebsite_redirectOdooClient  website_redirectOdooClient;

    public Iwebsite_redirect createModel() {		
		return new website_redirectImpl();
	}


        public void remove(Iwebsite_redirect website_redirect){
this.website_redirectOdooClient.remove(website_redirect) ;
        }
        
        public void update(Iwebsite_redirect website_redirect){
this.website_redirectOdooClient.update(website_redirect) ;
        }
        
        public Page<Iwebsite_redirect> search(SearchContext context){
            return this.website_redirectOdooClient.search(context) ;
        }
        
        public void get(Iwebsite_redirect website_redirect){
            this.website_redirectOdooClient.get(website_redirect) ;
        }
        
        public void createBatch(List<Iwebsite_redirect> website_redirects){
            
        }
        
        public void updateBatch(List<Iwebsite_redirect> website_redirects){
            
        }
        
        public void create(Iwebsite_redirect website_redirect){
this.website_redirectOdooClient.create(website_redirect) ;
        }
        
        public void removeBatch(List<Iwebsite_redirect> website_redirects){
            
        }
        
        public Page<Iwebsite_redirect> select(SearchContext context){
            return null ;
        }
        

}

