package cn.ibizlab.odoo.client.odoo_website.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Iwebsite_multi_mixin;
import cn.ibizlab.odoo.core.client.service.Iwebsite_multi_mixinClientService;
import cn.ibizlab.odoo.client.odoo_website.model.website_multi_mixinImpl;
import cn.ibizlab.odoo.client.odoo_website.odooclient.Iwebsite_multi_mixinOdooClient;
import cn.ibizlab.odoo.client.odoo_website.odooclient.impl.website_multi_mixinOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[website_multi_mixin] 服务对象接口
 */
@Service
public class website_multi_mixinClientServiceImpl implements Iwebsite_multi_mixinClientService {
    @Autowired
    private  Iwebsite_multi_mixinOdooClient  website_multi_mixinOdooClient;

    public Iwebsite_multi_mixin createModel() {		
		return new website_multi_mixinImpl();
	}


        public void get(Iwebsite_multi_mixin website_multi_mixin){
            this.website_multi_mixinOdooClient.get(website_multi_mixin) ;
        }
        
        public void create(Iwebsite_multi_mixin website_multi_mixin){
this.website_multi_mixinOdooClient.create(website_multi_mixin) ;
        }
        
        public void remove(Iwebsite_multi_mixin website_multi_mixin){
this.website_multi_mixinOdooClient.remove(website_multi_mixin) ;
        }
        
        public void createBatch(List<Iwebsite_multi_mixin> website_multi_mixins){
            
        }
        
        public Page<Iwebsite_multi_mixin> search(SearchContext context){
            return this.website_multi_mixinOdooClient.search(context) ;
        }
        
        public void updateBatch(List<Iwebsite_multi_mixin> website_multi_mixins){
            
        }
        
        public void update(Iwebsite_multi_mixin website_multi_mixin){
this.website_multi_mixinOdooClient.update(website_multi_mixin) ;
        }
        
        public void removeBatch(List<Iwebsite_multi_mixin> website_multi_mixins){
            
        }
        
        public Page<Iwebsite_multi_mixin> select(SearchContext context){
            return null ;
        }
        

}

