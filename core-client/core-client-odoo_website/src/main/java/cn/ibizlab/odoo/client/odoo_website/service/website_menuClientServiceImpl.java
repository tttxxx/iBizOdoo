package cn.ibizlab.odoo.client.odoo_website.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Iwebsite_menu;
import cn.ibizlab.odoo.core.client.service.Iwebsite_menuClientService;
import cn.ibizlab.odoo.client.odoo_website.model.website_menuImpl;
import cn.ibizlab.odoo.client.odoo_website.odooclient.Iwebsite_menuOdooClient;
import cn.ibizlab.odoo.client.odoo_website.odooclient.impl.website_menuOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[website_menu] 服务对象接口
 */
@Service
public class website_menuClientServiceImpl implements Iwebsite_menuClientService {
    @Autowired
    private  Iwebsite_menuOdooClient  website_menuOdooClient;

    public Iwebsite_menu createModel() {		
		return new website_menuImpl();
	}


        public void get(Iwebsite_menu website_menu){
            this.website_menuOdooClient.get(website_menu) ;
        }
        
        public void removeBatch(List<Iwebsite_menu> website_menus){
            
        }
        
        public void updateBatch(List<Iwebsite_menu> website_menus){
            
        }
        
        public void create(Iwebsite_menu website_menu){
this.website_menuOdooClient.create(website_menu) ;
        }
        
        public void remove(Iwebsite_menu website_menu){
this.website_menuOdooClient.remove(website_menu) ;
        }
        
        public void update(Iwebsite_menu website_menu){
this.website_menuOdooClient.update(website_menu) ;
        }
        
        public Page<Iwebsite_menu> search(SearchContext context){
            return this.website_menuOdooClient.search(context) ;
        }
        
        public void createBatch(List<Iwebsite_menu> website_menus){
            
        }
        
        public Page<Iwebsite_menu> select(SearchContext context){
            return null ;
        }
        

}

