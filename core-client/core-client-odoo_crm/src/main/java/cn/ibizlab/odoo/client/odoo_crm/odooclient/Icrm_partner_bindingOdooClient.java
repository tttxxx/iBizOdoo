package cn.ibizlab.odoo.client.odoo_crm.odooclient;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.core.client.model.Icrm_partner_binding;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[crm_partner_binding] 服务对象客户端接口
 */
public interface Icrm_partner_bindingOdooClient {
    
        public Page<Icrm_partner_binding> search(SearchContext context);

        public void update(Icrm_partner_binding crm_partner_binding);

        public void updateBatch(Icrm_partner_binding crm_partner_binding);

        public void removeBatch(Icrm_partner_binding crm_partner_binding);

        public void get(Icrm_partner_binding crm_partner_binding);

        public void remove(Icrm_partner_binding crm_partner_binding);

        public void createBatch(Icrm_partner_binding crm_partner_binding);

        public void create(Icrm_partner_binding crm_partner_binding);

        public List<Icrm_partner_binding> select();


}