package cn.ibizlab.odoo.client.odoo_crm.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import lombok.Data;

@ConfigurationProperties(prefix = "client.odoo.crm")
@Data
public class odoo_crmClientProperties {

	private String tokenUrl ;

	private String clientId ;

	private String clientSecret ;

}
