package cn.ibizlab.odoo.client.odoo_crm.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Icrm_partner_binding;
import cn.ibizlab.odoo.core.client.service.Icrm_partner_bindingClientService;
import cn.ibizlab.odoo.client.odoo_crm.model.crm_partner_bindingImpl;
import cn.ibizlab.odoo.client.odoo_crm.odooclient.Icrm_partner_bindingOdooClient;
import cn.ibizlab.odoo.client.odoo_crm.odooclient.impl.crm_partner_bindingOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[crm_partner_binding] 服务对象接口
 */
@Service
public class crm_partner_bindingClientServiceImpl implements Icrm_partner_bindingClientService {
    @Autowired
    private  Icrm_partner_bindingOdooClient  crm_partner_bindingOdooClient;

    public Icrm_partner_binding createModel() {		
		return new crm_partner_bindingImpl();
	}


        public Page<Icrm_partner_binding> search(SearchContext context){
            return this.crm_partner_bindingOdooClient.search(context) ;
        }
        
        public void update(Icrm_partner_binding crm_partner_binding){
this.crm_partner_bindingOdooClient.update(crm_partner_binding) ;
        }
        
        public void updateBatch(List<Icrm_partner_binding> crm_partner_bindings){
            
        }
        
        public void removeBatch(List<Icrm_partner_binding> crm_partner_bindings){
            
        }
        
        public void get(Icrm_partner_binding crm_partner_binding){
            this.crm_partner_bindingOdooClient.get(crm_partner_binding) ;
        }
        
        public void remove(Icrm_partner_binding crm_partner_binding){
this.crm_partner_bindingOdooClient.remove(crm_partner_binding) ;
        }
        
        public void createBatch(List<Icrm_partner_binding> crm_partner_bindings){
            
        }
        
        public void create(Icrm_partner_binding crm_partner_binding){
this.crm_partner_bindingOdooClient.create(crm_partner_binding) ;
        }
        
        public Page<Icrm_partner_binding> select(SearchContext context){
            return null ;
        }
        

}

