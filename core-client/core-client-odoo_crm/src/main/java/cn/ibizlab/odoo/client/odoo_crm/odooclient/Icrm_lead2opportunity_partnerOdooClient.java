package cn.ibizlab.odoo.client.odoo_crm.odooclient;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.core.client.model.Icrm_lead2opportunity_partner;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[crm_lead2opportunity_partner] 服务对象客户端接口
 */
public interface Icrm_lead2opportunity_partnerOdooClient {
    
        public void createBatch(Icrm_lead2opportunity_partner crm_lead2opportunity_partner);

        public void update(Icrm_lead2opportunity_partner crm_lead2opportunity_partner);

        public Page<Icrm_lead2opportunity_partner> search(SearchContext context);

        public void removeBatch(Icrm_lead2opportunity_partner crm_lead2opportunity_partner);

        public void get(Icrm_lead2opportunity_partner crm_lead2opportunity_partner);

        public void create(Icrm_lead2opportunity_partner crm_lead2opportunity_partner);

        public void updateBatch(Icrm_lead2opportunity_partner crm_lead2opportunity_partner);

        public void remove(Icrm_lead2opportunity_partner crm_lead2opportunity_partner);

        public List<Icrm_lead2opportunity_partner> select();


}