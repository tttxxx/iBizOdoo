package cn.ibizlab.odoo.client.odoo_crm.odooclient;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.core.client.model.Icrm_merge_opportunity;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[crm_merge_opportunity] 服务对象客户端接口
 */
public interface Icrm_merge_opportunityOdooClient {
    
        public void createBatch(Icrm_merge_opportunity crm_merge_opportunity);

        public void remove(Icrm_merge_opportunity crm_merge_opportunity);

        public void updateBatch(Icrm_merge_opportunity crm_merge_opportunity);

        public void update(Icrm_merge_opportunity crm_merge_opportunity);

        public void removeBatch(Icrm_merge_opportunity crm_merge_opportunity);

        public void create(Icrm_merge_opportunity crm_merge_opportunity);

        public void get(Icrm_merge_opportunity crm_merge_opportunity);

        public Page<Icrm_merge_opportunity> search(SearchContext context);

        public List<Icrm_merge_opportunity> select();


}