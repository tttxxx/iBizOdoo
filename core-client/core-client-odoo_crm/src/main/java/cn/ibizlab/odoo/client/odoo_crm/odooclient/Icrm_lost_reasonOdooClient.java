package cn.ibizlab.odoo.client.odoo_crm.odooclient;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.core.client.model.Icrm_lost_reason;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[crm_lost_reason] 服务对象客户端接口
 */
public interface Icrm_lost_reasonOdooClient {
    
        public Page<Icrm_lost_reason> search(SearchContext context);

        public void update(Icrm_lost_reason crm_lost_reason);

        public void removeBatch(Icrm_lost_reason crm_lost_reason);

        public void updateBatch(Icrm_lost_reason crm_lost_reason);

        public void remove(Icrm_lost_reason crm_lost_reason);

        public void create(Icrm_lost_reason crm_lost_reason);

        public void get(Icrm_lost_reason crm_lost_reason);

        public void createBatch(Icrm_lost_reason crm_lost_reason);

        public List<Icrm_lost_reason> select();


}