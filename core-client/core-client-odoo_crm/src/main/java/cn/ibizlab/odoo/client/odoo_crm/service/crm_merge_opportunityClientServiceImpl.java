package cn.ibizlab.odoo.client.odoo_crm.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Icrm_merge_opportunity;
import cn.ibizlab.odoo.core.client.service.Icrm_merge_opportunityClientService;
import cn.ibizlab.odoo.client.odoo_crm.model.crm_merge_opportunityImpl;
import cn.ibizlab.odoo.client.odoo_crm.odooclient.Icrm_merge_opportunityOdooClient;
import cn.ibizlab.odoo.client.odoo_crm.odooclient.impl.crm_merge_opportunityOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[crm_merge_opportunity] 服务对象接口
 */
@Service
public class crm_merge_opportunityClientServiceImpl implements Icrm_merge_opportunityClientService {
    @Autowired
    private  Icrm_merge_opportunityOdooClient  crm_merge_opportunityOdooClient;

    public Icrm_merge_opportunity createModel() {		
		return new crm_merge_opportunityImpl();
	}


        public void createBatch(List<Icrm_merge_opportunity> crm_merge_opportunities){
            
        }
        
        public void remove(Icrm_merge_opportunity crm_merge_opportunity){
this.crm_merge_opportunityOdooClient.remove(crm_merge_opportunity) ;
        }
        
        public void updateBatch(List<Icrm_merge_opportunity> crm_merge_opportunities){
            
        }
        
        public void update(Icrm_merge_opportunity crm_merge_opportunity){
this.crm_merge_opportunityOdooClient.update(crm_merge_opportunity) ;
        }
        
        public void removeBatch(List<Icrm_merge_opportunity> crm_merge_opportunities){
            
        }
        
        public void create(Icrm_merge_opportunity crm_merge_opportunity){
this.crm_merge_opportunityOdooClient.create(crm_merge_opportunity) ;
        }
        
        public void get(Icrm_merge_opportunity crm_merge_opportunity){
            this.crm_merge_opportunityOdooClient.get(crm_merge_opportunity) ;
        }
        
        public Page<Icrm_merge_opportunity> search(SearchContext context){
            return this.crm_merge_opportunityOdooClient.search(context) ;
        }
        
        public Page<Icrm_merge_opportunity> select(SearchContext context){
            return null ;
        }
        

}

