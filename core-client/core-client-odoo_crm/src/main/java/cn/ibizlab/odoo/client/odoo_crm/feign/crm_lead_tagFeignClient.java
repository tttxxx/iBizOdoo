package cn.ibizlab.odoo.client.odoo_crm.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Icrm_lead_tag;
import cn.ibizlab.odoo.client.odoo_crm.model.crm_lead_tagImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[crm_lead_tag] 服务对象接口
 */
public interface crm_lead_tagFeignClient {

    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_crm/crm_lead_tags/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_crm/crm_lead_tags/{id}")
    public crm_lead_tagImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_crm/crm_lead_tags/{id}")
    public crm_lead_tagImpl update(@PathVariable("id") Integer id,@RequestBody crm_lead_tagImpl crm_lead_tag);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_crm/crm_lead_tags/updatebatch")
    public crm_lead_tagImpl updateBatch(@RequestBody List<crm_lead_tagImpl> crm_lead_tags);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_crm/crm_lead_tags")
    public crm_lead_tagImpl create(@RequestBody crm_lead_tagImpl crm_lead_tag);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_crm/crm_lead_tags/removebatch")
    public crm_lead_tagImpl removeBatch(@RequestBody List<crm_lead_tagImpl> crm_lead_tags);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_crm/crm_lead_tags/search")
    public Page<crm_lead_tagImpl> search(SearchContext context);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_crm/crm_lead_tags/createbatch")
    public crm_lead_tagImpl createBatch(@RequestBody List<crm_lead_tagImpl> crm_lead_tags);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_crm/crm_lead_tags/select")
    public Page<crm_lead_tagImpl> select();



}
