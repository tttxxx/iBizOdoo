package cn.ibizlab.odoo.client.odoo_base.odooclient;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.core.client.model.Ires_partner;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[res_partner] 服务对象客户端接口
 */
public interface Ires_partnerOdooClient {
    
        public void get(Ires_partner res_partner);

        public void update(Ires_partner res_partner);

        public Page<Ires_partner> search(SearchContext context);

        public void create(Ires_partner res_partner);

        public void createBatch(Ires_partner res_partner);

        public void removeBatch(Ires_partner res_partner);

        public void updateBatch(Ires_partner res_partner);

        public void remove(Ires_partner res_partner);

        public List<Ires_partner> select();


}