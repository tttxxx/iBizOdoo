package cn.ibizlab.odoo.client.odoo_base.odooclient;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.core.client.model.Ibase_partner_merge_line;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[base_partner_merge_line] 服务对象客户端接口
 */
public interface Ibase_partner_merge_lineOdooClient {
    
        public void create(Ibase_partner_merge_line base_partner_merge_line);

        public Page<Ibase_partner_merge_line> search(SearchContext context);

        public void update(Ibase_partner_merge_line base_partner_merge_line);

        public void updateBatch(Ibase_partner_merge_line base_partner_merge_line);

        public void remove(Ibase_partner_merge_line base_partner_merge_line);

        public void createBatch(Ibase_partner_merge_line base_partner_merge_line);

        public void removeBatch(Ibase_partner_merge_line base_partner_merge_line);

        public void get(Ibase_partner_merge_line base_partner_merge_line);

        public List<Ibase_partner_merge_line> select();


}