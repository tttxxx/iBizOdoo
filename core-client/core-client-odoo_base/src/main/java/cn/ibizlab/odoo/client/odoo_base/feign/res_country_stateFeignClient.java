package cn.ibizlab.odoo.client.odoo_base.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Ires_country_state;
import cn.ibizlab.odoo.client.odoo_base.model.res_country_stateImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[res_country_state] 服务对象接口
 */
public interface res_country_stateFeignClient {

    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_base/res_country_states/{id}")
    public res_country_stateImpl update(@PathVariable("id") Integer id,@RequestBody res_country_stateImpl res_country_state);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_base/res_country_states/createbatch")
    public res_country_stateImpl createBatch(@RequestBody List<res_country_stateImpl> res_country_states);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_base/res_country_states/removebatch")
    public res_country_stateImpl removeBatch(@RequestBody List<res_country_stateImpl> res_country_states);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_base/res_country_states/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_base/res_country_states/{id}")
    public res_country_stateImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_base/res_country_states/search")
    public Page<res_country_stateImpl> search(SearchContext context);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_base/res_country_states")
    public res_country_stateImpl create(@RequestBody res_country_stateImpl res_country_state);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_base/res_country_states/updatebatch")
    public res_country_stateImpl updateBatch(@RequestBody List<res_country_stateImpl> res_country_states);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_base/res_country_states/select")
    public Page<res_country_stateImpl> select();



}
