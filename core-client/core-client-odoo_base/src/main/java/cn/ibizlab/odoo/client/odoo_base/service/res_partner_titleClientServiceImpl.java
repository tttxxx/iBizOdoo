package cn.ibizlab.odoo.client.odoo_base.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Ires_partner_title;
import cn.ibizlab.odoo.core.client.service.Ires_partner_titleClientService;
import cn.ibizlab.odoo.client.odoo_base.model.res_partner_titleImpl;
import cn.ibizlab.odoo.client.odoo_base.odooclient.Ires_partner_titleOdooClient;
import cn.ibizlab.odoo.client.odoo_base.odooclient.impl.res_partner_titleOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[res_partner_title] 服务对象接口
 */
@Service
public class res_partner_titleClientServiceImpl implements Ires_partner_titleClientService {
    @Autowired
    private  Ires_partner_titleOdooClient  res_partner_titleOdooClient;

    public Ires_partner_title createModel() {		
		return new res_partner_titleImpl();
	}


        public void remove(Ires_partner_title res_partner_title){
this.res_partner_titleOdooClient.remove(res_partner_title) ;
        }
        
        public void updateBatch(List<Ires_partner_title> res_partner_titles){
            
        }
        
        public Page<Ires_partner_title> search(SearchContext context){
            return this.res_partner_titleOdooClient.search(context) ;
        }
        
        public void update(Ires_partner_title res_partner_title){
this.res_partner_titleOdooClient.update(res_partner_title) ;
        }
        
        public void create(Ires_partner_title res_partner_title){
this.res_partner_titleOdooClient.create(res_partner_title) ;
        }
        
        public void get(Ires_partner_title res_partner_title){
            this.res_partner_titleOdooClient.get(res_partner_title) ;
        }
        
        public void createBatch(List<Ires_partner_title> res_partner_titles){
            
        }
        
        public void removeBatch(List<Ires_partner_title> res_partner_titles){
            
        }
        
        public Page<Ires_partner_title> select(SearchContext context){
            return null ;
        }
        

}

