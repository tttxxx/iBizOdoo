package cn.ibizlab.odoo.client.odoo_base.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Ires_users_log;
import cn.ibizlab.odoo.core.client.service.Ires_users_logClientService;
import cn.ibizlab.odoo.client.odoo_base.model.res_users_logImpl;
import cn.ibizlab.odoo.client.odoo_base.odooclient.Ires_users_logOdooClient;
import cn.ibizlab.odoo.client.odoo_base.odooclient.impl.res_users_logOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[res_users_log] 服务对象接口
 */
@Service
public class res_users_logClientServiceImpl implements Ires_users_logClientService {
    @Autowired
    private  Ires_users_logOdooClient  res_users_logOdooClient;

    public Ires_users_log createModel() {		
		return new res_users_logImpl();
	}


        public Page<Ires_users_log> search(SearchContext context){
            return this.res_users_logOdooClient.search(context) ;
        }
        
        public void removeBatch(List<Ires_users_log> res_users_logs){
            
        }
        
        public void remove(Ires_users_log res_users_log){
this.res_users_logOdooClient.remove(res_users_log) ;
        }
        
        public void create(Ires_users_log res_users_log){
this.res_users_logOdooClient.create(res_users_log) ;
        }
        
        public void update(Ires_users_log res_users_log){
this.res_users_logOdooClient.update(res_users_log) ;
        }
        
        public void get(Ires_users_log res_users_log){
            this.res_users_logOdooClient.get(res_users_log) ;
        }
        
        public void updateBatch(List<Ires_users_log> res_users_logs){
            
        }
        
        public void createBatch(List<Ires_users_log> res_users_logs){
            
        }
        
        public Page<Ires_users_log> select(SearchContext context){
            return null ;
        }
        

}

