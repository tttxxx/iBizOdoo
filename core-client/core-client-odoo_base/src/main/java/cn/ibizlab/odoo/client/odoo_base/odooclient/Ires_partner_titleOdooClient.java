package cn.ibizlab.odoo.client.odoo_base.odooclient;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.core.client.model.Ires_partner_title;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[res_partner_title] 服务对象客户端接口
 */
public interface Ires_partner_titleOdooClient {
    
        public void remove(Ires_partner_title res_partner_title);

        public void updateBatch(Ires_partner_title res_partner_title);

        public Page<Ires_partner_title> search(SearchContext context);

        public void update(Ires_partner_title res_partner_title);

        public void create(Ires_partner_title res_partner_title);

        public void get(Ires_partner_title res_partner_title);

        public void createBatch(Ires_partner_title res_partner_title);

        public void removeBatch(Ires_partner_title res_partner_title);

        public List<Ires_partner_title> select();


}