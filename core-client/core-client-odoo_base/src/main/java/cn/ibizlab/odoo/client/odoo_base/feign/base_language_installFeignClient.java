package cn.ibizlab.odoo.client.odoo_base.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Ibase_language_install;
import cn.ibizlab.odoo.client.odoo_base.model.base_language_installImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[base_language_install] 服务对象接口
 */
public interface base_language_installFeignClient {

    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_base/base_language_installs/updatebatch")
    public base_language_installImpl updateBatch(@RequestBody List<base_language_installImpl> base_language_installs);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_base/base_language_installs/{id}")
    public base_language_installImpl update(@PathVariable("id") Integer id,@RequestBody base_language_installImpl base_language_install);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_base/base_language_installs/removebatch")
    public base_language_installImpl removeBatch(@RequestBody List<base_language_installImpl> base_language_installs);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_base/base_language_installs/search")
    public Page<base_language_installImpl> search(SearchContext context);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_base/base_language_installs/createbatch")
    public base_language_installImpl createBatch(@RequestBody List<base_language_installImpl> base_language_installs);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_base/base_language_installs")
    public base_language_installImpl create(@RequestBody base_language_installImpl base_language_install);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_base/base_language_installs/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_base/base_language_installs/{id}")
    public base_language_installImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_base/base_language_installs/select")
    public Page<base_language_installImpl> select();



}
