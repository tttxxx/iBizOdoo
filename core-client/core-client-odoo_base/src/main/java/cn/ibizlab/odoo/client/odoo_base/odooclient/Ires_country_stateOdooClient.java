package cn.ibizlab.odoo.client.odoo_base.odooclient;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.core.client.model.Ires_country_state;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[res_country_state] 服务对象客户端接口
 */
public interface Ires_country_stateOdooClient {
    
        public void update(Ires_country_state res_country_state);

        public void createBatch(Ires_country_state res_country_state);

        public void removeBatch(Ires_country_state res_country_state);

        public void remove(Ires_country_state res_country_state);

        public void get(Ires_country_state res_country_state);

        public Page<Ires_country_state> search(SearchContext context);

        public void create(Ires_country_state res_country_state);

        public void updateBatch(Ires_country_state res_country_state);

        public List<Ires_country_state> select();


}