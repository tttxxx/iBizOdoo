package cn.ibizlab.odoo.client.odoo_base.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Ires_country_group;
import cn.ibizlab.odoo.core.client.service.Ires_country_groupClientService;
import cn.ibizlab.odoo.client.odoo_base.model.res_country_groupImpl;
import cn.ibizlab.odoo.client.odoo_base.odooclient.Ires_country_groupOdooClient;
import cn.ibizlab.odoo.client.odoo_base.odooclient.impl.res_country_groupOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[res_country_group] 服务对象接口
 */
@Service
public class res_country_groupClientServiceImpl implements Ires_country_groupClientService {
    @Autowired
    private  Ires_country_groupOdooClient  res_country_groupOdooClient;

    public Ires_country_group createModel() {		
		return new res_country_groupImpl();
	}


        public void get(Ires_country_group res_country_group){
            this.res_country_groupOdooClient.get(res_country_group) ;
        }
        
        public void update(Ires_country_group res_country_group){
this.res_country_groupOdooClient.update(res_country_group) ;
        }
        
        public void removeBatch(List<Ires_country_group> res_country_groups){
            
        }
        
        public void createBatch(List<Ires_country_group> res_country_groups){
            
        }
        
        public void remove(Ires_country_group res_country_group){
this.res_country_groupOdooClient.remove(res_country_group) ;
        }
        
        public void updateBatch(List<Ires_country_group> res_country_groups){
            
        }
        
        public Page<Ires_country_group> search(SearchContext context){
            return this.res_country_groupOdooClient.search(context) ;
        }
        
        public void create(Ires_country_group res_country_group){
this.res_country_groupOdooClient.create(res_country_group) ;
        }
        
        public Page<Ires_country_group> select(SearchContext context){
            return null ;
        }
        

}

