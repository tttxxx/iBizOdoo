package cn.ibizlab.odoo.client.odoo_base.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Ibase_automation;
import cn.ibizlab.odoo.core.client.service.Ibase_automationClientService;
import cn.ibizlab.odoo.client.odoo_base.model.base_automationImpl;
import cn.ibizlab.odoo.client.odoo_base.odooclient.Ibase_automationOdooClient;
import cn.ibizlab.odoo.client.odoo_base.odooclient.impl.base_automationOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[base_automation] 服务对象接口
 */
@Service
public class base_automationClientServiceImpl implements Ibase_automationClientService {
    @Autowired
    private  Ibase_automationOdooClient  base_automationOdooClient;

    public Ibase_automation createModel() {		
		return new base_automationImpl();
	}


        public void removeBatch(List<Ibase_automation> base_automations){
            
        }
        
        public void updateBatch(List<Ibase_automation> base_automations){
            
        }
        
        public void createBatch(List<Ibase_automation> base_automations){
            
        }
        
        public Page<Ibase_automation> search(SearchContext context){
            return this.base_automationOdooClient.search(context) ;
        }
        
        public void create(Ibase_automation base_automation){
this.base_automationOdooClient.create(base_automation) ;
        }
        
        public void get(Ibase_automation base_automation){
            this.base_automationOdooClient.get(base_automation) ;
        }
        
        public void remove(Ibase_automation base_automation){
this.base_automationOdooClient.remove(base_automation) ;
        }
        
        public void update(Ibase_automation base_automation){
this.base_automationOdooClient.update(base_automation) ;
        }
        
        public Page<Ibase_automation> select(SearchContext context){
            return null ;
        }
        

}

