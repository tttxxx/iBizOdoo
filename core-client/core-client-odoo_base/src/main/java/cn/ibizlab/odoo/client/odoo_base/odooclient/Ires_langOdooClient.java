package cn.ibizlab.odoo.client.odoo_base.odooclient;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.core.client.model.Ires_lang;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[res_lang] 服务对象客户端接口
 */
public interface Ires_langOdooClient {
    
        public void get(Ires_lang res_lang);

        public void remove(Ires_lang res_lang);

        public void update(Ires_lang res_lang);

        public void updateBatch(Ires_lang res_lang);

        public void removeBatch(Ires_lang res_lang);

        public void create(Ires_lang res_lang);

        public void createBatch(Ires_lang res_lang);

        public Page<Ires_lang> search(SearchContext context);

        public List<Ires_lang> select();


}