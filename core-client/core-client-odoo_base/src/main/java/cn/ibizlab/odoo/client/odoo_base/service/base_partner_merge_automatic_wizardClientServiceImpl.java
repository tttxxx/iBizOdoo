package cn.ibizlab.odoo.client.odoo_base.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Ibase_partner_merge_automatic_wizard;
import cn.ibizlab.odoo.core.client.service.Ibase_partner_merge_automatic_wizardClientService;
import cn.ibizlab.odoo.client.odoo_base.model.base_partner_merge_automatic_wizardImpl;
import cn.ibizlab.odoo.client.odoo_base.odooclient.Ibase_partner_merge_automatic_wizardOdooClient;
import cn.ibizlab.odoo.client.odoo_base.odooclient.impl.base_partner_merge_automatic_wizardOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[base_partner_merge_automatic_wizard] 服务对象接口
 */
@Service
public class base_partner_merge_automatic_wizardClientServiceImpl implements Ibase_partner_merge_automatic_wizardClientService {
    @Autowired
    private  Ibase_partner_merge_automatic_wizardOdooClient  base_partner_merge_automatic_wizardOdooClient;

    public Ibase_partner_merge_automatic_wizard createModel() {		
		return new base_partner_merge_automatic_wizardImpl();
	}


        public void remove(Ibase_partner_merge_automatic_wizard base_partner_merge_automatic_wizard){
this.base_partner_merge_automatic_wizardOdooClient.remove(base_partner_merge_automatic_wizard) ;
        }
        
        public Page<Ibase_partner_merge_automatic_wizard> search(SearchContext context){
            return this.base_partner_merge_automatic_wizardOdooClient.search(context) ;
        }
        
        public void update(Ibase_partner_merge_automatic_wizard base_partner_merge_automatic_wizard){
this.base_partner_merge_automatic_wizardOdooClient.update(base_partner_merge_automatic_wizard) ;
        }
        
        public void createBatch(List<Ibase_partner_merge_automatic_wizard> base_partner_merge_automatic_wizards){
            
        }
        
        public void updateBatch(List<Ibase_partner_merge_automatic_wizard> base_partner_merge_automatic_wizards){
            
        }
        
        public void get(Ibase_partner_merge_automatic_wizard base_partner_merge_automatic_wizard){
            this.base_partner_merge_automatic_wizardOdooClient.get(base_partner_merge_automatic_wizard) ;
        }
        
        public void create(Ibase_partner_merge_automatic_wizard base_partner_merge_automatic_wizard){
this.base_partner_merge_automatic_wizardOdooClient.create(base_partner_merge_automatic_wizard) ;
        }
        
        public void removeBatch(List<Ibase_partner_merge_automatic_wizard> base_partner_merge_automatic_wizards){
            
        }
        
        public Page<Ibase_partner_merge_automatic_wizard> select(SearchContext context){
            return null ;
        }
        

}

