package cn.ibizlab.odoo.client.odoo_base.odooclient;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.core.client.model.Ires_users;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[res_users] 服务对象客户端接口
 */
public interface Ires_usersOdooClient {
    
        public void get(Ires_users res_users);

        public void removeBatch(Ires_users res_users);

        public void remove(Ires_users res_users);

        public void create(Ires_users res_users);

        public void updateBatch(Ires_users res_users);

        public void createBatch(Ires_users res_users);

        public void update(Ires_users res_users);

        public Page<Ires_users> search(SearchContext context);

        public List<Ires_users> select();


}