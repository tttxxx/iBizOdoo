package cn.ibizlab.odoo.client.odoo_base.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Ires_partner_industry;
import cn.ibizlab.odoo.core.client.service.Ires_partner_industryClientService;
import cn.ibizlab.odoo.client.odoo_base.model.res_partner_industryImpl;
import cn.ibizlab.odoo.client.odoo_base.odooclient.Ires_partner_industryOdooClient;
import cn.ibizlab.odoo.client.odoo_base.odooclient.impl.res_partner_industryOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[res_partner_industry] 服务对象接口
 */
@Service
public class res_partner_industryClientServiceImpl implements Ires_partner_industryClientService {
    @Autowired
    private  Ires_partner_industryOdooClient  res_partner_industryOdooClient;

    public Ires_partner_industry createModel() {		
		return new res_partner_industryImpl();
	}


        public void create(Ires_partner_industry res_partner_industry){
this.res_partner_industryOdooClient.create(res_partner_industry) ;
        }
        
        public void updateBatch(List<Ires_partner_industry> res_partner_industries){
            
        }
        
        public void update(Ires_partner_industry res_partner_industry){
this.res_partner_industryOdooClient.update(res_partner_industry) ;
        }
        
        public void createBatch(List<Ires_partner_industry> res_partner_industries){
            
        }
        
        public Page<Ires_partner_industry> search(SearchContext context){
            return this.res_partner_industryOdooClient.search(context) ;
        }
        
        public void removeBatch(List<Ires_partner_industry> res_partner_industries){
            
        }
        
        public void remove(Ires_partner_industry res_partner_industry){
this.res_partner_industryOdooClient.remove(res_partner_industry) ;
        }
        
        public void get(Ires_partner_industry res_partner_industry){
            this.res_partner_industryOdooClient.get(res_partner_industry) ;
        }
        
        public Page<Ires_partner_industry> select(SearchContext context){
            return null ;
        }
        

}

