package cn.ibizlab.odoo.client.odoo_base.odooclient;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.core.client.model.Ibase_module_update;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[base_module_update] 服务对象客户端接口
 */
public interface Ibase_module_updateOdooClient {
    
        public void updateBatch(Ibase_module_update base_module_update);

        public void update(Ibase_module_update base_module_update);

        public void createBatch(Ibase_module_update base_module_update);

        public void create(Ibase_module_update base_module_update);

        public void remove(Ibase_module_update base_module_update);

        public Page<Ibase_module_update> search(SearchContext context);

        public void get(Ibase_module_update base_module_update);

        public void removeBatch(Ibase_module_update base_module_update);

        public List<Ibase_module_update> select();


}