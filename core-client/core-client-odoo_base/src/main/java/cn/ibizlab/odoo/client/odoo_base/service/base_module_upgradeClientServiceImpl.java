package cn.ibizlab.odoo.client.odoo_base.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Ibase_module_upgrade;
import cn.ibizlab.odoo.core.client.service.Ibase_module_upgradeClientService;
import cn.ibizlab.odoo.client.odoo_base.model.base_module_upgradeImpl;
import cn.ibizlab.odoo.client.odoo_base.odooclient.Ibase_module_upgradeOdooClient;
import cn.ibizlab.odoo.client.odoo_base.odooclient.impl.base_module_upgradeOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[base_module_upgrade] 服务对象接口
 */
@Service
public class base_module_upgradeClientServiceImpl implements Ibase_module_upgradeClientService {
    @Autowired
    private  Ibase_module_upgradeOdooClient  base_module_upgradeOdooClient;

    public Ibase_module_upgrade createModel() {		
		return new base_module_upgradeImpl();
	}


        public void update(Ibase_module_upgrade base_module_upgrade){
this.base_module_upgradeOdooClient.update(base_module_upgrade) ;
        }
        
        public void createBatch(List<Ibase_module_upgrade> base_module_upgrades){
            
        }
        
        public Page<Ibase_module_upgrade> search(SearchContext context){
            return this.base_module_upgradeOdooClient.search(context) ;
        }
        
        public void remove(Ibase_module_upgrade base_module_upgrade){
this.base_module_upgradeOdooClient.remove(base_module_upgrade) ;
        }
        
        public void get(Ibase_module_upgrade base_module_upgrade){
            this.base_module_upgradeOdooClient.get(base_module_upgrade) ;
        }
        
        public void removeBatch(List<Ibase_module_upgrade> base_module_upgrades){
            
        }
        
        public void create(Ibase_module_upgrade base_module_upgrade){
this.base_module_upgradeOdooClient.create(base_module_upgrade) ;
        }
        
        public void updateBatch(List<Ibase_module_upgrade> base_module_upgrades){
            
        }
        
        public Page<Ibase_module_upgrade> select(SearchContext context){
            return null ;
        }
        

}

