package cn.ibizlab.odoo.client.odoo_base.odooclient;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.core.client.model.Ires_config_settings;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[res_config_settings] 服务对象客户端接口
 */
public interface Ires_config_settingsOdooClient {
    
        public void createBatch(Ires_config_settings res_config_settings);

        public Page<Ires_config_settings> search(SearchContext context);

        public void updateBatch(Ires_config_settings res_config_settings);

        public void removeBatch(Ires_config_settings res_config_settings);

        public void update(Ires_config_settings res_config_settings);

        public void remove(Ires_config_settings res_config_settings);

        public void create(Ires_config_settings res_config_settings);

        public void get(Ires_config_settings res_config_settings);

        public List<Ires_config_settings> select();


}