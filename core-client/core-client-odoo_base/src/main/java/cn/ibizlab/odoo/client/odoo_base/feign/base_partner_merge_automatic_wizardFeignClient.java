package cn.ibizlab.odoo.client.odoo_base.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Ibase_partner_merge_automatic_wizard;
import cn.ibizlab.odoo.client.odoo_base.model.base_partner_merge_automatic_wizardImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[base_partner_merge_automatic_wizard] 服务对象接口
 */
public interface base_partner_merge_automatic_wizardFeignClient {

    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_base/base_partner_merge_automatic_wizards/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_base/base_partner_merge_automatic_wizards/search")
    public Page<base_partner_merge_automatic_wizardImpl> search(SearchContext context);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_base/base_partner_merge_automatic_wizards/{id}")
    public base_partner_merge_automatic_wizardImpl update(@PathVariable("id") Integer id,@RequestBody base_partner_merge_automatic_wizardImpl base_partner_merge_automatic_wizard);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_base/base_partner_merge_automatic_wizards/createbatch")
    public base_partner_merge_automatic_wizardImpl createBatch(@RequestBody List<base_partner_merge_automatic_wizardImpl> base_partner_merge_automatic_wizards);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_base/base_partner_merge_automatic_wizards/updatebatch")
    public base_partner_merge_automatic_wizardImpl updateBatch(@RequestBody List<base_partner_merge_automatic_wizardImpl> base_partner_merge_automatic_wizards);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_base/base_partner_merge_automatic_wizards/{id}")
    public base_partner_merge_automatic_wizardImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_base/base_partner_merge_automatic_wizards")
    public base_partner_merge_automatic_wizardImpl create(@RequestBody base_partner_merge_automatic_wizardImpl base_partner_merge_automatic_wizard);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_base/base_partner_merge_automatic_wizards/removebatch")
    public base_partner_merge_automatic_wizardImpl removeBatch(@RequestBody List<base_partner_merge_automatic_wizardImpl> base_partner_merge_automatic_wizards);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_base/base_partner_merge_automatic_wizards/select")
    public Page<base_partner_merge_automatic_wizardImpl> select();



}
