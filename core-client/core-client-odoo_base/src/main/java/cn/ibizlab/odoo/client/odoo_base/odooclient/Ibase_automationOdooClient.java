package cn.ibizlab.odoo.client.odoo_base.odooclient;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.core.client.model.Ibase_automation;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[base_automation] 服务对象客户端接口
 */
public interface Ibase_automationOdooClient {
    
        public void removeBatch(Ibase_automation base_automation);

        public void updateBatch(Ibase_automation base_automation);

        public void createBatch(Ibase_automation base_automation);

        public Page<Ibase_automation> search(SearchContext context);

        public void create(Ibase_automation base_automation);

        public void get(Ibase_automation base_automation);

        public void remove(Ibase_automation base_automation);

        public void update(Ibase_automation base_automation);

        public List<Ibase_automation> select();


}