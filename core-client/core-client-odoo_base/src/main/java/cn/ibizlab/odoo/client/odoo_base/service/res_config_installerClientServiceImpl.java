package cn.ibizlab.odoo.client.odoo_base.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Ires_config_installer;
import cn.ibizlab.odoo.core.client.service.Ires_config_installerClientService;
import cn.ibizlab.odoo.client.odoo_base.model.res_config_installerImpl;
import cn.ibizlab.odoo.client.odoo_base.odooclient.Ires_config_installerOdooClient;
import cn.ibizlab.odoo.client.odoo_base.odooclient.impl.res_config_installerOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[res_config_installer] 服务对象接口
 */
@Service
public class res_config_installerClientServiceImpl implements Ires_config_installerClientService {
    @Autowired
    private  Ires_config_installerOdooClient  res_config_installerOdooClient;

    public Ires_config_installer createModel() {		
		return new res_config_installerImpl();
	}


        public void updateBatch(List<Ires_config_installer> res_config_installers){
            
        }
        
        public void remove(Ires_config_installer res_config_installer){
this.res_config_installerOdooClient.remove(res_config_installer) ;
        }
        
        public void create(Ires_config_installer res_config_installer){
this.res_config_installerOdooClient.create(res_config_installer) ;
        }
        
        public void removeBatch(List<Ires_config_installer> res_config_installers){
            
        }
        
        public Page<Ires_config_installer> search(SearchContext context){
            return this.res_config_installerOdooClient.search(context) ;
        }
        
        public void update(Ires_config_installer res_config_installer){
this.res_config_installerOdooClient.update(res_config_installer) ;
        }
        
        public void get(Ires_config_installer res_config_installer){
            this.res_config_installerOdooClient.get(res_config_installer) ;
        }
        
        public void createBatch(List<Ires_config_installer> res_config_installers){
            
        }
        
        public Page<Ires_config_installer> select(SearchContext context){
            return null ;
        }
        

}

