package cn.ibizlab.odoo.client.odoo_base.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Ibase_update_translations;
import cn.ibizlab.odoo.client.odoo_base.model.base_update_translationsImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[base_update_translations] 服务对象接口
 */
public interface base_update_translationsFeignClient {

    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_base/base_update_translations/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_base/base_update_translations/search")
    public Page<base_update_translationsImpl> search(SearchContext context);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_base/base_update_translations/{id}")
    public base_update_translationsImpl update(@PathVariable("id") Integer id,@RequestBody base_update_translationsImpl base_update_translations);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_base/base_update_translations")
    public base_update_translationsImpl create(@RequestBody base_update_translationsImpl base_update_translations);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_base/base_update_translations/updatebatch")
    public base_update_translationsImpl updateBatch(@RequestBody List<base_update_translationsImpl> base_update_translations);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_base/base_update_translations/createbatch")
    public base_update_translationsImpl createBatch(@RequestBody List<base_update_translationsImpl> base_update_translations);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_base/base_update_translations/removebatch")
    public base_update_translationsImpl removeBatch(@RequestBody List<base_update_translationsImpl> base_update_translations);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_base/base_update_translations/{id}")
    public base_update_translationsImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_base/base_update_translations/select")
    public Page<base_update_translationsImpl> select();



}
