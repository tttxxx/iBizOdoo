package cn.ibizlab.odoo.client.odoo_base.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Ires_currency;
import cn.ibizlab.odoo.core.client.service.Ires_currencyClientService;
import cn.ibizlab.odoo.client.odoo_base.model.res_currencyImpl;
import cn.ibizlab.odoo.client.odoo_base.odooclient.Ires_currencyOdooClient;
import cn.ibizlab.odoo.client.odoo_base.odooclient.impl.res_currencyOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[res_currency] 服务对象接口
 */
@Service
public class res_currencyClientServiceImpl implements Ires_currencyClientService {
    @Autowired
    private  Ires_currencyOdooClient  res_currencyOdooClient;

    public Ires_currency createModel() {		
		return new res_currencyImpl();
	}


        public void get(Ires_currency res_currency){
            this.res_currencyOdooClient.get(res_currency) ;
        }
        
        public void updateBatch(List<Ires_currency> res_currencies){
            
        }
        
        public Page<Ires_currency> search(SearchContext context){
            return this.res_currencyOdooClient.search(context) ;
        }
        
        public void remove(Ires_currency res_currency){
this.res_currencyOdooClient.remove(res_currency) ;
        }
        
        public void update(Ires_currency res_currency){
this.res_currencyOdooClient.update(res_currency) ;
        }
        
        public void create(Ires_currency res_currency){
this.res_currencyOdooClient.create(res_currency) ;
        }
        
        public void removeBatch(List<Ires_currency> res_currencies){
            
        }
        
        public void createBatch(List<Ires_currency> res_currencies){
            
        }
        
        public Page<Ires_currency> select(SearchContext context){
            return null ;
        }
        

}

