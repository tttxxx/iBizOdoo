package cn.ibizlab.odoo.client.odoo_base.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Ibase_partner_merge_line;
import cn.ibizlab.odoo.core.client.service.Ibase_partner_merge_lineClientService;
import cn.ibizlab.odoo.client.odoo_base.model.base_partner_merge_lineImpl;
import cn.ibizlab.odoo.client.odoo_base.odooclient.Ibase_partner_merge_lineOdooClient;
import cn.ibizlab.odoo.client.odoo_base.odooclient.impl.base_partner_merge_lineOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[base_partner_merge_line] 服务对象接口
 */
@Service
public class base_partner_merge_lineClientServiceImpl implements Ibase_partner_merge_lineClientService {
    @Autowired
    private  Ibase_partner_merge_lineOdooClient  base_partner_merge_lineOdooClient;

    public Ibase_partner_merge_line createModel() {		
		return new base_partner_merge_lineImpl();
	}


        public void create(Ibase_partner_merge_line base_partner_merge_line){
this.base_partner_merge_lineOdooClient.create(base_partner_merge_line) ;
        }
        
        public Page<Ibase_partner_merge_line> search(SearchContext context){
            return this.base_partner_merge_lineOdooClient.search(context) ;
        }
        
        public void update(Ibase_partner_merge_line base_partner_merge_line){
this.base_partner_merge_lineOdooClient.update(base_partner_merge_line) ;
        }
        
        public void updateBatch(List<Ibase_partner_merge_line> base_partner_merge_lines){
            
        }
        
        public void remove(Ibase_partner_merge_line base_partner_merge_line){
this.base_partner_merge_lineOdooClient.remove(base_partner_merge_line) ;
        }
        
        public void createBatch(List<Ibase_partner_merge_line> base_partner_merge_lines){
            
        }
        
        public void removeBatch(List<Ibase_partner_merge_line> base_partner_merge_lines){
            
        }
        
        public void get(Ibase_partner_merge_line base_partner_merge_line){
            this.base_partner_merge_lineOdooClient.get(base_partner_merge_line) ;
        }
        
        public Page<Ibase_partner_merge_line> select(SearchContext context){
            return null ;
        }
        

}

