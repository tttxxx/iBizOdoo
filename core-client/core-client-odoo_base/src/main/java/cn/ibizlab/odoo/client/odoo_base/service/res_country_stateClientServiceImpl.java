package cn.ibizlab.odoo.client.odoo_base.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Ires_country_state;
import cn.ibizlab.odoo.core.client.service.Ires_country_stateClientService;
import cn.ibizlab.odoo.client.odoo_base.model.res_country_stateImpl;
import cn.ibizlab.odoo.client.odoo_base.odooclient.Ires_country_stateOdooClient;
import cn.ibizlab.odoo.client.odoo_base.odooclient.impl.res_country_stateOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[res_country_state] 服务对象接口
 */
@Service
public class res_country_stateClientServiceImpl implements Ires_country_stateClientService {
    @Autowired
    private  Ires_country_stateOdooClient  res_country_stateOdooClient;

    public Ires_country_state createModel() {		
		return new res_country_stateImpl();
	}


        public void update(Ires_country_state res_country_state){
this.res_country_stateOdooClient.update(res_country_state) ;
        }
        
        public void createBatch(List<Ires_country_state> res_country_states){
            
        }
        
        public void removeBatch(List<Ires_country_state> res_country_states){
            
        }
        
        public void remove(Ires_country_state res_country_state){
this.res_country_stateOdooClient.remove(res_country_state) ;
        }
        
        public void get(Ires_country_state res_country_state){
            this.res_country_stateOdooClient.get(res_country_state) ;
        }
        
        public Page<Ires_country_state> search(SearchContext context){
            return this.res_country_stateOdooClient.search(context) ;
        }
        
        public void create(Ires_country_state res_country_state){
this.res_country_stateOdooClient.create(res_country_state) ;
        }
        
        public void updateBatch(List<Ires_country_state> res_country_states){
            
        }
        
        public Page<Ires_country_state> select(SearchContext context){
            return null ;
        }
        

}

