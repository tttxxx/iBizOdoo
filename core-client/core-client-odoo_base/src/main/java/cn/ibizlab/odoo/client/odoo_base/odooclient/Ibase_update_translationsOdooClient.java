package cn.ibizlab.odoo.client.odoo_base.odooclient;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.core.client.model.Ibase_update_translations;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[base_update_translations] 服务对象客户端接口
 */
public interface Ibase_update_translationsOdooClient {
    
        public void remove(Ibase_update_translations base_update_translations);

        public Page<Ibase_update_translations> search(SearchContext context);

        public void update(Ibase_update_translations base_update_translations);

        public void create(Ibase_update_translations base_update_translations);

        public void updateBatch(Ibase_update_translations base_update_translations);

        public void createBatch(Ibase_update_translations base_update_translations);

        public void removeBatch(Ibase_update_translations base_update_translations);

        public void get(Ibase_update_translations base_update_translations);

        public List<Ibase_update_translations> select();


}