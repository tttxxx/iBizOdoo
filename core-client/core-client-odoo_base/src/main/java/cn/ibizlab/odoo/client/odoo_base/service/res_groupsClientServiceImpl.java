package cn.ibizlab.odoo.client.odoo_base.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Ires_groups;
import cn.ibizlab.odoo.core.client.service.Ires_groupsClientService;
import cn.ibizlab.odoo.client.odoo_base.model.res_groupsImpl;
import cn.ibizlab.odoo.client.odoo_base.odooclient.Ires_groupsOdooClient;
import cn.ibizlab.odoo.client.odoo_base.odooclient.impl.res_groupsOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[res_groups] 服务对象接口
 */
@Service
public class res_groupsClientServiceImpl implements Ires_groupsClientService {
    @Autowired
    private  Ires_groupsOdooClient  res_groupsOdooClient;

    public Ires_groups createModel() {		
		return new res_groupsImpl();
	}


        public void removeBatch(List<Ires_groups> res_groups){
            
        }
        
        public void remove(Ires_groups res_groups){
this.res_groupsOdooClient.remove(res_groups) ;
        }
        
        public void create(Ires_groups res_groups){
this.res_groupsOdooClient.create(res_groups) ;
        }
        
        public void update(Ires_groups res_groups){
this.res_groupsOdooClient.update(res_groups) ;
        }
        
        public void createBatch(List<Ires_groups> res_groups){
            
        }
        
        public void get(Ires_groups res_groups){
            this.res_groupsOdooClient.get(res_groups) ;
        }
        
        public Page<Ires_groups> search(SearchContext context){
            return this.res_groupsOdooClient.search(context) ;
        }
        
        public void updateBatch(List<Ires_groups> res_groups){
            
        }
        
        public Page<Ires_groups> select(SearchContext context){
            return null ;
        }
        

}

