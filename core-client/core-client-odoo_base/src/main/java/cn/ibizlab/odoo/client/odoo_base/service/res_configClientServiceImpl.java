package cn.ibizlab.odoo.client.odoo_base.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Ires_config;
import cn.ibizlab.odoo.core.client.service.Ires_configClientService;
import cn.ibizlab.odoo.client.odoo_base.model.res_configImpl;
import cn.ibizlab.odoo.client.odoo_base.odooclient.Ires_configOdooClient;
import cn.ibizlab.odoo.client.odoo_base.odooclient.impl.res_configOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[res_config] 服务对象接口
 */
@Service
public class res_configClientServiceImpl implements Ires_configClientService {
    @Autowired
    private  Ires_configOdooClient  res_configOdooClient;

    public Ires_config createModel() {		
		return new res_configImpl();
	}


        public void remove(Ires_config res_config){
this.res_configOdooClient.remove(res_config) ;
        }
        
        public void create(Ires_config res_config){
this.res_configOdooClient.create(res_config) ;
        }
        
        public void updateBatch(List<Ires_config> res_configs){
            
        }
        
        public void get(Ires_config res_config){
            this.res_configOdooClient.get(res_config) ;
        }
        
        public void update(Ires_config res_config){
this.res_configOdooClient.update(res_config) ;
        }
        
        public void createBatch(List<Ires_config> res_configs){
            
        }
        
        public Page<Ires_config> search(SearchContext context){
            return this.res_configOdooClient.search(context) ;
        }
        
        public void removeBatch(List<Ires_config> res_configs){
            
        }
        
        public Page<Ires_config> select(SearchContext context){
            return null ;
        }
        

}

