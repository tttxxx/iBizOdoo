package cn.ibizlab.odoo.client.odoo_base.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Ires_config_installer;
import cn.ibizlab.odoo.client.odoo_base.model.res_config_installerImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[res_config_installer] 服务对象接口
 */
public interface res_config_installerFeignClient {

    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_base/res_config_installers/updatebatch")
    public res_config_installerImpl updateBatch(@RequestBody List<res_config_installerImpl> res_config_installers);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_base/res_config_installers/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_base/res_config_installers")
    public res_config_installerImpl create(@RequestBody res_config_installerImpl res_config_installer);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_base/res_config_installers/removebatch")
    public res_config_installerImpl removeBatch(@RequestBody List<res_config_installerImpl> res_config_installers);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_base/res_config_installers/search")
    public Page<res_config_installerImpl> search(SearchContext context);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_base/res_config_installers/{id}")
    public res_config_installerImpl update(@PathVariable("id") Integer id,@RequestBody res_config_installerImpl res_config_installer);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_base/res_config_installers/{id}")
    public res_config_installerImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_base/res_config_installers/createbatch")
    public res_config_installerImpl createBatch(@RequestBody List<res_config_installerImpl> res_config_installers);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_base/res_config_installers/select")
    public Page<res_config_installerImpl> select();



}
