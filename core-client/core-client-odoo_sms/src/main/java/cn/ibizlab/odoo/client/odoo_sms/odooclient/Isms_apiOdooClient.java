package cn.ibizlab.odoo.client.odoo_sms.odooclient;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.core.client.model.Isms_api;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[sms_api] 服务对象客户端接口
 */
public interface Isms_apiOdooClient {
    
        public void create(Isms_api sms_api);

        public void updateBatch(Isms_api sms_api);

        public Page<Isms_api> search(SearchContext context);

        public void removeBatch(Isms_api sms_api);

        public void remove(Isms_api sms_api);

        public void get(Isms_api sms_api);

        public void update(Isms_api sms_api);

        public void createBatch(Isms_api sms_api);

        public List<Isms_api> select();


}