package cn.ibizlab.odoo.client.odoo_sms.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import lombok.Data;

@ConfigurationProperties(prefix = "client.odoo.sms")
@Data
public class odoo_smsClientProperties {

	private String tokenUrl ;

	private String clientId ;

	private String clientSecret ;

}
