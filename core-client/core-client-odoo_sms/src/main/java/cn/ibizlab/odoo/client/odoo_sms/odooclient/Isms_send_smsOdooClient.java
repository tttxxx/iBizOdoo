package cn.ibizlab.odoo.client.odoo_sms.odooclient;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.core.client.model.Isms_send_sms;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[sms_send_sms] 服务对象客户端接口
 */
public interface Isms_send_smsOdooClient {
    
        public void remove(Isms_send_sms sms_send_sms);

        public void updateBatch(Isms_send_sms sms_send_sms);

        public void get(Isms_send_sms sms_send_sms);

        public void create(Isms_send_sms sms_send_sms);

        public Page<Isms_send_sms> search(SearchContext context);

        public void removeBatch(Isms_send_sms sms_send_sms);

        public void update(Isms_send_sms sms_send_sms);

        public void createBatch(Isms_send_sms sms_send_sms);

        public List<Isms_send_sms> select();


}