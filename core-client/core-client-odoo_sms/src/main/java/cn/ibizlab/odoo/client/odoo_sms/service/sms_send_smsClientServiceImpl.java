package cn.ibizlab.odoo.client.odoo_sms.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Isms_send_sms;
import cn.ibizlab.odoo.core.client.service.Isms_send_smsClientService;
import cn.ibizlab.odoo.client.odoo_sms.model.sms_send_smsImpl;
import cn.ibizlab.odoo.client.odoo_sms.odooclient.Isms_send_smsOdooClient;
import cn.ibizlab.odoo.client.odoo_sms.odooclient.impl.sms_send_smsOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[sms_send_sms] 服务对象接口
 */
@Service
public class sms_send_smsClientServiceImpl implements Isms_send_smsClientService {
    @Autowired
    private  Isms_send_smsOdooClient  sms_send_smsOdooClient;

    public Isms_send_sms createModel() {		
		return new sms_send_smsImpl();
	}


        public void remove(Isms_send_sms sms_send_sms){
this.sms_send_smsOdooClient.remove(sms_send_sms) ;
        }
        
        public void updateBatch(List<Isms_send_sms> sms_send_sms){
            
        }
        
        public void get(Isms_send_sms sms_send_sms){
            this.sms_send_smsOdooClient.get(sms_send_sms) ;
        }
        
        public void create(Isms_send_sms sms_send_sms){
this.sms_send_smsOdooClient.create(sms_send_sms) ;
        }
        
        public Page<Isms_send_sms> search(SearchContext context){
            return this.sms_send_smsOdooClient.search(context) ;
        }
        
        public void removeBatch(List<Isms_send_sms> sms_send_sms){
            
        }
        
        public void update(Isms_send_sms sms_send_sms){
this.sms_send_smsOdooClient.update(sms_send_sms) ;
        }
        
        public void createBatch(List<Isms_send_sms> sms_send_sms){
            
        }
        
        public Page<Isms_send_sms> select(SearchContext context){
            return null ;
        }
        

}

