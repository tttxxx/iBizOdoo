package cn.ibizlab.odoo.client.odoo_barcodes.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Ibarcodes_barcode_events_mixin;
import cn.ibizlab.odoo.core.client.service.Ibarcodes_barcode_events_mixinClientService;
import cn.ibizlab.odoo.client.odoo_barcodes.model.barcodes_barcode_events_mixinImpl;
import cn.ibizlab.odoo.client.odoo_barcodes.odooclient.Ibarcodes_barcode_events_mixinOdooClient;
import cn.ibizlab.odoo.client.odoo_barcodes.odooclient.impl.barcodes_barcode_events_mixinOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[barcodes_barcode_events_mixin] 服务对象接口
 */
@Service
public class barcodes_barcode_events_mixinClientServiceImpl implements Ibarcodes_barcode_events_mixinClientService {
    @Autowired
    private  Ibarcodes_barcode_events_mixinOdooClient  barcodes_barcode_events_mixinOdooClient;

    public Ibarcodes_barcode_events_mixin createModel() {		
		return new barcodes_barcode_events_mixinImpl();
	}


        public void updateBatch(List<Ibarcodes_barcode_events_mixin> barcodes_barcode_events_mixins){
            
        }
        
        public void create(Ibarcodes_barcode_events_mixin barcodes_barcode_events_mixin){
this.barcodes_barcode_events_mixinOdooClient.create(barcodes_barcode_events_mixin) ;
        }
        
        public void update(Ibarcodes_barcode_events_mixin barcodes_barcode_events_mixin){
this.barcodes_barcode_events_mixinOdooClient.update(barcodes_barcode_events_mixin) ;
        }
        
        public void remove(Ibarcodes_barcode_events_mixin barcodes_barcode_events_mixin){
this.barcodes_barcode_events_mixinOdooClient.remove(barcodes_barcode_events_mixin) ;
        }
        
        public Page<Ibarcodes_barcode_events_mixin> search(SearchContext context){
            return this.barcodes_barcode_events_mixinOdooClient.search(context) ;
        }
        
        public void get(Ibarcodes_barcode_events_mixin barcodes_barcode_events_mixin){
            this.barcodes_barcode_events_mixinOdooClient.get(barcodes_barcode_events_mixin) ;
        }
        
        public void removeBatch(List<Ibarcodes_barcode_events_mixin> barcodes_barcode_events_mixins){
            
        }
        
        public void createBatch(List<Ibarcodes_barcode_events_mixin> barcodes_barcode_events_mixins){
            
        }
        
        public Page<Ibarcodes_barcode_events_mixin> select(SearchContext context){
            return null ;
        }
        

}

