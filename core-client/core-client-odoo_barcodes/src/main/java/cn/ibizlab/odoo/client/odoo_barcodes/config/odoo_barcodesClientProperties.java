package cn.ibizlab.odoo.client.odoo_barcodes.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import lombok.Data;

@ConfigurationProperties(prefix = "client.odoo.barcodes")
@Data
public class odoo_barcodesClientProperties {

	private String tokenUrl ;

	private String clientId ;

	private String clientSecret ;

}
