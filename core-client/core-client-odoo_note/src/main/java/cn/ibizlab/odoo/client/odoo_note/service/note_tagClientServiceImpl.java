package cn.ibizlab.odoo.client.odoo_note.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Inote_tag;
import cn.ibizlab.odoo.core.client.service.Inote_tagClientService;
import cn.ibizlab.odoo.client.odoo_note.model.note_tagImpl;
import cn.ibizlab.odoo.client.odoo_note.odooclient.Inote_tagOdooClient;
import cn.ibizlab.odoo.client.odoo_note.odooclient.impl.note_tagOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[note_tag] 服务对象接口
 */
@Service
public class note_tagClientServiceImpl implements Inote_tagClientService {
    @Autowired
    private  Inote_tagOdooClient  note_tagOdooClient;

    public Inote_tag createModel() {		
		return new note_tagImpl();
	}


        public void removeBatch(List<Inote_tag> note_tags){
            
        }
        
        public Page<Inote_tag> search(SearchContext context){
            return this.note_tagOdooClient.search(context) ;
        }
        
        public void get(Inote_tag note_tag){
            this.note_tagOdooClient.get(note_tag) ;
        }
        
        public void create(Inote_tag note_tag){
this.note_tagOdooClient.create(note_tag) ;
        }
        
        public void remove(Inote_tag note_tag){
this.note_tagOdooClient.remove(note_tag) ;
        }
        
        public void createBatch(List<Inote_tag> note_tags){
            
        }
        
        public void updateBatch(List<Inote_tag> note_tags){
            
        }
        
        public void update(Inote_tag note_tag){
this.note_tagOdooClient.update(note_tag) ;
        }
        
        public Page<Inote_tag> select(SearchContext context){
            return null ;
        }
        

}

