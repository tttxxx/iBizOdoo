package cn.ibizlab.odoo.client.odoo_mrp.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Imrp_workcenter;
import cn.ibizlab.odoo.core.client.service.Imrp_workcenterClientService;
import cn.ibizlab.odoo.client.odoo_mrp.model.mrp_workcenterImpl;
import cn.ibizlab.odoo.client.odoo_mrp.odooclient.Imrp_workcenterOdooClient;
import cn.ibizlab.odoo.client.odoo_mrp.odooclient.impl.mrp_workcenterOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[mrp_workcenter] 服务对象接口
 */
@Service
public class mrp_workcenterClientServiceImpl implements Imrp_workcenterClientService {
    @Autowired
    private  Imrp_workcenterOdooClient  mrp_workcenterOdooClient;

    public Imrp_workcenter createModel() {		
		return new mrp_workcenterImpl();
	}


        public void updateBatch(List<Imrp_workcenter> mrp_workcenters){
            
        }
        
        public void createBatch(List<Imrp_workcenter> mrp_workcenters){
            
        }
        
        public Page<Imrp_workcenter> search(SearchContext context){
            return this.mrp_workcenterOdooClient.search(context) ;
        }
        
        public void create(Imrp_workcenter mrp_workcenter){
this.mrp_workcenterOdooClient.create(mrp_workcenter) ;
        }
        
        public void removeBatch(List<Imrp_workcenter> mrp_workcenters){
            
        }
        
        public void get(Imrp_workcenter mrp_workcenter){
            this.mrp_workcenterOdooClient.get(mrp_workcenter) ;
        }
        
        public void remove(Imrp_workcenter mrp_workcenter){
this.mrp_workcenterOdooClient.remove(mrp_workcenter) ;
        }
        
        public void update(Imrp_workcenter mrp_workcenter){
this.mrp_workcenterOdooClient.update(mrp_workcenter) ;
        }
        
        public Page<Imrp_workcenter> select(SearchContext context){
            return null ;
        }
        

}

