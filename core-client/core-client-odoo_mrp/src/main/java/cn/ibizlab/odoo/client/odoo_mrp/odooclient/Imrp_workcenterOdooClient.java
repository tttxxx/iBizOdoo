package cn.ibizlab.odoo.client.odoo_mrp.odooclient;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.core.client.model.Imrp_workcenter;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[mrp_workcenter] 服务对象客户端接口
 */
public interface Imrp_workcenterOdooClient {
    
        public void updateBatch(Imrp_workcenter mrp_workcenter);

        public void createBatch(Imrp_workcenter mrp_workcenter);

        public Page<Imrp_workcenter> search(SearchContext context);

        public void create(Imrp_workcenter mrp_workcenter);

        public void removeBatch(Imrp_workcenter mrp_workcenter);

        public void get(Imrp_workcenter mrp_workcenter);

        public void remove(Imrp_workcenter mrp_workcenter);

        public void update(Imrp_workcenter mrp_workcenter);

        public List<Imrp_workcenter> select();


}