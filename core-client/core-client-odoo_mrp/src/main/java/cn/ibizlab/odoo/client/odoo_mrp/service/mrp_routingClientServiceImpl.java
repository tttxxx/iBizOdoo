package cn.ibizlab.odoo.client.odoo_mrp.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Imrp_routing;
import cn.ibizlab.odoo.core.client.service.Imrp_routingClientService;
import cn.ibizlab.odoo.client.odoo_mrp.model.mrp_routingImpl;
import cn.ibizlab.odoo.client.odoo_mrp.odooclient.Imrp_routingOdooClient;
import cn.ibizlab.odoo.client.odoo_mrp.odooclient.impl.mrp_routingOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[mrp_routing] 服务对象接口
 */
@Service
public class mrp_routingClientServiceImpl implements Imrp_routingClientService {
    @Autowired
    private  Imrp_routingOdooClient  mrp_routingOdooClient;

    public Imrp_routing createModel() {		
		return new mrp_routingImpl();
	}


        public void update(Imrp_routing mrp_routing){
this.mrp_routingOdooClient.update(mrp_routing) ;
        }
        
        public void get(Imrp_routing mrp_routing){
            this.mrp_routingOdooClient.get(mrp_routing) ;
        }
        
        public void create(Imrp_routing mrp_routing){
this.mrp_routingOdooClient.create(mrp_routing) ;
        }
        
        public void createBatch(List<Imrp_routing> mrp_routings){
            
        }
        
        public Page<Imrp_routing> search(SearchContext context){
            return this.mrp_routingOdooClient.search(context) ;
        }
        
        public void removeBatch(List<Imrp_routing> mrp_routings){
            
        }
        
        public void remove(Imrp_routing mrp_routing){
this.mrp_routingOdooClient.remove(mrp_routing) ;
        }
        
        public void updateBatch(List<Imrp_routing> mrp_routings){
            
        }
        
        public Page<Imrp_routing> select(SearchContext context){
            return null ;
        }
        

}

