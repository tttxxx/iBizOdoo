package cn.ibizlab.odoo.client.odoo_mrp.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Imrp_product_produce_line;
import cn.ibizlab.odoo.core.client.service.Imrp_product_produce_lineClientService;
import cn.ibizlab.odoo.client.odoo_mrp.model.mrp_product_produce_lineImpl;
import cn.ibizlab.odoo.client.odoo_mrp.odooclient.Imrp_product_produce_lineOdooClient;
import cn.ibizlab.odoo.client.odoo_mrp.odooclient.impl.mrp_product_produce_lineOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[mrp_product_produce_line] 服务对象接口
 */
@Service
public class mrp_product_produce_lineClientServiceImpl implements Imrp_product_produce_lineClientService {
    @Autowired
    private  Imrp_product_produce_lineOdooClient  mrp_product_produce_lineOdooClient;

    public Imrp_product_produce_line createModel() {		
		return new mrp_product_produce_lineImpl();
	}


        public void createBatch(List<Imrp_product_produce_line> mrp_product_produce_lines){
            
        }
        
        public void removeBatch(List<Imrp_product_produce_line> mrp_product_produce_lines){
            
        }
        
        public void update(Imrp_product_produce_line mrp_product_produce_line){
this.mrp_product_produce_lineOdooClient.update(mrp_product_produce_line) ;
        }
        
        public Page<Imrp_product_produce_line> search(SearchContext context){
            return this.mrp_product_produce_lineOdooClient.search(context) ;
        }
        
        public void remove(Imrp_product_produce_line mrp_product_produce_line){
this.mrp_product_produce_lineOdooClient.remove(mrp_product_produce_line) ;
        }
        
        public void get(Imrp_product_produce_line mrp_product_produce_line){
            this.mrp_product_produce_lineOdooClient.get(mrp_product_produce_line) ;
        }
        
        public void updateBatch(List<Imrp_product_produce_line> mrp_product_produce_lines){
            
        }
        
        public void create(Imrp_product_produce_line mrp_product_produce_line){
this.mrp_product_produce_lineOdooClient.create(mrp_product_produce_line) ;
        }
        
        public Page<Imrp_product_produce_line> select(SearchContext context){
            return null ;
        }
        

}

