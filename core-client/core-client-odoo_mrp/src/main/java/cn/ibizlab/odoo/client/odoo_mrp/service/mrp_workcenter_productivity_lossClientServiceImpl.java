package cn.ibizlab.odoo.client.odoo_mrp.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Imrp_workcenter_productivity_loss;
import cn.ibizlab.odoo.core.client.service.Imrp_workcenter_productivity_lossClientService;
import cn.ibizlab.odoo.client.odoo_mrp.model.mrp_workcenter_productivity_lossImpl;
import cn.ibizlab.odoo.client.odoo_mrp.odooclient.Imrp_workcenter_productivity_lossOdooClient;
import cn.ibizlab.odoo.client.odoo_mrp.odooclient.impl.mrp_workcenter_productivity_lossOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[mrp_workcenter_productivity_loss] 服务对象接口
 */
@Service
public class mrp_workcenter_productivity_lossClientServiceImpl implements Imrp_workcenter_productivity_lossClientService {
    @Autowired
    private  Imrp_workcenter_productivity_lossOdooClient  mrp_workcenter_productivity_lossOdooClient;

    public Imrp_workcenter_productivity_loss createModel() {		
		return new mrp_workcenter_productivity_lossImpl();
	}


        public Page<Imrp_workcenter_productivity_loss> search(SearchContext context){
            return this.mrp_workcenter_productivity_lossOdooClient.search(context) ;
        }
        
        public void update(Imrp_workcenter_productivity_loss mrp_workcenter_productivity_loss){
this.mrp_workcenter_productivity_lossOdooClient.update(mrp_workcenter_productivity_loss) ;
        }
        
        public void create(Imrp_workcenter_productivity_loss mrp_workcenter_productivity_loss){
this.mrp_workcenter_productivity_lossOdooClient.create(mrp_workcenter_productivity_loss) ;
        }
        
        public void remove(Imrp_workcenter_productivity_loss mrp_workcenter_productivity_loss){
this.mrp_workcenter_productivity_lossOdooClient.remove(mrp_workcenter_productivity_loss) ;
        }
        
        public void removeBatch(List<Imrp_workcenter_productivity_loss> mrp_workcenter_productivity_losses){
            
        }
        
        public void createBatch(List<Imrp_workcenter_productivity_loss> mrp_workcenter_productivity_losses){
            
        }
        
        public void updateBatch(List<Imrp_workcenter_productivity_loss> mrp_workcenter_productivity_losses){
            
        }
        
        public void get(Imrp_workcenter_productivity_loss mrp_workcenter_productivity_loss){
            this.mrp_workcenter_productivity_lossOdooClient.get(mrp_workcenter_productivity_loss) ;
        }
        
        public Page<Imrp_workcenter_productivity_loss> select(SearchContext context){
            return null ;
        }
        

}

