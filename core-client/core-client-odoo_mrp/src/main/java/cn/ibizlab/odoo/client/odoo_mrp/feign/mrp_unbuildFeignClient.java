package cn.ibizlab.odoo.client.odoo_mrp.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Imrp_unbuild;
import cn.ibizlab.odoo.client.odoo_mrp.model.mrp_unbuildImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[mrp_unbuild] 服务对象接口
 */
public interface mrp_unbuildFeignClient {

    @RequestMapping(method = RequestMethod.POST, value = "/odoo_mrp/mrp_unbuilds/createbatch")
    public mrp_unbuildImpl createBatch(@RequestBody List<mrp_unbuildImpl> mrp_unbuilds);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_mrp/mrp_unbuilds/updatebatch")
    public mrp_unbuildImpl updateBatch(@RequestBody List<mrp_unbuildImpl> mrp_unbuilds);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_mrp/mrp_unbuilds")
    public mrp_unbuildImpl create(@RequestBody mrp_unbuildImpl mrp_unbuild);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_mrp/mrp_unbuilds/search")
    public Page<mrp_unbuildImpl> search(SearchContext context);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_mrp/mrp_unbuilds/removebatch")
    public mrp_unbuildImpl removeBatch(@RequestBody List<mrp_unbuildImpl> mrp_unbuilds);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_mrp/mrp_unbuilds/{id}")
    public mrp_unbuildImpl update(@PathVariable("id") Integer id,@RequestBody mrp_unbuildImpl mrp_unbuild);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_mrp/mrp_unbuilds/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_mrp/mrp_unbuilds/{id}")
    public mrp_unbuildImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_mrp/mrp_unbuilds/select")
    public Page<mrp_unbuildImpl> select();



}
