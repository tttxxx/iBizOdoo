package cn.ibizlab.odoo.client.odoo_mrp.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Imrp_bom_line;
import cn.ibizlab.odoo.core.client.service.Imrp_bom_lineClientService;
import cn.ibizlab.odoo.client.odoo_mrp.model.mrp_bom_lineImpl;
import cn.ibizlab.odoo.client.odoo_mrp.odooclient.Imrp_bom_lineOdooClient;
import cn.ibizlab.odoo.client.odoo_mrp.odooclient.impl.mrp_bom_lineOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[mrp_bom_line] 服务对象接口
 */
@Service
public class mrp_bom_lineClientServiceImpl implements Imrp_bom_lineClientService {
    @Autowired
    private  Imrp_bom_lineOdooClient  mrp_bom_lineOdooClient;

    public Imrp_bom_line createModel() {		
		return new mrp_bom_lineImpl();
	}


        public void updateBatch(List<Imrp_bom_line> mrp_bom_lines){
            
        }
        
        public Page<Imrp_bom_line> search(SearchContext context){
            return this.mrp_bom_lineOdooClient.search(context) ;
        }
        
        public void createBatch(List<Imrp_bom_line> mrp_bom_lines){
            
        }
        
        public void removeBatch(List<Imrp_bom_line> mrp_bom_lines){
            
        }
        
        public void update(Imrp_bom_line mrp_bom_line){
this.mrp_bom_lineOdooClient.update(mrp_bom_line) ;
        }
        
        public void get(Imrp_bom_line mrp_bom_line){
            this.mrp_bom_lineOdooClient.get(mrp_bom_line) ;
        }
        
        public void remove(Imrp_bom_line mrp_bom_line){
this.mrp_bom_lineOdooClient.remove(mrp_bom_line) ;
        }
        
        public void create(Imrp_bom_line mrp_bom_line){
this.mrp_bom_lineOdooClient.create(mrp_bom_line) ;
        }
        
        public Page<Imrp_bom_line> select(SearchContext context){
            return null ;
        }
        

}

