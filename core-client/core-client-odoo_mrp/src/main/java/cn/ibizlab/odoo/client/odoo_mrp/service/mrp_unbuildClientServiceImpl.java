package cn.ibizlab.odoo.client.odoo_mrp.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Imrp_unbuild;
import cn.ibizlab.odoo.core.client.service.Imrp_unbuildClientService;
import cn.ibizlab.odoo.client.odoo_mrp.model.mrp_unbuildImpl;
import cn.ibizlab.odoo.client.odoo_mrp.odooclient.Imrp_unbuildOdooClient;
import cn.ibizlab.odoo.client.odoo_mrp.odooclient.impl.mrp_unbuildOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[mrp_unbuild] 服务对象接口
 */
@Service
public class mrp_unbuildClientServiceImpl implements Imrp_unbuildClientService {
    @Autowired
    private  Imrp_unbuildOdooClient  mrp_unbuildOdooClient;

    public Imrp_unbuild createModel() {		
		return new mrp_unbuildImpl();
	}


        public void createBatch(List<Imrp_unbuild> mrp_unbuilds){
            
        }
        
        public void updateBatch(List<Imrp_unbuild> mrp_unbuilds){
            
        }
        
        public void create(Imrp_unbuild mrp_unbuild){
this.mrp_unbuildOdooClient.create(mrp_unbuild) ;
        }
        
        public Page<Imrp_unbuild> search(SearchContext context){
            return this.mrp_unbuildOdooClient.search(context) ;
        }
        
        public void removeBatch(List<Imrp_unbuild> mrp_unbuilds){
            
        }
        
        public void update(Imrp_unbuild mrp_unbuild){
this.mrp_unbuildOdooClient.update(mrp_unbuild) ;
        }
        
        public void remove(Imrp_unbuild mrp_unbuild){
this.mrp_unbuildOdooClient.remove(mrp_unbuild) ;
        }
        
        public void get(Imrp_unbuild mrp_unbuild){
            this.mrp_unbuildOdooClient.get(mrp_unbuild) ;
        }
        
        public Page<Imrp_unbuild> select(SearchContext context){
            return null ;
        }
        

}

