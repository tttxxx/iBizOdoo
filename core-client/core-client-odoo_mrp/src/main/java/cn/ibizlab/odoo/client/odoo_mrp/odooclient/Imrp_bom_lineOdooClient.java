package cn.ibizlab.odoo.client.odoo_mrp.odooclient;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.core.client.model.Imrp_bom_line;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[mrp_bom_line] 服务对象客户端接口
 */
public interface Imrp_bom_lineOdooClient {
    
        public void updateBatch(Imrp_bom_line mrp_bom_line);

        public Page<Imrp_bom_line> search(SearchContext context);

        public void createBatch(Imrp_bom_line mrp_bom_line);

        public void removeBatch(Imrp_bom_line mrp_bom_line);

        public void update(Imrp_bom_line mrp_bom_line);

        public void get(Imrp_bom_line mrp_bom_line);

        public void remove(Imrp_bom_line mrp_bom_line);

        public void create(Imrp_bom_line mrp_bom_line);

        public List<Imrp_bom_line> select();


}