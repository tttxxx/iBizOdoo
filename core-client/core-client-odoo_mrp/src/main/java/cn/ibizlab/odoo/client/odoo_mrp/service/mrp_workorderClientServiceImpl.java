package cn.ibizlab.odoo.client.odoo_mrp.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Imrp_workorder;
import cn.ibizlab.odoo.core.client.service.Imrp_workorderClientService;
import cn.ibizlab.odoo.client.odoo_mrp.model.mrp_workorderImpl;
import cn.ibizlab.odoo.client.odoo_mrp.odooclient.Imrp_workorderOdooClient;
import cn.ibizlab.odoo.client.odoo_mrp.odooclient.impl.mrp_workorderOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[mrp_workorder] 服务对象接口
 */
@Service
public class mrp_workorderClientServiceImpl implements Imrp_workorderClientService {
    @Autowired
    private  Imrp_workorderOdooClient  mrp_workorderOdooClient;

    public Imrp_workorder createModel() {		
		return new mrp_workorderImpl();
	}


        public void updateBatch(List<Imrp_workorder> mrp_workorders){
            
        }
        
        public Page<Imrp_workorder> search(SearchContext context){
            return this.mrp_workorderOdooClient.search(context) ;
        }
        
        public void removeBatch(List<Imrp_workorder> mrp_workorders){
            
        }
        
        public void create(Imrp_workorder mrp_workorder){
this.mrp_workorderOdooClient.create(mrp_workorder) ;
        }
        
        public void createBatch(List<Imrp_workorder> mrp_workorders){
            
        }
        
        public void remove(Imrp_workorder mrp_workorder){
this.mrp_workorderOdooClient.remove(mrp_workorder) ;
        }
        
        public void update(Imrp_workorder mrp_workorder){
this.mrp_workorderOdooClient.update(mrp_workorder) ;
        }
        
        public void get(Imrp_workorder mrp_workorder){
            this.mrp_workorderOdooClient.get(mrp_workorder) ;
        }
        
        public Page<Imrp_workorder> select(SearchContext context){
            return null ;
        }
        

}

