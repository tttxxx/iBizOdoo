package cn.ibizlab.odoo.client.odoo_web_editor.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Iweb_editor_converter_test_sub;
import cn.ibizlab.odoo.client.odoo_web_editor.model.web_editor_converter_test_subImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[web_editor_converter_test_sub] 服务对象接口
 */
public interface web_editor_converter_test_subFeignClient {

    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_web_editor/web_editor_converter_test_subs/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_web_editor/web_editor_converter_test_subs")
    public web_editor_converter_test_subImpl create(@RequestBody web_editor_converter_test_subImpl web_editor_converter_test_sub);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_web_editor/web_editor_converter_test_subs/search")
    public Page<web_editor_converter_test_subImpl> search(SearchContext context);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_web_editor/web_editor_converter_test_subs/updatebatch")
    public web_editor_converter_test_subImpl updateBatch(@RequestBody List<web_editor_converter_test_subImpl> web_editor_converter_test_subs);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_web_editor/web_editor_converter_test_subs/createbatch")
    public web_editor_converter_test_subImpl createBatch(@RequestBody List<web_editor_converter_test_subImpl> web_editor_converter_test_subs);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_web_editor/web_editor_converter_test_subs/{id}")
    public web_editor_converter_test_subImpl update(@PathVariable("id") Integer id,@RequestBody web_editor_converter_test_subImpl web_editor_converter_test_sub);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_web_editor/web_editor_converter_test_subs/removebatch")
    public web_editor_converter_test_subImpl removeBatch(@RequestBody List<web_editor_converter_test_subImpl> web_editor_converter_test_subs);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_web_editor/web_editor_converter_test_subs/{id}")
    public web_editor_converter_test_subImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_web_editor/web_editor_converter_test_subs/select")
    public Page<web_editor_converter_test_subImpl> select();



}
