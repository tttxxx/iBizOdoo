package cn.ibizlab.odoo.client.odoo_web_editor.odooclient;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.core.client.model.Iweb_editor_converter_test;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[web_editor_converter_test] 服务对象客户端接口
 */
public interface Iweb_editor_converter_testOdooClient {
    
        public Page<Iweb_editor_converter_test> search(SearchContext context);

        public void removeBatch(Iweb_editor_converter_test web_editor_converter_test);

        public void remove(Iweb_editor_converter_test web_editor_converter_test);

        public void update(Iweb_editor_converter_test web_editor_converter_test);

        public void createBatch(Iweb_editor_converter_test web_editor_converter_test);

        public void create(Iweb_editor_converter_test web_editor_converter_test);

        public void get(Iweb_editor_converter_test web_editor_converter_test);

        public void updateBatch(Iweb_editor_converter_test web_editor_converter_test);

        public List<Iweb_editor_converter_test> select();


}