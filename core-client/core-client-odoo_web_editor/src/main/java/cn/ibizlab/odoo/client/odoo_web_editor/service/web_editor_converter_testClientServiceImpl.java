package cn.ibizlab.odoo.client.odoo_web_editor.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Iweb_editor_converter_test;
import cn.ibizlab.odoo.core.client.service.Iweb_editor_converter_testClientService;
import cn.ibizlab.odoo.client.odoo_web_editor.model.web_editor_converter_testImpl;
import cn.ibizlab.odoo.client.odoo_web_editor.odooclient.Iweb_editor_converter_testOdooClient;
import cn.ibizlab.odoo.client.odoo_web_editor.odooclient.impl.web_editor_converter_testOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[web_editor_converter_test] 服务对象接口
 */
@Service
public class web_editor_converter_testClientServiceImpl implements Iweb_editor_converter_testClientService {
    @Autowired
    private  Iweb_editor_converter_testOdooClient  web_editor_converter_testOdooClient;

    public Iweb_editor_converter_test createModel() {		
		return new web_editor_converter_testImpl();
	}


        public Page<Iweb_editor_converter_test> search(SearchContext context){
            return this.web_editor_converter_testOdooClient.search(context) ;
        }
        
        public void removeBatch(List<Iweb_editor_converter_test> web_editor_converter_tests){
            
        }
        
        public void remove(Iweb_editor_converter_test web_editor_converter_test){
this.web_editor_converter_testOdooClient.remove(web_editor_converter_test) ;
        }
        
        public void update(Iweb_editor_converter_test web_editor_converter_test){
this.web_editor_converter_testOdooClient.update(web_editor_converter_test) ;
        }
        
        public void createBatch(List<Iweb_editor_converter_test> web_editor_converter_tests){
            
        }
        
        public void create(Iweb_editor_converter_test web_editor_converter_test){
this.web_editor_converter_testOdooClient.create(web_editor_converter_test) ;
        }
        
        public void get(Iweb_editor_converter_test web_editor_converter_test){
            this.web_editor_converter_testOdooClient.get(web_editor_converter_test) ;
        }
        
        public void updateBatch(List<Iweb_editor_converter_test> web_editor_converter_tests){
            
        }
        
        public Page<Iweb_editor_converter_test> select(SearchContext context){
            return null ;
        }
        

}

