package cn.ibizlab.odoo.client.odoo_web_editor.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Iweb_editor_converter_test_sub;
import cn.ibizlab.odoo.core.client.service.Iweb_editor_converter_test_subClientService;
import cn.ibizlab.odoo.client.odoo_web_editor.model.web_editor_converter_test_subImpl;
import cn.ibizlab.odoo.client.odoo_web_editor.odooclient.Iweb_editor_converter_test_subOdooClient;
import cn.ibizlab.odoo.client.odoo_web_editor.odooclient.impl.web_editor_converter_test_subOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[web_editor_converter_test_sub] 服务对象接口
 */
@Service
public class web_editor_converter_test_subClientServiceImpl implements Iweb_editor_converter_test_subClientService {
    @Autowired
    private  Iweb_editor_converter_test_subOdooClient  web_editor_converter_test_subOdooClient;

    public Iweb_editor_converter_test_sub createModel() {		
		return new web_editor_converter_test_subImpl();
	}


        public void remove(Iweb_editor_converter_test_sub web_editor_converter_test_sub){
this.web_editor_converter_test_subOdooClient.remove(web_editor_converter_test_sub) ;
        }
        
        public void create(Iweb_editor_converter_test_sub web_editor_converter_test_sub){
this.web_editor_converter_test_subOdooClient.create(web_editor_converter_test_sub) ;
        }
        
        public Page<Iweb_editor_converter_test_sub> search(SearchContext context){
            return this.web_editor_converter_test_subOdooClient.search(context) ;
        }
        
        public void updateBatch(List<Iweb_editor_converter_test_sub> web_editor_converter_test_subs){
            
        }
        
        public void createBatch(List<Iweb_editor_converter_test_sub> web_editor_converter_test_subs){
            
        }
        
        public void update(Iweb_editor_converter_test_sub web_editor_converter_test_sub){
this.web_editor_converter_test_subOdooClient.update(web_editor_converter_test_sub) ;
        }
        
        public void removeBatch(List<Iweb_editor_converter_test_sub> web_editor_converter_test_subs){
            
        }
        
        public void get(Iweb_editor_converter_test_sub web_editor_converter_test_sub){
            this.web_editor_converter_test_subOdooClient.get(web_editor_converter_test_sub) ;
        }
        
        public Page<Iweb_editor_converter_test_sub> select(SearchContext context){
            return null ;
        }
        

}

