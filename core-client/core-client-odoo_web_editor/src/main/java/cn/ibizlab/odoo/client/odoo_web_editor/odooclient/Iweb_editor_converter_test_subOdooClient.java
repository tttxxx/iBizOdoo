package cn.ibizlab.odoo.client.odoo_web_editor.odooclient;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.core.client.model.Iweb_editor_converter_test_sub;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[web_editor_converter_test_sub] 服务对象客户端接口
 */
public interface Iweb_editor_converter_test_subOdooClient {
    
        public void remove(Iweb_editor_converter_test_sub web_editor_converter_test_sub);

        public void create(Iweb_editor_converter_test_sub web_editor_converter_test_sub);

        public Page<Iweb_editor_converter_test_sub> search(SearchContext context);

        public void updateBatch(Iweb_editor_converter_test_sub web_editor_converter_test_sub);

        public void createBatch(Iweb_editor_converter_test_sub web_editor_converter_test_sub);

        public void update(Iweb_editor_converter_test_sub web_editor_converter_test_sub);

        public void removeBatch(Iweb_editor_converter_test_sub web_editor_converter_test_sub);

        public void get(Iweb_editor_converter_test_sub web_editor_converter_test_sub);

        public List<Iweb_editor_converter_test_sub> select();


}