package cn.ibizlab.odoo.client.odoo_repair.odooclient;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.core.client.model.Irepair_cancel;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[repair_cancel] 服务对象客户端接口
 */
public interface Irepair_cancelOdooClient {
    
        public void create(Irepair_cancel repair_cancel);

        public void createBatch(Irepair_cancel repair_cancel);

        public Page<Irepair_cancel> search(SearchContext context);

        public void get(Irepair_cancel repair_cancel);

        public void update(Irepair_cancel repair_cancel);

        public void updateBatch(Irepair_cancel repair_cancel);

        public void remove(Irepair_cancel repair_cancel);

        public void removeBatch(Irepair_cancel repair_cancel);

        public List<Irepair_cancel> select();


}