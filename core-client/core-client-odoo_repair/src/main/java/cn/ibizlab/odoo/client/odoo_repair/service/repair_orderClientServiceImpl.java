package cn.ibizlab.odoo.client.odoo_repair.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Irepair_order;
import cn.ibizlab.odoo.core.client.service.Irepair_orderClientService;
import cn.ibizlab.odoo.client.odoo_repair.model.repair_orderImpl;
import cn.ibizlab.odoo.client.odoo_repair.odooclient.Irepair_orderOdooClient;
import cn.ibizlab.odoo.client.odoo_repair.odooclient.impl.repair_orderOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[repair_order] 服务对象接口
 */
@Service
public class repair_orderClientServiceImpl implements Irepair_orderClientService {
    @Autowired
    private  Irepair_orderOdooClient  repair_orderOdooClient;

    public Irepair_order createModel() {		
		return new repair_orderImpl();
	}


        public void createBatch(List<Irepair_order> repair_orders){
            
        }
        
        public void update(Irepair_order repair_order){
this.repair_orderOdooClient.update(repair_order) ;
        }
        
        public Page<Irepair_order> search(SearchContext context){
            return this.repair_orderOdooClient.search(context) ;
        }
        
        public void removeBatch(List<Irepair_order> repair_orders){
            
        }
        
        public void updateBatch(List<Irepair_order> repair_orders){
            
        }
        
        public void create(Irepair_order repair_order){
this.repair_orderOdooClient.create(repair_order) ;
        }
        
        public void get(Irepair_order repair_order){
            this.repair_orderOdooClient.get(repair_order) ;
        }
        
        public void remove(Irepair_order repair_order){
this.repair_orderOdooClient.remove(repair_order) ;
        }
        
        public Page<Irepair_order> select(SearchContext context){
            return null ;
        }
        

}

