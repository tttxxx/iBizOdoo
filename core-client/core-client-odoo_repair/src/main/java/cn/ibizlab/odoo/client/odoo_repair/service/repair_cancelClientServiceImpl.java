package cn.ibizlab.odoo.client.odoo_repair.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Irepair_cancel;
import cn.ibizlab.odoo.core.client.service.Irepair_cancelClientService;
import cn.ibizlab.odoo.client.odoo_repair.model.repair_cancelImpl;
import cn.ibizlab.odoo.client.odoo_repair.odooclient.Irepair_cancelOdooClient;
import cn.ibizlab.odoo.client.odoo_repair.odooclient.impl.repair_cancelOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[repair_cancel] 服务对象接口
 */
@Service
public class repair_cancelClientServiceImpl implements Irepair_cancelClientService {
    @Autowired
    private  Irepair_cancelOdooClient  repair_cancelOdooClient;

    public Irepair_cancel createModel() {		
		return new repair_cancelImpl();
	}


        public void create(Irepair_cancel repair_cancel){
this.repair_cancelOdooClient.create(repair_cancel) ;
        }
        
        public void createBatch(List<Irepair_cancel> repair_cancels){
            
        }
        
        public Page<Irepair_cancel> search(SearchContext context){
            return this.repair_cancelOdooClient.search(context) ;
        }
        
        public void get(Irepair_cancel repair_cancel){
            this.repair_cancelOdooClient.get(repair_cancel) ;
        }
        
        public void update(Irepair_cancel repair_cancel){
this.repair_cancelOdooClient.update(repair_cancel) ;
        }
        
        public void updateBatch(List<Irepair_cancel> repair_cancels){
            
        }
        
        public void remove(Irepair_cancel repair_cancel){
this.repair_cancelOdooClient.remove(repair_cancel) ;
        }
        
        public void removeBatch(List<Irepair_cancel> repair_cancels){
            
        }
        
        public Page<Irepair_cancel> select(SearchContext context){
            return null ;
        }
        

}

