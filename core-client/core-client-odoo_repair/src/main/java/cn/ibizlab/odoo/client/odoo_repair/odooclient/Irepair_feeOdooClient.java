package cn.ibizlab.odoo.client.odoo_repair.odooclient;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.core.client.model.Irepair_fee;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[repair_fee] 服务对象客户端接口
 */
public interface Irepair_feeOdooClient {
    
        public void get(Irepair_fee repair_fee);

        public void create(Irepair_fee repair_fee);

        public void updateBatch(Irepair_fee repair_fee);

        public void createBatch(Irepair_fee repair_fee);

        public void update(Irepair_fee repair_fee);

        public void removeBatch(Irepair_fee repair_fee);

        public void remove(Irepair_fee repair_fee);

        public Page<Irepair_fee> search(SearchContext context);

        public List<Irepair_fee> select();


}