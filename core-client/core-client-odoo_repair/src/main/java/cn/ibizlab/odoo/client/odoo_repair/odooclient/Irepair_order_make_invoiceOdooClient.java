package cn.ibizlab.odoo.client.odoo_repair.odooclient;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.core.client.model.Irepair_order_make_invoice;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[repair_order_make_invoice] 服务对象客户端接口
 */
public interface Irepair_order_make_invoiceOdooClient {
    
        public Page<Irepair_order_make_invoice> search(SearchContext context);

        public void update(Irepair_order_make_invoice repair_order_make_invoice);

        public void create(Irepair_order_make_invoice repair_order_make_invoice);

        public void get(Irepair_order_make_invoice repair_order_make_invoice);

        public void updateBatch(Irepair_order_make_invoice repair_order_make_invoice);

        public void createBatch(Irepair_order_make_invoice repair_order_make_invoice);

        public void removeBatch(Irepair_order_make_invoice repair_order_make_invoice);

        public void remove(Irepair_order_make_invoice repair_order_make_invoice);

        public List<Irepair_order_make_invoice> select();


}