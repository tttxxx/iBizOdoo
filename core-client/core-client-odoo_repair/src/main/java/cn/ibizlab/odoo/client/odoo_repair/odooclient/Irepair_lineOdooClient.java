package cn.ibizlab.odoo.client.odoo_repair.odooclient;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.core.client.model.Irepair_line;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[repair_line] 服务对象客户端接口
 */
public interface Irepair_lineOdooClient {
    
        public void updateBatch(Irepair_line repair_line);

        public void createBatch(Irepair_line repair_line);

        public void get(Irepair_line repair_line);

        public void update(Irepair_line repair_line);

        public Page<Irepair_line> search(SearchContext context);

        public void removeBatch(Irepair_line repair_line);

        public void create(Irepair_line repair_line);

        public void remove(Irepair_line repair_line);

        public List<Irepair_line> select();


}