package cn.ibizlab.odoo.client.odoo_survey.odooclient;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.core.client.model.Isurvey_mail_compose_message;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[survey_mail_compose_message] 服务对象客户端接口
 */
public interface Isurvey_mail_compose_messageOdooClient {
    
        public void remove(Isurvey_mail_compose_message survey_mail_compose_message);

        public void createBatch(Isurvey_mail_compose_message survey_mail_compose_message);

        public void update(Isurvey_mail_compose_message survey_mail_compose_message);

        public void get(Isurvey_mail_compose_message survey_mail_compose_message);

        public void removeBatch(Isurvey_mail_compose_message survey_mail_compose_message);

        public void updateBatch(Isurvey_mail_compose_message survey_mail_compose_message);

        public void create(Isurvey_mail_compose_message survey_mail_compose_message);

        public Page<Isurvey_mail_compose_message> search(SearchContext context);

        public List<Isurvey_mail_compose_message> select();


}