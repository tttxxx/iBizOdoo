package cn.ibizlab.odoo.client.odoo_survey.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import lombok.Data;

@ConfigurationProperties(prefix = "client.odoo.survey")
@Data
public class odoo_surveyClientProperties {

	private String tokenUrl ;

	private String clientId ;

	private String clientSecret ;

}
