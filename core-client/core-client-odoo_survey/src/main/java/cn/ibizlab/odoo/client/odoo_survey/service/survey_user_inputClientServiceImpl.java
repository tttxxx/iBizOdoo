package cn.ibizlab.odoo.client.odoo_survey.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Isurvey_user_input;
import cn.ibizlab.odoo.core.client.service.Isurvey_user_inputClientService;
import cn.ibizlab.odoo.client.odoo_survey.model.survey_user_inputImpl;
import cn.ibizlab.odoo.client.odoo_survey.odooclient.Isurvey_user_inputOdooClient;
import cn.ibizlab.odoo.client.odoo_survey.odooclient.impl.survey_user_inputOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[survey_user_input] 服务对象接口
 */
@Service
public class survey_user_inputClientServiceImpl implements Isurvey_user_inputClientService {
    @Autowired
    private  Isurvey_user_inputOdooClient  survey_user_inputOdooClient;

    public Isurvey_user_input createModel() {		
		return new survey_user_inputImpl();
	}


        public void remove(Isurvey_user_input survey_user_input){
this.survey_user_inputOdooClient.remove(survey_user_input) ;
        }
        
        public void create(Isurvey_user_input survey_user_input){
this.survey_user_inputOdooClient.create(survey_user_input) ;
        }
        
        public void createBatch(List<Isurvey_user_input> survey_user_inputs){
            
        }
        
        public void update(Isurvey_user_input survey_user_input){
this.survey_user_inputOdooClient.update(survey_user_input) ;
        }
        
        public void updateBatch(List<Isurvey_user_input> survey_user_inputs){
            
        }
        
        public Page<Isurvey_user_input> search(SearchContext context){
            return this.survey_user_inputOdooClient.search(context) ;
        }
        
        public void get(Isurvey_user_input survey_user_input){
            this.survey_user_inputOdooClient.get(survey_user_input) ;
        }
        
        public void removeBatch(List<Isurvey_user_input> survey_user_inputs){
            
        }
        
        public Page<Isurvey_user_input> select(SearchContext context){
            return null ;
        }
        

}

