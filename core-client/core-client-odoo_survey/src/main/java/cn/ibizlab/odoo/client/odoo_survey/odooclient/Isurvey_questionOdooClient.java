package cn.ibizlab.odoo.client.odoo_survey.odooclient;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.core.client.model.Isurvey_question;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[survey_question] 服务对象客户端接口
 */
public interface Isurvey_questionOdooClient {
    
        public Page<Isurvey_question> search(SearchContext context);

        public void remove(Isurvey_question survey_question);

        public void update(Isurvey_question survey_question);

        public void get(Isurvey_question survey_question);

        public void create(Isurvey_question survey_question);

        public void createBatch(Isurvey_question survey_question);

        public void updateBatch(Isurvey_question survey_question);

        public void removeBatch(Isurvey_question survey_question);

        public List<Isurvey_question> select();


}