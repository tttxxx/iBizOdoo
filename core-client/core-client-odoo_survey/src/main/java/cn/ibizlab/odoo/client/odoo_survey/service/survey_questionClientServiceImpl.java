package cn.ibizlab.odoo.client.odoo_survey.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Isurvey_question;
import cn.ibizlab.odoo.core.client.service.Isurvey_questionClientService;
import cn.ibizlab.odoo.client.odoo_survey.model.survey_questionImpl;
import cn.ibizlab.odoo.client.odoo_survey.odooclient.Isurvey_questionOdooClient;
import cn.ibizlab.odoo.client.odoo_survey.odooclient.impl.survey_questionOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[survey_question] 服务对象接口
 */
@Service
public class survey_questionClientServiceImpl implements Isurvey_questionClientService {
    @Autowired
    private  Isurvey_questionOdooClient  survey_questionOdooClient;

    public Isurvey_question createModel() {		
		return new survey_questionImpl();
	}


        public Page<Isurvey_question> search(SearchContext context){
            return this.survey_questionOdooClient.search(context) ;
        }
        
        public void remove(Isurvey_question survey_question){
this.survey_questionOdooClient.remove(survey_question) ;
        }
        
        public void update(Isurvey_question survey_question){
this.survey_questionOdooClient.update(survey_question) ;
        }
        
        public void get(Isurvey_question survey_question){
            this.survey_questionOdooClient.get(survey_question) ;
        }
        
        public void create(Isurvey_question survey_question){
this.survey_questionOdooClient.create(survey_question) ;
        }
        
        public void createBatch(List<Isurvey_question> survey_questions){
            
        }
        
        public void updateBatch(List<Isurvey_question> survey_questions){
            
        }
        
        public void removeBatch(List<Isurvey_question> survey_questions){
            
        }
        
        public Page<Isurvey_question> select(SearchContext context){
            return null ;
        }
        

}

