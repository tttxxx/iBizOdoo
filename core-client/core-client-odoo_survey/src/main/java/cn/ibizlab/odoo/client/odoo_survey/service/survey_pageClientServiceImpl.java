package cn.ibizlab.odoo.client.odoo_survey.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Isurvey_page;
import cn.ibizlab.odoo.core.client.service.Isurvey_pageClientService;
import cn.ibizlab.odoo.client.odoo_survey.model.survey_pageImpl;
import cn.ibizlab.odoo.client.odoo_survey.odooclient.Isurvey_pageOdooClient;
import cn.ibizlab.odoo.client.odoo_survey.odooclient.impl.survey_pageOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[survey_page] 服务对象接口
 */
@Service
public class survey_pageClientServiceImpl implements Isurvey_pageClientService {
    @Autowired
    private  Isurvey_pageOdooClient  survey_pageOdooClient;

    public Isurvey_page createModel() {		
		return new survey_pageImpl();
	}


        public void get(Isurvey_page survey_page){
            this.survey_pageOdooClient.get(survey_page) ;
        }
        
        public void updateBatch(List<Isurvey_page> survey_pages){
            
        }
        
        public void update(Isurvey_page survey_page){
this.survey_pageOdooClient.update(survey_page) ;
        }
        
        public void createBatch(List<Isurvey_page> survey_pages){
            
        }
        
        public void removeBatch(List<Isurvey_page> survey_pages){
            
        }
        
        public Page<Isurvey_page> search(SearchContext context){
            return this.survey_pageOdooClient.search(context) ;
        }
        
        public void remove(Isurvey_page survey_page){
this.survey_pageOdooClient.remove(survey_page) ;
        }
        
        public void create(Isurvey_page survey_page){
this.survey_pageOdooClient.create(survey_page) ;
        }
        
        public Page<Isurvey_page> select(SearchContext context){
            return null ;
        }
        

}

