package cn.ibizlab.odoo.client.odoo_survey.odooclient;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.core.client.model.Isurvey_page;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[survey_page] 服务对象客户端接口
 */
public interface Isurvey_pageOdooClient {
    
        public void get(Isurvey_page survey_page);

        public void updateBatch(Isurvey_page survey_page);

        public void update(Isurvey_page survey_page);

        public void createBatch(Isurvey_page survey_page);

        public void removeBatch(Isurvey_page survey_page);

        public Page<Isurvey_page> search(SearchContext context);

        public void remove(Isurvey_page survey_page);

        public void create(Isurvey_page survey_page);

        public List<Isurvey_page> select();


}