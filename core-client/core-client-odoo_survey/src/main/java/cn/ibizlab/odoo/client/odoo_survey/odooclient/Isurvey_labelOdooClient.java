package cn.ibizlab.odoo.client.odoo_survey.odooclient;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.core.client.model.Isurvey_label;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[survey_label] 服务对象客户端接口
 */
public interface Isurvey_labelOdooClient {
    
        public void remove(Isurvey_label survey_label);

        public void updateBatch(Isurvey_label survey_label);

        public void update(Isurvey_label survey_label);

        public void get(Isurvey_label survey_label);

        public void removeBatch(Isurvey_label survey_label);

        public void createBatch(Isurvey_label survey_label);

        public Page<Isurvey_label> search(SearchContext context);

        public void create(Isurvey_label survey_label);

        public List<Isurvey_label> select();


}