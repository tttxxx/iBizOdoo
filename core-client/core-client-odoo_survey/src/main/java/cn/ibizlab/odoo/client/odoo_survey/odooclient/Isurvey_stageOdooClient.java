package cn.ibizlab.odoo.client.odoo_survey.odooclient;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.core.client.model.Isurvey_stage;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[survey_stage] 服务对象客户端接口
 */
public interface Isurvey_stageOdooClient {
    
        public Page<Isurvey_stage> search(SearchContext context);

        public void updateBatch(Isurvey_stage survey_stage);

        public void removeBatch(Isurvey_stage survey_stage);

        public void create(Isurvey_stage survey_stage);

        public void remove(Isurvey_stage survey_stage);

        public void createBatch(Isurvey_stage survey_stage);

        public void update(Isurvey_stage survey_stage);

        public void get(Isurvey_stage survey_stage);

        public List<Isurvey_stage> select();


}