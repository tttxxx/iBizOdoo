package cn.ibizlab.odoo.client.odoo_survey.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Isurvey_label;
import cn.ibizlab.odoo.core.client.service.Isurvey_labelClientService;
import cn.ibizlab.odoo.client.odoo_survey.model.survey_labelImpl;
import cn.ibizlab.odoo.client.odoo_survey.odooclient.Isurvey_labelOdooClient;
import cn.ibizlab.odoo.client.odoo_survey.odooclient.impl.survey_labelOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[survey_label] 服务对象接口
 */
@Service
public class survey_labelClientServiceImpl implements Isurvey_labelClientService {
    @Autowired
    private  Isurvey_labelOdooClient  survey_labelOdooClient;

    public Isurvey_label createModel() {		
		return new survey_labelImpl();
	}


        public void remove(Isurvey_label survey_label){
this.survey_labelOdooClient.remove(survey_label) ;
        }
        
        public void updateBatch(List<Isurvey_label> survey_labels){
            
        }
        
        public void update(Isurvey_label survey_label){
this.survey_labelOdooClient.update(survey_label) ;
        }
        
        public void get(Isurvey_label survey_label){
            this.survey_labelOdooClient.get(survey_label) ;
        }
        
        public void removeBatch(List<Isurvey_label> survey_labels){
            
        }
        
        public void createBatch(List<Isurvey_label> survey_labels){
            
        }
        
        public Page<Isurvey_label> search(SearchContext context){
            return this.survey_labelOdooClient.search(context) ;
        }
        
        public void create(Isurvey_label survey_label){
this.survey_labelOdooClient.create(survey_label) ;
        }
        
        public Page<Isurvey_label> select(SearchContext context){
            return null ;
        }
        

}

