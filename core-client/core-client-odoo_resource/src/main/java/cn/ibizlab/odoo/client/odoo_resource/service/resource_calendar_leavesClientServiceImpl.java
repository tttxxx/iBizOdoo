package cn.ibizlab.odoo.client.odoo_resource.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Iresource_calendar_leaves;
import cn.ibizlab.odoo.core.client.service.Iresource_calendar_leavesClientService;
import cn.ibizlab.odoo.client.odoo_resource.model.resource_calendar_leavesImpl;
import cn.ibizlab.odoo.client.odoo_resource.odooclient.Iresource_calendar_leavesOdooClient;
import cn.ibizlab.odoo.client.odoo_resource.odooclient.impl.resource_calendar_leavesOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[resource_calendar_leaves] 服务对象接口
 */
@Service
public class resource_calendar_leavesClientServiceImpl implements Iresource_calendar_leavesClientService {
    @Autowired
    private  Iresource_calendar_leavesOdooClient  resource_calendar_leavesOdooClient;

    public Iresource_calendar_leaves createModel() {		
		return new resource_calendar_leavesImpl();
	}


        public void updateBatch(List<Iresource_calendar_leaves> resource_calendar_leaves){
            
        }
        
        public void update(Iresource_calendar_leaves resource_calendar_leaves){
this.resource_calendar_leavesOdooClient.update(resource_calendar_leaves) ;
        }
        
        public void remove(Iresource_calendar_leaves resource_calendar_leaves){
this.resource_calendar_leavesOdooClient.remove(resource_calendar_leaves) ;
        }
        
        public void get(Iresource_calendar_leaves resource_calendar_leaves){
            this.resource_calendar_leavesOdooClient.get(resource_calendar_leaves) ;
        }
        
        public Page<Iresource_calendar_leaves> search(SearchContext context){
            return this.resource_calendar_leavesOdooClient.search(context) ;
        }
        
        public void create(Iresource_calendar_leaves resource_calendar_leaves){
this.resource_calendar_leavesOdooClient.create(resource_calendar_leaves) ;
        }
        
        public void createBatch(List<Iresource_calendar_leaves> resource_calendar_leaves){
            
        }
        
        public void removeBatch(List<Iresource_calendar_leaves> resource_calendar_leaves){
            
        }
        
        public Page<Iresource_calendar_leaves> select(SearchContext context){
            return null ;
        }
        

}

