package cn.ibizlab.odoo.client.odoo_resource.odooclient;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.core.client.model.Iresource_mixin;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[resource_mixin] 服务对象客户端接口
 */
public interface Iresource_mixinOdooClient {
    
        public void create(Iresource_mixin resource_mixin);

        public void createBatch(Iresource_mixin resource_mixin);

        public void remove(Iresource_mixin resource_mixin);

        public void removeBatch(Iresource_mixin resource_mixin);

        public Page<Iresource_mixin> search(SearchContext context);

        public void get(Iresource_mixin resource_mixin);

        public void update(Iresource_mixin resource_mixin);

        public void updateBatch(Iresource_mixin resource_mixin);

        public List<Iresource_mixin> select();


}