package cn.ibizlab.odoo.client.odoo_resource.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Iresource_resource;
import cn.ibizlab.odoo.core.client.service.Iresource_resourceClientService;
import cn.ibizlab.odoo.client.odoo_resource.model.resource_resourceImpl;
import cn.ibizlab.odoo.client.odoo_resource.odooclient.Iresource_resourceOdooClient;
import cn.ibizlab.odoo.client.odoo_resource.odooclient.impl.resource_resourceOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[resource_resource] 服务对象接口
 */
@Service
public class resource_resourceClientServiceImpl implements Iresource_resourceClientService {
    @Autowired
    private  Iresource_resourceOdooClient  resource_resourceOdooClient;

    public Iresource_resource createModel() {		
		return new resource_resourceImpl();
	}


        public void updateBatch(List<Iresource_resource> resource_resources){
            
        }
        
        public void remove(Iresource_resource resource_resource){
this.resource_resourceOdooClient.remove(resource_resource) ;
        }
        
        public void removeBatch(List<Iresource_resource> resource_resources){
            
        }
        
        public void get(Iresource_resource resource_resource){
            this.resource_resourceOdooClient.get(resource_resource) ;
        }
        
        public void createBatch(List<Iresource_resource> resource_resources){
            
        }
        
        public Page<Iresource_resource> search(SearchContext context){
            return this.resource_resourceOdooClient.search(context) ;
        }
        
        public void update(Iresource_resource resource_resource){
this.resource_resourceOdooClient.update(resource_resource) ;
        }
        
        public void create(Iresource_resource resource_resource){
this.resource_resourceOdooClient.create(resource_resource) ;
        }
        
        public Page<Iresource_resource> select(SearchContext context){
            return null ;
        }
        

}

