package cn.ibizlab.odoo.client.odoo_resource.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import lombok.Data;

@ConfigurationProperties(prefix = "client.odoo.resource")
@Data
public class odoo_resourceClientProperties {

	private String tokenUrl ;

	private String clientId ;

	private String clientSecret ;

}
