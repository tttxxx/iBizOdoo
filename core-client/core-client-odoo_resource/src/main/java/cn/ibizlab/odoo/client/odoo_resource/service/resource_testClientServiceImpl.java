package cn.ibizlab.odoo.client.odoo_resource.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Iresource_test;
import cn.ibizlab.odoo.core.client.service.Iresource_testClientService;
import cn.ibizlab.odoo.client.odoo_resource.model.resource_testImpl;
import cn.ibizlab.odoo.client.odoo_resource.odooclient.Iresource_testOdooClient;
import cn.ibizlab.odoo.client.odoo_resource.odooclient.impl.resource_testOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[resource_test] 服务对象接口
 */
@Service
public class resource_testClientServiceImpl implements Iresource_testClientService {
    @Autowired
    private  Iresource_testOdooClient  resource_testOdooClient;

    public Iresource_test createModel() {		
		return new resource_testImpl();
	}


        public void remove(Iresource_test resource_test){
this.resource_testOdooClient.remove(resource_test) ;
        }
        
        public void createBatch(List<Iresource_test> resource_tests){
            
        }
        
        public void create(Iresource_test resource_test){
this.resource_testOdooClient.create(resource_test) ;
        }
        
        public void update(Iresource_test resource_test){
this.resource_testOdooClient.update(resource_test) ;
        }
        
        public void get(Iresource_test resource_test){
            this.resource_testOdooClient.get(resource_test) ;
        }
        
        public void removeBatch(List<Iresource_test> resource_tests){
            
        }
        
        public Page<Iresource_test> search(SearchContext context){
            return this.resource_testOdooClient.search(context) ;
        }
        
        public void updateBatch(List<Iresource_test> resource_tests){
            
        }
        
        public Page<Iresource_test> select(SearchContext context){
            return null ;
        }
        

}

