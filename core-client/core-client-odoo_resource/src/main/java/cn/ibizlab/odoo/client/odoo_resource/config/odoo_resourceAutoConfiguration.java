package cn.ibizlab.odoo.client.odoo_resource.config;

import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnWebApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.cloud.openfeign.FeignClientsConfiguration;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@Configuration
@ConditionalOnClass(odoo_resourceClientConfiguration.class)
@ConditionalOnWebApplication
@EnableConfigurationProperties(odoo_resourceClientProperties.class)
@Import({
    FeignClientsConfiguration.class
})
public class odoo_resourceAutoConfiguration {

}
