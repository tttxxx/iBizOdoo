package cn.ibizlab.odoo.client.odoo_resource.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Iresource_calendar;
import cn.ibizlab.odoo.core.client.service.Iresource_calendarClientService;
import cn.ibizlab.odoo.client.odoo_resource.model.resource_calendarImpl;
import cn.ibizlab.odoo.client.odoo_resource.odooclient.Iresource_calendarOdooClient;
import cn.ibizlab.odoo.client.odoo_resource.odooclient.impl.resource_calendarOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[resource_calendar] 服务对象接口
 */
@Service
public class resource_calendarClientServiceImpl implements Iresource_calendarClientService {
    @Autowired
    private  Iresource_calendarOdooClient  resource_calendarOdooClient;

    public Iresource_calendar createModel() {		
		return new resource_calendarImpl();
	}


        public void update(Iresource_calendar resource_calendar){
this.resource_calendarOdooClient.update(resource_calendar) ;
        }
        
        public void removeBatch(List<Iresource_calendar> resource_calendars){
            
        }
        
        public void remove(Iresource_calendar resource_calendar){
this.resource_calendarOdooClient.remove(resource_calendar) ;
        }
        
        public void get(Iresource_calendar resource_calendar){
            this.resource_calendarOdooClient.get(resource_calendar) ;
        }
        
        public void createBatch(List<Iresource_calendar> resource_calendars){
            
        }
        
        public void updateBatch(List<Iresource_calendar> resource_calendars){
            
        }
        
        public void create(Iresource_calendar resource_calendar){
this.resource_calendarOdooClient.create(resource_calendar) ;
        }
        
        public Page<Iresource_calendar> search(SearchContext context){
            return this.resource_calendarOdooClient.search(context) ;
        }
        
        public Page<Iresource_calendar> select(SearchContext context){
            return null ;
        }
        

}

