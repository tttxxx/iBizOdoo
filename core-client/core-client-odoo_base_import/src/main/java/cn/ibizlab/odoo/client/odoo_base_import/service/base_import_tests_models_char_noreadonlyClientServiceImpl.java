package cn.ibizlab.odoo.client.odoo_base_import.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Ibase_import_tests_models_char_noreadonly;
import cn.ibizlab.odoo.core.client.service.Ibase_import_tests_models_char_noreadonlyClientService;
import cn.ibizlab.odoo.client.odoo_base_import.model.base_import_tests_models_char_noreadonlyImpl;
import cn.ibizlab.odoo.client.odoo_base_import.odooclient.Ibase_import_tests_models_char_noreadonlyOdooClient;
import cn.ibizlab.odoo.client.odoo_base_import.odooclient.impl.base_import_tests_models_char_noreadonlyOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[base_import_tests_models_char_noreadonly] 服务对象接口
 */
@Service
public class base_import_tests_models_char_noreadonlyClientServiceImpl implements Ibase_import_tests_models_char_noreadonlyClientService {
    @Autowired
    private  Ibase_import_tests_models_char_noreadonlyOdooClient  base_import_tests_models_char_noreadonlyOdooClient;

    public Ibase_import_tests_models_char_noreadonly createModel() {		
		return new base_import_tests_models_char_noreadonlyImpl();
	}


        public void update(Ibase_import_tests_models_char_noreadonly base_import_tests_models_char_noreadonly){
this.base_import_tests_models_char_noreadonlyOdooClient.update(base_import_tests_models_char_noreadonly) ;
        }
        
        public void removeBatch(List<Ibase_import_tests_models_char_noreadonly> base_import_tests_models_char_noreadonlies){
            
        }
        
        public void createBatch(List<Ibase_import_tests_models_char_noreadonly> base_import_tests_models_char_noreadonlies){
            
        }
        
        public void updateBatch(List<Ibase_import_tests_models_char_noreadonly> base_import_tests_models_char_noreadonlies){
            
        }
        
        public void get(Ibase_import_tests_models_char_noreadonly base_import_tests_models_char_noreadonly){
            this.base_import_tests_models_char_noreadonlyOdooClient.get(base_import_tests_models_char_noreadonly) ;
        }
        
        public void create(Ibase_import_tests_models_char_noreadonly base_import_tests_models_char_noreadonly){
this.base_import_tests_models_char_noreadonlyOdooClient.create(base_import_tests_models_char_noreadonly) ;
        }
        
        public void remove(Ibase_import_tests_models_char_noreadonly base_import_tests_models_char_noreadonly){
this.base_import_tests_models_char_noreadonlyOdooClient.remove(base_import_tests_models_char_noreadonly) ;
        }
        
        public Page<Ibase_import_tests_models_char_noreadonly> search(SearchContext context){
            return this.base_import_tests_models_char_noreadonlyOdooClient.search(context) ;
        }
        
        public Page<Ibase_import_tests_models_char_noreadonly> select(SearchContext context){
            return null ;
        }
        

}

