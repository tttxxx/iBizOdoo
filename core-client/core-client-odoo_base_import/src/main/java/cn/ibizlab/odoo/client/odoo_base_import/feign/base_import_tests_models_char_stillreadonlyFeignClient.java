package cn.ibizlab.odoo.client.odoo_base_import.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Ibase_import_tests_models_char_stillreadonly;
import cn.ibizlab.odoo.client.odoo_base_import.model.base_import_tests_models_char_stillreadonlyImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[base_import_tests_models_char_stillreadonly] 服务对象接口
 */
public interface base_import_tests_models_char_stillreadonlyFeignClient {

    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_base_import/base_import_tests_models_char_stillreadonlies/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_base_import/base_import_tests_models_char_stillreadonlies/{id}")
    public base_import_tests_models_char_stillreadonlyImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_base_import/base_import_tests_models_char_stillreadonlies/search")
    public Page<base_import_tests_models_char_stillreadonlyImpl> search(SearchContext context);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_base_import/base_import_tests_models_char_stillreadonlies")
    public base_import_tests_models_char_stillreadonlyImpl create(@RequestBody base_import_tests_models_char_stillreadonlyImpl base_import_tests_models_char_stillreadonly);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_base_import/base_import_tests_models_char_stillreadonlies/removebatch")
    public base_import_tests_models_char_stillreadonlyImpl removeBatch(@RequestBody List<base_import_tests_models_char_stillreadonlyImpl> base_import_tests_models_char_stillreadonlies);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_base_import/base_import_tests_models_char_stillreadonlies/updatebatch")
    public base_import_tests_models_char_stillreadonlyImpl updateBatch(@RequestBody List<base_import_tests_models_char_stillreadonlyImpl> base_import_tests_models_char_stillreadonlies);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_base_import/base_import_tests_models_char_stillreadonlies/createbatch")
    public base_import_tests_models_char_stillreadonlyImpl createBatch(@RequestBody List<base_import_tests_models_char_stillreadonlyImpl> base_import_tests_models_char_stillreadonlies);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_base_import/base_import_tests_models_char_stillreadonlies/{id}")
    public base_import_tests_models_char_stillreadonlyImpl update(@PathVariable("id") Integer id,@RequestBody base_import_tests_models_char_stillreadonlyImpl base_import_tests_models_char_stillreadonly);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_base_import/base_import_tests_models_char_stillreadonlies/select")
    public Page<base_import_tests_models_char_stillreadonlyImpl> select();



}
