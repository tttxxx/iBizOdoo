package cn.ibizlab.odoo.client.odoo_base_import.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Ibase_import_tests_models_o2m;
import cn.ibizlab.odoo.core.client.service.Ibase_import_tests_models_o2mClientService;
import cn.ibizlab.odoo.client.odoo_base_import.model.base_import_tests_models_o2mImpl;
import cn.ibizlab.odoo.client.odoo_base_import.odooclient.Ibase_import_tests_models_o2mOdooClient;
import cn.ibizlab.odoo.client.odoo_base_import.odooclient.impl.base_import_tests_models_o2mOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[base_import_tests_models_o2m] 服务对象接口
 */
@Service
public class base_import_tests_models_o2mClientServiceImpl implements Ibase_import_tests_models_o2mClientService {
    @Autowired
    private  Ibase_import_tests_models_o2mOdooClient  base_import_tests_models_o2mOdooClient;

    public Ibase_import_tests_models_o2m createModel() {		
		return new base_import_tests_models_o2mImpl();
	}


        public void update(Ibase_import_tests_models_o2m base_import_tests_models_o2m){
this.base_import_tests_models_o2mOdooClient.update(base_import_tests_models_o2m) ;
        }
        
        public void get(Ibase_import_tests_models_o2m base_import_tests_models_o2m){
            this.base_import_tests_models_o2mOdooClient.get(base_import_tests_models_o2m) ;
        }
        
        public void create(Ibase_import_tests_models_o2m base_import_tests_models_o2m){
this.base_import_tests_models_o2mOdooClient.create(base_import_tests_models_o2m) ;
        }
        
        public void updateBatch(List<Ibase_import_tests_models_o2m> base_import_tests_models_o2ms){
            
        }
        
        public void createBatch(List<Ibase_import_tests_models_o2m> base_import_tests_models_o2ms){
            
        }
        
        public void removeBatch(List<Ibase_import_tests_models_o2m> base_import_tests_models_o2ms){
            
        }
        
        public Page<Ibase_import_tests_models_o2m> search(SearchContext context){
            return this.base_import_tests_models_o2mOdooClient.search(context) ;
        }
        
        public void remove(Ibase_import_tests_models_o2m base_import_tests_models_o2m){
this.base_import_tests_models_o2mOdooClient.remove(base_import_tests_models_o2m) ;
        }
        
        public Page<Ibase_import_tests_models_o2m> select(SearchContext context){
            return null ;
        }
        

}

