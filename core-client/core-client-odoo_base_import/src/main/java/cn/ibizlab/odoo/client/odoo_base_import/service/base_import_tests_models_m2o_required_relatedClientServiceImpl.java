package cn.ibizlab.odoo.client.odoo_base_import.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Ibase_import_tests_models_m2o_required_related;
import cn.ibizlab.odoo.core.client.service.Ibase_import_tests_models_m2o_required_relatedClientService;
import cn.ibizlab.odoo.client.odoo_base_import.model.base_import_tests_models_m2o_required_relatedImpl;
import cn.ibizlab.odoo.client.odoo_base_import.odooclient.Ibase_import_tests_models_m2o_required_relatedOdooClient;
import cn.ibizlab.odoo.client.odoo_base_import.odooclient.impl.base_import_tests_models_m2o_required_relatedOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[base_import_tests_models_m2o_required_related] 服务对象接口
 */
@Service
public class base_import_tests_models_m2o_required_relatedClientServiceImpl implements Ibase_import_tests_models_m2o_required_relatedClientService {
    @Autowired
    private  Ibase_import_tests_models_m2o_required_relatedOdooClient  base_import_tests_models_m2o_required_relatedOdooClient;

    public Ibase_import_tests_models_m2o_required_related createModel() {		
		return new base_import_tests_models_m2o_required_relatedImpl();
	}


        public void updateBatch(List<Ibase_import_tests_models_m2o_required_related> base_import_tests_models_m2o_required_relateds){
            
        }
        
        public void createBatch(List<Ibase_import_tests_models_m2o_required_related> base_import_tests_models_m2o_required_relateds){
            
        }
        
        public void create(Ibase_import_tests_models_m2o_required_related base_import_tests_models_m2o_required_related){
this.base_import_tests_models_m2o_required_relatedOdooClient.create(base_import_tests_models_m2o_required_related) ;
        }
        
        public void update(Ibase_import_tests_models_m2o_required_related base_import_tests_models_m2o_required_related){
this.base_import_tests_models_m2o_required_relatedOdooClient.update(base_import_tests_models_m2o_required_related) ;
        }
        
        public void removeBatch(List<Ibase_import_tests_models_m2o_required_related> base_import_tests_models_m2o_required_relateds){
            
        }
        
        public void get(Ibase_import_tests_models_m2o_required_related base_import_tests_models_m2o_required_related){
            this.base_import_tests_models_m2o_required_relatedOdooClient.get(base_import_tests_models_m2o_required_related) ;
        }
        
        public Page<Ibase_import_tests_models_m2o_required_related> search(SearchContext context){
            return this.base_import_tests_models_m2o_required_relatedOdooClient.search(context) ;
        }
        
        public void remove(Ibase_import_tests_models_m2o_required_related base_import_tests_models_m2o_required_related){
this.base_import_tests_models_m2o_required_relatedOdooClient.remove(base_import_tests_models_m2o_required_related) ;
        }
        
        public Page<Ibase_import_tests_models_m2o_required_related> select(SearchContext context){
            return null ;
        }
        

}

