package cn.ibizlab.odoo.client.odoo_base_import.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Ibase_import_tests_models_m2o;
import cn.ibizlab.odoo.core.client.service.Ibase_import_tests_models_m2oClientService;
import cn.ibizlab.odoo.client.odoo_base_import.model.base_import_tests_models_m2oImpl;
import cn.ibizlab.odoo.client.odoo_base_import.odooclient.Ibase_import_tests_models_m2oOdooClient;
import cn.ibizlab.odoo.client.odoo_base_import.odooclient.impl.base_import_tests_models_m2oOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[base_import_tests_models_m2o] 服务对象接口
 */
@Service
public class base_import_tests_models_m2oClientServiceImpl implements Ibase_import_tests_models_m2oClientService {
    @Autowired
    private  Ibase_import_tests_models_m2oOdooClient  base_import_tests_models_m2oOdooClient;

    public Ibase_import_tests_models_m2o createModel() {		
		return new base_import_tests_models_m2oImpl();
	}


        public void create(Ibase_import_tests_models_m2o base_import_tests_models_m2o){
this.base_import_tests_models_m2oOdooClient.create(base_import_tests_models_m2o) ;
        }
        
        public void removeBatch(List<Ibase_import_tests_models_m2o> base_import_tests_models_m2os){
            
        }
        
        public void remove(Ibase_import_tests_models_m2o base_import_tests_models_m2o){
this.base_import_tests_models_m2oOdooClient.remove(base_import_tests_models_m2o) ;
        }
        
        public void get(Ibase_import_tests_models_m2o base_import_tests_models_m2o){
            this.base_import_tests_models_m2oOdooClient.get(base_import_tests_models_m2o) ;
        }
        
        public void update(Ibase_import_tests_models_m2o base_import_tests_models_m2o){
this.base_import_tests_models_m2oOdooClient.update(base_import_tests_models_m2o) ;
        }
        
        public void updateBatch(List<Ibase_import_tests_models_m2o> base_import_tests_models_m2os){
            
        }
        
        public void createBatch(List<Ibase_import_tests_models_m2o> base_import_tests_models_m2os){
            
        }
        
        public Page<Ibase_import_tests_models_m2o> search(SearchContext context){
            return this.base_import_tests_models_m2oOdooClient.search(context) ;
        }
        
        public Page<Ibase_import_tests_models_m2o> select(SearchContext context){
            return null ;
        }
        

}

