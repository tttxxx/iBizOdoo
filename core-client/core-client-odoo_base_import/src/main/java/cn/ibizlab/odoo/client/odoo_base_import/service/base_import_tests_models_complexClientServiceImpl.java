package cn.ibizlab.odoo.client.odoo_base_import.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Ibase_import_tests_models_complex;
import cn.ibizlab.odoo.core.client.service.Ibase_import_tests_models_complexClientService;
import cn.ibizlab.odoo.client.odoo_base_import.model.base_import_tests_models_complexImpl;
import cn.ibizlab.odoo.client.odoo_base_import.odooclient.Ibase_import_tests_models_complexOdooClient;
import cn.ibizlab.odoo.client.odoo_base_import.odooclient.impl.base_import_tests_models_complexOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[base_import_tests_models_complex] 服务对象接口
 */
@Service
public class base_import_tests_models_complexClientServiceImpl implements Ibase_import_tests_models_complexClientService {
    @Autowired
    private  Ibase_import_tests_models_complexOdooClient  base_import_tests_models_complexOdooClient;

    public Ibase_import_tests_models_complex createModel() {		
		return new base_import_tests_models_complexImpl();
	}


        public void update(Ibase_import_tests_models_complex base_import_tests_models_complex){
this.base_import_tests_models_complexOdooClient.update(base_import_tests_models_complex) ;
        }
        
        public void createBatch(List<Ibase_import_tests_models_complex> base_import_tests_models_complices){
            
        }
        
        public void remove(Ibase_import_tests_models_complex base_import_tests_models_complex){
this.base_import_tests_models_complexOdooClient.remove(base_import_tests_models_complex) ;
        }
        
        public void get(Ibase_import_tests_models_complex base_import_tests_models_complex){
            this.base_import_tests_models_complexOdooClient.get(base_import_tests_models_complex) ;
        }
        
        public void create(Ibase_import_tests_models_complex base_import_tests_models_complex){
this.base_import_tests_models_complexOdooClient.create(base_import_tests_models_complex) ;
        }
        
        public void updateBatch(List<Ibase_import_tests_models_complex> base_import_tests_models_complices){
            
        }
        
        public void removeBatch(List<Ibase_import_tests_models_complex> base_import_tests_models_complices){
            
        }
        
        public Page<Ibase_import_tests_models_complex> search(SearchContext context){
            return this.base_import_tests_models_complexOdooClient.search(context) ;
        }
        
        public Page<Ibase_import_tests_models_complex> select(SearchContext context){
            return null ;
        }
        

}

