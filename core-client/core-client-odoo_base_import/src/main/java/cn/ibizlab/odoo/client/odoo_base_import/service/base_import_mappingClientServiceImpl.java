package cn.ibizlab.odoo.client.odoo_base_import.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Ibase_import_mapping;
import cn.ibizlab.odoo.core.client.service.Ibase_import_mappingClientService;
import cn.ibizlab.odoo.client.odoo_base_import.model.base_import_mappingImpl;
import cn.ibizlab.odoo.client.odoo_base_import.odooclient.Ibase_import_mappingOdooClient;
import cn.ibizlab.odoo.client.odoo_base_import.odooclient.impl.base_import_mappingOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[base_import_mapping] 服务对象接口
 */
@Service
public class base_import_mappingClientServiceImpl implements Ibase_import_mappingClientService {
    @Autowired
    private  Ibase_import_mappingOdooClient  base_import_mappingOdooClient;

    public Ibase_import_mapping createModel() {		
		return new base_import_mappingImpl();
	}


        public void update(Ibase_import_mapping base_import_mapping){
this.base_import_mappingOdooClient.update(base_import_mapping) ;
        }
        
        public void createBatch(List<Ibase_import_mapping> base_import_mappings){
            
        }
        
        public void removeBatch(List<Ibase_import_mapping> base_import_mappings){
            
        }
        
        public void remove(Ibase_import_mapping base_import_mapping){
this.base_import_mappingOdooClient.remove(base_import_mapping) ;
        }
        
        public void updateBatch(List<Ibase_import_mapping> base_import_mappings){
            
        }
        
        public void create(Ibase_import_mapping base_import_mapping){
this.base_import_mappingOdooClient.create(base_import_mapping) ;
        }
        
        public Page<Ibase_import_mapping> search(SearchContext context){
            return this.base_import_mappingOdooClient.search(context) ;
        }
        
        public void get(Ibase_import_mapping base_import_mapping){
            this.base_import_mappingOdooClient.get(base_import_mapping) ;
        }
        
        public Page<Ibase_import_mapping> select(SearchContext context){
            return null ;
        }
        

}

