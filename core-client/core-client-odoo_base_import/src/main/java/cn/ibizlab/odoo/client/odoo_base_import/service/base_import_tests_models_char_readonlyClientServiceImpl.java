package cn.ibizlab.odoo.client.odoo_base_import.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Ibase_import_tests_models_char_readonly;
import cn.ibizlab.odoo.core.client.service.Ibase_import_tests_models_char_readonlyClientService;
import cn.ibizlab.odoo.client.odoo_base_import.model.base_import_tests_models_char_readonlyImpl;
import cn.ibizlab.odoo.client.odoo_base_import.odooclient.Ibase_import_tests_models_char_readonlyOdooClient;
import cn.ibizlab.odoo.client.odoo_base_import.odooclient.impl.base_import_tests_models_char_readonlyOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[base_import_tests_models_char_readonly] 服务对象接口
 */
@Service
public class base_import_tests_models_char_readonlyClientServiceImpl implements Ibase_import_tests_models_char_readonlyClientService {
    @Autowired
    private  Ibase_import_tests_models_char_readonlyOdooClient  base_import_tests_models_char_readonlyOdooClient;

    public Ibase_import_tests_models_char_readonly createModel() {		
		return new base_import_tests_models_char_readonlyImpl();
	}


        public void update(Ibase_import_tests_models_char_readonly base_import_tests_models_char_readonly){
this.base_import_tests_models_char_readonlyOdooClient.update(base_import_tests_models_char_readonly) ;
        }
        
        public void createBatch(List<Ibase_import_tests_models_char_readonly> base_import_tests_models_char_readonlies){
            
        }
        
        public void removeBatch(List<Ibase_import_tests_models_char_readonly> base_import_tests_models_char_readonlies){
            
        }
        
        public void create(Ibase_import_tests_models_char_readonly base_import_tests_models_char_readonly){
this.base_import_tests_models_char_readonlyOdooClient.create(base_import_tests_models_char_readonly) ;
        }
        
        public void updateBatch(List<Ibase_import_tests_models_char_readonly> base_import_tests_models_char_readonlies){
            
        }
        
        public Page<Ibase_import_tests_models_char_readonly> search(SearchContext context){
            return this.base_import_tests_models_char_readonlyOdooClient.search(context) ;
        }
        
        public void remove(Ibase_import_tests_models_char_readonly base_import_tests_models_char_readonly){
this.base_import_tests_models_char_readonlyOdooClient.remove(base_import_tests_models_char_readonly) ;
        }
        
        public void get(Ibase_import_tests_models_char_readonly base_import_tests_models_char_readonly){
            this.base_import_tests_models_char_readonlyOdooClient.get(base_import_tests_models_char_readonly) ;
        }
        
        public Page<Ibase_import_tests_models_char_readonly> select(SearchContext context){
            return null ;
        }
        

}

