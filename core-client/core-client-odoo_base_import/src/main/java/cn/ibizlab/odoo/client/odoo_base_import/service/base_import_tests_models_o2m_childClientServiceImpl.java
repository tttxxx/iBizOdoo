package cn.ibizlab.odoo.client.odoo_base_import.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Ibase_import_tests_models_o2m_child;
import cn.ibizlab.odoo.core.client.service.Ibase_import_tests_models_o2m_childClientService;
import cn.ibizlab.odoo.client.odoo_base_import.model.base_import_tests_models_o2m_childImpl;
import cn.ibizlab.odoo.client.odoo_base_import.odooclient.Ibase_import_tests_models_o2m_childOdooClient;
import cn.ibizlab.odoo.client.odoo_base_import.odooclient.impl.base_import_tests_models_o2m_childOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[base_import_tests_models_o2m_child] 服务对象接口
 */
@Service
public class base_import_tests_models_o2m_childClientServiceImpl implements Ibase_import_tests_models_o2m_childClientService {
    @Autowired
    private  Ibase_import_tests_models_o2m_childOdooClient  base_import_tests_models_o2m_childOdooClient;

    public Ibase_import_tests_models_o2m_child createModel() {		
		return new base_import_tests_models_o2m_childImpl();
	}


        public void create(Ibase_import_tests_models_o2m_child base_import_tests_models_o2m_child){
this.base_import_tests_models_o2m_childOdooClient.create(base_import_tests_models_o2m_child) ;
        }
        
        public void remove(Ibase_import_tests_models_o2m_child base_import_tests_models_o2m_child){
this.base_import_tests_models_o2m_childOdooClient.remove(base_import_tests_models_o2m_child) ;
        }
        
        public void createBatch(List<Ibase_import_tests_models_o2m_child> base_import_tests_models_o2m_children){
            
        }
        
        public void removeBatch(List<Ibase_import_tests_models_o2m_child> base_import_tests_models_o2m_children){
            
        }
        
        public void updateBatch(List<Ibase_import_tests_models_o2m_child> base_import_tests_models_o2m_children){
            
        }
        
        public void get(Ibase_import_tests_models_o2m_child base_import_tests_models_o2m_child){
            this.base_import_tests_models_o2m_childOdooClient.get(base_import_tests_models_o2m_child) ;
        }
        
        public void update(Ibase_import_tests_models_o2m_child base_import_tests_models_o2m_child){
this.base_import_tests_models_o2m_childOdooClient.update(base_import_tests_models_o2m_child) ;
        }
        
        public Page<Ibase_import_tests_models_o2m_child> search(SearchContext context){
            return this.base_import_tests_models_o2m_childOdooClient.search(context) ;
        }
        
        public Page<Ibase_import_tests_models_o2m_child> select(SearchContext context){
            return null ;
        }
        

}

