package cn.ibizlab.odoo.client.odoo_base_import.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Ibase_import_tests_models_float;
import cn.ibizlab.odoo.core.client.service.Ibase_import_tests_models_floatClientService;
import cn.ibizlab.odoo.client.odoo_base_import.model.base_import_tests_models_floatImpl;
import cn.ibizlab.odoo.client.odoo_base_import.odooclient.Ibase_import_tests_models_floatOdooClient;
import cn.ibizlab.odoo.client.odoo_base_import.odooclient.impl.base_import_tests_models_floatOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[base_import_tests_models_float] 服务对象接口
 */
@Service
public class base_import_tests_models_floatClientServiceImpl implements Ibase_import_tests_models_floatClientService {
    @Autowired
    private  Ibase_import_tests_models_floatOdooClient  base_import_tests_models_floatOdooClient;

    public Ibase_import_tests_models_float createModel() {		
		return new base_import_tests_models_floatImpl();
	}


        public void create(Ibase_import_tests_models_float base_import_tests_models_float){
this.base_import_tests_models_floatOdooClient.create(base_import_tests_models_float) ;
        }
        
        public void remove(Ibase_import_tests_models_float base_import_tests_models_float){
this.base_import_tests_models_floatOdooClient.remove(base_import_tests_models_float) ;
        }
        
        public void get(Ibase_import_tests_models_float base_import_tests_models_float){
            this.base_import_tests_models_floatOdooClient.get(base_import_tests_models_float) ;
        }
        
        public void createBatch(List<Ibase_import_tests_models_float> base_import_tests_models_floats){
            
        }
        
        public void removeBatch(List<Ibase_import_tests_models_float> base_import_tests_models_floats){
            
        }
        
        public void update(Ibase_import_tests_models_float base_import_tests_models_float){
this.base_import_tests_models_floatOdooClient.update(base_import_tests_models_float) ;
        }
        
        public void updateBatch(List<Ibase_import_tests_models_float> base_import_tests_models_floats){
            
        }
        
        public Page<Ibase_import_tests_models_float> search(SearchContext context){
            return this.base_import_tests_models_floatOdooClient.search(context) ;
        }
        
        public Page<Ibase_import_tests_models_float> select(SearchContext context){
            return null ;
        }
        

}

