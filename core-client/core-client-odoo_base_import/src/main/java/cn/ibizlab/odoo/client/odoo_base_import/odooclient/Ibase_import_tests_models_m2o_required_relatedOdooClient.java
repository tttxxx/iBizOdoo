package cn.ibizlab.odoo.client.odoo_base_import.odooclient;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.core.client.model.Ibase_import_tests_models_m2o_required_related;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[base_import_tests_models_m2o_required_related] 服务对象客户端接口
 */
public interface Ibase_import_tests_models_m2o_required_relatedOdooClient {
    
        public void updateBatch(Ibase_import_tests_models_m2o_required_related base_import_tests_models_m2o_required_related);

        public void createBatch(Ibase_import_tests_models_m2o_required_related base_import_tests_models_m2o_required_related);

        public void create(Ibase_import_tests_models_m2o_required_related base_import_tests_models_m2o_required_related);

        public void update(Ibase_import_tests_models_m2o_required_related base_import_tests_models_m2o_required_related);

        public void removeBatch(Ibase_import_tests_models_m2o_required_related base_import_tests_models_m2o_required_related);

        public void get(Ibase_import_tests_models_m2o_required_related base_import_tests_models_m2o_required_related);

        public Page<Ibase_import_tests_models_m2o_required_related> search(SearchContext context);

        public void remove(Ibase_import_tests_models_m2o_required_related base_import_tests_models_m2o_required_related);

        public List<Ibase_import_tests_models_m2o_required_related> select();


}