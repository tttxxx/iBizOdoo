package cn.ibizlab.odoo.client.odoo_base_import.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import lombok.Data;

@ConfigurationProperties(prefix = "client.odoo.base.import")
@Data
public class odoo_base_importClientProperties {

	private String tokenUrl ;

	private String clientId ;

	private String clientSecret ;

}
