package cn.ibizlab.odoo.client.odoo_hr.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Ihr_leave_report;
import cn.ibizlab.odoo.core.client.service.Ihr_leave_reportClientService;
import cn.ibizlab.odoo.client.odoo_hr.model.hr_leave_reportImpl;
import cn.ibizlab.odoo.client.odoo_hr.odooclient.Ihr_leave_reportOdooClient;
import cn.ibizlab.odoo.client.odoo_hr.odooclient.impl.hr_leave_reportOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[hr_leave_report] 服务对象接口
 */
@Service
public class hr_leave_reportClientServiceImpl implements Ihr_leave_reportClientService {
    @Autowired
    private  Ihr_leave_reportOdooClient  hr_leave_reportOdooClient;

    public Ihr_leave_report createModel() {		
		return new hr_leave_reportImpl();
	}


        public void remove(Ihr_leave_report hr_leave_report){
this.hr_leave_reportOdooClient.remove(hr_leave_report) ;
        }
        
        public void update(Ihr_leave_report hr_leave_report){
this.hr_leave_reportOdooClient.update(hr_leave_report) ;
        }
        
        public void removeBatch(List<Ihr_leave_report> hr_leave_reports){
            
        }
        
        public Page<Ihr_leave_report> search(SearchContext context){
            return this.hr_leave_reportOdooClient.search(context) ;
        }
        
        public void createBatch(List<Ihr_leave_report> hr_leave_reports){
            
        }
        
        public void create(Ihr_leave_report hr_leave_report){
this.hr_leave_reportOdooClient.create(hr_leave_report) ;
        }
        
        public void get(Ihr_leave_report hr_leave_report){
            this.hr_leave_reportOdooClient.get(hr_leave_report) ;
        }
        
        public void updateBatch(List<Ihr_leave_report> hr_leave_reports){
            
        }
        
        public Page<Ihr_leave_report> select(SearchContext context){
            return null ;
        }
        

}

