package cn.ibizlab.odoo.client.odoo_hr.odooclient;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.core.client.model.Ihr_employee;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[hr_employee] 服务对象客户端接口
 */
public interface Ihr_employeeOdooClient {
    
        public void createBatch(Ihr_employee hr_employee);

        public void update(Ihr_employee hr_employee);

        public void updateBatch(Ihr_employee hr_employee);

        public Page<Ihr_employee> search(SearchContext context);

        public void removeBatch(Ihr_employee hr_employee);

        public void remove(Ihr_employee hr_employee);

        public void create(Ihr_employee hr_employee);

        public void get(Ihr_employee hr_employee);

        public List<Ihr_employee> select();


}