package cn.ibizlab.odoo.client.odoo_hr.odooclient;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.core.client.model.Ihr_contract;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[hr_contract] 服务对象客户端接口
 */
public interface Ihr_contractOdooClient {
    
        public void remove(Ihr_contract hr_contract);

        public void get(Ihr_contract hr_contract);

        public void createBatch(Ihr_contract hr_contract);

        public Page<Ihr_contract> search(SearchContext context);

        public void updateBatch(Ihr_contract hr_contract);

        public void create(Ihr_contract hr_contract);

        public void update(Ihr_contract hr_contract);

        public void removeBatch(Ihr_contract hr_contract);

        public List<Ihr_contract> select();


}