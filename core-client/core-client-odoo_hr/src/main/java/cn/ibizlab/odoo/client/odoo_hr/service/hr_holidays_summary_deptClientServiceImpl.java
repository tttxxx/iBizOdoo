package cn.ibizlab.odoo.client.odoo_hr.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Ihr_holidays_summary_dept;
import cn.ibizlab.odoo.core.client.service.Ihr_holidays_summary_deptClientService;
import cn.ibizlab.odoo.client.odoo_hr.model.hr_holidays_summary_deptImpl;
import cn.ibizlab.odoo.client.odoo_hr.odooclient.Ihr_holidays_summary_deptOdooClient;
import cn.ibizlab.odoo.client.odoo_hr.odooclient.impl.hr_holidays_summary_deptOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[hr_holidays_summary_dept] 服务对象接口
 */
@Service
public class hr_holidays_summary_deptClientServiceImpl implements Ihr_holidays_summary_deptClientService {
    @Autowired
    private  Ihr_holidays_summary_deptOdooClient  hr_holidays_summary_deptOdooClient;

    public Ihr_holidays_summary_dept createModel() {		
		return new hr_holidays_summary_deptImpl();
	}


        public void remove(Ihr_holidays_summary_dept hr_holidays_summary_dept){
this.hr_holidays_summary_deptOdooClient.remove(hr_holidays_summary_dept) ;
        }
        
        public void get(Ihr_holidays_summary_dept hr_holidays_summary_dept){
            this.hr_holidays_summary_deptOdooClient.get(hr_holidays_summary_dept) ;
        }
        
        public Page<Ihr_holidays_summary_dept> search(SearchContext context){
            return this.hr_holidays_summary_deptOdooClient.search(context) ;
        }
        
        public void createBatch(List<Ihr_holidays_summary_dept> hr_holidays_summary_depts){
            
        }
        
        public void create(Ihr_holidays_summary_dept hr_holidays_summary_dept){
this.hr_holidays_summary_deptOdooClient.create(hr_holidays_summary_dept) ;
        }
        
        public void removeBatch(List<Ihr_holidays_summary_dept> hr_holidays_summary_depts){
            
        }
        
        public void update(Ihr_holidays_summary_dept hr_holidays_summary_dept){
this.hr_holidays_summary_deptOdooClient.update(hr_holidays_summary_dept) ;
        }
        
        public void updateBatch(List<Ihr_holidays_summary_dept> hr_holidays_summary_depts){
            
        }
        
        public Page<Ihr_holidays_summary_dept> select(SearchContext context){
            return null ;
        }
        

}

