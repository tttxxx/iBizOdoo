package cn.ibizlab.odoo.client.odoo_hr.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Ihr_leave_type;
import cn.ibizlab.odoo.core.client.service.Ihr_leave_typeClientService;
import cn.ibizlab.odoo.client.odoo_hr.model.hr_leave_typeImpl;
import cn.ibizlab.odoo.client.odoo_hr.odooclient.Ihr_leave_typeOdooClient;
import cn.ibizlab.odoo.client.odoo_hr.odooclient.impl.hr_leave_typeOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[hr_leave_type] 服务对象接口
 */
@Service
public class hr_leave_typeClientServiceImpl implements Ihr_leave_typeClientService {
    @Autowired
    private  Ihr_leave_typeOdooClient  hr_leave_typeOdooClient;

    public Ihr_leave_type createModel() {		
		return new hr_leave_typeImpl();
	}


        public void updateBatch(List<Ihr_leave_type> hr_leave_types){
            
        }
        
        public Page<Ihr_leave_type> search(SearchContext context){
            return this.hr_leave_typeOdooClient.search(context) ;
        }
        
        public void remove(Ihr_leave_type hr_leave_type){
this.hr_leave_typeOdooClient.remove(hr_leave_type) ;
        }
        
        public void create(Ihr_leave_type hr_leave_type){
this.hr_leave_typeOdooClient.create(hr_leave_type) ;
        }
        
        public void get(Ihr_leave_type hr_leave_type){
            this.hr_leave_typeOdooClient.get(hr_leave_type) ;
        }
        
        public void update(Ihr_leave_type hr_leave_type){
this.hr_leave_typeOdooClient.update(hr_leave_type) ;
        }
        
        public void createBatch(List<Ihr_leave_type> hr_leave_types){
            
        }
        
        public void removeBatch(List<Ihr_leave_type> hr_leave_types){
            
        }
        
        public Page<Ihr_leave_type> select(SearchContext context){
            return null ;
        }
        

}

