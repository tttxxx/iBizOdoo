package cn.ibizlab.odoo.client.odoo_hr.model;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.math.BigInteger;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.core.client.model.Ihr_employee;
import cn.ibizlab.odoo.util.helper.OdooClientHelper;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang.ArrayUtils;
import org.springframework.util.StringUtils;

/**
 * 接口实体[hr_employee] 对象
 */
public class hr_employeeImpl implements Ihr_employee,Serializable{

    /**
     * 有效
     */
    public String active;

    @JsonIgnore
    public boolean activeDirtyFlag;
    
    /**
     * 下一活动截止日期
     */
    public Timestamp activity_date_deadline;

    @JsonIgnore
    public boolean activity_date_deadlineDirtyFlag;
    
    /**
     * 活动
     */
    public String activity_ids;

    @JsonIgnore
    public boolean activity_idsDirtyFlag;
    
    /**
     * 活动状态
     */
    public String activity_state;

    @JsonIgnore
    public boolean activity_stateDirtyFlag;
    
    /**
     * 下一活动摘要
     */
    public String activity_summary;

    @JsonIgnore
    public boolean activity_summaryDirtyFlag;
    
    /**
     * 下一活动类型
     */
    public Integer activity_type_id;

    @JsonIgnore
    public boolean activity_type_idDirtyFlag;
    
    /**
     * 责任用户
     */
    public Integer activity_user_id;

    @JsonIgnore
    public boolean activity_user_idDirtyFlag;
    
    /**
     * 附加说明
     */
    public String additional_note;

    @JsonIgnore
    public boolean additional_noteDirtyFlag;
    
    /**
     * 家庭住址
     */
    public Integer address_home_id;

    @JsonIgnore
    public boolean address_home_idDirtyFlag;
    
    /**
     * 家庭住址
     */
    public String address_home_id_text;

    @JsonIgnore
    public boolean address_home_id_textDirtyFlag;
    
    /**
     * 工作地址
     */
    public Integer address_id;

    @JsonIgnore
    public boolean address_idDirtyFlag;
    
    /**
     * 工作地址
     */
    public String address_id_text;

    @JsonIgnore
    public boolean address_id_textDirtyFlag;
    
    /**
     * 出勤
     */
    public String attendance_ids;

    @JsonIgnore
    public boolean attendance_idsDirtyFlag;
    
    /**
     * 出勤状态
     */
    public String attendance_state;

    @JsonIgnore
    public boolean attendance_stateDirtyFlag;
    
    /**
     * 员工徽章
     */
    public String badge_ids;

    @JsonIgnore
    public boolean badge_idsDirtyFlag;
    
    /**
     * 银行账户号码
     */
    public Integer bank_account_id;

    @JsonIgnore
    public boolean bank_account_idDirtyFlag;
    
    /**
     * 工牌 ID
     */
    public String barcode;

    @JsonIgnore
    public boolean barcodeDirtyFlag;
    
    /**
     * 出生日期
     */
    public Timestamp birthday;

    @JsonIgnore
    public boolean birthdayDirtyFlag;
    
    /**
     * 标签
     */
    public String category_ids;

    @JsonIgnore
    public boolean category_idsDirtyFlag;
    
    /**
     * 证书等级
     */
    public String certificate;

    @JsonIgnore
    public boolean certificateDirtyFlag;
    
    /**
     * 子女数目
     */
    public Integer children;

    @JsonIgnore
    public boolean childrenDirtyFlag;
    
    /**
     * 下属
     */
    public String child_ids;

    @JsonIgnore
    public boolean child_idsDirtyFlag;
    
    /**
     * 教练
     */
    public Integer coach_id;

    @JsonIgnore
    public boolean coach_idDirtyFlag;
    
    /**
     * 教练
     */
    public String coach_id_text;

    @JsonIgnore
    public boolean coach_id_textDirtyFlag;
    
    /**
     * 颜色索引
     */
    public Integer color;

    @JsonIgnore
    public boolean colorDirtyFlag;
    
    /**
     * 公司
     */
    public Integer company_id;

    @JsonIgnore
    public boolean company_idDirtyFlag;
    
    /**
     * 公司
     */
    public String company_id_text;

    @JsonIgnore
    public boolean company_id_textDirtyFlag;
    
    /**
     * 合同统计
     */
    public Integer contracts_count;

    @JsonIgnore
    public boolean contracts_countDirtyFlag;
    
    /**
     * 当前合同
     */
    public Integer contract_id;

    @JsonIgnore
    public boolean contract_idDirtyFlag;
    
    /**
     * 员工合同
     */
    public String contract_ids;

    @JsonIgnore
    public boolean contract_idsDirtyFlag;
    
    /**
     * 国籍(国家)
     */
    public Integer country_id;

    @JsonIgnore
    public boolean country_idDirtyFlag;
    
    /**
     * 国籍(国家)
     */
    public String country_id_text;

    @JsonIgnore
    public boolean country_id_textDirtyFlag;
    
    /**
     * 国籍
     */
    public Integer country_of_birth;

    @JsonIgnore
    public boolean country_of_birthDirtyFlag;
    
    /**
     * 国籍
     */
    public String country_of_birth_text;

    @JsonIgnore
    public boolean country_of_birth_textDirtyFlag;
    
    /**
     * 创建时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp create_date;

    @JsonIgnore
    public boolean create_dateDirtyFlag;
    
    /**
     * 创建人
     */
    public Integer create_uid;

    @JsonIgnore
    public boolean create_uidDirtyFlag;
    
    /**
     * 创建人
     */
    public String create_uid_text;

    @JsonIgnore
    public boolean create_uid_textDirtyFlag;
    
    /**
     * 当前休假类型
     */
    public Integer current_leave_id;

    @JsonIgnore
    public boolean current_leave_idDirtyFlag;
    
    /**
     * 当前休假状态
     */
    public String current_leave_state;

    @JsonIgnore
    public boolean current_leave_stateDirtyFlag;
    
    /**
     * 部门
     */
    public Integer department_id;

    @JsonIgnore
    public boolean department_idDirtyFlag;
    
    /**
     * 部门
     */
    public String department_id_text;

    @JsonIgnore
    public boolean department_id_textDirtyFlag;
    
    /**
     * 直接徽章
     */
    public String direct_badge_ids;

    @JsonIgnore
    public boolean direct_badge_idsDirtyFlag;
    
    /**
     * 显示名称
     */
    public String display_name;

    @JsonIgnore
    public boolean display_nameDirtyFlag;
    
    /**
     * 紧急联系人
     */
    public String emergency_contact;

    @JsonIgnore
    public boolean emergency_contactDirtyFlag;
    
    /**
     * 紧急电话
     */
    public String emergency_phone;

    @JsonIgnore
    public boolean emergency_phoneDirtyFlag;
    
    /**
     * 负责人
     */
    public Integer expense_manager_id;

    @JsonIgnore
    public boolean expense_manager_idDirtyFlag;
    
    /**
     * 负责人
     */
    public String expense_manager_id_text;

    @JsonIgnore
    public boolean expense_manager_id_textDirtyFlag;
    
    /**
     * 性别
     */
    public String gender;

    @JsonIgnore
    public boolean genderDirtyFlag;
    
    /**
     * 员工人力资源目标
     */
    public String goal_ids;

    @JsonIgnore
    public boolean goal_idsDirtyFlag;
    
    /**
     * 员工文档
     */
    public String google_drive_link;

    @JsonIgnore
    public boolean google_drive_linkDirtyFlag;
    
    /**
     * 拥有徽章
     */
    public String has_badges;

    @JsonIgnore
    public boolean has_badgesDirtyFlag;
    
    /**
     * ID
     */
    public Integer id;

    @JsonIgnore
    public boolean idDirtyFlag;
    
    /**
     * 身份证号
     */
    public String identification_id;

    @JsonIgnore
    public boolean identification_idDirtyFlag;
    
    /**
     * 照片
     */
    public byte[] image;

    @JsonIgnore
    public boolean imageDirtyFlag;
    
    /**
     * 中等尺寸照片
     */
    public byte[] image_medium;

    @JsonIgnore
    public boolean image_mediumDirtyFlag;
    
    /**
     * 小尺寸照片
     */
    public byte[] image_small;

    @JsonIgnore
    public boolean image_smallDirtyFlag;
    
    /**
     * 今日缺勤
     */
    public String is_absent_totay;

    @JsonIgnore
    public boolean is_absent_totayDirtyFlag;
    
    /**
     * 已与公司地址相关联的员工住址
     */
    public String is_address_home_a_company;

    @JsonIgnore
    public boolean is_address_home_a_companyDirtyFlag;
    
    /**
     * 工作岗位
     */
    public Integer job_id;

    @JsonIgnore
    public boolean job_idDirtyFlag;
    
    /**
     * 工作岗位
     */
    public String job_id_text;

    @JsonIgnore
    public boolean job_id_textDirtyFlag;
    
    /**
     * 工作头衔
     */
    public String job_title;

    @JsonIgnore
    public boolean job_titleDirtyFlag;
    
    /**
     * 上班距离
     */
    public Integer km_home_work;

    @JsonIgnore
    public boolean km_home_workDirtyFlag;
    
    /**
     * 上次出勤
     */
    public Integer last_attendance_id;

    @JsonIgnore
    public boolean last_attendance_idDirtyFlag;
    
    /**
     * 休假天数
     */
    public Double leaves_count;

    @JsonIgnore
    public boolean leaves_countDirtyFlag;
    
    /**
     * 起始日期
     */
    public Timestamp leave_date_from;

    @JsonIgnore
    public boolean leave_date_fromDirtyFlag;
    
    /**
     * 至日期
     */
    public Timestamp leave_date_to;

    @JsonIgnore
    public boolean leave_date_toDirtyFlag;
    
    /**
     * 是管理者
     */
    public String manager;

    @JsonIgnore
    public boolean managerDirtyFlag;
    
    /**
     * 手动出勤
     */
    public String manual_attendance;

    @JsonIgnore
    public boolean manual_attendanceDirtyFlag;
    
    /**
     * 婚姻状况
     */
    public String marital;

    @JsonIgnore
    public boolean maritalDirtyFlag;
    
    /**
     * 体检日期
     */
    public Timestamp medic_exam;

    @JsonIgnore
    public boolean medic_examDirtyFlag;
    
    /**
     * 附件数量
     */
    public Integer message_attachment_count;

    @JsonIgnore
    public boolean message_attachment_countDirtyFlag;
    
    /**
     * 关注者(渠道)
     */
    public String message_channel_ids;

    @JsonIgnore
    public boolean message_channel_idsDirtyFlag;
    
    /**
     * 关注者
     */
    public String message_follower_ids;

    @JsonIgnore
    public boolean message_follower_idsDirtyFlag;
    
    /**
     * 消息递送错误
     */
    public String message_has_error;

    @JsonIgnore
    public boolean message_has_errorDirtyFlag;
    
    /**
     * 错误数
     */
    public Integer message_has_error_counter;

    @JsonIgnore
    public boolean message_has_error_counterDirtyFlag;
    
    /**
     * 信息
     */
    public String message_ids;

    @JsonIgnore
    public boolean message_idsDirtyFlag;
    
    /**
     * 是关注者
     */
    public String message_is_follower;

    @JsonIgnore
    public boolean message_is_followerDirtyFlag;
    
    /**
     * 附件
     */
    public Integer message_main_attachment_id;

    @JsonIgnore
    public boolean message_main_attachment_idDirtyFlag;
    
    /**
     * 需要采取行动
     */
    public String message_needaction;

    @JsonIgnore
    public boolean message_needactionDirtyFlag;
    
    /**
     * 行动数量
     */
    public Integer message_needaction_counter;

    @JsonIgnore
    public boolean message_needaction_counterDirtyFlag;
    
    /**
     * 关注者(业务伙伴)
     */
    public String message_partner_ids;

    @JsonIgnore
    public boolean message_partner_idsDirtyFlag;
    
    /**
     * 未读消息
     */
    public String message_unread;

    @JsonIgnore
    public boolean message_unreadDirtyFlag;
    
    /**
     * 未读消息计数器
     */
    public Integer message_unread_counter;

    @JsonIgnore
    public boolean message_unread_counterDirtyFlag;
    
    /**
     * 办公手机
     */
    public String mobile_phone;

    @JsonIgnore
    public boolean mobile_phoneDirtyFlag;
    
    /**
     * 名称
     */
    public String name;

    @JsonIgnore
    public boolean nameDirtyFlag;
    
    /**
     * 新近雇用的员工
     */
    public String newly_hired_employee;

    @JsonIgnore
    public boolean newly_hired_employeeDirtyFlag;
    
    /**
     * 备注
     */
    public String notes;

    @JsonIgnore
    public boolean notesDirtyFlag;
    
    /**
     * 经理
     */
    public Integer parent_id;

    @JsonIgnore
    public boolean parent_idDirtyFlag;
    
    /**
     * 经理
     */
    public String parent_id_text;

    @JsonIgnore
    public boolean parent_id_textDirtyFlag;
    
    /**
     * 护照号
     */
    public String passport_id;

    @JsonIgnore
    public boolean passport_idDirtyFlag;
    
    /**
     * 工作许可编号
     */
    public String permit_no;

    @JsonIgnore
    public boolean permit_noDirtyFlag;
    
    /**
     * PIN
     */
    public String pin;

    @JsonIgnore
    public boolean pinDirtyFlag;
    
    /**
     * 出生地
     */
    public String place_of_birth;

    @JsonIgnore
    public boolean place_of_birthDirtyFlag;
    
    /**
     * 剩余的法定休假
     */
    public Double remaining_leaves;

    @JsonIgnore
    public boolean remaining_leavesDirtyFlag;
    
    /**
     * 工作时间
     */
    public Integer resource_calendar_id;

    @JsonIgnore
    public boolean resource_calendar_idDirtyFlag;
    
    /**
     * 工作时间
     */
    public String resource_calendar_id_text;

    @JsonIgnore
    public boolean resource_calendar_id_textDirtyFlag;
    
    /**
     * 资源
     */
    public Integer resource_id;

    @JsonIgnore
    public boolean resource_idDirtyFlag;
    
    /**
     * 能查看剩余的休假
     */
    public String show_leaves;

    @JsonIgnore
    public boolean show_leavesDirtyFlag;
    
    /**
     * 社会保险号SIN
     */
    public String sinid;

    @JsonIgnore
    public boolean sinidDirtyFlag;
    
    /**
     * 配偶生日
     */
    public Timestamp spouse_birthdate;

    @JsonIgnore
    public boolean spouse_birthdateDirtyFlag;
    
    /**
     * 配偶全名
     */
    public String spouse_complete_name;

    @JsonIgnore
    public boolean spouse_complete_nameDirtyFlag;
    
    /**
     * 社会保障号SSN
     */
    public String ssnid;

    @JsonIgnore
    public boolean ssnidDirtyFlag;
    
    /**
     * 研究领域
     */
    public String study_field;

    @JsonIgnore
    public boolean study_fieldDirtyFlag;
    
    /**
     * 毕业院校
     */
    public String study_school;

    @JsonIgnore
    public boolean study_schoolDirtyFlag;
    
    /**
     * 时区
     */
    public String tz;

    @JsonIgnore
    public boolean tzDirtyFlag;
    
    /**
     * 用户
     */
    public Integer user_id;

    @JsonIgnore
    public boolean user_idDirtyFlag;
    
    /**
     * 用户
     */
    public String user_id_text;

    @JsonIgnore
    public boolean user_id_textDirtyFlag;
    
    /**
     * 公司汽车
     */
    public String vehicle;

    @JsonIgnore
    public boolean vehicleDirtyFlag;
    
    /**
     * 签证到期日期
     */
    public Timestamp visa_expire;

    @JsonIgnore
    public boolean visa_expireDirtyFlag;
    
    /**
     * 签证号
     */
    public String visa_no;

    @JsonIgnore
    public boolean visa_noDirtyFlag;
    
    /**
     * 网站消息
     */
    public String website_message_ids;

    @JsonIgnore
    public boolean website_message_idsDirtyFlag;
    
    /**
     * 工作EMail
     */
    public String work_email;

    @JsonIgnore
    public boolean work_emailDirtyFlag;
    
    /**
     * 工作地点
     */
    public String work_location;

    @JsonIgnore
    public boolean work_locationDirtyFlag;
    
    /**
     * 办公电话
     */
    public String work_phone;

    @JsonIgnore
    public boolean work_phoneDirtyFlag;
    
    /**
     * 最后更新时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp write_date;

    @JsonIgnore
    public boolean write_dateDirtyFlag;
    
    /**
     * 最后更新人
     */
    public Integer write_uid;

    @JsonIgnore
    public boolean write_uidDirtyFlag;
    
    /**
     * 最后更新人
     */
    public String write_uid_text;

    @JsonIgnore
    public boolean write_uid_textDirtyFlag;
    
    /**
     * 最后修改日
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp __last_update;

    @JsonIgnore
    public boolean __last_updateDirtyFlag;
    
    /**
     * 获取 [有效]
     */
    @JsonProperty("active")
    public String getActive(){
        return this.active ;
    }

    /**
     * 设置 [有效]
     */
    @JsonProperty("active")
    public void setActive(String  active){
        this.active = active ;
        this.activeDirtyFlag = true ;
    }

     /**
     * 获取 [有效]脏标记
     */
    @JsonIgnore
    public boolean getActiveDirtyFlag(){
        return this.activeDirtyFlag ;
    }   

    /**
     * 获取 [下一活动截止日期]
     */
    @JsonProperty("activity_date_deadline")
    public Timestamp getActivity_date_deadline(){
        return this.activity_date_deadline ;
    }

    /**
     * 设置 [下一活动截止日期]
     */
    @JsonProperty("activity_date_deadline")
    public void setActivity_date_deadline(Timestamp  activity_date_deadline){
        this.activity_date_deadline = activity_date_deadline ;
        this.activity_date_deadlineDirtyFlag = true ;
    }

     /**
     * 获取 [下一活动截止日期]脏标记
     */
    @JsonIgnore
    public boolean getActivity_date_deadlineDirtyFlag(){
        return this.activity_date_deadlineDirtyFlag ;
    }   

    /**
     * 获取 [活动]
     */
    @JsonProperty("activity_ids")
    public String getActivity_ids(){
        return this.activity_ids ;
    }

    /**
     * 设置 [活动]
     */
    @JsonProperty("activity_ids")
    public void setActivity_ids(String  activity_ids){
        this.activity_ids = activity_ids ;
        this.activity_idsDirtyFlag = true ;
    }

     /**
     * 获取 [活动]脏标记
     */
    @JsonIgnore
    public boolean getActivity_idsDirtyFlag(){
        return this.activity_idsDirtyFlag ;
    }   

    /**
     * 获取 [活动状态]
     */
    @JsonProperty("activity_state")
    public String getActivity_state(){
        return this.activity_state ;
    }

    /**
     * 设置 [活动状态]
     */
    @JsonProperty("activity_state")
    public void setActivity_state(String  activity_state){
        this.activity_state = activity_state ;
        this.activity_stateDirtyFlag = true ;
    }

     /**
     * 获取 [活动状态]脏标记
     */
    @JsonIgnore
    public boolean getActivity_stateDirtyFlag(){
        return this.activity_stateDirtyFlag ;
    }   

    /**
     * 获取 [下一活动摘要]
     */
    @JsonProperty("activity_summary")
    public String getActivity_summary(){
        return this.activity_summary ;
    }

    /**
     * 设置 [下一活动摘要]
     */
    @JsonProperty("activity_summary")
    public void setActivity_summary(String  activity_summary){
        this.activity_summary = activity_summary ;
        this.activity_summaryDirtyFlag = true ;
    }

     /**
     * 获取 [下一活动摘要]脏标记
     */
    @JsonIgnore
    public boolean getActivity_summaryDirtyFlag(){
        return this.activity_summaryDirtyFlag ;
    }   

    /**
     * 获取 [下一活动类型]
     */
    @JsonProperty("activity_type_id")
    public Integer getActivity_type_id(){
        return this.activity_type_id ;
    }

    /**
     * 设置 [下一活动类型]
     */
    @JsonProperty("activity_type_id")
    public void setActivity_type_id(Integer  activity_type_id){
        this.activity_type_id = activity_type_id ;
        this.activity_type_idDirtyFlag = true ;
    }

     /**
     * 获取 [下一活动类型]脏标记
     */
    @JsonIgnore
    public boolean getActivity_type_idDirtyFlag(){
        return this.activity_type_idDirtyFlag ;
    }   

    /**
     * 获取 [责任用户]
     */
    @JsonProperty("activity_user_id")
    public Integer getActivity_user_id(){
        return this.activity_user_id ;
    }

    /**
     * 设置 [责任用户]
     */
    @JsonProperty("activity_user_id")
    public void setActivity_user_id(Integer  activity_user_id){
        this.activity_user_id = activity_user_id ;
        this.activity_user_idDirtyFlag = true ;
    }

     /**
     * 获取 [责任用户]脏标记
     */
    @JsonIgnore
    public boolean getActivity_user_idDirtyFlag(){
        return this.activity_user_idDirtyFlag ;
    }   

    /**
     * 获取 [附加说明]
     */
    @JsonProperty("additional_note")
    public String getAdditional_note(){
        return this.additional_note ;
    }

    /**
     * 设置 [附加说明]
     */
    @JsonProperty("additional_note")
    public void setAdditional_note(String  additional_note){
        this.additional_note = additional_note ;
        this.additional_noteDirtyFlag = true ;
    }

     /**
     * 获取 [附加说明]脏标记
     */
    @JsonIgnore
    public boolean getAdditional_noteDirtyFlag(){
        return this.additional_noteDirtyFlag ;
    }   

    /**
     * 获取 [家庭住址]
     */
    @JsonProperty("address_home_id")
    public Integer getAddress_home_id(){
        return this.address_home_id ;
    }

    /**
     * 设置 [家庭住址]
     */
    @JsonProperty("address_home_id")
    public void setAddress_home_id(Integer  address_home_id){
        this.address_home_id = address_home_id ;
        this.address_home_idDirtyFlag = true ;
    }

     /**
     * 获取 [家庭住址]脏标记
     */
    @JsonIgnore
    public boolean getAddress_home_idDirtyFlag(){
        return this.address_home_idDirtyFlag ;
    }   

    /**
     * 获取 [家庭住址]
     */
    @JsonProperty("address_home_id_text")
    public String getAddress_home_id_text(){
        return this.address_home_id_text ;
    }

    /**
     * 设置 [家庭住址]
     */
    @JsonProperty("address_home_id_text")
    public void setAddress_home_id_text(String  address_home_id_text){
        this.address_home_id_text = address_home_id_text ;
        this.address_home_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [家庭住址]脏标记
     */
    @JsonIgnore
    public boolean getAddress_home_id_textDirtyFlag(){
        return this.address_home_id_textDirtyFlag ;
    }   

    /**
     * 获取 [工作地址]
     */
    @JsonProperty("address_id")
    public Integer getAddress_id(){
        return this.address_id ;
    }

    /**
     * 设置 [工作地址]
     */
    @JsonProperty("address_id")
    public void setAddress_id(Integer  address_id){
        this.address_id = address_id ;
        this.address_idDirtyFlag = true ;
    }

     /**
     * 获取 [工作地址]脏标记
     */
    @JsonIgnore
    public boolean getAddress_idDirtyFlag(){
        return this.address_idDirtyFlag ;
    }   

    /**
     * 获取 [工作地址]
     */
    @JsonProperty("address_id_text")
    public String getAddress_id_text(){
        return this.address_id_text ;
    }

    /**
     * 设置 [工作地址]
     */
    @JsonProperty("address_id_text")
    public void setAddress_id_text(String  address_id_text){
        this.address_id_text = address_id_text ;
        this.address_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [工作地址]脏标记
     */
    @JsonIgnore
    public boolean getAddress_id_textDirtyFlag(){
        return this.address_id_textDirtyFlag ;
    }   

    /**
     * 获取 [出勤]
     */
    @JsonProperty("attendance_ids")
    public String getAttendance_ids(){
        return this.attendance_ids ;
    }

    /**
     * 设置 [出勤]
     */
    @JsonProperty("attendance_ids")
    public void setAttendance_ids(String  attendance_ids){
        this.attendance_ids = attendance_ids ;
        this.attendance_idsDirtyFlag = true ;
    }

     /**
     * 获取 [出勤]脏标记
     */
    @JsonIgnore
    public boolean getAttendance_idsDirtyFlag(){
        return this.attendance_idsDirtyFlag ;
    }   

    /**
     * 获取 [出勤状态]
     */
    @JsonProperty("attendance_state")
    public String getAttendance_state(){
        return this.attendance_state ;
    }

    /**
     * 设置 [出勤状态]
     */
    @JsonProperty("attendance_state")
    public void setAttendance_state(String  attendance_state){
        this.attendance_state = attendance_state ;
        this.attendance_stateDirtyFlag = true ;
    }

     /**
     * 获取 [出勤状态]脏标记
     */
    @JsonIgnore
    public boolean getAttendance_stateDirtyFlag(){
        return this.attendance_stateDirtyFlag ;
    }   

    /**
     * 获取 [员工徽章]
     */
    @JsonProperty("badge_ids")
    public String getBadge_ids(){
        return this.badge_ids ;
    }

    /**
     * 设置 [员工徽章]
     */
    @JsonProperty("badge_ids")
    public void setBadge_ids(String  badge_ids){
        this.badge_ids = badge_ids ;
        this.badge_idsDirtyFlag = true ;
    }

     /**
     * 获取 [员工徽章]脏标记
     */
    @JsonIgnore
    public boolean getBadge_idsDirtyFlag(){
        return this.badge_idsDirtyFlag ;
    }   

    /**
     * 获取 [银行账户号码]
     */
    @JsonProperty("bank_account_id")
    public Integer getBank_account_id(){
        return this.bank_account_id ;
    }

    /**
     * 设置 [银行账户号码]
     */
    @JsonProperty("bank_account_id")
    public void setBank_account_id(Integer  bank_account_id){
        this.bank_account_id = bank_account_id ;
        this.bank_account_idDirtyFlag = true ;
    }

     /**
     * 获取 [银行账户号码]脏标记
     */
    @JsonIgnore
    public boolean getBank_account_idDirtyFlag(){
        return this.bank_account_idDirtyFlag ;
    }   

    /**
     * 获取 [工牌 ID]
     */
    @JsonProperty("barcode")
    public String getBarcode(){
        return this.barcode ;
    }

    /**
     * 设置 [工牌 ID]
     */
    @JsonProperty("barcode")
    public void setBarcode(String  barcode){
        this.barcode = barcode ;
        this.barcodeDirtyFlag = true ;
    }

     /**
     * 获取 [工牌 ID]脏标记
     */
    @JsonIgnore
    public boolean getBarcodeDirtyFlag(){
        return this.barcodeDirtyFlag ;
    }   

    /**
     * 获取 [出生日期]
     */
    @JsonProperty("birthday")
    public Timestamp getBirthday(){
        return this.birthday ;
    }

    /**
     * 设置 [出生日期]
     */
    @JsonProperty("birthday")
    public void setBirthday(Timestamp  birthday){
        this.birthday = birthday ;
        this.birthdayDirtyFlag = true ;
    }

     /**
     * 获取 [出生日期]脏标记
     */
    @JsonIgnore
    public boolean getBirthdayDirtyFlag(){
        return this.birthdayDirtyFlag ;
    }   

    /**
     * 获取 [标签]
     */
    @JsonProperty("category_ids")
    public String getCategory_ids(){
        return this.category_ids ;
    }

    /**
     * 设置 [标签]
     */
    @JsonProperty("category_ids")
    public void setCategory_ids(String  category_ids){
        this.category_ids = category_ids ;
        this.category_idsDirtyFlag = true ;
    }

     /**
     * 获取 [标签]脏标记
     */
    @JsonIgnore
    public boolean getCategory_idsDirtyFlag(){
        return this.category_idsDirtyFlag ;
    }   

    /**
     * 获取 [证书等级]
     */
    @JsonProperty("certificate")
    public String getCertificate(){
        return this.certificate ;
    }

    /**
     * 设置 [证书等级]
     */
    @JsonProperty("certificate")
    public void setCertificate(String  certificate){
        this.certificate = certificate ;
        this.certificateDirtyFlag = true ;
    }

     /**
     * 获取 [证书等级]脏标记
     */
    @JsonIgnore
    public boolean getCertificateDirtyFlag(){
        return this.certificateDirtyFlag ;
    }   

    /**
     * 获取 [子女数目]
     */
    @JsonProperty("children")
    public Integer getChildren(){
        return this.children ;
    }

    /**
     * 设置 [子女数目]
     */
    @JsonProperty("children")
    public void setChildren(Integer  children){
        this.children = children ;
        this.childrenDirtyFlag = true ;
    }

     /**
     * 获取 [子女数目]脏标记
     */
    @JsonIgnore
    public boolean getChildrenDirtyFlag(){
        return this.childrenDirtyFlag ;
    }   

    /**
     * 获取 [下属]
     */
    @JsonProperty("child_ids")
    public String getChild_ids(){
        return this.child_ids ;
    }

    /**
     * 设置 [下属]
     */
    @JsonProperty("child_ids")
    public void setChild_ids(String  child_ids){
        this.child_ids = child_ids ;
        this.child_idsDirtyFlag = true ;
    }

     /**
     * 获取 [下属]脏标记
     */
    @JsonIgnore
    public boolean getChild_idsDirtyFlag(){
        return this.child_idsDirtyFlag ;
    }   

    /**
     * 获取 [教练]
     */
    @JsonProperty("coach_id")
    public Integer getCoach_id(){
        return this.coach_id ;
    }

    /**
     * 设置 [教练]
     */
    @JsonProperty("coach_id")
    public void setCoach_id(Integer  coach_id){
        this.coach_id = coach_id ;
        this.coach_idDirtyFlag = true ;
    }

     /**
     * 获取 [教练]脏标记
     */
    @JsonIgnore
    public boolean getCoach_idDirtyFlag(){
        return this.coach_idDirtyFlag ;
    }   

    /**
     * 获取 [教练]
     */
    @JsonProperty("coach_id_text")
    public String getCoach_id_text(){
        return this.coach_id_text ;
    }

    /**
     * 设置 [教练]
     */
    @JsonProperty("coach_id_text")
    public void setCoach_id_text(String  coach_id_text){
        this.coach_id_text = coach_id_text ;
        this.coach_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [教练]脏标记
     */
    @JsonIgnore
    public boolean getCoach_id_textDirtyFlag(){
        return this.coach_id_textDirtyFlag ;
    }   

    /**
     * 获取 [颜色索引]
     */
    @JsonProperty("color")
    public Integer getColor(){
        return this.color ;
    }

    /**
     * 设置 [颜色索引]
     */
    @JsonProperty("color")
    public void setColor(Integer  color){
        this.color = color ;
        this.colorDirtyFlag = true ;
    }

     /**
     * 获取 [颜色索引]脏标记
     */
    @JsonIgnore
    public boolean getColorDirtyFlag(){
        return this.colorDirtyFlag ;
    }   

    /**
     * 获取 [公司]
     */
    @JsonProperty("company_id")
    public Integer getCompany_id(){
        return this.company_id ;
    }

    /**
     * 设置 [公司]
     */
    @JsonProperty("company_id")
    public void setCompany_id(Integer  company_id){
        this.company_id = company_id ;
        this.company_idDirtyFlag = true ;
    }

     /**
     * 获取 [公司]脏标记
     */
    @JsonIgnore
    public boolean getCompany_idDirtyFlag(){
        return this.company_idDirtyFlag ;
    }   

    /**
     * 获取 [公司]
     */
    @JsonProperty("company_id_text")
    public String getCompany_id_text(){
        return this.company_id_text ;
    }

    /**
     * 设置 [公司]
     */
    @JsonProperty("company_id_text")
    public void setCompany_id_text(String  company_id_text){
        this.company_id_text = company_id_text ;
        this.company_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [公司]脏标记
     */
    @JsonIgnore
    public boolean getCompany_id_textDirtyFlag(){
        return this.company_id_textDirtyFlag ;
    }   

    /**
     * 获取 [合同统计]
     */
    @JsonProperty("contracts_count")
    public Integer getContracts_count(){
        return this.contracts_count ;
    }

    /**
     * 设置 [合同统计]
     */
    @JsonProperty("contracts_count")
    public void setContracts_count(Integer  contracts_count){
        this.contracts_count = contracts_count ;
        this.contracts_countDirtyFlag = true ;
    }

     /**
     * 获取 [合同统计]脏标记
     */
    @JsonIgnore
    public boolean getContracts_countDirtyFlag(){
        return this.contracts_countDirtyFlag ;
    }   

    /**
     * 获取 [当前合同]
     */
    @JsonProperty("contract_id")
    public Integer getContract_id(){
        return this.contract_id ;
    }

    /**
     * 设置 [当前合同]
     */
    @JsonProperty("contract_id")
    public void setContract_id(Integer  contract_id){
        this.contract_id = contract_id ;
        this.contract_idDirtyFlag = true ;
    }

     /**
     * 获取 [当前合同]脏标记
     */
    @JsonIgnore
    public boolean getContract_idDirtyFlag(){
        return this.contract_idDirtyFlag ;
    }   

    /**
     * 获取 [员工合同]
     */
    @JsonProperty("contract_ids")
    public String getContract_ids(){
        return this.contract_ids ;
    }

    /**
     * 设置 [员工合同]
     */
    @JsonProperty("contract_ids")
    public void setContract_ids(String  contract_ids){
        this.contract_ids = contract_ids ;
        this.contract_idsDirtyFlag = true ;
    }

     /**
     * 获取 [员工合同]脏标记
     */
    @JsonIgnore
    public boolean getContract_idsDirtyFlag(){
        return this.contract_idsDirtyFlag ;
    }   

    /**
     * 获取 [国籍(国家)]
     */
    @JsonProperty("country_id")
    public Integer getCountry_id(){
        return this.country_id ;
    }

    /**
     * 设置 [国籍(国家)]
     */
    @JsonProperty("country_id")
    public void setCountry_id(Integer  country_id){
        this.country_id = country_id ;
        this.country_idDirtyFlag = true ;
    }

     /**
     * 获取 [国籍(国家)]脏标记
     */
    @JsonIgnore
    public boolean getCountry_idDirtyFlag(){
        return this.country_idDirtyFlag ;
    }   

    /**
     * 获取 [国籍(国家)]
     */
    @JsonProperty("country_id_text")
    public String getCountry_id_text(){
        return this.country_id_text ;
    }

    /**
     * 设置 [国籍(国家)]
     */
    @JsonProperty("country_id_text")
    public void setCountry_id_text(String  country_id_text){
        this.country_id_text = country_id_text ;
        this.country_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [国籍(国家)]脏标记
     */
    @JsonIgnore
    public boolean getCountry_id_textDirtyFlag(){
        return this.country_id_textDirtyFlag ;
    }   

    /**
     * 获取 [国籍]
     */
    @JsonProperty("country_of_birth")
    public Integer getCountry_of_birth(){
        return this.country_of_birth ;
    }

    /**
     * 设置 [国籍]
     */
    @JsonProperty("country_of_birth")
    public void setCountry_of_birth(Integer  country_of_birth){
        this.country_of_birth = country_of_birth ;
        this.country_of_birthDirtyFlag = true ;
    }

     /**
     * 获取 [国籍]脏标记
     */
    @JsonIgnore
    public boolean getCountry_of_birthDirtyFlag(){
        return this.country_of_birthDirtyFlag ;
    }   

    /**
     * 获取 [国籍]
     */
    @JsonProperty("country_of_birth_text")
    public String getCountry_of_birth_text(){
        return this.country_of_birth_text ;
    }

    /**
     * 设置 [国籍]
     */
    @JsonProperty("country_of_birth_text")
    public void setCountry_of_birth_text(String  country_of_birth_text){
        this.country_of_birth_text = country_of_birth_text ;
        this.country_of_birth_textDirtyFlag = true ;
    }

     /**
     * 获取 [国籍]脏标记
     */
    @JsonIgnore
    public boolean getCountry_of_birth_textDirtyFlag(){
        return this.country_of_birth_textDirtyFlag ;
    }   

    /**
     * 获取 [创建时间]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return this.create_date ;
    }

    /**
     * 设置 [创建时间]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

     /**
     * 获取 [创建时间]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return this.create_dateDirtyFlag ;
    }   

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return this.create_uid ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

     /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return this.create_uidDirtyFlag ;
    }   

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return this.create_uid_text ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

     /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return this.create_uid_textDirtyFlag ;
    }   

    /**
     * 获取 [当前休假类型]
     */
    @JsonProperty("current_leave_id")
    public Integer getCurrent_leave_id(){
        return this.current_leave_id ;
    }

    /**
     * 设置 [当前休假类型]
     */
    @JsonProperty("current_leave_id")
    public void setCurrent_leave_id(Integer  current_leave_id){
        this.current_leave_id = current_leave_id ;
        this.current_leave_idDirtyFlag = true ;
    }

     /**
     * 获取 [当前休假类型]脏标记
     */
    @JsonIgnore
    public boolean getCurrent_leave_idDirtyFlag(){
        return this.current_leave_idDirtyFlag ;
    }   

    /**
     * 获取 [当前休假状态]
     */
    @JsonProperty("current_leave_state")
    public String getCurrent_leave_state(){
        return this.current_leave_state ;
    }

    /**
     * 设置 [当前休假状态]
     */
    @JsonProperty("current_leave_state")
    public void setCurrent_leave_state(String  current_leave_state){
        this.current_leave_state = current_leave_state ;
        this.current_leave_stateDirtyFlag = true ;
    }

     /**
     * 获取 [当前休假状态]脏标记
     */
    @JsonIgnore
    public boolean getCurrent_leave_stateDirtyFlag(){
        return this.current_leave_stateDirtyFlag ;
    }   

    /**
     * 获取 [部门]
     */
    @JsonProperty("department_id")
    public Integer getDepartment_id(){
        return this.department_id ;
    }

    /**
     * 设置 [部门]
     */
    @JsonProperty("department_id")
    public void setDepartment_id(Integer  department_id){
        this.department_id = department_id ;
        this.department_idDirtyFlag = true ;
    }

     /**
     * 获取 [部门]脏标记
     */
    @JsonIgnore
    public boolean getDepartment_idDirtyFlag(){
        return this.department_idDirtyFlag ;
    }   

    /**
     * 获取 [部门]
     */
    @JsonProperty("department_id_text")
    public String getDepartment_id_text(){
        return this.department_id_text ;
    }

    /**
     * 设置 [部门]
     */
    @JsonProperty("department_id_text")
    public void setDepartment_id_text(String  department_id_text){
        this.department_id_text = department_id_text ;
        this.department_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [部门]脏标记
     */
    @JsonIgnore
    public boolean getDepartment_id_textDirtyFlag(){
        return this.department_id_textDirtyFlag ;
    }   

    /**
     * 获取 [直接徽章]
     */
    @JsonProperty("direct_badge_ids")
    public String getDirect_badge_ids(){
        return this.direct_badge_ids ;
    }

    /**
     * 设置 [直接徽章]
     */
    @JsonProperty("direct_badge_ids")
    public void setDirect_badge_ids(String  direct_badge_ids){
        this.direct_badge_ids = direct_badge_ids ;
        this.direct_badge_idsDirtyFlag = true ;
    }

     /**
     * 获取 [直接徽章]脏标记
     */
    @JsonIgnore
    public boolean getDirect_badge_idsDirtyFlag(){
        return this.direct_badge_idsDirtyFlag ;
    }   

    /**
     * 获取 [显示名称]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return this.display_name ;
    }

    /**
     * 设置 [显示名称]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

     /**
     * 获取 [显示名称]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return this.display_nameDirtyFlag ;
    }   

    /**
     * 获取 [紧急联系人]
     */
    @JsonProperty("emergency_contact")
    public String getEmergency_contact(){
        return this.emergency_contact ;
    }

    /**
     * 设置 [紧急联系人]
     */
    @JsonProperty("emergency_contact")
    public void setEmergency_contact(String  emergency_contact){
        this.emergency_contact = emergency_contact ;
        this.emergency_contactDirtyFlag = true ;
    }

     /**
     * 获取 [紧急联系人]脏标记
     */
    @JsonIgnore
    public boolean getEmergency_contactDirtyFlag(){
        return this.emergency_contactDirtyFlag ;
    }   

    /**
     * 获取 [紧急电话]
     */
    @JsonProperty("emergency_phone")
    public String getEmergency_phone(){
        return this.emergency_phone ;
    }

    /**
     * 设置 [紧急电话]
     */
    @JsonProperty("emergency_phone")
    public void setEmergency_phone(String  emergency_phone){
        this.emergency_phone = emergency_phone ;
        this.emergency_phoneDirtyFlag = true ;
    }

     /**
     * 获取 [紧急电话]脏标记
     */
    @JsonIgnore
    public boolean getEmergency_phoneDirtyFlag(){
        return this.emergency_phoneDirtyFlag ;
    }   

    /**
     * 获取 [负责人]
     */
    @JsonProperty("expense_manager_id")
    public Integer getExpense_manager_id(){
        return this.expense_manager_id ;
    }

    /**
     * 设置 [负责人]
     */
    @JsonProperty("expense_manager_id")
    public void setExpense_manager_id(Integer  expense_manager_id){
        this.expense_manager_id = expense_manager_id ;
        this.expense_manager_idDirtyFlag = true ;
    }

     /**
     * 获取 [负责人]脏标记
     */
    @JsonIgnore
    public boolean getExpense_manager_idDirtyFlag(){
        return this.expense_manager_idDirtyFlag ;
    }   

    /**
     * 获取 [负责人]
     */
    @JsonProperty("expense_manager_id_text")
    public String getExpense_manager_id_text(){
        return this.expense_manager_id_text ;
    }

    /**
     * 设置 [负责人]
     */
    @JsonProperty("expense_manager_id_text")
    public void setExpense_manager_id_text(String  expense_manager_id_text){
        this.expense_manager_id_text = expense_manager_id_text ;
        this.expense_manager_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [负责人]脏标记
     */
    @JsonIgnore
    public boolean getExpense_manager_id_textDirtyFlag(){
        return this.expense_manager_id_textDirtyFlag ;
    }   

    /**
     * 获取 [性别]
     */
    @JsonProperty("gender")
    public String getGender(){
        return this.gender ;
    }

    /**
     * 设置 [性别]
     */
    @JsonProperty("gender")
    public void setGender(String  gender){
        this.gender = gender ;
        this.genderDirtyFlag = true ;
    }

     /**
     * 获取 [性别]脏标记
     */
    @JsonIgnore
    public boolean getGenderDirtyFlag(){
        return this.genderDirtyFlag ;
    }   

    /**
     * 获取 [员工人力资源目标]
     */
    @JsonProperty("goal_ids")
    public String getGoal_ids(){
        return this.goal_ids ;
    }

    /**
     * 设置 [员工人力资源目标]
     */
    @JsonProperty("goal_ids")
    public void setGoal_ids(String  goal_ids){
        this.goal_ids = goal_ids ;
        this.goal_idsDirtyFlag = true ;
    }

     /**
     * 获取 [员工人力资源目标]脏标记
     */
    @JsonIgnore
    public boolean getGoal_idsDirtyFlag(){
        return this.goal_idsDirtyFlag ;
    }   

    /**
     * 获取 [员工文档]
     */
    @JsonProperty("google_drive_link")
    public String getGoogle_drive_link(){
        return this.google_drive_link ;
    }

    /**
     * 设置 [员工文档]
     */
    @JsonProperty("google_drive_link")
    public void setGoogle_drive_link(String  google_drive_link){
        this.google_drive_link = google_drive_link ;
        this.google_drive_linkDirtyFlag = true ;
    }

     /**
     * 获取 [员工文档]脏标记
     */
    @JsonIgnore
    public boolean getGoogle_drive_linkDirtyFlag(){
        return this.google_drive_linkDirtyFlag ;
    }   

    /**
     * 获取 [拥有徽章]
     */
    @JsonProperty("has_badges")
    public String getHas_badges(){
        return this.has_badges ;
    }

    /**
     * 设置 [拥有徽章]
     */
    @JsonProperty("has_badges")
    public void setHas_badges(String  has_badges){
        this.has_badges = has_badges ;
        this.has_badgesDirtyFlag = true ;
    }

     /**
     * 获取 [拥有徽章]脏标记
     */
    @JsonIgnore
    public boolean getHas_badgesDirtyFlag(){
        return this.has_badgesDirtyFlag ;
    }   

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return this.id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

     /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return this.idDirtyFlag ;
    }   

    /**
     * 获取 [身份证号]
     */
    @JsonProperty("identification_id")
    public String getIdentification_id(){
        return this.identification_id ;
    }

    /**
     * 设置 [身份证号]
     */
    @JsonProperty("identification_id")
    public void setIdentification_id(String  identification_id){
        this.identification_id = identification_id ;
        this.identification_idDirtyFlag = true ;
    }

     /**
     * 获取 [身份证号]脏标记
     */
    @JsonIgnore
    public boolean getIdentification_idDirtyFlag(){
        return this.identification_idDirtyFlag ;
    }   

    /**
     * 获取 [照片]
     */
    @JsonProperty("image")
    public byte[] getImage(){
        return this.image ;
    }

    /**
     * 设置 [照片]
     */
    @JsonProperty("image")
    public void setImage(byte[]  image){
        this.image = image ;
        this.imageDirtyFlag = true ;
    }

     /**
     * 获取 [照片]脏标记
     */
    @JsonIgnore
    public boolean getImageDirtyFlag(){
        return this.imageDirtyFlag ;
    }   

    /**
     * 获取 [中等尺寸照片]
     */
    @JsonProperty("image_medium")
    public byte[] getImage_medium(){
        return this.image_medium ;
    }

    /**
     * 设置 [中等尺寸照片]
     */
    @JsonProperty("image_medium")
    public void setImage_medium(byte[]  image_medium){
        this.image_medium = image_medium ;
        this.image_mediumDirtyFlag = true ;
    }

     /**
     * 获取 [中等尺寸照片]脏标记
     */
    @JsonIgnore
    public boolean getImage_mediumDirtyFlag(){
        return this.image_mediumDirtyFlag ;
    }   

    /**
     * 获取 [小尺寸照片]
     */
    @JsonProperty("image_small")
    public byte[] getImage_small(){
        return this.image_small ;
    }

    /**
     * 设置 [小尺寸照片]
     */
    @JsonProperty("image_small")
    public void setImage_small(byte[]  image_small){
        this.image_small = image_small ;
        this.image_smallDirtyFlag = true ;
    }

     /**
     * 获取 [小尺寸照片]脏标记
     */
    @JsonIgnore
    public boolean getImage_smallDirtyFlag(){
        return this.image_smallDirtyFlag ;
    }   

    /**
     * 获取 [今日缺勤]
     */
    @JsonProperty("is_absent_totay")
    public String getIs_absent_totay(){
        return this.is_absent_totay ;
    }

    /**
     * 设置 [今日缺勤]
     */
    @JsonProperty("is_absent_totay")
    public void setIs_absent_totay(String  is_absent_totay){
        this.is_absent_totay = is_absent_totay ;
        this.is_absent_totayDirtyFlag = true ;
    }

     /**
     * 获取 [今日缺勤]脏标记
     */
    @JsonIgnore
    public boolean getIs_absent_totayDirtyFlag(){
        return this.is_absent_totayDirtyFlag ;
    }   

    /**
     * 获取 [已与公司地址相关联的员工住址]
     */
    @JsonProperty("is_address_home_a_company")
    public String getIs_address_home_a_company(){
        return this.is_address_home_a_company ;
    }

    /**
     * 设置 [已与公司地址相关联的员工住址]
     */
    @JsonProperty("is_address_home_a_company")
    public void setIs_address_home_a_company(String  is_address_home_a_company){
        this.is_address_home_a_company = is_address_home_a_company ;
        this.is_address_home_a_companyDirtyFlag = true ;
    }

     /**
     * 获取 [已与公司地址相关联的员工住址]脏标记
     */
    @JsonIgnore
    public boolean getIs_address_home_a_companyDirtyFlag(){
        return this.is_address_home_a_companyDirtyFlag ;
    }   

    /**
     * 获取 [工作岗位]
     */
    @JsonProperty("job_id")
    public Integer getJob_id(){
        return this.job_id ;
    }

    /**
     * 设置 [工作岗位]
     */
    @JsonProperty("job_id")
    public void setJob_id(Integer  job_id){
        this.job_id = job_id ;
        this.job_idDirtyFlag = true ;
    }

     /**
     * 获取 [工作岗位]脏标记
     */
    @JsonIgnore
    public boolean getJob_idDirtyFlag(){
        return this.job_idDirtyFlag ;
    }   

    /**
     * 获取 [工作岗位]
     */
    @JsonProperty("job_id_text")
    public String getJob_id_text(){
        return this.job_id_text ;
    }

    /**
     * 设置 [工作岗位]
     */
    @JsonProperty("job_id_text")
    public void setJob_id_text(String  job_id_text){
        this.job_id_text = job_id_text ;
        this.job_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [工作岗位]脏标记
     */
    @JsonIgnore
    public boolean getJob_id_textDirtyFlag(){
        return this.job_id_textDirtyFlag ;
    }   

    /**
     * 获取 [工作头衔]
     */
    @JsonProperty("job_title")
    public String getJob_title(){
        return this.job_title ;
    }

    /**
     * 设置 [工作头衔]
     */
    @JsonProperty("job_title")
    public void setJob_title(String  job_title){
        this.job_title = job_title ;
        this.job_titleDirtyFlag = true ;
    }

     /**
     * 获取 [工作头衔]脏标记
     */
    @JsonIgnore
    public boolean getJob_titleDirtyFlag(){
        return this.job_titleDirtyFlag ;
    }   

    /**
     * 获取 [上班距离]
     */
    @JsonProperty("km_home_work")
    public Integer getKm_home_work(){
        return this.km_home_work ;
    }

    /**
     * 设置 [上班距离]
     */
    @JsonProperty("km_home_work")
    public void setKm_home_work(Integer  km_home_work){
        this.km_home_work = km_home_work ;
        this.km_home_workDirtyFlag = true ;
    }

     /**
     * 获取 [上班距离]脏标记
     */
    @JsonIgnore
    public boolean getKm_home_workDirtyFlag(){
        return this.km_home_workDirtyFlag ;
    }   

    /**
     * 获取 [上次出勤]
     */
    @JsonProperty("last_attendance_id")
    public Integer getLast_attendance_id(){
        return this.last_attendance_id ;
    }

    /**
     * 设置 [上次出勤]
     */
    @JsonProperty("last_attendance_id")
    public void setLast_attendance_id(Integer  last_attendance_id){
        this.last_attendance_id = last_attendance_id ;
        this.last_attendance_idDirtyFlag = true ;
    }

     /**
     * 获取 [上次出勤]脏标记
     */
    @JsonIgnore
    public boolean getLast_attendance_idDirtyFlag(){
        return this.last_attendance_idDirtyFlag ;
    }   

    /**
     * 获取 [休假天数]
     */
    @JsonProperty("leaves_count")
    public Double getLeaves_count(){
        return this.leaves_count ;
    }

    /**
     * 设置 [休假天数]
     */
    @JsonProperty("leaves_count")
    public void setLeaves_count(Double  leaves_count){
        this.leaves_count = leaves_count ;
        this.leaves_countDirtyFlag = true ;
    }

     /**
     * 获取 [休假天数]脏标记
     */
    @JsonIgnore
    public boolean getLeaves_countDirtyFlag(){
        return this.leaves_countDirtyFlag ;
    }   

    /**
     * 获取 [起始日期]
     */
    @JsonProperty("leave_date_from")
    public Timestamp getLeave_date_from(){
        return this.leave_date_from ;
    }

    /**
     * 设置 [起始日期]
     */
    @JsonProperty("leave_date_from")
    public void setLeave_date_from(Timestamp  leave_date_from){
        this.leave_date_from = leave_date_from ;
        this.leave_date_fromDirtyFlag = true ;
    }

     /**
     * 获取 [起始日期]脏标记
     */
    @JsonIgnore
    public boolean getLeave_date_fromDirtyFlag(){
        return this.leave_date_fromDirtyFlag ;
    }   

    /**
     * 获取 [至日期]
     */
    @JsonProperty("leave_date_to")
    public Timestamp getLeave_date_to(){
        return this.leave_date_to ;
    }

    /**
     * 设置 [至日期]
     */
    @JsonProperty("leave_date_to")
    public void setLeave_date_to(Timestamp  leave_date_to){
        this.leave_date_to = leave_date_to ;
        this.leave_date_toDirtyFlag = true ;
    }

     /**
     * 获取 [至日期]脏标记
     */
    @JsonIgnore
    public boolean getLeave_date_toDirtyFlag(){
        return this.leave_date_toDirtyFlag ;
    }   

    /**
     * 获取 [是管理者]
     */
    @JsonProperty("manager")
    public String getManager(){
        return this.manager ;
    }

    /**
     * 设置 [是管理者]
     */
    @JsonProperty("manager")
    public void setManager(String  manager){
        this.manager = manager ;
        this.managerDirtyFlag = true ;
    }

     /**
     * 获取 [是管理者]脏标记
     */
    @JsonIgnore
    public boolean getManagerDirtyFlag(){
        return this.managerDirtyFlag ;
    }   

    /**
     * 获取 [手动出勤]
     */
    @JsonProperty("manual_attendance")
    public String getManual_attendance(){
        return this.manual_attendance ;
    }

    /**
     * 设置 [手动出勤]
     */
    @JsonProperty("manual_attendance")
    public void setManual_attendance(String  manual_attendance){
        this.manual_attendance = manual_attendance ;
        this.manual_attendanceDirtyFlag = true ;
    }

     /**
     * 获取 [手动出勤]脏标记
     */
    @JsonIgnore
    public boolean getManual_attendanceDirtyFlag(){
        return this.manual_attendanceDirtyFlag ;
    }   

    /**
     * 获取 [婚姻状况]
     */
    @JsonProperty("marital")
    public String getMarital(){
        return this.marital ;
    }

    /**
     * 设置 [婚姻状况]
     */
    @JsonProperty("marital")
    public void setMarital(String  marital){
        this.marital = marital ;
        this.maritalDirtyFlag = true ;
    }

     /**
     * 获取 [婚姻状况]脏标记
     */
    @JsonIgnore
    public boolean getMaritalDirtyFlag(){
        return this.maritalDirtyFlag ;
    }   

    /**
     * 获取 [体检日期]
     */
    @JsonProperty("medic_exam")
    public Timestamp getMedic_exam(){
        return this.medic_exam ;
    }

    /**
     * 设置 [体检日期]
     */
    @JsonProperty("medic_exam")
    public void setMedic_exam(Timestamp  medic_exam){
        this.medic_exam = medic_exam ;
        this.medic_examDirtyFlag = true ;
    }

     /**
     * 获取 [体检日期]脏标记
     */
    @JsonIgnore
    public boolean getMedic_examDirtyFlag(){
        return this.medic_examDirtyFlag ;
    }   

    /**
     * 获取 [附件数量]
     */
    @JsonProperty("message_attachment_count")
    public Integer getMessage_attachment_count(){
        return this.message_attachment_count ;
    }

    /**
     * 设置 [附件数量]
     */
    @JsonProperty("message_attachment_count")
    public void setMessage_attachment_count(Integer  message_attachment_count){
        this.message_attachment_count = message_attachment_count ;
        this.message_attachment_countDirtyFlag = true ;
    }

     /**
     * 获取 [附件数量]脏标记
     */
    @JsonIgnore
    public boolean getMessage_attachment_countDirtyFlag(){
        return this.message_attachment_countDirtyFlag ;
    }   

    /**
     * 获取 [关注者(渠道)]
     */
    @JsonProperty("message_channel_ids")
    public String getMessage_channel_ids(){
        return this.message_channel_ids ;
    }

    /**
     * 设置 [关注者(渠道)]
     */
    @JsonProperty("message_channel_ids")
    public void setMessage_channel_ids(String  message_channel_ids){
        this.message_channel_ids = message_channel_ids ;
        this.message_channel_idsDirtyFlag = true ;
    }

     /**
     * 获取 [关注者(渠道)]脏标记
     */
    @JsonIgnore
    public boolean getMessage_channel_idsDirtyFlag(){
        return this.message_channel_idsDirtyFlag ;
    }   

    /**
     * 获取 [关注者]
     */
    @JsonProperty("message_follower_ids")
    public String getMessage_follower_ids(){
        return this.message_follower_ids ;
    }

    /**
     * 设置 [关注者]
     */
    @JsonProperty("message_follower_ids")
    public void setMessage_follower_ids(String  message_follower_ids){
        this.message_follower_ids = message_follower_ids ;
        this.message_follower_idsDirtyFlag = true ;
    }

     /**
     * 获取 [关注者]脏标记
     */
    @JsonIgnore
    public boolean getMessage_follower_idsDirtyFlag(){
        return this.message_follower_idsDirtyFlag ;
    }   

    /**
     * 获取 [消息递送错误]
     */
    @JsonProperty("message_has_error")
    public String getMessage_has_error(){
        return this.message_has_error ;
    }

    /**
     * 设置 [消息递送错误]
     */
    @JsonProperty("message_has_error")
    public void setMessage_has_error(String  message_has_error){
        this.message_has_error = message_has_error ;
        this.message_has_errorDirtyFlag = true ;
    }

     /**
     * 获取 [消息递送错误]脏标记
     */
    @JsonIgnore
    public boolean getMessage_has_errorDirtyFlag(){
        return this.message_has_errorDirtyFlag ;
    }   

    /**
     * 获取 [错误数]
     */
    @JsonProperty("message_has_error_counter")
    public Integer getMessage_has_error_counter(){
        return this.message_has_error_counter ;
    }

    /**
     * 设置 [错误数]
     */
    @JsonProperty("message_has_error_counter")
    public void setMessage_has_error_counter(Integer  message_has_error_counter){
        this.message_has_error_counter = message_has_error_counter ;
        this.message_has_error_counterDirtyFlag = true ;
    }

     /**
     * 获取 [错误数]脏标记
     */
    @JsonIgnore
    public boolean getMessage_has_error_counterDirtyFlag(){
        return this.message_has_error_counterDirtyFlag ;
    }   

    /**
     * 获取 [信息]
     */
    @JsonProperty("message_ids")
    public String getMessage_ids(){
        return this.message_ids ;
    }

    /**
     * 设置 [信息]
     */
    @JsonProperty("message_ids")
    public void setMessage_ids(String  message_ids){
        this.message_ids = message_ids ;
        this.message_idsDirtyFlag = true ;
    }

     /**
     * 获取 [信息]脏标记
     */
    @JsonIgnore
    public boolean getMessage_idsDirtyFlag(){
        return this.message_idsDirtyFlag ;
    }   

    /**
     * 获取 [是关注者]
     */
    @JsonProperty("message_is_follower")
    public String getMessage_is_follower(){
        return this.message_is_follower ;
    }

    /**
     * 设置 [是关注者]
     */
    @JsonProperty("message_is_follower")
    public void setMessage_is_follower(String  message_is_follower){
        this.message_is_follower = message_is_follower ;
        this.message_is_followerDirtyFlag = true ;
    }

     /**
     * 获取 [是关注者]脏标记
     */
    @JsonIgnore
    public boolean getMessage_is_followerDirtyFlag(){
        return this.message_is_followerDirtyFlag ;
    }   

    /**
     * 获取 [附件]
     */
    @JsonProperty("message_main_attachment_id")
    public Integer getMessage_main_attachment_id(){
        return this.message_main_attachment_id ;
    }

    /**
     * 设置 [附件]
     */
    @JsonProperty("message_main_attachment_id")
    public void setMessage_main_attachment_id(Integer  message_main_attachment_id){
        this.message_main_attachment_id = message_main_attachment_id ;
        this.message_main_attachment_idDirtyFlag = true ;
    }

     /**
     * 获取 [附件]脏标记
     */
    @JsonIgnore
    public boolean getMessage_main_attachment_idDirtyFlag(){
        return this.message_main_attachment_idDirtyFlag ;
    }   

    /**
     * 获取 [需要采取行动]
     */
    @JsonProperty("message_needaction")
    public String getMessage_needaction(){
        return this.message_needaction ;
    }

    /**
     * 设置 [需要采取行动]
     */
    @JsonProperty("message_needaction")
    public void setMessage_needaction(String  message_needaction){
        this.message_needaction = message_needaction ;
        this.message_needactionDirtyFlag = true ;
    }

     /**
     * 获取 [需要采取行动]脏标记
     */
    @JsonIgnore
    public boolean getMessage_needactionDirtyFlag(){
        return this.message_needactionDirtyFlag ;
    }   

    /**
     * 获取 [行动数量]
     */
    @JsonProperty("message_needaction_counter")
    public Integer getMessage_needaction_counter(){
        return this.message_needaction_counter ;
    }

    /**
     * 设置 [行动数量]
     */
    @JsonProperty("message_needaction_counter")
    public void setMessage_needaction_counter(Integer  message_needaction_counter){
        this.message_needaction_counter = message_needaction_counter ;
        this.message_needaction_counterDirtyFlag = true ;
    }

     /**
     * 获取 [行动数量]脏标记
     */
    @JsonIgnore
    public boolean getMessage_needaction_counterDirtyFlag(){
        return this.message_needaction_counterDirtyFlag ;
    }   

    /**
     * 获取 [关注者(业务伙伴)]
     */
    @JsonProperty("message_partner_ids")
    public String getMessage_partner_ids(){
        return this.message_partner_ids ;
    }

    /**
     * 设置 [关注者(业务伙伴)]
     */
    @JsonProperty("message_partner_ids")
    public void setMessage_partner_ids(String  message_partner_ids){
        this.message_partner_ids = message_partner_ids ;
        this.message_partner_idsDirtyFlag = true ;
    }

     /**
     * 获取 [关注者(业务伙伴)]脏标记
     */
    @JsonIgnore
    public boolean getMessage_partner_idsDirtyFlag(){
        return this.message_partner_idsDirtyFlag ;
    }   

    /**
     * 获取 [未读消息]
     */
    @JsonProperty("message_unread")
    public String getMessage_unread(){
        return this.message_unread ;
    }

    /**
     * 设置 [未读消息]
     */
    @JsonProperty("message_unread")
    public void setMessage_unread(String  message_unread){
        this.message_unread = message_unread ;
        this.message_unreadDirtyFlag = true ;
    }

     /**
     * 获取 [未读消息]脏标记
     */
    @JsonIgnore
    public boolean getMessage_unreadDirtyFlag(){
        return this.message_unreadDirtyFlag ;
    }   

    /**
     * 获取 [未读消息计数器]
     */
    @JsonProperty("message_unread_counter")
    public Integer getMessage_unread_counter(){
        return this.message_unread_counter ;
    }

    /**
     * 设置 [未读消息计数器]
     */
    @JsonProperty("message_unread_counter")
    public void setMessage_unread_counter(Integer  message_unread_counter){
        this.message_unread_counter = message_unread_counter ;
        this.message_unread_counterDirtyFlag = true ;
    }

     /**
     * 获取 [未读消息计数器]脏标记
     */
    @JsonIgnore
    public boolean getMessage_unread_counterDirtyFlag(){
        return this.message_unread_counterDirtyFlag ;
    }   

    /**
     * 获取 [办公手机]
     */
    @JsonProperty("mobile_phone")
    public String getMobile_phone(){
        return this.mobile_phone ;
    }

    /**
     * 设置 [办公手机]
     */
    @JsonProperty("mobile_phone")
    public void setMobile_phone(String  mobile_phone){
        this.mobile_phone = mobile_phone ;
        this.mobile_phoneDirtyFlag = true ;
    }

     /**
     * 获取 [办公手机]脏标记
     */
    @JsonIgnore
    public boolean getMobile_phoneDirtyFlag(){
        return this.mobile_phoneDirtyFlag ;
    }   

    /**
     * 获取 [名称]
     */
    @JsonProperty("name")
    public String getName(){
        return this.name ;
    }

    /**
     * 设置 [名称]
     */
    @JsonProperty("name")
    public void setName(String  name){
        this.name = name ;
        this.nameDirtyFlag = true ;
    }

     /**
     * 获取 [名称]脏标记
     */
    @JsonIgnore
    public boolean getNameDirtyFlag(){
        return this.nameDirtyFlag ;
    }   

    /**
     * 获取 [新近雇用的员工]
     */
    @JsonProperty("newly_hired_employee")
    public String getNewly_hired_employee(){
        return this.newly_hired_employee ;
    }

    /**
     * 设置 [新近雇用的员工]
     */
    @JsonProperty("newly_hired_employee")
    public void setNewly_hired_employee(String  newly_hired_employee){
        this.newly_hired_employee = newly_hired_employee ;
        this.newly_hired_employeeDirtyFlag = true ;
    }

     /**
     * 获取 [新近雇用的员工]脏标记
     */
    @JsonIgnore
    public boolean getNewly_hired_employeeDirtyFlag(){
        return this.newly_hired_employeeDirtyFlag ;
    }   

    /**
     * 获取 [备注]
     */
    @JsonProperty("notes")
    public String getNotes(){
        return this.notes ;
    }

    /**
     * 设置 [备注]
     */
    @JsonProperty("notes")
    public void setNotes(String  notes){
        this.notes = notes ;
        this.notesDirtyFlag = true ;
    }

     /**
     * 获取 [备注]脏标记
     */
    @JsonIgnore
    public boolean getNotesDirtyFlag(){
        return this.notesDirtyFlag ;
    }   

    /**
     * 获取 [经理]
     */
    @JsonProperty("parent_id")
    public Integer getParent_id(){
        return this.parent_id ;
    }

    /**
     * 设置 [经理]
     */
    @JsonProperty("parent_id")
    public void setParent_id(Integer  parent_id){
        this.parent_id = parent_id ;
        this.parent_idDirtyFlag = true ;
    }

     /**
     * 获取 [经理]脏标记
     */
    @JsonIgnore
    public boolean getParent_idDirtyFlag(){
        return this.parent_idDirtyFlag ;
    }   

    /**
     * 获取 [经理]
     */
    @JsonProperty("parent_id_text")
    public String getParent_id_text(){
        return this.parent_id_text ;
    }

    /**
     * 设置 [经理]
     */
    @JsonProperty("parent_id_text")
    public void setParent_id_text(String  parent_id_text){
        this.parent_id_text = parent_id_text ;
        this.parent_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [经理]脏标记
     */
    @JsonIgnore
    public boolean getParent_id_textDirtyFlag(){
        return this.parent_id_textDirtyFlag ;
    }   

    /**
     * 获取 [护照号]
     */
    @JsonProperty("passport_id")
    public String getPassport_id(){
        return this.passport_id ;
    }

    /**
     * 设置 [护照号]
     */
    @JsonProperty("passport_id")
    public void setPassport_id(String  passport_id){
        this.passport_id = passport_id ;
        this.passport_idDirtyFlag = true ;
    }

     /**
     * 获取 [护照号]脏标记
     */
    @JsonIgnore
    public boolean getPassport_idDirtyFlag(){
        return this.passport_idDirtyFlag ;
    }   

    /**
     * 获取 [工作许可编号]
     */
    @JsonProperty("permit_no")
    public String getPermit_no(){
        return this.permit_no ;
    }

    /**
     * 设置 [工作许可编号]
     */
    @JsonProperty("permit_no")
    public void setPermit_no(String  permit_no){
        this.permit_no = permit_no ;
        this.permit_noDirtyFlag = true ;
    }

     /**
     * 获取 [工作许可编号]脏标记
     */
    @JsonIgnore
    public boolean getPermit_noDirtyFlag(){
        return this.permit_noDirtyFlag ;
    }   

    /**
     * 获取 [PIN]
     */
    @JsonProperty("pin")
    public String getPin(){
        return this.pin ;
    }

    /**
     * 设置 [PIN]
     */
    @JsonProperty("pin")
    public void setPin(String  pin){
        this.pin = pin ;
        this.pinDirtyFlag = true ;
    }

     /**
     * 获取 [PIN]脏标记
     */
    @JsonIgnore
    public boolean getPinDirtyFlag(){
        return this.pinDirtyFlag ;
    }   

    /**
     * 获取 [出生地]
     */
    @JsonProperty("place_of_birth")
    public String getPlace_of_birth(){
        return this.place_of_birth ;
    }

    /**
     * 设置 [出生地]
     */
    @JsonProperty("place_of_birth")
    public void setPlace_of_birth(String  place_of_birth){
        this.place_of_birth = place_of_birth ;
        this.place_of_birthDirtyFlag = true ;
    }

     /**
     * 获取 [出生地]脏标记
     */
    @JsonIgnore
    public boolean getPlace_of_birthDirtyFlag(){
        return this.place_of_birthDirtyFlag ;
    }   

    /**
     * 获取 [剩余的法定休假]
     */
    @JsonProperty("remaining_leaves")
    public Double getRemaining_leaves(){
        return this.remaining_leaves ;
    }

    /**
     * 设置 [剩余的法定休假]
     */
    @JsonProperty("remaining_leaves")
    public void setRemaining_leaves(Double  remaining_leaves){
        this.remaining_leaves = remaining_leaves ;
        this.remaining_leavesDirtyFlag = true ;
    }

     /**
     * 获取 [剩余的法定休假]脏标记
     */
    @JsonIgnore
    public boolean getRemaining_leavesDirtyFlag(){
        return this.remaining_leavesDirtyFlag ;
    }   

    /**
     * 获取 [工作时间]
     */
    @JsonProperty("resource_calendar_id")
    public Integer getResource_calendar_id(){
        return this.resource_calendar_id ;
    }

    /**
     * 设置 [工作时间]
     */
    @JsonProperty("resource_calendar_id")
    public void setResource_calendar_id(Integer  resource_calendar_id){
        this.resource_calendar_id = resource_calendar_id ;
        this.resource_calendar_idDirtyFlag = true ;
    }

     /**
     * 获取 [工作时间]脏标记
     */
    @JsonIgnore
    public boolean getResource_calendar_idDirtyFlag(){
        return this.resource_calendar_idDirtyFlag ;
    }   

    /**
     * 获取 [工作时间]
     */
    @JsonProperty("resource_calendar_id_text")
    public String getResource_calendar_id_text(){
        return this.resource_calendar_id_text ;
    }

    /**
     * 设置 [工作时间]
     */
    @JsonProperty("resource_calendar_id_text")
    public void setResource_calendar_id_text(String  resource_calendar_id_text){
        this.resource_calendar_id_text = resource_calendar_id_text ;
        this.resource_calendar_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [工作时间]脏标记
     */
    @JsonIgnore
    public boolean getResource_calendar_id_textDirtyFlag(){
        return this.resource_calendar_id_textDirtyFlag ;
    }   

    /**
     * 获取 [资源]
     */
    @JsonProperty("resource_id")
    public Integer getResource_id(){
        return this.resource_id ;
    }

    /**
     * 设置 [资源]
     */
    @JsonProperty("resource_id")
    public void setResource_id(Integer  resource_id){
        this.resource_id = resource_id ;
        this.resource_idDirtyFlag = true ;
    }

     /**
     * 获取 [资源]脏标记
     */
    @JsonIgnore
    public boolean getResource_idDirtyFlag(){
        return this.resource_idDirtyFlag ;
    }   

    /**
     * 获取 [能查看剩余的休假]
     */
    @JsonProperty("show_leaves")
    public String getShow_leaves(){
        return this.show_leaves ;
    }

    /**
     * 设置 [能查看剩余的休假]
     */
    @JsonProperty("show_leaves")
    public void setShow_leaves(String  show_leaves){
        this.show_leaves = show_leaves ;
        this.show_leavesDirtyFlag = true ;
    }

     /**
     * 获取 [能查看剩余的休假]脏标记
     */
    @JsonIgnore
    public boolean getShow_leavesDirtyFlag(){
        return this.show_leavesDirtyFlag ;
    }   

    /**
     * 获取 [社会保险号SIN]
     */
    @JsonProperty("sinid")
    public String getSinid(){
        return this.sinid ;
    }

    /**
     * 设置 [社会保险号SIN]
     */
    @JsonProperty("sinid")
    public void setSinid(String  sinid){
        this.sinid = sinid ;
        this.sinidDirtyFlag = true ;
    }

     /**
     * 获取 [社会保险号SIN]脏标记
     */
    @JsonIgnore
    public boolean getSinidDirtyFlag(){
        return this.sinidDirtyFlag ;
    }   

    /**
     * 获取 [配偶生日]
     */
    @JsonProperty("spouse_birthdate")
    public Timestamp getSpouse_birthdate(){
        return this.spouse_birthdate ;
    }

    /**
     * 设置 [配偶生日]
     */
    @JsonProperty("spouse_birthdate")
    public void setSpouse_birthdate(Timestamp  spouse_birthdate){
        this.spouse_birthdate = spouse_birthdate ;
        this.spouse_birthdateDirtyFlag = true ;
    }

     /**
     * 获取 [配偶生日]脏标记
     */
    @JsonIgnore
    public boolean getSpouse_birthdateDirtyFlag(){
        return this.spouse_birthdateDirtyFlag ;
    }   

    /**
     * 获取 [配偶全名]
     */
    @JsonProperty("spouse_complete_name")
    public String getSpouse_complete_name(){
        return this.spouse_complete_name ;
    }

    /**
     * 设置 [配偶全名]
     */
    @JsonProperty("spouse_complete_name")
    public void setSpouse_complete_name(String  spouse_complete_name){
        this.spouse_complete_name = spouse_complete_name ;
        this.spouse_complete_nameDirtyFlag = true ;
    }

     /**
     * 获取 [配偶全名]脏标记
     */
    @JsonIgnore
    public boolean getSpouse_complete_nameDirtyFlag(){
        return this.spouse_complete_nameDirtyFlag ;
    }   

    /**
     * 获取 [社会保障号SSN]
     */
    @JsonProperty("ssnid")
    public String getSsnid(){
        return this.ssnid ;
    }

    /**
     * 设置 [社会保障号SSN]
     */
    @JsonProperty("ssnid")
    public void setSsnid(String  ssnid){
        this.ssnid = ssnid ;
        this.ssnidDirtyFlag = true ;
    }

     /**
     * 获取 [社会保障号SSN]脏标记
     */
    @JsonIgnore
    public boolean getSsnidDirtyFlag(){
        return this.ssnidDirtyFlag ;
    }   

    /**
     * 获取 [研究领域]
     */
    @JsonProperty("study_field")
    public String getStudy_field(){
        return this.study_field ;
    }

    /**
     * 设置 [研究领域]
     */
    @JsonProperty("study_field")
    public void setStudy_field(String  study_field){
        this.study_field = study_field ;
        this.study_fieldDirtyFlag = true ;
    }

     /**
     * 获取 [研究领域]脏标记
     */
    @JsonIgnore
    public boolean getStudy_fieldDirtyFlag(){
        return this.study_fieldDirtyFlag ;
    }   

    /**
     * 获取 [毕业院校]
     */
    @JsonProperty("study_school")
    public String getStudy_school(){
        return this.study_school ;
    }

    /**
     * 设置 [毕业院校]
     */
    @JsonProperty("study_school")
    public void setStudy_school(String  study_school){
        this.study_school = study_school ;
        this.study_schoolDirtyFlag = true ;
    }

     /**
     * 获取 [毕业院校]脏标记
     */
    @JsonIgnore
    public boolean getStudy_schoolDirtyFlag(){
        return this.study_schoolDirtyFlag ;
    }   

    /**
     * 获取 [时区]
     */
    @JsonProperty("tz")
    public String getTz(){
        return this.tz ;
    }

    /**
     * 设置 [时区]
     */
    @JsonProperty("tz")
    public void setTz(String  tz){
        this.tz = tz ;
        this.tzDirtyFlag = true ;
    }

     /**
     * 获取 [时区]脏标记
     */
    @JsonIgnore
    public boolean getTzDirtyFlag(){
        return this.tzDirtyFlag ;
    }   

    /**
     * 获取 [用户]
     */
    @JsonProperty("user_id")
    public Integer getUser_id(){
        return this.user_id ;
    }

    /**
     * 设置 [用户]
     */
    @JsonProperty("user_id")
    public void setUser_id(Integer  user_id){
        this.user_id = user_id ;
        this.user_idDirtyFlag = true ;
    }

     /**
     * 获取 [用户]脏标记
     */
    @JsonIgnore
    public boolean getUser_idDirtyFlag(){
        return this.user_idDirtyFlag ;
    }   

    /**
     * 获取 [用户]
     */
    @JsonProperty("user_id_text")
    public String getUser_id_text(){
        return this.user_id_text ;
    }

    /**
     * 设置 [用户]
     */
    @JsonProperty("user_id_text")
    public void setUser_id_text(String  user_id_text){
        this.user_id_text = user_id_text ;
        this.user_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [用户]脏标记
     */
    @JsonIgnore
    public boolean getUser_id_textDirtyFlag(){
        return this.user_id_textDirtyFlag ;
    }   

    /**
     * 获取 [公司汽车]
     */
    @JsonProperty("vehicle")
    public String getVehicle(){
        return this.vehicle ;
    }

    /**
     * 设置 [公司汽车]
     */
    @JsonProperty("vehicle")
    public void setVehicle(String  vehicle){
        this.vehicle = vehicle ;
        this.vehicleDirtyFlag = true ;
    }

     /**
     * 获取 [公司汽车]脏标记
     */
    @JsonIgnore
    public boolean getVehicleDirtyFlag(){
        return this.vehicleDirtyFlag ;
    }   

    /**
     * 获取 [签证到期日期]
     */
    @JsonProperty("visa_expire")
    public Timestamp getVisa_expire(){
        return this.visa_expire ;
    }

    /**
     * 设置 [签证到期日期]
     */
    @JsonProperty("visa_expire")
    public void setVisa_expire(Timestamp  visa_expire){
        this.visa_expire = visa_expire ;
        this.visa_expireDirtyFlag = true ;
    }

     /**
     * 获取 [签证到期日期]脏标记
     */
    @JsonIgnore
    public boolean getVisa_expireDirtyFlag(){
        return this.visa_expireDirtyFlag ;
    }   

    /**
     * 获取 [签证号]
     */
    @JsonProperty("visa_no")
    public String getVisa_no(){
        return this.visa_no ;
    }

    /**
     * 设置 [签证号]
     */
    @JsonProperty("visa_no")
    public void setVisa_no(String  visa_no){
        this.visa_no = visa_no ;
        this.visa_noDirtyFlag = true ;
    }

     /**
     * 获取 [签证号]脏标记
     */
    @JsonIgnore
    public boolean getVisa_noDirtyFlag(){
        return this.visa_noDirtyFlag ;
    }   

    /**
     * 获取 [网站消息]
     */
    @JsonProperty("website_message_ids")
    public String getWebsite_message_ids(){
        return this.website_message_ids ;
    }

    /**
     * 设置 [网站消息]
     */
    @JsonProperty("website_message_ids")
    public void setWebsite_message_ids(String  website_message_ids){
        this.website_message_ids = website_message_ids ;
        this.website_message_idsDirtyFlag = true ;
    }

     /**
     * 获取 [网站消息]脏标记
     */
    @JsonIgnore
    public boolean getWebsite_message_idsDirtyFlag(){
        return this.website_message_idsDirtyFlag ;
    }   

    /**
     * 获取 [工作EMail]
     */
    @JsonProperty("work_email")
    public String getWork_email(){
        return this.work_email ;
    }

    /**
     * 设置 [工作EMail]
     */
    @JsonProperty("work_email")
    public void setWork_email(String  work_email){
        this.work_email = work_email ;
        this.work_emailDirtyFlag = true ;
    }

     /**
     * 获取 [工作EMail]脏标记
     */
    @JsonIgnore
    public boolean getWork_emailDirtyFlag(){
        return this.work_emailDirtyFlag ;
    }   

    /**
     * 获取 [工作地点]
     */
    @JsonProperty("work_location")
    public String getWork_location(){
        return this.work_location ;
    }

    /**
     * 设置 [工作地点]
     */
    @JsonProperty("work_location")
    public void setWork_location(String  work_location){
        this.work_location = work_location ;
        this.work_locationDirtyFlag = true ;
    }

     /**
     * 获取 [工作地点]脏标记
     */
    @JsonIgnore
    public boolean getWork_locationDirtyFlag(){
        return this.work_locationDirtyFlag ;
    }   

    /**
     * 获取 [办公电话]
     */
    @JsonProperty("work_phone")
    public String getWork_phone(){
        return this.work_phone ;
    }

    /**
     * 设置 [办公电话]
     */
    @JsonProperty("work_phone")
    public void setWork_phone(String  work_phone){
        this.work_phone = work_phone ;
        this.work_phoneDirtyFlag = true ;
    }

     /**
     * 获取 [办公电话]脏标记
     */
    @JsonIgnore
    public boolean getWork_phoneDirtyFlag(){
        return this.work_phoneDirtyFlag ;
    }   

    /**
     * 获取 [最后更新时间]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return this.write_date ;
    }

    /**
     * 设置 [最后更新时间]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新时间]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return this.write_dateDirtyFlag ;
    }   

    /**
     * 获取 [最后更新人]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return this.write_uid ;
    }

    /**
     * 设置 [最后更新人]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新人]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return this.write_uidDirtyFlag ;
    }   

    /**
     * 获取 [最后更新人]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return this.write_uid_text ;
    }

    /**
     * 设置 [最后更新人]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新人]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return this.write_uid_textDirtyFlag ;
    }   

    /**
     * 获取 [最后修改日]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return this.__last_update ;
    }

    /**
     * 设置 [最后修改日]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

     /**
     * 获取 [最后修改日]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return this.__last_updateDirtyFlag ;
    }   



    public void fromMap(Map<String, Object> map) throws Exception {
		if(map.get("active") instanceof Boolean){
			this.setActive(((Boolean)map.get("active"))? "true" : "false");
		}
		if(!(map.get("activity_date_deadline") instanceof Boolean)&& map.get("activity_date_deadline")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd").parse((String)map.get("activity_date_deadline"));
   			this.setActivity_date_deadline(new Timestamp(parse.getTime()));
		}
		if(!(map.get("activity_ids") instanceof Boolean)&& map.get("activity_ids")!=null){
			Object[] objs = (Object[])map.get("activity_ids");
			if(objs.length > 0){
				Integer[] activity_ids = Arrays.copyOfRange(objs,0,objs.length,Integer[].class);
				this.setActivity_ids(Arrays.toString(activity_ids));
			}
		}
		if(!(map.get("activity_state") instanceof Boolean)&& map.get("activity_state")!=null){
			this.setActivity_state((String)map.get("activity_state"));
		}
		if(!(map.get("activity_summary") instanceof Boolean)&& map.get("activity_summary")!=null){
			this.setActivity_summary((String)map.get("activity_summary"));
		}
		if(!(map.get("activity_type_id") instanceof Boolean)&& map.get("activity_type_id")!=null){
			Object[] objs = (Object[])map.get("activity_type_id");
			if(objs.length > 0){
				this.setActivity_type_id((Integer)objs[0]);
			}
		}
		if(!(map.get("activity_user_id") instanceof Boolean)&& map.get("activity_user_id")!=null){
			Object[] objs = (Object[])map.get("activity_user_id");
			if(objs.length > 0){
				this.setActivity_user_id((Integer)objs[0]);
			}
		}
		if(!(map.get("additional_note") instanceof Boolean)&& map.get("additional_note")!=null){
			this.setAdditional_note((String)map.get("additional_note"));
		}
		if(!(map.get("address_home_id") instanceof Boolean)&& map.get("address_home_id")!=null){
			Object[] objs = (Object[])map.get("address_home_id");
			if(objs.length > 0){
				this.setAddress_home_id((Integer)objs[0]);
			}
		}
		if(!(map.get("address_home_id") instanceof Boolean)&& map.get("address_home_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("address_home_id");
			if(objs.length > 1){
				this.setAddress_home_id_text((String)objs[1]);
			}
		}
		if(!(map.get("address_id") instanceof Boolean)&& map.get("address_id")!=null){
			Object[] objs = (Object[])map.get("address_id");
			if(objs.length > 0){
				this.setAddress_id((Integer)objs[0]);
			}
		}
		if(!(map.get("address_id") instanceof Boolean)&& map.get("address_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("address_id");
			if(objs.length > 1){
				this.setAddress_id_text((String)objs[1]);
			}
		}
		if(!(map.get("attendance_ids") instanceof Boolean)&& map.get("attendance_ids")!=null){
			Object[] objs = (Object[])map.get("attendance_ids");
			if(objs.length > 0){
				Integer[] attendance_ids = Arrays.copyOfRange(objs,0,objs.length,Integer[].class);
				this.setAttendance_ids(Arrays.toString(attendance_ids));
			}
		}
		if(!(map.get("attendance_state") instanceof Boolean)&& map.get("attendance_state")!=null){
			this.setAttendance_state((String)map.get("attendance_state"));
		}
		if(!(map.get("badge_ids") instanceof Boolean)&& map.get("badge_ids")!=null){
			Object[] objs = (Object[])map.get("badge_ids");
			if(objs.length > 0){
				Integer[] badge_ids = Arrays.copyOfRange(objs,0,objs.length,Integer[].class);
				this.setBadge_ids(Arrays.toString(badge_ids));
			}
		}
		if(!(map.get("bank_account_id") instanceof Boolean)&& map.get("bank_account_id")!=null){
			Object[] objs = (Object[])map.get("bank_account_id");
			if(objs.length > 0){
				this.setBank_account_id((Integer)objs[0]);
			}
		}
		if(!(map.get("barcode") instanceof Boolean)&& map.get("barcode")!=null){
			this.setBarcode((String)map.get("barcode"));
		}
		if(!(map.get("birthday") instanceof Boolean)&& map.get("birthday")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd").parse((String)map.get("birthday"));
   			this.setBirthday(new Timestamp(parse.getTime()));
		}
		if(!(map.get("category_ids") instanceof Boolean)&& map.get("category_ids")!=null){
			Object[] objs = (Object[])map.get("category_ids");
			if(objs.length > 0){
				Integer[] category_ids = Arrays.copyOfRange(objs,0,objs.length,Integer[].class);
				this.setCategory_ids(Arrays.toString(category_ids));
			}
		}
		if(!(map.get("certificate") instanceof Boolean)&& map.get("certificate")!=null){
			this.setCertificate((String)map.get("certificate"));
		}
		if(!(map.get("children") instanceof Boolean)&& map.get("children")!=null){
			this.setChildren((Integer)map.get("children"));
		}
		if(!(map.get("child_ids") instanceof Boolean)&& map.get("child_ids")!=null){
			Object[] objs = (Object[])map.get("child_ids");
			if(objs.length > 0){
				Integer[] child_ids = Arrays.copyOfRange(objs,0,objs.length,Integer[].class);
				this.setChild_ids(Arrays.toString(child_ids));
			}
		}
		if(!(map.get("coach_id") instanceof Boolean)&& map.get("coach_id")!=null){
			Object[] objs = (Object[])map.get("coach_id");
			if(objs.length > 0){
				this.setCoach_id((Integer)objs[0]);
			}
		}
		if(!(map.get("coach_id") instanceof Boolean)&& map.get("coach_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("coach_id");
			if(objs.length > 1){
				this.setCoach_id_text((String)objs[1]);
			}
		}
		if(!(map.get("color") instanceof Boolean)&& map.get("color")!=null){
			this.setColor((Integer)map.get("color"));
		}
		if(!(map.get("company_id") instanceof Boolean)&& map.get("company_id")!=null){
			Object[] objs = (Object[])map.get("company_id");
			if(objs.length > 0){
				this.setCompany_id((Integer)objs[0]);
			}
		}
		if(!(map.get("company_id") instanceof Boolean)&& map.get("company_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("company_id");
			if(objs.length > 1){
				this.setCompany_id_text((String)objs[1]);
			}
		}
		if(!(map.get("contracts_count") instanceof Boolean)&& map.get("contracts_count")!=null){
			this.setContracts_count((Integer)map.get("contracts_count"));
		}
		if(!(map.get("contract_id") instanceof Boolean)&& map.get("contract_id")!=null){
			Object[] objs = (Object[])map.get("contract_id");
			if(objs.length > 0){
				this.setContract_id((Integer)objs[0]);
			}
		}
		if(!(map.get("contract_ids") instanceof Boolean)&& map.get("contract_ids")!=null){
			Object[] objs = (Object[])map.get("contract_ids");
			if(objs.length > 0){
				Integer[] contract_ids = Arrays.copyOfRange(objs,0,objs.length,Integer[].class);
				this.setContract_ids(Arrays.toString(contract_ids));
			}
		}
		if(!(map.get("country_id") instanceof Boolean)&& map.get("country_id")!=null){
			Object[] objs = (Object[])map.get("country_id");
			if(objs.length > 0){
				this.setCountry_id((Integer)objs[0]);
			}
		}
		if(!(map.get("country_id") instanceof Boolean)&& map.get("country_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("country_id");
			if(objs.length > 1){
				this.setCountry_id_text((String)objs[1]);
			}
		}
		if(!(map.get("country_of_birth") instanceof Boolean)&& map.get("country_of_birth")!=null){
			Object[] objs = (Object[])map.get("country_of_birth");
			if(objs.length > 0){
				this.setCountry_of_birth((Integer)objs[0]);
			}
		}
		if(!(map.get("country_of_birth") instanceof Boolean)&& map.get("country_of_birth")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("country_of_birth");
			if(objs.length > 1){
				this.setCountry_of_birth_text((String)objs[1]);
			}
		}
		if(!(map.get("create_date") instanceof Boolean)&& map.get("create_date")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse((String)map.get("create_date"));
   			this.setCreate_date(new Timestamp(parse.getTime()));
		}
		if(!(map.get("create_uid") instanceof Boolean)&& map.get("create_uid")!=null){
			Object[] objs = (Object[])map.get("create_uid");
			if(objs.length > 0){
				this.setCreate_uid((Integer)objs[0]);
			}
		}
		if(!(map.get("create_uid") instanceof Boolean)&& map.get("create_uid")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("create_uid");
			if(objs.length > 1){
				this.setCreate_uid_text((String)objs[1]);
			}
		}
		if(!(map.get("current_leave_id") instanceof Boolean)&& map.get("current_leave_id")!=null){
			Object[] objs = (Object[])map.get("current_leave_id");
			if(objs.length > 0){
				this.setCurrent_leave_id((Integer)objs[0]);
			}
		}
		if(!(map.get("current_leave_state") instanceof Boolean)&& map.get("current_leave_state")!=null){
			this.setCurrent_leave_state((String)map.get("current_leave_state"));
		}
		if(!(map.get("department_id") instanceof Boolean)&& map.get("department_id")!=null){
			Object[] objs = (Object[])map.get("department_id");
			if(objs.length > 0){
				this.setDepartment_id((Integer)objs[0]);
			}
		}
		if(!(map.get("department_id") instanceof Boolean)&& map.get("department_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("department_id");
			if(objs.length > 1){
				this.setDepartment_id_text((String)objs[1]);
			}
		}
		if(!(map.get("direct_badge_ids") instanceof Boolean)&& map.get("direct_badge_ids")!=null){
			Object[] objs = (Object[])map.get("direct_badge_ids");
			if(objs.length > 0){
				Integer[] direct_badge_ids = Arrays.copyOfRange(objs,0,objs.length,Integer[].class);
				this.setDirect_badge_ids(Arrays.toString(direct_badge_ids));
			}
		}
		if(!(map.get("display_name") instanceof Boolean)&& map.get("display_name")!=null){
			this.setDisplay_name((String)map.get("display_name"));
		}
		if(!(map.get("emergency_contact") instanceof Boolean)&& map.get("emergency_contact")!=null){
			this.setEmergency_contact((String)map.get("emergency_contact"));
		}
		if(!(map.get("emergency_phone") instanceof Boolean)&& map.get("emergency_phone")!=null){
			this.setEmergency_phone((String)map.get("emergency_phone"));
		}
		if(!(map.get("expense_manager_id") instanceof Boolean)&& map.get("expense_manager_id")!=null){
			Object[] objs = (Object[])map.get("expense_manager_id");
			if(objs.length > 0){
				this.setExpense_manager_id((Integer)objs[0]);
			}
		}
		if(!(map.get("expense_manager_id") instanceof Boolean)&& map.get("expense_manager_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("expense_manager_id");
			if(objs.length > 1){
				this.setExpense_manager_id_text((String)objs[1]);
			}
		}
		if(!(map.get("gender") instanceof Boolean)&& map.get("gender")!=null){
			this.setGender((String)map.get("gender"));
		}
		if(!(map.get("goal_ids") instanceof Boolean)&& map.get("goal_ids")!=null){
			Object[] objs = (Object[])map.get("goal_ids");
			if(objs.length > 0){
				Integer[] goal_ids = Arrays.copyOfRange(objs,0,objs.length,Integer[].class);
				this.setGoal_ids(Arrays.toString(goal_ids));
			}
		}
		if(!(map.get("google_drive_link") instanceof Boolean)&& map.get("google_drive_link")!=null){
			this.setGoogle_drive_link((String)map.get("google_drive_link"));
		}
		if(map.get("has_badges") instanceof Boolean){
			this.setHas_badges(((Boolean)map.get("has_badges"))? "true" : "false");
		}
		if(!(map.get("id") instanceof Boolean)&& map.get("id")!=null){
			this.setId((Integer)map.get("id"));
		}
		if(!(map.get("identification_id") instanceof Boolean)&& map.get("identification_id")!=null){
			this.setIdentification_id((String)map.get("identification_id"));
		}
		if(!(map.get("image") instanceof Boolean)&& map.get("image")!=null){
			//暂时忽略
			//this.setImage(((String)map.get("image")).getBytes("UTF-8"));
		}
		if(!(map.get("image_medium") instanceof Boolean)&& map.get("image_medium")!=null){
			//暂时忽略
			//this.setImage_medium(((String)map.get("image_medium")).getBytes("UTF-8"));
		}
		if(!(map.get("image_small") instanceof Boolean)&& map.get("image_small")!=null){
			//暂时忽略
			//this.setImage_small(((String)map.get("image_small")).getBytes("UTF-8"));
		}
		if(map.get("is_absent_totay") instanceof Boolean){
			this.setIs_absent_totay(((Boolean)map.get("is_absent_totay"))? "true" : "false");
		}
		if(map.get("is_address_home_a_company") instanceof Boolean){
			this.setIs_address_home_a_company(((Boolean)map.get("is_address_home_a_company"))? "true" : "false");
		}
		if(!(map.get("job_id") instanceof Boolean)&& map.get("job_id")!=null){
			Object[] objs = (Object[])map.get("job_id");
			if(objs.length > 0){
				this.setJob_id((Integer)objs[0]);
			}
		}
		if(!(map.get("job_id") instanceof Boolean)&& map.get("job_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("job_id");
			if(objs.length > 1){
				this.setJob_id_text((String)objs[1]);
			}
		}
		if(!(map.get("job_title") instanceof Boolean)&& map.get("job_title")!=null){
			this.setJob_title((String)map.get("job_title"));
		}
		if(!(map.get("km_home_work") instanceof Boolean)&& map.get("km_home_work")!=null){
			this.setKm_home_work((Integer)map.get("km_home_work"));
		}
		if(!(map.get("last_attendance_id") instanceof Boolean)&& map.get("last_attendance_id")!=null){
			Object[] objs = (Object[])map.get("last_attendance_id");
			if(objs.length > 0){
				this.setLast_attendance_id((Integer)objs[0]);
			}
		}
		if(!(map.get("leaves_count") instanceof Boolean)&& map.get("leaves_count")!=null){
			this.setLeaves_count((Double)map.get("leaves_count"));
		}
		if(!(map.get("leave_date_from") instanceof Boolean)&& map.get("leave_date_from")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd").parse((String)map.get("leave_date_from"));
   			this.setLeave_date_from(new Timestamp(parse.getTime()));
		}
		if(!(map.get("leave_date_to") instanceof Boolean)&& map.get("leave_date_to")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd").parse((String)map.get("leave_date_to"));
   			this.setLeave_date_to(new Timestamp(parse.getTime()));
		}
		if(map.get("manager") instanceof Boolean){
			this.setManager(((Boolean)map.get("manager"))? "true" : "false");
		}
		if(map.get("manual_attendance") instanceof Boolean){
			this.setManual_attendance(((Boolean)map.get("manual_attendance"))? "true" : "false");
		}
		if(!(map.get("marital") instanceof Boolean)&& map.get("marital")!=null){
			this.setMarital((String)map.get("marital"));
		}
		if(!(map.get("medic_exam") instanceof Boolean)&& map.get("medic_exam")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd").parse((String)map.get("medic_exam"));
   			this.setMedic_exam(new Timestamp(parse.getTime()));
		}
		if(!(map.get("message_attachment_count") instanceof Boolean)&& map.get("message_attachment_count")!=null){
			this.setMessage_attachment_count((Integer)map.get("message_attachment_count"));
		}
		if(!(map.get("message_channel_ids") instanceof Boolean)&& map.get("message_channel_ids")!=null){
			Object[] objs = (Object[])map.get("message_channel_ids");
			if(objs.length > 0){
				Integer[] message_channel_ids = Arrays.copyOfRange(objs,0,objs.length,Integer[].class);
				this.setMessage_channel_ids(Arrays.toString(message_channel_ids));
			}
		}
		if(!(map.get("message_follower_ids") instanceof Boolean)&& map.get("message_follower_ids")!=null){
			Object[] objs = (Object[])map.get("message_follower_ids");
			if(objs.length > 0){
				Integer[] message_follower_ids = Arrays.copyOfRange(objs,0,objs.length,Integer[].class);
				this.setMessage_follower_ids(Arrays.toString(message_follower_ids));
			}
		}
		if(map.get("message_has_error") instanceof Boolean){
			this.setMessage_has_error(((Boolean)map.get("message_has_error"))? "true" : "false");
		}
		if(!(map.get("message_has_error_counter") instanceof Boolean)&& map.get("message_has_error_counter")!=null){
			this.setMessage_has_error_counter((Integer)map.get("message_has_error_counter"));
		}
		if(!(map.get("message_ids") instanceof Boolean)&& map.get("message_ids")!=null){
			Object[] objs = (Object[])map.get("message_ids");
			if(objs.length > 0){
				Integer[] message_ids = Arrays.copyOfRange(objs,0,objs.length,Integer[].class);
				this.setMessage_ids(Arrays.toString(message_ids));
			}
		}
		if(map.get("message_is_follower") instanceof Boolean){
			this.setMessage_is_follower(((Boolean)map.get("message_is_follower"))? "true" : "false");
		}
		if(!(map.get("message_main_attachment_id") instanceof Boolean)&& map.get("message_main_attachment_id")!=null){
			Object[] objs = (Object[])map.get("message_main_attachment_id");
			if(objs.length > 0){
				this.setMessage_main_attachment_id((Integer)objs[0]);
			}
		}
		if(map.get("message_needaction") instanceof Boolean){
			this.setMessage_needaction(((Boolean)map.get("message_needaction"))? "true" : "false");
		}
		if(!(map.get("message_needaction_counter") instanceof Boolean)&& map.get("message_needaction_counter")!=null){
			this.setMessage_needaction_counter((Integer)map.get("message_needaction_counter"));
		}
		if(!(map.get("message_partner_ids") instanceof Boolean)&& map.get("message_partner_ids")!=null){
			Object[] objs = (Object[])map.get("message_partner_ids");
			if(objs.length > 0){
				Integer[] message_partner_ids = Arrays.copyOfRange(objs,0,objs.length,Integer[].class);
				this.setMessage_partner_ids(Arrays.toString(message_partner_ids));
			}
		}
		if(map.get("message_unread") instanceof Boolean){
			this.setMessage_unread(((Boolean)map.get("message_unread"))? "true" : "false");
		}
		if(!(map.get("message_unread_counter") instanceof Boolean)&& map.get("message_unread_counter")!=null){
			this.setMessage_unread_counter((Integer)map.get("message_unread_counter"));
		}
		if(!(map.get("mobile_phone") instanceof Boolean)&& map.get("mobile_phone")!=null){
			this.setMobile_phone((String)map.get("mobile_phone"));
		}
		if(!(map.get("name") instanceof Boolean)&& map.get("name")!=null){
			this.setName((String)map.get("name"));
		}
		if(map.get("newly_hired_employee") instanceof Boolean){
			this.setNewly_hired_employee(((Boolean)map.get("newly_hired_employee"))? "true" : "false");
		}
		if(!(map.get("notes") instanceof Boolean)&& map.get("notes")!=null){
			this.setNotes((String)map.get("notes"));
		}
		if(!(map.get("parent_id") instanceof Boolean)&& map.get("parent_id")!=null){
			Object[] objs = (Object[])map.get("parent_id");
			if(objs.length > 0){
				this.setParent_id((Integer)objs[0]);
			}
		}
		if(!(map.get("parent_id") instanceof Boolean)&& map.get("parent_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("parent_id");
			if(objs.length > 1){
				this.setParent_id_text((String)objs[1]);
			}
		}
		if(!(map.get("passport_id") instanceof Boolean)&& map.get("passport_id")!=null){
			this.setPassport_id((String)map.get("passport_id"));
		}
		if(!(map.get("permit_no") instanceof Boolean)&& map.get("permit_no")!=null){
			this.setPermit_no((String)map.get("permit_no"));
		}
		if(!(map.get("pin") instanceof Boolean)&& map.get("pin")!=null){
			this.setPin((String)map.get("pin"));
		}
		if(!(map.get("place_of_birth") instanceof Boolean)&& map.get("place_of_birth")!=null){
			this.setPlace_of_birth((String)map.get("place_of_birth"));
		}
		if(!(map.get("remaining_leaves") instanceof Boolean)&& map.get("remaining_leaves")!=null){
			this.setRemaining_leaves((Double)map.get("remaining_leaves"));
		}
		if(!(map.get("resource_calendar_id") instanceof Boolean)&& map.get("resource_calendar_id")!=null){
			Object[] objs = (Object[])map.get("resource_calendar_id");
			if(objs.length > 0){
				this.setResource_calendar_id((Integer)objs[0]);
			}
		}
		if(!(map.get("resource_calendar_id") instanceof Boolean)&& map.get("resource_calendar_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("resource_calendar_id");
			if(objs.length > 1){
				this.setResource_calendar_id_text((String)objs[1]);
			}
		}
		if(!(map.get("resource_id") instanceof Boolean)&& map.get("resource_id")!=null){
			Object[] objs = (Object[])map.get("resource_id");
			if(objs.length > 0){
				this.setResource_id((Integer)objs[0]);
			}
		}
		if(map.get("show_leaves") instanceof Boolean){
			this.setShow_leaves(((Boolean)map.get("show_leaves"))? "true" : "false");
		}
		if(!(map.get("sinid") instanceof Boolean)&& map.get("sinid")!=null){
			this.setSinid((String)map.get("sinid"));
		}
		if(!(map.get("spouse_birthdate") instanceof Boolean)&& map.get("spouse_birthdate")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd").parse((String)map.get("spouse_birthdate"));
   			this.setSpouse_birthdate(new Timestamp(parse.getTime()));
		}
		if(!(map.get("spouse_complete_name") instanceof Boolean)&& map.get("spouse_complete_name")!=null){
			this.setSpouse_complete_name((String)map.get("spouse_complete_name"));
		}
		if(!(map.get("ssnid") instanceof Boolean)&& map.get("ssnid")!=null){
			this.setSsnid((String)map.get("ssnid"));
		}
		if(!(map.get("study_field") instanceof Boolean)&& map.get("study_field")!=null){
			this.setStudy_field((String)map.get("study_field"));
		}
		if(!(map.get("study_school") instanceof Boolean)&& map.get("study_school")!=null){
			this.setStudy_school((String)map.get("study_school"));
		}
		if(!(map.get("tz") instanceof Boolean)&& map.get("tz")!=null){
			this.setTz((String)map.get("tz"));
		}
		if(!(map.get("user_id") instanceof Boolean)&& map.get("user_id")!=null){
			Object[] objs = (Object[])map.get("user_id");
			if(objs.length > 0){
				this.setUser_id((Integer)objs[0]);
			}
		}
		if(!(map.get("user_id") instanceof Boolean)&& map.get("user_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("user_id");
			if(objs.length > 1){
				this.setUser_id_text((String)objs[1]);
			}
		}
		if(!(map.get("vehicle") instanceof Boolean)&& map.get("vehicle")!=null){
			this.setVehicle((String)map.get("vehicle"));
		}
		if(!(map.get("visa_expire") instanceof Boolean)&& map.get("visa_expire")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd").parse((String)map.get("visa_expire"));
   			this.setVisa_expire(new Timestamp(parse.getTime()));
		}
		if(!(map.get("visa_no") instanceof Boolean)&& map.get("visa_no")!=null){
			this.setVisa_no((String)map.get("visa_no"));
		}
		if(!(map.get("website_message_ids") instanceof Boolean)&& map.get("website_message_ids")!=null){
			Object[] objs = (Object[])map.get("website_message_ids");
			if(objs.length > 0){
				Integer[] website_message_ids = Arrays.copyOfRange(objs,0,objs.length,Integer[].class);
				this.setWebsite_message_ids(Arrays.toString(website_message_ids));
			}
		}
		if(!(map.get("work_email") instanceof Boolean)&& map.get("work_email")!=null){
			this.setWork_email((String)map.get("work_email"));
		}
		if(!(map.get("work_location") instanceof Boolean)&& map.get("work_location")!=null){
			this.setWork_location((String)map.get("work_location"));
		}
		if(!(map.get("work_phone") instanceof Boolean)&& map.get("work_phone")!=null){
			this.setWork_phone((String)map.get("work_phone"));
		}
		if(!(map.get("write_date") instanceof Boolean)&& map.get("write_date")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse((String)map.get("write_date"));
   			this.setWrite_date(new Timestamp(parse.getTime()));
		}
		if(!(map.get("write_uid") instanceof Boolean)&& map.get("write_uid")!=null){
			Object[] objs = (Object[])map.get("write_uid");
			if(objs.length > 0){
				this.setWrite_uid((Integer)objs[0]);
			}
		}
		if(!(map.get("write_uid") instanceof Boolean)&& map.get("write_uid")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("write_uid");
			if(objs.length > 1){
				this.setWrite_uid_text((String)objs[1]);
			}
		}
		if(!(map.get("__last_update") instanceof Boolean)&& map.get("__last_update")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse((String)map.get("__last_update"));
   			this.set__last_update(new Timestamp(parse.getTime()));
		}
	}

	public Map<String, Object> toMap() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		if(this.getActive()!=null&&this.getActiveDirtyFlag()){
			map.put("active",Boolean.parseBoolean(this.getActive()));		
		}		if(this.getActivity_date_deadline()!=null&&this.getActivity_date_deadlineDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			String datetimeStr = sdf.format(this.getActivity_date_deadline());
			map.put("activity_date_deadline",datetimeStr);
		}else if(this.getActivity_date_deadlineDirtyFlag()){
			map.put("activity_date_deadline",false);
		}
		if(this.getActivity_ids()!=null&&this.getActivity_idsDirtyFlag()){
			map.put("activity_ids",this.getActivity_ids());
		}else if(this.getActivity_idsDirtyFlag()){
			map.put("activity_ids",false);
		}
		if(this.getActivity_state()!=null&&this.getActivity_stateDirtyFlag()){
			map.put("activity_state",this.getActivity_state());
		}else if(this.getActivity_stateDirtyFlag()){
			map.put("activity_state",false);
		}
		if(this.getActivity_summary()!=null&&this.getActivity_summaryDirtyFlag()){
			map.put("activity_summary",this.getActivity_summary());
		}else if(this.getActivity_summaryDirtyFlag()){
			map.put("activity_summary",false);
		}
		if(this.getActivity_type_id()!=null&&this.getActivity_type_idDirtyFlag()){
			map.put("activity_type_id",this.getActivity_type_id());
		}else if(this.getActivity_type_idDirtyFlag()){
			map.put("activity_type_id",false);
		}
		if(this.getActivity_user_id()!=null&&this.getActivity_user_idDirtyFlag()){
			map.put("activity_user_id",this.getActivity_user_id());
		}else if(this.getActivity_user_idDirtyFlag()){
			map.put("activity_user_id",false);
		}
		if(this.getAdditional_note()!=null&&this.getAdditional_noteDirtyFlag()){
			map.put("additional_note",this.getAdditional_note());
		}else if(this.getAdditional_noteDirtyFlag()){
			map.put("additional_note",false);
		}
		if(this.getAddress_home_id()!=null&&this.getAddress_home_idDirtyFlag()){
			map.put("address_home_id",this.getAddress_home_id());
		}else if(this.getAddress_home_idDirtyFlag()){
			map.put("address_home_id",false);
		}
		if(this.getAddress_home_id_text()!=null&&this.getAddress_home_id_textDirtyFlag()){
			//忽略文本外键address_home_id_text
		}else if(this.getAddress_home_id_textDirtyFlag()){
			map.put("address_home_id",false);
		}
		if(this.getAddress_id()!=null&&this.getAddress_idDirtyFlag()){
			map.put("address_id",this.getAddress_id());
		}else if(this.getAddress_idDirtyFlag()){
			map.put("address_id",false);
		}
		if(this.getAddress_id_text()!=null&&this.getAddress_id_textDirtyFlag()){
			//忽略文本外键address_id_text
		}else if(this.getAddress_id_textDirtyFlag()){
			map.put("address_id",false);
		}
		if(this.getAttendance_ids()!=null&&this.getAttendance_idsDirtyFlag()){
			map.put("attendance_ids",this.getAttendance_ids());
		}else if(this.getAttendance_idsDirtyFlag()){
			map.put("attendance_ids",false);
		}
		if(this.getAttendance_state()!=null&&this.getAttendance_stateDirtyFlag()){
			map.put("attendance_state",this.getAttendance_state());
		}else if(this.getAttendance_stateDirtyFlag()){
			map.put("attendance_state",false);
		}
		if(this.getBadge_ids()!=null&&this.getBadge_idsDirtyFlag()){
			map.put("badge_ids",this.getBadge_ids());
		}else if(this.getBadge_idsDirtyFlag()){
			map.put("badge_ids",false);
		}
		if(this.getBank_account_id()!=null&&this.getBank_account_idDirtyFlag()){
			map.put("bank_account_id",this.getBank_account_id());
		}else if(this.getBank_account_idDirtyFlag()){
			map.put("bank_account_id",false);
		}
		if(this.getBarcode()!=null&&this.getBarcodeDirtyFlag()){
			map.put("barcode",this.getBarcode());
		}else if(this.getBarcodeDirtyFlag()){
			map.put("barcode",false);
		}
		if(this.getBirthday()!=null&&this.getBirthdayDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			String datetimeStr = sdf.format(this.getBirthday());
			map.put("birthday",datetimeStr);
		}else if(this.getBirthdayDirtyFlag()){
			map.put("birthday",false);
		}
		if(this.getCategory_ids()!=null&&this.getCategory_idsDirtyFlag()){
			map.put("category_ids",this.getCategory_ids());
		}else if(this.getCategory_idsDirtyFlag()){
			map.put("category_ids",false);
		}
		if(this.getCertificate()!=null&&this.getCertificateDirtyFlag()){
			map.put("certificate",this.getCertificate());
		}else if(this.getCertificateDirtyFlag()){
			map.put("certificate",false);
		}
		if(this.getChildren()!=null&&this.getChildrenDirtyFlag()){
			map.put("children",this.getChildren());
		}else if(this.getChildrenDirtyFlag()){
			map.put("children",false);
		}
		if(this.getChild_ids()!=null&&this.getChild_idsDirtyFlag()){
			map.put("child_ids",this.getChild_ids());
		}else if(this.getChild_idsDirtyFlag()){
			map.put("child_ids",false);
		}
		if(this.getCoach_id()!=null&&this.getCoach_idDirtyFlag()){
			map.put("coach_id",this.getCoach_id());
		}else if(this.getCoach_idDirtyFlag()){
			map.put("coach_id",false);
		}
		if(this.getCoach_id_text()!=null&&this.getCoach_id_textDirtyFlag()){
			//忽略文本外键coach_id_text
		}else if(this.getCoach_id_textDirtyFlag()){
			map.put("coach_id",false);
		}
		if(this.getColor()!=null&&this.getColorDirtyFlag()){
			map.put("color",this.getColor());
		}else if(this.getColorDirtyFlag()){
			map.put("color",false);
		}
		if(this.getCompany_id()!=null&&this.getCompany_idDirtyFlag()){
			map.put("company_id",this.getCompany_id());
		}else if(this.getCompany_idDirtyFlag()){
			map.put("company_id",false);
		}
		if(this.getCompany_id_text()!=null&&this.getCompany_id_textDirtyFlag()){
			//忽略文本外键company_id_text
		}else if(this.getCompany_id_textDirtyFlag()){
			map.put("company_id",false);
		}
		if(this.getContracts_count()!=null&&this.getContracts_countDirtyFlag()){
			map.put("contracts_count",this.getContracts_count());
		}else if(this.getContracts_countDirtyFlag()){
			map.put("contracts_count",false);
		}
		if(this.getContract_id()!=null&&this.getContract_idDirtyFlag()){
			map.put("contract_id",this.getContract_id());
		}else if(this.getContract_idDirtyFlag()){
			map.put("contract_id",false);
		}
		if(this.getContract_ids()!=null&&this.getContract_idsDirtyFlag()){
			map.put("contract_ids",this.getContract_ids());
		}else if(this.getContract_idsDirtyFlag()){
			map.put("contract_ids",false);
		}
		if(this.getCountry_id()!=null&&this.getCountry_idDirtyFlag()){
			map.put("country_id",this.getCountry_id());
		}else if(this.getCountry_idDirtyFlag()){
			map.put("country_id",false);
		}
		if(this.getCountry_id_text()!=null&&this.getCountry_id_textDirtyFlag()){
			//忽略文本外键country_id_text
		}else if(this.getCountry_id_textDirtyFlag()){
			map.put("country_id",false);
		}
		if(this.getCountry_of_birth()!=null&&this.getCountry_of_birthDirtyFlag()){
			map.put("country_of_birth",this.getCountry_of_birth());
		}else if(this.getCountry_of_birthDirtyFlag()){
			map.put("country_of_birth",false);
		}
		if(this.getCountry_of_birth_text()!=null&&this.getCountry_of_birth_textDirtyFlag()){
			//忽略文本外键country_of_birth_text
		}else if(this.getCountry_of_birth_textDirtyFlag()){
			map.put("country_of_birth",false);
		}
		if(this.getCreate_date()!=null&&this.getCreate_dateDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String datetimeStr = sdf.format(this.getCreate_date());
			map.put("create_date",datetimeStr);
		}else if(this.getCreate_dateDirtyFlag()){
			map.put("create_date",false);
		}
		if(this.getCreate_uid()!=null&&this.getCreate_uidDirtyFlag()){
			map.put("create_uid",this.getCreate_uid());
		}else if(this.getCreate_uidDirtyFlag()){
			map.put("create_uid",false);
		}
		if(this.getCreate_uid_text()!=null&&this.getCreate_uid_textDirtyFlag()){
			//忽略文本外键create_uid_text
		}else if(this.getCreate_uid_textDirtyFlag()){
			map.put("create_uid",false);
		}
		if(this.getCurrent_leave_id()!=null&&this.getCurrent_leave_idDirtyFlag()){
			map.put("current_leave_id",this.getCurrent_leave_id());
		}else if(this.getCurrent_leave_idDirtyFlag()){
			map.put("current_leave_id",false);
		}
		if(this.getCurrent_leave_state()!=null&&this.getCurrent_leave_stateDirtyFlag()){
			map.put("current_leave_state",this.getCurrent_leave_state());
		}else if(this.getCurrent_leave_stateDirtyFlag()){
			map.put("current_leave_state",false);
		}
		if(this.getDepartment_id()!=null&&this.getDepartment_idDirtyFlag()){
			map.put("department_id",this.getDepartment_id());
		}else if(this.getDepartment_idDirtyFlag()){
			map.put("department_id",false);
		}
		if(this.getDepartment_id_text()!=null&&this.getDepartment_id_textDirtyFlag()){
			//忽略文本外键department_id_text
		}else if(this.getDepartment_id_textDirtyFlag()){
			map.put("department_id",false);
		}
		if(this.getDirect_badge_ids()!=null&&this.getDirect_badge_idsDirtyFlag()){
			map.put("direct_badge_ids",this.getDirect_badge_ids());
		}else if(this.getDirect_badge_idsDirtyFlag()){
			map.put("direct_badge_ids",false);
		}
		if(this.getDisplay_name()!=null&&this.getDisplay_nameDirtyFlag()){
			map.put("display_name",this.getDisplay_name());
		}else if(this.getDisplay_nameDirtyFlag()){
			map.put("display_name",false);
		}
		if(this.getEmergency_contact()!=null&&this.getEmergency_contactDirtyFlag()){
			map.put("emergency_contact",this.getEmergency_contact());
		}else if(this.getEmergency_contactDirtyFlag()){
			map.put("emergency_contact",false);
		}
		if(this.getEmergency_phone()!=null&&this.getEmergency_phoneDirtyFlag()){
			map.put("emergency_phone",this.getEmergency_phone());
		}else if(this.getEmergency_phoneDirtyFlag()){
			map.put("emergency_phone",false);
		}
		if(this.getExpense_manager_id()!=null&&this.getExpense_manager_idDirtyFlag()){
			map.put("expense_manager_id",this.getExpense_manager_id());
		}else if(this.getExpense_manager_idDirtyFlag()){
			map.put("expense_manager_id",false);
		}
		if(this.getExpense_manager_id_text()!=null&&this.getExpense_manager_id_textDirtyFlag()){
			//忽略文本外键expense_manager_id_text
		}else if(this.getExpense_manager_id_textDirtyFlag()){
			map.put("expense_manager_id",false);
		}
		if(this.getGender()!=null&&this.getGenderDirtyFlag()){
			map.put("gender",this.getGender());
		}else if(this.getGenderDirtyFlag()){
			map.put("gender",false);
		}
		if(this.getGoal_ids()!=null&&this.getGoal_idsDirtyFlag()){
			map.put("goal_ids",this.getGoal_ids());
		}else if(this.getGoal_idsDirtyFlag()){
			map.put("goal_ids",false);
		}
		if(this.getGoogle_drive_link()!=null&&this.getGoogle_drive_linkDirtyFlag()){
			map.put("google_drive_link",this.getGoogle_drive_link());
		}else if(this.getGoogle_drive_linkDirtyFlag()){
			map.put("google_drive_link",false);
		}
		if(this.getHas_badges()!=null&&this.getHas_badgesDirtyFlag()){
			map.put("has_badges",Boolean.parseBoolean(this.getHas_badges()));		
		}		if(this.getId()!=null&&this.getIdDirtyFlag()){
			map.put("id",this.getId());
		}else if(this.getIdDirtyFlag()){
			map.put("id",false);
		}
		if(this.getIdentification_id()!=null&&this.getIdentification_idDirtyFlag()){
			map.put("identification_id",this.getIdentification_id());
		}else if(this.getIdentification_idDirtyFlag()){
			map.put("identification_id",false);
		}
		if(this.getImage()!=null&&this.getImageDirtyFlag()){
			//暂不支持binary类型image
		}else if(this.getImageDirtyFlag()){
			map.put("image",false);
		}
		if(this.getImage_medium()!=null&&this.getImage_mediumDirtyFlag()){
			//暂不支持binary类型image_medium
		}else if(this.getImage_mediumDirtyFlag()){
			map.put("image_medium",false);
		}
		if(this.getImage_small()!=null&&this.getImage_smallDirtyFlag()){
			//暂不支持binary类型image_small
		}else if(this.getImage_smallDirtyFlag()){
			map.put("image_small",false);
		}
		if(this.getIs_absent_totay()!=null&&this.getIs_absent_totayDirtyFlag()){
			map.put("is_absent_totay",Boolean.parseBoolean(this.getIs_absent_totay()));		
		}		if(this.getIs_address_home_a_company()!=null&&this.getIs_address_home_a_companyDirtyFlag()){
			map.put("is_address_home_a_company",Boolean.parseBoolean(this.getIs_address_home_a_company()));		
		}		if(this.getJob_id()!=null&&this.getJob_idDirtyFlag()){
			map.put("job_id",this.getJob_id());
		}else if(this.getJob_idDirtyFlag()){
			map.put("job_id",false);
		}
		if(this.getJob_id_text()!=null&&this.getJob_id_textDirtyFlag()){
			//忽略文本外键job_id_text
		}else if(this.getJob_id_textDirtyFlag()){
			map.put("job_id",false);
		}
		if(this.getJob_title()!=null&&this.getJob_titleDirtyFlag()){
			map.put("job_title",this.getJob_title());
		}else if(this.getJob_titleDirtyFlag()){
			map.put("job_title",false);
		}
		if(this.getKm_home_work()!=null&&this.getKm_home_workDirtyFlag()){
			map.put("km_home_work",this.getKm_home_work());
		}else if(this.getKm_home_workDirtyFlag()){
			map.put("km_home_work",false);
		}
		if(this.getLast_attendance_id()!=null&&this.getLast_attendance_idDirtyFlag()){
			map.put("last_attendance_id",this.getLast_attendance_id());
		}else if(this.getLast_attendance_idDirtyFlag()){
			map.put("last_attendance_id",false);
		}
		if(this.getLeaves_count()!=null&&this.getLeaves_countDirtyFlag()){
			map.put("leaves_count",this.getLeaves_count());
		}else if(this.getLeaves_countDirtyFlag()){
			map.put("leaves_count",false);
		}
		if(this.getLeave_date_from()!=null&&this.getLeave_date_fromDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			String datetimeStr = sdf.format(this.getLeave_date_from());
			map.put("leave_date_from",datetimeStr);
		}else if(this.getLeave_date_fromDirtyFlag()){
			map.put("leave_date_from",false);
		}
		if(this.getLeave_date_to()!=null&&this.getLeave_date_toDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			String datetimeStr = sdf.format(this.getLeave_date_to());
			map.put("leave_date_to",datetimeStr);
		}else if(this.getLeave_date_toDirtyFlag()){
			map.put("leave_date_to",false);
		}
		if(this.getManager()!=null&&this.getManagerDirtyFlag()){
			map.put("manager",Boolean.parseBoolean(this.getManager()));		
		}		if(this.getManual_attendance()!=null&&this.getManual_attendanceDirtyFlag()){
			map.put("manual_attendance",Boolean.parseBoolean(this.getManual_attendance()));		
		}		if(this.getMarital()!=null&&this.getMaritalDirtyFlag()){
			map.put("marital",this.getMarital());
		}else if(this.getMaritalDirtyFlag()){
			map.put("marital",false);
		}
		if(this.getMedic_exam()!=null&&this.getMedic_examDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			String datetimeStr = sdf.format(this.getMedic_exam());
			map.put("medic_exam",datetimeStr);
		}else if(this.getMedic_examDirtyFlag()){
			map.put("medic_exam",false);
		}
		if(this.getMessage_attachment_count()!=null&&this.getMessage_attachment_countDirtyFlag()){
			map.put("message_attachment_count",this.getMessage_attachment_count());
		}else if(this.getMessage_attachment_countDirtyFlag()){
			map.put("message_attachment_count",false);
		}
		if(this.getMessage_channel_ids()!=null&&this.getMessage_channel_idsDirtyFlag()){
			map.put("message_channel_ids",this.getMessage_channel_ids());
		}else if(this.getMessage_channel_idsDirtyFlag()){
			map.put("message_channel_ids",false);
		}
		if(this.getMessage_follower_ids()!=null&&this.getMessage_follower_idsDirtyFlag()){
			map.put("message_follower_ids",this.getMessage_follower_ids());
		}else if(this.getMessage_follower_idsDirtyFlag()){
			map.put("message_follower_ids",false);
		}
		if(this.getMessage_has_error()!=null&&this.getMessage_has_errorDirtyFlag()){
			map.put("message_has_error",Boolean.parseBoolean(this.getMessage_has_error()));		
		}		if(this.getMessage_has_error_counter()!=null&&this.getMessage_has_error_counterDirtyFlag()){
			map.put("message_has_error_counter",this.getMessage_has_error_counter());
		}else if(this.getMessage_has_error_counterDirtyFlag()){
			map.put("message_has_error_counter",false);
		}
		if(this.getMessage_ids()!=null&&this.getMessage_idsDirtyFlag()){
			map.put("message_ids",this.getMessage_ids());
		}else if(this.getMessage_idsDirtyFlag()){
			map.put("message_ids",false);
		}
		if(this.getMessage_is_follower()!=null&&this.getMessage_is_followerDirtyFlag()){
			map.put("message_is_follower",Boolean.parseBoolean(this.getMessage_is_follower()));		
		}		if(this.getMessage_main_attachment_id()!=null&&this.getMessage_main_attachment_idDirtyFlag()){
			map.put("message_main_attachment_id",this.getMessage_main_attachment_id());
		}else if(this.getMessage_main_attachment_idDirtyFlag()){
			map.put("message_main_attachment_id",false);
		}
		if(this.getMessage_needaction()!=null&&this.getMessage_needactionDirtyFlag()){
			map.put("message_needaction",Boolean.parseBoolean(this.getMessage_needaction()));		
		}		if(this.getMessage_needaction_counter()!=null&&this.getMessage_needaction_counterDirtyFlag()){
			map.put("message_needaction_counter",this.getMessage_needaction_counter());
		}else if(this.getMessage_needaction_counterDirtyFlag()){
			map.put("message_needaction_counter",false);
		}
		if(this.getMessage_partner_ids()!=null&&this.getMessage_partner_idsDirtyFlag()){
			map.put("message_partner_ids",this.getMessage_partner_ids());
		}else if(this.getMessage_partner_idsDirtyFlag()){
			map.put("message_partner_ids",false);
		}
		if(this.getMessage_unread()!=null&&this.getMessage_unreadDirtyFlag()){
			map.put("message_unread",Boolean.parseBoolean(this.getMessage_unread()));		
		}		if(this.getMessage_unread_counter()!=null&&this.getMessage_unread_counterDirtyFlag()){
			map.put("message_unread_counter",this.getMessage_unread_counter());
		}else if(this.getMessage_unread_counterDirtyFlag()){
			map.put("message_unread_counter",false);
		}
		if(this.getMobile_phone()!=null&&this.getMobile_phoneDirtyFlag()){
			map.put("mobile_phone",this.getMobile_phone());
		}else if(this.getMobile_phoneDirtyFlag()){
			map.put("mobile_phone",false);
		}
		if(this.getName()!=null&&this.getNameDirtyFlag()){
			map.put("name",this.getName());
		}else if(this.getNameDirtyFlag()){
			map.put("name",false);
		}
		if(this.getNewly_hired_employee()!=null&&this.getNewly_hired_employeeDirtyFlag()){
			map.put("newly_hired_employee",Boolean.parseBoolean(this.getNewly_hired_employee()));		
		}		if(this.getNotes()!=null&&this.getNotesDirtyFlag()){
			map.put("notes",this.getNotes());
		}else if(this.getNotesDirtyFlag()){
			map.put("notes",false);
		}
		if(this.getParent_id()!=null&&this.getParent_idDirtyFlag()){
			map.put("parent_id",this.getParent_id());
		}else if(this.getParent_idDirtyFlag()){
			map.put("parent_id",false);
		}
		if(this.getParent_id_text()!=null&&this.getParent_id_textDirtyFlag()){
			//忽略文本外键parent_id_text
		}else if(this.getParent_id_textDirtyFlag()){
			map.put("parent_id",false);
		}
		if(this.getPassport_id()!=null&&this.getPassport_idDirtyFlag()){
			map.put("passport_id",this.getPassport_id());
		}else if(this.getPassport_idDirtyFlag()){
			map.put("passport_id",false);
		}
		if(this.getPermit_no()!=null&&this.getPermit_noDirtyFlag()){
			map.put("permit_no",this.getPermit_no());
		}else if(this.getPermit_noDirtyFlag()){
			map.put("permit_no",false);
		}
		if(this.getPin()!=null&&this.getPinDirtyFlag()){
			map.put("pin",this.getPin());
		}else if(this.getPinDirtyFlag()){
			map.put("pin",false);
		}
		if(this.getPlace_of_birth()!=null&&this.getPlace_of_birthDirtyFlag()){
			map.put("place_of_birth",this.getPlace_of_birth());
		}else if(this.getPlace_of_birthDirtyFlag()){
			map.put("place_of_birth",false);
		}
		if(this.getRemaining_leaves()!=null&&this.getRemaining_leavesDirtyFlag()){
			map.put("remaining_leaves",this.getRemaining_leaves());
		}else if(this.getRemaining_leavesDirtyFlag()){
			map.put("remaining_leaves",false);
		}
		if(this.getResource_calendar_id()!=null&&this.getResource_calendar_idDirtyFlag()){
			map.put("resource_calendar_id",this.getResource_calendar_id());
		}else if(this.getResource_calendar_idDirtyFlag()){
			map.put("resource_calendar_id",false);
		}
		if(this.getResource_calendar_id_text()!=null&&this.getResource_calendar_id_textDirtyFlag()){
			//忽略文本外键resource_calendar_id_text
		}else if(this.getResource_calendar_id_textDirtyFlag()){
			map.put("resource_calendar_id",false);
		}
		if(this.getResource_id()!=null&&this.getResource_idDirtyFlag()){
			map.put("resource_id",this.getResource_id());
		}else if(this.getResource_idDirtyFlag()){
			map.put("resource_id",false);
		}
		if(this.getShow_leaves()!=null&&this.getShow_leavesDirtyFlag()){
			map.put("show_leaves",Boolean.parseBoolean(this.getShow_leaves()));		
		}		if(this.getSinid()!=null&&this.getSinidDirtyFlag()){
			map.put("sinid",this.getSinid());
		}else if(this.getSinidDirtyFlag()){
			map.put("sinid",false);
		}
		if(this.getSpouse_birthdate()!=null&&this.getSpouse_birthdateDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			String datetimeStr = sdf.format(this.getSpouse_birthdate());
			map.put("spouse_birthdate",datetimeStr);
		}else if(this.getSpouse_birthdateDirtyFlag()){
			map.put("spouse_birthdate",false);
		}
		if(this.getSpouse_complete_name()!=null&&this.getSpouse_complete_nameDirtyFlag()){
			map.put("spouse_complete_name",this.getSpouse_complete_name());
		}else if(this.getSpouse_complete_nameDirtyFlag()){
			map.put("spouse_complete_name",false);
		}
		if(this.getSsnid()!=null&&this.getSsnidDirtyFlag()){
			map.put("ssnid",this.getSsnid());
		}else if(this.getSsnidDirtyFlag()){
			map.put("ssnid",false);
		}
		if(this.getStudy_field()!=null&&this.getStudy_fieldDirtyFlag()){
			map.put("study_field",this.getStudy_field());
		}else if(this.getStudy_fieldDirtyFlag()){
			map.put("study_field",false);
		}
		if(this.getStudy_school()!=null&&this.getStudy_schoolDirtyFlag()){
			map.put("study_school",this.getStudy_school());
		}else if(this.getStudy_schoolDirtyFlag()){
			map.put("study_school",false);
		}
		if(this.getTz()!=null&&this.getTzDirtyFlag()){
			map.put("tz",this.getTz());
		}else if(this.getTzDirtyFlag()){
			map.put("tz",false);
		}
		if(this.getUser_id()!=null&&this.getUser_idDirtyFlag()){
			map.put("user_id",this.getUser_id());
		}else if(this.getUser_idDirtyFlag()){
			map.put("user_id",false);
		}
		if(this.getUser_id_text()!=null&&this.getUser_id_textDirtyFlag()){
			//忽略文本外键user_id_text
		}else if(this.getUser_id_textDirtyFlag()){
			map.put("user_id",false);
		}
		if(this.getVehicle()!=null&&this.getVehicleDirtyFlag()){
			map.put("vehicle",this.getVehicle());
		}else if(this.getVehicleDirtyFlag()){
			map.put("vehicle",false);
		}
		if(this.getVisa_expire()!=null&&this.getVisa_expireDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			String datetimeStr = sdf.format(this.getVisa_expire());
			map.put("visa_expire",datetimeStr);
		}else if(this.getVisa_expireDirtyFlag()){
			map.put("visa_expire",false);
		}
		if(this.getVisa_no()!=null&&this.getVisa_noDirtyFlag()){
			map.put("visa_no",this.getVisa_no());
		}else if(this.getVisa_noDirtyFlag()){
			map.put("visa_no",false);
		}
		if(this.getWebsite_message_ids()!=null&&this.getWebsite_message_idsDirtyFlag()){
			map.put("website_message_ids",this.getWebsite_message_ids());
		}else if(this.getWebsite_message_idsDirtyFlag()){
			map.put("website_message_ids",false);
		}
		if(this.getWork_email()!=null&&this.getWork_emailDirtyFlag()){
			map.put("work_email",this.getWork_email());
		}else if(this.getWork_emailDirtyFlag()){
			map.put("work_email",false);
		}
		if(this.getWork_location()!=null&&this.getWork_locationDirtyFlag()){
			map.put("work_location",this.getWork_location());
		}else if(this.getWork_locationDirtyFlag()){
			map.put("work_location",false);
		}
		if(this.getWork_phone()!=null&&this.getWork_phoneDirtyFlag()){
			map.put("work_phone",this.getWork_phone());
		}else if(this.getWork_phoneDirtyFlag()){
			map.put("work_phone",false);
		}
		if(this.getWrite_date()!=null&&this.getWrite_dateDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String datetimeStr = sdf.format(this.getWrite_date());
			map.put("write_date",datetimeStr);
		}else if(this.getWrite_dateDirtyFlag()){
			map.put("write_date",false);
		}
		if(this.getWrite_uid()!=null&&this.getWrite_uidDirtyFlag()){
			map.put("write_uid",this.getWrite_uid());
		}else if(this.getWrite_uidDirtyFlag()){
			map.put("write_uid",false);
		}
		if(this.getWrite_uid_text()!=null&&this.getWrite_uid_textDirtyFlag()){
			//忽略文本外键write_uid_text
		}else if(this.getWrite_uid_textDirtyFlag()){
			map.put("write_uid",false);
		}
		if(this.get__last_update()!=null&&this.get__last_updateDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String datetimeStr = sdf.format(this.get__last_update());
			map.put("__last_update",datetimeStr);
		}else if(this.get__last_updateDirtyFlag()){
			map.put("__last_update",false);
		}
		return map;
	}

}
