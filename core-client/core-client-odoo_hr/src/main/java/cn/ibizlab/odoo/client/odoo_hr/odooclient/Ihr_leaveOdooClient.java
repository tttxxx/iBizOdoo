package cn.ibizlab.odoo.client.odoo_hr.odooclient;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.core.client.model.Ihr_leave;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[hr_leave] 服务对象客户端接口
 */
public interface Ihr_leaveOdooClient {
    
        public void createBatch(Ihr_leave hr_leave);

        public void remove(Ihr_leave hr_leave);

        public void removeBatch(Ihr_leave hr_leave);

        public void updateBatch(Ihr_leave hr_leave);

        public void create(Ihr_leave hr_leave);

        public Page<Ihr_leave> search(SearchContext context);

        public void get(Ihr_leave hr_leave);

        public void update(Ihr_leave hr_leave);

        public List<Ihr_leave> select();


}