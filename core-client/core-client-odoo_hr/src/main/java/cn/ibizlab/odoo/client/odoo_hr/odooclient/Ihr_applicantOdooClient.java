package cn.ibizlab.odoo.client.odoo_hr.odooclient;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.core.client.model.Ihr_applicant;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[hr_applicant] 服务对象客户端接口
 */
public interface Ihr_applicantOdooClient {
    
        public void get(Ihr_applicant hr_applicant);

        public void createBatch(Ihr_applicant hr_applicant);

        public void create(Ihr_applicant hr_applicant);

        public void remove(Ihr_applicant hr_applicant);

        public Page<Ihr_applicant> search(SearchContext context);

        public void updateBatch(Ihr_applicant hr_applicant);

        public void update(Ihr_applicant hr_applicant);

        public void removeBatch(Ihr_applicant hr_applicant);

        public List<Ihr_applicant> select();


}