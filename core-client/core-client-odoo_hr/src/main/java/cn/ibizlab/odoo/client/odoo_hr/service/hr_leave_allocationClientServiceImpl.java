package cn.ibizlab.odoo.client.odoo_hr.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Ihr_leave_allocation;
import cn.ibizlab.odoo.core.client.service.Ihr_leave_allocationClientService;
import cn.ibizlab.odoo.client.odoo_hr.model.hr_leave_allocationImpl;
import cn.ibizlab.odoo.client.odoo_hr.odooclient.Ihr_leave_allocationOdooClient;
import cn.ibizlab.odoo.client.odoo_hr.odooclient.impl.hr_leave_allocationOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[hr_leave_allocation] 服务对象接口
 */
@Service
public class hr_leave_allocationClientServiceImpl implements Ihr_leave_allocationClientService {
    @Autowired
    private  Ihr_leave_allocationOdooClient  hr_leave_allocationOdooClient;

    public Ihr_leave_allocation createModel() {		
		return new hr_leave_allocationImpl();
	}


        public void removeBatch(List<Ihr_leave_allocation> hr_leave_allocations){
            
        }
        
        public void remove(Ihr_leave_allocation hr_leave_allocation){
this.hr_leave_allocationOdooClient.remove(hr_leave_allocation) ;
        }
        
        public void get(Ihr_leave_allocation hr_leave_allocation){
            this.hr_leave_allocationOdooClient.get(hr_leave_allocation) ;
        }
        
        public void updateBatch(List<Ihr_leave_allocation> hr_leave_allocations){
            
        }
        
        public void update(Ihr_leave_allocation hr_leave_allocation){
this.hr_leave_allocationOdooClient.update(hr_leave_allocation) ;
        }
        
        public void create(Ihr_leave_allocation hr_leave_allocation){
this.hr_leave_allocationOdooClient.create(hr_leave_allocation) ;
        }
        
        public void createBatch(List<Ihr_leave_allocation> hr_leave_allocations){
            
        }
        
        public Page<Ihr_leave_allocation> search(SearchContext context){
            return this.hr_leave_allocationOdooClient.search(context) ;
        }
        
        public Page<Ihr_leave_allocation> select(SearchContext context){
            return null ;
        }
        

}

