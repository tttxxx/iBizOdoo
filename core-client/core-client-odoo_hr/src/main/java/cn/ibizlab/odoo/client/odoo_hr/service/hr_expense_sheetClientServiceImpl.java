package cn.ibizlab.odoo.client.odoo_hr.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Ihr_expense_sheet;
import cn.ibizlab.odoo.core.client.service.Ihr_expense_sheetClientService;
import cn.ibizlab.odoo.client.odoo_hr.model.hr_expense_sheetImpl;
import cn.ibizlab.odoo.client.odoo_hr.odooclient.Ihr_expense_sheetOdooClient;
import cn.ibizlab.odoo.client.odoo_hr.odooclient.impl.hr_expense_sheetOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[hr_expense_sheet] 服务对象接口
 */
@Service
public class hr_expense_sheetClientServiceImpl implements Ihr_expense_sheetClientService {
    @Autowired
    private  Ihr_expense_sheetOdooClient  hr_expense_sheetOdooClient;

    public Ihr_expense_sheet createModel() {		
		return new hr_expense_sheetImpl();
	}


        public void create(Ihr_expense_sheet hr_expense_sheet){
this.hr_expense_sheetOdooClient.create(hr_expense_sheet) ;
        }
        
        public Page<Ihr_expense_sheet> search(SearchContext context){
            return this.hr_expense_sheetOdooClient.search(context) ;
        }
        
        public void createBatch(List<Ihr_expense_sheet> hr_expense_sheets){
            
        }
        
        public void updateBatch(List<Ihr_expense_sheet> hr_expense_sheets){
            
        }
        
        public void update(Ihr_expense_sheet hr_expense_sheet){
this.hr_expense_sheetOdooClient.update(hr_expense_sheet) ;
        }
        
        public void get(Ihr_expense_sheet hr_expense_sheet){
            this.hr_expense_sheetOdooClient.get(hr_expense_sheet) ;
        }
        
        public void remove(Ihr_expense_sheet hr_expense_sheet){
this.hr_expense_sheetOdooClient.remove(hr_expense_sheet) ;
        }
        
        public void removeBatch(List<Ihr_expense_sheet> hr_expense_sheets){
            
        }
        
        public Page<Ihr_expense_sheet> select(SearchContext context){
            return null ;
        }
        

}

