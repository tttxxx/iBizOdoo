package cn.ibizlab.odoo.client.odoo_hr.odooclient;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.core.client.model.Ihr_recruitment_degree;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[hr_recruitment_degree] 服务对象客户端接口
 */
public interface Ihr_recruitment_degreeOdooClient {
    
        public Page<Ihr_recruitment_degree> search(SearchContext context);

        public void create(Ihr_recruitment_degree hr_recruitment_degree);

        public void updateBatch(Ihr_recruitment_degree hr_recruitment_degree);

        public void removeBatch(Ihr_recruitment_degree hr_recruitment_degree);

        public void remove(Ihr_recruitment_degree hr_recruitment_degree);

        public void update(Ihr_recruitment_degree hr_recruitment_degree);

        public void get(Ihr_recruitment_degree hr_recruitment_degree);

        public void createBatch(Ihr_recruitment_degree hr_recruitment_degree);

        public List<Ihr_recruitment_degree> select();


}