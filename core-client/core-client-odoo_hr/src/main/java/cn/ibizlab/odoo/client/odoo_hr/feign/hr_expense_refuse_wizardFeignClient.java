package cn.ibizlab.odoo.client.odoo_hr.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Ihr_expense_refuse_wizard;
import cn.ibizlab.odoo.client.odoo_hr.model.hr_expense_refuse_wizardImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[hr_expense_refuse_wizard] 服务对象接口
 */
public interface hr_expense_refuse_wizardFeignClient {

    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_hr/hr_expense_refuse_wizards/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_hr/hr_expense_refuse_wizards/createbatch")
    public hr_expense_refuse_wizardImpl createBatch(@RequestBody List<hr_expense_refuse_wizardImpl> hr_expense_refuse_wizards);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_hr/hr_expense_refuse_wizards/search")
    public Page<hr_expense_refuse_wizardImpl> search(SearchContext context);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_hr/hr_expense_refuse_wizards/{id}")
    public hr_expense_refuse_wizardImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_hr/hr_expense_refuse_wizards/updatebatch")
    public hr_expense_refuse_wizardImpl updateBatch(@RequestBody List<hr_expense_refuse_wizardImpl> hr_expense_refuse_wizards);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_hr/hr_expense_refuse_wizards/removebatch")
    public hr_expense_refuse_wizardImpl removeBatch(@RequestBody List<hr_expense_refuse_wizardImpl> hr_expense_refuse_wizards);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_hr/hr_expense_refuse_wizards/{id}")
    public hr_expense_refuse_wizardImpl update(@PathVariable("id") Integer id,@RequestBody hr_expense_refuse_wizardImpl hr_expense_refuse_wizard);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_hr/hr_expense_refuse_wizards")
    public hr_expense_refuse_wizardImpl create(@RequestBody hr_expense_refuse_wizardImpl hr_expense_refuse_wizard);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_hr/hr_expense_refuse_wizards/select")
    public Page<hr_expense_refuse_wizardImpl> select();



}
