package cn.ibizlab.odoo.client.odoo_hr.odooclient;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.core.client.model.Ihr_expense;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[hr_expense] 服务对象客户端接口
 */
public interface Ihr_expenseOdooClient {
    
        public void removeBatch(Ihr_expense hr_expense);

        public void create(Ihr_expense hr_expense);

        public void get(Ihr_expense hr_expense);

        public void createBatch(Ihr_expense hr_expense);

        public void updateBatch(Ihr_expense hr_expense);

        public Page<Ihr_expense> search(SearchContext context);

        public void remove(Ihr_expense hr_expense);

        public void update(Ihr_expense hr_expense);

        public List<Ihr_expense> select();


}