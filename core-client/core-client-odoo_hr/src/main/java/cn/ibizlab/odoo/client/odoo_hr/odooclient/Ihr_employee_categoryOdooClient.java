package cn.ibizlab.odoo.client.odoo_hr.odooclient;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.core.client.model.Ihr_employee_category;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[hr_employee_category] 服务对象客户端接口
 */
public interface Ihr_employee_categoryOdooClient {
    
        public void updateBatch(Ihr_employee_category hr_employee_category);

        public Page<Ihr_employee_category> search(SearchContext context);

        public void get(Ihr_employee_category hr_employee_category);

        public void createBatch(Ihr_employee_category hr_employee_category);

        public void update(Ihr_employee_category hr_employee_category);

        public void removeBatch(Ihr_employee_category hr_employee_category);

        public void create(Ihr_employee_category hr_employee_category);

        public void remove(Ihr_employee_category hr_employee_category);

        public List<Ihr_employee_category> select();


}