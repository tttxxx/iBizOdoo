package cn.ibizlab.odoo.client.odoo_hr.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Ihr_applicant_category;
import cn.ibizlab.odoo.core.client.service.Ihr_applicant_categoryClientService;
import cn.ibizlab.odoo.client.odoo_hr.model.hr_applicant_categoryImpl;
import cn.ibizlab.odoo.client.odoo_hr.odooclient.Ihr_applicant_categoryOdooClient;
import cn.ibizlab.odoo.client.odoo_hr.odooclient.impl.hr_applicant_categoryOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[hr_applicant_category] 服务对象接口
 */
@Service
public class hr_applicant_categoryClientServiceImpl implements Ihr_applicant_categoryClientService {
    @Autowired
    private  Ihr_applicant_categoryOdooClient  hr_applicant_categoryOdooClient;

    public Ihr_applicant_category createModel() {		
		return new hr_applicant_categoryImpl();
	}


        public void updateBatch(List<Ihr_applicant_category> hr_applicant_categories){
            
        }
        
        public Page<Ihr_applicant_category> search(SearchContext context){
            return this.hr_applicant_categoryOdooClient.search(context) ;
        }
        
        public void get(Ihr_applicant_category hr_applicant_category){
            this.hr_applicant_categoryOdooClient.get(hr_applicant_category) ;
        }
        
        public void create(Ihr_applicant_category hr_applicant_category){
this.hr_applicant_categoryOdooClient.create(hr_applicant_category) ;
        }
        
        public void removeBatch(List<Ihr_applicant_category> hr_applicant_categories){
            
        }
        
        public void remove(Ihr_applicant_category hr_applicant_category){
this.hr_applicant_categoryOdooClient.remove(hr_applicant_category) ;
        }
        
        public void update(Ihr_applicant_category hr_applicant_category){
this.hr_applicant_categoryOdooClient.update(hr_applicant_category) ;
        }
        
        public void createBatch(List<Ihr_applicant_category> hr_applicant_categories){
            
        }
        
        public Page<Ihr_applicant_category> select(SearchContext context){
            return null ;
        }
        

}

