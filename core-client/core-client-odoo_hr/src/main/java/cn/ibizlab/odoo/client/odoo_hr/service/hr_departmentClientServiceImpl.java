package cn.ibizlab.odoo.client.odoo_hr.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Ihr_department;
import cn.ibizlab.odoo.core.client.service.Ihr_departmentClientService;
import cn.ibizlab.odoo.client.odoo_hr.model.hr_departmentImpl;
import cn.ibizlab.odoo.client.odoo_hr.odooclient.Ihr_departmentOdooClient;
import cn.ibizlab.odoo.client.odoo_hr.odooclient.impl.hr_departmentOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[hr_department] 服务对象接口
 */
@Service
public class hr_departmentClientServiceImpl implements Ihr_departmentClientService {
    @Autowired
    private  Ihr_departmentOdooClient  hr_departmentOdooClient;

    public Ihr_department createModel() {		
		return new hr_departmentImpl();
	}


        public void remove(Ihr_department hr_department){
this.hr_departmentOdooClient.remove(hr_department) ;
        }
        
        public Page<Ihr_department> search(SearchContext context){
            return this.hr_departmentOdooClient.search(context) ;
        }
        
        public void get(Ihr_department hr_department){
            this.hr_departmentOdooClient.get(hr_department) ;
        }
        
        public void update(Ihr_department hr_department){
this.hr_departmentOdooClient.update(hr_department) ;
        }
        
        public void removeBatch(List<Ihr_department> hr_departments){
            
        }
        
        public void createBatch(List<Ihr_department> hr_departments){
            
        }
        
        public void create(Ihr_department hr_department){
this.hr_departmentOdooClient.create(hr_department) ;
        }
        
        public void updateBatch(List<Ihr_department> hr_departments){
            
        }
        
        public Page<Ihr_department> select(SearchContext context){
            return null ;
        }
        

}

