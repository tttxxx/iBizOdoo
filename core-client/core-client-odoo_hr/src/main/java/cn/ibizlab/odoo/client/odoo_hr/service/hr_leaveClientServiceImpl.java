package cn.ibizlab.odoo.client.odoo_hr.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Ihr_leave;
import cn.ibizlab.odoo.core.client.service.Ihr_leaveClientService;
import cn.ibizlab.odoo.client.odoo_hr.model.hr_leaveImpl;
import cn.ibizlab.odoo.client.odoo_hr.odooclient.Ihr_leaveOdooClient;
import cn.ibizlab.odoo.client.odoo_hr.odooclient.impl.hr_leaveOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[hr_leave] 服务对象接口
 */
@Service
public class hr_leaveClientServiceImpl implements Ihr_leaveClientService {
    @Autowired
    private  Ihr_leaveOdooClient  hr_leaveOdooClient;

    public Ihr_leave createModel() {		
		return new hr_leaveImpl();
	}


        public void createBatch(List<Ihr_leave> hr_leaves){
            
        }
        
        public void remove(Ihr_leave hr_leave){
this.hr_leaveOdooClient.remove(hr_leave) ;
        }
        
        public void removeBatch(List<Ihr_leave> hr_leaves){
            
        }
        
        public void updateBatch(List<Ihr_leave> hr_leaves){
            
        }
        
        public void create(Ihr_leave hr_leave){
this.hr_leaveOdooClient.create(hr_leave) ;
        }
        
        public Page<Ihr_leave> search(SearchContext context){
            return this.hr_leaveOdooClient.search(context) ;
        }
        
        public void get(Ihr_leave hr_leave){
            this.hr_leaveOdooClient.get(hr_leave) ;
        }
        
        public void update(Ihr_leave hr_leave){
this.hr_leaveOdooClient.update(hr_leave) ;
        }
        
        public Page<Ihr_leave> select(SearchContext context){
            return null ;
        }
        

}

