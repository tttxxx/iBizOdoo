package cn.ibizlab.odoo.client.odoo_hr.odooclient;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.core.client.model.Ihr_leave_report;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[hr_leave_report] 服务对象客户端接口
 */
public interface Ihr_leave_reportOdooClient {
    
        public void remove(Ihr_leave_report hr_leave_report);

        public void update(Ihr_leave_report hr_leave_report);

        public void removeBatch(Ihr_leave_report hr_leave_report);

        public Page<Ihr_leave_report> search(SearchContext context);

        public void createBatch(Ihr_leave_report hr_leave_report);

        public void create(Ihr_leave_report hr_leave_report);

        public void get(Ihr_leave_report hr_leave_report);

        public void updateBatch(Ihr_leave_report hr_leave_report);

        public List<Ihr_leave_report> select();


}