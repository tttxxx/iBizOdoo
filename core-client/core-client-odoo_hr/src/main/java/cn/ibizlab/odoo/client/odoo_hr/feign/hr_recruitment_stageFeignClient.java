package cn.ibizlab.odoo.client.odoo_hr.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Ihr_recruitment_stage;
import cn.ibizlab.odoo.client.odoo_hr.model.hr_recruitment_stageImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[hr_recruitment_stage] 服务对象接口
 */
public interface hr_recruitment_stageFeignClient {

    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_hr/hr_recruitment_stages/updatebatch")
    public hr_recruitment_stageImpl updateBatch(@RequestBody List<hr_recruitment_stageImpl> hr_recruitment_stages);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_hr/hr_recruitment_stages/search")
    public Page<hr_recruitment_stageImpl> search(SearchContext context);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_hr/hr_recruitment_stages")
    public hr_recruitment_stageImpl create(@RequestBody hr_recruitment_stageImpl hr_recruitment_stage);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_hr/hr_recruitment_stages/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_hr/hr_recruitment_stages/{id}")
    public hr_recruitment_stageImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_hr/hr_recruitment_stages/createbatch")
    public hr_recruitment_stageImpl createBatch(@RequestBody List<hr_recruitment_stageImpl> hr_recruitment_stages);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_hr/hr_recruitment_stages/{id}")
    public hr_recruitment_stageImpl update(@PathVariable("id") Integer id,@RequestBody hr_recruitment_stageImpl hr_recruitment_stage);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_hr/hr_recruitment_stages/removebatch")
    public hr_recruitment_stageImpl removeBatch(@RequestBody List<hr_recruitment_stageImpl> hr_recruitment_stages);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_hr/hr_recruitment_stages/select")
    public Page<hr_recruitment_stageImpl> select();



}
