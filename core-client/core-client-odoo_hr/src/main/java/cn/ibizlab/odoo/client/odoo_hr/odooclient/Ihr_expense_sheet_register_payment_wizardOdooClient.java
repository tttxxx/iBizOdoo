package cn.ibizlab.odoo.client.odoo_hr.odooclient;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.core.client.model.Ihr_expense_sheet_register_payment_wizard;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[hr_expense_sheet_register_payment_wizard] 服务对象客户端接口
 */
public interface Ihr_expense_sheet_register_payment_wizardOdooClient {
    
        public void updateBatch(Ihr_expense_sheet_register_payment_wizard hr_expense_sheet_register_payment_wizard);

        public void update(Ihr_expense_sheet_register_payment_wizard hr_expense_sheet_register_payment_wizard);

        public void removeBatch(Ihr_expense_sheet_register_payment_wizard hr_expense_sheet_register_payment_wizard);

        public void create(Ihr_expense_sheet_register_payment_wizard hr_expense_sheet_register_payment_wizard);

        public Page<Ihr_expense_sheet_register_payment_wizard> search(SearchContext context);

        public void remove(Ihr_expense_sheet_register_payment_wizard hr_expense_sheet_register_payment_wizard);

        public void createBatch(Ihr_expense_sheet_register_payment_wizard hr_expense_sheet_register_payment_wizard);

        public void get(Ihr_expense_sheet_register_payment_wizard hr_expense_sheet_register_payment_wizard);

        public List<Ihr_expense_sheet_register_payment_wizard> select();


}