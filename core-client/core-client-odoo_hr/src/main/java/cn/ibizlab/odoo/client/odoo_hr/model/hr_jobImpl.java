package cn.ibizlab.odoo.client.odoo_hr.model;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.math.BigInteger;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.core.client.model.Ihr_job;
import cn.ibizlab.odoo.util.helper.OdooClientHelper;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang.ArrayUtils;
import org.springframework.util.StringUtils;

/**
 * 接口实体[hr_job] 对象
 */
public class hr_jobImpl implements Ihr_job,Serializable{

    /**
     * 工作地点
     */
    public Integer address_id;

    @JsonIgnore
    public boolean address_idDirtyFlag;
    
    /**
     * 工作地点
     */
    public String address_id_text;

    @JsonIgnore
    public boolean address_id_textDirtyFlag;
    
    /**
     * 别名联系人安全
     */
    public String alias_contact;

    @JsonIgnore
    public boolean alias_contactDirtyFlag;
    
    /**
     * 默认值
     */
    public String alias_defaults;

    @JsonIgnore
    public boolean alias_defaultsDirtyFlag;
    
    /**
     * 网域别名
     */
    public String alias_domain;

    @JsonIgnore
    public boolean alias_domainDirtyFlag;
    
    /**
     * 记录线索ID
     */
    public Integer alias_force_thread_id;

    @JsonIgnore
    public boolean alias_force_thread_idDirtyFlag;
    
    /**
     * 别名
     */
    public Integer alias_id;

    @JsonIgnore
    public boolean alias_idDirtyFlag;
    
    /**
     * 模型别名
     */
    public Integer alias_model_id;

    @JsonIgnore
    public boolean alias_model_idDirtyFlag;
    
    /**
     * 别名
     */
    public String alias_name;

    @JsonIgnore
    public boolean alias_nameDirtyFlag;
    
    /**
     * 上级模型
     */
    public Integer alias_parent_model_id;

    @JsonIgnore
    public boolean alias_parent_model_idDirtyFlag;
    
    /**
     * 上级记录ID
     */
    public Integer alias_parent_thread_id;

    @JsonIgnore
    public boolean alias_parent_thread_idDirtyFlag;
    
    /**
     * 所有者
     */
    public Integer alias_user_id;

    @JsonIgnore
    public boolean alias_user_idDirtyFlag;
    
    /**
     * 应用数量
     */
    public Integer application_count;

    @JsonIgnore
    public boolean application_countDirtyFlag;
    
    /**
     * 求职申请
     */
    public String application_ids;

    @JsonIgnore
    public boolean application_idsDirtyFlag;
    
    /**
     * 颜色索引
     */
    public Integer color;

    @JsonIgnore
    public boolean colorDirtyFlag;
    
    /**
     * 公司
     */
    public Integer company_id;

    @JsonIgnore
    public boolean company_idDirtyFlag;
    
    /**
     * 公司
     */
    public String company_id_text;

    @JsonIgnore
    public boolean company_id_textDirtyFlag;
    
    /**
     * 创建时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp create_date;

    @JsonIgnore
    public boolean create_dateDirtyFlag;
    
    /**
     * 创建人
     */
    public Integer create_uid;

    @JsonIgnore
    public boolean create_uidDirtyFlag;
    
    /**
     * 创建人
     */
    public String create_uid_text;

    @JsonIgnore
    public boolean create_uid_textDirtyFlag;
    
    /**
     * 部门
     */
    public Integer department_id;

    @JsonIgnore
    public boolean department_idDirtyFlag;
    
    /**
     * 部门
     */
    public String department_id_text;

    @JsonIgnore
    public boolean department_id_textDirtyFlag;
    
    /**
     * 工作说明
     */
    public String description;

    @JsonIgnore
    public boolean descriptionDirtyFlag;
    
    /**
     * 显示名称
     */
    public String display_name;

    @JsonIgnore
    public boolean display_nameDirtyFlag;
    
    /**
     * 文档数
     */
    public Integer documents_count;

    @JsonIgnore
    public boolean documents_countDirtyFlag;
    
    /**
     * 文档
     */
    public String document_ids;

    @JsonIgnore
    public boolean document_idsDirtyFlag;
    
    /**
     * 员工
     */
    public String employee_ids;

    @JsonIgnore
    public boolean employee_idsDirtyFlag;
    
    /**
     * 预计员工数合计
     */
    public Integer expected_employees;

    @JsonIgnore
    public boolean expected_employeesDirtyFlag;
    
    /**
     * 人力资源主管
     */
    public Integer hr_responsible_id;

    @JsonIgnore
    public boolean hr_responsible_idDirtyFlag;
    
    /**
     * 人力资源主管
     */
    public String hr_responsible_id_text;

    @JsonIgnore
    public boolean hr_responsible_id_textDirtyFlag;
    
    /**
     * ID
     */
    public Integer id;

    @JsonIgnore
    public boolean idDirtyFlag;
    
    /**
     * 已发布
     */
    public String is_published;

    @JsonIgnore
    public boolean is_publishedDirtyFlag;
    
    /**
     * SEO优化
     */
    public String is_seo_optimized;

    @JsonIgnore
    public boolean is_seo_optimizedDirtyFlag;
    
    /**
     * 部门经理
     */
    public Integer manager_id;

    @JsonIgnore
    public boolean manager_idDirtyFlag;
    
    /**
     * 部门经理
     */
    public String manager_id_text;

    @JsonIgnore
    public boolean manager_id_textDirtyFlag;
    
    /**
     * 附件数量
     */
    public Integer message_attachment_count;

    @JsonIgnore
    public boolean message_attachment_countDirtyFlag;
    
    /**
     * 关注者(渠道)
     */
    public String message_channel_ids;

    @JsonIgnore
    public boolean message_channel_idsDirtyFlag;
    
    /**
     * 关注者
     */
    public String message_follower_ids;

    @JsonIgnore
    public boolean message_follower_idsDirtyFlag;
    
    /**
     * 消息递送错误
     */
    public String message_has_error;

    @JsonIgnore
    public boolean message_has_errorDirtyFlag;
    
    /**
     * 错误数
     */
    public Integer message_has_error_counter;

    @JsonIgnore
    public boolean message_has_error_counterDirtyFlag;
    
    /**
     * 信息
     */
    public String message_ids;

    @JsonIgnore
    public boolean message_idsDirtyFlag;
    
    /**
     * 是关注者
     */
    public String message_is_follower;

    @JsonIgnore
    public boolean message_is_followerDirtyFlag;
    
    /**
     * 附件
     */
    public Integer message_main_attachment_id;

    @JsonIgnore
    public boolean message_main_attachment_idDirtyFlag;
    
    /**
     * 需要采取行动
     */
    public String message_needaction;

    @JsonIgnore
    public boolean message_needactionDirtyFlag;
    
    /**
     * 行动数量
     */
    public Integer message_needaction_counter;

    @JsonIgnore
    public boolean message_needaction_counterDirtyFlag;
    
    /**
     * 关注者(业务伙伴)
     */
    public String message_partner_ids;

    @JsonIgnore
    public boolean message_partner_idsDirtyFlag;
    
    /**
     * 未读消息
     */
    public String message_unread;

    @JsonIgnore
    public boolean message_unreadDirtyFlag;
    
    /**
     * 未读消息计数器
     */
    public Integer message_unread_counter;

    @JsonIgnore
    public boolean message_unread_counterDirtyFlag;
    
    /**
     * 工作岗位
     */
    public String name;

    @JsonIgnore
    public boolean nameDirtyFlag;
    
    /**
     * 当前员工数量
     */
    public Integer no_of_employee;

    @JsonIgnore
    public boolean no_of_employeeDirtyFlag;
    
    /**
     * 已雇用员工
     */
    public Integer no_of_hired_employee;

    @JsonIgnore
    public boolean no_of_hired_employeeDirtyFlag;
    
    /**
     * 期望的新员工
     */
    public Integer no_of_recruitment;

    @JsonIgnore
    public boolean no_of_recruitmentDirtyFlag;
    
    /**
     * 要求
     */
    public String requirements;

    @JsonIgnore
    public boolean requirementsDirtyFlag;
    
    /**
     * 状态
     */
    public String state;

    @JsonIgnore
    public boolean stateDirtyFlag;
    
    /**
     * 招聘负责人
     */
    public Integer user_id;

    @JsonIgnore
    public boolean user_idDirtyFlag;
    
    /**
     * 招聘负责人
     */
    public String user_id_text;

    @JsonIgnore
    public boolean user_id_textDirtyFlag;
    
    /**
     * 网站说明
     */
    public String website_description;

    @JsonIgnore
    public boolean website_descriptionDirtyFlag;
    
    /**
     * 网站
     */
    public Integer website_id;

    @JsonIgnore
    public boolean website_idDirtyFlag;
    
    /**
     * 网站消息
     */
    public String website_message_ids;

    @JsonIgnore
    public boolean website_message_idsDirtyFlag;
    
    /**
     * 网站元说明
     */
    public String website_meta_description;

    @JsonIgnore
    public boolean website_meta_descriptionDirtyFlag;
    
    /**
     * 网站meta关键词
     */
    public String website_meta_keywords;

    @JsonIgnore
    public boolean website_meta_keywordsDirtyFlag;
    
    /**
     * 网站opengraph图像
     */
    public String website_meta_og_img;

    @JsonIgnore
    public boolean website_meta_og_imgDirtyFlag;
    
    /**
     * 网站meta标题
     */
    public String website_meta_title;

    @JsonIgnore
    public boolean website_meta_titleDirtyFlag;
    
    /**
     * 在当前网站显示
     */
    public String website_published;

    @JsonIgnore
    public boolean website_publishedDirtyFlag;
    
    /**
     * 网站网址
     */
    public String website_url;

    @JsonIgnore
    public boolean website_urlDirtyFlag;
    
    /**
     * 最后更新时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp write_date;

    @JsonIgnore
    public boolean write_dateDirtyFlag;
    
    /**
     * 最后更新人
     */
    public Integer write_uid;

    @JsonIgnore
    public boolean write_uidDirtyFlag;
    
    /**
     * 最后更新人
     */
    public String write_uid_text;

    @JsonIgnore
    public boolean write_uid_textDirtyFlag;
    
    /**
     * 最后修改日
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp __last_update;

    @JsonIgnore
    public boolean __last_updateDirtyFlag;
    
    /**
     * 获取 [工作地点]
     */
    @JsonProperty("address_id")
    public Integer getAddress_id(){
        return this.address_id ;
    }

    /**
     * 设置 [工作地点]
     */
    @JsonProperty("address_id")
    public void setAddress_id(Integer  address_id){
        this.address_id = address_id ;
        this.address_idDirtyFlag = true ;
    }

     /**
     * 获取 [工作地点]脏标记
     */
    @JsonIgnore
    public boolean getAddress_idDirtyFlag(){
        return this.address_idDirtyFlag ;
    }   

    /**
     * 获取 [工作地点]
     */
    @JsonProperty("address_id_text")
    public String getAddress_id_text(){
        return this.address_id_text ;
    }

    /**
     * 设置 [工作地点]
     */
    @JsonProperty("address_id_text")
    public void setAddress_id_text(String  address_id_text){
        this.address_id_text = address_id_text ;
        this.address_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [工作地点]脏标记
     */
    @JsonIgnore
    public boolean getAddress_id_textDirtyFlag(){
        return this.address_id_textDirtyFlag ;
    }   

    /**
     * 获取 [别名联系人安全]
     */
    @JsonProperty("alias_contact")
    public String getAlias_contact(){
        return this.alias_contact ;
    }

    /**
     * 设置 [别名联系人安全]
     */
    @JsonProperty("alias_contact")
    public void setAlias_contact(String  alias_contact){
        this.alias_contact = alias_contact ;
        this.alias_contactDirtyFlag = true ;
    }

     /**
     * 获取 [别名联系人安全]脏标记
     */
    @JsonIgnore
    public boolean getAlias_contactDirtyFlag(){
        return this.alias_contactDirtyFlag ;
    }   

    /**
     * 获取 [默认值]
     */
    @JsonProperty("alias_defaults")
    public String getAlias_defaults(){
        return this.alias_defaults ;
    }

    /**
     * 设置 [默认值]
     */
    @JsonProperty("alias_defaults")
    public void setAlias_defaults(String  alias_defaults){
        this.alias_defaults = alias_defaults ;
        this.alias_defaultsDirtyFlag = true ;
    }

     /**
     * 获取 [默认值]脏标记
     */
    @JsonIgnore
    public boolean getAlias_defaultsDirtyFlag(){
        return this.alias_defaultsDirtyFlag ;
    }   

    /**
     * 获取 [网域别名]
     */
    @JsonProperty("alias_domain")
    public String getAlias_domain(){
        return this.alias_domain ;
    }

    /**
     * 设置 [网域别名]
     */
    @JsonProperty("alias_domain")
    public void setAlias_domain(String  alias_domain){
        this.alias_domain = alias_domain ;
        this.alias_domainDirtyFlag = true ;
    }

     /**
     * 获取 [网域别名]脏标记
     */
    @JsonIgnore
    public boolean getAlias_domainDirtyFlag(){
        return this.alias_domainDirtyFlag ;
    }   

    /**
     * 获取 [记录线索ID]
     */
    @JsonProperty("alias_force_thread_id")
    public Integer getAlias_force_thread_id(){
        return this.alias_force_thread_id ;
    }

    /**
     * 设置 [记录线索ID]
     */
    @JsonProperty("alias_force_thread_id")
    public void setAlias_force_thread_id(Integer  alias_force_thread_id){
        this.alias_force_thread_id = alias_force_thread_id ;
        this.alias_force_thread_idDirtyFlag = true ;
    }

     /**
     * 获取 [记录线索ID]脏标记
     */
    @JsonIgnore
    public boolean getAlias_force_thread_idDirtyFlag(){
        return this.alias_force_thread_idDirtyFlag ;
    }   

    /**
     * 获取 [别名]
     */
    @JsonProperty("alias_id")
    public Integer getAlias_id(){
        return this.alias_id ;
    }

    /**
     * 设置 [别名]
     */
    @JsonProperty("alias_id")
    public void setAlias_id(Integer  alias_id){
        this.alias_id = alias_id ;
        this.alias_idDirtyFlag = true ;
    }

     /**
     * 获取 [别名]脏标记
     */
    @JsonIgnore
    public boolean getAlias_idDirtyFlag(){
        return this.alias_idDirtyFlag ;
    }   

    /**
     * 获取 [模型别名]
     */
    @JsonProperty("alias_model_id")
    public Integer getAlias_model_id(){
        return this.alias_model_id ;
    }

    /**
     * 设置 [模型别名]
     */
    @JsonProperty("alias_model_id")
    public void setAlias_model_id(Integer  alias_model_id){
        this.alias_model_id = alias_model_id ;
        this.alias_model_idDirtyFlag = true ;
    }

     /**
     * 获取 [模型别名]脏标记
     */
    @JsonIgnore
    public boolean getAlias_model_idDirtyFlag(){
        return this.alias_model_idDirtyFlag ;
    }   

    /**
     * 获取 [别名]
     */
    @JsonProperty("alias_name")
    public String getAlias_name(){
        return this.alias_name ;
    }

    /**
     * 设置 [别名]
     */
    @JsonProperty("alias_name")
    public void setAlias_name(String  alias_name){
        this.alias_name = alias_name ;
        this.alias_nameDirtyFlag = true ;
    }

     /**
     * 获取 [别名]脏标记
     */
    @JsonIgnore
    public boolean getAlias_nameDirtyFlag(){
        return this.alias_nameDirtyFlag ;
    }   

    /**
     * 获取 [上级模型]
     */
    @JsonProperty("alias_parent_model_id")
    public Integer getAlias_parent_model_id(){
        return this.alias_parent_model_id ;
    }

    /**
     * 设置 [上级模型]
     */
    @JsonProperty("alias_parent_model_id")
    public void setAlias_parent_model_id(Integer  alias_parent_model_id){
        this.alias_parent_model_id = alias_parent_model_id ;
        this.alias_parent_model_idDirtyFlag = true ;
    }

     /**
     * 获取 [上级模型]脏标记
     */
    @JsonIgnore
    public boolean getAlias_parent_model_idDirtyFlag(){
        return this.alias_parent_model_idDirtyFlag ;
    }   

    /**
     * 获取 [上级记录ID]
     */
    @JsonProperty("alias_parent_thread_id")
    public Integer getAlias_parent_thread_id(){
        return this.alias_parent_thread_id ;
    }

    /**
     * 设置 [上级记录ID]
     */
    @JsonProperty("alias_parent_thread_id")
    public void setAlias_parent_thread_id(Integer  alias_parent_thread_id){
        this.alias_parent_thread_id = alias_parent_thread_id ;
        this.alias_parent_thread_idDirtyFlag = true ;
    }

     /**
     * 获取 [上级记录ID]脏标记
     */
    @JsonIgnore
    public boolean getAlias_parent_thread_idDirtyFlag(){
        return this.alias_parent_thread_idDirtyFlag ;
    }   

    /**
     * 获取 [所有者]
     */
    @JsonProperty("alias_user_id")
    public Integer getAlias_user_id(){
        return this.alias_user_id ;
    }

    /**
     * 设置 [所有者]
     */
    @JsonProperty("alias_user_id")
    public void setAlias_user_id(Integer  alias_user_id){
        this.alias_user_id = alias_user_id ;
        this.alias_user_idDirtyFlag = true ;
    }

     /**
     * 获取 [所有者]脏标记
     */
    @JsonIgnore
    public boolean getAlias_user_idDirtyFlag(){
        return this.alias_user_idDirtyFlag ;
    }   

    /**
     * 获取 [应用数量]
     */
    @JsonProperty("application_count")
    public Integer getApplication_count(){
        return this.application_count ;
    }

    /**
     * 设置 [应用数量]
     */
    @JsonProperty("application_count")
    public void setApplication_count(Integer  application_count){
        this.application_count = application_count ;
        this.application_countDirtyFlag = true ;
    }

     /**
     * 获取 [应用数量]脏标记
     */
    @JsonIgnore
    public boolean getApplication_countDirtyFlag(){
        return this.application_countDirtyFlag ;
    }   

    /**
     * 获取 [求职申请]
     */
    @JsonProperty("application_ids")
    public String getApplication_ids(){
        return this.application_ids ;
    }

    /**
     * 设置 [求职申请]
     */
    @JsonProperty("application_ids")
    public void setApplication_ids(String  application_ids){
        this.application_ids = application_ids ;
        this.application_idsDirtyFlag = true ;
    }

     /**
     * 获取 [求职申请]脏标记
     */
    @JsonIgnore
    public boolean getApplication_idsDirtyFlag(){
        return this.application_idsDirtyFlag ;
    }   

    /**
     * 获取 [颜色索引]
     */
    @JsonProperty("color")
    public Integer getColor(){
        return this.color ;
    }

    /**
     * 设置 [颜色索引]
     */
    @JsonProperty("color")
    public void setColor(Integer  color){
        this.color = color ;
        this.colorDirtyFlag = true ;
    }

     /**
     * 获取 [颜色索引]脏标记
     */
    @JsonIgnore
    public boolean getColorDirtyFlag(){
        return this.colorDirtyFlag ;
    }   

    /**
     * 获取 [公司]
     */
    @JsonProperty("company_id")
    public Integer getCompany_id(){
        return this.company_id ;
    }

    /**
     * 设置 [公司]
     */
    @JsonProperty("company_id")
    public void setCompany_id(Integer  company_id){
        this.company_id = company_id ;
        this.company_idDirtyFlag = true ;
    }

     /**
     * 获取 [公司]脏标记
     */
    @JsonIgnore
    public boolean getCompany_idDirtyFlag(){
        return this.company_idDirtyFlag ;
    }   

    /**
     * 获取 [公司]
     */
    @JsonProperty("company_id_text")
    public String getCompany_id_text(){
        return this.company_id_text ;
    }

    /**
     * 设置 [公司]
     */
    @JsonProperty("company_id_text")
    public void setCompany_id_text(String  company_id_text){
        this.company_id_text = company_id_text ;
        this.company_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [公司]脏标记
     */
    @JsonIgnore
    public boolean getCompany_id_textDirtyFlag(){
        return this.company_id_textDirtyFlag ;
    }   

    /**
     * 获取 [创建时间]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return this.create_date ;
    }

    /**
     * 设置 [创建时间]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

     /**
     * 获取 [创建时间]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return this.create_dateDirtyFlag ;
    }   

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return this.create_uid ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

     /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return this.create_uidDirtyFlag ;
    }   

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return this.create_uid_text ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

     /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return this.create_uid_textDirtyFlag ;
    }   

    /**
     * 获取 [部门]
     */
    @JsonProperty("department_id")
    public Integer getDepartment_id(){
        return this.department_id ;
    }

    /**
     * 设置 [部门]
     */
    @JsonProperty("department_id")
    public void setDepartment_id(Integer  department_id){
        this.department_id = department_id ;
        this.department_idDirtyFlag = true ;
    }

     /**
     * 获取 [部门]脏标记
     */
    @JsonIgnore
    public boolean getDepartment_idDirtyFlag(){
        return this.department_idDirtyFlag ;
    }   

    /**
     * 获取 [部门]
     */
    @JsonProperty("department_id_text")
    public String getDepartment_id_text(){
        return this.department_id_text ;
    }

    /**
     * 设置 [部门]
     */
    @JsonProperty("department_id_text")
    public void setDepartment_id_text(String  department_id_text){
        this.department_id_text = department_id_text ;
        this.department_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [部门]脏标记
     */
    @JsonIgnore
    public boolean getDepartment_id_textDirtyFlag(){
        return this.department_id_textDirtyFlag ;
    }   

    /**
     * 获取 [工作说明]
     */
    @JsonProperty("description")
    public String getDescription(){
        return this.description ;
    }

    /**
     * 设置 [工作说明]
     */
    @JsonProperty("description")
    public void setDescription(String  description){
        this.description = description ;
        this.descriptionDirtyFlag = true ;
    }

     /**
     * 获取 [工作说明]脏标记
     */
    @JsonIgnore
    public boolean getDescriptionDirtyFlag(){
        return this.descriptionDirtyFlag ;
    }   

    /**
     * 获取 [显示名称]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return this.display_name ;
    }

    /**
     * 设置 [显示名称]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

     /**
     * 获取 [显示名称]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return this.display_nameDirtyFlag ;
    }   

    /**
     * 获取 [文档数]
     */
    @JsonProperty("documents_count")
    public Integer getDocuments_count(){
        return this.documents_count ;
    }

    /**
     * 设置 [文档数]
     */
    @JsonProperty("documents_count")
    public void setDocuments_count(Integer  documents_count){
        this.documents_count = documents_count ;
        this.documents_countDirtyFlag = true ;
    }

     /**
     * 获取 [文档数]脏标记
     */
    @JsonIgnore
    public boolean getDocuments_countDirtyFlag(){
        return this.documents_countDirtyFlag ;
    }   

    /**
     * 获取 [文档]
     */
    @JsonProperty("document_ids")
    public String getDocument_ids(){
        return this.document_ids ;
    }

    /**
     * 设置 [文档]
     */
    @JsonProperty("document_ids")
    public void setDocument_ids(String  document_ids){
        this.document_ids = document_ids ;
        this.document_idsDirtyFlag = true ;
    }

     /**
     * 获取 [文档]脏标记
     */
    @JsonIgnore
    public boolean getDocument_idsDirtyFlag(){
        return this.document_idsDirtyFlag ;
    }   

    /**
     * 获取 [员工]
     */
    @JsonProperty("employee_ids")
    public String getEmployee_ids(){
        return this.employee_ids ;
    }

    /**
     * 设置 [员工]
     */
    @JsonProperty("employee_ids")
    public void setEmployee_ids(String  employee_ids){
        this.employee_ids = employee_ids ;
        this.employee_idsDirtyFlag = true ;
    }

     /**
     * 获取 [员工]脏标记
     */
    @JsonIgnore
    public boolean getEmployee_idsDirtyFlag(){
        return this.employee_idsDirtyFlag ;
    }   

    /**
     * 获取 [预计员工数合计]
     */
    @JsonProperty("expected_employees")
    public Integer getExpected_employees(){
        return this.expected_employees ;
    }

    /**
     * 设置 [预计员工数合计]
     */
    @JsonProperty("expected_employees")
    public void setExpected_employees(Integer  expected_employees){
        this.expected_employees = expected_employees ;
        this.expected_employeesDirtyFlag = true ;
    }

     /**
     * 获取 [预计员工数合计]脏标记
     */
    @JsonIgnore
    public boolean getExpected_employeesDirtyFlag(){
        return this.expected_employeesDirtyFlag ;
    }   

    /**
     * 获取 [人力资源主管]
     */
    @JsonProperty("hr_responsible_id")
    public Integer getHr_responsible_id(){
        return this.hr_responsible_id ;
    }

    /**
     * 设置 [人力资源主管]
     */
    @JsonProperty("hr_responsible_id")
    public void setHr_responsible_id(Integer  hr_responsible_id){
        this.hr_responsible_id = hr_responsible_id ;
        this.hr_responsible_idDirtyFlag = true ;
    }

     /**
     * 获取 [人力资源主管]脏标记
     */
    @JsonIgnore
    public boolean getHr_responsible_idDirtyFlag(){
        return this.hr_responsible_idDirtyFlag ;
    }   

    /**
     * 获取 [人力资源主管]
     */
    @JsonProperty("hr_responsible_id_text")
    public String getHr_responsible_id_text(){
        return this.hr_responsible_id_text ;
    }

    /**
     * 设置 [人力资源主管]
     */
    @JsonProperty("hr_responsible_id_text")
    public void setHr_responsible_id_text(String  hr_responsible_id_text){
        this.hr_responsible_id_text = hr_responsible_id_text ;
        this.hr_responsible_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [人力资源主管]脏标记
     */
    @JsonIgnore
    public boolean getHr_responsible_id_textDirtyFlag(){
        return this.hr_responsible_id_textDirtyFlag ;
    }   

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return this.id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

     /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return this.idDirtyFlag ;
    }   

    /**
     * 获取 [已发布]
     */
    @JsonProperty("is_published")
    public String getIs_published(){
        return this.is_published ;
    }

    /**
     * 设置 [已发布]
     */
    @JsonProperty("is_published")
    public void setIs_published(String  is_published){
        this.is_published = is_published ;
        this.is_publishedDirtyFlag = true ;
    }

     /**
     * 获取 [已发布]脏标记
     */
    @JsonIgnore
    public boolean getIs_publishedDirtyFlag(){
        return this.is_publishedDirtyFlag ;
    }   

    /**
     * 获取 [SEO优化]
     */
    @JsonProperty("is_seo_optimized")
    public String getIs_seo_optimized(){
        return this.is_seo_optimized ;
    }

    /**
     * 设置 [SEO优化]
     */
    @JsonProperty("is_seo_optimized")
    public void setIs_seo_optimized(String  is_seo_optimized){
        this.is_seo_optimized = is_seo_optimized ;
        this.is_seo_optimizedDirtyFlag = true ;
    }

     /**
     * 获取 [SEO优化]脏标记
     */
    @JsonIgnore
    public boolean getIs_seo_optimizedDirtyFlag(){
        return this.is_seo_optimizedDirtyFlag ;
    }   

    /**
     * 获取 [部门经理]
     */
    @JsonProperty("manager_id")
    public Integer getManager_id(){
        return this.manager_id ;
    }

    /**
     * 设置 [部门经理]
     */
    @JsonProperty("manager_id")
    public void setManager_id(Integer  manager_id){
        this.manager_id = manager_id ;
        this.manager_idDirtyFlag = true ;
    }

     /**
     * 获取 [部门经理]脏标记
     */
    @JsonIgnore
    public boolean getManager_idDirtyFlag(){
        return this.manager_idDirtyFlag ;
    }   

    /**
     * 获取 [部门经理]
     */
    @JsonProperty("manager_id_text")
    public String getManager_id_text(){
        return this.manager_id_text ;
    }

    /**
     * 设置 [部门经理]
     */
    @JsonProperty("manager_id_text")
    public void setManager_id_text(String  manager_id_text){
        this.manager_id_text = manager_id_text ;
        this.manager_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [部门经理]脏标记
     */
    @JsonIgnore
    public boolean getManager_id_textDirtyFlag(){
        return this.manager_id_textDirtyFlag ;
    }   

    /**
     * 获取 [附件数量]
     */
    @JsonProperty("message_attachment_count")
    public Integer getMessage_attachment_count(){
        return this.message_attachment_count ;
    }

    /**
     * 设置 [附件数量]
     */
    @JsonProperty("message_attachment_count")
    public void setMessage_attachment_count(Integer  message_attachment_count){
        this.message_attachment_count = message_attachment_count ;
        this.message_attachment_countDirtyFlag = true ;
    }

     /**
     * 获取 [附件数量]脏标记
     */
    @JsonIgnore
    public boolean getMessage_attachment_countDirtyFlag(){
        return this.message_attachment_countDirtyFlag ;
    }   

    /**
     * 获取 [关注者(渠道)]
     */
    @JsonProperty("message_channel_ids")
    public String getMessage_channel_ids(){
        return this.message_channel_ids ;
    }

    /**
     * 设置 [关注者(渠道)]
     */
    @JsonProperty("message_channel_ids")
    public void setMessage_channel_ids(String  message_channel_ids){
        this.message_channel_ids = message_channel_ids ;
        this.message_channel_idsDirtyFlag = true ;
    }

     /**
     * 获取 [关注者(渠道)]脏标记
     */
    @JsonIgnore
    public boolean getMessage_channel_idsDirtyFlag(){
        return this.message_channel_idsDirtyFlag ;
    }   

    /**
     * 获取 [关注者]
     */
    @JsonProperty("message_follower_ids")
    public String getMessage_follower_ids(){
        return this.message_follower_ids ;
    }

    /**
     * 设置 [关注者]
     */
    @JsonProperty("message_follower_ids")
    public void setMessage_follower_ids(String  message_follower_ids){
        this.message_follower_ids = message_follower_ids ;
        this.message_follower_idsDirtyFlag = true ;
    }

     /**
     * 获取 [关注者]脏标记
     */
    @JsonIgnore
    public boolean getMessage_follower_idsDirtyFlag(){
        return this.message_follower_idsDirtyFlag ;
    }   

    /**
     * 获取 [消息递送错误]
     */
    @JsonProperty("message_has_error")
    public String getMessage_has_error(){
        return this.message_has_error ;
    }

    /**
     * 设置 [消息递送错误]
     */
    @JsonProperty("message_has_error")
    public void setMessage_has_error(String  message_has_error){
        this.message_has_error = message_has_error ;
        this.message_has_errorDirtyFlag = true ;
    }

     /**
     * 获取 [消息递送错误]脏标记
     */
    @JsonIgnore
    public boolean getMessage_has_errorDirtyFlag(){
        return this.message_has_errorDirtyFlag ;
    }   

    /**
     * 获取 [错误数]
     */
    @JsonProperty("message_has_error_counter")
    public Integer getMessage_has_error_counter(){
        return this.message_has_error_counter ;
    }

    /**
     * 设置 [错误数]
     */
    @JsonProperty("message_has_error_counter")
    public void setMessage_has_error_counter(Integer  message_has_error_counter){
        this.message_has_error_counter = message_has_error_counter ;
        this.message_has_error_counterDirtyFlag = true ;
    }

     /**
     * 获取 [错误数]脏标记
     */
    @JsonIgnore
    public boolean getMessage_has_error_counterDirtyFlag(){
        return this.message_has_error_counterDirtyFlag ;
    }   

    /**
     * 获取 [信息]
     */
    @JsonProperty("message_ids")
    public String getMessage_ids(){
        return this.message_ids ;
    }

    /**
     * 设置 [信息]
     */
    @JsonProperty("message_ids")
    public void setMessage_ids(String  message_ids){
        this.message_ids = message_ids ;
        this.message_idsDirtyFlag = true ;
    }

     /**
     * 获取 [信息]脏标记
     */
    @JsonIgnore
    public boolean getMessage_idsDirtyFlag(){
        return this.message_idsDirtyFlag ;
    }   

    /**
     * 获取 [是关注者]
     */
    @JsonProperty("message_is_follower")
    public String getMessage_is_follower(){
        return this.message_is_follower ;
    }

    /**
     * 设置 [是关注者]
     */
    @JsonProperty("message_is_follower")
    public void setMessage_is_follower(String  message_is_follower){
        this.message_is_follower = message_is_follower ;
        this.message_is_followerDirtyFlag = true ;
    }

     /**
     * 获取 [是关注者]脏标记
     */
    @JsonIgnore
    public boolean getMessage_is_followerDirtyFlag(){
        return this.message_is_followerDirtyFlag ;
    }   

    /**
     * 获取 [附件]
     */
    @JsonProperty("message_main_attachment_id")
    public Integer getMessage_main_attachment_id(){
        return this.message_main_attachment_id ;
    }

    /**
     * 设置 [附件]
     */
    @JsonProperty("message_main_attachment_id")
    public void setMessage_main_attachment_id(Integer  message_main_attachment_id){
        this.message_main_attachment_id = message_main_attachment_id ;
        this.message_main_attachment_idDirtyFlag = true ;
    }

     /**
     * 获取 [附件]脏标记
     */
    @JsonIgnore
    public boolean getMessage_main_attachment_idDirtyFlag(){
        return this.message_main_attachment_idDirtyFlag ;
    }   

    /**
     * 获取 [需要采取行动]
     */
    @JsonProperty("message_needaction")
    public String getMessage_needaction(){
        return this.message_needaction ;
    }

    /**
     * 设置 [需要采取行动]
     */
    @JsonProperty("message_needaction")
    public void setMessage_needaction(String  message_needaction){
        this.message_needaction = message_needaction ;
        this.message_needactionDirtyFlag = true ;
    }

     /**
     * 获取 [需要采取行动]脏标记
     */
    @JsonIgnore
    public boolean getMessage_needactionDirtyFlag(){
        return this.message_needactionDirtyFlag ;
    }   

    /**
     * 获取 [行动数量]
     */
    @JsonProperty("message_needaction_counter")
    public Integer getMessage_needaction_counter(){
        return this.message_needaction_counter ;
    }

    /**
     * 设置 [行动数量]
     */
    @JsonProperty("message_needaction_counter")
    public void setMessage_needaction_counter(Integer  message_needaction_counter){
        this.message_needaction_counter = message_needaction_counter ;
        this.message_needaction_counterDirtyFlag = true ;
    }

     /**
     * 获取 [行动数量]脏标记
     */
    @JsonIgnore
    public boolean getMessage_needaction_counterDirtyFlag(){
        return this.message_needaction_counterDirtyFlag ;
    }   

    /**
     * 获取 [关注者(业务伙伴)]
     */
    @JsonProperty("message_partner_ids")
    public String getMessage_partner_ids(){
        return this.message_partner_ids ;
    }

    /**
     * 设置 [关注者(业务伙伴)]
     */
    @JsonProperty("message_partner_ids")
    public void setMessage_partner_ids(String  message_partner_ids){
        this.message_partner_ids = message_partner_ids ;
        this.message_partner_idsDirtyFlag = true ;
    }

     /**
     * 获取 [关注者(业务伙伴)]脏标记
     */
    @JsonIgnore
    public boolean getMessage_partner_idsDirtyFlag(){
        return this.message_partner_idsDirtyFlag ;
    }   

    /**
     * 获取 [未读消息]
     */
    @JsonProperty("message_unread")
    public String getMessage_unread(){
        return this.message_unread ;
    }

    /**
     * 设置 [未读消息]
     */
    @JsonProperty("message_unread")
    public void setMessage_unread(String  message_unread){
        this.message_unread = message_unread ;
        this.message_unreadDirtyFlag = true ;
    }

     /**
     * 获取 [未读消息]脏标记
     */
    @JsonIgnore
    public boolean getMessage_unreadDirtyFlag(){
        return this.message_unreadDirtyFlag ;
    }   

    /**
     * 获取 [未读消息计数器]
     */
    @JsonProperty("message_unread_counter")
    public Integer getMessage_unread_counter(){
        return this.message_unread_counter ;
    }

    /**
     * 设置 [未读消息计数器]
     */
    @JsonProperty("message_unread_counter")
    public void setMessage_unread_counter(Integer  message_unread_counter){
        this.message_unread_counter = message_unread_counter ;
        this.message_unread_counterDirtyFlag = true ;
    }

     /**
     * 获取 [未读消息计数器]脏标记
     */
    @JsonIgnore
    public boolean getMessage_unread_counterDirtyFlag(){
        return this.message_unread_counterDirtyFlag ;
    }   

    /**
     * 获取 [工作岗位]
     */
    @JsonProperty("name")
    public String getName(){
        return this.name ;
    }

    /**
     * 设置 [工作岗位]
     */
    @JsonProperty("name")
    public void setName(String  name){
        this.name = name ;
        this.nameDirtyFlag = true ;
    }

     /**
     * 获取 [工作岗位]脏标记
     */
    @JsonIgnore
    public boolean getNameDirtyFlag(){
        return this.nameDirtyFlag ;
    }   

    /**
     * 获取 [当前员工数量]
     */
    @JsonProperty("no_of_employee")
    public Integer getNo_of_employee(){
        return this.no_of_employee ;
    }

    /**
     * 设置 [当前员工数量]
     */
    @JsonProperty("no_of_employee")
    public void setNo_of_employee(Integer  no_of_employee){
        this.no_of_employee = no_of_employee ;
        this.no_of_employeeDirtyFlag = true ;
    }

     /**
     * 获取 [当前员工数量]脏标记
     */
    @JsonIgnore
    public boolean getNo_of_employeeDirtyFlag(){
        return this.no_of_employeeDirtyFlag ;
    }   

    /**
     * 获取 [已雇用员工]
     */
    @JsonProperty("no_of_hired_employee")
    public Integer getNo_of_hired_employee(){
        return this.no_of_hired_employee ;
    }

    /**
     * 设置 [已雇用员工]
     */
    @JsonProperty("no_of_hired_employee")
    public void setNo_of_hired_employee(Integer  no_of_hired_employee){
        this.no_of_hired_employee = no_of_hired_employee ;
        this.no_of_hired_employeeDirtyFlag = true ;
    }

     /**
     * 获取 [已雇用员工]脏标记
     */
    @JsonIgnore
    public boolean getNo_of_hired_employeeDirtyFlag(){
        return this.no_of_hired_employeeDirtyFlag ;
    }   

    /**
     * 获取 [期望的新员工]
     */
    @JsonProperty("no_of_recruitment")
    public Integer getNo_of_recruitment(){
        return this.no_of_recruitment ;
    }

    /**
     * 设置 [期望的新员工]
     */
    @JsonProperty("no_of_recruitment")
    public void setNo_of_recruitment(Integer  no_of_recruitment){
        this.no_of_recruitment = no_of_recruitment ;
        this.no_of_recruitmentDirtyFlag = true ;
    }

     /**
     * 获取 [期望的新员工]脏标记
     */
    @JsonIgnore
    public boolean getNo_of_recruitmentDirtyFlag(){
        return this.no_of_recruitmentDirtyFlag ;
    }   

    /**
     * 获取 [要求]
     */
    @JsonProperty("requirements")
    public String getRequirements(){
        return this.requirements ;
    }

    /**
     * 设置 [要求]
     */
    @JsonProperty("requirements")
    public void setRequirements(String  requirements){
        this.requirements = requirements ;
        this.requirementsDirtyFlag = true ;
    }

     /**
     * 获取 [要求]脏标记
     */
    @JsonIgnore
    public boolean getRequirementsDirtyFlag(){
        return this.requirementsDirtyFlag ;
    }   

    /**
     * 获取 [状态]
     */
    @JsonProperty("state")
    public String getState(){
        return this.state ;
    }

    /**
     * 设置 [状态]
     */
    @JsonProperty("state")
    public void setState(String  state){
        this.state = state ;
        this.stateDirtyFlag = true ;
    }

     /**
     * 获取 [状态]脏标记
     */
    @JsonIgnore
    public boolean getStateDirtyFlag(){
        return this.stateDirtyFlag ;
    }   

    /**
     * 获取 [招聘负责人]
     */
    @JsonProperty("user_id")
    public Integer getUser_id(){
        return this.user_id ;
    }

    /**
     * 设置 [招聘负责人]
     */
    @JsonProperty("user_id")
    public void setUser_id(Integer  user_id){
        this.user_id = user_id ;
        this.user_idDirtyFlag = true ;
    }

     /**
     * 获取 [招聘负责人]脏标记
     */
    @JsonIgnore
    public boolean getUser_idDirtyFlag(){
        return this.user_idDirtyFlag ;
    }   

    /**
     * 获取 [招聘负责人]
     */
    @JsonProperty("user_id_text")
    public String getUser_id_text(){
        return this.user_id_text ;
    }

    /**
     * 设置 [招聘负责人]
     */
    @JsonProperty("user_id_text")
    public void setUser_id_text(String  user_id_text){
        this.user_id_text = user_id_text ;
        this.user_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [招聘负责人]脏标记
     */
    @JsonIgnore
    public boolean getUser_id_textDirtyFlag(){
        return this.user_id_textDirtyFlag ;
    }   

    /**
     * 获取 [网站说明]
     */
    @JsonProperty("website_description")
    public String getWebsite_description(){
        return this.website_description ;
    }

    /**
     * 设置 [网站说明]
     */
    @JsonProperty("website_description")
    public void setWebsite_description(String  website_description){
        this.website_description = website_description ;
        this.website_descriptionDirtyFlag = true ;
    }

     /**
     * 获取 [网站说明]脏标记
     */
    @JsonIgnore
    public boolean getWebsite_descriptionDirtyFlag(){
        return this.website_descriptionDirtyFlag ;
    }   

    /**
     * 获取 [网站]
     */
    @JsonProperty("website_id")
    public Integer getWebsite_id(){
        return this.website_id ;
    }

    /**
     * 设置 [网站]
     */
    @JsonProperty("website_id")
    public void setWebsite_id(Integer  website_id){
        this.website_id = website_id ;
        this.website_idDirtyFlag = true ;
    }

     /**
     * 获取 [网站]脏标记
     */
    @JsonIgnore
    public boolean getWebsite_idDirtyFlag(){
        return this.website_idDirtyFlag ;
    }   

    /**
     * 获取 [网站消息]
     */
    @JsonProperty("website_message_ids")
    public String getWebsite_message_ids(){
        return this.website_message_ids ;
    }

    /**
     * 设置 [网站消息]
     */
    @JsonProperty("website_message_ids")
    public void setWebsite_message_ids(String  website_message_ids){
        this.website_message_ids = website_message_ids ;
        this.website_message_idsDirtyFlag = true ;
    }

     /**
     * 获取 [网站消息]脏标记
     */
    @JsonIgnore
    public boolean getWebsite_message_idsDirtyFlag(){
        return this.website_message_idsDirtyFlag ;
    }   

    /**
     * 获取 [网站元说明]
     */
    @JsonProperty("website_meta_description")
    public String getWebsite_meta_description(){
        return this.website_meta_description ;
    }

    /**
     * 设置 [网站元说明]
     */
    @JsonProperty("website_meta_description")
    public void setWebsite_meta_description(String  website_meta_description){
        this.website_meta_description = website_meta_description ;
        this.website_meta_descriptionDirtyFlag = true ;
    }

     /**
     * 获取 [网站元说明]脏标记
     */
    @JsonIgnore
    public boolean getWebsite_meta_descriptionDirtyFlag(){
        return this.website_meta_descriptionDirtyFlag ;
    }   

    /**
     * 获取 [网站meta关键词]
     */
    @JsonProperty("website_meta_keywords")
    public String getWebsite_meta_keywords(){
        return this.website_meta_keywords ;
    }

    /**
     * 设置 [网站meta关键词]
     */
    @JsonProperty("website_meta_keywords")
    public void setWebsite_meta_keywords(String  website_meta_keywords){
        this.website_meta_keywords = website_meta_keywords ;
        this.website_meta_keywordsDirtyFlag = true ;
    }

     /**
     * 获取 [网站meta关键词]脏标记
     */
    @JsonIgnore
    public boolean getWebsite_meta_keywordsDirtyFlag(){
        return this.website_meta_keywordsDirtyFlag ;
    }   

    /**
     * 获取 [网站opengraph图像]
     */
    @JsonProperty("website_meta_og_img")
    public String getWebsite_meta_og_img(){
        return this.website_meta_og_img ;
    }

    /**
     * 设置 [网站opengraph图像]
     */
    @JsonProperty("website_meta_og_img")
    public void setWebsite_meta_og_img(String  website_meta_og_img){
        this.website_meta_og_img = website_meta_og_img ;
        this.website_meta_og_imgDirtyFlag = true ;
    }

     /**
     * 获取 [网站opengraph图像]脏标记
     */
    @JsonIgnore
    public boolean getWebsite_meta_og_imgDirtyFlag(){
        return this.website_meta_og_imgDirtyFlag ;
    }   

    /**
     * 获取 [网站meta标题]
     */
    @JsonProperty("website_meta_title")
    public String getWebsite_meta_title(){
        return this.website_meta_title ;
    }

    /**
     * 设置 [网站meta标题]
     */
    @JsonProperty("website_meta_title")
    public void setWebsite_meta_title(String  website_meta_title){
        this.website_meta_title = website_meta_title ;
        this.website_meta_titleDirtyFlag = true ;
    }

     /**
     * 获取 [网站meta标题]脏标记
     */
    @JsonIgnore
    public boolean getWebsite_meta_titleDirtyFlag(){
        return this.website_meta_titleDirtyFlag ;
    }   

    /**
     * 获取 [在当前网站显示]
     */
    @JsonProperty("website_published")
    public String getWebsite_published(){
        return this.website_published ;
    }

    /**
     * 设置 [在当前网站显示]
     */
    @JsonProperty("website_published")
    public void setWebsite_published(String  website_published){
        this.website_published = website_published ;
        this.website_publishedDirtyFlag = true ;
    }

     /**
     * 获取 [在当前网站显示]脏标记
     */
    @JsonIgnore
    public boolean getWebsite_publishedDirtyFlag(){
        return this.website_publishedDirtyFlag ;
    }   

    /**
     * 获取 [网站网址]
     */
    @JsonProperty("website_url")
    public String getWebsite_url(){
        return this.website_url ;
    }

    /**
     * 设置 [网站网址]
     */
    @JsonProperty("website_url")
    public void setWebsite_url(String  website_url){
        this.website_url = website_url ;
        this.website_urlDirtyFlag = true ;
    }

     /**
     * 获取 [网站网址]脏标记
     */
    @JsonIgnore
    public boolean getWebsite_urlDirtyFlag(){
        return this.website_urlDirtyFlag ;
    }   

    /**
     * 获取 [最后更新时间]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return this.write_date ;
    }

    /**
     * 设置 [最后更新时间]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新时间]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return this.write_dateDirtyFlag ;
    }   

    /**
     * 获取 [最后更新人]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return this.write_uid ;
    }

    /**
     * 设置 [最后更新人]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新人]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return this.write_uidDirtyFlag ;
    }   

    /**
     * 获取 [最后更新人]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return this.write_uid_text ;
    }

    /**
     * 设置 [最后更新人]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新人]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return this.write_uid_textDirtyFlag ;
    }   

    /**
     * 获取 [最后修改日]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return this.__last_update ;
    }

    /**
     * 设置 [最后修改日]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

     /**
     * 获取 [最后修改日]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return this.__last_updateDirtyFlag ;
    }   



    public void fromMap(Map<String, Object> map) throws Exception {
		if(!(map.get("address_id") instanceof Boolean)&& map.get("address_id")!=null){
			Object[] objs = (Object[])map.get("address_id");
			if(objs.length > 0){
				this.setAddress_id((Integer)objs[0]);
			}
		}
		if(!(map.get("address_id") instanceof Boolean)&& map.get("address_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("address_id");
			if(objs.length > 1){
				this.setAddress_id_text((String)objs[1]);
			}
		}
		if(!(map.get("alias_contact") instanceof Boolean)&& map.get("alias_contact")!=null){
			this.setAlias_contact((String)map.get("alias_contact"));
		}
		if(!(map.get("alias_defaults") instanceof Boolean)&& map.get("alias_defaults")!=null){
			this.setAlias_defaults((String)map.get("alias_defaults"));
		}
		if(!(map.get("alias_domain") instanceof Boolean)&& map.get("alias_domain")!=null){
			this.setAlias_domain((String)map.get("alias_domain"));
		}
		if(!(map.get("alias_force_thread_id") instanceof Boolean)&& map.get("alias_force_thread_id")!=null){
			this.setAlias_force_thread_id((Integer)map.get("alias_force_thread_id"));
		}
		if(!(map.get("alias_id") instanceof Boolean)&& map.get("alias_id")!=null){
			Object[] objs = (Object[])map.get("alias_id");
			if(objs.length > 0){
				this.setAlias_id((Integer)objs[0]);
			}
		}
		if(!(map.get("alias_model_id") instanceof Boolean)&& map.get("alias_model_id")!=null){
			Object[] objs = (Object[])map.get("alias_model_id");
			if(objs.length > 0){
				this.setAlias_model_id((Integer)objs[0]);
			}
		}
		if(!(map.get("alias_name") instanceof Boolean)&& map.get("alias_name")!=null){
			this.setAlias_name((String)map.get("alias_name"));
		}
		if(!(map.get("alias_parent_model_id") instanceof Boolean)&& map.get("alias_parent_model_id")!=null){
			Object[] objs = (Object[])map.get("alias_parent_model_id");
			if(objs.length > 0){
				this.setAlias_parent_model_id((Integer)objs[0]);
			}
		}
		if(!(map.get("alias_parent_thread_id") instanceof Boolean)&& map.get("alias_parent_thread_id")!=null){
			this.setAlias_parent_thread_id((Integer)map.get("alias_parent_thread_id"));
		}
		if(!(map.get("alias_user_id") instanceof Boolean)&& map.get("alias_user_id")!=null){
			Object[] objs = (Object[])map.get("alias_user_id");
			if(objs.length > 0){
				this.setAlias_user_id((Integer)objs[0]);
			}
		}
		if(!(map.get("application_count") instanceof Boolean)&& map.get("application_count")!=null){
			this.setApplication_count((Integer)map.get("application_count"));
		}
		if(!(map.get("application_ids") instanceof Boolean)&& map.get("application_ids")!=null){
			Object[] objs = (Object[])map.get("application_ids");
			if(objs.length > 0){
				Integer[] application_ids = Arrays.copyOfRange(objs,0,objs.length,Integer[].class);
				this.setApplication_ids(Arrays.toString(application_ids));
			}
		}
		if(!(map.get("color") instanceof Boolean)&& map.get("color")!=null){
			this.setColor((Integer)map.get("color"));
		}
		if(!(map.get("company_id") instanceof Boolean)&& map.get("company_id")!=null){
			Object[] objs = (Object[])map.get("company_id");
			if(objs.length > 0){
				this.setCompany_id((Integer)objs[0]);
			}
		}
		if(!(map.get("company_id") instanceof Boolean)&& map.get("company_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("company_id");
			if(objs.length > 1){
				this.setCompany_id_text((String)objs[1]);
			}
		}
		if(!(map.get("create_date") instanceof Boolean)&& map.get("create_date")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse((String)map.get("create_date"));
   			this.setCreate_date(new Timestamp(parse.getTime()));
		}
		if(!(map.get("create_uid") instanceof Boolean)&& map.get("create_uid")!=null){
			Object[] objs = (Object[])map.get("create_uid");
			if(objs.length > 0){
				this.setCreate_uid((Integer)objs[0]);
			}
		}
		if(!(map.get("create_uid") instanceof Boolean)&& map.get("create_uid")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("create_uid");
			if(objs.length > 1){
				this.setCreate_uid_text((String)objs[1]);
			}
		}
		if(!(map.get("department_id") instanceof Boolean)&& map.get("department_id")!=null){
			Object[] objs = (Object[])map.get("department_id");
			if(objs.length > 0){
				this.setDepartment_id((Integer)objs[0]);
			}
		}
		if(!(map.get("department_id") instanceof Boolean)&& map.get("department_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("department_id");
			if(objs.length > 1){
				this.setDepartment_id_text((String)objs[1]);
			}
		}
		if(!(map.get("description") instanceof Boolean)&& map.get("description")!=null){
			this.setDescription((String)map.get("description"));
		}
		if(!(map.get("display_name") instanceof Boolean)&& map.get("display_name")!=null){
			this.setDisplay_name((String)map.get("display_name"));
		}
		if(!(map.get("documents_count") instanceof Boolean)&& map.get("documents_count")!=null){
			this.setDocuments_count((Integer)map.get("documents_count"));
		}
		if(!(map.get("document_ids") instanceof Boolean)&& map.get("document_ids")!=null){
			Object[] objs = (Object[])map.get("document_ids");
			if(objs.length > 0){
				Integer[] document_ids = Arrays.copyOfRange(objs,0,objs.length,Integer[].class);
				this.setDocument_ids(Arrays.toString(document_ids));
			}
		}
		if(!(map.get("employee_ids") instanceof Boolean)&& map.get("employee_ids")!=null){
			Object[] objs = (Object[])map.get("employee_ids");
			if(objs.length > 0){
				Integer[] employee_ids = Arrays.copyOfRange(objs,0,objs.length,Integer[].class);
				this.setEmployee_ids(Arrays.toString(employee_ids));
			}
		}
		if(!(map.get("expected_employees") instanceof Boolean)&& map.get("expected_employees")!=null){
			this.setExpected_employees((Integer)map.get("expected_employees"));
		}
		if(!(map.get("hr_responsible_id") instanceof Boolean)&& map.get("hr_responsible_id")!=null){
			Object[] objs = (Object[])map.get("hr_responsible_id");
			if(objs.length > 0){
				this.setHr_responsible_id((Integer)objs[0]);
			}
		}
		if(!(map.get("hr_responsible_id") instanceof Boolean)&& map.get("hr_responsible_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("hr_responsible_id");
			if(objs.length > 1){
				this.setHr_responsible_id_text((String)objs[1]);
			}
		}
		if(!(map.get("id") instanceof Boolean)&& map.get("id")!=null){
			this.setId((Integer)map.get("id"));
		}
		if(map.get("is_published") instanceof Boolean){
			this.setIs_published(((Boolean)map.get("is_published"))? "true" : "false");
		}
		if(map.get("is_seo_optimized") instanceof Boolean){
			this.setIs_seo_optimized(((Boolean)map.get("is_seo_optimized"))? "true" : "false");
		}
		if(!(map.get("manager_id") instanceof Boolean)&& map.get("manager_id")!=null){
			Object[] objs = (Object[])map.get("manager_id");
			if(objs.length > 0){
				this.setManager_id((Integer)objs[0]);
			}
		}
		if(!(map.get("manager_id") instanceof Boolean)&& map.get("manager_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("manager_id");
			if(objs.length > 1){
				this.setManager_id_text((String)objs[1]);
			}
		}
		if(!(map.get("message_attachment_count") instanceof Boolean)&& map.get("message_attachment_count")!=null){
			this.setMessage_attachment_count((Integer)map.get("message_attachment_count"));
		}
		if(!(map.get("message_channel_ids") instanceof Boolean)&& map.get("message_channel_ids")!=null){
			Object[] objs = (Object[])map.get("message_channel_ids");
			if(objs.length > 0){
				Integer[] message_channel_ids = Arrays.copyOfRange(objs,0,objs.length,Integer[].class);
				this.setMessage_channel_ids(Arrays.toString(message_channel_ids));
			}
		}
		if(!(map.get("message_follower_ids") instanceof Boolean)&& map.get("message_follower_ids")!=null){
			Object[] objs = (Object[])map.get("message_follower_ids");
			if(objs.length > 0){
				Integer[] message_follower_ids = Arrays.copyOfRange(objs,0,objs.length,Integer[].class);
				this.setMessage_follower_ids(Arrays.toString(message_follower_ids));
			}
		}
		if(map.get("message_has_error") instanceof Boolean){
			this.setMessage_has_error(((Boolean)map.get("message_has_error"))? "true" : "false");
		}
		if(!(map.get("message_has_error_counter") instanceof Boolean)&& map.get("message_has_error_counter")!=null){
			this.setMessage_has_error_counter((Integer)map.get("message_has_error_counter"));
		}
		if(!(map.get("message_ids") instanceof Boolean)&& map.get("message_ids")!=null){
			Object[] objs = (Object[])map.get("message_ids");
			if(objs.length > 0){
				Integer[] message_ids = Arrays.copyOfRange(objs,0,objs.length,Integer[].class);
				this.setMessage_ids(Arrays.toString(message_ids));
			}
		}
		if(map.get("message_is_follower") instanceof Boolean){
			this.setMessage_is_follower(((Boolean)map.get("message_is_follower"))? "true" : "false");
		}
		if(!(map.get("message_main_attachment_id") instanceof Boolean)&& map.get("message_main_attachment_id")!=null){
			Object[] objs = (Object[])map.get("message_main_attachment_id");
			if(objs.length > 0){
				this.setMessage_main_attachment_id((Integer)objs[0]);
			}
		}
		if(map.get("message_needaction") instanceof Boolean){
			this.setMessage_needaction(((Boolean)map.get("message_needaction"))? "true" : "false");
		}
		if(!(map.get("message_needaction_counter") instanceof Boolean)&& map.get("message_needaction_counter")!=null){
			this.setMessage_needaction_counter((Integer)map.get("message_needaction_counter"));
		}
		if(!(map.get("message_partner_ids") instanceof Boolean)&& map.get("message_partner_ids")!=null){
			Object[] objs = (Object[])map.get("message_partner_ids");
			if(objs.length > 0){
				Integer[] message_partner_ids = Arrays.copyOfRange(objs,0,objs.length,Integer[].class);
				this.setMessage_partner_ids(Arrays.toString(message_partner_ids));
			}
		}
		if(map.get("message_unread") instanceof Boolean){
			this.setMessage_unread(((Boolean)map.get("message_unread"))? "true" : "false");
		}
		if(!(map.get("message_unread_counter") instanceof Boolean)&& map.get("message_unread_counter")!=null){
			this.setMessage_unread_counter((Integer)map.get("message_unread_counter"));
		}
		if(!(map.get("name") instanceof Boolean)&& map.get("name")!=null){
			this.setName((String)map.get("name"));
		}
		if(!(map.get("no_of_employee") instanceof Boolean)&& map.get("no_of_employee")!=null){
			this.setNo_of_employee((Integer)map.get("no_of_employee"));
		}
		if(!(map.get("no_of_hired_employee") instanceof Boolean)&& map.get("no_of_hired_employee")!=null){
			this.setNo_of_hired_employee((Integer)map.get("no_of_hired_employee"));
		}
		if(!(map.get("no_of_recruitment") instanceof Boolean)&& map.get("no_of_recruitment")!=null){
			this.setNo_of_recruitment((Integer)map.get("no_of_recruitment"));
		}
		if(!(map.get("requirements") instanceof Boolean)&& map.get("requirements")!=null){
			this.setRequirements((String)map.get("requirements"));
		}
		if(!(map.get("state") instanceof Boolean)&& map.get("state")!=null){
			this.setState((String)map.get("state"));
		}
		if(!(map.get("user_id") instanceof Boolean)&& map.get("user_id")!=null){
			Object[] objs = (Object[])map.get("user_id");
			if(objs.length > 0){
				this.setUser_id((Integer)objs[0]);
			}
		}
		if(!(map.get("user_id") instanceof Boolean)&& map.get("user_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("user_id");
			if(objs.length > 1){
				this.setUser_id_text((String)objs[1]);
			}
		}
		if(!(map.get("website_description") instanceof Boolean)&& map.get("website_description")!=null){
			this.setWebsite_description((String)map.get("website_description"));
		}
		if(!(map.get("website_id") instanceof Boolean)&& map.get("website_id")!=null){
			Object[] objs = (Object[])map.get("website_id");
			if(objs.length > 0){
				this.setWebsite_id((Integer)objs[0]);
			}
		}
		if(!(map.get("website_message_ids") instanceof Boolean)&& map.get("website_message_ids")!=null){
			Object[] objs = (Object[])map.get("website_message_ids");
			if(objs.length > 0){
				Integer[] website_message_ids = Arrays.copyOfRange(objs,0,objs.length,Integer[].class);
				this.setWebsite_message_ids(Arrays.toString(website_message_ids));
			}
		}
		if(!(map.get("website_meta_description") instanceof Boolean)&& map.get("website_meta_description")!=null){
			this.setWebsite_meta_description((String)map.get("website_meta_description"));
		}
		if(!(map.get("website_meta_keywords") instanceof Boolean)&& map.get("website_meta_keywords")!=null){
			this.setWebsite_meta_keywords((String)map.get("website_meta_keywords"));
		}
		if(!(map.get("website_meta_og_img") instanceof Boolean)&& map.get("website_meta_og_img")!=null){
			this.setWebsite_meta_og_img((String)map.get("website_meta_og_img"));
		}
		if(!(map.get("website_meta_title") instanceof Boolean)&& map.get("website_meta_title")!=null){
			this.setWebsite_meta_title((String)map.get("website_meta_title"));
		}
		if(map.get("website_published") instanceof Boolean){
			this.setWebsite_published(((Boolean)map.get("website_published"))? "true" : "false");
		}
		if(!(map.get("website_url") instanceof Boolean)&& map.get("website_url")!=null){
			this.setWebsite_url((String)map.get("website_url"));
		}
		if(!(map.get("write_date") instanceof Boolean)&& map.get("write_date")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse((String)map.get("write_date"));
   			this.setWrite_date(new Timestamp(parse.getTime()));
		}
		if(!(map.get("write_uid") instanceof Boolean)&& map.get("write_uid")!=null){
			Object[] objs = (Object[])map.get("write_uid");
			if(objs.length > 0){
				this.setWrite_uid((Integer)objs[0]);
			}
		}
		if(!(map.get("write_uid") instanceof Boolean)&& map.get("write_uid")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("write_uid");
			if(objs.length > 1){
				this.setWrite_uid_text((String)objs[1]);
			}
		}
		if(!(map.get("__last_update") instanceof Boolean)&& map.get("__last_update")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse((String)map.get("__last_update"));
   			this.set__last_update(new Timestamp(parse.getTime()));
		}
	}

	public Map<String, Object> toMap() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		if(this.getAddress_id()!=null&&this.getAddress_idDirtyFlag()){
			map.put("address_id",this.getAddress_id());
		}else if(this.getAddress_idDirtyFlag()){
			map.put("address_id",false);
		}
		if(this.getAddress_id_text()!=null&&this.getAddress_id_textDirtyFlag()){
			//忽略文本外键address_id_text
		}else if(this.getAddress_id_textDirtyFlag()){
			map.put("address_id",false);
		}
		if(this.getAlias_contact()!=null&&this.getAlias_contactDirtyFlag()){
			map.put("alias_contact",this.getAlias_contact());
		}else if(this.getAlias_contactDirtyFlag()){
			map.put("alias_contact",false);
		}
		if(this.getAlias_defaults()!=null&&this.getAlias_defaultsDirtyFlag()){
			map.put("alias_defaults",this.getAlias_defaults());
		}else if(this.getAlias_defaultsDirtyFlag()){
			map.put("alias_defaults",false);
		}
		if(this.getAlias_domain()!=null&&this.getAlias_domainDirtyFlag()){
			map.put("alias_domain",this.getAlias_domain());
		}else if(this.getAlias_domainDirtyFlag()){
			map.put("alias_domain",false);
		}
		if(this.getAlias_force_thread_id()!=null&&this.getAlias_force_thread_idDirtyFlag()){
			map.put("alias_force_thread_id",this.getAlias_force_thread_id());
		}else if(this.getAlias_force_thread_idDirtyFlag()){
			map.put("alias_force_thread_id",false);
		}
		if(this.getAlias_id()!=null&&this.getAlias_idDirtyFlag()){
			map.put("alias_id",this.getAlias_id());
		}else if(this.getAlias_idDirtyFlag()){
			map.put("alias_id",false);
		}
		if(this.getAlias_model_id()!=null&&this.getAlias_model_idDirtyFlag()){
			map.put("alias_model_id",this.getAlias_model_id());
		}else if(this.getAlias_model_idDirtyFlag()){
			map.put("alias_model_id",false);
		}
		if(this.getAlias_name()!=null&&this.getAlias_nameDirtyFlag()){
			map.put("alias_name",this.getAlias_name());
		}else if(this.getAlias_nameDirtyFlag()){
			map.put("alias_name",false);
		}
		if(this.getAlias_parent_model_id()!=null&&this.getAlias_parent_model_idDirtyFlag()){
			map.put("alias_parent_model_id",this.getAlias_parent_model_id());
		}else if(this.getAlias_parent_model_idDirtyFlag()){
			map.put("alias_parent_model_id",false);
		}
		if(this.getAlias_parent_thread_id()!=null&&this.getAlias_parent_thread_idDirtyFlag()){
			map.put("alias_parent_thread_id",this.getAlias_parent_thread_id());
		}else if(this.getAlias_parent_thread_idDirtyFlag()){
			map.put("alias_parent_thread_id",false);
		}
		if(this.getAlias_user_id()!=null&&this.getAlias_user_idDirtyFlag()){
			map.put("alias_user_id",this.getAlias_user_id());
		}else if(this.getAlias_user_idDirtyFlag()){
			map.put("alias_user_id",false);
		}
		if(this.getApplication_count()!=null&&this.getApplication_countDirtyFlag()){
			map.put("application_count",this.getApplication_count());
		}else if(this.getApplication_countDirtyFlag()){
			map.put("application_count",false);
		}
		if(this.getApplication_ids()!=null&&this.getApplication_idsDirtyFlag()){
			map.put("application_ids",this.getApplication_ids());
		}else if(this.getApplication_idsDirtyFlag()){
			map.put("application_ids",false);
		}
		if(this.getColor()!=null&&this.getColorDirtyFlag()){
			map.put("color",this.getColor());
		}else if(this.getColorDirtyFlag()){
			map.put("color",false);
		}
		if(this.getCompany_id()!=null&&this.getCompany_idDirtyFlag()){
			map.put("company_id",this.getCompany_id());
		}else if(this.getCompany_idDirtyFlag()){
			map.put("company_id",false);
		}
		if(this.getCompany_id_text()!=null&&this.getCompany_id_textDirtyFlag()){
			//忽略文本外键company_id_text
		}else if(this.getCompany_id_textDirtyFlag()){
			map.put("company_id",false);
		}
		if(this.getCreate_date()!=null&&this.getCreate_dateDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String datetimeStr = sdf.format(this.getCreate_date());
			map.put("create_date",datetimeStr);
		}else if(this.getCreate_dateDirtyFlag()){
			map.put("create_date",false);
		}
		if(this.getCreate_uid()!=null&&this.getCreate_uidDirtyFlag()){
			map.put("create_uid",this.getCreate_uid());
		}else if(this.getCreate_uidDirtyFlag()){
			map.put("create_uid",false);
		}
		if(this.getCreate_uid_text()!=null&&this.getCreate_uid_textDirtyFlag()){
			//忽略文本外键create_uid_text
		}else if(this.getCreate_uid_textDirtyFlag()){
			map.put("create_uid",false);
		}
		if(this.getDepartment_id()!=null&&this.getDepartment_idDirtyFlag()){
			map.put("department_id",this.getDepartment_id());
		}else if(this.getDepartment_idDirtyFlag()){
			map.put("department_id",false);
		}
		if(this.getDepartment_id_text()!=null&&this.getDepartment_id_textDirtyFlag()){
			//忽略文本外键department_id_text
		}else if(this.getDepartment_id_textDirtyFlag()){
			map.put("department_id",false);
		}
		if(this.getDescription()!=null&&this.getDescriptionDirtyFlag()){
			map.put("description",this.getDescription());
		}else if(this.getDescriptionDirtyFlag()){
			map.put("description",false);
		}
		if(this.getDisplay_name()!=null&&this.getDisplay_nameDirtyFlag()){
			map.put("display_name",this.getDisplay_name());
		}else if(this.getDisplay_nameDirtyFlag()){
			map.put("display_name",false);
		}
		if(this.getDocuments_count()!=null&&this.getDocuments_countDirtyFlag()){
			map.put("documents_count",this.getDocuments_count());
		}else if(this.getDocuments_countDirtyFlag()){
			map.put("documents_count",false);
		}
		if(this.getDocument_ids()!=null&&this.getDocument_idsDirtyFlag()){
			map.put("document_ids",this.getDocument_ids());
		}else if(this.getDocument_idsDirtyFlag()){
			map.put("document_ids",false);
		}
		if(this.getEmployee_ids()!=null&&this.getEmployee_idsDirtyFlag()){
			map.put("employee_ids",this.getEmployee_ids());
		}else if(this.getEmployee_idsDirtyFlag()){
			map.put("employee_ids",false);
		}
		if(this.getExpected_employees()!=null&&this.getExpected_employeesDirtyFlag()){
			map.put("expected_employees",this.getExpected_employees());
		}else if(this.getExpected_employeesDirtyFlag()){
			map.put("expected_employees",false);
		}
		if(this.getHr_responsible_id()!=null&&this.getHr_responsible_idDirtyFlag()){
			map.put("hr_responsible_id",this.getHr_responsible_id());
		}else if(this.getHr_responsible_idDirtyFlag()){
			map.put("hr_responsible_id",false);
		}
		if(this.getHr_responsible_id_text()!=null&&this.getHr_responsible_id_textDirtyFlag()){
			//忽略文本外键hr_responsible_id_text
		}else if(this.getHr_responsible_id_textDirtyFlag()){
			map.put("hr_responsible_id",false);
		}
		if(this.getId()!=null&&this.getIdDirtyFlag()){
			map.put("id",this.getId());
		}else if(this.getIdDirtyFlag()){
			map.put("id",false);
		}
		if(this.getIs_published()!=null&&this.getIs_publishedDirtyFlag()){
			map.put("is_published",Boolean.parseBoolean(this.getIs_published()));		
		}		if(this.getIs_seo_optimized()!=null&&this.getIs_seo_optimizedDirtyFlag()){
			map.put("is_seo_optimized",Boolean.parseBoolean(this.getIs_seo_optimized()));		
		}		if(this.getManager_id()!=null&&this.getManager_idDirtyFlag()){
			map.put("manager_id",this.getManager_id());
		}else if(this.getManager_idDirtyFlag()){
			map.put("manager_id",false);
		}
		if(this.getManager_id_text()!=null&&this.getManager_id_textDirtyFlag()){
			//忽略文本外键manager_id_text
		}else if(this.getManager_id_textDirtyFlag()){
			map.put("manager_id",false);
		}
		if(this.getMessage_attachment_count()!=null&&this.getMessage_attachment_countDirtyFlag()){
			map.put("message_attachment_count",this.getMessage_attachment_count());
		}else if(this.getMessage_attachment_countDirtyFlag()){
			map.put("message_attachment_count",false);
		}
		if(this.getMessage_channel_ids()!=null&&this.getMessage_channel_idsDirtyFlag()){
			map.put("message_channel_ids",this.getMessage_channel_ids());
		}else if(this.getMessage_channel_idsDirtyFlag()){
			map.put("message_channel_ids",false);
		}
		if(this.getMessage_follower_ids()!=null&&this.getMessage_follower_idsDirtyFlag()){
			map.put("message_follower_ids",this.getMessage_follower_ids());
		}else if(this.getMessage_follower_idsDirtyFlag()){
			map.put("message_follower_ids",false);
		}
		if(this.getMessage_has_error()!=null&&this.getMessage_has_errorDirtyFlag()){
			map.put("message_has_error",Boolean.parseBoolean(this.getMessage_has_error()));		
		}		if(this.getMessage_has_error_counter()!=null&&this.getMessage_has_error_counterDirtyFlag()){
			map.put("message_has_error_counter",this.getMessage_has_error_counter());
		}else if(this.getMessage_has_error_counterDirtyFlag()){
			map.put("message_has_error_counter",false);
		}
		if(this.getMessage_ids()!=null&&this.getMessage_idsDirtyFlag()){
			map.put("message_ids",this.getMessage_ids());
		}else if(this.getMessage_idsDirtyFlag()){
			map.put("message_ids",false);
		}
		if(this.getMessage_is_follower()!=null&&this.getMessage_is_followerDirtyFlag()){
			map.put("message_is_follower",Boolean.parseBoolean(this.getMessage_is_follower()));		
		}		if(this.getMessage_main_attachment_id()!=null&&this.getMessage_main_attachment_idDirtyFlag()){
			map.put("message_main_attachment_id",this.getMessage_main_attachment_id());
		}else if(this.getMessage_main_attachment_idDirtyFlag()){
			map.put("message_main_attachment_id",false);
		}
		if(this.getMessage_needaction()!=null&&this.getMessage_needactionDirtyFlag()){
			map.put("message_needaction",Boolean.parseBoolean(this.getMessage_needaction()));		
		}		if(this.getMessage_needaction_counter()!=null&&this.getMessage_needaction_counterDirtyFlag()){
			map.put("message_needaction_counter",this.getMessage_needaction_counter());
		}else if(this.getMessage_needaction_counterDirtyFlag()){
			map.put("message_needaction_counter",false);
		}
		if(this.getMessage_partner_ids()!=null&&this.getMessage_partner_idsDirtyFlag()){
			map.put("message_partner_ids",this.getMessage_partner_ids());
		}else if(this.getMessage_partner_idsDirtyFlag()){
			map.put("message_partner_ids",false);
		}
		if(this.getMessage_unread()!=null&&this.getMessage_unreadDirtyFlag()){
			map.put("message_unread",Boolean.parseBoolean(this.getMessage_unread()));		
		}		if(this.getMessage_unread_counter()!=null&&this.getMessage_unread_counterDirtyFlag()){
			map.put("message_unread_counter",this.getMessage_unread_counter());
		}else if(this.getMessage_unread_counterDirtyFlag()){
			map.put("message_unread_counter",false);
		}
		if(this.getName()!=null&&this.getNameDirtyFlag()){
			map.put("name",this.getName());
		}else if(this.getNameDirtyFlag()){
			map.put("name",false);
		}
		if(this.getNo_of_employee()!=null&&this.getNo_of_employeeDirtyFlag()){
			map.put("no_of_employee",this.getNo_of_employee());
		}else if(this.getNo_of_employeeDirtyFlag()){
			map.put("no_of_employee",false);
		}
		if(this.getNo_of_hired_employee()!=null&&this.getNo_of_hired_employeeDirtyFlag()){
			map.put("no_of_hired_employee",this.getNo_of_hired_employee());
		}else if(this.getNo_of_hired_employeeDirtyFlag()){
			map.put("no_of_hired_employee",false);
		}
		if(this.getNo_of_recruitment()!=null&&this.getNo_of_recruitmentDirtyFlag()){
			map.put("no_of_recruitment",this.getNo_of_recruitment());
		}else if(this.getNo_of_recruitmentDirtyFlag()){
			map.put("no_of_recruitment",false);
		}
		if(this.getRequirements()!=null&&this.getRequirementsDirtyFlag()){
			map.put("requirements",this.getRequirements());
		}else if(this.getRequirementsDirtyFlag()){
			map.put("requirements",false);
		}
		if(this.getState()!=null&&this.getStateDirtyFlag()){
			map.put("state",this.getState());
		}else if(this.getStateDirtyFlag()){
			map.put("state",false);
		}
		if(this.getUser_id()!=null&&this.getUser_idDirtyFlag()){
			map.put("user_id",this.getUser_id());
		}else if(this.getUser_idDirtyFlag()){
			map.put("user_id",false);
		}
		if(this.getUser_id_text()!=null&&this.getUser_id_textDirtyFlag()){
			//忽略文本外键user_id_text
		}else if(this.getUser_id_textDirtyFlag()){
			map.put("user_id",false);
		}
		if(this.getWebsite_description()!=null&&this.getWebsite_descriptionDirtyFlag()){
			map.put("website_description",this.getWebsite_description());
		}else if(this.getWebsite_descriptionDirtyFlag()){
			map.put("website_description",false);
		}
		if(this.getWebsite_id()!=null&&this.getWebsite_idDirtyFlag()){
			map.put("website_id",this.getWebsite_id());
		}else if(this.getWebsite_idDirtyFlag()){
			map.put("website_id",false);
		}
		if(this.getWebsite_message_ids()!=null&&this.getWebsite_message_idsDirtyFlag()){
			map.put("website_message_ids",this.getWebsite_message_ids());
		}else if(this.getWebsite_message_idsDirtyFlag()){
			map.put("website_message_ids",false);
		}
		if(this.getWebsite_meta_description()!=null&&this.getWebsite_meta_descriptionDirtyFlag()){
			map.put("website_meta_description",this.getWebsite_meta_description());
		}else if(this.getWebsite_meta_descriptionDirtyFlag()){
			map.put("website_meta_description",false);
		}
		if(this.getWebsite_meta_keywords()!=null&&this.getWebsite_meta_keywordsDirtyFlag()){
			map.put("website_meta_keywords",this.getWebsite_meta_keywords());
		}else if(this.getWebsite_meta_keywordsDirtyFlag()){
			map.put("website_meta_keywords",false);
		}
		if(this.getWebsite_meta_og_img()!=null&&this.getWebsite_meta_og_imgDirtyFlag()){
			map.put("website_meta_og_img",this.getWebsite_meta_og_img());
		}else if(this.getWebsite_meta_og_imgDirtyFlag()){
			map.put("website_meta_og_img",false);
		}
		if(this.getWebsite_meta_title()!=null&&this.getWebsite_meta_titleDirtyFlag()){
			map.put("website_meta_title",this.getWebsite_meta_title());
		}else if(this.getWebsite_meta_titleDirtyFlag()){
			map.put("website_meta_title",false);
		}
		if(this.getWebsite_published()!=null&&this.getWebsite_publishedDirtyFlag()){
			map.put("website_published",Boolean.parseBoolean(this.getWebsite_published()));		
		}		if(this.getWebsite_url()!=null&&this.getWebsite_urlDirtyFlag()){
			map.put("website_url",this.getWebsite_url());
		}else if(this.getWebsite_urlDirtyFlag()){
			map.put("website_url",false);
		}
		if(this.getWrite_date()!=null&&this.getWrite_dateDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String datetimeStr = sdf.format(this.getWrite_date());
			map.put("write_date",datetimeStr);
		}else if(this.getWrite_dateDirtyFlag()){
			map.put("write_date",false);
		}
		if(this.getWrite_uid()!=null&&this.getWrite_uidDirtyFlag()){
			map.put("write_uid",this.getWrite_uid());
		}else if(this.getWrite_uidDirtyFlag()){
			map.put("write_uid",false);
		}
		if(this.getWrite_uid_text()!=null&&this.getWrite_uid_textDirtyFlag()){
			//忽略文本外键write_uid_text
		}else if(this.getWrite_uid_textDirtyFlag()){
			map.put("write_uid",false);
		}
		if(this.get__last_update()!=null&&this.get__last_updateDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String datetimeStr = sdf.format(this.get__last_update());
			map.put("__last_update",datetimeStr);
		}else if(this.get__last_updateDirtyFlag()){
			map.put("__last_update",false);
		}
		return map;
	}

}
