package cn.ibizlab.odoo.client.odoo_hr.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Ihr_job;
import cn.ibizlab.odoo.core.client.service.Ihr_jobClientService;
import cn.ibizlab.odoo.client.odoo_hr.model.hr_jobImpl;
import cn.ibizlab.odoo.client.odoo_hr.odooclient.Ihr_jobOdooClient;
import cn.ibizlab.odoo.client.odoo_hr.odooclient.impl.hr_jobOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[hr_job] 服务对象接口
 */
@Service
public class hr_jobClientServiceImpl implements Ihr_jobClientService {
    @Autowired
    private  Ihr_jobOdooClient  hr_jobOdooClient;

    public Ihr_job createModel() {		
		return new hr_jobImpl();
	}


        public void update(Ihr_job hr_job){
this.hr_jobOdooClient.update(hr_job) ;
        }
        
        public void createBatch(List<Ihr_job> hr_jobs){
            
        }
        
        public void remove(Ihr_job hr_job){
this.hr_jobOdooClient.remove(hr_job) ;
        }
        
        public void removeBatch(List<Ihr_job> hr_jobs){
            
        }
        
        public void create(Ihr_job hr_job){
this.hr_jobOdooClient.create(hr_job) ;
        }
        
        public Page<Ihr_job> search(SearchContext context){
            return this.hr_jobOdooClient.search(context) ;
        }
        
        public void updateBatch(List<Ihr_job> hr_jobs){
            
        }
        
        public void get(Ihr_job hr_job){
            this.hr_jobOdooClient.get(hr_job) ;
        }
        
        public Page<Ihr_job> select(SearchContext context){
            return null ;
        }
        

}

