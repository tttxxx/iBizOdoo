package cn.ibizlab.odoo.client.odoo_im_livechat.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Iim_livechat_report_operator;
import cn.ibizlab.odoo.core.client.service.Iim_livechat_report_operatorClientService;
import cn.ibizlab.odoo.client.odoo_im_livechat.model.im_livechat_report_operatorImpl;
import cn.ibizlab.odoo.client.odoo_im_livechat.odooclient.Iim_livechat_report_operatorOdooClient;
import cn.ibizlab.odoo.client.odoo_im_livechat.odooclient.impl.im_livechat_report_operatorOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[im_livechat_report_operator] 服务对象接口
 */
@Service
public class im_livechat_report_operatorClientServiceImpl implements Iim_livechat_report_operatorClientService {
    @Autowired
    private  Iim_livechat_report_operatorOdooClient  im_livechat_report_operatorOdooClient;

    public Iim_livechat_report_operator createModel() {		
		return new im_livechat_report_operatorImpl();
	}


        public void createBatch(List<Iim_livechat_report_operator> im_livechat_report_operators){
            
        }
        
        public void updateBatch(List<Iim_livechat_report_operator> im_livechat_report_operators){
            
        }
        
        public Page<Iim_livechat_report_operator> search(SearchContext context){
            return this.im_livechat_report_operatorOdooClient.search(context) ;
        }
        
        public void removeBatch(List<Iim_livechat_report_operator> im_livechat_report_operators){
            
        }
        
        public void remove(Iim_livechat_report_operator im_livechat_report_operator){
this.im_livechat_report_operatorOdooClient.remove(im_livechat_report_operator) ;
        }
        
        public void create(Iim_livechat_report_operator im_livechat_report_operator){
this.im_livechat_report_operatorOdooClient.create(im_livechat_report_operator) ;
        }
        
        public void update(Iim_livechat_report_operator im_livechat_report_operator){
this.im_livechat_report_operatorOdooClient.update(im_livechat_report_operator) ;
        }
        
        public void get(Iim_livechat_report_operator im_livechat_report_operator){
            this.im_livechat_report_operatorOdooClient.get(im_livechat_report_operator) ;
        }
        
        public Page<Iim_livechat_report_operator> select(SearchContext context){
            return null ;
        }
        

}

