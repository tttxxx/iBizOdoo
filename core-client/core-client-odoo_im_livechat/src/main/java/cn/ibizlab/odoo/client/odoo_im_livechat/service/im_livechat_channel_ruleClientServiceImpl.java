package cn.ibizlab.odoo.client.odoo_im_livechat.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Iim_livechat_channel_rule;
import cn.ibizlab.odoo.core.client.service.Iim_livechat_channel_ruleClientService;
import cn.ibizlab.odoo.client.odoo_im_livechat.model.im_livechat_channel_ruleImpl;
import cn.ibizlab.odoo.client.odoo_im_livechat.odooclient.Iim_livechat_channel_ruleOdooClient;
import cn.ibizlab.odoo.client.odoo_im_livechat.odooclient.impl.im_livechat_channel_ruleOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[im_livechat_channel_rule] 服务对象接口
 */
@Service
public class im_livechat_channel_ruleClientServiceImpl implements Iim_livechat_channel_ruleClientService {
    @Autowired
    private  Iim_livechat_channel_ruleOdooClient  im_livechat_channel_ruleOdooClient;

    public Iim_livechat_channel_rule createModel() {		
		return new im_livechat_channel_ruleImpl();
	}


        public void createBatch(List<Iim_livechat_channel_rule> im_livechat_channel_rules){
            
        }
        
        public void removeBatch(List<Iim_livechat_channel_rule> im_livechat_channel_rules){
            
        }
        
        public void get(Iim_livechat_channel_rule im_livechat_channel_rule){
            this.im_livechat_channel_ruleOdooClient.get(im_livechat_channel_rule) ;
        }
        
        public void update(Iim_livechat_channel_rule im_livechat_channel_rule){
this.im_livechat_channel_ruleOdooClient.update(im_livechat_channel_rule) ;
        }
        
        public void create(Iim_livechat_channel_rule im_livechat_channel_rule){
this.im_livechat_channel_ruleOdooClient.create(im_livechat_channel_rule) ;
        }
        
        public void remove(Iim_livechat_channel_rule im_livechat_channel_rule){
this.im_livechat_channel_ruleOdooClient.remove(im_livechat_channel_rule) ;
        }
        
        public Page<Iim_livechat_channel_rule> search(SearchContext context){
            return this.im_livechat_channel_ruleOdooClient.search(context) ;
        }
        
        public void updateBatch(List<Iim_livechat_channel_rule> im_livechat_channel_rules){
            
        }
        
        public Page<Iim_livechat_channel_rule> select(SearchContext context){
            return null ;
        }
        

}

