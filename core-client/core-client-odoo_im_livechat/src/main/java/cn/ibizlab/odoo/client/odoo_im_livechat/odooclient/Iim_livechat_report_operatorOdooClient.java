package cn.ibizlab.odoo.client.odoo_im_livechat.odooclient;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.core.client.model.Iim_livechat_report_operator;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[im_livechat_report_operator] 服务对象客户端接口
 */
public interface Iim_livechat_report_operatorOdooClient {
    
        public void createBatch(Iim_livechat_report_operator im_livechat_report_operator);

        public void updateBatch(Iim_livechat_report_operator im_livechat_report_operator);

        public Page<Iim_livechat_report_operator> search(SearchContext context);

        public void removeBatch(Iim_livechat_report_operator im_livechat_report_operator);

        public void remove(Iim_livechat_report_operator im_livechat_report_operator);

        public void create(Iim_livechat_report_operator im_livechat_report_operator);

        public void update(Iim_livechat_report_operator im_livechat_report_operator);

        public void get(Iim_livechat_report_operator im_livechat_report_operator);

        public List<Iim_livechat_report_operator> select();


}