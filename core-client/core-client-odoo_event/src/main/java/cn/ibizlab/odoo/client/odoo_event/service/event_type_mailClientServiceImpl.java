package cn.ibizlab.odoo.client.odoo_event.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Ievent_type_mail;
import cn.ibizlab.odoo.core.client.service.Ievent_type_mailClientService;
import cn.ibizlab.odoo.client.odoo_event.model.event_type_mailImpl;
import cn.ibizlab.odoo.client.odoo_event.odooclient.Ievent_type_mailOdooClient;
import cn.ibizlab.odoo.client.odoo_event.odooclient.impl.event_type_mailOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[event_type_mail] 服务对象接口
 */
@Service
public class event_type_mailClientServiceImpl implements Ievent_type_mailClientService {
    @Autowired
    private  Ievent_type_mailOdooClient  event_type_mailOdooClient;

    public Ievent_type_mail createModel() {		
		return new event_type_mailImpl();
	}


        public void createBatch(List<Ievent_type_mail> event_type_mails){
            
        }
        
        public void update(Ievent_type_mail event_type_mail){
this.event_type_mailOdooClient.update(event_type_mail) ;
        }
        
        public Page<Ievent_type_mail> search(SearchContext context){
            return this.event_type_mailOdooClient.search(context) ;
        }
        
        public void remove(Ievent_type_mail event_type_mail){
this.event_type_mailOdooClient.remove(event_type_mail) ;
        }
        
        public void updateBatch(List<Ievent_type_mail> event_type_mails){
            
        }
        
        public void removeBatch(List<Ievent_type_mail> event_type_mails){
            
        }
        
        public void get(Ievent_type_mail event_type_mail){
            this.event_type_mailOdooClient.get(event_type_mail) ;
        }
        
        public void create(Ievent_type_mail event_type_mail){
this.event_type_mailOdooClient.create(event_type_mail) ;
        }
        
        public Page<Ievent_type_mail> select(SearchContext context){
            return null ;
        }
        

}

