package cn.ibizlab.odoo.client.odoo_event.odooclient;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.core.client.model.Ievent_confirm;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[event_confirm] 服务对象客户端接口
 */
public interface Ievent_confirmOdooClient {
    
        public Page<Ievent_confirm> search(SearchContext context);

        public void create(Ievent_confirm event_confirm);

        public void update(Ievent_confirm event_confirm);

        public void removeBatch(Ievent_confirm event_confirm);

        public void updateBatch(Ievent_confirm event_confirm);

        public void get(Ievent_confirm event_confirm);

        public void remove(Ievent_confirm event_confirm);

        public void createBatch(Ievent_confirm event_confirm);

        public List<Ievent_confirm> select();


}