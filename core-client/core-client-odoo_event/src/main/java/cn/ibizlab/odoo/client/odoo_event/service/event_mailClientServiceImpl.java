package cn.ibizlab.odoo.client.odoo_event.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Ievent_mail;
import cn.ibizlab.odoo.core.client.service.Ievent_mailClientService;
import cn.ibizlab.odoo.client.odoo_event.model.event_mailImpl;
import cn.ibizlab.odoo.client.odoo_event.odooclient.Ievent_mailOdooClient;
import cn.ibizlab.odoo.client.odoo_event.odooclient.impl.event_mailOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[event_mail] 服务对象接口
 */
@Service
public class event_mailClientServiceImpl implements Ievent_mailClientService {
    @Autowired
    private  Ievent_mailOdooClient  event_mailOdooClient;

    public Ievent_mail createModel() {		
		return new event_mailImpl();
	}


        public void removeBatch(List<Ievent_mail> event_mails){
            
        }
        
        public void remove(Ievent_mail event_mail){
this.event_mailOdooClient.remove(event_mail) ;
        }
        
        public void create(Ievent_mail event_mail){
this.event_mailOdooClient.create(event_mail) ;
        }
        
        public void createBatch(List<Ievent_mail> event_mails){
            
        }
        
        public void updateBatch(List<Ievent_mail> event_mails){
            
        }
        
        public Page<Ievent_mail> search(SearchContext context){
            return this.event_mailOdooClient.search(context) ;
        }
        
        public void get(Ievent_mail event_mail){
            this.event_mailOdooClient.get(event_mail) ;
        }
        
        public void update(Ievent_mail event_mail){
this.event_mailOdooClient.update(event_mail) ;
        }
        
        public Page<Ievent_mail> select(SearchContext context){
            return null ;
        }
        

}

