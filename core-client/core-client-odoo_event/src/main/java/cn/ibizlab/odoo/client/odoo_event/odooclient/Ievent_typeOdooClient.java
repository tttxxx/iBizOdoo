package cn.ibizlab.odoo.client.odoo_event.odooclient;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.core.client.model.Ievent_type;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[event_type] 服务对象客户端接口
 */
public interface Ievent_typeOdooClient {
    
        public void updateBatch(Ievent_type event_type);

        public void createBatch(Ievent_type event_type);

        public void removeBatch(Ievent_type event_type);

        public Page<Ievent_type> search(SearchContext context);

        public void get(Ievent_type event_type);

        public void create(Ievent_type event_type);

        public void remove(Ievent_type event_type);

        public void update(Ievent_type event_type);

        public List<Ievent_type> select();


}