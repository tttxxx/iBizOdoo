package cn.ibizlab.odoo.client.odoo_event.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Ievent_event_ticket;
import cn.ibizlab.odoo.core.client.service.Ievent_event_ticketClientService;
import cn.ibizlab.odoo.client.odoo_event.model.event_event_ticketImpl;
import cn.ibizlab.odoo.client.odoo_event.odooclient.Ievent_event_ticketOdooClient;
import cn.ibizlab.odoo.client.odoo_event.odooclient.impl.event_event_ticketOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[event_event_ticket] 服务对象接口
 */
@Service
public class event_event_ticketClientServiceImpl implements Ievent_event_ticketClientService {
    @Autowired
    private  Ievent_event_ticketOdooClient  event_event_ticketOdooClient;

    public Ievent_event_ticket createModel() {		
		return new event_event_ticketImpl();
	}


        public void create(Ievent_event_ticket event_event_ticket){
this.event_event_ticketOdooClient.create(event_event_ticket) ;
        }
        
        public void get(Ievent_event_ticket event_event_ticket){
            this.event_event_ticketOdooClient.get(event_event_ticket) ;
        }
        
        public void update(Ievent_event_ticket event_event_ticket){
this.event_event_ticketOdooClient.update(event_event_ticket) ;
        }
        
        public void removeBatch(List<Ievent_event_ticket> event_event_tickets){
            
        }
        
        public Page<Ievent_event_ticket> search(SearchContext context){
            return this.event_event_ticketOdooClient.search(context) ;
        }
        
        public void remove(Ievent_event_ticket event_event_ticket){
this.event_event_ticketOdooClient.remove(event_event_ticket) ;
        }
        
        public void createBatch(List<Ievent_event_ticket> event_event_tickets){
            
        }
        
        public void updateBatch(List<Ievent_event_ticket> event_event_tickets){
            
        }
        
        public Page<Ievent_event_ticket> select(SearchContext context){
            return null ;
        }
        

}

