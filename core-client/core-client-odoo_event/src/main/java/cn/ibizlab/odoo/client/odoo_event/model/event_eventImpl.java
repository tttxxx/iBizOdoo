package cn.ibizlab.odoo.client.odoo_event.model;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.math.BigInteger;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.core.client.model.Ievent_event;
import cn.ibizlab.odoo.util.helper.OdooClientHelper;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang.ArrayUtils;
import org.springframework.util.StringUtils;

/**
 * 接口实体[event_event] 对象
 */
public class event_eventImpl implements Ievent_event,Serializable{

    /**
     * 有效
     */
    public String active;

    @JsonIgnore
    public boolean activeDirtyFlag;
    
    /**
     * 地点
     */
    public Integer address_id;

    @JsonIgnore
    public boolean address_idDirtyFlag;
    
    /**
     * 地点
     */
    public String address_id_text;

    @JsonIgnore
    public boolean address_id_textDirtyFlag;
    
    /**
     * 自动确认注册
     */
    public String auto_confirm;

    @JsonIgnore
    public boolean auto_confirmDirtyFlag;
    
    /**
     * 徽章背面
     */
    public String badge_back;

    @JsonIgnore
    public boolean badge_backDirtyFlag;
    
    /**
     * 徽章字体
     */
    public String badge_front;

    @JsonIgnore
    public boolean badge_frontDirtyFlag;
    
    /**
     * 徽章内部左边
     */
    public String badge_innerleft;

    @JsonIgnore
    public boolean badge_innerleftDirtyFlag;
    
    /**
     * 徽章内部右边
     */
    public String badge_innerright;

    @JsonIgnore
    public boolean badge_innerrightDirtyFlag;
    
    /**
     * 看板颜色索引
     */
    public Integer color;

    @JsonIgnore
    public boolean colorDirtyFlag;
    
    /**
     * 公司
     */
    public Integer company_id;

    @JsonIgnore
    public boolean company_idDirtyFlag;
    
    /**
     * 公司
     */
    public String company_id_text;

    @JsonIgnore
    public boolean company_id_textDirtyFlag;
    
    /**
     * 国家
     */
    public Integer country_id;

    @JsonIgnore
    public boolean country_idDirtyFlag;
    
    /**
     * 国家
     */
    public String country_id_text;

    @JsonIgnore
    public boolean country_id_textDirtyFlag;
    
    /**
     * 创建时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp create_date;

    @JsonIgnore
    public boolean create_dateDirtyFlag;
    
    /**
     * 创建人
     */
    public Integer create_uid;

    @JsonIgnore
    public boolean create_uidDirtyFlag;
    
    /**
     * 创建人
     */
    public String create_uid_text;

    @JsonIgnore
    public boolean create_uid_textDirtyFlag;
    
    /**
     * 开始日期
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp date_begin;

    @JsonIgnore
    public boolean date_beginDirtyFlag;
    
    /**
     * 定位开始日期
     */
    public String date_begin_located;

    @JsonIgnore
    public boolean date_begin_locatedDirtyFlag;
    
    /**
     * 结束日期
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp date_end;

    @JsonIgnore
    public boolean date_endDirtyFlag;
    
    /**
     * 定位最后日期
     */
    public String date_end_located;

    @JsonIgnore
    public boolean date_end_locatedDirtyFlag;
    
    /**
     * 时区
     */
    public String date_tz;

    @JsonIgnore
    public boolean date_tzDirtyFlag;
    
    /**
     * 说明
     */
    public String description;

    @JsonIgnore
    public boolean descriptionDirtyFlag;
    
    /**
     * 显示名称
     */
    public String display_name;

    @JsonIgnore
    public boolean display_nameDirtyFlag;
    
    /**
     * 事件图标
     */
    public String event_logo;

    @JsonIgnore
    public boolean event_logoDirtyFlag;
    
    /**
     * 邮件排程
     */
    public String event_mail_ids;

    @JsonIgnore
    public boolean event_mail_idsDirtyFlag;
    
    /**
     * 活动入场券
     */
    public String event_ticket_ids;

    @JsonIgnore
    public boolean event_ticket_idsDirtyFlag;
    
    /**
     * 类别
     */
    public Integer event_type_id;

    @JsonIgnore
    public boolean event_type_idDirtyFlag;
    
    /**
     * 类别
     */
    public String event_type_id_text;

    @JsonIgnore
    public boolean event_type_id_textDirtyFlag;
    
    /**
     * ID
     */
    public Integer id;

    @JsonIgnore
    public boolean idDirtyFlag;
    
    /**
     * 在线活动
     */
    public String is_online;

    @JsonIgnore
    public boolean is_onlineDirtyFlag;
    
    /**
     * 正在参加
     */
    public String is_participating;

    @JsonIgnore
    public boolean is_participatingDirtyFlag;
    
    /**
     * 已发布
     */
    public String is_published;

    @JsonIgnore
    public boolean is_publishedDirtyFlag;
    
    /**
     * SEO优化
     */
    public String is_seo_optimized;

    @JsonIgnore
    public boolean is_seo_optimizedDirtyFlag;
    
    /**
     * 活动菜单
     */
    public Integer menu_id;

    @JsonIgnore
    public boolean menu_idDirtyFlag;
    
    /**
     * 活动菜单
     */
    public String menu_id_text;

    @JsonIgnore
    public boolean menu_id_textDirtyFlag;
    
    /**
     * 附件数量
     */
    public Integer message_attachment_count;

    @JsonIgnore
    public boolean message_attachment_countDirtyFlag;
    
    /**
     * 关注者(渠道)
     */
    public String message_channel_ids;

    @JsonIgnore
    public boolean message_channel_idsDirtyFlag;
    
    /**
     * 关注者
     */
    public String message_follower_ids;

    @JsonIgnore
    public boolean message_follower_idsDirtyFlag;
    
    /**
     * 消息递送错误
     */
    public String message_has_error;

    @JsonIgnore
    public boolean message_has_errorDirtyFlag;
    
    /**
     * 错误数
     */
    public Integer message_has_error_counter;

    @JsonIgnore
    public boolean message_has_error_counterDirtyFlag;
    
    /**
     * 消息
     */
    public String message_ids;

    @JsonIgnore
    public boolean message_idsDirtyFlag;
    
    /**
     * 是关注者
     */
    public String message_is_follower;

    @JsonIgnore
    public boolean message_is_followerDirtyFlag;
    
    /**
     * 附件
     */
    public Integer message_main_attachment_id;

    @JsonIgnore
    public boolean message_main_attachment_idDirtyFlag;
    
    /**
     * 需要采取行动
     */
    public String message_needaction;

    @JsonIgnore
    public boolean message_needactionDirtyFlag;
    
    /**
     * 行动数量
     */
    public Integer message_needaction_counter;

    @JsonIgnore
    public boolean message_needaction_counterDirtyFlag;
    
    /**
     * 关注者(业务伙伴)
     */
    public String message_partner_ids;

    @JsonIgnore
    public boolean message_partner_idsDirtyFlag;
    
    /**
     * 未读消息
     */
    public String message_unread;

    @JsonIgnore
    public boolean message_unreadDirtyFlag;
    
    /**
     * 未读消息计数器
     */
    public Integer message_unread_counter;

    @JsonIgnore
    public boolean message_unread_counterDirtyFlag;
    
    /**
     * 活动
     */
    public String name;

    @JsonIgnore
    public boolean nameDirtyFlag;
    
    /**
     * 组织者
     */
    public Integer organizer_id;

    @JsonIgnore
    public boolean organizer_idDirtyFlag;
    
    /**
     * 组织者
     */
    public String organizer_id_text;

    @JsonIgnore
    public boolean organizer_id_textDirtyFlag;
    
    /**
     * 与会者
     */
    public String registration_ids;

    @JsonIgnore
    public boolean registration_idsDirtyFlag;
    
    /**
     * 与会者最多人数
     */
    public String seats_availability;

    @JsonIgnore
    public boolean seats_availabilityDirtyFlag;
    
    /**
     * 可用席位
     */
    public Integer seats_available;

    @JsonIgnore
    public boolean seats_availableDirtyFlag;
    
    /**
     * 预期的与会人员数量
     */
    public Integer seats_expected;

    @JsonIgnore
    public boolean seats_expectedDirtyFlag;
    
    /**
     * 与会者最多人数
     */
    public Integer seats_max;

    @JsonIgnore
    public boolean seats_maxDirtyFlag;
    
    /**
     * 与会者最少数量
     */
    public Integer seats_min;

    @JsonIgnore
    public boolean seats_minDirtyFlag;
    
    /**
     * 预订席位
     */
    public Integer seats_reserved;

    @JsonIgnore
    public boolean seats_reservedDirtyFlag;
    
    /**
     * 未确认的席位预订
     */
    public Integer seats_unconfirmed;

    @JsonIgnore
    public boolean seats_unconfirmedDirtyFlag;
    
    /**
     * 参与者数目
     */
    public Integer seats_used;

    @JsonIgnore
    public boolean seats_usedDirtyFlag;
    
    /**
     * 状态
     */
    public String state;

    @JsonIgnore
    public boolean stateDirtyFlag;
    
    /**
     * Twitter主题标签
     */
    public String twitter_hashtag;

    @JsonIgnore
    public boolean twitter_hashtagDirtyFlag;
    
    /**
     * 负责人
     */
    public Integer user_id;

    @JsonIgnore
    public boolean user_idDirtyFlag;
    
    /**
     * 负责人
     */
    public String user_id_text;

    @JsonIgnore
    public boolean user_id_textDirtyFlag;
    
    /**
     * 网站
     */
    public Integer website_id;

    @JsonIgnore
    public boolean website_idDirtyFlag;
    
    /**
     * 专用菜单
     */
    public String website_menu;

    @JsonIgnore
    public boolean website_menuDirtyFlag;
    
    /**
     * 网站消息
     */
    public String website_message_ids;

    @JsonIgnore
    public boolean website_message_idsDirtyFlag;
    
    /**
     * 网站元说明
     */
    public String website_meta_description;

    @JsonIgnore
    public boolean website_meta_descriptionDirtyFlag;
    
    /**
     * 网站meta关键词
     */
    public String website_meta_keywords;

    @JsonIgnore
    public boolean website_meta_keywordsDirtyFlag;
    
    /**
     * 网站opengraph图像
     */
    public String website_meta_og_img;

    @JsonIgnore
    public boolean website_meta_og_imgDirtyFlag;
    
    /**
     * 网站meta标题
     */
    public String website_meta_title;

    @JsonIgnore
    public boolean website_meta_titleDirtyFlag;
    
    /**
     * 在当前网站显示
     */
    public String website_published;

    @JsonIgnore
    public boolean website_publishedDirtyFlag;
    
    /**
     * 网站网址
     */
    public String website_url;

    @JsonIgnore
    public boolean website_urlDirtyFlag;
    
    /**
     * 最后更新时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp write_date;

    @JsonIgnore
    public boolean write_dateDirtyFlag;
    
    /**
     * 最后更新人
     */
    public Integer write_uid;

    @JsonIgnore
    public boolean write_uidDirtyFlag;
    
    /**
     * 最后更新人
     */
    public String write_uid_text;

    @JsonIgnore
    public boolean write_uid_textDirtyFlag;
    
    /**
     * 最后修改日
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp __last_update;

    @JsonIgnore
    public boolean __last_updateDirtyFlag;
    
    /**
     * 获取 [有效]
     */
    @JsonProperty("active")
    public String getActive(){
        return this.active ;
    }

    /**
     * 设置 [有效]
     */
    @JsonProperty("active")
    public void setActive(String  active){
        this.active = active ;
        this.activeDirtyFlag = true ;
    }

     /**
     * 获取 [有效]脏标记
     */
    @JsonIgnore
    public boolean getActiveDirtyFlag(){
        return this.activeDirtyFlag ;
    }   

    /**
     * 获取 [地点]
     */
    @JsonProperty("address_id")
    public Integer getAddress_id(){
        return this.address_id ;
    }

    /**
     * 设置 [地点]
     */
    @JsonProperty("address_id")
    public void setAddress_id(Integer  address_id){
        this.address_id = address_id ;
        this.address_idDirtyFlag = true ;
    }

     /**
     * 获取 [地点]脏标记
     */
    @JsonIgnore
    public boolean getAddress_idDirtyFlag(){
        return this.address_idDirtyFlag ;
    }   

    /**
     * 获取 [地点]
     */
    @JsonProperty("address_id_text")
    public String getAddress_id_text(){
        return this.address_id_text ;
    }

    /**
     * 设置 [地点]
     */
    @JsonProperty("address_id_text")
    public void setAddress_id_text(String  address_id_text){
        this.address_id_text = address_id_text ;
        this.address_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [地点]脏标记
     */
    @JsonIgnore
    public boolean getAddress_id_textDirtyFlag(){
        return this.address_id_textDirtyFlag ;
    }   

    /**
     * 获取 [自动确认注册]
     */
    @JsonProperty("auto_confirm")
    public String getAuto_confirm(){
        return this.auto_confirm ;
    }

    /**
     * 设置 [自动确认注册]
     */
    @JsonProperty("auto_confirm")
    public void setAuto_confirm(String  auto_confirm){
        this.auto_confirm = auto_confirm ;
        this.auto_confirmDirtyFlag = true ;
    }

     /**
     * 获取 [自动确认注册]脏标记
     */
    @JsonIgnore
    public boolean getAuto_confirmDirtyFlag(){
        return this.auto_confirmDirtyFlag ;
    }   

    /**
     * 获取 [徽章背面]
     */
    @JsonProperty("badge_back")
    public String getBadge_back(){
        return this.badge_back ;
    }

    /**
     * 设置 [徽章背面]
     */
    @JsonProperty("badge_back")
    public void setBadge_back(String  badge_back){
        this.badge_back = badge_back ;
        this.badge_backDirtyFlag = true ;
    }

     /**
     * 获取 [徽章背面]脏标记
     */
    @JsonIgnore
    public boolean getBadge_backDirtyFlag(){
        return this.badge_backDirtyFlag ;
    }   

    /**
     * 获取 [徽章字体]
     */
    @JsonProperty("badge_front")
    public String getBadge_front(){
        return this.badge_front ;
    }

    /**
     * 设置 [徽章字体]
     */
    @JsonProperty("badge_front")
    public void setBadge_front(String  badge_front){
        this.badge_front = badge_front ;
        this.badge_frontDirtyFlag = true ;
    }

     /**
     * 获取 [徽章字体]脏标记
     */
    @JsonIgnore
    public boolean getBadge_frontDirtyFlag(){
        return this.badge_frontDirtyFlag ;
    }   

    /**
     * 获取 [徽章内部左边]
     */
    @JsonProperty("badge_innerleft")
    public String getBadge_innerleft(){
        return this.badge_innerleft ;
    }

    /**
     * 设置 [徽章内部左边]
     */
    @JsonProperty("badge_innerleft")
    public void setBadge_innerleft(String  badge_innerleft){
        this.badge_innerleft = badge_innerleft ;
        this.badge_innerleftDirtyFlag = true ;
    }

     /**
     * 获取 [徽章内部左边]脏标记
     */
    @JsonIgnore
    public boolean getBadge_innerleftDirtyFlag(){
        return this.badge_innerleftDirtyFlag ;
    }   

    /**
     * 获取 [徽章内部右边]
     */
    @JsonProperty("badge_innerright")
    public String getBadge_innerright(){
        return this.badge_innerright ;
    }

    /**
     * 设置 [徽章内部右边]
     */
    @JsonProperty("badge_innerright")
    public void setBadge_innerright(String  badge_innerright){
        this.badge_innerright = badge_innerright ;
        this.badge_innerrightDirtyFlag = true ;
    }

     /**
     * 获取 [徽章内部右边]脏标记
     */
    @JsonIgnore
    public boolean getBadge_innerrightDirtyFlag(){
        return this.badge_innerrightDirtyFlag ;
    }   

    /**
     * 获取 [看板颜色索引]
     */
    @JsonProperty("color")
    public Integer getColor(){
        return this.color ;
    }

    /**
     * 设置 [看板颜色索引]
     */
    @JsonProperty("color")
    public void setColor(Integer  color){
        this.color = color ;
        this.colorDirtyFlag = true ;
    }

     /**
     * 获取 [看板颜色索引]脏标记
     */
    @JsonIgnore
    public boolean getColorDirtyFlag(){
        return this.colorDirtyFlag ;
    }   

    /**
     * 获取 [公司]
     */
    @JsonProperty("company_id")
    public Integer getCompany_id(){
        return this.company_id ;
    }

    /**
     * 设置 [公司]
     */
    @JsonProperty("company_id")
    public void setCompany_id(Integer  company_id){
        this.company_id = company_id ;
        this.company_idDirtyFlag = true ;
    }

     /**
     * 获取 [公司]脏标记
     */
    @JsonIgnore
    public boolean getCompany_idDirtyFlag(){
        return this.company_idDirtyFlag ;
    }   

    /**
     * 获取 [公司]
     */
    @JsonProperty("company_id_text")
    public String getCompany_id_text(){
        return this.company_id_text ;
    }

    /**
     * 设置 [公司]
     */
    @JsonProperty("company_id_text")
    public void setCompany_id_text(String  company_id_text){
        this.company_id_text = company_id_text ;
        this.company_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [公司]脏标记
     */
    @JsonIgnore
    public boolean getCompany_id_textDirtyFlag(){
        return this.company_id_textDirtyFlag ;
    }   

    /**
     * 获取 [国家]
     */
    @JsonProperty("country_id")
    public Integer getCountry_id(){
        return this.country_id ;
    }

    /**
     * 设置 [国家]
     */
    @JsonProperty("country_id")
    public void setCountry_id(Integer  country_id){
        this.country_id = country_id ;
        this.country_idDirtyFlag = true ;
    }

     /**
     * 获取 [国家]脏标记
     */
    @JsonIgnore
    public boolean getCountry_idDirtyFlag(){
        return this.country_idDirtyFlag ;
    }   

    /**
     * 获取 [国家]
     */
    @JsonProperty("country_id_text")
    public String getCountry_id_text(){
        return this.country_id_text ;
    }

    /**
     * 设置 [国家]
     */
    @JsonProperty("country_id_text")
    public void setCountry_id_text(String  country_id_text){
        this.country_id_text = country_id_text ;
        this.country_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [国家]脏标记
     */
    @JsonIgnore
    public boolean getCountry_id_textDirtyFlag(){
        return this.country_id_textDirtyFlag ;
    }   

    /**
     * 获取 [创建时间]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return this.create_date ;
    }

    /**
     * 设置 [创建时间]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

     /**
     * 获取 [创建时间]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return this.create_dateDirtyFlag ;
    }   

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return this.create_uid ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

     /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return this.create_uidDirtyFlag ;
    }   

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return this.create_uid_text ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

     /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return this.create_uid_textDirtyFlag ;
    }   

    /**
     * 获取 [开始日期]
     */
    @JsonProperty("date_begin")
    public Timestamp getDate_begin(){
        return this.date_begin ;
    }

    /**
     * 设置 [开始日期]
     */
    @JsonProperty("date_begin")
    public void setDate_begin(Timestamp  date_begin){
        this.date_begin = date_begin ;
        this.date_beginDirtyFlag = true ;
    }

     /**
     * 获取 [开始日期]脏标记
     */
    @JsonIgnore
    public boolean getDate_beginDirtyFlag(){
        return this.date_beginDirtyFlag ;
    }   

    /**
     * 获取 [定位开始日期]
     */
    @JsonProperty("date_begin_located")
    public String getDate_begin_located(){
        return this.date_begin_located ;
    }

    /**
     * 设置 [定位开始日期]
     */
    @JsonProperty("date_begin_located")
    public void setDate_begin_located(String  date_begin_located){
        this.date_begin_located = date_begin_located ;
        this.date_begin_locatedDirtyFlag = true ;
    }

     /**
     * 获取 [定位开始日期]脏标记
     */
    @JsonIgnore
    public boolean getDate_begin_locatedDirtyFlag(){
        return this.date_begin_locatedDirtyFlag ;
    }   

    /**
     * 获取 [结束日期]
     */
    @JsonProperty("date_end")
    public Timestamp getDate_end(){
        return this.date_end ;
    }

    /**
     * 设置 [结束日期]
     */
    @JsonProperty("date_end")
    public void setDate_end(Timestamp  date_end){
        this.date_end = date_end ;
        this.date_endDirtyFlag = true ;
    }

     /**
     * 获取 [结束日期]脏标记
     */
    @JsonIgnore
    public boolean getDate_endDirtyFlag(){
        return this.date_endDirtyFlag ;
    }   

    /**
     * 获取 [定位最后日期]
     */
    @JsonProperty("date_end_located")
    public String getDate_end_located(){
        return this.date_end_located ;
    }

    /**
     * 设置 [定位最后日期]
     */
    @JsonProperty("date_end_located")
    public void setDate_end_located(String  date_end_located){
        this.date_end_located = date_end_located ;
        this.date_end_locatedDirtyFlag = true ;
    }

     /**
     * 获取 [定位最后日期]脏标记
     */
    @JsonIgnore
    public boolean getDate_end_locatedDirtyFlag(){
        return this.date_end_locatedDirtyFlag ;
    }   

    /**
     * 获取 [时区]
     */
    @JsonProperty("date_tz")
    public String getDate_tz(){
        return this.date_tz ;
    }

    /**
     * 设置 [时区]
     */
    @JsonProperty("date_tz")
    public void setDate_tz(String  date_tz){
        this.date_tz = date_tz ;
        this.date_tzDirtyFlag = true ;
    }

     /**
     * 获取 [时区]脏标记
     */
    @JsonIgnore
    public boolean getDate_tzDirtyFlag(){
        return this.date_tzDirtyFlag ;
    }   

    /**
     * 获取 [说明]
     */
    @JsonProperty("description")
    public String getDescription(){
        return this.description ;
    }

    /**
     * 设置 [说明]
     */
    @JsonProperty("description")
    public void setDescription(String  description){
        this.description = description ;
        this.descriptionDirtyFlag = true ;
    }

     /**
     * 获取 [说明]脏标记
     */
    @JsonIgnore
    public boolean getDescriptionDirtyFlag(){
        return this.descriptionDirtyFlag ;
    }   

    /**
     * 获取 [显示名称]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return this.display_name ;
    }

    /**
     * 设置 [显示名称]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

     /**
     * 获取 [显示名称]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return this.display_nameDirtyFlag ;
    }   

    /**
     * 获取 [事件图标]
     */
    @JsonProperty("event_logo")
    public String getEvent_logo(){
        return this.event_logo ;
    }

    /**
     * 设置 [事件图标]
     */
    @JsonProperty("event_logo")
    public void setEvent_logo(String  event_logo){
        this.event_logo = event_logo ;
        this.event_logoDirtyFlag = true ;
    }

     /**
     * 获取 [事件图标]脏标记
     */
    @JsonIgnore
    public boolean getEvent_logoDirtyFlag(){
        return this.event_logoDirtyFlag ;
    }   

    /**
     * 获取 [邮件排程]
     */
    @JsonProperty("event_mail_ids")
    public String getEvent_mail_ids(){
        return this.event_mail_ids ;
    }

    /**
     * 设置 [邮件排程]
     */
    @JsonProperty("event_mail_ids")
    public void setEvent_mail_ids(String  event_mail_ids){
        this.event_mail_ids = event_mail_ids ;
        this.event_mail_idsDirtyFlag = true ;
    }

     /**
     * 获取 [邮件排程]脏标记
     */
    @JsonIgnore
    public boolean getEvent_mail_idsDirtyFlag(){
        return this.event_mail_idsDirtyFlag ;
    }   

    /**
     * 获取 [活动入场券]
     */
    @JsonProperty("event_ticket_ids")
    public String getEvent_ticket_ids(){
        return this.event_ticket_ids ;
    }

    /**
     * 设置 [活动入场券]
     */
    @JsonProperty("event_ticket_ids")
    public void setEvent_ticket_ids(String  event_ticket_ids){
        this.event_ticket_ids = event_ticket_ids ;
        this.event_ticket_idsDirtyFlag = true ;
    }

     /**
     * 获取 [活动入场券]脏标记
     */
    @JsonIgnore
    public boolean getEvent_ticket_idsDirtyFlag(){
        return this.event_ticket_idsDirtyFlag ;
    }   

    /**
     * 获取 [类别]
     */
    @JsonProperty("event_type_id")
    public Integer getEvent_type_id(){
        return this.event_type_id ;
    }

    /**
     * 设置 [类别]
     */
    @JsonProperty("event_type_id")
    public void setEvent_type_id(Integer  event_type_id){
        this.event_type_id = event_type_id ;
        this.event_type_idDirtyFlag = true ;
    }

     /**
     * 获取 [类别]脏标记
     */
    @JsonIgnore
    public boolean getEvent_type_idDirtyFlag(){
        return this.event_type_idDirtyFlag ;
    }   

    /**
     * 获取 [类别]
     */
    @JsonProperty("event_type_id_text")
    public String getEvent_type_id_text(){
        return this.event_type_id_text ;
    }

    /**
     * 设置 [类别]
     */
    @JsonProperty("event_type_id_text")
    public void setEvent_type_id_text(String  event_type_id_text){
        this.event_type_id_text = event_type_id_text ;
        this.event_type_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [类别]脏标记
     */
    @JsonIgnore
    public boolean getEvent_type_id_textDirtyFlag(){
        return this.event_type_id_textDirtyFlag ;
    }   

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return this.id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

     /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return this.idDirtyFlag ;
    }   

    /**
     * 获取 [在线活动]
     */
    @JsonProperty("is_online")
    public String getIs_online(){
        return this.is_online ;
    }

    /**
     * 设置 [在线活动]
     */
    @JsonProperty("is_online")
    public void setIs_online(String  is_online){
        this.is_online = is_online ;
        this.is_onlineDirtyFlag = true ;
    }

     /**
     * 获取 [在线活动]脏标记
     */
    @JsonIgnore
    public boolean getIs_onlineDirtyFlag(){
        return this.is_onlineDirtyFlag ;
    }   

    /**
     * 获取 [正在参加]
     */
    @JsonProperty("is_participating")
    public String getIs_participating(){
        return this.is_participating ;
    }

    /**
     * 设置 [正在参加]
     */
    @JsonProperty("is_participating")
    public void setIs_participating(String  is_participating){
        this.is_participating = is_participating ;
        this.is_participatingDirtyFlag = true ;
    }

     /**
     * 获取 [正在参加]脏标记
     */
    @JsonIgnore
    public boolean getIs_participatingDirtyFlag(){
        return this.is_participatingDirtyFlag ;
    }   

    /**
     * 获取 [已发布]
     */
    @JsonProperty("is_published")
    public String getIs_published(){
        return this.is_published ;
    }

    /**
     * 设置 [已发布]
     */
    @JsonProperty("is_published")
    public void setIs_published(String  is_published){
        this.is_published = is_published ;
        this.is_publishedDirtyFlag = true ;
    }

     /**
     * 获取 [已发布]脏标记
     */
    @JsonIgnore
    public boolean getIs_publishedDirtyFlag(){
        return this.is_publishedDirtyFlag ;
    }   

    /**
     * 获取 [SEO优化]
     */
    @JsonProperty("is_seo_optimized")
    public String getIs_seo_optimized(){
        return this.is_seo_optimized ;
    }

    /**
     * 设置 [SEO优化]
     */
    @JsonProperty("is_seo_optimized")
    public void setIs_seo_optimized(String  is_seo_optimized){
        this.is_seo_optimized = is_seo_optimized ;
        this.is_seo_optimizedDirtyFlag = true ;
    }

     /**
     * 获取 [SEO优化]脏标记
     */
    @JsonIgnore
    public boolean getIs_seo_optimizedDirtyFlag(){
        return this.is_seo_optimizedDirtyFlag ;
    }   

    /**
     * 获取 [活动菜单]
     */
    @JsonProperty("menu_id")
    public Integer getMenu_id(){
        return this.menu_id ;
    }

    /**
     * 设置 [活动菜单]
     */
    @JsonProperty("menu_id")
    public void setMenu_id(Integer  menu_id){
        this.menu_id = menu_id ;
        this.menu_idDirtyFlag = true ;
    }

     /**
     * 获取 [活动菜单]脏标记
     */
    @JsonIgnore
    public boolean getMenu_idDirtyFlag(){
        return this.menu_idDirtyFlag ;
    }   

    /**
     * 获取 [活动菜单]
     */
    @JsonProperty("menu_id_text")
    public String getMenu_id_text(){
        return this.menu_id_text ;
    }

    /**
     * 设置 [活动菜单]
     */
    @JsonProperty("menu_id_text")
    public void setMenu_id_text(String  menu_id_text){
        this.menu_id_text = menu_id_text ;
        this.menu_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [活动菜单]脏标记
     */
    @JsonIgnore
    public boolean getMenu_id_textDirtyFlag(){
        return this.menu_id_textDirtyFlag ;
    }   

    /**
     * 获取 [附件数量]
     */
    @JsonProperty("message_attachment_count")
    public Integer getMessage_attachment_count(){
        return this.message_attachment_count ;
    }

    /**
     * 设置 [附件数量]
     */
    @JsonProperty("message_attachment_count")
    public void setMessage_attachment_count(Integer  message_attachment_count){
        this.message_attachment_count = message_attachment_count ;
        this.message_attachment_countDirtyFlag = true ;
    }

     /**
     * 获取 [附件数量]脏标记
     */
    @JsonIgnore
    public boolean getMessage_attachment_countDirtyFlag(){
        return this.message_attachment_countDirtyFlag ;
    }   

    /**
     * 获取 [关注者(渠道)]
     */
    @JsonProperty("message_channel_ids")
    public String getMessage_channel_ids(){
        return this.message_channel_ids ;
    }

    /**
     * 设置 [关注者(渠道)]
     */
    @JsonProperty("message_channel_ids")
    public void setMessage_channel_ids(String  message_channel_ids){
        this.message_channel_ids = message_channel_ids ;
        this.message_channel_idsDirtyFlag = true ;
    }

     /**
     * 获取 [关注者(渠道)]脏标记
     */
    @JsonIgnore
    public boolean getMessage_channel_idsDirtyFlag(){
        return this.message_channel_idsDirtyFlag ;
    }   

    /**
     * 获取 [关注者]
     */
    @JsonProperty("message_follower_ids")
    public String getMessage_follower_ids(){
        return this.message_follower_ids ;
    }

    /**
     * 设置 [关注者]
     */
    @JsonProperty("message_follower_ids")
    public void setMessage_follower_ids(String  message_follower_ids){
        this.message_follower_ids = message_follower_ids ;
        this.message_follower_idsDirtyFlag = true ;
    }

     /**
     * 获取 [关注者]脏标记
     */
    @JsonIgnore
    public boolean getMessage_follower_idsDirtyFlag(){
        return this.message_follower_idsDirtyFlag ;
    }   

    /**
     * 获取 [消息递送错误]
     */
    @JsonProperty("message_has_error")
    public String getMessage_has_error(){
        return this.message_has_error ;
    }

    /**
     * 设置 [消息递送错误]
     */
    @JsonProperty("message_has_error")
    public void setMessage_has_error(String  message_has_error){
        this.message_has_error = message_has_error ;
        this.message_has_errorDirtyFlag = true ;
    }

     /**
     * 获取 [消息递送错误]脏标记
     */
    @JsonIgnore
    public boolean getMessage_has_errorDirtyFlag(){
        return this.message_has_errorDirtyFlag ;
    }   

    /**
     * 获取 [错误数]
     */
    @JsonProperty("message_has_error_counter")
    public Integer getMessage_has_error_counter(){
        return this.message_has_error_counter ;
    }

    /**
     * 设置 [错误数]
     */
    @JsonProperty("message_has_error_counter")
    public void setMessage_has_error_counter(Integer  message_has_error_counter){
        this.message_has_error_counter = message_has_error_counter ;
        this.message_has_error_counterDirtyFlag = true ;
    }

     /**
     * 获取 [错误数]脏标记
     */
    @JsonIgnore
    public boolean getMessage_has_error_counterDirtyFlag(){
        return this.message_has_error_counterDirtyFlag ;
    }   

    /**
     * 获取 [消息]
     */
    @JsonProperty("message_ids")
    public String getMessage_ids(){
        return this.message_ids ;
    }

    /**
     * 设置 [消息]
     */
    @JsonProperty("message_ids")
    public void setMessage_ids(String  message_ids){
        this.message_ids = message_ids ;
        this.message_idsDirtyFlag = true ;
    }

     /**
     * 获取 [消息]脏标记
     */
    @JsonIgnore
    public boolean getMessage_idsDirtyFlag(){
        return this.message_idsDirtyFlag ;
    }   

    /**
     * 获取 [是关注者]
     */
    @JsonProperty("message_is_follower")
    public String getMessage_is_follower(){
        return this.message_is_follower ;
    }

    /**
     * 设置 [是关注者]
     */
    @JsonProperty("message_is_follower")
    public void setMessage_is_follower(String  message_is_follower){
        this.message_is_follower = message_is_follower ;
        this.message_is_followerDirtyFlag = true ;
    }

     /**
     * 获取 [是关注者]脏标记
     */
    @JsonIgnore
    public boolean getMessage_is_followerDirtyFlag(){
        return this.message_is_followerDirtyFlag ;
    }   

    /**
     * 获取 [附件]
     */
    @JsonProperty("message_main_attachment_id")
    public Integer getMessage_main_attachment_id(){
        return this.message_main_attachment_id ;
    }

    /**
     * 设置 [附件]
     */
    @JsonProperty("message_main_attachment_id")
    public void setMessage_main_attachment_id(Integer  message_main_attachment_id){
        this.message_main_attachment_id = message_main_attachment_id ;
        this.message_main_attachment_idDirtyFlag = true ;
    }

     /**
     * 获取 [附件]脏标记
     */
    @JsonIgnore
    public boolean getMessage_main_attachment_idDirtyFlag(){
        return this.message_main_attachment_idDirtyFlag ;
    }   

    /**
     * 获取 [需要采取行动]
     */
    @JsonProperty("message_needaction")
    public String getMessage_needaction(){
        return this.message_needaction ;
    }

    /**
     * 设置 [需要采取行动]
     */
    @JsonProperty("message_needaction")
    public void setMessage_needaction(String  message_needaction){
        this.message_needaction = message_needaction ;
        this.message_needactionDirtyFlag = true ;
    }

     /**
     * 获取 [需要采取行动]脏标记
     */
    @JsonIgnore
    public boolean getMessage_needactionDirtyFlag(){
        return this.message_needactionDirtyFlag ;
    }   

    /**
     * 获取 [行动数量]
     */
    @JsonProperty("message_needaction_counter")
    public Integer getMessage_needaction_counter(){
        return this.message_needaction_counter ;
    }

    /**
     * 设置 [行动数量]
     */
    @JsonProperty("message_needaction_counter")
    public void setMessage_needaction_counter(Integer  message_needaction_counter){
        this.message_needaction_counter = message_needaction_counter ;
        this.message_needaction_counterDirtyFlag = true ;
    }

     /**
     * 获取 [行动数量]脏标记
     */
    @JsonIgnore
    public boolean getMessage_needaction_counterDirtyFlag(){
        return this.message_needaction_counterDirtyFlag ;
    }   

    /**
     * 获取 [关注者(业务伙伴)]
     */
    @JsonProperty("message_partner_ids")
    public String getMessage_partner_ids(){
        return this.message_partner_ids ;
    }

    /**
     * 设置 [关注者(业务伙伴)]
     */
    @JsonProperty("message_partner_ids")
    public void setMessage_partner_ids(String  message_partner_ids){
        this.message_partner_ids = message_partner_ids ;
        this.message_partner_idsDirtyFlag = true ;
    }

     /**
     * 获取 [关注者(业务伙伴)]脏标记
     */
    @JsonIgnore
    public boolean getMessage_partner_idsDirtyFlag(){
        return this.message_partner_idsDirtyFlag ;
    }   

    /**
     * 获取 [未读消息]
     */
    @JsonProperty("message_unread")
    public String getMessage_unread(){
        return this.message_unread ;
    }

    /**
     * 设置 [未读消息]
     */
    @JsonProperty("message_unread")
    public void setMessage_unread(String  message_unread){
        this.message_unread = message_unread ;
        this.message_unreadDirtyFlag = true ;
    }

     /**
     * 获取 [未读消息]脏标记
     */
    @JsonIgnore
    public boolean getMessage_unreadDirtyFlag(){
        return this.message_unreadDirtyFlag ;
    }   

    /**
     * 获取 [未读消息计数器]
     */
    @JsonProperty("message_unread_counter")
    public Integer getMessage_unread_counter(){
        return this.message_unread_counter ;
    }

    /**
     * 设置 [未读消息计数器]
     */
    @JsonProperty("message_unread_counter")
    public void setMessage_unread_counter(Integer  message_unread_counter){
        this.message_unread_counter = message_unread_counter ;
        this.message_unread_counterDirtyFlag = true ;
    }

     /**
     * 获取 [未读消息计数器]脏标记
     */
    @JsonIgnore
    public boolean getMessage_unread_counterDirtyFlag(){
        return this.message_unread_counterDirtyFlag ;
    }   

    /**
     * 获取 [活动]
     */
    @JsonProperty("name")
    public String getName(){
        return this.name ;
    }

    /**
     * 设置 [活动]
     */
    @JsonProperty("name")
    public void setName(String  name){
        this.name = name ;
        this.nameDirtyFlag = true ;
    }

     /**
     * 获取 [活动]脏标记
     */
    @JsonIgnore
    public boolean getNameDirtyFlag(){
        return this.nameDirtyFlag ;
    }   

    /**
     * 获取 [组织者]
     */
    @JsonProperty("organizer_id")
    public Integer getOrganizer_id(){
        return this.organizer_id ;
    }

    /**
     * 设置 [组织者]
     */
    @JsonProperty("organizer_id")
    public void setOrganizer_id(Integer  organizer_id){
        this.organizer_id = organizer_id ;
        this.organizer_idDirtyFlag = true ;
    }

     /**
     * 获取 [组织者]脏标记
     */
    @JsonIgnore
    public boolean getOrganizer_idDirtyFlag(){
        return this.organizer_idDirtyFlag ;
    }   

    /**
     * 获取 [组织者]
     */
    @JsonProperty("organizer_id_text")
    public String getOrganizer_id_text(){
        return this.organizer_id_text ;
    }

    /**
     * 设置 [组织者]
     */
    @JsonProperty("organizer_id_text")
    public void setOrganizer_id_text(String  organizer_id_text){
        this.organizer_id_text = organizer_id_text ;
        this.organizer_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [组织者]脏标记
     */
    @JsonIgnore
    public boolean getOrganizer_id_textDirtyFlag(){
        return this.organizer_id_textDirtyFlag ;
    }   

    /**
     * 获取 [与会者]
     */
    @JsonProperty("registration_ids")
    public String getRegistration_ids(){
        return this.registration_ids ;
    }

    /**
     * 设置 [与会者]
     */
    @JsonProperty("registration_ids")
    public void setRegistration_ids(String  registration_ids){
        this.registration_ids = registration_ids ;
        this.registration_idsDirtyFlag = true ;
    }

     /**
     * 获取 [与会者]脏标记
     */
    @JsonIgnore
    public boolean getRegistration_idsDirtyFlag(){
        return this.registration_idsDirtyFlag ;
    }   

    /**
     * 获取 [与会者最多人数]
     */
    @JsonProperty("seats_availability")
    public String getSeats_availability(){
        return this.seats_availability ;
    }

    /**
     * 设置 [与会者最多人数]
     */
    @JsonProperty("seats_availability")
    public void setSeats_availability(String  seats_availability){
        this.seats_availability = seats_availability ;
        this.seats_availabilityDirtyFlag = true ;
    }

     /**
     * 获取 [与会者最多人数]脏标记
     */
    @JsonIgnore
    public boolean getSeats_availabilityDirtyFlag(){
        return this.seats_availabilityDirtyFlag ;
    }   

    /**
     * 获取 [可用席位]
     */
    @JsonProperty("seats_available")
    public Integer getSeats_available(){
        return this.seats_available ;
    }

    /**
     * 设置 [可用席位]
     */
    @JsonProperty("seats_available")
    public void setSeats_available(Integer  seats_available){
        this.seats_available = seats_available ;
        this.seats_availableDirtyFlag = true ;
    }

     /**
     * 获取 [可用席位]脏标记
     */
    @JsonIgnore
    public boolean getSeats_availableDirtyFlag(){
        return this.seats_availableDirtyFlag ;
    }   

    /**
     * 获取 [预期的与会人员数量]
     */
    @JsonProperty("seats_expected")
    public Integer getSeats_expected(){
        return this.seats_expected ;
    }

    /**
     * 设置 [预期的与会人员数量]
     */
    @JsonProperty("seats_expected")
    public void setSeats_expected(Integer  seats_expected){
        this.seats_expected = seats_expected ;
        this.seats_expectedDirtyFlag = true ;
    }

     /**
     * 获取 [预期的与会人员数量]脏标记
     */
    @JsonIgnore
    public boolean getSeats_expectedDirtyFlag(){
        return this.seats_expectedDirtyFlag ;
    }   

    /**
     * 获取 [与会者最多人数]
     */
    @JsonProperty("seats_max")
    public Integer getSeats_max(){
        return this.seats_max ;
    }

    /**
     * 设置 [与会者最多人数]
     */
    @JsonProperty("seats_max")
    public void setSeats_max(Integer  seats_max){
        this.seats_max = seats_max ;
        this.seats_maxDirtyFlag = true ;
    }

     /**
     * 获取 [与会者最多人数]脏标记
     */
    @JsonIgnore
    public boolean getSeats_maxDirtyFlag(){
        return this.seats_maxDirtyFlag ;
    }   

    /**
     * 获取 [与会者最少数量]
     */
    @JsonProperty("seats_min")
    public Integer getSeats_min(){
        return this.seats_min ;
    }

    /**
     * 设置 [与会者最少数量]
     */
    @JsonProperty("seats_min")
    public void setSeats_min(Integer  seats_min){
        this.seats_min = seats_min ;
        this.seats_minDirtyFlag = true ;
    }

     /**
     * 获取 [与会者最少数量]脏标记
     */
    @JsonIgnore
    public boolean getSeats_minDirtyFlag(){
        return this.seats_minDirtyFlag ;
    }   

    /**
     * 获取 [预订席位]
     */
    @JsonProperty("seats_reserved")
    public Integer getSeats_reserved(){
        return this.seats_reserved ;
    }

    /**
     * 设置 [预订席位]
     */
    @JsonProperty("seats_reserved")
    public void setSeats_reserved(Integer  seats_reserved){
        this.seats_reserved = seats_reserved ;
        this.seats_reservedDirtyFlag = true ;
    }

     /**
     * 获取 [预订席位]脏标记
     */
    @JsonIgnore
    public boolean getSeats_reservedDirtyFlag(){
        return this.seats_reservedDirtyFlag ;
    }   

    /**
     * 获取 [未确认的席位预订]
     */
    @JsonProperty("seats_unconfirmed")
    public Integer getSeats_unconfirmed(){
        return this.seats_unconfirmed ;
    }

    /**
     * 设置 [未确认的席位预订]
     */
    @JsonProperty("seats_unconfirmed")
    public void setSeats_unconfirmed(Integer  seats_unconfirmed){
        this.seats_unconfirmed = seats_unconfirmed ;
        this.seats_unconfirmedDirtyFlag = true ;
    }

     /**
     * 获取 [未确认的席位预订]脏标记
     */
    @JsonIgnore
    public boolean getSeats_unconfirmedDirtyFlag(){
        return this.seats_unconfirmedDirtyFlag ;
    }   

    /**
     * 获取 [参与者数目]
     */
    @JsonProperty("seats_used")
    public Integer getSeats_used(){
        return this.seats_used ;
    }

    /**
     * 设置 [参与者数目]
     */
    @JsonProperty("seats_used")
    public void setSeats_used(Integer  seats_used){
        this.seats_used = seats_used ;
        this.seats_usedDirtyFlag = true ;
    }

     /**
     * 获取 [参与者数目]脏标记
     */
    @JsonIgnore
    public boolean getSeats_usedDirtyFlag(){
        return this.seats_usedDirtyFlag ;
    }   

    /**
     * 获取 [状态]
     */
    @JsonProperty("state")
    public String getState(){
        return this.state ;
    }

    /**
     * 设置 [状态]
     */
    @JsonProperty("state")
    public void setState(String  state){
        this.state = state ;
        this.stateDirtyFlag = true ;
    }

     /**
     * 获取 [状态]脏标记
     */
    @JsonIgnore
    public boolean getStateDirtyFlag(){
        return this.stateDirtyFlag ;
    }   

    /**
     * 获取 [Twitter主题标签]
     */
    @JsonProperty("twitter_hashtag")
    public String getTwitter_hashtag(){
        return this.twitter_hashtag ;
    }

    /**
     * 设置 [Twitter主题标签]
     */
    @JsonProperty("twitter_hashtag")
    public void setTwitter_hashtag(String  twitter_hashtag){
        this.twitter_hashtag = twitter_hashtag ;
        this.twitter_hashtagDirtyFlag = true ;
    }

     /**
     * 获取 [Twitter主题标签]脏标记
     */
    @JsonIgnore
    public boolean getTwitter_hashtagDirtyFlag(){
        return this.twitter_hashtagDirtyFlag ;
    }   

    /**
     * 获取 [负责人]
     */
    @JsonProperty("user_id")
    public Integer getUser_id(){
        return this.user_id ;
    }

    /**
     * 设置 [负责人]
     */
    @JsonProperty("user_id")
    public void setUser_id(Integer  user_id){
        this.user_id = user_id ;
        this.user_idDirtyFlag = true ;
    }

     /**
     * 获取 [负责人]脏标记
     */
    @JsonIgnore
    public boolean getUser_idDirtyFlag(){
        return this.user_idDirtyFlag ;
    }   

    /**
     * 获取 [负责人]
     */
    @JsonProperty("user_id_text")
    public String getUser_id_text(){
        return this.user_id_text ;
    }

    /**
     * 设置 [负责人]
     */
    @JsonProperty("user_id_text")
    public void setUser_id_text(String  user_id_text){
        this.user_id_text = user_id_text ;
        this.user_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [负责人]脏标记
     */
    @JsonIgnore
    public boolean getUser_id_textDirtyFlag(){
        return this.user_id_textDirtyFlag ;
    }   

    /**
     * 获取 [网站]
     */
    @JsonProperty("website_id")
    public Integer getWebsite_id(){
        return this.website_id ;
    }

    /**
     * 设置 [网站]
     */
    @JsonProperty("website_id")
    public void setWebsite_id(Integer  website_id){
        this.website_id = website_id ;
        this.website_idDirtyFlag = true ;
    }

     /**
     * 获取 [网站]脏标记
     */
    @JsonIgnore
    public boolean getWebsite_idDirtyFlag(){
        return this.website_idDirtyFlag ;
    }   

    /**
     * 获取 [专用菜单]
     */
    @JsonProperty("website_menu")
    public String getWebsite_menu(){
        return this.website_menu ;
    }

    /**
     * 设置 [专用菜单]
     */
    @JsonProperty("website_menu")
    public void setWebsite_menu(String  website_menu){
        this.website_menu = website_menu ;
        this.website_menuDirtyFlag = true ;
    }

     /**
     * 获取 [专用菜单]脏标记
     */
    @JsonIgnore
    public boolean getWebsite_menuDirtyFlag(){
        return this.website_menuDirtyFlag ;
    }   

    /**
     * 获取 [网站消息]
     */
    @JsonProperty("website_message_ids")
    public String getWebsite_message_ids(){
        return this.website_message_ids ;
    }

    /**
     * 设置 [网站消息]
     */
    @JsonProperty("website_message_ids")
    public void setWebsite_message_ids(String  website_message_ids){
        this.website_message_ids = website_message_ids ;
        this.website_message_idsDirtyFlag = true ;
    }

     /**
     * 获取 [网站消息]脏标记
     */
    @JsonIgnore
    public boolean getWebsite_message_idsDirtyFlag(){
        return this.website_message_idsDirtyFlag ;
    }   

    /**
     * 获取 [网站元说明]
     */
    @JsonProperty("website_meta_description")
    public String getWebsite_meta_description(){
        return this.website_meta_description ;
    }

    /**
     * 设置 [网站元说明]
     */
    @JsonProperty("website_meta_description")
    public void setWebsite_meta_description(String  website_meta_description){
        this.website_meta_description = website_meta_description ;
        this.website_meta_descriptionDirtyFlag = true ;
    }

     /**
     * 获取 [网站元说明]脏标记
     */
    @JsonIgnore
    public boolean getWebsite_meta_descriptionDirtyFlag(){
        return this.website_meta_descriptionDirtyFlag ;
    }   

    /**
     * 获取 [网站meta关键词]
     */
    @JsonProperty("website_meta_keywords")
    public String getWebsite_meta_keywords(){
        return this.website_meta_keywords ;
    }

    /**
     * 设置 [网站meta关键词]
     */
    @JsonProperty("website_meta_keywords")
    public void setWebsite_meta_keywords(String  website_meta_keywords){
        this.website_meta_keywords = website_meta_keywords ;
        this.website_meta_keywordsDirtyFlag = true ;
    }

     /**
     * 获取 [网站meta关键词]脏标记
     */
    @JsonIgnore
    public boolean getWebsite_meta_keywordsDirtyFlag(){
        return this.website_meta_keywordsDirtyFlag ;
    }   

    /**
     * 获取 [网站opengraph图像]
     */
    @JsonProperty("website_meta_og_img")
    public String getWebsite_meta_og_img(){
        return this.website_meta_og_img ;
    }

    /**
     * 设置 [网站opengraph图像]
     */
    @JsonProperty("website_meta_og_img")
    public void setWebsite_meta_og_img(String  website_meta_og_img){
        this.website_meta_og_img = website_meta_og_img ;
        this.website_meta_og_imgDirtyFlag = true ;
    }

     /**
     * 获取 [网站opengraph图像]脏标记
     */
    @JsonIgnore
    public boolean getWebsite_meta_og_imgDirtyFlag(){
        return this.website_meta_og_imgDirtyFlag ;
    }   

    /**
     * 获取 [网站meta标题]
     */
    @JsonProperty("website_meta_title")
    public String getWebsite_meta_title(){
        return this.website_meta_title ;
    }

    /**
     * 设置 [网站meta标题]
     */
    @JsonProperty("website_meta_title")
    public void setWebsite_meta_title(String  website_meta_title){
        this.website_meta_title = website_meta_title ;
        this.website_meta_titleDirtyFlag = true ;
    }

     /**
     * 获取 [网站meta标题]脏标记
     */
    @JsonIgnore
    public boolean getWebsite_meta_titleDirtyFlag(){
        return this.website_meta_titleDirtyFlag ;
    }   

    /**
     * 获取 [在当前网站显示]
     */
    @JsonProperty("website_published")
    public String getWebsite_published(){
        return this.website_published ;
    }

    /**
     * 设置 [在当前网站显示]
     */
    @JsonProperty("website_published")
    public void setWebsite_published(String  website_published){
        this.website_published = website_published ;
        this.website_publishedDirtyFlag = true ;
    }

     /**
     * 获取 [在当前网站显示]脏标记
     */
    @JsonIgnore
    public boolean getWebsite_publishedDirtyFlag(){
        return this.website_publishedDirtyFlag ;
    }   

    /**
     * 获取 [网站网址]
     */
    @JsonProperty("website_url")
    public String getWebsite_url(){
        return this.website_url ;
    }

    /**
     * 设置 [网站网址]
     */
    @JsonProperty("website_url")
    public void setWebsite_url(String  website_url){
        this.website_url = website_url ;
        this.website_urlDirtyFlag = true ;
    }

     /**
     * 获取 [网站网址]脏标记
     */
    @JsonIgnore
    public boolean getWebsite_urlDirtyFlag(){
        return this.website_urlDirtyFlag ;
    }   

    /**
     * 获取 [最后更新时间]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return this.write_date ;
    }

    /**
     * 设置 [最后更新时间]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新时间]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return this.write_dateDirtyFlag ;
    }   

    /**
     * 获取 [最后更新人]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return this.write_uid ;
    }

    /**
     * 设置 [最后更新人]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新人]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return this.write_uidDirtyFlag ;
    }   

    /**
     * 获取 [最后更新人]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return this.write_uid_text ;
    }

    /**
     * 设置 [最后更新人]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新人]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return this.write_uid_textDirtyFlag ;
    }   

    /**
     * 获取 [最后修改日]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return this.__last_update ;
    }

    /**
     * 设置 [最后修改日]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

     /**
     * 获取 [最后修改日]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return this.__last_updateDirtyFlag ;
    }   



    public void fromMap(Map<String, Object> map) throws Exception {
		if(map.get("active") instanceof Boolean){
			this.setActive(((Boolean)map.get("active"))? "true" : "false");
		}
		if(!(map.get("address_id") instanceof Boolean)&& map.get("address_id")!=null){
			Object[] objs = (Object[])map.get("address_id");
			if(objs.length > 0){
				this.setAddress_id((Integer)objs[0]);
			}
		}
		if(!(map.get("address_id") instanceof Boolean)&& map.get("address_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("address_id");
			if(objs.length > 1){
				this.setAddress_id_text((String)objs[1]);
			}
		}
		if(map.get("auto_confirm") instanceof Boolean){
			this.setAuto_confirm(((Boolean)map.get("auto_confirm"))? "true" : "false");
		}
		if(!(map.get("badge_back") instanceof Boolean)&& map.get("badge_back")!=null){
			this.setBadge_back((String)map.get("badge_back"));
		}
		if(!(map.get("badge_front") instanceof Boolean)&& map.get("badge_front")!=null){
			this.setBadge_front((String)map.get("badge_front"));
		}
		if(!(map.get("badge_innerleft") instanceof Boolean)&& map.get("badge_innerleft")!=null){
			this.setBadge_innerleft((String)map.get("badge_innerleft"));
		}
		if(!(map.get("badge_innerright") instanceof Boolean)&& map.get("badge_innerright")!=null){
			this.setBadge_innerright((String)map.get("badge_innerright"));
		}
		if(!(map.get("color") instanceof Boolean)&& map.get("color")!=null){
			this.setColor((Integer)map.get("color"));
		}
		if(!(map.get("company_id") instanceof Boolean)&& map.get("company_id")!=null){
			Object[] objs = (Object[])map.get("company_id");
			if(objs.length > 0){
				this.setCompany_id((Integer)objs[0]);
			}
		}
		if(!(map.get("company_id") instanceof Boolean)&& map.get("company_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("company_id");
			if(objs.length > 1){
				this.setCompany_id_text((String)objs[1]);
			}
		}
		if(!(map.get("country_id") instanceof Boolean)&& map.get("country_id")!=null){
			Object[] objs = (Object[])map.get("country_id");
			if(objs.length > 0){
				this.setCountry_id((Integer)objs[0]);
			}
		}
		if(!(map.get("country_id") instanceof Boolean)&& map.get("country_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("country_id");
			if(objs.length > 1){
				this.setCountry_id_text((String)objs[1]);
			}
		}
		if(!(map.get("create_date") instanceof Boolean)&& map.get("create_date")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse((String)map.get("create_date"));
   			this.setCreate_date(new Timestamp(parse.getTime()));
		}
		if(!(map.get("create_uid") instanceof Boolean)&& map.get("create_uid")!=null){
			Object[] objs = (Object[])map.get("create_uid");
			if(objs.length > 0){
				this.setCreate_uid((Integer)objs[0]);
			}
		}
		if(!(map.get("create_uid") instanceof Boolean)&& map.get("create_uid")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("create_uid");
			if(objs.length > 1){
				this.setCreate_uid_text((String)objs[1]);
			}
		}
		if(!(map.get("date_begin") instanceof Boolean)&& map.get("date_begin")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse((String)map.get("date_begin"));
   			this.setDate_begin(new Timestamp(parse.getTime()));
		}
		if(!(map.get("date_begin_located") instanceof Boolean)&& map.get("date_begin_located")!=null){
			this.setDate_begin_located((String)map.get("date_begin_located"));
		}
		if(!(map.get("date_end") instanceof Boolean)&& map.get("date_end")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse((String)map.get("date_end"));
   			this.setDate_end(new Timestamp(parse.getTime()));
		}
		if(!(map.get("date_end_located") instanceof Boolean)&& map.get("date_end_located")!=null){
			this.setDate_end_located((String)map.get("date_end_located"));
		}
		if(!(map.get("date_tz") instanceof Boolean)&& map.get("date_tz")!=null){
			this.setDate_tz((String)map.get("date_tz"));
		}
		if(!(map.get("description") instanceof Boolean)&& map.get("description")!=null){
			this.setDescription((String)map.get("description"));
		}
		if(!(map.get("display_name") instanceof Boolean)&& map.get("display_name")!=null){
			this.setDisplay_name((String)map.get("display_name"));
		}
		if(!(map.get("event_logo") instanceof Boolean)&& map.get("event_logo")!=null){
			this.setEvent_logo((String)map.get("event_logo"));
		}
		if(!(map.get("event_mail_ids") instanceof Boolean)&& map.get("event_mail_ids")!=null){
			Object[] objs = (Object[])map.get("event_mail_ids");
			if(objs.length > 0){
				Integer[] event_mail_ids = Arrays.copyOfRange(objs,0,objs.length,Integer[].class);
				this.setEvent_mail_ids(Arrays.toString(event_mail_ids));
			}
		}
		if(!(map.get("event_ticket_ids") instanceof Boolean)&& map.get("event_ticket_ids")!=null){
			Object[] objs = (Object[])map.get("event_ticket_ids");
			if(objs.length > 0){
				Integer[] event_ticket_ids = Arrays.copyOfRange(objs,0,objs.length,Integer[].class);
				this.setEvent_ticket_ids(Arrays.toString(event_ticket_ids));
			}
		}
		if(!(map.get("event_type_id") instanceof Boolean)&& map.get("event_type_id")!=null){
			Object[] objs = (Object[])map.get("event_type_id");
			if(objs.length > 0){
				this.setEvent_type_id((Integer)objs[0]);
			}
		}
		if(!(map.get("event_type_id") instanceof Boolean)&& map.get("event_type_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("event_type_id");
			if(objs.length > 1){
				this.setEvent_type_id_text((String)objs[1]);
			}
		}
		if(!(map.get("id") instanceof Boolean)&& map.get("id")!=null){
			this.setId((Integer)map.get("id"));
		}
		if(map.get("is_online") instanceof Boolean){
			this.setIs_online(((Boolean)map.get("is_online"))? "true" : "false");
		}
		if(map.get("is_participating") instanceof Boolean){
			this.setIs_participating(((Boolean)map.get("is_participating"))? "true" : "false");
		}
		if(map.get("is_published") instanceof Boolean){
			this.setIs_published(((Boolean)map.get("is_published"))? "true" : "false");
		}
		if(map.get("is_seo_optimized") instanceof Boolean){
			this.setIs_seo_optimized(((Boolean)map.get("is_seo_optimized"))? "true" : "false");
		}
		if(!(map.get("menu_id") instanceof Boolean)&& map.get("menu_id")!=null){
			Object[] objs = (Object[])map.get("menu_id");
			if(objs.length > 0){
				this.setMenu_id((Integer)objs[0]);
			}
		}
		if(!(map.get("menu_id") instanceof Boolean)&& map.get("menu_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("menu_id");
			if(objs.length > 1){
				this.setMenu_id_text((String)objs[1]);
			}
		}
		if(!(map.get("message_attachment_count") instanceof Boolean)&& map.get("message_attachment_count")!=null){
			this.setMessage_attachment_count((Integer)map.get("message_attachment_count"));
		}
		if(!(map.get("message_channel_ids") instanceof Boolean)&& map.get("message_channel_ids")!=null){
			Object[] objs = (Object[])map.get("message_channel_ids");
			if(objs.length > 0){
				Integer[] message_channel_ids = Arrays.copyOfRange(objs,0,objs.length,Integer[].class);
				this.setMessage_channel_ids(Arrays.toString(message_channel_ids));
			}
		}
		if(!(map.get("message_follower_ids") instanceof Boolean)&& map.get("message_follower_ids")!=null){
			Object[] objs = (Object[])map.get("message_follower_ids");
			if(objs.length > 0){
				Integer[] message_follower_ids = Arrays.copyOfRange(objs,0,objs.length,Integer[].class);
				this.setMessage_follower_ids(Arrays.toString(message_follower_ids));
			}
		}
		if(map.get("message_has_error") instanceof Boolean){
			this.setMessage_has_error(((Boolean)map.get("message_has_error"))? "true" : "false");
		}
		if(!(map.get("message_has_error_counter") instanceof Boolean)&& map.get("message_has_error_counter")!=null){
			this.setMessage_has_error_counter((Integer)map.get("message_has_error_counter"));
		}
		if(!(map.get("message_ids") instanceof Boolean)&& map.get("message_ids")!=null){
			Object[] objs = (Object[])map.get("message_ids");
			if(objs.length > 0){
				Integer[] message_ids = Arrays.copyOfRange(objs,0,objs.length,Integer[].class);
				this.setMessage_ids(Arrays.toString(message_ids));
			}
		}
		if(map.get("message_is_follower") instanceof Boolean){
			this.setMessage_is_follower(((Boolean)map.get("message_is_follower"))? "true" : "false");
		}
		if(!(map.get("message_main_attachment_id") instanceof Boolean)&& map.get("message_main_attachment_id")!=null){
			Object[] objs = (Object[])map.get("message_main_attachment_id");
			if(objs.length > 0){
				this.setMessage_main_attachment_id((Integer)objs[0]);
			}
		}
		if(map.get("message_needaction") instanceof Boolean){
			this.setMessage_needaction(((Boolean)map.get("message_needaction"))? "true" : "false");
		}
		if(!(map.get("message_needaction_counter") instanceof Boolean)&& map.get("message_needaction_counter")!=null){
			this.setMessage_needaction_counter((Integer)map.get("message_needaction_counter"));
		}
		if(!(map.get("message_partner_ids") instanceof Boolean)&& map.get("message_partner_ids")!=null){
			Object[] objs = (Object[])map.get("message_partner_ids");
			if(objs.length > 0){
				Integer[] message_partner_ids = Arrays.copyOfRange(objs,0,objs.length,Integer[].class);
				this.setMessage_partner_ids(Arrays.toString(message_partner_ids));
			}
		}
		if(map.get("message_unread") instanceof Boolean){
			this.setMessage_unread(((Boolean)map.get("message_unread"))? "true" : "false");
		}
		if(!(map.get("message_unread_counter") instanceof Boolean)&& map.get("message_unread_counter")!=null){
			this.setMessage_unread_counter((Integer)map.get("message_unread_counter"));
		}
		if(!(map.get("name") instanceof Boolean)&& map.get("name")!=null){
			this.setName((String)map.get("name"));
		}
		if(!(map.get("organizer_id") instanceof Boolean)&& map.get("organizer_id")!=null){
			Object[] objs = (Object[])map.get("organizer_id");
			if(objs.length > 0){
				this.setOrganizer_id((Integer)objs[0]);
			}
		}
		if(!(map.get("organizer_id") instanceof Boolean)&& map.get("organizer_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("organizer_id");
			if(objs.length > 1){
				this.setOrganizer_id_text((String)objs[1]);
			}
		}
		if(!(map.get("registration_ids") instanceof Boolean)&& map.get("registration_ids")!=null){
			Object[] objs = (Object[])map.get("registration_ids");
			if(objs.length > 0){
				Integer[] registration_ids = Arrays.copyOfRange(objs,0,objs.length,Integer[].class);
				this.setRegistration_ids(Arrays.toString(registration_ids));
			}
		}
		if(!(map.get("seats_availability") instanceof Boolean)&& map.get("seats_availability")!=null){
			this.setSeats_availability((String)map.get("seats_availability"));
		}
		if(!(map.get("seats_available") instanceof Boolean)&& map.get("seats_available")!=null){
			this.setSeats_available((Integer)map.get("seats_available"));
		}
		if(!(map.get("seats_expected") instanceof Boolean)&& map.get("seats_expected")!=null){
			this.setSeats_expected((Integer)map.get("seats_expected"));
		}
		if(!(map.get("seats_max") instanceof Boolean)&& map.get("seats_max")!=null){
			this.setSeats_max((Integer)map.get("seats_max"));
		}
		if(!(map.get("seats_min") instanceof Boolean)&& map.get("seats_min")!=null){
			this.setSeats_min((Integer)map.get("seats_min"));
		}
		if(!(map.get("seats_reserved") instanceof Boolean)&& map.get("seats_reserved")!=null){
			this.setSeats_reserved((Integer)map.get("seats_reserved"));
		}
		if(!(map.get("seats_unconfirmed") instanceof Boolean)&& map.get("seats_unconfirmed")!=null){
			this.setSeats_unconfirmed((Integer)map.get("seats_unconfirmed"));
		}
		if(!(map.get("seats_used") instanceof Boolean)&& map.get("seats_used")!=null){
			this.setSeats_used((Integer)map.get("seats_used"));
		}
		if(!(map.get("state") instanceof Boolean)&& map.get("state")!=null){
			this.setState((String)map.get("state"));
		}
		if(!(map.get("twitter_hashtag") instanceof Boolean)&& map.get("twitter_hashtag")!=null){
			this.setTwitter_hashtag((String)map.get("twitter_hashtag"));
		}
		if(!(map.get("user_id") instanceof Boolean)&& map.get("user_id")!=null){
			Object[] objs = (Object[])map.get("user_id");
			if(objs.length > 0){
				this.setUser_id((Integer)objs[0]);
			}
		}
		if(!(map.get("user_id") instanceof Boolean)&& map.get("user_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("user_id");
			if(objs.length > 1){
				this.setUser_id_text((String)objs[1]);
			}
		}
		if(!(map.get("website_id") instanceof Boolean)&& map.get("website_id")!=null){
			Object[] objs = (Object[])map.get("website_id");
			if(objs.length > 0){
				this.setWebsite_id((Integer)objs[0]);
			}
		}
		if(map.get("website_menu") instanceof Boolean){
			this.setWebsite_menu(((Boolean)map.get("website_menu"))? "true" : "false");
		}
		if(!(map.get("website_message_ids") instanceof Boolean)&& map.get("website_message_ids")!=null){
			Object[] objs = (Object[])map.get("website_message_ids");
			if(objs.length > 0){
				Integer[] website_message_ids = Arrays.copyOfRange(objs,0,objs.length,Integer[].class);
				this.setWebsite_message_ids(Arrays.toString(website_message_ids));
			}
		}
		if(!(map.get("website_meta_description") instanceof Boolean)&& map.get("website_meta_description")!=null){
			this.setWebsite_meta_description((String)map.get("website_meta_description"));
		}
		if(!(map.get("website_meta_keywords") instanceof Boolean)&& map.get("website_meta_keywords")!=null){
			this.setWebsite_meta_keywords((String)map.get("website_meta_keywords"));
		}
		if(!(map.get("website_meta_og_img") instanceof Boolean)&& map.get("website_meta_og_img")!=null){
			this.setWebsite_meta_og_img((String)map.get("website_meta_og_img"));
		}
		if(!(map.get("website_meta_title") instanceof Boolean)&& map.get("website_meta_title")!=null){
			this.setWebsite_meta_title((String)map.get("website_meta_title"));
		}
		if(map.get("website_published") instanceof Boolean){
			this.setWebsite_published(((Boolean)map.get("website_published"))? "true" : "false");
		}
		if(!(map.get("website_url") instanceof Boolean)&& map.get("website_url")!=null){
			this.setWebsite_url((String)map.get("website_url"));
		}
		if(!(map.get("write_date") instanceof Boolean)&& map.get("write_date")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse((String)map.get("write_date"));
   			this.setWrite_date(new Timestamp(parse.getTime()));
		}
		if(!(map.get("write_uid") instanceof Boolean)&& map.get("write_uid")!=null){
			Object[] objs = (Object[])map.get("write_uid");
			if(objs.length > 0){
				this.setWrite_uid((Integer)objs[0]);
			}
		}
		if(!(map.get("write_uid") instanceof Boolean)&& map.get("write_uid")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("write_uid");
			if(objs.length > 1){
				this.setWrite_uid_text((String)objs[1]);
			}
		}
		if(!(map.get("__last_update") instanceof Boolean)&& map.get("__last_update")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse((String)map.get("__last_update"));
   			this.set__last_update(new Timestamp(parse.getTime()));
		}
	}

	public Map<String, Object> toMap() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		if(this.getActive()!=null&&this.getActiveDirtyFlag()){
			map.put("active",Boolean.parseBoolean(this.getActive()));		
		}		if(this.getAddress_id()!=null&&this.getAddress_idDirtyFlag()){
			map.put("address_id",this.getAddress_id());
		}else if(this.getAddress_idDirtyFlag()){
			map.put("address_id",false);
		}
		if(this.getAddress_id_text()!=null&&this.getAddress_id_textDirtyFlag()){
			//忽略文本外键address_id_text
		}else if(this.getAddress_id_textDirtyFlag()){
			map.put("address_id",false);
		}
		if(this.getAuto_confirm()!=null&&this.getAuto_confirmDirtyFlag()){
			map.put("auto_confirm",Boolean.parseBoolean(this.getAuto_confirm()));		
		}		if(this.getBadge_back()!=null&&this.getBadge_backDirtyFlag()){
			map.put("badge_back",this.getBadge_back());
		}else if(this.getBadge_backDirtyFlag()){
			map.put("badge_back",false);
		}
		if(this.getBadge_front()!=null&&this.getBadge_frontDirtyFlag()){
			map.put("badge_front",this.getBadge_front());
		}else if(this.getBadge_frontDirtyFlag()){
			map.put("badge_front",false);
		}
		if(this.getBadge_innerleft()!=null&&this.getBadge_innerleftDirtyFlag()){
			map.put("badge_innerleft",this.getBadge_innerleft());
		}else if(this.getBadge_innerleftDirtyFlag()){
			map.put("badge_innerleft",false);
		}
		if(this.getBadge_innerright()!=null&&this.getBadge_innerrightDirtyFlag()){
			map.put("badge_innerright",this.getBadge_innerright());
		}else if(this.getBadge_innerrightDirtyFlag()){
			map.put("badge_innerright",false);
		}
		if(this.getColor()!=null&&this.getColorDirtyFlag()){
			map.put("color",this.getColor());
		}else if(this.getColorDirtyFlag()){
			map.put("color",false);
		}
		if(this.getCompany_id()!=null&&this.getCompany_idDirtyFlag()){
			map.put("company_id",this.getCompany_id());
		}else if(this.getCompany_idDirtyFlag()){
			map.put("company_id",false);
		}
		if(this.getCompany_id_text()!=null&&this.getCompany_id_textDirtyFlag()){
			//忽略文本外键company_id_text
		}else if(this.getCompany_id_textDirtyFlag()){
			map.put("company_id",false);
		}
		if(this.getCountry_id()!=null&&this.getCountry_idDirtyFlag()){
			map.put("country_id",this.getCountry_id());
		}else if(this.getCountry_idDirtyFlag()){
			map.put("country_id",false);
		}
		if(this.getCountry_id_text()!=null&&this.getCountry_id_textDirtyFlag()){
			//忽略文本外键country_id_text
		}else if(this.getCountry_id_textDirtyFlag()){
			map.put("country_id",false);
		}
		if(this.getCreate_date()!=null&&this.getCreate_dateDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String datetimeStr = sdf.format(this.getCreate_date());
			map.put("create_date",datetimeStr);
		}else if(this.getCreate_dateDirtyFlag()){
			map.put("create_date",false);
		}
		if(this.getCreate_uid()!=null&&this.getCreate_uidDirtyFlag()){
			map.put("create_uid",this.getCreate_uid());
		}else if(this.getCreate_uidDirtyFlag()){
			map.put("create_uid",false);
		}
		if(this.getCreate_uid_text()!=null&&this.getCreate_uid_textDirtyFlag()){
			//忽略文本外键create_uid_text
		}else if(this.getCreate_uid_textDirtyFlag()){
			map.put("create_uid",false);
		}
		if(this.getDate_begin()!=null&&this.getDate_beginDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String datetimeStr = sdf.format(this.getDate_begin());
			map.put("date_begin",datetimeStr);
		}else if(this.getDate_beginDirtyFlag()){
			map.put("date_begin",false);
		}
		if(this.getDate_begin_located()!=null&&this.getDate_begin_locatedDirtyFlag()){
			map.put("date_begin_located",this.getDate_begin_located());
		}else if(this.getDate_begin_locatedDirtyFlag()){
			map.put("date_begin_located",false);
		}
		if(this.getDate_end()!=null&&this.getDate_endDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String datetimeStr = sdf.format(this.getDate_end());
			map.put("date_end",datetimeStr);
		}else if(this.getDate_endDirtyFlag()){
			map.put("date_end",false);
		}
		if(this.getDate_end_located()!=null&&this.getDate_end_locatedDirtyFlag()){
			map.put("date_end_located",this.getDate_end_located());
		}else if(this.getDate_end_locatedDirtyFlag()){
			map.put("date_end_located",false);
		}
		if(this.getDate_tz()!=null&&this.getDate_tzDirtyFlag()){
			map.put("date_tz",this.getDate_tz());
		}else if(this.getDate_tzDirtyFlag()){
			map.put("date_tz",false);
		}
		if(this.getDescription()!=null&&this.getDescriptionDirtyFlag()){
			map.put("description",this.getDescription());
		}else if(this.getDescriptionDirtyFlag()){
			map.put("description",false);
		}
		if(this.getDisplay_name()!=null&&this.getDisplay_nameDirtyFlag()){
			map.put("display_name",this.getDisplay_name());
		}else if(this.getDisplay_nameDirtyFlag()){
			map.put("display_name",false);
		}
		if(this.getEvent_logo()!=null&&this.getEvent_logoDirtyFlag()){
			map.put("event_logo",this.getEvent_logo());
		}else if(this.getEvent_logoDirtyFlag()){
			map.put("event_logo",false);
		}
		if(this.getEvent_mail_ids()!=null&&this.getEvent_mail_idsDirtyFlag()){
			map.put("event_mail_ids",this.getEvent_mail_ids());
		}else if(this.getEvent_mail_idsDirtyFlag()){
			map.put("event_mail_ids",false);
		}
		if(this.getEvent_ticket_ids()!=null&&this.getEvent_ticket_idsDirtyFlag()){
			map.put("event_ticket_ids",this.getEvent_ticket_ids());
		}else if(this.getEvent_ticket_idsDirtyFlag()){
			map.put("event_ticket_ids",false);
		}
		if(this.getEvent_type_id()!=null&&this.getEvent_type_idDirtyFlag()){
			map.put("event_type_id",this.getEvent_type_id());
		}else if(this.getEvent_type_idDirtyFlag()){
			map.put("event_type_id",false);
		}
		if(this.getEvent_type_id_text()!=null&&this.getEvent_type_id_textDirtyFlag()){
			//忽略文本外键event_type_id_text
		}else if(this.getEvent_type_id_textDirtyFlag()){
			map.put("event_type_id",false);
		}
		if(this.getId()!=null&&this.getIdDirtyFlag()){
			map.put("id",this.getId());
		}else if(this.getIdDirtyFlag()){
			map.put("id",false);
		}
		if(this.getIs_online()!=null&&this.getIs_onlineDirtyFlag()){
			map.put("is_online",Boolean.parseBoolean(this.getIs_online()));		
		}		if(this.getIs_participating()!=null&&this.getIs_participatingDirtyFlag()){
			map.put("is_participating",Boolean.parseBoolean(this.getIs_participating()));		
		}		if(this.getIs_published()!=null&&this.getIs_publishedDirtyFlag()){
			map.put("is_published",Boolean.parseBoolean(this.getIs_published()));		
		}		if(this.getIs_seo_optimized()!=null&&this.getIs_seo_optimizedDirtyFlag()){
			map.put("is_seo_optimized",Boolean.parseBoolean(this.getIs_seo_optimized()));		
		}		if(this.getMenu_id()!=null&&this.getMenu_idDirtyFlag()){
			map.put("menu_id",this.getMenu_id());
		}else if(this.getMenu_idDirtyFlag()){
			map.put("menu_id",false);
		}
		if(this.getMenu_id_text()!=null&&this.getMenu_id_textDirtyFlag()){
			//忽略文本外键menu_id_text
		}else if(this.getMenu_id_textDirtyFlag()){
			map.put("menu_id",false);
		}
		if(this.getMessage_attachment_count()!=null&&this.getMessage_attachment_countDirtyFlag()){
			map.put("message_attachment_count",this.getMessage_attachment_count());
		}else if(this.getMessage_attachment_countDirtyFlag()){
			map.put("message_attachment_count",false);
		}
		if(this.getMessage_channel_ids()!=null&&this.getMessage_channel_idsDirtyFlag()){
			map.put("message_channel_ids",this.getMessage_channel_ids());
		}else if(this.getMessage_channel_idsDirtyFlag()){
			map.put("message_channel_ids",false);
		}
		if(this.getMessage_follower_ids()!=null&&this.getMessage_follower_idsDirtyFlag()){
			map.put("message_follower_ids",this.getMessage_follower_ids());
		}else if(this.getMessage_follower_idsDirtyFlag()){
			map.put("message_follower_ids",false);
		}
		if(this.getMessage_has_error()!=null&&this.getMessage_has_errorDirtyFlag()){
			map.put("message_has_error",Boolean.parseBoolean(this.getMessage_has_error()));		
		}		if(this.getMessage_has_error_counter()!=null&&this.getMessage_has_error_counterDirtyFlag()){
			map.put("message_has_error_counter",this.getMessage_has_error_counter());
		}else if(this.getMessage_has_error_counterDirtyFlag()){
			map.put("message_has_error_counter",false);
		}
		if(this.getMessage_ids()!=null&&this.getMessage_idsDirtyFlag()){
			map.put("message_ids",this.getMessage_ids());
		}else if(this.getMessage_idsDirtyFlag()){
			map.put("message_ids",false);
		}
		if(this.getMessage_is_follower()!=null&&this.getMessage_is_followerDirtyFlag()){
			map.put("message_is_follower",Boolean.parseBoolean(this.getMessage_is_follower()));		
		}		if(this.getMessage_main_attachment_id()!=null&&this.getMessage_main_attachment_idDirtyFlag()){
			map.put("message_main_attachment_id",this.getMessage_main_attachment_id());
		}else if(this.getMessage_main_attachment_idDirtyFlag()){
			map.put("message_main_attachment_id",false);
		}
		if(this.getMessage_needaction()!=null&&this.getMessage_needactionDirtyFlag()){
			map.put("message_needaction",Boolean.parseBoolean(this.getMessage_needaction()));		
		}		if(this.getMessage_needaction_counter()!=null&&this.getMessage_needaction_counterDirtyFlag()){
			map.put("message_needaction_counter",this.getMessage_needaction_counter());
		}else if(this.getMessage_needaction_counterDirtyFlag()){
			map.put("message_needaction_counter",false);
		}
		if(this.getMessage_partner_ids()!=null&&this.getMessage_partner_idsDirtyFlag()){
			map.put("message_partner_ids",this.getMessage_partner_ids());
		}else if(this.getMessage_partner_idsDirtyFlag()){
			map.put("message_partner_ids",false);
		}
		if(this.getMessage_unread()!=null&&this.getMessage_unreadDirtyFlag()){
			map.put("message_unread",Boolean.parseBoolean(this.getMessage_unread()));		
		}		if(this.getMessage_unread_counter()!=null&&this.getMessage_unread_counterDirtyFlag()){
			map.put("message_unread_counter",this.getMessage_unread_counter());
		}else if(this.getMessage_unread_counterDirtyFlag()){
			map.put("message_unread_counter",false);
		}
		if(this.getName()!=null&&this.getNameDirtyFlag()){
			map.put("name",this.getName());
		}else if(this.getNameDirtyFlag()){
			map.put("name",false);
		}
		if(this.getOrganizer_id()!=null&&this.getOrganizer_idDirtyFlag()){
			map.put("organizer_id",this.getOrganizer_id());
		}else if(this.getOrganizer_idDirtyFlag()){
			map.put("organizer_id",false);
		}
		if(this.getOrganizer_id_text()!=null&&this.getOrganizer_id_textDirtyFlag()){
			//忽略文本外键organizer_id_text
		}else if(this.getOrganizer_id_textDirtyFlag()){
			map.put("organizer_id",false);
		}
		if(this.getRegistration_ids()!=null&&this.getRegistration_idsDirtyFlag()){
			map.put("registration_ids",this.getRegistration_ids());
		}else if(this.getRegistration_idsDirtyFlag()){
			map.put("registration_ids",false);
		}
		if(this.getSeats_availability()!=null&&this.getSeats_availabilityDirtyFlag()){
			map.put("seats_availability",this.getSeats_availability());
		}else if(this.getSeats_availabilityDirtyFlag()){
			map.put("seats_availability",false);
		}
		if(this.getSeats_available()!=null&&this.getSeats_availableDirtyFlag()){
			map.put("seats_available",this.getSeats_available());
		}else if(this.getSeats_availableDirtyFlag()){
			map.put("seats_available",false);
		}
		if(this.getSeats_expected()!=null&&this.getSeats_expectedDirtyFlag()){
			map.put("seats_expected",this.getSeats_expected());
		}else if(this.getSeats_expectedDirtyFlag()){
			map.put("seats_expected",false);
		}
		if(this.getSeats_max()!=null&&this.getSeats_maxDirtyFlag()){
			map.put("seats_max",this.getSeats_max());
		}else if(this.getSeats_maxDirtyFlag()){
			map.put("seats_max",false);
		}
		if(this.getSeats_min()!=null&&this.getSeats_minDirtyFlag()){
			map.put("seats_min",this.getSeats_min());
		}else if(this.getSeats_minDirtyFlag()){
			map.put("seats_min",false);
		}
		if(this.getSeats_reserved()!=null&&this.getSeats_reservedDirtyFlag()){
			map.put("seats_reserved",this.getSeats_reserved());
		}else if(this.getSeats_reservedDirtyFlag()){
			map.put("seats_reserved",false);
		}
		if(this.getSeats_unconfirmed()!=null&&this.getSeats_unconfirmedDirtyFlag()){
			map.put("seats_unconfirmed",this.getSeats_unconfirmed());
		}else if(this.getSeats_unconfirmedDirtyFlag()){
			map.put("seats_unconfirmed",false);
		}
		if(this.getSeats_used()!=null&&this.getSeats_usedDirtyFlag()){
			map.put("seats_used",this.getSeats_used());
		}else if(this.getSeats_usedDirtyFlag()){
			map.put("seats_used",false);
		}
		if(this.getState()!=null&&this.getStateDirtyFlag()){
			map.put("state",this.getState());
		}else if(this.getStateDirtyFlag()){
			map.put("state",false);
		}
		if(this.getTwitter_hashtag()!=null&&this.getTwitter_hashtagDirtyFlag()){
			map.put("twitter_hashtag",this.getTwitter_hashtag());
		}else if(this.getTwitter_hashtagDirtyFlag()){
			map.put("twitter_hashtag",false);
		}
		if(this.getUser_id()!=null&&this.getUser_idDirtyFlag()){
			map.put("user_id",this.getUser_id());
		}else if(this.getUser_idDirtyFlag()){
			map.put("user_id",false);
		}
		if(this.getUser_id_text()!=null&&this.getUser_id_textDirtyFlag()){
			//忽略文本外键user_id_text
		}else if(this.getUser_id_textDirtyFlag()){
			map.put("user_id",false);
		}
		if(this.getWebsite_id()!=null&&this.getWebsite_idDirtyFlag()){
			map.put("website_id",this.getWebsite_id());
		}else if(this.getWebsite_idDirtyFlag()){
			map.put("website_id",false);
		}
		if(this.getWebsite_menu()!=null&&this.getWebsite_menuDirtyFlag()){
			map.put("website_menu",Boolean.parseBoolean(this.getWebsite_menu()));		
		}		if(this.getWebsite_message_ids()!=null&&this.getWebsite_message_idsDirtyFlag()){
			map.put("website_message_ids",this.getWebsite_message_ids());
		}else if(this.getWebsite_message_idsDirtyFlag()){
			map.put("website_message_ids",false);
		}
		if(this.getWebsite_meta_description()!=null&&this.getWebsite_meta_descriptionDirtyFlag()){
			map.put("website_meta_description",this.getWebsite_meta_description());
		}else if(this.getWebsite_meta_descriptionDirtyFlag()){
			map.put("website_meta_description",false);
		}
		if(this.getWebsite_meta_keywords()!=null&&this.getWebsite_meta_keywordsDirtyFlag()){
			map.put("website_meta_keywords",this.getWebsite_meta_keywords());
		}else if(this.getWebsite_meta_keywordsDirtyFlag()){
			map.put("website_meta_keywords",false);
		}
		if(this.getWebsite_meta_og_img()!=null&&this.getWebsite_meta_og_imgDirtyFlag()){
			map.put("website_meta_og_img",this.getWebsite_meta_og_img());
		}else if(this.getWebsite_meta_og_imgDirtyFlag()){
			map.put("website_meta_og_img",false);
		}
		if(this.getWebsite_meta_title()!=null&&this.getWebsite_meta_titleDirtyFlag()){
			map.put("website_meta_title",this.getWebsite_meta_title());
		}else if(this.getWebsite_meta_titleDirtyFlag()){
			map.put("website_meta_title",false);
		}
		if(this.getWebsite_published()!=null&&this.getWebsite_publishedDirtyFlag()){
			map.put("website_published",Boolean.parseBoolean(this.getWebsite_published()));		
		}		if(this.getWebsite_url()!=null&&this.getWebsite_urlDirtyFlag()){
			map.put("website_url",this.getWebsite_url());
		}else if(this.getWebsite_urlDirtyFlag()){
			map.put("website_url",false);
		}
		if(this.getWrite_date()!=null&&this.getWrite_dateDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String datetimeStr = sdf.format(this.getWrite_date());
			map.put("write_date",datetimeStr);
		}else if(this.getWrite_dateDirtyFlag()){
			map.put("write_date",false);
		}
		if(this.getWrite_uid()!=null&&this.getWrite_uidDirtyFlag()){
			map.put("write_uid",this.getWrite_uid());
		}else if(this.getWrite_uidDirtyFlag()){
			map.put("write_uid",false);
		}
		if(this.getWrite_uid_text()!=null&&this.getWrite_uid_textDirtyFlag()){
			//忽略文本外键write_uid_text
		}else if(this.getWrite_uid_textDirtyFlag()){
			map.put("write_uid",false);
		}
		if(this.get__last_update()!=null&&this.get__last_updateDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String datetimeStr = sdf.format(this.get__last_update());
			map.put("__last_update",datetimeStr);
		}else if(this.get__last_updateDirtyFlag()){
			map.put("__last_update",false);
		}
		return map;
	}

}
