package cn.ibizlab.odoo.client.odoo_event.odooclient;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.core.client.model.Ievent_event_ticket;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[event_event_ticket] 服务对象客户端接口
 */
public interface Ievent_event_ticketOdooClient {
    
        public void create(Ievent_event_ticket event_event_ticket);

        public void get(Ievent_event_ticket event_event_ticket);

        public void update(Ievent_event_ticket event_event_ticket);

        public void removeBatch(Ievent_event_ticket event_event_ticket);

        public Page<Ievent_event_ticket> search(SearchContext context);

        public void remove(Ievent_event_ticket event_event_ticket);

        public void createBatch(Ievent_event_ticket event_event_ticket);

        public void updateBatch(Ievent_event_ticket event_event_ticket);

        public List<Ievent_event_ticket> select();


}