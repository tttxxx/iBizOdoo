package cn.ibizlab.odoo.client.odoo_mro.odooclient;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.core.client.model.Imro_request_reject;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[mro_request_reject] 服务对象客户端接口
 */
public interface Imro_request_rejectOdooClient {
    
        public Page<Imro_request_reject> search(SearchContext context);

        public void update(Imro_request_reject mro_request_reject);

        public void updateBatch(Imro_request_reject mro_request_reject);

        public void create(Imro_request_reject mro_request_reject);

        public void remove(Imro_request_reject mro_request_reject);

        public void removeBatch(Imro_request_reject mro_request_reject);

        public void createBatch(Imro_request_reject mro_request_reject);

        public void get(Imro_request_reject mro_request_reject);

        public List<Imro_request_reject> select();


}