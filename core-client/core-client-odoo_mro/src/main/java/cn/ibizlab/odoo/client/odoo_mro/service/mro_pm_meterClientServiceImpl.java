package cn.ibizlab.odoo.client.odoo_mro.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Imro_pm_meter;
import cn.ibizlab.odoo.core.client.service.Imro_pm_meterClientService;
import cn.ibizlab.odoo.client.odoo_mro.model.mro_pm_meterImpl;
import cn.ibizlab.odoo.client.odoo_mro.odooclient.Imro_pm_meterOdooClient;
import cn.ibizlab.odoo.client.odoo_mro.odooclient.impl.mro_pm_meterOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[mro_pm_meter] 服务对象接口
 */
@Service
public class mro_pm_meterClientServiceImpl implements Imro_pm_meterClientService {
    @Autowired
    private  Imro_pm_meterOdooClient  mro_pm_meterOdooClient;

    public Imro_pm_meter createModel() {		
		return new mro_pm_meterImpl();
	}


        public void update(Imro_pm_meter mro_pm_meter){
this.mro_pm_meterOdooClient.update(mro_pm_meter) ;
        }
        
        public void removeBatch(List<Imro_pm_meter> mro_pm_meters){
            
        }
        
        public void create(Imro_pm_meter mro_pm_meter){
this.mro_pm_meterOdooClient.create(mro_pm_meter) ;
        }
        
        public void get(Imro_pm_meter mro_pm_meter){
            this.mro_pm_meterOdooClient.get(mro_pm_meter) ;
        }
        
        public void updateBatch(List<Imro_pm_meter> mro_pm_meters){
            
        }
        
        public Page<Imro_pm_meter> search(SearchContext context){
            return this.mro_pm_meterOdooClient.search(context) ;
        }
        
        public void remove(Imro_pm_meter mro_pm_meter){
this.mro_pm_meterOdooClient.remove(mro_pm_meter) ;
        }
        
        public void createBatch(List<Imro_pm_meter> mro_pm_meters){
            
        }
        
        public Page<Imro_pm_meter> select(SearchContext context){
            return null ;
        }
        

}

