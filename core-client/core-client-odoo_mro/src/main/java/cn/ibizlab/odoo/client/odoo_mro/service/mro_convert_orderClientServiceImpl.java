package cn.ibizlab.odoo.client.odoo_mro.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Imro_convert_order;
import cn.ibizlab.odoo.core.client.service.Imro_convert_orderClientService;
import cn.ibizlab.odoo.client.odoo_mro.model.mro_convert_orderImpl;
import cn.ibizlab.odoo.client.odoo_mro.odooclient.Imro_convert_orderOdooClient;
import cn.ibizlab.odoo.client.odoo_mro.odooclient.impl.mro_convert_orderOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[mro_convert_order] 服务对象接口
 */
@Service
public class mro_convert_orderClientServiceImpl implements Imro_convert_orderClientService {
    @Autowired
    private  Imro_convert_orderOdooClient  mro_convert_orderOdooClient;

    public Imro_convert_order createModel() {		
		return new mro_convert_orderImpl();
	}


        public void create(Imro_convert_order mro_convert_order){
this.mro_convert_orderOdooClient.create(mro_convert_order) ;
        }
        
        public void updateBatch(List<Imro_convert_order> mro_convert_orders){
            
        }
        
        public void get(Imro_convert_order mro_convert_order){
            this.mro_convert_orderOdooClient.get(mro_convert_order) ;
        }
        
        public void remove(Imro_convert_order mro_convert_order){
this.mro_convert_orderOdooClient.remove(mro_convert_order) ;
        }
        
        public void update(Imro_convert_order mro_convert_order){
this.mro_convert_orderOdooClient.update(mro_convert_order) ;
        }
        
        public Page<Imro_convert_order> search(SearchContext context){
            return this.mro_convert_orderOdooClient.search(context) ;
        }
        
        public void removeBatch(List<Imro_convert_order> mro_convert_orders){
            
        }
        
        public void createBatch(List<Imro_convert_order> mro_convert_orders){
            
        }
        
        public Page<Imro_convert_order> select(SearchContext context){
            return null ;
        }
        

}

