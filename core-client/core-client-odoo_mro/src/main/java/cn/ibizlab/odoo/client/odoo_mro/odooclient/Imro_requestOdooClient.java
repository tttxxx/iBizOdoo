package cn.ibizlab.odoo.client.odoo_mro.odooclient;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.core.client.model.Imro_request;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[mro_request] 服务对象客户端接口
 */
public interface Imro_requestOdooClient {
    
        public void remove(Imro_request mro_request);

        public void create(Imro_request mro_request);

        public void removeBatch(Imro_request mro_request);

        public void update(Imro_request mro_request);

        public Page<Imro_request> search(SearchContext context);

        public void createBatch(Imro_request mro_request);

        public void updateBatch(Imro_request mro_request);

        public void get(Imro_request mro_request);

        public List<Imro_request> select();


}