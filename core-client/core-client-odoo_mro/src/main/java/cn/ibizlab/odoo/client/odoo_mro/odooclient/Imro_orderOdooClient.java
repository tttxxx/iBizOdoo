package cn.ibizlab.odoo.client.odoo_mro.odooclient;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.core.client.model.Imro_order;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[mro_order] 服务对象客户端接口
 */
public interface Imro_orderOdooClient {
    
        public void update(Imro_order mro_order);

        public void createBatch(Imro_order mro_order);

        public void removeBatch(Imro_order mro_order);

        public void create(Imro_order mro_order);

        public void updateBatch(Imro_order mro_order);

        public void get(Imro_order mro_order);

        public Page<Imro_order> search(SearchContext context);

        public void remove(Imro_order mro_order);

        public List<Imro_order> select();


}