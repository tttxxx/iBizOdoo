package cn.ibizlab.odoo.client.odoo_mro.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Imro_workorder;
import cn.ibizlab.odoo.core.client.service.Imro_workorderClientService;
import cn.ibizlab.odoo.client.odoo_mro.model.mro_workorderImpl;
import cn.ibizlab.odoo.client.odoo_mro.odooclient.Imro_workorderOdooClient;
import cn.ibizlab.odoo.client.odoo_mro.odooclient.impl.mro_workorderOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[mro_workorder] 服务对象接口
 */
@Service
public class mro_workorderClientServiceImpl implements Imro_workorderClientService {
    @Autowired
    private  Imro_workorderOdooClient  mro_workorderOdooClient;

    public Imro_workorder createModel() {		
		return new mro_workorderImpl();
	}


        public void create(Imro_workorder mro_workorder){
this.mro_workorderOdooClient.create(mro_workorder) ;
        }
        
        public Page<Imro_workorder> search(SearchContext context){
            return this.mro_workorderOdooClient.search(context) ;
        }
        
        public void update(Imro_workorder mro_workorder){
this.mro_workorderOdooClient.update(mro_workorder) ;
        }
        
        public void get(Imro_workorder mro_workorder){
            this.mro_workorderOdooClient.get(mro_workorder) ;
        }
        
        public void removeBatch(List<Imro_workorder> mro_workorders){
            
        }
        
        public void remove(Imro_workorder mro_workorder){
this.mro_workorderOdooClient.remove(mro_workorder) ;
        }
        
        public void updateBatch(List<Imro_workorder> mro_workorders){
            
        }
        
        public void createBatch(List<Imro_workorder> mro_workorders){
            
        }
        
        public Page<Imro_workorder> select(SearchContext context){
            return null ;
        }
        

}

