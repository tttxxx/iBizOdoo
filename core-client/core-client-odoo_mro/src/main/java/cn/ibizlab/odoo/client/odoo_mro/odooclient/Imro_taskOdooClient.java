package cn.ibizlab.odoo.client.odoo_mro.odooclient;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.core.client.model.Imro_task;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[mro_task] 服务对象客户端接口
 */
public interface Imro_taskOdooClient {
    
        public void updateBatch(Imro_task mro_task);

        public void createBatch(Imro_task mro_task);

        public Page<Imro_task> search(SearchContext context);

        public void get(Imro_task mro_task);

        public void create(Imro_task mro_task);

        public void update(Imro_task mro_task);

        public void remove(Imro_task mro_task);

        public void removeBatch(Imro_task mro_task);

        public List<Imro_task> select();


}