package cn.ibizlab.odoo.client.odoo_mro.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import lombok.Data;

@ConfigurationProperties(prefix = "client.odoo.mro")
@Data
public class odoo_mroClientProperties {

	private String tokenUrl ;

	private String clientId ;

	private String clientSecret ;

}
