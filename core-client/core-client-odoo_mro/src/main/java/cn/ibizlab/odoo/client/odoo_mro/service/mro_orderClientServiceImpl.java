package cn.ibizlab.odoo.client.odoo_mro.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Imro_order;
import cn.ibizlab.odoo.core.client.service.Imro_orderClientService;
import cn.ibizlab.odoo.client.odoo_mro.model.mro_orderImpl;
import cn.ibizlab.odoo.client.odoo_mro.odooclient.Imro_orderOdooClient;
import cn.ibizlab.odoo.client.odoo_mro.odooclient.impl.mro_orderOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[mro_order] 服务对象接口
 */
@Service
public class mro_orderClientServiceImpl implements Imro_orderClientService {
    @Autowired
    private  Imro_orderOdooClient  mro_orderOdooClient;

    public Imro_order createModel() {		
		return new mro_orderImpl();
	}


        public void update(Imro_order mro_order){
this.mro_orderOdooClient.update(mro_order) ;
        }
        
        public void createBatch(List<Imro_order> mro_orders){
            
        }
        
        public void removeBatch(List<Imro_order> mro_orders){
            
        }
        
        public void create(Imro_order mro_order){
this.mro_orderOdooClient.create(mro_order) ;
        }
        
        public void updateBatch(List<Imro_order> mro_orders){
            
        }
        
        public void get(Imro_order mro_order){
            this.mro_orderOdooClient.get(mro_order) ;
        }
        
        public Page<Imro_order> search(SearchContext context){
            return this.mro_orderOdooClient.search(context) ;
        }
        
        public void remove(Imro_order mro_order){
this.mro_orderOdooClient.remove(mro_order) ;
        }
        
        public Page<Imro_order> select(SearchContext context){
            return null ;
        }
        

}

