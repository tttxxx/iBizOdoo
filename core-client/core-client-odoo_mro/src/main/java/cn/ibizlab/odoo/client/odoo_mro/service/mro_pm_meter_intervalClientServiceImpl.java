package cn.ibizlab.odoo.client.odoo_mro.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Imro_pm_meter_interval;
import cn.ibizlab.odoo.core.client.service.Imro_pm_meter_intervalClientService;
import cn.ibizlab.odoo.client.odoo_mro.model.mro_pm_meter_intervalImpl;
import cn.ibizlab.odoo.client.odoo_mro.odooclient.Imro_pm_meter_intervalOdooClient;
import cn.ibizlab.odoo.client.odoo_mro.odooclient.impl.mro_pm_meter_intervalOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[mro_pm_meter_interval] 服务对象接口
 */
@Service
public class mro_pm_meter_intervalClientServiceImpl implements Imro_pm_meter_intervalClientService {
    @Autowired
    private  Imro_pm_meter_intervalOdooClient  mro_pm_meter_intervalOdooClient;

    public Imro_pm_meter_interval createModel() {		
		return new mro_pm_meter_intervalImpl();
	}


        public void update(Imro_pm_meter_interval mro_pm_meter_interval){
this.mro_pm_meter_intervalOdooClient.update(mro_pm_meter_interval) ;
        }
        
        public void removeBatch(List<Imro_pm_meter_interval> mro_pm_meter_intervals){
            
        }
        
        public void remove(Imro_pm_meter_interval mro_pm_meter_interval){
this.mro_pm_meter_intervalOdooClient.remove(mro_pm_meter_interval) ;
        }
        
        public void createBatch(List<Imro_pm_meter_interval> mro_pm_meter_intervals){
            
        }
        
        public void create(Imro_pm_meter_interval mro_pm_meter_interval){
this.mro_pm_meter_intervalOdooClient.create(mro_pm_meter_interval) ;
        }
        
        public Page<Imro_pm_meter_interval> search(SearchContext context){
            return this.mro_pm_meter_intervalOdooClient.search(context) ;
        }
        
        public void get(Imro_pm_meter_interval mro_pm_meter_interval){
            this.mro_pm_meter_intervalOdooClient.get(mro_pm_meter_interval) ;
        }
        
        public void updateBatch(List<Imro_pm_meter_interval> mro_pm_meter_intervals){
            
        }
        
        public Page<Imro_pm_meter_interval> select(SearchContext context){
            return null ;
        }
        

}

