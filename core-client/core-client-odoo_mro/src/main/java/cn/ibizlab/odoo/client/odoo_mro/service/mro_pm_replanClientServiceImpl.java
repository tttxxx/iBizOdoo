package cn.ibizlab.odoo.client.odoo_mro.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Imro_pm_replan;
import cn.ibizlab.odoo.core.client.service.Imro_pm_replanClientService;
import cn.ibizlab.odoo.client.odoo_mro.model.mro_pm_replanImpl;
import cn.ibizlab.odoo.client.odoo_mro.odooclient.Imro_pm_replanOdooClient;
import cn.ibizlab.odoo.client.odoo_mro.odooclient.impl.mro_pm_replanOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[mro_pm_replan] 服务对象接口
 */
@Service
public class mro_pm_replanClientServiceImpl implements Imro_pm_replanClientService {
    @Autowired
    private  Imro_pm_replanOdooClient  mro_pm_replanOdooClient;

    public Imro_pm_replan createModel() {		
		return new mro_pm_replanImpl();
	}


        public void remove(Imro_pm_replan mro_pm_replan){
this.mro_pm_replanOdooClient.remove(mro_pm_replan) ;
        }
        
        public void update(Imro_pm_replan mro_pm_replan){
this.mro_pm_replanOdooClient.update(mro_pm_replan) ;
        }
        
        public void get(Imro_pm_replan mro_pm_replan){
            this.mro_pm_replanOdooClient.get(mro_pm_replan) ;
        }
        
        public void updateBatch(List<Imro_pm_replan> mro_pm_replans){
            
        }
        
        public Page<Imro_pm_replan> search(SearchContext context){
            return this.mro_pm_replanOdooClient.search(context) ;
        }
        
        public void createBatch(List<Imro_pm_replan> mro_pm_replans){
            
        }
        
        public void removeBatch(List<Imro_pm_replan> mro_pm_replans){
            
        }
        
        public void create(Imro_pm_replan mro_pm_replan){
this.mro_pm_replanOdooClient.create(mro_pm_replan) ;
        }
        
        public Page<Imro_pm_replan> select(SearchContext context){
            return null ;
        }
        

}

