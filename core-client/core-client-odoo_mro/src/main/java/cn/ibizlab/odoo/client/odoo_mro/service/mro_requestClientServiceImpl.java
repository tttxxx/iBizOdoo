package cn.ibizlab.odoo.client.odoo_mro.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Imro_request;
import cn.ibizlab.odoo.core.client.service.Imro_requestClientService;
import cn.ibizlab.odoo.client.odoo_mro.model.mro_requestImpl;
import cn.ibizlab.odoo.client.odoo_mro.odooclient.Imro_requestOdooClient;
import cn.ibizlab.odoo.client.odoo_mro.odooclient.impl.mro_requestOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[mro_request] 服务对象接口
 */
@Service
public class mro_requestClientServiceImpl implements Imro_requestClientService {
    @Autowired
    private  Imro_requestOdooClient  mro_requestOdooClient;

    public Imro_request createModel() {		
		return new mro_requestImpl();
	}


        public void remove(Imro_request mro_request){
this.mro_requestOdooClient.remove(mro_request) ;
        }
        
        public void create(Imro_request mro_request){
this.mro_requestOdooClient.create(mro_request) ;
        }
        
        public void removeBatch(List<Imro_request> mro_requests){
            
        }
        
        public void update(Imro_request mro_request){
this.mro_requestOdooClient.update(mro_request) ;
        }
        
        public Page<Imro_request> search(SearchContext context){
            return this.mro_requestOdooClient.search(context) ;
        }
        
        public void createBatch(List<Imro_request> mro_requests){
            
        }
        
        public void updateBatch(List<Imro_request> mro_requests){
            
        }
        
        public void get(Imro_request mro_request){
            this.mro_requestOdooClient.get(mro_request) ;
        }
        
        public Page<Imro_request> select(SearchContext context){
            return null ;
        }
        

}

