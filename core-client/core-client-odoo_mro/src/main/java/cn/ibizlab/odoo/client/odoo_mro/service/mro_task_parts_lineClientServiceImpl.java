package cn.ibizlab.odoo.client.odoo_mro.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Imro_task_parts_line;
import cn.ibizlab.odoo.core.client.service.Imro_task_parts_lineClientService;
import cn.ibizlab.odoo.client.odoo_mro.model.mro_task_parts_lineImpl;
import cn.ibizlab.odoo.client.odoo_mro.odooclient.Imro_task_parts_lineOdooClient;
import cn.ibizlab.odoo.client.odoo_mro.odooclient.impl.mro_task_parts_lineOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[mro_task_parts_line] 服务对象接口
 */
@Service
public class mro_task_parts_lineClientServiceImpl implements Imro_task_parts_lineClientService {
    @Autowired
    private  Imro_task_parts_lineOdooClient  mro_task_parts_lineOdooClient;

    public Imro_task_parts_line createModel() {		
		return new mro_task_parts_lineImpl();
	}


        public Page<Imro_task_parts_line> search(SearchContext context){
            return this.mro_task_parts_lineOdooClient.search(context) ;
        }
        
        public void updateBatch(List<Imro_task_parts_line> mro_task_parts_lines){
            
        }
        
        public void removeBatch(List<Imro_task_parts_line> mro_task_parts_lines){
            
        }
        
        public void create(Imro_task_parts_line mro_task_parts_line){
this.mro_task_parts_lineOdooClient.create(mro_task_parts_line) ;
        }
        
        public void get(Imro_task_parts_line mro_task_parts_line){
            this.mro_task_parts_lineOdooClient.get(mro_task_parts_line) ;
        }
        
        public void createBatch(List<Imro_task_parts_line> mro_task_parts_lines){
            
        }
        
        public void remove(Imro_task_parts_line mro_task_parts_line){
this.mro_task_parts_lineOdooClient.remove(mro_task_parts_line) ;
        }
        
        public void update(Imro_task_parts_line mro_task_parts_line){
this.mro_task_parts_lineOdooClient.update(mro_task_parts_line) ;
        }
        
        public Page<Imro_task_parts_line> select(SearchContext context){
            return null ;
        }
        

}

