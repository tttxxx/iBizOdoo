package cn.ibizlab.odoo.client.odoo_mro.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Imro_pm_meter_line;
import cn.ibizlab.odoo.core.client.service.Imro_pm_meter_lineClientService;
import cn.ibizlab.odoo.client.odoo_mro.model.mro_pm_meter_lineImpl;
import cn.ibizlab.odoo.client.odoo_mro.odooclient.Imro_pm_meter_lineOdooClient;
import cn.ibizlab.odoo.client.odoo_mro.odooclient.impl.mro_pm_meter_lineOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[mro_pm_meter_line] 服务对象接口
 */
@Service
public class mro_pm_meter_lineClientServiceImpl implements Imro_pm_meter_lineClientService {
    @Autowired
    private  Imro_pm_meter_lineOdooClient  mro_pm_meter_lineOdooClient;

    public Imro_pm_meter_line createModel() {		
		return new mro_pm_meter_lineImpl();
	}


        public void create(Imro_pm_meter_line mro_pm_meter_line){
this.mro_pm_meter_lineOdooClient.create(mro_pm_meter_line) ;
        }
        
        public void get(Imro_pm_meter_line mro_pm_meter_line){
            this.mro_pm_meter_lineOdooClient.get(mro_pm_meter_line) ;
        }
        
        public void removeBatch(List<Imro_pm_meter_line> mro_pm_meter_lines){
            
        }
        
        public void update(Imro_pm_meter_line mro_pm_meter_line){
this.mro_pm_meter_lineOdooClient.update(mro_pm_meter_line) ;
        }
        
        public void updateBatch(List<Imro_pm_meter_line> mro_pm_meter_lines){
            
        }
        
        public void remove(Imro_pm_meter_line mro_pm_meter_line){
this.mro_pm_meter_lineOdooClient.remove(mro_pm_meter_line) ;
        }
        
        public void createBatch(List<Imro_pm_meter_line> mro_pm_meter_lines){
            
        }
        
        public Page<Imro_pm_meter_line> search(SearchContext context){
            return this.mro_pm_meter_lineOdooClient.search(context) ;
        }
        
        public Page<Imro_pm_meter_line> select(SearchContext context){
            return null ;
        }
        

}

