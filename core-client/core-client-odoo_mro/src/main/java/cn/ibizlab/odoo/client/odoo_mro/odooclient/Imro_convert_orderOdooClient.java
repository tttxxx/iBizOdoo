package cn.ibizlab.odoo.client.odoo_mro.odooclient;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.core.client.model.Imro_convert_order;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[mro_convert_order] 服务对象客户端接口
 */
public interface Imro_convert_orderOdooClient {
    
        public void create(Imro_convert_order mro_convert_order);

        public void updateBatch(Imro_convert_order mro_convert_order);

        public void get(Imro_convert_order mro_convert_order);

        public void remove(Imro_convert_order mro_convert_order);

        public void update(Imro_convert_order mro_convert_order);

        public Page<Imro_convert_order> search(SearchContext context);

        public void removeBatch(Imro_convert_order mro_convert_order);

        public void createBatch(Imro_convert_order mro_convert_order);

        public List<Imro_convert_order> select();


}