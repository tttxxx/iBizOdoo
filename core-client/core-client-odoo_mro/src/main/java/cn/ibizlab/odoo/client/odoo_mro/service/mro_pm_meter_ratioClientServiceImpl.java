package cn.ibizlab.odoo.client.odoo_mro.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Imro_pm_meter_ratio;
import cn.ibizlab.odoo.core.client.service.Imro_pm_meter_ratioClientService;
import cn.ibizlab.odoo.client.odoo_mro.model.mro_pm_meter_ratioImpl;
import cn.ibizlab.odoo.client.odoo_mro.odooclient.Imro_pm_meter_ratioOdooClient;
import cn.ibizlab.odoo.client.odoo_mro.odooclient.impl.mro_pm_meter_ratioOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[mro_pm_meter_ratio] 服务对象接口
 */
@Service
public class mro_pm_meter_ratioClientServiceImpl implements Imro_pm_meter_ratioClientService {
    @Autowired
    private  Imro_pm_meter_ratioOdooClient  mro_pm_meter_ratioOdooClient;

    public Imro_pm_meter_ratio createModel() {		
		return new mro_pm_meter_ratioImpl();
	}


        public Page<Imro_pm_meter_ratio> search(SearchContext context){
            return this.mro_pm_meter_ratioOdooClient.search(context) ;
        }
        
        public void get(Imro_pm_meter_ratio mro_pm_meter_ratio){
            this.mro_pm_meter_ratioOdooClient.get(mro_pm_meter_ratio) ;
        }
        
        public void create(Imro_pm_meter_ratio mro_pm_meter_ratio){
this.mro_pm_meter_ratioOdooClient.create(mro_pm_meter_ratio) ;
        }
        
        public void update(Imro_pm_meter_ratio mro_pm_meter_ratio){
this.mro_pm_meter_ratioOdooClient.update(mro_pm_meter_ratio) ;
        }
        
        public void removeBatch(List<Imro_pm_meter_ratio> mro_pm_meter_ratios){
            
        }
        
        public void remove(Imro_pm_meter_ratio mro_pm_meter_ratio){
this.mro_pm_meter_ratioOdooClient.remove(mro_pm_meter_ratio) ;
        }
        
        public void updateBatch(List<Imro_pm_meter_ratio> mro_pm_meter_ratios){
            
        }
        
        public void createBatch(List<Imro_pm_meter_ratio> mro_pm_meter_ratios){
            
        }
        
        public Page<Imro_pm_meter_ratio> select(SearchContext context){
            return null ;
        }
        

}

