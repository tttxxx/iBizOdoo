package cn.ibizlab.odoo.client.odoo_digest.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Idigest_tip;
import cn.ibizlab.odoo.core.client.service.Idigest_tipClientService;
import cn.ibizlab.odoo.client.odoo_digest.model.digest_tipImpl;
import cn.ibizlab.odoo.client.odoo_digest.odooclient.Idigest_tipOdooClient;
import cn.ibizlab.odoo.client.odoo_digest.odooclient.impl.digest_tipOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[digest_tip] 服务对象接口
 */
@Service
public class digest_tipClientServiceImpl implements Idigest_tipClientService {
    @Autowired
    private  Idigest_tipOdooClient  digest_tipOdooClient;

    public Idigest_tip createModel() {		
		return new digest_tipImpl();
	}


        public void updateBatch(List<Idigest_tip> digest_tips){
            
        }
        
        public Page<Idigest_tip> search(SearchContext context){
            return this.digest_tipOdooClient.search(context) ;
        }
        
        public void get(Idigest_tip digest_tip){
            this.digest_tipOdooClient.get(digest_tip) ;
        }
        
        public void remove(Idigest_tip digest_tip){
this.digest_tipOdooClient.remove(digest_tip) ;
        }
        
        public void createBatch(List<Idigest_tip> digest_tips){
            
        }
        
        public void update(Idigest_tip digest_tip){
this.digest_tipOdooClient.update(digest_tip) ;
        }
        
        public void create(Idigest_tip digest_tip){
this.digest_tipOdooClient.create(digest_tip) ;
        }
        
        public void removeBatch(List<Idigest_tip> digest_tips){
            
        }
        
        public Page<Idigest_tip> select(SearchContext context){
            return null ;
        }
        

}

