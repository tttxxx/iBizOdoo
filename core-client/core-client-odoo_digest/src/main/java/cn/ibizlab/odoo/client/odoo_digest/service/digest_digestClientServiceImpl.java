package cn.ibizlab.odoo.client.odoo_digest.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Idigest_digest;
import cn.ibizlab.odoo.core.client.service.Idigest_digestClientService;
import cn.ibizlab.odoo.client.odoo_digest.model.digest_digestImpl;
import cn.ibizlab.odoo.client.odoo_digest.odooclient.Idigest_digestOdooClient;
import cn.ibizlab.odoo.client.odoo_digest.odooclient.impl.digest_digestOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[digest_digest] 服务对象接口
 */
@Service
public class digest_digestClientServiceImpl implements Idigest_digestClientService {
    @Autowired
    private  Idigest_digestOdooClient  digest_digestOdooClient;

    public Idigest_digest createModel() {		
		return new digest_digestImpl();
	}


        public void updateBatch(List<Idigest_digest> digest_digests){
            
        }
        
        public void createBatch(List<Idigest_digest> digest_digests){
            
        }
        
        public void create(Idigest_digest digest_digest){
this.digest_digestOdooClient.create(digest_digest) ;
        }
        
        public void get(Idigest_digest digest_digest){
            this.digest_digestOdooClient.get(digest_digest) ;
        }
        
        public void removeBatch(List<Idigest_digest> digest_digests){
            
        }
        
        public void update(Idigest_digest digest_digest){
this.digest_digestOdooClient.update(digest_digest) ;
        }
        
        public Page<Idigest_digest> search(SearchContext context){
            return this.digest_digestOdooClient.search(context) ;
        }
        
        public void remove(Idigest_digest digest_digest){
this.digest_digestOdooClient.remove(digest_digest) ;
        }
        
        public Page<Idigest_digest> select(SearchContext context){
            return null ;
        }
        

}

