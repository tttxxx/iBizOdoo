package cn.ibizlab.odoo.client.odoo_calendar.odooclient;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.core.client.model.Icalendar_contacts;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[calendar_contacts] 服务对象客户端接口
 */
public interface Icalendar_contactsOdooClient {
    
        public void update(Icalendar_contacts calendar_contacts);

        public void create(Icalendar_contacts calendar_contacts);

        public void remove(Icalendar_contacts calendar_contacts);

        public Page<Icalendar_contacts> search(SearchContext context);

        public void get(Icalendar_contacts calendar_contacts);

        public void createBatch(Icalendar_contacts calendar_contacts);

        public void updateBatch(Icalendar_contacts calendar_contacts);

        public void removeBatch(Icalendar_contacts calendar_contacts);

        public List<Icalendar_contacts> select();


}