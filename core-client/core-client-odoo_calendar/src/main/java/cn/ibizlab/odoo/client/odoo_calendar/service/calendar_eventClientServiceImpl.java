package cn.ibizlab.odoo.client.odoo_calendar.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Icalendar_event;
import cn.ibizlab.odoo.core.client.service.Icalendar_eventClientService;
import cn.ibizlab.odoo.client.odoo_calendar.model.calendar_eventImpl;
import cn.ibizlab.odoo.client.odoo_calendar.odooclient.Icalendar_eventOdooClient;
import cn.ibizlab.odoo.client.odoo_calendar.odooclient.impl.calendar_eventOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[calendar_event] 服务对象接口
 */
@Service
public class calendar_eventClientServiceImpl implements Icalendar_eventClientService {
    @Autowired
    private  Icalendar_eventOdooClient  calendar_eventOdooClient;

    public Icalendar_event createModel() {		
		return new calendar_eventImpl();
	}


        public void get(Icalendar_event calendar_event){
            this.calendar_eventOdooClient.get(calendar_event) ;
        }
        
        public void update(Icalendar_event calendar_event){
this.calendar_eventOdooClient.update(calendar_event) ;
        }
        
        public Page<Icalendar_event> search(SearchContext context){
            return this.calendar_eventOdooClient.search(context) ;
        }
        
        public void createBatch(List<Icalendar_event> calendar_events){
            
        }
        
        public void updateBatch(List<Icalendar_event> calendar_events){
            
        }
        
        public void removeBatch(List<Icalendar_event> calendar_events){
            
        }
        
        public void create(Icalendar_event calendar_event){
this.calendar_eventOdooClient.create(calendar_event) ;
        }
        
        public void remove(Icalendar_event calendar_event){
this.calendar_eventOdooClient.remove(calendar_event) ;
        }
        
        public Page<Icalendar_event> select(SearchContext context){
            return null ;
        }
        

}

