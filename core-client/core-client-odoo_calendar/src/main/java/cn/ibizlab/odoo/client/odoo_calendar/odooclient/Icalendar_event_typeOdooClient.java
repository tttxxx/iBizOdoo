package cn.ibizlab.odoo.client.odoo_calendar.odooclient;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.core.client.model.Icalendar_event_type;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[calendar_event_type] 服务对象客户端接口
 */
public interface Icalendar_event_typeOdooClient {
    
        public void createBatch(Icalendar_event_type calendar_event_type);

        public void update(Icalendar_event_type calendar_event_type);

        public void get(Icalendar_event_type calendar_event_type);

        public Page<Icalendar_event_type> search(SearchContext context);

        public void create(Icalendar_event_type calendar_event_type);

        public void remove(Icalendar_event_type calendar_event_type);

        public void updateBatch(Icalendar_event_type calendar_event_type);

        public void removeBatch(Icalendar_event_type calendar_event_type);

        public List<Icalendar_event_type> select();


}