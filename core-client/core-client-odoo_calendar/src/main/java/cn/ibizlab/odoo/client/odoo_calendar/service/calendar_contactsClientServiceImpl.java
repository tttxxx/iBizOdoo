package cn.ibizlab.odoo.client.odoo_calendar.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Icalendar_contacts;
import cn.ibizlab.odoo.core.client.service.Icalendar_contactsClientService;
import cn.ibizlab.odoo.client.odoo_calendar.model.calendar_contactsImpl;
import cn.ibizlab.odoo.client.odoo_calendar.odooclient.Icalendar_contactsOdooClient;
import cn.ibizlab.odoo.client.odoo_calendar.odooclient.impl.calendar_contactsOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[calendar_contacts] 服务对象接口
 */
@Service
public class calendar_contactsClientServiceImpl implements Icalendar_contactsClientService {
    @Autowired
    private  Icalendar_contactsOdooClient  calendar_contactsOdooClient;

    public Icalendar_contacts createModel() {		
		return new calendar_contactsImpl();
	}


        public void update(Icalendar_contacts calendar_contacts){
this.calendar_contactsOdooClient.update(calendar_contacts) ;
        }
        
        public void create(Icalendar_contacts calendar_contacts){
this.calendar_contactsOdooClient.create(calendar_contacts) ;
        }
        
        public void remove(Icalendar_contacts calendar_contacts){
this.calendar_contactsOdooClient.remove(calendar_contacts) ;
        }
        
        public Page<Icalendar_contacts> search(SearchContext context){
            return this.calendar_contactsOdooClient.search(context) ;
        }
        
        public void get(Icalendar_contacts calendar_contacts){
            this.calendar_contactsOdooClient.get(calendar_contacts) ;
        }
        
        public void createBatch(List<Icalendar_contacts> calendar_contacts){
            
        }
        
        public void updateBatch(List<Icalendar_contacts> calendar_contacts){
            
        }
        
        public void removeBatch(List<Icalendar_contacts> calendar_contacts){
            
        }
        
        public Page<Icalendar_contacts> select(SearchContext context){
            return null ;
        }
        

}

