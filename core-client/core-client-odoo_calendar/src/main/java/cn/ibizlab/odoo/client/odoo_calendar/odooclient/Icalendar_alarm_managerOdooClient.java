package cn.ibizlab.odoo.client.odoo_calendar.odooclient;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.core.client.model.Icalendar_alarm_manager;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[calendar_alarm_manager] 服务对象客户端接口
 */
public interface Icalendar_alarm_managerOdooClient {
    
        public void updateBatch(Icalendar_alarm_manager calendar_alarm_manager);

        public void update(Icalendar_alarm_manager calendar_alarm_manager);

        public void removeBatch(Icalendar_alarm_manager calendar_alarm_manager);

        public void get(Icalendar_alarm_manager calendar_alarm_manager);

        public Page<Icalendar_alarm_manager> search(SearchContext context);

        public void remove(Icalendar_alarm_manager calendar_alarm_manager);

        public void create(Icalendar_alarm_manager calendar_alarm_manager);

        public void createBatch(Icalendar_alarm_manager calendar_alarm_manager);

        public List<Icalendar_alarm_manager> select();


}