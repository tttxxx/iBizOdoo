package cn.ibizlab.odoo.client.odoo_calendar.odooclient;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.core.client.model.Icalendar_attendee;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[calendar_attendee] 服务对象客户端接口
 */
public interface Icalendar_attendeeOdooClient {
    
        public void update(Icalendar_attendee calendar_attendee);

        public void createBatch(Icalendar_attendee calendar_attendee);

        public void create(Icalendar_attendee calendar_attendee);

        public void removeBatch(Icalendar_attendee calendar_attendee);

        public void get(Icalendar_attendee calendar_attendee);

        public void updateBatch(Icalendar_attendee calendar_attendee);

        public void remove(Icalendar_attendee calendar_attendee);

        public Page<Icalendar_attendee> search(SearchContext context);

        public List<Icalendar_attendee> select();


}