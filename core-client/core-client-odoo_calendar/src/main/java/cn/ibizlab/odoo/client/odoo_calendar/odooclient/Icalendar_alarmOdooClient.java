package cn.ibizlab.odoo.client.odoo_calendar.odooclient;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.core.client.model.Icalendar_alarm;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[calendar_alarm] 服务对象客户端接口
 */
public interface Icalendar_alarmOdooClient {
    
        public void updateBatch(Icalendar_alarm calendar_alarm);

        public void create(Icalendar_alarm calendar_alarm);

        public void update(Icalendar_alarm calendar_alarm);

        public void remove(Icalendar_alarm calendar_alarm);

        public Page<Icalendar_alarm> search(SearchContext context);

        public void createBatch(Icalendar_alarm calendar_alarm);

        public void removeBatch(Icalendar_alarm calendar_alarm);

        public void get(Icalendar_alarm calendar_alarm);

        public List<Icalendar_alarm> select();


}