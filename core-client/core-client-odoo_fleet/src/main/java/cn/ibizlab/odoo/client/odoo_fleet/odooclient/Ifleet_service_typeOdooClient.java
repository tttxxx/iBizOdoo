package cn.ibizlab.odoo.client.odoo_fleet.odooclient;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.core.client.model.Ifleet_service_type;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[fleet_service_type] 服务对象客户端接口
 */
public interface Ifleet_service_typeOdooClient {
    
        public void updateBatch(Ifleet_service_type fleet_service_type);

        public void update(Ifleet_service_type fleet_service_type);

        public void removeBatch(Ifleet_service_type fleet_service_type);

        public void get(Ifleet_service_type fleet_service_type);

        public void createBatch(Ifleet_service_type fleet_service_type);

        public void create(Ifleet_service_type fleet_service_type);

        public void remove(Ifleet_service_type fleet_service_type);

        public Page<Ifleet_service_type> search(SearchContext context);

        public List<Ifleet_service_type> select();


}