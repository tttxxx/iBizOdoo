package cn.ibizlab.odoo.client.odoo_fleet.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import lombok.Data;

@ConfigurationProperties(prefix = "client.odoo.fleet")
@Data
public class odoo_fleetClientProperties {

	private String tokenUrl ;

	private String clientId ;

	private String clientSecret ;

}
