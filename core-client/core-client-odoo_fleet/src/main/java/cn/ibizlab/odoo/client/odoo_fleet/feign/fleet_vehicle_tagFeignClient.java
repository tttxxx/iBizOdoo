package cn.ibizlab.odoo.client.odoo_fleet.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Ifleet_vehicle_tag;
import cn.ibizlab.odoo.client.odoo_fleet.model.fleet_vehicle_tagImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[fleet_vehicle_tag] 服务对象接口
 */
public interface fleet_vehicle_tagFeignClient {

    @RequestMapping(method = RequestMethod.POST, value = "/odoo_fleet/fleet_vehicle_tags/createbatch")
    public fleet_vehicle_tagImpl createBatch(@RequestBody List<fleet_vehicle_tagImpl> fleet_vehicle_tags);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_fleet/fleet_vehicle_tags/removebatch")
    public fleet_vehicle_tagImpl removeBatch(@RequestBody List<fleet_vehicle_tagImpl> fleet_vehicle_tags);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_fleet/fleet_vehicle_tags/search")
    public Page<fleet_vehicle_tagImpl> search(SearchContext context);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_fleet/fleet_vehicle_tags/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_fleet/fleet_vehicle_tags")
    public fleet_vehicle_tagImpl create(@RequestBody fleet_vehicle_tagImpl fleet_vehicle_tag);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_fleet/fleet_vehicle_tags/updatebatch")
    public fleet_vehicle_tagImpl updateBatch(@RequestBody List<fleet_vehicle_tagImpl> fleet_vehicle_tags);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_fleet/fleet_vehicle_tags/{id}")
    public fleet_vehicle_tagImpl update(@PathVariable("id") Integer id,@RequestBody fleet_vehicle_tagImpl fleet_vehicle_tag);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_fleet/fleet_vehicle_tags/{id}")
    public fleet_vehicle_tagImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_fleet/fleet_vehicle_tags/select")
    public Page<fleet_vehicle_tagImpl> select();



}
