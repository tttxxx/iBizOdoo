package cn.ibizlab.odoo.client.odoo_fleet.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Ifleet_vehicle;
import cn.ibizlab.odoo.core.client.service.Ifleet_vehicleClientService;
import cn.ibizlab.odoo.client.odoo_fleet.model.fleet_vehicleImpl;
import cn.ibizlab.odoo.client.odoo_fleet.odooclient.Ifleet_vehicleOdooClient;
import cn.ibizlab.odoo.client.odoo_fleet.odooclient.impl.fleet_vehicleOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[fleet_vehicle] 服务对象接口
 */
@Service
public class fleet_vehicleClientServiceImpl implements Ifleet_vehicleClientService {
    @Autowired
    private  Ifleet_vehicleOdooClient  fleet_vehicleOdooClient;

    public Ifleet_vehicle createModel() {		
		return new fleet_vehicleImpl();
	}


        public void removeBatch(List<Ifleet_vehicle> fleet_vehicles){
            
        }
        
        public void remove(Ifleet_vehicle fleet_vehicle){
this.fleet_vehicleOdooClient.remove(fleet_vehicle) ;
        }
        
        public void createBatch(List<Ifleet_vehicle> fleet_vehicles){
            
        }
        
        public void updateBatch(List<Ifleet_vehicle> fleet_vehicles){
            
        }
        
        public void update(Ifleet_vehicle fleet_vehicle){
this.fleet_vehicleOdooClient.update(fleet_vehicle) ;
        }
        
        public void get(Ifleet_vehicle fleet_vehicle){
            this.fleet_vehicleOdooClient.get(fleet_vehicle) ;
        }
        
        public void create(Ifleet_vehicle fleet_vehicle){
this.fleet_vehicleOdooClient.create(fleet_vehicle) ;
        }
        
        public Page<Ifleet_vehicle> search(SearchContext context){
            return this.fleet_vehicleOdooClient.search(context) ;
        }
        
        public Page<Ifleet_vehicle> select(SearchContext context){
            return null ;
        }
        

}

