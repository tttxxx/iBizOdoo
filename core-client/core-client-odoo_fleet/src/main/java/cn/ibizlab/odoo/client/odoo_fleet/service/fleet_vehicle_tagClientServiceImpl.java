package cn.ibizlab.odoo.client.odoo_fleet.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Ifleet_vehicle_tag;
import cn.ibizlab.odoo.core.client.service.Ifleet_vehicle_tagClientService;
import cn.ibizlab.odoo.client.odoo_fleet.model.fleet_vehicle_tagImpl;
import cn.ibizlab.odoo.client.odoo_fleet.odooclient.Ifleet_vehicle_tagOdooClient;
import cn.ibizlab.odoo.client.odoo_fleet.odooclient.impl.fleet_vehicle_tagOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[fleet_vehicle_tag] 服务对象接口
 */
@Service
public class fleet_vehicle_tagClientServiceImpl implements Ifleet_vehicle_tagClientService {
    @Autowired
    private  Ifleet_vehicle_tagOdooClient  fleet_vehicle_tagOdooClient;

    public Ifleet_vehicle_tag createModel() {		
		return new fleet_vehicle_tagImpl();
	}


        public void createBatch(List<Ifleet_vehicle_tag> fleet_vehicle_tags){
            
        }
        
        public void removeBatch(List<Ifleet_vehicle_tag> fleet_vehicle_tags){
            
        }
        
        public Page<Ifleet_vehicle_tag> search(SearchContext context){
            return this.fleet_vehicle_tagOdooClient.search(context) ;
        }
        
        public void remove(Ifleet_vehicle_tag fleet_vehicle_tag){
this.fleet_vehicle_tagOdooClient.remove(fleet_vehicle_tag) ;
        }
        
        public void create(Ifleet_vehicle_tag fleet_vehicle_tag){
this.fleet_vehicle_tagOdooClient.create(fleet_vehicle_tag) ;
        }
        
        public void updateBatch(List<Ifleet_vehicle_tag> fleet_vehicle_tags){
            
        }
        
        public void update(Ifleet_vehicle_tag fleet_vehicle_tag){
this.fleet_vehicle_tagOdooClient.update(fleet_vehicle_tag) ;
        }
        
        public void get(Ifleet_vehicle_tag fleet_vehicle_tag){
            this.fleet_vehicle_tagOdooClient.get(fleet_vehicle_tag) ;
        }
        
        public Page<Ifleet_vehicle_tag> select(SearchContext context){
            return null ;
        }
        

}

