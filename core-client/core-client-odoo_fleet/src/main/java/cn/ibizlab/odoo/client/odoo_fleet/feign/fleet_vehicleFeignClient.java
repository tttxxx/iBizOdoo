package cn.ibizlab.odoo.client.odoo_fleet.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Ifleet_vehicle;
import cn.ibizlab.odoo.client.odoo_fleet.model.fleet_vehicleImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[fleet_vehicle] 服务对象接口
 */
public interface fleet_vehicleFeignClient {

    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_fleet/fleet_vehicles/removebatch")
    public fleet_vehicleImpl removeBatch(@RequestBody List<fleet_vehicleImpl> fleet_vehicles);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_fleet/fleet_vehicles/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_fleet/fleet_vehicles/createbatch")
    public fleet_vehicleImpl createBatch(@RequestBody List<fleet_vehicleImpl> fleet_vehicles);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_fleet/fleet_vehicles/updatebatch")
    public fleet_vehicleImpl updateBatch(@RequestBody List<fleet_vehicleImpl> fleet_vehicles);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_fleet/fleet_vehicles/{id}")
    public fleet_vehicleImpl update(@PathVariable("id") Integer id,@RequestBody fleet_vehicleImpl fleet_vehicle);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_fleet/fleet_vehicles/{id}")
    public fleet_vehicleImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_fleet/fleet_vehicles")
    public fleet_vehicleImpl create(@RequestBody fleet_vehicleImpl fleet_vehicle);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_fleet/fleet_vehicles/search")
    public Page<fleet_vehicleImpl> search(SearchContext context);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_fleet/fleet_vehicles/select")
    public Page<fleet_vehicleImpl> select();



}
