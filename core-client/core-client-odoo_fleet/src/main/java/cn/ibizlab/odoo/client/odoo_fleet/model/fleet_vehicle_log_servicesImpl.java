package cn.ibizlab.odoo.client.odoo_fleet.model;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.math.BigInteger;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.core.client.model.Ifleet_vehicle_log_services;
import cn.ibizlab.odoo.util.helper.OdooClientHelper;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang.ArrayUtils;
import org.springframework.util.StringUtils;

/**
 * 接口实体[fleet_vehicle_log_services] 对象
 */
public class fleet_vehicle_log_servicesImpl implements Ifleet_vehicle_log_services,Serializable{

    /**
     * 总价
     */
    public Double amount;

    @JsonIgnore
    public boolean amountDirtyFlag;
    
    /**
     * 自动生成
     */
    public String auto_generated;

    @JsonIgnore
    public boolean auto_generatedDirtyFlag;
    
    /**
     * 合同
     */
    public Integer contract_id;

    @JsonIgnore
    public boolean contract_idDirtyFlag;
    
    /**
     * 总额
     */
    public Double cost_amount;

    @JsonIgnore
    public boolean cost_amountDirtyFlag;
    
    /**
     * 成本
     */
    public Integer cost_id;

    @JsonIgnore
    public boolean cost_idDirtyFlag;
    
    /**
     * 包括服务
     */
    public String cost_ids;

    @JsonIgnore
    public boolean cost_idsDirtyFlag;
    
    /**
     * 类型
     */
    public Integer cost_subtype_id;

    @JsonIgnore
    public boolean cost_subtype_idDirtyFlag;
    
    /**
     * 费用所属类别
     */
    public String cost_type;

    @JsonIgnore
    public boolean cost_typeDirtyFlag;
    
    /**
     * 创建时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp create_date;

    @JsonIgnore
    public boolean create_dateDirtyFlag;
    
    /**
     * 创建人
     */
    public Integer create_uid;

    @JsonIgnore
    public boolean create_uidDirtyFlag;
    
    /**
     * 创建人
     */
    public String create_uid_text;

    @JsonIgnore
    public boolean create_uid_textDirtyFlag;
    
    /**
     * 日期
     */
    public Timestamp date;

    @JsonIgnore
    public boolean dateDirtyFlag;
    
    /**
     * 成本说明
     */
    public String description;

    @JsonIgnore
    public boolean descriptionDirtyFlag;
    
    /**
     * 显示名称
     */
    public String display_name;

    @JsonIgnore
    public boolean display_nameDirtyFlag;
    
    /**
     * ID
     */
    public Integer id;

    @JsonIgnore
    public boolean idDirtyFlag;
    
    /**
     * 发票参考
     */
    public String inv_ref;

    @JsonIgnore
    public boolean inv_refDirtyFlag;
    
    /**
     * 名称
     */
    public String name;

    @JsonIgnore
    public boolean nameDirtyFlag;
    
    /**
     * 便签
     */
    public String notes;

    @JsonIgnore
    public boolean notesDirtyFlag;
    
    /**
     * 里程表数值
     */
    public Double odometer;

    @JsonIgnore
    public boolean odometerDirtyFlag;
    
    /**
     * 里程表
     */
    public Integer odometer_id;

    @JsonIgnore
    public boolean odometer_idDirtyFlag;
    
    /**
     * 单位
     */
    public String odometer_unit;

    @JsonIgnore
    public boolean odometer_unitDirtyFlag;
    
    /**
     * 上级
     */
    public Integer parent_id;

    @JsonIgnore
    public boolean parent_idDirtyFlag;
    
    /**
     * 采购
     */
    public Integer purchaser_id;

    @JsonIgnore
    public boolean purchaser_idDirtyFlag;
    
    /**
     * 采购
     */
    public String purchaser_id_text;

    @JsonIgnore
    public boolean purchaser_id_textDirtyFlag;
    
    /**
     * 车辆
     */
    public Integer vehicle_id;

    @JsonIgnore
    public boolean vehicle_idDirtyFlag;
    
    /**
     * 供应商
     */
    public Integer vendor_id;

    @JsonIgnore
    public boolean vendor_idDirtyFlag;
    
    /**
     * 供应商
     */
    public String vendor_id_text;

    @JsonIgnore
    public boolean vendor_id_textDirtyFlag;
    
    /**
     * 最后更新时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp write_date;

    @JsonIgnore
    public boolean write_dateDirtyFlag;
    
    /**
     * 最后更新人
     */
    public Integer write_uid;

    @JsonIgnore
    public boolean write_uidDirtyFlag;
    
    /**
     * 最后更新人
     */
    public String write_uid_text;

    @JsonIgnore
    public boolean write_uid_textDirtyFlag;
    
    /**
     * 最后修改日
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp __last_update;

    @JsonIgnore
    public boolean __last_updateDirtyFlag;
    
    /**
     * 获取 [总价]
     */
    @JsonProperty("amount")
    public Double getAmount(){
        return this.amount ;
    }

    /**
     * 设置 [总价]
     */
    @JsonProperty("amount")
    public void setAmount(Double  amount){
        this.amount = amount ;
        this.amountDirtyFlag = true ;
    }

     /**
     * 获取 [总价]脏标记
     */
    @JsonIgnore
    public boolean getAmountDirtyFlag(){
        return this.amountDirtyFlag ;
    }   

    /**
     * 获取 [自动生成]
     */
    @JsonProperty("auto_generated")
    public String getAuto_generated(){
        return this.auto_generated ;
    }

    /**
     * 设置 [自动生成]
     */
    @JsonProperty("auto_generated")
    public void setAuto_generated(String  auto_generated){
        this.auto_generated = auto_generated ;
        this.auto_generatedDirtyFlag = true ;
    }

     /**
     * 获取 [自动生成]脏标记
     */
    @JsonIgnore
    public boolean getAuto_generatedDirtyFlag(){
        return this.auto_generatedDirtyFlag ;
    }   

    /**
     * 获取 [合同]
     */
    @JsonProperty("contract_id")
    public Integer getContract_id(){
        return this.contract_id ;
    }

    /**
     * 设置 [合同]
     */
    @JsonProperty("contract_id")
    public void setContract_id(Integer  contract_id){
        this.contract_id = contract_id ;
        this.contract_idDirtyFlag = true ;
    }

     /**
     * 获取 [合同]脏标记
     */
    @JsonIgnore
    public boolean getContract_idDirtyFlag(){
        return this.contract_idDirtyFlag ;
    }   

    /**
     * 获取 [总额]
     */
    @JsonProperty("cost_amount")
    public Double getCost_amount(){
        return this.cost_amount ;
    }

    /**
     * 设置 [总额]
     */
    @JsonProperty("cost_amount")
    public void setCost_amount(Double  cost_amount){
        this.cost_amount = cost_amount ;
        this.cost_amountDirtyFlag = true ;
    }

     /**
     * 获取 [总额]脏标记
     */
    @JsonIgnore
    public boolean getCost_amountDirtyFlag(){
        return this.cost_amountDirtyFlag ;
    }   

    /**
     * 获取 [成本]
     */
    @JsonProperty("cost_id")
    public Integer getCost_id(){
        return this.cost_id ;
    }

    /**
     * 设置 [成本]
     */
    @JsonProperty("cost_id")
    public void setCost_id(Integer  cost_id){
        this.cost_id = cost_id ;
        this.cost_idDirtyFlag = true ;
    }

     /**
     * 获取 [成本]脏标记
     */
    @JsonIgnore
    public boolean getCost_idDirtyFlag(){
        return this.cost_idDirtyFlag ;
    }   

    /**
     * 获取 [包括服务]
     */
    @JsonProperty("cost_ids")
    public String getCost_ids(){
        return this.cost_ids ;
    }

    /**
     * 设置 [包括服务]
     */
    @JsonProperty("cost_ids")
    public void setCost_ids(String  cost_ids){
        this.cost_ids = cost_ids ;
        this.cost_idsDirtyFlag = true ;
    }

     /**
     * 获取 [包括服务]脏标记
     */
    @JsonIgnore
    public boolean getCost_idsDirtyFlag(){
        return this.cost_idsDirtyFlag ;
    }   

    /**
     * 获取 [类型]
     */
    @JsonProperty("cost_subtype_id")
    public Integer getCost_subtype_id(){
        return this.cost_subtype_id ;
    }

    /**
     * 设置 [类型]
     */
    @JsonProperty("cost_subtype_id")
    public void setCost_subtype_id(Integer  cost_subtype_id){
        this.cost_subtype_id = cost_subtype_id ;
        this.cost_subtype_idDirtyFlag = true ;
    }

     /**
     * 获取 [类型]脏标记
     */
    @JsonIgnore
    public boolean getCost_subtype_idDirtyFlag(){
        return this.cost_subtype_idDirtyFlag ;
    }   

    /**
     * 获取 [费用所属类别]
     */
    @JsonProperty("cost_type")
    public String getCost_type(){
        return this.cost_type ;
    }

    /**
     * 设置 [费用所属类别]
     */
    @JsonProperty("cost_type")
    public void setCost_type(String  cost_type){
        this.cost_type = cost_type ;
        this.cost_typeDirtyFlag = true ;
    }

     /**
     * 获取 [费用所属类别]脏标记
     */
    @JsonIgnore
    public boolean getCost_typeDirtyFlag(){
        return this.cost_typeDirtyFlag ;
    }   

    /**
     * 获取 [创建时间]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return this.create_date ;
    }

    /**
     * 设置 [创建时间]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

     /**
     * 获取 [创建时间]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return this.create_dateDirtyFlag ;
    }   

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return this.create_uid ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

     /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return this.create_uidDirtyFlag ;
    }   

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return this.create_uid_text ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

     /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return this.create_uid_textDirtyFlag ;
    }   

    /**
     * 获取 [日期]
     */
    @JsonProperty("date")
    public Timestamp getDate(){
        return this.date ;
    }

    /**
     * 设置 [日期]
     */
    @JsonProperty("date")
    public void setDate(Timestamp  date){
        this.date = date ;
        this.dateDirtyFlag = true ;
    }

     /**
     * 获取 [日期]脏标记
     */
    @JsonIgnore
    public boolean getDateDirtyFlag(){
        return this.dateDirtyFlag ;
    }   

    /**
     * 获取 [成本说明]
     */
    @JsonProperty("description")
    public String getDescription(){
        return this.description ;
    }

    /**
     * 设置 [成本说明]
     */
    @JsonProperty("description")
    public void setDescription(String  description){
        this.description = description ;
        this.descriptionDirtyFlag = true ;
    }

     /**
     * 获取 [成本说明]脏标记
     */
    @JsonIgnore
    public boolean getDescriptionDirtyFlag(){
        return this.descriptionDirtyFlag ;
    }   

    /**
     * 获取 [显示名称]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return this.display_name ;
    }

    /**
     * 设置 [显示名称]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

     /**
     * 获取 [显示名称]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return this.display_nameDirtyFlag ;
    }   

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return this.id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

     /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return this.idDirtyFlag ;
    }   

    /**
     * 获取 [发票参考]
     */
    @JsonProperty("inv_ref")
    public String getInv_ref(){
        return this.inv_ref ;
    }

    /**
     * 设置 [发票参考]
     */
    @JsonProperty("inv_ref")
    public void setInv_ref(String  inv_ref){
        this.inv_ref = inv_ref ;
        this.inv_refDirtyFlag = true ;
    }

     /**
     * 获取 [发票参考]脏标记
     */
    @JsonIgnore
    public boolean getInv_refDirtyFlag(){
        return this.inv_refDirtyFlag ;
    }   

    /**
     * 获取 [名称]
     */
    @JsonProperty("name")
    public String getName(){
        return this.name ;
    }

    /**
     * 设置 [名称]
     */
    @JsonProperty("name")
    public void setName(String  name){
        this.name = name ;
        this.nameDirtyFlag = true ;
    }

     /**
     * 获取 [名称]脏标记
     */
    @JsonIgnore
    public boolean getNameDirtyFlag(){
        return this.nameDirtyFlag ;
    }   

    /**
     * 获取 [便签]
     */
    @JsonProperty("notes")
    public String getNotes(){
        return this.notes ;
    }

    /**
     * 设置 [便签]
     */
    @JsonProperty("notes")
    public void setNotes(String  notes){
        this.notes = notes ;
        this.notesDirtyFlag = true ;
    }

     /**
     * 获取 [便签]脏标记
     */
    @JsonIgnore
    public boolean getNotesDirtyFlag(){
        return this.notesDirtyFlag ;
    }   

    /**
     * 获取 [里程表数值]
     */
    @JsonProperty("odometer")
    public Double getOdometer(){
        return this.odometer ;
    }

    /**
     * 设置 [里程表数值]
     */
    @JsonProperty("odometer")
    public void setOdometer(Double  odometer){
        this.odometer = odometer ;
        this.odometerDirtyFlag = true ;
    }

     /**
     * 获取 [里程表数值]脏标记
     */
    @JsonIgnore
    public boolean getOdometerDirtyFlag(){
        return this.odometerDirtyFlag ;
    }   

    /**
     * 获取 [里程表]
     */
    @JsonProperty("odometer_id")
    public Integer getOdometer_id(){
        return this.odometer_id ;
    }

    /**
     * 设置 [里程表]
     */
    @JsonProperty("odometer_id")
    public void setOdometer_id(Integer  odometer_id){
        this.odometer_id = odometer_id ;
        this.odometer_idDirtyFlag = true ;
    }

     /**
     * 获取 [里程表]脏标记
     */
    @JsonIgnore
    public boolean getOdometer_idDirtyFlag(){
        return this.odometer_idDirtyFlag ;
    }   

    /**
     * 获取 [单位]
     */
    @JsonProperty("odometer_unit")
    public String getOdometer_unit(){
        return this.odometer_unit ;
    }

    /**
     * 设置 [单位]
     */
    @JsonProperty("odometer_unit")
    public void setOdometer_unit(String  odometer_unit){
        this.odometer_unit = odometer_unit ;
        this.odometer_unitDirtyFlag = true ;
    }

     /**
     * 获取 [单位]脏标记
     */
    @JsonIgnore
    public boolean getOdometer_unitDirtyFlag(){
        return this.odometer_unitDirtyFlag ;
    }   

    /**
     * 获取 [上级]
     */
    @JsonProperty("parent_id")
    public Integer getParent_id(){
        return this.parent_id ;
    }

    /**
     * 设置 [上级]
     */
    @JsonProperty("parent_id")
    public void setParent_id(Integer  parent_id){
        this.parent_id = parent_id ;
        this.parent_idDirtyFlag = true ;
    }

     /**
     * 获取 [上级]脏标记
     */
    @JsonIgnore
    public boolean getParent_idDirtyFlag(){
        return this.parent_idDirtyFlag ;
    }   

    /**
     * 获取 [采购]
     */
    @JsonProperty("purchaser_id")
    public Integer getPurchaser_id(){
        return this.purchaser_id ;
    }

    /**
     * 设置 [采购]
     */
    @JsonProperty("purchaser_id")
    public void setPurchaser_id(Integer  purchaser_id){
        this.purchaser_id = purchaser_id ;
        this.purchaser_idDirtyFlag = true ;
    }

     /**
     * 获取 [采购]脏标记
     */
    @JsonIgnore
    public boolean getPurchaser_idDirtyFlag(){
        return this.purchaser_idDirtyFlag ;
    }   

    /**
     * 获取 [采购]
     */
    @JsonProperty("purchaser_id_text")
    public String getPurchaser_id_text(){
        return this.purchaser_id_text ;
    }

    /**
     * 设置 [采购]
     */
    @JsonProperty("purchaser_id_text")
    public void setPurchaser_id_text(String  purchaser_id_text){
        this.purchaser_id_text = purchaser_id_text ;
        this.purchaser_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [采购]脏标记
     */
    @JsonIgnore
    public boolean getPurchaser_id_textDirtyFlag(){
        return this.purchaser_id_textDirtyFlag ;
    }   

    /**
     * 获取 [车辆]
     */
    @JsonProperty("vehicle_id")
    public Integer getVehicle_id(){
        return this.vehicle_id ;
    }

    /**
     * 设置 [车辆]
     */
    @JsonProperty("vehicle_id")
    public void setVehicle_id(Integer  vehicle_id){
        this.vehicle_id = vehicle_id ;
        this.vehicle_idDirtyFlag = true ;
    }

     /**
     * 获取 [车辆]脏标记
     */
    @JsonIgnore
    public boolean getVehicle_idDirtyFlag(){
        return this.vehicle_idDirtyFlag ;
    }   

    /**
     * 获取 [供应商]
     */
    @JsonProperty("vendor_id")
    public Integer getVendor_id(){
        return this.vendor_id ;
    }

    /**
     * 设置 [供应商]
     */
    @JsonProperty("vendor_id")
    public void setVendor_id(Integer  vendor_id){
        this.vendor_id = vendor_id ;
        this.vendor_idDirtyFlag = true ;
    }

     /**
     * 获取 [供应商]脏标记
     */
    @JsonIgnore
    public boolean getVendor_idDirtyFlag(){
        return this.vendor_idDirtyFlag ;
    }   

    /**
     * 获取 [供应商]
     */
    @JsonProperty("vendor_id_text")
    public String getVendor_id_text(){
        return this.vendor_id_text ;
    }

    /**
     * 设置 [供应商]
     */
    @JsonProperty("vendor_id_text")
    public void setVendor_id_text(String  vendor_id_text){
        this.vendor_id_text = vendor_id_text ;
        this.vendor_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [供应商]脏标记
     */
    @JsonIgnore
    public boolean getVendor_id_textDirtyFlag(){
        return this.vendor_id_textDirtyFlag ;
    }   

    /**
     * 获取 [最后更新时间]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return this.write_date ;
    }

    /**
     * 设置 [最后更新时间]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新时间]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return this.write_dateDirtyFlag ;
    }   

    /**
     * 获取 [最后更新人]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return this.write_uid ;
    }

    /**
     * 设置 [最后更新人]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新人]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return this.write_uidDirtyFlag ;
    }   

    /**
     * 获取 [最后更新人]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return this.write_uid_text ;
    }

    /**
     * 设置 [最后更新人]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新人]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return this.write_uid_textDirtyFlag ;
    }   

    /**
     * 获取 [最后修改日]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return this.__last_update ;
    }

    /**
     * 设置 [最后修改日]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

     /**
     * 获取 [最后修改日]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return this.__last_updateDirtyFlag ;
    }   



    public void fromMap(Map<String, Object> map) throws Exception {
		if(!(map.get("amount") instanceof Boolean)&& map.get("amount")!=null){
			this.setAmount((Double)map.get("amount"));
		}
		if(map.get("auto_generated") instanceof Boolean){
			this.setAuto_generated(((Boolean)map.get("auto_generated"))? "true" : "false");
		}
		if(!(map.get("contract_id") instanceof Boolean)&& map.get("contract_id")!=null){
			Object[] objs = (Object[])map.get("contract_id");
			if(objs.length > 0){
				this.setContract_id((Integer)objs[0]);
			}
		}
		if(!(map.get("cost_amount") instanceof Boolean)&& map.get("cost_amount")!=null){
			this.setCost_amount((Double)map.get("cost_amount"));
		}
		if(!(map.get("cost_id") instanceof Boolean)&& map.get("cost_id")!=null){
			Object[] objs = (Object[])map.get("cost_id");
			if(objs.length > 0){
				this.setCost_id((Integer)objs[0]);
			}
		}
		if(!(map.get("cost_ids") instanceof Boolean)&& map.get("cost_ids")!=null){
			Object[] objs = (Object[])map.get("cost_ids");
			if(objs.length > 0){
				Integer[] cost_ids = Arrays.copyOfRange(objs,0,objs.length,Integer[].class);
				this.setCost_ids(Arrays.toString(cost_ids));
			}
		}
		if(!(map.get("cost_subtype_id") instanceof Boolean)&& map.get("cost_subtype_id")!=null){
			Object[] objs = (Object[])map.get("cost_subtype_id");
			if(objs.length > 0){
				this.setCost_subtype_id((Integer)objs[0]);
			}
		}
		if(!(map.get("cost_type") instanceof Boolean)&& map.get("cost_type")!=null){
			this.setCost_type((String)map.get("cost_type"));
		}
		if(!(map.get("create_date") instanceof Boolean)&& map.get("create_date")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse((String)map.get("create_date"));
   			this.setCreate_date(new Timestamp(parse.getTime()));
		}
		if(!(map.get("create_uid") instanceof Boolean)&& map.get("create_uid")!=null){
			Object[] objs = (Object[])map.get("create_uid");
			if(objs.length > 0){
				this.setCreate_uid((Integer)objs[0]);
			}
		}
		if(!(map.get("create_uid") instanceof Boolean)&& map.get("create_uid")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("create_uid");
			if(objs.length > 1){
				this.setCreate_uid_text((String)objs[1]);
			}
		}
		if(!(map.get("date") instanceof Boolean)&& map.get("date")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd").parse((String)map.get("date"));
   			this.setDate(new Timestamp(parse.getTime()));
		}
		if(!(map.get("description") instanceof Boolean)&& map.get("description")!=null){
			this.setDescription((String)map.get("description"));
		}
		if(!(map.get("display_name") instanceof Boolean)&& map.get("display_name")!=null){
			this.setDisplay_name((String)map.get("display_name"));
		}
		if(!(map.get("id") instanceof Boolean)&& map.get("id")!=null){
			this.setId((Integer)map.get("id"));
		}
		if(!(map.get("inv_ref") instanceof Boolean)&& map.get("inv_ref")!=null){
			this.setInv_ref((String)map.get("inv_ref"));
		}
		if(!(map.get("name") instanceof Boolean)&& map.get("name")!=null){
			this.setName((String)map.get("name"));
		}
		if(!(map.get("notes") instanceof Boolean)&& map.get("notes")!=null){
			this.setNotes((String)map.get("notes"));
		}
		if(!(map.get("odometer") instanceof Boolean)&& map.get("odometer")!=null){
			this.setOdometer((Double)map.get("odometer"));
		}
		if(!(map.get("odometer_id") instanceof Boolean)&& map.get("odometer_id")!=null){
			Object[] objs = (Object[])map.get("odometer_id");
			if(objs.length > 0){
				this.setOdometer_id((Integer)objs[0]);
			}
		}
		if(!(map.get("odometer_unit") instanceof Boolean)&& map.get("odometer_unit")!=null){
			this.setOdometer_unit((String)map.get("odometer_unit"));
		}
		if(!(map.get("parent_id") instanceof Boolean)&& map.get("parent_id")!=null){
			Object[] objs = (Object[])map.get("parent_id");
			if(objs.length > 0){
				this.setParent_id((Integer)objs[0]);
			}
		}
		if(!(map.get("purchaser_id") instanceof Boolean)&& map.get("purchaser_id")!=null){
			Object[] objs = (Object[])map.get("purchaser_id");
			if(objs.length > 0){
				this.setPurchaser_id((Integer)objs[0]);
			}
		}
		if(!(map.get("purchaser_id") instanceof Boolean)&& map.get("purchaser_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("purchaser_id");
			if(objs.length > 1){
				this.setPurchaser_id_text((String)objs[1]);
			}
		}
		if(!(map.get("vehicle_id") instanceof Boolean)&& map.get("vehicle_id")!=null){
			Object[] objs = (Object[])map.get("vehicle_id");
			if(objs.length > 0){
				this.setVehicle_id((Integer)objs[0]);
			}
		}
		if(!(map.get("vendor_id") instanceof Boolean)&& map.get("vendor_id")!=null){
			Object[] objs = (Object[])map.get("vendor_id");
			if(objs.length > 0){
				this.setVendor_id((Integer)objs[0]);
			}
		}
		if(!(map.get("vendor_id") instanceof Boolean)&& map.get("vendor_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("vendor_id");
			if(objs.length > 1){
				this.setVendor_id_text((String)objs[1]);
			}
		}
		if(!(map.get("write_date") instanceof Boolean)&& map.get("write_date")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse((String)map.get("write_date"));
   			this.setWrite_date(new Timestamp(parse.getTime()));
		}
		if(!(map.get("write_uid") instanceof Boolean)&& map.get("write_uid")!=null){
			Object[] objs = (Object[])map.get("write_uid");
			if(objs.length > 0){
				this.setWrite_uid((Integer)objs[0]);
			}
		}
		if(!(map.get("write_uid") instanceof Boolean)&& map.get("write_uid")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("write_uid");
			if(objs.length > 1){
				this.setWrite_uid_text((String)objs[1]);
			}
		}
		if(!(map.get("__last_update") instanceof Boolean)&& map.get("__last_update")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse((String)map.get("__last_update"));
   			this.set__last_update(new Timestamp(parse.getTime()));
		}
	}

	public Map<String, Object> toMap() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		if(this.getAmount()!=null&&this.getAmountDirtyFlag()){
			map.put("amount",this.getAmount());
		}else if(this.getAmountDirtyFlag()){
			map.put("amount",false);
		}
		if(this.getAuto_generated()!=null&&this.getAuto_generatedDirtyFlag()){
			map.put("auto_generated",Boolean.parseBoolean(this.getAuto_generated()));		
		}		if(this.getContract_id()!=null&&this.getContract_idDirtyFlag()){
			map.put("contract_id",this.getContract_id());
		}else if(this.getContract_idDirtyFlag()){
			map.put("contract_id",false);
		}
		if(this.getCost_amount()!=null&&this.getCost_amountDirtyFlag()){
			map.put("cost_amount",this.getCost_amount());
		}else if(this.getCost_amountDirtyFlag()){
			map.put("cost_amount",false);
		}
		if(this.getCost_id()!=null&&this.getCost_idDirtyFlag()){
			map.put("cost_id",this.getCost_id());
		}else if(this.getCost_idDirtyFlag()){
			map.put("cost_id",false);
		}
		if(this.getCost_ids()!=null&&this.getCost_idsDirtyFlag()){
			map.put("cost_ids",this.getCost_ids());
		}else if(this.getCost_idsDirtyFlag()){
			map.put("cost_ids",false);
		}
		if(this.getCost_subtype_id()!=null&&this.getCost_subtype_idDirtyFlag()){
			map.put("cost_subtype_id",this.getCost_subtype_id());
		}else if(this.getCost_subtype_idDirtyFlag()){
			map.put("cost_subtype_id",false);
		}
		if(this.getCost_type()!=null&&this.getCost_typeDirtyFlag()){
			map.put("cost_type",this.getCost_type());
		}else if(this.getCost_typeDirtyFlag()){
			map.put("cost_type",false);
		}
		if(this.getCreate_date()!=null&&this.getCreate_dateDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String datetimeStr = sdf.format(this.getCreate_date());
			map.put("create_date",datetimeStr);
		}else if(this.getCreate_dateDirtyFlag()){
			map.put("create_date",false);
		}
		if(this.getCreate_uid()!=null&&this.getCreate_uidDirtyFlag()){
			map.put("create_uid",this.getCreate_uid());
		}else if(this.getCreate_uidDirtyFlag()){
			map.put("create_uid",false);
		}
		if(this.getCreate_uid_text()!=null&&this.getCreate_uid_textDirtyFlag()){
			//忽略文本外键create_uid_text
		}else if(this.getCreate_uid_textDirtyFlag()){
			map.put("create_uid",false);
		}
		if(this.getDate()!=null&&this.getDateDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			String datetimeStr = sdf.format(this.getDate());
			map.put("date",datetimeStr);
		}else if(this.getDateDirtyFlag()){
			map.put("date",false);
		}
		if(this.getDescription()!=null&&this.getDescriptionDirtyFlag()){
			map.put("description",this.getDescription());
		}else if(this.getDescriptionDirtyFlag()){
			map.put("description",false);
		}
		if(this.getDisplay_name()!=null&&this.getDisplay_nameDirtyFlag()){
			map.put("display_name",this.getDisplay_name());
		}else if(this.getDisplay_nameDirtyFlag()){
			map.put("display_name",false);
		}
		if(this.getId()!=null&&this.getIdDirtyFlag()){
			map.put("id",this.getId());
		}else if(this.getIdDirtyFlag()){
			map.put("id",false);
		}
		if(this.getInv_ref()!=null&&this.getInv_refDirtyFlag()){
			map.put("inv_ref",this.getInv_ref());
		}else if(this.getInv_refDirtyFlag()){
			map.put("inv_ref",false);
		}
		if(this.getName()!=null&&this.getNameDirtyFlag()){
			map.put("name",this.getName());
		}else if(this.getNameDirtyFlag()){
			map.put("name",false);
		}
		if(this.getNotes()!=null&&this.getNotesDirtyFlag()){
			map.put("notes",this.getNotes());
		}else if(this.getNotesDirtyFlag()){
			map.put("notes",false);
		}
		if(this.getOdometer()!=null&&this.getOdometerDirtyFlag()){
			map.put("odometer",this.getOdometer());
		}else if(this.getOdometerDirtyFlag()){
			map.put("odometer",false);
		}
		if(this.getOdometer_id()!=null&&this.getOdometer_idDirtyFlag()){
			map.put("odometer_id",this.getOdometer_id());
		}else if(this.getOdometer_idDirtyFlag()){
			map.put("odometer_id",false);
		}
		if(this.getOdometer_unit()!=null&&this.getOdometer_unitDirtyFlag()){
			map.put("odometer_unit",this.getOdometer_unit());
		}else if(this.getOdometer_unitDirtyFlag()){
			map.put("odometer_unit",false);
		}
		if(this.getParent_id()!=null&&this.getParent_idDirtyFlag()){
			map.put("parent_id",this.getParent_id());
		}else if(this.getParent_idDirtyFlag()){
			map.put("parent_id",false);
		}
		if(this.getPurchaser_id()!=null&&this.getPurchaser_idDirtyFlag()){
			map.put("purchaser_id",this.getPurchaser_id());
		}else if(this.getPurchaser_idDirtyFlag()){
			map.put("purchaser_id",false);
		}
		if(this.getPurchaser_id_text()!=null&&this.getPurchaser_id_textDirtyFlag()){
			//忽略文本外键purchaser_id_text
		}else if(this.getPurchaser_id_textDirtyFlag()){
			map.put("purchaser_id",false);
		}
		if(this.getVehicle_id()!=null&&this.getVehicle_idDirtyFlag()){
			map.put("vehicle_id",this.getVehicle_id());
		}else if(this.getVehicle_idDirtyFlag()){
			map.put("vehicle_id",false);
		}
		if(this.getVendor_id()!=null&&this.getVendor_idDirtyFlag()){
			map.put("vendor_id",this.getVendor_id());
		}else if(this.getVendor_idDirtyFlag()){
			map.put("vendor_id",false);
		}
		if(this.getVendor_id_text()!=null&&this.getVendor_id_textDirtyFlag()){
			//忽略文本外键vendor_id_text
		}else if(this.getVendor_id_textDirtyFlag()){
			map.put("vendor_id",false);
		}
		if(this.getWrite_date()!=null&&this.getWrite_dateDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String datetimeStr = sdf.format(this.getWrite_date());
			map.put("write_date",datetimeStr);
		}else if(this.getWrite_dateDirtyFlag()){
			map.put("write_date",false);
		}
		if(this.getWrite_uid()!=null&&this.getWrite_uidDirtyFlag()){
			map.put("write_uid",this.getWrite_uid());
		}else if(this.getWrite_uidDirtyFlag()){
			map.put("write_uid",false);
		}
		if(this.getWrite_uid_text()!=null&&this.getWrite_uid_textDirtyFlag()){
			//忽略文本外键write_uid_text
		}else if(this.getWrite_uid_textDirtyFlag()){
			map.put("write_uid",false);
		}
		if(this.get__last_update()!=null&&this.get__last_updateDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String datetimeStr = sdf.format(this.get__last_update());
			map.put("__last_update",datetimeStr);
		}else if(this.get__last_updateDirtyFlag()){
			map.put("__last_update",false);
		}
		return map;
	}

}
