package cn.ibizlab.odoo.client.odoo_fleet.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Ifleet_service_type;
import cn.ibizlab.odoo.core.client.service.Ifleet_service_typeClientService;
import cn.ibizlab.odoo.client.odoo_fleet.model.fleet_service_typeImpl;
import cn.ibizlab.odoo.client.odoo_fleet.odooclient.Ifleet_service_typeOdooClient;
import cn.ibizlab.odoo.client.odoo_fleet.odooclient.impl.fleet_service_typeOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[fleet_service_type] 服务对象接口
 */
@Service
public class fleet_service_typeClientServiceImpl implements Ifleet_service_typeClientService {
    @Autowired
    private  Ifleet_service_typeOdooClient  fleet_service_typeOdooClient;

    public Ifleet_service_type createModel() {		
		return new fleet_service_typeImpl();
	}


        public void updateBatch(List<Ifleet_service_type> fleet_service_types){
            
        }
        
        public void update(Ifleet_service_type fleet_service_type){
this.fleet_service_typeOdooClient.update(fleet_service_type) ;
        }
        
        public void removeBatch(List<Ifleet_service_type> fleet_service_types){
            
        }
        
        public void get(Ifleet_service_type fleet_service_type){
            this.fleet_service_typeOdooClient.get(fleet_service_type) ;
        }
        
        public void createBatch(List<Ifleet_service_type> fleet_service_types){
            
        }
        
        public void create(Ifleet_service_type fleet_service_type){
this.fleet_service_typeOdooClient.create(fleet_service_type) ;
        }
        
        public void remove(Ifleet_service_type fleet_service_type){
this.fleet_service_typeOdooClient.remove(fleet_service_type) ;
        }
        
        public Page<Ifleet_service_type> search(SearchContext context){
            return this.fleet_service_typeOdooClient.search(context) ;
        }
        
        public Page<Ifleet_service_type> select(SearchContext context){
            return null ;
        }
        

}

