package cn.ibizlab.odoo.client.odoo_fleet.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Ifleet_vehicle_model_brand;
import cn.ibizlab.odoo.core.client.service.Ifleet_vehicle_model_brandClientService;
import cn.ibizlab.odoo.client.odoo_fleet.model.fleet_vehicle_model_brandImpl;
import cn.ibizlab.odoo.client.odoo_fleet.odooclient.Ifleet_vehicle_model_brandOdooClient;
import cn.ibizlab.odoo.client.odoo_fleet.odooclient.impl.fleet_vehicle_model_brandOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[fleet_vehicle_model_brand] 服务对象接口
 */
@Service
public class fleet_vehicle_model_brandClientServiceImpl implements Ifleet_vehicle_model_brandClientService {
    @Autowired
    private  Ifleet_vehicle_model_brandOdooClient  fleet_vehicle_model_brandOdooClient;

    public Ifleet_vehicle_model_brand createModel() {		
		return new fleet_vehicle_model_brandImpl();
	}


        public void updateBatch(List<Ifleet_vehicle_model_brand> fleet_vehicle_model_brands){
            
        }
        
        public void createBatch(List<Ifleet_vehicle_model_brand> fleet_vehicle_model_brands){
            
        }
        
        public void removeBatch(List<Ifleet_vehicle_model_brand> fleet_vehicle_model_brands){
            
        }
        
        public void get(Ifleet_vehicle_model_brand fleet_vehicle_model_brand){
            this.fleet_vehicle_model_brandOdooClient.get(fleet_vehicle_model_brand) ;
        }
        
        public Page<Ifleet_vehicle_model_brand> search(SearchContext context){
            return this.fleet_vehicle_model_brandOdooClient.search(context) ;
        }
        
        public void remove(Ifleet_vehicle_model_brand fleet_vehicle_model_brand){
this.fleet_vehicle_model_brandOdooClient.remove(fleet_vehicle_model_brand) ;
        }
        
        public void update(Ifleet_vehicle_model_brand fleet_vehicle_model_brand){
this.fleet_vehicle_model_brandOdooClient.update(fleet_vehicle_model_brand) ;
        }
        
        public void create(Ifleet_vehicle_model_brand fleet_vehicle_model_brand){
this.fleet_vehicle_model_brandOdooClient.create(fleet_vehicle_model_brand) ;
        }
        
        public Page<Ifleet_vehicle_model_brand> select(SearchContext context){
            return null ;
        }
        

}

