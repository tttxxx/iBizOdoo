package cn.ibizlab.odoo.client.odoo_fleet.odooclient;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.core.client.model.Ifleet_vehicle_log_fuel;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[fleet_vehicle_log_fuel] 服务对象客户端接口
 */
public interface Ifleet_vehicle_log_fuelOdooClient {
    
        public void removeBatch(Ifleet_vehicle_log_fuel fleet_vehicle_log_fuel);

        public void update(Ifleet_vehicle_log_fuel fleet_vehicle_log_fuel);

        public void create(Ifleet_vehicle_log_fuel fleet_vehicle_log_fuel);

        public void updateBatch(Ifleet_vehicle_log_fuel fleet_vehicle_log_fuel);

        public void remove(Ifleet_vehicle_log_fuel fleet_vehicle_log_fuel);

        public void get(Ifleet_vehicle_log_fuel fleet_vehicle_log_fuel);

        public Page<Ifleet_vehicle_log_fuel> search(SearchContext context);

        public void createBatch(Ifleet_vehicle_log_fuel fleet_vehicle_log_fuel);

        public List<Ifleet_vehicle_log_fuel> select();


}