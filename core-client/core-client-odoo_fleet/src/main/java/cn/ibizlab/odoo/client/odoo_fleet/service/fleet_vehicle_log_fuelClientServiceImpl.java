package cn.ibizlab.odoo.client.odoo_fleet.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Ifleet_vehicle_log_fuel;
import cn.ibizlab.odoo.core.client.service.Ifleet_vehicle_log_fuelClientService;
import cn.ibizlab.odoo.client.odoo_fleet.model.fleet_vehicle_log_fuelImpl;
import cn.ibizlab.odoo.client.odoo_fleet.odooclient.Ifleet_vehicle_log_fuelOdooClient;
import cn.ibizlab.odoo.client.odoo_fleet.odooclient.impl.fleet_vehicle_log_fuelOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[fleet_vehicle_log_fuel] 服务对象接口
 */
@Service
public class fleet_vehicle_log_fuelClientServiceImpl implements Ifleet_vehicle_log_fuelClientService {
    @Autowired
    private  Ifleet_vehicle_log_fuelOdooClient  fleet_vehicle_log_fuelOdooClient;

    public Ifleet_vehicle_log_fuel createModel() {		
		return new fleet_vehicle_log_fuelImpl();
	}


        public void removeBatch(List<Ifleet_vehicle_log_fuel> fleet_vehicle_log_fuels){
            
        }
        
        public void update(Ifleet_vehicle_log_fuel fleet_vehicle_log_fuel){
this.fleet_vehicle_log_fuelOdooClient.update(fleet_vehicle_log_fuel) ;
        }
        
        public void create(Ifleet_vehicle_log_fuel fleet_vehicle_log_fuel){
this.fleet_vehicle_log_fuelOdooClient.create(fleet_vehicle_log_fuel) ;
        }
        
        public void updateBatch(List<Ifleet_vehicle_log_fuel> fleet_vehicle_log_fuels){
            
        }
        
        public void remove(Ifleet_vehicle_log_fuel fleet_vehicle_log_fuel){
this.fleet_vehicle_log_fuelOdooClient.remove(fleet_vehicle_log_fuel) ;
        }
        
        public void get(Ifleet_vehicle_log_fuel fleet_vehicle_log_fuel){
            this.fleet_vehicle_log_fuelOdooClient.get(fleet_vehicle_log_fuel) ;
        }
        
        public Page<Ifleet_vehicle_log_fuel> search(SearchContext context){
            return this.fleet_vehicle_log_fuelOdooClient.search(context) ;
        }
        
        public void createBatch(List<Ifleet_vehicle_log_fuel> fleet_vehicle_log_fuels){
            
        }
        
        public Page<Ifleet_vehicle_log_fuel> select(SearchContext context){
            return null ;
        }
        

}

