package cn.ibizlab.odoo.client.odoo_payment.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Ipayment_token;
import cn.ibizlab.odoo.core.client.service.Ipayment_tokenClientService;
import cn.ibizlab.odoo.client.odoo_payment.model.payment_tokenImpl;
import cn.ibizlab.odoo.client.odoo_payment.odooclient.Ipayment_tokenOdooClient;
import cn.ibizlab.odoo.client.odoo_payment.odooclient.impl.payment_tokenOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[payment_token] 服务对象接口
 */
@Service
public class payment_tokenClientServiceImpl implements Ipayment_tokenClientService {
    @Autowired
    private  Ipayment_tokenOdooClient  payment_tokenOdooClient;

    public Ipayment_token createModel() {		
		return new payment_tokenImpl();
	}


        public void get(Ipayment_token payment_token){
            this.payment_tokenOdooClient.get(payment_token) ;
        }
        
        public void updateBatch(List<Ipayment_token> payment_tokens){
            
        }
        
        public void remove(Ipayment_token payment_token){
this.payment_tokenOdooClient.remove(payment_token) ;
        }
        
        public void createBatch(List<Ipayment_token> payment_tokens){
            
        }
        
        public void create(Ipayment_token payment_token){
this.payment_tokenOdooClient.create(payment_token) ;
        }
        
        public Page<Ipayment_token> search(SearchContext context){
            return this.payment_tokenOdooClient.search(context) ;
        }
        
        public void removeBatch(List<Ipayment_token> payment_tokens){
            
        }
        
        public void update(Ipayment_token payment_token){
this.payment_tokenOdooClient.update(payment_token) ;
        }
        
        public Page<Ipayment_token> select(SearchContext context){
            return null ;
        }
        

}

