package cn.ibizlab.odoo.client.odoo_payment.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Ipayment_acquirer_onboarding_wizard;
import cn.ibizlab.odoo.core.client.service.Ipayment_acquirer_onboarding_wizardClientService;
import cn.ibizlab.odoo.client.odoo_payment.model.payment_acquirer_onboarding_wizardImpl;
import cn.ibizlab.odoo.client.odoo_payment.odooclient.Ipayment_acquirer_onboarding_wizardOdooClient;
import cn.ibizlab.odoo.client.odoo_payment.odooclient.impl.payment_acquirer_onboarding_wizardOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[payment_acquirer_onboarding_wizard] 服务对象接口
 */
@Service
public class payment_acquirer_onboarding_wizardClientServiceImpl implements Ipayment_acquirer_onboarding_wizardClientService {
    @Autowired
    private  Ipayment_acquirer_onboarding_wizardOdooClient  payment_acquirer_onboarding_wizardOdooClient;

    public Ipayment_acquirer_onboarding_wizard createModel() {		
		return new payment_acquirer_onboarding_wizardImpl();
	}


        public void updateBatch(List<Ipayment_acquirer_onboarding_wizard> payment_acquirer_onboarding_wizards){
            
        }
        
        public void create(Ipayment_acquirer_onboarding_wizard payment_acquirer_onboarding_wizard){
this.payment_acquirer_onboarding_wizardOdooClient.create(payment_acquirer_onboarding_wizard) ;
        }
        
        public Page<Ipayment_acquirer_onboarding_wizard> search(SearchContext context){
            return this.payment_acquirer_onboarding_wizardOdooClient.search(context) ;
        }
        
        public void update(Ipayment_acquirer_onboarding_wizard payment_acquirer_onboarding_wizard){
this.payment_acquirer_onboarding_wizardOdooClient.update(payment_acquirer_onboarding_wizard) ;
        }
        
        public void createBatch(List<Ipayment_acquirer_onboarding_wizard> payment_acquirer_onboarding_wizards){
            
        }
        
        public void get(Ipayment_acquirer_onboarding_wizard payment_acquirer_onboarding_wizard){
            this.payment_acquirer_onboarding_wizardOdooClient.get(payment_acquirer_onboarding_wizard) ;
        }
        
        public void remove(Ipayment_acquirer_onboarding_wizard payment_acquirer_onboarding_wizard){
this.payment_acquirer_onboarding_wizardOdooClient.remove(payment_acquirer_onboarding_wizard) ;
        }
        
        public void removeBatch(List<Ipayment_acquirer_onboarding_wizard> payment_acquirer_onboarding_wizards){
            
        }
        
        public Page<Ipayment_acquirer_onboarding_wizard> select(SearchContext context){
            return null ;
        }
        

}

