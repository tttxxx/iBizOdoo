package cn.ibizlab.odoo.client.odoo_payment.odooclient;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.core.client.model.Ipayment_icon;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[payment_icon] 服务对象客户端接口
 */
public interface Ipayment_iconOdooClient {
    
        public void removeBatch(Ipayment_icon payment_icon);

        public void create(Ipayment_icon payment_icon);

        public void updateBatch(Ipayment_icon payment_icon);

        public void remove(Ipayment_icon payment_icon);

        public void createBatch(Ipayment_icon payment_icon);

        public Page<Ipayment_icon> search(SearchContext context);

        public void get(Ipayment_icon payment_icon);

        public void update(Ipayment_icon payment_icon);

        public List<Ipayment_icon> select();


}