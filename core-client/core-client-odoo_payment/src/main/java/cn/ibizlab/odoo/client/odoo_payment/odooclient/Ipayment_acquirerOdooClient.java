package cn.ibizlab.odoo.client.odoo_payment.odooclient;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.core.client.model.Ipayment_acquirer;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[payment_acquirer] 服务对象客户端接口
 */
public interface Ipayment_acquirerOdooClient {
    
        public void update(Ipayment_acquirer payment_acquirer);

        public void create(Ipayment_acquirer payment_acquirer);

        public Page<Ipayment_acquirer> search(SearchContext context);

        public void createBatch(Ipayment_acquirer payment_acquirer);

        public void get(Ipayment_acquirer payment_acquirer);

        public void removeBatch(Ipayment_acquirer payment_acquirer);

        public void remove(Ipayment_acquirer payment_acquirer);

        public void updateBatch(Ipayment_acquirer payment_acquirer);

        public List<Ipayment_acquirer> select();


}