package cn.ibizlab.odoo.client.odoo_payment.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Ipayment_acquirer;
import cn.ibizlab.odoo.core.client.service.Ipayment_acquirerClientService;
import cn.ibizlab.odoo.client.odoo_payment.model.payment_acquirerImpl;
import cn.ibizlab.odoo.client.odoo_payment.odooclient.Ipayment_acquirerOdooClient;
import cn.ibizlab.odoo.client.odoo_payment.odooclient.impl.payment_acquirerOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[payment_acquirer] 服务对象接口
 */
@Service
public class payment_acquirerClientServiceImpl implements Ipayment_acquirerClientService {
    @Autowired
    private  Ipayment_acquirerOdooClient  payment_acquirerOdooClient;

    public Ipayment_acquirer createModel() {		
		return new payment_acquirerImpl();
	}


        public void update(Ipayment_acquirer payment_acquirer){
this.payment_acquirerOdooClient.update(payment_acquirer) ;
        }
        
        public void create(Ipayment_acquirer payment_acquirer){
this.payment_acquirerOdooClient.create(payment_acquirer) ;
        }
        
        public Page<Ipayment_acquirer> search(SearchContext context){
            return this.payment_acquirerOdooClient.search(context) ;
        }
        
        public void createBatch(List<Ipayment_acquirer> payment_acquirers){
            
        }
        
        public void get(Ipayment_acquirer payment_acquirer){
            this.payment_acquirerOdooClient.get(payment_acquirer) ;
        }
        
        public void removeBatch(List<Ipayment_acquirer> payment_acquirers){
            
        }
        
        public void remove(Ipayment_acquirer payment_acquirer){
this.payment_acquirerOdooClient.remove(payment_acquirer) ;
        }
        
        public void updateBatch(List<Ipayment_acquirer> payment_acquirers){
            
        }
        
        public Page<Ipayment_acquirer> select(SearchContext context){
            return null ;
        }
        

}

