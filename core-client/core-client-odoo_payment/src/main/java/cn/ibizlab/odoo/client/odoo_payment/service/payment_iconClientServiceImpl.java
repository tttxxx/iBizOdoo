package cn.ibizlab.odoo.client.odoo_payment.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Ipayment_icon;
import cn.ibizlab.odoo.core.client.service.Ipayment_iconClientService;
import cn.ibizlab.odoo.client.odoo_payment.model.payment_iconImpl;
import cn.ibizlab.odoo.client.odoo_payment.odooclient.Ipayment_iconOdooClient;
import cn.ibizlab.odoo.client.odoo_payment.odooclient.impl.payment_iconOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[payment_icon] 服务对象接口
 */
@Service
public class payment_iconClientServiceImpl implements Ipayment_iconClientService {
    @Autowired
    private  Ipayment_iconOdooClient  payment_iconOdooClient;

    public Ipayment_icon createModel() {		
		return new payment_iconImpl();
	}


        public void removeBatch(List<Ipayment_icon> payment_icons){
            
        }
        
        public void create(Ipayment_icon payment_icon){
this.payment_iconOdooClient.create(payment_icon) ;
        }
        
        public void updateBatch(List<Ipayment_icon> payment_icons){
            
        }
        
        public void remove(Ipayment_icon payment_icon){
this.payment_iconOdooClient.remove(payment_icon) ;
        }
        
        public void createBatch(List<Ipayment_icon> payment_icons){
            
        }
        
        public Page<Ipayment_icon> search(SearchContext context){
            return this.payment_iconOdooClient.search(context) ;
        }
        
        public void get(Ipayment_icon payment_icon){
            this.payment_iconOdooClient.get(payment_icon) ;
        }
        
        public void update(Ipayment_icon payment_icon){
this.payment_iconOdooClient.update(payment_icon) ;
        }
        
        public Page<Ipayment_icon> select(SearchContext context){
            return null ;
        }
        

}

