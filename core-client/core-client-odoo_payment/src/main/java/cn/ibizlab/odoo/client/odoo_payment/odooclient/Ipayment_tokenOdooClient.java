package cn.ibizlab.odoo.client.odoo_payment.odooclient;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.core.client.model.Ipayment_token;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[payment_token] 服务对象客户端接口
 */
public interface Ipayment_tokenOdooClient {
    
        public void get(Ipayment_token payment_token);

        public void updateBatch(Ipayment_token payment_token);

        public void remove(Ipayment_token payment_token);

        public void createBatch(Ipayment_token payment_token);

        public void create(Ipayment_token payment_token);

        public Page<Ipayment_token> search(SearchContext context);

        public void removeBatch(Ipayment_token payment_token);

        public void update(Ipayment_token payment_token);

        public List<Ipayment_token> select();


}