package cn.ibizlab.odoo.client.odoo_payment.odooclient;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.core.client.model.Ipayment_acquirer_onboarding_wizard;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[payment_acquirer_onboarding_wizard] 服务对象客户端接口
 */
public interface Ipayment_acquirer_onboarding_wizardOdooClient {
    
        public void updateBatch(Ipayment_acquirer_onboarding_wizard payment_acquirer_onboarding_wizard);

        public void create(Ipayment_acquirer_onboarding_wizard payment_acquirer_onboarding_wizard);

        public Page<Ipayment_acquirer_onboarding_wizard> search(SearchContext context);

        public void update(Ipayment_acquirer_onboarding_wizard payment_acquirer_onboarding_wizard);

        public void createBatch(Ipayment_acquirer_onboarding_wizard payment_acquirer_onboarding_wizard);

        public void get(Ipayment_acquirer_onboarding_wizard payment_acquirer_onboarding_wizard);

        public void remove(Ipayment_acquirer_onboarding_wizard payment_acquirer_onboarding_wizard);

        public void removeBatch(Ipayment_acquirer_onboarding_wizard payment_acquirer_onboarding_wizard);

        public List<Ipayment_acquirer_onboarding_wizard> select();


}