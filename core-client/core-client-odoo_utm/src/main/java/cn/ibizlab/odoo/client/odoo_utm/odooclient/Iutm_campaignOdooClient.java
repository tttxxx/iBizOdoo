package cn.ibizlab.odoo.client.odoo_utm.odooclient;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.core.client.model.Iutm_campaign;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[utm_campaign] 服务对象客户端接口
 */
public interface Iutm_campaignOdooClient {
    
        public void remove(Iutm_campaign utm_campaign);

        public void get(Iutm_campaign utm_campaign);

        public Page<Iutm_campaign> search(SearchContext context);

        public void update(Iutm_campaign utm_campaign);

        public void createBatch(Iutm_campaign utm_campaign);

        public void create(Iutm_campaign utm_campaign);

        public void removeBatch(Iutm_campaign utm_campaign);

        public void updateBatch(Iutm_campaign utm_campaign);

        public List<Iutm_campaign> select();


}