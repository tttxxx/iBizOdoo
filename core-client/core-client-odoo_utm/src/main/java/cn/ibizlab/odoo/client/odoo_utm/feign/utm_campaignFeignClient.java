package cn.ibizlab.odoo.client.odoo_utm.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Iutm_campaign;
import cn.ibizlab.odoo.client.odoo_utm.model.utm_campaignImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[utm_campaign] 服务对象接口
 */
public interface utm_campaignFeignClient {

    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_utm/utm_campaigns/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_utm/utm_campaigns/{id}")
    public utm_campaignImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_utm/utm_campaigns/search")
    public Page<utm_campaignImpl> search(SearchContext context);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_utm/utm_campaigns/{id}")
    public utm_campaignImpl update(@PathVariable("id") Integer id,@RequestBody utm_campaignImpl utm_campaign);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_utm/utm_campaigns/createbatch")
    public utm_campaignImpl createBatch(@RequestBody List<utm_campaignImpl> utm_campaigns);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_utm/utm_campaigns")
    public utm_campaignImpl create(@RequestBody utm_campaignImpl utm_campaign);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_utm/utm_campaigns/removebatch")
    public utm_campaignImpl removeBatch(@RequestBody List<utm_campaignImpl> utm_campaigns);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_utm/utm_campaigns/updatebatch")
    public utm_campaignImpl updateBatch(@RequestBody List<utm_campaignImpl> utm_campaigns);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_utm/utm_campaigns/select")
    public Page<utm_campaignImpl> select();



}
