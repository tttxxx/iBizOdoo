package cn.ibizlab.odoo.client.odoo_utm.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import lombok.Data;

@ConfigurationProperties(prefix = "client.odoo.utm")
@Data
public class odoo_utmClientProperties {

	private String tokenUrl ;

	private String clientId ;

	private String clientSecret ;

}
