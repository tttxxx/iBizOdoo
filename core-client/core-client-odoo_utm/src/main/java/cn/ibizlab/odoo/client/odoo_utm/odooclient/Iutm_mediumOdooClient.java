package cn.ibizlab.odoo.client.odoo_utm.odooclient;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.core.client.model.Iutm_medium;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[utm_medium] 服务对象客户端接口
 */
public interface Iutm_mediumOdooClient {
    
        public void removeBatch(Iutm_medium utm_medium);

        public void createBatch(Iutm_medium utm_medium);

        public void create(Iutm_medium utm_medium);

        public void remove(Iutm_medium utm_medium);

        public void updateBatch(Iutm_medium utm_medium);

        public Page<Iutm_medium> search(SearchContext context);

        public void update(Iutm_medium utm_medium);

        public void get(Iutm_medium utm_medium);

        public List<Iutm_medium> select();


}