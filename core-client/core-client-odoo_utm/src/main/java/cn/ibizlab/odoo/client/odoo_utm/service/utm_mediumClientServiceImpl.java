package cn.ibizlab.odoo.client.odoo_utm.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Iutm_medium;
import cn.ibizlab.odoo.core.client.service.Iutm_mediumClientService;
import cn.ibizlab.odoo.client.odoo_utm.model.utm_mediumImpl;
import cn.ibizlab.odoo.client.odoo_utm.odooclient.Iutm_mediumOdooClient;
import cn.ibizlab.odoo.client.odoo_utm.odooclient.impl.utm_mediumOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[utm_medium] 服务对象接口
 */
@Service
public class utm_mediumClientServiceImpl implements Iutm_mediumClientService {
    @Autowired
    private  Iutm_mediumOdooClient  utm_mediumOdooClient;

    public Iutm_medium createModel() {		
		return new utm_mediumImpl();
	}


        public void removeBatch(List<Iutm_medium> utm_media){
            
        }
        
        public void createBatch(List<Iutm_medium> utm_media){
            
        }
        
        public void create(Iutm_medium utm_medium){
this.utm_mediumOdooClient.create(utm_medium) ;
        }
        
        public void remove(Iutm_medium utm_medium){
this.utm_mediumOdooClient.remove(utm_medium) ;
        }
        
        public void updateBatch(List<Iutm_medium> utm_media){
            
        }
        
        public Page<Iutm_medium> search(SearchContext context){
            return this.utm_mediumOdooClient.search(context) ;
        }
        
        public void update(Iutm_medium utm_medium){
this.utm_mediumOdooClient.update(utm_medium) ;
        }
        
        public void get(Iutm_medium utm_medium){
            this.utm_mediumOdooClient.get(utm_medium) ;
        }
        
        public Page<Iutm_medium> select(SearchContext context){
            return null ;
        }
        

}

