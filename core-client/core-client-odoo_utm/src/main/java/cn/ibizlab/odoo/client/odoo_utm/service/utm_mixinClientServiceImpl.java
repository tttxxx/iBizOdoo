package cn.ibizlab.odoo.client.odoo_utm.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Iutm_mixin;
import cn.ibizlab.odoo.core.client.service.Iutm_mixinClientService;
import cn.ibizlab.odoo.client.odoo_utm.model.utm_mixinImpl;
import cn.ibizlab.odoo.client.odoo_utm.odooclient.Iutm_mixinOdooClient;
import cn.ibizlab.odoo.client.odoo_utm.odooclient.impl.utm_mixinOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[utm_mixin] 服务对象接口
 */
@Service
public class utm_mixinClientServiceImpl implements Iutm_mixinClientService {
    @Autowired
    private  Iutm_mixinOdooClient  utm_mixinOdooClient;

    public Iutm_mixin createModel() {		
		return new utm_mixinImpl();
	}


        public void createBatch(List<Iutm_mixin> utm_mixins){
            
        }
        
        public void removeBatch(List<Iutm_mixin> utm_mixins){
            
        }
        
        public void remove(Iutm_mixin utm_mixin){
this.utm_mixinOdooClient.remove(utm_mixin) ;
        }
        
        public void updateBatch(List<Iutm_mixin> utm_mixins){
            
        }
        
        public void create(Iutm_mixin utm_mixin){
this.utm_mixinOdooClient.create(utm_mixin) ;
        }
        
        public void get(Iutm_mixin utm_mixin){
            this.utm_mixinOdooClient.get(utm_mixin) ;
        }
        
        public Page<Iutm_mixin> search(SearchContext context){
            return this.utm_mixinOdooClient.search(context) ;
        }
        
        public void update(Iutm_mixin utm_mixin){
this.utm_mixinOdooClient.update(utm_mixin) ;
        }
        
        public Page<Iutm_mixin> select(SearchContext context){
            return null ;
        }
        

}

