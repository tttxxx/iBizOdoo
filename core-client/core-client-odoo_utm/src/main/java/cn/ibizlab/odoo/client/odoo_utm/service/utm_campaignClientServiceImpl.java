package cn.ibizlab.odoo.client.odoo_utm.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Iutm_campaign;
import cn.ibizlab.odoo.core.client.service.Iutm_campaignClientService;
import cn.ibizlab.odoo.client.odoo_utm.model.utm_campaignImpl;
import cn.ibizlab.odoo.client.odoo_utm.odooclient.Iutm_campaignOdooClient;
import cn.ibizlab.odoo.client.odoo_utm.odooclient.impl.utm_campaignOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[utm_campaign] 服务对象接口
 */
@Service
public class utm_campaignClientServiceImpl implements Iutm_campaignClientService {
    @Autowired
    private  Iutm_campaignOdooClient  utm_campaignOdooClient;

    public Iutm_campaign createModel() {		
		return new utm_campaignImpl();
	}


        public void remove(Iutm_campaign utm_campaign){
this.utm_campaignOdooClient.remove(utm_campaign) ;
        }
        
        public void get(Iutm_campaign utm_campaign){
            this.utm_campaignOdooClient.get(utm_campaign) ;
        }
        
        public Page<Iutm_campaign> search(SearchContext context){
            return this.utm_campaignOdooClient.search(context) ;
        }
        
        public void update(Iutm_campaign utm_campaign){
this.utm_campaignOdooClient.update(utm_campaign) ;
        }
        
        public void createBatch(List<Iutm_campaign> utm_campaigns){
            
        }
        
        public void create(Iutm_campaign utm_campaign){
this.utm_campaignOdooClient.create(utm_campaign) ;
        }
        
        public void removeBatch(List<Iutm_campaign> utm_campaigns){
            
        }
        
        public void updateBatch(List<Iutm_campaign> utm_campaigns){
            
        }
        
        public Page<Iutm_campaign> select(SearchContext context){
            return null ;
        }
        

}

