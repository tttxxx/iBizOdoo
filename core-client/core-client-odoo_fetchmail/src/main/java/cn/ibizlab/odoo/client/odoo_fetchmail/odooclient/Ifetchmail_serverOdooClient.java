package cn.ibizlab.odoo.client.odoo_fetchmail.odooclient;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.core.client.model.Ifetchmail_server;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[fetchmail_server] 服务对象客户端接口
 */
public interface Ifetchmail_serverOdooClient {
    
        public void remove(Ifetchmail_server fetchmail_server);

        public void createBatch(Ifetchmail_server fetchmail_server);

        public void create(Ifetchmail_server fetchmail_server);

        public void update(Ifetchmail_server fetchmail_server);

        public Page<Ifetchmail_server> search(SearchContext context);

        public void removeBatch(Ifetchmail_server fetchmail_server);

        public void updateBatch(Ifetchmail_server fetchmail_server);

        public void get(Ifetchmail_server fetchmail_server);

        public List<Ifetchmail_server> select();


}