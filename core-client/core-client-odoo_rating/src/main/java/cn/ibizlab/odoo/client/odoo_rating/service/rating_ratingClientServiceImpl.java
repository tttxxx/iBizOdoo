package cn.ibizlab.odoo.client.odoo_rating.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Irating_rating;
import cn.ibizlab.odoo.core.client.service.Irating_ratingClientService;
import cn.ibizlab.odoo.client.odoo_rating.model.rating_ratingImpl;
import cn.ibizlab.odoo.client.odoo_rating.odooclient.Irating_ratingOdooClient;
import cn.ibizlab.odoo.client.odoo_rating.odooclient.impl.rating_ratingOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[rating_rating] 服务对象接口
 */
@Service
public class rating_ratingClientServiceImpl implements Irating_ratingClientService {
    @Autowired
    private  Irating_ratingOdooClient  rating_ratingOdooClient;

    public Irating_rating createModel() {		
		return new rating_ratingImpl();
	}


        public void get(Irating_rating rating_rating){
            this.rating_ratingOdooClient.get(rating_rating) ;
        }
        
        public void update(Irating_rating rating_rating){
this.rating_ratingOdooClient.update(rating_rating) ;
        }
        
        public void create(Irating_rating rating_rating){
this.rating_ratingOdooClient.create(rating_rating) ;
        }
        
        public void remove(Irating_rating rating_rating){
this.rating_ratingOdooClient.remove(rating_rating) ;
        }
        
        public void updateBatch(List<Irating_rating> rating_ratings){
            
        }
        
        public void removeBatch(List<Irating_rating> rating_ratings){
            
        }
        
        public Page<Irating_rating> search(SearchContext context){
            return this.rating_ratingOdooClient.search(context) ;
        }
        
        public void createBatch(List<Irating_rating> rating_ratings){
            
        }
        
        public Page<Irating_rating> select(SearchContext context){
            return null ;
        }
        

}

