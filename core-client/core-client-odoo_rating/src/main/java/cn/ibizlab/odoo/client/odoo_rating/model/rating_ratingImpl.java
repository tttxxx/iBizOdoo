package cn.ibizlab.odoo.client.odoo_rating.model;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.math.BigInteger;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.core.client.model.Irating_rating;
import cn.ibizlab.odoo.util.helper.OdooClientHelper;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang.ArrayUtils;
import org.springframework.util.StringUtils;

/**
 * 接口实体[rating_rating] 对象
 */
public class rating_ratingImpl implements Irating_rating,Serializable{

    /**
     * 安全令牌
     */
    public String access_token;

    @JsonIgnore
    public boolean access_tokenDirtyFlag;
    
    /**
     * 已填写的评级
     */
    public String consumed;

    @JsonIgnore
    public boolean consumedDirtyFlag;
    
    /**
     * 创建时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp create_date;

    @JsonIgnore
    public boolean create_dateDirtyFlag;
    
    /**
     * 创建人
     */
    public Integer create_uid;

    @JsonIgnore
    public boolean create_uidDirtyFlag;
    
    /**
     * 创建人
     */
    public String create_uid_text;

    @JsonIgnore
    public boolean create_uid_textDirtyFlag;
    
    /**
     * 显示名称
     */
    public String display_name;

    @JsonIgnore
    public boolean display_nameDirtyFlag;
    
    /**
     * 备注
     */
    public String feedback;

    @JsonIgnore
    public boolean feedbackDirtyFlag;
    
    /**
     * ID
     */
    public Integer id;

    @JsonIgnore
    public boolean idDirtyFlag;
    
    /**
     * 链接信息
     */
    public Integer message_id;

    @JsonIgnore
    public boolean message_idDirtyFlag;
    
    /**
     * 父级文档
     */
    public Integer parent_res_id;

    @JsonIgnore
    public boolean parent_res_idDirtyFlag;
    
    /**
     * 父级文档模型
     */
    public String parent_res_model;

    @JsonIgnore
    public boolean parent_res_modelDirtyFlag;
    
    /**
     * 父级相关文档模型
     */
    public Integer parent_res_model_id;

    @JsonIgnore
    public boolean parent_res_model_idDirtyFlag;
    
    /**
     * 父级文档名称
     */
    public String parent_res_name;

    @JsonIgnore
    public boolean parent_res_nameDirtyFlag;
    
    /**
     * 客户
     */
    public Integer partner_id;

    @JsonIgnore
    public boolean partner_idDirtyFlag;
    
    /**
     * 客户
     */
    public String partner_id_text;

    @JsonIgnore
    public boolean partner_id_textDirtyFlag;
    
    /**
     * 评级人员
     */
    public Integer rated_partner_id;

    @JsonIgnore
    public boolean rated_partner_idDirtyFlag;
    
    /**
     * 评级人员
     */
    public String rated_partner_id_text;

    @JsonIgnore
    public boolean rated_partner_id_textDirtyFlag;
    
    /**
     * 评级数值
     */
    public Double rating;

    @JsonIgnore
    public boolean ratingDirtyFlag;
    
    /**
     * 图像
     */
    public byte[] rating_image;

    @JsonIgnore
    public boolean rating_imageDirtyFlag;
    
    /**
     * 评级
     */
    public String rating_text;

    @JsonIgnore
    public boolean rating_textDirtyFlag;
    
    /**
     * 文档
     */
    public Integer res_id;

    @JsonIgnore
    public boolean res_idDirtyFlag;
    
    /**
     * 文档模型
     */
    public String res_model;

    @JsonIgnore
    public boolean res_modelDirtyFlag;
    
    /**
     * 相关的文档模型
     */
    public Integer res_model_id;

    @JsonIgnore
    public boolean res_model_idDirtyFlag;
    
    /**
     * 资源名称
     */
    public String res_name;

    @JsonIgnore
    public boolean res_nameDirtyFlag;
    
    /**
     * 已发布
     */
    public String website_published;

    @JsonIgnore
    public boolean website_publishedDirtyFlag;
    
    /**
     * 最后更新时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp write_date;

    @JsonIgnore
    public boolean write_dateDirtyFlag;
    
    /**
     * 最后更新人
     */
    public Integer write_uid;

    @JsonIgnore
    public boolean write_uidDirtyFlag;
    
    /**
     * 最后更新人
     */
    public String write_uid_text;

    @JsonIgnore
    public boolean write_uid_textDirtyFlag;
    
    /**
     * 最后修改日
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp __last_update;

    @JsonIgnore
    public boolean __last_updateDirtyFlag;
    
    /**
     * 获取 [安全令牌]
     */
    @JsonProperty("access_token")
    public String getAccess_token(){
        return this.access_token ;
    }

    /**
     * 设置 [安全令牌]
     */
    @JsonProperty("access_token")
    public void setAccess_token(String  access_token){
        this.access_token = access_token ;
        this.access_tokenDirtyFlag = true ;
    }

     /**
     * 获取 [安全令牌]脏标记
     */
    @JsonIgnore
    public boolean getAccess_tokenDirtyFlag(){
        return this.access_tokenDirtyFlag ;
    }   

    /**
     * 获取 [已填写的评级]
     */
    @JsonProperty("consumed")
    public String getConsumed(){
        return this.consumed ;
    }

    /**
     * 设置 [已填写的评级]
     */
    @JsonProperty("consumed")
    public void setConsumed(String  consumed){
        this.consumed = consumed ;
        this.consumedDirtyFlag = true ;
    }

     /**
     * 获取 [已填写的评级]脏标记
     */
    @JsonIgnore
    public boolean getConsumedDirtyFlag(){
        return this.consumedDirtyFlag ;
    }   

    /**
     * 获取 [创建时间]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return this.create_date ;
    }

    /**
     * 设置 [创建时间]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

     /**
     * 获取 [创建时间]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return this.create_dateDirtyFlag ;
    }   

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return this.create_uid ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

     /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return this.create_uidDirtyFlag ;
    }   

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return this.create_uid_text ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

     /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return this.create_uid_textDirtyFlag ;
    }   

    /**
     * 获取 [显示名称]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return this.display_name ;
    }

    /**
     * 设置 [显示名称]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

     /**
     * 获取 [显示名称]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return this.display_nameDirtyFlag ;
    }   

    /**
     * 获取 [备注]
     */
    @JsonProperty("feedback")
    public String getFeedback(){
        return this.feedback ;
    }

    /**
     * 设置 [备注]
     */
    @JsonProperty("feedback")
    public void setFeedback(String  feedback){
        this.feedback = feedback ;
        this.feedbackDirtyFlag = true ;
    }

     /**
     * 获取 [备注]脏标记
     */
    @JsonIgnore
    public boolean getFeedbackDirtyFlag(){
        return this.feedbackDirtyFlag ;
    }   

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return this.id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

     /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return this.idDirtyFlag ;
    }   

    /**
     * 获取 [链接信息]
     */
    @JsonProperty("message_id")
    public Integer getMessage_id(){
        return this.message_id ;
    }

    /**
     * 设置 [链接信息]
     */
    @JsonProperty("message_id")
    public void setMessage_id(Integer  message_id){
        this.message_id = message_id ;
        this.message_idDirtyFlag = true ;
    }

     /**
     * 获取 [链接信息]脏标记
     */
    @JsonIgnore
    public boolean getMessage_idDirtyFlag(){
        return this.message_idDirtyFlag ;
    }   

    /**
     * 获取 [父级文档]
     */
    @JsonProperty("parent_res_id")
    public Integer getParent_res_id(){
        return this.parent_res_id ;
    }

    /**
     * 设置 [父级文档]
     */
    @JsonProperty("parent_res_id")
    public void setParent_res_id(Integer  parent_res_id){
        this.parent_res_id = parent_res_id ;
        this.parent_res_idDirtyFlag = true ;
    }

     /**
     * 获取 [父级文档]脏标记
     */
    @JsonIgnore
    public boolean getParent_res_idDirtyFlag(){
        return this.parent_res_idDirtyFlag ;
    }   

    /**
     * 获取 [父级文档模型]
     */
    @JsonProperty("parent_res_model")
    public String getParent_res_model(){
        return this.parent_res_model ;
    }

    /**
     * 设置 [父级文档模型]
     */
    @JsonProperty("parent_res_model")
    public void setParent_res_model(String  parent_res_model){
        this.parent_res_model = parent_res_model ;
        this.parent_res_modelDirtyFlag = true ;
    }

     /**
     * 获取 [父级文档模型]脏标记
     */
    @JsonIgnore
    public boolean getParent_res_modelDirtyFlag(){
        return this.parent_res_modelDirtyFlag ;
    }   

    /**
     * 获取 [父级相关文档模型]
     */
    @JsonProperty("parent_res_model_id")
    public Integer getParent_res_model_id(){
        return this.parent_res_model_id ;
    }

    /**
     * 设置 [父级相关文档模型]
     */
    @JsonProperty("parent_res_model_id")
    public void setParent_res_model_id(Integer  parent_res_model_id){
        this.parent_res_model_id = parent_res_model_id ;
        this.parent_res_model_idDirtyFlag = true ;
    }

     /**
     * 获取 [父级相关文档模型]脏标记
     */
    @JsonIgnore
    public boolean getParent_res_model_idDirtyFlag(){
        return this.parent_res_model_idDirtyFlag ;
    }   

    /**
     * 获取 [父级文档名称]
     */
    @JsonProperty("parent_res_name")
    public String getParent_res_name(){
        return this.parent_res_name ;
    }

    /**
     * 设置 [父级文档名称]
     */
    @JsonProperty("parent_res_name")
    public void setParent_res_name(String  parent_res_name){
        this.parent_res_name = parent_res_name ;
        this.parent_res_nameDirtyFlag = true ;
    }

     /**
     * 获取 [父级文档名称]脏标记
     */
    @JsonIgnore
    public boolean getParent_res_nameDirtyFlag(){
        return this.parent_res_nameDirtyFlag ;
    }   

    /**
     * 获取 [客户]
     */
    @JsonProperty("partner_id")
    public Integer getPartner_id(){
        return this.partner_id ;
    }

    /**
     * 设置 [客户]
     */
    @JsonProperty("partner_id")
    public void setPartner_id(Integer  partner_id){
        this.partner_id = partner_id ;
        this.partner_idDirtyFlag = true ;
    }

     /**
     * 获取 [客户]脏标记
     */
    @JsonIgnore
    public boolean getPartner_idDirtyFlag(){
        return this.partner_idDirtyFlag ;
    }   

    /**
     * 获取 [客户]
     */
    @JsonProperty("partner_id_text")
    public String getPartner_id_text(){
        return this.partner_id_text ;
    }

    /**
     * 设置 [客户]
     */
    @JsonProperty("partner_id_text")
    public void setPartner_id_text(String  partner_id_text){
        this.partner_id_text = partner_id_text ;
        this.partner_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [客户]脏标记
     */
    @JsonIgnore
    public boolean getPartner_id_textDirtyFlag(){
        return this.partner_id_textDirtyFlag ;
    }   

    /**
     * 获取 [评级人员]
     */
    @JsonProperty("rated_partner_id")
    public Integer getRated_partner_id(){
        return this.rated_partner_id ;
    }

    /**
     * 设置 [评级人员]
     */
    @JsonProperty("rated_partner_id")
    public void setRated_partner_id(Integer  rated_partner_id){
        this.rated_partner_id = rated_partner_id ;
        this.rated_partner_idDirtyFlag = true ;
    }

     /**
     * 获取 [评级人员]脏标记
     */
    @JsonIgnore
    public boolean getRated_partner_idDirtyFlag(){
        return this.rated_partner_idDirtyFlag ;
    }   

    /**
     * 获取 [评级人员]
     */
    @JsonProperty("rated_partner_id_text")
    public String getRated_partner_id_text(){
        return this.rated_partner_id_text ;
    }

    /**
     * 设置 [评级人员]
     */
    @JsonProperty("rated_partner_id_text")
    public void setRated_partner_id_text(String  rated_partner_id_text){
        this.rated_partner_id_text = rated_partner_id_text ;
        this.rated_partner_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [评级人员]脏标记
     */
    @JsonIgnore
    public boolean getRated_partner_id_textDirtyFlag(){
        return this.rated_partner_id_textDirtyFlag ;
    }   

    /**
     * 获取 [评级数值]
     */
    @JsonProperty("rating")
    public Double getRating(){
        return this.rating ;
    }

    /**
     * 设置 [评级数值]
     */
    @JsonProperty("rating")
    public void setRating(Double  rating){
        this.rating = rating ;
        this.ratingDirtyFlag = true ;
    }

     /**
     * 获取 [评级数值]脏标记
     */
    @JsonIgnore
    public boolean getRatingDirtyFlag(){
        return this.ratingDirtyFlag ;
    }   

    /**
     * 获取 [图像]
     */
    @JsonProperty("rating_image")
    public byte[] getRating_image(){
        return this.rating_image ;
    }

    /**
     * 设置 [图像]
     */
    @JsonProperty("rating_image")
    public void setRating_image(byte[]  rating_image){
        this.rating_image = rating_image ;
        this.rating_imageDirtyFlag = true ;
    }

     /**
     * 获取 [图像]脏标记
     */
    @JsonIgnore
    public boolean getRating_imageDirtyFlag(){
        return this.rating_imageDirtyFlag ;
    }   

    /**
     * 获取 [评级]
     */
    @JsonProperty("rating_text")
    public String getRating_text(){
        return this.rating_text ;
    }

    /**
     * 设置 [评级]
     */
    @JsonProperty("rating_text")
    public void setRating_text(String  rating_text){
        this.rating_text = rating_text ;
        this.rating_textDirtyFlag = true ;
    }

     /**
     * 获取 [评级]脏标记
     */
    @JsonIgnore
    public boolean getRating_textDirtyFlag(){
        return this.rating_textDirtyFlag ;
    }   

    /**
     * 获取 [文档]
     */
    @JsonProperty("res_id")
    public Integer getRes_id(){
        return this.res_id ;
    }

    /**
     * 设置 [文档]
     */
    @JsonProperty("res_id")
    public void setRes_id(Integer  res_id){
        this.res_id = res_id ;
        this.res_idDirtyFlag = true ;
    }

     /**
     * 获取 [文档]脏标记
     */
    @JsonIgnore
    public boolean getRes_idDirtyFlag(){
        return this.res_idDirtyFlag ;
    }   

    /**
     * 获取 [文档模型]
     */
    @JsonProperty("res_model")
    public String getRes_model(){
        return this.res_model ;
    }

    /**
     * 设置 [文档模型]
     */
    @JsonProperty("res_model")
    public void setRes_model(String  res_model){
        this.res_model = res_model ;
        this.res_modelDirtyFlag = true ;
    }

     /**
     * 获取 [文档模型]脏标记
     */
    @JsonIgnore
    public boolean getRes_modelDirtyFlag(){
        return this.res_modelDirtyFlag ;
    }   

    /**
     * 获取 [相关的文档模型]
     */
    @JsonProperty("res_model_id")
    public Integer getRes_model_id(){
        return this.res_model_id ;
    }

    /**
     * 设置 [相关的文档模型]
     */
    @JsonProperty("res_model_id")
    public void setRes_model_id(Integer  res_model_id){
        this.res_model_id = res_model_id ;
        this.res_model_idDirtyFlag = true ;
    }

     /**
     * 获取 [相关的文档模型]脏标记
     */
    @JsonIgnore
    public boolean getRes_model_idDirtyFlag(){
        return this.res_model_idDirtyFlag ;
    }   

    /**
     * 获取 [资源名称]
     */
    @JsonProperty("res_name")
    public String getRes_name(){
        return this.res_name ;
    }

    /**
     * 设置 [资源名称]
     */
    @JsonProperty("res_name")
    public void setRes_name(String  res_name){
        this.res_name = res_name ;
        this.res_nameDirtyFlag = true ;
    }

     /**
     * 获取 [资源名称]脏标记
     */
    @JsonIgnore
    public boolean getRes_nameDirtyFlag(){
        return this.res_nameDirtyFlag ;
    }   

    /**
     * 获取 [已发布]
     */
    @JsonProperty("website_published")
    public String getWebsite_published(){
        return this.website_published ;
    }

    /**
     * 设置 [已发布]
     */
    @JsonProperty("website_published")
    public void setWebsite_published(String  website_published){
        this.website_published = website_published ;
        this.website_publishedDirtyFlag = true ;
    }

     /**
     * 获取 [已发布]脏标记
     */
    @JsonIgnore
    public boolean getWebsite_publishedDirtyFlag(){
        return this.website_publishedDirtyFlag ;
    }   

    /**
     * 获取 [最后更新时间]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return this.write_date ;
    }

    /**
     * 设置 [最后更新时间]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新时间]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return this.write_dateDirtyFlag ;
    }   

    /**
     * 获取 [最后更新人]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return this.write_uid ;
    }

    /**
     * 设置 [最后更新人]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新人]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return this.write_uidDirtyFlag ;
    }   

    /**
     * 获取 [最后更新人]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return this.write_uid_text ;
    }

    /**
     * 设置 [最后更新人]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新人]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return this.write_uid_textDirtyFlag ;
    }   

    /**
     * 获取 [最后修改日]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return this.__last_update ;
    }

    /**
     * 设置 [最后修改日]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

     /**
     * 获取 [最后修改日]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return this.__last_updateDirtyFlag ;
    }   



    public void fromMap(Map<String, Object> map) throws Exception {
		if(!(map.get("access_token") instanceof Boolean)&& map.get("access_token")!=null){
			this.setAccess_token((String)map.get("access_token"));
		}
		if(map.get("consumed") instanceof Boolean){
			this.setConsumed(((Boolean)map.get("consumed"))? "true" : "false");
		}
		if(!(map.get("create_date") instanceof Boolean)&& map.get("create_date")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse((String)map.get("create_date"));
   			this.setCreate_date(new Timestamp(parse.getTime()));
		}
		if(!(map.get("create_uid") instanceof Boolean)&& map.get("create_uid")!=null){
			Object[] objs = (Object[])map.get("create_uid");
			if(objs.length > 0){
				this.setCreate_uid((Integer)objs[0]);
			}
		}
		if(!(map.get("create_uid") instanceof Boolean)&& map.get("create_uid")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("create_uid");
			if(objs.length > 1){
				this.setCreate_uid_text((String)objs[1]);
			}
		}
		if(!(map.get("display_name") instanceof Boolean)&& map.get("display_name")!=null){
			this.setDisplay_name((String)map.get("display_name"));
		}
		if(!(map.get("feedback") instanceof Boolean)&& map.get("feedback")!=null){
			this.setFeedback((String)map.get("feedback"));
		}
		if(!(map.get("id") instanceof Boolean)&& map.get("id")!=null){
			this.setId((Integer)map.get("id"));
		}
		if(!(map.get("message_id") instanceof Boolean)&& map.get("message_id")!=null){
			Object[] objs = (Object[])map.get("message_id");
			if(objs.length > 0){
				this.setMessage_id((Integer)objs[0]);
			}
		}
		if(!(map.get("parent_res_id") instanceof Boolean)&& map.get("parent_res_id")!=null){
			this.setParent_res_id((Integer)map.get("parent_res_id"));
		}
		if(!(map.get("parent_res_model") instanceof Boolean)&& map.get("parent_res_model")!=null){
			this.setParent_res_model((String)map.get("parent_res_model"));
		}
		if(!(map.get("parent_res_model_id") instanceof Boolean)&& map.get("parent_res_model_id")!=null){
			Object[] objs = (Object[])map.get("parent_res_model_id");
			if(objs.length > 0){
				this.setParent_res_model_id((Integer)objs[0]);
			}
		}
		if(!(map.get("parent_res_name") instanceof Boolean)&& map.get("parent_res_name")!=null){
			this.setParent_res_name((String)map.get("parent_res_name"));
		}
		if(!(map.get("partner_id") instanceof Boolean)&& map.get("partner_id")!=null){
			Object[] objs = (Object[])map.get("partner_id");
			if(objs.length > 0){
				this.setPartner_id((Integer)objs[0]);
			}
		}
		if(!(map.get("partner_id") instanceof Boolean)&& map.get("partner_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("partner_id");
			if(objs.length > 1){
				this.setPartner_id_text((String)objs[1]);
			}
		}
		if(!(map.get("rated_partner_id") instanceof Boolean)&& map.get("rated_partner_id")!=null){
			Object[] objs = (Object[])map.get("rated_partner_id");
			if(objs.length > 0){
				this.setRated_partner_id((Integer)objs[0]);
			}
		}
		if(!(map.get("rated_partner_id") instanceof Boolean)&& map.get("rated_partner_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("rated_partner_id");
			if(objs.length > 1){
				this.setRated_partner_id_text((String)objs[1]);
			}
		}
		if(!(map.get("rating") instanceof Boolean)&& map.get("rating")!=null){
			this.setRating((Double)map.get("rating"));
		}
		if(!(map.get("rating_image") instanceof Boolean)&& map.get("rating_image")!=null){
			//暂时忽略
			//this.setRating_image(((String)map.get("rating_image")).getBytes("UTF-8"));
		}
		if(!(map.get("rating_text") instanceof Boolean)&& map.get("rating_text")!=null){
			this.setRating_text((String)map.get("rating_text"));
		}
		if(!(map.get("res_id") instanceof Boolean)&& map.get("res_id")!=null){
			this.setRes_id((Integer)map.get("res_id"));
		}
		if(!(map.get("res_model") instanceof Boolean)&& map.get("res_model")!=null){
			this.setRes_model((String)map.get("res_model"));
		}
		if(!(map.get("res_model_id") instanceof Boolean)&& map.get("res_model_id")!=null){
			Object[] objs = (Object[])map.get("res_model_id");
			if(objs.length > 0){
				this.setRes_model_id((Integer)objs[0]);
			}
		}
		if(!(map.get("res_name") instanceof Boolean)&& map.get("res_name")!=null){
			this.setRes_name((String)map.get("res_name"));
		}
		if(map.get("website_published") instanceof Boolean){
			this.setWebsite_published(((Boolean)map.get("website_published"))? "true" : "false");
		}
		if(!(map.get("write_date") instanceof Boolean)&& map.get("write_date")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse((String)map.get("write_date"));
   			this.setWrite_date(new Timestamp(parse.getTime()));
		}
		if(!(map.get("write_uid") instanceof Boolean)&& map.get("write_uid")!=null){
			Object[] objs = (Object[])map.get("write_uid");
			if(objs.length > 0){
				this.setWrite_uid((Integer)objs[0]);
			}
		}
		if(!(map.get("write_uid") instanceof Boolean)&& map.get("write_uid")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("write_uid");
			if(objs.length > 1){
				this.setWrite_uid_text((String)objs[1]);
			}
		}
		if(!(map.get("__last_update") instanceof Boolean)&& map.get("__last_update")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse((String)map.get("__last_update"));
   			this.set__last_update(new Timestamp(parse.getTime()));
		}
	}

	public Map<String, Object> toMap() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		if(this.getAccess_token()!=null&&this.getAccess_tokenDirtyFlag()){
			map.put("access_token",this.getAccess_token());
		}else if(this.getAccess_tokenDirtyFlag()){
			map.put("access_token",false);
		}
		if(this.getConsumed()!=null&&this.getConsumedDirtyFlag()){
			map.put("consumed",Boolean.parseBoolean(this.getConsumed()));		
		}		if(this.getCreate_date()!=null&&this.getCreate_dateDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String datetimeStr = sdf.format(this.getCreate_date());
			map.put("create_date",datetimeStr);
		}else if(this.getCreate_dateDirtyFlag()){
			map.put("create_date",false);
		}
		if(this.getCreate_uid()!=null&&this.getCreate_uidDirtyFlag()){
			map.put("create_uid",this.getCreate_uid());
		}else if(this.getCreate_uidDirtyFlag()){
			map.put("create_uid",false);
		}
		if(this.getCreate_uid_text()!=null&&this.getCreate_uid_textDirtyFlag()){
			//忽略文本外键create_uid_text
		}else if(this.getCreate_uid_textDirtyFlag()){
			map.put("create_uid",false);
		}
		if(this.getDisplay_name()!=null&&this.getDisplay_nameDirtyFlag()){
			map.put("display_name",this.getDisplay_name());
		}else if(this.getDisplay_nameDirtyFlag()){
			map.put("display_name",false);
		}
		if(this.getFeedback()!=null&&this.getFeedbackDirtyFlag()){
			map.put("feedback",this.getFeedback());
		}else if(this.getFeedbackDirtyFlag()){
			map.put("feedback",false);
		}
		if(this.getId()!=null&&this.getIdDirtyFlag()){
			map.put("id",this.getId());
		}else if(this.getIdDirtyFlag()){
			map.put("id",false);
		}
		if(this.getMessage_id()!=null&&this.getMessage_idDirtyFlag()){
			map.put("message_id",this.getMessage_id());
		}else if(this.getMessage_idDirtyFlag()){
			map.put("message_id",false);
		}
		if(this.getParent_res_id()!=null&&this.getParent_res_idDirtyFlag()){
			map.put("parent_res_id",this.getParent_res_id());
		}else if(this.getParent_res_idDirtyFlag()){
			map.put("parent_res_id",false);
		}
		if(this.getParent_res_model()!=null&&this.getParent_res_modelDirtyFlag()){
			map.put("parent_res_model",this.getParent_res_model());
		}else if(this.getParent_res_modelDirtyFlag()){
			map.put("parent_res_model",false);
		}
		if(this.getParent_res_model_id()!=null&&this.getParent_res_model_idDirtyFlag()){
			map.put("parent_res_model_id",this.getParent_res_model_id());
		}else if(this.getParent_res_model_idDirtyFlag()){
			map.put("parent_res_model_id",false);
		}
		if(this.getParent_res_name()!=null&&this.getParent_res_nameDirtyFlag()){
			map.put("parent_res_name",this.getParent_res_name());
		}else if(this.getParent_res_nameDirtyFlag()){
			map.put("parent_res_name",false);
		}
		if(this.getPartner_id()!=null&&this.getPartner_idDirtyFlag()){
			map.put("partner_id",this.getPartner_id());
		}else if(this.getPartner_idDirtyFlag()){
			map.put("partner_id",false);
		}
		if(this.getPartner_id_text()!=null&&this.getPartner_id_textDirtyFlag()){
			//忽略文本外键partner_id_text
		}else if(this.getPartner_id_textDirtyFlag()){
			map.put("partner_id",false);
		}
		if(this.getRated_partner_id()!=null&&this.getRated_partner_idDirtyFlag()){
			map.put("rated_partner_id",this.getRated_partner_id());
		}else if(this.getRated_partner_idDirtyFlag()){
			map.put("rated_partner_id",false);
		}
		if(this.getRated_partner_id_text()!=null&&this.getRated_partner_id_textDirtyFlag()){
			//忽略文本外键rated_partner_id_text
		}else if(this.getRated_partner_id_textDirtyFlag()){
			map.put("rated_partner_id",false);
		}
		if(this.getRating()!=null&&this.getRatingDirtyFlag()){
			map.put("rating",this.getRating());
		}else if(this.getRatingDirtyFlag()){
			map.put("rating",false);
		}
		if(this.getRating_image()!=null&&this.getRating_imageDirtyFlag()){
			//暂不支持binary类型rating_image
		}else if(this.getRating_imageDirtyFlag()){
			map.put("rating_image",false);
		}
		if(this.getRating_text()!=null&&this.getRating_textDirtyFlag()){
			map.put("rating_text",this.getRating_text());
		}else if(this.getRating_textDirtyFlag()){
			map.put("rating_text",false);
		}
		if(this.getRes_id()!=null&&this.getRes_idDirtyFlag()){
			map.put("res_id",this.getRes_id());
		}else if(this.getRes_idDirtyFlag()){
			map.put("res_id",false);
		}
		if(this.getRes_model()!=null&&this.getRes_modelDirtyFlag()){
			map.put("res_model",this.getRes_model());
		}else if(this.getRes_modelDirtyFlag()){
			map.put("res_model",false);
		}
		if(this.getRes_model_id()!=null&&this.getRes_model_idDirtyFlag()){
			map.put("res_model_id",this.getRes_model_id());
		}else if(this.getRes_model_idDirtyFlag()){
			map.put("res_model_id",false);
		}
		if(this.getRes_name()!=null&&this.getRes_nameDirtyFlag()){
			map.put("res_name",this.getRes_name());
		}else if(this.getRes_nameDirtyFlag()){
			map.put("res_name",false);
		}
		if(this.getWebsite_published()!=null&&this.getWebsite_publishedDirtyFlag()){
			map.put("website_published",Boolean.parseBoolean(this.getWebsite_published()));		
		}		if(this.getWrite_date()!=null&&this.getWrite_dateDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String datetimeStr = sdf.format(this.getWrite_date());
			map.put("write_date",datetimeStr);
		}else if(this.getWrite_dateDirtyFlag()){
			map.put("write_date",false);
		}
		if(this.getWrite_uid()!=null&&this.getWrite_uidDirtyFlag()){
			map.put("write_uid",this.getWrite_uid());
		}else if(this.getWrite_uidDirtyFlag()){
			map.put("write_uid",false);
		}
		if(this.getWrite_uid_text()!=null&&this.getWrite_uid_textDirtyFlag()){
			//忽略文本外键write_uid_text
		}else if(this.getWrite_uid_textDirtyFlag()){
			map.put("write_uid",false);
		}
		if(this.get__last_update()!=null&&this.get__last_updateDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String datetimeStr = sdf.format(this.get__last_update());
			map.put("__last_update",datetimeStr);
		}else if(this.get__last_updateDirtyFlag()){
			map.put("__last_update",false);
		}
		return map;
	}

}
