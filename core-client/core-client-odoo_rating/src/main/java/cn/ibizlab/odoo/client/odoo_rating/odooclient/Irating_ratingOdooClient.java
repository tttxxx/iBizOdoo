package cn.ibizlab.odoo.client.odoo_rating.odooclient;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.core.client.model.Irating_rating;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[rating_rating] 服务对象客户端接口
 */
public interface Irating_ratingOdooClient {
    
        public void get(Irating_rating rating_rating);

        public void update(Irating_rating rating_rating);

        public void create(Irating_rating rating_rating);

        public void remove(Irating_rating rating_rating);

        public void updateBatch(Irating_rating rating_rating);

        public void removeBatch(Irating_rating rating_rating);

        public Page<Irating_rating> search(SearchContext context);

        public void createBatch(Irating_rating rating_rating);

        public List<Irating_rating> select();


}