package cn.ibizlab.odoo.client.odoo_stock.odooclient;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.core.client.model.Istock_scheduler_compute;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[stock_scheduler_compute] 服务对象客户端接口
 */
public interface Istock_scheduler_computeOdooClient {
    
        public void updateBatch(Istock_scheduler_compute stock_scheduler_compute);

        public void removeBatch(Istock_scheduler_compute stock_scheduler_compute);

        public void createBatch(Istock_scheduler_compute stock_scheduler_compute);

        public void create(Istock_scheduler_compute stock_scheduler_compute);

        public void get(Istock_scheduler_compute stock_scheduler_compute);

        public Page<Istock_scheduler_compute> search(SearchContext context);

        public void remove(Istock_scheduler_compute stock_scheduler_compute);

        public void update(Istock_scheduler_compute stock_scheduler_compute);

        public List<Istock_scheduler_compute> select();


}