package cn.ibizlab.odoo.client.odoo_stock.odooclient;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.core.client.model.Istock_overprocessed_transfer;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[stock_overprocessed_transfer] 服务对象客户端接口
 */
public interface Istock_overprocessed_transferOdooClient {
    
        public void create(Istock_overprocessed_transfer stock_overprocessed_transfer);

        public void get(Istock_overprocessed_transfer stock_overprocessed_transfer);

        public void removeBatch(Istock_overprocessed_transfer stock_overprocessed_transfer);

        public Page<Istock_overprocessed_transfer> search(SearchContext context);

        public void update(Istock_overprocessed_transfer stock_overprocessed_transfer);

        public void updateBatch(Istock_overprocessed_transfer stock_overprocessed_transfer);

        public void remove(Istock_overprocessed_transfer stock_overprocessed_transfer);

        public void createBatch(Istock_overprocessed_transfer stock_overprocessed_transfer);

        public List<Istock_overprocessed_transfer> select();


}