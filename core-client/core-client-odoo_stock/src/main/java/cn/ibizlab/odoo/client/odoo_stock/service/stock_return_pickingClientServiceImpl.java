package cn.ibizlab.odoo.client.odoo_stock.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Istock_return_picking;
import cn.ibizlab.odoo.core.client.service.Istock_return_pickingClientService;
import cn.ibizlab.odoo.client.odoo_stock.model.stock_return_pickingImpl;
import cn.ibizlab.odoo.client.odoo_stock.odooclient.Istock_return_pickingOdooClient;
import cn.ibizlab.odoo.client.odoo_stock.odooclient.impl.stock_return_pickingOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[stock_return_picking] 服务对象接口
 */
@Service
public class stock_return_pickingClientServiceImpl implements Istock_return_pickingClientService {
    @Autowired
    private  Istock_return_pickingOdooClient  stock_return_pickingOdooClient;

    public Istock_return_picking createModel() {		
		return new stock_return_pickingImpl();
	}


        public void updateBatch(List<Istock_return_picking> stock_return_pickings){
            
        }
        
        public void remove(Istock_return_picking stock_return_picking){
this.stock_return_pickingOdooClient.remove(stock_return_picking) ;
        }
        
        public Page<Istock_return_picking> search(SearchContext context){
            return this.stock_return_pickingOdooClient.search(context) ;
        }
        
        public void removeBatch(List<Istock_return_picking> stock_return_pickings){
            
        }
        
        public void createBatch(List<Istock_return_picking> stock_return_pickings){
            
        }
        
        public void create(Istock_return_picking stock_return_picking){
this.stock_return_pickingOdooClient.create(stock_return_picking) ;
        }
        
        public void update(Istock_return_picking stock_return_picking){
this.stock_return_pickingOdooClient.update(stock_return_picking) ;
        }
        
        public void get(Istock_return_picking stock_return_picking){
            this.stock_return_pickingOdooClient.get(stock_return_picking) ;
        }
        
        public Page<Istock_return_picking> select(SearchContext context){
            return null ;
        }
        

}

