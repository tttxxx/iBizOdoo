package cn.ibizlab.odoo.client.odoo_stock.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Istock_track_confirmation;
import cn.ibizlab.odoo.core.client.service.Istock_track_confirmationClientService;
import cn.ibizlab.odoo.client.odoo_stock.model.stock_track_confirmationImpl;
import cn.ibizlab.odoo.client.odoo_stock.odooclient.Istock_track_confirmationOdooClient;
import cn.ibizlab.odoo.client.odoo_stock.odooclient.impl.stock_track_confirmationOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[stock_track_confirmation] 服务对象接口
 */
@Service
public class stock_track_confirmationClientServiceImpl implements Istock_track_confirmationClientService {
    @Autowired
    private  Istock_track_confirmationOdooClient  stock_track_confirmationOdooClient;

    public Istock_track_confirmation createModel() {		
		return new stock_track_confirmationImpl();
	}


        public void updateBatch(List<Istock_track_confirmation> stock_track_confirmations){
            
        }
        
        public void createBatch(List<Istock_track_confirmation> stock_track_confirmations){
            
        }
        
        public void removeBatch(List<Istock_track_confirmation> stock_track_confirmations){
            
        }
        
        public void create(Istock_track_confirmation stock_track_confirmation){
this.stock_track_confirmationOdooClient.create(stock_track_confirmation) ;
        }
        
        public void update(Istock_track_confirmation stock_track_confirmation){
this.stock_track_confirmationOdooClient.update(stock_track_confirmation) ;
        }
        
        public void get(Istock_track_confirmation stock_track_confirmation){
            this.stock_track_confirmationOdooClient.get(stock_track_confirmation) ;
        }
        
        public Page<Istock_track_confirmation> search(SearchContext context){
            return this.stock_track_confirmationOdooClient.search(context) ;
        }
        
        public void remove(Istock_track_confirmation stock_track_confirmation){
this.stock_track_confirmationOdooClient.remove(stock_track_confirmation) ;
        }
        
        public Page<Istock_track_confirmation> select(SearchContext context){
            return null ;
        }
        

}

