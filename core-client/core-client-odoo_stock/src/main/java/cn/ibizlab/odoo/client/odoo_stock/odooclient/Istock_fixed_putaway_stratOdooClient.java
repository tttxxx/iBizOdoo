package cn.ibizlab.odoo.client.odoo_stock.odooclient;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.core.client.model.Istock_fixed_putaway_strat;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[stock_fixed_putaway_strat] 服务对象客户端接口
 */
public interface Istock_fixed_putaway_stratOdooClient {
    
        public void updateBatch(Istock_fixed_putaway_strat stock_fixed_putaway_strat);

        public void removeBatch(Istock_fixed_putaway_strat stock_fixed_putaway_strat);

        public void update(Istock_fixed_putaway_strat stock_fixed_putaway_strat);

        public void create(Istock_fixed_putaway_strat stock_fixed_putaway_strat);

        public void get(Istock_fixed_putaway_strat stock_fixed_putaway_strat);

        public void createBatch(Istock_fixed_putaway_strat stock_fixed_putaway_strat);

        public Page<Istock_fixed_putaway_strat> search(SearchContext context);

        public void remove(Istock_fixed_putaway_strat stock_fixed_putaway_strat);

        public List<Istock_fixed_putaway_strat> select();


}