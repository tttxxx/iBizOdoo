package cn.ibizlab.odoo.client.odoo_stock.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Istock_package_level;
import cn.ibizlab.odoo.core.client.service.Istock_package_levelClientService;
import cn.ibizlab.odoo.client.odoo_stock.model.stock_package_levelImpl;
import cn.ibizlab.odoo.client.odoo_stock.odooclient.Istock_package_levelOdooClient;
import cn.ibizlab.odoo.client.odoo_stock.odooclient.impl.stock_package_levelOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[stock_package_level] 服务对象接口
 */
@Service
public class stock_package_levelClientServiceImpl implements Istock_package_levelClientService {
    @Autowired
    private  Istock_package_levelOdooClient  stock_package_levelOdooClient;

    public Istock_package_level createModel() {		
		return new stock_package_levelImpl();
	}


        public void createBatch(List<Istock_package_level> stock_package_levels){
            
        }
        
        public void create(Istock_package_level stock_package_level){
this.stock_package_levelOdooClient.create(stock_package_level) ;
        }
        
        public Page<Istock_package_level> search(SearchContext context){
            return this.stock_package_levelOdooClient.search(context) ;
        }
        
        public void update(Istock_package_level stock_package_level){
this.stock_package_levelOdooClient.update(stock_package_level) ;
        }
        
        public void updateBatch(List<Istock_package_level> stock_package_levels){
            
        }
        
        public void removeBatch(List<Istock_package_level> stock_package_levels){
            
        }
        
        public void remove(Istock_package_level stock_package_level){
this.stock_package_levelOdooClient.remove(stock_package_level) ;
        }
        
        public void get(Istock_package_level stock_package_level){
            this.stock_package_levelOdooClient.get(stock_package_level) ;
        }
        
        public Page<Istock_package_level> select(SearchContext context){
            return null ;
        }
        

}

