package cn.ibizlab.odoo.client.odoo_stock.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Istock_rules_report;
import cn.ibizlab.odoo.client.odoo_stock.model.stock_rules_reportImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[stock_rules_report] 服务对象接口
 */
public interface stock_rules_reportFeignClient {

    @RequestMapping(method = RequestMethod.POST, value = "/odoo_stock/stock_rules_reports/createbatch")
    public stock_rules_reportImpl createBatch(@RequestBody List<stock_rules_reportImpl> stock_rules_reports);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_stock/stock_rules_reports/{id}")
    public stock_rules_reportImpl update(@PathVariable("id") Integer id,@RequestBody stock_rules_reportImpl stock_rules_report);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_stock/stock_rules_reports/removebatch")
    public stock_rules_reportImpl removeBatch(@RequestBody List<stock_rules_reportImpl> stock_rules_reports);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_stock/stock_rules_reports/{id}")
    public stock_rules_reportImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_stock/stock_rules_reports/search")
    public Page<stock_rules_reportImpl> search(SearchContext context);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_stock/stock_rules_reports/updatebatch")
    public stock_rules_reportImpl updateBatch(@RequestBody List<stock_rules_reportImpl> stock_rules_reports);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_stock/stock_rules_reports")
    public stock_rules_reportImpl create(@RequestBody stock_rules_reportImpl stock_rules_report);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_stock/stock_rules_reports/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_stock/stock_rules_reports/select")
    public Page<stock_rules_reportImpl> select();



}
