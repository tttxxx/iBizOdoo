package cn.ibizlab.odoo.client.odoo_stock.odooclient;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.core.client.model.Istock_rule;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[stock_rule] 服务对象客户端接口
 */
public interface Istock_ruleOdooClient {
    
        public void update(Istock_rule stock_rule);

        public void remove(Istock_rule stock_rule);

        public void removeBatch(Istock_rule stock_rule);

        public void create(Istock_rule stock_rule);

        public Page<Istock_rule> search(SearchContext context);

        public void updateBatch(Istock_rule stock_rule);

        public void createBatch(Istock_rule stock_rule);

        public void get(Istock_rule stock_rule);

        public List<Istock_rule> select();


}