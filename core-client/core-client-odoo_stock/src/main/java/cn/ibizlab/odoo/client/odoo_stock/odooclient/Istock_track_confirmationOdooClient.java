package cn.ibizlab.odoo.client.odoo_stock.odooclient;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.core.client.model.Istock_track_confirmation;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[stock_track_confirmation] 服务对象客户端接口
 */
public interface Istock_track_confirmationOdooClient {
    
        public void updateBatch(Istock_track_confirmation stock_track_confirmation);

        public void createBatch(Istock_track_confirmation stock_track_confirmation);

        public void removeBatch(Istock_track_confirmation stock_track_confirmation);

        public void create(Istock_track_confirmation stock_track_confirmation);

        public void update(Istock_track_confirmation stock_track_confirmation);

        public void get(Istock_track_confirmation stock_track_confirmation);

        public Page<Istock_track_confirmation> search(SearchContext context);

        public void remove(Istock_track_confirmation stock_track_confirmation);

        public List<Istock_track_confirmation> select();


}