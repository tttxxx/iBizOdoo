package cn.ibizlab.odoo.client.odoo_stock.odooclient;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.core.client.model.Istock_rules_report;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[stock_rules_report] 服务对象客户端接口
 */
public interface Istock_rules_reportOdooClient {
    
        public void createBatch(Istock_rules_report stock_rules_report);

        public void update(Istock_rules_report stock_rules_report);

        public void removeBatch(Istock_rules_report stock_rules_report);

        public void get(Istock_rules_report stock_rules_report);

        public Page<Istock_rules_report> search(SearchContext context);

        public void updateBatch(Istock_rules_report stock_rules_report);

        public void create(Istock_rules_report stock_rules_report);

        public void remove(Istock_rules_report stock_rules_report);

        public List<Istock_rules_report> select();


}