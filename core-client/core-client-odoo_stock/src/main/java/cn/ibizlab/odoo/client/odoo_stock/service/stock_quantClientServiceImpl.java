package cn.ibizlab.odoo.client.odoo_stock.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Istock_quant;
import cn.ibizlab.odoo.core.client.service.Istock_quantClientService;
import cn.ibizlab.odoo.client.odoo_stock.model.stock_quantImpl;
import cn.ibizlab.odoo.client.odoo_stock.odooclient.Istock_quantOdooClient;
import cn.ibizlab.odoo.client.odoo_stock.odooclient.impl.stock_quantOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[stock_quant] 服务对象接口
 */
@Service
public class stock_quantClientServiceImpl implements Istock_quantClientService {
    @Autowired
    private  Istock_quantOdooClient  stock_quantOdooClient;

    public Istock_quant createModel() {		
		return new stock_quantImpl();
	}


        public void update(Istock_quant stock_quant){
this.stock_quantOdooClient.update(stock_quant) ;
        }
        
        public Page<Istock_quant> search(SearchContext context){
            return this.stock_quantOdooClient.search(context) ;
        }
        
        public void removeBatch(List<Istock_quant> stock_quants){
            
        }
        
        public void get(Istock_quant stock_quant){
            this.stock_quantOdooClient.get(stock_quant) ;
        }
        
        public void create(Istock_quant stock_quant){
this.stock_quantOdooClient.create(stock_quant) ;
        }
        
        public void createBatch(List<Istock_quant> stock_quants){
            
        }
        
        public void remove(Istock_quant stock_quant){
this.stock_quantOdooClient.remove(stock_quant) ;
        }
        
        public void updateBatch(List<Istock_quant> stock_quants){
            
        }
        
        public Page<Istock_quant> select(SearchContext context){
            return null ;
        }
        

}

