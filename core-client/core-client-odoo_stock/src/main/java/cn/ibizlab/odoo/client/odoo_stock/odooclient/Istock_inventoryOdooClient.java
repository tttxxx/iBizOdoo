package cn.ibizlab.odoo.client.odoo_stock.odooclient;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.core.client.model.Istock_inventory;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[stock_inventory] 服务对象客户端接口
 */
public interface Istock_inventoryOdooClient {
    
        public void update(Istock_inventory stock_inventory);

        public Page<Istock_inventory> search(SearchContext context);

        public void updateBatch(Istock_inventory stock_inventory);

        public void remove(Istock_inventory stock_inventory);

        public void create(Istock_inventory stock_inventory);

        public void removeBatch(Istock_inventory stock_inventory);

        public void createBatch(Istock_inventory stock_inventory);

        public void get(Istock_inventory stock_inventory);

        public List<Istock_inventory> select();


}