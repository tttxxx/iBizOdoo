package cn.ibizlab.odoo.client.odoo_stock.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Istock_fixed_putaway_strat;
import cn.ibizlab.odoo.core.client.service.Istock_fixed_putaway_stratClientService;
import cn.ibizlab.odoo.client.odoo_stock.model.stock_fixed_putaway_stratImpl;
import cn.ibizlab.odoo.client.odoo_stock.odooclient.Istock_fixed_putaway_stratOdooClient;
import cn.ibizlab.odoo.client.odoo_stock.odooclient.impl.stock_fixed_putaway_stratOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[stock_fixed_putaway_strat] 服务对象接口
 */
@Service
public class stock_fixed_putaway_stratClientServiceImpl implements Istock_fixed_putaway_stratClientService {
    @Autowired
    private  Istock_fixed_putaway_stratOdooClient  stock_fixed_putaway_stratOdooClient;

    public Istock_fixed_putaway_strat createModel() {		
		return new stock_fixed_putaway_stratImpl();
	}


        public void updateBatch(List<Istock_fixed_putaway_strat> stock_fixed_putaway_strats){
            
        }
        
        public void removeBatch(List<Istock_fixed_putaway_strat> stock_fixed_putaway_strats){
            
        }
        
        public void update(Istock_fixed_putaway_strat stock_fixed_putaway_strat){
this.stock_fixed_putaway_stratOdooClient.update(stock_fixed_putaway_strat) ;
        }
        
        public void create(Istock_fixed_putaway_strat stock_fixed_putaway_strat){
this.stock_fixed_putaway_stratOdooClient.create(stock_fixed_putaway_strat) ;
        }
        
        public void get(Istock_fixed_putaway_strat stock_fixed_putaway_strat){
            this.stock_fixed_putaway_stratOdooClient.get(stock_fixed_putaway_strat) ;
        }
        
        public void createBatch(List<Istock_fixed_putaway_strat> stock_fixed_putaway_strats){
            
        }
        
        public Page<Istock_fixed_putaway_strat> search(SearchContext context){
            return this.stock_fixed_putaway_stratOdooClient.search(context) ;
        }
        
        public void remove(Istock_fixed_putaway_strat stock_fixed_putaway_strat){
this.stock_fixed_putaway_stratOdooClient.remove(stock_fixed_putaway_strat) ;
        }
        
        public Page<Istock_fixed_putaway_strat> select(SearchContext context){
            return null ;
        }
        

}

