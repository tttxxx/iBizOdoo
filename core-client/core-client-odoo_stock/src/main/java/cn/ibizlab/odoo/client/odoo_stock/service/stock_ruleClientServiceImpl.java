package cn.ibizlab.odoo.client.odoo_stock.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Istock_rule;
import cn.ibizlab.odoo.core.client.service.Istock_ruleClientService;
import cn.ibizlab.odoo.client.odoo_stock.model.stock_ruleImpl;
import cn.ibizlab.odoo.client.odoo_stock.odooclient.Istock_ruleOdooClient;
import cn.ibizlab.odoo.client.odoo_stock.odooclient.impl.stock_ruleOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[stock_rule] 服务对象接口
 */
@Service
public class stock_ruleClientServiceImpl implements Istock_ruleClientService {
    @Autowired
    private  Istock_ruleOdooClient  stock_ruleOdooClient;

    public Istock_rule createModel() {		
		return new stock_ruleImpl();
	}


        public void update(Istock_rule stock_rule){
this.stock_ruleOdooClient.update(stock_rule) ;
        }
        
        public void remove(Istock_rule stock_rule){
this.stock_ruleOdooClient.remove(stock_rule) ;
        }
        
        public void removeBatch(List<Istock_rule> stock_rules){
            
        }
        
        public void create(Istock_rule stock_rule){
this.stock_ruleOdooClient.create(stock_rule) ;
        }
        
        public Page<Istock_rule> search(SearchContext context){
            return this.stock_ruleOdooClient.search(context) ;
        }
        
        public void updateBatch(List<Istock_rule> stock_rules){
            
        }
        
        public void createBatch(List<Istock_rule> stock_rules){
            
        }
        
        public void get(Istock_rule stock_rule){
            this.stock_ruleOdooClient.get(stock_rule) ;
        }
        
        public Page<Istock_rule> select(SearchContext context){
            return null ;
        }
        

}

