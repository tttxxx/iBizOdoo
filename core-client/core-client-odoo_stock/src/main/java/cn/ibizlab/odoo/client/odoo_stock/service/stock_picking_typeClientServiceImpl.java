package cn.ibizlab.odoo.client.odoo_stock.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Istock_picking_type;
import cn.ibizlab.odoo.core.client.service.Istock_picking_typeClientService;
import cn.ibizlab.odoo.client.odoo_stock.model.stock_picking_typeImpl;
import cn.ibizlab.odoo.client.odoo_stock.odooclient.Istock_picking_typeOdooClient;
import cn.ibizlab.odoo.client.odoo_stock.odooclient.impl.stock_picking_typeOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[stock_picking_type] 服务对象接口
 */
@Service
public class stock_picking_typeClientServiceImpl implements Istock_picking_typeClientService {
    @Autowired
    private  Istock_picking_typeOdooClient  stock_picking_typeOdooClient;

    public Istock_picking_type createModel() {		
		return new stock_picking_typeImpl();
	}


        public Page<Istock_picking_type> search(SearchContext context){
            return this.stock_picking_typeOdooClient.search(context) ;
        }
        
        public void createBatch(List<Istock_picking_type> stock_picking_types){
            
        }
        
        public void removeBatch(List<Istock_picking_type> stock_picking_types){
            
        }
        
        public void create(Istock_picking_type stock_picking_type){
this.stock_picking_typeOdooClient.create(stock_picking_type) ;
        }
        
        public void update(Istock_picking_type stock_picking_type){
this.stock_picking_typeOdooClient.update(stock_picking_type) ;
        }
        
        public void get(Istock_picking_type stock_picking_type){
            this.stock_picking_typeOdooClient.get(stock_picking_type) ;
        }
        
        public void updateBatch(List<Istock_picking_type> stock_picking_types){
            
        }
        
        public void remove(Istock_picking_type stock_picking_type){
this.stock_picking_typeOdooClient.remove(stock_picking_type) ;
        }
        
        public Page<Istock_picking_type> select(SearchContext context){
            return null ;
        }
        

}

