package cn.ibizlab.odoo.client.odoo_stock.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Istock_rules_report;
import cn.ibizlab.odoo.core.client.service.Istock_rules_reportClientService;
import cn.ibizlab.odoo.client.odoo_stock.model.stock_rules_reportImpl;
import cn.ibizlab.odoo.client.odoo_stock.odooclient.Istock_rules_reportOdooClient;
import cn.ibizlab.odoo.client.odoo_stock.odooclient.impl.stock_rules_reportOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[stock_rules_report] 服务对象接口
 */
@Service
public class stock_rules_reportClientServiceImpl implements Istock_rules_reportClientService {
    @Autowired
    private  Istock_rules_reportOdooClient  stock_rules_reportOdooClient;

    public Istock_rules_report createModel() {		
		return new stock_rules_reportImpl();
	}


        public void createBatch(List<Istock_rules_report> stock_rules_reports){
            
        }
        
        public void update(Istock_rules_report stock_rules_report){
this.stock_rules_reportOdooClient.update(stock_rules_report) ;
        }
        
        public void removeBatch(List<Istock_rules_report> stock_rules_reports){
            
        }
        
        public void get(Istock_rules_report stock_rules_report){
            this.stock_rules_reportOdooClient.get(stock_rules_report) ;
        }
        
        public Page<Istock_rules_report> search(SearchContext context){
            return this.stock_rules_reportOdooClient.search(context) ;
        }
        
        public void updateBatch(List<Istock_rules_report> stock_rules_reports){
            
        }
        
        public void create(Istock_rules_report stock_rules_report){
this.stock_rules_reportOdooClient.create(stock_rules_report) ;
        }
        
        public void remove(Istock_rules_report stock_rules_report){
this.stock_rules_reportOdooClient.remove(stock_rules_report) ;
        }
        
        public Page<Istock_rules_report> select(SearchContext context){
            return null ;
        }
        

}

