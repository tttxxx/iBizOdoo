package cn.ibizlab.odoo.client.odoo_stock.odooclient;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.core.client.model.Istock_immediate_transfer;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[stock_immediate_transfer] 服务对象客户端接口
 */
public interface Istock_immediate_transferOdooClient {
    
        public void createBatch(Istock_immediate_transfer stock_immediate_transfer);

        public void create(Istock_immediate_transfer stock_immediate_transfer);

        public void update(Istock_immediate_transfer stock_immediate_transfer);

        public void get(Istock_immediate_transfer stock_immediate_transfer);

        public void removeBatch(Istock_immediate_transfer stock_immediate_transfer);

        public Page<Istock_immediate_transfer> search(SearchContext context);

        public void remove(Istock_immediate_transfer stock_immediate_transfer);

        public void updateBatch(Istock_immediate_transfer stock_immediate_transfer);

        public List<Istock_immediate_transfer> select();


}