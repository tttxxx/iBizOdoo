package cn.ibizlab.odoo.client.odoo_stock.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Istock_location_route;
import cn.ibizlab.odoo.core.client.service.Istock_location_routeClientService;
import cn.ibizlab.odoo.client.odoo_stock.model.stock_location_routeImpl;
import cn.ibizlab.odoo.client.odoo_stock.odooclient.Istock_location_routeOdooClient;
import cn.ibizlab.odoo.client.odoo_stock.odooclient.impl.stock_location_routeOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[stock_location_route] 服务对象接口
 */
@Service
public class stock_location_routeClientServiceImpl implements Istock_location_routeClientService {
    @Autowired
    private  Istock_location_routeOdooClient  stock_location_routeOdooClient;

    public Istock_location_route createModel() {		
		return new stock_location_routeImpl();
	}


        public void create(Istock_location_route stock_location_route){
this.stock_location_routeOdooClient.create(stock_location_route) ;
        }
        
        public void createBatch(List<Istock_location_route> stock_location_routes){
            
        }
        
        public void removeBatch(List<Istock_location_route> stock_location_routes){
            
        }
        
        public void updateBatch(List<Istock_location_route> stock_location_routes){
            
        }
        
        public void get(Istock_location_route stock_location_route){
            this.stock_location_routeOdooClient.get(stock_location_route) ;
        }
        
        public Page<Istock_location_route> search(SearchContext context){
            return this.stock_location_routeOdooClient.search(context) ;
        }
        
        public void update(Istock_location_route stock_location_route){
this.stock_location_routeOdooClient.update(stock_location_route) ;
        }
        
        public void remove(Istock_location_route stock_location_route){
this.stock_location_routeOdooClient.remove(stock_location_route) ;
        }
        
        public Page<Istock_location_route> select(SearchContext context){
            return null ;
        }
        

}

