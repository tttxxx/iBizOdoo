package cn.ibizlab.odoo.client.odoo_stock.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Istock_warn_insufficient_qty;
import cn.ibizlab.odoo.core.client.service.Istock_warn_insufficient_qtyClientService;
import cn.ibizlab.odoo.client.odoo_stock.model.stock_warn_insufficient_qtyImpl;
import cn.ibizlab.odoo.client.odoo_stock.odooclient.Istock_warn_insufficient_qtyOdooClient;
import cn.ibizlab.odoo.client.odoo_stock.odooclient.impl.stock_warn_insufficient_qtyOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[stock_warn_insufficient_qty] 服务对象接口
 */
@Service
public class stock_warn_insufficient_qtyClientServiceImpl implements Istock_warn_insufficient_qtyClientService {
    @Autowired
    private  Istock_warn_insufficient_qtyOdooClient  stock_warn_insufficient_qtyOdooClient;

    public Istock_warn_insufficient_qty createModel() {		
		return new stock_warn_insufficient_qtyImpl();
	}


        public void removeBatch(List<Istock_warn_insufficient_qty> stock_warn_insufficient_qties){
            
        }
        
        public void update(Istock_warn_insufficient_qty stock_warn_insufficient_qty){
this.stock_warn_insufficient_qtyOdooClient.update(stock_warn_insufficient_qty) ;
        }
        
        public void updateBatch(List<Istock_warn_insufficient_qty> stock_warn_insufficient_qties){
            
        }
        
        public void remove(Istock_warn_insufficient_qty stock_warn_insufficient_qty){
this.stock_warn_insufficient_qtyOdooClient.remove(stock_warn_insufficient_qty) ;
        }
        
        public void createBatch(List<Istock_warn_insufficient_qty> stock_warn_insufficient_qties){
            
        }
        
        public void get(Istock_warn_insufficient_qty stock_warn_insufficient_qty){
            this.stock_warn_insufficient_qtyOdooClient.get(stock_warn_insufficient_qty) ;
        }
        
        public Page<Istock_warn_insufficient_qty> search(SearchContext context){
            return this.stock_warn_insufficient_qtyOdooClient.search(context) ;
        }
        
        public void create(Istock_warn_insufficient_qty stock_warn_insufficient_qty){
this.stock_warn_insufficient_qtyOdooClient.create(stock_warn_insufficient_qty) ;
        }
        
        public Page<Istock_warn_insufficient_qty> select(SearchContext context){
            return null ;
        }
        

}

