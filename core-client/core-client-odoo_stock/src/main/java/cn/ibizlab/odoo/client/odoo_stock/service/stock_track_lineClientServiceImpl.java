package cn.ibizlab.odoo.client.odoo_stock.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Istock_track_line;
import cn.ibizlab.odoo.core.client.service.Istock_track_lineClientService;
import cn.ibizlab.odoo.client.odoo_stock.model.stock_track_lineImpl;
import cn.ibizlab.odoo.client.odoo_stock.odooclient.Istock_track_lineOdooClient;
import cn.ibizlab.odoo.client.odoo_stock.odooclient.impl.stock_track_lineOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[stock_track_line] 服务对象接口
 */
@Service
public class stock_track_lineClientServiceImpl implements Istock_track_lineClientService {
    @Autowired
    private  Istock_track_lineOdooClient  stock_track_lineOdooClient;

    public Istock_track_line createModel() {		
		return new stock_track_lineImpl();
	}


        public void updateBatch(List<Istock_track_line> stock_track_lines){
            
        }
        
        public void get(Istock_track_line stock_track_line){
            this.stock_track_lineOdooClient.get(stock_track_line) ;
        }
        
        public void create(Istock_track_line stock_track_line){
this.stock_track_lineOdooClient.create(stock_track_line) ;
        }
        
        public void removeBatch(List<Istock_track_line> stock_track_lines){
            
        }
        
        public void update(Istock_track_line stock_track_line){
this.stock_track_lineOdooClient.update(stock_track_line) ;
        }
        
        public void remove(Istock_track_line stock_track_line){
this.stock_track_lineOdooClient.remove(stock_track_line) ;
        }
        
        public void createBatch(List<Istock_track_line> stock_track_lines){
            
        }
        
        public Page<Istock_track_line> search(SearchContext context){
            return this.stock_track_lineOdooClient.search(context) ;
        }
        
        public Page<Istock_track_line> select(SearchContext context){
            return null ;
        }
        

}

