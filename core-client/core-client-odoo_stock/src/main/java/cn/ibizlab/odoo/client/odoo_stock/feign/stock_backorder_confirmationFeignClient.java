package cn.ibizlab.odoo.client.odoo_stock.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Istock_backorder_confirmation;
import cn.ibizlab.odoo.client.odoo_stock.model.stock_backorder_confirmationImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[stock_backorder_confirmation] 服务对象接口
 */
public interface stock_backorder_confirmationFeignClient {

    @RequestMapping(method = RequestMethod.POST, value = "/odoo_stock/stock_backorder_confirmations")
    public stock_backorder_confirmationImpl create(@RequestBody stock_backorder_confirmationImpl stock_backorder_confirmation);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_stock/stock_backorder_confirmations/{id}")
    public stock_backorder_confirmationImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_stock/stock_backorder_confirmations/removebatch")
    public stock_backorder_confirmationImpl removeBatch(@RequestBody List<stock_backorder_confirmationImpl> stock_backorder_confirmations);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_stock/stock_backorder_confirmations/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_stock/stock_backorder_confirmations/updatebatch")
    public stock_backorder_confirmationImpl updateBatch(@RequestBody List<stock_backorder_confirmationImpl> stock_backorder_confirmations);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_stock/stock_backorder_confirmations/search")
    public Page<stock_backorder_confirmationImpl> search(SearchContext context);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_stock/stock_backorder_confirmations/{id}")
    public stock_backorder_confirmationImpl update(@PathVariable("id") Integer id,@RequestBody stock_backorder_confirmationImpl stock_backorder_confirmation);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_stock/stock_backorder_confirmations/createbatch")
    public stock_backorder_confirmationImpl createBatch(@RequestBody List<stock_backorder_confirmationImpl> stock_backorder_confirmations);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_stock/stock_backorder_confirmations/select")
    public Page<stock_backorder_confirmationImpl> select();



}
