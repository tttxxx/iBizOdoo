package cn.ibizlab.odoo.client.odoo_stock.odooclient;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.core.client.model.Istock_production_lot;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[stock_production_lot] 服务对象客户端接口
 */
public interface Istock_production_lotOdooClient {
    
        public void update(Istock_production_lot stock_production_lot);

        public void updateBatch(Istock_production_lot stock_production_lot);

        public void createBatch(Istock_production_lot stock_production_lot);

        public void get(Istock_production_lot stock_production_lot);

        public void removeBatch(Istock_production_lot stock_production_lot);

        public void create(Istock_production_lot stock_production_lot);

        public Page<Istock_production_lot> search(SearchContext context);

        public void remove(Istock_production_lot stock_production_lot);

        public List<Istock_production_lot> select();


}