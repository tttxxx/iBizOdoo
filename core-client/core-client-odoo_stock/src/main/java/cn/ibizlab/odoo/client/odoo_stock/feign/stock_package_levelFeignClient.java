package cn.ibizlab.odoo.client.odoo_stock.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Istock_package_level;
import cn.ibizlab.odoo.client.odoo_stock.model.stock_package_levelImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[stock_package_level] 服务对象接口
 */
public interface stock_package_levelFeignClient {

    @RequestMapping(method = RequestMethod.POST, value = "/odoo_stock/stock_package_levels/createbatch")
    public stock_package_levelImpl createBatch(@RequestBody List<stock_package_levelImpl> stock_package_levels);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_stock/stock_package_levels")
    public stock_package_levelImpl create(@RequestBody stock_package_levelImpl stock_package_level);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_stock/stock_package_levels/search")
    public Page<stock_package_levelImpl> search(SearchContext context);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_stock/stock_package_levels/{id}")
    public stock_package_levelImpl update(@PathVariable("id") Integer id,@RequestBody stock_package_levelImpl stock_package_level);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_stock/stock_package_levels/updatebatch")
    public stock_package_levelImpl updateBatch(@RequestBody List<stock_package_levelImpl> stock_package_levels);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_stock/stock_package_levels/removebatch")
    public stock_package_levelImpl removeBatch(@RequestBody List<stock_package_levelImpl> stock_package_levels);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_stock/stock_package_levels/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_stock/stock_package_levels/{id}")
    public stock_package_levelImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_stock/stock_package_levels/select")
    public Page<stock_package_levelImpl> select();



}
