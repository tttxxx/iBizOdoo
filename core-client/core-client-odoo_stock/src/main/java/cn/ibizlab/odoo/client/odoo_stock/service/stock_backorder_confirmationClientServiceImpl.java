package cn.ibizlab.odoo.client.odoo_stock.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Istock_backorder_confirmation;
import cn.ibizlab.odoo.core.client.service.Istock_backorder_confirmationClientService;
import cn.ibizlab.odoo.client.odoo_stock.model.stock_backorder_confirmationImpl;
import cn.ibizlab.odoo.client.odoo_stock.odooclient.Istock_backorder_confirmationOdooClient;
import cn.ibizlab.odoo.client.odoo_stock.odooclient.impl.stock_backorder_confirmationOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[stock_backorder_confirmation] 服务对象接口
 */
@Service
public class stock_backorder_confirmationClientServiceImpl implements Istock_backorder_confirmationClientService {
    @Autowired
    private  Istock_backorder_confirmationOdooClient  stock_backorder_confirmationOdooClient;

    public Istock_backorder_confirmation createModel() {		
		return new stock_backorder_confirmationImpl();
	}


        public void create(Istock_backorder_confirmation stock_backorder_confirmation){
this.stock_backorder_confirmationOdooClient.create(stock_backorder_confirmation) ;
        }
        
        public void get(Istock_backorder_confirmation stock_backorder_confirmation){
            this.stock_backorder_confirmationOdooClient.get(stock_backorder_confirmation) ;
        }
        
        public void removeBatch(List<Istock_backorder_confirmation> stock_backorder_confirmations){
            
        }
        
        public void remove(Istock_backorder_confirmation stock_backorder_confirmation){
this.stock_backorder_confirmationOdooClient.remove(stock_backorder_confirmation) ;
        }
        
        public void updateBatch(List<Istock_backorder_confirmation> stock_backorder_confirmations){
            
        }
        
        public Page<Istock_backorder_confirmation> search(SearchContext context){
            return this.stock_backorder_confirmationOdooClient.search(context) ;
        }
        
        public void update(Istock_backorder_confirmation stock_backorder_confirmation){
this.stock_backorder_confirmationOdooClient.update(stock_backorder_confirmation) ;
        }
        
        public void createBatch(List<Istock_backorder_confirmation> stock_backorder_confirmations){
            
        }
        
        public Page<Istock_backorder_confirmation> select(SearchContext context){
            return null ;
        }
        

}

