package cn.ibizlab.odoo.client.odoo_stock.odooclient;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.core.client.model.Istock_location_route;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[stock_location_route] 服务对象客户端接口
 */
public interface Istock_location_routeOdooClient {
    
        public void create(Istock_location_route stock_location_route);

        public void createBatch(Istock_location_route stock_location_route);

        public void removeBatch(Istock_location_route stock_location_route);

        public void updateBatch(Istock_location_route stock_location_route);

        public void get(Istock_location_route stock_location_route);

        public Page<Istock_location_route> search(SearchContext context);

        public void update(Istock_location_route stock_location_route);

        public void remove(Istock_location_route stock_location_route);

        public List<Istock_location_route> select();


}