package cn.ibizlab.odoo.client.odoo_stock.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Istock_immediate_transfer;
import cn.ibizlab.odoo.core.client.service.Istock_immediate_transferClientService;
import cn.ibizlab.odoo.client.odoo_stock.model.stock_immediate_transferImpl;
import cn.ibizlab.odoo.client.odoo_stock.odooclient.Istock_immediate_transferOdooClient;
import cn.ibizlab.odoo.client.odoo_stock.odooclient.impl.stock_immediate_transferOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[stock_immediate_transfer] 服务对象接口
 */
@Service
public class stock_immediate_transferClientServiceImpl implements Istock_immediate_transferClientService {
    @Autowired
    private  Istock_immediate_transferOdooClient  stock_immediate_transferOdooClient;

    public Istock_immediate_transfer createModel() {		
		return new stock_immediate_transferImpl();
	}


        public void createBatch(List<Istock_immediate_transfer> stock_immediate_transfers){
            
        }
        
        public void create(Istock_immediate_transfer stock_immediate_transfer){
this.stock_immediate_transferOdooClient.create(stock_immediate_transfer) ;
        }
        
        public void update(Istock_immediate_transfer stock_immediate_transfer){
this.stock_immediate_transferOdooClient.update(stock_immediate_transfer) ;
        }
        
        public void get(Istock_immediate_transfer stock_immediate_transfer){
            this.stock_immediate_transferOdooClient.get(stock_immediate_transfer) ;
        }
        
        public void removeBatch(List<Istock_immediate_transfer> stock_immediate_transfers){
            
        }
        
        public Page<Istock_immediate_transfer> search(SearchContext context){
            return this.stock_immediate_transferOdooClient.search(context) ;
        }
        
        public void remove(Istock_immediate_transfer stock_immediate_transfer){
this.stock_immediate_transferOdooClient.remove(stock_immediate_transfer) ;
        }
        
        public void updateBatch(List<Istock_immediate_transfer> stock_immediate_transfers){
            
        }
        
        public Page<Istock_immediate_transfer> select(SearchContext context){
            return null ;
        }
        

}

