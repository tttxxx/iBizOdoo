package cn.ibizlab.odoo.client.odoo_stock.odooclient;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.core.client.model.Istock_traceability_report;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[stock_traceability_report] 服务对象客户端接口
 */
public interface Istock_traceability_reportOdooClient {
    
        public Page<Istock_traceability_report> search(SearchContext context);

        public void create(Istock_traceability_report stock_traceability_report);

        public void update(Istock_traceability_report stock_traceability_report);

        public void removeBatch(Istock_traceability_report stock_traceability_report);

        public void createBatch(Istock_traceability_report stock_traceability_report);

        public void remove(Istock_traceability_report stock_traceability_report);

        public void updateBatch(Istock_traceability_report stock_traceability_report);

        public void get(Istock_traceability_report stock_traceability_report);

        public List<Istock_traceability_report> select();


}