package cn.ibizlab.odoo.client.odoo_stock.odooclient;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.core.client.model.Istock_quant;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[stock_quant] 服务对象客户端接口
 */
public interface Istock_quantOdooClient {
    
        public void update(Istock_quant stock_quant);

        public Page<Istock_quant> search(SearchContext context);

        public void removeBatch(Istock_quant stock_quant);

        public void get(Istock_quant stock_quant);

        public void create(Istock_quant stock_quant);

        public void createBatch(Istock_quant stock_quant);

        public void remove(Istock_quant stock_quant);

        public void updateBatch(Istock_quant stock_quant);

        public List<Istock_quant> select();


}