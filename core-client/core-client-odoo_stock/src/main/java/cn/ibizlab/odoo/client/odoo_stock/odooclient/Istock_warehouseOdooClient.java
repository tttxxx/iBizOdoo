package cn.ibizlab.odoo.client.odoo_stock.odooclient;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.core.client.model.Istock_warehouse;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[stock_warehouse] 服务对象客户端接口
 */
public interface Istock_warehouseOdooClient {
    
        public void updateBatch(Istock_warehouse stock_warehouse);

        public void remove(Istock_warehouse stock_warehouse);

        public void update(Istock_warehouse stock_warehouse);

        public Page<Istock_warehouse> search(SearchContext context);

        public void create(Istock_warehouse stock_warehouse);

        public void createBatch(Istock_warehouse stock_warehouse);

        public void removeBatch(Istock_warehouse stock_warehouse);

        public void get(Istock_warehouse stock_warehouse);

        public List<Istock_warehouse> select();


}