package cn.ibizlab.odoo.client.odoo_stock.odooclient;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.core.client.model.Istock_move_line;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[stock_move_line] 服务对象客户端接口
 */
public interface Istock_move_lineOdooClient {
    
        public void removeBatch(Istock_move_line stock_move_line);

        public void remove(Istock_move_line stock_move_line);

        public void create(Istock_move_line stock_move_line);

        public void createBatch(Istock_move_line stock_move_line);

        public Page<Istock_move_line> search(SearchContext context);

        public void updateBatch(Istock_move_line stock_move_line);

        public void get(Istock_move_line stock_move_line);

        public void update(Istock_move_line stock_move_line);

        public List<Istock_move_line> select();


}