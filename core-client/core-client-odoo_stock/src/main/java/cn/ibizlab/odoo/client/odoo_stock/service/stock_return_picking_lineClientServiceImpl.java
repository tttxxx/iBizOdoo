package cn.ibizlab.odoo.client.odoo_stock.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Istock_return_picking_line;
import cn.ibizlab.odoo.core.client.service.Istock_return_picking_lineClientService;
import cn.ibizlab.odoo.client.odoo_stock.model.stock_return_picking_lineImpl;
import cn.ibizlab.odoo.client.odoo_stock.odooclient.Istock_return_picking_lineOdooClient;
import cn.ibizlab.odoo.client.odoo_stock.odooclient.impl.stock_return_picking_lineOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[stock_return_picking_line] 服务对象接口
 */
@Service
public class stock_return_picking_lineClientServiceImpl implements Istock_return_picking_lineClientService {
    @Autowired
    private  Istock_return_picking_lineOdooClient  stock_return_picking_lineOdooClient;

    public Istock_return_picking_line createModel() {		
		return new stock_return_picking_lineImpl();
	}


        public void removeBatch(List<Istock_return_picking_line> stock_return_picking_lines){
            
        }
        
        public void remove(Istock_return_picking_line stock_return_picking_line){
this.stock_return_picking_lineOdooClient.remove(stock_return_picking_line) ;
        }
        
        public void createBatch(List<Istock_return_picking_line> stock_return_picking_lines){
            
        }
        
        public void get(Istock_return_picking_line stock_return_picking_line){
            this.stock_return_picking_lineOdooClient.get(stock_return_picking_line) ;
        }
        
        public Page<Istock_return_picking_line> search(SearchContext context){
            return this.stock_return_picking_lineOdooClient.search(context) ;
        }
        
        public void update(Istock_return_picking_line stock_return_picking_line){
this.stock_return_picking_lineOdooClient.update(stock_return_picking_line) ;
        }
        
        public void updateBatch(List<Istock_return_picking_line> stock_return_picking_lines){
            
        }
        
        public void create(Istock_return_picking_line stock_return_picking_line){
this.stock_return_picking_lineOdooClient.create(stock_return_picking_line) ;
        }
        
        public Page<Istock_return_picking_line> select(SearchContext context){
            return null ;
        }
        

}

