package cn.ibizlab.odoo.client.odoo_stock.odooclient;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.core.client.model.Istock_picking;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[stock_picking] 服务对象客户端接口
 */
public interface Istock_pickingOdooClient {
    
        public void remove(Istock_picking stock_picking);

        public void create(Istock_picking stock_picking);

        public void updateBatch(Istock_picking stock_picking);

        public void get(Istock_picking stock_picking);

        public void removeBatch(Istock_picking stock_picking);

        public void update(Istock_picking stock_picking);

        public void createBatch(Istock_picking stock_picking);

        public Page<Istock_picking> search(SearchContext context);

        public List<Istock_picking> select();


}