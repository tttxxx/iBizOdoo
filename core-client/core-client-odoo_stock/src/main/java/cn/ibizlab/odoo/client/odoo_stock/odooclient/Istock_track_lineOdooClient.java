package cn.ibizlab.odoo.client.odoo_stock.odooclient;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.core.client.model.Istock_track_line;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[stock_track_line] 服务对象客户端接口
 */
public interface Istock_track_lineOdooClient {
    
        public void updateBatch(Istock_track_line stock_track_line);

        public void get(Istock_track_line stock_track_line);

        public void create(Istock_track_line stock_track_line);

        public void removeBatch(Istock_track_line stock_track_line);

        public void update(Istock_track_line stock_track_line);

        public void remove(Istock_track_line stock_track_line);

        public void createBatch(Istock_track_line stock_track_line);

        public Page<Istock_track_line> search(SearchContext context);

        public List<Istock_track_line> select();


}