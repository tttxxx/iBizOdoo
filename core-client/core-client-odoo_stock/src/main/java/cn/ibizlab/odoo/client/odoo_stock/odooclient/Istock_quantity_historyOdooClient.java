package cn.ibizlab.odoo.client.odoo_stock.odooclient;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.core.client.model.Istock_quantity_history;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[stock_quantity_history] 服务对象客户端接口
 */
public interface Istock_quantity_historyOdooClient {
    
        public void update(Istock_quantity_history stock_quantity_history);

        public void create(Istock_quantity_history stock_quantity_history);

        public void updateBatch(Istock_quantity_history stock_quantity_history);

        public void removeBatch(Istock_quantity_history stock_quantity_history);

        public Page<Istock_quantity_history> search(SearchContext context);

        public void get(Istock_quantity_history stock_quantity_history);

        public void createBatch(Istock_quantity_history stock_quantity_history);

        public void remove(Istock_quantity_history stock_quantity_history);

        public List<Istock_quantity_history> select();


}