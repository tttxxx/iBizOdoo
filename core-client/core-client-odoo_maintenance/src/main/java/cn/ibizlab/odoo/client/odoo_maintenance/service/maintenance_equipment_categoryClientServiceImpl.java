package cn.ibizlab.odoo.client.odoo_maintenance.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Imaintenance_equipment_category;
import cn.ibizlab.odoo.core.client.service.Imaintenance_equipment_categoryClientService;
import cn.ibizlab.odoo.client.odoo_maintenance.model.maintenance_equipment_categoryImpl;
import cn.ibizlab.odoo.client.odoo_maintenance.odooclient.Imaintenance_equipment_categoryOdooClient;
import cn.ibizlab.odoo.client.odoo_maintenance.odooclient.impl.maintenance_equipment_categoryOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[maintenance_equipment_category] 服务对象接口
 */
@Service
public class maintenance_equipment_categoryClientServiceImpl implements Imaintenance_equipment_categoryClientService {
    @Autowired
    private  Imaintenance_equipment_categoryOdooClient  maintenance_equipment_categoryOdooClient;

    public Imaintenance_equipment_category createModel() {		
		return new maintenance_equipment_categoryImpl();
	}


        public void createBatch(List<Imaintenance_equipment_category> maintenance_equipment_categories){
            
        }
        
        public void create(Imaintenance_equipment_category maintenance_equipment_category){
this.maintenance_equipment_categoryOdooClient.create(maintenance_equipment_category) ;
        }
        
        public Page<Imaintenance_equipment_category> search(SearchContext context){
            return this.maintenance_equipment_categoryOdooClient.search(context) ;
        }
        
        public void update(Imaintenance_equipment_category maintenance_equipment_category){
this.maintenance_equipment_categoryOdooClient.update(maintenance_equipment_category) ;
        }
        
        public void get(Imaintenance_equipment_category maintenance_equipment_category){
            this.maintenance_equipment_categoryOdooClient.get(maintenance_equipment_category) ;
        }
        
        public void updateBatch(List<Imaintenance_equipment_category> maintenance_equipment_categories){
            
        }
        
        public void removeBatch(List<Imaintenance_equipment_category> maintenance_equipment_categories){
            
        }
        
        public void remove(Imaintenance_equipment_category maintenance_equipment_category){
this.maintenance_equipment_categoryOdooClient.remove(maintenance_equipment_category) ;
        }
        
        public Page<Imaintenance_equipment_category> select(SearchContext context){
            return null ;
        }
        

}

