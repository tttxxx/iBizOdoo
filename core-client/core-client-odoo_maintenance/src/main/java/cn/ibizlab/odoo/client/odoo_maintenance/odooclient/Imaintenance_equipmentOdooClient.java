package cn.ibizlab.odoo.client.odoo_maintenance.odooclient;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.core.client.model.Imaintenance_equipment;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[maintenance_equipment] 服务对象客户端接口
 */
public interface Imaintenance_equipmentOdooClient {
    
        public void remove(Imaintenance_equipment maintenance_equipment);

        public void removeBatch(Imaintenance_equipment maintenance_equipment);

        public void create(Imaintenance_equipment maintenance_equipment);

        public void updateBatch(Imaintenance_equipment maintenance_equipment);

        public void update(Imaintenance_equipment maintenance_equipment);

        public void get(Imaintenance_equipment maintenance_equipment);

        public Page<Imaintenance_equipment> search(SearchContext context);

        public void createBatch(Imaintenance_equipment maintenance_equipment);

        public List<Imaintenance_equipment> select();


}