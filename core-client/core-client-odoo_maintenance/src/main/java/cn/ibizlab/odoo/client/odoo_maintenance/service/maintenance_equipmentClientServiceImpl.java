package cn.ibizlab.odoo.client.odoo_maintenance.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Imaintenance_equipment;
import cn.ibizlab.odoo.core.client.service.Imaintenance_equipmentClientService;
import cn.ibizlab.odoo.client.odoo_maintenance.model.maintenance_equipmentImpl;
import cn.ibizlab.odoo.client.odoo_maintenance.odooclient.Imaintenance_equipmentOdooClient;
import cn.ibizlab.odoo.client.odoo_maintenance.odooclient.impl.maintenance_equipmentOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[maintenance_equipment] 服务对象接口
 */
@Service
public class maintenance_equipmentClientServiceImpl implements Imaintenance_equipmentClientService {
    @Autowired
    private  Imaintenance_equipmentOdooClient  maintenance_equipmentOdooClient;

    public Imaintenance_equipment createModel() {		
		return new maintenance_equipmentImpl();
	}


        public void remove(Imaintenance_equipment maintenance_equipment){
this.maintenance_equipmentOdooClient.remove(maintenance_equipment) ;
        }
        
        public void removeBatch(List<Imaintenance_equipment> maintenance_equipments){
            
        }
        
        public void create(Imaintenance_equipment maintenance_equipment){
this.maintenance_equipmentOdooClient.create(maintenance_equipment) ;
        }
        
        public void updateBatch(List<Imaintenance_equipment> maintenance_equipments){
            
        }
        
        public void update(Imaintenance_equipment maintenance_equipment){
this.maintenance_equipmentOdooClient.update(maintenance_equipment) ;
        }
        
        public void get(Imaintenance_equipment maintenance_equipment){
            this.maintenance_equipmentOdooClient.get(maintenance_equipment) ;
        }
        
        public Page<Imaintenance_equipment> search(SearchContext context){
            return this.maintenance_equipmentOdooClient.search(context) ;
        }
        
        public void createBatch(List<Imaintenance_equipment> maintenance_equipments){
            
        }
        
        public Page<Imaintenance_equipment> select(SearchContext context){
            return null ;
        }
        

}

