package cn.ibizlab.odoo.client.odoo_maintenance.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Imaintenance_team;
import cn.ibizlab.odoo.core.client.service.Imaintenance_teamClientService;
import cn.ibizlab.odoo.client.odoo_maintenance.model.maintenance_teamImpl;
import cn.ibizlab.odoo.client.odoo_maintenance.odooclient.Imaintenance_teamOdooClient;
import cn.ibizlab.odoo.client.odoo_maintenance.odooclient.impl.maintenance_teamOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[maintenance_team] 服务对象接口
 */
@Service
public class maintenance_teamClientServiceImpl implements Imaintenance_teamClientService {
    @Autowired
    private  Imaintenance_teamOdooClient  maintenance_teamOdooClient;

    public Imaintenance_team createModel() {		
		return new maintenance_teamImpl();
	}


        public void get(Imaintenance_team maintenance_team){
            this.maintenance_teamOdooClient.get(maintenance_team) ;
        }
        
        public void remove(Imaintenance_team maintenance_team){
this.maintenance_teamOdooClient.remove(maintenance_team) ;
        }
        
        public void update(Imaintenance_team maintenance_team){
this.maintenance_teamOdooClient.update(maintenance_team) ;
        }
        
        public void createBatch(List<Imaintenance_team> maintenance_teams){
            
        }
        
        public Page<Imaintenance_team> search(SearchContext context){
            return this.maintenance_teamOdooClient.search(context) ;
        }
        
        public void updateBatch(List<Imaintenance_team> maintenance_teams){
            
        }
        
        public void create(Imaintenance_team maintenance_team){
this.maintenance_teamOdooClient.create(maintenance_team) ;
        }
        
        public void removeBatch(List<Imaintenance_team> maintenance_teams){
            
        }
        
        public Page<Imaintenance_team> select(SearchContext context){
            return null ;
        }
        

}

