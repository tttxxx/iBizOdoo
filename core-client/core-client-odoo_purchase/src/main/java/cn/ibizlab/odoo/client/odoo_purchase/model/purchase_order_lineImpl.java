package cn.ibizlab.odoo.client.odoo_purchase.model;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.math.BigInteger;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.core.client.model.Ipurchase_order_line;
import cn.ibizlab.odoo.util.helper.OdooClientHelper;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang.ArrayUtils;
import org.springframework.util.StringUtils;

/**
 * 接口实体[purchase_order_line] 对象
 */
public class purchase_order_lineImpl implements Ipurchase_order_line,Serializable{

    /**
     * 分析账户
     */
    public Integer account_analytic_id;

    @JsonIgnore
    public boolean account_analytic_idDirtyFlag;
    
    /**
     * 分析账户
     */
    public String account_analytic_id_text;

    @JsonIgnore
    public boolean account_analytic_id_textDirtyFlag;
    
    /**
     * 分析标签
     */
    public String analytic_tag_ids;

    @JsonIgnore
    public boolean analytic_tag_idsDirtyFlag;
    
    /**
     * 公司
     */
    public Integer company_id;

    @JsonIgnore
    public boolean company_idDirtyFlag;
    
    /**
     * 公司
     */
    public String company_id_text;

    @JsonIgnore
    public boolean company_id_textDirtyFlag;
    
    /**
     * 创建时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp create_date;

    @JsonIgnore
    public boolean create_dateDirtyFlag;
    
    /**
     * 创建人
     */
    public Integer create_uid;

    @JsonIgnore
    public boolean create_uidDirtyFlag;
    
    /**
     * 创建人
     */
    public String create_uid_text;

    @JsonIgnore
    public boolean create_uid_textDirtyFlag;
    
    /**
     * 币种
     */
    public Integer currency_id;

    @JsonIgnore
    public boolean currency_idDirtyFlag;
    
    /**
     * 币种
     */
    public String currency_id_text;

    @JsonIgnore
    public boolean currency_id_textDirtyFlag;
    
    /**
     * 单据日期
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp date_order;

    @JsonIgnore
    public boolean date_orderDirtyFlag;
    
    /**
     * 计划日期
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp date_planned;

    @JsonIgnore
    public boolean date_plannedDirtyFlag;
    
    /**
     * 显示名称
     */
    public String display_name;

    @JsonIgnore
    public boolean display_nameDirtyFlag;
    
    /**
     * ID
     */
    public Integer id;

    @JsonIgnore
    public boolean idDirtyFlag;
    
    /**
     * 账单明细行
     */
    public String invoice_lines;

    @JsonIgnore
    public boolean invoice_linesDirtyFlag;
    
    /**
     * 下游移动
     */
    public String move_dest_ids;

    @JsonIgnore
    public boolean move_dest_idsDirtyFlag;
    
    /**
     * 保留
     */
    public String move_ids;

    @JsonIgnore
    public boolean move_idsDirtyFlag;
    
    /**
     * 说明
     */
    public String name;

    @JsonIgnore
    public boolean nameDirtyFlag;
    
    /**
     * 订货点
     */
    public Integer orderpoint_id;

    @JsonIgnore
    public boolean orderpoint_idDirtyFlag;
    
    /**
     * 订货点
     */
    public String orderpoint_id_text;

    @JsonIgnore
    public boolean orderpoint_id_textDirtyFlag;
    
    /**
     * 订单关联
     */
    public Integer order_id;

    @JsonIgnore
    public boolean order_idDirtyFlag;
    
    /**
     * 订单关联
     */
    public String order_id_text;

    @JsonIgnore
    public boolean order_id_textDirtyFlag;
    
    /**
     * 业务伙伴
     */
    public Integer partner_id;

    @JsonIgnore
    public boolean partner_idDirtyFlag;
    
    /**
     * 业务伙伴
     */
    public String partner_id_text;

    @JsonIgnore
    public boolean partner_id_textDirtyFlag;
    
    /**
     * 小计
     */
    public Double price_subtotal;

    @JsonIgnore
    public boolean price_subtotalDirtyFlag;
    
    /**
     * 税率
     */
    public Double price_tax;

    @JsonIgnore
    public boolean price_taxDirtyFlag;
    
    /**
     * 总计
     */
    public Double price_total;

    @JsonIgnore
    public boolean price_totalDirtyFlag;
    
    /**
     * 单价
     */
    public Double price_unit;

    @JsonIgnore
    public boolean price_unitDirtyFlag;
    
    /**
     * 产品
     */
    public Integer product_id;

    @JsonIgnore
    public boolean product_idDirtyFlag;
    
    /**
     * 产品
     */
    public String product_id_text;

    @JsonIgnore
    public boolean product_id_textDirtyFlag;
    
    /**
     * 产品图片
     */
    public byte[] product_image;

    @JsonIgnore
    public boolean product_imageDirtyFlag;
    
    /**
     * 数量
     */
    public Double product_qty;

    @JsonIgnore
    public boolean product_qtyDirtyFlag;
    
    /**
     * 产品类型
     */
    public String product_type;

    @JsonIgnore
    public boolean product_typeDirtyFlag;
    
    /**
     * 计量单位
     */
    public Integer product_uom;

    @JsonIgnore
    public boolean product_uomDirtyFlag;
    
    /**
     * 数量总计
     */
    public Double product_uom_qty;

    @JsonIgnore
    public boolean product_uom_qtyDirtyFlag;
    
    /**
     * 计量单位
     */
    public String product_uom_text;

    @JsonIgnore
    public boolean product_uom_textDirtyFlag;
    
    /**
     * 开票数量
     */
    public Double qty_invoiced;

    @JsonIgnore
    public boolean qty_invoicedDirtyFlag;
    
    /**
     * 已接收数量
     */
    public Double qty_received;

    @JsonIgnore
    public boolean qty_receivedDirtyFlag;
    
    /**
     * 原销售项
     */
    public Integer sale_line_id;

    @JsonIgnore
    public boolean sale_line_idDirtyFlag;
    
    /**
     * 原销售项
     */
    public String sale_line_id_text;

    @JsonIgnore
    public boolean sale_line_id_textDirtyFlag;
    
    /**
     * 销售订单
     */
    public Integer sale_order_id;

    @JsonIgnore
    public boolean sale_order_idDirtyFlag;
    
    /**
     * 销售订单
     */
    public String sale_order_id_text;

    @JsonIgnore
    public boolean sale_order_id_textDirtyFlag;
    
    /**
     * 序列
     */
    public Integer sequence;

    @JsonIgnore
    public boolean sequenceDirtyFlag;
    
    /**
     * 状态
     */
    public String state;

    @JsonIgnore
    public boolean stateDirtyFlag;
    
    /**
     * 税率
     */
    public String taxes_id;

    @JsonIgnore
    public boolean taxes_idDirtyFlag;
    
    /**
     * 最后更新时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp write_date;

    @JsonIgnore
    public boolean write_dateDirtyFlag;
    
    /**
     * 最后更新人
     */
    public Integer write_uid;

    @JsonIgnore
    public boolean write_uidDirtyFlag;
    
    /**
     * 最后更新人
     */
    public String write_uid_text;

    @JsonIgnore
    public boolean write_uid_textDirtyFlag;
    
    /**
     * 最后修改日
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp __last_update;

    @JsonIgnore
    public boolean __last_updateDirtyFlag;
    
    /**
     * 获取 [分析账户]
     */
    @JsonProperty("account_analytic_id")
    public Integer getAccount_analytic_id(){
        return this.account_analytic_id ;
    }

    /**
     * 设置 [分析账户]
     */
    @JsonProperty("account_analytic_id")
    public void setAccount_analytic_id(Integer  account_analytic_id){
        this.account_analytic_id = account_analytic_id ;
        this.account_analytic_idDirtyFlag = true ;
    }

     /**
     * 获取 [分析账户]脏标记
     */
    @JsonIgnore
    public boolean getAccount_analytic_idDirtyFlag(){
        return this.account_analytic_idDirtyFlag ;
    }   

    /**
     * 获取 [分析账户]
     */
    @JsonProperty("account_analytic_id_text")
    public String getAccount_analytic_id_text(){
        return this.account_analytic_id_text ;
    }

    /**
     * 设置 [分析账户]
     */
    @JsonProperty("account_analytic_id_text")
    public void setAccount_analytic_id_text(String  account_analytic_id_text){
        this.account_analytic_id_text = account_analytic_id_text ;
        this.account_analytic_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [分析账户]脏标记
     */
    @JsonIgnore
    public boolean getAccount_analytic_id_textDirtyFlag(){
        return this.account_analytic_id_textDirtyFlag ;
    }   

    /**
     * 获取 [分析标签]
     */
    @JsonProperty("analytic_tag_ids")
    public String getAnalytic_tag_ids(){
        return this.analytic_tag_ids ;
    }

    /**
     * 设置 [分析标签]
     */
    @JsonProperty("analytic_tag_ids")
    public void setAnalytic_tag_ids(String  analytic_tag_ids){
        this.analytic_tag_ids = analytic_tag_ids ;
        this.analytic_tag_idsDirtyFlag = true ;
    }

     /**
     * 获取 [分析标签]脏标记
     */
    @JsonIgnore
    public boolean getAnalytic_tag_idsDirtyFlag(){
        return this.analytic_tag_idsDirtyFlag ;
    }   

    /**
     * 获取 [公司]
     */
    @JsonProperty("company_id")
    public Integer getCompany_id(){
        return this.company_id ;
    }

    /**
     * 设置 [公司]
     */
    @JsonProperty("company_id")
    public void setCompany_id(Integer  company_id){
        this.company_id = company_id ;
        this.company_idDirtyFlag = true ;
    }

     /**
     * 获取 [公司]脏标记
     */
    @JsonIgnore
    public boolean getCompany_idDirtyFlag(){
        return this.company_idDirtyFlag ;
    }   

    /**
     * 获取 [公司]
     */
    @JsonProperty("company_id_text")
    public String getCompany_id_text(){
        return this.company_id_text ;
    }

    /**
     * 设置 [公司]
     */
    @JsonProperty("company_id_text")
    public void setCompany_id_text(String  company_id_text){
        this.company_id_text = company_id_text ;
        this.company_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [公司]脏标记
     */
    @JsonIgnore
    public boolean getCompany_id_textDirtyFlag(){
        return this.company_id_textDirtyFlag ;
    }   

    /**
     * 获取 [创建时间]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return this.create_date ;
    }

    /**
     * 设置 [创建时间]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

     /**
     * 获取 [创建时间]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return this.create_dateDirtyFlag ;
    }   

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return this.create_uid ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

     /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return this.create_uidDirtyFlag ;
    }   

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return this.create_uid_text ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

     /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return this.create_uid_textDirtyFlag ;
    }   

    /**
     * 获取 [币种]
     */
    @JsonProperty("currency_id")
    public Integer getCurrency_id(){
        return this.currency_id ;
    }

    /**
     * 设置 [币种]
     */
    @JsonProperty("currency_id")
    public void setCurrency_id(Integer  currency_id){
        this.currency_id = currency_id ;
        this.currency_idDirtyFlag = true ;
    }

     /**
     * 获取 [币种]脏标记
     */
    @JsonIgnore
    public boolean getCurrency_idDirtyFlag(){
        return this.currency_idDirtyFlag ;
    }   

    /**
     * 获取 [币种]
     */
    @JsonProperty("currency_id_text")
    public String getCurrency_id_text(){
        return this.currency_id_text ;
    }

    /**
     * 设置 [币种]
     */
    @JsonProperty("currency_id_text")
    public void setCurrency_id_text(String  currency_id_text){
        this.currency_id_text = currency_id_text ;
        this.currency_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [币种]脏标记
     */
    @JsonIgnore
    public boolean getCurrency_id_textDirtyFlag(){
        return this.currency_id_textDirtyFlag ;
    }   

    /**
     * 获取 [单据日期]
     */
    @JsonProperty("date_order")
    public Timestamp getDate_order(){
        return this.date_order ;
    }

    /**
     * 设置 [单据日期]
     */
    @JsonProperty("date_order")
    public void setDate_order(Timestamp  date_order){
        this.date_order = date_order ;
        this.date_orderDirtyFlag = true ;
    }

     /**
     * 获取 [单据日期]脏标记
     */
    @JsonIgnore
    public boolean getDate_orderDirtyFlag(){
        return this.date_orderDirtyFlag ;
    }   

    /**
     * 获取 [计划日期]
     */
    @JsonProperty("date_planned")
    public Timestamp getDate_planned(){
        return this.date_planned ;
    }

    /**
     * 设置 [计划日期]
     */
    @JsonProperty("date_planned")
    public void setDate_planned(Timestamp  date_planned){
        this.date_planned = date_planned ;
        this.date_plannedDirtyFlag = true ;
    }

     /**
     * 获取 [计划日期]脏标记
     */
    @JsonIgnore
    public boolean getDate_plannedDirtyFlag(){
        return this.date_plannedDirtyFlag ;
    }   

    /**
     * 获取 [显示名称]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return this.display_name ;
    }

    /**
     * 设置 [显示名称]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

     /**
     * 获取 [显示名称]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return this.display_nameDirtyFlag ;
    }   

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return this.id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

     /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return this.idDirtyFlag ;
    }   

    /**
     * 获取 [账单明细行]
     */
    @JsonProperty("invoice_lines")
    public String getInvoice_lines(){
        return this.invoice_lines ;
    }

    /**
     * 设置 [账单明细行]
     */
    @JsonProperty("invoice_lines")
    public void setInvoice_lines(String  invoice_lines){
        this.invoice_lines = invoice_lines ;
        this.invoice_linesDirtyFlag = true ;
    }

     /**
     * 获取 [账单明细行]脏标记
     */
    @JsonIgnore
    public boolean getInvoice_linesDirtyFlag(){
        return this.invoice_linesDirtyFlag ;
    }   

    /**
     * 获取 [下游移动]
     */
    @JsonProperty("move_dest_ids")
    public String getMove_dest_ids(){
        return this.move_dest_ids ;
    }

    /**
     * 设置 [下游移动]
     */
    @JsonProperty("move_dest_ids")
    public void setMove_dest_ids(String  move_dest_ids){
        this.move_dest_ids = move_dest_ids ;
        this.move_dest_idsDirtyFlag = true ;
    }

     /**
     * 获取 [下游移动]脏标记
     */
    @JsonIgnore
    public boolean getMove_dest_idsDirtyFlag(){
        return this.move_dest_idsDirtyFlag ;
    }   

    /**
     * 获取 [保留]
     */
    @JsonProperty("move_ids")
    public String getMove_ids(){
        return this.move_ids ;
    }

    /**
     * 设置 [保留]
     */
    @JsonProperty("move_ids")
    public void setMove_ids(String  move_ids){
        this.move_ids = move_ids ;
        this.move_idsDirtyFlag = true ;
    }

     /**
     * 获取 [保留]脏标记
     */
    @JsonIgnore
    public boolean getMove_idsDirtyFlag(){
        return this.move_idsDirtyFlag ;
    }   

    /**
     * 获取 [说明]
     */
    @JsonProperty("name")
    public String getName(){
        return this.name ;
    }

    /**
     * 设置 [说明]
     */
    @JsonProperty("name")
    public void setName(String  name){
        this.name = name ;
        this.nameDirtyFlag = true ;
    }

     /**
     * 获取 [说明]脏标记
     */
    @JsonIgnore
    public boolean getNameDirtyFlag(){
        return this.nameDirtyFlag ;
    }   

    /**
     * 获取 [订货点]
     */
    @JsonProperty("orderpoint_id")
    public Integer getOrderpoint_id(){
        return this.orderpoint_id ;
    }

    /**
     * 设置 [订货点]
     */
    @JsonProperty("orderpoint_id")
    public void setOrderpoint_id(Integer  orderpoint_id){
        this.orderpoint_id = orderpoint_id ;
        this.orderpoint_idDirtyFlag = true ;
    }

     /**
     * 获取 [订货点]脏标记
     */
    @JsonIgnore
    public boolean getOrderpoint_idDirtyFlag(){
        return this.orderpoint_idDirtyFlag ;
    }   

    /**
     * 获取 [订货点]
     */
    @JsonProperty("orderpoint_id_text")
    public String getOrderpoint_id_text(){
        return this.orderpoint_id_text ;
    }

    /**
     * 设置 [订货点]
     */
    @JsonProperty("orderpoint_id_text")
    public void setOrderpoint_id_text(String  orderpoint_id_text){
        this.orderpoint_id_text = orderpoint_id_text ;
        this.orderpoint_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [订货点]脏标记
     */
    @JsonIgnore
    public boolean getOrderpoint_id_textDirtyFlag(){
        return this.orderpoint_id_textDirtyFlag ;
    }   

    /**
     * 获取 [订单关联]
     */
    @JsonProperty("order_id")
    public Integer getOrder_id(){
        return this.order_id ;
    }

    /**
     * 设置 [订单关联]
     */
    @JsonProperty("order_id")
    public void setOrder_id(Integer  order_id){
        this.order_id = order_id ;
        this.order_idDirtyFlag = true ;
    }

     /**
     * 获取 [订单关联]脏标记
     */
    @JsonIgnore
    public boolean getOrder_idDirtyFlag(){
        return this.order_idDirtyFlag ;
    }   

    /**
     * 获取 [订单关联]
     */
    @JsonProperty("order_id_text")
    public String getOrder_id_text(){
        return this.order_id_text ;
    }

    /**
     * 设置 [订单关联]
     */
    @JsonProperty("order_id_text")
    public void setOrder_id_text(String  order_id_text){
        this.order_id_text = order_id_text ;
        this.order_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [订单关联]脏标记
     */
    @JsonIgnore
    public boolean getOrder_id_textDirtyFlag(){
        return this.order_id_textDirtyFlag ;
    }   

    /**
     * 获取 [业务伙伴]
     */
    @JsonProperty("partner_id")
    public Integer getPartner_id(){
        return this.partner_id ;
    }

    /**
     * 设置 [业务伙伴]
     */
    @JsonProperty("partner_id")
    public void setPartner_id(Integer  partner_id){
        this.partner_id = partner_id ;
        this.partner_idDirtyFlag = true ;
    }

     /**
     * 获取 [业务伙伴]脏标记
     */
    @JsonIgnore
    public boolean getPartner_idDirtyFlag(){
        return this.partner_idDirtyFlag ;
    }   

    /**
     * 获取 [业务伙伴]
     */
    @JsonProperty("partner_id_text")
    public String getPartner_id_text(){
        return this.partner_id_text ;
    }

    /**
     * 设置 [业务伙伴]
     */
    @JsonProperty("partner_id_text")
    public void setPartner_id_text(String  partner_id_text){
        this.partner_id_text = partner_id_text ;
        this.partner_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [业务伙伴]脏标记
     */
    @JsonIgnore
    public boolean getPartner_id_textDirtyFlag(){
        return this.partner_id_textDirtyFlag ;
    }   

    /**
     * 获取 [小计]
     */
    @JsonProperty("price_subtotal")
    public Double getPrice_subtotal(){
        return this.price_subtotal ;
    }

    /**
     * 设置 [小计]
     */
    @JsonProperty("price_subtotal")
    public void setPrice_subtotal(Double  price_subtotal){
        this.price_subtotal = price_subtotal ;
        this.price_subtotalDirtyFlag = true ;
    }

     /**
     * 获取 [小计]脏标记
     */
    @JsonIgnore
    public boolean getPrice_subtotalDirtyFlag(){
        return this.price_subtotalDirtyFlag ;
    }   

    /**
     * 获取 [税率]
     */
    @JsonProperty("price_tax")
    public Double getPrice_tax(){
        return this.price_tax ;
    }

    /**
     * 设置 [税率]
     */
    @JsonProperty("price_tax")
    public void setPrice_tax(Double  price_tax){
        this.price_tax = price_tax ;
        this.price_taxDirtyFlag = true ;
    }

     /**
     * 获取 [税率]脏标记
     */
    @JsonIgnore
    public boolean getPrice_taxDirtyFlag(){
        return this.price_taxDirtyFlag ;
    }   

    /**
     * 获取 [总计]
     */
    @JsonProperty("price_total")
    public Double getPrice_total(){
        return this.price_total ;
    }

    /**
     * 设置 [总计]
     */
    @JsonProperty("price_total")
    public void setPrice_total(Double  price_total){
        this.price_total = price_total ;
        this.price_totalDirtyFlag = true ;
    }

     /**
     * 获取 [总计]脏标记
     */
    @JsonIgnore
    public boolean getPrice_totalDirtyFlag(){
        return this.price_totalDirtyFlag ;
    }   

    /**
     * 获取 [单价]
     */
    @JsonProperty("price_unit")
    public Double getPrice_unit(){
        return this.price_unit ;
    }

    /**
     * 设置 [单价]
     */
    @JsonProperty("price_unit")
    public void setPrice_unit(Double  price_unit){
        this.price_unit = price_unit ;
        this.price_unitDirtyFlag = true ;
    }

     /**
     * 获取 [单价]脏标记
     */
    @JsonIgnore
    public boolean getPrice_unitDirtyFlag(){
        return this.price_unitDirtyFlag ;
    }   

    /**
     * 获取 [产品]
     */
    @JsonProperty("product_id")
    public Integer getProduct_id(){
        return this.product_id ;
    }

    /**
     * 设置 [产品]
     */
    @JsonProperty("product_id")
    public void setProduct_id(Integer  product_id){
        this.product_id = product_id ;
        this.product_idDirtyFlag = true ;
    }

     /**
     * 获取 [产品]脏标记
     */
    @JsonIgnore
    public boolean getProduct_idDirtyFlag(){
        return this.product_idDirtyFlag ;
    }   

    /**
     * 获取 [产品]
     */
    @JsonProperty("product_id_text")
    public String getProduct_id_text(){
        return this.product_id_text ;
    }

    /**
     * 设置 [产品]
     */
    @JsonProperty("product_id_text")
    public void setProduct_id_text(String  product_id_text){
        this.product_id_text = product_id_text ;
        this.product_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [产品]脏标记
     */
    @JsonIgnore
    public boolean getProduct_id_textDirtyFlag(){
        return this.product_id_textDirtyFlag ;
    }   

    /**
     * 获取 [产品图片]
     */
    @JsonProperty("product_image")
    public byte[] getProduct_image(){
        return this.product_image ;
    }

    /**
     * 设置 [产品图片]
     */
    @JsonProperty("product_image")
    public void setProduct_image(byte[]  product_image){
        this.product_image = product_image ;
        this.product_imageDirtyFlag = true ;
    }

     /**
     * 获取 [产品图片]脏标记
     */
    @JsonIgnore
    public boolean getProduct_imageDirtyFlag(){
        return this.product_imageDirtyFlag ;
    }   

    /**
     * 获取 [数量]
     */
    @JsonProperty("product_qty")
    public Double getProduct_qty(){
        return this.product_qty ;
    }

    /**
     * 设置 [数量]
     */
    @JsonProperty("product_qty")
    public void setProduct_qty(Double  product_qty){
        this.product_qty = product_qty ;
        this.product_qtyDirtyFlag = true ;
    }

     /**
     * 获取 [数量]脏标记
     */
    @JsonIgnore
    public boolean getProduct_qtyDirtyFlag(){
        return this.product_qtyDirtyFlag ;
    }   

    /**
     * 获取 [产品类型]
     */
    @JsonProperty("product_type")
    public String getProduct_type(){
        return this.product_type ;
    }

    /**
     * 设置 [产品类型]
     */
    @JsonProperty("product_type")
    public void setProduct_type(String  product_type){
        this.product_type = product_type ;
        this.product_typeDirtyFlag = true ;
    }

     /**
     * 获取 [产品类型]脏标记
     */
    @JsonIgnore
    public boolean getProduct_typeDirtyFlag(){
        return this.product_typeDirtyFlag ;
    }   

    /**
     * 获取 [计量单位]
     */
    @JsonProperty("product_uom")
    public Integer getProduct_uom(){
        return this.product_uom ;
    }

    /**
     * 设置 [计量单位]
     */
    @JsonProperty("product_uom")
    public void setProduct_uom(Integer  product_uom){
        this.product_uom = product_uom ;
        this.product_uomDirtyFlag = true ;
    }

     /**
     * 获取 [计量单位]脏标记
     */
    @JsonIgnore
    public boolean getProduct_uomDirtyFlag(){
        return this.product_uomDirtyFlag ;
    }   

    /**
     * 获取 [数量总计]
     */
    @JsonProperty("product_uom_qty")
    public Double getProduct_uom_qty(){
        return this.product_uom_qty ;
    }

    /**
     * 设置 [数量总计]
     */
    @JsonProperty("product_uom_qty")
    public void setProduct_uom_qty(Double  product_uom_qty){
        this.product_uom_qty = product_uom_qty ;
        this.product_uom_qtyDirtyFlag = true ;
    }

     /**
     * 获取 [数量总计]脏标记
     */
    @JsonIgnore
    public boolean getProduct_uom_qtyDirtyFlag(){
        return this.product_uom_qtyDirtyFlag ;
    }   

    /**
     * 获取 [计量单位]
     */
    @JsonProperty("product_uom_text")
    public String getProduct_uom_text(){
        return this.product_uom_text ;
    }

    /**
     * 设置 [计量单位]
     */
    @JsonProperty("product_uom_text")
    public void setProduct_uom_text(String  product_uom_text){
        this.product_uom_text = product_uom_text ;
        this.product_uom_textDirtyFlag = true ;
    }

     /**
     * 获取 [计量单位]脏标记
     */
    @JsonIgnore
    public boolean getProduct_uom_textDirtyFlag(){
        return this.product_uom_textDirtyFlag ;
    }   

    /**
     * 获取 [开票数量]
     */
    @JsonProperty("qty_invoiced")
    public Double getQty_invoiced(){
        return this.qty_invoiced ;
    }

    /**
     * 设置 [开票数量]
     */
    @JsonProperty("qty_invoiced")
    public void setQty_invoiced(Double  qty_invoiced){
        this.qty_invoiced = qty_invoiced ;
        this.qty_invoicedDirtyFlag = true ;
    }

     /**
     * 获取 [开票数量]脏标记
     */
    @JsonIgnore
    public boolean getQty_invoicedDirtyFlag(){
        return this.qty_invoicedDirtyFlag ;
    }   

    /**
     * 获取 [已接收数量]
     */
    @JsonProperty("qty_received")
    public Double getQty_received(){
        return this.qty_received ;
    }

    /**
     * 设置 [已接收数量]
     */
    @JsonProperty("qty_received")
    public void setQty_received(Double  qty_received){
        this.qty_received = qty_received ;
        this.qty_receivedDirtyFlag = true ;
    }

     /**
     * 获取 [已接收数量]脏标记
     */
    @JsonIgnore
    public boolean getQty_receivedDirtyFlag(){
        return this.qty_receivedDirtyFlag ;
    }   

    /**
     * 获取 [原销售项]
     */
    @JsonProperty("sale_line_id")
    public Integer getSale_line_id(){
        return this.sale_line_id ;
    }

    /**
     * 设置 [原销售项]
     */
    @JsonProperty("sale_line_id")
    public void setSale_line_id(Integer  sale_line_id){
        this.sale_line_id = sale_line_id ;
        this.sale_line_idDirtyFlag = true ;
    }

     /**
     * 获取 [原销售项]脏标记
     */
    @JsonIgnore
    public boolean getSale_line_idDirtyFlag(){
        return this.sale_line_idDirtyFlag ;
    }   

    /**
     * 获取 [原销售项]
     */
    @JsonProperty("sale_line_id_text")
    public String getSale_line_id_text(){
        return this.sale_line_id_text ;
    }

    /**
     * 设置 [原销售项]
     */
    @JsonProperty("sale_line_id_text")
    public void setSale_line_id_text(String  sale_line_id_text){
        this.sale_line_id_text = sale_line_id_text ;
        this.sale_line_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [原销售项]脏标记
     */
    @JsonIgnore
    public boolean getSale_line_id_textDirtyFlag(){
        return this.sale_line_id_textDirtyFlag ;
    }   

    /**
     * 获取 [销售订单]
     */
    @JsonProperty("sale_order_id")
    public Integer getSale_order_id(){
        return this.sale_order_id ;
    }

    /**
     * 设置 [销售订单]
     */
    @JsonProperty("sale_order_id")
    public void setSale_order_id(Integer  sale_order_id){
        this.sale_order_id = sale_order_id ;
        this.sale_order_idDirtyFlag = true ;
    }

     /**
     * 获取 [销售订单]脏标记
     */
    @JsonIgnore
    public boolean getSale_order_idDirtyFlag(){
        return this.sale_order_idDirtyFlag ;
    }   

    /**
     * 获取 [销售订单]
     */
    @JsonProperty("sale_order_id_text")
    public String getSale_order_id_text(){
        return this.sale_order_id_text ;
    }

    /**
     * 设置 [销售订单]
     */
    @JsonProperty("sale_order_id_text")
    public void setSale_order_id_text(String  sale_order_id_text){
        this.sale_order_id_text = sale_order_id_text ;
        this.sale_order_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [销售订单]脏标记
     */
    @JsonIgnore
    public boolean getSale_order_id_textDirtyFlag(){
        return this.sale_order_id_textDirtyFlag ;
    }   

    /**
     * 获取 [序列]
     */
    @JsonProperty("sequence")
    public Integer getSequence(){
        return this.sequence ;
    }

    /**
     * 设置 [序列]
     */
    @JsonProperty("sequence")
    public void setSequence(Integer  sequence){
        this.sequence = sequence ;
        this.sequenceDirtyFlag = true ;
    }

     /**
     * 获取 [序列]脏标记
     */
    @JsonIgnore
    public boolean getSequenceDirtyFlag(){
        return this.sequenceDirtyFlag ;
    }   

    /**
     * 获取 [状态]
     */
    @JsonProperty("state")
    public String getState(){
        return this.state ;
    }

    /**
     * 设置 [状态]
     */
    @JsonProperty("state")
    public void setState(String  state){
        this.state = state ;
        this.stateDirtyFlag = true ;
    }

     /**
     * 获取 [状态]脏标记
     */
    @JsonIgnore
    public boolean getStateDirtyFlag(){
        return this.stateDirtyFlag ;
    }   

    /**
     * 获取 [税率]
     */
    @JsonProperty("taxes_id")
    public String getTaxes_id(){
        return this.taxes_id ;
    }

    /**
     * 设置 [税率]
     */
    @JsonProperty("taxes_id")
    public void setTaxes_id(String  taxes_id){
        this.taxes_id = taxes_id ;
        this.taxes_idDirtyFlag = true ;
    }

     /**
     * 获取 [税率]脏标记
     */
    @JsonIgnore
    public boolean getTaxes_idDirtyFlag(){
        return this.taxes_idDirtyFlag ;
    }   

    /**
     * 获取 [最后更新时间]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return this.write_date ;
    }

    /**
     * 设置 [最后更新时间]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新时间]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return this.write_dateDirtyFlag ;
    }   

    /**
     * 获取 [最后更新人]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return this.write_uid ;
    }

    /**
     * 设置 [最后更新人]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新人]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return this.write_uidDirtyFlag ;
    }   

    /**
     * 获取 [最后更新人]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return this.write_uid_text ;
    }

    /**
     * 设置 [最后更新人]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新人]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return this.write_uid_textDirtyFlag ;
    }   

    /**
     * 获取 [最后修改日]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return this.__last_update ;
    }

    /**
     * 设置 [最后修改日]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

     /**
     * 获取 [最后修改日]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return this.__last_updateDirtyFlag ;
    }   



    public void fromMap(Map<String, Object> map) throws Exception {
		if(!(map.get("account_analytic_id") instanceof Boolean)&& map.get("account_analytic_id")!=null){
			Object[] objs = (Object[])map.get("account_analytic_id");
			if(objs.length > 0){
				this.setAccount_analytic_id((Integer)objs[0]);
			}
		}
		if(!(map.get("account_analytic_id") instanceof Boolean)&& map.get("account_analytic_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("account_analytic_id");
			if(objs.length > 1){
				this.setAccount_analytic_id_text((String)objs[1]);
			}
		}
		if(!(map.get("analytic_tag_ids") instanceof Boolean)&& map.get("analytic_tag_ids")!=null){
			Object[] objs = (Object[])map.get("analytic_tag_ids");
			if(objs.length > 0){
				Integer[] analytic_tag_ids = Arrays.copyOfRange(objs,0,objs.length,Integer[].class);
				this.setAnalytic_tag_ids(Arrays.toString(analytic_tag_ids));
			}
		}
		if(!(map.get("company_id") instanceof Boolean)&& map.get("company_id")!=null){
			Object[] objs = (Object[])map.get("company_id");
			if(objs.length > 0){
				this.setCompany_id((Integer)objs[0]);
			}
		}
		if(!(map.get("company_id") instanceof Boolean)&& map.get("company_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("company_id");
			if(objs.length > 1){
				this.setCompany_id_text((String)objs[1]);
			}
		}
		if(!(map.get("create_date") instanceof Boolean)&& map.get("create_date")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse((String)map.get("create_date"));
   			this.setCreate_date(new Timestamp(parse.getTime()));
		}
		if(!(map.get("create_uid") instanceof Boolean)&& map.get("create_uid")!=null){
			Object[] objs = (Object[])map.get("create_uid");
			if(objs.length > 0){
				this.setCreate_uid((Integer)objs[0]);
			}
		}
		if(!(map.get("create_uid") instanceof Boolean)&& map.get("create_uid")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("create_uid");
			if(objs.length > 1){
				this.setCreate_uid_text((String)objs[1]);
			}
		}
		if(!(map.get("currency_id") instanceof Boolean)&& map.get("currency_id")!=null){
			Object[] objs = (Object[])map.get("currency_id");
			if(objs.length > 0){
				this.setCurrency_id((Integer)objs[0]);
			}
		}
		if(!(map.get("currency_id") instanceof Boolean)&& map.get("currency_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("currency_id");
			if(objs.length > 1){
				this.setCurrency_id_text((String)objs[1]);
			}
		}
		if(!(map.get("date_order") instanceof Boolean)&& map.get("date_order")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse((String)map.get("date_order"));
   			this.setDate_order(new Timestamp(parse.getTime()));
		}
		if(!(map.get("date_planned") instanceof Boolean)&& map.get("date_planned")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse((String)map.get("date_planned"));
   			this.setDate_planned(new Timestamp(parse.getTime()));
		}
		if(!(map.get("display_name") instanceof Boolean)&& map.get("display_name")!=null){
			this.setDisplay_name((String)map.get("display_name"));
		}
		if(!(map.get("id") instanceof Boolean)&& map.get("id")!=null){
			this.setId((Integer)map.get("id"));
		}
		if(!(map.get("invoice_lines") instanceof Boolean)&& map.get("invoice_lines")!=null){
			Object[] objs = (Object[])map.get("invoice_lines");
			if(objs.length > 0){
				Integer[] invoice_lines = Arrays.copyOfRange(objs,0,objs.length,Integer[].class);
				this.setInvoice_lines(Arrays.toString(invoice_lines));
			}
		}
		if(!(map.get("move_dest_ids") instanceof Boolean)&& map.get("move_dest_ids")!=null){
			Object[] objs = (Object[])map.get("move_dest_ids");
			if(objs.length > 0){
				Integer[] move_dest_ids = Arrays.copyOfRange(objs,0,objs.length,Integer[].class);
				this.setMove_dest_ids(Arrays.toString(move_dest_ids));
			}
		}
		if(!(map.get("move_ids") instanceof Boolean)&& map.get("move_ids")!=null){
			Object[] objs = (Object[])map.get("move_ids");
			if(objs.length > 0){
				Integer[] move_ids = Arrays.copyOfRange(objs,0,objs.length,Integer[].class);
				this.setMove_ids(Arrays.toString(move_ids));
			}
		}
		if(!(map.get("name") instanceof Boolean)&& map.get("name")!=null){
			this.setName((String)map.get("name"));
		}
		if(!(map.get("orderpoint_id") instanceof Boolean)&& map.get("orderpoint_id")!=null){
			Object[] objs = (Object[])map.get("orderpoint_id");
			if(objs.length > 0){
				this.setOrderpoint_id((Integer)objs[0]);
			}
		}
		if(!(map.get("orderpoint_id") instanceof Boolean)&& map.get("orderpoint_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("orderpoint_id");
			if(objs.length > 1){
				this.setOrderpoint_id_text((String)objs[1]);
			}
		}
		if(!(map.get("order_id") instanceof Boolean)&& map.get("order_id")!=null){
			Object[] objs = (Object[])map.get("order_id");
			if(objs.length > 0){
				this.setOrder_id((Integer)objs[0]);
			}
		}
		if(!(map.get("order_id") instanceof Boolean)&& map.get("order_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("order_id");
			if(objs.length > 1){
				this.setOrder_id_text((String)objs[1]);
			}
		}
		if(!(map.get("partner_id") instanceof Boolean)&& map.get("partner_id")!=null){
			Object[] objs = (Object[])map.get("partner_id");
			if(objs.length > 0){
				this.setPartner_id((Integer)objs[0]);
			}
		}
		if(!(map.get("partner_id") instanceof Boolean)&& map.get("partner_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("partner_id");
			if(objs.length > 1){
				this.setPartner_id_text((String)objs[1]);
			}
		}
		if(!(map.get("price_subtotal") instanceof Boolean)&& map.get("price_subtotal")!=null){
			this.setPrice_subtotal((Double)map.get("price_subtotal"));
		}
		if(!(map.get("price_tax") instanceof Boolean)&& map.get("price_tax")!=null){
			this.setPrice_tax((Double)map.get("price_tax"));
		}
		if(!(map.get("price_total") instanceof Boolean)&& map.get("price_total")!=null){
			this.setPrice_total((Double)map.get("price_total"));
		}
		if(!(map.get("price_unit") instanceof Boolean)&& map.get("price_unit")!=null){
			this.setPrice_unit((Double)map.get("price_unit"));
		}
		if(!(map.get("product_id") instanceof Boolean)&& map.get("product_id")!=null){
			Object[] objs = (Object[])map.get("product_id");
			if(objs.length > 0){
				this.setProduct_id((Integer)objs[0]);
			}
		}
		if(!(map.get("product_id") instanceof Boolean)&& map.get("product_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("product_id");
			if(objs.length > 1){
				this.setProduct_id_text((String)objs[1]);
			}
		}
		if(!(map.get("product_image") instanceof Boolean)&& map.get("product_image")!=null){
			//暂时忽略
			//this.setProduct_image(((String)map.get("product_image")).getBytes("UTF-8"));
		}
		if(!(map.get("product_qty") instanceof Boolean)&& map.get("product_qty")!=null){
			this.setProduct_qty((Double)map.get("product_qty"));
		}
		if(!(map.get("product_type") instanceof Boolean)&& map.get("product_type")!=null){
			this.setProduct_type((String)map.get("product_type"));
		}
		if(!(map.get("product_uom") instanceof Boolean)&& map.get("product_uom")!=null){
			Object[] objs = (Object[])map.get("product_uom");
			if(objs.length > 0){
				this.setProduct_uom((Integer)objs[0]);
			}
		}
		if(!(map.get("product_uom_qty") instanceof Boolean)&& map.get("product_uom_qty")!=null){
			this.setProduct_uom_qty((Double)map.get("product_uom_qty"));
		}
		if(!(map.get("product_uom") instanceof Boolean)&& map.get("product_uom")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("product_uom");
			if(objs.length > 1){
				this.setProduct_uom_text((String)objs[1]);
			}
		}
		if(!(map.get("qty_invoiced") instanceof Boolean)&& map.get("qty_invoiced")!=null){
			this.setQty_invoiced((Double)map.get("qty_invoiced"));
		}
		if(!(map.get("qty_received") instanceof Boolean)&& map.get("qty_received")!=null){
			this.setQty_received((Double)map.get("qty_received"));
		}
		if(!(map.get("sale_line_id") instanceof Boolean)&& map.get("sale_line_id")!=null){
			Object[] objs = (Object[])map.get("sale_line_id");
			if(objs.length > 0){
				this.setSale_line_id((Integer)objs[0]);
			}
		}
		if(!(map.get("sale_line_id") instanceof Boolean)&& map.get("sale_line_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("sale_line_id");
			if(objs.length > 1){
				this.setSale_line_id_text((String)objs[1]);
			}
		}
		if(!(map.get("sale_order_id") instanceof Boolean)&& map.get("sale_order_id")!=null){
			Object[] objs = (Object[])map.get("sale_order_id");
			if(objs.length > 0){
				this.setSale_order_id((Integer)objs[0]);
			}
		}
		if(!(map.get("sale_order_id") instanceof Boolean)&& map.get("sale_order_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("sale_order_id");
			if(objs.length > 1){
				this.setSale_order_id_text((String)objs[1]);
			}
		}
		if(!(map.get("sequence") instanceof Boolean)&& map.get("sequence")!=null){
			this.setSequence((Integer)map.get("sequence"));
		}
		if(!(map.get("state") instanceof Boolean)&& map.get("state")!=null){
			this.setState((String)map.get("state"));
		}
		if(!(map.get("taxes_id") instanceof Boolean)&& map.get("taxes_id")!=null){
			Object[] objs = (Object[])map.get("taxes_id");
			if(objs.length > 0){
				Integer[] taxes_id = Arrays.copyOfRange(objs,0,objs.length,Integer[].class);
				this.setTaxes_id(Arrays.toString(taxes_id));
			}
		}
		if(!(map.get("write_date") instanceof Boolean)&& map.get("write_date")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse((String)map.get("write_date"));
   			this.setWrite_date(new Timestamp(parse.getTime()));
		}
		if(!(map.get("write_uid") instanceof Boolean)&& map.get("write_uid")!=null){
			Object[] objs = (Object[])map.get("write_uid");
			if(objs.length > 0){
				this.setWrite_uid((Integer)objs[0]);
			}
		}
		if(!(map.get("write_uid") instanceof Boolean)&& map.get("write_uid")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("write_uid");
			if(objs.length > 1){
				this.setWrite_uid_text((String)objs[1]);
			}
		}
		if(!(map.get("__last_update") instanceof Boolean)&& map.get("__last_update")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse((String)map.get("__last_update"));
   			this.set__last_update(new Timestamp(parse.getTime()));
		}
	}

	public Map<String, Object> toMap() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		if(this.getAccount_analytic_id()!=null&&this.getAccount_analytic_idDirtyFlag()){
			map.put("account_analytic_id",this.getAccount_analytic_id());
		}else if(this.getAccount_analytic_idDirtyFlag()){
			map.put("account_analytic_id",false);
		}
		if(this.getAccount_analytic_id_text()!=null&&this.getAccount_analytic_id_textDirtyFlag()){
			//忽略文本外键account_analytic_id_text
		}else if(this.getAccount_analytic_id_textDirtyFlag()){
			map.put("account_analytic_id",false);
		}
		if(this.getAnalytic_tag_ids()!=null&&this.getAnalytic_tag_idsDirtyFlag()){
			map.put("analytic_tag_ids",this.getAnalytic_tag_ids());
		}else if(this.getAnalytic_tag_idsDirtyFlag()){
			map.put("analytic_tag_ids",false);
		}
		if(this.getCompany_id()!=null&&this.getCompany_idDirtyFlag()){
			map.put("company_id",this.getCompany_id());
		}else if(this.getCompany_idDirtyFlag()){
			map.put("company_id",false);
		}
		if(this.getCompany_id_text()!=null&&this.getCompany_id_textDirtyFlag()){
			//忽略文本外键company_id_text
		}else if(this.getCompany_id_textDirtyFlag()){
			map.put("company_id",false);
		}
		if(this.getCreate_date()!=null&&this.getCreate_dateDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String datetimeStr = sdf.format(this.getCreate_date());
			map.put("create_date",datetimeStr);
		}else if(this.getCreate_dateDirtyFlag()){
			map.put("create_date",false);
		}
		if(this.getCreate_uid()!=null&&this.getCreate_uidDirtyFlag()){
			map.put("create_uid",this.getCreate_uid());
		}else if(this.getCreate_uidDirtyFlag()){
			map.put("create_uid",false);
		}
		if(this.getCreate_uid_text()!=null&&this.getCreate_uid_textDirtyFlag()){
			//忽略文本外键create_uid_text
		}else if(this.getCreate_uid_textDirtyFlag()){
			map.put("create_uid",false);
		}
		if(this.getCurrency_id()!=null&&this.getCurrency_idDirtyFlag()){
			map.put("currency_id",this.getCurrency_id());
		}else if(this.getCurrency_idDirtyFlag()){
			map.put("currency_id",false);
		}
		if(this.getCurrency_id_text()!=null&&this.getCurrency_id_textDirtyFlag()){
			//忽略文本外键currency_id_text
		}else if(this.getCurrency_id_textDirtyFlag()){
			map.put("currency_id",false);
		}
		if(this.getDate_order()!=null&&this.getDate_orderDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String datetimeStr = sdf.format(this.getDate_order());
			map.put("date_order",datetimeStr);
		}else if(this.getDate_orderDirtyFlag()){
			map.put("date_order",false);
		}
		if(this.getDate_planned()!=null&&this.getDate_plannedDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String datetimeStr = sdf.format(this.getDate_planned());
			map.put("date_planned",datetimeStr);
		}else if(this.getDate_plannedDirtyFlag()){
			map.put("date_planned",false);
		}
		if(this.getDisplay_name()!=null&&this.getDisplay_nameDirtyFlag()){
			map.put("display_name",this.getDisplay_name());
		}else if(this.getDisplay_nameDirtyFlag()){
			map.put("display_name",false);
		}
		if(this.getId()!=null&&this.getIdDirtyFlag()){
			map.put("id",this.getId());
		}else if(this.getIdDirtyFlag()){
			map.put("id",false);
		}
		if(this.getInvoice_lines()!=null&&this.getInvoice_linesDirtyFlag()){
			map.put("invoice_lines",this.getInvoice_lines());
		}else if(this.getInvoice_linesDirtyFlag()){
			map.put("invoice_lines",false);
		}
		if(this.getMove_dest_ids()!=null&&this.getMove_dest_idsDirtyFlag()){
			map.put("move_dest_ids",this.getMove_dest_ids());
		}else if(this.getMove_dest_idsDirtyFlag()){
			map.put("move_dest_ids",false);
		}
		if(this.getMove_ids()!=null&&this.getMove_idsDirtyFlag()){
			map.put("move_ids",this.getMove_ids());
		}else if(this.getMove_idsDirtyFlag()){
			map.put("move_ids",false);
		}
		if(this.getName()!=null&&this.getNameDirtyFlag()){
			map.put("name",this.getName());
		}else if(this.getNameDirtyFlag()){
			map.put("name",false);
		}
		if(this.getOrderpoint_id()!=null&&this.getOrderpoint_idDirtyFlag()){
			map.put("orderpoint_id",this.getOrderpoint_id());
		}else if(this.getOrderpoint_idDirtyFlag()){
			map.put("orderpoint_id",false);
		}
		if(this.getOrderpoint_id_text()!=null&&this.getOrderpoint_id_textDirtyFlag()){
			//忽略文本外键orderpoint_id_text
		}else if(this.getOrderpoint_id_textDirtyFlag()){
			map.put("orderpoint_id",false);
		}
		if(this.getOrder_id()!=null&&this.getOrder_idDirtyFlag()){
			map.put("order_id",this.getOrder_id());
		}else if(this.getOrder_idDirtyFlag()){
			map.put("order_id",false);
		}
		if(this.getOrder_id_text()!=null&&this.getOrder_id_textDirtyFlag()){
			//忽略文本外键order_id_text
		}else if(this.getOrder_id_textDirtyFlag()){
			map.put("order_id",false);
		}
		if(this.getPartner_id()!=null&&this.getPartner_idDirtyFlag()){
			map.put("partner_id",this.getPartner_id());
		}else if(this.getPartner_idDirtyFlag()){
			map.put("partner_id",false);
		}
		if(this.getPartner_id_text()!=null&&this.getPartner_id_textDirtyFlag()){
			//忽略文本外键partner_id_text
		}else if(this.getPartner_id_textDirtyFlag()){
			map.put("partner_id",false);
		}
		if(this.getPrice_subtotal()!=null&&this.getPrice_subtotalDirtyFlag()){
			map.put("price_subtotal",this.getPrice_subtotal());
		}else if(this.getPrice_subtotalDirtyFlag()){
			map.put("price_subtotal",false);
		}
		if(this.getPrice_tax()!=null&&this.getPrice_taxDirtyFlag()){
			map.put("price_tax",this.getPrice_tax());
		}else if(this.getPrice_taxDirtyFlag()){
			map.put("price_tax",false);
		}
		if(this.getPrice_total()!=null&&this.getPrice_totalDirtyFlag()){
			map.put("price_total",this.getPrice_total());
		}else if(this.getPrice_totalDirtyFlag()){
			map.put("price_total",false);
		}
		if(this.getPrice_unit()!=null&&this.getPrice_unitDirtyFlag()){
			map.put("price_unit",this.getPrice_unit());
		}else if(this.getPrice_unitDirtyFlag()){
			map.put("price_unit",false);
		}
		if(this.getProduct_id()!=null&&this.getProduct_idDirtyFlag()){
			map.put("product_id",this.getProduct_id());
		}else if(this.getProduct_idDirtyFlag()){
			map.put("product_id",false);
		}
		if(this.getProduct_id_text()!=null&&this.getProduct_id_textDirtyFlag()){
			//忽略文本外键product_id_text
		}else if(this.getProduct_id_textDirtyFlag()){
			map.put("product_id",false);
		}
		if(this.getProduct_image()!=null&&this.getProduct_imageDirtyFlag()){
			//暂不支持binary类型product_image
		}else if(this.getProduct_imageDirtyFlag()){
			map.put("product_image",false);
		}
		if(this.getProduct_qty()!=null&&this.getProduct_qtyDirtyFlag()){
			map.put("product_qty",this.getProduct_qty());
		}else if(this.getProduct_qtyDirtyFlag()){
			map.put("product_qty",false);
		}
		if(this.getProduct_type()!=null&&this.getProduct_typeDirtyFlag()){
			map.put("product_type",this.getProduct_type());
		}else if(this.getProduct_typeDirtyFlag()){
			map.put("product_type",false);
		}
		if(this.getProduct_uom()!=null&&this.getProduct_uomDirtyFlag()){
			map.put("product_uom",this.getProduct_uom());
		}else if(this.getProduct_uomDirtyFlag()){
			map.put("product_uom",false);
		}
		if(this.getProduct_uom_qty()!=null&&this.getProduct_uom_qtyDirtyFlag()){
			map.put("product_uom_qty",this.getProduct_uom_qty());
		}else if(this.getProduct_uom_qtyDirtyFlag()){
			map.put("product_uom_qty",false);
		}
		if(this.getProduct_uom_text()!=null&&this.getProduct_uom_textDirtyFlag()){
			//忽略文本外键product_uom_text
		}else if(this.getProduct_uom_textDirtyFlag()){
			map.put("product_uom",false);
		}
		if(this.getQty_invoiced()!=null&&this.getQty_invoicedDirtyFlag()){
			map.put("qty_invoiced",this.getQty_invoiced());
		}else if(this.getQty_invoicedDirtyFlag()){
			map.put("qty_invoiced",false);
		}
		if(this.getQty_received()!=null&&this.getQty_receivedDirtyFlag()){
			map.put("qty_received",this.getQty_received());
		}else if(this.getQty_receivedDirtyFlag()){
			map.put("qty_received",false);
		}
		if(this.getSale_line_id()!=null&&this.getSale_line_idDirtyFlag()){
			map.put("sale_line_id",this.getSale_line_id());
		}else if(this.getSale_line_idDirtyFlag()){
			map.put("sale_line_id",false);
		}
		if(this.getSale_line_id_text()!=null&&this.getSale_line_id_textDirtyFlag()){
			//忽略文本外键sale_line_id_text
		}else if(this.getSale_line_id_textDirtyFlag()){
			map.put("sale_line_id",false);
		}
		if(this.getSale_order_id()!=null&&this.getSale_order_idDirtyFlag()){
			map.put("sale_order_id",this.getSale_order_id());
		}else if(this.getSale_order_idDirtyFlag()){
			map.put("sale_order_id",false);
		}
		if(this.getSale_order_id_text()!=null&&this.getSale_order_id_textDirtyFlag()){
			//忽略文本外键sale_order_id_text
		}else if(this.getSale_order_id_textDirtyFlag()){
			map.put("sale_order_id",false);
		}
		if(this.getSequence()!=null&&this.getSequenceDirtyFlag()){
			map.put("sequence",this.getSequence());
		}else if(this.getSequenceDirtyFlag()){
			map.put("sequence",false);
		}
		if(this.getState()!=null&&this.getStateDirtyFlag()){
			map.put("state",this.getState());
		}else if(this.getStateDirtyFlag()){
			map.put("state",false);
		}
		if(this.getTaxes_id()!=null&&this.getTaxes_idDirtyFlag()){
			map.put("taxes_id",this.getTaxes_id());
		}else if(this.getTaxes_idDirtyFlag()){
			map.put("taxes_id",false);
		}
		if(this.getWrite_date()!=null&&this.getWrite_dateDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String datetimeStr = sdf.format(this.getWrite_date());
			map.put("write_date",datetimeStr);
		}else if(this.getWrite_dateDirtyFlag()){
			map.put("write_date",false);
		}
		if(this.getWrite_uid()!=null&&this.getWrite_uidDirtyFlag()){
			map.put("write_uid",this.getWrite_uid());
		}else if(this.getWrite_uidDirtyFlag()){
			map.put("write_uid",false);
		}
		if(this.getWrite_uid_text()!=null&&this.getWrite_uid_textDirtyFlag()){
			//忽略文本外键write_uid_text
		}else if(this.getWrite_uid_textDirtyFlag()){
			map.put("write_uid",false);
		}
		if(this.get__last_update()!=null&&this.get__last_updateDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String datetimeStr = sdf.format(this.get__last_update());
			map.put("__last_update",datetimeStr);
		}else if(this.get__last_updateDirtyFlag()){
			map.put("__last_update",false);
		}
		return map;
	}

}
