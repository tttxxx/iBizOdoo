package cn.ibizlab.odoo.client.odoo_purchase.odooclient;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.core.client.model.Ipurchase_order_line;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[purchase_order_line] 服务对象客户端接口
 */
public interface Ipurchase_order_lineOdooClient {
    
        public void createBatch(Ipurchase_order_line purchase_order_line);

        public void updateBatch(Ipurchase_order_line purchase_order_line);

        public void remove(Ipurchase_order_line purchase_order_line);

        public void removeBatch(Ipurchase_order_line purchase_order_line);

        public void get(Ipurchase_order_line purchase_order_line);

        public void create(Ipurchase_order_line purchase_order_line);

        public Page<Ipurchase_order_line> search(SearchContext context);

        public void update(Ipurchase_order_line purchase_order_line);

        public List<Ipurchase_order_line> select();


}