package cn.ibizlab.odoo.client.odoo_purchase.odooclient;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.core.client.model.Ipurchase_order;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[purchase_order] 服务对象客户端接口
 */
public interface Ipurchase_orderOdooClient {
    
        public void remove(Ipurchase_order purchase_order);

        public Page<Ipurchase_order> search(SearchContext context);

        public void update(Ipurchase_order purchase_order);

        public void removeBatch(Ipurchase_order purchase_order);

        public void createBatch(Ipurchase_order purchase_order);

        public void updateBatch(Ipurchase_order purchase_order);

        public void get(Ipurchase_order purchase_order);

        public void create(Ipurchase_order purchase_order);

        public List<Ipurchase_order> select();


}