package cn.ibizlab.odoo.client.odoo_purchase.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Ipurchase_order_line;
import cn.ibizlab.odoo.client.odoo_purchase.model.purchase_order_lineImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[purchase_order_line] 服务对象接口
 */
public interface purchase_order_lineFeignClient {

    @RequestMapping(method = RequestMethod.POST, value = "/odoo_purchase/purchase_order_lines/createbatch")
    public purchase_order_lineImpl createBatch(@RequestBody List<purchase_order_lineImpl> purchase_order_lines);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_purchase/purchase_order_lines/updatebatch")
    public purchase_order_lineImpl updateBatch(@RequestBody List<purchase_order_lineImpl> purchase_order_lines);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_purchase/purchase_order_lines/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_purchase/purchase_order_lines/removebatch")
    public purchase_order_lineImpl removeBatch(@RequestBody List<purchase_order_lineImpl> purchase_order_lines);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_purchase/purchase_order_lines/{id}")
    public purchase_order_lineImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_purchase/purchase_order_lines")
    public purchase_order_lineImpl create(@RequestBody purchase_order_lineImpl purchase_order_line);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_purchase/purchase_order_lines/search")
    public Page<purchase_order_lineImpl> search(SearchContext context);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_purchase/purchase_order_lines/{id}")
    public purchase_order_lineImpl update(@PathVariable("id") Integer id,@RequestBody purchase_order_lineImpl purchase_order_line);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_purchase/purchase_order_lines/select")
    public Page<purchase_order_lineImpl> select();



}
