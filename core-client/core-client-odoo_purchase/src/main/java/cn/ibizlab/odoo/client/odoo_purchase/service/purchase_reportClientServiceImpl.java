package cn.ibizlab.odoo.client.odoo_purchase.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Ipurchase_report;
import cn.ibizlab.odoo.core.client.service.Ipurchase_reportClientService;
import cn.ibizlab.odoo.client.odoo_purchase.model.purchase_reportImpl;
import cn.ibizlab.odoo.client.odoo_purchase.odooclient.Ipurchase_reportOdooClient;
import cn.ibizlab.odoo.client.odoo_purchase.odooclient.impl.purchase_reportOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[purchase_report] 服务对象接口
 */
@Service
public class purchase_reportClientServiceImpl implements Ipurchase_reportClientService {
    @Autowired
    private  Ipurchase_reportOdooClient  purchase_reportOdooClient;

    public Ipurchase_report createModel() {		
		return new purchase_reportImpl();
	}


        public void removeBatch(List<Ipurchase_report> purchase_reports){
            
        }
        
        public void update(Ipurchase_report purchase_report){
this.purchase_reportOdooClient.update(purchase_report) ;
        }
        
        public void updateBatch(List<Ipurchase_report> purchase_reports){
            
        }
        
        public void get(Ipurchase_report purchase_report){
            this.purchase_reportOdooClient.get(purchase_report) ;
        }
        
        public void create(Ipurchase_report purchase_report){
this.purchase_reportOdooClient.create(purchase_report) ;
        }
        
        public void createBatch(List<Ipurchase_report> purchase_reports){
            
        }
        
        public Page<Ipurchase_report> search(SearchContext context){
            return this.purchase_reportOdooClient.search(context) ;
        }
        
        public void remove(Ipurchase_report purchase_report){
this.purchase_reportOdooClient.remove(purchase_report) ;
        }
        
        public Page<Ipurchase_report> select(SearchContext context){
            return null ;
        }
        

}

