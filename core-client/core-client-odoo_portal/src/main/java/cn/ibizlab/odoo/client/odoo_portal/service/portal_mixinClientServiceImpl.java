package cn.ibizlab.odoo.client.odoo_portal.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Iportal_mixin;
import cn.ibizlab.odoo.core.client.service.Iportal_mixinClientService;
import cn.ibizlab.odoo.client.odoo_portal.model.portal_mixinImpl;
import cn.ibizlab.odoo.client.odoo_portal.odooclient.Iportal_mixinOdooClient;
import cn.ibizlab.odoo.client.odoo_portal.odooclient.impl.portal_mixinOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[portal_mixin] 服务对象接口
 */
@Service
public class portal_mixinClientServiceImpl implements Iportal_mixinClientService {
    @Autowired
    private  Iportal_mixinOdooClient  portal_mixinOdooClient;

    public Iportal_mixin createModel() {		
		return new portal_mixinImpl();
	}


        public void create(Iportal_mixin portal_mixin){
this.portal_mixinOdooClient.create(portal_mixin) ;
        }
        
        public void createBatch(List<Iportal_mixin> portal_mixins){
            
        }
        
        public void update(Iportal_mixin portal_mixin){
this.portal_mixinOdooClient.update(portal_mixin) ;
        }
        
        public void remove(Iportal_mixin portal_mixin){
this.portal_mixinOdooClient.remove(portal_mixin) ;
        }
        
        public void updateBatch(List<Iportal_mixin> portal_mixins){
            
        }
        
        public void removeBatch(List<Iportal_mixin> portal_mixins){
            
        }
        
        public void get(Iportal_mixin portal_mixin){
            this.portal_mixinOdooClient.get(portal_mixin) ;
        }
        
        public Page<Iportal_mixin> search(SearchContext context){
            return this.portal_mixinOdooClient.search(context) ;
        }
        
        public Page<Iportal_mixin> select(SearchContext context){
            return null ;
        }
        

}

