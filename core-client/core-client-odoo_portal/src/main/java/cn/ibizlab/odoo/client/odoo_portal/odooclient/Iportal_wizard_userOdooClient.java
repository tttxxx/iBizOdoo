package cn.ibizlab.odoo.client.odoo_portal.odooclient;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.core.client.model.Iportal_wizard_user;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[portal_wizard_user] 服务对象客户端接口
 */
public interface Iportal_wizard_userOdooClient {
    
        public void get(Iportal_wizard_user portal_wizard_user);

        public void create(Iportal_wizard_user portal_wizard_user);

        public void createBatch(Iportal_wizard_user portal_wizard_user);

        public void updateBatch(Iportal_wizard_user portal_wizard_user);

        public Page<Iportal_wizard_user> search(SearchContext context);

        public void removeBatch(Iportal_wizard_user portal_wizard_user);

        public void remove(Iportal_wizard_user portal_wizard_user);

        public void update(Iportal_wizard_user portal_wizard_user);

        public List<Iportal_wizard_user> select();


}