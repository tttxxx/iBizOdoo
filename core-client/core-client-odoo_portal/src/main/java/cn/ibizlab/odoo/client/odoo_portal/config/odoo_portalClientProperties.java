package cn.ibizlab.odoo.client.odoo_portal.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import lombok.Data;

@ConfigurationProperties(prefix = "client.odoo.portal")
@Data
public class odoo_portalClientProperties {

	private String tokenUrl ;

	private String clientId ;

	private String clientSecret ;

}
