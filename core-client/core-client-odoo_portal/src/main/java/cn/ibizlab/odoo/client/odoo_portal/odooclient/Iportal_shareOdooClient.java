package cn.ibizlab.odoo.client.odoo_portal.odooclient;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.core.client.model.Iportal_share;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[portal_share] 服务对象客户端接口
 */
public interface Iportal_shareOdooClient {
    
        public void create(Iportal_share portal_share);

        public Page<Iportal_share> search(SearchContext context);

        public void get(Iportal_share portal_share);

        public void removeBatch(Iportal_share portal_share);

        public void updateBatch(Iportal_share portal_share);

        public void createBatch(Iportal_share portal_share);

        public void update(Iportal_share portal_share);

        public void remove(Iportal_share portal_share);

        public List<Iportal_share> select();


}