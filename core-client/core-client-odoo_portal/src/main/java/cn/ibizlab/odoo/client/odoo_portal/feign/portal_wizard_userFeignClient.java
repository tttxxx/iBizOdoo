package cn.ibizlab.odoo.client.odoo_portal.feign;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import cn.ibizlab.odoo.core.client.model.Iportal_wizard_user;
import cn.ibizlab.odoo.client.odoo_portal.model.portal_wizard_userImpl;
import cn.ibizlab.odoo.util.SearchContext;
/**
 * 实体[portal_wizard_user] 服务对象接口
 */
public interface portal_wizard_userFeignClient {

    @RequestMapping(method = RequestMethod.GET, value = "/odoo_portal/portal_wizard_users/{id}")
    public portal_wizard_userImpl get(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_portal/portal_wizard_users")
    public portal_wizard_userImpl create(@RequestBody portal_wizard_userImpl portal_wizard_user);


    @RequestMapping(method = RequestMethod.POST, value = "/odoo_portal/portal_wizard_users/createbatch")
    public portal_wizard_userImpl createBatch(@RequestBody List<portal_wizard_userImpl> portal_wizard_users);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_portal/portal_wizard_users/updatebatch")
    public portal_wizard_userImpl updateBatch(@RequestBody List<portal_wizard_userImpl> portal_wizard_users);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_portal/portal_wizard_users/search")
    public Page<portal_wizard_userImpl> search(SearchContext context);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_portal/portal_wizard_users/removebatch")
    public portal_wizard_userImpl removeBatch(@RequestBody List<portal_wizard_userImpl> portal_wizard_users);


    @RequestMapping(method = RequestMethod.DELETE, value = "/odoo_portal/portal_wizard_users/{id}")
    public Boolean remove(@PathVariable("id") Integer id);


    @RequestMapping(method = RequestMethod.PUT, value = "/odoo_portal/portal_wizard_users/{id}")
    public portal_wizard_userImpl update(@PathVariable("id") Integer id,@RequestBody portal_wizard_userImpl portal_wizard_user);


    @RequestMapping(method = RequestMethod.GET, value = "/odoo_portal/portal_wizard_users/select")
    public Page<portal_wizard_userImpl> select();



}
