package cn.ibizlab.odoo.client.odoo_portal.odooclient;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.core.client.model.Iportal_mixin;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[portal_mixin] 服务对象客户端接口
 */
public interface Iportal_mixinOdooClient {
    
        public void create(Iportal_mixin portal_mixin);

        public void createBatch(Iportal_mixin portal_mixin);

        public void update(Iportal_mixin portal_mixin);

        public void remove(Iportal_mixin portal_mixin);

        public void updateBatch(Iportal_mixin portal_mixin);

        public void removeBatch(Iportal_mixin portal_mixin);

        public void get(Iportal_mixin portal_mixin);

        public Page<Iportal_mixin> search(SearchContext context);

        public List<Iportal_mixin> select();


}