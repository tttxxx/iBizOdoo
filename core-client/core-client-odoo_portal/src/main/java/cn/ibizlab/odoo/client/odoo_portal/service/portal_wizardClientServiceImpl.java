package cn.ibizlab.odoo.client.odoo_portal.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import cn.ibizlab.odoo.core.client.model.Iportal_wizard;
import cn.ibizlab.odoo.core.client.service.Iportal_wizardClientService;
import cn.ibizlab.odoo.client.odoo_portal.model.portal_wizardImpl;
import cn.ibizlab.odoo.client.odoo_portal.odooclient.Iportal_wizardOdooClient;
import cn.ibizlab.odoo.client.odoo_portal.odooclient.impl.portal_wizardOdooClient;
import cn.ibizlab.odoo.util.SearchContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * 实体[portal_wizard] 服务对象接口
 */
@Service
public class portal_wizardClientServiceImpl implements Iportal_wizardClientService {
    @Autowired
    private  Iportal_wizardOdooClient  portal_wizardOdooClient;

    public Iportal_wizard createModel() {		
		return new portal_wizardImpl();
	}


        public void updateBatch(List<Iportal_wizard> portal_wizards){
            
        }
        
        public void createBatch(List<Iportal_wizard> portal_wizards){
            
        }
        
        public void removeBatch(List<Iportal_wizard> portal_wizards){
            
        }
        
        public void create(Iportal_wizard portal_wizard){
this.portal_wizardOdooClient.create(portal_wizard) ;
        }
        
        public void remove(Iportal_wizard portal_wizard){
this.portal_wizardOdooClient.remove(portal_wizard) ;
        }
        
        public void get(Iportal_wizard portal_wizard){
            this.portal_wizardOdooClient.get(portal_wizard) ;
        }
        
        public Page<Iportal_wizard> search(SearchContext context){
            return this.portal_wizardOdooClient.search(context) ;
        }
        
        public void update(Iportal_wizard portal_wizard){
this.portal_wizardOdooClient.update(portal_wizard) ;
        }
        
        public Page<Iportal_wizard> select(SearchContext context){
            return null ;
        }
        

}

