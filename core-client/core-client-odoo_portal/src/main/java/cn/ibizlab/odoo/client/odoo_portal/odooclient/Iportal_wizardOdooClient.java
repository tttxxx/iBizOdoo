package cn.ibizlab.odoo.client.odoo_portal.odooclient;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.core.client.model.Iportal_wizard;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[portal_wizard] 服务对象客户端接口
 */
public interface Iportal_wizardOdooClient {
    
        public void updateBatch(Iportal_wizard portal_wizard);

        public void createBatch(Iportal_wizard portal_wizard);

        public void removeBatch(Iportal_wizard portal_wizard);

        public void create(Iportal_wizard portal_wizard);

        public void remove(Iportal_wizard portal_wizard);

        public void get(Iportal_wizard portal_wizard);

        public Page<Iportal_wizard> search(SearchContext context);

        public void update(Iportal_wizard portal_wizard);

        public List<Iportal_wizard> select();


}