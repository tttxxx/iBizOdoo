package cn.ibizlab.odoo.client.odoo_board.odooclient;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import cn.ibizlab.odoo.core.client.model.Iboard_board;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[board_board] 服务对象客户端接口
 */
public interface Iboard_boardOdooClient {
    
        public void removeBatch(Iboard_board board_board);

        public void remove(Iboard_board board_board);

        public void create(Iboard_board board_board);

        public void update(Iboard_board board_board);

        public Page<Iboard_board> search(SearchContext context);

        public void createBatch(Iboard_board board_board);

        public void get(Iboard_board board_board);

        public void updateBatch(Iboard_board board_board);

        public List<Iboard_board> select();


}