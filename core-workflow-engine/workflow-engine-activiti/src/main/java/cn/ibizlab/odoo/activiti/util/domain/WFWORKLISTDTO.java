package cn.ibizlab.odoo.activiti.util.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
//import com.ibiz.core.base.valuerule.anno.wfworklist.*;
import org.springframework.cglib.beans.BeanCopier;
import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * 服务DTO对象[WFWORKLISTDTO]
 */
public class WFWORKLISTDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 属性 [WFWORKLISTID]
     *
     */
    private String WFWORKLISTId;

    @JsonIgnore
    private boolean WFWORKLISTIdDirtyFlag;

    /**
     * 属性 [WFWORKLISTNAME]
     *
     */
    private String WFWORKLISTName;

    @JsonIgnore
    private boolean WFWORKLISTNameDirtyFlag;

    /**
     * 属性 [UPDATEDATE]
     *
     */
    private Timestamp UpdateDate;

    @JsonIgnore
    private boolean UpdateDateDirtyFlag;

    /**
     * 属性 [CREATEDATE]
     *
     */
    private Timestamp CreateDate;

    @JsonIgnore
    private boolean CreateDateDirtyFlag;

    /**
     * 属性 [UPDATEMAN]
     *
     */
    private String UpdateMan;

    @JsonIgnore
    private boolean UpdateManDirtyFlag;

    /**
     * 属性 [CREATEMAN]
     *
     */
    private String CreateMan;

    @JsonIgnore
    private boolean CreateManDirtyFlag;

    /**
     * 属性 [WFENGINEWORKLISTID]
     *
     */
    private String Wfengineworklistid;

    @JsonIgnore
    private boolean WfengineworklistidDirtyFlag;

    /**
     * 属性 [WFINSTANCEID]
     *
     */
    private String WFINSTANCEId;

    @JsonIgnore
    private boolean WFINSTANCEIdDirtyFlag;

    /**
     * 属性 [WFUSERID]
     *
     */
    private String WFUSERId;

    @JsonIgnore
    private boolean WFUSERIdDirtyFlag;

    /**
     * 属性 [READ]
     *
     */
    private Integer Read;

    @JsonIgnore
    private boolean ReadDirtyFlag;

    /**
     * 属性 [LOGICNAME]
     *
     */
    private String Logicname;

    @JsonIgnore
    private boolean LogicnameDirtyFlag;

    /**
     * 属性 [ISCLOSED]
     *
     */
    private Integer Isclosed;

    @JsonIgnore
    private boolean IsclosedDirtyFlag;

    /**
     * 属性 [DONE]
     *
     */
    private Integer Done;

    @JsonIgnore
    private boolean DoneDirtyFlag;

    /**
     * 属性 [WFSTEP]
     *
     */
    private String WFstep;

    @JsonIgnore
    private boolean WFstepDirtyFlag;

    /**
     * 属性 [URGENCY]
     *
     */
    private String Urgency;

    @JsonIgnore
    private boolean UrgencyDirtyFlag;

    /**
     * 属性 [WORKDATE]
     *
     */
    private Timestamp Workdate;

    @JsonIgnore
    private boolean WorkdateDirtyFlag;

    /**
     * 属性 [INITIATOR]
     *
     */
    private String Initiator;

    @JsonIgnore
    private boolean InitiatorDirtyFlag;


    /**
     * 获取 [WFWORKLISTID]
     */
    public String getWFWORKLISTId(){
        return WFWORKLISTId ;
    }

    /**
     * 设置 [WFWORKLISTID]
     */
    public void setWFWORKLISTId(String  wFWORKLISTId){
        this.WFWORKLISTId = wFWORKLISTId ;
        this.WFWORKLISTIdDirtyFlag = true ;
    }

    /**
     * 获取 [WFWORKLISTID]脏标记
     */
    @JsonIgnore
    public boolean getWFWORKLISTIdDirtyFlag(){
        return WFWORKLISTIdDirtyFlag ;
    }

    /**
     * 获取 [WFWORKLISTNAME]
     */
    public String getWFWORKLISTName(){
        return WFWORKLISTName ;
    }

    /**
     * 设置 [WFWORKLISTNAME]
     */
    public void setWFWORKLISTName(String  wFWORKLISTName){
        this.WFWORKLISTName = wFWORKLISTName ;
        this.WFWORKLISTNameDirtyFlag = true ;
    }

    /**
     * 获取 [WFWORKLISTNAME]脏标记
     */
    @JsonIgnore
    public boolean getWFWORKLISTNameDirtyFlag(){
        return WFWORKLISTNameDirtyFlag ;
    }

    /**
     * 获取 [UPDATEDATE]
     */
    public Timestamp getUpdateDate(){
        return UpdateDate ;
    }

    /**
     * 设置 [UPDATEDATE]
     */
    public void setUpdateDate(Timestamp  updateDate){
        this.UpdateDate = updateDate ;
        this.UpdateDateDirtyFlag = true ;
    }

    /**
     * 获取 [UPDATEDATE]脏标记
     */
    @JsonIgnore
    public boolean getUpdateDateDirtyFlag(){
        return UpdateDateDirtyFlag ;
    }

    /**
     * 获取 [CREATEDATE]
     */
    public Timestamp getCreateDate(){
        return CreateDate ;
    }

    /**
     * 设置 [CREATEDATE]
     */
    public void setCreateDate(Timestamp  createDate){
        this.CreateDate = createDate ;
        this.CreateDateDirtyFlag = true ;
    }

    /**
     * 获取 [CREATEDATE]脏标记
     */
    @JsonIgnore
    public boolean getCreateDateDirtyFlag(){
        return CreateDateDirtyFlag ;
    }

    /**
     * 获取 [UPDATEMAN]
     */
    public String getUpdateMan(){
        return UpdateMan ;
    }

    /**
     * 设置 [UPDATEMAN]
     */
    public void setUpdateMan(String  updateMan){
        this.UpdateMan = updateMan ;
        this.UpdateManDirtyFlag = true ;
    }

    /**
     * 获取 [UPDATEMAN]脏标记
     */
    @JsonIgnore
    public boolean getUpdateManDirtyFlag(){
        return UpdateManDirtyFlag ;
    }

    /**
     * 获取 [CREATEMAN]
     */
    public String getCreateMan(){
        return CreateMan ;
    }

    /**
     * 设置 [CREATEMAN]
     */
    public void setCreateMan(String  createMan){
        this.CreateMan = createMan ;
        this.CreateManDirtyFlag = true ;
    }

    /**
     * 获取 [CREATEMAN]脏标记
     */
    @JsonIgnore
    public boolean getCreateManDirtyFlag(){
        return CreateManDirtyFlag ;
    }

    /**
     * 获取 [WFENGINEWORKLISTID]
     */
    public String getWfengineworklistid(){
        return Wfengineworklistid ;
    }

    /**
     * 设置 [WFENGINEWORKLISTID]
     */
    public void setWfengineworklistid(String  wfengineworklistid){
        this.Wfengineworklistid = wfengineworklistid ;
        this.WfengineworklistidDirtyFlag = true ;
    }

    /**
     * 获取 [WFENGINEWORKLISTID]脏标记
     */
    @JsonIgnore
    public boolean getWfengineworklistidDirtyFlag(){
        return WfengineworklistidDirtyFlag ;
    }

    /**
     * 获取 [WFINSTANCEID]
     */
    public String getWFINSTANCEId(){
        return WFINSTANCEId ;
    }

    /**
     * 设置 [WFINSTANCEID]
     */
    public void setWFINSTANCEId(String  wFINSTANCEId){
        this.WFINSTANCEId = wFINSTANCEId ;
        this.WFINSTANCEIdDirtyFlag = true ;
    }

    /**
     * 获取 [WFINSTANCEID]脏标记
     */
    @JsonIgnore
    public boolean getWFINSTANCEIdDirtyFlag(){
        return WFINSTANCEIdDirtyFlag ;
    }

    /**
     * 获取 [WFUSERID]
     */
    public String getWFUSERId(){
        return WFUSERId ;
    }

    /**
     * 设置 [WFUSERID]
     */
    public void setWFUSERId(String  wFUSERId){
        this.WFUSERId = wFUSERId ;
        this.WFUSERIdDirtyFlag = true ;
    }

    /**
     * 获取 [WFUSERID]脏标记
     */
    @JsonIgnore
    public boolean getWFUSERIdDirtyFlag(){
        return WFUSERIdDirtyFlag ;
    }

    /**
     * 获取 [READ]
     */
    public Integer getRead(){
        return Read ;
    }

    /**
     * 设置 [READ]
     */
    public void setRead(Integer  read){
        this.Read = read ;
        this.ReadDirtyFlag = true ;
    }

    /**
     * 获取 [READ]脏标记
     */
    @JsonIgnore
    public boolean getReadDirtyFlag(){
        return ReadDirtyFlag ;
    }

    /**
     * 获取 [LOGICNAME]
     */
    public String getLogicname(){
        return Logicname ;
    }

    /**
     * 设置 [LOGICNAME]
     */
    public void setLogicname(String  logicname){
        this.Logicname = logicname ;
        this.LogicnameDirtyFlag = true ;
    }

    /**
     * 获取 [LOGICNAME]脏标记
     */
    @JsonIgnore
    public boolean getLogicnameDirtyFlag(){
        return LogicnameDirtyFlag ;
    }

    /**
     * 获取 [ISCLOSED]
     */
    public Integer getIsclosed(){
        return Isclosed ;
    }

    /**
     * 设置 [ISCLOSED]
     */
    public void setIsclosed(Integer  isclosed){
        this.Isclosed = isclosed ;
        this.IsclosedDirtyFlag = true ;
    }

    /**
     * 获取 [ISCLOSED]脏标记
     */
    @JsonIgnore
    public boolean getIsclosedDirtyFlag(){
        return IsclosedDirtyFlag ;
    }

    /**
     * 获取 [DONE]
     */
    public Integer getDone(){
        return Done ;
    }

    /**
     * 设置 [DONE]
     */
    public void setDone(Integer  done){
        this.Done = done ;
        this.DoneDirtyFlag = true ;
    }

    /**
     * 获取 [DONE]脏标记
     */
    @JsonIgnore
    public boolean getDoneDirtyFlag(){
        return DoneDirtyFlag ;
    }

    /**
     * 获取 [WFSTEP]
     */
    public String getWFstep(){
        return WFstep ;
    }

    /**
     * 设置 [WFSTEP]
     */
    public void setWFstep(String  wFstep){
        this.WFstep = wFstep ;
        this.WFstepDirtyFlag = true ;
    }

    /**
     * 获取 [WFSTEP]脏标记
     */
    @JsonIgnore
    public boolean getWFstepDirtyFlag(){
        return WFstepDirtyFlag ;
    }

    /**
     * 获取 [URGENCY]
     */
    public String getUrgency(){
        return Urgency ;
    }

    /**
     * 设置 [URGENCY]
     */
    public void setUrgency(String  urgency){
        this.Urgency = urgency ;
        this.UrgencyDirtyFlag = true ;
    }

    /**
     * 获取 [URGENCY]脏标记
     */
    @JsonIgnore
    public boolean getUrgencyDirtyFlag(){
        return UrgencyDirtyFlag ;
    }

    /**
     * 获取 [WORKDATE]
     */
    public Timestamp getWorkdate(){
        return Workdate ;
    }

    /**
     * 设置 [WORKDATE]
     */
    public void setWorkdate(Timestamp  workdate){
        this.Workdate = workdate ;
        this.WorkdateDirtyFlag = true ;
    }

    /**
     * 获取 [WORKDATE]脏标记
     */
    @JsonIgnore
    public boolean getWorkdateDirtyFlag(){
        return WorkdateDirtyFlag ;
    }

    /**
     * 获取 [INITIATOR]
     */
    public String getInitiator(){
        return Initiator ;
    }

    /**
     * 设置 [INITIATOR]
     */
    public void setInitiator(String  initiator){
        this.Initiator = initiator ;
        this.InitiatorDirtyFlag = true ;
    }

    /**
     * 获取 [INITIATOR]脏标记
     */
    @JsonIgnore
    public boolean getInitiatorDirtyFlag(){
        return InitiatorDirtyFlag ;
    }




}
