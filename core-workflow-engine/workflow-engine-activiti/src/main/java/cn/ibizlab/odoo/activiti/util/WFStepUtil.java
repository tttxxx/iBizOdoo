package cn.ibizlab.odoo.activiti.util;

import org.activiti.engine.HistoryService;
import org.activiti.engine.RuntimeService;
import org.activiti.engine.TaskService;
import org.activiti.engine.history.HistoricActivityInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import java.util.*;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class WFStepUtil {


    @Autowired
    RuntimeService runtimeService;

    @Autowired
    TaskService taskService;

    @Autowired
    HistoryService historyService;


    /**
     * 获取流程上一步
     * @param instanceId
     * @return
     */
    public HistoricActivityInstance  getPreStep(String instanceId){

        HistoricActivityInstance historicActivityInstance=null;

        List<HistoricActivityInstance> historicActivityInstances = historyService.createHistoricActivityInstanceQuery()
                .activityType("userTask")
                .processInstanceId(instanceId)
                .finished()
                .orderByHistoricActivityInstanceEndTime()
                .desc()
                .list();

        if(historicActivityInstances.size()>0){
            historicActivityInstance=historicActivityInstances.get(0);
        }

        return historicActivityInstance;
    }

    /**
     * 获取当前流程步骤
     * @param instanceId
     * @return
     */
    public HistoricActivityInstance getCurStep(String instanceId){

        HistoricActivityInstance curActivityInstance=null;

        List<HistoricActivityInstance> historicActivityInstances = historyService.createHistoricActivityInstanceQuery()
                .activityType("userTask")
                .processInstanceId(instanceId)
                .orderByHistoricActivityInstanceStartTime()
                .desc()
                .list();

        if(historicActivityInstances.size()>0){
            curActivityInstance=historicActivityInstances.get(0);
        }

        return curActivityInstance;
    }
}
