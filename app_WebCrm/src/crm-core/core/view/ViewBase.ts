import { Vue, Prop, Watch } from 'vue-property-decorator';
import { Subject } from 'rxjs';

/**
 * 视图基类
 *
 * @export
 * @class ViewBase
 * @extends {Vue}
 */
export class ViewBase extends Vue {

    /**
     * 视图状态订阅对象
     *
     * @private
     * @type {Subject<{action: string, data: any}>}
     * @memberof ViewBase
     */
    protected viewState: Subject<ViewState> = new Subject();

    /**
     * 视图标识
     *
     * @type {string}
     * @memberof ViewBase
     */
    protected viewtag: string = '';

    /**
     * 当前视图上下文
     *
     * @type {*}
     * @memberof ViewBase
     */
    protected context: any = {};

    /**
     * 视图参数
     *
     * @type {*}
     * @memberof ViewBase
     */
    protected viewparams: any = {};

    /**
     * 视图默认引擎
     *
     * @protected
     * @type {*}
     * @memberof ViewBase
     */
    protected engine: any;

    /**
     * 视图打开模式
     *
     * @protected
     * @type {('DEFAULT' | 'MODAL')}
     * @memberof ViewBase
     */
    @Prop({ default: 'DEFAULT' })
    protected openMode!: 'DEFAULT' | 'MODAL';

    /**
     * 视图默认使用
     *
     * @type {boolean}
     * @memberof ViewBase
     */
    @Prop({ default: true })
    protected viewDefaultUsage!: boolean;

    /**
     * 数据视图
     *
     * @type {string}
     * @memberof ViewBase
     */
    @Prop()
    protected viewdata!: string;

    /**
     * 传入视图参数
     *
     * @protected
     * @type {*}
     * @memberof ViewBase
     */
    @Prop()
    protected viewparam: any;

    /**
     * 处理值变化
     *
     * @param {*} newVal
     * @param {*} oldVal
     * @memberof ViewBase
     */
    @Watch("viewdata")
    protected onViewData(newVal: any, oldVal: any) {
        if (!Object.is(newVal, oldVal) && this.engine) {
            this.parseViewParam();
            this.engine.load();
        }
    }

    /**
     * Vue声明周期
     *
     * @memberof ViewBase
     */
    public created() {
        const secondtag = this.$util.createUUID();
        this.$store.commit("viewaction/createdView", {
            viewtag: this.viewtag,
            secondtag: secondtag
        });
        this.viewtag = secondtag;
        this.parseViewParam();
    }

    /**
     * Vue声明周期(组件初始化完毕)
     *
     * @memberof ViewBase
     */
    public mounted() {
        this.engineInit();
        this.loadModel();
        this.$viewTool.setIndexViewParam(this.context);
        this.viewMounted();
    }

    /**
     * 视图组件挂载完毕
     *
     * @protected
     * @memberof ViewBase
     */
    protected viewMounted(): void { }

    /**
     * 销毁之前
     *
     * @memberof ViewBase
     */
    public beforeDestroy() {
        this.$store.commit("viewaction/removeView", this.viewtag);
    }

    /**
     * 是否为默认类型视图
     *
     * @protected
     * @returns {boolean}
     * @memberof ViewBase
     */
    protected isDefaultView(): boolean {
        return Object.is(this.openMode, 'DEFAULT');
    }

    /**
     * 是否为模态类型视图
     *
     * @protected
     * @returns {boolean}
     * @memberof ViewBase
     */
    protected isModalView(): boolean {
        return Object.is(this.openMode, 'MODAL');
    }

    /**
     * 引擎初始化
     *
     * @protected
     * @memberof ViewBase
     */
    protected engineInit(): void { }

    /**
     * 加载模型
     *
     * @protected
     * @returns {Promise<any>}
     * @memberof ViewBase
     */
    protected async loadModel(): Promise<any> { }

    /**
     * 解析视图参数
     *
     * @protected
     * @memberof ViewBase
     */
    protected parseViewParam(): void {
        if (
            !this.viewDefaultUsage &&
            this.viewdata &&
            !Object.is(this.viewdata, "")
        ) {
            Object.assign(this.context, JSON.parse(this.viewdata));
            if (this.context && this.context.srfparentdename) {
                Object.assign(this.viewparams, { srfparentdename: this.context.srfparentdename });
            }
            if (this.context && this.context.srfparentkey) {
                Object.assign(this.viewparams, { srfparentkey: this.context.srfparentkey });
            }
            return;
        }
        const path = this.$route.matched[this.$route.matched.length - 1].path;
        const keys: Array<any> = [];
        const curReg = this.$pathToRegExp.pathToRegexp(path, keys);
        const matchArray = curReg.exec(this.$route.path);
        let tempValue: Object = {};
        keys.forEach((item: any, index: number) => {
            Object.defineProperty(tempValue, item.name, {
                enumerable: true,
                value: matchArray[index + 1]
            });
        });
        this.$viewTool.formatRouteParams(tempValue, this.$route, this.context, this.viewparams);
        if (this.$store.getters.getAppData() && this.$store.getters.getAppData().context) {
            Object.assign(this.context, this.$store.getters.getAppData().context);
        }
    }

    /**
     * 关闭视图
     *
     * @param {any[]} args
     * @memberof IndexBase
     */
    public closeView(args: any[]): void {
        if (!this.viewDefaultUsage) {
            this.$emit("viewdataschange", args);
            this.$emit("close");
        } else {
            this.$router.back();
        }
    }

}