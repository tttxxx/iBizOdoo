package cn.ibizlab.odoo.odoo_portal.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_portal.dto.*;
import cn.ibizlab.odoo.odoo_portal.mapping.*;
import cn.ibizlab.odoo.core.odoo_portal.domain.Portal_share;
import cn.ibizlab.odoo.core.odoo_portal.service.IPortal_shareService;
import cn.ibizlab.odoo.core.odoo_portal.filter.Portal_shareSearchContext;




@Slf4j
@Api(tags = {"Portal_share" })
@RestController("odoo_portal-portal_share")
@RequestMapping("")
public class Portal_shareResource {

    @Autowired
    private IPortal_shareService portal_shareService;

    @Autowired
    @Lazy
    private Portal_shareMapping portal_shareMapping;




    @PreAuthorize("hasPermission(#portal_share_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Portal_share" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/portal_shares/{portal_share_id}")

    public ResponseEntity<Portal_shareDTO> update(@PathVariable("portal_share_id") Integer portal_share_id, @RequestBody Portal_shareDTO portal_sharedto) {
		Portal_share domain = portal_shareMapping.toDomain(portal_sharedto);
        domain.setId(portal_share_id);
		portal_shareService.update(domain);
		Portal_shareDTO dto = portal_shareMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#portal_share_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Portal_share" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/portal_shares/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Portal_shareDTO> portal_sharedtos) {
        portal_shareService.updateBatch(portal_shareMapping.toDomain(portal_sharedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }







    @PreAuthorize("hasPermission(#portal_share_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Portal_share" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/portal_shares/{portal_share_id}")
    public ResponseEntity<Portal_shareDTO> get(@PathVariable("portal_share_id") Integer portal_share_id) {
        Portal_share domain = portal_shareService.get(portal_share_id);
        Portal_shareDTO dto = portal_shareMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }







    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Portal_share" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/portal_shares")

    public ResponseEntity<Portal_shareDTO> create(@RequestBody Portal_shareDTO portal_sharedto) {
        Portal_share domain = portal_shareMapping.toDomain(portal_sharedto);
		portal_shareService.create(domain);
        Portal_shareDTO dto = portal_shareMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Portal_share" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/portal_shares/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Portal_shareDTO> portal_sharedtos) {
        portal_shareService.createBatch(portal_shareMapping.toDomain(portal_sharedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }







    @PreAuthorize("hasPermission('Remove',{#portal_share_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Portal_share" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/portal_shares/{portal_share_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("portal_share_id") Integer portal_share_id) {
         return ResponseEntity.status(HttpStatus.OK).body(portal_shareService.remove(portal_share_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Portal_share" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/portal_shares/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        portal_shareService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Portal_share" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/portal_shares/fetchdefault")
	public ResponseEntity<List<Portal_shareDTO>> fetchDefault(Portal_shareSearchContext context) {
        Page<Portal_share> domains = portal_shareService.searchDefault(context) ;
        List<Portal_shareDTO> list = portal_shareMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Portal_share" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/portal_shares/searchdefault")
	public ResponseEntity<Page<Portal_shareDTO>> searchDefault(Portal_shareSearchContext context) {
        Page<Portal_share> domains = portal_shareService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(portal_shareMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Portal_share getEntity(){
        return new Portal_share();
    }

}
