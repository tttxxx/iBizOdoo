package cn.ibizlab.odoo.odoo_portal.mapping;

import org.mapstruct.*;
import cn.ibizlab.odoo.core.odoo_portal.domain.Portal_mixin;
import cn.ibizlab.odoo.odoo_portal.dto.Portal_mixinDTO;
import cn.ibizlab.odoo.util.domain.MappingBase;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring", uses = {},
    nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
    nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface Portal_mixinMapping extends MappingBase<Portal_mixinDTO, Portal_mixin> {


}

