package cn.ibizlab.odoo.odoo_portal.mapping;

import org.mapstruct.*;
import cn.ibizlab.odoo.core.odoo_portal.domain.Portal_wizard_user;
import cn.ibizlab.odoo.odoo_portal.dto.Portal_wizard_userDTO;
import cn.ibizlab.odoo.util.domain.MappingBase;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring", uses = {},
    nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
    nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface Portal_wizard_userMapping extends MappingBase<Portal_wizard_userDTO, Portal_wizard_user> {


}

