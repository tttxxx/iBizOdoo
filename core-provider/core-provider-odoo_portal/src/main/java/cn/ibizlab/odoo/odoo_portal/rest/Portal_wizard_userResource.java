package cn.ibizlab.odoo.odoo_portal.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_portal.dto.*;
import cn.ibizlab.odoo.odoo_portal.mapping.*;
import cn.ibizlab.odoo.core.odoo_portal.domain.Portal_wizard_user;
import cn.ibizlab.odoo.core.odoo_portal.service.IPortal_wizard_userService;
import cn.ibizlab.odoo.core.odoo_portal.filter.Portal_wizard_userSearchContext;




@Slf4j
@Api(tags = {"Portal_wizard_user" })
@RestController("odoo_portal-portal_wizard_user")
@RequestMapping("")
public class Portal_wizard_userResource {

    @Autowired
    private IPortal_wizard_userService portal_wizard_userService;

    @Autowired
    @Lazy
    private Portal_wizard_userMapping portal_wizard_userMapping;




    @PreAuthorize("hasPermission('Remove',{#portal_wizard_user_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Portal_wizard_user" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/portal_wizard_users/{portal_wizard_user_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("portal_wizard_user_id") Integer portal_wizard_user_id) {
         return ResponseEntity.status(HttpStatus.OK).body(portal_wizard_userService.remove(portal_wizard_user_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Portal_wizard_user" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/portal_wizard_users/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        portal_wizard_userService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }













    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Portal_wizard_user" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/portal_wizard_users")

    public ResponseEntity<Portal_wizard_userDTO> create(@RequestBody Portal_wizard_userDTO portal_wizard_userdto) {
        Portal_wizard_user domain = portal_wizard_userMapping.toDomain(portal_wizard_userdto);
		portal_wizard_userService.create(domain);
        Portal_wizard_userDTO dto = portal_wizard_userMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Portal_wizard_user" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/portal_wizard_users/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Portal_wizard_userDTO> portal_wizard_userdtos) {
        portal_wizard_userService.createBatch(portal_wizard_userMapping.toDomain(portal_wizard_userdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission(#portal_wizard_user_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Portal_wizard_user" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/portal_wizard_users/{portal_wizard_user_id}")
    public ResponseEntity<Portal_wizard_userDTO> get(@PathVariable("portal_wizard_user_id") Integer portal_wizard_user_id) {
        Portal_wizard_user domain = portal_wizard_userService.get(portal_wizard_user_id);
        Portal_wizard_userDTO dto = portal_wizard_userMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }




    @PreAuthorize("hasPermission(#portal_wizard_user_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Portal_wizard_user" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/portal_wizard_users/{portal_wizard_user_id}")

    public ResponseEntity<Portal_wizard_userDTO> update(@PathVariable("portal_wizard_user_id") Integer portal_wizard_user_id, @RequestBody Portal_wizard_userDTO portal_wizard_userdto) {
		Portal_wizard_user domain = portal_wizard_userMapping.toDomain(portal_wizard_userdto);
        domain.setId(portal_wizard_user_id);
		portal_wizard_userService.update(domain);
		Portal_wizard_userDTO dto = portal_wizard_userMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#portal_wizard_user_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Portal_wizard_user" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/portal_wizard_users/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Portal_wizard_userDTO> portal_wizard_userdtos) {
        portal_wizard_userService.updateBatch(portal_wizard_userMapping.toDomain(portal_wizard_userdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Portal_wizard_user" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/portal_wizard_users/fetchdefault")
	public ResponseEntity<List<Portal_wizard_userDTO>> fetchDefault(Portal_wizard_userSearchContext context) {
        Page<Portal_wizard_user> domains = portal_wizard_userService.searchDefault(context) ;
        List<Portal_wizard_userDTO> list = portal_wizard_userMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Portal_wizard_user" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/portal_wizard_users/searchdefault")
	public ResponseEntity<Page<Portal_wizard_userDTO>> searchDefault(Portal_wizard_userSearchContext context) {
        Page<Portal_wizard_user> domains = portal_wizard_userService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(portal_wizard_userMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Portal_wizard_user getEntity(){
        return new Portal_wizard_user();
    }

}
