package cn.ibizlab.odoo.odoo_fetchmail.mapping;

import org.mapstruct.*;
import cn.ibizlab.odoo.core.odoo_fetchmail.domain.Fetchmail_server;
import cn.ibizlab.odoo.odoo_fetchmail.dto.Fetchmail_serverDTO;
import cn.ibizlab.odoo.util.domain.MappingBase;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring", uses = {},
    nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
    nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface Fetchmail_serverMapping extends MappingBase<Fetchmail_serverDTO, Fetchmail_server> {


}

