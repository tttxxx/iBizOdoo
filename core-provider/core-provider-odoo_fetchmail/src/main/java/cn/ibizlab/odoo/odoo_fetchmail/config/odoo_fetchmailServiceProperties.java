package cn.ibizlab.odoo.odoo_fetchmail.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import lombok.Data;

@ConfigurationProperties(prefix = "service.odoo-fetchmail")
@Data
public class odoo_fetchmailServiceProperties {

	private boolean enabled;

	private boolean auth;


}