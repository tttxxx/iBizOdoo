package cn.ibizlab.odoo.odoo_fetchmail.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;
import cn.ibizlab.odoo.util.domain.DTOBase;
import lombok.Data;

/**
 * 服务DTO对象[Fetchmail_serverDTO]
 */
@Data
public class Fetchmail_serverDTO extends DTOBase implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [USER]
     *
     */
    @JSONField(name = "user")
    @JsonProperty("user")
    private String user;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    private String displayName;

    /**
     * 属性 [SCRIPT]
     *
     */
    @JSONField(name = "script")
    @JsonProperty("script")
    private String script;

    /**
     * 属性 [CONFIGURATION]
     *
     */
    @JSONField(name = "configuration")
    @JsonProperty("configuration")
    private String configuration;

    /**
     * 属性 [DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("date")
    private Timestamp date;

    /**
     * 属性 [TYPE]
     *
     */
    @JSONField(name = "type")
    @JsonProperty("type")
    private String type;

    /**
     * 属性 [PASSWORD]
     *
     */
    @JSONField(name = "password")
    @JsonProperty("password")
    private String password;

    /**
     * 属性 [MESSAGE_IDS]
     *
     */
    @JSONField(name = "message_ids")
    @JsonProperty("message_ids")
    private String messageIds;

    /**
     * 属性 [CREATE_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;

    /**
     * 属性 [SERVER]
     *
     */
    @JSONField(name = "server")
    @JsonProperty("server")
    private String server;

    /**
     * 属性 [PORT]
     *
     */
    @JSONField(name = "port")
    @JsonProperty("port")
    private Integer port;

    /**
     * 属性 [IS_SSL]
     *
     */
    @JSONField(name = "is_ssl")
    @JsonProperty("is_ssl")
    private String isSsl;

    /**
     * 属性 [NAME]
     *
     */
    @JSONField(name = "name")
    @JsonProperty("name")
    private String name;

    /**
     * 属性 [PRIORITY]
     *
     */
    @JSONField(name = "priority")
    @JsonProperty("priority")
    private Integer priority;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;

    /**
     * 属性 [OBJECT_ID]
     *
     */
    @JSONField(name = "object_id")
    @JsonProperty("object_id")
    private Integer objectId;

    /**
     * 属性 [ID]
     *
     */
    @JSONField(name = "id")
    @JsonProperty("id")
    private Integer id;

    /**
     * 属性 [STATE]
     *
     */
    @JSONField(name = "state")
    @JsonProperty("state")
    private String state;

    /**
     * 属性 [ACTIVE]
     *
     */
    @JSONField(name = "active")
    @JsonProperty("active")
    private String active;

    /**
     * 属性 [WRITE_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;

    /**
     * 属性 [ATTACH]
     *
     */
    @JSONField(name = "attach")
    @JsonProperty("attach")
    private String attach;

    /**
     * 属性 [ORIGINAL]
     *
     */
    @JSONField(name = "original")
    @JsonProperty("original")
    private String original;

    /**
     * 属性 [CREATE_UID_TEXT]
     *
     */
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    private String createUidText;

    /**
     * 属性 [WRITE_UID_TEXT]
     *
     */
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    private String writeUidText;

    /**
     * 属性 [CREATE_UID]
     *
     */
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    private Integer createUid;

    /**
     * 属性 [WRITE_UID]
     *
     */
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    private Integer writeUid;


    /**
     * 设置 [USER]
     */
    public void setUser(String  user){
        this.user = user ;
        this.modify("user",user);
    }

    /**
     * 设置 [SCRIPT]
     */
    public void setScript(String  script){
        this.script = script ;
        this.modify("script",script);
    }

    /**
     * 设置 [CONFIGURATION]
     */
    public void setConfiguration(String  configuration){
        this.configuration = configuration ;
        this.modify("configuration",configuration);
    }

    /**
     * 设置 [DATE]
     */
    public void setDate(Timestamp  date){
        this.date = date ;
        this.modify("date",date);
    }

    /**
     * 设置 [TYPE]
     */
    public void setType(String  type){
        this.type = type ;
        this.modify("type",type);
    }

    /**
     * 设置 [PASSWORD]
     */
    public void setPassword(String  password){
        this.password = password ;
        this.modify("password",password);
    }

    /**
     * 设置 [SERVER]
     */
    public void setServer(String  server){
        this.server = server ;
        this.modify("server",server);
    }

    /**
     * 设置 [PORT]
     */
    public void setPort(Integer  port){
        this.port = port ;
        this.modify("port",port);
    }

    /**
     * 设置 [IS_SSL]
     */
    public void setIsSsl(String  isSsl){
        this.isSsl = isSsl ;
        this.modify("is_ssl",isSsl);
    }

    /**
     * 设置 [NAME]
     */
    public void setName(String  name){
        this.name = name ;
        this.modify("name",name);
    }

    /**
     * 设置 [PRIORITY]
     */
    public void setPriority(Integer  priority){
        this.priority = priority ;
        this.modify("priority",priority);
    }

    /**
     * 设置 [OBJECT_ID]
     */
    public void setObjectId(Integer  objectId){
        this.objectId = objectId ;
        this.modify("object_id",objectId);
    }

    /**
     * 设置 [STATE]
     */
    public void setState(String  state){
        this.state = state ;
        this.modify("state",state);
    }

    /**
     * 设置 [ACTIVE]
     */
    public void setActive(String  active){
        this.active = active ;
        this.modify("active",active);
    }

    /**
     * 设置 [ATTACH]
     */
    public void setAttach(String  attach){
        this.attach = attach ;
        this.modify("attach",attach);
    }

    /**
     * 设置 [ORIGINAL]
     */
    public void setOriginal(String  original){
        this.original = original ;
        this.modify("original",original);
    }


}

