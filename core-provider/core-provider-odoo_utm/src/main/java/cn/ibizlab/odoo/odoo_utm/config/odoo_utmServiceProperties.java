package cn.ibizlab.odoo.odoo_utm.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import lombok.Data;

@ConfigurationProperties(prefix = "service.odoo-utm")
@Data
public class odoo_utmServiceProperties {

	private boolean enabled;

	private boolean auth;


}