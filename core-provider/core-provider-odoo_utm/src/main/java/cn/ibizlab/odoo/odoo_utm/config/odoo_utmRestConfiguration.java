package cn.ibizlab.odoo.odoo_utm.config;

import org.springframework.context.annotation.ComponentScan;

@ComponentScan("cn.ibizlab.odoo.odoo_utm")
public class odoo_utmRestConfiguration {

}
