package cn.ibizlab.odoo.odoo_utm.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_utm.dto.*;
import cn.ibizlab.odoo.odoo_utm.mapping.*;
import cn.ibizlab.odoo.core.odoo_utm.domain.Utm_medium;
import cn.ibizlab.odoo.core.odoo_utm.service.IUtm_mediumService;
import cn.ibizlab.odoo.core.odoo_utm.filter.Utm_mediumSearchContext;




@Slf4j
@Api(tags = {"Utm_medium" })
@RestController("odoo_utm-utm_medium")
@RequestMapping("")
public class Utm_mediumResource {

    @Autowired
    private IUtm_mediumService utm_mediumService;

    @Autowired
    @Lazy
    private Utm_mediumMapping utm_mediumMapping;







    @PreAuthorize("hasPermission('Remove',{#utm_medium_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Utm_medium" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/utm_media/{utm_medium_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("utm_medium_id") Integer utm_medium_id) {
         return ResponseEntity.status(HttpStatus.OK).body(utm_mediumService.remove(utm_medium_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Utm_medium" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/utm_media/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        utm_mediumService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Utm_medium" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/utm_media")

    public ResponseEntity<Utm_mediumDTO> create(@RequestBody Utm_mediumDTO utm_mediumdto) {
        Utm_medium domain = utm_mediumMapping.toDomain(utm_mediumdto);
		utm_mediumService.create(domain);
        Utm_mediumDTO dto = utm_mediumMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Utm_medium" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/utm_media/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Utm_mediumDTO> utm_mediumdtos) {
        utm_mediumService.createBatch(utm_mediumMapping.toDomain(utm_mediumdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission(#utm_medium_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Utm_medium" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/utm_media/{utm_medium_id}")

    public ResponseEntity<Utm_mediumDTO> update(@PathVariable("utm_medium_id") Integer utm_medium_id, @RequestBody Utm_mediumDTO utm_mediumdto) {
		Utm_medium domain = utm_mediumMapping.toDomain(utm_mediumdto);
        domain.setId(utm_medium_id);
		utm_mediumService.update(domain);
		Utm_mediumDTO dto = utm_mediumMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#utm_medium_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Utm_medium" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/utm_media/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Utm_mediumDTO> utm_mediumdtos) {
        utm_mediumService.updateBatch(utm_mediumMapping.toDomain(utm_mediumdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission(#utm_medium_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Utm_medium" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/utm_media/{utm_medium_id}")
    public ResponseEntity<Utm_mediumDTO> get(@PathVariable("utm_medium_id") Integer utm_medium_id) {
        Utm_medium domain = utm_mediumService.get(utm_medium_id);
        Utm_mediumDTO dto = utm_mediumMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }







    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Utm_medium" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/utm_media/fetchdefault")
	public ResponseEntity<List<Utm_mediumDTO>> fetchDefault(Utm_mediumSearchContext context) {
        Page<Utm_medium> domains = utm_mediumService.searchDefault(context) ;
        List<Utm_mediumDTO> list = utm_mediumMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Utm_medium" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/utm_media/searchdefault")
	public ResponseEntity<Page<Utm_mediumDTO>> searchDefault(Utm_mediumSearchContext context) {
        Page<Utm_medium> domains = utm_mediumService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(utm_mediumMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Utm_medium getEntity(){
        return new Utm_medium();
    }

}
