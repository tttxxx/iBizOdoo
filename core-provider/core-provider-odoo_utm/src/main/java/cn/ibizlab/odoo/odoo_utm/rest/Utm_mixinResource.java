package cn.ibizlab.odoo.odoo_utm.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_utm.dto.*;
import cn.ibizlab.odoo.odoo_utm.mapping.*;
import cn.ibizlab.odoo.core.odoo_utm.domain.Utm_mixin;
import cn.ibizlab.odoo.core.odoo_utm.service.IUtm_mixinService;
import cn.ibizlab.odoo.core.odoo_utm.filter.Utm_mixinSearchContext;




@Slf4j
@Api(tags = {"Utm_mixin" })
@RestController("odoo_utm-utm_mixin")
@RequestMapping("")
public class Utm_mixinResource {

    @Autowired
    private IUtm_mixinService utm_mixinService;

    @Autowired
    @Lazy
    private Utm_mixinMapping utm_mixinMapping;




    @PreAuthorize("hasPermission(#utm_mixin_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Utm_mixin" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/utm_mixins/{utm_mixin_id}")

    public ResponseEntity<Utm_mixinDTO> update(@PathVariable("utm_mixin_id") Integer utm_mixin_id, @RequestBody Utm_mixinDTO utm_mixindto) {
		Utm_mixin domain = utm_mixinMapping.toDomain(utm_mixindto);
        domain.setId(utm_mixin_id);
		utm_mixinService.update(domain);
		Utm_mixinDTO dto = utm_mixinMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#utm_mixin_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Utm_mixin" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/utm_mixins/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Utm_mixinDTO> utm_mixindtos) {
        utm_mixinService.updateBatch(utm_mixinMapping.toDomain(utm_mixindtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission(#utm_mixin_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Utm_mixin" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/utm_mixins/{utm_mixin_id}")
    public ResponseEntity<Utm_mixinDTO> get(@PathVariable("utm_mixin_id") Integer utm_mixin_id) {
        Utm_mixin domain = utm_mixinService.get(utm_mixin_id);
        Utm_mixinDTO dto = utm_mixinMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }




    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Utm_mixin" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/utm_mixins")

    public ResponseEntity<Utm_mixinDTO> create(@RequestBody Utm_mixinDTO utm_mixindto) {
        Utm_mixin domain = utm_mixinMapping.toDomain(utm_mixindto);
		utm_mixinService.create(domain);
        Utm_mixinDTO dto = utm_mixinMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Utm_mixin" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/utm_mixins/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Utm_mixinDTO> utm_mixindtos) {
        utm_mixinService.createBatch(utm_mixinMapping.toDomain(utm_mixindtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }







    @PreAuthorize("hasPermission('Remove',{#utm_mixin_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Utm_mixin" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/utm_mixins/{utm_mixin_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("utm_mixin_id") Integer utm_mixin_id) {
         return ResponseEntity.status(HttpStatus.OK).body(utm_mixinService.remove(utm_mixin_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Utm_mixin" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/utm_mixins/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        utm_mixinService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }







    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Utm_mixin" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/utm_mixins/fetchdefault")
	public ResponseEntity<List<Utm_mixinDTO>> fetchDefault(Utm_mixinSearchContext context) {
        Page<Utm_mixin> domains = utm_mixinService.searchDefault(context) ;
        List<Utm_mixinDTO> list = utm_mixinMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Utm_mixin" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/utm_mixins/searchdefault")
	public ResponseEntity<Page<Utm_mixinDTO>> searchDefault(Utm_mixinSearchContext context) {
        Page<Utm_mixin> domains = utm_mixinService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(utm_mixinMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Utm_mixin getEntity(){
        return new Utm_mixin();
    }

}
