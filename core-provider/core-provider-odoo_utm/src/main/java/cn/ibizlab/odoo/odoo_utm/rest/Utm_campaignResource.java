package cn.ibizlab.odoo.odoo_utm.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_utm.dto.*;
import cn.ibizlab.odoo.odoo_utm.mapping.*;
import cn.ibizlab.odoo.core.odoo_utm.domain.Utm_campaign;
import cn.ibizlab.odoo.core.odoo_utm.service.IUtm_campaignService;
import cn.ibizlab.odoo.core.odoo_utm.filter.Utm_campaignSearchContext;




@Slf4j
@Api(tags = {"Utm_campaign" })
@RestController("odoo_utm-utm_campaign")
@RequestMapping("")
public class Utm_campaignResource {

    @Autowired
    private IUtm_campaignService utm_campaignService;

    @Autowired
    @Lazy
    private Utm_campaignMapping utm_campaignMapping;




    @PreAuthorize("hasPermission(#utm_campaign_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Utm_campaign" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/utm_campaigns/{utm_campaign_id}")

    public ResponseEntity<Utm_campaignDTO> update(@PathVariable("utm_campaign_id") Integer utm_campaign_id, @RequestBody Utm_campaignDTO utm_campaigndto) {
		Utm_campaign domain = utm_campaignMapping.toDomain(utm_campaigndto);
        domain.setId(utm_campaign_id);
		utm_campaignService.update(domain);
		Utm_campaignDTO dto = utm_campaignMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#utm_campaign_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Utm_campaign" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/utm_campaigns/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Utm_campaignDTO> utm_campaigndtos) {
        utm_campaignService.updateBatch(utm_campaignMapping.toDomain(utm_campaigndtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission(#utm_campaign_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Utm_campaign" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/utm_campaigns/{utm_campaign_id}")
    public ResponseEntity<Utm_campaignDTO> get(@PathVariable("utm_campaign_id") Integer utm_campaign_id) {
        Utm_campaign domain = utm_campaignService.get(utm_campaign_id);
        Utm_campaignDTO dto = utm_campaignMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }




    @PreAuthorize("hasPermission('Remove',{#utm_campaign_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Utm_campaign" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/utm_campaigns/{utm_campaign_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("utm_campaign_id") Integer utm_campaign_id) {
         return ResponseEntity.status(HttpStatus.OK).body(utm_campaignService.remove(utm_campaign_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Utm_campaign" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/utm_campaigns/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        utm_campaignService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Utm_campaign" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/utm_campaigns")

    public ResponseEntity<Utm_campaignDTO> create(@RequestBody Utm_campaignDTO utm_campaigndto) {
        Utm_campaign domain = utm_campaignMapping.toDomain(utm_campaigndto);
		utm_campaignService.create(domain);
        Utm_campaignDTO dto = utm_campaignMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Utm_campaign" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/utm_campaigns/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Utm_campaignDTO> utm_campaigndtos) {
        utm_campaignService.createBatch(utm_campaignMapping.toDomain(utm_campaigndtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }










    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Utm_campaign" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/utm_campaigns/fetchdefault")
	public ResponseEntity<List<Utm_campaignDTO>> fetchDefault(Utm_campaignSearchContext context) {
        Page<Utm_campaign> domains = utm_campaignService.searchDefault(context) ;
        List<Utm_campaignDTO> list = utm_campaignMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Utm_campaign" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/utm_campaigns/searchdefault")
	public ResponseEntity<Page<Utm_campaignDTO>> searchDefault(Utm_campaignSearchContext context) {
        Page<Utm_campaign> domains = utm_campaignService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(utm_campaignMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Utm_campaign getEntity(){
        return new Utm_campaign();
    }

}
