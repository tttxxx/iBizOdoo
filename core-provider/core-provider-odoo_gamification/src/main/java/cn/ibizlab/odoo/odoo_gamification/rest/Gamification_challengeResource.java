package cn.ibizlab.odoo.odoo_gamification.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_gamification.dto.*;
import cn.ibizlab.odoo.odoo_gamification.mapping.*;
import cn.ibizlab.odoo.core.odoo_gamification.domain.Gamification_challenge;
import cn.ibizlab.odoo.core.odoo_gamification.service.IGamification_challengeService;
import cn.ibizlab.odoo.core.odoo_gamification.filter.Gamification_challengeSearchContext;




@Slf4j
@Api(tags = {"Gamification_challenge" })
@RestController("odoo_gamification-gamification_challenge")
@RequestMapping("")
public class Gamification_challengeResource {

    @Autowired
    private IGamification_challengeService gamification_challengeService;

    @Autowired
    @Lazy
    private Gamification_challengeMapping gamification_challengeMapping;




    @PreAuthorize("hasPermission('Remove',{#gamification_challenge_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Gamification_challenge" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/gamification_challenges/{gamification_challenge_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("gamification_challenge_id") Integer gamification_challenge_id) {
         return ResponseEntity.status(HttpStatus.OK).body(gamification_challengeService.remove(gamification_challenge_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Gamification_challenge" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/gamification_challenges/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        gamification_challengeService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }







    @PreAuthorize("hasPermission(#gamification_challenge_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Gamification_challenge" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/gamification_challenges/{gamification_challenge_id}")
    public ResponseEntity<Gamification_challengeDTO> get(@PathVariable("gamification_challenge_id") Integer gamification_challenge_id) {
        Gamification_challenge domain = gamification_challengeService.get(gamification_challenge_id);
        Gamification_challengeDTO dto = gamification_challengeMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }







    @PreAuthorize("hasPermission(#gamification_challenge_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Gamification_challenge" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/gamification_challenges/{gamification_challenge_id}")

    public ResponseEntity<Gamification_challengeDTO> update(@PathVariable("gamification_challenge_id") Integer gamification_challenge_id, @RequestBody Gamification_challengeDTO gamification_challengedto) {
		Gamification_challenge domain = gamification_challengeMapping.toDomain(gamification_challengedto);
        domain.setId(gamification_challenge_id);
		gamification_challengeService.update(domain);
		Gamification_challengeDTO dto = gamification_challengeMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#gamification_challenge_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Gamification_challenge" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/gamification_challenges/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Gamification_challengeDTO> gamification_challengedtos) {
        gamification_challengeService.updateBatch(gamification_challengeMapping.toDomain(gamification_challengedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Gamification_challenge" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/gamification_challenges")

    public ResponseEntity<Gamification_challengeDTO> create(@RequestBody Gamification_challengeDTO gamification_challengedto) {
        Gamification_challenge domain = gamification_challengeMapping.toDomain(gamification_challengedto);
		gamification_challengeService.create(domain);
        Gamification_challengeDTO dto = gamification_challengeMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Gamification_challenge" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/gamification_challenges/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Gamification_challengeDTO> gamification_challengedtos) {
        gamification_challengeService.createBatch(gamification_challengeMapping.toDomain(gamification_challengedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Gamification_challenge" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/gamification_challenges/fetchdefault")
	public ResponseEntity<List<Gamification_challengeDTO>> fetchDefault(Gamification_challengeSearchContext context) {
        Page<Gamification_challenge> domains = gamification_challengeService.searchDefault(context) ;
        List<Gamification_challengeDTO> list = gamification_challengeMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Gamification_challenge" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/gamification_challenges/searchdefault")
	public ResponseEntity<Page<Gamification_challengeDTO>> searchDefault(Gamification_challengeSearchContext context) {
        Page<Gamification_challenge> domains = gamification_challengeService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(gamification_challengeMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Gamification_challenge getEntity(){
        return new Gamification_challenge();
    }

}
