package cn.ibizlab.odoo.odoo_gamification.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_gamification.dto.*;
import cn.ibizlab.odoo.odoo_gamification.mapping.*;
import cn.ibizlab.odoo.core.odoo_gamification.domain.Gamification_goal_definition;
import cn.ibizlab.odoo.core.odoo_gamification.service.IGamification_goal_definitionService;
import cn.ibizlab.odoo.core.odoo_gamification.filter.Gamification_goal_definitionSearchContext;




@Slf4j
@Api(tags = {"Gamification_goal_definition" })
@RestController("odoo_gamification-gamification_goal_definition")
@RequestMapping("")
public class Gamification_goal_definitionResource {

    @Autowired
    private IGamification_goal_definitionService gamification_goal_definitionService;

    @Autowired
    @Lazy
    private Gamification_goal_definitionMapping gamification_goal_definitionMapping;




    @PreAuthorize("hasPermission(#gamification_goal_definition_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Gamification_goal_definition" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/gamification_goal_definitions/{gamification_goal_definition_id}")
    public ResponseEntity<Gamification_goal_definitionDTO> get(@PathVariable("gamification_goal_definition_id") Integer gamification_goal_definition_id) {
        Gamification_goal_definition domain = gamification_goal_definitionService.get(gamification_goal_definition_id);
        Gamification_goal_definitionDTO dto = gamification_goal_definitionMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }




    @PreAuthorize("hasPermission(#gamification_goal_definition_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Gamification_goal_definition" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/gamification_goal_definitions/{gamification_goal_definition_id}")

    public ResponseEntity<Gamification_goal_definitionDTO> update(@PathVariable("gamification_goal_definition_id") Integer gamification_goal_definition_id, @RequestBody Gamification_goal_definitionDTO gamification_goal_definitiondto) {
		Gamification_goal_definition domain = gamification_goal_definitionMapping.toDomain(gamification_goal_definitiondto);
        domain.setId(gamification_goal_definition_id);
		gamification_goal_definitionService.update(domain);
		Gamification_goal_definitionDTO dto = gamification_goal_definitionMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#gamification_goal_definition_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Gamification_goal_definition" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/gamification_goal_definitions/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Gamification_goal_definitionDTO> gamification_goal_definitiondtos) {
        gamification_goal_definitionService.updateBatch(gamification_goal_definitionMapping.toDomain(gamification_goal_definitiondtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Gamification_goal_definition" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/gamification_goal_definitions")

    public ResponseEntity<Gamification_goal_definitionDTO> create(@RequestBody Gamification_goal_definitionDTO gamification_goal_definitiondto) {
        Gamification_goal_definition domain = gamification_goal_definitionMapping.toDomain(gamification_goal_definitiondto);
		gamification_goal_definitionService.create(domain);
        Gamification_goal_definitionDTO dto = gamification_goal_definitionMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Gamification_goal_definition" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/gamification_goal_definitions/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Gamification_goal_definitionDTO> gamification_goal_definitiondtos) {
        gamification_goal_definitionService.createBatch(gamification_goal_definitionMapping.toDomain(gamification_goal_definitiondtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }







    @PreAuthorize("hasPermission('Remove',{#gamification_goal_definition_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Gamification_goal_definition" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/gamification_goal_definitions/{gamification_goal_definition_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("gamification_goal_definition_id") Integer gamification_goal_definition_id) {
         return ResponseEntity.status(HttpStatus.OK).body(gamification_goal_definitionService.remove(gamification_goal_definition_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Gamification_goal_definition" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/gamification_goal_definitions/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        gamification_goal_definitionService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }







    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Gamification_goal_definition" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/gamification_goal_definitions/fetchdefault")
	public ResponseEntity<List<Gamification_goal_definitionDTO>> fetchDefault(Gamification_goal_definitionSearchContext context) {
        Page<Gamification_goal_definition> domains = gamification_goal_definitionService.searchDefault(context) ;
        List<Gamification_goal_definitionDTO> list = gamification_goal_definitionMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Gamification_goal_definition" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/gamification_goal_definitions/searchdefault")
	public ResponseEntity<Page<Gamification_goal_definitionDTO>> searchDefault(Gamification_goal_definitionSearchContext context) {
        Page<Gamification_goal_definition> domains = gamification_goal_definitionService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(gamification_goal_definitionMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Gamification_goal_definition getEntity(){
        return new Gamification_goal_definition();
    }

}
