package cn.ibizlab.odoo.odoo_gamification.mapping;

import org.mapstruct.*;
import cn.ibizlab.odoo.core.odoo_gamification.domain.Gamification_goal_wizard;
import cn.ibizlab.odoo.odoo_gamification.dto.Gamification_goal_wizardDTO;
import cn.ibizlab.odoo.util.domain.MappingBase;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring", uses = {},
    nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
    nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface Gamification_goal_wizardMapping extends MappingBase<Gamification_goal_wizardDTO, Gamification_goal_wizard> {


}

