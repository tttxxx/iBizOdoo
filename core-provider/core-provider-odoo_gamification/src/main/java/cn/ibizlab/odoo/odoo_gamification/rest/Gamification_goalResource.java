package cn.ibizlab.odoo.odoo_gamification.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_gamification.dto.*;
import cn.ibizlab.odoo.odoo_gamification.mapping.*;
import cn.ibizlab.odoo.core.odoo_gamification.domain.Gamification_goal;
import cn.ibizlab.odoo.core.odoo_gamification.service.IGamification_goalService;
import cn.ibizlab.odoo.core.odoo_gamification.filter.Gamification_goalSearchContext;




@Slf4j
@Api(tags = {"Gamification_goal" })
@RestController("odoo_gamification-gamification_goal")
@RequestMapping("")
public class Gamification_goalResource {

    @Autowired
    private IGamification_goalService gamification_goalService;

    @Autowired
    @Lazy
    private Gamification_goalMapping gamification_goalMapping;




    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Gamification_goal" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/gamification_goals")

    public ResponseEntity<Gamification_goalDTO> create(@RequestBody Gamification_goalDTO gamification_goaldto) {
        Gamification_goal domain = gamification_goalMapping.toDomain(gamification_goaldto);
		gamification_goalService.create(domain);
        Gamification_goalDTO dto = gamification_goalMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Gamification_goal" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/gamification_goals/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Gamification_goalDTO> gamification_goaldtos) {
        gamification_goalService.createBatch(gamification_goalMapping.toDomain(gamification_goaldtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission('Remove',{#gamification_goal_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Gamification_goal" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/gamification_goals/{gamification_goal_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("gamification_goal_id") Integer gamification_goal_id) {
         return ResponseEntity.status(HttpStatus.OK).body(gamification_goalService.remove(gamification_goal_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Gamification_goal" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/gamification_goals/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        gamification_goalService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }







    @PreAuthorize("hasPermission(#gamification_goal_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Gamification_goal" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/gamification_goals/{gamification_goal_id}")

    public ResponseEntity<Gamification_goalDTO> update(@PathVariable("gamification_goal_id") Integer gamification_goal_id, @RequestBody Gamification_goalDTO gamification_goaldto) {
		Gamification_goal domain = gamification_goalMapping.toDomain(gamification_goaldto);
        domain.setId(gamification_goal_id);
		gamification_goalService.update(domain);
		Gamification_goalDTO dto = gamification_goalMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#gamification_goal_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Gamification_goal" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/gamification_goals/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Gamification_goalDTO> gamification_goaldtos) {
        gamification_goalService.updateBatch(gamification_goalMapping.toDomain(gamification_goaldtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }







    @PreAuthorize("hasPermission(#gamification_goal_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Gamification_goal" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/gamification_goals/{gamification_goal_id}")
    public ResponseEntity<Gamification_goalDTO> get(@PathVariable("gamification_goal_id") Integer gamification_goal_id) {
        Gamification_goal domain = gamification_goalService.get(gamification_goal_id);
        Gamification_goalDTO dto = gamification_goalMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }




    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Gamification_goal" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/gamification_goals/fetchdefault")
	public ResponseEntity<List<Gamification_goalDTO>> fetchDefault(Gamification_goalSearchContext context) {
        Page<Gamification_goal> domains = gamification_goalService.searchDefault(context) ;
        List<Gamification_goalDTO> list = gamification_goalMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Gamification_goal" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/gamification_goals/searchdefault")
	public ResponseEntity<Page<Gamification_goalDTO>> searchDefault(Gamification_goalSearchContext context) {
        Page<Gamification_goal> domains = gamification_goalService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(gamification_goalMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Gamification_goal getEntity(){
        return new Gamification_goal();
    }

}
