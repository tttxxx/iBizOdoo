package cn.ibizlab.odoo.odoo_gamification.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;
import cn.ibizlab.odoo.util.domain.DTOBase;
import lombok.Data;

/**
 * 服务DTO对象[Gamification_challengeDTO]
 */
@Data
public class Gamification_challengeDTO extends DTOBase implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [WEBSITE_MESSAGE_IDS]
     *
     */
    @JSONField(name = "website_message_ids")
    @JsonProperty("website_message_ids")
    private String websiteMessageIds;

    /**
     * 属性 [NAME]
     *
     */
    @JSONField(name = "name")
    @JsonProperty("name")
    private String name;

    /**
     * 属性 [MESSAGE_NEEDACTION]
     *
     */
    @JSONField(name = "message_needaction")
    @JsonProperty("message_needaction")
    private String messageNeedaction;

    /**
     * 属性 [ID]
     *
     */
    @JSONField(name = "id")
    @JsonProperty("id")
    private Integer id;

    /**
     * 属性 [CREATE_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;

    /**
     * 属性 [MESSAGE_UNREAD]
     *
     */
    @JSONField(name = "message_unread")
    @JsonProperty("message_unread")
    private String messageUnread;

    /**
     * 属性 [START_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "start_date" , format="yyyy-MM-dd")
    @JsonProperty("start_date")
    private Timestamp startDate;

    /**
     * 属性 [MESSAGE_HAS_ERROR_COUNTER]
     *
     */
    @JSONField(name = "message_has_error_counter")
    @JsonProperty("message_has_error_counter")
    private Integer messageHasErrorCounter;

    /**
     * 属性 [MESSAGE_MAIN_ATTACHMENT_ID]
     *
     */
    @JSONField(name = "message_main_attachment_id")
    @JsonProperty("message_main_attachment_id")
    private Integer messageMainAttachmentId;

    /**
     * 属性 [MESSAGE_HAS_ERROR]
     *
     */
    @JSONField(name = "message_has_error")
    @JsonProperty("message_has_error")
    private String messageHasError;

    /**
     * 属性 [LAST_REPORT_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "last_report_date" , format="yyyy-MM-dd")
    @JsonProperty("last_report_date")
    private Timestamp lastReportDate;

    /**
     * 属性 [END_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "end_date" , format="yyyy-MM-dd")
    @JsonProperty("end_date")
    private Timestamp endDate;

    /**
     * 属性 [MESSAGE_FOLLOWER_IDS]
     *
     */
    @JSONField(name = "message_follower_ids")
    @JsonProperty("message_follower_ids")
    private String messageFollowerIds;

    /**
     * 属性 [MESSAGE_ATTACHMENT_COUNT]
     *
     */
    @JSONField(name = "message_attachment_count")
    @JsonProperty("message_attachment_count")
    private Integer messageAttachmentCount;

    /**
     * 属性 [MESSAGE_IS_FOLLOWER]
     *
     */
    @JSONField(name = "message_is_follower")
    @JsonProperty("message_is_follower")
    private String messageIsFollower;

    /**
     * 属性 [USER_DOMAIN]
     *
     */
    @JSONField(name = "user_domain")
    @JsonProperty("user_domain")
    private String userDomain;

    /**
     * 属性 [CATEGORY]
     *
     */
    @JSONField(name = "category")
    @JsonProperty("category")
    private String category;

    /**
     * 属性 [DESCRIPTION]
     *
     */
    @JSONField(name = "description")
    @JsonProperty("description")
    private String description;

    /**
     * 属性 [USER_IDS]
     *
     */
    @JSONField(name = "user_ids")
    @JsonProperty("user_ids")
    private String userIds;

    /**
     * 属性 [MESSAGE_UNREAD_COUNTER]
     *
     */
    @JSONField(name = "message_unread_counter")
    @JsonProperty("message_unread_counter")
    private Integer messageUnreadCounter;

    /**
     * 属性 [LINE_IDS]
     *
     */
    @JSONField(name = "line_ids")
    @JsonProperty("line_ids")
    private String lineIds;

    /**
     * 属性 [REPORT_MESSAGE_FREQUENCY]
     *
     */
    @JSONField(name = "report_message_frequency")
    @JsonProperty("report_message_frequency")
    private String reportMessageFrequency;

    /**
     * 属性 [MESSAGE_PARTNER_IDS]
     *
     */
    @JSONField(name = "message_partner_ids")
    @JsonProperty("message_partner_ids")
    private String messagePartnerIds;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    private String displayName;

    /**
     * 属性 [MESSAGE_CHANNEL_IDS]
     *
     */
    @JSONField(name = "message_channel_ids")
    @JsonProperty("message_channel_ids")
    private String messageChannelIds;

    /**
     * 属性 [MESSAGE_IDS]
     *
     */
    @JSONField(name = "message_ids")
    @JsonProperty("message_ids")
    private String messageIds;

    /**
     * 属性 [REWARD_REALTIME]
     *
     */
    @JSONField(name = "reward_realtime")
    @JsonProperty("reward_realtime")
    private String rewardRealtime;

    /**
     * 属性 [REWARD_FAILURE]
     *
     */
    @JSONField(name = "reward_failure")
    @JsonProperty("reward_failure")
    private String rewardFailure;

    /**
     * 属性 [PERIOD]
     *
     */
    @JSONField(name = "period")
    @JsonProperty("period")
    private String period;

    /**
     * 属性 [VISIBILITY_MODE]
     *
     */
    @JSONField(name = "visibility_mode")
    @JsonProperty("visibility_mode")
    private String visibilityMode;

    /**
     * 属性 [REMIND_UPDATE_DELAY]
     *
     */
    @JSONField(name = "remind_update_delay")
    @JsonProperty("remind_update_delay")
    private Integer remindUpdateDelay;

    /**
     * 属性 [WRITE_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;

    /**
     * 属性 [INVITED_USER_IDS]
     *
     */
    @JSONField(name = "invited_user_ids")
    @JsonProperty("invited_user_ids")
    private String invitedUserIds;

    /**
     * 属性 [MESSAGE_NEEDACTION_COUNTER]
     *
     */
    @JSONField(name = "message_needaction_counter")
    @JsonProperty("message_needaction_counter")
    private Integer messageNeedactionCounter;

    /**
     * 属性 [STATE]
     *
     */
    @JSONField(name = "state")
    @JsonProperty("state")
    private String state;

    /**
     * 属性 [NEXT_REPORT_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "next_report_date" , format="yyyy-MM-dd")
    @JsonProperty("next_report_date")
    private Timestamp nextReportDate;

    /**
     * 属性 [MANAGER_ID_TEXT]
     *
     */
    @JSONField(name = "manager_id_text")
    @JsonProperty("manager_id_text")
    private String managerIdText;

    /**
     * 属性 [REPORT_MESSAGE_GROUP_ID_TEXT]
     *
     */
    @JSONField(name = "report_message_group_id_text")
    @JsonProperty("report_message_group_id_text")
    private String reportMessageGroupIdText;

    /**
     * 属性 [REPORT_TEMPLATE_ID_TEXT]
     *
     */
    @JSONField(name = "report_template_id_text")
    @JsonProperty("report_template_id_text")
    private String reportTemplateIdText;

    /**
     * 属性 [REWARD_ID_TEXT]
     *
     */
    @JSONField(name = "reward_id_text")
    @JsonProperty("reward_id_text")
    private String rewardIdText;

    /**
     * 属性 [REWARD_FIRST_ID_TEXT]
     *
     */
    @JSONField(name = "reward_first_id_text")
    @JsonProperty("reward_first_id_text")
    private String rewardFirstIdText;

    /**
     * 属性 [CREATE_UID_TEXT]
     *
     */
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    private String createUidText;

    /**
     * 属性 [WRITE_UID_TEXT]
     *
     */
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    private String writeUidText;

    /**
     * 属性 [REWARD_SECOND_ID_TEXT]
     *
     */
    @JSONField(name = "reward_second_id_text")
    @JsonProperty("reward_second_id_text")
    private String rewardSecondIdText;

    /**
     * 属性 [REWARD_THIRD_ID_TEXT]
     *
     */
    @JSONField(name = "reward_third_id_text")
    @JsonProperty("reward_third_id_text")
    private String rewardThirdIdText;

    /**
     * 属性 [REWARD_FIRST_ID]
     *
     */
    @JSONField(name = "reward_first_id")
    @JsonProperty("reward_first_id")
    private Integer rewardFirstId;

    /**
     * 属性 [REPORT_TEMPLATE_ID]
     *
     */
    @JSONField(name = "report_template_id")
    @JsonProperty("report_template_id")
    private Integer reportTemplateId;

    /**
     * 属性 [REWARD_SECOND_ID]
     *
     */
    @JSONField(name = "reward_second_id")
    @JsonProperty("reward_second_id")
    private Integer rewardSecondId;

    /**
     * 属性 [REWARD_THIRD_ID]
     *
     */
    @JSONField(name = "reward_third_id")
    @JsonProperty("reward_third_id")
    private Integer rewardThirdId;

    /**
     * 属性 [REPORT_MESSAGE_GROUP_ID]
     *
     */
    @JSONField(name = "report_message_group_id")
    @JsonProperty("report_message_group_id")
    private Integer reportMessageGroupId;

    /**
     * 属性 [REWARD_ID]
     *
     */
    @JSONField(name = "reward_id")
    @JsonProperty("reward_id")
    private Integer rewardId;

    /**
     * 属性 [MANAGER_ID]
     *
     */
    @JSONField(name = "manager_id")
    @JsonProperty("manager_id")
    private Integer managerId;

    /**
     * 属性 [WRITE_UID]
     *
     */
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    private Integer writeUid;

    /**
     * 属性 [CREATE_UID]
     *
     */
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    private Integer createUid;


    /**
     * 设置 [NAME]
     */
    public void setName(String  name){
        this.name = name ;
        this.modify("name",name);
    }

    /**
     * 设置 [START_DATE]
     */
    public void setStartDate(Timestamp  startDate){
        this.startDate = startDate ;
        this.modify("start_date",startDate);
    }

    /**
     * 设置 [MESSAGE_MAIN_ATTACHMENT_ID]
     */
    public void setMessageMainAttachmentId(Integer  messageMainAttachmentId){
        this.messageMainAttachmentId = messageMainAttachmentId ;
        this.modify("message_main_attachment_id",messageMainAttachmentId);
    }

    /**
     * 设置 [LAST_REPORT_DATE]
     */
    public void setLastReportDate(Timestamp  lastReportDate){
        this.lastReportDate = lastReportDate ;
        this.modify("last_report_date",lastReportDate);
    }

    /**
     * 设置 [END_DATE]
     */
    public void setEndDate(Timestamp  endDate){
        this.endDate = endDate ;
        this.modify("end_date",endDate);
    }

    /**
     * 设置 [USER_DOMAIN]
     */
    public void setUserDomain(String  userDomain){
        this.userDomain = userDomain ;
        this.modify("user_domain",userDomain);
    }

    /**
     * 设置 [CATEGORY]
     */
    public void setCategory(String  category){
        this.category = category ;
        this.modify("category",category);
    }

    /**
     * 设置 [DESCRIPTION]
     */
    public void setDescription(String  description){
        this.description = description ;
        this.modify("description",description);
    }

    /**
     * 设置 [REPORT_MESSAGE_FREQUENCY]
     */
    public void setReportMessageFrequency(String  reportMessageFrequency){
        this.reportMessageFrequency = reportMessageFrequency ;
        this.modify("report_message_frequency",reportMessageFrequency);
    }

    /**
     * 设置 [REWARD_REALTIME]
     */
    public void setRewardRealtime(String  rewardRealtime){
        this.rewardRealtime = rewardRealtime ;
        this.modify("reward_realtime",rewardRealtime);
    }

    /**
     * 设置 [REWARD_FAILURE]
     */
    public void setRewardFailure(String  rewardFailure){
        this.rewardFailure = rewardFailure ;
        this.modify("reward_failure",rewardFailure);
    }

    /**
     * 设置 [PERIOD]
     */
    public void setPeriod(String  period){
        this.period = period ;
        this.modify("period",period);
    }

    /**
     * 设置 [VISIBILITY_MODE]
     */
    public void setVisibilityMode(String  visibilityMode){
        this.visibilityMode = visibilityMode ;
        this.modify("visibility_mode",visibilityMode);
    }

    /**
     * 设置 [REMIND_UPDATE_DELAY]
     */
    public void setRemindUpdateDelay(Integer  remindUpdateDelay){
        this.remindUpdateDelay = remindUpdateDelay ;
        this.modify("remind_update_delay",remindUpdateDelay);
    }

    /**
     * 设置 [STATE]
     */
    public void setState(String  state){
        this.state = state ;
        this.modify("state",state);
    }

    /**
     * 设置 [NEXT_REPORT_DATE]
     */
    public void setNextReportDate(Timestamp  nextReportDate){
        this.nextReportDate = nextReportDate ;
        this.modify("next_report_date",nextReportDate);
    }

    /**
     * 设置 [REWARD_FIRST_ID]
     */
    public void setRewardFirstId(Integer  rewardFirstId){
        this.rewardFirstId = rewardFirstId ;
        this.modify("reward_first_id",rewardFirstId);
    }

    /**
     * 设置 [REPORT_TEMPLATE_ID]
     */
    public void setReportTemplateId(Integer  reportTemplateId){
        this.reportTemplateId = reportTemplateId ;
        this.modify("report_template_id",reportTemplateId);
    }

    /**
     * 设置 [REWARD_SECOND_ID]
     */
    public void setRewardSecondId(Integer  rewardSecondId){
        this.rewardSecondId = rewardSecondId ;
        this.modify("reward_second_id",rewardSecondId);
    }

    /**
     * 设置 [REWARD_THIRD_ID]
     */
    public void setRewardThirdId(Integer  rewardThirdId){
        this.rewardThirdId = rewardThirdId ;
        this.modify("reward_third_id",rewardThirdId);
    }

    /**
     * 设置 [REPORT_MESSAGE_GROUP_ID]
     */
    public void setReportMessageGroupId(Integer  reportMessageGroupId){
        this.reportMessageGroupId = reportMessageGroupId ;
        this.modify("report_message_group_id",reportMessageGroupId);
    }

    /**
     * 设置 [REWARD_ID]
     */
    public void setRewardId(Integer  rewardId){
        this.rewardId = rewardId ;
        this.modify("reward_id",rewardId);
    }

    /**
     * 设置 [MANAGER_ID]
     */
    public void setManagerId(Integer  managerId){
        this.managerId = managerId ;
        this.modify("manager_id",managerId);
    }


}

