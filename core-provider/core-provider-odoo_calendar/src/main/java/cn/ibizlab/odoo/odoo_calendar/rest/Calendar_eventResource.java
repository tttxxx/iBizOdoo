package cn.ibizlab.odoo.odoo_calendar.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_calendar.dto.*;
import cn.ibizlab.odoo.odoo_calendar.mapping.*;
import cn.ibizlab.odoo.core.odoo_calendar.domain.Calendar_event;
import cn.ibizlab.odoo.core.odoo_calendar.service.ICalendar_eventService;
import cn.ibizlab.odoo.core.odoo_calendar.filter.Calendar_eventSearchContext;




@Slf4j
@Api(tags = {"Calendar_event" })
@RestController("odoo_calendar-calendar_event")
@RequestMapping("")
public class Calendar_eventResource {

    @Autowired
    private ICalendar_eventService calendar_eventService;

    @Autowired
    @Lazy
    private Calendar_eventMapping calendar_eventMapping;




    @PreAuthorize("hasPermission(#calendar_event_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Calendar_event" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/calendar_events/{calendar_event_id}")

    public ResponseEntity<Calendar_eventDTO> update(@PathVariable("calendar_event_id") Integer calendar_event_id, @RequestBody Calendar_eventDTO calendar_eventdto) {
		Calendar_event domain = calendar_eventMapping.toDomain(calendar_eventdto);
        domain.setId(calendar_event_id);
		calendar_eventService.update(domain);
		Calendar_eventDTO dto = calendar_eventMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#calendar_event_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Calendar_event" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/calendar_events/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Calendar_eventDTO> calendar_eventdtos) {
        calendar_eventService.updateBatch(calendar_eventMapping.toDomain(calendar_eventdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }










    @PreAuthorize("hasPermission(#calendar_event_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Calendar_event" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/calendar_events/{calendar_event_id}")
    public ResponseEntity<Calendar_eventDTO> get(@PathVariable("calendar_event_id") Integer calendar_event_id) {
        Calendar_event domain = calendar_eventService.get(calendar_event_id);
        Calendar_eventDTO dto = calendar_eventMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }







    @PreAuthorize("hasPermission('Remove',{#calendar_event_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Calendar_event" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/calendar_events/{calendar_event_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("calendar_event_id") Integer calendar_event_id) {
         return ResponseEntity.status(HttpStatus.OK).body(calendar_eventService.remove(calendar_event_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Calendar_event" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/calendar_events/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        calendar_eventService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Calendar_event" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/calendar_events")

    public ResponseEntity<Calendar_eventDTO> create(@RequestBody Calendar_eventDTO calendar_eventdto) {
        Calendar_event domain = calendar_eventMapping.toDomain(calendar_eventdto);
		calendar_eventService.create(domain);
        Calendar_eventDTO dto = calendar_eventMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Calendar_event" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/calendar_events/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Calendar_eventDTO> calendar_eventdtos) {
        calendar_eventService.createBatch(calendar_eventMapping.toDomain(calendar_eventdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Calendar_event" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/calendar_events/fetchdefault")
	public ResponseEntity<List<Calendar_eventDTO>> fetchDefault(Calendar_eventSearchContext context) {
        Page<Calendar_event> domains = calendar_eventService.searchDefault(context) ;
        List<Calendar_eventDTO> list = calendar_eventMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Calendar_event" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/calendar_events/searchdefault")
	public ResponseEntity<Page<Calendar_eventDTO>> searchDefault(Calendar_eventSearchContext context) {
        Page<Calendar_event> domains = calendar_eventService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(calendar_eventMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Calendar_event getEntity(){
        return new Calendar_event();
    }

}
