package cn.ibizlab.odoo.odoo_calendar.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_calendar.dto.*;
import cn.ibizlab.odoo.odoo_calendar.mapping.*;
import cn.ibizlab.odoo.core.odoo_calendar.domain.Calendar_event_type;
import cn.ibizlab.odoo.core.odoo_calendar.service.ICalendar_event_typeService;
import cn.ibizlab.odoo.core.odoo_calendar.filter.Calendar_event_typeSearchContext;




@Slf4j
@Api(tags = {"Calendar_event_type" })
@RestController("odoo_calendar-calendar_event_type")
@RequestMapping("")
public class Calendar_event_typeResource {

    @Autowired
    private ICalendar_event_typeService calendar_event_typeService;

    @Autowired
    @Lazy
    private Calendar_event_typeMapping calendar_event_typeMapping;







    @PreAuthorize("hasPermission(#calendar_event_type_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Calendar_event_type" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/calendar_event_types/{calendar_event_type_id}")
    public ResponseEntity<Calendar_event_typeDTO> get(@PathVariable("calendar_event_type_id") Integer calendar_event_type_id) {
        Calendar_event_type domain = calendar_event_typeService.get(calendar_event_type_id);
        Calendar_event_typeDTO dto = calendar_event_typeMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }







    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Calendar_event_type" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/calendar_event_types")

    public ResponseEntity<Calendar_event_typeDTO> create(@RequestBody Calendar_event_typeDTO calendar_event_typedto) {
        Calendar_event_type domain = calendar_event_typeMapping.toDomain(calendar_event_typedto);
		calendar_event_typeService.create(domain);
        Calendar_event_typeDTO dto = calendar_event_typeMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Calendar_event_type" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/calendar_event_types/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Calendar_event_typeDTO> calendar_event_typedtos) {
        calendar_event_typeService.createBatch(calendar_event_typeMapping.toDomain(calendar_event_typedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission('Remove',{#calendar_event_type_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Calendar_event_type" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/calendar_event_types/{calendar_event_type_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("calendar_event_type_id") Integer calendar_event_type_id) {
         return ResponseEntity.status(HttpStatus.OK).body(calendar_event_typeService.remove(calendar_event_type_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Calendar_event_type" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/calendar_event_types/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        calendar_event_typeService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission(#calendar_event_type_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Calendar_event_type" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/calendar_event_types/{calendar_event_type_id}")

    public ResponseEntity<Calendar_event_typeDTO> update(@PathVariable("calendar_event_type_id") Integer calendar_event_type_id, @RequestBody Calendar_event_typeDTO calendar_event_typedto) {
		Calendar_event_type domain = calendar_event_typeMapping.toDomain(calendar_event_typedto);
        domain.setId(calendar_event_type_id);
		calendar_event_typeService.update(domain);
		Calendar_event_typeDTO dto = calendar_event_typeMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#calendar_event_type_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Calendar_event_type" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/calendar_event_types/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Calendar_event_typeDTO> calendar_event_typedtos) {
        calendar_event_typeService.updateBatch(calendar_event_typeMapping.toDomain(calendar_event_typedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Calendar_event_type" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/calendar_event_types/fetchdefault")
	public ResponseEntity<List<Calendar_event_typeDTO>> fetchDefault(Calendar_event_typeSearchContext context) {
        Page<Calendar_event_type> domains = calendar_event_typeService.searchDefault(context) ;
        List<Calendar_event_typeDTO> list = calendar_event_typeMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Calendar_event_type" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/calendar_event_types/searchdefault")
	public ResponseEntity<Page<Calendar_event_typeDTO>> searchDefault(Calendar_event_typeSearchContext context) {
        Page<Calendar_event_type> domains = calendar_event_typeService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(calendar_event_typeMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Calendar_event_type getEntity(){
        return new Calendar_event_type();
    }

}
