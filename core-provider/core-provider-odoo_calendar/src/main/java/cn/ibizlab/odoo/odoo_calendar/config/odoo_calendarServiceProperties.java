package cn.ibizlab.odoo.odoo_calendar.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import lombok.Data;

@ConfigurationProperties(prefix = "service.odoo-calendar")
@Data
public class odoo_calendarServiceProperties {

	private boolean enabled;

	private boolean auth;


}