package cn.ibizlab.odoo.odoo_calendar.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_calendar.dto.*;
import cn.ibizlab.odoo.odoo_calendar.mapping.*;
import cn.ibizlab.odoo.core.odoo_calendar.domain.Calendar_contacts;
import cn.ibizlab.odoo.core.odoo_calendar.service.ICalendar_contactsService;
import cn.ibizlab.odoo.core.odoo_calendar.filter.Calendar_contactsSearchContext;




@Slf4j
@Api(tags = {"Calendar_contacts" })
@RestController("odoo_calendar-calendar_contacts")
@RequestMapping("")
public class Calendar_contactsResource {

    @Autowired
    private ICalendar_contactsService calendar_contactsService;

    @Autowired
    @Lazy
    private Calendar_contactsMapping calendar_contactsMapping;







    @PreAuthorize("hasPermission(#calendar_contacts_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Calendar_contacts" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/calendar_contacts/{calendar_contacts_id}")
    public ResponseEntity<Calendar_contactsDTO> get(@PathVariable("calendar_contacts_id") Integer calendar_contacts_id) {
        Calendar_contacts domain = calendar_contactsService.get(calendar_contacts_id);
        Calendar_contactsDTO dto = calendar_contactsMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }




    @PreAuthorize("hasPermission('Remove',{#calendar_contacts_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Calendar_contacts" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/calendar_contacts/{calendar_contacts_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("calendar_contacts_id") Integer calendar_contacts_id) {
         return ResponseEntity.status(HttpStatus.OK).body(calendar_contactsService.remove(calendar_contacts_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Calendar_contacts" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/calendar_contacts/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        calendar_contactsService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }










    @PreAuthorize("hasPermission(#calendar_contacts_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Calendar_contacts" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/calendar_contacts/{calendar_contacts_id}")

    public ResponseEntity<Calendar_contactsDTO> update(@PathVariable("calendar_contacts_id") Integer calendar_contacts_id, @RequestBody Calendar_contactsDTO calendar_contactsdto) {
		Calendar_contacts domain = calendar_contactsMapping.toDomain(calendar_contactsdto);
        domain.setId(calendar_contacts_id);
		calendar_contactsService.update(domain);
		Calendar_contactsDTO dto = calendar_contactsMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#calendar_contacts_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Calendar_contacts" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/calendar_contacts/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Calendar_contactsDTO> calendar_contactsdtos) {
        calendar_contactsService.updateBatch(calendar_contactsMapping.toDomain(calendar_contactsdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Calendar_contacts" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/calendar_contacts")

    public ResponseEntity<Calendar_contactsDTO> create(@RequestBody Calendar_contactsDTO calendar_contactsdto) {
        Calendar_contacts domain = calendar_contactsMapping.toDomain(calendar_contactsdto);
		calendar_contactsService.create(domain);
        Calendar_contactsDTO dto = calendar_contactsMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Calendar_contacts" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/calendar_contacts/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Calendar_contactsDTO> calendar_contactsdtos) {
        calendar_contactsService.createBatch(calendar_contactsMapping.toDomain(calendar_contactsdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Calendar_contacts" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/calendar_contacts/fetchdefault")
	public ResponseEntity<List<Calendar_contactsDTO>> fetchDefault(Calendar_contactsSearchContext context) {
        Page<Calendar_contacts> domains = calendar_contactsService.searchDefault(context) ;
        List<Calendar_contactsDTO> list = calendar_contactsMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Calendar_contacts" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/calendar_contacts/searchdefault")
	public ResponseEntity<Page<Calendar_contactsDTO>> searchDefault(Calendar_contactsSearchContext context) {
        Page<Calendar_contacts> domains = calendar_contactsService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(calendar_contactsMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Calendar_contacts getEntity(){
        return new Calendar_contacts();
    }

}
