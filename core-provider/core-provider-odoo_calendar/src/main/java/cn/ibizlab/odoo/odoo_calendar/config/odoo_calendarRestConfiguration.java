package cn.ibizlab.odoo.odoo_calendar.config;

import org.springframework.context.annotation.ComponentScan;

@ComponentScan("cn.ibizlab.odoo.odoo_calendar")
public class odoo_calendarRestConfiguration {

}
