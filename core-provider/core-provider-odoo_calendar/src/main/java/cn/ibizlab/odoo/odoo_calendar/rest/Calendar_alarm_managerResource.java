package cn.ibizlab.odoo.odoo_calendar.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_calendar.dto.*;
import cn.ibizlab.odoo.odoo_calendar.mapping.*;
import cn.ibizlab.odoo.core.odoo_calendar.domain.Calendar_alarm_manager;
import cn.ibizlab.odoo.core.odoo_calendar.service.ICalendar_alarm_managerService;
import cn.ibizlab.odoo.core.odoo_calendar.filter.Calendar_alarm_managerSearchContext;




@Slf4j
@Api(tags = {"Calendar_alarm_manager" })
@RestController("odoo_calendar-calendar_alarm_manager")
@RequestMapping("")
public class Calendar_alarm_managerResource {

    @Autowired
    private ICalendar_alarm_managerService calendar_alarm_managerService;

    @Autowired
    @Lazy
    private Calendar_alarm_managerMapping calendar_alarm_managerMapping;




    @PreAuthorize("hasPermission(#calendar_alarm_manager_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Calendar_alarm_manager" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/calendar_alarm_managers/{calendar_alarm_manager_id}")
    public ResponseEntity<Calendar_alarm_managerDTO> get(@PathVariable("calendar_alarm_manager_id") Integer calendar_alarm_manager_id) {
        Calendar_alarm_manager domain = calendar_alarm_managerService.get(calendar_alarm_manager_id);
        Calendar_alarm_managerDTO dto = calendar_alarm_managerMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }







    @PreAuthorize("hasPermission(#calendar_alarm_manager_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Calendar_alarm_manager" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/calendar_alarm_managers/{calendar_alarm_manager_id}")

    public ResponseEntity<Calendar_alarm_managerDTO> update(@PathVariable("calendar_alarm_manager_id") Integer calendar_alarm_manager_id, @RequestBody Calendar_alarm_managerDTO calendar_alarm_managerdto) {
		Calendar_alarm_manager domain = calendar_alarm_managerMapping.toDomain(calendar_alarm_managerdto);
        domain.setId(calendar_alarm_manager_id);
		calendar_alarm_managerService.update(domain);
		Calendar_alarm_managerDTO dto = calendar_alarm_managerMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#calendar_alarm_manager_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Calendar_alarm_manager" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/calendar_alarm_managers/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Calendar_alarm_managerDTO> calendar_alarm_managerdtos) {
        calendar_alarm_managerService.updateBatch(calendar_alarm_managerMapping.toDomain(calendar_alarm_managerdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }










    @PreAuthorize("hasPermission('Remove',{#calendar_alarm_manager_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Calendar_alarm_manager" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/calendar_alarm_managers/{calendar_alarm_manager_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("calendar_alarm_manager_id") Integer calendar_alarm_manager_id) {
         return ResponseEntity.status(HttpStatus.OK).body(calendar_alarm_managerService.remove(calendar_alarm_manager_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Calendar_alarm_manager" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/calendar_alarm_managers/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        calendar_alarm_managerService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Calendar_alarm_manager" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/calendar_alarm_managers")

    public ResponseEntity<Calendar_alarm_managerDTO> create(@RequestBody Calendar_alarm_managerDTO calendar_alarm_managerdto) {
        Calendar_alarm_manager domain = calendar_alarm_managerMapping.toDomain(calendar_alarm_managerdto);
		calendar_alarm_managerService.create(domain);
        Calendar_alarm_managerDTO dto = calendar_alarm_managerMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Calendar_alarm_manager" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/calendar_alarm_managers/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Calendar_alarm_managerDTO> calendar_alarm_managerdtos) {
        calendar_alarm_managerService.createBatch(calendar_alarm_managerMapping.toDomain(calendar_alarm_managerdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Calendar_alarm_manager" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/calendar_alarm_managers/fetchdefault")
	public ResponseEntity<List<Calendar_alarm_managerDTO>> fetchDefault(Calendar_alarm_managerSearchContext context) {
        Page<Calendar_alarm_manager> domains = calendar_alarm_managerService.searchDefault(context) ;
        List<Calendar_alarm_managerDTO> list = calendar_alarm_managerMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Calendar_alarm_manager" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/calendar_alarm_managers/searchdefault")
	public ResponseEntity<Page<Calendar_alarm_managerDTO>> searchDefault(Calendar_alarm_managerSearchContext context) {
        Page<Calendar_alarm_manager> domains = calendar_alarm_managerService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(calendar_alarm_managerMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Calendar_alarm_manager getEntity(){
        return new Calendar_alarm_manager();
    }

}
