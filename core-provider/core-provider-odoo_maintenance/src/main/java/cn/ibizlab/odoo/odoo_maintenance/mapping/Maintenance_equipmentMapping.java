package cn.ibizlab.odoo.odoo_maintenance.mapping;

import org.mapstruct.*;
import cn.ibizlab.odoo.core.odoo_maintenance.domain.Maintenance_equipment;
import cn.ibizlab.odoo.odoo_maintenance.dto.Maintenance_equipmentDTO;
import cn.ibizlab.odoo.util.domain.MappingBase;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring", uses = {},
    nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
    nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface Maintenance_equipmentMapping extends MappingBase<Maintenance_equipmentDTO, Maintenance_equipment> {


}

