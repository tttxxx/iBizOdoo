package cn.ibizlab.odoo.odoo_maintenance.config;

import org.springframework.context.annotation.ComponentScan;

@ComponentScan("cn.ibizlab.odoo.odoo_maintenance")
public class odoo_maintenanceRestConfiguration {

}
