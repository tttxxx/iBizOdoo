package cn.ibizlab.odoo.odoo_maintenance.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_maintenance.dto.*;
import cn.ibizlab.odoo.odoo_maintenance.mapping.*;
import cn.ibizlab.odoo.core.odoo_maintenance.domain.Maintenance_equipment;
import cn.ibizlab.odoo.core.odoo_maintenance.service.IMaintenance_equipmentService;
import cn.ibizlab.odoo.core.odoo_maintenance.filter.Maintenance_equipmentSearchContext;




@Slf4j
@Api(tags = {"Maintenance_equipment" })
@RestController("odoo_maintenance-maintenance_equipment")
@RequestMapping("")
public class Maintenance_equipmentResource {

    @Autowired
    private IMaintenance_equipmentService maintenance_equipmentService;

    @Autowired
    @Lazy
    private Maintenance_equipmentMapping maintenance_equipmentMapping;




    @PreAuthorize("hasPermission('Remove',{#maintenance_equipment_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Maintenance_equipment" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/maintenance_equipments/{maintenance_equipment_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("maintenance_equipment_id") Integer maintenance_equipment_id) {
         return ResponseEntity.status(HttpStatus.OK).body(maintenance_equipmentService.remove(maintenance_equipment_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Maintenance_equipment" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/maintenance_equipments/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        maintenance_equipmentService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission(#maintenance_equipment_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Maintenance_equipment" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/maintenance_equipments/{maintenance_equipment_id}")
    public ResponseEntity<Maintenance_equipmentDTO> get(@PathVariable("maintenance_equipment_id") Integer maintenance_equipment_id) {
        Maintenance_equipment domain = maintenance_equipmentService.get(maintenance_equipment_id);
        Maintenance_equipmentDTO dto = maintenance_equipmentMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }













    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Maintenance_equipment" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/maintenance_equipments")

    public ResponseEntity<Maintenance_equipmentDTO> create(@RequestBody Maintenance_equipmentDTO maintenance_equipmentdto) {
        Maintenance_equipment domain = maintenance_equipmentMapping.toDomain(maintenance_equipmentdto);
		maintenance_equipmentService.create(domain);
        Maintenance_equipmentDTO dto = maintenance_equipmentMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Maintenance_equipment" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/maintenance_equipments/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Maintenance_equipmentDTO> maintenance_equipmentdtos) {
        maintenance_equipmentService.createBatch(maintenance_equipmentMapping.toDomain(maintenance_equipmentdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission(#maintenance_equipment_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Maintenance_equipment" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/maintenance_equipments/{maintenance_equipment_id}")

    public ResponseEntity<Maintenance_equipmentDTO> update(@PathVariable("maintenance_equipment_id") Integer maintenance_equipment_id, @RequestBody Maintenance_equipmentDTO maintenance_equipmentdto) {
		Maintenance_equipment domain = maintenance_equipmentMapping.toDomain(maintenance_equipmentdto);
        domain.setId(maintenance_equipment_id);
		maintenance_equipmentService.update(domain);
		Maintenance_equipmentDTO dto = maintenance_equipmentMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#maintenance_equipment_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Maintenance_equipment" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/maintenance_equipments/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Maintenance_equipmentDTO> maintenance_equipmentdtos) {
        maintenance_equipmentService.updateBatch(maintenance_equipmentMapping.toDomain(maintenance_equipmentdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Maintenance_equipment" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/maintenance_equipments/fetchdefault")
	public ResponseEntity<List<Maintenance_equipmentDTO>> fetchDefault(Maintenance_equipmentSearchContext context) {
        Page<Maintenance_equipment> domains = maintenance_equipmentService.searchDefault(context) ;
        List<Maintenance_equipmentDTO> list = maintenance_equipmentMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Maintenance_equipment" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/maintenance_equipments/searchdefault")
	public ResponseEntity<Page<Maintenance_equipmentDTO>> searchDefault(Maintenance_equipmentSearchContext context) {
        Page<Maintenance_equipment> domains = maintenance_equipmentService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(maintenance_equipmentMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Maintenance_equipment getEntity(){
        return new Maintenance_equipment();
    }

}
