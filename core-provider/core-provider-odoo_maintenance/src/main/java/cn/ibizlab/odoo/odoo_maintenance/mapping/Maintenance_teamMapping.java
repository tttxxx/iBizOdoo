package cn.ibizlab.odoo.odoo_maintenance.mapping;

import org.mapstruct.*;
import cn.ibizlab.odoo.core.odoo_maintenance.domain.Maintenance_team;
import cn.ibizlab.odoo.odoo_maintenance.dto.Maintenance_teamDTO;
import cn.ibizlab.odoo.util.domain.MappingBase;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring", uses = {},
    nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
    nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface Maintenance_teamMapping extends MappingBase<Maintenance_teamDTO, Maintenance_team> {


}

