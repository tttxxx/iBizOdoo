package cn.ibizlab.odoo.odoo_maintenance.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_maintenance.dto.*;
import cn.ibizlab.odoo.odoo_maintenance.mapping.*;
import cn.ibizlab.odoo.core.odoo_maintenance.domain.Maintenance_stage;
import cn.ibizlab.odoo.core.odoo_maintenance.service.IMaintenance_stageService;
import cn.ibizlab.odoo.core.odoo_maintenance.filter.Maintenance_stageSearchContext;




@Slf4j
@Api(tags = {"Maintenance_stage" })
@RestController("odoo_maintenance-maintenance_stage")
@RequestMapping("")
public class Maintenance_stageResource {

    @Autowired
    private IMaintenance_stageService maintenance_stageService;

    @Autowired
    @Lazy
    private Maintenance_stageMapping maintenance_stageMapping;




    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Maintenance_stage" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/maintenance_stages")

    public ResponseEntity<Maintenance_stageDTO> create(@RequestBody Maintenance_stageDTO maintenance_stagedto) {
        Maintenance_stage domain = maintenance_stageMapping.toDomain(maintenance_stagedto);
		maintenance_stageService.create(domain);
        Maintenance_stageDTO dto = maintenance_stageMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Maintenance_stage" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/maintenance_stages/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Maintenance_stageDTO> maintenance_stagedtos) {
        maintenance_stageService.createBatch(maintenance_stageMapping.toDomain(maintenance_stagedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission(#maintenance_stage_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Maintenance_stage" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/maintenance_stages/{maintenance_stage_id}")
    public ResponseEntity<Maintenance_stageDTO> get(@PathVariable("maintenance_stage_id") Integer maintenance_stage_id) {
        Maintenance_stage domain = maintenance_stageService.get(maintenance_stage_id);
        Maintenance_stageDTO dto = maintenance_stageMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }







    @PreAuthorize("hasPermission(#maintenance_stage_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Maintenance_stage" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/maintenance_stages/{maintenance_stage_id}")

    public ResponseEntity<Maintenance_stageDTO> update(@PathVariable("maintenance_stage_id") Integer maintenance_stage_id, @RequestBody Maintenance_stageDTO maintenance_stagedto) {
		Maintenance_stage domain = maintenance_stageMapping.toDomain(maintenance_stagedto);
        domain.setId(maintenance_stage_id);
		maintenance_stageService.update(domain);
		Maintenance_stageDTO dto = maintenance_stageMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#maintenance_stage_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Maintenance_stage" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/maintenance_stages/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Maintenance_stageDTO> maintenance_stagedtos) {
        maintenance_stageService.updateBatch(maintenance_stageMapping.toDomain(maintenance_stagedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }










    @PreAuthorize("hasPermission('Remove',{#maintenance_stage_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Maintenance_stage" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/maintenance_stages/{maintenance_stage_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("maintenance_stage_id") Integer maintenance_stage_id) {
         return ResponseEntity.status(HttpStatus.OK).body(maintenance_stageService.remove(maintenance_stage_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Maintenance_stage" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/maintenance_stages/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        maintenance_stageService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Maintenance_stage" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/maintenance_stages/fetchdefault")
	public ResponseEntity<List<Maintenance_stageDTO>> fetchDefault(Maintenance_stageSearchContext context) {
        Page<Maintenance_stage> domains = maintenance_stageService.searchDefault(context) ;
        List<Maintenance_stageDTO> list = maintenance_stageMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Maintenance_stage" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/maintenance_stages/searchdefault")
	public ResponseEntity<Page<Maintenance_stageDTO>> searchDefault(Maintenance_stageSearchContext context) {
        Page<Maintenance_stage> domains = maintenance_stageService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(maintenance_stageMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Maintenance_stage getEntity(){
        return new Maintenance_stage();
    }

}
