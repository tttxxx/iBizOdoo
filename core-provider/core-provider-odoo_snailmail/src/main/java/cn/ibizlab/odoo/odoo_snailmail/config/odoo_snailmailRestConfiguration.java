package cn.ibizlab.odoo.odoo_snailmail.config;

import org.springframework.context.annotation.ComponentScan;

@ComponentScan("cn.ibizlab.odoo.odoo_snailmail")
public class odoo_snailmailRestConfiguration {

}
