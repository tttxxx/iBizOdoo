package cn.ibizlab.odoo.odoo_snailmail.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_snailmail.dto.*;
import cn.ibizlab.odoo.odoo_snailmail.mapping.*;
import cn.ibizlab.odoo.core.odoo_snailmail.domain.Snailmail_letter;
import cn.ibizlab.odoo.core.odoo_snailmail.service.ISnailmail_letterService;
import cn.ibizlab.odoo.core.odoo_snailmail.filter.Snailmail_letterSearchContext;




@Slf4j
@Api(tags = {"Snailmail_letter" })
@RestController("odoo_snailmail-snailmail_letter")
@RequestMapping("")
public class Snailmail_letterResource {

    @Autowired
    private ISnailmail_letterService snailmail_letterService;

    @Autowired
    @Lazy
    private Snailmail_letterMapping snailmail_letterMapping;







    @PreAuthorize("hasPermission('Remove',{#snailmail_letter_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Snailmail_letter" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/snailmail_letters/{snailmail_letter_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("snailmail_letter_id") Integer snailmail_letter_id) {
         return ResponseEntity.status(HttpStatus.OK).body(snailmail_letterService.remove(snailmail_letter_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Snailmail_letter" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/snailmail_letters/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        snailmail_letterService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }







    @PreAuthorize("hasPermission(#snailmail_letter_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Snailmail_letter" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/snailmail_letters/{snailmail_letter_id}")
    public ResponseEntity<Snailmail_letterDTO> get(@PathVariable("snailmail_letter_id") Integer snailmail_letter_id) {
        Snailmail_letter domain = snailmail_letterService.get(snailmail_letter_id);
        Snailmail_letterDTO dto = snailmail_letterMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }







    @PreAuthorize("hasPermission(#snailmail_letter_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Snailmail_letter" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/snailmail_letters/{snailmail_letter_id}")

    public ResponseEntity<Snailmail_letterDTO> update(@PathVariable("snailmail_letter_id") Integer snailmail_letter_id, @RequestBody Snailmail_letterDTO snailmail_letterdto) {
		Snailmail_letter domain = snailmail_letterMapping.toDomain(snailmail_letterdto);
        domain.setId(snailmail_letter_id);
		snailmail_letterService.update(domain);
		Snailmail_letterDTO dto = snailmail_letterMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#snailmail_letter_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Snailmail_letter" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/snailmail_letters/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Snailmail_letterDTO> snailmail_letterdtos) {
        snailmail_letterService.updateBatch(snailmail_letterMapping.toDomain(snailmail_letterdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Snailmail_letter" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/snailmail_letters")

    public ResponseEntity<Snailmail_letterDTO> create(@RequestBody Snailmail_letterDTO snailmail_letterdto) {
        Snailmail_letter domain = snailmail_letterMapping.toDomain(snailmail_letterdto);
		snailmail_letterService.create(domain);
        Snailmail_letterDTO dto = snailmail_letterMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Snailmail_letter" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/snailmail_letters/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Snailmail_letterDTO> snailmail_letterdtos) {
        snailmail_letterService.createBatch(snailmail_letterMapping.toDomain(snailmail_letterdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Snailmail_letter" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/snailmail_letters/fetchdefault")
	public ResponseEntity<List<Snailmail_letterDTO>> fetchDefault(Snailmail_letterSearchContext context) {
        Page<Snailmail_letter> domains = snailmail_letterService.searchDefault(context) ;
        List<Snailmail_letterDTO> list = snailmail_letterMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Snailmail_letter" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/snailmail_letters/searchdefault")
	public ResponseEntity<Page<Snailmail_letterDTO>> searchDefault(Snailmail_letterSearchContext context) {
        Page<Snailmail_letter> domains = snailmail_letterService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(snailmail_letterMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Snailmail_letter getEntity(){
        return new Snailmail_letter();
    }

}
