package cn.ibizlab.odoo.odoo_repair.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_repair.dto.*;
import cn.ibizlab.odoo.odoo_repair.mapping.*;
import cn.ibizlab.odoo.core.odoo_repair.domain.Repair_order_make_invoice;
import cn.ibizlab.odoo.core.odoo_repair.service.IRepair_order_make_invoiceService;
import cn.ibizlab.odoo.core.odoo_repair.filter.Repair_order_make_invoiceSearchContext;




@Slf4j
@Api(tags = {"Repair_order_make_invoice" })
@RestController("odoo_repair-repair_order_make_invoice")
@RequestMapping("")
public class Repair_order_make_invoiceResource {

    @Autowired
    private IRepair_order_make_invoiceService repair_order_make_invoiceService;

    @Autowired
    @Lazy
    private Repair_order_make_invoiceMapping repair_order_make_invoiceMapping;




    @PreAuthorize("hasPermission(#repair_order_make_invoice_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Repair_order_make_invoice" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/repair_order_make_invoices/{repair_order_make_invoice_id}")

    public ResponseEntity<Repair_order_make_invoiceDTO> update(@PathVariable("repair_order_make_invoice_id") Integer repair_order_make_invoice_id, @RequestBody Repair_order_make_invoiceDTO repair_order_make_invoicedto) {
		Repair_order_make_invoice domain = repair_order_make_invoiceMapping.toDomain(repair_order_make_invoicedto);
        domain.setId(repair_order_make_invoice_id);
		repair_order_make_invoiceService.update(domain);
		Repair_order_make_invoiceDTO dto = repair_order_make_invoiceMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#repair_order_make_invoice_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Repair_order_make_invoice" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/repair_order_make_invoices/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Repair_order_make_invoiceDTO> repair_order_make_invoicedtos) {
        repair_order_make_invoiceService.updateBatch(repair_order_make_invoiceMapping.toDomain(repair_order_make_invoicedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission(#repair_order_make_invoice_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Repair_order_make_invoice" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/repair_order_make_invoices/{repair_order_make_invoice_id}")
    public ResponseEntity<Repair_order_make_invoiceDTO> get(@PathVariable("repair_order_make_invoice_id") Integer repair_order_make_invoice_id) {
        Repair_order_make_invoice domain = repair_order_make_invoiceService.get(repair_order_make_invoice_id);
        Repair_order_make_invoiceDTO dto = repair_order_make_invoiceMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }










    @PreAuthorize("hasPermission('Remove',{#repair_order_make_invoice_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Repair_order_make_invoice" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/repair_order_make_invoices/{repair_order_make_invoice_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("repair_order_make_invoice_id") Integer repair_order_make_invoice_id) {
         return ResponseEntity.status(HttpStatus.OK).body(repair_order_make_invoiceService.remove(repair_order_make_invoice_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Repair_order_make_invoice" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/repair_order_make_invoices/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        repair_order_make_invoiceService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Repair_order_make_invoice" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/repair_order_make_invoices")

    public ResponseEntity<Repair_order_make_invoiceDTO> create(@RequestBody Repair_order_make_invoiceDTO repair_order_make_invoicedto) {
        Repair_order_make_invoice domain = repair_order_make_invoiceMapping.toDomain(repair_order_make_invoicedto);
		repair_order_make_invoiceService.create(domain);
        Repair_order_make_invoiceDTO dto = repair_order_make_invoiceMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Repair_order_make_invoice" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/repair_order_make_invoices/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Repair_order_make_invoiceDTO> repair_order_make_invoicedtos) {
        repair_order_make_invoiceService.createBatch(repair_order_make_invoiceMapping.toDomain(repair_order_make_invoicedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Repair_order_make_invoice" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/repair_order_make_invoices/fetchdefault")
	public ResponseEntity<List<Repair_order_make_invoiceDTO>> fetchDefault(Repair_order_make_invoiceSearchContext context) {
        Page<Repair_order_make_invoice> domains = repair_order_make_invoiceService.searchDefault(context) ;
        List<Repair_order_make_invoiceDTO> list = repair_order_make_invoiceMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Repair_order_make_invoice" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/repair_order_make_invoices/searchdefault")
	public ResponseEntity<Page<Repair_order_make_invoiceDTO>> searchDefault(Repair_order_make_invoiceSearchContext context) {
        Page<Repair_order_make_invoice> domains = repair_order_make_invoiceService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(repair_order_make_invoiceMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Repair_order_make_invoice getEntity(){
        return new Repair_order_make_invoice();
    }

}
