package cn.ibizlab.odoo.odoo_repair.mapping;

import org.mapstruct.*;
import cn.ibizlab.odoo.core.odoo_repair.domain.Repair_fee;
import cn.ibizlab.odoo.odoo_repair.dto.Repair_feeDTO;
import cn.ibizlab.odoo.util.domain.MappingBase;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring", uses = {},
    nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
    nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface Repair_feeMapping extends MappingBase<Repair_feeDTO, Repair_fee> {


}

