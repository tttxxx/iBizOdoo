package cn.ibizlab.odoo.odoo_repair.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import lombok.Data;

@ConfigurationProperties(prefix = "service.odoo-repair")
@Data
public class odoo_repairServiceProperties {

	private boolean enabled;

	private boolean auth;


}