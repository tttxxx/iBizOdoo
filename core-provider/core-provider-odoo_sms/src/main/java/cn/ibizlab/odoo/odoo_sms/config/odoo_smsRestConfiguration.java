package cn.ibizlab.odoo.odoo_sms.config;

import org.springframework.context.annotation.ComponentScan;

@ComponentScan("cn.ibizlab.odoo.odoo_sms")
public class odoo_smsRestConfiguration {

}
