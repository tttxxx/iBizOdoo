package cn.ibizlab.odoo.odoo_sms.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_sms.dto.*;
import cn.ibizlab.odoo.odoo_sms.mapping.*;
import cn.ibizlab.odoo.core.odoo_sms.domain.Sms_send_sms;
import cn.ibizlab.odoo.core.odoo_sms.service.ISms_send_smsService;
import cn.ibizlab.odoo.core.odoo_sms.filter.Sms_send_smsSearchContext;




@Slf4j
@Api(tags = {"Sms_send_sms" })
@RestController("odoo_sms-sms_send_sms")
@RequestMapping("")
public class Sms_send_smsResource {

    @Autowired
    private ISms_send_smsService sms_send_smsService;

    @Autowired
    @Lazy
    private Sms_send_smsMapping sms_send_smsMapping;







    @PreAuthorize("hasPermission(#sms_send_sms_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Sms_send_sms" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/sms_send_sms/{sms_send_sms_id}")

    public ResponseEntity<Sms_send_smsDTO> update(@PathVariable("sms_send_sms_id") Integer sms_send_sms_id, @RequestBody Sms_send_smsDTO sms_send_smsdto) {
		Sms_send_sms domain = sms_send_smsMapping.toDomain(sms_send_smsdto);
        domain.setId(sms_send_sms_id);
		sms_send_smsService.update(domain);
		Sms_send_smsDTO dto = sms_send_smsMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#sms_send_sms_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Sms_send_sms" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/sms_send_sms/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Sms_send_smsDTO> sms_send_smsdtos) {
        sms_send_smsService.updateBatch(sms_send_smsMapping.toDomain(sms_send_smsdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Sms_send_sms" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/sms_send_sms")

    public ResponseEntity<Sms_send_smsDTO> create(@RequestBody Sms_send_smsDTO sms_send_smsdto) {
        Sms_send_sms domain = sms_send_smsMapping.toDomain(sms_send_smsdto);
		sms_send_smsService.create(domain);
        Sms_send_smsDTO dto = sms_send_smsMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Sms_send_sms" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/sms_send_sms/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Sms_send_smsDTO> sms_send_smsdtos) {
        sms_send_smsService.createBatch(sms_send_smsMapping.toDomain(sms_send_smsdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }







    @PreAuthorize("hasPermission('Remove',{#sms_send_sms_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Sms_send_sms" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/sms_send_sms/{sms_send_sms_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("sms_send_sms_id") Integer sms_send_sms_id) {
         return ResponseEntity.status(HttpStatus.OK).body(sms_send_smsService.remove(sms_send_sms_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Sms_send_sms" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/sms_send_sms/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        sms_send_smsService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }







    @PreAuthorize("hasPermission(#sms_send_sms_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Sms_send_sms" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/sms_send_sms/{sms_send_sms_id}")
    public ResponseEntity<Sms_send_smsDTO> get(@PathVariable("sms_send_sms_id") Integer sms_send_sms_id) {
        Sms_send_sms domain = sms_send_smsService.get(sms_send_sms_id);
        Sms_send_smsDTO dto = sms_send_smsMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Sms_send_sms" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/sms_send_sms/fetchdefault")
	public ResponseEntity<List<Sms_send_smsDTO>> fetchDefault(Sms_send_smsSearchContext context) {
        Page<Sms_send_sms> domains = sms_send_smsService.searchDefault(context) ;
        List<Sms_send_smsDTO> list = sms_send_smsMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Sms_send_sms" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/sms_send_sms/searchdefault")
	public ResponseEntity<Page<Sms_send_smsDTO>> searchDefault(Sms_send_smsSearchContext context) {
        Page<Sms_send_sms> domains = sms_send_smsService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(sms_send_smsMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Sms_send_sms getEntity(){
        return new Sms_send_sms();
    }

}
