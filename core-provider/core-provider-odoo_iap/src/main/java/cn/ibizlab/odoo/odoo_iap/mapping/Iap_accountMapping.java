package cn.ibizlab.odoo.odoo_iap.mapping;

import org.mapstruct.*;
import cn.ibizlab.odoo.core.odoo_iap.domain.Iap_account;
import cn.ibizlab.odoo.odoo_iap.dto.Iap_accountDTO;
import cn.ibizlab.odoo.util.domain.MappingBase;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring", uses = {},
    nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
    nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface Iap_accountMapping extends MappingBase<Iap_accountDTO, Iap_account> {


}

