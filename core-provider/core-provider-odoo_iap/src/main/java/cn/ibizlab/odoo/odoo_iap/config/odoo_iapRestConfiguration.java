package cn.ibizlab.odoo.odoo_iap.config;

import org.springframework.context.annotation.ComponentScan;

@ComponentScan("cn.ibizlab.odoo.odoo_iap")
public class odoo_iapRestConfiguration {

}
