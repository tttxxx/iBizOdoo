package cn.ibizlab.odoo.odoo_mrp.mapping;

import org.mapstruct.*;
import cn.ibizlab.odoo.core.odoo_mrp.domain.Mrp_bom_line;
import cn.ibizlab.odoo.odoo_mrp.dto.Mrp_bom_lineDTO;
import cn.ibizlab.odoo.util.domain.MappingBase;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring", uses = {},
    nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
    nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface Mrp_bom_lineMapping extends MappingBase<Mrp_bom_lineDTO, Mrp_bom_line> {


}

