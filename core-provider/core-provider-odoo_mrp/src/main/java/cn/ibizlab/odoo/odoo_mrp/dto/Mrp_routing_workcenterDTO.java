package cn.ibizlab.odoo.odoo_mrp.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;
import cn.ibizlab.odoo.util.domain.DTOBase;
import lombok.Data;

/**
 * 服务DTO对象[Mrp_routing_workcenterDTO]
 */
@Data
public class Mrp_routing_workcenterDTO extends DTOBase implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [TIME_MODE_BATCH]
     *
     */
    @JSONField(name = "time_mode_batch")
    @JsonProperty("time_mode_batch")
    private Integer timeModeBatch;

    /**
     * 属性 [BATCH_SIZE]
     *
     */
    @JSONField(name = "batch_size")
    @JsonProperty("batch_size")
    private Double batchSize;

    /**
     * 属性 [WORKORDER_IDS]
     *
     */
    @JSONField(name = "workorder_ids")
    @JsonProperty("workorder_ids")
    private String workorderIds;

    /**
     * 属性 [WORKORDER_COUNT]
     *
     */
    @JSONField(name = "workorder_count")
    @JsonProperty("workorder_count")
    private Integer workorderCount;

    /**
     * 属性 [SEQUENCE]
     *
     */
    @JSONField(name = "sequence")
    @JsonProperty("sequence")
    private Integer sequence;

    /**
     * 属性 [WRITE_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;

    /**
     * 属性 [BATCH]
     *
     */
    @JSONField(name = "batch")
    @JsonProperty("batch")
    private String batch;

    /**
     * 属性 [TIME_CYCLE_MANUAL]
     *
     */
    @JSONField(name = "time_cycle_manual")
    @JsonProperty("time_cycle_manual")
    private Double timeCycleManual;

    /**
     * 属性 [ID]
     *
     */
    @JSONField(name = "id")
    @JsonProperty("id")
    private Integer id;

    /**
     * 属性 [NOTE]
     *
     */
    @JSONField(name = "note")
    @JsonProperty("note")
    private String note;

    /**
     * 属性 [TIME_MODE]
     *
     */
    @JSONField(name = "time_mode")
    @JsonProperty("time_mode")
    private String timeMode;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    private String displayName;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;

    /**
     * 属性 [NAME]
     *
     */
    @JSONField(name = "name")
    @JsonProperty("name")
    private String name;

    /**
     * 属性 [TIME_CYCLE]
     *
     */
    @JSONField(name = "time_cycle")
    @JsonProperty("time_cycle")
    private Double timeCycle;

    /**
     * 属性 [WORKSHEET]
     *
     */
    @JSONField(name = "worksheet")
    @JsonProperty("worksheet")
    private byte[] worksheet;

    /**
     * 属性 [CREATE_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;

    /**
     * 属性 [COMPANY_ID_TEXT]
     *
     */
    @JSONField(name = "company_id_text")
    @JsonProperty("company_id_text")
    private String companyIdText;

    /**
     * 属性 [WORKCENTER_ID_TEXT]
     *
     */
    @JSONField(name = "workcenter_id_text")
    @JsonProperty("workcenter_id_text")
    private String workcenterIdText;

    /**
     * 属性 [ROUTING_ID_TEXT]
     *
     */
    @JSONField(name = "routing_id_text")
    @JsonProperty("routing_id_text")
    private String routingIdText;

    /**
     * 属性 [WRITE_UID_TEXT]
     *
     */
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    private String writeUidText;

    /**
     * 属性 [CREATE_UID_TEXT]
     *
     */
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    private String createUidText;

    /**
     * 属性 [ROUTING_ID]
     *
     */
    @JSONField(name = "routing_id")
    @JsonProperty("routing_id")
    private Integer routingId;

    /**
     * 属性 [WRITE_UID]
     *
     */
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    private Integer writeUid;

    /**
     * 属性 [COMPANY_ID]
     *
     */
    @JSONField(name = "company_id")
    @JsonProperty("company_id")
    private Integer companyId;

    /**
     * 属性 [WORKCENTER_ID]
     *
     */
    @JSONField(name = "workcenter_id")
    @JsonProperty("workcenter_id")
    private Integer workcenterId;

    /**
     * 属性 [CREATE_UID]
     *
     */
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    private Integer createUid;


    /**
     * 设置 [TIME_MODE_BATCH]
     */
    public void setTimeModeBatch(Integer  timeModeBatch){
        this.timeModeBatch = timeModeBatch ;
        this.modify("time_mode_batch",timeModeBatch);
    }

    /**
     * 设置 [BATCH_SIZE]
     */
    public void setBatchSize(Double  batchSize){
        this.batchSize = batchSize ;
        this.modify("batch_size",batchSize);
    }

    /**
     * 设置 [SEQUENCE]
     */
    public void setSequence(Integer  sequence){
        this.sequence = sequence ;
        this.modify("sequence",sequence);
    }

    /**
     * 设置 [BATCH]
     */
    public void setBatch(String  batch){
        this.batch = batch ;
        this.modify("batch",batch);
    }

    /**
     * 设置 [TIME_CYCLE_MANUAL]
     */
    public void setTimeCycleManual(Double  timeCycleManual){
        this.timeCycleManual = timeCycleManual ;
        this.modify("time_cycle_manual",timeCycleManual);
    }

    /**
     * 设置 [NOTE]
     */
    public void setNote(String  note){
        this.note = note ;
        this.modify("note",note);
    }

    /**
     * 设置 [TIME_MODE]
     */
    public void setTimeMode(String  timeMode){
        this.timeMode = timeMode ;
        this.modify("time_mode",timeMode);
    }

    /**
     * 设置 [NAME]
     */
    public void setName(String  name){
        this.name = name ;
        this.modify("name",name);
    }

    /**
     * 设置 [ROUTING_ID]
     */
    public void setRoutingId(Integer  routingId){
        this.routingId = routingId ;
        this.modify("routing_id",routingId);
    }

    /**
     * 设置 [COMPANY_ID]
     */
    public void setCompanyId(Integer  companyId){
        this.companyId = companyId ;
        this.modify("company_id",companyId);
    }

    /**
     * 设置 [WORKCENTER_ID]
     */
    public void setWorkcenterId(Integer  workcenterId){
        this.workcenterId = workcenterId ;
        this.modify("workcenter_id",workcenterId);
    }


}

