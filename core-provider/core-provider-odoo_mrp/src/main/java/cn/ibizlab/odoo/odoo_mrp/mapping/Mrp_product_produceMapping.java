package cn.ibizlab.odoo.odoo_mrp.mapping;

import org.mapstruct.*;
import cn.ibizlab.odoo.core.odoo_mrp.domain.Mrp_product_produce;
import cn.ibizlab.odoo.odoo_mrp.dto.Mrp_product_produceDTO;
import cn.ibizlab.odoo.util.domain.MappingBase;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring", uses = {},
    nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
    nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface Mrp_product_produceMapping extends MappingBase<Mrp_product_produceDTO, Mrp_product_produce> {


}

