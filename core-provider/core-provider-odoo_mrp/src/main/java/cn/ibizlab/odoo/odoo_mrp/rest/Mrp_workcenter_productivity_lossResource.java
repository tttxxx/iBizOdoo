package cn.ibizlab.odoo.odoo_mrp.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_mrp.dto.*;
import cn.ibizlab.odoo.odoo_mrp.mapping.*;
import cn.ibizlab.odoo.core.odoo_mrp.domain.Mrp_workcenter_productivity_loss;
import cn.ibizlab.odoo.core.odoo_mrp.service.IMrp_workcenter_productivity_lossService;
import cn.ibizlab.odoo.core.odoo_mrp.filter.Mrp_workcenter_productivity_lossSearchContext;




@Slf4j
@Api(tags = {"Mrp_workcenter_productivity_loss" })
@RestController("odoo_mrp-mrp_workcenter_productivity_loss")
@RequestMapping("")
public class Mrp_workcenter_productivity_lossResource {

    @Autowired
    private IMrp_workcenter_productivity_lossService mrp_workcenter_productivity_lossService;

    @Autowired
    @Lazy
    private Mrp_workcenter_productivity_lossMapping mrp_workcenter_productivity_lossMapping;




    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Mrp_workcenter_productivity_loss" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/mrp_workcenter_productivity_losses")

    public ResponseEntity<Mrp_workcenter_productivity_lossDTO> create(@RequestBody Mrp_workcenter_productivity_lossDTO mrp_workcenter_productivity_lossdto) {
        Mrp_workcenter_productivity_loss domain = mrp_workcenter_productivity_lossMapping.toDomain(mrp_workcenter_productivity_lossdto);
		mrp_workcenter_productivity_lossService.create(domain);
        Mrp_workcenter_productivity_lossDTO dto = mrp_workcenter_productivity_lossMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Mrp_workcenter_productivity_loss" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/mrp_workcenter_productivity_losses/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Mrp_workcenter_productivity_lossDTO> mrp_workcenter_productivity_lossdtos) {
        mrp_workcenter_productivity_lossService.createBatch(mrp_workcenter_productivity_lossMapping.toDomain(mrp_workcenter_productivity_lossdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }










    @PreAuthorize("hasPermission('Remove',{#mrp_workcenter_productivity_loss_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Mrp_workcenter_productivity_loss" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/mrp_workcenter_productivity_losses/{mrp_workcenter_productivity_loss_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("mrp_workcenter_productivity_loss_id") Integer mrp_workcenter_productivity_loss_id) {
         return ResponseEntity.status(HttpStatus.OK).body(mrp_workcenter_productivity_lossService.remove(mrp_workcenter_productivity_loss_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Mrp_workcenter_productivity_loss" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/mrp_workcenter_productivity_losses/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        mrp_workcenter_productivity_lossService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission(#mrp_workcenter_productivity_loss_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Mrp_workcenter_productivity_loss" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/mrp_workcenter_productivity_losses/{mrp_workcenter_productivity_loss_id}")
    public ResponseEntity<Mrp_workcenter_productivity_lossDTO> get(@PathVariable("mrp_workcenter_productivity_loss_id") Integer mrp_workcenter_productivity_loss_id) {
        Mrp_workcenter_productivity_loss domain = mrp_workcenter_productivity_lossService.get(mrp_workcenter_productivity_loss_id);
        Mrp_workcenter_productivity_lossDTO dto = mrp_workcenter_productivity_lossMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }







    @PreAuthorize("hasPermission(#mrp_workcenter_productivity_loss_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Mrp_workcenter_productivity_loss" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/mrp_workcenter_productivity_losses/{mrp_workcenter_productivity_loss_id}")

    public ResponseEntity<Mrp_workcenter_productivity_lossDTO> update(@PathVariable("mrp_workcenter_productivity_loss_id") Integer mrp_workcenter_productivity_loss_id, @RequestBody Mrp_workcenter_productivity_lossDTO mrp_workcenter_productivity_lossdto) {
		Mrp_workcenter_productivity_loss domain = mrp_workcenter_productivity_lossMapping.toDomain(mrp_workcenter_productivity_lossdto);
        domain.setId(mrp_workcenter_productivity_loss_id);
		mrp_workcenter_productivity_lossService.update(domain);
		Mrp_workcenter_productivity_lossDTO dto = mrp_workcenter_productivity_lossMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#mrp_workcenter_productivity_loss_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Mrp_workcenter_productivity_loss" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/mrp_workcenter_productivity_losses/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Mrp_workcenter_productivity_lossDTO> mrp_workcenter_productivity_lossdtos) {
        mrp_workcenter_productivity_lossService.updateBatch(mrp_workcenter_productivity_lossMapping.toDomain(mrp_workcenter_productivity_lossdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Mrp_workcenter_productivity_loss" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/mrp_workcenter_productivity_losses/fetchdefault")
	public ResponseEntity<List<Mrp_workcenter_productivity_lossDTO>> fetchDefault(Mrp_workcenter_productivity_lossSearchContext context) {
        Page<Mrp_workcenter_productivity_loss> domains = mrp_workcenter_productivity_lossService.searchDefault(context) ;
        List<Mrp_workcenter_productivity_lossDTO> list = mrp_workcenter_productivity_lossMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Mrp_workcenter_productivity_loss" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/mrp_workcenter_productivity_losses/searchdefault")
	public ResponseEntity<Page<Mrp_workcenter_productivity_lossDTO>> searchDefault(Mrp_workcenter_productivity_lossSearchContext context) {
        Page<Mrp_workcenter_productivity_loss> domains = mrp_workcenter_productivity_lossService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(mrp_workcenter_productivity_lossMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Mrp_workcenter_productivity_loss getEntity(){
        return new Mrp_workcenter_productivity_loss();
    }

}
