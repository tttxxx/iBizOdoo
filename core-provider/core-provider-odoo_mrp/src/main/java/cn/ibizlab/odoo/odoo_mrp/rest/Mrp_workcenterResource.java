package cn.ibizlab.odoo.odoo_mrp.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_mrp.dto.*;
import cn.ibizlab.odoo.odoo_mrp.mapping.*;
import cn.ibizlab.odoo.core.odoo_mrp.domain.Mrp_workcenter;
import cn.ibizlab.odoo.core.odoo_mrp.service.IMrp_workcenterService;
import cn.ibizlab.odoo.core.odoo_mrp.filter.Mrp_workcenterSearchContext;




@Slf4j
@Api(tags = {"Mrp_workcenter" })
@RestController("odoo_mrp-mrp_workcenter")
@RequestMapping("")
public class Mrp_workcenterResource {

    @Autowired
    private IMrp_workcenterService mrp_workcenterService;

    @Autowired
    @Lazy
    private Mrp_workcenterMapping mrp_workcenterMapping;










    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Mrp_workcenter" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/mrp_workcenters")

    public ResponseEntity<Mrp_workcenterDTO> create(@RequestBody Mrp_workcenterDTO mrp_workcenterdto) {
        Mrp_workcenter domain = mrp_workcenterMapping.toDomain(mrp_workcenterdto);
		mrp_workcenterService.create(domain);
        Mrp_workcenterDTO dto = mrp_workcenterMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Mrp_workcenter" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/mrp_workcenters/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Mrp_workcenterDTO> mrp_workcenterdtos) {
        mrp_workcenterService.createBatch(mrp_workcenterMapping.toDomain(mrp_workcenterdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission(#mrp_workcenter_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Mrp_workcenter" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/mrp_workcenters/{mrp_workcenter_id}")
    public ResponseEntity<Mrp_workcenterDTO> get(@PathVariable("mrp_workcenter_id") Integer mrp_workcenter_id) {
        Mrp_workcenter domain = mrp_workcenterService.get(mrp_workcenter_id);
        Mrp_workcenterDTO dto = mrp_workcenterMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }







    @PreAuthorize("hasPermission(#mrp_workcenter_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Mrp_workcenter" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/mrp_workcenters/{mrp_workcenter_id}")

    public ResponseEntity<Mrp_workcenterDTO> update(@PathVariable("mrp_workcenter_id") Integer mrp_workcenter_id, @RequestBody Mrp_workcenterDTO mrp_workcenterdto) {
		Mrp_workcenter domain = mrp_workcenterMapping.toDomain(mrp_workcenterdto);
        domain.setId(mrp_workcenter_id);
		mrp_workcenterService.update(domain);
		Mrp_workcenterDTO dto = mrp_workcenterMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#mrp_workcenter_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Mrp_workcenter" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/mrp_workcenters/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Mrp_workcenterDTO> mrp_workcenterdtos) {
        mrp_workcenterService.updateBatch(mrp_workcenterMapping.toDomain(mrp_workcenterdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission('Remove',{#mrp_workcenter_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Mrp_workcenter" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/mrp_workcenters/{mrp_workcenter_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("mrp_workcenter_id") Integer mrp_workcenter_id) {
         return ResponseEntity.status(HttpStatus.OK).body(mrp_workcenterService.remove(mrp_workcenter_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Mrp_workcenter" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/mrp_workcenters/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        mrp_workcenterService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Mrp_workcenter" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/mrp_workcenters/fetchdefault")
	public ResponseEntity<List<Mrp_workcenterDTO>> fetchDefault(Mrp_workcenterSearchContext context) {
        Page<Mrp_workcenter> domains = mrp_workcenterService.searchDefault(context) ;
        List<Mrp_workcenterDTO> list = mrp_workcenterMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Mrp_workcenter" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/mrp_workcenters/searchdefault")
	public ResponseEntity<Page<Mrp_workcenterDTO>> searchDefault(Mrp_workcenterSearchContext context) {
        Page<Mrp_workcenter> domains = mrp_workcenterService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(mrp_workcenterMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Mrp_workcenter getEntity(){
        return new Mrp_workcenter();
    }

}
