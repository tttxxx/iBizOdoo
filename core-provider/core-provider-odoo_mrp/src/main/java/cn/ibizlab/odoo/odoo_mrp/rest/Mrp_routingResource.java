package cn.ibizlab.odoo.odoo_mrp.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_mrp.dto.*;
import cn.ibizlab.odoo.odoo_mrp.mapping.*;
import cn.ibizlab.odoo.core.odoo_mrp.domain.Mrp_routing;
import cn.ibizlab.odoo.core.odoo_mrp.service.IMrp_routingService;
import cn.ibizlab.odoo.core.odoo_mrp.filter.Mrp_routingSearchContext;




@Slf4j
@Api(tags = {"Mrp_routing" })
@RestController("odoo_mrp-mrp_routing")
@RequestMapping("")
public class Mrp_routingResource {

    @Autowired
    private IMrp_routingService mrp_routingService;

    @Autowired
    @Lazy
    private Mrp_routingMapping mrp_routingMapping;




    @PreAuthorize("hasPermission('Remove',{#mrp_routing_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Mrp_routing" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/mrp_routings/{mrp_routing_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("mrp_routing_id") Integer mrp_routing_id) {
         return ResponseEntity.status(HttpStatus.OK).body(mrp_routingService.remove(mrp_routing_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Mrp_routing" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/mrp_routings/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        mrp_routingService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission(#mrp_routing_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Mrp_routing" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/mrp_routings/{mrp_routing_id}")
    public ResponseEntity<Mrp_routingDTO> get(@PathVariable("mrp_routing_id") Integer mrp_routing_id) {
        Mrp_routing domain = mrp_routingService.get(mrp_routing_id);
        Mrp_routingDTO dto = mrp_routingMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }










    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Mrp_routing" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/mrp_routings")

    public ResponseEntity<Mrp_routingDTO> create(@RequestBody Mrp_routingDTO mrp_routingdto) {
        Mrp_routing domain = mrp_routingMapping.toDomain(mrp_routingdto);
		mrp_routingService.create(domain);
        Mrp_routingDTO dto = mrp_routingMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Mrp_routing" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/mrp_routings/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Mrp_routingDTO> mrp_routingdtos) {
        mrp_routingService.createBatch(mrp_routingMapping.toDomain(mrp_routingdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission(#mrp_routing_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Mrp_routing" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/mrp_routings/{mrp_routing_id}")

    public ResponseEntity<Mrp_routingDTO> update(@PathVariable("mrp_routing_id") Integer mrp_routing_id, @RequestBody Mrp_routingDTO mrp_routingdto) {
		Mrp_routing domain = mrp_routingMapping.toDomain(mrp_routingdto);
        domain.setId(mrp_routing_id);
		mrp_routingService.update(domain);
		Mrp_routingDTO dto = mrp_routingMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#mrp_routing_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Mrp_routing" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/mrp_routings/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Mrp_routingDTO> mrp_routingdtos) {
        mrp_routingService.updateBatch(mrp_routingMapping.toDomain(mrp_routingdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Mrp_routing" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/mrp_routings/fetchdefault")
	public ResponseEntity<List<Mrp_routingDTO>> fetchDefault(Mrp_routingSearchContext context) {
        Page<Mrp_routing> domains = mrp_routingService.searchDefault(context) ;
        List<Mrp_routingDTO> list = mrp_routingMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Mrp_routing" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/mrp_routings/searchdefault")
	public ResponseEntity<Page<Mrp_routingDTO>> searchDefault(Mrp_routingSearchContext context) {
        Page<Mrp_routing> domains = mrp_routingService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(mrp_routingMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Mrp_routing getEntity(){
        return new Mrp_routing();
    }

}
