package cn.ibizlab.odoo.odoo_mrp.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_mrp.dto.*;
import cn.ibizlab.odoo.odoo_mrp.mapping.*;
import cn.ibizlab.odoo.core.odoo_mrp.domain.Mrp_product_produce_line;
import cn.ibizlab.odoo.core.odoo_mrp.service.IMrp_product_produce_lineService;
import cn.ibizlab.odoo.core.odoo_mrp.filter.Mrp_product_produce_lineSearchContext;




@Slf4j
@Api(tags = {"Mrp_product_produce_line" })
@RestController("odoo_mrp-mrp_product_produce_line")
@RequestMapping("")
public class Mrp_product_produce_lineResource {

    @Autowired
    private IMrp_product_produce_lineService mrp_product_produce_lineService;

    @Autowired
    @Lazy
    private Mrp_product_produce_lineMapping mrp_product_produce_lineMapping;







    @PreAuthorize("hasPermission('Remove',{#mrp_product_produce_line_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Mrp_product_produce_line" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/mrp_product_produce_lines/{mrp_product_produce_line_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("mrp_product_produce_line_id") Integer mrp_product_produce_line_id) {
         return ResponseEntity.status(HttpStatus.OK).body(mrp_product_produce_lineService.remove(mrp_product_produce_line_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Mrp_product_produce_line" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/mrp_product_produce_lines/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        mrp_product_produce_lineService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }







    @PreAuthorize("hasPermission(#mrp_product_produce_line_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Mrp_product_produce_line" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/mrp_product_produce_lines/{mrp_product_produce_line_id}")
    public ResponseEntity<Mrp_product_produce_lineDTO> get(@PathVariable("mrp_product_produce_line_id") Integer mrp_product_produce_line_id) {
        Mrp_product_produce_line domain = mrp_product_produce_lineService.get(mrp_product_produce_line_id);
        Mrp_product_produce_lineDTO dto = mrp_product_produce_lineMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }







    @PreAuthorize("hasPermission(#mrp_product_produce_line_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Mrp_product_produce_line" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/mrp_product_produce_lines/{mrp_product_produce_line_id}")

    public ResponseEntity<Mrp_product_produce_lineDTO> update(@PathVariable("mrp_product_produce_line_id") Integer mrp_product_produce_line_id, @RequestBody Mrp_product_produce_lineDTO mrp_product_produce_linedto) {
		Mrp_product_produce_line domain = mrp_product_produce_lineMapping.toDomain(mrp_product_produce_linedto);
        domain.setId(mrp_product_produce_line_id);
		mrp_product_produce_lineService.update(domain);
		Mrp_product_produce_lineDTO dto = mrp_product_produce_lineMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#mrp_product_produce_line_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Mrp_product_produce_line" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/mrp_product_produce_lines/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Mrp_product_produce_lineDTO> mrp_product_produce_linedtos) {
        mrp_product_produce_lineService.updateBatch(mrp_product_produce_lineMapping.toDomain(mrp_product_produce_linedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Mrp_product_produce_line" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/mrp_product_produce_lines")

    public ResponseEntity<Mrp_product_produce_lineDTO> create(@RequestBody Mrp_product_produce_lineDTO mrp_product_produce_linedto) {
        Mrp_product_produce_line domain = mrp_product_produce_lineMapping.toDomain(mrp_product_produce_linedto);
		mrp_product_produce_lineService.create(domain);
        Mrp_product_produce_lineDTO dto = mrp_product_produce_lineMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Mrp_product_produce_line" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/mrp_product_produce_lines/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Mrp_product_produce_lineDTO> mrp_product_produce_linedtos) {
        mrp_product_produce_lineService.createBatch(mrp_product_produce_lineMapping.toDomain(mrp_product_produce_linedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Mrp_product_produce_line" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/mrp_product_produce_lines/fetchdefault")
	public ResponseEntity<List<Mrp_product_produce_lineDTO>> fetchDefault(Mrp_product_produce_lineSearchContext context) {
        Page<Mrp_product_produce_line> domains = mrp_product_produce_lineService.searchDefault(context) ;
        List<Mrp_product_produce_lineDTO> list = mrp_product_produce_lineMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Mrp_product_produce_line" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/mrp_product_produce_lines/searchdefault")
	public ResponseEntity<Page<Mrp_product_produce_lineDTO>> searchDefault(Mrp_product_produce_lineSearchContext context) {
        Page<Mrp_product_produce_line> domains = mrp_product_produce_lineService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(mrp_product_produce_lineMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Mrp_product_produce_line getEntity(){
        return new Mrp_product_produce_line();
    }

}
