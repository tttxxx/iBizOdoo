package cn.ibizlab.odoo.odoo_mrp.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;
import cn.ibizlab.odoo.util.domain.DTOBase;
import lombok.Data;

/**
 * 服务DTO对象[Mrp_bom_lineDTO]
 */
@Data
public class Mrp_bom_lineDTO extends DTOBase implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [ATTRIBUTE_VALUE_IDS]
     *
     */
    @JSONField(name = "attribute_value_ids")
    @JsonProperty("attribute_value_ids")
    private String attributeValueIds;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;

    /**
     * 属性 [PRODUCT_QTY]
     *
     */
    @JSONField(name = "product_qty")
    @JsonProperty("product_qty")
    private Double productQty;

    /**
     * 属性 [CHILD_LINE_IDS]
     *
     */
    @JSONField(name = "child_line_ids")
    @JsonProperty("child_line_ids")
    private String childLineIds;

    /**
     * 属性 [ID]
     *
     */
    @JSONField(name = "id")
    @JsonProperty("id")
    private Integer id;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    private String displayName;

    /**
     * 属性 [WRITE_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;

    /**
     * 属性 [CREATE_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;

    /**
     * 属性 [SEQUENCE]
     *
     */
    @JSONField(name = "sequence")
    @JsonProperty("sequence")
    private Integer sequence;

    /**
     * 属性 [VALID_PRODUCT_ATTRIBUTE_VALUE_IDS]
     *
     */
    @JSONField(name = "valid_product_attribute_value_ids")
    @JsonProperty("valid_product_attribute_value_ids")
    private String validProductAttributeValueIds;

    /**
     * 属性 [HAS_ATTACHMENTS]
     *
     */
    @JSONField(name = "has_attachments")
    @JsonProperty("has_attachments")
    private String hasAttachments;

    /**
     * 属性 [CHILD_BOM_ID]
     *
     */
    @JSONField(name = "child_bom_id")
    @JsonProperty("child_bom_id")
    private Integer childBomId;

    /**
     * 属性 [PRODUCT_TMPL_ID]
     *
     */
    @JSONField(name = "product_tmpl_id")
    @JsonProperty("product_tmpl_id")
    private Integer productTmplId;

    /**
     * 属性 [PRODUCT_UOM_ID_TEXT]
     *
     */
    @JSONField(name = "product_uom_id_text")
    @JsonProperty("product_uom_id_text")
    private String productUomIdText;

    /**
     * 属性 [ROUTING_ID_TEXT]
     *
     */
    @JSONField(name = "routing_id_text")
    @JsonProperty("routing_id_text")
    private String routingIdText;

    /**
     * 属性 [PARENT_PRODUCT_TMPL_ID]
     *
     */
    @JSONField(name = "parent_product_tmpl_id")
    @JsonProperty("parent_product_tmpl_id")
    private Integer parentProductTmplId;

    /**
     * 属性 [WRITE_UID_TEXT]
     *
     */
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    private String writeUidText;

    /**
     * 属性 [PRODUCT_ID_TEXT]
     *
     */
    @JSONField(name = "product_id_text")
    @JsonProperty("product_id_text")
    private String productIdText;

    /**
     * 属性 [CREATE_UID_TEXT]
     *
     */
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    private String createUidText;

    /**
     * 属性 [OPERATION_ID_TEXT]
     *
     */
    @JSONField(name = "operation_id_text")
    @JsonProperty("operation_id_text")
    private String operationIdText;

    /**
     * 属性 [OPERATION_ID]
     *
     */
    @JSONField(name = "operation_id")
    @JsonProperty("operation_id")
    private Integer operationId;

    /**
     * 属性 [PRODUCT_UOM_ID]
     *
     */
    @JSONField(name = "product_uom_id")
    @JsonProperty("product_uom_id")
    private Integer productUomId;

    /**
     * 属性 [BOM_ID]
     *
     */
    @JSONField(name = "bom_id")
    @JsonProperty("bom_id")
    private Integer bomId;

    /**
     * 属性 [CREATE_UID]
     *
     */
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    private Integer createUid;

    /**
     * 属性 [WRITE_UID]
     *
     */
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    private Integer writeUid;

    /**
     * 属性 [ROUTING_ID]
     *
     */
    @JSONField(name = "routing_id")
    @JsonProperty("routing_id")
    private Integer routingId;

    /**
     * 属性 [PRODUCT_ID]
     *
     */
    @JSONField(name = "product_id")
    @JsonProperty("product_id")
    private Integer productId;


    /**
     * 设置 [PRODUCT_QTY]
     */
    public void setProductQty(Double  productQty){
        this.productQty = productQty ;
        this.modify("product_qty",productQty);
    }

    /**
     * 设置 [SEQUENCE]
     */
    public void setSequence(Integer  sequence){
        this.sequence = sequence ;
        this.modify("sequence",sequence);
    }

    /**
     * 设置 [OPERATION_ID]
     */
    public void setOperationId(Integer  operationId){
        this.operationId = operationId ;
        this.modify("operation_id",operationId);
    }

    /**
     * 设置 [PRODUCT_UOM_ID]
     */
    public void setProductUomId(Integer  productUomId){
        this.productUomId = productUomId ;
        this.modify("product_uom_id",productUomId);
    }

    /**
     * 设置 [BOM_ID]
     */
    public void setBomId(Integer  bomId){
        this.bomId = bomId ;
        this.modify("bom_id",bomId);
    }

    /**
     * 设置 [ROUTING_ID]
     */
    public void setRoutingId(Integer  routingId){
        this.routingId = routingId ;
        this.modify("routing_id",routingId);
    }

    /**
     * 设置 [PRODUCT_ID]
     */
    public void setProductId(Integer  productId){
        this.productId = productId ;
        this.modify("product_id",productId);
    }


}

