package cn.ibizlab.odoo.odoo_mrp.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;
import cn.ibizlab.odoo.util.domain.DTOBase;
import lombok.Data;

/**
 * 服务DTO对象[Mrp_documentDTO]
 */
@Data
public class Mrp_documentDTO extends DTOBase implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [CREATE_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;

    /**
     * 属性 [ACTIVE]
     *
     */
    @JSONField(name = "active")
    @JsonProperty("active")
    private String active;

    /**
     * 属性 [RES_FIELD]
     *
     */
    @JSONField(name = "res_field")
    @JsonProperty("res_field")
    private String resField;

    /**
     * 属性 [RES_NAME]
     *
     */
    @JSONField(name = "res_name")
    @JsonProperty("res_name")
    private String resName;

    /**
     * 属性 [RES_MODEL_NAME]
     *
     */
    @JSONField(name = "res_model_name")
    @JsonProperty("res_model_name")
    private String resModelName;

    /**
     * 属性 [DATAS_FNAME]
     *
     */
    @JSONField(name = "datas_fname")
    @JsonProperty("datas_fname")
    private String datasFname;

    /**
     * 属性 [THEME_TEMPLATE_ID]
     *
     */
    @JSONField(name = "theme_template_id")
    @JsonProperty("theme_template_id")
    private Integer themeTemplateId;

    /**
     * 属性 [MIMETYPE]
     *
     */
    @JSONField(name = "mimetype")
    @JsonProperty("mimetype")
    private String mimetype;

    /**
     * 属性 [RES_ID]
     *
     */
    @JSONField(name = "res_id")
    @JsonProperty("res_id")
    private Integer resId;

    /**
     * 属性 [STORE_FNAME]
     *
     */
    @JSONField(name = "store_fname")
    @JsonProperty("store_fname")
    private String storeFname;

    /**
     * 属性 [WRITE_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;

    /**
     * 属性 [LOCAL_URL]
     *
     */
    @JSONField(name = "local_url")
    @JsonProperty("local_url")
    private String localUrl;

    /**
     * 属性 [KEY]
     *
     */
    @JSONField(name = "key")
    @JsonProperty("key")
    private String key;

    /**
     * 属性 [IBIZPUBLIC]
     *
     */
    @JSONField(name = "ibizpublic")
    @JsonProperty("ibizpublic")
    private String ibizpublic;

    /**
     * 属性 [RES_MODEL]
     *
     */
    @JSONField(name = "res_model")
    @JsonProperty("res_model")
    private String resModel;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;

    /**
     * 属性 [THUMBNAIL]
     *
     */
    @JSONField(name = "thumbnail")
    @JsonProperty("thumbnail")
    private byte[] thumbnail;

    /**
     * 属性 [URL]
     *
     */
    @JSONField(name = "url")
    @JsonProperty("url")
    private String url;

    /**
     * 属性 [FILE_SIZE]
     *
     */
    @JSONField(name = "file_size")
    @JsonProperty("file_size")
    private Integer fileSize;

    /**
     * 属性 [ACCESS_TOKEN]
     *
     */
    @JSONField(name = "access_token")
    @JsonProperty("access_token")
    private String accessToken;

    /**
     * 属性 [COMPANY_ID]
     *
     */
    @JSONField(name = "company_id")
    @JsonProperty("company_id")
    private Integer companyId;

    /**
     * 属性 [IR_ATTACHMENT_ID]
     *
     */
    @JSONField(name = "ir_attachment_id")
    @JsonProperty("ir_attachment_id")
    private Integer irAttachmentId;

    /**
     * 属性 [ID]
     *
     */
    @JSONField(name = "id")
    @JsonProperty("id")
    private Integer id;

    /**
     * 属性 [TYPE]
     *
     */
    @JSONField(name = "type")
    @JsonProperty("type")
    private String type;

    /**
     * 属性 [CHECKSUM]
     *
     */
    @JSONField(name = "checksum")
    @JsonProperty("checksum")
    private String checksum;

    /**
     * 属性 [DB_DATAS]
     *
     */
    @JSONField(name = "db_datas")
    @JsonProperty("db_datas")
    private byte[] dbDatas;

    /**
     * 属性 [INDEX_CONTENT]
     *
     */
    @JSONField(name = "index_content")
    @JsonProperty("index_content")
    private String indexContent;

    /**
     * 属性 [DESCRIPTION]
     *
     */
    @JSONField(name = "description")
    @JsonProperty("description")
    private String description;

    /**
     * 属性 [NAME]
     *
     */
    @JSONField(name = "name")
    @JsonProperty("name")
    private String name;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    private String displayName;

    /**
     * 属性 [WEBSITE_URL]
     *
     */
    @JSONField(name = "website_url")
    @JsonProperty("website_url")
    private String websiteUrl;

    /**
     * 属性 [WEBSITE_ID]
     *
     */
    @JSONField(name = "website_id")
    @JsonProperty("website_id")
    private Integer websiteId;

    /**
     * 属性 [DATAS]
     *
     */
    @JSONField(name = "datas")
    @JsonProperty("datas")
    private byte[] datas;

    /**
     * 属性 [PRIORITY]
     *
     */
    @JSONField(name = "priority")
    @JsonProperty("priority")
    private String priority;

    /**
     * 属性 [WRITE_UID_TEXT]
     *
     */
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    private String writeUidText;

    /**
     * 属性 [CREATE_UID_TEXT]
     *
     */
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    private String createUidText;

    /**
     * 属性 [CREATE_UID]
     *
     */
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    private Integer createUid;

    /**
     * 属性 [WRITE_UID]
     *
     */
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    private Integer writeUid;


    /**
     * 设置 [ACTIVE]
     */
    public void setActive(String  active){
        this.active = active ;
        this.modify("active",active);
    }

    /**
     * 设置 [IR_ATTACHMENT_ID]
     */
    public void setIrAttachmentId(Integer  irAttachmentId){
        this.irAttachmentId = irAttachmentId ;
        this.modify("ir_attachment_id",irAttachmentId);
    }

    /**
     * 设置 [PRIORITY]
     */
    public void setPriority(String  priority){
        this.priority = priority ;
        this.modify("priority",priority);
    }


}

