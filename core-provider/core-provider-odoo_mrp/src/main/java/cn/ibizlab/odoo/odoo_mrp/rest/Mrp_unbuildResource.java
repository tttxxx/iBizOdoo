package cn.ibizlab.odoo.odoo_mrp.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_mrp.dto.*;
import cn.ibizlab.odoo.odoo_mrp.mapping.*;
import cn.ibizlab.odoo.core.odoo_mrp.domain.Mrp_unbuild;
import cn.ibizlab.odoo.core.odoo_mrp.service.IMrp_unbuildService;
import cn.ibizlab.odoo.core.odoo_mrp.filter.Mrp_unbuildSearchContext;




@Slf4j
@Api(tags = {"Mrp_unbuild" })
@RestController("odoo_mrp-mrp_unbuild")
@RequestMapping("")
public class Mrp_unbuildResource {

    @Autowired
    private IMrp_unbuildService mrp_unbuildService;

    @Autowired
    @Lazy
    private Mrp_unbuildMapping mrp_unbuildMapping;




    @PreAuthorize("hasPermission(#mrp_unbuild_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Mrp_unbuild" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/mrp_unbuilds/{mrp_unbuild_id}")

    public ResponseEntity<Mrp_unbuildDTO> update(@PathVariable("mrp_unbuild_id") Integer mrp_unbuild_id, @RequestBody Mrp_unbuildDTO mrp_unbuilddto) {
		Mrp_unbuild domain = mrp_unbuildMapping.toDomain(mrp_unbuilddto);
        domain.setId(mrp_unbuild_id);
		mrp_unbuildService.update(domain);
		Mrp_unbuildDTO dto = mrp_unbuildMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#mrp_unbuild_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Mrp_unbuild" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/mrp_unbuilds/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Mrp_unbuildDTO> mrp_unbuilddtos) {
        mrp_unbuildService.updateBatch(mrp_unbuildMapping.toDomain(mrp_unbuilddtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }







    @PreAuthorize("hasPermission('Remove',{#mrp_unbuild_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Mrp_unbuild" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/mrp_unbuilds/{mrp_unbuild_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("mrp_unbuild_id") Integer mrp_unbuild_id) {
         return ResponseEntity.status(HttpStatus.OK).body(mrp_unbuildService.remove(mrp_unbuild_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Mrp_unbuild" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/mrp_unbuilds/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        mrp_unbuildService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission(#mrp_unbuild_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Mrp_unbuild" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/mrp_unbuilds/{mrp_unbuild_id}")
    public ResponseEntity<Mrp_unbuildDTO> get(@PathVariable("mrp_unbuild_id") Integer mrp_unbuild_id) {
        Mrp_unbuild domain = mrp_unbuildService.get(mrp_unbuild_id);
        Mrp_unbuildDTO dto = mrp_unbuildMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }










    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Mrp_unbuild" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/mrp_unbuilds")

    public ResponseEntity<Mrp_unbuildDTO> create(@RequestBody Mrp_unbuildDTO mrp_unbuilddto) {
        Mrp_unbuild domain = mrp_unbuildMapping.toDomain(mrp_unbuilddto);
		mrp_unbuildService.create(domain);
        Mrp_unbuildDTO dto = mrp_unbuildMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Mrp_unbuild" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/mrp_unbuilds/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Mrp_unbuildDTO> mrp_unbuilddtos) {
        mrp_unbuildService.createBatch(mrp_unbuildMapping.toDomain(mrp_unbuilddtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Mrp_unbuild" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/mrp_unbuilds/fetchdefault")
	public ResponseEntity<List<Mrp_unbuildDTO>> fetchDefault(Mrp_unbuildSearchContext context) {
        Page<Mrp_unbuild> domains = mrp_unbuildService.searchDefault(context) ;
        List<Mrp_unbuildDTO> list = mrp_unbuildMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Mrp_unbuild" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/mrp_unbuilds/searchdefault")
	public ResponseEntity<Page<Mrp_unbuildDTO>> searchDefault(Mrp_unbuildSearchContext context) {
        Page<Mrp_unbuild> domains = mrp_unbuildService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(mrp_unbuildMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Mrp_unbuild getEntity(){
        return new Mrp_unbuild();
    }

}
