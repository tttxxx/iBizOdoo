package cn.ibizlab.odoo.odoo_product.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_product.dto.*;
import cn.ibizlab.odoo.odoo_product.mapping.*;
import cn.ibizlab.odoo.core.odoo_product.domain.Product_attribute_value;
import cn.ibizlab.odoo.core.odoo_product.service.IProduct_attribute_valueService;
import cn.ibizlab.odoo.core.odoo_product.filter.Product_attribute_valueSearchContext;




@Slf4j
@Api(tags = {"Product_attribute_value" })
@RestController("odoo_product-product_attribute_value")
@RequestMapping("")
public class Product_attribute_valueResource {

    @Autowired
    private IProduct_attribute_valueService product_attribute_valueService;

    @Autowired
    @Lazy
    private Product_attribute_valueMapping product_attribute_valueMapping;




    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Product_attribute_value" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/product_attribute_values")

    public ResponseEntity<Product_attribute_valueDTO> create(@RequestBody Product_attribute_valueDTO product_attribute_valuedto) {
        Product_attribute_value domain = product_attribute_valueMapping.toDomain(product_attribute_valuedto);
		product_attribute_valueService.create(domain);
        Product_attribute_valueDTO dto = product_attribute_valueMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Product_attribute_value" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/product_attribute_values/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Product_attribute_valueDTO> product_attribute_valuedtos) {
        product_attribute_valueService.createBatch(product_attribute_valueMapping.toDomain(product_attribute_valuedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission(#product_attribute_value_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Product_attribute_value" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/product_attribute_values/{product_attribute_value_id}")
    public ResponseEntity<Product_attribute_valueDTO> get(@PathVariable("product_attribute_value_id") Integer product_attribute_value_id) {
        Product_attribute_value domain = product_attribute_valueService.get(product_attribute_value_id);
        Product_attribute_valueDTO dto = product_attribute_valueMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }










    @PreAuthorize("hasPermission(#product_attribute_value_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Product_attribute_value" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/product_attribute_values/{product_attribute_value_id}")

    public ResponseEntity<Product_attribute_valueDTO> update(@PathVariable("product_attribute_value_id") Integer product_attribute_value_id, @RequestBody Product_attribute_valueDTO product_attribute_valuedto) {
		Product_attribute_value domain = product_attribute_valueMapping.toDomain(product_attribute_valuedto);
        domain.setId(product_attribute_value_id);
		product_attribute_valueService.update(domain);
		Product_attribute_valueDTO dto = product_attribute_valueMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#product_attribute_value_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Product_attribute_value" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/product_attribute_values/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Product_attribute_valueDTO> product_attribute_valuedtos) {
        product_attribute_valueService.updateBatch(product_attribute_valueMapping.toDomain(product_attribute_valuedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }







    @PreAuthorize("hasPermission('Remove',{#product_attribute_value_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Product_attribute_value" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/product_attribute_values/{product_attribute_value_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("product_attribute_value_id") Integer product_attribute_value_id) {
         return ResponseEntity.status(HttpStatus.OK).body(product_attribute_valueService.remove(product_attribute_value_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Product_attribute_value" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/product_attribute_values/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        product_attribute_valueService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Product_attribute_value" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/product_attribute_values/fetchdefault")
	public ResponseEntity<List<Product_attribute_valueDTO>> fetchDefault(Product_attribute_valueSearchContext context) {
        Page<Product_attribute_value> domains = product_attribute_valueService.searchDefault(context) ;
        List<Product_attribute_valueDTO> list = product_attribute_valueMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Product_attribute_value" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/product_attribute_values/searchdefault")
	public ResponseEntity<Page<Product_attribute_valueDTO>> searchDefault(Product_attribute_valueSearchContext context) {
        Page<Product_attribute_value> domains = product_attribute_valueService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(product_attribute_valueMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Product_attribute_value getEntity(){
        return new Product_attribute_value();
    }

}
