package cn.ibizlab.odoo.odoo_product.config;

import org.springframework.context.annotation.ComponentScan;

@ComponentScan("cn.ibizlab.odoo.odoo_product")
public class odoo_productRestConfiguration {

}
