package cn.ibizlab.odoo.odoo_product.mapping;

import org.mapstruct.*;
import cn.ibizlab.odoo.core.odoo_product.domain.Product_price_list;
import cn.ibizlab.odoo.odoo_product.dto.Product_price_listDTO;
import cn.ibizlab.odoo.util.domain.MappingBase;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring", uses = {},
    nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
    nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface Product_price_listMapping extends MappingBase<Product_price_listDTO, Product_price_list> {


}

