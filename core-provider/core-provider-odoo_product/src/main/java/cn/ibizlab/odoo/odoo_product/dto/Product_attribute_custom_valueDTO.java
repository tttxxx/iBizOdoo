package cn.ibizlab.odoo.odoo_product.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;
import cn.ibizlab.odoo.util.domain.DTOBase;
import lombok.Data;

/**
 * 服务DTO对象[Product_attribute_custom_valueDTO]
 */
@Data
public class Product_attribute_custom_valueDTO extends DTOBase implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [CREATE_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    private String displayName;

    /**
     * 属性 [ID]
     *
     */
    @JSONField(name = "id")
    @JsonProperty("id")
    private Integer id;

    /**
     * 属性 [CUSTOM_VALUE]
     *
     */
    @JSONField(name = "custom_value")
    @JsonProperty("custom_value")
    private String customValue;

    /**
     * 属性 [WRITE_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;

    /**
     * 属性 [WRITE_UID_TEXT]
     *
     */
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    private String writeUidText;

    /**
     * 属性 [CREATE_UID_TEXT]
     *
     */
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    private String createUidText;

    /**
     * 属性 [SALE_ORDER_LINE_ID_TEXT]
     *
     */
    @JSONField(name = "sale_order_line_id_text")
    @JsonProperty("sale_order_line_id_text")
    private String saleOrderLineIdText;

    /**
     * 属性 [ATTRIBUTE_VALUE_ID_TEXT]
     *
     */
    @JSONField(name = "attribute_value_id_text")
    @JsonProperty("attribute_value_id_text")
    private String attributeValueIdText;

    /**
     * 属性 [CREATE_UID]
     *
     */
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    private Integer createUid;

    /**
     * 属性 [WRITE_UID]
     *
     */
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    private Integer writeUid;

    /**
     * 属性 [SALE_ORDER_LINE_ID]
     *
     */
    @JSONField(name = "sale_order_line_id")
    @JsonProperty("sale_order_line_id")
    private Integer saleOrderLineId;

    /**
     * 属性 [ATTRIBUTE_VALUE_ID]
     *
     */
    @JSONField(name = "attribute_value_id")
    @JsonProperty("attribute_value_id")
    private Integer attributeValueId;


    /**
     * 设置 [CUSTOM_VALUE]
     */
    public void setCustomValue(String  customValue){
        this.customValue = customValue ;
        this.modify("custom_value",customValue);
    }

    /**
     * 设置 [SALE_ORDER_LINE_ID]
     */
    public void setSaleOrderLineId(Integer  saleOrderLineId){
        this.saleOrderLineId = saleOrderLineId ;
        this.modify("sale_order_line_id",saleOrderLineId);
    }

    /**
     * 设置 [ATTRIBUTE_VALUE_ID]
     */
    public void setAttributeValueId(Integer  attributeValueId){
        this.attributeValueId = attributeValueId ;
        this.modify("attribute_value_id",attributeValueId);
    }


}

