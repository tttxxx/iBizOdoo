package cn.ibizlab.odoo.odoo_product.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_product.dto.*;
import cn.ibizlab.odoo.odoo_product.mapping.*;
import cn.ibizlab.odoo.core.odoo_product.domain.Product_removal;
import cn.ibizlab.odoo.core.odoo_product.service.IProduct_removalService;
import cn.ibizlab.odoo.core.odoo_product.filter.Product_removalSearchContext;




@Slf4j
@Api(tags = {"Product_removal" })
@RestController("odoo_product-product_removal")
@RequestMapping("")
public class Product_removalResource {

    @Autowired
    private IProduct_removalService product_removalService;

    @Autowired
    @Lazy
    private Product_removalMapping product_removalMapping;




    @PreAuthorize("hasPermission('Remove',{#product_removal_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Product_removal" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/product_removals/{product_removal_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("product_removal_id") Integer product_removal_id) {
         return ResponseEntity.status(HttpStatus.OK).body(product_removalService.remove(product_removal_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Product_removal" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/product_removals/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        product_removalService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }










    @PreAuthorize("hasPermission(#product_removal_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Product_removal" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/product_removals/{product_removal_id}")

    public ResponseEntity<Product_removalDTO> update(@PathVariable("product_removal_id") Integer product_removal_id, @RequestBody Product_removalDTO product_removaldto) {
		Product_removal domain = product_removalMapping.toDomain(product_removaldto);
        domain.setId(product_removal_id);
		product_removalService.update(domain);
		Product_removalDTO dto = product_removalMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#product_removal_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Product_removal" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/product_removals/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Product_removalDTO> product_removaldtos) {
        product_removalService.updateBatch(product_removalMapping.toDomain(product_removaldtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Product_removal" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/product_removals")

    public ResponseEntity<Product_removalDTO> create(@RequestBody Product_removalDTO product_removaldto) {
        Product_removal domain = product_removalMapping.toDomain(product_removaldto);
		product_removalService.create(domain);
        Product_removalDTO dto = product_removalMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Product_removal" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/product_removals/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Product_removalDTO> product_removaldtos) {
        product_removalService.createBatch(product_removalMapping.toDomain(product_removaldtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }







    @PreAuthorize("hasPermission(#product_removal_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Product_removal" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/product_removals/{product_removal_id}")
    public ResponseEntity<Product_removalDTO> get(@PathVariable("product_removal_id") Integer product_removal_id) {
        Product_removal domain = product_removalService.get(product_removal_id);
        Product_removalDTO dto = product_removalMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Product_removal" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/product_removals/fetchdefault")
	public ResponseEntity<List<Product_removalDTO>> fetchDefault(Product_removalSearchContext context) {
        Page<Product_removal> domains = product_removalService.searchDefault(context) ;
        List<Product_removalDTO> list = product_removalMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Product_removal" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/product_removals/searchdefault")
	public ResponseEntity<Page<Product_removalDTO>> searchDefault(Product_removalSearchContext context) {
        Page<Product_removal> domains = product_removalService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(product_removalMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Product_removal getEntity(){
        return new Product_removal();
    }

}
