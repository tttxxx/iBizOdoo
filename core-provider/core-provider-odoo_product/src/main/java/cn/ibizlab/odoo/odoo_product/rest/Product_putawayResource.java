package cn.ibizlab.odoo.odoo_product.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_product.dto.*;
import cn.ibizlab.odoo.odoo_product.mapping.*;
import cn.ibizlab.odoo.core.odoo_product.domain.Product_putaway;
import cn.ibizlab.odoo.core.odoo_product.service.IProduct_putawayService;
import cn.ibizlab.odoo.core.odoo_product.filter.Product_putawaySearchContext;




@Slf4j
@Api(tags = {"Product_putaway" })
@RestController("odoo_product-product_putaway")
@RequestMapping("")
public class Product_putawayResource {

    @Autowired
    private IProduct_putawayService product_putawayService;

    @Autowired
    @Lazy
    private Product_putawayMapping product_putawayMapping;




    @PreAuthorize("hasPermission('Remove',{#product_putaway_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Product_putaway" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/product_putaways/{product_putaway_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("product_putaway_id") Integer product_putaway_id) {
         return ResponseEntity.status(HttpStatus.OK).body(product_putawayService.remove(product_putaway_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Product_putaway" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/product_putaways/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        product_putawayService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }










    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Product_putaway" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/product_putaways")

    public ResponseEntity<Product_putawayDTO> create(@RequestBody Product_putawayDTO product_putawaydto) {
        Product_putaway domain = product_putawayMapping.toDomain(product_putawaydto);
		product_putawayService.create(domain);
        Product_putawayDTO dto = product_putawayMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Product_putaway" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/product_putaways/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Product_putawayDTO> product_putawaydtos) {
        product_putawayService.createBatch(product_putawayMapping.toDomain(product_putawaydtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }







    @PreAuthorize("hasPermission(#product_putaway_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Product_putaway" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/product_putaways/{product_putaway_id}")
    public ResponseEntity<Product_putawayDTO> get(@PathVariable("product_putaway_id") Integer product_putaway_id) {
        Product_putaway domain = product_putawayService.get(product_putaway_id);
        Product_putawayDTO dto = product_putawayMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }




    @PreAuthorize("hasPermission(#product_putaway_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Product_putaway" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/product_putaways/{product_putaway_id}")

    public ResponseEntity<Product_putawayDTO> update(@PathVariable("product_putaway_id") Integer product_putaway_id, @RequestBody Product_putawayDTO product_putawaydto) {
		Product_putaway domain = product_putawayMapping.toDomain(product_putawaydto);
        domain.setId(product_putaway_id);
		product_putawayService.update(domain);
		Product_putawayDTO dto = product_putawayMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#product_putaway_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Product_putaway" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/product_putaways/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Product_putawayDTO> product_putawaydtos) {
        product_putawayService.updateBatch(product_putawayMapping.toDomain(product_putawaydtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Product_putaway" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/product_putaways/fetchdefault")
	public ResponseEntity<List<Product_putawayDTO>> fetchDefault(Product_putawaySearchContext context) {
        Page<Product_putaway> domains = product_putawayService.searchDefault(context) ;
        List<Product_putawayDTO> list = product_putawayMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Product_putaway" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/product_putaways/searchdefault")
	public ResponseEntity<Page<Product_putawayDTO>> searchDefault(Product_putawaySearchContext context) {
        Page<Product_putaway> domains = product_putawayService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(product_putawayMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Product_putaway getEntity(){
        return new Product_putaway();
    }

}
