package cn.ibizlab.odoo.odoo_product.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_product.dto.*;
import cn.ibizlab.odoo.odoo_product.mapping.*;
import cn.ibizlab.odoo.core.odoo_product.domain.Product_public_category;
import cn.ibizlab.odoo.core.odoo_product.service.IProduct_public_categoryService;
import cn.ibizlab.odoo.core.odoo_product.filter.Product_public_categorySearchContext;




@Slf4j
@Api(tags = {"Product_public_category" })
@RestController("odoo_product-product_public_category")
@RequestMapping("")
public class Product_public_categoryResource {

    @Autowired
    private IProduct_public_categoryService product_public_categoryService;

    @Autowired
    @Lazy
    private Product_public_categoryMapping product_public_categoryMapping;




    @PreAuthorize("hasPermission(#product_public_category_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Product_public_category" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/product_public_categories/{product_public_category_id}")
    public ResponseEntity<Product_public_categoryDTO> get(@PathVariable("product_public_category_id") Integer product_public_category_id) {
        Product_public_category domain = product_public_categoryService.get(product_public_category_id);
        Product_public_categoryDTO dto = product_public_categoryMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }










    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Product_public_category" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/product_public_categories")

    public ResponseEntity<Product_public_categoryDTO> create(@RequestBody Product_public_categoryDTO product_public_categorydto) {
        Product_public_category domain = product_public_categoryMapping.toDomain(product_public_categorydto);
		product_public_categoryService.create(domain);
        Product_public_categoryDTO dto = product_public_categoryMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Product_public_category" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/product_public_categories/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Product_public_categoryDTO> product_public_categorydtos) {
        product_public_categoryService.createBatch(product_public_categoryMapping.toDomain(product_public_categorydtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }







    @PreAuthorize("hasPermission('Remove',{#product_public_category_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Product_public_category" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/product_public_categories/{product_public_category_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("product_public_category_id") Integer product_public_category_id) {
         return ResponseEntity.status(HttpStatus.OK).body(product_public_categoryService.remove(product_public_category_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Product_public_category" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/product_public_categories/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        product_public_categoryService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission(#product_public_category_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Product_public_category" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/product_public_categories/{product_public_category_id}")

    public ResponseEntity<Product_public_categoryDTO> update(@PathVariable("product_public_category_id") Integer product_public_category_id, @RequestBody Product_public_categoryDTO product_public_categorydto) {
		Product_public_category domain = product_public_categoryMapping.toDomain(product_public_categorydto);
        domain.setId(product_public_category_id);
		product_public_categoryService.update(domain);
		Product_public_categoryDTO dto = product_public_categoryMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#product_public_category_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Product_public_category" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/product_public_categories/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Product_public_categoryDTO> product_public_categorydtos) {
        product_public_categoryService.updateBatch(product_public_categoryMapping.toDomain(product_public_categorydtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Product_public_category" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/product_public_categories/fetchdefault")
	public ResponseEntity<List<Product_public_categoryDTO>> fetchDefault(Product_public_categorySearchContext context) {
        Page<Product_public_category> domains = product_public_categoryService.searchDefault(context) ;
        List<Product_public_categoryDTO> list = product_public_categoryMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Product_public_category" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/product_public_categories/searchdefault")
	public ResponseEntity<Page<Product_public_categoryDTO>> searchDefault(Product_public_categorySearchContext context) {
        Page<Product_public_category> domains = product_public_categoryService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(product_public_categoryMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Product_public_category getEntity(){
        return new Product_public_category();
    }

}
