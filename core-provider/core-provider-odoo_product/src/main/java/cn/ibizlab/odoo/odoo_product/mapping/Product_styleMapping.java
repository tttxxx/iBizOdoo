package cn.ibizlab.odoo.odoo_product.mapping;

import org.mapstruct.*;
import cn.ibizlab.odoo.core.odoo_product.domain.Product_style;
import cn.ibizlab.odoo.odoo_product.dto.Product_styleDTO;
import cn.ibizlab.odoo.util.domain.MappingBase;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring", uses = {},
    nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
    nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface Product_styleMapping extends MappingBase<Product_styleDTO, Product_style> {


}

