package cn.ibizlab.odoo.odoo_product.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_product.dto.*;
import cn.ibizlab.odoo.odoo_product.mapping.*;
import cn.ibizlab.odoo.core.odoo_product.domain.Product_replenish;
import cn.ibizlab.odoo.core.odoo_product.service.IProduct_replenishService;
import cn.ibizlab.odoo.core.odoo_product.filter.Product_replenishSearchContext;




@Slf4j
@Api(tags = {"Product_replenish" })
@RestController("odoo_product-product_replenish")
@RequestMapping("")
public class Product_replenishResource {

    @Autowired
    private IProduct_replenishService product_replenishService;

    @Autowired
    @Lazy
    private Product_replenishMapping product_replenishMapping;







    @PreAuthorize("hasPermission(#product_replenish_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Product_replenish" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/product_replenishes/{product_replenish_id}")
    public ResponseEntity<Product_replenishDTO> get(@PathVariable("product_replenish_id") Integer product_replenish_id) {
        Product_replenish domain = product_replenishService.get(product_replenish_id);
        Product_replenishDTO dto = product_replenishMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }




    @PreAuthorize("hasPermission('Remove',{#product_replenish_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Product_replenish" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/product_replenishes/{product_replenish_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("product_replenish_id") Integer product_replenish_id) {
         return ResponseEntity.status(HttpStatus.OK).body(product_replenishService.remove(product_replenish_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Product_replenish" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/product_replenishes/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        product_replenishService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Product_replenish" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/product_replenishes")

    public ResponseEntity<Product_replenishDTO> create(@RequestBody Product_replenishDTO product_replenishdto) {
        Product_replenish domain = product_replenishMapping.toDomain(product_replenishdto);
		product_replenishService.create(domain);
        Product_replenishDTO dto = product_replenishMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Product_replenish" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/product_replenishes/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Product_replenishDTO> product_replenishdtos) {
        product_replenishService.createBatch(product_replenishMapping.toDomain(product_replenishdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission(#product_replenish_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Product_replenish" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/product_replenishes/{product_replenish_id}")

    public ResponseEntity<Product_replenishDTO> update(@PathVariable("product_replenish_id") Integer product_replenish_id, @RequestBody Product_replenishDTO product_replenishdto) {
		Product_replenish domain = product_replenishMapping.toDomain(product_replenishdto);
        domain.setId(product_replenish_id);
		product_replenishService.update(domain);
		Product_replenishDTO dto = product_replenishMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#product_replenish_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Product_replenish" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/product_replenishes/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Product_replenishDTO> product_replenishdtos) {
        product_replenishService.updateBatch(product_replenishMapping.toDomain(product_replenishdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }







    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Product_replenish" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/product_replenishes/fetchdefault")
	public ResponseEntity<List<Product_replenishDTO>> fetchDefault(Product_replenishSearchContext context) {
        Page<Product_replenish> domains = product_replenishService.searchDefault(context) ;
        List<Product_replenishDTO> list = product_replenishMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Product_replenish" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/product_replenishes/searchdefault")
	public ResponseEntity<Page<Product_replenishDTO>> searchDefault(Product_replenishSearchContext context) {
        Page<Product_replenish> domains = product_replenishService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(product_replenishMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Product_replenish getEntity(){
        return new Product_replenish();
    }

}
