package cn.ibizlab.odoo.odoo_product.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_product.dto.*;
import cn.ibizlab.odoo.odoo_product.mapping.*;
import cn.ibizlab.odoo.core.odoo_product.domain.Product_category;
import cn.ibizlab.odoo.core.odoo_product.service.IProduct_categoryService;
import cn.ibizlab.odoo.core.odoo_product.filter.Product_categorySearchContext;




@Slf4j
@Api(tags = {"Product_category" })
@RestController("odoo_product-product_category")
@RequestMapping("")
public class Product_categoryResource {

    @Autowired
    private IProduct_categoryService product_categoryService;

    @Autowired
    @Lazy
    private Product_categoryMapping product_categoryMapping;




    @PreAuthorize("hasPermission('Remove',{#product_category_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Product_category" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/product_categories/{product_category_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("product_category_id") Integer product_category_id) {
         return ResponseEntity.status(HttpStatus.OK).body(product_categoryService.remove(product_category_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Product_category" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/product_categories/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        product_categoryService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Product_category" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/product_categories")

    public ResponseEntity<Product_categoryDTO> create(@RequestBody Product_categoryDTO product_categorydto) {
        Product_category domain = product_categoryMapping.toDomain(product_categorydto);
		product_categoryService.create(domain);
        Product_categoryDTO dto = product_categoryMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Product_category" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/product_categories/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Product_categoryDTO> product_categorydtos) {
        product_categoryService.createBatch(product_categoryMapping.toDomain(product_categorydtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }










    @PreAuthorize("hasPermission(#product_category_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Product_category" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/product_categories/{product_category_id}")

    public ResponseEntity<Product_categoryDTO> update(@PathVariable("product_category_id") Integer product_category_id, @RequestBody Product_categoryDTO product_categorydto) {
		Product_category domain = product_categoryMapping.toDomain(product_categorydto);
        domain.setId(product_category_id);
		product_categoryService.update(domain);
		Product_categoryDTO dto = product_categoryMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#product_category_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Product_category" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/product_categories/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Product_categoryDTO> product_categorydtos) {
        product_categoryService.updateBatch(product_categoryMapping.toDomain(product_categorydtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }







    @PreAuthorize("hasPermission(#product_category_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Product_category" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/product_categories/{product_category_id}")
    public ResponseEntity<Product_categoryDTO> get(@PathVariable("product_category_id") Integer product_category_id) {
        Product_category domain = product_categoryService.get(product_category_id);
        Product_categoryDTO dto = product_categoryMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Product_category" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/product_categories/fetchdefault")
	public ResponseEntity<List<Product_categoryDTO>> fetchDefault(Product_categorySearchContext context) {
        Page<Product_category> domains = product_categoryService.searchDefault(context) ;
        List<Product_categoryDTO> list = product_categoryMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Product_category" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/product_categories/searchdefault")
	public ResponseEntity<Page<Product_categoryDTO>> searchDefault(Product_categorySearchContext context) {
        Page<Product_category> domains = product_categoryService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(product_categoryMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Product_category getEntity(){
        return new Product_category();
    }

}
