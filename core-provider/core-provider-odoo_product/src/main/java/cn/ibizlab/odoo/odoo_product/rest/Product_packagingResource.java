package cn.ibizlab.odoo.odoo_product.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_product.dto.*;
import cn.ibizlab.odoo.odoo_product.mapping.*;
import cn.ibizlab.odoo.core.odoo_product.domain.Product_packaging;
import cn.ibizlab.odoo.core.odoo_product.service.IProduct_packagingService;
import cn.ibizlab.odoo.core.odoo_product.filter.Product_packagingSearchContext;




@Slf4j
@Api(tags = {"Product_packaging" })
@RestController("odoo_product-product_packaging")
@RequestMapping("")
public class Product_packagingResource {

    @Autowired
    private IProduct_packagingService product_packagingService;

    @Autowired
    @Lazy
    private Product_packagingMapping product_packagingMapping;







    @PreAuthorize("hasPermission('Remove',{#product_packaging_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Product_packaging" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/product_packagings/{product_packaging_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("product_packaging_id") Integer product_packaging_id) {
         return ResponseEntity.status(HttpStatus.OK).body(product_packagingService.remove(product_packaging_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Product_packaging" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/product_packagings/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        product_packagingService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }










    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Product_packaging" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/product_packagings")

    public ResponseEntity<Product_packagingDTO> create(@RequestBody Product_packagingDTO product_packagingdto) {
        Product_packaging domain = product_packagingMapping.toDomain(product_packagingdto);
		product_packagingService.create(domain);
        Product_packagingDTO dto = product_packagingMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Product_packaging" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/product_packagings/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Product_packagingDTO> product_packagingdtos) {
        product_packagingService.createBatch(product_packagingMapping.toDomain(product_packagingdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission(#product_packaging_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Product_packaging" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/product_packagings/{product_packaging_id}")
    public ResponseEntity<Product_packagingDTO> get(@PathVariable("product_packaging_id") Integer product_packaging_id) {
        Product_packaging domain = product_packagingService.get(product_packaging_id);
        Product_packagingDTO dto = product_packagingMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }




    @PreAuthorize("hasPermission(#product_packaging_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Product_packaging" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/product_packagings/{product_packaging_id}")

    public ResponseEntity<Product_packagingDTO> update(@PathVariable("product_packaging_id") Integer product_packaging_id, @RequestBody Product_packagingDTO product_packagingdto) {
		Product_packaging domain = product_packagingMapping.toDomain(product_packagingdto);
        domain.setId(product_packaging_id);
		product_packagingService.update(domain);
		Product_packagingDTO dto = product_packagingMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#product_packaging_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Product_packaging" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/product_packagings/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Product_packagingDTO> product_packagingdtos) {
        product_packagingService.updateBatch(product_packagingMapping.toDomain(product_packagingdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Product_packaging" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/product_packagings/fetchdefault")
	public ResponseEntity<List<Product_packagingDTO>> fetchDefault(Product_packagingSearchContext context) {
        Page<Product_packaging> domains = product_packagingService.searchDefault(context) ;
        List<Product_packagingDTO> list = product_packagingMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Product_packaging" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/product_packagings/searchdefault")
	public ResponseEntity<Page<Product_packagingDTO>> searchDefault(Product_packagingSearchContext context) {
        Page<Product_packaging> domains = product_packagingService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(product_packagingMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Product_packaging getEntity(){
        return new Product_packaging();
    }

}
