package cn.ibizlab.odoo.odoo_product.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_product.dto.*;
import cn.ibizlab.odoo.odoo_product.mapping.*;
import cn.ibizlab.odoo.core.odoo_product.domain.Product_style;
import cn.ibizlab.odoo.core.odoo_product.service.IProduct_styleService;
import cn.ibizlab.odoo.core.odoo_product.filter.Product_styleSearchContext;




@Slf4j
@Api(tags = {"Product_style" })
@RestController("odoo_product-product_style")
@RequestMapping("")
public class Product_styleResource {

    @Autowired
    private IProduct_styleService product_styleService;

    @Autowired
    @Lazy
    private Product_styleMapping product_styleMapping;




    @PreAuthorize("hasPermission('Remove',{#product_style_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Product_style" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/product_styles/{product_style_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("product_style_id") Integer product_style_id) {
         return ResponseEntity.status(HttpStatus.OK).body(product_styleService.remove(product_style_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Product_style" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/product_styles/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        product_styleService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission(#product_style_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Product_style" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/product_styles/{product_style_id}")
    public ResponseEntity<Product_styleDTO> get(@PathVariable("product_style_id") Integer product_style_id) {
        Product_style domain = product_styleService.get(product_style_id);
        Product_styleDTO dto = product_styleMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }




    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Product_style" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/product_styles")

    public ResponseEntity<Product_styleDTO> create(@RequestBody Product_styleDTO product_styledto) {
        Product_style domain = product_styleMapping.toDomain(product_styledto);
		product_styleService.create(domain);
        Product_styleDTO dto = product_styleMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Product_style" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/product_styles/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Product_styleDTO> product_styledtos) {
        product_styleService.createBatch(product_styleMapping.toDomain(product_styledtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }










    @PreAuthorize("hasPermission(#product_style_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Product_style" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/product_styles/{product_style_id}")

    public ResponseEntity<Product_styleDTO> update(@PathVariable("product_style_id") Integer product_style_id, @RequestBody Product_styleDTO product_styledto) {
		Product_style domain = product_styleMapping.toDomain(product_styledto);
        domain.setId(product_style_id);
		product_styleService.update(domain);
		Product_styleDTO dto = product_styleMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#product_style_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Product_style" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/product_styles/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Product_styleDTO> product_styledtos) {
        product_styleService.updateBatch(product_styleMapping.toDomain(product_styledtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Product_style" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/product_styles/fetchdefault")
	public ResponseEntity<List<Product_styleDTO>> fetchDefault(Product_styleSearchContext context) {
        Page<Product_style> domains = product_styleService.searchDefault(context) ;
        List<Product_styleDTO> list = product_styleMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Product_style" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/product_styles/searchdefault")
	public ResponseEntity<Page<Product_styleDTO>> searchDefault(Product_styleSearchContext context) {
        Page<Product_style> domains = product_styleService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(product_styleMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Product_style getEntity(){
        return new Product_style();
    }

}
