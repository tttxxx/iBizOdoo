package cn.ibizlab.odoo.odoo_crm.mapping;

import org.mapstruct.*;
import cn.ibizlab.odoo.core.odoo_crm.domain.Crm_lead2opportunity_partner_mass;
import cn.ibizlab.odoo.odoo_crm.dto.Crm_lead2opportunity_partner_massDTO;
import cn.ibizlab.odoo.util.domain.MappingBase;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring", uses = {},
    nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
    nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface Crm_lead2opportunity_partner_massMapping extends MappingBase<Crm_lead2opportunity_partner_massDTO, Crm_lead2opportunity_partner_mass> {


}

