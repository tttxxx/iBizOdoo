package cn.ibizlab.odoo.odoo_crm.config;

import org.springframework.context.annotation.ComponentScan;

@ComponentScan("cn.ibizlab.odoo.odoo_crm")
public class odoo_crmRestConfiguration {

}
