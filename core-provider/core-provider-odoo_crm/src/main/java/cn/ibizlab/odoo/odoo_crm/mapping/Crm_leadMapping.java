package cn.ibizlab.odoo.odoo_crm.mapping;

import org.mapstruct.*;
import cn.ibizlab.odoo.core.odoo_crm.domain.Crm_lead;
import cn.ibizlab.odoo.odoo_crm.dto.Crm_leadDTO;
import cn.ibizlab.odoo.util.domain.MappingBase;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring", uses = {},
    nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
    nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface Crm_leadMapping extends MappingBase<Crm_leadDTO, Crm_lead> {


}

