package cn.ibizlab.odoo.odoo_crm.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_crm.dto.*;
import cn.ibizlab.odoo.odoo_crm.mapping.*;
import cn.ibizlab.odoo.core.odoo_crm.domain.Crm_lead;
import cn.ibizlab.odoo.core.odoo_crm.service.ICrm_leadService;
import cn.ibizlab.odoo.core.odoo_crm.filter.Crm_leadSearchContext;




@Slf4j
@Api(tags = {"Crm_lead" })
@RestController("odoo_crm-crm_lead")
@RequestMapping("")
public class Crm_leadResource {

    @Autowired
    private ICrm_leadService crm_leadService;

    @Autowired
    @Lazy
    private Crm_leadMapping crm_leadMapping;




    @PreAuthorize("hasPermission('Remove',{#crm_lead_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Crm_lead" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/crm_leads/{crm_lead_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("crm_lead_id") Integer crm_lead_id) {
         return ResponseEntity.status(HttpStatus.OK).body(crm_leadService.remove(crm_lead_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Crm_lead" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/crm_leads/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        crm_leadService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }







    @PreAuthorize("hasPermission(#crm_lead_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Crm_lead" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/crm_leads/{crm_lead_id}")
    public ResponseEntity<Crm_leadDTO> get(@PathVariable("crm_lead_id") Integer crm_lead_id) {
        Crm_lead domain = crm_leadService.get(crm_lead_id);
        Crm_leadDTO dto = crm_leadMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }










    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Crm_lead" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/crm_leads")

    public ResponseEntity<Crm_leadDTO> create(@RequestBody Crm_leadDTO crm_leaddto) {
        Crm_lead domain = crm_leadMapping.toDomain(crm_leaddto);
		crm_leadService.create(domain);
        Crm_leadDTO dto = crm_leadMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Crm_lead" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/crm_leads/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Crm_leadDTO> crm_leaddtos) {
        crm_leadService.createBatch(crm_leadMapping.toDomain(crm_leaddtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission(#crm_lead_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Crm_lead" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/crm_leads/{crm_lead_id}")

    public ResponseEntity<Crm_leadDTO> update(@PathVariable("crm_lead_id") Integer crm_lead_id, @RequestBody Crm_leadDTO crm_leaddto) {
		Crm_lead domain = crm_leadMapping.toDomain(crm_leaddto);
        domain.setId(crm_lead_id);
		crm_leadService.update(domain);
		Crm_leadDTO dto = crm_leadMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#crm_lead_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Crm_lead" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/crm_leads/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Crm_leadDTO> crm_leaddtos) {
        crm_leadService.updateBatch(crm_leadMapping.toDomain(crm_leaddtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Crm_lead" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/crm_leads/fetchdefault")
	public ResponseEntity<List<Crm_leadDTO>> fetchDefault(Crm_leadSearchContext context) {
        Page<Crm_lead> domains = crm_leadService.searchDefault(context) ;
        List<Crm_leadDTO> list = crm_leadMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Crm_lead" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/crm_leads/searchdefault")
	public ResponseEntity<Page<Crm_leadDTO>> searchDefault(Crm_leadSearchContext context) {
        Page<Crm_lead> domains = crm_leadService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(crm_leadMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Crm_lead getEntity(){
        return new Crm_lead();
    }

}
