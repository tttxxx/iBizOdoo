package cn.ibizlab.odoo.odoo_survey.mapping;

import org.mapstruct.*;
import cn.ibizlab.odoo.core.odoo_survey.domain.Survey_label;
import cn.ibizlab.odoo.odoo_survey.dto.Survey_labelDTO;
import cn.ibizlab.odoo.util.domain.MappingBase;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring", uses = {},
    nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
    nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface Survey_labelMapping extends MappingBase<Survey_labelDTO, Survey_label> {


}

