package cn.ibizlab.odoo.odoo_survey.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_survey.dto.*;
import cn.ibizlab.odoo.odoo_survey.mapping.*;
import cn.ibizlab.odoo.core.odoo_survey.domain.Survey_user_input_line;
import cn.ibizlab.odoo.core.odoo_survey.service.ISurvey_user_input_lineService;
import cn.ibizlab.odoo.core.odoo_survey.filter.Survey_user_input_lineSearchContext;




@Slf4j
@Api(tags = {"Survey_user_input_line" })
@RestController("odoo_survey-survey_user_input_line")
@RequestMapping("")
public class Survey_user_input_lineResource {

    @Autowired
    private ISurvey_user_input_lineService survey_user_input_lineService;

    @Autowired
    @Lazy
    private Survey_user_input_lineMapping survey_user_input_lineMapping;







    @PreAuthorize("hasPermission(#survey_user_input_line_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Survey_user_input_line" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/survey_user_input_lines/{survey_user_input_line_id}")

    public ResponseEntity<Survey_user_input_lineDTO> update(@PathVariable("survey_user_input_line_id") Integer survey_user_input_line_id, @RequestBody Survey_user_input_lineDTO survey_user_input_linedto) {
		Survey_user_input_line domain = survey_user_input_lineMapping.toDomain(survey_user_input_linedto);
        domain.setId(survey_user_input_line_id);
		survey_user_input_lineService.update(domain);
		Survey_user_input_lineDTO dto = survey_user_input_lineMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#survey_user_input_line_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Survey_user_input_line" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/survey_user_input_lines/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Survey_user_input_lineDTO> survey_user_input_linedtos) {
        survey_user_input_lineService.updateBatch(survey_user_input_lineMapping.toDomain(survey_user_input_linedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }










    @PreAuthorize("hasPermission(#survey_user_input_line_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Survey_user_input_line" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/survey_user_input_lines/{survey_user_input_line_id}")
    public ResponseEntity<Survey_user_input_lineDTO> get(@PathVariable("survey_user_input_line_id") Integer survey_user_input_line_id) {
        Survey_user_input_line domain = survey_user_input_lineService.get(survey_user_input_line_id);
        Survey_user_input_lineDTO dto = survey_user_input_lineMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }




    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Survey_user_input_line" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/survey_user_input_lines")

    public ResponseEntity<Survey_user_input_lineDTO> create(@RequestBody Survey_user_input_lineDTO survey_user_input_linedto) {
        Survey_user_input_line domain = survey_user_input_lineMapping.toDomain(survey_user_input_linedto);
		survey_user_input_lineService.create(domain);
        Survey_user_input_lineDTO dto = survey_user_input_lineMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Survey_user_input_line" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/survey_user_input_lines/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Survey_user_input_lineDTO> survey_user_input_linedtos) {
        survey_user_input_lineService.createBatch(survey_user_input_lineMapping.toDomain(survey_user_input_linedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission('Remove',{#survey_user_input_line_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Survey_user_input_line" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/survey_user_input_lines/{survey_user_input_line_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("survey_user_input_line_id") Integer survey_user_input_line_id) {
         return ResponseEntity.status(HttpStatus.OK).body(survey_user_input_lineService.remove(survey_user_input_line_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Survey_user_input_line" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/survey_user_input_lines/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        survey_user_input_lineService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Survey_user_input_line" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/survey_user_input_lines/fetchdefault")
	public ResponseEntity<List<Survey_user_input_lineDTO>> fetchDefault(Survey_user_input_lineSearchContext context) {
        Page<Survey_user_input_line> domains = survey_user_input_lineService.searchDefault(context) ;
        List<Survey_user_input_lineDTO> list = survey_user_input_lineMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Survey_user_input_line" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/survey_user_input_lines/searchdefault")
	public ResponseEntity<Page<Survey_user_input_lineDTO>> searchDefault(Survey_user_input_lineSearchContext context) {
        Page<Survey_user_input_line> domains = survey_user_input_lineService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(survey_user_input_lineMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Survey_user_input_line getEntity(){
        return new Survey_user_input_line();
    }

}
