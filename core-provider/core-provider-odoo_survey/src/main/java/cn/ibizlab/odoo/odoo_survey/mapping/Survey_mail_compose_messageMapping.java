package cn.ibizlab.odoo.odoo_survey.mapping;

import org.mapstruct.*;
import cn.ibizlab.odoo.core.odoo_survey.domain.Survey_mail_compose_message;
import cn.ibizlab.odoo.odoo_survey.dto.Survey_mail_compose_messageDTO;
import cn.ibizlab.odoo.util.domain.MappingBase;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring", uses = {},
    nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
    nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface Survey_mail_compose_messageMapping extends MappingBase<Survey_mail_compose_messageDTO, Survey_mail_compose_message> {


}

