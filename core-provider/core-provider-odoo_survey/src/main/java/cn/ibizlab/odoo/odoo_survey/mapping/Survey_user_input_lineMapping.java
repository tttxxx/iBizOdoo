package cn.ibizlab.odoo.odoo_survey.mapping;

import org.mapstruct.*;
import cn.ibizlab.odoo.core.odoo_survey.domain.Survey_user_input_line;
import cn.ibizlab.odoo.odoo_survey.dto.Survey_user_input_lineDTO;
import cn.ibizlab.odoo.util.domain.MappingBase;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring", uses = {},
    nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
    nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface Survey_user_input_lineMapping extends MappingBase<Survey_user_input_lineDTO, Survey_user_input_line> {


}

