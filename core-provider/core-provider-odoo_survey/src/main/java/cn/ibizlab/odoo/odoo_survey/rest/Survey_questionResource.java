package cn.ibizlab.odoo.odoo_survey.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_survey.dto.*;
import cn.ibizlab.odoo.odoo_survey.mapping.*;
import cn.ibizlab.odoo.core.odoo_survey.domain.Survey_question;
import cn.ibizlab.odoo.core.odoo_survey.service.ISurvey_questionService;
import cn.ibizlab.odoo.core.odoo_survey.filter.Survey_questionSearchContext;




@Slf4j
@Api(tags = {"Survey_question" })
@RestController("odoo_survey-survey_question")
@RequestMapping("")
public class Survey_questionResource {

    @Autowired
    private ISurvey_questionService survey_questionService;

    @Autowired
    @Lazy
    private Survey_questionMapping survey_questionMapping;




    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Survey_question" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/survey_questions")

    public ResponseEntity<Survey_questionDTO> create(@RequestBody Survey_questionDTO survey_questiondto) {
        Survey_question domain = survey_questionMapping.toDomain(survey_questiondto);
		survey_questionService.create(domain);
        Survey_questionDTO dto = survey_questionMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Survey_question" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/survey_questions/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Survey_questionDTO> survey_questiondtos) {
        survey_questionService.createBatch(survey_questionMapping.toDomain(survey_questiondtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission(#survey_question_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Survey_question" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/survey_questions/{survey_question_id}")

    public ResponseEntity<Survey_questionDTO> update(@PathVariable("survey_question_id") Integer survey_question_id, @RequestBody Survey_questionDTO survey_questiondto) {
		Survey_question domain = survey_questionMapping.toDomain(survey_questiondto);
        domain.setId(survey_question_id);
		survey_questionService.update(domain);
		Survey_questionDTO dto = survey_questionMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#survey_question_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Survey_question" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/survey_questions/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Survey_questionDTO> survey_questiondtos) {
        survey_questionService.updateBatch(survey_questionMapping.toDomain(survey_questiondtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }










    @PreAuthorize("hasPermission('Remove',{#survey_question_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Survey_question" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/survey_questions/{survey_question_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("survey_question_id") Integer survey_question_id) {
         return ResponseEntity.status(HttpStatus.OK).body(survey_questionService.remove(survey_question_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Survey_question" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/survey_questions/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        survey_questionService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission(#survey_question_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Survey_question" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/survey_questions/{survey_question_id}")
    public ResponseEntity<Survey_questionDTO> get(@PathVariable("survey_question_id") Integer survey_question_id) {
        Survey_question domain = survey_questionService.get(survey_question_id);
        Survey_questionDTO dto = survey_questionMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }




    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Survey_question" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/survey_questions/fetchdefault")
	public ResponseEntity<List<Survey_questionDTO>> fetchDefault(Survey_questionSearchContext context) {
        Page<Survey_question> domains = survey_questionService.searchDefault(context) ;
        List<Survey_questionDTO> list = survey_questionMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Survey_question" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/survey_questions/searchdefault")
	public ResponseEntity<Page<Survey_questionDTO>> searchDefault(Survey_questionSearchContext context) {
        Page<Survey_question> domains = survey_questionService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(survey_questionMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Survey_question getEntity(){
        return new Survey_question();
    }

}
