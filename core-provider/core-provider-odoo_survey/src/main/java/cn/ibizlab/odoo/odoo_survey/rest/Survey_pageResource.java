package cn.ibizlab.odoo.odoo_survey.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_survey.dto.*;
import cn.ibizlab.odoo.odoo_survey.mapping.*;
import cn.ibizlab.odoo.core.odoo_survey.domain.Survey_page;
import cn.ibizlab.odoo.core.odoo_survey.service.ISurvey_pageService;
import cn.ibizlab.odoo.core.odoo_survey.filter.Survey_pageSearchContext;




@Slf4j
@Api(tags = {"Survey_page" })
@RestController("odoo_survey-survey_page")
@RequestMapping("")
public class Survey_pageResource {

    @Autowired
    private ISurvey_pageService survey_pageService;

    @Autowired
    @Lazy
    private Survey_pageMapping survey_pageMapping;




    @PreAuthorize("hasPermission(#survey_page_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Survey_page" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/survey_pages/{survey_page_id}")

    public ResponseEntity<Survey_pageDTO> update(@PathVariable("survey_page_id") Integer survey_page_id, @RequestBody Survey_pageDTO survey_pagedto) {
		Survey_page domain = survey_pageMapping.toDomain(survey_pagedto);
        domain.setId(survey_page_id);
		survey_pageService.update(domain);
		Survey_pageDTO dto = survey_pageMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#survey_page_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Survey_page" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/survey_pages/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Survey_pageDTO> survey_pagedtos) {
        survey_pageService.updateBatch(survey_pageMapping.toDomain(survey_pagedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission(#survey_page_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Survey_page" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/survey_pages/{survey_page_id}")
    public ResponseEntity<Survey_pageDTO> get(@PathVariable("survey_page_id") Integer survey_page_id) {
        Survey_page domain = survey_pageService.get(survey_page_id);
        Survey_pageDTO dto = survey_pageMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }




    @PreAuthorize("hasPermission('Remove',{#survey_page_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Survey_page" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/survey_pages/{survey_page_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("survey_page_id") Integer survey_page_id) {
         return ResponseEntity.status(HttpStatus.OK).body(survey_pageService.remove(survey_page_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Survey_page" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/survey_pages/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        survey_pageService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }










    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Survey_page" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/survey_pages")

    public ResponseEntity<Survey_pageDTO> create(@RequestBody Survey_pageDTO survey_pagedto) {
        Survey_page domain = survey_pageMapping.toDomain(survey_pagedto);
		survey_pageService.create(domain);
        Survey_pageDTO dto = survey_pageMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Survey_page" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/survey_pages/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Survey_pageDTO> survey_pagedtos) {
        survey_pageService.createBatch(survey_pageMapping.toDomain(survey_pagedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Survey_page" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/survey_pages/fetchdefault")
	public ResponseEntity<List<Survey_pageDTO>> fetchDefault(Survey_pageSearchContext context) {
        Page<Survey_page> domains = survey_pageService.searchDefault(context) ;
        List<Survey_pageDTO> list = survey_pageMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Survey_page" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/survey_pages/searchdefault")
	public ResponseEntity<Page<Survey_pageDTO>> searchDefault(Survey_pageSearchContext context) {
        Page<Survey_page> domains = survey_pageService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(survey_pageMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Survey_page getEntity(){
        return new Survey_page();
    }

}
