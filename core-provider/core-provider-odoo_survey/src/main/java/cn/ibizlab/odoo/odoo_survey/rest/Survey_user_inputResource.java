package cn.ibizlab.odoo.odoo_survey.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_survey.dto.*;
import cn.ibizlab.odoo.odoo_survey.mapping.*;
import cn.ibizlab.odoo.core.odoo_survey.domain.Survey_user_input;
import cn.ibizlab.odoo.core.odoo_survey.service.ISurvey_user_inputService;
import cn.ibizlab.odoo.core.odoo_survey.filter.Survey_user_inputSearchContext;




@Slf4j
@Api(tags = {"Survey_user_input" })
@RestController("odoo_survey-survey_user_input")
@RequestMapping("")
public class Survey_user_inputResource {

    @Autowired
    private ISurvey_user_inputService survey_user_inputService;

    @Autowired
    @Lazy
    private Survey_user_inputMapping survey_user_inputMapping;







    @PreAuthorize("hasPermission(#survey_user_input_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Survey_user_input" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/survey_user_inputs/{survey_user_input_id}")

    public ResponseEntity<Survey_user_inputDTO> update(@PathVariable("survey_user_input_id") Integer survey_user_input_id, @RequestBody Survey_user_inputDTO survey_user_inputdto) {
		Survey_user_input domain = survey_user_inputMapping.toDomain(survey_user_inputdto);
        domain.setId(survey_user_input_id);
		survey_user_inputService.update(domain);
		Survey_user_inputDTO dto = survey_user_inputMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#survey_user_input_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Survey_user_input" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/survey_user_inputs/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Survey_user_inputDTO> survey_user_inputdtos) {
        survey_user_inputService.updateBatch(survey_user_inputMapping.toDomain(survey_user_inputdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Survey_user_input" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/survey_user_inputs")

    public ResponseEntity<Survey_user_inputDTO> create(@RequestBody Survey_user_inputDTO survey_user_inputdto) {
        Survey_user_input domain = survey_user_inputMapping.toDomain(survey_user_inputdto);
		survey_user_inputService.create(domain);
        Survey_user_inputDTO dto = survey_user_inputMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Survey_user_input" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/survey_user_inputs/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Survey_user_inputDTO> survey_user_inputdtos) {
        survey_user_inputService.createBatch(survey_user_inputMapping.toDomain(survey_user_inputdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission('Remove',{#survey_user_input_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Survey_user_input" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/survey_user_inputs/{survey_user_input_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("survey_user_input_id") Integer survey_user_input_id) {
         return ResponseEntity.status(HttpStatus.OK).body(survey_user_inputService.remove(survey_user_input_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Survey_user_input" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/survey_user_inputs/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        survey_user_inputService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }










    @PreAuthorize("hasPermission(#survey_user_input_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Survey_user_input" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/survey_user_inputs/{survey_user_input_id}")
    public ResponseEntity<Survey_user_inputDTO> get(@PathVariable("survey_user_input_id") Integer survey_user_input_id) {
        Survey_user_input domain = survey_user_inputService.get(survey_user_input_id);
        Survey_user_inputDTO dto = survey_user_inputMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Survey_user_input" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/survey_user_inputs/fetchdefault")
	public ResponseEntity<List<Survey_user_inputDTO>> fetchDefault(Survey_user_inputSearchContext context) {
        Page<Survey_user_input> domains = survey_user_inputService.searchDefault(context) ;
        List<Survey_user_inputDTO> list = survey_user_inputMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Survey_user_input" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/survey_user_inputs/searchdefault")
	public ResponseEntity<Page<Survey_user_inputDTO>> searchDefault(Survey_user_inputSearchContext context) {
        Page<Survey_user_input> domains = survey_user_inputService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(survey_user_inputMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Survey_user_input getEntity(){
        return new Survey_user_input();
    }

}
