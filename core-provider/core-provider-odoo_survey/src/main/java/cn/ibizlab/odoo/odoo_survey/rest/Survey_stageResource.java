package cn.ibizlab.odoo.odoo_survey.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_survey.dto.*;
import cn.ibizlab.odoo.odoo_survey.mapping.*;
import cn.ibizlab.odoo.core.odoo_survey.domain.Survey_stage;
import cn.ibizlab.odoo.core.odoo_survey.service.ISurvey_stageService;
import cn.ibizlab.odoo.core.odoo_survey.filter.Survey_stageSearchContext;




@Slf4j
@Api(tags = {"Survey_stage" })
@RestController("odoo_survey-survey_stage")
@RequestMapping("")
public class Survey_stageResource {

    @Autowired
    private ISurvey_stageService survey_stageService;

    @Autowired
    @Lazy
    private Survey_stageMapping survey_stageMapping;













    @PreAuthorize("hasPermission(#survey_stage_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Survey_stage" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/survey_stages/{survey_stage_id}")

    public ResponseEntity<Survey_stageDTO> update(@PathVariable("survey_stage_id") Integer survey_stage_id, @RequestBody Survey_stageDTO survey_stagedto) {
		Survey_stage domain = survey_stageMapping.toDomain(survey_stagedto);
        domain.setId(survey_stage_id);
		survey_stageService.update(domain);
		Survey_stageDTO dto = survey_stageMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#survey_stage_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Survey_stage" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/survey_stages/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Survey_stageDTO> survey_stagedtos) {
        survey_stageService.updateBatch(survey_stageMapping.toDomain(survey_stagedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission(#survey_stage_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Survey_stage" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/survey_stages/{survey_stage_id}")
    public ResponseEntity<Survey_stageDTO> get(@PathVariable("survey_stage_id") Integer survey_stage_id) {
        Survey_stage domain = survey_stageService.get(survey_stage_id);
        Survey_stageDTO dto = survey_stageMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }




    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Survey_stage" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/survey_stages")

    public ResponseEntity<Survey_stageDTO> create(@RequestBody Survey_stageDTO survey_stagedto) {
        Survey_stage domain = survey_stageMapping.toDomain(survey_stagedto);
		survey_stageService.create(domain);
        Survey_stageDTO dto = survey_stageMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Survey_stage" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/survey_stages/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Survey_stageDTO> survey_stagedtos) {
        survey_stageService.createBatch(survey_stageMapping.toDomain(survey_stagedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission('Remove',{#survey_stage_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Survey_stage" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/survey_stages/{survey_stage_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("survey_stage_id") Integer survey_stage_id) {
         return ResponseEntity.status(HttpStatus.OK).body(survey_stageService.remove(survey_stage_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Survey_stage" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/survey_stages/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        survey_stageService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Survey_stage" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/survey_stages/fetchdefault")
	public ResponseEntity<List<Survey_stageDTO>> fetchDefault(Survey_stageSearchContext context) {
        Page<Survey_stage> domains = survey_stageService.searchDefault(context) ;
        List<Survey_stageDTO> list = survey_stageMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Survey_stage" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/survey_stages/searchdefault")
	public ResponseEntity<Page<Survey_stageDTO>> searchDefault(Survey_stageSearchContext context) {
        Page<Survey_stage> domains = survey_stageService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(survey_stageMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Survey_stage getEntity(){
        return new Survey_stage();
    }

}
