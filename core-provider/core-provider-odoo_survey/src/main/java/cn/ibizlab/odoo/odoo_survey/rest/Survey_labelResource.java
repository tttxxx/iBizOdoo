package cn.ibizlab.odoo.odoo_survey.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_survey.dto.*;
import cn.ibizlab.odoo.odoo_survey.mapping.*;
import cn.ibizlab.odoo.core.odoo_survey.domain.Survey_label;
import cn.ibizlab.odoo.core.odoo_survey.service.ISurvey_labelService;
import cn.ibizlab.odoo.core.odoo_survey.filter.Survey_labelSearchContext;




@Slf4j
@Api(tags = {"Survey_label" })
@RestController("odoo_survey-survey_label")
@RequestMapping("")
public class Survey_labelResource {

    @Autowired
    private ISurvey_labelService survey_labelService;

    @Autowired
    @Lazy
    private Survey_labelMapping survey_labelMapping;




    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Survey_label" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/survey_labels")

    public ResponseEntity<Survey_labelDTO> create(@RequestBody Survey_labelDTO survey_labeldto) {
        Survey_label domain = survey_labelMapping.toDomain(survey_labeldto);
		survey_labelService.create(domain);
        Survey_labelDTO dto = survey_labelMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Survey_label" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/survey_labels/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Survey_labelDTO> survey_labeldtos) {
        survey_labelService.createBatch(survey_labelMapping.toDomain(survey_labeldtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }







    @PreAuthorize("hasPermission(#survey_label_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Survey_label" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/survey_labels/{survey_label_id}")

    public ResponseEntity<Survey_labelDTO> update(@PathVariable("survey_label_id") Integer survey_label_id, @RequestBody Survey_labelDTO survey_labeldto) {
		Survey_label domain = survey_labelMapping.toDomain(survey_labeldto);
        domain.setId(survey_label_id);
		survey_labelService.update(domain);
		Survey_labelDTO dto = survey_labelMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#survey_label_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Survey_label" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/survey_labels/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Survey_labelDTO> survey_labeldtos) {
        survey_labelService.updateBatch(survey_labelMapping.toDomain(survey_labeldtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }







    @PreAuthorize("hasPermission(#survey_label_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Survey_label" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/survey_labels/{survey_label_id}")
    public ResponseEntity<Survey_labelDTO> get(@PathVariable("survey_label_id") Integer survey_label_id) {
        Survey_label domain = survey_labelService.get(survey_label_id);
        Survey_labelDTO dto = survey_labelMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }







    @PreAuthorize("hasPermission('Remove',{#survey_label_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Survey_label" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/survey_labels/{survey_label_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("survey_label_id") Integer survey_label_id) {
         return ResponseEntity.status(HttpStatus.OK).body(survey_labelService.remove(survey_label_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Survey_label" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/survey_labels/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        survey_labelService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Survey_label" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/survey_labels/fetchdefault")
	public ResponseEntity<List<Survey_labelDTO>> fetchDefault(Survey_labelSearchContext context) {
        Page<Survey_label> domains = survey_labelService.searchDefault(context) ;
        List<Survey_labelDTO> list = survey_labelMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Survey_label" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/survey_labels/searchdefault")
	public ResponseEntity<Page<Survey_labelDTO>> searchDefault(Survey_labelSearchContext context) {
        Page<Survey_label> domains = survey_labelService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(survey_labelMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Survey_label getEntity(){
        return new Survey_label();
    }

}
