package cn.ibizlab.odoo.odoo_base_import.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_base_import.dto.*;
import cn.ibizlab.odoo.odoo_base_import.mapping.*;
import cn.ibizlab.odoo.core.odoo_base_import.domain.Base_import_tests_models_float;
import cn.ibizlab.odoo.core.odoo_base_import.service.IBase_import_tests_models_floatService;
import cn.ibizlab.odoo.core.odoo_base_import.filter.Base_import_tests_models_floatSearchContext;




@Slf4j
@Api(tags = {"Base_import_tests_models_float" })
@RestController("odoo_base_import-base_import_tests_models_float")
@RequestMapping("")
public class Base_import_tests_models_floatResource {

    @Autowired
    private IBase_import_tests_models_floatService base_import_tests_models_floatService;

    @Autowired
    @Lazy
    private Base_import_tests_models_floatMapping base_import_tests_models_floatMapping;




    @PreAuthorize("hasPermission(#base_import_tests_models_float_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Base_import_tests_models_float" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/base_import_tests_models_floats/{base_import_tests_models_float_id}")
    public ResponseEntity<Base_import_tests_models_floatDTO> get(@PathVariable("base_import_tests_models_float_id") Integer base_import_tests_models_float_id) {
        Base_import_tests_models_float domain = base_import_tests_models_floatService.get(base_import_tests_models_float_id);
        Base_import_tests_models_floatDTO dto = base_import_tests_models_floatMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }













    @PreAuthorize("hasPermission('Remove',{#base_import_tests_models_float_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Base_import_tests_models_float" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/base_import_tests_models_floats/{base_import_tests_models_float_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("base_import_tests_models_float_id") Integer base_import_tests_models_float_id) {
         return ResponseEntity.status(HttpStatus.OK).body(base_import_tests_models_floatService.remove(base_import_tests_models_float_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Base_import_tests_models_float" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/base_import_tests_models_floats/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        base_import_tests_models_floatService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission(#base_import_tests_models_float_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Base_import_tests_models_float" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/base_import_tests_models_floats/{base_import_tests_models_float_id}")

    public ResponseEntity<Base_import_tests_models_floatDTO> update(@PathVariable("base_import_tests_models_float_id") Integer base_import_tests_models_float_id, @RequestBody Base_import_tests_models_floatDTO base_import_tests_models_floatdto) {
		Base_import_tests_models_float domain = base_import_tests_models_floatMapping.toDomain(base_import_tests_models_floatdto);
        domain.setId(base_import_tests_models_float_id);
		base_import_tests_models_floatService.update(domain);
		Base_import_tests_models_floatDTO dto = base_import_tests_models_floatMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#base_import_tests_models_float_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Base_import_tests_models_float" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/base_import_tests_models_floats/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Base_import_tests_models_floatDTO> base_import_tests_models_floatdtos) {
        base_import_tests_models_floatService.updateBatch(base_import_tests_models_floatMapping.toDomain(base_import_tests_models_floatdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Base_import_tests_models_float" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/base_import_tests_models_floats")

    public ResponseEntity<Base_import_tests_models_floatDTO> create(@RequestBody Base_import_tests_models_floatDTO base_import_tests_models_floatdto) {
        Base_import_tests_models_float domain = base_import_tests_models_floatMapping.toDomain(base_import_tests_models_floatdto);
		base_import_tests_models_floatService.create(domain);
        Base_import_tests_models_floatDTO dto = base_import_tests_models_floatMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Base_import_tests_models_float" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/base_import_tests_models_floats/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Base_import_tests_models_floatDTO> base_import_tests_models_floatdtos) {
        base_import_tests_models_floatService.createBatch(base_import_tests_models_floatMapping.toDomain(base_import_tests_models_floatdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Base_import_tests_models_float" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/base_import_tests_models_floats/fetchdefault")
	public ResponseEntity<List<Base_import_tests_models_floatDTO>> fetchDefault(Base_import_tests_models_floatSearchContext context) {
        Page<Base_import_tests_models_float> domains = base_import_tests_models_floatService.searchDefault(context) ;
        List<Base_import_tests_models_floatDTO> list = base_import_tests_models_floatMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Base_import_tests_models_float" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/base_import_tests_models_floats/searchdefault")
	public ResponseEntity<Page<Base_import_tests_models_floatDTO>> searchDefault(Base_import_tests_models_floatSearchContext context) {
        Page<Base_import_tests_models_float> domains = base_import_tests_models_floatService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(base_import_tests_models_floatMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Base_import_tests_models_float getEntity(){
        return new Base_import_tests_models_float();
    }

}
