package cn.ibizlab.odoo.odoo_base_import.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_base_import.dto.*;
import cn.ibizlab.odoo.odoo_base_import.mapping.*;
import cn.ibizlab.odoo.core.odoo_base_import.domain.Base_import_import;
import cn.ibizlab.odoo.core.odoo_base_import.service.IBase_import_importService;
import cn.ibizlab.odoo.core.odoo_base_import.filter.Base_import_importSearchContext;




@Slf4j
@Api(tags = {"Base_import_import" })
@RestController("odoo_base_import-base_import_import")
@RequestMapping("")
public class Base_import_importResource {

    @Autowired
    private IBase_import_importService base_import_importService;

    @Autowired
    @Lazy
    private Base_import_importMapping base_import_importMapping;




    @PreAuthorize("hasPermission(#base_import_import_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Base_import_import" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/base_import_imports/{base_import_import_id}")
    public ResponseEntity<Base_import_importDTO> get(@PathVariable("base_import_import_id") Integer base_import_import_id) {
        Base_import_import domain = base_import_importService.get(base_import_import_id);
        Base_import_importDTO dto = base_import_importMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }







    @PreAuthorize("hasPermission(#base_import_import_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Base_import_import" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/base_import_imports/{base_import_import_id}")

    public ResponseEntity<Base_import_importDTO> update(@PathVariable("base_import_import_id") Integer base_import_import_id, @RequestBody Base_import_importDTO base_import_importdto) {
		Base_import_import domain = base_import_importMapping.toDomain(base_import_importdto);
        domain.setId(base_import_import_id);
		base_import_importService.update(domain);
		Base_import_importDTO dto = base_import_importMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#base_import_import_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Base_import_import" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/base_import_imports/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Base_import_importDTO> base_import_importdtos) {
        base_import_importService.updateBatch(base_import_importMapping.toDomain(base_import_importdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Base_import_import" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/base_import_imports")

    public ResponseEntity<Base_import_importDTO> create(@RequestBody Base_import_importDTO base_import_importdto) {
        Base_import_import domain = base_import_importMapping.toDomain(base_import_importdto);
		base_import_importService.create(domain);
        Base_import_importDTO dto = base_import_importMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Base_import_import" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/base_import_imports/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Base_import_importDTO> base_import_importdtos) {
        base_import_importService.createBatch(base_import_importMapping.toDomain(base_import_importdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }







    @PreAuthorize("hasPermission('Remove',{#base_import_import_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Base_import_import" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/base_import_imports/{base_import_import_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("base_import_import_id") Integer base_import_import_id) {
         return ResponseEntity.status(HttpStatus.OK).body(base_import_importService.remove(base_import_import_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Base_import_import" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/base_import_imports/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        base_import_importService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Base_import_import" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/base_import_imports/fetchdefault")
	public ResponseEntity<List<Base_import_importDTO>> fetchDefault(Base_import_importSearchContext context) {
        Page<Base_import_import> domains = base_import_importService.searchDefault(context) ;
        List<Base_import_importDTO> list = base_import_importMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Base_import_import" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/base_import_imports/searchdefault")
	public ResponseEntity<Page<Base_import_importDTO>> searchDefault(Base_import_importSearchContext context) {
        Page<Base_import_import> domains = base_import_importService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(base_import_importMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Base_import_import getEntity(){
        return new Base_import_import();
    }

}
