package cn.ibizlab.odoo.odoo_base_import.mapping;

import org.mapstruct.*;
import cn.ibizlab.odoo.core.odoo_base_import.domain.Base_import_import;
import cn.ibizlab.odoo.odoo_base_import.dto.Base_import_importDTO;
import cn.ibizlab.odoo.util.domain.MappingBase;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring", uses = {},
    nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
    nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface Base_import_importMapping extends MappingBase<Base_import_importDTO, Base_import_import> {


}

