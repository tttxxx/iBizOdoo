package cn.ibizlab.odoo.odoo_base_import.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_base_import.dto.*;
import cn.ibizlab.odoo.odoo_base_import.mapping.*;
import cn.ibizlab.odoo.core.odoo_base_import.domain.Base_import_tests_models_o2m;
import cn.ibizlab.odoo.core.odoo_base_import.service.IBase_import_tests_models_o2mService;
import cn.ibizlab.odoo.core.odoo_base_import.filter.Base_import_tests_models_o2mSearchContext;




@Slf4j
@Api(tags = {"Base_import_tests_models_o2m" })
@RestController("odoo_base_import-base_import_tests_models_o2m")
@RequestMapping("")
public class Base_import_tests_models_o2mResource {

    @Autowired
    private IBase_import_tests_models_o2mService base_import_tests_models_o2mService;

    @Autowired
    @Lazy
    private Base_import_tests_models_o2mMapping base_import_tests_models_o2mMapping;




    @PreAuthorize("hasPermission(#base_import_tests_models_o2m_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Base_import_tests_models_o2m" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/base_import_tests_models_o2ms/{base_import_tests_models_o2m_id}")

    public ResponseEntity<Base_import_tests_models_o2mDTO> update(@PathVariable("base_import_tests_models_o2m_id") Integer base_import_tests_models_o2m_id, @RequestBody Base_import_tests_models_o2mDTO base_import_tests_models_o2mdto) {
		Base_import_tests_models_o2m domain = base_import_tests_models_o2mMapping.toDomain(base_import_tests_models_o2mdto);
        domain.setId(base_import_tests_models_o2m_id);
		base_import_tests_models_o2mService.update(domain);
		Base_import_tests_models_o2mDTO dto = base_import_tests_models_o2mMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#base_import_tests_models_o2m_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Base_import_tests_models_o2m" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/base_import_tests_models_o2ms/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Base_import_tests_models_o2mDTO> base_import_tests_models_o2mdtos) {
        base_import_tests_models_o2mService.updateBatch(base_import_tests_models_o2mMapping.toDomain(base_import_tests_models_o2mdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }







    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Base_import_tests_models_o2m" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/base_import_tests_models_o2ms")

    public ResponseEntity<Base_import_tests_models_o2mDTO> create(@RequestBody Base_import_tests_models_o2mDTO base_import_tests_models_o2mdto) {
        Base_import_tests_models_o2m domain = base_import_tests_models_o2mMapping.toDomain(base_import_tests_models_o2mdto);
		base_import_tests_models_o2mService.create(domain);
        Base_import_tests_models_o2mDTO dto = base_import_tests_models_o2mMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Base_import_tests_models_o2m" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/base_import_tests_models_o2ms/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Base_import_tests_models_o2mDTO> base_import_tests_models_o2mdtos) {
        base_import_tests_models_o2mService.createBatch(base_import_tests_models_o2mMapping.toDomain(base_import_tests_models_o2mdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }







    @PreAuthorize("hasPermission(#base_import_tests_models_o2m_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Base_import_tests_models_o2m" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/base_import_tests_models_o2ms/{base_import_tests_models_o2m_id}")
    public ResponseEntity<Base_import_tests_models_o2mDTO> get(@PathVariable("base_import_tests_models_o2m_id") Integer base_import_tests_models_o2m_id) {
        Base_import_tests_models_o2m domain = base_import_tests_models_o2mService.get(base_import_tests_models_o2m_id);
        Base_import_tests_models_o2mDTO dto = base_import_tests_models_o2mMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }




    @PreAuthorize("hasPermission('Remove',{#base_import_tests_models_o2m_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Base_import_tests_models_o2m" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/base_import_tests_models_o2ms/{base_import_tests_models_o2m_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("base_import_tests_models_o2m_id") Integer base_import_tests_models_o2m_id) {
         return ResponseEntity.status(HttpStatus.OK).body(base_import_tests_models_o2mService.remove(base_import_tests_models_o2m_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Base_import_tests_models_o2m" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/base_import_tests_models_o2ms/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        base_import_tests_models_o2mService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Base_import_tests_models_o2m" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/base_import_tests_models_o2ms/fetchdefault")
	public ResponseEntity<List<Base_import_tests_models_o2mDTO>> fetchDefault(Base_import_tests_models_o2mSearchContext context) {
        Page<Base_import_tests_models_o2m> domains = base_import_tests_models_o2mService.searchDefault(context) ;
        List<Base_import_tests_models_o2mDTO> list = base_import_tests_models_o2mMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Base_import_tests_models_o2m" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/base_import_tests_models_o2ms/searchdefault")
	public ResponseEntity<Page<Base_import_tests_models_o2mDTO>> searchDefault(Base_import_tests_models_o2mSearchContext context) {
        Page<Base_import_tests_models_o2m> domains = base_import_tests_models_o2mService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(base_import_tests_models_o2mMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Base_import_tests_models_o2m getEntity(){
        return new Base_import_tests_models_o2m();
    }

}
