package cn.ibizlab.odoo.odoo_base_import.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_base_import.dto.*;
import cn.ibizlab.odoo.odoo_base_import.mapping.*;
import cn.ibizlab.odoo.core.odoo_base_import.domain.Base_import_tests_models_o2m_child;
import cn.ibizlab.odoo.core.odoo_base_import.service.IBase_import_tests_models_o2m_childService;
import cn.ibizlab.odoo.core.odoo_base_import.filter.Base_import_tests_models_o2m_childSearchContext;




@Slf4j
@Api(tags = {"Base_import_tests_models_o2m_child" })
@RestController("odoo_base_import-base_import_tests_models_o2m_child")
@RequestMapping("")
public class Base_import_tests_models_o2m_childResource {

    @Autowired
    private IBase_import_tests_models_o2m_childService base_import_tests_models_o2m_childService;

    @Autowired
    @Lazy
    private Base_import_tests_models_o2m_childMapping base_import_tests_models_o2m_childMapping;




    @PreAuthorize("hasPermission('Remove',{#base_import_tests_models_o2m_child_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Base_import_tests_models_o2m_child" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/base_import_tests_models_o2m_children/{base_import_tests_models_o2m_child_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("base_import_tests_models_o2m_child_id") Integer base_import_tests_models_o2m_child_id) {
         return ResponseEntity.status(HttpStatus.OK).body(base_import_tests_models_o2m_childService.remove(base_import_tests_models_o2m_child_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Base_import_tests_models_o2m_child" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/base_import_tests_models_o2m_children/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        base_import_tests_models_o2m_childService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Base_import_tests_models_o2m_child" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/base_import_tests_models_o2m_children")

    public ResponseEntity<Base_import_tests_models_o2m_childDTO> create(@RequestBody Base_import_tests_models_o2m_childDTO base_import_tests_models_o2m_childdto) {
        Base_import_tests_models_o2m_child domain = base_import_tests_models_o2m_childMapping.toDomain(base_import_tests_models_o2m_childdto);
		base_import_tests_models_o2m_childService.create(domain);
        Base_import_tests_models_o2m_childDTO dto = base_import_tests_models_o2m_childMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Base_import_tests_models_o2m_child" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/base_import_tests_models_o2m_children/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Base_import_tests_models_o2m_childDTO> base_import_tests_models_o2m_childdtos) {
        base_import_tests_models_o2m_childService.createBatch(base_import_tests_models_o2m_childMapping.toDomain(base_import_tests_models_o2m_childdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }










    @PreAuthorize("hasPermission(#base_import_tests_models_o2m_child_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Base_import_tests_models_o2m_child" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/base_import_tests_models_o2m_children/{base_import_tests_models_o2m_child_id}")
    public ResponseEntity<Base_import_tests_models_o2m_childDTO> get(@PathVariable("base_import_tests_models_o2m_child_id") Integer base_import_tests_models_o2m_child_id) {
        Base_import_tests_models_o2m_child domain = base_import_tests_models_o2m_childService.get(base_import_tests_models_o2m_child_id);
        Base_import_tests_models_o2m_childDTO dto = base_import_tests_models_o2m_childMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }




    @PreAuthorize("hasPermission(#base_import_tests_models_o2m_child_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Base_import_tests_models_o2m_child" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/base_import_tests_models_o2m_children/{base_import_tests_models_o2m_child_id}")

    public ResponseEntity<Base_import_tests_models_o2m_childDTO> update(@PathVariable("base_import_tests_models_o2m_child_id") Integer base_import_tests_models_o2m_child_id, @RequestBody Base_import_tests_models_o2m_childDTO base_import_tests_models_o2m_childdto) {
		Base_import_tests_models_o2m_child domain = base_import_tests_models_o2m_childMapping.toDomain(base_import_tests_models_o2m_childdto);
        domain.setId(base_import_tests_models_o2m_child_id);
		base_import_tests_models_o2m_childService.update(domain);
		Base_import_tests_models_o2m_childDTO dto = base_import_tests_models_o2m_childMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#base_import_tests_models_o2m_child_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Base_import_tests_models_o2m_child" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/base_import_tests_models_o2m_children/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Base_import_tests_models_o2m_childDTO> base_import_tests_models_o2m_childdtos) {
        base_import_tests_models_o2m_childService.updateBatch(base_import_tests_models_o2m_childMapping.toDomain(base_import_tests_models_o2m_childdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Base_import_tests_models_o2m_child" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/base_import_tests_models_o2m_children/fetchdefault")
	public ResponseEntity<List<Base_import_tests_models_o2m_childDTO>> fetchDefault(Base_import_tests_models_o2m_childSearchContext context) {
        Page<Base_import_tests_models_o2m_child> domains = base_import_tests_models_o2m_childService.searchDefault(context) ;
        List<Base_import_tests_models_o2m_childDTO> list = base_import_tests_models_o2m_childMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Base_import_tests_models_o2m_child" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/base_import_tests_models_o2m_children/searchdefault")
	public ResponseEntity<Page<Base_import_tests_models_o2m_childDTO>> searchDefault(Base_import_tests_models_o2m_childSearchContext context) {
        Page<Base_import_tests_models_o2m_child> domains = base_import_tests_models_o2m_childService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(base_import_tests_models_o2m_childMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Base_import_tests_models_o2m_child getEntity(){
        return new Base_import_tests_models_o2m_child();
    }

}
