package cn.ibizlab.odoo.odoo_lunch.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_lunch.dto.*;
import cn.ibizlab.odoo.odoo_lunch.mapping.*;
import cn.ibizlab.odoo.core.odoo_lunch.domain.Lunch_order_line;
import cn.ibizlab.odoo.core.odoo_lunch.service.ILunch_order_lineService;
import cn.ibizlab.odoo.core.odoo_lunch.filter.Lunch_order_lineSearchContext;




@Slf4j
@Api(tags = {"Lunch_order_line" })
@RestController("odoo_lunch-lunch_order_line")
@RequestMapping("")
public class Lunch_order_lineResource {

    @Autowired
    private ILunch_order_lineService lunch_order_lineService;

    @Autowired
    @Lazy
    private Lunch_order_lineMapping lunch_order_lineMapping;




    @PreAuthorize("hasPermission(#lunch_order_line_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Lunch_order_line" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/lunch_order_lines/{lunch_order_line_id}")

    public ResponseEntity<Lunch_order_lineDTO> update(@PathVariable("lunch_order_line_id") Integer lunch_order_line_id, @RequestBody Lunch_order_lineDTO lunch_order_linedto) {
		Lunch_order_line domain = lunch_order_lineMapping.toDomain(lunch_order_linedto);
        domain.setId(lunch_order_line_id);
		lunch_order_lineService.update(domain);
		Lunch_order_lineDTO dto = lunch_order_lineMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#lunch_order_line_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Lunch_order_line" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/lunch_order_lines/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Lunch_order_lineDTO> lunch_order_linedtos) {
        lunch_order_lineService.updateBatch(lunch_order_lineMapping.toDomain(lunch_order_linedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }







    @PreAuthorize("hasPermission(#lunch_order_line_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Lunch_order_line" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/lunch_order_lines/{lunch_order_line_id}")
    public ResponseEntity<Lunch_order_lineDTO> get(@PathVariable("lunch_order_line_id") Integer lunch_order_line_id) {
        Lunch_order_line domain = lunch_order_lineService.get(lunch_order_line_id);
        Lunch_order_lineDTO dto = lunch_order_lineMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }







    @PreAuthorize("hasPermission('Remove',{#lunch_order_line_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Lunch_order_line" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/lunch_order_lines/{lunch_order_line_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("lunch_order_line_id") Integer lunch_order_line_id) {
         return ResponseEntity.status(HttpStatus.OK).body(lunch_order_lineService.remove(lunch_order_line_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Lunch_order_line" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/lunch_order_lines/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        lunch_order_lineService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }







    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Lunch_order_line" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/lunch_order_lines")

    public ResponseEntity<Lunch_order_lineDTO> create(@RequestBody Lunch_order_lineDTO lunch_order_linedto) {
        Lunch_order_line domain = lunch_order_lineMapping.toDomain(lunch_order_linedto);
		lunch_order_lineService.create(domain);
        Lunch_order_lineDTO dto = lunch_order_lineMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Lunch_order_line" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/lunch_order_lines/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Lunch_order_lineDTO> lunch_order_linedtos) {
        lunch_order_lineService.createBatch(lunch_order_lineMapping.toDomain(lunch_order_linedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Lunch_order_line" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/lunch_order_lines/fetchdefault")
	public ResponseEntity<List<Lunch_order_lineDTO>> fetchDefault(Lunch_order_lineSearchContext context) {
        Page<Lunch_order_line> domains = lunch_order_lineService.searchDefault(context) ;
        List<Lunch_order_lineDTO> list = lunch_order_lineMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Lunch_order_line" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/lunch_order_lines/searchdefault")
	public ResponseEntity<Page<Lunch_order_lineDTO>> searchDefault(Lunch_order_lineSearchContext context) {
        Page<Lunch_order_line> domains = lunch_order_lineService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(lunch_order_lineMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Lunch_order_line getEntity(){
        return new Lunch_order_line();
    }

}
