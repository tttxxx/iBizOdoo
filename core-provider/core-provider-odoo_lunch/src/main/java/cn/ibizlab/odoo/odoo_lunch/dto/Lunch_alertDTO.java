package cn.ibizlab.odoo.odoo_lunch.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;
import cn.ibizlab.odoo.util.domain.DTOBase;
import lombok.Data;

/**
 * 服务DTO对象[Lunch_alertDTO]
 */
@Data
public class Lunch_alertDTO extends DTOBase implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [SPECIFIC_DAY]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "specific_day" , format="yyyy-MM-dd")
    @JsonProperty("specific_day")
    private Timestamp specificDay;

    /**
     * 属性 [DISPLAY]
     *
     */
    @JSONField(name = "display")
    @JsonProperty("display")
    private String display;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    private String displayName;

    /**
     * 属性 [THURSDAY]
     *
     */
    @JSONField(name = "thursday")
    @JsonProperty("thursday")
    private String thursday;

    /**
     * 属性 [CREATE_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;

    /**
     * 属性 [ID]
     *
     */
    @JSONField(name = "id")
    @JsonProperty("id")
    private Integer id;

    /**
     * 属性 [WRITE_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;

    /**
     * 属性 [WEDNESDAY]
     *
     */
    @JSONField(name = "wednesday")
    @JsonProperty("wednesday")
    private String wednesday;

    /**
     * 属性 [TUESDAY]
     *
     */
    @JSONField(name = "tuesday")
    @JsonProperty("tuesday")
    private String tuesday;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;

    /**
     * 属性 [MESSAGE]
     *
     */
    @JSONField(name = "message")
    @JsonProperty("message")
    private String message;

    /**
     * 属性 [ALERT_TYPE]
     *
     */
    @JSONField(name = "alert_type")
    @JsonProperty("alert_type")
    private String alertType;

    /**
     * 属性 [ACTIVE]
     *
     */
    @JSONField(name = "active")
    @JsonProperty("active")
    private String active;

    /**
     * 属性 [SATURDAY]
     *
     */
    @JSONField(name = "saturday")
    @JsonProperty("saturday")
    private String saturday;

    /**
     * 属性 [START_HOUR]
     *
     */
    @JSONField(name = "start_hour")
    @JsonProperty("start_hour")
    private Double startHour;

    /**
     * 属性 [MONDAY]
     *
     */
    @JSONField(name = "monday")
    @JsonProperty("monday")
    private String monday;

    /**
     * 属性 [FRIDAY]
     *
     */
    @JSONField(name = "friday")
    @JsonProperty("friday")
    private String friday;

    /**
     * 属性 [END_HOUR]
     *
     */
    @JSONField(name = "end_hour")
    @JsonProperty("end_hour")
    private Double endHour;

    /**
     * 属性 [SUNDAY]
     *
     */
    @JSONField(name = "sunday")
    @JsonProperty("sunday")
    private String sunday;

    /**
     * 属性 [CREATE_UID_TEXT]
     *
     */
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    private String createUidText;

    /**
     * 属性 [WRITE_UID_TEXT]
     *
     */
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    private String writeUidText;

    /**
     * 属性 [PARTNER_ID_TEXT]
     *
     */
    @JSONField(name = "partner_id_text")
    @JsonProperty("partner_id_text")
    private String partnerIdText;

    /**
     * 属性 [CREATE_UID]
     *
     */
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    private Integer createUid;

    /**
     * 属性 [PARTNER_ID]
     *
     */
    @JSONField(name = "partner_id")
    @JsonProperty("partner_id")
    private Integer partnerId;

    /**
     * 属性 [WRITE_UID]
     *
     */
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    private Integer writeUid;


    /**
     * 设置 [SPECIFIC_DAY]
     */
    public void setSpecificDay(Timestamp  specificDay){
        this.specificDay = specificDay ;
        this.modify("specific_day",specificDay);
    }

    /**
     * 设置 [THURSDAY]
     */
    public void setThursday(String  thursday){
        this.thursday = thursday ;
        this.modify("thursday",thursday);
    }

    /**
     * 设置 [WEDNESDAY]
     */
    public void setWednesday(String  wednesday){
        this.wednesday = wednesday ;
        this.modify("wednesday",wednesday);
    }

    /**
     * 设置 [TUESDAY]
     */
    public void setTuesday(String  tuesday){
        this.tuesday = tuesday ;
        this.modify("tuesday",tuesday);
    }

    /**
     * 设置 [MESSAGE]
     */
    public void setMessage(String  message){
        this.message = message ;
        this.modify("message",message);
    }

    /**
     * 设置 [ALERT_TYPE]
     */
    public void setAlertType(String  alertType){
        this.alertType = alertType ;
        this.modify("alert_type",alertType);
    }

    /**
     * 设置 [ACTIVE]
     */
    public void setActive(String  active){
        this.active = active ;
        this.modify("active",active);
    }

    /**
     * 设置 [SATURDAY]
     */
    public void setSaturday(String  saturday){
        this.saturday = saturday ;
        this.modify("saturday",saturday);
    }

    /**
     * 设置 [START_HOUR]
     */
    public void setStartHour(Double  startHour){
        this.startHour = startHour ;
        this.modify("start_hour",startHour);
    }

    /**
     * 设置 [MONDAY]
     */
    public void setMonday(String  monday){
        this.monday = monday ;
        this.modify("monday",monday);
    }

    /**
     * 设置 [FRIDAY]
     */
    public void setFriday(String  friday){
        this.friday = friday ;
        this.modify("friday",friday);
    }

    /**
     * 设置 [END_HOUR]
     */
    public void setEndHour(Double  endHour){
        this.endHour = endHour ;
        this.modify("end_hour",endHour);
    }

    /**
     * 设置 [SUNDAY]
     */
    public void setSunday(String  sunday){
        this.sunday = sunday ;
        this.modify("sunday",sunday);
    }

    /**
     * 设置 [PARTNER_ID]
     */
    public void setPartnerId(Integer  partnerId){
        this.partnerId = partnerId ;
        this.modify("partner_id",partnerId);
    }


}

