package cn.ibizlab.odoo.odoo_lunch.mapping;

import org.mapstruct.*;
import cn.ibizlab.odoo.core.odoo_lunch.domain.Lunch_order;
import cn.ibizlab.odoo.odoo_lunch.dto.Lunch_orderDTO;
import cn.ibizlab.odoo.util.domain.MappingBase;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring", uses = {},
    nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
    nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface Lunch_orderMapping extends MappingBase<Lunch_orderDTO, Lunch_order> {


}

