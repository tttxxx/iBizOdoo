package cn.ibizlab.odoo.odoo_lunch.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_lunch.dto.*;
import cn.ibizlab.odoo.odoo_lunch.mapping.*;
import cn.ibizlab.odoo.core.odoo_lunch.domain.Lunch_cashmove;
import cn.ibizlab.odoo.core.odoo_lunch.service.ILunch_cashmoveService;
import cn.ibizlab.odoo.core.odoo_lunch.filter.Lunch_cashmoveSearchContext;




@Slf4j
@Api(tags = {"Lunch_cashmove" })
@RestController("odoo_lunch-lunch_cashmove")
@RequestMapping("")
public class Lunch_cashmoveResource {

    @Autowired
    private ILunch_cashmoveService lunch_cashmoveService;

    @Autowired
    @Lazy
    private Lunch_cashmoveMapping lunch_cashmoveMapping;




    @PreAuthorize("hasPermission('Remove',{#lunch_cashmove_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Lunch_cashmove" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/lunch_cashmoves/{lunch_cashmove_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("lunch_cashmove_id") Integer lunch_cashmove_id) {
         return ResponseEntity.status(HttpStatus.OK).body(lunch_cashmoveService.remove(lunch_cashmove_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Lunch_cashmove" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/lunch_cashmoves/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        lunch_cashmoveService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }










    @PreAuthorize("hasPermission(#lunch_cashmove_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Lunch_cashmove" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/lunch_cashmoves/{lunch_cashmove_id}")

    public ResponseEntity<Lunch_cashmoveDTO> update(@PathVariable("lunch_cashmove_id") Integer lunch_cashmove_id, @RequestBody Lunch_cashmoveDTO lunch_cashmovedto) {
		Lunch_cashmove domain = lunch_cashmoveMapping.toDomain(lunch_cashmovedto);
        domain.setId(lunch_cashmove_id);
		lunch_cashmoveService.update(domain);
		Lunch_cashmoveDTO dto = lunch_cashmoveMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#lunch_cashmove_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Lunch_cashmove" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/lunch_cashmoves/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Lunch_cashmoveDTO> lunch_cashmovedtos) {
        lunch_cashmoveService.updateBatch(lunch_cashmoveMapping.toDomain(lunch_cashmovedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }







    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Lunch_cashmove" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/lunch_cashmoves")

    public ResponseEntity<Lunch_cashmoveDTO> create(@RequestBody Lunch_cashmoveDTO lunch_cashmovedto) {
        Lunch_cashmove domain = lunch_cashmoveMapping.toDomain(lunch_cashmovedto);
		lunch_cashmoveService.create(domain);
        Lunch_cashmoveDTO dto = lunch_cashmoveMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Lunch_cashmove" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/lunch_cashmoves/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Lunch_cashmoveDTO> lunch_cashmovedtos) {
        lunch_cashmoveService.createBatch(lunch_cashmoveMapping.toDomain(lunch_cashmovedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission(#lunch_cashmove_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Lunch_cashmove" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/lunch_cashmoves/{lunch_cashmove_id}")
    public ResponseEntity<Lunch_cashmoveDTO> get(@PathVariable("lunch_cashmove_id") Integer lunch_cashmove_id) {
        Lunch_cashmove domain = lunch_cashmoveService.get(lunch_cashmove_id);
        Lunch_cashmoveDTO dto = lunch_cashmoveMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Lunch_cashmove" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/lunch_cashmoves/fetchdefault")
	public ResponseEntity<List<Lunch_cashmoveDTO>> fetchDefault(Lunch_cashmoveSearchContext context) {
        Page<Lunch_cashmove> domains = lunch_cashmoveService.searchDefault(context) ;
        List<Lunch_cashmoveDTO> list = lunch_cashmoveMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Lunch_cashmove" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/lunch_cashmoves/searchdefault")
	public ResponseEntity<Page<Lunch_cashmoveDTO>> searchDefault(Lunch_cashmoveSearchContext context) {
        Page<Lunch_cashmove> domains = lunch_cashmoveService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(lunch_cashmoveMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Lunch_cashmove getEntity(){
        return new Lunch_cashmove();
    }

}
