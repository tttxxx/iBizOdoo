package cn.ibizlab.odoo.odoo_lunch.mapping;

import org.mapstruct.*;
import cn.ibizlab.odoo.core.odoo_lunch.domain.Lunch_cashmove;
import cn.ibizlab.odoo.odoo_lunch.dto.Lunch_cashmoveDTO;
import cn.ibizlab.odoo.util.domain.MappingBase;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring", uses = {},
    nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
    nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface Lunch_cashmoveMapping extends MappingBase<Lunch_cashmoveDTO, Lunch_cashmove> {


}

