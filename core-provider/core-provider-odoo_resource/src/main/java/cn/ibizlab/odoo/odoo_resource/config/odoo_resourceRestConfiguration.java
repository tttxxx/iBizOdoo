package cn.ibizlab.odoo.odoo_resource.config;

import org.springframework.context.annotation.ComponentScan;

@ComponentScan("cn.ibizlab.odoo.odoo_resource")
public class odoo_resourceRestConfiguration {

}
