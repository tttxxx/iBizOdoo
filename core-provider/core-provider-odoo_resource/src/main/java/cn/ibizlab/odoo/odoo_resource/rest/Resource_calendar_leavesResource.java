package cn.ibizlab.odoo.odoo_resource.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_resource.dto.*;
import cn.ibizlab.odoo.odoo_resource.mapping.*;
import cn.ibizlab.odoo.core.odoo_resource.domain.Resource_calendar_leaves;
import cn.ibizlab.odoo.core.odoo_resource.service.IResource_calendar_leavesService;
import cn.ibizlab.odoo.core.odoo_resource.filter.Resource_calendar_leavesSearchContext;




@Slf4j
@Api(tags = {"Resource_calendar_leaves" })
@RestController("odoo_resource-resource_calendar_leaves")
@RequestMapping("")
public class Resource_calendar_leavesResource {

    @Autowired
    private IResource_calendar_leavesService resource_calendar_leavesService;

    @Autowired
    @Lazy
    private Resource_calendar_leavesMapping resource_calendar_leavesMapping;




    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Resource_calendar_leaves" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/resource_calendar_leaves")

    public ResponseEntity<Resource_calendar_leavesDTO> create(@RequestBody Resource_calendar_leavesDTO resource_calendar_leavesdto) {
        Resource_calendar_leaves domain = resource_calendar_leavesMapping.toDomain(resource_calendar_leavesdto);
		resource_calendar_leavesService.create(domain);
        Resource_calendar_leavesDTO dto = resource_calendar_leavesMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Resource_calendar_leaves" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/resource_calendar_leaves/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Resource_calendar_leavesDTO> resource_calendar_leavesdtos) {
        resource_calendar_leavesService.createBatch(resource_calendar_leavesMapping.toDomain(resource_calendar_leavesdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }







    @PreAuthorize("hasPermission('Remove',{#resource_calendar_leaves_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Resource_calendar_leaves" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/resource_calendar_leaves/{resource_calendar_leaves_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("resource_calendar_leaves_id") Integer resource_calendar_leaves_id) {
         return ResponseEntity.status(HttpStatus.OK).body(resource_calendar_leavesService.remove(resource_calendar_leaves_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Resource_calendar_leaves" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/resource_calendar_leaves/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        resource_calendar_leavesService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }







    @PreAuthorize("hasPermission(#resource_calendar_leaves_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Resource_calendar_leaves" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/resource_calendar_leaves/{resource_calendar_leaves_id}")

    public ResponseEntity<Resource_calendar_leavesDTO> update(@PathVariable("resource_calendar_leaves_id") Integer resource_calendar_leaves_id, @RequestBody Resource_calendar_leavesDTO resource_calendar_leavesdto) {
		Resource_calendar_leaves domain = resource_calendar_leavesMapping.toDomain(resource_calendar_leavesdto);
        domain.setId(resource_calendar_leaves_id);
		resource_calendar_leavesService.update(domain);
		Resource_calendar_leavesDTO dto = resource_calendar_leavesMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#resource_calendar_leaves_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Resource_calendar_leaves" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/resource_calendar_leaves/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Resource_calendar_leavesDTO> resource_calendar_leavesdtos) {
        resource_calendar_leavesService.updateBatch(resource_calendar_leavesMapping.toDomain(resource_calendar_leavesdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }







    @PreAuthorize("hasPermission(#resource_calendar_leaves_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Resource_calendar_leaves" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/resource_calendar_leaves/{resource_calendar_leaves_id}")
    public ResponseEntity<Resource_calendar_leavesDTO> get(@PathVariable("resource_calendar_leaves_id") Integer resource_calendar_leaves_id) {
        Resource_calendar_leaves domain = resource_calendar_leavesService.get(resource_calendar_leaves_id);
        Resource_calendar_leavesDTO dto = resource_calendar_leavesMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Resource_calendar_leaves" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/resource_calendar_leaves/fetchdefault")
	public ResponseEntity<List<Resource_calendar_leavesDTO>> fetchDefault(Resource_calendar_leavesSearchContext context) {
        Page<Resource_calendar_leaves> domains = resource_calendar_leavesService.searchDefault(context) ;
        List<Resource_calendar_leavesDTO> list = resource_calendar_leavesMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Resource_calendar_leaves" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/resource_calendar_leaves/searchdefault")
	public ResponseEntity<Page<Resource_calendar_leavesDTO>> searchDefault(Resource_calendar_leavesSearchContext context) {
        Page<Resource_calendar_leaves> domains = resource_calendar_leavesService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(resource_calendar_leavesMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Resource_calendar_leaves getEntity(){
        return new Resource_calendar_leaves();
    }

}
