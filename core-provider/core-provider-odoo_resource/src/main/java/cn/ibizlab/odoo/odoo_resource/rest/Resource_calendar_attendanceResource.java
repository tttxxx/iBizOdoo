package cn.ibizlab.odoo.odoo_resource.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_resource.dto.*;
import cn.ibizlab.odoo.odoo_resource.mapping.*;
import cn.ibizlab.odoo.core.odoo_resource.domain.Resource_calendar_attendance;
import cn.ibizlab.odoo.core.odoo_resource.service.IResource_calendar_attendanceService;
import cn.ibizlab.odoo.core.odoo_resource.filter.Resource_calendar_attendanceSearchContext;




@Slf4j
@Api(tags = {"Resource_calendar_attendance" })
@RestController("odoo_resource-resource_calendar_attendance")
@RequestMapping("")
public class Resource_calendar_attendanceResource {

    @Autowired
    private IResource_calendar_attendanceService resource_calendar_attendanceService;

    @Autowired
    @Lazy
    private Resource_calendar_attendanceMapping resource_calendar_attendanceMapping;




    @PreAuthorize("hasPermission('Remove',{#resource_calendar_attendance_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Resource_calendar_attendance" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/resource_calendar_attendances/{resource_calendar_attendance_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("resource_calendar_attendance_id") Integer resource_calendar_attendance_id) {
         return ResponseEntity.status(HttpStatus.OK).body(resource_calendar_attendanceService.remove(resource_calendar_attendance_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Resource_calendar_attendance" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/resource_calendar_attendances/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        resource_calendar_attendanceService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission(#resource_calendar_attendance_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Resource_calendar_attendance" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/resource_calendar_attendances/{resource_calendar_attendance_id}")
    public ResponseEntity<Resource_calendar_attendanceDTO> get(@PathVariable("resource_calendar_attendance_id") Integer resource_calendar_attendance_id) {
        Resource_calendar_attendance domain = resource_calendar_attendanceService.get(resource_calendar_attendance_id);
        Resource_calendar_attendanceDTO dto = resource_calendar_attendanceMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }







    @PreAuthorize("hasPermission(#resource_calendar_attendance_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Resource_calendar_attendance" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/resource_calendar_attendances/{resource_calendar_attendance_id}")

    public ResponseEntity<Resource_calendar_attendanceDTO> update(@PathVariable("resource_calendar_attendance_id") Integer resource_calendar_attendance_id, @RequestBody Resource_calendar_attendanceDTO resource_calendar_attendancedto) {
		Resource_calendar_attendance domain = resource_calendar_attendanceMapping.toDomain(resource_calendar_attendancedto);
        domain.setId(resource_calendar_attendance_id);
		resource_calendar_attendanceService.update(domain);
		Resource_calendar_attendanceDTO dto = resource_calendar_attendanceMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#resource_calendar_attendance_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Resource_calendar_attendance" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/resource_calendar_attendances/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Resource_calendar_attendanceDTO> resource_calendar_attendancedtos) {
        resource_calendar_attendanceService.updateBatch(resource_calendar_attendanceMapping.toDomain(resource_calendar_attendancedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }







    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Resource_calendar_attendance" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/resource_calendar_attendances")

    public ResponseEntity<Resource_calendar_attendanceDTO> create(@RequestBody Resource_calendar_attendanceDTO resource_calendar_attendancedto) {
        Resource_calendar_attendance domain = resource_calendar_attendanceMapping.toDomain(resource_calendar_attendancedto);
		resource_calendar_attendanceService.create(domain);
        Resource_calendar_attendanceDTO dto = resource_calendar_attendanceMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Resource_calendar_attendance" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/resource_calendar_attendances/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Resource_calendar_attendanceDTO> resource_calendar_attendancedtos) {
        resource_calendar_attendanceService.createBatch(resource_calendar_attendanceMapping.toDomain(resource_calendar_attendancedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Resource_calendar_attendance" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/resource_calendar_attendances/fetchdefault")
	public ResponseEntity<List<Resource_calendar_attendanceDTO>> fetchDefault(Resource_calendar_attendanceSearchContext context) {
        Page<Resource_calendar_attendance> domains = resource_calendar_attendanceService.searchDefault(context) ;
        List<Resource_calendar_attendanceDTO> list = resource_calendar_attendanceMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Resource_calendar_attendance" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/resource_calendar_attendances/searchdefault")
	public ResponseEntity<Page<Resource_calendar_attendanceDTO>> searchDefault(Resource_calendar_attendanceSearchContext context) {
        Page<Resource_calendar_attendance> domains = resource_calendar_attendanceService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(resource_calendar_attendanceMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Resource_calendar_attendance getEntity(){
        return new Resource_calendar_attendance();
    }

}
