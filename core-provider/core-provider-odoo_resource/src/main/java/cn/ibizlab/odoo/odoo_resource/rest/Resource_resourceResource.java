package cn.ibizlab.odoo.odoo_resource.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_resource.dto.*;
import cn.ibizlab.odoo.odoo_resource.mapping.*;
import cn.ibizlab.odoo.core.odoo_resource.domain.Resource_resource;
import cn.ibizlab.odoo.core.odoo_resource.service.IResource_resourceService;
import cn.ibizlab.odoo.core.odoo_resource.filter.Resource_resourceSearchContext;




@Slf4j
@Api(tags = {"Resource_resource" })
@RestController("odoo_resource-resource_resource")
@RequestMapping("")
public class Resource_resourceResource {

    @Autowired
    private IResource_resourceService resource_resourceService;

    @Autowired
    @Lazy
    private Resource_resourceMapping resource_resourceMapping;










    @PreAuthorize("hasPermission(#resource_resource_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Resource_resource" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/resource_resources/{resource_resource_id}")

    public ResponseEntity<Resource_resourceDTO> update(@PathVariable("resource_resource_id") Integer resource_resource_id, @RequestBody Resource_resourceDTO resource_resourcedto) {
		Resource_resource domain = resource_resourceMapping.toDomain(resource_resourcedto);
        domain.setId(resource_resource_id);
		resource_resourceService.update(domain);
		Resource_resourceDTO dto = resource_resourceMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#resource_resource_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Resource_resource" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/resource_resources/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Resource_resourceDTO> resource_resourcedtos) {
        resource_resourceService.updateBatch(resource_resourceMapping.toDomain(resource_resourcedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission(#resource_resource_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Resource_resource" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/resource_resources/{resource_resource_id}")
    public ResponseEntity<Resource_resourceDTO> get(@PathVariable("resource_resource_id") Integer resource_resource_id) {
        Resource_resource domain = resource_resourceService.get(resource_resource_id);
        Resource_resourceDTO dto = resource_resourceMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }




    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Resource_resource" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/resource_resources")

    public ResponseEntity<Resource_resourceDTO> create(@RequestBody Resource_resourceDTO resource_resourcedto) {
        Resource_resource domain = resource_resourceMapping.toDomain(resource_resourcedto);
		resource_resourceService.create(domain);
        Resource_resourceDTO dto = resource_resourceMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Resource_resource" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/resource_resources/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Resource_resourceDTO> resource_resourcedtos) {
        resource_resourceService.createBatch(resource_resourceMapping.toDomain(resource_resourcedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }







    @PreAuthorize("hasPermission('Remove',{#resource_resource_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Resource_resource" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/resource_resources/{resource_resource_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("resource_resource_id") Integer resource_resource_id) {
         return ResponseEntity.status(HttpStatus.OK).body(resource_resourceService.remove(resource_resource_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Resource_resource" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/resource_resources/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        resource_resourceService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Resource_resource" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/resource_resources/fetchdefault")
	public ResponseEntity<List<Resource_resourceDTO>> fetchDefault(Resource_resourceSearchContext context) {
        Page<Resource_resource> domains = resource_resourceService.searchDefault(context) ;
        List<Resource_resourceDTO> list = resource_resourceMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Resource_resource" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/resource_resources/searchdefault")
	public ResponseEntity<Page<Resource_resourceDTO>> searchDefault(Resource_resourceSearchContext context) {
        Page<Resource_resource> domains = resource_resourceService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(resource_resourceMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Resource_resource getEntity(){
        return new Resource_resource();
    }

}
