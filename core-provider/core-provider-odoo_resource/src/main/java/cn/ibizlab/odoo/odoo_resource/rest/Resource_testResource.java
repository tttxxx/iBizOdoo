package cn.ibizlab.odoo.odoo_resource.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_resource.dto.*;
import cn.ibizlab.odoo.odoo_resource.mapping.*;
import cn.ibizlab.odoo.core.odoo_resource.domain.Resource_test;
import cn.ibizlab.odoo.core.odoo_resource.service.IResource_testService;
import cn.ibizlab.odoo.core.odoo_resource.filter.Resource_testSearchContext;




@Slf4j
@Api(tags = {"Resource_test" })
@RestController("odoo_resource-resource_test")
@RequestMapping("")
public class Resource_testResource {

    @Autowired
    private IResource_testService resource_testService;

    @Autowired
    @Lazy
    private Resource_testMapping resource_testMapping;




    @PreAuthorize("hasPermission('Remove',{#resource_test_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Resource_test" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/resource_tests/{resource_test_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("resource_test_id") Integer resource_test_id) {
         return ResponseEntity.status(HttpStatus.OK).body(resource_testService.remove(resource_test_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Resource_test" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/resource_tests/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        resource_testService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission(#resource_test_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Resource_test" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/resource_tests/{resource_test_id}")

    public ResponseEntity<Resource_testDTO> update(@PathVariable("resource_test_id") Integer resource_test_id, @RequestBody Resource_testDTO resource_testdto) {
		Resource_test domain = resource_testMapping.toDomain(resource_testdto);
        domain.setId(resource_test_id);
		resource_testService.update(domain);
		Resource_testDTO dto = resource_testMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#resource_test_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Resource_test" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/resource_tests/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Resource_testDTO> resource_testdtos) {
        resource_testService.updateBatch(resource_testMapping.toDomain(resource_testdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }







    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Resource_test" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/resource_tests")

    public ResponseEntity<Resource_testDTO> create(@RequestBody Resource_testDTO resource_testdto) {
        Resource_test domain = resource_testMapping.toDomain(resource_testdto);
		resource_testService.create(domain);
        Resource_testDTO dto = resource_testMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Resource_test" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/resource_tests/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Resource_testDTO> resource_testdtos) {
        resource_testService.createBatch(resource_testMapping.toDomain(resource_testdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }







    @PreAuthorize("hasPermission(#resource_test_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Resource_test" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/resource_tests/{resource_test_id}")
    public ResponseEntity<Resource_testDTO> get(@PathVariable("resource_test_id") Integer resource_test_id) {
        Resource_test domain = resource_testService.get(resource_test_id);
        Resource_testDTO dto = resource_testMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }




    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Resource_test" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/resource_tests/fetchdefault")
	public ResponseEntity<List<Resource_testDTO>> fetchDefault(Resource_testSearchContext context) {
        Page<Resource_test> domains = resource_testService.searchDefault(context) ;
        List<Resource_testDTO> list = resource_testMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Resource_test" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/resource_tests/searchdefault")
	public ResponseEntity<Page<Resource_testDTO>> searchDefault(Resource_testSearchContext context) {
        Page<Resource_test> domains = resource_testService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(resource_testMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Resource_test getEntity(){
        return new Resource_test();
    }

}
