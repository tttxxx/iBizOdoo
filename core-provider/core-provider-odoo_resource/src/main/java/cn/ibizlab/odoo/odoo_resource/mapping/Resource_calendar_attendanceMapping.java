package cn.ibizlab.odoo.odoo_resource.mapping;

import org.mapstruct.*;
import cn.ibizlab.odoo.core.odoo_resource.domain.Resource_calendar_attendance;
import cn.ibizlab.odoo.odoo_resource.dto.Resource_calendar_attendanceDTO;
import cn.ibizlab.odoo.util.domain.MappingBase;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring", uses = {},
    nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
    nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface Resource_calendar_attendanceMapping extends MappingBase<Resource_calendar_attendanceDTO, Resource_calendar_attendance> {


}

