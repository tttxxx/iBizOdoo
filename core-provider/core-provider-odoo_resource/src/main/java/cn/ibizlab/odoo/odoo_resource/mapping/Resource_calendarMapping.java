package cn.ibizlab.odoo.odoo_resource.mapping;

import org.mapstruct.*;
import cn.ibizlab.odoo.core.odoo_resource.domain.Resource_calendar;
import cn.ibizlab.odoo.odoo_resource.dto.Resource_calendarDTO;
import cn.ibizlab.odoo.util.domain.MappingBase;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring", uses = {},
    nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
    nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface Resource_calendarMapping extends MappingBase<Resource_calendarDTO, Resource_calendar> {


}

