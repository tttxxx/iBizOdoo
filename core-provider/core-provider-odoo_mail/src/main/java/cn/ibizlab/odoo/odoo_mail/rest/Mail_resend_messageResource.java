package cn.ibizlab.odoo.odoo_mail.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_mail.dto.*;
import cn.ibizlab.odoo.odoo_mail.mapping.*;
import cn.ibizlab.odoo.core.odoo_mail.domain.Mail_resend_message;
import cn.ibizlab.odoo.core.odoo_mail.service.IMail_resend_messageService;
import cn.ibizlab.odoo.core.odoo_mail.filter.Mail_resend_messageSearchContext;




@Slf4j
@Api(tags = {"Mail_resend_message" })
@RestController("odoo_mail-mail_resend_message")
@RequestMapping("")
public class Mail_resend_messageResource {

    @Autowired
    private IMail_resend_messageService mail_resend_messageService;

    @Autowired
    @Lazy
    private Mail_resend_messageMapping mail_resend_messageMapping;




    @PreAuthorize("hasPermission(#mail_resend_message_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Mail_resend_message" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/mail_resend_messages/{mail_resend_message_id}")
    public ResponseEntity<Mail_resend_messageDTO> get(@PathVariable("mail_resend_message_id") Integer mail_resend_message_id) {
        Mail_resend_message domain = mail_resend_messageService.get(mail_resend_message_id);
        Mail_resend_messageDTO dto = mail_resend_messageMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }







    @PreAuthorize("hasPermission('Remove',{#mail_resend_message_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Mail_resend_message" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/mail_resend_messages/{mail_resend_message_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("mail_resend_message_id") Integer mail_resend_message_id) {
         return ResponseEntity.status(HttpStatus.OK).body(mail_resend_messageService.remove(mail_resend_message_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Mail_resend_message" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/mail_resend_messages/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        mail_resend_messageService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }







    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Mail_resend_message" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/mail_resend_messages")

    public ResponseEntity<Mail_resend_messageDTO> create(@RequestBody Mail_resend_messageDTO mail_resend_messagedto) {
        Mail_resend_message domain = mail_resend_messageMapping.toDomain(mail_resend_messagedto);
		mail_resend_messageService.create(domain);
        Mail_resend_messageDTO dto = mail_resend_messageMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Mail_resend_message" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/mail_resend_messages/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Mail_resend_messageDTO> mail_resend_messagedtos) {
        mail_resend_messageService.createBatch(mail_resend_messageMapping.toDomain(mail_resend_messagedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }







    @PreAuthorize("hasPermission(#mail_resend_message_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Mail_resend_message" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/mail_resend_messages/{mail_resend_message_id}")

    public ResponseEntity<Mail_resend_messageDTO> update(@PathVariable("mail_resend_message_id") Integer mail_resend_message_id, @RequestBody Mail_resend_messageDTO mail_resend_messagedto) {
		Mail_resend_message domain = mail_resend_messageMapping.toDomain(mail_resend_messagedto);
        domain.setId(mail_resend_message_id);
		mail_resend_messageService.update(domain);
		Mail_resend_messageDTO dto = mail_resend_messageMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#mail_resend_message_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Mail_resend_message" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/mail_resend_messages/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Mail_resend_messageDTO> mail_resend_messagedtos) {
        mail_resend_messageService.updateBatch(mail_resend_messageMapping.toDomain(mail_resend_messagedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Mail_resend_message" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/mail_resend_messages/fetchdefault")
	public ResponseEntity<List<Mail_resend_messageDTO>> fetchDefault(Mail_resend_messageSearchContext context) {
        Page<Mail_resend_message> domains = mail_resend_messageService.searchDefault(context) ;
        List<Mail_resend_messageDTO> list = mail_resend_messageMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Mail_resend_message" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/mail_resend_messages/searchdefault")
	public ResponseEntity<Page<Mail_resend_messageDTO>> searchDefault(Mail_resend_messageSearchContext context) {
        Page<Mail_resend_message> domains = mail_resend_messageService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(mail_resend_messageMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Mail_resend_message getEntity(){
        return new Mail_resend_message();
    }

}
