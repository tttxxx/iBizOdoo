package cn.ibizlab.odoo.odoo_mail.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_mail.dto.*;
import cn.ibizlab.odoo.odoo_mail.mapping.*;
import cn.ibizlab.odoo.core.odoo_mail.domain.Mail_mass_mailing_list;
import cn.ibizlab.odoo.core.odoo_mail.service.IMail_mass_mailing_listService;
import cn.ibizlab.odoo.core.odoo_mail.filter.Mail_mass_mailing_listSearchContext;




@Slf4j
@Api(tags = {"Mail_mass_mailing_list" })
@RestController("odoo_mail-mail_mass_mailing_list")
@RequestMapping("")
public class Mail_mass_mailing_listResource {

    @Autowired
    private IMail_mass_mailing_listService mail_mass_mailing_listService;

    @Autowired
    @Lazy
    private Mail_mass_mailing_listMapping mail_mass_mailing_listMapping;




    @PreAuthorize("hasPermission(#mail_mass_mailing_list_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Mail_mass_mailing_list" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/mail_mass_mailing_lists/{mail_mass_mailing_list_id}")

    public ResponseEntity<Mail_mass_mailing_listDTO> update(@PathVariable("mail_mass_mailing_list_id") Integer mail_mass_mailing_list_id, @RequestBody Mail_mass_mailing_listDTO mail_mass_mailing_listdto) {
		Mail_mass_mailing_list domain = mail_mass_mailing_listMapping.toDomain(mail_mass_mailing_listdto);
        domain.setId(mail_mass_mailing_list_id);
		mail_mass_mailing_listService.update(domain);
		Mail_mass_mailing_listDTO dto = mail_mass_mailing_listMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#mail_mass_mailing_list_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Mail_mass_mailing_list" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/mail_mass_mailing_lists/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Mail_mass_mailing_listDTO> mail_mass_mailing_listdtos) {
        mail_mass_mailing_listService.updateBatch(mail_mass_mailing_listMapping.toDomain(mail_mass_mailing_listdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Mail_mass_mailing_list" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/mail_mass_mailing_lists")

    public ResponseEntity<Mail_mass_mailing_listDTO> create(@RequestBody Mail_mass_mailing_listDTO mail_mass_mailing_listdto) {
        Mail_mass_mailing_list domain = mail_mass_mailing_listMapping.toDomain(mail_mass_mailing_listdto);
		mail_mass_mailing_listService.create(domain);
        Mail_mass_mailing_listDTO dto = mail_mass_mailing_listMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Mail_mass_mailing_list" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/mail_mass_mailing_lists/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Mail_mass_mailing_listDTO> mail_mass_mailing_listdtos) {
        mail_mass_mailing_listService.createBatch(mail_mass_mailing_listMapping.toDomain(mail_mass_mailing_listdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission(#mail_mass_mailing_list_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Mail_mass_mailing_list" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/mail_mass_mailing_lists/{mail_mass_mailing_list_id}")
    public ResponseEntity<Mail_mass_mailing_listDTO> get(@PathVariable("mail_mass_mailing_list_id") Integer mail_mass_mailing_list_id) {
        Mail_mass_mailing_list domain = mail_mass_mailing_listService.get(mail_mass_mailing_list_id);
        Mail_mass_mailing_listDTO dto = mail_mass_mailing_listMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }










    @PreAuthorize("hasPermission('Remove',{#mail_mass_mailing_list_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Mail_mass_mailing_list" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/mail_mass_mailing_lists/{mail_mass_mailing_list_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("mail_mass_mailing_list_id") Integer mail_mass_mailing_list_id) {
         return ResponseEntity.status(HttpStatus.OK).body(mail_mass_mailing_listService.remove(mail_mass_mailing_list_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Mail_mass_mailing_list" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/mail_mass_mailing_lists/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        mail_mass_mailing_listService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Mail_mass_mailing_list" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/mail_mass_mailing_lists/fetchdefault")
	public ResponseEntity<List<Mail_mass_mailing_listDTO>> fetchDefault(Mail_mass_mailing_listSearchContext context) {
        Page<Mail_mass_mailing_list> domains = mail_mass_mailing_listService.searchDefault(context) ;
        List<Mail_mass_mailing_listDTO> list = mail_mass_mailing_listMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Mail_mass_mailing_list" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/mail_mass_mailing_lists/searchdefault")
	public ResponseEntity<Page<Mail_mass_mailing_listDTO>> searchDefault(Mail_mass_mailing_listSearchContext context) {
        Page<Mail_mass_mailing_list> domains = mail_mass_mailing_listService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(mail_mass_mailing_listMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Mail_mass_mailing_list getEntity(){
        return new Mail_mass_mailing_list();
    }

}
