package cn.ibizlab.odoo.odoo_mail.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_mail.dto.*;
import cn.ibizlab.odoo.odoo_mail.mapping.*;
import cn.ibizlab.odoo.core.odoo_mail.domain.Mail_channel_partner;
import cn.ibizlab.odoo.core.odoo_mail.service.IMail_channel_partnerService;
import cn.ibizlab.odoo.core.odoo_mail.filter.Mail_channel_partnerSearchContext;




@Slf4j
@Api(tags = {"Mail_channel_partner" })
@RestController("odoo_mail-mail_channel_partner")
@RequestMapping("")
public class Mail_channel_partnerResource {

    @Autowired
    private IMail_channel_partnerService mail_channel_partnerService;

    @Autowired
    @Lazy
    private Mail_channel_partnerMapping mail_channel_partnerMapping;







    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Mail_channel_partner" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/mail_channel_partners")

    public ResponseEntity<Mail_channel_partnerDTO> create(@RequestBody Mail_channel_partnerDTO mail_channel_partnerdto) {
        Mail_channel_partner domain = mail_channel_partnerMapping.toDomain(mail_channel_partnerdto);
		mail_channel_partnerService.create(domain);
        Mail_channel_partnerDTO dto = mail_channel_partnerMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Mail_channel_partner" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/mail_channel_partners/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Mail_channel_partnerDTO> mail_channel_partnerdtos) {
        mail_channel_partnerService.createBatch(mail_channel_partnerMapping.toDomain(mail_channel_partnerdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }







    @PreAuthorize("hasPermission('Remove',{#mail_channel_partner_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Mail_channel_partner" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/mail_channel_partners/{mail_channel_partner_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("mail_channel_partner_id") Integer mail_channel_partner_id) {
         return ResponseEntity.status(HttpStatus.OK).body(mail_channel_partnerService.remove(mail_channel_partner_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Mail_channel_partner" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/mail_channel_partners/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        mail_channel_partnerService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission(#mail_channel_partner_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Mail_channel_partner" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/mail_channel_partners/{mail_channel_partner_id}")
    public ResponseEntity<Mail_channel_partnerDTO> get(@PathVariable("mail_channel_partner_id") Integer mail_channel_partner_id) {
        Mail_channel_partner domain = mail_channel_partnerService.get(mail_channel_partner_id);
        Mail_channel_partnerDTO dto = mail_channel_partnerMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }




    @PreAuthorize("hasPermission(#mail_channel_partner_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Mail_channel_partner" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/mail_channel_partners/{mail_channel_partner_id}")

    public ResponseEntity<Mail_channel_partnerDTO> update(@PathVariable("mail_channel_partner_id") Integer mail_channel_partner_id, @RequestBody Mail_channel_partnerDTO mail_channel_partnerdto) {
		Mail_channel_partner domain = mail_channel_partnerMapping.toDomain(mail_channel_partnerdto);
        domain.setId(mail_channel_partner_id);
		mail_channel_partnerService.update(domain);
		Mail_channel_partnerDTO dto = mail_channel_partnerMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#mail_channel_partner_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Mail_channel_partner" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/mail_channel_partners/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Mail_channel_partnerDTO> mail_channel_partnerdtos) {
        mail_channel_partnerService.updateBatch(mail_channel_partnerMapping.toDomain(mail_channel_partnerdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Mail_channel_partner" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/mail_channel_partners/fetchdefault")
	public ResponseEntity<List<Mail_channel_partnerDTO>> fetchDefault(Mail_channel_partnerSearchContext context) {
        Page<Mail_channel_partner> domains = mail_channel_partnerService.searchDefault(context) ;
        List<Mail_channel_partnerDTO> list = mail_channel_partnerMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Mail_channel_partner" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/mail_channel_partners/searchdefault")
	public ResponseEntity<Page<Mail_channel_partnerDTO>> searchDefault(Mail_channel_partnerSearchContext context) {
        Page<Mail_channel_partner> domains = mail_channel_partnerService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(mail_channel_partnerMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Mail_channel_partner getEntity(){
        return new Mail_channel_partner();
    }

}
