package cn.ibizlab.odoo.odoo_mail.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_mail.dto.*;
import cn.ibizlab.odoo.odoo_mail.mapping.*;
import cn.ibizlab.odoo.core.odoo_mail.domain.Mail_activity_mixin;
import cn.ibizlab.odoo.core.odoo_mail.service.IMail_activity_mixinService;
import cn.ibizlab.odoo.core.odoo_mail.filter.Mail_activity_mixinSearchContext;




@Slf4j
@Api(tags = {"Mail_activity_mixin" })
@RestController("odoo_mail-mail_activity_mixin")
@RequestMapping("")
public class Mail_activity_mixinResource {

    @Autowired
    private IMail_activity_mixinService mail_activity_mixinService;

    @Autowired
    @Lazy
    private Mail_activity_mixinMapping mail_activity_mixinMapping;




    @PreAuthorize("hasPermission(#mail_activity_mixin_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Mail_activity_mixin" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/mail_activity_mixins/{mail_activity_mixin_id}")
    public ResponseEntity<Mail_activity_mixinDTO> get(@PathVariable("mail_activity_mixin_id") Integer mail_activity_mixin_id) {
        Mail_activity_mixin domain = mail_activity_mixinService.get(mail_activity_mixin_id);
        Mail_activity_mixinDTO dto = mail_activity_mixinMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }




    @PreAuthorize("hasPermission(#mail_activity_mixin_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Mail_activity_mixin" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/mail_activity_mixins/{mail_activity_mixin_id}")

    public ResponseEntity<Mail_activity_mixinDTO> update(@PathVariable("mail_activity_mixin_id") Integer mail_activity_mixin_id, @RequestBody Mail_activity_mixinDTO mail_activity_mixindto) {
		Mail_activity_mixin domain = mail_activity_mixinMapping.toDomain(mail_activity_mixindto);
        domain.setId(mail_activity_mixin_id);
		mail_activity_mixinService.update(domain);
		Mail_activity_mixinDTO dto = mail_activity_mixinMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#mail_activity_mixin_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Mail_activity_mixin" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/mail_activity_mixins/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Mail_activity_mixinDTO> mail_activity_mixindtos) {
        mail_activity_mixinService.updateBatch(mail_activity_mixinMapping.toDomain(mail_activity_mixindtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Mail_activity_mixin" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/mail_activity_mixins")

    public ResponseEntity<Mail_activity_mixinDTO> create(@RequestBody Mail_activity_mixinDTO mail_activity_mixindto) {
        Mail_activity_mixin domain = mail_activity_mixinMapping.toDomain(mail_activity_mixindto);
		mail_activity_mixinService.create(domain);
        Mail_activity_mixinDTO dto = mail_activity_mixinMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Mail_activity_mixin" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/mail_activity_mixins/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Mail_activity_mixinDTO> mail_activity_mixindtos) {
        mail_activity_mixinService.createBatch(mail_activity_mixinMapping.toDomain(mail_activity_mixindtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }













    @PreAuthorize("hasPermission('Remove',{#mail_activity_mixin_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Mail_activity_mixin" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/mail_activity_mixins/{mail_activity_mixin_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("mail_activity_mixin_id") Integer mail_activity_mixin_id) {
         return ResponseEntity.status(HttpStatus.OK).body(mail_activity_mixinService.remove(mail_activity_mixin_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Mail_activity_mixin" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/mail_activity_mixins/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        mail_activity_mixinService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Mail_activity_mixin" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/mail_activity_mixins/fetchdefault")
	public ResponseEntity<List<Mail_activity_mixinDTO>> fetchDefault(Mail_activity_mixinSearchContext context) {
        Page<Mail_activity_mixin> domains = mail_activity_mixinService.searchDefault(context) ;
        List<Mail_activity_mixinDTO> list = mail_activity_mixinMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Mail_activity_mixin" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/mail_activity_mixins/searchdefault")
	public ResponseEntity<Page<Mail_activity_mixinDTO>> searchDefault(Mail_activity_mixinSearchContext context) {
        Page<Mail_activity_mixin> domains = mail_activity_mixinService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(mail_activity_mixinMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Mail_activity_mixin getEntity(){
        return new Mail_activity_mixin();
    }

}
