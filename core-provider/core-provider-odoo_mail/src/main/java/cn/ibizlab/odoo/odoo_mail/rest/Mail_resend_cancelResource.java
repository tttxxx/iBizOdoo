package cn.ibizlab.odoo.odoo_mail.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_mail.dto.*;
import cn.ibizlab.odoo.odoo_mail.mapping.*;
import cn.ibizlab.odoo.core.odoo_mail.domain.Mail_resend_cancel;
import cn.ibizlab.odoo.core.odoo_mail.service.IMail_resend_cancelService;
import cn.ibizlab.odoo.core.odoo_mail.filter.Mail_resend_cancelSearchContext;




@Slf4j
@Api(tags = {"Mail_resend_cancel" })
@RestController("odoo_mail-mail_resend_cancel")
@RequestMapping("")
public class Mail_resend_cancelResource {

    @Autowired
    private IMail_resend_cancelService mail_resend_cancelService;

    @Autowired
    @Lazy
    private Mail_resend_cancelMapping mail_resend_cancelMapping;










    @PreAuthorize("hasPermission(#mail_resend_cancel_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Mail_resend_cancel" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/mail_resend_cancels/{mail_resend_cancel_id}")

    public ResponseEntity<Mail_resend_cancelDTO> update(@PathVariable("mail_resend_cancel_id") Integer mail_resend_cancel_id, @RequestBody Mail_resend_cancelDTO mail_resend_canceldto) {
		Mail_resend_cancel domain = mail_resend_cancelMapping.toDomain(mail_resend_canceldto);
        domain.setId(mail_resend_cancel_id);
		mail_resend_cancelService.update(domain);
		Mail_resend_cancelDTO dto = mail_resend_cancelMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#mail_resend_cancel_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Mail_resend_cancel" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/mail_resend_cancels/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Mail_resend_cancelDTO> mail_resend_canceldtos) {
        mail_resend_cancelService.updateBatch(mail_resend_cancelMapping.toDomain(mail_resend_canceldtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }







    @PreAuthorize("hasPermission('Remove',{#mail_resend_cancel_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Mail_resend_cancel" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/mail_resend_cancels/{mail_resend_cancel_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("mail_resend_cancel_id") Integer mail_resend_cancel_id) {
         return ResponseEntity.status(HttpStatus.OK).body(mail_resend_cancelService.remove(mail_resend_cancel_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Mail_resend_cancel" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/mail_resend_cancels/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        mail_resend_cancelService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Mail_resend_cancel" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/mail_resend_cancels")

    public ResponseEntity<Mail_resend_cancelDTO> create(@RequestBody Mail_resend_cancelDTO mail_resend_canceldto) {
        Mail_resend_cancel domain = mail_resend_cancelMapping.toDomain(mail_resend_canceldto);
		mail_resend_cancelService.create(domain);
        Mail_resend_cancelDTO dto = mail_resend_cancelMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Mail_resend_cancel" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/mail_resend_cancels/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Mail_resend_cancelDTO> mail_resend_canceldtos) {
        mail_resend_cancelService.createBatch(mail_resend_cancelMapping.toDomain(mail_resend_canceldtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission(#mail_resend_cancel_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Mail_resend_cancel" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/mail_resend_cancels/{mail_resend_cancel_id}")
    public ResponseEntity<Mail_resend_cancelDTO> get(@PathVariable("mail_resend_cancel_id") Integer mail_resend_cancel_id) {
        Mail_resend_cancel domain = mail_resend_cancelService.get(mail_resend_cancel_id);
        Mail_resend_cancelDTO dto = mail_resend_cancelMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Mail_resend_cancel" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/mail_resend_cancels/fetchdefault")
	public ResponseEntity<List<Mail_resend_cancelDTO>> fetchDefault(Mail_resend_cancelSearchContext context) {
        Page<Mail_resend_cancel> domains = mail_resend_cancelService.searchDefault(context) ;
        List<Mail_resend_cancelDTO> list = mail_resend_cancelMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Mail_resend_cancel" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/mail_resend_cancels/searchdefault")
	public ResponseEntity<Page<Mail_resend_cancelDTO>> searchDefault(Mail_resend_cancelSearchContext context) {
        Page<Mail_resend_cancel> domains = mail_resend_cancelService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(mail_resend_cancelMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Mail_resend_cancel getEntity(){
        return new Mail_resend_cancel();
    }

}
