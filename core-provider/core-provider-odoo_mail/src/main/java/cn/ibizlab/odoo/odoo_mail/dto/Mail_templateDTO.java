package cn.ibizlab.odoo.odoo_mail.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;
import cn.ibizlab.odoo.util.domain.DTOBase;
import lombok.Data;

/**
 * 服务DTO对象[Mail_templateDTO]
 */
@Data
public class Mail_templateDTO extends DTOBase implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [ID]
     *
     */
    @JSONField(name = "id")
    @JsonProperty("id")
    private Integer id;

    /**
     * 属性 [USE_DEFAULT_TO]
     *
     */
    @JSONField(name = "use_default_to")
    @JsonProperty("use_default_to")
    private String useDefaultTo;

    /**
     * 属性 [SUBJECT]
     *
     */
    @JSONField(name = "subject")
    @JsonProperty("subject")
    private String subject;

    /**
     * 属性 [ATTACHMENT_IDS]
     *
     */
    @JSONField(name = "attachment_ids")
    @JsonProperty("attachment_ids")
    private String attachmentIds;

    /**
     * 属性 [WRITE_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;

    /**
     * 属性 [REPLY_TO]
     *
     */
    @JSONField(name = "reply_to")
    @JsonProperty("reply_to")
    private String replyTo;

    /**
     * 属性 [SUB_OBJECT]
     *
     */
    @JSONField(name = "sub_object")
    @JsonProperty("sub_object")
    private Integer subObject;

    /**
     * 属性 [SCHEDULED_DATE]
     *
     */
    @JSONField(name = "scheduled_date")
    @JsonProperty("scheduled_date")
    private String scheduledDate;

    /**
     * 属性 [MODEL_ID]
     *
     */
    @JSONField(name = "model_id")
    @JsonProperty("model_id")
    private Integer modelId;

    /**
     * 属性 [MAIL_SERVER_ID]
     *
     */
    @JSONField(name = "mail_server_id")
    @JsonProperty("mail_server_id")
    private Integer mailServerId;

    /**
     * 属性 [REPORT_TEMPLATE]
     *
     */
    @JSONField(name = "report_template")
    @JsonProperty("report_template")
    private Integer reportTemplate;

    /**
     * 属性 [BODY_HTML]
     *
     */
    @JSONField(name = "body_html")
    @JsonProperty("body_html")
    private String bodyHtml;

    /**
     * 属性 [REF_IR_ACT_WINDOW]
     *
     */
    @JSONField(name = "ref_ir_act_window")
    @JsonProperty("ref_ir_act_window")
    private Integer refIrActWindow;

    /**
     * 属性 [NAME]
     *
     */
    @JSONField(name = "name")
    @JsonProperty("name")
    private String name;

    /**
     * 属性 [PARTNER_TO]
     *
     */
    @JSONField(name = "partner_to")
    @JsonProperty("partner_to")
    private String partnerTo;

    /**
     * 属性 [MODEL]
     *
     */
    @JSONField(name = "model")
    @JsonProperty("model")
    private String model;

    /**
     * 属性 [EMAIL_FROM]
     *
     */
    @JSONField(name = "email_from")
    @JsonProperty("email_from")
    private String emailFrom;

    /**
     * 属性 [AUTO_DELETE]
     *
     */
    @JSONField(name = "auto_delete")
    @JsonProperty("auto_delete")
    private String autoDelete;

    /**
     * 属性 [EMAIL_CC]
     *
     */
    @JSONField(name = "email_cc")
    @JsonProperty("email_cc")
    private String emailCc;

    /**
     * 属性 [REPORT_NAME]
     *
     */
    @JSONField(name = "report_name")
    @JsonProperty("report_name")
    private String reportName;

    /**
     * 属性 [CREATE_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;

    /**
     * 属性 [MODEL_OBJECT_FIELD]
     *
     */
    @JSONField(name = "model_object_field")
    @JsonProperty("model_object_field")
    private Integer modelObjectField;

    /**
     * 属性 [EMAIL_TO]
     *
     */
    @JSONField(name = "email_to")
    @JsonProperty("email_to")
    private String emailTo;

    /**
     * 属性 [NULL_VALUE]
     *
     */
    @JSONField(name = "null_value")
    @JsonProperty("null_value")
    private String nullValue;

    /**
     * 属性 [COPYVALUE]
     *
     */
    @JSONField(name = "copyvalue")
    @JsonProperty("copyvalue")
    private String copyvalue;

    /**
     * 属性 [USER_SIGNATURE]
     *
     */
    @JSONField(name = "user_signature")
    @JsonProperty("user_signature")
    private String userSignature;

    /**
     * 属性 [SUB_MODEL_OBJECT_FIELD]
     *
     */
    @JSONField(name = "sub_model_object_field")
    @JsonProperty("sub_model_object_field")
    private Integer subModelObjectField;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    private String displayName;

    /**
     * 属性 [LANG]
     *
     */
    @JSONField(name = "lang")
    @JsonProperty("lang")
    private String lang;

    /**
     * 属性 [CREATE_UID_TEXT]
     *
     */
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    private String createUidText;

    /**
     * 属性 [WRITE_UID_TEXT]
     *
     */
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    private String writeUidText;

    /**
     * 属性 [CREATE_UID]
     *
     */
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    private Integer createUid;

    /**
     * 属性 [WRITE_UID]
     *
     */
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    private Integer writeUid;


    /**
     * 设置 [USE_DEFAULT_TO]
     */
    public void setUseDefaultTo(String  useDefaultTo){
        this.useDefaultTo = useDefaultTo ;
        this.modify("use_default_to",useDefaultTo);
    }

    /**
     * 设置 [SUBJECT]
     */
    public void setSubject(String  subject){
        this.subject = subject ;
        this.modify("subject",subject);
    }

    /**
     * 设置 [REPLY_TO]
     */
    public void setReplyTo(String  replyTo){
        this.replyTo = replyTo ;
        this.modify("reply_to",replyTo);
    }

    /**
     * 设置 [SUB_OBJECT]
     */
    public void setSubObject(Integer  subObject){
        this.subObject = subObject ;
        this.modify("sub_object",subObject);
    }

    /**
     * 设置 [SCHEDULED_DATE]
     */
    public void setScheduledDate(String  scheduledDate){
        this.scheduledDate = scheduledDate ;
        this.modify("scheduled_date",scheduledDate);
    }

    /**
     * 设置 [MODEL_ID]
     */
    public void setModelId(Integer  modelId){
        this.modelId = modelId ;
        this.modify("model_id",modelId);
    }

    /**
     * 设置 [MAIL_SERVER_ID]
     */
    public void setMailServerId(Integer  mailServerId){
        this.mailServerId = mailServerId ;
        this.modify("mail_server_id",mailServerId);
    }

    /**
     * 设置 [REPORT_TEMPLATE]
     */
    public void setReportTemplate(Integer  reportTemplate){
        this.reportTemplate = reportTemplate ;
        this.modify("report_template",reportTemplate);
    }

    /**
     * 设置 [BODY_HTML]
     */
    public void setBodyHtml(String  bodyHtml){
        this.bodyHtml = bodyHtml ;
        this.modify("body_html",bodyHtml);
    }

    /**
     * 设置 [REF_IR_ACT_WINDOW]
     */
    public void setRefIrActWindow(Integer  refIrActWindow){
        this.refIrActWindow = refIrActWindow ;
        this.modify("ref_ir_act_window",refIrActWindow);
    }

    /**
     * 设置 [NAME]
     */
    public void setName(String  name){
        this.name = name ;
        this.modify("name",name);
    }

    /**
     * 设置 [PARTNER_TO]
     */
    public void setPartnerTo(String  partnerTo){
        this.partnerTo = partnerTo ;
        this.modify("partner_to",partnerTo);
    }

    /**
     * 设置 [MODEL]
     */
    public void setModel(String  model){
        this.model = model ;
        this.modify("model",model);
    }

    /**
     * 设置 [EMAIL_FROM]
     */
    public void setEmailFrom(String  emailFrom){
        this.emailFrom = emailFrom ;
        this.modify("email_from",emailFrom);
    }

    /**
     * 设置 [AUTO_DELETE]
     */
    public void setAutoDelete(String  autoDelete){
        this.autoDelete = autoDelete ;
        this.modify("auto_delete",autoDelete);
    }

    /**
     * 设置 [EMAIL_CC]
     */
    public void setEmailCc(String  emailCc){
        this.emailCc = emailCc ;
        this.modify("email_cc",emailCc);
    }

    /**
     * 设置 [REPORT_NAME]
     */
    public void setReportName(String  reportName){
        this.reportName = reportName ;
        this.modify("report_name",reportName);
    }

    /**
     * 设置 [MODEL_OBJECT_FIELD]
     */
    public void setModelObjectField(Integer  modelObjectField){
        this.modelObjectField = modelObjectField ;
        this.modify("model_object_field",modelObjectField);
    }

    /**
     * 设置 [EMAIL_TO]
     */
    public void setEmailTo(String  emailTo){
        this.emailTo = emailTo ;
        this.modify("email_to",emailTo);
    }

    /**
     * 设置 [NULL_VALUE]
     */
    public void setNullValue(String  nullValue){
        this.nullValue = nullValue ;
        this.modify("null_value",nullValue);
    }

    /**
     * 设置 [COPYVALUE]
     */
    public void setCopyvalue(String  copyvalue){
        this.copyvalue = copyvalue ;
        this.modify("copyvalue",copyvalue);
    }

    /**
     * 设置 [USER_SIGNATURE]
     */
    public void setUserSignature(String  userSignature){
        this.userSignature = userSignature ;
        this.modify("user_signature",userSignature);
    }

    /**
     * 设置 [SUB_MODEL_OBJECT_FIELD]
     */
    public void setSubModelObjectField(Integer  subModelObjectField){
        this.subModelObjectField = subModelObjectField ;
        this.modify("sub_model_object_field",subModelObjectField);
    }

    /**
     * 设置 [LANG]
     */
    public void setLang(String  lang){
        this.lang = lang ;
        this.modify("lang",lang);
    }


}

