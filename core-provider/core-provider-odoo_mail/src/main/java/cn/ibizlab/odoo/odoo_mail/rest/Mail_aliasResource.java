package cn.ibizlab.odoo.odoo_mail.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_mail.dto.*;
import cn.ibizlab.odoo.odoo_mail.mapping.*;
import cn.ibizlab.odoo.core.odoo_mail.domain.Mail_alias;
import cn.ibizlab.odoo.core.odoo_mail.service.IMail_aliasService;
import cn.ibizlab.odoo.core.odoo_mail.filter.Mail_aliasSearchContext;




@Slf4j
@Api(tags = {"Mail_alias" })
@RestController("odoo_mail-mail_alias")
@RequestMapping("")
public class Mail_aliasResource {

    @Autowired
    private IMail_aliasService mail_aliasService;

    @Autowired
    @Lazy
    private Mail_aliasMapping mail_aliasMapping;










    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Mail_alias" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/mail_aliases")

    public ResponseEntity<Mail_aliasDTO> create(@RequestBody Mail_aliasDTO mail_aliasdto) {
        Mail_alias domain = mail_aliasMapping.toDomain(mail_aliasdto);
		mail_aliasService.create(domain);
        Mail_aliasDTO dto = mail_aliasMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Mail_alias" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/mail_aliases/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Mail_aliasDTO> mail_aliasdtos) {
        mail_aliasService.createBatch(mail_aliasMapping.toDomain(mail_aliasdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission('Remove',{#mail_alias_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Mail_alias" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/mail_aliases/{mail_alias_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("mail_alias_id") Integer mail_alias_id) {
         return ResponseEntity.status(HttpStatus.OK).body(mail_aliasService.remove(mail_alias_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Mail_alias" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/mail_aliases/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        mail_aliasService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission(#mail_alias_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Mail_alias" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/mail_aliases/{mail_alias_id}")

    public ResponseEntity<Mail_aliasDTO> update(@PathVariable("mail_alias_id") Integer mail_alias_id, @RequestBody Mail_aliasDTO mail_aliasdto) {
		Mail_alias domain = mail_aliasMapping.toDomain(mail_aliasdto);
        domain.setId(mail_alias_id);
		mail_aliasService.update(domain);
		Mail_aliasDTO dto = mail_aliasMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#mail_alias_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Mail_alias" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/mail_aliases/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Mail_aliasDTO> mail_aliasdtos) {
        mail_aliasService.updateBatch(mail_aliasMapping.toDomain(mail_aliasdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission(#mail_alias_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Mail_alias" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/mail_aliases/{mail_alias_id}")
    public ResponseEntity<Mail_aliasDTO> get(@PathVariable("mail_alias_id") Integer mail_alias_id) {
        Mail_alias domain = mail_aliasService.get(mail_alias_id);
        Mail_aliasDTO dto = mail_aliasMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }




    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Mail_alias" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/mail_aliases/fetchdefault")
	public ResponseEntity<List<Mail_aliasDTO>> fetchDefault(Mail_aliasSearchContext context) {
        Page<Mail_alias> domains = mail_aliasService.searchDefault(context) ;
        List<Mail_aliasDTO> list = mail_aliasMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Mail_alias" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/mail_aliases/searchdefault")
	public ResponseEntity<Page<Mail_aliasDTO>> searchDefault(Mail_aliasSearchContext context) {
        Page<Mail_alias> domains = mail_aliasService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(mail_aliasMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Mail_alias getEntity(){
        return new Mail_alias();
    }

}
