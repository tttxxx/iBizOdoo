package cn.ibizlab.odoo.odoo_mail.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_mail.dto.*;
import cn.ibizlab.odoo.odoo_mail.mapping.*;
import cn.ibizlab.odoo.core.odoo_mail.domain.Mail_template;
import cn.ibizlab.odoo.core.odoo_mail.service.IMail_templateService;
import cn.ibizlab.odoo.core.odoo_mail.filter.Mail_templateSearchContext;




@Slf4j
@Api(tags = {"Mail_template" })
@RestController("odoo_mail-mail_template")
@RequestMapping("")
public class Mail_templateResource {

    @Autowired
    private IMail_templateService mail_templateService;

    @Autowired
    @Lazy
    private Mail_templateMapping mail_templateMapping;




    @PreAuthorize("hasPermission(#mail_template_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Mail_template" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/mail_templates/{mail_template_id}")
    public ResponseEntity<Mail_templateDTO> get(@PathVariable("mail_template_id") Integer mail_template_id) {
        Mail_template domain = mail_templateService.get(mail_template_id);
        Mail_templateDTO dto = mail_templateMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }










    @PreAuthorize("hasPermission(#mail_template_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Mail_template" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/mail_templates/{mail_template_id}")

    public ResponseEntity<Mail_templateDTO> update(@PathVariable("mail_template_id") Integer mail_template_id, @RequestBody Mail_templateDTO mail_templatedto) {
		Mail_template domain = mail_templateMapping.toDomain(mail_templatedto);
        domain.setId(mail_template_id);
		mail_templateService.update(domain);
		Mail_templateDTO dto = mail_templateMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#mail_template_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Mail_template" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/mail_templates/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Mail_templateDTO> mail_templatedtos) {
        mail_templateService.updateBatch(mail_templateMapping.toDomain(mail_templatedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Mail_template" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/mail_templates")

    public ResponseEntity<Mail_templateDTO> create(@RequestBody Mail_templateDTO mail_templatedto) {
        Mail_template domain = mail_templateMapping.toDomain(mail_templatedto);
		mail_templateService.create(domain);
        Mail_templateDTO dto = mail_templateMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Mail_template" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/mail_templates/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Mail_templateDTO> mail_templatedtos) {
        mail_templateService.createBatch(mail_templateMapping.toDomain(mail_templatedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission('Remove',{#mail_template_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Mail_template" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/mail_templates/{mail_template_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("mail_template_id") Integer mail_template_id) {
         return ResponseEntity.status(HttpStatus.OK).body(mail_templateService.remove(mail_template_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Mail_template" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/mail_templates/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        mail_templateService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Mail_template" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/mail_templates/fetchdefault")
	public ResponseEntity<List<Mail_templateDTO>> fetchDefault(Mail_templateSearchContext context) {
        Page<Mail_template> domains = mail_templateService.searchDefault(context) ;
        List<Mail_templateDTO> list = mail_templateMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Mail_template" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/mail_templates/searchdefault")
	public ResponseEntity<Page<Mail_templateDTO>> searchDefault(Mail_templateSearchContext context) {
        Page<Mail_template> domains = mail_templateService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(mail_templateMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Mail_template getEntity(){
        return new Mail_template();
    }

}
