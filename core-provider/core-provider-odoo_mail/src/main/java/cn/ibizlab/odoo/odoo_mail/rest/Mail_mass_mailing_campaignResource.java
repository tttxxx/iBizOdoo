package cn.ibizlab.odoo.odoo_mail.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_mail.dto.*;
import cn.ibizlab.odoo.odoo_mail.mapping.*;
import cn.ibizlab.odoo.core.odoo_mail.domain.Mail_mass_mailing_campaign;
import cn.ibizlab.odoo.core.odoo_mail.service.IMail_mass_mailing_campaignService;
import cn.ibizlab.odoo.core.odoo_mail.filter.Mail_mass_mailing_campaignSearchContext;




@Slf4j
@Api(tags = {"Mail_mass_mailing_campaign" })
@RestController("odoo_mail-mail_mass_mailing_campaign")
@RequestMapping("")
public class Mail_mass_mailing_campaignResource {

    @Autowired
    private IMail_mass_mailing_campaignService mail_mass_mailing_campaignService;

    @Autowired
    @Lazy
    private Mail_mass_mailing_campaignMapping mail_mass_mailing_campaignMapping;




    @PreAuthorize("hasPermission(#mail_mass_mailing_campaign_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Mail_mass_mailing_campaign" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/mail_mass_mailing_campaigns/{mail_mass_mailing_campaign_id}")
    public ResponseEntity<Mail_mass_mailing_campaignDTO> get(@PathVariable("mail_mass_mailing_campaign_id") Integer mail_mass_mailing_campaign_id) {
        Mail_mass_mailing_campaign domain = mail_mass_mailing_campaignService.get(mail_mass_mailing_campaign_id);
        Mail_mass_mailing_campaignDTO dto = mail_mass_mailing_campaignMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }













    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Mail_mass_mailing_campaign" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/mail_mass_mailing_campaigns")

    public ResponseEntity<Mail_mass_mailing_campaignDTO> create(@RequestBody Mail_mass_mailing_campaignDTO mail_mass_mailing_campaigndto) {
        Mail_mass_mailing_campaign domain = mail_mass_mailing_campaignMapping.toDomain(mail_mass_mailing_campaigndto);
		mail_mass_mailing_campaignService.create(domain);
        Mail_mass_mailing_campaignDTO dto = mail_mass_mailing_campaignMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Mail_mass_mailing_campaign" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/mail_mass_mailing_campaigns/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Mail_mass_mailing_campaignDTO> mail_mass_mailing_campaigndtos) {
        mail_mass_mailing_campaignService.createBatch(mail_mass_mailing_campaignMapping.toDomain(mail_mass_mailing_campaigndtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission(#mail_mass_mailing_campaign_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Mail_mass_mailing_campaign" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/mail_mass_mailing_campaigns/{mail_mass_mailing_campaign_id}")

    public ResponseEntity<Mail_mass_mailing_campaignDTO> update(@PathVariable("mail_mass_mailing_campaign_id") Integer mail_mass_mailing_campaign_id, @RequestBody Mail_mass_mailing_campaignDTO mail_mass_mailing_campaigndto) {
		Mail_mass_mailing_campaign domain = mail_mass_mailing_campaignMapping.toDomain(mail_mass_mailing_campaigndto);
        domain.setId(mail_mass_mailing_campaign_id);
		mail_mass_mailing_campaignService.update(domain);
		Mail_mass_mailing_campaignDTO dto = mail_mass_mailing_campaignMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#mail_mass_mailing_campaign_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Mail_mass_mailing_campaign" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/mail_mass_mailing_campaigns/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Mail_mass_mailing_campaignDTO> mail_mass_mailing_campaigndtos) {
        mail_mass_mailing_campaignService.updateBatch(mail_mass_mailing_campaignMapping.toDomain(mail_mass_mailing_campaigndtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission('Remove',{#mail_mass_mailing_campaign_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Mail_mass_mailing_campaign" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/mail_mass_mailing_campaigns/{mail_mass_mailing_campaign_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("mail_mass_mailing_campaign_id") Integer mail_mass_mailing_campaign_id) {
         return ResponseEntity.status(HttpStatus.OK).body(mail_mass_mailing_campaignService.remove(mail_mass_mailing_campaign_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Mail_mass_mailing_campaign" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/mail_mass_mailing_campaigns/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        mail_mass_mailing_campaignService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Mail_mass_mailing_campaign" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/mail_mass_mailing_campaigns/fetchdefault")
	public ResponseEntity<List<Mail_mass_mailing_campaignDTO>> fetchDefault(Mail_mass_mailing_campaignSearchContext context) {
        Page<Mail_mass_mailing_campaign> domains = mail_mass_mailing_campaignService.searchDefault(context) ;
        List<Mail_mass_mailing_campaignDTO> list = mail_mass_mailing_campaignMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Mail_mass_mailing_campaign" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/mail_mass_mailing_campaigns/searchdefault")
	public ResponseEntity<Page<Mail_mass_mailing_campaignDTO>> searchDefault(Mail_mass_mailing_campaignSearchContext context) {
        Page<Mail_mass_mailing_campaign> domains = mail_mass_mailing_campaignService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(mail_mass_mailing_campaignMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Mail_mass_mailing_campaign getEntity(){
        return new Mail_mass_mailing_campaign();
    }

}
