package cn.ibizlab.odoo.odoo_mail.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_mail.dto.*;
import cn.ibizlab.odoo.odoo_mail.mapping.*;
import cn.ibizlab.odoo.core.odoo_mail.domain.Mail_channel;
import cn.ibizlab.odoo.core.odoo_mail.service.IMail_channelService;
import cn.ibizlab.odoo.core.odoo_mail.filter.Mail_channelSearchContext;




@Slf4j
@Api(tags = {"Mail_channel" })
@RestController("odoo_mail-mail_channel")
@RequestMapping("")
public class Mail_channelResource {

    @Autowired
    private IMail_channelService mail_channelService;

    @Autowired
    @Lazy
    private Mail_channelMapping mail_channelMapping;







    @PreAuthorize("hasPermission(#mail_channel_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Mail_channel" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/mail_channels/{mail_channel_id}")

    public ResponseEntity<Mail_channelDTO> update(@PathVariable("mail_channel_id") Integer mail_channel_id, @RequestBody Mail_channelDTO mail_channeldto) {
		Mail_channel domain = mail_channelMapping.toDomain(mail_channeldto);
        domain.setId(mail_channel_id);
		mail_channelService.update(domain);
		Mail_channelDTO dto = mail_channelMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#mail_channel_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Mail_channel" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/mail_channels/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Mail_channelDTO> mail_channeldtos) {
        mail_channelService.updateBatch(mail_channelMapping.toDomain(mail_channeldtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission('Remove',{#mail_channel_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Mail_channel" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/mail_channels/{mail_channel_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("mail_channel_id") Integer mail_channel_id) {
         return ResponseEntity.status(HttpStatus.OK).body(mail_channelService.remove(mail_channel_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Mail_channel" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/mail_channels/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        mail_channelService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Mail_channel" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/mail_channels")

    public ResponseEntity<Mail_channelDTO> create(@RequestBody Mail_channelDTO mail_channeldto) {
        Mail_channel domain = mail_channelMapping.toDomain(mail_channeldto);
		mail_channelService.create(domain);
        Mail_channelDTO dto = mail_channelMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Mail_channel" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/mail_channels/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Mail_channelDTO> mail_channeldtos) {
        mail_channelService.createBatch(mail_channelMapping.toDomain(mail_channeldtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }







    @PreAuthorize("hasPermission(#mail_channel_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Mail_channel" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/mail_channels/{mail_channel_id}")
    public ResponseEntity<Mail_channelDTO> get(@PathVariable("mail_channel_id") Integer mail_channel_id) {
        Mail_channel domain = mail_channelService.get(mail_channel_id);
        Mail_channelDTO dto = mail_channelMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }




    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Mail_channel" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/mail_channels/fetchdefault")
	public ResponseEntity<List<Mail_channelDTO>> fetchDefault(Mail_channelSearchContext context) {
        Page<Mail_channel> domains = mail_channelService.searchDefault(context) ;
        List<Mail_channelDTO> list = mail_channelMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Mail_channel" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/mail_channels/searchdefault")
	public ResponseEntity<Page<Mail_channelDTO>> searchDefault(Mail_channelSearchContext context) {
        Page<Mail_channel> domains = mail_channelService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(mail_channelMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Mail_channel getEntity(){
        return new Mail_channel();
    }

}
