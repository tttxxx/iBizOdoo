package cn.ibizlab.odoo.odoo_mail.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_mail.dto.*;
import cn.ibizlab.odoo.odoo_mail.mapping.*;
import cn.ibizlab.odoo.core.odoo_mail.domain.Mail_moderation;
import cn.ibizlab.odoo.core.odoo_mail.service.IMail_moderationService;
import cn.ibizlab.odoo.core.odoo_mail.filter.Mail_moderationSearchContext;




@Slf4j
@Api(tags = {"Mail_moderation" })
@RestController("odoo_mail-mail_moderation")
@RequestMapping("")
public class Mail_moderationResource {

    @Autowired
    private IMail_moderationService mail_moderationService;

    @Autowired
    @Lazy
    private Mail_moderationMapping mail_moderationMapping;




    @PreAuthorize("hasPermission('Remove',{#mail_moderation_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Mail_moderation" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/mail_moderations/{mail_moderation_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("mail_moderation_id") Integer mail_moderation_id) {
         return ResponseEntity.status(HttpStatus.OK).body(mail_moderationService.remove(mail_moderation_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Mail_moderation" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/mail_moderations/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        mail_moderationService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission(#mail_moderation_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Mail_moderation" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/mail_moderations/{mail_moderation_id}")

    public ResponseEntity<Mail_moderationDTO> update(@PathVariable("mail_moderation_id") Integer mail_moderation_id, @RequestBody Mail_moderationDTO mail_moderationdto) {
		Mail_moderation domain = mail_moderationMapping.toDomain(mail_moderationdto);
        domain.setId(mail_moderation_id);
		mail_moderationService.update(domain);
		Mail_moderationDTO dto = mail_moderationMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#mail_moderation_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Mail_moderation" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/mail_moderations/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Mail_moderationDTO> mail_moderationdtos) {
        mail_moderationService.updateBatch(mail_moderationMapping.toDomain(mail_moderationdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }







    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Mail_moderation" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/mail_moderations")

    public ResponseEntity<Mail_moderationDTO> create(@RequestBody Mail_moderationDTO mail_moderationdto) {
        Mail_moderation domain = mail_moderationMapping.toDomain(mail_moderationdto);
		mail_moderationService.create(domain);
        Mail_moderationDTO dto = mail_moderationMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Mail_moderation" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/mail_moderations/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Mail_moderationDTO> mail_moderationdtos) {
        mail_moderationService.createBatch(mail_moderationMapping.toDomain(mail_moderationdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }










    @PreAuthorize("hasPermission(#mail_moderation_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Mail_moderation" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/mail_moderations/{mail_moderation_id}")
    public ResponseEntity<Mail_moderationDTO> get(@PathVariable("mail_moderation_id") Integer mail_moderation_id) {
        Mail_moderation domain = mail_moderationService.get(mail_moderation_id);
        Mail_moderationDTO dto = mail_moderationMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Mail_moderation" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/mail_moderations/fetchdefault")
	public ResponseEntity<List<Mail_moderationDTO>> fetchDefault(Mail_moderationSearchContext context) {
        Page<Mail_moderation> domains = mail_moderationService.searchDefault(context) ;
        List<Mail_moderationDTO> list = mail_moderationMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Mail_moderation" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/mail_moderations/searchdefault")
	public ResponseEntity<Page<Mail_moderationDTO>> searchDefault(Mail_moderationSearchContext context) {
        Page<Mail_moderation> domains = mail_moderationService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(mail_moderationMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Mail_moderation getEntity(){
        return new Mail_moderation();
    }

}
