package cn.ibizlab.odoo.odoo_mail.config;

import org.springframework.context.annotation.ComponentScan;

@ComponentScan("cn.ibizlab.odoo.odoo_mail")
public class odoo_mailRestConfiguration {

}
