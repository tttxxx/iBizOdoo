package cn.ibizlab.odoo.odoo_mail.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_mail.dto.*;
import cn.ibizlab.odoo.odoo_mail.mapping.*;
import cn.ibizlab.odoo.core.odoo_mail.domain.Mail_mass_mailing_tag;
import cn.ibizlab.odoo.core.odoo_mail.service.IMail_mass_mailing_tagService;
import cn.ibizlab.odoo.core.odoo_mail.filter.Mail_mass_mailing_tagSearchContext;




@Slf4j
@Api(tags = {"Mail_mass_mailing_tag" })
@RestController("odoo_mail-mail_mass_mailing_tag")
@RequestMapping("")
public class Mail_mass_mailing_tagResource {

    @Autowired
    private IMail_mass_mailing_tagService mail_mass_mailing_tagService;

    @Autowired
    @Lazy
    private Mail_mass_mailing_tagMapping mail_mass_mailing_tagMapping;




    @PreAuthorize("hasPermission(#mail_mass_mailing_tag_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Mail_mass_mailing_tag" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/mail_mass_mailing_tags/{mail_mass_mailing_tag_id}")

    public ResponseEntity<Mail_mass_mailing_tagDTO> update(@PathVariable("mail_mass_mailing_tag_id") Integer mail_mass_mailing_tag_id, @RequestBody Mail_mass_mailing_tagDTO mail_mass_mailing_tagdto) {
		Mail_mass_mailing_tag domain = mail_mass_mailing_tagMapping.toDomain(mail_mass_mailing_tagdto);
        domain.setId(mail_mass_mailing_tag_id);
		mail_mass_mailing_tagService.update(domain);
		Mail_mass_mailing_tagDTO dto = mail_mass_mailing_tagMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#mail_mass_mailing_tag_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Mail_mass_mailing_tag" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/mail_mass_mailing_tags/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Mail_mass_mailing_tagDTO> mail_mass_mailing_tagdtos) {
        mail_mass_mailing_tagService.updateBatch(mail_mass_mailing_tagMapping.toDomain(mail_mass_mailing_tagdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }










    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Mail_mass_mailing_tag" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/mail_mass_mailing_tags")

    public ResponseEntity<Mail_mass_mailing_tagDTO> create(@RequestBody Mail_mass_mailing_tagDTO mail_mass_mailing_tagdto) {
        Mail_mass_mailing_tag domain = mail_mass_mailing_tagMapping.toDomain(mail_mass_mailing_tagdto);
		mail_mass_mailing_tagService.create(domain);
        Mail_mass_mailing_tagDTO dto = mail_mass_mailing_tagMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Mail_mass_mailing_tag" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/mail_mass_mailing_tags/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Mail_mass_mailing_tagDTO> mail_mass_mailing_tagdtos) {
        mail_mass_mailing_tagService.createBatch(mail_mass_mailing_tagMapping.toDomain(mail_mass_mailing_tagdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }







    @PreAuthorize("hasPermission('Remove',{#mail_mass_mailing_tag_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Mail_mass_mailing_tag" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/mail_mass_mailing_tags/{mail_mass_mailing_tag_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("mail_mass_mailing_tag_id") Integer mail_mass_mailing_tag_id) {
         return ResponseEntity.status(HttpStatus.OK).body(mail_mass_mailing_tagService.remove(mail_mass_mailing_tag_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Mail_mass_mailing_tag" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/mail_mass_mailing_tags/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        mail_mass_mailing_tagService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission(#mail_mass_mailing_tag_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Mail_mass_mailing_tag" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/mail_mass_mailing_tags/{mail_mass_mailing_tag_id}")
    public ResponseEntity<Mail_mass_mailing_tagDTO> get(@PathVariable("mail_mass_mailing_tag_id") Integer mail_mass_mailing_tag_id) {
        Mail_mass_mailing_tag domain = mail_mass_mailing_tagService.get(mail_mass_mailing_tag_id);
        Mail_mass_mailing_tagDTO dto = mail_mass_mailing_tagMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Mail_mass_mailing_tag" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/mail_mass_mailing_tags/fetchdefault")
	public ResponseEntity<List<Mail_mass_mailing_tagDTO>> fetchDefault(Mail_mass_mailing_tagSearchContext context) {
        Page<Mail_mass_mailing_tag> domains = mail_mass_mailing_tagService.searchDefault(context) ;
        List<Mail_mass_mailing_tagDTO> list = mail_mass_mailing_tagMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Mail_mass_mailing_tag" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/mail_mass_mailing_tags/searchdefault")
	public ResponseEntity<Page<Mail_mass_mailing_tagDTO>> searchDefault(Mail_mass_mailing_tagSearchContext context) {
        Page<Mail_mass_mailing_tag> domains = mail_mass_mailing_tagService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(mail_mass_mailing_tagMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Mail_mass_mailing_tag getEntity(){
        return new Mail_mass_mailing_tag();
    }

}
