package cn.ibizlab.odoo.odoo_mail.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;
import cn.ibizlab.odoo.util.domain.DTOBase;
import lombok.Data;

/**
 * 服务DTO对象[Mail_notificationDTO]
 */
@Data
public class Mail_notificationDTO extends DTOBase implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;

    /**
     * 属性 [ID]
     *
     */
    @JSONField(name = "id")
    @JsonProperty("id")
    private Integer id;

    /**
     * 属性 [FAILURE_REASON]
     *
     */
    @JSONField(name = "failure_reason")
    @JsonProperty("failure_reason")
    private String failureReason;

    /**
     * 属性 [IS_EMAIL]
     *
     */
    @JSONField(name = "is_email")
    @JsonProperty("is_email")
    private String isEmail;

    /**
     * 属性 [IS_READ]
     *
     */
    @JSONField(name = "is_read")
    @JsonProperty("is_read")
    private String isRead;

    /**
     * 属性 [EMAIL_STATUS]
     *
     */
    @JSONField(name = "email_status")
    @JsonProperty("email_status")
    private String emailStatus;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    private String displayName;

    /**
     * 属性 [FAILURE_TYPE]
     *
     */
    @JSONField(name = "failure_type")
    @JsonProperty("failure_type")
    private String failureType;

    /**
     * 属性 [RES_PARTNER_ID_TEXT]
     *
     */
    @JSONField(name = "res_partner_id_text")
    @JsonProperty("res_partner_id_text")
    private String resPartnerIdText;

    /**
     * 属性 [MAIL_ID]
     *
     */
    @JSONField(name = "mail_id")
    @JsonProperty("mail_id")
    private Integer mailId;

    /**
     * 属性 [MAIL_MESSAGE_ID]
     *
     */
    @JSONField(name = "mail_message_id")
    @JsonProperty("mail_message_id")
    private Integer mailMessageId;

    /**
     * 属性 [RES_PARTNER_ID]
     *
     */
    @JSONField(name = "res_partner_id")
    @JsonProperty("res_partner_id")
    private Integer resPartnerId;


    /**
     * 设置 [FAILURE_REASON]
     */
    public void setFailureReason(String  failureReason){
        this.failureReason = failureReason ;
        this.modify("failure_reason",failureReason);
    }

    /**
     * 设置 [IS_EMAIL]
     */
    public void setIsEmail(String  isEmail){
        this.isEmail = isEmail ;
        this.modify("is_email",isEmail);
    }

    /**
     * 设置 [IS_READ]
     */
    public void setIsRead(String  isRead){
        this.isRead = isRead ;
        this.modify("is_read",isRead);
    }

    /**
     * 设置 [EMAIL_STATUS]
     */
    public void setEmailStatus(String  emailStatus){
        this.emailStatus = emailStatus ;
        this.modify("email_status",emailStatus);
    }

    /**
     * 设置 [FAILURE_TYPE]
     */
    public void setFailureType(String  failureType){
        this.failureType = failureType ;
        this.modify("failure_type",failureType);
    }

    /**
     * 设置 [MAIL_ID]
     */
    public void setMailId(Integer  mailId){
        this.mailId = mailId ;
        this.modify("mail_id",mailId);
    }

    /**
     * 设置 [MAIL_MESSAGE_ID]
     */
    public void setMailMessageId(Integer  mailMessageId){
        this.mailMessageId = mailMessageId ;
        this.modify("mail_message_id",mailMessageId);
    }

    /**
     * 设置 [RES_PARTNER_ID]
     */
    public void setResPartnerId(Integer  resPartnerId){
        this.resPartnerId = resPartnerId ;
        this.modify("res_partner_id",resPartnerId);
    }


}

