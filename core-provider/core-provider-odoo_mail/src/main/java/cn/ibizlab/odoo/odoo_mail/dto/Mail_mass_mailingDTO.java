package cn.ibizlab.odoo.odoo_mail.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;
import cn.ibizlab.odoo.util.domain.DTOBase;
import lombok.Data;

/**
 * 服务DTO对象[Mail_mass_mailingDTO]
 */
@Data
public class Mail_mass_mailingDTO extends DTOBase implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [SENT_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "sent_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("sent_date")
    private Timestamp sentDate;

    /**
     * 属性 [SALE_QUOTATION_COUNT]
     *
     */
    @JSONField(name = "sale_quotation_count")
    @JsonProperty("sale_quotation_count")
    private Integer saleQuotationCount;

    /**
     * 属性 [CREATE_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;

    /**
     * 属性 [MAILING_DOMAIN]
     *
     */
    @JSONField(name = "mailing_domain")
    @JsonProperty("mailing_domain")
    private String mailingDomain;

    /**
     * 属性 [OPENED]
     *
     */
    @JSONField(name = "opened")
    @JsonProperty("opened")
    private Integer opened;

    /**
     * 属性 [REPLY_TO]
     *
     */
    @JSONField(name = "reply_to")
    @JsonProperty("reply_to")
    private String replyTo;

    /**
     * 属性 [REPLIED]
     *
     */
    @JSONField(name = "replied")
    @JsonProperty("replied")
    private Integer replied;

    /**
     * 属性 [CRM_LEAD_COUNT]
     *
     */
    @JSONField(name = "crm_lead_count")
    @JsonProperty("crm_lead_count")
    private Integer crmLeadCount;

    /**
     * 属性 [MAILING_MODEL_REAL]
     *
     */
    @JSONField(name = "mailing_model_real")
    @JsonProperty("mailing_model_real")
    private String mailingModelReal;

    /**
     * 属性 [CLICKS_RATIO]
     *
     */
    @JSONField(name = "clicks_ratio")
    @JsonProperty("clicks_ratio")
    private Integer clicksRatio;

    /**
     * 属性 [DELIVERED]
     *
     */
    @JSONField(name = "delivered")
    @JsonProperty("delivered")
    private Integer delivered;

    /**
     * 属性 [MAILING_MODEL_ID]
     *
     */
    @JSONField(name = "mailing_model_id")
    @JsonProperty("mailing_model_id")
    private Integer mailingModelId;

    /**
     * 属性 [REPLY_TO_MODE]
     *
     */
    @JSONField(name = "reply_to_mode")
    @JsonProperty("reply_to_mode")
    private String replyToMode;

    /**
     * 属性 [SCHEDULE_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "schedule_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("schedule_date")
    private Timestamp scheduleDate;

    /**
     * 属性 [EMAIL_FROM]
     *
     */
    @JSONField(name = "email_from")
    @JsonProperty("email_from")
    private String emailFrom;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    private String displayName;

    /**
     * 属性 [SALE_INVOICED_AMOUNT]
     *
     */
    @JSONField(name = "sale_invoiced_amount")
    @JsonProperty("sale_invoiced_amount")
    private Integer saleInvoicedAmount;

    /**
     * 属性 [ATTACHMENT_IDS]
     *
     */
    @JSONField(name = "attachment_ids")
    @JsonProperty("attachment_ids")
    private String attachmentIds;

    /**
     * 属性 [TOTAL]
     *
     */
    @JSONField(name = "total")
    @JsonProperty("total")
    private Integer total;

    /**
     * 属性 [ACTIVE]
     *
     */
    @JSONField(name = "active")
    @JsonProperty("active")
    private String active;

    /**
     * 属性 [BOUNCED_RATIO]
     *
     */
    @JSONField(name = "bounced_ratio")
    @JsonProperty("bounced_ratio")
    private Integer bouncedRatio;

    /**
     * 属性 [NEXT_DEPARTURE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "next_departure" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("next_departure")
    private Timestamp nextDeparture;

    /**
     * 属性 [MAIL_SERVER_ID]
     *
     */
    @JSONField(name = "mail_server_id")
    @JsonProperty("mail_server_id")
    private Integer mailServerId;

    /**
     * 属性 [IGNORED]
     *
     */
    @JSONField(name = "ignored")
    @JsonProperty("ignored")
    private Integer ignored;

    /**
     * 属性 [SCHEDULED]
     *
     */
    @JSONField(name = "scheduled")
    @JsonProperty("scheduled")
    private Integer scheduled;

    /**
     * 属性 [CRM_LEAD_ACTIVATED]
     *
     */
    @JSONField(name = "crm_lead_activated")
    @JsonProperty("crm_lead_activated")
    private String crmLeadActivated;

    /**
     * 属性 [CLICKED]
     *
     */
    @JSONField(name = "clicked")
    @JsonProperty("clicked")
    private Integer clicked;

    /**
     * 属性 [REPLIED_RATIO]
     *
     */
    @JSONField(name = "replied_ratio")
    @JsonProperty("replied_ratio")
    private Integer repliedRatio;

    /**
     * 属性 [WRITE_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;

    /**
     * 属性 [STATE]
     *
     */
    @JSONField(name = "state")
    @JsonProperty("state")
    private String state;

    /**
     * 属性 [STATISTICS_IDS]
     *
     */
    @JSONField(name = "statistics_ids")
    @JsonProperty("statistics_ids")
    private String statisticsIds;

    /**
     * 属性 [OPENED_RATIO]
     *
     */
    @JSONField(name = "opened_ratio")
    @JsonProperty("opened_ratio")
    private Integer openedRatio;

    /**
     * 属性 [BODY_HTML]
     *
     */
    @JSONField(name = "body_html")
    @JsonProperty("body_html")
    private String bodyHtml;

    /**
     * 属性 [BOUNCED]
     *
     */
    @JSONField(name = "bounced")
    @JsonProperty("bounced")
    private Integer bounced;

    /**
     * 属性 [COLOR]
     *
     */
    @JSONField(name = "color")
    @JsonProperty("color")
    private Integer color;

    /**
     * 属性 [MAILING_MODEL_NAME]
     *
     */
    @JSONField(name = "mailing_model_name")
    @JsonProperty("mailing_model_name")
    private String mailingModelName;

    /**
     * 属性 [CONTACT_LIST_IDS]
     *
     */
    @JSONField(name = "contact_list_ids")
    @JsonProperty("contact_list_ids")
    private String contactListIds;

    /**
     * 属性 [EXPECTED]
     *
     */
    @JSONField(name = "expected")
    @JsonProperty("expected")
    private Integer expected;

    /**
     * 属性 [FAILED]
     *
     */
    @JSONField(name = "failed")
    @JsonProperty("failed")
    private Integer failed;

    /**
     * 属性 [ID]
     *
     */
    @JSONField(name = "id")
    @JsonProperty("id")
    private Integer id;

    /**
     * 属性 [RECEIVED_RATIO]
     *
     */
    @JSONField(name = "received_ratio")
    @JsonProperty("received_ratio")
    private Integer receivedRatio;

    /**
     * 属性 [CRM_OPPORTUNITIES_COUNT]
     *
     */
    @JSONField(name = "crm_opportunities_count")
    @JsonProperty("crm_opportunities_count")
    private Integer crmOpportunitiesCount;

    /**
     * 属性 [KEEP_ARCHIVES]
     *
     */
    @JSONField(name = "keep_archives")
    @JsonProperty("keep_archives")
    private String keepArchives;

    /**
     * 属性 [CONTACT_AB_PC]
     *
     */
    @JSONField(name = "contact_ab_pc")
    @JsonProperty("contact_ab_pc")
    private Integer contactAbPc;

    /**
     * 属性 [SENT]
     *
     */
    @JSONField(name = "sent")
    @JsonProperty("sent")
    private Integer sent;

    /**
     * 属性 [CAMPAIGN_ID_TEXT]
     *
     */
    @JSONField(name = "campaign_id_text")
    @JsonProperty("campaign_id_text")
    private String campaignIdText;

    /**
     * 属性 [USER_ID_TEXT]
     *
     */
    @JSONField(name = "user_id_text")
    @JsonProperty("user_id_text")
    private String userIdText;

    /**
     * 属性 [NAME]
     *
     */
    @JSONField(name = "name")
    @JsonProperty("name")
    private String name;

    /**
     * 属性 [WRITE_UID_TEXT]
     *
     */
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    private String writeUidText;

    /**
     * 属性 [MASS_MAILING_CAMPAIGN_ID_TEXT]
     *
     */
    @JSONField(name = "mass_mailing_campaign_id_text")
    @JsonProperty("mass_mailing_campaign_id_text")
    private String massMailingCampaignIdText;

    /**
     * 属性 [MEDIUM_ID_TEXT]
     *
     */
    @JSONField(name = "medium_id_text")
    @JsonProperty("medium_id_text")
    private String mediumIdText;

    /**
     * 属性 [CREATE_UID_TEXT]
     *
     */
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    private String createUidText;

    /**
     * 属性 [CREATE_UID]
     *
     */
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    private Integer createUid;

    /**
     * 属性 [USER_ID]
     *
     */
    @JSONField(name = "user_id")
    @JsonProperty("user_id")
    private Integer userId;

    /**
     * 属性 [MASS_MAILING_CAMPAIGN_ID]
     *
     */
    @JSONField(name = "mass_mailing_campaign_id")
    @JsonProperty("mass_mailing_campaign_id")
    private Integer massMailingCampaignId;

    /**
     * 属性 [CAMPAIGN_ID]
     *
     */
    @JSONField(name = "campaign_id")
    @JsonProperty("campaign_id")
    private Integer campaignId;

    /**
     * 属性 [WRITE_UID]
     *
     */
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    private Integer writeUid;

    /**
     * 属性 [MEDIUM_ID]
     *
     */
    @JSONField(name = "medium_id")
    @JsonProperty("medium_id")
    private Integer mediumId;

    /**
     * 属性 [SOURCE_ID]
     *
     */
    @JSONField(name = "source_id")
    @JsonProperty("source_id")
    private Integer sourceId;


    /**
     * 设置 [SENT_DATE]
     */
    public void setSentDate(Timestamp  sentDate){
        this.sentDate = sentDate ;
        this.modify("sent_date",sentDate);
    }

    /**
     * 设置 [MAILING_DOMAIN]
     */
    public void setMailingDomain(String  mailingDomain){
        this.mailingDomain = mailingDomain ;
        this.modify("mailing_domain",mailingDomain);
    }

    /**
     * 设置 [REPLY_TO]
     */
    public void setReplyTo(String  replyTo){
        this.replyTo = replyTo ;
        this.modify("reply_to",replyTo);
    }

    /**
     * 设置 [MAILING_MODEL_ID]
     */
    public void setMailingModelId(Integer  mailingModelId){
        this.mailingModelId = mailingModelId ;
        this.modify("mailing_model_id",mailingModelId);
    }

    /**
     * 设置 [REPLY_TO_MODE]
     */
    public void setReplyToMode(String  replyToMode){
        this.replyToMode = replyToMode ;
        this.modify("reply_to_mode",replyToMode);
    }

    /**
     * 设置 [SCHEDULE_DATE]
     */
    public void setScheduleDate(Timestamp  scheduleDate){
        this.scheduleDate = scheduleDate ;
        this.modify("schedule_date",scheduleDate);
    }

    /**
     * 设置 [EMAIL_FROM]
     */
    public void setEmailFrom(String  emailFrom){
        this.emailFrom = emailFrom ;
        this.modify("email_from",emailFrom);
    }

    /**
     * 设置 [ACTIVE]
     */
    public void setActive(String  active){
        this.active = active ;
        this.modify("active",active);
    }

    /**
     * 设置 [MAIL_SERVER_ID]
     */
    public void setMailServerId(Integer  mailServerId){
        this.mailServerId = mailServerId ;
        this.modify("mail_server_id",mailServerId);
    }

    /**
     * 设置 [STATE]
     */
    public void setState(String  state){
        this.state = state ;
        this.modify("state",state);
    }

    /**
     * 设置 [BODY_HTML]
     */
    public void setBodyHtml(String  bodyHtml){
        this.bodyHtml = bodyHtml ;
        this.modify("body_html",bodyHtml);
    }

    /**
     * 设置 [COLOR]
     */
    public void setColor(Integer  color){
        this.color = color ;
        this.modify("color",color);
    }

    /**
     * 设置 [KEEP_ARCHIVES]
     */
    public void setKeepArchives(String  keepArchives){
        this.keepArchives = keepArchives ;
        this.modify("keep_archives",keepArchives);
    }

    /**
     * 设置 [CONTACT_AB_PC]
     */
    public void setContactAbPc(Integer  contactAbPc){
        this.contactAbPc = contactAbPc ;
        this.modify("contact_ab_pc",contactAbPc);
    }

    /**
     * 设置 [USER_ID]
     */
    public void setUserId(Integer  userId){
        this.userId = userId ;
        this.modify("user_id",userId);
    }

    /**
     * 设置 [MASS_MAILING_CAMPAIGN_ID]
     */
    public void setMassMailingCampaignId(Integer  massMailingCampaignId){
        this.massMailingCampaignId = massMailingCampaignId ;
        this.modify("mass_mailing_campaign_id",massMailingCampaignId);
    }

    /**
     * 设置 [CAMPAIGN_ID]
     */
    public void setCampaignId(Integer  campaignId){
        this.campaignId = campaignId ;
        this.modify("campaign_id",campaignId);
    }

    /**
     * 设置 [MEDIUM_ID]
     */
    public void setMediumId(Integer  mediumId){
        this.mediumId = mediumId ;
        this.modify("medium_id",mediumId);
    }

    /**
     * 设置 [SOURCE_ID]
     */
    public void setSourceId(Integer  sourceId){
        this.sourceId = sourceId ;
        this.modify("source_id",sourceId);
    }


}

