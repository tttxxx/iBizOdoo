package cn.ibizlab.odoo.odoo_mail.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;
import cn.ibizlab.odoo.util.domain.DTOBase;
import lombok.Data;

/**
 * 服务DTO对象[Mail_mail_statisticsDTO]
 */
@Data
public class Mail_mail_statisticsDTO extends DTOBase implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [SCHEDULED]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "scheduled" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("scheduled")
    private Timestamp scheduled;

    /**
     * 属性 [CLICKED]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "clicked" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("clicked")
    private Timestamp clicked;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    private String displayName;

    /**
     * 属性 [EMAIL]
     *
     */
    @JSONField(name = "email")
    @JsonProperty("email")
    private String email;

    /**
     * 属性 [CREATE_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;

    /**
     * 属性 [STATE_UPDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "state_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("state_update")
    private Timestamp stateUpdate;

    /**
     * 属性 [REPLIED]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "replied" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("replied")
    private Timestamp replied;

    /**
     * 属性 [WRITE_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;

    /**
     * 属性 [MODEL]
     *
     */
    @JSONField(name = "model")
    @JsonProperty("model")
    private String model;

    /**
     * 属性 [MESSAGE_ID]
     *
     */
    @JSONField(name = "message_id")
    @JsonProperty("message_id")
    private String messageId;

    /**
     * 属性 [EXCEPTION]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "exception" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("exception")
    private Timestamp exception;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;

    /**
     * 属性 [BOUNCED]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "bounced" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("bounced")
    private Timestamp bounced;

    /**
     * 属性 [IGNORED]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "ignored" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("ignored")
    private Timestamp ignored;

    /**
     * 属性 [RES_ID]
     *
     */
    @JSONField(name = "res_id")
    @JsonProperty("res_id")
    private Integer resId;

    /**
     * 属性 [SENT]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "sent" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("sent")
    private Timestamp sent;

    /**
     * 属性 [MAIL_MAIL_ID_INT]
     *
     */
    @JSONField(name = "mail_mail_id_int")
    @JsonProperty("mail_mail_id_int")
    private Integer mailMailIdInt;

    /**
     * 属性 [LINKS_CLICK_IDS]
     *
     */
    @JSONField(name = "links_click_ids")
    @JsonProperty("links_click_ids")
    private String linksClickIds;

    /**
     * 属性 [ID]
     *
     */
    @JSONField(name = "id")
    @JsonProperty("id")
    private Integer id;

    /**
     * 属性 [STATE]
     *
     */
    @JSONField(name = "state")
    @JsonProperty("state")
    private String state;

    /**
     * 属性 [OPENED]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "opened" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("opened")
    private Timestamp opened;

    /**
     * 属性 [CREATE_UID_TEXT]
     *
     */
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    private String createUidText;

    /**
     * 属性 [WRITE_UID_TEXT]
     *
     */
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    private String writeUidText;

    /**
     * 属性 [MASS_MAILING_CAMPAIGN_ID_TEXT]
     *
     */
    @JSONField(name = "mass_mailing_campaign_id_text")
    @JsonProperty("mass_mailing_campaign_id_text")
    private String massMailingCampaignIdText;

    /**
     * 属性 [MASS_MAILING_ID_TEXT]
     *
     */
    @JSONField(name = "mass_mailing_id_text")
    @JsonProperty("mass_mailing_id_text")
    private String massMailingIdText;

    /**
     * 属性 [WRITE_UID]
     *
     */
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    private Integer writeUid;

    /**
     * 属性 [MASS_MAILING_CAMPAIGN_ID]
     *
     */
    @JSONField(name = "mass_mailing_campaign_id")
    @JsonProperty("mass_mailing_campaign_id")
    private Integer massMailingCampaignId;

    /**
     * 属性 [MAIL_MAIL_ID]
     *
     */
    @JSONField(name = "mail_mail_id")
    @JsonProperty("mail_mail_id")
    private Integer mailMailId;

    /**
     * 属性 [CREATE_UID]
     *
     */
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    private Integer createUid;

    /**
     * 属性 [MASS_MAILING_ID]
     *
     */
    @JSONField(name = "mass_mailing_id")
    @JsonProperty("mass_mailing_id")
    private Integer massMailingId;


    /**
     * 设置 [SCHEDULED]
     */
    public void setScheduled(Timestamp  scheduled){
        this.scheduled = scheduled ;
        this.modify("scheduled",scheduled);
    }

    /**
     * 设置 [CLICKED]
     */
    public void setClicked(Timestamp  clicked){
        this.clicked = clicked ;
        this.modify("clicked",clicked);
    }

    /**
     * 设置 [EMAIL]
     */
    public void setEmail(String  email){
        this.email = email ;
        this.modify("email",email);
    }

    /**
     * 设置 [STATE_UPDATE]
     */
    public void setStateUpdate(Timestamp  stateUpdate){
        this.stateUpdate = stateUpdate ;
        this.modify("state_update",stateUpdate);
    }

    /**
     * 设置 [REPLIED]
     */
    public void setReplied(Timestamp  replied){
        this.replied = replied ;
        this.modify("replied",replied);
    }

    /**
     * 设置 [MODEL]
     */
    public void setModel(String  model){
        this.model = model ;
        this.modify("model",model);
    }

    /**
     * 设置 [MESSAGE_ID]
     */
    public void setMessageId(String  messageId){
        this.messageId = messageId ;
        this.modify("message_id",messageId);
    }

    /**
     * 设置 [EXCEPTION]
     */
    public void setException(Timestamp  exception){
        this.exception = exception ;
        this.modify("exception",exception);
    }

    /**
     * 设置 [BOUNCED]
     */
    public void setBounced(Timestamp  bounced){
        this.bounced = bounced ;
        this.modify("bounced",bounced);
    }

    /**
     * 设置 [IGNORED]
     */
    public void setIgnored(Timestamp  ignored){
        this.ignored = ignored ;
        this.modify("ignored",ignored);
    }

    /**
     * 设置 [RES_ID]
     */
    public void setResId(Integer  resId){
        this.resId = resId ;
        this.modify("res_id",resId);
    }

    /**
     * 设置 [SENT]
     */
    public void setSent(Timestamp  sent){
        this.sent = sent ;
        this.modify("sent",sent);
    }

    /**
     * 设置 [MAIL_MAIL_ID_INT]
     */
    public void setMailMailIdInt(Integer  mailMailIdInt){
        this.mailMailIdInt = mailMailIdInt ;
        this.modify("mail_mail_id_int",mailMailIdInt);
    }

    /**
     * 设置 [STATE]
     */
    public void setState(String  state){
        this.state = state ;
        this.modify("state",state);
    }

    /**
     * 设置 [OPENED]
     */
    public void setOpened(Timestamp  opened){
        this.opened = opened ;
        this.modify("opened",opened);
    }

    /**
     * 设置 [MASS_MAILING_CAMPAIGN_ID]
     */
    public void setMassMailingCampaignId(Integer  massMailingCampaignId){
        this.massMailingCampaignId = massMailingCampaignId ;
        this.modify("mass_mailing_campaign_id",massMailingCampaignId);
    }

    /**
     * 设置 [MAIL_MAIL_ID]
     */
    public void setMailMailId(Integer  mailMailId){
        this.mailMailId = mailMailId ;
        this.modify("mail_mail_id",mailMailId);
    }

    /**
     * 设置 [MASS_MAILING_ID]
     */
    public void setMassMailingId(Integer  massMailingId){
        this.massMailingId = massMailingId ;
        this.modify("mass_mailing_id",massMailingId);
    }


}

