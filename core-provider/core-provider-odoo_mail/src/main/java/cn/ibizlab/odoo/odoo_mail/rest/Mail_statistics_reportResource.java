package cn.ibizlab.odoo.odoo_mail.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_mail.dto.*;
import cn.ibizlab.odoo.odoo_mail.mapping.*;
import cn.ibizlab.odoo.core.odoo_mail.domain.Mail_statistics_report;
import cn.ibizlab.odoo.core.odoo_mail.service.IMail_statistics_reportService;
import cn.ibizlab.odoo.core.odoo_mail.filter.Mail_statistics_reportSearchContext;




@Slf4j
@Api(tags = {"Mail_statistics_report" })
@RestController("odoo_mail-mail_statistics_report")
@RequestMapping("")
public class Mail_statistics_reportResource {

    @Autowired
    private IMail_statistics_reportService mail_statistics_reportService;

    @Autowired
    @Lazy
    private Mail_statistics_reportMapping mail_statistics_reportMapping;




    @PreAuthorize("hasPermission(#mail_statistics_report_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Mail_statistics_report" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/mail_statistics_reports/{mail_statistics_report_id}")

    public ResponseEntity<Mail_statistics_reportDTO> update(@PathVariable("mail_statistics_report_id") Integer mail_statistics_report_id, @RequestBody Mail_statistics_reportDTO mail_statistics_reportdto) {
		Mail_statistics_report domain = mail_statistics_reportMapping.toDomain(mail_statistics_reportdto);
        domain.setId(mail_statistics_report_id);
		mail_statistics_reportService.update(domain);
		Mail_statistics_reportDTO dto = mail_statistics_reportMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#mail_statistics_report_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Mail_statistics_report" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/mail_statistics_reports/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Mail_statistics_reportDTO> mail_statistics_reportdtos) {
        mail_statistics_reportService.updateBatch(mail_statistics_reportMapping.toDomain(mail_statistics_reportdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }







    @PreAuthorize("hasPermission('Remove',{#mail_statistics_report_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Mail_statistics_report" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/mail_statistics_reports/{mail_statistics_report_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("mail_statistics_report_id") Integer mail_statistics_report_id) {
         return ResponseEntity.status(HttpStatus.OK).body(mail_statistics_reportService.remove(mail_statistics_report_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Mail_statistics_report" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/mail_statistics_reports/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        mail_statistics_reportService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }







    @PreAuthorize("hasPermission(#mail_statistics_report_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Mail_statistics_report" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/mail_statistics_reports/{mail_statistics_report_id}")
    public ResponseEntity<Mail_statistics_reportDTO> get(@PathVariable("mail_statistics_report_id") Integer mail_statistics_report_id) {
        Mail_statistics_report domain = mail_statistics_reportService.get(mail_statistics_report_id);
        Mail_statistics_reportDTO dto = mail_statistics_reportMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }




    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Mail_statistics_report" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/mail_statistics_reports")

    public ResponseEntity<Mail_statistics_reportDTO> create(@RequestBody Mail_statistics_reportDTO mail_statistics_reportdto) {
        Mail_statistics_report domain = mail_statistics_reportMapping.toDomain(mail_statistics_reportdto);
		mail_statistics_reportService.create(domain);
        Mail_statistics_reportDTO dto = mail_statistics_reportMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Mail_statistics_report" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/mail_statistics_reports/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Mail_statistics_reportDTO> mail_statistics_reportdtos) {
        mail_statistics_reportService.createBatch(mail_statistics_reportMapping.toDomain(mail_statistics_reportdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Mail_statistics_report" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/mail_statistics_reports/fetchdefault")
	public ResponseEntity<List<Mail_statistics_reportDTO>> fetchDefault(Mail_statistics_reportSearchContext context) {
        Page<Mail_statistics_report> domains = mail_statistics_reportService.searchDefault(context) ;
        List<Mail_statistics_reportDTO> list = mail_statistics_reportMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Mail_statistics_report" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/mail_statistics_reports/searchdefault")
	public ResponseEntity<Page<Mail_statistics_reportDTO>> searchDefault(Mail_statistics_reportSearchContext context) {
        Page<Mail_statistics_report> domains = mail_statistics_reportService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(mail_statistics_reportMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Mail_statistics_report getEntity(){
        return new Mail_statistics_report();
    }

}
