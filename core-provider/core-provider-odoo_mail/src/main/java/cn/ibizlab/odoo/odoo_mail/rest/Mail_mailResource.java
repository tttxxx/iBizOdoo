package cn.ibizlab.odoo.odoo_mail.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_mail.dto.*;
import cn.ibizlab.odoo.odoo_mail.mapping.*;
import cn.ibizlab.odoo.core.odoo_mail.domain.Mail_mail;
import cn.ibizlab.odoo.core.odoo_mail.service.IMail_mailService;
import cn.ibizlab.odoo.core.odoo_mail.filter.Mail_mailSearchContext;




@Slf4j
@Api(tags = {"Mail_mail" })
@RestController("odoo_mail-mail_mail")
@RequestMapping("")
public class Mail_mailResource {

    @Autowired
    private IMail_mailService mail_mailService;

    @Autowired
    @Lazy
    private Mail_mailMapping mail_mailMapping;







    @PreAuthorize("hasPermission(#mail_mail_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Mail_mail" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/mail_mails/{mail_mail_id}")

    public ResponseEntity<Mail_mailDTO> update(@PathVariable("mail_mail_id") Integer mail_mail_id, @RequestBody Mail_mailDTO mail_maildto) {
		Mail_mail domain = mail_mailMapping.toDomain(mail_maildto);
        domain.setId(mail_mail_id);
		mail_mailService.update(domain);
		Mail_mailDTO dto = mail_mailMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#mail_mail_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Mail_mail" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/mail_mails/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Mail_mailDTO> mail_maildtos) {
        mail_mailService.updateBatch(mail_mailMapping.toDomain(mail_maildtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }







    @PreAuthorize("hasPermission(#mail_mail_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Mail_mail" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/mail_mails/{mail_mail_id}")
    public ResponseEntity<Mail_mailDTO> get(@PathVariable("mail_mail_id") Integer mail_mail_id) {
        Mail_mail domain = mail_mailService.get(mail_mail_id);
        Mail_mailDTO dto = mail_mailMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }




    @PreAuthorize("hasPermission('Remove',{#mail_mail_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Mail_mail" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/mail_mails/{mail_mail_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("mail_mail_id") Integer mail_mail_id) {
         return ResponseEntity.status(HttpStatus.OK).body(mail_mailService.remove(mail_mail_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Mail_mail" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/mail_mails/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        mail_mailService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Mail_mail" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/mail_mails")

    public ResponseEntity<Mail_mailDTO> create(@RequestBody Mail_mailDTO mail_maildto) {
        Mail_mail domain = mail_mailMapping.toDomain(mail_maildto);
		mail_mailService.create(domain);
        Mail_mailDTO dto = mail_mailMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Mail_mail" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/mail_mails/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Mail_mailDTO> mail_maildtos) {
        mail_mailService.createBatch(mail_mailMapping.toDomain(mail_maildtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Mail_mail" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/mail_mails/fetchdefault")
	public ResponseEntity<List<Mail_mailDTO>> fetchDefault(Mail_mailSearchContext context) {
        Page<Mail_mail> domains = mail_mailService.searchDefault(context) ;
        List<Mail_mailDTO> list = mail_mailMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Mail_mail" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/mail_mails/searchdefault")
	public ResponseEntity<Page<Mail_mailDTO>> searchDefault(Mail_mailSearchContext context) {
        Page<Mail_mail> domains = mail_mailService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(mail_mailMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Mail_mail getEntity(){
        return new Mail_mail();
    }

}
