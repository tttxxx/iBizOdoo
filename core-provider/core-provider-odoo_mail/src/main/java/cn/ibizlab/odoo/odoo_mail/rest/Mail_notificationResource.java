package cn.ibizlab.odoo.odoo_mail.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_mail.dto.*;
import cn.ibizlab.odoo.odoo_mail.mapping.*;
import cn.ibizlab.odoo.core.odoo_mail.domain.Mail_notification;
import cn.ibizlab.odoo.core.odoo_mail.service.IMail_notificationService;
import cn.ibizlab.odoo.core.odoo_mail.filter.Mail_notificationSearchContext;




@Slf4j
@Api(tags = {"Mail_notification" })
@RestController("odoo_mail-mail_notification")
@RequestMapping("")
public class Mail_notificationResource {

    @Autowired
    private IMail_notificationService mail_notificationService;

    @Autowired
    @Lazy
    private Mail_notificationMapping mail_notificationMapping;




    @PreAuthorize("hasPermission(#mail_notification_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Mail_notification" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/mail_notifications/{mail_notification_id}")

    public ResponseEntity<Mail_notificationDTO> update(@PathVariable("mail_notification_id") Integer mail_notification_id, @RequestBody Mail_notificationDTO mail_notificationdto) {
		Mail_notification domain = mail_notificationMapping.toDomain(mail_notificationdto);
        domain.setId(mail_notification_id);
		mail_notificationService.update(domain);
		Mail_notificationDTO dto = mail_notificationMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#mail_notification_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Mail_notification" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/mail_notifications/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Mail_notificationDTO> mail_notificationdtos) {
        mail_notificationService.updateBatch(mail_notificationMapping.toDomain(mail_notificationdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }







    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Mail_notification" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/mail_notifications")

    public ResponseEntity<Mail_notificationDTO> create(@RequestBody Mail_notificationDTO mail_notificationdto) {
        Mail_notification domain = mail_notificationMapping.toDomain(mail_notificationdto);
		mail_notificationService.create(domain);
        Mail_notificationDTO dto = mail_notificationMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Mail_notification" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/mail_notifications/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Mail_notificationDTO> mail_notificationdtos) {
        mail_notificationService.createBatch(mail_notificationMapping.toDomain(mail_notificationdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission(#mail_notification_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Mail_notification" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/mail_notifications/{mail_notification_id}")
    public ResponseEntity<Mail_notificationDTO> get(@PathVariable("mail_notification_id") Integer mail_notification_id) {
        Mail_notification domain = mail_notificationService.get(mail_notification_id);
        Mail_notificationDTO dto = mail_notificationMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }







    @PreAuthorize("hasPermission('Remove',{#mail_notification_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Mail_notification" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/mail_notifications/{mail_notification_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("mail_notification_id") Integer mail_notification_id) {
         return ResponseEntity.status(HttpStatus.OK).body(mail_notificationService.remove(mail_notification_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Mail_notification" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/mail_notifications/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        mail_notificationService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Mail_notification" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/mail_notifications/fetchdefault")
	public ResponseEntity<List<Mail_notificationDTO>> fetchDefault(Mail_notificationSearchContext context) {
        Page<Mail_notification> domains = mail_notificationService.searchDefault(context) ;
        List<Mail_notificationDTO> list = mail_notificationMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Mail_notification" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/mail_notifications/searchdefault")
	public ResponseEntity<Page<Mail_notificationDTO>> searchDefault(Mail_notificationSearchContext context) {
        Page<Mail_notification> domains = mail_notificationService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(mail_notificationMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Mail_notification getEntity(){
        return new Mail_notification();
    }

}
