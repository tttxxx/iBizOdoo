package cn.ibizlab.odoo.odoo_mail.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_mail.dto.*;
import cn.ibizlab.odoo.odoo_mail.mapping.*;
import cn.ibizlab.odoo.core.odoo_mail.domain.Mail_bot;
import cn.ibizlab.odoo.core.odoo_mail.service.IMail_botService;
import cn.ibizlab.odoo.core.odoo_mail.filter.Mail_botSearchContext;




@Slf4j
@Api(tags = {"Mail_bot" })
@RestController("odoo_mail-mail_bot")
@RequestMapping("")
public class Mail_botResource {

    @Autowired
    private IMail_botService mail_botService;

    @Autowired
    @Lazy
    private Mail_botMapping mail_botMapping;




    @PreAuthorize("hasPermission(#mail_bot_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Mail_bot" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/mail_bots/{mail_bot_id}")

    public ResponseEntity<Mail_botDTO> update(@PathVariable("mail_bot_id") Integer mail_bot_id, @RequestBody Mail_botDTO mail_botdto) {
		Mail_bot domain = mail_botMapping.toDomain(mail_botdto);
        domain.setId(mail_bot_id);
		mail_botService.update(domain);
		Mail_botDTO dto = mail_botMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#mail_bot_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Mail_bot" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/mail_bots/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Mail_botDTO> mail_botdtos) {
        mail_botService.updateBatch(mail_botMapping.toDomain(mail_botdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission(#mail_bot_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Mail_bot" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/mail_bots/{mail_bot_id}")
    public ResponseEntity<Mail_botDTO> get(@PathVariable("mail_bot_id") Integer mail_bot_id) {
        Mail_bot domain = mail_botService.get(mail_bot_id);
        Mail_botDTO dto = mail_botMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }




    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Mail_bot" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/mail_bots")

    public ResponseEntity<Mail_botDTO> create(@RequestBody Mail_botDTO mail_botdto) {
        Mail_bot domain = mail_botMapping.toDomain(mail_botdto);
		mail_botService.create(domain);
        Mail_botDTO dto = mail_botMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Mail_bot" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/mail_bots/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Mail_botDTO> mail_botdtos) {
        mail_botService.createBatch(mail_botMapping.toDomain(mail_botdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }







    @PreAuthorize("hasPermission('Remove',{#mail_bot_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Mail_bot" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/mail_bots/{mail_bot_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("mail_bot_id") Integer mail_bot_id) {
         return ResponseEntity.status(HttpStatus.OK).body(mail_botService.remove(mail_bot_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Mail_bot" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/mail_bots/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        mail_botService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }







    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Mail_bot" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/mail_bots/fetchdefault")
	public ResponseEntity<List<Mail_botDTO>> fetchDefault(Mail_botSearchContext context) {
        Page<Mail_bot> domains = mail_botService.searchDefault(context) ;
        List<Mail_botDTO> list = mail_botMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Mail_bot" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/mail_bots/searchdefault")
	public ResponseEntity<Page<Mail_botDTO>> searchDefault(Mail_botSearchContext context) {
        Page<Mail_bot> domains = mail_botService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(mail_botMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Mail_bot getEntity(){
        return new Mail_bot();
    }

}
