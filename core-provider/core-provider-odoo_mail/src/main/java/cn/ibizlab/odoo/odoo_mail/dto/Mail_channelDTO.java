package cn.ibizlab.odoo.odoo_mail.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;
import cn.ibizlab.odoo.util.domain.DTOBase;
import lombok.Data;

/**
 * 服务DTO对象[Mail_channelDTO]
 */
@Data
public class Mail_channelDTO extends DTOBase implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [WEBSITE_MESSAGE_IDS]
     *
     */
    @JSONField(name = "website_message_ids")
    @JsonProperty("website_message_ids")
    private String websiteMessageIds;

    /**
     * 属性 [IMAGE_MEDIUM]
     *
     */
    @JSONField(name = "image_medium")
    @JsonProperty("image_medium")
    private byte[] imageMedium;

    /**
     * 属性 [MODERATION_COUNT]
     *
     */
    @JSONField(name = "moderation_count")
    @JsonProperty("moderation_count")
    private Integer moderationCount;

    /**
     * 属性 [DESCRIPTION]
     *
     */
    @JSONField(name = "description")
    @JsonProperty("description")
    private String description;

    /**
     * 属性 [MODERATION_NOTIFY]
     *
     */
    @JSONField(name = "moderation_notify")
    @JsonProperty("moderation_notify")
    private String moderationNotify;

    /**
     * 属性 [MESSAGE_MAIN_ATTACHMENT_ID]
     *
     */
    @JSONField(name = "message_main_attachment_id")
    @JsonProperty("message_main_attachment_id")
    private Integer messageMainAttachmentId;

    /**
     * 属性 [MODERATION]
     *
     */
    @JSONField(name = "moderation")
    @JsonProperty("moderation")
    private String moderation;

    /**
     * 属性 [IS_MODERATOR]
     *
     */
    @JSONField(name = "is_moderator")
    @JsonProperty("is_moderator")
    private String isModerator;

    /**
     * 属性 [MODERATION_NOTIFY_MSG]
     *
     */
    @JSONField(name = "moderation_notify_msg")
    @JsonProperty("moderation_notify_msg")
    private String moderationNotifyMsg;

    /**
     * 属性 [MODERATION_IDS]
     *
     */
    @JSONField(name = "moderation_ids")
    @JsonProperty("moderation_ids")
    private String moderationIds;

    /**
     * 属性 [RATING_LAST_IMAGE]
     *
     */
    @JSONField(name = "rating_last_image")
    @JsonProperty("rating_last_image")
    private byte[] ratingLastImage;

    /**
     * 属性 [MESSAGE_HAS_ERROR]
     *
     */
    @JSONField(name = "message_has_error")
    @JsonProperty("message_has_error")
    private String messageHasError;

    /**
     * 属性 [MODERATION_GUIDELINES]
     *
     */
    @JSONField(name = "moderation_guidelines")
    @JsonProperty("moderation_guidelines")
    private String moderationGuidelines;

    /**
     * 属性 [RATING_LAST_FEEDBACK]
     *
     */
    @JSONField(name = "rating_last_feedback")
    @JsonProperty("rating_last_feedback")
    private String ratingLastFeedback;

    /**
     * 属性 [MESSAGE_IDS]
     *
     */
    @JSONField(name = "message_ids")
    @JsonProperty("message_ids")
    private String messageIds;

    /**
     * 属性 [SUBSCRIPTION_DEPARTMENT_IDS]
     *
     */
    @JSONField(name = "subscription_department_ids")
    @JsonProperty("subscription_department_ids")
    private String subscriptionDepartmentIds;

    /**
     * 属性 [MESSAGE_UNREAD_COUNTER]
     *
     */
    @JSONField(name = "message_unread_counter")
    @JsonProperty("message_unread_counter")
    private Integer messageUnreadCounter;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;

    /**
     * 属性 [IBIZPUBLIC]
     *
     */
    @JSONField(name = "ibizpublic")
    @JsonProperty("ibizpublic")
    private String ibizpublic;

    /**
     * 属性 [MESSAGE_FOLLOWER_IDS]
     *
     */
    @JSONField(name = "message_follower_ids")
    @JsonProperty("message_follower_ids")
    private String messageFollowerIds;

    /**
     * 属性 [MESSAGE_CHANNEL_IDS]
     *
     */
    @JSONField(name = "message_channel_ids")
    @JsonProperty("message_channel_ids")
    private String messageChannelIds;

    /**
     * 属性 [RATING_IDS]
     *
     */
    @JSONField(name = "rating_ids")
    @JsonProperty("rating_ids")
    private String ratingIds;

    /**
     * 属性 [ANONYMOUS_NAME]
     *
     */
    @JSONField(name = "anonymous_name")
    @JsonProperty("anonymous_name")
    private String anonymousName;

    /**
     * 属性 [NAME]
     *
     */
    @JSONField(name = "name")
    @JsonProperty("name")
    private String name;

    /**
     * 属性 [IS_MEMBER]
     *
     */
    @JSONField(name = "is_member")
    @JsonProperty("is_member")
    private String isMember;

    /**
     * 属性 [IS_SUBSCRIBED]
     *
     */
    @JSONField(name = "is_subscribed")
    @JsonProperty("is_subscribed")
    private String isSubscribed;

    /**
     * 属性 [MODERATION_GUIDELINES_MSG]
     *
     */
    @JSONField(name = "moderation_guidelines_msg")
    @JsonProperty("moderation_guidelines_msg")
    private String moderationGuidelinesMsg;

    /**
     * 属性 [MESSAGE_IS_FOLLOWER]
     *
     */
    @JSONField(name = "message_is_follower")
    @JsonProperty("message_is_follower")
    private String messageIsFollower;

    /**
     * 属性 [ID]
     *
     */
    @JSONField(name = "id")
    @JsonProperty("id")
    private Integer id;

    /**
     * 属性 [WRITE_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;

    /**
     * 属性 [IMAGE]
     *
     */
    @JSONField(name = "image")
    @JsonProperty("image")
    private byte[] image;

    /**
     * 属性 [MESSAGE_ATTACHMENT_COUNT]
     *
     */
    @JSONField(name = "message_attachment_count")
    @JsonProperty("message_attachment_count")
    private Integer messageAttachmentCount;

    /**
     * 属性 [MESSAGE_UNREAD]
     *
     */
    @JSONField(name = "message_unread")
    @JsonProperty("message_unread")
    private String messageUnread;

    /**
     * 属性 [CREATE_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;

    /**
     * 属性 [IMAGE_SMALL]
     *
     */
    @JSONField(name = "image_small")
    @JsonProperty("image_small")
    private byte[] imageSmall;

    /**
     * 属性 [MESSAGE_PARTNER_IDS]
     *
     */
    @JSONField(name = "message_partner_ids")
    @JsonProperty("message_partner_ids")
    private String messagePartnerIds;

    /**
     * 属性 [MESSAGE_HAS_ERROR_COUNTER]
     *
     */
    @JSONField(name = "message_has_error_counter")
    @JsonProperty("message_has_error_counter")
    private Integer messageHasErrorCounter;

    /**
     * 属性 [CHANNEL_PARTNER_IDS]
     *
     */
    @JSONField(name = "channel_partner_ids")
    @JsonProperty("channel_partner_ids")
    private String channelPartnerIds;

    /**
     * 属性 [MODERATOR_IDS]
     *
     */
    @JSONField(name = "moderator_ids")
    @JsonProperty("moderator_ids")
    private String moderatorIds;

    /**
     * 属性 [GROUP_IDS]
     *
     */
    @JSONField(name = "group_ids")
    @JsonProperty("group_ids")
    private String groupIds;

    /**
     * 属性 [EMAIL_SEND]
     *
     */
    @JSONField(name = "email_send")
    @JsonProperty("email_send")
    private String emailSend;

    /**
     * 属性 [CHANNEL_LAST_SEEN_PARTNER_IDS]
     *
     */
    @JSONField(name = "channel_last_seen_partner_ids")
    @JsonProperty("channel_last_seen_partner_ids")
    private String channelLastSeenPartnerIds;

    /**
     * 属性 [MESSAGE_NEEDACTION]
     *
     */
    @JSONField(name = "message_needaction")
    @JsonProperty("message_needaction")
    private String messageNeedaction;

    /**
     * 属性 [UUID]
     *
     */
    @JSONField(name = "uuid")
    @JsonProperty("uuid")
    private String uuid;

    /**
     * 属性 [RATING_COUNT]
     *
     */
    @JSONField(name = "rating_count")
    @JsonProperty("rating_count")
    private Integer ratingCount;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    private String displayName;

    /**
     * 属性 [MESSAGE_NEEDACTION_COUNTER]
     *
     */
    @JSONField(name = "message_needaction_counter")
    @JsonProperty("message_needaction_counter")
    private Integer messageNeedactionCounter;

    /**
     * 属性 [CHANNEL_MESSAGE_IDS]
     *
     */
    @JSONField(name = "channel_message_ids")
    @JsonProperty("channel_message_ids")
    private String channelMessageIds;

    /**
     * 属性 [CHANNEL_TYPE]
     *
     */
    @JSONField(name = "channel_type")
    @JsonProperty("channel_type")
    private String channelType;

    /**
     * 属性 [IS_CHAT]
     *
     */
    @JSONField(name = "is_chat")
    @JsonProperty("is_chat")
    private String isChat;

    /**
     * 属性 [RATING_LAST_VALUE]
     *
     */
    @JSONField(name = "rating_last_value")
    @JsonProperty("rating_last_value")
    private Double ratingLastValue;

    /**
     * 属性 [ALIAS_FORCE_THREAD_ID]
     *
     */
    @JSONField(name = "alias_force_thread_id")
    @JsonProperty("alias_force_thread_id")
    private Integer aliasForceThreadId;

    /**
     * 属性 [ALIAS_DEFAULTS]
     *
     */
    @JSONField(name = "alias_defaults")
    @JsonProperty("alias_defaults")
    private String aliasDefaults;

    /**
     * 属性 [ALIAS_USER_ID]
     *
     */
    @JSONField(name = "alias_user_id")
    @JsonProperty("alias_user_id")
    private Integer aliasUserId;

    /**
     * 属性 [ALIAS_PARENT_MODEL_ID]
     *
     */
    @JSONField(name = "alias_parent_model_id")
    @JsonProperty("alias_parent_model_id")
    private Integer aliasParentModelId;

    /**
     * 属性 [GROUP_PUBLIC_ID_TEXT]
     *
     */
    @JSONField(name = "group_public_id_text")
    @JsonProperty("group_public_id_text")
    private String groupPublicIdText;

    /**
     * 属性 [WRITE_UID_TEXT]
     *
     */
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    private String writeUidText;

    /**
     * 属性 [ALIAS_PARENT_THREAD_ID]
     *
     */
    @JSONField(name = "alias_parent_thread_id")
    @JsonProperty("alias_parent_thread_id")
    private Integer aliasParentThreadId;

    /**
     * 属性 [ALIAS_NAME]
     *
     */
    @JSONField(name = "alias_name")
    @JsonProperty("alias_name")
    private String aliasName;

    /**
     * 属性 [ALIAS_MODEL_ID]
     *
     */
    @JSONField(name = "alias_model_id")
    @JsonProperty("alias_model_id")
    private Integer aliasModelId;

    /**
     * 属性 [LIVECHAT_CHANNEL_ID_TEXT]
     *
     */
    @JSONField(name = "livechat_channel_id_text")
    @JsonProperty("livechat_channel_id_text")
    private String livechatChannelIdText;

    /**
     * 属性 [ALIAS_DOMAIN]
     *
     */
    @JSONField(name = "alias_domain")
    @JsonProperty("alias_domain")
    private String aliasDomain;

    /**
     * 属性 [CREATE_UID_TEXT]
     *
     */
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    private String createUidText;

    /**
     * 属性 [ALIAS_CONTACT]
     *
     */
    @JSONField(name = "alias_contact")
    @JsonProperty("alias_contact")
    private String aliasContact;

    /**
     * 属性 [GROUP_PUBLIC_ID]
     *
     */
    @JSONField(name = "group_public_id")
    @JsonProperty("group_public_id")
    private Integer groupPublicId;

    /**
     * 属性 [WRITE_UID]
     *
     */
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    private Integer writeUid;

    /**
     * 属性 [CREATE_UID]
     *
     */
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    private Integer createUid;

    /**
     * 属性 [ALIAS_ID]
     *
     */
    @JSONField(name = "alias_id")
    @JsonProperty("alias_id")
    private Integer aliasId;

    /**
     * 属性 [LIVECHAT_CHANNEL_ID]
     *
     */
    @JSONField(name = "livechat_channel_id")
    @JsonProperty("livechat_channel_id")
    private Integer livechatChannelId;


    /**
     * 设置 [DESCRIPTION]
     */
    public void setDescription(String  description){
        this.description = description ;
        this.modify("description",description);
    }

    /**
     * 设置 [MODERATION_NOTIFY]
     */
    public void setModerationNotify(String  moderationNotify){
        this.moderationNotify = moderationNotify ;
        this.modify("moderation_notify",moderationNotify);
    }

    /**
     * 设置 [MESSAGE_MAIN_ATTACHMENT_ID]
     */
    public void setMessageMainAttachmentId(Integer  messageMainAttachmentId){
        this.messageMainAttachmentId = messageMainAttachmentId ;
        this.modify("message_main_attachment_id",messageMainAttachmentId);
    }

    /**
     * 设置 [MODERATION]
     */
    public void setModeration(String  moderation){
        this.moderation = moderation ;
        this.modify("moderation",moderation);
    }

    /**
     * 设置 [MODERATION_NOTIFY_MSG]
     */
    public void setModerationNotifyMsg(String  moderationNotifyMsg){
        this.moderationNotifyMsg = moderationNotifyMsg ;
        this.modify("moderation_notify_msg",moderationNotifyMsg);
    }

    /**
     * 设置 [MODERATION_GUIDELINES]
     */
    public void setModerationGuidelines(String  moderationGuidelines){
        this.moderationGuidelines = moderationGuidelines ;
        this.modify("moderation_guidelines",moderationGuidelines);
    }

    /**
     * 设置 [IBIZPUBLIC]
     */
    public void setIbizpublic(String  ibizpublic){
        this.ibizpublic = ibizpublic ;
        this.modify("ibizpublic",ibizpublic);
    }

    /**
     * 设置 [ANONYMOUS_NAME]
     */
    public void setAnonymousName(String  anonymousName){
        this.anonymousName = anonymousName ;
        this.modify("anonymous_name",anonymousName);
    }

    /**
     * 设置 [NAME]
     */
    public void setName(String  name){
        this.name = name ;
        this.modify("name",name);
    }

    /**
     * 设置 [MODERATION_GUIDELINES_MSG]
     */
    public void setModerationGuidelinesMsg(String  moderationGuidelinesMsg){
        this.moderationGuidelinesMsg = moderationGuidelinesMsg ;
        this.modify("moderation_guidelines_msg",moderationGuidelinesMsg);
    }

    /**
     * 设置 [EMAIL_SEND]
     */
    public void setEmailSend(String  emailSend){
        this.emailSend = emailSend ;
        this.modify("email_send",emailSend);
    }

    /**
     * 设置 [UUID]
     */
    public void setUuid(String  uuid){
        this.uuid = uuid ;
        this.modify("uuid",uuid);
    }

    /**
     * 设置 [CHANNEL_TYPE]
     */
    public void setChannelType(String  channelType){
        this.channelType = channelType ;
        this.modify("channel_type",channelType);
    }

    /**
     * 设置 [RATING_LAST_VALUE]
     */
    public void setRatingLastValue(Double  ratingLastValue){
        this.ratingLastValue = ratingLastValue ;
        this.modify("rating_last_value",ratingLastValue);
    }

    /**
     * 设置 [GROUP_PUBLIC_ID]
     */
    public void setGroupPublicId(Integer  groupPublicId){
        this.groupPublicId = groupPublicId ;
        this.modify("group_public_id",groupPublicId);
    }

    /**
     * 设置 [ALIAS_ID]
     */
    public void setAliasId(Integer  aliasId){
        this.aliasId = aliasId ;
        this.modify("alias_id",aliasId);
    }

    /**
     * 设置 [LIVECHAT_CHANNEL_ID]
     */
    public void setLivechatChannelId(Integer  livechatChannelId){
        this.livechatChannelId = livechatChannelId ;
        this.modify("livechat_channel_id",livechatChannelId);
    }


}

