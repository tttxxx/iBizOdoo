package cn.ibizlab.odoo.odoo_web_tour.config;

import org.springframework.context.annotation.ComponentScan;

@ComponentScan("cn.ibizlab.odoo.odoo_web_tour")
public class odoo_web_tourRestConfiguration {

}
