package cn.ibizlab.odoo.odoo_web_tour.mapping;

import org.mapstruct.*;
import cn.ibizlab.odoo.core.odoo_web_tour.domain.Web_tour_tour;
import cn.ibizlab.odoo.odoo_web_tour.dto.Web_tour_tourDTO;
import cn.ibizlab.odoo.util.domain.MappingBase;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring", uses = {},
    nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
    nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface Web_tour_tourMapping extends MappingBase<Web_tour_tourDTO, Web_tour_tour> {


}

