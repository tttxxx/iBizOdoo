package cn.ibizlab.odoo.odoo_web_tour.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_web_tour.dto.*;
import cn.ibizlab.odoo.odoo_web_tour.mapping.*;
import cn.ibizlab.odoo.core.odoo_web_tour.domain.Web_tour_tour;
import cn.ibizlab.odoo.core.odoo_web_tour.service.IWeb_tour_tourService;
import cn.ibizlab.odoo.core.odoo_web_tour.filter.Web_tour_tourSearchContext;




@Slf4j
@Api(tags = {"Web_tour_tour" })
@RestController("odoo_web_tour-web_tour_tour")
@RequestMapping("")
public class Web_tour_tourResource {

    @Autowired
    private IWeb_tour_tourService web_tour_tourService;

    @Autowired
    @Lazy
    private Web_tour_tourMapping web_tour_tourMapping;




    @PreAuthorize("hasPermission(#web_tour_tour_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Web_tour_tour" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/web_tour_tours/{web_tour_tour_id}")

    public ResponseEntity<Web_tour_tourDTO> update(@PathVariable("web_tour_tour_id") Integer web_tour_tour_id, @RequestBody Web_tour_tourDTO web_tour_tourdto) {
		Web_tour_tour domain = web_tour_tourMapping.toDomain(web_tour_tourdto);
        domain.setId(web_tour_tour_id);
		web_tour_tourService.update(domain);
		Web_tour_tourDTO dto = web_tour_tourMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#web_tour_tour_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Web_tour_tour" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/web_tour_tours/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Web_tour_tourDTO> web_tour_tourdtos) {
        web_tour_tourService.updateBatch(web_tour_tourMapping.toDomain(web_tour_tourdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission('Remove',{#web_tour_tour_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Web_tour_tour" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/web_tour_tours/{web_tour_tour_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("web_tour_tour_id") Integer web_tour_tour_id) {
         return ResponseEntity.status(HttpStatus.OK).body(web_tour_tourService.remove(web_tour_tour_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Web_tour_tour" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/web_tour_tours/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        web_tour_tourService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }







    @PreAuthorize("hasPermission(#web_tour_tour_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Web_tour_tour" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/web_tour_tours/{web_tour_tour_id}")
    public ResponseEntity<Web_tour_tourDTO> get(@PathVariable("web_tour_tour_id") Integer web_tour_tour_id) {
        Web_tour_tour domain = web_tour_tourService.get(web_tour_tour_id);
        Web_tour_tourDTO dto = web_tour_tourMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }







    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Web_tour_tour" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/web_tour_tours")

    public ResponseEntity<Web_tour_tourDTO> create(@RequestBody Web_tour_tourDTO web_tour_tourdto) {
        Web_tour_tour domain = web_tour_tourMapping.toDomain(web_tour_tourdto);
		web_tour_tourService.create(domain);
        Web_tour_tourDTO dto = web_tour_tourMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Web_tour_tour" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/web_tour_tours/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Web_tour_tourDTO> web_tour_tourdtos) {
        web_tour_tourService.createBatch(web_tour_tourMapping.toDomain(web_tour_tourdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Web_tour_tour" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/web_tour_tours/fetchdefault")
	public ResponseEntity<List<Web_tour_tourDTO>> fetchDefault(Web_tour_tourSearchContext context) {
        Page<Web_tour_tour> domains = web_tour_tourService.searchDefault(context) ;
        List<Web_tour_tourDTO> list = web_tour_tourMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Web_tour_tour" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/web_tour_tours/searchdefault")
	public ResponseEntity<Page<Web_tour_tourDTO>> searchDefault(Web_tour_tourSearchContext context) {
        Page<Web_tour_tour> domains = web_tour_tourService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(web_tour_tourMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Web_tour_tour getEntity(){
        return new Web_tour_tour();
    }

}
