package cn.ibizlab.odoo.odoo_hr.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_hr.dto.*;
import cn.ibizlab.odoo.odoo_hr.mapping.*;
import cn.ibizlab.odoo.core.odoo_hr.domain.Hr_leave_report;
import cn.ibizlab.odoo.core.odoo_hr.service.IHr_leave_reportService;
import cn.ibizlab.odoo.core.odoo_hr.filter.Hr_leave_reportSearchContext;




@Slf4j
@Api(tags = {"Hr_leave_report" })
@RestController("odoo_hr-hr_leave_report")
@RequestMapping("")
public class Hr_leave_reportResource {

    @Autowired
    private IHr_leave_reportService hr_leave_reportService;

    @Autowired
    @Lazy
    private Hr_leave_reportMapping hr_leave_reportMapping;










    @PreAuthorize("hasPermission(#hr_leave_report_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Hr_leave_report" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/hr_leave_reports/{hr_leave_report_id}")
    public ResponseEntity<Hr_leave_reportDTO> get(@PathVariable("hr_leave_report_id") Integer hr_leave_report_id) {
        Hr_leave_report domain = hr_leave_reportService.get(hr_leave_report_id);
        Hr_leave_reportDTO dto = hr_leave_reportMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }




    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Hr_leave_report" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/hr_leave_reports")

    public ResponseEntity<Hr_leave_reportDTO> create(@RequestBody Hr_leave_reportDTO hr_leave_reportdto) {
        Hr_leave_report domain = hr_leave_reportMapping.toDomain(hr_leave_reportdto);
		hr_leave_reportService.create(domain);
        Hr_leave_reportDTO dto = hr_leave_reportMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Hr_leave_report" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/hr_leave_reports/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Hr_leave_reportDTO> hr_leave_reportdtos) {
        hr_leave_reportService.createBatch(hr_leave_reportMapping.toDomain(hr_leave_reportdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission(#hr_leave_report_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Hr_leave_report" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/hr_leave_reports/{hr_leave_report_id}")

    public ResponseEntity<Hr_leave_reportDTO> update(@PathVariable("hr_leave_report_id") Integer hr_leave_report_id, @RequestBody Hr_leave_reportDTO hr_leave_reportdto) {
		Hr_leave_report domain = hr_leave_reportMapping.toDomain(hr_leave_reportdto);
        domain.setId(hr_leave_report_id);
		hr_leave_reportService.update(domain);
		Hr_leave_reportDTO dto = hr_leave_reportMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#hr_leave_report_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Hr_leave_report" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/hr_leave_reports/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Hr_leave_reportDTO> hr_leave_reportdtos) {
        hr_leave_reportService.updateBatch(hr_leave_reportMapping.toDomain(hr_leave_reportdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }







    @PreAuthorize("hasPermission('Remove',{#hr_leave_report_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Hr_leave_report" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/hr_leave_reports/{hr_leave_report_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("hr_leave_report_id") Integer hr_leave_report_id) {
         return ResponseEntity.status(HttpStatus.OK).body(hr_leave_reportService.remove(hr_leave_report_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Hr_leave_report" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/hr_leave_reports/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        hr_leave_reportService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Hr_leave_report" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/hr_leave_reports/fetchdefault")
	public ResponseEntity<List<Hr_leave_reportDTO>> fetchDefault(Hr_leave_reportSearchContext context) {
        Page<Hr_leave_report> domains = hr_leave_reportService.searchDefault(context) ;
        List<Hr_leave_reportDTO> list = hr_leave_reportMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Hr_leave_report" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/hr_leave_reports/searchdefault")
	public ResponseEntity<Page<Hr_leave_reportDTO>> searchDefault(Hr_leave_reportSearchContext context) {
        Page<Hr_leave_report> domains = hr_leave_reportService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(hr_leave_reportMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Hr_leave_report getEntity(){
        return new Hr_leave_report();
    }

}
