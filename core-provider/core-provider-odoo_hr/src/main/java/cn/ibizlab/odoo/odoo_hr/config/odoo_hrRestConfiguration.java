package cn.ibizlab.odoo.odoo_hr.config;

import org.springframework.context.annotation.ComponentScan;

@ComponentScan("cn.ibizlab.odoo.odoo_hr")
public class odoo_hrRestConfiguration {

}
