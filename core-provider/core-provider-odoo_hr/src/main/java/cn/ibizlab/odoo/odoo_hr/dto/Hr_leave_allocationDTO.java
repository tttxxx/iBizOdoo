package cn.ibizlab.odoo.odoo_hr.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;
import cn.ibizlab.odoo.util.domain.DTOBase;
import lombok.Data;

/**
 * 服务DTO对象[Hr_leave_allocationDTO]
 */
@Data
public class Hr_leave_allocationDTO extends DTOBase implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [MESSAGE_NEEDACTION]
     *
     */
    @JSONField(name = "message_needaction")
    @JsonProperty("message_needaction")
    private String messageNeedaction;

    /**
     * 属性 [NAME]
     *
     */
    @JSONField(name = "name")
    @JsonProperty("name")
    private String name;

    /**
     * 属性 [DATE_TO]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "date_to" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("date_to")
    private Timestamp dateTo;

    /**
     * 属性 [MESSAGE_MAIN_ATTACHMENT_ID]
     *
     */
    @JSONField(name = "message_main_attachment_id")
    @JsonProperty("message_main_attachment_id")
    private Integer messageMainAttachmentId;

    /**
     * 属性 [NUMBER_PER_INTERVAL]
     *
     */
    @JSONField(name = "number_per_interval")
    @JsonProperty("number_per_interval")
    private Double numberPerInterval;

    /**
     * 属性 [NUMBER_OF_DAYS_DISPLAY]
     *
     */
    @JSONField(name = "number_of_days_display")
    @JsonProperty("number_of_days_display")
    private Double numberOfDaysDisplay;

    /**
     * 属性 [MESSAGE_FOLLOWER_IDS]
     *
     */
    @JSONField(name = "message_follower_ids")
    @JsonProperty("message_follower_ids")
    private String messageFollowerIds;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;

    /**
     * 属性 [HOLIDAY_TYPE]
     *
     */
    @JSONField(name = "holiday_type")
    @JsonProperty("holiday_type")
    private String holidayType;

    /**
     * 属性 [INTERVAL_NUMBER]
     *
     */
    @JSONField(name = "interval_number")
    @JsonProperty("interval_number")
    private Integer intervalNumber;

    /**
     * 属性 [MESSAGE_ATTACHMENT_COUNT]
     *
     */
    @JSONField(name = "message_attachment_count")
    @JsonProperty("message_attachment_count")
    private Integer messageAttachmentCount;

    /**
     * 属性 [NUMBER_OF_HOURS_DISPLAY]
     *
     */
    @JSONField(name = "number_of_hours_display")
    @JsonProperty("number_of_hours_display")
    private Double numberOfHoursDisplay;

    /**
     * 属性 [ACCRUAL]
     *
     */
    @JSONField(name = "accrual")
    @JsonProperty("accrual")
    private String accrual;

    /**
     * 属性 [MESSAGE_UNREAD]
     *
     */
    @JSONField(name = "message_unread")
    @JsonProperty("message_unread")
    private String messageUnread;

    /**
     * 属性 [ACTIVITY_USER_ID]
     *
     */
    @JSONField(name = "activity_user_id")
    @JsonProperty("activity_user_id")
    private Integer activityUserId;

    /**
     * 属性 [LINKED_REQUEST_IDS]
     *
     */
    @JSONField(name = "linked_request_ids")
    @JsonProperty("linked_request_ids")
    private String linkedRequestIds;

    /**
     * 属性 [CREATE_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;

    /**
     * 属性 [ACTIVITY_IDS]
     *
     */
    @JSONField(name = "activity_ids")
    @JsonProperty("activity_ids")
    private String activityIds;

    /**
     * 属性 [MESSAGE_IDS]
     *
     */
    @JSONField(name = "message_ids")
    @JsonProperty("message_ids")
    private String messageIds;

    /**
     * 属性 [ACTIVITY_TYPE_ID]
     *
     */
    @JSONField(name = "activity_type_id")
    @JsonProperty("activity_type_id")
    private Integer activityTypeId;

    /**
     * 属性 [NEXTCALL]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "nextcall" , format="yyyy-MM-dd")
    @JsonProperty("nextcall")
    private Timestamp nextcall;

    /**
     * 属性 [MESSAGE_CHANNEL_IDS]
     *
     */
    @JSONField(name = "message_channel_ids")
    @JsonProperty("message_channel_ids")
    private String messageChannelIds;

    /**
     * 属性 [MESSAGE_NEEDACTION_COUNTER]
     *
     */
    @JSONField(name = "message_needaction_counter")
    @JsonProperty("message_needaction_counter")
    private Integer messageNeedactionCounter;

    /**
     * 属性 [ACTIVITY_SUMMARY]
     *
     */
    @JSONField(name = "activity_summary")
    @JsonProperty("activity_summary")
    private String activitySummary;

    /**
     * 属性 [NUMBER_OF_DAYS]
     *
     */
    @JSONField(name = "number_of_days")
    @JsonProperty("number_of_days")
    private Double numberOfDays;

    /**
     * 属性 [NOTES]
     *
     */
    @JSONField(name = "notes")
    @JsonProperty("notes")
    private String notes;

    /**
     * 属性 [ACTIVITY_STATE]
     *
     */
    @JSONField(name = "activity_state")
    @JsonProperty("activity_state")
    private String activityState;

    /**
     * 属性 [MESSAGE_IS_FOLLOWER]
     *
     */
    @JSONField(name = "message_is_follower")
    @JsonProperty("message_is_follower")
    private String messageIsFollower;

    /**
     * 属性 [MESSAGE_PARTNER_IDS]
     *
     */
    @JSONField(name = "message_partner_ids")
    @JsonProperty("message_partner_ids")
    private String messagePartnerIds;

    /**
     * 属性 [STATE]
     *
     */
    @JSONField(name = "state")
    @JsonProperty("state")
    private String state;

    /**
     * 属性 [INTERVAL_UNIT]
     *
     */
    @JSONField(name = "interval_unit")
    @JsonProperty("interval_unit")
    private String intervalUnit;

    /**
     * 属性 [DURATION_DISPLAY]
     *
     */
    @JSONField(name = "duration_display")
    @JsonProperty("duration_display")
    private String durationDisplay;

    /**
     * 属性 [CAN_APPROVE]
     *
     */
    @JSONField(name = "can_approve")
    @JsonProperty("can_approve")
    private String canApprove;

    /**
     * 属性 [ID]
     *
     */
    @JSONField(name = "id")
    @JsonProperty("id")
    private Integer id;

    /**
     * 属性 [CAN_RESET]
     *
     */
    @JSONField(name = "can_reset")
    @JsonProperty("can_reset")
    private String canReset;

    /**
     * 属性 [MESSAGE_HAS_ERROR_COUNTER]
     *
     */
    @JSONField(name = "message_has_error_counter")
    @JsonProperty("message_has_error_counter")
    private Integer messageHasErrorCounter;

    /**
     * 属性 [WEBSITE_MESSAGE_IDS]
     *
     */
    @JSONField(name = "website_message_ids")
    @JsonProperty("website_message_ids")
    private String websiteMessageIds;

    /**
     * 属性 [UNIT_PER_INTERVAL]
     *
     */
    @JSONField(name = "unit_per_interval")
    @JsonProperty("unit_per_interval")
    private String unitPerInterval;

    /**
     * 属性 [ACCRUAL_LIMIT]
     *
     */
    @JSONField(name = "accrual_limit")
    @JsonProperty("accrual_limit")
    private Integer accrualLimit;

    /**
     * 属性 [WRITE_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;

    /**
     * 属性 [MESSAGE_UNREAD_COUNTER]
     *
     */
    @JSONField(name = "message_unread_counter")
    @JsonProperty("message_unread_counter")
    private Integer messageUnreadCounter;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    private String displayName;

    /**
     * 属性 [DATE_FROM]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "date_from" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("date_from")
    private Timestamp dateFrom;

    /**
     * 属性 [MESSAGE_HAS_ERROR]
     *
     */
    @JSONField(name = "message_has_error")
    @JsonProperty("message_has_error")
    private String messageHasError;

    /**
     * 属性 [ACTIVITY_DATE_DEADLINE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "activity_date_deadline" , format="yyyy-MM-dd")
    @JsonProperty("activity_date_deadline")
    private Timestamp activityDateDeadline;

    /**
     * 属性 [DEPARTMENT_ID_TEXT]
     *
     */
    @JSONField(name = "department_id_text")
    @JsonProperty("department_id_text")
    private String departmentIdText;

    /**
     * 属性 [EMPLOYEE_ID_TEXT]
     *
     */
    @JSONField(name = "employee_id_text")
    @JsonProperty("employee_id_text")
    private String employeeIdText;

    /**
     * 属性 [VALIDATION_TYPE]
     *
     */
    @JSONField(name = "validation_type")
    @JsonProperty("validation_type")
    private String validationType;

    /**
     * 属性 [CREATE_UID_TEXT]
     *
     */
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    private String createUidText;

    /**
     * 属性 [MODE_COMPANY_ID_TEXT]
     *
     */
    @JSONField(name = "mode_company_id_text")
    @JsonProperty("mode_company_id_text")
    private String modeCompanyIdText;

    /**
     * 属性 [FIRST_APPROVER_ID_TEXT]
     *
     */
    @JSONField(name = "first_approver_id_text")
    @JsonProperty("first_approver_id_text")
    private String firstApproverIdText;

    /**
     * 属性 [HOLIDAY_STATUS_ID_TEXT]
     *
     */
    @JSONField(name = "holiday_status_id_text")
    @JsonProperty("holiday_status_id_text")
    private String holidayStatusIdText;

    /**
     * 属性 [WRITE_UID_TEXT]
     *
     */
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    private String writeUidText;

    /**
     * 属性 [TYPE_REQUEST_UNIT]
     *
     */
    @JSONField(name = "type_request_unit")
    @JsonProperty("type_request_unit")
    private String typeRequestUnit;

    /**
     * 属性 [PARENT_ID_TEXT]
     *
     */
    @JSONField(name = "parent_id_text")
    @JsonProperty("parent_id_text")
    private String parentIdText;

    /**
     * 属性 [SECOND_APPROVER_ID_TEXT]
     *
     */
    @JSONField(name = "second_approver_id_text")
    @JsonProperty("second_approver_id_text")
    private String secondApproverIdText;

    /**
     * 属性 [CATEGORY_ID_TEXT]
     *
     */
    @JSONField(name = "category_id_text")
    @JsonProperty("category_id_text")
    private String categoryIdText;

    /**
     * 属性 [HOLIDAY_STATUS_ID]
     *
     */
    @JSONField(name = "holiday_status_id")
    @JsonProperty("holiday_status_id")
    private Integer holidayStatusId;

    /**
     * 属性 [DEPARTMENT_ID]
     *
     */
    @JSONField(name = "department_id")
    @JsonProperty("department_id")
    private Integer departmentId;

    /**
     * 属性 [PARENT_ID]
     *
     */
    @JSONField(name = "parent_id")
    @JsonProperty("parent_id")
    private Integer parentId;

    /**
     * 属性 [CREATE_UID]
     *
     */
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    private Integer createUid;

    /**
     * 属性 [SECOND_APPROVER_ID]
     *
     */
    @JSONField(name = "second_approver_id")
    @JsonProperty("second_approver_id")
    private Integer secondApproverId;

    /**
     * 属性 [MODE_COMPANY_ID]
     *
     */
    @JSONField(name = "mode_company_id")
    @JsonProperty("mode_company_id")
    private Integer modeCompanyId;

    /**
     * 属性 [WRITE_UID]
     *
     */
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    private Integer writeUid;

    /**
     * 属性 [FIRST_APPROVER_ID]
     *
     */
    @JSONField(name = "first_approver_id")
    @JsonProperty("first_approver_id")
    private Integer firstApproverId;

    /**
     * 属性 [CATEGORY_ID]
     *
     */
    @JSONField(name = "category_id")
    @JsonProperty("category_id")
    private Integer categoryId;

    /**
     * 属性 [EMPLOYEE_ID]
     *
     */
    @JSONField(name = "employee_id")
    @JsonProperty("employee_id")
    private Integer employeeId;


    /**
     * 设置 [NAME]
     */
    public void setName(String  name){
        this.name = name ;
        this.modify("name",name);
    }

    /**
     * 设置 [DATE_TO]
     */
    public void setDateTo(Timestamp  dateTo){
        this.dateTo = dateTo ;
        this.modify("date_to",dateTo);
    }

    /**
     * 设置 [MESSAGE_MAIN_ATTACHMENT_ID]
     */
    public void setMessageMainAttachmentId(Integer  messageMainAttachmentId){
        this.messageMainAttachmentId = messageMainAttachmentId ;
        this.modify("message_main_attachment_id",messageMainAttachmentId);
    }

    /**
     * 设置 [NUMBER_PER_INTERVAL]
     */
    public void setNumberPerInterval(Double  numberPerInterval){
        this.numberPerInterval = numberPerInterval ;
        this.modify("number_per_interval",numberPerInterval);
    }

    /**
     * 设置 [HOLIDAY_TYPE]
     */
    public void setHolidayType(String  holidayType){
        this.holidayType = holidayType ;
        this.modify("holiday_type",holidayType);
    }

    /**
     * 设置 [INTERVAL_NUMBER]
     */
    public void setIntervalNumber(Integer  intervalNumber){
        this.intervalNumber = intervalNumber ;
        this.modify("interval_number",intervalNumber);
    }

    /**
     * 设置 [ACCRUAL]
     */
    public void setAccrual(String  accrual){
        this.accrual = accrual ;
        this.modify("accrual",accrual);
    }

    /**
     * 设置 [NEXTCALL]
     */
    public void setNextcall(Timestamp  nextcall){
        this.nextcall = nextcall ;
        this.modify("nextcall",nextcall);
    }

    /**
     * 设置 [NUMBER_OF_DAYS]
     */
    public void setNumberOfDays(Double  numberOfDays){
        this.numberOfDays = numberOfDays ;
        this.modify("number_of_days",numberOfDays);
    }

    /**
     * 设置 [NOTES]
     */
    public void setNotes(String  notes){
        this.notes = notes ;
        this.modify("notes",notes);
    }

    /**
     * 设置 [STATE]
     */
    public void setState(String  state){
        this.state = state ;
        this.modify("state",state);
    }

    /**
     * 设置 [INTERVAL_UNIT]
     */
    public void setIntervalUnit(String  intervalUnit){
        this.intervalUnit = intervalUnit ;
        this.modify("interval_unit",intervalUnit);
    }

    /**
     * 设置 [UNIT_PER_INTERVAL]
     */
    public void setUnitPerInterval(String  unitPerInterval){
        this.unitPerInterval = unitPerInterval ;
        this.modify("unit_per_interval",unitPerInterval);
    }

    /**
     * 设置 [ACCRUAL_LIMIT]
     */
    public void setAccrualLimit(Integer  accrualLimit){
        this.accrualLimit = accrualLimit ;
        this.modify("accrual_limit",accrualLimit);
    }

    /**
     * 设置 [DATE_FROM]
     */
    public void setDateFrom(Timestamp  dateFrom){
        this.dateFrom = dateFrom ;
        this.modify("date_from",dateFrom);
    }

    /**
     * 设置 [HOLIDAY_STATUS_ID]
     */
    public void setHolidayStatusId(Integer  holidayStatusId){
        this.holidayStatusId = holidayStatusId ;
        this.modify("holiday_status_id",holidayStatusId);
    }

    /**
     * 设置 [DEPARTMENT_ID]
     */
    public void setDepartmentId(Integer  departmentId){
        this.departmentId = departmentId ;
        this.modify("department_id",departmentId);
    }

    /**
     * 设置 [PARENT_ID]
     */
    public void setParentId(Integer  parentId){
        this.parentId = parentId ;
        this.modify("parent_id",parentId);
    }

    /**
     * 设置 [SECOND_APPROVER_ID]
     */
    public void setSecondApproverId(Integer  secondApproverId){
        this.secondApproverId = secondApproverId ;
        this.modify("second_approver_id",secondApproverId);
    }

    /**
     * 设置 [MODE_COMPANY_ID]
     */
    public void setModeCompanyId(Integer  modeCompanyId){
        this.modeCompanyId = modeCompanyId ;
        this.modify("mode_company_id",modeCompanyId);
    }

    /**
     * 设置 [FIRST_APPROVER_ID]
     */
    public void setFirstApproverId(Integer  firstApproverId){
        this.firstApproverId = firstApproverId ;
        this.modify("first_approver_id",firstApproverId);
    }

    /**
     * 设置 [CATEGORY_ID]
     */
    public void setCategoryId(Integer  categoryId){
        this.categoryId = categoryId ;
        this.modify("category_id",categoryId);
    }

    /**
     * 设置 [EMPLOYEE_ID]
     */
    public void setEmployeeId(Integer  employeeId){
        this.employeeId = employeeId ;
        this.modify("employee_id",employeeId);
    }


}

