package cn.ibizlab.odoo.odoo_hr.mapping;

import org.mapstruct.*;
import cn.ibizlab.odoo.core.odoo_hr.domain.Hr_leave_type;
import cn.ibizlab.odoo.odoo_hr.dto.Hr_leave_typeDTO;
import cn.ibizlab.odoo.util.domain.MappingBase;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring", uses = {},
    nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
    nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface Hr_leave_typeMapping extends MappingBase<Hr_leave_typeDTO, Hr_leave_type> {


}

