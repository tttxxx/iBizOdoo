package cn.ibizlab.odoo.odoo_hr.mapping;

import org.mapstruct.*;
import cn.ibizlab.odoo.core.odoo_hr.domain.Hr_job;
import cn.ibizlab.odoo.odoo_hr.dto.Hr_jobDTO;
import cn.ibizlab.odoo.util.domain.MappingBase;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring", uses = {},
    nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
    nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface Hr_jobMapping extends MappingBase<Hr_jobDTO, Hr_job> {


}

