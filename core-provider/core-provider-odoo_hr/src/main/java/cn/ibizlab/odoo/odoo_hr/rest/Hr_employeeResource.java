package cn.ibizlab.odoo.odoo_hr.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_hr.dto.*;
import cn.ibizlab.odoo.odoo_hr.mapping.*;
import cn.ibizlab.odoo.core.odoo_hr.domain.Hr_employee;
import cn.ibizlab.odoo.core.odoo_hr.service.IHr_employeeService;
import cn.ibizlab.odoo.core.odoo_hr.filter.Hr_employeeSearchContext;




@Slf4j
@Api(tags = {"Hr_employee" })
@RestController("odoo_hr-hr_employee")
@RequestMapping("")
public class Hr_employeeResource {

    @Autowired
    private IHr_employeeService hr_employeeService;

    @Autowired
    @Lazy
    private Hr_employeeMapping hr_employeeMapping;




    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Hr_employee" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/hr_employees")

    public ResponseEntity<Hr_employeeDTO> create(@RequestBody Hr_employeeDTO hr_employeedto) {
        Hr_employee domain = hr_employeeMapping.toDomain(hr_employeedto);
		hr_employeeService.create(domain);
        Hr_employeeDTO dto = hr_employeeMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Hr_employee" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/hr_employees/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Hr_employeeDTO> hr_employeedtos) {
        hr_employeeService.createBatch(hr_employeeMapping.toDomain(hr_employeedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission('Remove',{#hr_employee_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Hr_employee" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/hr_employees/{hr_employee_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("hr_employee_id") Integer hr_employee_id) {
         return ResponseEntity.status(HttpStatus.OK).body(hr_employeeService.remove(hr_employee_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Hr_employee" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/hr_employees/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        hr_employeeService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission(#hr_employee_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Hr_employee" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/hr_employees/{hr_employee_id}")

    public ResponseEntity<Hr_employeeDTO> update(@PathVariable("hr_employee_id") Integer hr_employee_id, @RequestBody Hr_employeeDTO hr_employeedto) {
		Hr_employee domain = hr_employeeMapping.toDomain(hr_employeedto);
        domain.setId(hr_employee_id);
		hr_employeeService.update(domain);
		Hr_employeeDTO dto = hr_employeeMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#hr_employee_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Hr_employee" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/hr_employees/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Hr_employeeDTO> hr_employeedtos) {
        hr_employeeService.updateBatch(hr_employeeMapping.toDomain(hr_employeedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }







    @PreAuthorize("hasPermission(#hr_employee_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Hr_employee" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/hr_employees/{hr_employee_id}")
    public ResponseEntity<Hr_employeeDTO> get(@PathVariable("hr_employee_id") Integer hr_employee_id) {
        Hr_employee domain = hr_employeeService.get(hr_employee_id);
        Hr_employeeDTO dto = hr_employeeMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }







    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Hr_employee" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/hr_employees/fetchdefault")
	public ResponseEntity<List<Hr_employeeDTO>> fetchDefault(Hr_employeeSearchContext context) {
        Page<Hr_employee> domains = hr_employeeService.searchDefault(context) ;
        List<Hr_employeeDTO> list = hr_employeeMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Hr_employee" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/hr_employees/searchdefault")
	public ResponseEntity<Page<Hr_employeeDTO>> searchDefault(Hr_employeeSearchContext context) {
        Page<Hr_employee> domains = hr_employeeService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(hr_employeeMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Hr_employee getEntity(){
        return new Hr_employee();
    }

}
