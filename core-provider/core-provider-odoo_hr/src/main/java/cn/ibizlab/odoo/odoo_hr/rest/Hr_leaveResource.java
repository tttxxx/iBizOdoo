package cn.ibizlab.odoo.odoo_hr.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_hr.dto.*;
import cn.ibizlab.odoo.odoo_hr.mapping.*;
import cn.ibizlab.odoo.core.odoo_hr.domain.Hr_leave;
import cn.ibizlab.odoo.core.odoo_hr.service.IHr_leaveService;
import cn.ibizlab.odoo.core.odoo_hr.filter.Hr_leaveSearchContext;




@Slf4j
@Api(tags = {"Hr_leave" })
@RestController("odoo_hr-hr_leave")
@RequestMapping("")
public class Hr_leaveResource {

    @Autowired
    private IHr_leaveService hr_leaveService;

    @Autowired
    @Lazy
    private Hr_leaveMapping hr_leaveMapping;




    @PreAuthorize("hasPermission(#hr_leave_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Hr_leave" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/hr_leaves/{hr_leave_id}")

    public ResponseEntity<Hr_leaveDTO> update(@PathVariable("hr_leave_id") Integer hr_leave_id, @RequestBody Hr_leaveDTO hr_leavedto) {
		Hr_leave domain = hr_leaveMapping.toDomain(hr_leavedto);
        domain.setId(hr_leave_id);
		hr_leaveService.update(domain);
		Hr_leaveDTO dto = hr_leaveMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#hr_leave_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Hr_leave" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/hr_leaves/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Hr_leaveDTO> hr_leavedtos) {
        hr_leaveService.updateBatch(hr_leaveMapping.toDomain(hr_leavedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }










    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Hr_leave" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/hr_leaves")

    public ResponseEntity<Hr_leaveDTO> create(@RequestBody Hr_leaveDTO hr_leavedto) {
        Hr_leave domain = hr_leaveMapping.toDomain(hr_leavedto);
		hr_leaveService.create(domain);
        Hr_leaveDTO dto = hr_leaveMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Hr_leave" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/hr_leaves/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Hr_leaveDTO> hr_leavedtos) {
        hr_leaveService.createBatch(hr_leaveMapping.toDomain(hr_leavedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }







    @PreAuthorize("hasPermission(#hr_leave_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Hr_leave" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/hr_leaves/{hr_leave_id}")
    public ResponseEntity<Hr_leaveDTO> get(@PathVariable("hr_leave_id") Integer hr_leave_id) {
        Hr_leave domain = hr_leaveService.get(hr_leave_id);
        Hr_leaveDTO dto = hr_leaveMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }




    @PreAuthorize("hasPermission('Remove',{#hr_leave_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Hr_leave" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/hr_leaves/{hr_leave_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("hr_leave_id") Integer hr_leave_id) {
         return ResponseEntity.status(HttpStatus.OK).body(hr_leaveService.remove(hr_leave_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Hr_leave" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/hr_leaves/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        hr_leaveService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Hr_leave" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/hr_leaves/fetchdefault")
	public ResponseEntity<List<Hr_leaveDTO>> fetchDefault(Hr_leaveSearchContext context) {
        Page<Hr_leave> domains = hr_leaveService.searchDefault(context) ;
        List<Hr_leaveDTO> list = hr_leaveMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Hr_leave" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/hr_leaves/searchdefault")
	public ResponseEntity<Page<Hr_leaveDTO>> searchDefault(Hr_leaveSearchContext context) {
        Page<Hr_leave> domains = hr_leaveService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(hr_leaveMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Hr_leave getEntity(){
        return new Hr_leave();
    }

}
