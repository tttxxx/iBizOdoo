package cn.ibizlab.odoo.odoo_hr.mapping;

import org.mapstruct.*;
import cn.ibizlab.odoo.core.odoo_hr.domain.Hr_holidays_summary_dept;
import cn.ibizlab.odoo.odoo_hr.dto.Hr_holidays_summary_deptDTO;
import cn.ibizlab.odoo.util.domain.MappingBase;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring", uses = {},
    nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
    nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface Hr_holidays_summary_deptMapping extends MappingBase<Hr_holidays_summary_deptDTO, Hr_holidays_summary_dept> {


}

