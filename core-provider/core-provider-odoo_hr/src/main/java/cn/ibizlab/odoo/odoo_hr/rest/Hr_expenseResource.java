package cn.ibizlab.odoo.odoo_hr.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_hr.dto.*;
import cn.ibizlab.odoo.odoo_hr.mapping.*;
import cn.ibizlab.odoo.core.odoo_hr.domain.Hr_expense;
import cn.ibizlab.odoo.core.odoo_hr.service.IHr_expenseService;
import cn.ibizlab.odoo.core.odoo_hr.filter.Hr_expenseSearchContext;




@Slf4j
@Api(tags = {"Hr_expense" })
@RestController("odoo_hr-hr_expense")
@RequestMapping("")
public class Hr_expenseResource {

    @Autowired
    private IHr_expenseService hr_expenseService;

    @Autowired
    @Lazy
    private Hr_expenseMapping hr_expenseMapping;







    @PreAuthorize("hasPermission(#hr_expense_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Hr_expense" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/hr_expenses/{hr_expense_id}")

    public ResponseEntity<Hr_expenseDTO> update(@PathVariable("hr_expense_id") Integer hr_expense_id, @RequestBody Hr_expenseDTO hr_expensedto) {
		Hr_expense domain = hr_expenseMapping.toDomain(hr_expensedto);
        domain.setId(hr_expense_id);
		hr_expenseService.update(domain);
		Hr_expenseDTO dto = hr_expenseMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#hr_expense_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Hr_expense" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/hr_expenses/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Hr_expenseDTO> hr_expensedtos) {
        hr_expenseService.updateBatch(hr_expenseMapping.toDomain(hr_expensedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission('Remove',{#hr_expense_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Hr_expense" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/hr_expenses/{hr_expense_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("hr_expense_id") Integer hr_expense_id) {
         return ResponseEntity.status(HttpStatus.OK).body(hr_expenseService.remove(hr_expense_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Hr_expense" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/hr_expenses/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        hr_expenseService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission(#hr_expense_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Hr_expense" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/hr_expenses/{hr_expense_id}")
    public ResponseEntity<Hr_expenseDTO> get(@PathVariable("hr_expense_id") Integer hr_expense_id) {
        Hr_expense domain = hr_expenseService.get(hr_expense_id);
        Hr_expenseDTO dto = hr_expenseMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }










    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Hr_expense" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/hr_expenses")

    public ResponseEntity<Hr_expenseDTO> create(@RequestBody Hr_expenseDTO hr_expensedto) {
        Hr_expense domain = hr_expenseMapping.toDomain(hr_expensedto);
		hr_expenseService.create(domain);
        Hr_expenseDTO dto = hr_expenseMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Hr_expense" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/hr_expenses/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Hr_expenseDTO> hr_expensedtos) {
        hr_expenseService.createBatch(hr_expenseMapping.toDomain(hr_expensedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Hr_expense" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/hr_expenses/fetchdefault")
	public ResponseEntity<List<Hr_expenseDTO>> fetchDefault(Hr_expenseSearchContext context) {
        Page<Hr_expense> domains = hr_expenseService.searchDefault(context) ;
        List<Hr_expenseDTO> list = hr_expenseMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Hr_expense" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/hr_expenses/searchdefault")
	public ResponseEntity<Page<Hr_expenseDTO>> searchDefault(Hr_expenseSearchContext context) {
        Page<Hr_expense> domains = hr_expenseService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(hr_expenseMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Hr_expense getEntity(){
        return new Hr_expense();
    }

}
