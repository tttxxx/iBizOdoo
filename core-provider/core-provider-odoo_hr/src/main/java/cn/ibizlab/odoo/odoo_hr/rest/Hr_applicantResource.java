package cn.ibizlab.odoo.odoo_hr.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_hr.dto.*;
import cn.ibizlab.odoo.odoo_hr.mapping.*;
import cn.ibizlab.odoo.core.odoo_hr.domain.Hr_applicant;
import cn.ibizlab.odoo.core.odoo_hr.service.IHr_applicantService;
import cn.ibizlab.odoo.core.odoo_hr.filter.Hr_applicantSearchContext;




@Slf4j
@Api(tags = {"Hr_applicant" })
@RestController("odoo_hr-hr_applicant")
@RequestMapping("")
public class Hr_applicantResource {

    @Autowired
    private IHr_applicantService hr_applicantService;

    @Autowired
    @Lazy
    private Hr_applicantMapping hr_applicantMapping;




    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Hr_applicant" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/hr_applicants")

    public ResponseEntity<Hr_applicantDTO> create(@RequestBody Hr_applicantDTO hr_applicantdto) {
        Hr_applicant domain = hr_applicantMapping.toDomain(hr_applicantdto);
		hr_applicantService.create(domain);
        Hr_applicantDTO dto = hr_applicantMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Hr_applicant" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/hr_applicants/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Hr_applicantDTO> hr_applicantdtos) {
        hr_applicantService.createBatch(hr_applicantMapping.toDomain(hr_applicantdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }







    @PreAuthorize("hasPermission('Remove',{#hr_applicant_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Hr_applicant" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/hr_applicants/{hr_applicant_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("hr_applicant_id") Integer hr_applicant_id) {
         return ResponseEntity.status(HttpStatus.OK).body(hr_applicantService.remove(hr_applicant_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Hr_applicant" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/hr_applicants/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        hr_applicantService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }







    @PreAuthorize("hasPermission(#hr_applicant_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Hr_applicant" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/hr_applicants/{hr_applicant_id}")

    public ResponseEntity<Hr_applicantDTO> update(@PathVariable("hr_applicant_id") Integer hr_applicant_id, @RequestBody Hr_applicantDTO hr_applicantdto) {
		Hr_applicant domain = hr_applicantMapping.toDomain(hr_applicantdto);
        domain.setId(hr_applicant_id);
		hr_applicantService.update(domain);
		Hr_applicantDTO dto = hr_applicantMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#hr_applicant_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Hr_applicant" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/hr_applicants/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Hr_applicantDTO> hr_applicantdtos) {
        hr_applicantService.updateBatch(hr_applicantMapping.toDomain(hr_applicantdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }







    @PreAuthorize("hasPermission(#hr_applicant_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Hr_applicant" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/hr_applicants/{hr_applicant_id}")
    public ResponseEntity<Hr_applicantDTO> get(@PathVariable("hr_applicant_id") Integer hr_applicant_id) {
        Hr_applicant domain = hr_applicantService.get(hr_applicant_id);
        Hr_applicantDTO dto = hr_applicantMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Hr_applicant" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/hr_applicants/fetchdefault")
	public ResponseEntity<List<Hr_applicantDTO>> fetchDefault(Hr_applicantSearchContext context) {
        Page<Hr_applicant> domains = hr_applicantService.searchDefault(context) ;
        List<Hr_applicantDTO> list = hr_applicantMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Hr_applicant" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/hr_applicants/searchdefault")
	public ResponseEntity<Page<Hr_applicantDTO>> searchDefault(Hr_applicantSearchContext context) {
        Page<Hr_applicant> domains = hr_applicantService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(hr_applicantMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Hr_applicant getEntity(){
        return new Hr_applicant();
    }

}
