package cn.ibizlab.odoo.odoo_hr.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_hr.dto.*;
import cn.ibizlab.odoo.odoo_hr.mapping.*;
import cn.ibizlab.odoo.core.odoo_hr.domain.Hr_job;
import cn.ibizlab.odoo.core.odoo_hr.service.IHr_jobService;
import cn.ibizlab.odoo.core.odoo_hr.filter.Hr_jobSearchContext;




@Slf4j
@Api(tags = {"Hr_job" })
@RestController("odoo_hr-hr_job")
@RequestMapping("")
public class Hr_jobResource {

    @Autowired
    private IHr_jobService hr_jobService;

    @Autowired
    @Lazy
    private Hr_jobMapping hr_jobMapping;




    @PreAuthorize("hasPermission(#hr_job_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Hr_job" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/hr_jobs/{hr_job_id}")

    public ResponseEntity<Hr_jobDTO> update(@PathVariable("hr_job_id") Integer hr_job_id, @RequestBody Hr_jobDTO hr_jobdto) {
		Hr_job domain = hr_jobMapping.toDomain(hr_jobdto);
        domain.setId(hr_job_id);
		hr_jobService.update(domain);
		Hr_jobDTO dto = hr_jobMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#hr_job_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Hr_job" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/hr_jobs/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Hr_jobDTO> hr_jobdtos) {
        hr_jobService.updateBatch(hr_jobMapping.toDomain(hr_jobdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }










    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Hr_job" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/hr_jobs")

    public ResponseEntity<Hr_jobDTO> create(@RequestBody Hr_jobDTO hr_jobdto) {
        Hr_job domain = hr_jobMapping.toDomain(hr_jobdto);
		hr_jobService.create(domain);
        Hr_jobDTO dto = hr_jobMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Hr_job" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/hr_jobs/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Hr_jobDTO> hr_jobdtos) {
        hr_jobService.createBatch(hr_jobMapping.toDomain(hr_jobdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission('Remove',{#hr_job_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Hr_job" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/hr_jobs/{hr_job_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("hr_job_id") Integer hr_job_id) {
         return ResponseEntity.status(HttpStatus.OK).body(hr_jobService.remove(hr_job_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Hr_job" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/hr_jobs/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        hr_jobService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission(#hr_job_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Hr_job" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/hr_jobs/{hr_job_id}")
    public ResponseEntity<Hr_jobDTO> get(@PathVariable("hr_job_id") Integer hr_job_id) {
        Hr_job domain = hr_jobService.get(hr_job_id);
        Hr_jobDTO dto = hr_jobMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }




    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Hr_job" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/hr_jobs/fetchdefault")
	public ResponseEntity<List<Hr_jobDTO>> fetchDefault(Hr_jobSearchContext context) {
        Page<Hr_job> domains = hr_jobService.searchDefault(context) ;
        List<Hr_jobDTO> list = hr_jobMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Hr_job" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/hr_jobs/searchdefault")
	public ResponseEntity<Page<Hr_jobDTO>> searchDefault(Hr_jobSearchContext context) {
        Page<Hr_job> domains = hr_jobService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(hr_jobMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Hr_job getEntity(){
        return new Hr_job();
    }

}
