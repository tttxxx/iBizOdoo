package cn.ibizlab.odoo.odoo_hr.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_hr.dto.*;
import cn.ibizlab.odoo.odoo_hr.mapping.*;
import cn.ibizlab.odoo.core.odoo_hr.domain.Hr_department;
import cn.ibizlab.odoo.core.odoo_hr.service.IHr_departmentService;
import cn.ibizlab.odoo.core.odoo_hr.filter.Hr_departmentSearchContext;




@Slf4j
@Api(tags = {"Hr_department" })
@RestController("odoo_hr-hr_department")
@RequestMapping("")
public class Hr_departmentResource {

    @Autowired
    private IHr_departmentService hr_departmentService;

    @Autowired
    @Lazy
    private Hr_departmentMapping hr_departmentMapping;




    @PreAuthorize("hasPermission(#hr_department_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Hr_department" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/hr_departments/{hr_department_id}")

    public ResponseEntity<Hr_departmentDTO> update(@PathVariable("hr_department_id") Integer hr_department_id, @RequestBody Hr_departmentDTO hr_departmentdto) {
		Hr_department domain = hr_departmentMapping.toDomain(hr_departmentdto);
        domain.setId(hr_department_id);
		hr_departmentService.update(domain);
		Hr_departmentDTO dto = hr_departmentMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#hr_department_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Hr_department" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/hr_departments/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Hr_departmentDTO> hr_departmentdtos) {
        hr_departmentService.updateBatch(hr_departmentMapping.toDomain(hr_departmentdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }













    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Hr_department" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/hr_departments")

    public ResponseEntity<Hr_departmentDTO> create(@RequestBody Hr_departmentDTO hr_departmentdto) {
        Hr_department domain = hr_departmentMapping.toDomain(hr_departmentdto);
		hr_departmentService.create(domain);
        Hr_departmentDTO dto = hr_departmentMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Hr_department" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/hr_departments/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Hr_departmentDTO> hr_departmentdtos) {
        hr_departmentService.createBatch(hr_departmentMapping.toDomain(hr_departmentdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission('Remove',{#hr_department_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Hr_department" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/hr_departments/{hr_department_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("hr_department_id") Integer hr_department_id) {
         return ResponseEntity.status(HttpStatus.OK).body(hr_departmentService.remove(hr_department_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Hr_department" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/hr_departments/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        hr_departmentService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission(#hr_department_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Hr_department" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/hr_departments/{hr_department_id}")
    public ResponseEntity<Hr_departmentDTO> get(@PathVariable("hr_department_id") Integer hr_department_id) {
        Hr_department domain = hr_departmentService.get(hr_department_id);
        Hr_departmentDTO dto = hr_departmentMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Hr_department" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/hr_departments/fetchdefault")
	public ResponseEntity<List<Hr_departmentDTO>> fetchDefault(Hr_departmentSearchContext context) {
        Page<Hr_department> domains = hr_departmentService.searchDefault(context) ;
        List<Hr_departmentDTO> list = hr_departmentMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Hr_department" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/hr_departments/searchdefault")
	public ResponseEntity<Page<Hr_departmentDTO>> searchDefault(Hr_departmentSearchContext context) {
        Page<Hr_department> domains = hr_departmentService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(hr_departmentMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Hr_department getEntity(){
        return new Hr_department();
    }

}
