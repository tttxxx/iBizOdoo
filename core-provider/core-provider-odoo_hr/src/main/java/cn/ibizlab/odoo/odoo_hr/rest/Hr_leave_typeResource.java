package cn.ibizlab.odoo.odoo_hr.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_hr.dto.*;
import cn.ibizlab.odoo.odoo_hr.mapping.*;
import cn.ibizlab.odoo.core.odoo_hr.domain.Hr_leave_type;
import cn.ibizlab.odoo.core.odoo_hr.service.IHr_leave_typeService;
import cn.ibizlab.odoo.core.odoo_hr.filter.Hr_leave_typeSearchContext;




@Slf4j
@Api(tags = {"Hr_leave_type" })
@RestController("odoo_hr-hr_leave_type")
@RequestMapping("")
public class Hr_leave_typeResource {

    @Autowired
    private IHr_leave_typeService hr_leave_typeService;

    @Autowired
    @Lazy
    private Hr_leave_typeMapping hr_leave_typeMapping;




    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Hr_leave_type" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/hr_leave_types")

    public ResponseEntity<Hr_leave_typeDTO> create(@RequestBody Hr_leave_typeDTO hr_leave_typedto) {
        Hr_leave_type domain = hr_leave_typeMapping.toDomain(hr_leave_typedto);
		hr_leave_typeService.create(domain);
        Hr_leave_typeDTO dto = hr_leave_typeMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Hr_leave_type" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/hr_leave_types/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Hr_leave_typeDTO> hr_leave_typedtos) {
        hr_leave_typeService.createBatch(hr_leave_typeMapping.toDomain(hr_leave_typedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }







    @PreAuthorize("hasPermission(#hr_leave_type_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Hr_leave_type" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/hr_leave_types/{hr_leave_type_id}")

    public ResponseEntity<Hr_leave_typeDTO> update(@PathVariable("hr_leave_type_id") Integer hr_leave_type_id, @RequestBody Hr_leave_typeDTO hr_leave_typedto) {
		Hr_leave_type domain = hr_leave_typeMapping.toDomain(hr_leave_typedto);
        domain.setId(hr_leave_type_id);
		hr_leave_typeService.update(domain);
		Hr_leave_typeDTO dto = hr_leave_typeMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#hr_leave_type_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Hr_leave_type" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/hr_leave_types/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Hr_leave_typeDTO> hr_leave_typedtos) {
        hr_leave_typeService.updateBatch(hr_leave_typeMapping.toDomain(hr_leave_typedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }







    @PreAuthorize("hasPermission(#hr_leave_type_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Hr_leave_type" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/hr_leave_types/{hr_leave_type_id}")
    public ResponseEntity<Hr_leave_typeDTO> get(@PathVariable("hr_leave_type_id") Integer hr_leave_type_id) {
        Hr_leave_type domain = hr_leave_typeService.get(hr_leave_type_id);
        Hr_leave_typeDTO dto = hr_leave_typeMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }







    @PreAuthorize("hasPermission('Remove',{#hr_leave_type_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Hr_leave_type" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/hr_leave_types/{hr_leave_type_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("hr_leave_type_id") Integer hr_leave_type_id) {
         return ResponseEntity.status(HttpStatus.OK).body(hr_leave_typeService.remove(hr_leave_type_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Hr_leave_type" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/hr_leave_types/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        hr_leave_typeService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Hr_leave_type" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/hr_leave_types/fetchdefault")
	public ResponseEntity<List<Hr_leave_typeDTO>> fetchDefault(Hr_leave_typeSearchContext context) {
        Page<Hr_leave_type> domains = hr_leave_typeService.searchDefault(context) ;
        List<Hr_leave_typeDTO> list = hr_leave_typeMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Hr_leave_type" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/hr_leave_types/searchdefault")
	public ResponseEntity<Page<Hr_leave_typeDTO>> searchDefault(Hr_leave_typeSearchContext context) {
        Page<Hr_leave_type> domains = hr_leave_typeService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(hr_leave_typeMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Hr_leave_type getEntity(){
        return new Hr_leave_type();
    }

}
