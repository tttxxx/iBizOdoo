package cn.ibizlab.odoo.odoo_hr.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_hr.dto.*;
import cn.ibizlab.odoo.odoo_hr.mapping.*;
import cn.ibizlab.odoo.core.odoo_hr.domain.Hr_expense_refuse_wizard;
import cn.ibizlab.odoo.core.odoo_hr.service.IHr_expense_refuse_wizardService;
import cn.ibizlab.odoo.core.odoo_hr.filter.Hr_expense_refuse_wizardSearchContext;




@Slf4j
@Api(tags = {"Hr_expense_refuse_wizard" })
@RestController("odoo_hr-hr_expense_refuse_wizard")
@RequestMapping("")
public class Hr_expense_refuse_wizardResource {

    @Autowired
    private IHr_expense_refuse_wizardService hr_expense_refuse_wizardService;

    @Autowired
    @Lazy
    private Hr_expense_refuse_wizardMapping hr_expense_refuse_wizardMapping;




    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Hr_expense_refuse_wizard" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/hr_expense_refuse_wizards")

    public ResponseEntity<Hr_expense_refuse_wizardDTO> create(@RequestBody Hr_expense_refuse_wizardDTO hr_expense_refuse_wizarddto) {
        Hr_expense_refuse_wizard domain = hr_expense_refuse_wizardMapping.toDomain(hr_expense_refuse_wizarddto);
		hr_expense_refuse_wizardService.create(domain);
        Hr_expense_refuse_wizardDTO dto = hr_expense_refuse_wizardMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Hr_expense_refuse_wizard" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/hr_expense_refuse_wizards/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Hr_expense_refuse_wizardDTO> hr_expense_refuse_wizarddtos) {
        hr_expense_refuse_wizardService.createBatch(hr_expense_refuse_wizardMapping.toDomain(hr_expense_refuse_wizarddtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission(#hr_expense_refuse_wizard_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Hr_expense_refuse_wizard" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/hr_expense_refuse_wizards/{hr_expense_refuse_wizard_id}")

    public ResponseEntity<Hr_expense_refuse_wizardDTO> update(@PathVariable("hr_expense_refuse_wizard_id") Integer hr_expense_refuse_wizard_id, @RequestBody Hr_expense_refuse_wizardDTO hr_expense_refuse_wizarddto) {
		Hr_expense_refuse_wizard domain = hr_expense_refuse_wizardMapping.toDomain(hr_expense_refuse_wizarddto);
        domain.setId(hr_expense_refuse_wizard_id);
		hr_expense_refuse_wizardService.update(domain);
		Hr_expense_refuse_wizardDTO dto = hr_expense_refuse_wizardMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#hr_expense_refuse_wizard_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Hr_expense_refuse_wizard" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/hr_expense_refuse_wizards/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Hr_expense_refuse_wizardDTO> hr_expense_refuse_wizarddtos) {
        hr_expense_refuse_wizardService.updateBatch(hr_expense_refuse_wizardMapping.toDomain(hr_expense_refuse_wizarddtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission(#hr_expense_refuse_wizard_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Hr_expense_refuse_wizard" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/hr_expense_refuse_wizards/{hr_expense_refuse_wizard_id}")
    public ResponseEntity<Hr_expense_refuse_wizardDTO> get(@PathVariable("hr_expense_refuse_wizard_id") Integer hr_expense_refuse_wizard_id) {
        Hr_expense_refuse_wizard domain = hr_expense_refuse_wizardService.get(hr_expense_refuse_wizard_id);
        Hr_expense_refuse_wizardDTO dto = hr_expense_refuse_wizardMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }







    @PreAuthorize("hasPermission('Remove',{#hr_expense_refuse_wizard_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Hr_expense_refuse_wizard" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/hr_expense_refuse_wizards/{hr_expense_refuse_wizard_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("hr_expense_refuse_wizard_id") Integer hr_expense_refuse_wizard_id) {
         return ResponseEntity.status(HttpStatus.OK).body(hr_expense_refuse_wizardService.remove(hr_expense_refuse_wizard_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Hr_expense_refuse_wizard" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/hr_expense_refuse_wizards/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        hr_expense_refuse_wizardService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }







    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Hr_expense_refuse_wizard" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/hr_expense_refuse_wizards/fetchdefault")
	public ResponseEntity<List<Hr_expense_refuse_wizardDTO>> fetchDefault(Hr_expense_refuse_wizardSearchContext context) {
        Page<Hr_expense_refuse_wizard> domains = hr_expense_refuse_wizardService.searchDefault(context) ;
        List<Hr_expense_refuse_wizardDTO> list = hr_expense_refuse_wizardMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Hr_expense_refuse_wizard" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/hr_expense_refuse_wizards/searchdefault")
	public ResponseEntity<Page<Hr_expense_refuse_wizardDTO>> searchDefault(Hr_expense_refuse_wizardSearchContext context) {
        Page<Hr_expense_refuse_wizard> domains = hr_expense_refuse_wizardService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(hr_expense_refuse_wizardMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Hr_expense_refuse_wizard getEntity(){
        return new Hr_expense_refuse_wizard();
    }

}
