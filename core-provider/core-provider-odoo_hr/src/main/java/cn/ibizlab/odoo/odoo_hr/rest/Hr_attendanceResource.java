package cn.ibizlab.odoo.odoo_hr.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_hr.dto.*;
import cn.ibizlab.odoo.odoo_hr.mapping.*;
import cn.ibizlab.odoo.core.odoo_hr.domain.Hr_attendance;
import cn.ibizlab.odoo.core.odoo_hr.service.IHr_attendanceService;
import cn.ibizlab.odoo.core.odoo_hr.filter.Hr_attendanceSearchContext;




@Slf4j
@Api(tags = {"Hr_attendance" })
@RestController("odoo_hr-hr_attendance")
@RequestMapping("")
public class Hr_attendanceResource {

    @Autowired
    private IHr_attendanceService hr_attendanceService;

    @Autowired
    @Lazy
    private Hr_attendanceMapping hr_attendanceMapping;




    @PreAuthorize("hasPermission('Remove',{#hr_attendance_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Hr_attendance" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/hr_attendances/{hr_attendance_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("hr_attendance_id") Integer hr_attendance_id) {
         return ResponseEntity.status(HttpStatus.OK).body(hr_attendanceService.remove(hr_attendance_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Hr_attendance" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/hr_attendances/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        hr_attendanceService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }







    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Hr_attendance" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/hr_attendances")

    public ResponseEntity<Hr_attendanceDTO> create(@RequestBody Hr_attendanceDTO hr_attendancedto) {
        Hr_attendance domain = hr_attendanceMapping.toDomain(hr_attendancedto);
		hr_attendanceService.create(domain);
        Hr_attendanceDTO dto = hr_attendanceMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Hr_attendance" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/hr_attendances/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Hr_attendanceDTO> hr_attendancedtos) {
        hr_attendanceService.createBatch(hr_attendanceMapping.toDomain(hr_attendancedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission(#hr_attendance_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Hr_attendance" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/hr_attendances/{hr_attendance_id}")

    public ResponseEntity<Hr_attendanceDTO> update(@PathVariable("hr_attendance_id") Integer hr_attendance_id, @RequestBody Hr_attendanceDTO hr_attendancedto) {
		Hr_attendance domain = hr_attendanceMapping.toDomain(hr_attendancedto);
        domain.setId(hr_attendance_id);
		hr_attendanceService.update(domain);
		Hr_attendanceDTO dto = hr_attendanceMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#hr_attendance_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Hr_attendance" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/hr_attendances/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Hr_attendanceDTO> hr_attendancedtos) {
        hr_attendanceService.updateBatch(hr_attendanceMapping.toDomain(hr_attendancedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission(#hr_attendance_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Hr_attendance" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/hr_attendances/{hr_attendance_id}")
    public ResponseEntity<Hr_attendanceDTO> get(@PathVariable("hr_attendance_id") Integer hr_attendance_id) {
        Hr_attendance domain = hr_attendanceService.get(hr_attendance_id);
        Hr_attendanceDTO dto = hr_attendanceMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }







    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Hr_attendance" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/hr_attendances/fetchdefault")
	public ResponseEntity<List<Hr_attendanceDTO>> fetchDefault(Hr_attendanceSearchContext context) {
        Page<Hr_attendance> domains = hr_attendanceService.searchDefault(context) ;
        List<Hr_attendanceDTO> list = hr_attendanceMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Hr_attendance" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/hr_attendances/searchdefault")
	public ResponseEntity<Page<Hr_attendanceDTO>> searchDefault(Hr_attendanceSearchContext context) {
        Page<Hr_attendance> domains = hr_attendanceService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(hr_attendanceMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Hr_attendance getEntity(){
        return new Hr_attendance();
    }

}
