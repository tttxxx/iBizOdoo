package cn.ibizlab.odoo.odoo_hr.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_hr.dto.*;
import cn.ibizlab.odoo.odoo_hr.mapping.*;
import cn.ibizlab.odoo.core.odoo_hr.domain.Hr_employee_category;
import cn.ibizlab.odoo.core.odoo_hr.service.IHr_employee_categoryService;
import cn.ibizlab.odoo.core.odoo_hr.filter.Hr_employee_categorySearchContext;




@Slf4j
@Api(tags = {"Hr_employee_category" })
@RestController("odoo_hr-hr_employee_category")
@RequestMapping("")
public class Hr_employee_categoryResource {

    @Autowired
    private IHr_employee_categoryService hr_employee_categoryService;

    @Autowired
    @Lazy
    private Hr_employee_categoryMapping hr_employee_categoryMapping;







    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Hr_employee_category" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/hr_employee_categories")

    public ResponseEntity<Hr_employee_categoryDTO> create(@RequestBody Hr_employee_categoryDTO hr_employee_categorydto) {
        Hr_employee_category domain = hr_employee_categoryMapping.toDomain(hr_employee_categorydto);
		hr_employee_categoryService.create(domain);
        Hr_employee_categoryDTO dto = hr_employee_categoryMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Hr_employee_category" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/hr_employee_categories/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Hr_employee_categoryDTO> hr_employee_categorydtos) {
        hr_employee_categoryService.createBatch(hr_employee_categoryMapping.toDomain(hr_employee_categorydtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }







    @PreAuthorize("hasPermission('Remove',{#hr_employee_category_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Hr_employee_category" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/hr_employee_categories/{hr_employee_category_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("hr_employee_category_id") Integer hr_employee_category_id) {
         return ResponseEntity.status(HttpStatus.OK).body(hr_employee_categoryService.remove(hr_employee_category_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Hr_employee_category" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/hr_employee_categories/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        hr_employee_categoryService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission(#hr_employee_category_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Hr_employee_category" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/hr_employee_categories/{hr_employee_category_id}")

    public ResponseEntity<Hr_employee_categoryDTO> update(@PathVariable("hr_employee_category_id") Integer hr_employee_category_id, @RequestBody Hr_employee_categoryDTO hr_employee_categorydto) {
		Hr_employee_category domain = hr_employee_categoryMapping.toDomain(hr_employee_categorydto);
        domain.setId(hr_employee_category_id);
		hr_employee_categoryService.update(domain);
		Hr_employee_categoryDTO dto = hr_employee_categoryMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#hr_employee_category_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Hr_employee_category" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/hr_employee_categories/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Hr_employee_categoryDTO> hr_employee_categorydtos) {
        hr_employee_categoryService.updateBatch(hr_employee_categoryMapping.toDomain(hr_employee_categorydtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission(#hr_employee_category_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Hr_employee_category" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/hr_employee_categories/{hr_employee_category_id}")
    public ResponseEntity<Hr_employee_categoryDTO> get(@PathVariable("hr_employee_category_id") Integer hr_employee_category_id) {
        Hr_employee_category domain = hr_employee_categoryService.get(hr_employee_category_id);
        Hr_employee_categoryDTO dto = hr_employee_categoryMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }




    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Hr_employee_category" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/hr_employee_categories/fetchdefault")
	public ResponseEntity<List<Hr_employee_categoryDTO>> fetchDefault(Hr_employee_categorySearchContext context) {
        Page<Hr_employee_category> domains = hr_employee_categoryService.searchDefault(context) ;
        List<Hr_employee_categoryDTO> list = hr_employee_categoryMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Hr_employee_category" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/hr_employee_categories/searchdefault")
	public ResponseEntity<Page<Hr_employee_categoryDTO>> searchDefault(Hr_employee_categorySearchContext context) {
        Page<Hr_employee_category> domains = hr_employee_categoryService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(hr_employee_categoryMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Hr_employee_category getEntity(){
        return new Hr_employee_category();
    }

}
