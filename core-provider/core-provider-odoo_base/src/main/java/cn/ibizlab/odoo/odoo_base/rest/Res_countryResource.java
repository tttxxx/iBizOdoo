package cn.ibizlab.odoo.odoo_base.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_base.dto.*;
import cn.ibizlab.odoo.odoo_base.mapping.*;
import cn.ibizlab.odoo.core.odoo_base.domain.Res_country;
import cn.ibizlab.odoo.core.odoo_base.service.IRes_countryService;
import cn.ibizlab.odoo.core.odoo_base.filter.Res_countrySearchContext;




@Slf4j
@Api(tags = {"Res_country" })
@RestController("odoo_base-res_country")
@RequestMapping("")
public class Res_countryResource {

    @Autowired
    private IRes_countryService res_countryService;

    @Autowired
    @Lazy
    private Res_countryMapping res_countryMapping;




    @PreAuthorize("hasPermission(#res_country_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Res_country" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/res_countries/{res_country_id}")

    public ResponseEntity<Res_countryDTO> update(@PathVariable("res_country_id") Integer res_country_id, @RequestBody Res_countryDTO res_countrydto) {
		Res_country domain = res_countryMapping.toDomain(res_countrydto);
        domain.setId(res_country_id);
		res_countryService.update(domain);
		Res_countryDTO dto = res_countryMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#res_country_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Res_country" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/res_countries/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Res_countryDTO> res_countrydtos) {
        res_countryService.updateBatch(res_countryMapping.toDomain(res_countrydtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission(#res_country_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Res_country" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/res_countries/{res_country_id}")
    public ResponseEntity<Res_countryDTO> get(@PathVariable("res_country_id") Integer res_country_id) {
        Res_country domain = res_countryService.get(res_country_id);
        Res_countryDTO dto = res_countryMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }




    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Res_country" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/res_countries")

    public ResponseEntity<Res_countryDTO> create(@RequestBody Res_countryDTO res_countrydto) {
        Res_country domain = res_countryMapping.toDomain(res_countrydto);
		res_countryService.create(domain);
        Res_countryDTO dto = res_countryMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Res_country" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/res_countries/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Res_countryDTO> res_countrydtos) {
        res_countryService.createBatch(res_countryMapping.toDomain(res_countrydtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }







    @PreAuthorize("hasPermission('Remove',{#res_country_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Res_country" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/res_countries/{res_country_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("res_country_id") Integer res_country_id) {
         return ResponseEntity.status(HttpStatus.OK).body(res_countryService.remove(res_country_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Res_country" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/res_countries/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        res_countryService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }







    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Res_country" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/res_countries/fetchdefault")
	public ResponseEntity<List<Res_countryDTO>> fetchDefault(Res_countrySearchContext context) {
        Page<Res_country> domains = res_countryService.searchDefault(context) ;
        List<Res_countryDTO> list = res_countryMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Res_country" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/res_countries/searchdefault")
	public ResponseEntity<Page<Res_countryDTO>> searchDefault(Res_countrySearchContext context) {
        Page<Res_country> domains = res_countryService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(res_countryMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Res_country getEntity(){
        return new Res_country();
    }

}
