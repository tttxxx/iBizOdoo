package cn.ibizlab.odoo.odoo_base.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_base.dto.*;
import cn.ibizlab.odoo.odoo_base.mapping.*;
import cn.ibizlab.odoo.core.odoo_base.domain.Res_groups;
import cn.ibizlab.odoo.core.odoo_base.service.IRes_groupsService;
import cn.ibizlab.odoo.core.odoo_base.filter.Res_groupsSearchContext;




@Slf4j
@Api(tags = {"Res_groups" })
@RestController("odoo_base-res_groups")
@RequestMapping("")
public class Res_groupsResource {

    @Autowired
    private IRes_groupsService res_groupsService;

    @Autowired
    @Lazy
    private Res_groupsMapping res_groupsMapping;




    @PreAuthorize("hasPermission(#res_groups_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Res_groups" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/res_groups/{res_groups_id}")
    public ResponseEntity<Res_groupsDTO> get(@PathVariable("res_groups_id") Integer res_groups_id) {
        Res_groups domain = res_groupsService.get(res_groups_id);
        Res_groupsDTO dto = res_groupsMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }







    @PreAuthorize("hasPermission(#res_groups_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Res_groups" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/res_groups/{res_groups_id}")

    public ResponseEntity<Res_groupsDTO> update(@PathVariable("res_groups_id") Integer res_groups_id, @RequestBody Res_groupsDTO res_groupsdto) {
		Res_groups domain = res_groupsMapping.toDomain(res_groupsdto);
        domain.setId(res_groups_id);
		res_groupsService.update(domain);
		Res_groupsDTO dto = res_groupsMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#res_groups_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Res_groups" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/res_groups/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Res_groupsDTO> res_groupsdtos) {
        res_groupsService.updateBatch(res_groupsMapping.toDomain(res_groupsdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }







    @PreAuthorize("hasPermission('Remove',{#res_groups_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Res_groups" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/res_groups/{res_groups_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("res_groups_id") Integer res_groups_id) {
         return ResponseEntity.status(HttpStatus.OK).body(res_groupsService.remove(res_groups_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Res_groups" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/res_groups/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        res_groupsService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }







    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Res_groups" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/res_groups")

    public ResponseEntity<Res_groupsDTO> create(@RequestBody Res_groupsDTO res_groupsdto) {
        Res_groups domain = res_groupsMapping.toDomain(res_groupsdto);
		res_groupsService.create(domain);
        Res_groupsDTO dto = res_groupsMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Res_groups" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/res_groups/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Res_groupsDTO> res_groupsdtos) {
        res_groupsService.createBatch(res_groupsMapping.toDomain(res_groupsdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Res_groups" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/res_groups/fetchdefault")
	public ResponseEntity<List<Res_groupsDTO>> fetchDefault(Res_groupsSearchContext context) {
        Page<Res_groups> domains = res_groupsService.searchDefault(context) ;
        List<Res_groupsDTO> list = res_groupsMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Res_groups" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/res_groups/searchdefault")
	public ResponseEntity<Page<Res_groupsDTO>> searchDefault(Res_groupsSearchContext context) {
        Page<Res_groups> domains = res_groupsService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(res_groupsMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Res_groups getEntity(){
        return new Res_groups();
    }

}
