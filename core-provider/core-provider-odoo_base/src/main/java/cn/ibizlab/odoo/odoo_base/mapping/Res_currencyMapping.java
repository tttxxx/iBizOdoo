package cn.ibizlab.odoo.odoo_base.mapping;

import org.mapstruct.*;
import cn.ibizlab.odoo.core.odoo_base.domain.Res_currency;
import cn.ibizlab.odoo.odoo_base.dto.Res_currencyDTO;
import cn.ibizlab.odoo.util.domain.MappingBase;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring", uses = {},
    nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
    nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface Res_currencyMapping extends MappingBase<Res_currencyDTO, Res_currency> {


}

