package cn.ibizlab.odoo.odoo_base.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_base.dto.*;
import cn.ibizlab.odoo.odoo_base.mapping.*;
import cn.ibizlab.odoo.core.odoo_base.domain.Res_config;
import cn.ibizlab.odoo.core.odoo_base.service.IRes_configService;
import cn.ibizlab.odoo.core.odoo_base.filter.Res_configSearchContext;




@Slf4j
@Api(tags = {"Res_config" })
@RestController("odoo_base-res_config")
@RequestMapping("")
public class Res_configResource {

    @Autowired
    private IRes_configService res_configService;

    @Autowired
    @Lazy
    private Res_configMapping res_configMapping;




    @PreAuthorize("hasPermission(#res_config_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Res_config" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/res_configs/{res_config_id}")
    public ResponseEntity<Res_configDTO> get(@PathVariable("res_config_id") Integer res_config_id) {
        Res_config domain = res_configService.get(res_config_id);
        Res_configDTO dto = res_configMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }




    @PreAuthorize("hasPermission('Remove',{#res_config_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Res_config" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/res_configs/{res_config_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("res_config_id") Integer res_config_id) {
         return ResponseEntity.status(HttpStatus.OK).body(res_configService.remove(res_config_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Res_config" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/res_configs/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        res_configService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }







    @PreAuthorize("hasPermission(#res_config_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Res_config" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/res_configs/{res_config_id}")

    public ResponseEntity<Res_configDTO> update(@PathVariable("res_config_id") Integer res_config_id, @RequestBody Res_configDTO res_configdto) {
		Res_config domain = res_configMapping.toDomain(res_configdto);
        domain.setId(res_config_id);
		res_configService.update(domain);
		Res_configDTO dto = res_configMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#res_config_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Res_config" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/res_configs/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Res_configDTO> res_configdtos) {
        res_configService.updateBatch(res_configMapping.toDomain(res_configdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }







    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Res_config" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/res_configs")

    public ResponseEntity<Res_configDTO> create(@RequestBody Res_configDTO res_configdto) {
        Res_config domain = res_configMapping.toDomain(res_configdto);
		res_configService.create(domain);
        Res_configDTO dto = res_configMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Res_config" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/res_configs/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Res_configDTO> res_configdtos) {
        res_configService.createBatch(res_configMapping.toDomain(res_configdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Res_config" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/res_configs/fetchdefault")
	public ResponseEntity<List<Res_configDTO>> fetchDefault(Res_configSearchContext context) {
        Page<Res_config> domains = res_configService.searchDefault(context) ;
        List<Res_configDTO> list = res_configMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Res_config" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/res_configs/searchdefault")
	public ResponseEntity<Page<Res_configDTO>> searchDefault(Res_configSearchContext context) {
        Page<Res_config> domains = res_configService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(res_configMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Res_config getEntity(){
        return new Res_config();
    }

}
