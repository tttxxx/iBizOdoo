package cn.ibizlab.odoo.odoo_base.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_base.dto.*;
import cn.ibizlab.odoo.odoo_base.mapping.*;
import cn.ibizlab.odoo.core.odoo_base.domain.Res_bank;
import cn.ibizlab.odoo.core.odoo_base.service.IRes_bankService;
import cn.ibizlab.odoo.core.odoo_base.filter.Res_bankSearchContext;




@Slf4j
@Api(tags = {"Res_bank" })
@RestController("odoo_base-res_bank")
@RequestMapping("")
public class Res_bankResource {

    @Autowired
    private IRes_bankService res_bankService;

    @Autowired
    @Lazy
    private Res_bankMapping res_bankMapping;




    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Res_bank" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/res_banks")

    public ResponseEntity<Res_bankDTO> create(@RequestBody Res_bankDTO res_bankdto) {
        Res_bank domain = res_bankMapping.toDomain(res_bankdto);
		res_bankService.create(domain);
        Res_bankDTO dto = res_bankMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Res_bank" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/res_banks/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Res_bankDTO> res_bankdtos) {
        res_bankService.createBatch(res_bankMapping.toDomain(res_bankdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission(#res_bank_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Res_bank" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/res_banks/{res_bank_id}")
    public ResponseEntity<Res_bankDTO> get(@PathVariable("res_bank_id") Integer res_bank_id) {
        Res_bank domain = res_bankService.get(res_bank_id);
        Res_bankDTO dto = res_bankMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }




    @PreAuthorize("hasPermission('Remove',{#res_bank_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Res_bank" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/res_banks/{res_bank_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("res_bank_id") Integer res_bank_id) {
         return ResponseEntity.status(HttpStatus.OK).body(res_bankService.remove(res_bank_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Res_bank" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/res_banks/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        res_bankService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }










    @PreAuthorize("hasPermission(#res_bank_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Res_bank" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/res_banks/{res_bank_id}")

    public ResponseEntity<Res_bankDTO> update(@PathVariable("res_bank_id") Integer res_bank_id, @RequestBody Res_bankDTO res_bankdto) {
		Res_bank domain = res_bankMapping.toDomain(res_bankdto);
        domain.setId(res_bank_id);
		res_bankService.update(domain);
		Res_bankDTO dto = res_bankMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#res_bank_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Res_bank" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/res_banks/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Res_bankDTO> res_bankdtos) {
        res_bankService.updateBatch(res_bankMapping.toDomain(res_bankdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Res_bank" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/res_banks/fetchdefault")
	public ResponseEntity<List<Res_bankDTO>> fetchDefault(Res_bankSearchContext context) {
        Page<Res_bank> domains = res_bankService.searchDefault(context) ;
        List<Res_bankDTO> list = res_bankMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Res_bank" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/res_banks/searchdefault")
	public ResponseEntity<Page<Res_bankDTO>> searchDefault(Res_bankSearchContext context) {
        Page<Res_bank> domains = res_bankService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(res_bankMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Res_bank getEntity(){
        return new Res_bank();
    }

}
