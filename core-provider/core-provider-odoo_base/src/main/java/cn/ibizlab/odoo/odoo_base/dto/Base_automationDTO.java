package cn.ibizlab.odoo.odoo_base.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;
import cn.ibizlab.odoo.util.domain.DTOBase;
import lombok.Data;

/**
 * 服务DTO对象[Base_automationDTO]
 */
@Data
public class Base_automationDTO extends DTOBase implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [BINDING_MODEL_ID]
     *
     */
    @JSONField(name = "binding_model_id")
    @JsonProperty("binding_model_id")
    private Integer bindingModelId;

    /**
     * 属性 [SEQUENCE]
     *
     */
    @JSONField(name = "sequence")
    @JsonProperty("sequence")
    private Integer sequence;

    /**
     * 属性 [FILTER_PRE_DOMAIN]
     *
     */
    @JSONField(name = "filter_pre_domain")
    @JsonProperty("filter_pre_domain")
    private String filterPreDomain;

    /**
     * 属性 [ON_CHANGE_FIELDS]
     *
     */
    @JSONField(name = "on_change_fields")
    @JsonProperty("on_change_fields")
    private String onChangeFields;

    /**
     * 属性 [ACTIVITY_SUMMARY]
     *
     */
    @JSONField(name = "activity_summary")
    @JsonProperty("activity_summary")
    private String activitySummary;

    /**
     * 属性 [ACTIVITY_USER_FIELD_NAME]
     *
     */
    @JSONField(name = "activity_user_field_name")
    @JsonProperty("activity_user_field_name")
    private String activityUserFieldName;

    /**
     * 属性 [WEBSITE_PUBLISHED]
     *
     */
    @JSONField(name = "website_published")
    @JsonProperty("website_published")
    private String websitePublished;

    /**
     * 属性 [USAGE]
     *
     */
    @JSONField(name = "usage")
    @JsonProperty("usage")
    private String usage;

    /**
     * 属性 [CRUD_MODEL_NAME]
     *
     */
    @JSONField(name = "crud_model_name")
    @JsonProperty("crud_model_name")
    private String crudModelName;

    /**
     * 属性 [TRG_DATE_RANGE_TYPE]
     *
     */
    @JSONField(name = "trg_date_range_type")
    @JsonProperty("trg_date_range_type")
    private String trgDateRangeType;

    /**
     * 属性 [ID]
     *
     */
    @JSONField(name = "id")
    @JsonProperty("id")
    private Integer id;

    /**
     * 属性 [FIELDS_LINES]
     *
     */
    @JSONField(name = "fields_lines")
    @JsonProperty("fields_lines")
    private String fieldsLines;

    /**
     * 属性 [ACTIVITY_NOTE]
     *
     */
    @JSONField(name = "activity_note")
    @JsonProperty("activity_note")
    private String activityNote;

    /**
     * 属性 [ACTIVE]
     *
     */
    @JSONField(name = "active")
    @JsonProperty("active")
    private String active;

    /**
     * 属性 [ACTIVITY_TYPE_ID]
     *
     */
    @JSONField(name = "activity_type_id")
    @JsonProperty("activity_type_id")
    private Integer activityTypeId;

    /**
     * 属性 [CODE]
     *
     */
    @JSONField(name = "code")
    @JsonProperty("code")
    private String code;

    /**
     * 属性 [ACTIVITY_USER_TYPE]
     *
     */
    @JSONField(name = "activity_user_type")
    @JsonProperty("activity_user_type")
    private String activityUserType;

    /**
     * 属性 [WEBSITE_PATH]
     *
     */
    @JSONField(name = "website_path")
    @JsonProperty("website_path")
    private String websitePath;

    /**
     * 属性 [PARTNER_IDS]
     *
     */
    @JSONField(name = "partner_ids")
    @JsonProperty("partner_ids")
    private String partnerIds;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;

    /**
     * 属性 [TRG_DATE_ID]
     *
     */
    @JSONField(name = "trg_date_id")
    @JsonProperty("trg_date_id")
    private Integer trgDateId;

    /**
     * 属性 [XML_ID]
     *
     */
    @JSONField(name = "xml_id")
    @JsonProperty("xml_id")
    private String xmlId;

    /**
     * 属性 [LAST_RUN]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "last_run" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("last_run")
    private Timestamp lastRun;

    /**
     * 属性 [TEMPLATE_ID]
     *
     */
    @JSONField(name = "template_id")
    @JsonProperty("template_id")
    private Integer templateId;

    /**
     * 属性 [BINDING_TYPE]
     *
     */
    @JSONField(name = "binding_type")
    @JsonProperty("binding_type")
    private String bindingType;

    /**
     * 属性 [NAME]
     *
     */
    @JSONField(name = "name")
    @JsonProperty("name")
    private String name;

    /**
     * 属性 [MODEL_NAME]
     *
     */
    @JSONField(name = "model_name")
    @JsonProperty("model_name")
    private String modelName;

    /**
     * 属性 [STATE]
     *
     */
    @JSONField(name = "state")
    @JsonProperty("state")
    private String state;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    private String displayName;

    /**
     * 属性 [CHANNEL_IDS]
     *
     */
    @JSONField(name = "channel_ids")
    @JsonProperty("channel_ids")
    private String channelIds;

    /**
     * 属性 [MODEL_ID]
     *
     */
    @JSONField(name = "model_id")
    @JsonProperty("model_id")
    private Integer modelId;

    /**
     * 属性 [HELP]
     *
     */
    @JSONField(name = "help")
    @JsonProperty("help")
    private String help;

    /**
     * 属性 [WEBSITE_URL]
     *
     */
    @JSONField(name = "website_url")
    @JsonProperty("website_url")
    private String websiteUrl;

    /**
     * 属性 [ACTIVITY_USER_ID]
     *
     */
    @JSONField(name = "activity_user_id")
    @JsonProperty("activity_user_id")
    private Integer activityUserId;

    /**
     * 属性 [ACTIVITY_DATE_DEADLINE_RANGE]
     *
     */
    @JSONField(name = "activity_date_deadline_range")
    @JsonProperty("activity_date_deadline_range")
    private Integer activityDateDeadlineRange;

    /**
     * 属性 [TRG_DATE_RANGE]
     *
     */
    @JSONField(name = "trg_date_range")
    @JsonProperty("trg_date_range")
    private Integer trgDateRange;

    /**
     * 属性 [TYPE]
     *
     */
    @JSONField(name = "type")
    @JsonProperty("type")
    private String type;

    /**
     * 属性 [FILTER_DOMAIN]
     *
     */
    @JSONField(name = "filter_domain")
    @JsonProperty("filter_domain")
    private String filterDomain;

    /**
     * 属性 [ACTIVITY_DATE_DEADLINE_RANGE_TYPE]
     *
     */
    @JSONField(name = "activity_date_deadline_range_type")
    @JsonProperty("activity_date_deadline_range_type")
    private String activityDateDeadlineRangeType;

    /**
     * 属性 [WRITE_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;

    /**
     * 属性 [CHILD_IDS]
     *
     */
    @JSONField(name = "child_ids")
    @JsonProperty("child_ids")
    private String childIds;

    /**
     * 属性 [LINK_FIELD_ID]
     *
     */
    @JSONField(name = "link_field_id")
    @JsonProperty("link_field_id")
    private Integer linkFieldId;

    /**
     * 属性 [CREATE_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;

    /**
     * 属性 [TRIGGER]
     *
     */
    @JSONField(name = "trigger")
    @JsonProperty("trigger")
    private String trigger;

    /**
     * 属性 [ACTION_SERVER_ID]
     *
     */
    @JSONField(name = "action_server_id")
    @JsonProperty("action_server_id")
    private Integer actionServerId;

    /**
     * 属性 [CRUD_MODEL_ID]
     *
     */
    @JSONField(name = "crud_model_id")
    @JsonProperty("crud_model_id")
    private Integer crudModelId;

    /**
     * 属性 [CREATE_UID_TEXT]
     *
     */
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    private String createUidText;

    /**
     * 属性 [WRITE_UID_TEXT]
     *
     */
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    private String writeUidText;

    /**
     * 属性 [TRG_DATE_CALENDAR_ID_TEXT]
     *
     */
    @JSONField(name = "trg_date_calendar_id_text")
    @JsonProperty("trg_date_calendar_id_text")
    private String trgDateCalendarIdText;

    /**
     * 属性 [WRITE_UID]
     *
     */
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    private Integer writeUid;

    /**
     * 属性 [TRG_DATE_CALENDAR_ID]
     *
     */
    @JSONField(name = "trg_date_calendar_id")
    @JsonProperty("trg_date_calendar_id")
    private Integer trgDateCalendarId;

    /**
     * 属性 [CREATE_UID]
     *
     */
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    private Integer createUid;


    /**
     * 设置 [FILTER_PRE_DOMAIN]
     */
    public void setFilterPreDomain(String  filterPreDomain){
        this.filterPreDomain = filterPreDomain ;
        this.modify("filter_pre_domain",filterPreDomain);
    }

    /**
     * 设置 [ON_CHANGE_FIELDS]
     */
    public void setOnChangeFields(String  onChangeFields){
        this.onChangeFields = onChangeFields ;
        this.modify("on_change_fields",onChangeFields);
    }

    /**
     * 设置 [TRG_DATE_RANGE_TYPE]
     */
    public void setTrgDateRangeType(String  trgDateRangeType){
        this.trgDateRangeType = trgDateRangeType ;
        this.modify("trg_date_range_type",trgDateRangeType);
    }

    /**
     * 设置 [ACTIVE]
     */
    public void setActive(String  active){
        this.active = active ;
        this.modify("active",active);
    }

    /**
     * 设置 [TRG_DATE_ID]
     */
    public void setTrgDateId(Integer  trgDateId){
        this.trgDateId = trgDateId ;
        this.modify("trg_date_id",trgDateId);
    }

    /**
     * 设置 [LAST_RUN]
     */
    public void setLastRun(Timestamp  lastRun){
        this.lastRun = lastRun ;
        this.modify("last_run",lastRun);
    }

    /**
     * 设置 [TRG_DATE_RANGE]
     */
    public void setTrgDateRange(Integer  trgDateRange){
        this.trgDateRange = trgDateRange ;
        this.modify("trg_date_range",trgDateRange);
    }

    /**
     * 设置 [FILTER_DOMAIN]
     */
    public void setFilterDomain(String  filterDomain){
        this.filterDomain = filterDomain ;
        this.modify("filter_domain",filterDomain);
    }

    /**
     * 设置 [TRIGGER]
     */
    public void setTrigger(String  trigger){
        this.trigger = trigger ;
        this.modify("trigger",trigger);
    }

    /**
     * 设置 [ACTION_SERVER_ID]
     */
    public void setActionServerId(Integer  actionServerId){
        this.actionServerId = actionServerId ;
        this.modify("action_server_id",actionServerId);
    }

    /**
     * 设置 [TRG_DATE_CALENDAR_ID]
     */
    public void setTrgDateCalendarId(Integer  trgDateCalendarId){
        this.trgDateCalendarId = trgDateCalendarId ;
        this.modify("trg_date_calendar_id",trgDateCalendarId);
    }


}

