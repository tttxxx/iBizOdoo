package cn.ibizlab.odoo.odoo_base.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_base.dto.*;
import cn.ibizlab.odoo.odoo_base.mapping.*;
import cn.ibizlab.odoo.core.odoo_base.domain.Res_company;
import cn.ibizlab.odoo.core.odoo_base.service.IRes_companyService;
import cn.ibizlab.odoo.core.odoo_base.filter.Res_companySearchContext;




@Slf4j
@Api(tags = {"Res_company" })
@RestController("odoo_base-res_company")
@RequestMapping("")
public class Res_companyResource {

    @Autowired
    private IRes_companyService res_companyService;

    @Autowired
    @Lazy
    private Res_companyMapping res_companyMapping;




    @PreAuthorize("hasPermission(#res_company_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Res_company" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/res_companies/{res_company_id}")
    public ResponseEntity<Res_companyDTO> get(@PathVariable("res_company_id") Integer res_company_id) {
        Res_company domain = res_companyService.get(res_company_id);
        Res_companyDTO dto = res_companyMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }




    @PreAuthorize("hasPermission('Remove',{#res_company_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Res_company" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/res_companies/{res_company_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("res_company_id") Integer res_company_id) {
         return ResponseEntity.status(HttpStatus.OK).body(res_companyService.remove(res_company_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Res_company" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/res_companies/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        res_companyService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }










    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Res_company" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/res_companies")

    public ResponseEntity<Res_companyDTO> create(@RequestBody Res_companyDTO res_companydto) {
        Res_company domain = res_companyMapping.toDomain(res_companydto);
		res_companyService.create(domain);
        Res_companyDTO dto = res_companyMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Res_company" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/res_companies/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Res_companyDTO> res_companydtos) {
        res_companyService.createBatch(res_companyMapping.toDomain(res_companydtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission(#res_company_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Res_company" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/res_companies/{res_company_id}")

    public ResponseEntity<Res_companyDTO> update(@PathVariable("res_company_id") Integer res_company_id, @RequestBody Res_companyDTO res_companydto) {
		Res_company domain = res_companyMapping.toDomain(res_companydto);
        domain.setId(res_company_id);
		res_companyService.update(domain);
		Res_companyDTO dto = res_companyMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#res_company_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Res_company" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/res_companies/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Res_companyDTO> res_companydtos) {
        res_companyService.updateBatch(res_companyMapping.toDomain(res_companydtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Res_company" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/res_companies/fetchdefault")
	public ResponseEntity<List<Res_companyDTO>> fetchDefault(Res_companySearchContext context) {
        Page<Res_company> domains = res_companyService.searchDefault(context) ;
        List<Res_companyDTO> list = res_companyMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Res_company" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/res_companies/searchdefault")
	public ResponseEntity<Page<Res_companyDTO>> searchDefault(Res_companySearchContext context) {
        Page<Res_company> domains = res_companyService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(res_companyMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Res_company getEntity(){
        return new Res_company();
    }

}
