package cn.ibizlab.odoo.odoo_base.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_base.dto.*;
import cn.ibizlab.odoo.odoo_base.mapping.*;
import cn.ibizlab.odoo.core.odoo_base.domain.Base_automation;
import cn.ibizlab.odoo.core.odoo_base.service.IBase_automationService;
import cn.ibizlab.odoo.core.odoo_base.filter.Base_automationSearchContext;




@Slf4j
@Api(tags = {"Base_automation" })
@RestController("odoo_base-base_automation")
@RequestMapping("")
public class Base_automationResource {

    @Autowired
    private IBase_automationService base_automationService;

    @Autowired
    @Lazy
    private Base_automationMapping base_automationMapping;




    @PreAuthorize("hasPermission(#base_automation_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Base_automation" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/base_automations/{base_automation_id}")
    public ResponseEntity<Base_automationDTO> get(@PathVariable("base_automation_id") Integer base_automation_id) {
        Base_automation domain = base_automationService.get(base_automation_id);
        Base_automationDTO dto = base_automationMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }




    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Base_automation" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/base_automations")

    public ResponseEntity<Base_automationDTO> create(@RequestBody Base_automationDTO base_automationdto) {
        Base_automation domain = base_automationMapping.toDomain(base_automationdto);
		base_automationService.create(domain);
        Base_automationDTO dto = base_automationMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Base_automation" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/base_automations/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Base_automationDTO> base_automationdtos) {
        base_automationService.createBatch(base_automationMapping.toDomain(base_automationdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }










    @PreAuthorize("hasPermission(#base_automation_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Base_automation" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/base_automations/{base_automation_id}")

    public ResponseEntity<Base_automationDTO> update(@PathVariable("base_automation_id") Integer base_automation_id, @RequestBody Base_automationDTO base_automationdto) {
		Base_automation domain = base_automationMapping.toDomain(base_automationdto);
        domain.setId(base_automation_id);
		base_automationService.update(domain);
		Base_automationDTO dto = base_automationMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#base_automation_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Base_automation" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/base_automations/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Base_automationDTO> base_automationdtos) {
        base_automationService.updateBatch(base_automationMapping.toDomain(base_automationdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission('Remove',{#base_automation_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Base_automation" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/base_automations/{base_automation_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("base_automation_id") Integer base_automation_id) {
         return ResponseEntity.status(HttpStatus.OK).body(base_automationService.remove(base_automation_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Base_automation" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/base_automations/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        base_automationService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Base_automation" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/base_automations/fetchdefault")
	public ResponseEntity<List<Base_automationDTO>> fetchDefault(Base_automationSearchContext context) {
        Page<Base_automation> domains = base_automationService.searchDefault(context) ;
        List<Base_automationDTO> list = base_automationMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Base_automation" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/base_automations/searchdefault")
	public ResponseEntity<Page<Base_automationDTO>> searchDefault(Base_automationSearchContext context) {
        Page<Base_automation> domains = base_automationService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(base_automationMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Base_automation getEntity(){
        return new Base_automation();
    }

}
