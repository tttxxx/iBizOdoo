package cn.ibizlab.odoo.odoo_base.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_base.dto.*;
import cn.ibizlab.odoo.odoo_base.mapping.*;
import cn.ibizlab.odoo.core.odoo_base.domain.Res_country_state;
import cn.ibizlab.odoo.core.odoo_base.service.IRes_country_stateService;
import cn.ibizlab.odoo.core.odoo_base.filter.Res_country_stateSearchContext;




@Slf4j
@Api(tags = {"Res_country_state" })
@RestController("odoo_base-res_country_state")
@RequestMapping("")
public class Res_country_stateResource {

    @Autowired
    private IRes_country_stateService res_country_stateService;

    @Autowired
    @Lazy
    private Res_country_stateMapping res_country_stateMapping;







    @PreAuthorize("hasPermission(#res_country_state_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Res_country_state" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/res_country_states/{res_country_state_id}")
    public ResponseEntity<Res_country_stateDTO> get(@PathVariable("res_country_state_id") Integer res_country_state_id) {
        Res_country_state domain = res_country_stateService.get(res_country_state_id);
        Res_country_stateDTO dto = res_country_stateMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }




    @PreAuthorize("hasPermission(#res_country_state_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Res_country_state" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/res_country_states/{res_country_state_id}")

    public ResponseEntity<Res_country_stateDTO> update(@PathVariable("res_country_state_id") Integer res_country_state_id, @RequestBody Res_country_stateDTO res_country_statedto) {
		Res_country_state domain = res_country_stateMapping.toDomain(res_country_statedto);
        domain.setId(res_country_state_id);
		res_country_stateService.update(domain);
		Res_country_stateDTO dto = res_country_stateMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#res_country_state_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Res_country_state" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/res_country_states/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Res_country_stateDTO> res_country_statedtos) {
        res_country_stateService.updateBatch(res_country_stateMapping.toDomain(res_country_statedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }







    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Res_country_state" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/res_country_states")

    public ResponseEntity<Res_country_stateDTO> create(@RequestBody Res_country_stateDTO res_country_statedto) {
        Res_country_state domain = res_country_stateMapping.toDomain(res_country_statedto);
		res_country_stateService.create(domain);
        Res_country_stateDTO dto = res_country_stateMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Res_country_state" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/res_country_states/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Res_country_stateDTO> res_country_statedtos) {
        res_country_stateService.createBatch(res_country_stateMapping.toDomain(res_country_statedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission('Remove',{#res_country_state_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Res_country_state" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/res_country_states/{res_country_state_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("res_country_state_id") Integer res_country_state_id) {
         return ResponseEntity.status(HttpStatus.OK).body(res_country_stateService.remove(res_country_state_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Res_country_state" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/res_country_states/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        res_country_stateService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Res_country_state" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/res_country_states/fetchdefault")
	public ResponseEntity<List<Res_country_stateDTO>> fetchDefault(Res_country_stateSearchContext context) {
        Page<Res_country_state> domains = res_country_stateService.searchDefault(context) ;
        List<Res_country_stateDTO> list = res_country_stateMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Res_country_state" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/res_country_states/searchdefault")
	public ResponseEntity<Page<Res_country_stateDTO>> searchDefault(Res_country_stateSearchContext context) {
        Page<Res_country_state> domains = res_country_stateService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(res_country_stateMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Res_country_state getEntity(){
        return new Res_country_state();
    }

}
