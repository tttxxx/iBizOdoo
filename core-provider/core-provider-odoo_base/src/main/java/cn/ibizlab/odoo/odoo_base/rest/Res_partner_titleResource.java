package cn.ibizlab.odoo.odoo_base.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_base.dto.*;
import cn.ibizlab.odoo.odoo_base.mapping.*;
import cn.ibizlab.odoo.core.odoo_base.domain.Res_partner_title;
import cn.ibizlab.odoo.core.odoo_base.service.IRes_partner_titleService;
import cn.ibizlab.odoo.core.odoo_base.filter.Res_partner_titleSearchContext;




@Slf4j
@Api(tags = {"Res_partner_title" })
@RestController("odoo_base-res_partner_title")
@RequestMapping("")
public class Res_partner_titleResource {

    @Autowired
    private IRes_partner_titleService res_partner_titleService;

    @Autowired
    @Lazy
    private Res_partner_titleMapping res_partner_titleMapping;










    @PreAuthorize("hasPermission('Remove',{#res_partner_title_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Res_partner_title" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/res_partner_titles/{res_partner_title_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("res_partner_title_id") Integer res_partner_title_id) {
         return ResponseEntity.status(HttpStatus.OK).body(res_partner_titleService.remove(res_partner_title_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Res_partner_title" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/res_partner_titles/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        res_partner_titleService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission(#res_partner_title_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Res_partner_title" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/res_partner_titles/{res_partner_title_id}")
    public ResponseEntity<Res_partner_titleDTO> get(@PathVariable("res_partner_title_id") Integer res_partner_title_id) {
        Res_partner_title domain = res_partner_titleService.get(res_partner_title_id);
        Res_partner_titleDTO dto = res_partner_titleMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }




    @PreAuthorize("hasPermission(#res_partner_title_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Res_partner_title" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/res_partner_titles/{res_partner_title_id}")

    public ResponseEntity<Res_partner_titleDTO> update(@PathVariable("res_partner_title_id") Integer res_partner_title_id, @RequestBody Res_partner_titleDTO res_partner_titledto) {
		Res_partner_title domain = res_partner_titleMapping.toDomain(res_partner_titledto);
        domain.setId(res_partner_title_id);
		res_partner_titleService.update(domain);
		Res_partner_titleDTO dto = res_partner_titleMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#res_partner_title_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Res_partner_title" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/res_partner_titles/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Res_partner_titleDTO> res_partner_titledtos) {
        res_partner_titleService.updateBatch(res_partner_titleMapping.toDomain(res_partner_titledtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }







    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Res_partner_title" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/res_partner_titles")

    public ResponseEntity<Res_partner_titleDTO> create(@RequestBody Res_partner_titleDTO res_partner_titledto) {
        Res_partner_title domain = res_partner_titleMapping.toDomain(res_partner_titledto);
		res_partner_titleService.create(domain);
        Res_partner_titleDTO dto = res_partner_titleMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Res_partner_title" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/res_partner_titles/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Res_partner_titleDTO> res_partner_titledtos) {
        res_partner_titleService.createBatch(res_partner_titleMapping.toDomain(res_partner_titledtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Res_partner_title" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/res_partner_titles/fetchdefault")
	public ResponseEntity<List<Res_partner_titleDTO>> fetchDefault(Res_partner_titleSearchContext context) {
        Page<Res_partner_title> domains = res_partner_titleService.searchDefault(context) ;
        List<Res_partner_titleDTO> list = res_partner_titleMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Res_partner_title" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/res_partner_titles/searchdefault")
	public ResponseEntity<Page<Res_partner_titleDTO>> searchDefault(Res_partner_titleSearchContext context) {
        Page<Res_partner_title> domains = res_partner_titleService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(res_partner_titleMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Res_partner_title getEntity(){
        return new Res_partner_title();
    }

}
