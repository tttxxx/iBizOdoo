package cn.ibizlab.odoo.odoo_base.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_base.dto.*;
import cn.ibizlab.odoo.odoo_base.mapping.*;
import cn.ibizlab.odoo.core.odoo_base.domain.Res_partner_industry;
import cn.ibizlab.odoo.core.odoo_base.service.IRes_partner_industryService;
import cn.ibizlab.odoo.core.odoo_base.filter.Res_partner_industrySearchContext;




@Slf4j
@Api(tags = {"Res_partner_industry" })
@RestController("odoo_base-res_partner_industry")
@RequestMapping("")
public class Res_partner_industryResource {

    @Autowired
    private IRes_partner_industryService res_partner_industryService;

    @Autowired
    @Lazy
    private Res_partner_industryMapping res_partner_industryMapping;







    @PreAuthorize("hasPermission(#res_partner_industry_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Res_partner_industry" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/res_partner_industries/{res_partner_industry_id}")

    public ResponseEntity<Res_partner_industryDTO> update(@PathVariable("res_partner_industry_id") Integer res_partner_industry_id, @RequestBody Res_partner_industryDTO res_partner_industrydto) {
		Res_partner_industry domain = res_partner_industryMapping.toDomain(res_partner_industrydto);
        domain.setId(res_partner_industry_id);
		res_partner_industryService.update(domain);
		Res_partner_industryDTO dto = res_partner_industryMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#res_partner_industry_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Res_partner_industry" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/res_partner_industries/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Res_partner_industryDTO> res_partner_industrydtos) {
        res_partner_industryService.updateBatch(res_partner_industryMapping.toDomain(res_partner_industrydtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Res_partner_industry" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/res_partner_industries")

    public ResponseEntity<Res_partner_industryDTO> create(@RequestBody Res_partner_industryDTO res_partner_industrydto) {
        Res_partner_industry domain = res_partner_industryMapping.toDomain(res_partner_industrydto);
		res_partner_industryService.create(domain);
        Res_partner_industryDTO dto = res_partner_industryMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Res_partner_industry" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/res_partner_industries/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Res_partner_industryDTO> res_partner_industrydtos) {
        res_partner_industryService.createBatch(res_partner_industryMapping.toDomain(res_partner_industrydtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }







    @PreAuthorize("hasPermission('Remove',{#res_partner_industry_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Res_partner_industry" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/res_partner_industries/{res_partner_industry_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("res_partner_industry_id") Integer res_partner_industry_id) {
         return ResponseEntity.status(HttpStatus.OK).body(res_partner_industryService.remove(res_partner_industry_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Res_partner_industry" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/res_partner_industries/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        res_partner_industryService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission(#res_partner_industry_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Res_partner_industry" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/res_partner_industries/{res_partner_industry_id}")
    public ResponseEntity<Res_partner_industryDTO> get(@PathVariable("res_partner_industry_id") Integer res_partner_industry_id) {
        Res_partner_industry domain = res_partner_industryService.get(res_partner_industry_id);
        Res_partner_industryDTO dto = res_partner_industryMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }




    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Res_partner_industry" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/res_partner_industries/fetchdefault")
	public ResponseEntity<List<Res_partner_industryDTO>> fetchDefault(Res_partner_industrySearchContext context) {
        Page<Res_partner_industry> domains = res_partner_industryService.searchDefault(context) ;
        List<Res_partner_industryDTO> list = res_partner_industryMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Res_partner_industry" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/res_partner_industries/searchdefault")
	public ResponseEntity<Page<Res_partner_industryDTO>> searchDefault(Res_partner_industrySearchContext context) {
        Page<Res_partner_industry> domains = res_partner_industryService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(res_partner_industryMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Res_partner_industry getEntity(){
        return new Res_partner_industry();
    }

}
