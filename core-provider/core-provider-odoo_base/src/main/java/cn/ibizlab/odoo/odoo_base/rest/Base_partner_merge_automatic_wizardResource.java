package cn.ibizlab.odoo.odoo_base.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_base.dto.*;
import cn.ibizlab.odoo.odoo_base.mapping.*;
import cn.ibizlab.odoo.core.odoo_base.domain.Base_partner_merge_automatic_wizard;
import cn.ibizlab.odoo.core.odoo_base.service.IBase_partner_merge_automatic_wizardService;
import cn.ibizlab.odoo.core.odoo_base.filter.Base_partner_merge_automatic_wizardSearchContext;




@Slf4j
@Api(tags = {"Base_partner_merge_automatic_wizard" })
@RestController("odoo_base-base_partner_merge_automatic_wizard")
@RequestMapping("")
public class Base_partner_merge_automatic_wizardResource {

    @Autowired
    private IBase_partner_merge_automatic_wizardService base_partner_merge_automatic_wizardService;

    @Autowired
    @Lazy
    private Base_partner_merge_automatic_wizardMapping base_partner_merge_automatic_wizardMapping;







    @PreAuthorize("hasPermission(#base_partner_merge_automatic_wizard_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Base_partner_merge_automatic_wizard" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/base_partner_merge_automatic_wizards/{base_partner_merge_automatic_wizard_id}")

    public ResponseEntity<Base_partner_merge_automatic_wizardDTO> update(@PathVariable("base_partner_merge_automatic_wizard_id") Integer base_partner_merge_automatic_wizard_id, @RequestBody Base_partner_merge_automatic_wizardDTO base_partner_merge_automatic_wizarddto) {
		Base_partner_merge_automatic_wizard domain = base_partner_merge_automatic_wizardMapping.toDomain(base_partner_merge_automatic_wizarddto);
        domain.setId(base_partner_merge_automatic_wizard_id);
		base_partner_merge_automatic_wizardService.update(domain);
		Base_partner_merge_automatic_wizardDTO dto = base_partner_merge_automatic_wizardMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#base_partner_merge_automatic_wizard_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Base_partner_merge_automatic_wizard" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/base_partner_merge_automatic_wizards/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Base_partner_merge_automatic_wizardDTO> base_partner_merge_automatic_wizarddtos) {
        base_partner_merge_automatic_wizardService.updateBatch(base_partner_merge_automatic_wizardMapping.toDomain(base_partner_merge_automatic_wizarddtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }










    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Base_partner_merge_automatic_wizard" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/base_partner_merge_automatic_wizards")

    public ResponseEntity<Base_partner_merge_automatic_wizardDTO> create(@RequestBody Base_partner_merge_automatic_wizardDTO base_partner_merge_automatic_wizarddto) {
        Base_partner_merge_automatic_wizard domain = base_partner_merge_automatic_wizardMapping.toDomain(base_partner_merge_automatic_wizarddto);
		base_partner_merge_automatic_wizardService.create(domain);
        Base_partner_merge_automatic_wizardDTO dto = base_partner_merge_automatic_wizardMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Base_partner_merge_automatic_wizard" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/base_partner_merge_automatic_wizards/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Base_partner_merge_automatic_wizardDTO> base_partner_merge_automatic_wizarddtos) {
        base_partner_merge_automatic_wizardService.createBatch(base_partner_merge_automatic_wizardMapping.toDomain(base_partner_merge_automatic_wizarddtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission(#base_partner_merge_automatic_wizard_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Base_partner_merge_automatic_wizard" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/base_partner_merge_automatic_wizards/{base_partner_merge_automatic_wizard_id}")
    public ResponseEntity<Base_partner_merge_automatic_wizardDTO> get(@PathVariable("base_partner_merge_automatic_wizard_id") Integer base_partner_merge_automatic_wizard_id) {
        Base_partner_merge_automatic_wizard domain = base_partner_merge_automatic_wizardService.get(base_partner_merge_automatic_wizard_id);
        Base_partner_merge_automatic_wizardDTO dto = base_partner_merge_automatic_wizardMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }




    @PreAuthorize("hasPermission('Remove',{#base_partner_merge_automatic_wizard_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Base_partner_merge_automatic_wizard" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/base_partner_merge_automatic_wizards/{base_partner_merge_automatic_wizard_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("base_partner_merge_automatic_wizard_id") Integer base_partner_merge_automatic_wizard_id) {
         return ResponseEntity.status(HttpStatus.OK).body(base_partner_merge_automatic_wizardService.remove(base_partner_merge_automatic_wizard_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Base_partner_merge_automatic_wizard" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/base_partner_merge_automatic_wizards/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        base_partner_merge_automatic_wizardService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Base_partner_merge_automatic_wizard" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/base_partner_merge_automatic_wizards/fetchdefault")
	public ResponseEntity<List<Base_partner_merge_automatic_wizardDTO>> fetchDefault(Base_partner_merge_automatic_wizardSearchContext context) {
        Page<Base_partner_merge_automatic_wizard> domains = base_partner_merge_automatic_wizardService.searchDefault(context) ;
        List<Base_partner_merge_automatic_wizardDTO> list = base_partner_merge_automatic_wizardMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Base_partner_merge_automatic_wizard" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/base_partner_merge_automatic_wizards/searchdefault")
	public ResponseEntity<Page<Base_partner_merge_automatic_wizardDTO>> searchDefault(Base_partner_merge_automatic_wizardSearchContext context) {
        Page<Base_partner_merge_automatic_wizard> domains = base_partner_merge_automatic_wizardService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(base_partner_merge_automatic_wizardMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Base_partner_merge_automatic_wizard getEntity(){
        return new Base_partner_merge_automatic_wizard();
    }

}
