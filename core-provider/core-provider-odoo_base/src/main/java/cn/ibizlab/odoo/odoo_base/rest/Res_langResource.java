package cn.ibizlab.odoo.odoo_base.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_base.dto.*;
import cn.ibizlab.odoo.odoo_base.mapping.*;
import cn.ibizlab.odoo.core.odoo_base.domain.Res_lang;
import cn.ibizlab.odoo.core.odoo_base.service.IRes_langService;
import cn.ibizlab.odoo.core.odoo_base.filter.Res_langSearchContext;




@Slf4j
@Api(tags = {"Res_lang" })
@RestController("odoo_base-res_lang")
@RequestMapping("")
public class Res_langResource {

    @Autowired
    private IRes_langService res_langService;

    @Autowired
    @Lazy
    private Res_langMapping res_langMapping;







    @PreAuthorize("hasPermission(#res_lang_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Res_lang" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/res_langs/{res_lang_id}")

    public ResponseEntity<Res_langDTO> update(@PathVariable("res_lang_id") Integer res_lang_id, @RequestBody Res_langDTO res_langdto) {
		Res_lang domain = res_langMapping.toDomain(res_langdto);
        domain.setId(res_lang_id);
		res_langService.update(domain);
		Res_langDTO dto = res_langMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#res_lang_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Res_lang" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/res_langs/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Res_langDTO> res_langdtos) {
        res_langService.updateBatch(res_langMapping.toDomain(res_langdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Res_lang" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/res_langs")

    public ResponseEntity<Res_langDTO> create(@RequestBody Res_langDTO res_langdto) {
        Res_lang domain = res_langMapping.toDomain(res_langdto);
		res_langService.create(domain);
        Res_langDTO dto = res_langMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Res_lang" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/res_langs/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Res_langDTO> res_langdtos) {
        res_langService.createBatch(res_langMapping.toDomain(res_langdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission('Remove',{#res_lang_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Res_lang" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/res_langs/{res_lang_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("res_lang_id") Integer res_lang_id) {
         return ResponseEntity.status(HttpStatus.OK).body(res_langService.remove(res_lang_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Res_lang" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/res_langs/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        res_langService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }










    @PreAuthorize("hasPermission(#res_lang_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Res_lang" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/res_langs/{res_lang_id}")
    public ResponseEntity<Res_langDTO> get(@PathVariable("res_lang_id") Integer res_lang_id) {
        Res_lang domain = res_langService.get(res_lang_id);
        Res_langDTO dto = res_langMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Res_lang" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/res_langs/fetchdefault")
	public ResponseEntity<List<Res_langDTO>> fetchDefault(Res_langSearchContext context) {
        Page<Res_lang> domains = res_langService.searchDefault(context) ;
        List<Res_langDTO> list = res_langMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Res_lang" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/res_langs/searchdefault")
	public ResponseEntity<Page<Res_langDTO>> searchDefault(Res_langSearchContext context) {
        Page<Res_lang> domains = res_langService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(res_langMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Res_lang getEntity(){
        return new Res_lang();
    }

}
