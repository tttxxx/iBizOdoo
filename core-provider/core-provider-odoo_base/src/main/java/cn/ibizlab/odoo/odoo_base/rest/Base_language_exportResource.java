package cn.ibizlab.odoo.odoo_base.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_base.dto.*;
import cn.ibizlab.odoo.odoo_base.mapping.*;
import cn.ibizlab.odoo.core.odoo_base.domain.Base_language_export;
import cn.ibizlab.odoo.core.odoo_base.service.IBase_language_exportService;
import cn.ibizlab.odoo.core.odoo_base.filter.Base_language_exportSearchContext;




@Slf4j
@Api(tags = {"Base_language_export" })
@RestController("odoo_base-base_language_export")
@RequestMapping("")
public class Base_language_exportResource {

    @Autowired
    private IBase_language_exportService base_language_exportService;

    @Autowired
    @Lazy
    private Base_language_exportMapping base_language_exportMapping;




    @PreAuthorize("hasPermission('Remove',{#base_language_export_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Base_language_export" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/base_language_exports/{base_language_export_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("base_language_export_id") Integer base_language_export_id) {
         return ResponseEntity.status(HttpStatus.OK).body(base_language_exportService.remove(base_language_export_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Base_language_export" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/base_language_exports/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        base_language_exportService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission(#base_language_export_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Base_language_export" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/base_language_exports/{base_language_export_id}")
    public ResponseEntity<Base_language_exportDTO> get(@PathVariable("base_language_export_id") Integer base_language_export_id) {
        Base_language_export domain = base_language_exportService.get(base_language_export_id);
        Base_language_exportDTO dto = base_language_exportMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }




    @PreAuthorize("hasPermission(#base_language_export_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Base_language_export" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/base_language_exports/{base_language_export_id}")

    public ResponseEntity<Base_language_exportDTO> update(@PathVariable("base_language_export_id") Integer base_language_export_id, @RequestBody Base_language_exportDTO base_language_exportdto) {
		Base_language_export domain = base_language_exportMapping.toDomain(base_language_exportdto);
        domain.setId(base_language_export_id);
		base_language_exportService.update(domain);
		Base_language_exportDTO dto = base_language_exportMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#base_language_export_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Base_language_export" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/base_language_exports/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Base_language_exportDTO> base_language_exportdtos) {
        base_language_exportService.updateBatch(base_language_exportMapping.toDomain(base_language_exportdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }










    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Base_language_export" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/base_language_exports")

    public ResponseEntity<Base_language_exportDTO> create(@RequestBody Base_language_exportDTO base_language_exportdto) {
        Base_language_export domain = base_language_exportMapping.toDomain(base_language_exportdto);
		base_language_exportService.create(domain);
        Base_language_exportDTO dto = base_language_exportMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Base_language_export" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/base_language_exports/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Base_language_exportDTO> base_language_exportdtos) {
        base_language_exportService.createBatch(base_language_exportMapping.toDomain(base_language_exportdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Base_language_export" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/base_language_exports/fetchdefault")
	public ResponseEntity<List<Base_language_exportDTO>> fetchDefault(Base_language_exportSearchContext context) {
        Page<Base_language_export> domains = base_language_exportService.searchDefault(context) ;
        List<Base_language_exportDTO> list = base_language_exportMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Base_language_export" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/base_language_exports/searchdefault")
	public ResponseEntity<Page<Base_language_exportDTO>> searchDefault(Base_language_exportSearchContext context) {
        Page<Base_language_export> domains = base_language_exportService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(base_language_exportMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Base_language_export getEntity(){
        return new Base_language_export();
    }

}
