package cn.ibizlab.odoo.odoo_base.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_base.dto.*;
import cn.ibizlab.odoo.odoo_base.mapping.*;
import cn.ibizlab.odoo.core.odoo_base.domain.Base_partner_merge_line;
import cn.ibizlab.odoo.core.odoo_base.service.IBase_partner_merge_lineService;
import cn.ibizlab.odoo.core.odoo_base.filter.Base_partner_merge_lineSearchContext;




@Slf4j
@Api(tags = {"Base_partner_merge_line" })
@RestController("odoo_base-base_partner_merge_line")
@RequestMapping("")
public class Base_partner_merge_lineResource {

    @Autowired
    private IBase_partner_merge_lineService base_partner_merge_lineService;

    @Autowired
    @Lazy
    private Base_partner_merge_lineMapping base_partner_merge_lineMapping;




    @PreAuthorize("hasPermission(#base_partner_merge_line_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Base_partner_merge_line" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/base_partner_merge_lines/{base_partner_merge_line_id}")
    public ResponseEntity<Base_partner_merge_lineDTO> get(@PathVariable("base_partner_merge_line_id") Integer base_partner_merge_line_id) {
        Base_partner_merge_line domain = base_partner_merge_lineService.get(base_partner_merge_line_id);
        Base_partner_merge_lineDTO dto = base_partner_merge_lineMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }




    @PreAuthorize("hasPermission(#base_partner_merge_line_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Base_partner_merge_line" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/base_partner_merge_lines/{base_partner_merge_line_id}")

    public ResponseEntity<Base_partner_merge_lineDTO> update(@PathVariable("base_partner_merge_line_id") Integer base_partner_merge_line_id, @RequestBody Base_partner_merge_lineDTO base_partner_merge_linedto) {
		Base_partner_merge_line domain = base_partner_merge_lineMapping.toDomain(base_partner_merge_linedto);
        domain.setId(base_partner_merge_line_id);
		base_partner_merge_lineService.update(domain);
		Base_partner_merge_lineDTO dto = base_partner_merge_lineMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#base_partner_merge_line_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Base_partner_merge_line" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/base_partner_merge_lines/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Base_partner_merge_lineDTO> base_partner_merge_linedtos) {
        base_partner_merge_lineService.updateBatch(base_partner_merge_lineMapping.toDomain(base_partner_merge_linedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Base_partner_merge_line" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/base_partner_merge_lines")

    public ResponseEntity<Base_partner_merge_lineDTO> create(@RequestBody Base_partner_merge_lineDTO base_partner_merge_linedto) {
        Base_partner_merge_line domain = base_partner_merge_lineMapping.toDomain(base_partner_merge_linedto);
		base_partner_merge_lineService.create(domain);
        Base_partner_merge_lineDTO dto = base_partner_merge_lineMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Base_partner_merge_line" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/base_partner_merge_lines/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Base_partner_merge_lineDTO> base_partner_merge_linedtos) {
        base_partner_merge_lineService.createBatch(base_partner_merge_lineMapping.toDomain(base_partner_merge_linedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }













    @PreAuthorize("hasPermission('Remove',{#base_partner_merge_line_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Base_partner_merge_line" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/base_partner_merge_lines/{base_partner_merge_line_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("base_partner_merge_line_id") Integer base_partner_merge_line_id) {
         return ResponseEntity.status(HttpStatus.OK).body(base_partner_merge_lineService.remove(base_partner_merge_line_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Base_partner_merge_line" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/base_partner_merge_lines/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        base_partner_merge_lineService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Base_partner_merge_line" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/base_partner_merge_lines/fetchdefault")
	public ResponseEntity<List<Base_partner_merge_lineDTO>> fetchDefault(Base_partner_merge_lineSearchContext context) {
        Page<Base_partner_merge_line> domains = base_partner_merge_lineService.searchDefault(context) ;
        List<Base_partner_merge_lineDTO> list = base_partner_merge_lineMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Base_partner_merge_line" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/base_partner_merge_lines/searchdefault")
	public ResponseEntity<Page<Base_partner_merge_lineDTO>> searchDefault(Base_partner_merge_lineSearchContext context) {
        Page<Base_partner_merge_line> domains = base_partner_merge_lineService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(base_partner_merge_lineMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Base_partner_merge_line getEntity(){
        return new Base_partner_merge_line();
    }

}
