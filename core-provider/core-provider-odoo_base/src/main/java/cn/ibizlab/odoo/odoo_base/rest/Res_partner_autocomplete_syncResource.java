package cn.ibizlab.odoo.odoo_base.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_base.dto.*;
import cn.ibizlab.odoo.odoo_base.mapping.*;
import cn.ibizlab.odoo.core.odoo_base.domain.Res_partner_autocomplete_sync;
import cn.ibizlab.odoo.core.odoo_base.service.IRes_partner_autocomplete_syncService;
import cn.ibizlab.odoo.core.odoo_base.filter.Res_partner_autocomplete_syncSearchContext;




@Slf4j
@Api(tags = {"Res_partner_autocomplete_sync" })
@RestController("odoo_base-res_partner_autocomplete_sync")
@RequestMapping("")
public class Res_partner_autocomplete_syncResource {

    @Autowired
    private IRes_partner_autocomplete_syncService res_partner_autocomplete_syncService;

    @Autowired
    @Lazy
    private Res_partner_autocomplete_syncMapping res_partner_autocomplete_syncMapping;




    @PreAuthorize("hasPermission('Remove',{#res_partner_autocomplete_sync_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Res_partner_autocomplete_sync" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/res_partner_autocomplete_syncs/{res_partner_autocomplete_sync_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("res_partner_autocomplete_sync_id") Integer res_partner_autocomplete_sync_id) {
         return ResponseEntity.status(HttpStatus.OK).body(res_partner_autocomplete_syncService.remove(res_partner_autocomplete_sync_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Res_partner_autocomplete_sync" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/res_partner_autocomplete_syncs/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        res_partner_autocomplete_syncService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }










    @PreAuthorize("hasPermission(#res_partner_autocomplete_sync_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Res_partner_autocomplete_sync" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/res_partner_autocomplete_syncs/{res_partner_autocomplete_sync_id}")

    public ResponseEntity<Res_partner_autocomplete_syncDTO> update(@PathVariable("res_partner_autocomplete_sync_id") Integer res_partner_autocomplete_sync_id, @RequestBody Res_partner_autocomplete_syncDTO res_partner_autocomplete_syncdto) {
		Res_partner_autocomplete_sync domain = res_partner_autocomplete_syncMapping.toDomain(res_partner_autocomplete_syncdto);
        domain.setId(res_partner_autocomplete_sync_id);
		res_partner_autocomplete_syncService.update(domain);
		Res_partner_autocomplete_syncDTO dto = res_partner_autocomplete_syncMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#res_partner_autocomplete_sync_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Res_partner_autocomplete_sync" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/res_partner_autocomplete_syncs/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Res_partner_autocomplete_syncDTO> res_partner_autocomplete_syncdtos) {
        res_partner_autocomplete_syncService.updateBatch(res_partner_autocomplete_syncMapping.toDomain(res_partner_autocomplete_syncdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }







    @PreAuthorize("hasPermission(#res_partner_autocomplete_sync_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Res_partner_autocomplete_sync" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/res_partner_autocomplete_syncs/{res_partner_autocomplete_sync_id}")
    public ResponseEntity<Res_partner_autocomplete_syncDTO> get(@PathVariable("res_partner_autocomplete_sync_id") Integer res_partner_autocomplete_sync_id) {
        Res_partner_autocomplete_sync domain = res_partner_autocomplete_syncService.get(res_partner_autocomplete_sync_id);
        Res_partner_autocomplete_syncDTO dto = res_partner_autocomplete_syncMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }




    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Res_partner_autocomplete_sync" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/res_partner_autocomplete_syncs")

    public ResponseEntity<Res_partner_autocomplete_syncDTO> create(@RequestBody Res_partner_autocomplete_syncDTO res_partner_autocomplete_syncdto) {
        Res_partner_autocomplete_sync domain = res_partner_autocomplete_syncMapping.toDomain(res_partner_autocomplete_syncdto);
		res_partner_autocomplete_syncService.create(domain);
        Res_partner_autocomplete_syncDTO dto = res_partner_autocomplete_syncMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Res_partner_autocomplete_sync" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/res_partner_autocomplete_syncs/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Res_partner_autocomplete_syncDTO> res_partner_autocomplete_syncdtos) {
        res_partner_autocomplete_syncService.createBatch(res_partner_autocomplete_syncMapping.toDomain(res_partner_autocomplete_syncdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Res_partner_autocomplete_sync" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/res_partner_autocomplete_syncs/fetchdefault")
	public ResponseEntity<List<Res_partner_autocomplete_syncDTO>> fetchDefault(Res_partner_autocomplete_syncSearchContext context) {
        Page<Res_partner_autocomplete_sync> domains = res_partner_autocomplete_syncService.searchDefault(context) ;
        List<Res_partner_autocomplete_syncDTO> list = res_partner_autocomplete_syncMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Res_partner_autocomplete_sync" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/res_partner_autocomplete_syncs/searchdefault")
	public ResponseEntity<Page<Res_partner_autocomplete_syncDTO>> searchDefault(Res_partner_autocomplete_syncSearchContext context) {
        Page<Res_partner_autocomplete_sync> domains = res_partner_autocomplete_syncService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(res_partner_autocomplete_syncMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Res_partner_autocomplete_sync getEntity(){
        return new Res_partner_autocomplete_sync();
    }

}
