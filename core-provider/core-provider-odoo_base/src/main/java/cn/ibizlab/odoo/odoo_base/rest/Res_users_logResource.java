package cn.ibizlab.odoo.odoo_base.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_base.dto.*;
import cn.ibizlab.odoo.odoo_base.mapping.*;
import cn.ibizlab.odoo.core.odoo_base.domain.Res_users_log;
import cn.ibizlab.odoo.core.odoo_base.service.IRes_users_logService;
import cn.ibizlab.odoo.core.odoo_base.filter.Res_users_logSearchContext;




@Slf4j
@Api(tags = {"Res_users_log" })
@RestController("odoo_base-res_users_log")
@RequestMapping("")
public class Res_users_logResource {

    @Autowired
    private IRes_users_logService res_users_logService;

    @Autowired
    @Lazy
    private Res_users_logMapping res_users_logMapping;




    @PreAuthorize("hasPermission(#res_users_log_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Res_users_log" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/res_users_logs/{res_users_log_id}")

    public ResponseEntity<Res_users_logDTO> update(@PathVariable("res_users_log_id") Integer res_users_log_id, @RequestBody Res_users_logDTO res_users_logdto) {
		Res_users_log domain = res_users_logMapping.toDomain(res_users_logdto);
        domain.setId(res_users_log_id);
		res_users_logService.update(domain);
		Res_users_logDTO dto = res_users_logMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#res_users_log_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Res_users_log" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/res_users_logs/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Res_users_logDTO> res_users_logdtos) {
        res_users_logService.updateBatch(res_users_logMapping.toDomain(res_users_logdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission(#res_users_log_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Res_users_log" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/res_users_logs/{res_users_log_id}")
    public ResponseEntity<Res_users_logDTO> get(@PathVariable("res_users_log_id") Integer res_users_log_id) {
        Res_users_log domain = res_users_logService.get(res_users_log_id);
        Res_users_logDTO dto = res_users_logMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }




    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Res_users_log" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/res_users_logs")

    public ResponseEntity<Res_users_logDTO> create(@RequestBody Res_users_logDTO res_users_logdto) {
        Res_users_log domain = res_users_logMapping.toDomain(res_users_logdto);
		res_users_logService.create(domain);
        Res_users_logDTO dto = res_users_logMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Res_users_log" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/res_users_logs/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Res_users_logDTO> res_users_logdtos) {
        res_users_logService.createBatch(res_users_logMapping.toDomain(res_users_logdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }







    @PreAuthorize("hasPermission('Remove',{#res_users_log_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Res_users_log" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/res_users_logs/{res_users_log_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("res_users_log_id") Integer res_users_log_id) {
         return ResponseEntity.status(HttpStatus.OK).body(res_users_logService.remove(res_users_log_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Res_users_log" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/res_users_logs/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        res_users_logService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }







    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Res_users_log" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/res_users_logs/fetchdefault")
	public ResponseEntity<List<Res_users_logDTO>> fetchDefault(Res_users_logSearchContext context) {
        Page<Res_users_log> domains = res_users_logService.searchDefault(context) ;
        List<Res_users_logDTO> list = res_users_logMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Res_users_log" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/res_users_logs/searchdefault")
	public ResponseEntity<Page<Res_users_logDTO>> searchDefault(Res_users_logSearchContext context) {
        Page<Res_users_log> domains = res_users_logService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(res_users_logMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Res_users_log getEntity(){
        return new Res_users_log();
    }

}
