package cn.ibizlab.odoo.odoo_base.mapping;

import org.mapstruct.*;
import cn.ibizlab.odoo.core.odoo_base.domain.Base_module_upgrade;
import cn.ibizlab.odoo.odoo_base.dto.Base_module_upgradeDTO;
import cn.ibizlab.odoo.util.domain.MappingBase;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring", uses = {},
    nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
    nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface Base_module_upgradeMapping extends MappingBase<Base_module_upgradeDTO, Base_module_upgrade> {


}

