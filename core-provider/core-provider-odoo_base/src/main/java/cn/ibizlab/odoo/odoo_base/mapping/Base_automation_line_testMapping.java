package cn.ibizlab.odoo.odoo_base.mapping;

import org.mapstruct.*;
import cn.ibizlab.odoo.core.odoo_base.domain.Base_automation_line_test;
import cn.ibizlab.odoo.odoo_base.dto.Base_automation_line_testDTO;
import cn.ibizlab.odoo.util.domain.MappingBase;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring", uses = {},
    nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
    nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface Base_automation_line_testMapping extends MappingBase<Base_automation_line_testDTO, Base_automation_line_test> {


}

