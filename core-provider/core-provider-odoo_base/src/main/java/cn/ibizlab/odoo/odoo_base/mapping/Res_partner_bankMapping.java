package cn.ibizlab.odoo.odoo_base.mapping;

import org.mapstruct.*;
import cn.ibizlab.odoo.core.odoo_base.domain.Res_partner_bank;
import cn.ibizlab.odoo.odoo_base.dto.Res_partner_bankDTO;
import cn.ibizlab.odoo.util.domain.MappingBase;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring", uses = {},
    nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
    nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface Res_partner_bankMapping extends MappingBase<Res_partner_bankDTO, Res_partner_bank> {


}

