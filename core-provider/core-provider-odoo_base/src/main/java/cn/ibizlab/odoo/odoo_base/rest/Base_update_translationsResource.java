package cn.ibizlab.odoo.odoo_base.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_base.dto.*;
import cn.ibizlab.odoo.odoo_base.mapping.*;
import cn.ibizlab.odoo.core.odoo_base.domain.Base_update_translations;
import cn.ibizlab.odoo.core.odoo_base.service.IBase_update_translationsService;
import cn.ibizlab.odoo.core.odoo_base.filter.Base_update_translationsSearchContext;




@Slf4j
@Api(tags = {"Base_update_translations" })
@RestController("odoo_base-base_update_translations")
@RequestMapping("")
public class Base_update_translationsResource {

    @Autowired
    private IBase_update_translationsService base_update_translationsService;

    @Autowired
    @Lazy
    private Base_update_translationsMapping base_update_translationsMapping;




    @PreAuthorize("hasPermission(#base_update_translations_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Base_update_translations" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/base_update_translations/{base_update_translations_id}")

    public ResponseEntity<Base_update_translationsDTO> update(@PathVariable("base_update_translations_id") Integer base_update_translations_id, @RequestBody Base_update_translationsDTO base_update_translationsdto) {
		Base_update_translations domain = base_update_translationsMapping.toDomain(base_update_translationsdto);
        domain.setId(base_update_translations_id);
		base_update_translationsService.update(domain);
		Base_update_translationsDTO dto = base_update_translationsMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#base_update_translations_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Base_update_translations" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/base_update_translations/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Base_update_translationsDTO> base_update_translationsdtos) {
        base_update_translationsService.updateBatch(base_update_translationsMapping.toDomain(base_update_translationsdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission(#base_update_translations_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Base_update_translations" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/base_update_translations/{base_update_translations_id}")
    public ResponseEntity<Base_update_translationsDTO> get(@PathVariable("base_update_translations_id") Integer base_update_translations_id) {
        Base_update_translations domain = base_update_translationsService.get(base_update_translations_id);
        Base_update_translationsDTO dto = base_update_translationsMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }







    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Base_update_translations" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/base_update_translations")

    public ResponseEntity<Base_update_translationsDTO> create(@RequestBody Base_update_translationsDTO base_update_translationsdto) {
        Base_update_translations domain = base_update_translationsMapping.toDomain(base_update_translationsdto);
		base_update_translationsService.create(domain);
        Base_update_translationsDTO dto = base_update_translationsMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Base_update_translations" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/base_update_translations/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Base_update_translationsDTO> base_update_translationsdtos) {
        base_update_translationsService.createBatch(base_update_translationsMapping.toDomain(base_update_translationsdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }







    @PreAuthorize("hasPermission('Remove',{#base_update_translations_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Base_update_translations" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/base_update_translations/{base_update_translations_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("base_update_translations_id") Integer base_update_translations_id) {
         return ResponseEntity.status(HttpStatus.OK).body(base_update_translationsService.remove(base_update_translations_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Base_update_translations" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/base_update_translations/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        base_update_translationsService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Base_update_translations" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/base_update_translations/fetchdefault")
	public ResponseEntity<List<Base_update_translationsDTO>> fetchDefault(Base_update_translationsSearchContext context) {
        Page<Base_update_translations> domains = base_update_translationsService.searchDefault(context) ;
        List<Base_update_translationsDTO> list = base_update_translationsMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Base_update_translations" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/base_update_translations/searchdefault")
	public ResponseEntity<Page<Base_update_translationsDTO>> searchDefault(Base_update_translationsSearchContext context) {
        Page<Base_update_translations> domains = base_update_translationsService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(base_update_translationsMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Base_update_translations getEntity(){
        return new Base_update_translations();
    }

}
