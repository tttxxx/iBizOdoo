package cn.ibizlab.odoo.odoo_base.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_base.dto.*;
import cn.ibizlab.odoo.odoo_base.mapping.*;
import cn.ibizlab.odoo.core.odoo_base.domain.Res_users;
import cn.ibizlab.odoo.core.odoo_base.service.IRes_usersService;
import cn.ibizlab.odoo.core.odoo_base.filter.Res_usersSearchContext;




@Slf4j
@Api(tags = {"Res_users" })
@RestController("odoo_base-res_users")
@RequestMapping("")
public class Res_usersResource {

    @Autowired
    private IRes_usersService res_usersService;

    @Autowired
    @Lazy
    private Res_usersMapping res_usersMapping;




    @PreAuthorize("hasPermission(#res_users_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Res_users" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/res_users/{res_users_id}")

    public ResponseEntity<Res_usersDTO> update(@PathVariable("res_users_id") Integer res_users_id, @RequestBody Res_usersDTO res_usersdto) {
		Res_users domain = res_usersMapping.toDomain(res_usersdto);
        domain.setId(res_users_id);
		res_usersService.update(domain);
		Res_usersDTO dto = res_usersMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#res_users_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Res_users" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/res_users/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Res_usersDTO> res_usersdtos) {
        res_usersService.updateBatch(res_usersMapping.toDomain(res_usersdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Res_users" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/res_users")

    public ResponseEntity<Res_usersDTO> create(@RequestBody Res_usersDTO res_usersdto) {
        Res_users domain = res_usersMapping.toDomain(res_usersdto);
		res_usersService.create(domain);
        Res_usersDTO dto = res_usersMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Res_users" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/res_users/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Res_usersDTO> res_usersdtos) {
        res_usersService.createBatch(res_usersMapping.toDomain(res_usersdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission(#res_users_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Res_users" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/res_users/{res_users_id}")
    public ResponseEntity<Res_usersDTO> get(@PathVariable("res_users_id") Integer res_users_id) {
        Res_users domain = res_usersService.get(res_users_id);
        Res_usersDTO dto = res_usersMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }













    @PreAuthorize("hasPermission('Remove',{#res_users_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Res_users" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/res_users/{res_users_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("res_users_id") Integer res_users_id) {
         return ResponseEntity.status(HttpStatus.OK).body(res_usersService.remove(res_users_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Res_users" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/res_users/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        res_usersService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Res_users" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/res_users/fetchdefault")
	public ResponseEntity<List<Res_usersDTO>> fetchDefault(Res_usersSearchContext context) {
        Page<Res_users> domains = res_usersService.searchDefault(context) ;
        List<Res_usersDTO> list = res_usersMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Res_users" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/res_users/searchdefault")
	public ResponseEntity<Page<Res_usersDTO>> searchDefault(Res_usersSearchContext context) {
        Page<Res_users> domains = res_usersService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(res_usersMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Res_users getEntity(){
        return new Res_users();
    }

}
