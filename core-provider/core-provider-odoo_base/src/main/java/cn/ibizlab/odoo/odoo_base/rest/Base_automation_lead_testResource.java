package cn.ibizlab.odoo.odoo_base.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_base.dto.*;
import cn.ibizlab.odoo.odoo_base.mapping.*;
import cn.ibizlab.odoo.core.odoo_base.domain.Base_automation_lead_test;
import cn.ibizlab.odoo.core.odoo_base.service.IBase_automation_lead_testService;
import cn.ibizlab.odoo.core.odoo_base.filter.Base_automation_lead_testSearchContext;




@Slf4j
@Api(tags = {"Base_automation_lead_test" })
@RestController("odoo_base-base_automation_lead_test")
@RequestMapping("")
public class Base_automation_lead_testResource {

    @Autowired
    private IBase_automation_lead_testService base_automation_lead_testService;

    @Autowired
    @Lazy
    private Base_automation_lead_testMapping base_automation_lead_testMapping;







    @PreAuthorize("hasPermission('Remove',{#base_automation_lead_test_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Base_automation_lead_test" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/base_automation_lead_tests/{base_automation_lead_test_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("base_automation_lead_test_id") Integer base_automation_lead_test_id) {
         return ResponseEntity.status(HttpStatus.OK).body(base_automation_lead_testService.remove(base_automation_lead_test_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Base_automation_lead_test" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/base_automation_lead_tests/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        base_automation_lead_testService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Base_automation_lead_test" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/base_automation_lead_tests")

    public ResponseEntity<Base_automation_lead_testDTO> create(@RequestBody Base_automation_lead_testDTO base_automation_lead_testdto) {
        Base_automation_lead_test domain = base_automation_lead_testMapping.toDomain(base_automation_lead_testdto);
		base_automation_lead_testService.create(domain);
        Base_automation_lead_testDTO dto = base_automation_lead_testMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Base_automation_lead_test" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/base_automation_lead_tests/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Base_automation_lead_testDTO> base_automation_lead_testdtos) {
        base_automation_lead_testService.createBatch(base_automation_lead_testMapping.toDomain(base_automation_lead_testdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }







    @PreAuthorize("hasPermission(#base_automation_lead_test_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Base_automation_lead_test" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/base_automation_lead_tests/{base_automation_lead_test_id}")

    public ResponseEntity<Base_automation_lead_testDTO> update(@PathVariable("base_automation_lead_test_id") Integer base_automation_lead_test_id, @RequestBody Base_automation_lead_testDTO base_automation_lead_testdto) {
		Base_automation_lead_test domain = base_automation_lead_testMapping.toDomain(base_automation_lead_testdto);
        domain.setId(base_automation_lead_test_id);
		base_automation_lead_testService.update(domain);
		Base_automation_lead_testDTO dto = base_automation_lead_testMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#base_automation_lead_test_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Base_automation_lead_test" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/base_automation_lead_tests/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Base_automation_lead_testDTO> base_automation_lead_testdtos) {
        base_automation_lead_testService.updateBatch(base_automation_lead_testMapping.toDomain(base_automation_lead_testdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }







    @PreAuthorize("hasPermission(#base_automation_lead_test_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Base_automation_lead_test" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/base_automation_lead_tests/{base_automation_lead_test_id}")
    public ResponseEntity<Base_automation_lead_testDTO> get(@PathVariable("base_automation_lead_test_id") Integer base_automation_lead_test_id) {
        Base_automation_lead_test domain = base_automation_lead_testService.get(base_automation_lead_test_id);
        Base_automation_lead_testDTO dto = base_automation_lead_testMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Base_automation_lead_test" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/base_automation_lead_tests/fetchdefault")
	public ResponseEntity<List<Base_automation_lead_testDTO>> fetchDefault(Base_automation_lead_testSearchContext context) {
        Page<Base_automation_lead_test> domains = base_automation_lead_testService.searchDefault(context) ;
        List<Base_automation_lead_testDTO> list = base_automation_lead_testMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Base_automation_lead_test" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/base_automation_lead_tests/searchdefault")
	public ResponseEntity<Page<Base_automation_lead_testDTO>> searchDefault(Base_automation_lead_testSearchContext context) {
        Page<Base_automation_lead_test> domains = base_automation_lead_testService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(base_automation_lead_testMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Base_automation_lead_test getEntity(){
        return new Base_automation_lead_test();
    }

}
