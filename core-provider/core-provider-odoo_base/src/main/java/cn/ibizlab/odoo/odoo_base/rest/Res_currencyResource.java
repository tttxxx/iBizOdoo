package cn.ibizlab.odoo.odoo_base.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_base.dto.*;
import cn.ibizlab.odoo.odoo_base.mapping.*;
import cn.ibizlab.odoo.core.odoo_base.domain.Res_currency;
import cn.ibizlab.odoo.core.odoo_base.service.IRes_currencyService;
import cn.ibizlab.odoo.core.odoo_base.filter.Res_currencySearchContext;




@Slf4j
@Api(tags = {"Res_currency" })
@RestController("odoo_base-res_currency")
@RequestMapping("")
public class Res_currencyResource {

    @Autowired
    private IRes_currencyService res_currencyService;

    @Autowired
    @Lazy
    private Res_currencyMapping res_currencyMapping;




    @PreAuthorize("hasPermission(#res_currency_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Res_currency" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/res_currencies/{res_currency_id}")
    public ResponseEntity<Res_currencyDTO> get(@PathVariable("res_currency_id") Integer res_currency_id) {
        Res_currency domain = res_currencyService.get(res_currency_id);
        Res_currencyDTO dto = res_currencyMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }




    @PreAuthorize("hasPermission(#res_currency_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Res_currency" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/res_currencies/{res_currency_id}")

    public ResponseEntity<Res_currencyDTO> update(@PathVariable("res_currency_id") Integer res_currency_id, @RequestBody Res_currencyDTO res_currencydto) {
		Res_currency domain = res_currencyMapping.toDomain(res_currencydto);
        domain.setId(res_currency_id);
		res_currencyService.update(domain);
		Res_currencyDTO dto = res_currencyMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#res_currency_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Res_currency" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/res_currencies/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Res_currencyDTO> res_currencydtos) {
        res_currencyService.updateBatch(res_currencyMapping.toDomain(res_currencydtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission('Remove',{#res_currency_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Res_currency" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/res_currencies/{res_currency_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("res_currency_id") Integer res_currency_id) {
         return ResponseEntity.status(HttpStatus.OK).body(res_currencyService.remove(res_currency_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Res_currency" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/res_currencies/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        res_currencyService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }










    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Res_currency" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/res_currencies")

    public ResponseEntity<Res_currencyDTO> create(@RequestBody Res_currencyDTO res_currencydto) {
        Res_currency domain = res_currencyMapping.toDomain(res_currencydto);
		res_currencyService.create(domain);
        Res_currencyDTO dto = res_currencyMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Res_currency" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/res_currencies/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Res_currencyDTO> res_currencydtos) {
        res_currencyService.createBatch(res_currencyMapping.toDomain(res_currencydtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Res_currency" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/res_currencies/fetchdefault")
	public ResponseEntity<List<Res_currencyDTO>> fetchDefault(Res_currencySearchContext context) {
        Page<Res_currency> domains = res_currencyService.searchDefault(context) ;
        List<Res_currencyDTO> list = res_currencyMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Res_currency" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/res_currencies/searchdefault")
	public ResponseEntity<Page<Res_currencyDTO>> searchDefault(Res_currencySearchContext context) {
        Page<Res_currency> domains = res_currencyService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(res_currencyMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Res_currency getEntity(){
        return new Res_currency();
    }

}
