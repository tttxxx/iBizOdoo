package cn.ibizlab.odoo.odoo_base.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_base.dto.*;
import cn.ibizlab.odoo.odoo_base.mapping.*;
import cn.ibizlab.odoo.core.odoo_base.domain.Base_language_import;
import cn.ibizlab.odoo.core.odoo_base.service.IBase_language_importService;
import cn.ibizlab.odoo.core.odoo_base.filter.Base_language_importSearchContext;




@Slf4j
@Api(tags = {"Base_language_import" })
@RestController("odoo_base-base_language_import")
@RequestMapping("")
public class Base_language_importResource {

    @Autowired
    private IBase_language_importService base_language_importService;

    @Autowired
    @Lazy
    private Base_language_importMapping base_language_importMapping;




    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Base_language_import" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/base_language_imports")

    public ResponseEntity<Base_language_importDTO> create(@RequestBody Base_language_importDTO base_language_importdto) {
        Base_language_import domain = base_language_importMapping.toDomain(base_language_importdto);
		base_language_importService.create(domain);
        Base_language_importDTO dto = base_language_importMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Base_language_import" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/base_language_imports/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Base_language_importDTO> base_language_importdtos) {
        base_language_importService.createBatch(base_language_importMapping.toDomain(base_language_importdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }







    @PreAuthorize("hasPermission('Remove',{#base_language_import_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Base_language_import" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/base_language_imports/{base_language_import_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("base_language_import_id") Integer base_language_import_id) {
         return ResponseEntity.status(HttpStatus.OK).body(base_language_importService.remove(base_language_import_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Base_language_import" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/base_language_imports/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        base_language_importService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission(#base_language_import_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Base_language_import" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/base_language_imports/{base_language_import_id}")

    public ResponseEntity<Base_language_importDTO> update(@PathVariable("base_language_import_id") Integer base_language_import_id, @RequestBody Base_language_importDTO base_language_importdto) {
		Base_language_import domain = base_language_importMapping.toDomain(base_language_importdto);
        domain.setId(base_language_import_id);
		base_language_importService.update(domain);
		Base_language_importDTO dto = base_language_importMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#base_language_import_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Base_language_import" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/base_language_imports/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Base_language_importDTO> base_language_importdtos) {
        base_language_importService.updateBatch(base_language_importMapping.toDomain(base_language_importdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }







    @PreAuthorize("hasPermission(#base_language_import_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Base_language_import" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/base_language_imports/{base_language_import_id}")
    public ResponseEntity<Base_language_importDTO> get(@PathVariable("base_language_import_id") Integer base_language_import_id) {
        Base_language_import domain = base_language_importService.get(base_language_import_id);
        Base_language_importDTO dto = base_language_importMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }




    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Base_language_import" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/base_language_imports/fetchdefault")
	public ResponseEntity<List<Base_language_importDTO>> fetchDefault(Base_language_importSearchContext context) {
        Page<Base_language_import> domains = base_language_importService.searchDefault(context) ;
        List<Base_language_importDTO> list = base_language_importMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Base_language_import" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/base_language_imports/searchdefault")
	public ResponseEntity<Page<Base_language_importDTO>> searchDefault(Base_language_importSearchContext context) {
        Page<Base_language_import> domains = base_language_importService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(base_language_importMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Base_language_import getEntity(){
        return new Base_language_import();
    }

}
