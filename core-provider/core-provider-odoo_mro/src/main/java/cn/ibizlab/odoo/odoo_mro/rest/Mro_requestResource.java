package cn.ibizlab.odoo.odoo_mro.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_mro.dto.*;
import cn.ibizlab.odoo.odoo_mro.mapping.*;
import cn.ibizlab.odoo.core.odoo_mro.domain.Mro_request;
import cn.ibizlab.odoo.core.odoo_mro.service.IMro_requestService;
import cn.ibizlab.odoo.core.odoo_mro.filter.Mro_requestSearchContext;




@Slf4j
@Api(tags = {"Mro_request" })
@RestController("odoo_mro-mro_request")
@RequestMapping("")
public class Mro_requestResource {

    @Autowired
    private IMro_requestService mro_requestService;

    @Autowired
    @Lazy
    private Mro_requestMapping mro_requestMapping;







    @PreAuthorize("hasPermission(#mro_request_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Mro_request" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/mro_requests/{mro_request_id}")

    public ResponseEntity<Mro_requestDTO> update(@PathVariable("mro_request_id") Integer mro_request_id, @RequestBody Mro_requestDTO mro_requestdto) {
		Mro_request domain = mro_requestMapping.toDomain(mro_requestdto);
        domain.setId(mro_request_id);
		mro_requestService.update(domain);
		Mro_requestDTO dto = mro_requestMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#mro_request_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Mro_request" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/mro_requests/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Mro_requestDTO> mro_requestdtos) {
        mro_requestService.updateBatch(mro_requestMapping.toDomain(mro_requestdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission('Remove',{#mro_request_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Mro_request" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/mro_requests/{mro_request_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("mro_request_id") Integer mro_request_id) {
         return ResponseEntity.status(HttpStatus.OK).body(mro_requestService.remove(mro_request_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Mro_request" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/mro_requests/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        mro_requestService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission(#mro_request_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Mro_request" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/mro_requests/{mro_request_id}")
    public ResponseEntity<Mro_requestDTO> get(@PathVariable("mro_request_id") Integer mro_request_id) {
        Mro_request domain = mro_requestService.get(mro_request_id);
        Mro_requestDTO dto = mro_requestMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }




    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Mro_request" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/mro_requests")

    public ResponseEntity<Mro_requestDTO> create(@RequestBody Mro_requestDTO mro_requestdto) {
        Mro_request domain = mro_requestMapping.toDomain(mro_requestdto);
		mro_requestService.create(domain);
        Mro_requestDTO dto = mro_requestMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Mro_request" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/mro_requests/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Mro_requestDTO> mro_requestdtos) {
        mro_requestService.createBatch(mro_requestMapping.toDomain(mro_requestdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }







    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Mro_request" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/mro_requests/fetchdefault")
	public ResponseEntity<List<Mro_requestDTO>> fetchDefault(Mro_requestSearchContext context) {
        Page<Mro_request> domains = mro_requestService.searchDefault(context) ;
        List<Mro_requestDTO> list = mro_requestMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Mro_request" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/mro_requests/searchdefault")
	public ResponseEntity<Page<Mro_requestDTO>> searchDefault(Mro_requestSearchContext context) {
        Page<Mro_request> domains = mro_requestService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(mro_requestMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Mro_request getEntity(){
        return new Mro_request();
    }

}
