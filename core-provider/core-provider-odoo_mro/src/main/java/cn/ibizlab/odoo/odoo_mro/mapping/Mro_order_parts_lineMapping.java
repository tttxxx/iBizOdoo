package cn.ibizlab.odoo.odoo_mro.mapping;

import org.mapstruct.*;
import cn.ibizlab.odoo.core.odoo_mro.domain.Mro_order_parts_line;
import cn.ibizlab.odoo.odoo_mro.dto.Mro_order_parts_lineDTO;
import cn.ibizlab.odoo.util.domain.MappingBase;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring", uses = {},
    nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
    nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface Mro_order_parts_lineMapping extends MappingBase<Mro_order_parts_lineDTO, Mro_order_parts_line> {


}

