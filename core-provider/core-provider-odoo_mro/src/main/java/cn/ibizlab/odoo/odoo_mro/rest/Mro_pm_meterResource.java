package cn.ibizlab.odoo.odoo_mro.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_mro.dto.*;
import cn.ibizlab.odoo.odoo_mro.mapping.*;
import cn.ibizlab.odoo.core.odoo_mro.domain.Mro_pm_meter;
import cn.ibizlab.odoo.core.odoo_mro.service.IMro_pm_meterService;
import cn.ibizlab.odoo.core.odoo_mro.filter.Mro_pm_meterSearchContext;




@Slf4j
@Api(tags = {"Mro_pm_meter" })
@RestController("odoo_mro-mro_pm_meter")
@RequestMapping("")
public class Mro_pm_meterResource {

    @Autowired
    private IMro_pm_meterService mro_pm_meterService;

    @Autowired
    @Lazy
    private Mro_pm_meterMapping mro_pm_meterMapping;







    @PreAuthorize("hasPermission(#mro_pm_meter_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Mro_pm_meter" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/mro_pm_meters/{mro_pm_meter_id}")

    public ResponseEntity<Mro_pm_meterDTO> update(@PathVariable("mro_pm_meter_id") Integer mro_pm_meter_id, @RequestBody Mro_pm_meterDTO mro_pm_meterdto) {
		Mro_pm_meter domain = mro_pm_meterMapping.toDomain(mro_pm_meterdto);
        domain.setId(mro_pm_meter_id);
		mro_pm_meterService.update(domain);
		Mro_pm_meterDTO dto = mro_pm_meterMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#mro_pm_meter_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Mro_pm_meter" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/mro_pm_meters/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Mro_pm_meterDTO> mro_pm_meterdtos) {
        mro_pm_meterService.updateBatch(mro_pm_meterMapping.toDomain(mro_pm_meterdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission('Remove',{#mro_pm_meter_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Mro_pm_meter" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/mro_pm_meters/{mro_pm_meter_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("mro_pm_meter_id") Integer mro_pm_meter_id) {
         return ResponseEntity.status(HttpStatus.OK).body(mro_pm_meterService.remove(mro_pm_meter_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Mro_pm_meter" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/mro_pm_meters/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        mro_pm_meterService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission(#mro_pm_meter_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Mro_pm_meter" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/mro_pm_meters/{mro_pm_meter_id}")
    public ResponseEntity<Mro_pm_meterDTO> get(@PathVariable("mro_pm_meter_id") Integer mro_pm_meter_id) {
        Mro_pm_meter domain = mro_pm_meterService.get(mro_pm_meter_id);
        Mro_pm_meterDTO dto = mro_pm_meterMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }







    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Mro_pm_meter" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/mro_pm_meters")

    public ResponseEntity<Mro_pm_meterDTO> create(@RequestBody Mro_pm_meterDTO mro_pm_meterdto) {
        Mro_pm_meter domain = mro_pm_meterMapping.toDomain(mro_pm_meterdto);
		mro_pm_meterService.create(domain);
        Mro_pm_meterDTO dto = mro_pm_meterMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Mro_pm_meter" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/mro_pm_meters/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Mro_pm_meterDTO> mro_pm_meterdtos) {
        mro_pm_meterService.createBatch(mro_pm_meterMapping.toDomain(mro_pm_meterdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Mro_pm_meter" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/mro_pm_meters/fetchdefault")
	public ResponseEntity<List<Mro_pm_meterDTO>> fetchDefault(Mro_pm_meterSearchContext context) {
        Page<Mro_pm_meter> domains = mro_pm_meterService.searchDefault(context) ;
        List<Mro_pm_meterDTO> list = mro_pm_meterMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Mro_pm_meter" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/mro_pm_meters/searchdefault")
	public ResponseEntity<Page<Mro_pm_meterDTO>> searchDefault(Mro_pm_meterSearchContext context) {
        Page<Mro_pm_meter> domains = mro_pm_meterService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(mro_pm_meterMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Mro_pm_meter getEntity(){
        return new Mro_pm_meter();
    }

}
