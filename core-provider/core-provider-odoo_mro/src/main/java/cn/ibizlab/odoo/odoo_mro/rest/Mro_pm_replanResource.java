package cn.ibizlab.odoo.odoo_mro.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_mro.dto.*;
import cn.ibizlab.odoo.odoo_mro.mapping.*;
import cn.ibizlab.odoo.core.odoo_mro.domain.Mro_pm_replan;
import cn.ibizlab.odoo.core.odoo_mro.service.IMro_pm_replanService;
import cn.ibizlab.odoo.core.odoo_mro.filter.Mro_pm_replanSearchContext;




@Slf4j
@Api(tags = {"Mro_pm_replan" })
@RestController("odoo_mro-mro_pm_replan")
@RequestMapping("")
public class Mro_pm_replanResource {

    @Autowired
    private IMro_pm_replanService mro_pm_replanService;

    @Autowired
    @Lazy
    private Mro_pm_replanMapping mro_pm_replanMapping;




    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Mro_pm_replan" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/mro_pm_replans")

    public ResponseEntity<Mro_pm_replanDTO> create(@RequestBody Mro_pm_replanDTO mro_pm_replandto) {
        Mro_pm_replan domain = mro_pm_replanMapping.toDomain(mro_pm_replandto);
		mro_pm_replanService.create(domain);
        Mro_pm_replanDTO dto = mro_pm_replanMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Mro_pm_replan" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/mro_pm_replans/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Mro_pm_replanDTO> mro_pm_replandtos) {
        mro_pm_replanService.createBatch(mro_pm_replanMapping.toDomain(mro_pm_replandtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }







    @PreAuthorize("hasPermission('Remove',{#mro_pm_replan_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Mro_pm_replan" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/mro_pm_replans/{mro_pm_replan_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("mro_pm_replan_id") Integer mro_pm_replan_id) {
         return ResponseEntity.status(HttpStatus.OK).body(mro_pm_replanService.remove(mro_pm_replan_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Mro_pm_replan" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/mro_pm_replans/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        mro_pm_replanService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }







    @PreAuthorize("hasPermission(#mro_pm_replan_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Mro_pm_replan" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/mro_pm_replans/{mro_pm_replan_id}")

    public ResponseEntity<Mro_pm_replanDTO> update(@PathVariable("mro_pm_replan_id") Integer mro_pm_replan_id, @RequestBody Mro_pm_replanDTO mro_pm_replandto) {
		Mro_pm_replan domain = mro_pm_replanMapping.toDomain(mro_pm_replandto);
        domain.setId(mro_pm_replan_id);
		mro_pm_replanService.update(domain);
		Mro_pm_replanDTO dto = mro_pm_replanMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#mro_pm_replan_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Mro_pm_replan" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/mro_pm_replans/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Mro_pm_replanDTO> mro_pm_replandtos) {
        mro_pm_replanService.updateBatch(mro_pm_replanMapping.toDomain(mro_pm_replandtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }







    @PreAuthorize("hasPermission(#mro_pm_replan_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Mro_pm_replan" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/mro_pm_replans/{mro_pm_replan_id}")
    public ResponseEntity<Mro_pm_replanDTO> get(@PathVariable("mro_pm_replan_id") Integer mro_pm_replan_id) {
        Mro_pm_replan domain = mro_pm_replanService.get(mro_pm_replan_id);
        Mro_pm_replanDTO dto = mro_pm_replanMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Mro_pm_replan" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/mro_pm_replans/fetchdefault")
	public ResponseEntity<List<Mro_pm_replanDTO>> fetchDefault(Mro_pm_replanSearchContext context) {
        Page<Mro_pm_replan> domains = mro_pm_replanService.searchDefault(context) ;
        List<Mro_pm_replanDTO> list = mro_pm_replanMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Mro_pm_replan" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/mro_pm_replans/searchdefault")
	public ResponseEntity<Page<Mro_pm_replanDTO>> searchDefault(Mro_pm_replanSearchContext context) {
        Page<Mro_pm_replan> domains = mro_pm_replanService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(mro_pm_replanMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Mro_pm_replan getEntity(){
        return new Mro_pm_replan();
    }

}
