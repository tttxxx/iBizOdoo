package cn.ibizlab.odoo.odoo_mro.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_mro.dto.*;
import cn.ibizlab.odoo.odoo_mro.mapping.*;
import cn.ibizlab.odoo.core.odoo_mro.domain.Mro_workorder;
import cn.ibizlab.odoo.core.odoo_mro.service.IMro_workorderService;
import cn.ibizlab.odoo.core.odoo_mro.filter.Mro_workorderSearchContext;




@Slf4j
@Api(tags = {"Mro_workorder" })
@RestController("odoo_mro-mro_workorder")
@RequestMapping("")
public class Mro_workorderResource {

    @Autowired
    private IMro_workorderService mro_workorderService;

    @Autowired
    @Lazy
    private Mro_workorderMapping mro_workorderMapping;




    @PreAuthorize("hasPermission('Remove',{#mro_workorder_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Mro_workorder" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/mro_workorders/{mro_workorder_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("mro_workorder_id") Integer mro_workorder_id) {
         return ResponseEntity.status(HttpStatus.OK).body(mro_workorderService.remove(mro_workorder_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Mro_workorder" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/mro_workorders/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        mro_workorderService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }







    @PreAuthorize("hasPermission(#mro_workorder_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Mro_workorder" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/mro_workorders/{mro_workorder_id}")

    public ResponseEntity<Mro_workorderDTO> update(@PathVariable("mro_workorder_id") Integer mro_workorder_id, @RequestBody Mro_workorderDTO mro_workorderdto) {
		Mro_workorder domain = mro_workorderMapping.toDomain(mro_workorderdto);
        domain.setId(mro_workorder_id);
		mro_workorderService.update(domain);
		Mro_workorderDTO dto = mro_workorderMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#mro_workorder_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Mro_workorder" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/mro_workorders/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Mro_workorderDTO> mro_workorderdtos) {
        mro_workorderService.updateBatch(mro_workorderMapping.toDomain(mro_workorderdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }










    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Mro_workorder" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/mro_workorders")

    public ResponseEntity<Mro_workorderDTO> create(@RequestBody Mro_workorderDTO mro_workorderdto) {
        Mro_workorder domain = mro_workorderMapping.toDomain(mro_workorderdto);
		mro_workorderService.create(domain);
        Mro_workorderDTO dto = mro_workorderMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Mro_workorder" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/mro_workorders/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Mro_workorderDTO> mro_workorderdtos) {
        mro_workorderService.createBatch(mro_workorderMapping.toDomain(mro_workorderdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission(#mro_workorder_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Mro_workorder" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/mro_workorders/{mro_workorder_id}")
    public ResponseEntity<Mro_workorderDTO> get(@PathVariable("mro_workorder_id") Integer mro_workorder_id) {
        Mro_workorder domain = mro_workorderService.get(mro_workorder_id);
        Mro_workorderDTO dto = mro_workorderMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Mro_workorder" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/mro_workorders/fetchdefault")
	public ResponseEntity<List<Mro_workorderDTO>> fetchDefault(Mro_workorderSearchContext context) {
        Page<Mro_workorder> domains = mro_workorderService.searchDefault(context) ;
        List<Mro_workorderDTO> list = mro_workorderMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Mro_workorder" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/mro_workorders/searchdefault")
	public ResponseEntity<Page<Mro_workorderDTO>> searchDefault(Mro_workorderSearchContext context) {
        Page<Mro_workorder> domains = mro_workorderService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(mro_workorderMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Mro_workorder getEntity(){
        return new Mro_workorder();
    }

}
