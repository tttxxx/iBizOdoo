package cn.ibizlab.odoo.odoo_mro.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;
import cn.ibizlab.odoo.util.domain.DTOBase;
import lombok.Data;

/**
 * 服务DTO对象[Mro_pm_ruleDTO]
 */
@Data
public class Mro_pm_ruleDTO extends DTOBase implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [WRITE_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;

    /**
     * 属性 [HORIZON]
     *
     */
    @JSONField(name = "horizon")
    @JsonProperty("horizon")
    private Double horizon;

    /**
     * 属性 [PM_RULES_LINE_IDS]
     *
     */
    @JSONField(name = "pm_rules_line_ids")
    @JsonProperty("pm_rules_line_ids")
    private String pmRulesLineIds;

    /**
     * 属性 [NAME]
     *
     */
    @JSONField(name = "name")
    @JsonProperty("name")
    private String name;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    private String displayName;

    /**
     * 属性 [ID]
     *
     */
    @JSONField(name = "id")
    @JsonProperty("id")
    private Integer id;

    /**
     * 属性 [CREATE_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;

    /**
     * 属性 [ACTIVE]
     *
     */
    @JSONField(name = "active")
    @JsonProperty("active")
    private String active;

    /**
     * 属性 [CREATE_UID_TEXT]
     *
     */
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    private String createUidText;

    /**
     * 属性 [PARAMETER_ID_TEXT]
     *
     */
    @JSONField(name = "parameter_id_text")
    @JsonProperty("parameter_id_text")
    private String parameterIdText;

    /**
     * 属性 [CATEGORY_ID_TEXT]
     *
     */
    @JSONField(name = "category_id_text")
    @JsonProperty("category_id_text")
    private String categoryIdText;

    /**
     * 属性 [WRITE_UID_TEXT]
     *
     */
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    private String writeUidText;

    /**
     * 属性 [PARAMETER_UOM]
     *
     */
    @JSONField(name = "parameter_uom")
    @JsonProperty("parameter_uom")
    private Integer parameterUom;

    /**
     * 属性 [PARAMETER_ID]
     *
     */
    @JSONField(name = "parameter_id")
    @JsonProperty("parameter_id")
    private Integer parameterId;

    /**
     * 属性 [CREATE_UID]
     *
     */
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    private Integer createUid;

    /**
     * 属性 [CATEGORY_ID]
     *
     */
    @JSONField(name = "category_id")
    @JsonProperty("category_id")
    private Integer categoryId;

    /**
     * 属性 [WRITE_UID]
     *
     */
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    private Integer writeUid;


    /**
     * 设置 [HORIZON]
     */
    public void setHorizon(Double  horizon){
        this.horizon = horizon ;
        this.modify("horizon",horizon);
    }

    /**
     * 设置 [NAME]
     */
    public void setName(String  name){
        this.name = name ;
        this.modify("name",name);
    }

    /**
     * 设置 [ACTIVE]
     */
    public void setActive(String  active){
        this.active = active ;
        this.modify("active",active);
    }

    /**
     * 设置 [PARAMETER_ID]
     */
    public void setParameterId(Integer  parameterId){
        this.parameterId = parameterId ;
        this.modify("parameter_id",parameterId);
    }

    /**
     * 设置 [CATEGORY_ID]
     */
    public void setCategoryId(Integer  categoryId){
        this.categoryId = categoryId ;
        this.modify("category_id",categoryId);
    }


}

