package cn.ibizlab.odoo.odoo_mro.mapping;

import org.mapstruct.*;
import cn.ibizlab.odoo.core.odoo_mro.domain.Mro_request_reject;
import cn.ibizlab.odoo.odoo_mro.dto.Mro_request_rejectDTO;
import cn.ibizlab.odoo.util.domain.MappingBase;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring", uses = {},
    nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
    nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface Mro_request_rejectMapping extends MappingBase<Mro_request_rejectDTO, Mro_request_reject> {


}

