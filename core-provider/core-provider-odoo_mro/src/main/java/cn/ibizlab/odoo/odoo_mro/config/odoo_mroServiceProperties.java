package cn.ibizlab.odoo.odoo_mro.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import lombok.Data;

@ConfigurationProperties(prefix = "service.odoo-mro")
@Data
public class odoo_mroServiceProperties {

	private boolean enabled;

	private boolean auth;


}