package cn.ibizlab.odoo.odoo_mro.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_mro.dto.*;
import cn.ibizlab.odoo.odoo_mro.mapping.*;
import cn.ibizlab.odoo.core.odoo_mro.domain.Mro_order_parts_line;
import cn.ibizlab.odoo.core.odoo_mro.service.IMro_order_parts_lineService;
import cn.ibizlab.odoo.core.odoo_mro.filter.Mro_order_parts_lineSearchContext;




@Slf4j
@Api(tags = {"Mro_order_parts_line" })
@RestController("odoo_mro-mro_order_parts_line")
@RequestMapping("")
public class Mro_order_parts_lineResource {

    @Autowired
    private IMro_order_parts_lineService mro_order_parts_lineService;

    @Autowired
    @Lazy
    private Mro_order_parts_lineMapping mro_order_parts_lineMapping;







    @PreAuthorize("hasPermission('Remove',{#mro_order_parts_line_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Mro_order_parts_line" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/mro_order_parts_lines/{mro_order_parts_line_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("mro_order_parts_line_id") Integer mro_order_parts_line_id) {
         return ResponseEntity.status(HttpStatus.OK).body(mro_order_parts_lineService.remove(mro_order_parts_line_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Mro_order_parts_line" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/mro_order_parts_lines/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        mro_order_parts_lineService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission(#mro_order_parts_line_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Mro_order_parts_line" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/mro_order_parts_lines/{mro_order_parts_line_id}")
    public ResponseEntity<Mro_order_parts_lineDTO> get(@PathVariable("mro_order_parts_line_id") Integer mro_order_parts_line_id) {
        Mro_order_parts_line domain = mro_order_parts_lineService.get(mro_order_parts_line_id);
        Mro_order_parts_lineDTO dto = mro_order_parts_lineMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }







    @PreAuthorize("hasPermission(#mro_order_parts_line_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Mro_order_parts_line" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/mro_order_parts_lines/{mro_order_parts_line_id}")

    public ResponseEntity<Mro_order_parts_lineDTO> update(@PathVariable("mro_order_parts_line_id") Integer mro_order_parts_line_id, @RequestBody Mro_order_parts_lineDTO mro_order_parts_linedto) {
		Mro_order_parts_line domain = mro_order_parts_lineMapping.toDomain(mro_order_parts_linedto);
        domain.setId(mro_order_parts_line_id);
		mro_order_parts_lineService.update(domain);
		Mro_order_parts_lineDTO dto = mro_order_parts_lineMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#mro_order_parts_line_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Mro_order_parts_line" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/mro_order_parts_lines/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Mro_order_parts_lineDTO> mro_order_parts_linedtos) {
        mro_order_parts_lineService.updateBatch(mro_order_parts_lineMapping.toDomain(mro_order_parts_linedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }







    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Mro_order_parts_line" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/mro_order_parts_lines")

    public ResponseEntity<Mro_order_parts_lineDTO> create(@RequestBody Mro_order_parts_lineDTO mro_order_parts_linedto) {
        Mro_order_parts_line domain = mro_order_parts_lineMapping.toDomain(mro_order_parts_linedto);
		mro_order_parts_lineService.create(domain);
        Mro_order_parts_lineDTO dto = mro_order_parts_lineMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Mro_order_parts_line" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/mro_order_parts_lines/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Mro_order_parts_lineDTO> mro_order_parts_linedtos) {
        mro_order_parts_lineService.createBatch(mro_order_parts_lineMapping.toDomain(mro_order_parts_linedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Mro_order_parts_line" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/mro_order_parts_lines/fetchdefault")
	public ResponseEntity<List<Mro_order_parts_lineDTO>> fetchDefault(Mro_order_parts_lineSearchContext context) {
        Page<Mro_order_parts_line> domains = mro_order_parts_lineService.searchDefault(context) ;
        List<Mro_order_parts_lineDTO> list = mro_order_parts_lineMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Mro_order_parts_line" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/mro_order_parts_lines/searchdefault")
	public ResponseEntity<Page<Mro_order_parts_lineDTO>> searchDefault(Mro_order_parts_lineSearchContext context) {
        Page<Mro_order_parts_line> domains = mro_order_parts_lineService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(mro_order_parts_lineMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Mro_order_parts_line getEntity(){
        return new Mro_order_parts_line();
    }

}
