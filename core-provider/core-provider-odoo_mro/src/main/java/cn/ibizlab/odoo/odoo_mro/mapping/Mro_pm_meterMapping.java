package cn.ibizlab.odoo.odoo_mro.mapping;

import org.mapstruct.*;
import cn.ibizlab.odoo.core.odoo_mro.domain.Mro_pm_meter;
import cn.ibizlab.odoo.odoo_mro.dto.Mro_pm_meterDTO;
import cn.ibizlab.odoo.util.domain.MappingBase;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring", uses = {},
    nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
    nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface Mro_pm_meterMapping extends MappingBase<Mro_pm_meterDTO, Mro_pm_meter> {


}

