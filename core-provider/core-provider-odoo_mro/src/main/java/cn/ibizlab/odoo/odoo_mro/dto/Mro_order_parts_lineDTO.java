package cn.ibizlab.odoo.odoo_mro.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;
import cn.ibizlab.odoo.util.domain.DTOBase;
import lombok.Data;

/**
 * 服务DTO对象[Mro_order_parts_lineDTO]
 */
@Data
public class Mro_order_parts_lineDTO extends DTOBase implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [NAME]
     *
     */
    @JSONField(name = "name")
    @JsonProperty("name")
    private String name;

    /**
     * 属性 [CREATE_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;

    /**
     * 属性 [ID]
     *
     */
    @JSONField(name = "id")
    @JsonProperty("id")
    private Integer id;

    /**
     * 属性 [WRITE_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    private String displayName;

    /**
     * 属性 [PARTS_QTY]
     *
     */
    @JSONField(name = "parts_qty")
    @JsonProperty("parts_qty")
    private Double partsQty;

    /**
     * 属性 [CREATE_UID_TEXT]
     *
     */
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    private String createUidText;

    /**
     * 属性 [PARTS_UOM_TEXT]
     *
     */
    @JSONField(name = "parts_uom_text")
    @JsonProperty("parts_uom_text")
    private String partsUomText;

    /**
     * 属性 [WRITE_UID_TEXT]
     *
     */
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    private String writeUidText;

    /**
     * 属性 [PARTS_ID_TEXT]
     *
     */
    @JSONField(name = "parts_id_text")
    @JsonProperty("parts_id_text")
    private String partsIdText;

    /**
     * 属性 [MAINTENANCE_ID_TEXT]
     *
     */
    @JSONField(name = "maintenance_id_text")
    @JsonProperty("maintenance_id_text")
    private String maintenanceIdText;

    /**
     * 属性 [PARTS_ID]
     *
     */
    @JSONField(name = "parts_id")
    @JsonProperty("parts_id")
    private Integer partsId;

    /**
     * 属性 [MAINTENANCE_ID]
     *
     */
    @JSONField(name = "maintenance_id")
    @JsonProperty("maintenance_id")
    private Integer maintenanceId;

    /**
     * 属性 [WRITE_UID]
     *
     */
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    private Integer writeUid;

    /**
     * 属性 [PARTS_UOM]
     *
     */
    @JSONField(name = "parts_uom")
    @JsonProperty("parts_uom")
    private Integer partsUom;

    /**
     * 属性 [CREATE_UID]
     *
     */
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    private Integer createUid;


    /**
     * 设置 [NAME]
     */
    public void setName(String  name){
        this.name = name ;
        this.modify("name",name);
    }

    /**
     * 设置 [PARTS_QTY]
     */
    public void setPartsQty(Double  partsQty){
        this.partsQty = partsQty ;
        this.modify("parts_qty",partsQty);
    }

    /**
     * 设置 [PARTS_ID]
     */
    public void setPartsId(Integer  partsId){
        this.partsId = partsId ;
        this.modify("parts_id",partsId);
    }

    /**
     * 设置 [MAINTENANCE_ID]
     */
    public void setMaintenanceId(Integer  maintenanceId){
        this.maintenanceId = maintenanceId ;
        this.modify("maintenance_id",maintenanceId);
    }

    /**
     * 设置 [PARTS_UOM]
     */
    public void setPartsUom(Integer  partsUom){
        this.partsUom = partsUom ;
        this.modify("parts_uom",partsUom);
    }


}

