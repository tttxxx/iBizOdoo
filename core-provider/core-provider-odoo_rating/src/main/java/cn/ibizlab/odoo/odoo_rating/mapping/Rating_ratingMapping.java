package cn.ibizlab.odoo.odoo_rating.mapping;

import org.mapstruct.*;
import cn.ibizlab.odoo.core.odoo_rating.domain.Rating_rating;
import cn.ibizlab.odoo.odoo_rating.dto.Rating_ratingDTO;
import cn.ibizlab.odoo.util.domain.MappingBase;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring", uses = {},
    nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
    nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface Rating_ratingMapping extends MappingBase<Rating_ratingDTO, Rating_rating> {


}

