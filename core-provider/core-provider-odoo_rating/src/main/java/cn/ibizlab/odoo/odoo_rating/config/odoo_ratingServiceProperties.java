package cn.ibizlab.odoo.odoo_rating.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import lombok.Data;

@ConfigurationProperties(prefix = "service.odoo-rating")
@Data
public class odoo_ratingServiceProperties {

	private boolean enabled;

	private boolean auth;


}