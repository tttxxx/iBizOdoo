package cn.ibizlab.odoo.odoo_rating.mapping;

import org.mapstruct.*;
import cn.ibizlab.odoo.core.odoo_rating.domain.Rating_mixin;
import cn.ibizlab.odoo.odoo_rating.dto.Rating_mixinDTO;
import cn.ibizlab.odoo.util.domain.MappingBase;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring", uses = {},
    nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
    nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface Rating_mixinMapping extends MappingBase<Rating_mixinDTO, Rating_mixin> {


}

