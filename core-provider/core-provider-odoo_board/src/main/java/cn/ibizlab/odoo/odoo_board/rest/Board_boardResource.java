package cn.ibizlab.odoo.odoo_board.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_board.dto.*;
import cn.ibizlab.odoo.odoo_board.mapping.*;
import cn.ibizlab.odoo.core.odoo_board.domain.Board_board;
import cn.ibizlab.odoo.core.odoo_board.service.IBoard_boardService;
import cn.ibizlab.odoo.core.odoo_board.filter.Board_boardSearchContext;




@Slf4j
@Api(tags = {"Board_board" })
@RestController("odoo_board-board_board")
@RequestMapping("")
public class Board_boardResource {

    @Autowired
    private IBoard_boardService board_boardService;

    @Autowired
    @Lazy
    private Board_boardMapping board_boardMapping;




    @PreAuthorize("hasPermission(#board_board_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Board_board" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/board_boards/{board_board_id}")

    public ResponseEntity<Board_boardDTO> update(@PathVariable("board_board_id") Integer board_board_id, @RequestBody Board_boardDTO board_boarddto) {
		Board_board domain = board_boardMapping.toDomain(board_boarddto);
        domain.setId(board_board_id);
		board_boardService.update(domain);
		Board_boardDTO dto = board_boardMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#board_board_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Board_board" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/board_boards/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Board_boardDTO> board_boarddtos) {
        board_boardService.updateBatch(board_boardMapping.toDomain(board_boarddtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }










    @PreAuthorize("hasPermission('Remove',{#board_board_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Board_board" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/board_boards/{board_board_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("board_board_id") Integer board_board_id) {
         return ResponseEntity.status(HttpStatus.OK).body(board_boardService.remove(board_board_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Board_board" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/board_boards/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        board_boardService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Board_board" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/board_boards")

    public ResponseEntity<Board_boardDTO> create(@RequestBody Board_boardDTO board_boarddto) {
        Board_board domain = board_boardMapping.toDomain(board_boarddto);
		board_boardService.create(domain);
        Board_boardDTO dto = board_boardMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Board_board" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/board_boards/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Board_boardDTO> board_boarddtos) {
        board_boardService.createBatch(board_boardMapping.toDomain(board_boarddtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission(#board_board_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Board_board" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/board_boards/{board_board_id}")
    public ResponseEntity<Board_boardDTO> get(@PathVariable("board_board_id") Integer board_board_id) {
        Board_board domain = board_boardService.get(board_board_id);
        Board_boardDTO dto = board_boardMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }




    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Board_board" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/board_boards/fetchdefault")
	public ResponseEntity<List<Board_boardDTO>> fetchDefault(Board_boardSearchContext context) {
        Page<Board_board> domains = board_boardService.searchDefault(context) ;
        List<Board_boardDTO> list = board_boardMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Board_board" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/board_boards/searchdefault")
	public ResponseEntity<Page<Board_boardDTO>> searchDefault(Board_boardSearchContext context) {
        Page<Board_board> domains = board_boardService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(board_boardMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Board_board getEntity(){
        return new Board_board();
    }

}
