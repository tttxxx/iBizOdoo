package cn.ibizlab.odoo.odoo_event.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_event.dto.*;
import cn.ibizlab.odoo.odoo_event.mapping.*;
import cn.ibizlab.odoo.core.odoo_event.domain.Event_registration;
import cn.ibizlab.odoo.core.odoo_event.service.IEvent_registrationService;
import cn.ibizlab.odoo.core.odoo_event.filter.Event_registrationSearchContext;




@Slf4j
@Api(tags = {"Event_registration" })
@RestController("odoo_event-event_registration")
@RequestMapping("")
public class Event_registrationResource {

    @Autowired
    private IEvent_registrationService event_registrationService;

    @Autowired
    @Lazy
    private Event_registrationMapping event_registrationMapping;




    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Event_registration" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/event_registrations")

    public ResponseEntity<Event_registrationDTO> create(@RequestBody Event_registrationDTO event_registrationdto) {
        Event_registration domain = event_registrationMapping.toDomain(event_registrationdto);
		event_registrationService.create(domain);
        Event_registrationDTO dto = event_registrationMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Event_registration" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/event_registrations/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Event_registrationDTO> event_registrationdtos) {
        event_registrationService.createBatch(event_registrationMapping.toDomain(event_registrationdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }













    @PreAuthorize("hasPermission('Remove',{#event_registration_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Event_registration" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/event_registrations/{event_registration_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("event_registration_id") Integer event_registration_id) {
         return ResponseEntity.status(HttpStatus.OK).body(event_registrationService.remove(event_registration_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Event_registration" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/event_registrations/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        event_registrationService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission(#event_registration_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Event_registration" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/event_registrations/{event_registration_id}")

    public ResponseEntity<Event_registrationDTO> update(@PathVariable("event_registration_id") Integer event_registration_id, @RequestBody Event_registrationDTO event_registrationdto) {
		Event_registration domain = event_registrationMapping.toDomain(event_registrationdto);
        domain.setId(event_registration_id);
		event_registrationService.update(domain);
		Event_registrationDTO dto = event_registrationMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#event_registration_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Event_registration" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/event_registrations/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Event_registrationDTO> event_registrationdtos) {
        event_registrationService.updateBatch(event_registrationMapping.toDomain(event_registrationdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission(#event_registration_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Event_registration" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/event_registrations/{event_registration_id}")
    public ResponseEntity<Event_registrationDTO> get(@PathVariable("event_registration_id") Integer event_registration_id) {
        Event_registration domain = event_registrationService.get(event_registration_id);
        Event_registrationDTO dto = event_registrationMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Event_registration" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/event_registrations/fetchdefault")
	public ResponseEntity<List<Event_registrationDTO>> fetchDefault(Event_registrationSearchContext context) {
        Page<Event_registration> domains = event_registrationService.searchDefault(context) ;
        List<Event_registrationDTO> list = event_registrationMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Event_registration" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/event_registrations/searchdefault")
	public ResponseEntity<Page<Event_registrationDTO>> searchDefault(Event_registrationSearchContext context) {
        Page<Event_registration> domains = event_registrationService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(event_registrationMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Event_registration getEntity(){
        return new Event_registration();
    }

}
