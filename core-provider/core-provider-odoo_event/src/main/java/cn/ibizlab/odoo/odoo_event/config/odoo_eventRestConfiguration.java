package cn.ibizlab.odoo.odoo_event.config;

import org.springframework.context.annotation.ComponentScan;

@ComponentScan("cn.ibizlab.odoo.odoo_event")
public class odoo_eventRestConfiguration {

}
