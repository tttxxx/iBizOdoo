package cn.ibizlab.odoo.odoo_event.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_event.dto.*;
import cn.ibizlab.odoo.odoo_event.mapping.*;
import cn.ibizlab.odoo.core.odoo_event.domain.Event_type_mail;
import cn.ibizlab.odoo.core.odoo_event.service.IEvent_type_mailService;
import cn.ibizlab.odoo.core.odoo_event.filter.Event_type_mailSearchContext;




@Slf4j
@Api(tags = {"Event_type_mail" })
@RestController("odoo_event-event_type_mail")
@RequestMapping("")
public class Event_type_mailResource {

    @Autowired
    private IEvent_type_mailService event_type_mailService;

    @Autowired
    @Lazy
    private Event_type_mailMapping event_type_mailMapping;







    @PreAuthorize("hasPermission('Remove',{#event_type_mail_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Event_type_mail" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/event_type_mails/{event_type_mail_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("event_type_mail_id") Integer event_type_mail_id) {
         return ResponseEntity.status(HttpStatus.OK).body(event_type_mailService.remove(event_type_mail_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Event_type_mail" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/event_type_mails/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        event_type_mailService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Event_type_mail" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/event_type_mails")

    public ResponseEntity<Event_type_mailDTO> create(@RequestBody Event_type_mailDTO event_type_maildto) {
        Event_type_mail domain = event_type_mailMapping.toDomain(event_type_maildto);
		event_type_mailService.create(domain);
        Event_type_mailDTO dto = event_type_mailMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Event_type_mail" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/event_type_mails/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Event_type_mailDTO> event_type_maildtos) {
        event_type_mailService.createBatch(event_type_mailMapping.toDomain(event_type_maildtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission(#event_type_mail_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Event_type_mail" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/event_type_mails/{event_type_mail_id}")

    public ResponseEntity<Event_type_mailDTO> update(@PathVariable("event_type_mail_id") Integer event_type_mail_id, @RequestBody Event_type_mailDTO event_type_maildto) {
		Event_type_mail domain = event_type_mailMapping.toDomain(event_type_maildto);
        domain.setId(event_type_mail_id);
		event_type_mailService.update(domain);
		Event_type_mailDTO dto = event_type_mailMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#event_type_mail_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Event_type_mail" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/event_type_mails/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Event_type_mailDTO> event_type_maildtos) {
        event_type_mailService.updateBatch(event_type_mailMapping.toDomain(event_type_maildtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission(#event_type_mail_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Event_type_mail" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/event_type_mails/{event_type_mail_id}")
    public ResponseEntity<Event_type_mailDTO> get(@PathVariable("event_type_mail_id") Integer event_type_mail_id) {
        Event_type_mail domain = event_type_mailService.get(event_type_mail_id);
        Event_type_mailDTO dto = event_type_mailMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }







    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Event_type_mail" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/event_type_mails/fetchdefault")
	public ResponseEntity<List<Event_type_mailDTO>> fetchDefault(Event_type_mailSearchContext context) {
        Page<Event_type_mail> domains = event_type_mailService.searchDefault(context) ;
        List<Event_type_mailDTO> list = event_type_mailMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Event_type_mail" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/event_type_mails/searchdefault")
	public ResponseEntity<Page<Event_type_mailDTO>> searchDefault(Event_type_mailSearchContext context) {
        Page<Event_type_mail> domains = event_type_mailService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(event_type_mailMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Event_type_mail getEntity(){
        return new Event_type_mail();
    }

}
