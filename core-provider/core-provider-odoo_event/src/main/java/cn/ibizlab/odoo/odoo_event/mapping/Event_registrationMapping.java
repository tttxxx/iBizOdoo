package cn.ibizlab.odoo.odoo_event.mapping;

import org.mapstruct.*;
import cn.ibizlab.odoo.core.odoo_event.domain.Event_registration;
import cn.ibizlab.odoo.odoo_event.dto.Event_registrationDTO;
import cn.ibizlab.odoo.util.domain.MappingBase;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring", uses = {},
    nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
    nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface Event_registrationMapping extends MappingBase<Event_registrationDTO, Event_registration> {


}

