package cn.ibizlab.odoo.odoo_event.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;
import cn.ibizlab.odoo.util.domain.DTOBase;
import lombok.Data;

/**
 * 服务DTO对象[Event_event_ticketDTO]
 */
@Data
public class Event_event_ticketDTO extends DTOBase implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [CREATE_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;

    /**
     * 属性 [WRITE_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;

    /**
     * 属性 [SEATS_AVAILABLE]
     *
     */
    @JSONField(name = "seats_available")
    @JsonProperty("seats_available")
    private Integer seatsAvailable;

    /**
     * 属性 [ID]
     *
     */
    @JSONField(name = "id")
    @JsonProperty("id")
    private Integer id;

    /**
     * 属性 [NAME]
     *
     */
    @JSONField(name = "name")
    @JsonProperty("name")
    private String name;

    /**
     * 属性 [PRICE_REDUCE_TAXINC]
     *
     */
    @JSONField(name = "price_reduce_taxinc")
    @JsonProperty("price_reduce_taxinc")
    private Double priceReduceTaxinc;

    /**
     * 属性 [PRICE_REDUCE]
     *
     */
    @JSONField(name = "price_reduce")
    @JsonProperty("price_reduce")
    private Double priceReduce;

    /**
     * 属性 [REGISTRATION_IDS]
     *
     */
    @JSONField(name = "registration_ids")
    @JsonProperty("registration_ids")
    private String registrationIds;

    /**
     * 属性 [SEATS_UNCONFIRMED]
     *
     */
    @JSONField(name = "seats_unconfirmed")
    @JsonProperty("seats_unconfirmed")
    private Integer seatsUnconfirmed;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    private String displayName;

    /**
     * 属性 [SEATS_RESERVED]
     *
     */
    @JSONField(name = "seats_reserved")
    @JsonProperty("seats_reserved")
    private Integer seatsReserved;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;

    /**
     * 属性 [PRICE]
     *
     */
    @JSONField(name = "price")
    @JsonProperty("price")
    private Double price;

    /**
     * 属性 [SEATS_USED]
     *
     */
    @JSONField(name = "seats_used")
    @JsonProperty("seats_used")
    private Integer seatsUsed;

    /**
     * 属性 [DEADLINE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "deadline" , format="yyyy-MM-dd")
    @JsonProperty("deadline")
    private Timestamp deadline;

    /**
     * 属性 [SEATS_AVAILABILITY]
     *
     */
    @JSONField(name = "seats_availability")
    @JsonProperty("seats_availability")
    private String seatsAvailability;

    /**
     * 属性 [IS_EXPIRED]
     *
     */
    @JSONField(name = "is_expired")
    @JsonProperty("is_expired")
    private String isExpired;

    /**
     * 属性 [SEATS_MAX]
     *
     */
    @JSONField(name = "seats_max")
    @JsonProperty("seats_max")
    private Integer seatsMax;

    /**
     * 属性 [EVENT_TYPE_ID_TEXT]
     *
     */
    @JSONField(name = "event_type_id_text")
    @JsonProperty("event_type_id_text")
    private String eventTypeIdText;

    /**
     * 属性 [EVENT_ID_TEXT]
     *
     */
    @JSONField(name = "event_id_text")
    @JsonProperty("event_id_text")
    private String eventIdText;

    /**
     * 属性 [CREATE_UID_TEXT]
     *
     */
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    private String createUidText;

    /**
     * 属性 [WRITE_UID_TEXT]
     *
     */
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    private String writeUidText;

    /**
     * 属性 [PRODUCT_ID_TEXT]
     *
     */
    @JSONField(name = "product_id_text")
    @JsonProperty("product_id_text")
    private String productIdText;

    /**
     * 属性 [PRODUCT_ID]
     *
     */
    @JSONField(name = "product_id")
    @JsonProperty("product_id")
    private Integer productId;

    /**
     * 属性 [CREATE_UID]
     *
     */
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    private Integer createUid;

    /**
     * 属性 [EVENT_ID]
     *
     */
    @JSONField(name = "event_id")
    @JsonProperty("event_id")
    private Integer eventId;

    /**
     * 属性 [EVENT_TYPE_ID]
     *
     */
    @JSONField(name = "event_type_id")
    @JsonProperty("event_type_id")
    private Integer eventTypeId;

    /**
     * 属性 [WRITE_UID]
     *
     */
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    private Integer writeUid;


    /**
     * 设置 [SEATS_AVAILABLE]
     */
    public void setSeatsAvailable(Integer  seatsAvailable){
        this.seatsAvailable = seatsAvailable ;
        this.modify("seats_available",seatsAvailable);
    }

    /**
     * 设置 [NAME]
     */
    public void setName(String  name){
        this.name = name ;
        this.modify("name",name);
    }

    /**
     * 设置 [SEATS_UNCONFIRMED]
     */
    public void setSeatsUnconfirmed(Integer  seatsUnconfirmed){
        this.seatsUnconfirmed = seatsUnconfirmed ;
        this.modify("seats_unconfirmed",seatsUnconfirmed);
    }

    /**
     * 设置 [SEATS_RESERVED]
     */
    public void setSeatsReserved(Integer  seatsReserved){
        this.seatsReserved = seatsReserved ;
        this.modify("seats_reserved",seatsReserved);
    }

    /**
     * 设置 [PRICE]
     */
    public void setPrice(Double  price){
        this.price = price ;
        this.modify("price",price);
    }

    /**
     * 设置 [SEATS_USED]
     */
    public void setSeatsUsed(Integer  seatsUsed){
        this.seatsUsed = seatsUsed ;
        this.modify("seats_used",seatsUsed);
    }

    /**
     * 设置 [DEADLINE]
     */
    public void setDeadline(Timestamp  deadline){
        this.deadline = deadline ;
        this.modify("deadline",deadline);
    }

    /**
     * 设置 [SEATS_AVAILABILITY]
     */
    public void setSeatsAvailability(String  seatsAvailability){
        this.seatsAvailability = seatsAvailability ;
        this.modify("seats_availability",seatsAvailability);
    }

    /**
     * 设置 [SEATS_MAX]
     */
    public void setSeatsMax(Integer  seatsMax){
        this.seatsMax = seatsMax ;
        this.modify("seats_max",seatsMax);
    }

    /**
     * 设置 [PRODUCT_ID]
     */
    public void setProductId(Integer  productId){
        this.productId = productId ;
        this.modify("product_id",productId);
    }

    /**
     * 设置 [EVENT_ID]
     */
    public void setEventId(Integer  eventId){
        this.eventId = eventId ;
        this.modify("event_id",eventId);
    }

    /**
     * 设置 [EVENT_TYPE_ID]
     */
    public void setEventTypeId(Integer  eventTypeId){
        this.eventTypeId = eventTypeId ;
        this.modify("event_type_id",eventTypeId);
    }


}

