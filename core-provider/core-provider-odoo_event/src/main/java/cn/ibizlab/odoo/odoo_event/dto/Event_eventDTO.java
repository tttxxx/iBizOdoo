package cn.ibizlab.odoo.odoo_event.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;
import cn.ibizlab.odoo.util.domain.DTOBase;
import lombok.Data;

/**
 * 服务DTO对象[Event_eventDTO]
 */
@Data
public class Event_eventDTO extends DTOBase implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [SEATS_USED]
     *
     */
    @JSONField(name = "seats_used")
    @JsonProperty("seats_used")
    private Integer seatsUsed;

    /**
     * 属性 [IS_SEO_OPTIMIZED]
     *
     */
    @JSONField(name = "is_seo_optimized")
    @JsonProperty("is_seo_optimized")
    private String isSeoOptimized;

    /**
     * 属性 [ID]
     *
     */
    @JSONField(name = "id")
    @JsonProperty("id")
    private Integer id;

    /**
     * 属性 [BADGE_FRONT]
     *
     */
    @JSONField(name = "badge_front")
    @JsonProperty("badge_front")
    private String badgeFront;

    /**
     * 属性 [STATE]
     *
     */
    @JSONField(name = "state")
    @JsonProperty("state")
    private String state;

    /**
     * 属性 [EVENT_MAIL_IDS]
     *
     */
    @JSONField(name = "event_mail_ids")
    @JsonProperty("event_mail_ids")
    private String eventMailIds;

    /**
     * 属性 [CREATE_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;

    /**
     * 属性 [SEATS_EXPECTED]
     *
     */
    @JSONField(name = "seats_expected")
    @JsonProperty("seats_expected")
    private Integer seatsExpected;

    /**
     * 属性 [MESSAGE_UNREAD]
     *
     */
    @JSONField(name = "message_unread")
    @JsonProperty("message_unread")
    private String messageUnread;

    /**
     * 属性 [MESSAGE_NEEDACTION]
     *
     */
    @JSONField(name = "message_needaction")
    @JsonProperty("message_needaction")
    private String messageNeedaction;

    /**
     * 属性 [SEATS_AVAILABILITY]
     *
     */
    @JSONField(name = "seats_availability")
    @JsonProperty("seats_availability")
    private String seatsAvailability;

    /**
     * 属性 [EVENT_TICKET_IDS]
     *
     */
    @JSONField(name = "event_ticket_ids")
    @JsonProperty("event_ticket_ids")
    private String eventTicketIds;

    /**
     * 属性 [SEATS_UNCONFIRMED]
     *
     */
    @JSONField(name = "seats_unconfirmed")
    @JsonProperty("seats_unconfirmed")
    private Integer seatsUnconfirmed;

    /**
     * 属性 [IS_PARTICIPATING]
     *
     */
    @JSONField(name = "is_participating")
    @JsonProperty("is_participating")
    private String isParticipating;

    /**
     * 属性 [MESSAGE_CHANNEL_IDS]
     *
     */
    @JSONField(name = "message_channel_ids")
    @JsonProperty("message_channel_ids")
    private String messageChannelIds;

    /**
     * 属性 [TWITTER_HASHTAG]
     *
     */
    @JSONField(name = "twitter_hashtag")
    @JsonProperty("twitter_hashtag")
    private String twitterHashtag;

    /**
     * 属性 [SEATS_MAX]
     *
     */
    @JSONField(name = "seats_max")
    @JsonProperty("seats_max")
    private Integer seatsMax;

    /**
     * 属性 [DATE_BEGIN]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "date_begin" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("date_begin")
    private Timestamp dateBegin;

    /**
     * 属性 [MESSAGE_MAIN_ATTACHMENT_ID]
     *
     */
    @JSONField(name = "message_main_attachment_id")
    @JsonProperty("message_main_attachment_id")
    private Integer messageMainAttachmentId;

    /**
     * 属性 [WEBSITE_META_OG_IMG]
     *
     */
    @JSONField(name = "website_meta_og_img")
    @JsonProperty("website_meta_og_img")
    private String websiteMetaOgImg;

    /**
     * 属性 [WEBSITE_PUBLISHED]
     *
     */
    @JSONField(name = "website_published")
    @JsonProperty("website_published")
    private String websitePublished;

    /**
     * 属性 [MESSAGE_IDS]
     *
     */
    @JSONField(name = "message_ids")
    @JsonProperty("message_ids")
    private String messageIds;

    /**
     * 属性 [WEBSITE_META_TITLE]
     *
     */
    @JSONField(name = "website_meta_title")
    @JsonProperty("website_meta_title")
    private String websiteMetaTitle;

    /**
     * 属性 [MESSAGE_HAS_ERROR_COUNTER]
     *
     */
    @JSONField(name = "message_has_error_counter")
    @JsonProperty("message_has_error_counter")
    private Integer messageHasErrorCounter;

    /**
     * 属性 [REGISTRATION_IDS]
     *
     */
    @JSONField(name = "registration_ids")
    @JsonProperty("registration_ids")
    private String registrationIds;

    /**
     * 属性 [DESCRIPTION]
     *
     */
    @JSONField(name = "description")
    @JsonProperty("description")
    private String description;

    /**
     * 属性 [BADGE_INNERRIGHT]
     *
     */
    @JSONField(name = "badge_innerright")
    @JsonProperty("badge_innerright")
    private String badgeInnerright;

    /**
     * 属性 [MESSAGE_IS_FOLLOWER]
     *
     */
    @JSONField(name = "message_is_follower")
    @JsonProperty("message_is_follower")
    private String messageIsFollower;

    /**
     * 属性 [WEBSITE_META_DESCRIPTION]
     *
     */
    @JSONField(name = "website_meta_description")
    @JsonProperty("website_meta_description")
    private String websiteMetaDescription;

    /**
     * 属性 [SEATS_MIN]
     *
     */
    @JSONField(name = "seats_min")
    @JsonProperty("seats_min")
    private Integer seatsMin;

    /**
     * 属性 [MESSAGE_FOLLOWER_IDS]
     *
     */
    @JSONField(name = "message_follower_ids")
    @JsonProperty("message_follower_ids")
    private String messageFollowerIds;

    /**
     * 属性 [BADGE_BACK]
     *
     */
    @JSONField(name = "badge_back")
    @JsonProperty("badge_back")
    private String badgeBack;

    /**
     * 属性 [WEBSITE_META_KEYWORDS]
     *
     */
    @JSONField(name = "website_meta_keywords")
    @JsonProperty("website_meta_keywords")
    private String websiteMetaKeywords;

    /**
     * 属性 [MESSAGE_UNREAD_COUNTER]
     *
     */
    @JSONField(name = "message_unread_counter")
    @JsonProperty("message_unread_counter")
    private Integer messageUnreadCounter;

    /**
     * 属性 [AUTO_CONFIRM]
     *
     */
    @JSONField(name = "auto_confirm")
    @JsonProperty("auto_confirm")
    private String autoConfirm;

    /**
     * 属性 [DATE_END]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "date_end" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("date_end")
    private Timestamp dateEnd;

    /**
     * 属性 [NAME]
     *
     */
    @JSONField(name = "name")
    @JsonProperty("name")
    private String name;

    /**
     * 属性 [BADGE_INNERLEFT]
     *
     */
    @JSONField(name = "badge_innerleft")
    @JsonProperty("badge_innerleft")
    private String badgeInnerleft;

    /**
     * 属性 [SEATS_RESERVED]
     *
     */
    @JSONField(name = "seats_reserved")
    @JsonProperty("seats_reserved")
    private Integer seatsReserved;

    /**
     * 属性 [WEBSITE_MENU]
     *
     */
    @JSONField(name = "website_menu")
    @JsonProperty("website_menu")
    private String websiteMenu;

    /**
     * 属性 [MESSAGE_ATTACHMENT_COUNT]
     *
     */
    @JSONField(name = "message_attachment_count")
    @JsonProperty("message_attachment_count")
    private Integer messageAttachmentCount;

    /**
     * 属性 [WEBSITE_ID]
     *
     */
    @JSONField(name = "website_id")
    @JsonProperty("website_id")
    private Integer websiteId;

    /**
     * 属性 [ACTIVE]
     *
     */
    @JSONField(name = "active")
    @JsonProperty("active")
    private String active;

    /**
     * 属性 [DATE_END_LOCATED]
     *
     */
    @JSONField(name = "date_end_located")
    @JsonProperty("date_end_located")
    private String dateEndLocated;

    /**
     * 属性 [SEATS_AVAILABLE]
     *
     */
    @JSONField(name = "seats_available")
    @JsonProperty("seats_available")
    private Integer seatsAvailable;

    /**
     * 属性 [IS_PUBLISHED]
     *
     */
    @JSONField(name = "is_published")
    @JsonProperty("is_published")
    private String isPublished;

    /**
     * 属性 [MESSAGE_PARTNER_IDS]
     *
     */
    @JSONField(name = "message_partner_ids")
    @JsonProperty("message_partner_ids")
    private String messagePartnerIds;

    /**
     * 属性 [IS_ONLINE]
     *
     */
    @JSONField(name = "is_online")
    @JsonProperty("is_online")
    private String isOnline;

    /**
     * 属性 [DATE_TZ]
     *
     */
    @JSONField(name = "date_tz")
    @JsonProperty("date_tz")
    private String dateTz;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    private String displayName;

    /**
     * 属性 [MESSAGE_HAS_ERROR]
     *
     */
    @JSONField(name = "message_has_error")
    @JsonProperty("message_has_error")
    private String messageHasError;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;

    /**
     * 属性 [MESSAGE_NEEDACTION_COUNTER]
     *
     */
    @JSONField(name = "message_needaction_counter")
    @JsonProperty("message_needaction_counter")
    private Integer messageNeedactionCounter;

    /**
     * 属性 [WRITE_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;

    /**
     * 属性 [WEBSITE_MESSAGE_IDS]
     *
     */
    @JSONField(name = "website_message_ids")
    @JsonProperty("website_message_ids")
    private String websiteMessageIds;

    /**
     * 属性 [DATE_BEGIN_LOCATED]
     *
     */
    @JSONField(name = "date_begin_located")
    @JsonProperty("date_begin_located")
    private String dateBeginLocated;

    /**
     * 属性 [EVENT_LOGO]
     *
     */
    @JSONField(name = "event_logo")
    @JsonProperty("event_logo")
    private String eventLogo;

    /**
     * 属性 [WEBSITE_URL]
     *
     */
    @JSONField(name = "website_url")
    @JsonProperty("website_url")
    private String websiteUrl;

    /**
     * 属性 [COLOR]
     *
     */
    @JSONField(name = "color")
    @JsonProperty("color")
    private Integer color;

    /**
     * 属性 [WRITE_UID_TEXT]
     *
     */
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    private String writeUidText;

    /**
     * 属性 [EVENT_TYPE_ID_TEXT]
     *
     */
    @JSONField(name = "event_type_id_text")
    @JsonProperty("event_type_id_text")
    private String eventTypeIdText;

    /**
     * 属性 [CREATE_UID_TEXT]
     *
     */
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    private String createUidText;

    /**
     * 属性 [ADDRESS_ID_TEXT]
     *
     */
    @JSONField(name = "address_id_text")
    @JsonProperty("address_id_text")
    private String addressIdText;

    /**
     * 属性 [COUNTRY_ID_TEXT]
     *
     */
    @JSONField(name = "country_id_text")
    @JsonProperty("country_id_text")
    private String countryIdText;

    /**
     * 属性 [MENU_ID_TEXT]
     *
     */
    @JSONField(name = "menu_id_text")
    @JsonProperty("menu_id_text")
    private String menuIdText;

    /**
     * 属性 [COMPANY_ID_TEXT]
     *
     */
    @JSONField(name = "company_id_text")
    @JsonProperty("company_id_text")
    private String companyIdText;

    /**
     * 属性 [USER_ID_TEXT]
     *
     */
    @JSONField(name = "user_id_text")
    @JsonProperty("user_id_text")
    private String userIdText;

    /**
     * 属性 [ORGANIZER_ID_TEXT]
     *
     */
    @JSONField(name = "organizer_id_text")
    @JsonProperty("organizer_id_text")
    private String organizerIdText;

    /**
     * 属性 [USER_ID]
     *
     */
    @JSONField(name = "user_id")
    @JsonProperty("user_id")
    private Integer userId;

    /**
     * 属性 [COUNTRY_ID]
     *
     */
    @JSONField(name = "country_id")
    @JsonProperty("country_id")
    private Integer countryId;

    /**
     * 属性 [COMPANY_ID]
     *
     */
    @JSONField(name = "company_id")
    @JsonProperty("company_id")
    private Integer companyId;

    /**
     * 属性 [ORGANIZER_ID]
     *
     */
    @JSONField(name = "organizer_id")
    @JsonProperty("organizer_id")
    private Integer organizerId;

    /**
     * 属性 [MENU_ID]
     *
     */
    @JSONField(name = "menu_id")
    @JsonProperty("menu_id")
    private Integer menuId;

    /**
     * 属性 [CREATE_UID]
     *
     */
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    private Integer createUid;

    /**
     * 属性 [ADDRESS_ID]
     *
     */
    @JSONField(name = "address_id")
    @JsonProperty("address_id")
    private Integer addressId;

    /**
     * 属性 [WRITE_UID]
     *
     */
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    private Integer writeUid;

    /**
     * 属性 [EVENT_TYPE_ID]
     *
     */
    @JSONField(name = "event_type_id")
    @JsonProperty("event_type_id")
    private Integer eventTypeId;


    /**
     * 设置 [SEATS_USED]
     */
    public void setSeatsUsed(Integer  seatsUsed){
        this.seatsUsed = seatsUsed ;
        this.modify("seats_used",seatsUsed);
    }

    /**
     * 设置 [BADGE_FRONT]
     */
    public void setBadgeFront(String  badgeFront){
        this.badgeFront = badgeFront ;
        this.modify("badge_front",badgeFront);
    }

    /**
     * 设置 [STATE]
     */
    public void setState(String  state){
        this.state = state ;
        this.modify("state",state);
    }

    /**
     * 设置 [SEATS_AVAILABILITY]
     */
    public void setSeatsAvailability(String  seatsAvailability){
        this.seatsAvailability = seatsAvailability ;
        this.modify("seats_availability",seatsAvailability);
    }

    /**
     * 设置 [SEATS_UNCONFIRMED]
     */
    public void setSeatsUnconfirmed(Integer  seatsUnconfirmed){
        this.seatsUnconfirmed = seatsUnconfirmed ;
        this.modify("seats_unconfirmed",seatsUnconfirmed);
    }

    /**
     * 设置 [TWITTER_HASHTAG]
     */
    public void setTwitterHashtag(String  twitterHashtag){
        this.twitterHashtag = twitterHashtag ;
        this.modify("twitter_hashtag",twitterHashtag);
    }

    /**
     * 设置 [SEATS_MAX]
     */
    public void setSeatsMax(Integer  seatsMax){
        this.seatsMax = seatsMax ;
        this.modify("seats_max",seatsMax);
    }

    /**
     * 设置 [DATE_BEGIN]
     */
    public void setDateBegin(Timestamp  dateBegin){
        this.dateBegin = dateBegin ;
        this.modify("date_begin",dateBegin);
    }

    /**
     * 设置 [MESSAGE_MAIN_ATTACHMENT_ID]
     */
    public void setMessageMainAttachmentId(Integer  messageMainAttachmentId){
        this.messageMainAttachmentId = messageMainAttachmentId ;
        this.modify("message_main_attachment_id",messageMainAttachmentId);
    }

    /**
     * 设置 [WEBSITE_META_OG_IMG]
     */
    public void setWebsiteMetaOgImg(String  websiteMetaOgImg){
        this.websiteMetaOgImg = websiteMetaOgImg ;
        this.modify("website_meta_og_img",websiteMetaOgImg);
    }

    /**
     * 设置 [WEBSITE_META_TITLE]
     */
    public void setWebsiteMetaTitle(String  websiteMetaTitle){
        this.websiteMetaTitle = websiteMetaTitle ;
        this.modify("website_meta_title",websiteMetaTitle);
    }

    /**
     * 设置 [DESCRIPTION]
     */
    public void setDescription(String  description){
        this.description = description ;
        this.modify("description",description);
    }

    /**
     * 设置 [BADGE_INNERRIGHT]
     */
    public void setBadgeInnerright(String  badgeInnerright){
        this.badgeInnerright = badgeInnerright ;
        this.modify("badge_innerright",badgeInnerright);
    }

    /**
     * 设置 [WEBSITE_META_DESCRIPTION]
     */
    public void setWebsiteMetaDescription(String  websiteMetaDescription){
        this.websiteMetaDescription = websiteMetaDescription ;
        this.modify("website_meta_description",websiteMetaDescription);
    }

    /**
     * 设置 [SEATS_MIN]
     */
    public void setSeatsMin(Integer  seatsMin){
        this.seatsMin = seatsMin ;
        this.modify("seats_min",seatsMin);
    }

    /**
     * 设置 [BADGE_BACK]
     */
    public void setBadgeBack(String  badgeBack){
        this.badgeBack = badgeBack ;
        this.modify("badge_back",badgeBack);
    }

    /**
     * 设置 [WEBSITE_META_KEYWORDS]
     */
    public void setWebsiteMetaKeywords(String  websiteMetaKeywords){
        this.websiteMetaKeywords = websiteMetaKeywords ;
        this.modify("website_meta_keywords",websiteMetaKeywords);
    }

    /**
     * 设置 [AUTO_CONFIRM]
     */
    public void setAutoConfirm(String  autoConfirm){
        this.autoConfirm = autoConfirm ;
        this.modify("auto_confirm",autoConfirm);
    }

    /**
     * 设置 [DATE_END]
     */
    public void setDateEnd(Timestamp  dateEnd){
        this.dateEnd = dateEnd ;
        this.modify("date_end",dateEnd);
    }

    /**
     * 设置 [NAME]
     */
    public void setName(String  name){
        this.name = name ;
        this.modify("name",name);
    }

    /**
     * 设置 [BADGE_INNERLEFT]
     */
    public void setBadgeInnerleft(String  badgeInnerleft){
        this.badgeInnerleft = badgeInnerleft ;
        this.modify("badge_innerleft",badgeInnerleft);
    }

    /**
     * 设置 [SEATS_RESERVED]
     */
    public void setSeatsReserved(Integer  seatsReserved){
        this.seatsReserved = seatsReserved ;
        this.modify("seats_reserved",seatsReserved);
    }

    /**
     * 设置 [WEBSITE_MENU]
     */
    public void setWebsiteMenu(String  websiteMenu){
        this.websiteMenu = websiteMenu ;
        this.modify("website_menu",websiteMenu);
    }

    /**
     * 设置 [WEBSITE_ID]
     */
    public void setWebsiteId(Integer  websiteId){
        this.websiteId = websiteId ;
        this.modify("website_id",websiteId);
    }

    /**
     * 设置 [ACTIVE]
     */
    public void setActive(String  active){
        this.active = active ;
        this.modify("active",active);
    }

    /**
     * 设置 [SEATS_AVAILABLE]
     */
    public void setSeatsAvailable(Integer  seatsAvailable){
        this.seatsAvailable = seatsAvailable ;
        this.modify("seats_available",seatsAvailable);
    }

    /**
     * 设置 [IS_PUBLISHED]
     */
    public void setIsPublished(String  isPublished){
        this.isPublished = isPublished ;
        this.modify("is_published",isPublished);
    }

    /**
     * 设置 [IS_ONLINE]
     */
    public void setIsOnline(String  isOnline){
        this.isOnline = isOnline ;
        this.modify("is_online",isOnline);
    }

    /**
     * 设置 [DATE_TZ]
     */
    public void setDateTz(String  dateTz){
        this.dateTz = dateTz ;
        this.modify("date_tz",dateTz);
    }

    /**
     * 设置 [EVENT_LOGO]
     */
    public void setEventLogo(String  eventLogo){
        this.eventLogo = eventLogo ;
        this.modify("event_logo",eventLogo);
    }

    /**
     * 设置 [COLOR]
     */
    public void setColor(Integer  color){
        this.color = color ;
        this.modify("color",color);
    }

    /**
     * 设置 [USER_ID]
     */
    public void setUserId(Integer  userId){
        this.userId = userId ;
        this.modify("user_id",userId);
    }

    /**
     * 设置 [COUNTRY_ID]
     */
    public void setCountryId(Integer  countryId){
        this.countryId = countryId ;
        this.modify("country_id",countryId);
    }

    /**
     * 设置 [COMPANY_ID]
     */
    public void setCompanyId(Integer  companyId){
        this.companyId = companyId ;
        this.modify("company_id",companyId);
    }

    /**
     * 设置 [ORGANIZER_ID]
     */
    public void setOrganizerId(Integer  organizerId){
        this.organizerId = organizerId ;
        this.modify("organizer_id",organizerId);
    }

    /**
     * 设置 [MENU_ID]
     */
    public void setMenuId(Integer  menuId){
        this.menuId = menuId ;
        this.modify("menu_id",menuId);
    }

    /**
     * 设置 [ADDRESS_ID]
     */
    public void setAddressId(Integer  addressId){
        this.addressId = addressId ;
        this.modify("address_id",addressId);
    }

    /**
     * 设置 [EVENT_TYPE_ID]
     */
    public void setEventTypeId(Integer  eventTypeId){
        this.eventTypeId = eventTypeId ;
        this.modify("event_type_id",eventTypeId);
    }


}

