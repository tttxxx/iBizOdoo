package cn.ibizlab.odoo.odoo_purchase.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_purchase.dto.*;
import cn.ibizlab.odoo.odoo_purchase.mapping.*;
import cn.ibizlab.odoo.core.odoo_purchase.domain.Purchase_bill_union;
import cn.ibizlab.odoo.core.odoo_purchase.service.IPurchase_bill_unionService;
import cn.ibizlab.odoo.core.odoo_purchase.filter.Purchase_bill_unionSearchContext;




@Slf4j
@Api(tags = {"Purchase_bill_union" })
@RestController("odoo_purchase-purchase_bill_union")
@RequestMapping("")
public class Purchase_bill_unionResource {

    @Autowired
    private IPurchase_bill_unionService purchase_bill_unionService;

    @Autowired
    @Lazy
    private Purchase_bill_unionMapping purchase_bill_unionMapping;




    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Purchase_bill_union" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/purchase_bill_unions")

    public ResponseEntity<Purchase_bill_unionDTO> create(@RequestBody Purchase_bill_unionDTO purchase_bill_uniondto) {
        Purchase_bill_union domain = purchase_bill_unionMapping.toDomain(purchase_bill_uniondto);
		purchase_bill_unionService.create(domain);
        Purchase_bill_unionDTO dto = purchase_bill_unionMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Purchase_bill_union" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/purchase_bill_unions/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Purchase_bill_unionDTO> purchase_bill_uniondtos) {
        purchase_bill_unionService.createBatch(purchase_bill_unionMapping.toDomain(purchase_bill_uniondtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }







    @PreAuthorize("hasPermission(#purchase_bill_union_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Purchase_bill_union" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/purchase_bill_unions/{purchase_bill_union_id}")
    public ResponseEntity<Purchase_bill_unionDTO> get(@PathVariable("purchase_bill_union_id") Integer purchase_bill_union_id) {
        Purchase_bill_union domain = purchase_bill_unionService.get(purchase_bill_union_id);
        Purchase_bill_unionDTO dto = purchase_bill_unionMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }







    @PreAuthorize("hasPermission(#purchase_bill_union_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Purchase_bill_union" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/purchase_bill_unions/{purchase_bill_union_id}")

    public ResponseEntity<Purchase_bill_unionDTO> update(@PathVariable("purchase_bill_union_id") Integer purchase_bill_union_id, @RequestBody Purchase_bill_unionDTO purchase_bill_uniondto) {
		Purchase_bill_union domain = purchase_bill_unionMapping.toDomain(purchase_bill_uniondto);
        domain.setId(purchase_bill_union_id);
		purchase_bill_unionService.update(domain);
		Purchase_bill_unionDTO dto = purchase_bill_unionMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#purchase_bill_union_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Purchase_bill_union" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/purchase_bill_unions/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Purchase_bill_unionDTO> purchase_bill_uniondtos) {
        purchase_bill_unionService.updateBatch(purchase_bill_unionMapping.toDomain(purchase_bill_uniondtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }







    @PreAuthorize("hasPermission('Remove',{#purchase_bill_union_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Purchase_bill_union" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/purchase_bill_unions/{purchase_bill_union_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("purchase_bill_union_id") Integer purchase_bill_union_id) {
         return ResponseEntity.status(HttpStatus.OK).body(purchase_bill_unionService.remove(purchase_bill_union_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Purchase_bill_union" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/purchase_bill_unions/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        purchase_bill_unionService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Purchase_bill_union" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/purchase_bill_unions/fetchdefault")
	public ResponseEntity<List<Purchase_bill_unionDTO>> fetchDefault(Purchase_bill_unionSearchContext context) {
        Page<Purchase_bill_union> domains = purchase_bill_unionService.searchDefault(context) ;
        List<Purchase_bill_unionDTO> list = purchase_bill_unionMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Purchase_bill_union" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/purchase_bill_unions/searchdefault")
	public ResponseEntity<Page<Purchase_bill_unionDTO>> searchDefault(Purchase_bill_unionSearchContext context) {
        Page<Purchase_bill_union> domains = purchase_bill_unionService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(purchase_bill_unionMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Purchase_bill_union getEntity(){
        return new Purchase_bill_union();
    }

}
