package cn.ibizlab.odoo.odoo_purchase.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_purchase.dto.*;
import cn.ibizlab.odoo.odoo_purchase.mapping.*;
import cn.ibizlab.odoo.core.odoo_purchase.domain.Purchase_report;
import cn.ibizlab.odoo.core.odoo_purchase.service.IPurchase_reportService;
import cn.ibizlab.odoo.core.odoo_purchase.filter.Purchase_reportSearchContext;




@Slf4j
@Api(tags = {"Purchase_report" })
@RestController("odoo_purchase-purchase_report")
@RequestMapping("")
public class Purchase_reportResource {

    @Autowired
    private IPurchase_reportService purchase_reportService;

    @Autowired
    @Lazy
    private Purchase_reportMapping purchase_reportMapping;




    @PreAuthorize("hasPermission(#purchase_report_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Purchase_report" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/purchase_reports/{purchase_report_id}")

    public ResponseEntity<Purchase_reportDTO> update(@PathVariable("purchase_report_id") Integer purchase_report_id, @RequestBody Purchase_reportDTO purchase_reportdto) {
		Purchase_report domain = purchase_reportMapping.toDomain(purchase_reportdto);
        domain.setId(purchase_report_id);
		purchase_reportService.update(domain);
		Purchase_reportDTO dto = purchase_reportMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#purchase_report_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Purchase_report" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/purchase_reports/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Purchase_reportDTO> purchase_reportdtos) {
        purchase_reportService.updateBatch(purchase_reportMapping.toDomain(purchase_reportdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Purchase_report" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/purchase_reports")

    public ResponseEntity<Purchase_reportDTO> create(@RequestBody Purchase_reportDTO purchase_reportdto) {
        Purchase_report domain = purchase_reportMapping.toDomain(purchase_reportdto);
		purchase_reportService.create(domain);
        Purchase_reportDTO dto = purchase_reportMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Purchase_report" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/purchase_reports/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Purchase_reportDTO> purchase_reportdtos) {
        purchase_reportService.createBatch(purchase_reportMapping.toDomain(purchase_reportdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission('Remove',{#purchase_report_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Purchase_report" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/purchase_reports/{purchase_report_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("purchase_report_id") Integer purchase_report_id) {
         return ResponseEntity.status(HttpStatus.OK).body(purchase_reportService.remove(purchase_report_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Purchase_report" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/purchase_reports/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        purchase_reportService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission(#purchase_report_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Purchase_report" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/purchase_reports/{purchase_report_id}")
    public ResponseEntity<Purchase_reportDTO> get(@PathVariable("purchase_report_id") Integer purchase_report_id) {
        Purchase_report domain = purchase_reportService.get(purchase_report_id);
        Purchase_reportDTO dto = purchase_reportMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }










    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Purchase_report" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/purchase_reports/fetchdefault")
	public ResponseEntity<List<Purchase_reportDTO>> fetchDefault(Purchase_reportSearchContext context) {
        Page<Purchase_report> domains = purchase_reportService.searchDefault(context) ;
        List<Purchase_reportDTO> list = purchase_reportMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Purchase_report" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/purchase_reports/searchdefault")
	public ResponseEntity<Page<Purchase_reportDTO>> searchDefault(Purchase_reportSearchContext context) {
        Page<Purchase_report> domains = purchase_reportService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(purchase_reportMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Purchase_report getEntity(){
        return new Purchase_report();
    }

}
