package cn.ibizlab.odoo.odoo_purchase.mapping;

import org.mapstruct.*;
import cn.ibizlab.odoo.core.odoo_purchase.domain.Purchase_bill_union;
import cn.ibizlab.odoo.odoo_purchase.dto.Purchase_bill_unionDTO;
import cn.ibizlab.odoo.util.domain.MappingBase;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring", uses = {},
    nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
    nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface Purchase_bill_unionMapping extends MappingBase<Purchase_bill_unionDTO, Purchase_bill_union> {


}

