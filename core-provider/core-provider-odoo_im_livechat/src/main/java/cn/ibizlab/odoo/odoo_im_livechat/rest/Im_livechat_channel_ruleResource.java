package cn.ibizlab.odoo.odoo_im_livechat.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_im_livechat.dto.*;
import cn.ibizlab.odoo.odoo_im_livechat.mapping.*;
import cn.ibizlab.odoo.core.odoo_im_livechat.domain.Im_livechat_channel_rule;
import cn.ibizlab.odoo.core.odoo_im_livechat.service.IIm_livechat_channel_ruleService;
import cn.ibizlab.odoo.core.odoo_im_livechat.filter.Im_livechat_channel_ruleSearchContext;




@Slf4j
@Api(tags = {"Im_livechat_channel_rule" })
@RestController("odoo_im_livechat-im_livechat_channel_rule")
@RequestMapping("")
public class Im_livechat_channel_ruleResource {

    @Autowired
    private IIm_livechat_channel_ruleService im_livechat_channel_ruleService;

    @Autowired
    @Lazy
    private Im_livechat_channel_ruleMapping im_livechat_channel_ruleMapping;







    @PreAuthorize("hasPermission(#im_livechat_channel_rule_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Im_livechat_channel_rule" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/im_livechat_channel_rules/{im_livechat_channel_rule_id}")
    public ResponseEntity<Im_livechat_channel_ruleDTO> get(@PathVariable("im_livechat_channel_rule_id") Integer im_livechat_channel_rule_id) {
        Im_livechat_channel_rule domain = im_livechat_channel_ruleService.get(im_livechat_channel_rule_id);
        Im_livechat_channel_ruleDTO dto = im_livechat_channel_ruleMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }







    @PreAuthorize("hasPermission(#im_livechat_channel_rule_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Im_livechat_channel_rule" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/im_livechat_channel_rules/{im_livechat_channel_rule_id}")

    public ResponseEntity<Im_livechat_channel_ruleDTO> update(@PathVariable("im_livechat_channel_rule_id") Integer im_livechat_channel_rule_id, @RequestBody Im_livechat_channel_ruleDTO im_livechat_channel_ruledto) {
		Im_livechat_channel_rule domain = im_livechat_channel_ruleMapping.toDomain(im_livechat_channel_ruledto);
        domain.setId(im_livechat_channel_rule_id);
		im_livechat_channel_ruleService.update(domain);
		Im_livechat_channel_ruleDTO dto = im_livechat_channel_ruleMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#im_livechat_channel_rule_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Im_livechat_channel_rule" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/im_livechat_channel_rules/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Im_livechat_channel_ruleDTO> im_livechat_channel_ruledtos) {
        im_livechat_channel_ruleService.updateBatch(im_livechat_channel_ruleMapping.toDomain(im_livechat_channel_ruledtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Im_livechat_channel_rule" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/im_livechat_channel_rules")

    public ResponseEntity<Im_livechat_channel_ruleDTO> create(@RequestBody Im_livechat_channel_ruleDTO im_livechat_channel_ruledto) {
        Im_livechat_channel_rule domain = im_livechat_channel_ruleMapping.toDomain(im_livechat_channel_ruledto);
		im_livechat_channel_ruleService.create(domain);
        Im_livechat_channel_ruleDTO dto = im_livechat_channel_ruleMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Im_livechat_channel_rule" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/im_livechat_channel_rules/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Im_livechat_channel_ruleDTO> im_livechat_channel_ruledtos) {
        im_livechat_channel_ruleService.createBatch(im_livechat_channel_ruleMapping.toDomain(im_livechat_channel_ruledtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission('Remove',{#im_livechat_channel_rule_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Im_livechat_channel_rule" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/im_livechat_channel_rules/{im_livechat_channel_rule_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("im_livechat_channel_rule_id") Integer im_livechat_channel_rule_id) {
         return ResponseEntity.status(HttpStatus.OK).body(im_livechat_channel_ruleService.remove(im_livechat_channel_rule_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Im_livechat_channel_rule" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/im_livechat_channel_rules/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        im_livechat_channel_ruleService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Im_livechat_channel_rule" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/im_livechat_channel_rules/fetchdefault")
	public ResponseEntity<List<Im_livechat_channel_ruleDTO>> fetchDefault(Im_livechat_channel_ruleSearchContext context) {
        Page<Im_livechat_channel_rule> domains = im_livechat_channel_ruleService.searchDefault(context) ;
        List<Im_livechat_channel_ruleDTO> list = im_livechat_channel_ruleMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Im_livechat_channel_rule" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/im_livechat_channel_rules/searchdefault")
	public ResponseEntity<Page<Im_livechat_channel_ruleDTO>> searchDefault(Im_livechat_channel_ruleSearchContext context) {
        Page<Im_livechat_channel_rule> domains = im_livechat_channel_ruleService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(im_livechat_channel_ruleMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Im_livechat_channel_rule getEntity(){
        return new Im_livechat_channel_rule();
    }

}
