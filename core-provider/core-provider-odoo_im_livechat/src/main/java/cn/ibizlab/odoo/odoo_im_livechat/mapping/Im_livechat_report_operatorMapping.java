package cn.ibizlab.odoo.odoo_im_livechat.mapping;

import org.mapstruct.*;
import cn.ibizlab.odoo.core.odoo_im_livechat.domain.Im_livechat_report_operator;
import cn.ibizlab.odoo.odoo_im_livechat.dto.Im_livechat_report_operatorDTO;
import cn.ibizlab.odoo.util.domain.MappingBase;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring", uses = {},
    nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
    nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface Im_livechat_report_operatorMapping extends MappingBase<Im_livechat_report_operatorDTO, Im_livechat_report_operator> {


}

