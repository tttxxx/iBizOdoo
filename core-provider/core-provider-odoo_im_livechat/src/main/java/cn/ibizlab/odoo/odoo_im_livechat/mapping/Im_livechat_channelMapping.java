package cn.ibizlab.odoo.odoo_im_livechat.mapping;

import org.mapstruct.*;
import cn.ibizlab.odoo.core.odoo_im_livechat.domain.Im_livechat_channel;
import cn.ibizlab.odoo.odoo_im_livechat.dto.Im_livechat_channelDTO;
import cn.ibizlab.odoo.util.domain.MappingBase;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring", uses = {},
    nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
    nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface Im_livechat_channelMapping extends MappingBase<Im_livechat_channelDTO, Im_livechat_channel> {


}

