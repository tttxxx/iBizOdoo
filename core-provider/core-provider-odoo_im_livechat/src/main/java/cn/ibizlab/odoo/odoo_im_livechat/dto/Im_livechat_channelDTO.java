package cn.ibizlab.odoo.odoo_im_livechat.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;
import cn.ibizlab.odoo.util.domain.DTOBase;
import lombok.Data;

/**
 * 服务DTO对象[Im_livechat_channelDTO]
 */
@Data
public class Im_livechat_channelDTO extends DTOBase implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [USER_IDS]
     *
     */
    @JSONField(name = "user_ids")
    @JsonProperty("user_ids")
    private String userIds;

    /**
     * 属性 [WEBSITE_URL]
     *
     */
    @JSONField(name = "website_url")
    @JsonProperty("website_url")
    private String websiteUrl;

    /**
     * 属性 [RULE_IDS]
     *
     */
    @JSONField(name = "rule_ids")
    @JsonProperty("rule_ids")
    private String ruleIds;

    /**
     * 属性 [IMAGE]
     *
     */
    @JSONField(name = "image")
    @JsonProperty("image")
    private byte[] image;

    /**
     * 属性 [WEB_PAGE]
     *
     */
    @JSONField(name = "web_page")
    @JsonProperty("web_page")
    private String webPage;

    /**
     * 属性 [IS_PUBLISHED]
     *
     */
    @JSONField(name = "is_published")
    @JsonProperty("is_published")
    private String isPublished;

    /**
     * 属性 [SCRIPT_EXTERNAL]
     *
     */
    @JSONField(name = "script_external")
    @JsonProperty("script_external")
    private String scriptExternal;

    /**
     * 属性 [NBR_CHANNEL]
     *
     */
    @JSONField(name = "nbr_channel")
    @JsonProperty("nbr_channel")
    private Integer nbrChannel;

    /**
     * 属性 [BUTTON_TEXT]
     *
     */
    @JSONField(name = "button_text")
    @JsonProperty("button_text")
    private String buttonText;

    /**
     * 属性 [WEBSITE_DESCRIPTION]
     *
     */
    @JSONField(name = "website_description")
    @JsonProperty("website_description")
    private String websiteDescription;

    /**
     * 属性 [CHANNEL_IDS]
     *
     */
    @JSONField(name = "channel_ids")
    @JsonProperty("channel_ids")
    private String channelIds;

    /**
     * 属性 [NAME]
     *
     */
    @JSONField(name = "name")
    @JsonProperty("name")
    private String name;

    /**
     * 属性 [WEBSITE_PUBLISHED]
     *
     */
    @JSONField(name = "website_published")
    @JsonProperty("website_published")
    private String websitePublished;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;

    /**
     * 属性 [CREATE_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    private String displayName;

    /**
     * 属性 [ID]
     *
     */
    @JSONField(name = "id")
    @JsonProperty("id")
    private Integer id;

    /**
     * 属性 [IMAGE_MEDIUM]
     *
     */
    @JSONField(name = "image_medium")
    @JsonProperty("image_medium")
    private byte[] imageMedium;

    /**
     * 属性 [RATING_PERCENTAGE_SATISFACTION]
     *
     */
    @JSONField(name = "rating_percentage_satisfaction")
    @JsonProperty("rating_percentage_satisfaction")
    private Integer ratingPercentageSatisfaction;

    /**
     * 属性 [INPUT_PLACEHOLDER]
     *
     */
    @JSONField(name = "input_placeholder")
    @JsonProperty("input_placeholder")
    private String inputPlaceholder;

    /**
     * 属性 [IMAGE_SMALL]
     *
     */
    @JSONField(name = "image_small")
    @JsonProperty("image_small")
    private byte[] imageSmall;

    /**
     * 属性 [ARE_YOU_INSIDE]
     *
     */
    @JSONField(name = "are_you_inside")
    @JsonProperty("are_you_inside")
    private String areYouInside;

    /**
     * 属性 [DEFAULT_MESSAGE]
     *
     */
    @JSONField(name = "default_message")
    @JsonProperty("default_message")
    private String defaultMessage;

    /**
     * 属性 [WRITE_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;

    /**
     * 属性 [WRITE_UID_TEXT]
     *
     */
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    private String writeUidText;

    /**
     * 属性 [CREATE_UID_TEXT]
     *
     */
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    private String createUidText;

    /**
     * 属性 [WRITE_UID]
     *
     */
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    private Integer writeUid;

    /**
     * 属性 [CREATE_UID]
     *
     */
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    private Integer createUid;


    /**
     * 设置 [IS_PUBLISHED]
     */
    public void setIsPublished(String  isPublished){
        this.isPublished = isPublished ;
        this.modify("is_published",isPublished);
    }

    /**
     * 设置 [BUTTON_TEXT]
     */
    public void setButtonText(String  buttonText){
        this.buttonText = buttonText ;
        this.modify("button_text",buttonText);
    }

    /**
     * 设置 [WEBSITE_DESCRIPTION]
     */
    public void setWebsiteDescription(String  websiteDescription){
        this.websiteDescription = websiteDescription ;
        this.modify("website_description",websiteDescription);
    }

    /**
     * 设置 [NAME]
     */
    public void setName(String  name){
        this.name = name ;
        this.modify("name",name);
    }

    /**
     * 设置 [INPUT_PLACEHOLDER]
     */
    public void setInputPlaceholder(String  inputPlaceholder){
        this.inputPlaceholder = inputPlaceholder ;
        this.modify("input_placeholder",inputPlaceholder);
    }

    /**
     * 设置 [DEFAULT_MESSAGE]
     */
    public void setDefaultMessage(String  defaultMessage){
        this.defaultMessage = defaultMessage ;
        this.modify("default_message",defaultMessage);
    }


}

