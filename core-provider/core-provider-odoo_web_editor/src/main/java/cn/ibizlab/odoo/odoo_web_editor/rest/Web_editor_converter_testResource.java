package cn.ibizlab.odoo.odoo_web_editor.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_web_editor.dto.*;
import cn.ibizlab.odoo.odoo_web_editor.mapping.*;
import cn.ibizlab.odoo.core.odoo_web_editor.domain.Web_editor_converter_test;
import cn.ibizlab.odoo.core.odoo_web_editor.service.IWeb_editor_converter_testService;
import cn.ibizlab.odoo.core.odoo_web_editor.filter.Web_editor_converter_testSearchContext;




@Slf4j
@Api(tags = {"Web_editor_converter_test" })
@RestController("odoo_web_editor-web_editor_converter_test")
@RequestMapping("")
public class Web_editor_converter_testResource {

    @Autowired
    private IWeb_editor_converter_testService web_editor_converter_testService;

    @Autowired
    @Lazy
    private Web_editor_converter_testMapping web_editor_converter_testMapping;




    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Web_editor_converter_test" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/web_editor_converter_tests")

    public ResponseEntity<Web_editor_converter_testDTO> create(@RequestBody Web_editor_converter_testDTO web_editor_converter_testdto) {
        Web_editor_converter_test domain = web_editor_converter_testMapping.toDomain(web_editor_converter_testdto);
		web_editor_converter_testService.create(domain);
        Web_editor_converter_testDTO dto = web_editor_converter_testMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Web_editor_converter_test" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/web_editor_converter_tests/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Web_editor_converter_testDTO> web_editor_converter_testdtos) {
        web_editor_converter_testService.createBatch(web_editor_converter_testMapping.toDomain(web_editor_converter_testdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }







    @PreAuthorize("hasPermission(#web_editor_converter_test_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Web_editor_converter_test" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/web_editor_converter_tests/{web_editor_converter_test_id}")
    public ResponseEntity<Web_editor_converter_testDTO> get(@PathVariable("web_editor_converter_test_id") Integer web_editor_converter_test_id) {
        Web_editor_converter_test domain = web_editor_converter_testService.get(web_editor_converter_test_id);
        Web_editor_converter_testDTO dto = web_editor_converter_testMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }







    @PreAuthorize("hasPermission('Remove',{#web_editor_converter_test_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Web_editor_converter_test" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/web_editor_converter_tests/{web_editor_converter_test_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("web_editor_converter_test_id") Integer web_editor_converter_test_id) {
         return ResponseEntity.status(HttpStatus.OK).body(web_editor_converter_testService.remove(web_editor_converter_test_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Web_editor_converter_test" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/web_editor_converter_tests/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        web_editor_converter_testService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }







    @PreAuthorize("hasPermission(#web_editor_converter_test_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Web_editor_converter_test" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/web_editor_converter_tests/{web_editor_converter_test_id}")

    public ResponseEntity<Web_editor_converter_testDTO> update(@PathVariable("web_editor_converter_test_id") Integer web_editor_converter_test_id, @RequestBody Web_editor_converter_testDTO web_editor_converter_testdto) {
		Web_editor_converter_test domain = web_editor_converter_testMapping.toDomain(web_editor_converter_testdto);
        domain.setId(web_editor_converter_test_id);
		web_editor_converter_testService.update(domain);
		Web_editor_converter_testDTO dto = web_editor_converter_testMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#web_editor_converter_test_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Web_editor_converter_test" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/web_editor_converter_tests/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Web_editor_converter_testDTO> web_editor_converter_testdtos) {
        web_editor_converter_testService.updateBatch(web_editor_converter_testMapping.toDomain(web_editor_converter_testdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Web_editor_converter_test" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/web_editor_converter_tests/fetchdefault")
	public ResponseEntity<List<Web_editor_converter_testDTO>> fetchDefault(Web_editor_converter_testSearchContext context) {
        Page<Web_editor_converter_test> domains = web_editor_converter_testService.searchDefault(context) ;
        List<Web_editor_converter_testDTO> list = web_editor_converter_testMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Web_editor_converter_test" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/web_editor_converter_tests/searchdefault")
	public ResponseEntity<Page<Web_editor_converter_testDTO>> searchDefault(Web_editor_converter_testSearchContext context) {
        Page<Web_editor_converter_test> domains = web_editor_converter_testService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(web_editor_converter_testMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Web_editor_converter_test getEntity(){
        return new Web_editor_converter_test();
    }

}
