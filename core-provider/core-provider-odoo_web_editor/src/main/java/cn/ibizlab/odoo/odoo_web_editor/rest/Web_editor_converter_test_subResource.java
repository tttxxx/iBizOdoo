package cn.ibizlab.odoo.odoo_web_editor.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_web_editor.dto.*;
import cn.ibizlab.odoo.odoo_web_editor.mapping.*;
import cn.ibizlab.odoo.core.odoo_web_editor.domain.Web_editor_converter_test_sub;
import cn.ibizlab.odoo.core.odoo_web_editor.service.IWeb_editor_converter_test_subService;
import cn.ibizlab.odoo.core.odoo_web_editor.filter.Web_editor_converter_test_subSearchContext;




@Slf4j
@Api(tags = {"Web_editor_converter_test_sub" })
@RestController("odoo_web_editor-web_editor_converter_test_sub")
@RequestMapping("")
public class Web_editor_converter_test_subResource {

    @Autowired
    private IWeb_editor_converter_test_subService web_editor_converter_test_subService;

    @Autowired
    @Lazy
    private Web_editor_converter_test_subMapping web_editor_converter_test_subMapping;







    @PreAuthorize("hasPermission(#web_editor_converter_test_sub_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Web_editor_converter_test_sub" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/web_editor_converter_test_subs/{web_editor_converter_test_sub_id}")

    public ResponseEntity<Web_editor_converter_test_subDTO> update(@PathVariable("web_editor_converter_test_sub_id") Integer web_editor_converter_test_sub_id, @RequestBody Web_editor_converter_test_subDTO web_editor_converter_test_subdto) {
		Web_editor_converter_test_sub domain = web_editor_converter_test_subMapping.toDomain(web_editor_converter_test_subdto);
        domain.setId(web_editor_converter_test_sub_id);
		web_editor_converter_test_subService.update(domain);
		Web_editor_converter_test_subDTO dto = web_editor_converter_test_subMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#web_editor_converter_test_sub_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Web_editor_converter_test_sub" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/web_editor_converter_test_subs/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Web_editor_converter_test_subDTO> web_editor_converter_test_subdtos) {
        web_editor_converter_test_subService.updateBatch(web_editor_converter_test_subMapping.toDomain(web_editor_converter_test_subdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission(#web_editor_converter_test_sub_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Web_editor_converter_test_sub" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/web_editor_converter_test_subs/{web_editor_converter_test_sub_id}")
    public ResponseEntity<Web_editor_converter_test_subDTO> get(@PathVariable("web_editor_converter_test_sub_id") Integer web_editor_converter_test_sub_id) {
        Web_editor_converter_test_sub domain = web_editor_converter_test_subService.get(web_editor_converter_test_sub_id);
        Web_editor_converter_test_subDTO dto = web_editor_converter_test_subMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }




    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Web_editor_converter_test_sub" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/web_editor_converter_test_subs")

    public ResponseEntity<Web_editor_converter_test_subDTO> create(@RequestBody Web_editor_converter_test_subDTO web_editor_converter_test_subdto) {
        Web_editor_converter_test_sub domain = web_editor_converter_test_subMapping.toDomain(web_editor_converter_test_subdto);
		web_editor_converter_test_subService.create(domain);
        Web_editor_converter_test_subDTO dto = web_editor_converter_test_subMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Web_editor_converter_test_sub" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/web_editor_converter_test_subs/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Web_editor_converter_test_subDTO> web_editor_converter_test_subdtos) {
        web_editor_converter_test_subService.createBatch(web_editor_converter_test_subMapping.toDomain(web_editor_converter_test_subdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }










    @PreAuthorize("hasPermission('Remove',{#web_editor_converter_test_sub_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Web_editor_converter_test_sub" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/web_editor_converter_test_subs/{web_editor_converter_test_sub_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("web_editor_converter_test_sub_id") Integer web_editor_converter_test_sub_id) {
         return ResponseEntity.status(HttpStatus.OK).body(web_editor_converter_test_subService.remove(web_editor_converter_test_sub_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Web_editor_converter_test_sub" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/web_editor_converter_test_subs/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        web_editor_converter_test_subService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Web_editor_converter_test_sub" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/web_editor_converter_test_subs/fetchdefault")
	public ResponseEntity<List<Web_editor_converter_test_subDTO>> fetchDefault(Web_editor_converter_test_subSearchContext context) {
        Page<Web_editor_converter_test_sub> domains = web_editor_converter_test_subService.searchDefault(context) ;
        List<Web_editor_converter_test_subDTO> list = web_editor_converter_test_subMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Web_editor_converter_test_sub" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/web_editor_converter_test_subs/searchdefault")
	public ResponseEntity<Page<Web_editor_converter_test_subDTO>> searchDefault(Web_editor_converter_test_subSearchContext context) {
        Page<Web_editor_converter_test_sub> domains = web_editor_converter_test_subService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(web_editor_converter_test_subMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Web_editor_converter_test_sub getEntity(){
        return new Web_editor_converter_test_sub();
    }

}
