package cn.ibizlab.odoo.odoo_web_editor.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import lombok.Data;

@ConfigurationProperties(prefix = "service.odoo-web-editor")
@Data
public class odoo_web_editorServiceProperties {

	private boolean enabled;

	private boolean auth;


}