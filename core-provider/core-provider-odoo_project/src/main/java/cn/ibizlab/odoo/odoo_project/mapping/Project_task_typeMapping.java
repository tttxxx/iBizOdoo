package cn.ibizlab.odoo.odoo_project.mapping;

import org.mapstruct.*;
import cn.ibizlab.odoo.core.odoo_project.domain.Project_task_type;
import cn.ibizlab.odoo.odoo_project.dto.Project_task_typeDTO;
import cn.ibizlab.odoo.util.domain.MappingBase;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring", uses = {},
    nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
    nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface Project_task_typeMapping extends MappingBase<Project_task_typeDTO, Project_task_type> {


}

