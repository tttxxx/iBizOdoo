package cn.ibizlab.odoo.odoo_project.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_project.dto.*;
import cn.ibizlab.odoo.odoo_project.mapping.*;
import cn.ibizlab.odoo.core.odoo_project.domain.Project_task;
import cn.ibizlab.odoo.core.odoo_project.service.IProject_taskService;
import cn.ibizlab.odoo.core.odoo_project.filter.Project_taskSearchContext;




@Slf4j
@Api(tags = {"Project_task" })
@RestController("odoo_project-project_task")
@RequestMapping("")
public class Project_taskResource {

    @Autowired
    private IProject_taskService project_taskService;

    @Autowired
    @Lazy
    private Project_taskMapping project_taskMapping;




    @PreAuthorize("hasPermission(#project_task_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Project_task" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/project_tasks/{project_task_id}")

    public ResponseEntity<Project_taskDTO> update(@PathVariable("project_task_id") Integer project_task_id, @RequestBody Project_taskDTO project_taskdto) {
		Project_task domain = project_taskMapping.toDomain(project_taskdto);
        domain.setId(project_task_id);
		project_taskService.update(domain);
		Project_taskDTO dto = project_taskMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#project_task_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Project_task" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/project_tasks/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Project_taskDTO> project_taskdtos) {
        project_taskService.updateBatch(project_taskMapping.toDomain(project_taskdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission(#project_task_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Project_task" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/project_tasks/{project_task_id}")
    public ResponseEntity<Project_taskDTO> get(@PathVariable("project_task_id") Integer project_task_id) {
        Project_task domain = project_taskService.get(project_task_id);
        Project_taskDTO dto = project_taskMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }







    @PreAuthorize("hasPermission('Remove',{#project_task_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Project_task" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/project_tasks/{project_task_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("project_task_id") Integer project_task_id) {
         return ResponseEntity.status(HttpStatus.OK).body(project_taskService.remove(project_task_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Project_task" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/project_tasks/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        project_taskService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Project_task" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/project_tasks")

    public ResponseEntity<Project_taskDTO> create(@RequestBody Project_taskDTO project_taskdto) {
        Project_task domain = project_taskMapping.toDomain(project_taskdto);
		project_taskService.create(domain);
        Project_taskDTO dto = project_taskMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Project_task" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/project_tasks/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Project_taskDTO> project_taskdtos) {
        project_taskService.createBatch(project_taskMapping.toDomain(project_taskdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }







    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Project_task" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/project_tasks/fetchdefault")
	public ResponseEntity<List<Project_taskDTO>> fetchDefault(Project_taskSearchContext context) {
        Page<Project_task> domains = project_taskService.searchDefault(context) ;
        List<Project_taskDTO> list = project_taskMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Project_task" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/project_tasks/searchdefault")
	public ResponseEntity<Page<Project_taskDTO>> searchDefault(Project_taskSearchContext context) {
        Page<Project_task> domains = project_taskService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(project_taskMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Project_task getEntity(){
        return new Project_task();
    }

}
