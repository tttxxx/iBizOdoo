package cn.ibizlab.odoo.odoo_project.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;
import cn.ibizlab.odoo.util.domain.DTOBase;
import lombok.Data;

/**
 * 服务DTO对象[Project_task_typeDTO]
 */
@Data
public class Project_task_typeDTO extends DTOBase implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [NAME]
     *
     */
    @JSONField(name = "name")
    @JsonProperty("name")
    private String name;

    /**
     * 属性 [AUTO_VALIDATION_KANBAN_STATE]
     *
     */
    @JSONField(name = "auto_validation_kanban_state")
    @JsonProperty("auto_validation_kanban_state")
    private String autoValidationKanbanState;

    /**
     * 属性 [WRITE_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    private String displayName;

    /**
     * 属性 [ID]
     *
     */
    @JSONField(name = "id")
    @JsonProperty("id")
    private Integer id;

    /**
     * 属性 [PROJECT_IDS]
     *
     */
    @JSONField(name = "project_ids")
    @JsonProperty("project_ids")
    private String projectIds;

    /**
     * 属性 [SEQUENCE]
     *
     */
    @JSONField(name = "sequence")
    @JsonProperty("sequence")
    private Integer sequence;

    /**
     * 属性 [DESCRIPTION]
     *
     */
    @JSONField(name = "description")
    @JsonProperty("description")
    private String description;

    /**
     * 属性 [LEGEND_NORMAL]
     *
     */
    @JSONField(name = "legend_normal")
    @JsonProperty("legend_normal")
    private String legendNormal;

    /**
     * 属性 [FOLD]
     *
     */
    @JSONField(name = "fold")
    @JsonProperty("fold")
    private String fold;

    /**
     * 属性 [CREATE_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;

    /**
     * 属性 [LEGEND_BLOCKED]
     *
     */
    @JSONField(name = "legend_blocked")
    @JsonProperty("legend_blocked")
    private String legendBlocked;

    /**
     * 属性 [LEGEND_PRIORITY]
     *
     */
    @JSONField(name = "legend_priority")
    @JsonProperty("legend_priority")
    private String legendPriority;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;

    /**
     * 属性 [LEGEND_DONE]
     *
     */
    @JSONField(name = "legend_done")
    @JsonProperty("legend_done")
    private String legendDone;

    /**
     * 属性 [CREATE_UID_TEXT]
     *
     */
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    private String createUidText;

    /**
     * 属性 [RATING_TEMPLATE_ID_TEXT]
     *
     */
    @JSONField(name = "rating_template_id_text")
    @JsonProperty("rating_template_id_text")
    private String ratingTemplateIdText;

    /**
     * 属性 [WRITE_UID_TEXT]
     *
     */
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    private String writeUidText;

    /**
     * 属性 [MAIL_TEMPLATE_ID_TEXT]
     *
     */
    @JSONField(name = "mail_template_id_text")
    @JsonProperty("mail_template_id_text")
    private String mailTemplateIdText;

    /**
     * 属性 [MAIL_TEMPLATE_ID]
     *
     */
    @JSONField(name = "mail_template_id")
    @JsonProperty("mail_template_id")
    private Integer mailTemplateId;

    /**
     * 属性 [CREATE_UID]
     *
     */
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    private Integer createUid;

    /**
     * 属性 [RATING_TEMPLATE_ID]
     *
     */
    @JSONField(name = "rating_template_id")
    @JsonProperty("rating_template_id")
    private Integer ratingTemplateId;

    /**
     * 属性 [WRITE_UID]
     *
     */
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    private Integer writeUid;


    /**
     * 设置 [NAME]
     */
    public void setName(String  name){
        this.name = name ;
        this.modify("name",name);
    }

    /**
     * 设置 [AUTO_VALIDATION_KANBAN_STATE]
     */
    public void setAutoValidationKanbanState(String  autoValidationKanbanState){
        this.autoValidationKanbanState = autoValidationKanbanState ;
        this.modify("auto_validation_kanban_state",autoValidationKanbanState);
    }

    /**
     * 设置 [SEQUENCE]
     */
    public void setSequence(Integer  sequence){
        this.sequence = sequence ;
        this.modify("sequence",sequence);
    }

    /**
     * 设置 [DESCRIPTION]
     */
    public void setDescription(String  description){
        this.description = description ;
        this.modify("description",description);
    }

    /**
     * 设置 [LEGEND_NORMAL]
     */
    public void setLegendNormal(String  legendNormal){
        this.legendNormal = legendNormal ;
        this.modify("legend_normal",legendNormal);
    }

    /**
     * 设置 [FOLD]
     */
    public void setFold(String  fold){
        this.fold = fold ;
        this.modify("fold",fold);
    }

    /**
     * 设置 [LEGEND_BLOCKED]
     */
    public void setLegendBlocked(String  legendBlocked){
        this.legendBlocked = legendBlocked ;
        this.modify("legend_blocked",legendBlocked);
    }

    /**
     * 设置 [LEGEND_PRIORITY]
     */
    public void setLegendPriority(String  legendPriority){
        this.legendPriority = legendPriority ;
        this.modify("legend_priority",legendPriority);
    }

    /**
     * 设置 [LEGEND_DONE]
     */
    public void setLegendDone(String  legendDone){
        this.legendDone = legendDone ;
        this.modify("legend_done",legendDone);
    }

    /**
     * 设置 [MAIL_TEMPLATE_ID]
     */
    public void setMailTemplateId(Integer  mailTemplateId){
        this.mailTemplateId = mailTemplateId ;
        this.modify("mail_template_id",mailTemplateId);
    }

    /**
     * 设置 [RATING_TEMPLATE_ID]
     */
    public void setRatingTemplateId(Integer  ratingTemplateId){
        this.ratingTemplateId = ratingTemplateId ;
        this.modify("rating_template_id",ratingTemplateId);
    }


}

