package cn.ibizlab.odoo.odoo_project.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_project.dto.*;
import cn.ibizlab.odoo.odoo_project.mapping.*;
import cn.ibizlab.odoo.core.odoo_project.domain.Project_project;
import cn.ibizlab.odoo.core.odoo_project.service.IProject_projectService;
import cn.ibizlab.odoo.core.odoo_project.filter.Project_projectSearchContext;




@Slf4j
@Api(tags = {"Project_project" })
@RestController("odoo_project-project_project")
@RequestMapping("")
public class Project_projectResource {

    @Autowired
    private IProject_projectService project_projectService;

    @Autowired
    @Lazy
    private Project_projectMapping project_projectMapping;




    @PreAuthorize("hasPermission(#project_project_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Project_project" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/project_projects/{project_project_id}")
    public ResponseEntity<Project_projectDTO> get(@PathVariable("project_project_id") Integer project_project_id) {
        Project_project domain = project_projectService.get(project_project_id);
        Project_projectDTO dto = project_projectMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }







    @PreAuthorize("hasPermission(#project_project_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Project_project" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/project_projects/{project_project_id}")

    public ResponseEntity<Project_projectDTO> update(@PathVariable("project_project_id") Integer project_project_id, @RequestBody Project_projectDTO project_projectdto) {
		Project_project domain = project_projectMapping.toDomain(project_projectdto);
        domain.setId(project_project_id);
		project_projectService.update(domain);
		Project_projectDTO dto = project_projectMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#project_project_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Project_project" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/project_projects/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Project_projectDTO> project_projectdtos) {
        project_projectService.updateBatch(project_projectMapping.toDomain(project_projectdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Project_project" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/project_projects")

    public ResponseEntity<Project_projectDTO> create(@RequestBody Project_projectDTO project_projectdto) {
        Project_project domain = project_projectMapping.toDomain(project_projectdto);
		project_projectService.create(domain);
        Project_projectDTO dto = project_projectMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Project_project" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/project_projects/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Project_projectDTO> project_projectdtos) {
        project_projectService.createBatch(project_projectMapping.toDomain(project_projectdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }







    @PreAuthorize("hasPermission('Remove',{#project_project_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Project_project" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/project_projects/{project_project_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("project_project_id") Integer project_project_id) {
         return ResponseEntity.status(HttpStatus.OK).body(project_projectService.remove(project_project_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Project_project" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/project_projects/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        project_projectService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Project_project" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/project_projects/fetchdefault")
	public ResponseEntity<List<Project_projectDTO>> fetchDefault(Project_projectSearchContext context) {
        Page<Project_project> domains = project_projectService.searchDefault(context) ;
        List<Project_projectDTO> list = project_projectMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Project_project" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/project_projects/searchdefault")
	public ResponseEntity<Page<Project_projectDTO>> searchDefault(Project_projectSearchContext context) {
        Page<Project_project> domains = project_projectService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(project_projectMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Project_project getEntity(){
        return new Project_project();
    }

}
