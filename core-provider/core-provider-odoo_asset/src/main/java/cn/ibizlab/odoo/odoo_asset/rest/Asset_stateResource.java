package cn.ibizlab.odoo.odoo_asset.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_asset.dto.*;
import cn.ibizlab.odoo.odoo_asset.mapping.*;
import cn.ibizlab.odoo.core.odoo_asset.domain.Asset_state;
import cn.ibizlab.odoo.core.odoo_asset.service.IAsset_stateService;
import cn.ibizlab.odoo.core.odoo_asset.filter.Asset_stateSearchContext;




@Slf4j
@Api(tags = {"Asset_state" })
@RestController("odoo_asset-asset_state")
@RequestMapping("")
public class Asset_stateResource {

    @Autowired
    private IAsset_stateService asset_stateService;

    @Autowired
    @Lazy
    private Asset_stateMapping asset_stateMapping;




    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Asset_state" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/asset_states")

    public ResponseEntity<Asset_stateDTO> create(@RequestBody Asset_stateDTO asset_statedto) {
        Asset_state domain = asset_stateMapping.toDomain(asset_statedto);
		asset_stateService.create(domain);
        Asset_stateDTO dto = asset_stateMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Asset_state" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/asset_states/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Asset_stateDTO> asset_statedtos) {
        asset_stateService.createBatch(asset_stateMapping.toDomain(asset_statedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }







    @PreAuthorize("hasPermission('Remove',{#asset_state_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Asset_state" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/asset_states/{asset_state_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("asset_state_id") Integer asset_state_id) {
         return ResponseEntity.status(HttpStatus.OK).body(asset_stateService.remove(asset_state_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Asset_state" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/asset_states/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        asset_stateService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }







    @PreAuthorize("hasPermission(#asset_state_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Asset_state" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/asset_states/{asset_state_id}")
    public ResponseEntity<Asset_stateDTO> get(@PathVariable("asset_state_id") Integer asset_state_id) {
        Asset_state domain = asset_stateService.get(asset_state_id);
        Asset_stateDTO dto = asset_stateMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }




    @PreAuthorize("hasPermission(#asset_state_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Asset_state" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/asset_states/{asset_state_id}")

    public ResponseEntity<Asset_stateDTO> update(@PathVariable("asset_state_id") Integer asset_state_id, @RequestBody Asset_stateDTO asset_statedto) {
		Asset_state domain = asset_stateMapping.toDomain(asset_statedto);
        domain.setId(asset_state_id);
		asset_stateService.update(domain);
		Asset_stateDTO dto = asset_stateMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#asset_state_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Asset_state" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/asset_states/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Asset_stateDTO> asset_statedtos) {
        asset_stateService.updateBatch(asset_stateMapping.toDomain(asset_statedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Asset_state" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/asset_states/fetchdefault")
	public ResponseEntity<List<Asset_stateDTO>> fetchDefault(Asset_stateSearchContext context) {
        Page<Asset_state> domains = asset_stateService.searchDefault(context) ;
        List<Asset_stateDTO> list = asset_stateMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Asset_state" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/asset_states/searchdefault")
	public ResponseEntity<Page<Asset_stateDTO>> searchDefault(Asset_stateSearchContext context) {
        Page<Asset_state> domains = asset_stateService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(asset_stateMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Asset_state getEntity(){
        return new Asset_state();
    }

}
