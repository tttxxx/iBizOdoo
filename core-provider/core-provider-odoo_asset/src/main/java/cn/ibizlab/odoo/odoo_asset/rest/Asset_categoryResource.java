package cn.ibizlab.odoo.odoo_asset.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_asset.dto.*;
import cn.ibizlab.odoo.odoo_asset.mapping.*;
import cn.ibizlab.odoo.core.odoo_asset.domain.Asset_category;
import cn.ibizlab.odoo.core.odoo_asset.service.IAsset_categoryService;
import cn.ibizlab.odoo.core.odoo_asset.filter.Asset_categorySearchContext;




@Slf4j
@Api(tags = {"Asset_category" })
@RestController("odoo_asset-asset_category")
@RequestMapping("")
public class Asset_categoryResource {

    @Autowired
    private IAsset_categoryService asset_categoryService;

    @Autowired
    @Lazy
    private Asset_categoryMapping asset_categoryMapping;




    @PreAuthorize("hasPermission(#asset_category_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Asset_category" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/asset_categories/{asset_category_id}")
    public ResponseEntity<Asset_categoryDTO> get(@PathVariable("asset_category_id") Integer asset_category_id) {
        Asset_category domain = asset_categoryService.get(asset_category_id);
        Asset_categoryDTO dto = asset_categoryMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }







    @PreAuthorize("hasPermission('Remove',{#asset_category_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Asset_category" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/asset_categories/{asset_category_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("asset_category_id") Integer asset_category_id) {
         return ResponseEntity.status(HttpStatus.OK).body(asset_categoryService.remove(asset_category_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Asset_category" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/asset_categories/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        asset_categoryService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }










    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Asset_category" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/asset_categories")

    public ResponseEntity<Asset_categoryDTO> create(@RequestBody Asset_categoryDTO asset_categorydto) {
        Asset_category domain = asset_categoryMapping.toDomain(asset_categorydto);
		asset_categoryService.create(domain);
        Asset_categoryDTO dto = asset_categoryMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Asset_category" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/asset_categories/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Asset_categoryDTO> asset_categorydtos) {
        asset_categoryService.createBatch(asset_categoryMapping.toDomain(asset_categorydtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission(#asset_category_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Asset_category" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/asset_categories/{asset_category_id}")

    public ResponseEntity<Asset_categoryDTO> update(@PathVariable("asset_category_id") Integer asset_category_id, @RequestBody Asset_categoryDTO asset_categorydto) {
		Asset_category domain = asset_categoryMapping.toDomain(asset_categorydto);
        domain.setId(asset_category_id);
		asset_categoryService.update(domain);
		Asset_categoryDTO dto = asset_categoryMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#asset_category_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Asset_category" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/asset_categories/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Asset_categoryDTO> asset_categorydtos) {
        asset_categoryService.updateBatch(asset_categoryMapping.toDomain(asset_categorydtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Asset_category" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/asset_categories/fetchdefault")
	public ResponseEntity<List<Asset_categoryDTO>> fetchDefault(Asset_categorySearchContext context) {
        Page<Asset_category> domains = asset_categoryService.searchDefault(context) ;
        List<Asset_categoryDTO> list = asset_categoryMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Asset_category" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/asset_categories/searchdefault")
	public ResponseEntity<Page<Asset_categoryDTO>> searchDefault(Asset_categorySearchContext context) {
        Page<Asset_category> domains = asset_categoryService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(asset_categoryMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Asset_category getEntity(){
        return new Asset_category();
    }

}
