package cn.ibizlab.odoo.odoo_asset.mapping;

import org.mapstruct.*;
import cn.ibizlab.odoo.core.odoo_asset.domain.Asset_asset;
import cn.ibizlab.odoo.odoo_asset.dto.Asset_assetDTO;
import cn.ibizlab.odoo.util.domain.MappingBase;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring", uses = {},
    nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
    nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface Asset_assetMapping extends MappingBase<Asset_assetDTO, Asset_asset> {


}

