package cn.ibizlab.odoo.odoo_barcodes.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_barcodes.dto.*;
import cn.ibizlab.odoo.odoo_barcodes.mapping.*;
import cn.ibizlab.odoo.core.odoo_barcodes.domain.Barcodes_barcode_events_mixin;
import cn.ibizlab.odoo.core.odoo_barcodes.service.IBarcodes_barcode_events_mixinService;
import cn.ibizlab.odoo.core.odoo_barcodes.filter.Barcodes_barcode_events_mixinSearchContext;




@Slf4j
@Api(tags = {"Barcodes_barcode_events_mixin" })
@RestController("odoo_barcodes-barcodes_barcode_events_mixin")
@RequestMapping("")
public class Barcodes_barcode_events_mixinResource {

    @Autowired
    private IBarcodes_barcode_events_mixinService barcodes_barcode_events_mixinService;

    @Autowired
    @Lazy
    private Barcodes_barcode_events_mixinMapping barcodes_barcode_events_mixinMapping;




    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Barcodes_barcode_events_mixin" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/barcodes_barcode_events_mixins")

    public ResponseEntity<Barcodes_barcode_events_mixinDTO> create(@RequestBody Barcodes_barcode_events_mixinDTO barcodes_barcode_events_mixindto) {
        Barcodes_barcode_events_mixin domain = barcodes_barcode_events_mixinMapping.toDomain(barcodes_barcode_events_mixindto);
		barcodes_barcode_events_mixinService.create(domain);
        Barcodes_barcode_events_mixinDTO dto = barcodes_barcode_events_mixinMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Barcodes_barcode_events_mixin" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/barcodes_barcode_events_mixins/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Barcodes_barcode_events_mixinDTO> barcodes_barcode_events_mixindtos) {
        barcodes_barcode_events_mixinService.createBatch(barcodes_barcode_events_mixinMapping.toDomain(barcodes_barcode_events_mixindtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }







    @PreAuthorize("hasPermission(#barcodes_barcode_events_mixin_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Barcodes_barcode_events_mixin" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/barcodes_barcode_events_mixins/{barcodes_barcode_events_mixin_id}")

    public ResponseEntity<Barcodes_barcode_events_mixinDTO> update(@PathVariable("barcodes_barcode_events_mixin_id") Integer barcodes_barcode_events_mixin_id, @RequestBody Barcodes_barcode_events_mixinDTO barcodes_barcode_events_mixindto) {
		Barcodes_barcode_events_mixin domain = barcodes_barcode_events_mixinMapping.toDomain(barcodes_barcode_events_mixindto);
        domain.setId(barcodes_barcode_events_mixin_id);
		barcodes_barcode_events_mixinService.update(domain);
		Barcodes_barcode_events_mixinDTO dto = barcodes_barcode_events_mixinMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#barcodes_barcode_events_mixin_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Barcodes_barcode_events_mixin" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/barcodes_barcode_events_mixins/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Barcodes_barcode_events_mixinDTO> barcodes_barcode_events_mixindtos) {
        barcodes_barcode_events_mixinService.updateBatch(barcodes_barcode_events_mixinMapping.toDomain(barcodes_barcode_events_mixindtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission('Remove',{#barcodes_barcode_events_mixin_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Barcodes_barcode_events_mixin" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/barcodes_barcode_events_mixins/{barcodes_barcode_events_mixin_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("barcodes_barcode_events_mixin_id") Integer barcodes_barcode_events_mixin_id) {
         return ResponseEntity.status(HttpStatus.OK).body(barcodes_barcode_events_mixinService.remove(barcodes_barcode_events_mixin_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Barcodes_barcode_events_mixin" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/barcodes_barcode_events_mixins/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        barcodes_barcode_events_mixinService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }







    @PreAuthorize("hasPermission(#barcodes_barcode_events_mixin_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Barcodes_barcode_events_mixin" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/barcodes_barcode_events_mixins/{barcodes_barcode_events_mixin_id}")
    public ResponseEntity<Barcodes_barcode_events_mixinDTO> get(@PathVariable("barcodes_barcode_events_mixin_id") Integer barcodes_barcode_events_mixin_id) {
        Barcodes_barcode_events_mixin domain = barcodes_barcode_events_mixinService.get(barcodes_barcode_events_mixin_id);
        Barcodes_barcode_events_mixinDTO dto = barcodes_barcode_events_mixinMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }




    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Barcodes_barcode_events_mixin" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/barcodes_barcode_events_mixins/fetchdefault")
	public ResponseEntity<List<Barcodes_barcode_events_mixinDTO>> fetchDefault(Barcodes_barcode_events_mixinSearchContext context) {
        Page<Barcodes_barcode_events_mixin> domains = barcodes_barcode_events_mixinService.searchDefault(context) ;
        List<Barcodes_barcode_events_mixinDTO> list = barcodes_barcode_events_mixinMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Barcodes_barcode_events_mixin" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/barcodes_barcode_events_mixins/searchdefault")
	public ResponseEntity<Page<Barcodes_barcode_events_mixinDTO>> searchDefault(Barcodes_barcode_events_mixinSearchContext context) {
        Page<Barcodes_barcode_events_mixin> domains = barcodes_barcode_events_mixinService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(barcodes_barcode_events_mixinMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Barcodes_barcode_events_mixin getEntity(){
        return new Barcodes_barcode_events_mixin();
    }

}
