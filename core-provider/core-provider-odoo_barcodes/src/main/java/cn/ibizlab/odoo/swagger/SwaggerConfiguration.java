package cn.ibizlab.odoo.swagger;

import cn.ibizlab.odoo.util.security.SpringContextHolder;
import cn.ibizlab.odoo.util.web.IBZOperationParameterReader;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import org.springframework.plugin.core.PluginRegistry;
import org.springframework.plugin.core.PluginRegistrySupport;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spi.schema.EnumTypeDeterminer;
import springfox.documentation.spi.service.OperationBuilderPlugin;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.spring.web.readers.operation.OperationParameterReader;
import springfox.documentation.spring.web.readers.parameter.ModelAttributeParameterExpander;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

@Configuration
@EnableSwagger2
public class SwaggerConfiguration {

    @Bean
    public Docket docket() {
        Docket docket = new Docket(DocumentationType.SWAGGER_2)
                .groupName("DEFAULT")
                .pathMapping("/")
                .apiInfo(
						new ApiInfoBuilder()
						.title("DEFAULT")
						.build()
                    )
                .select()
				.apis(RequestHandlerSelectors.basePackage("cn.ibizlab.odoo"))
                //.paths(or(regex("/rest/.*")))
				.paths(PathSelectors.any())
                .build()
                ;
		removeDefaultPlugin();
		return docket ;
    }

	@Bean
	public Docket odoo_repairDocket() {
		return new Docket(DocumentationType.SWAGGER_2)
				.groupName("[ODOO标准服务]维修")
				.pathMapping("/")
				.apiInfo(
						new ApiInfoBuilder()
						.title("[ODOO标准服务]维修")
						.version("1")
						.build()
                    )
				.select()
				.apis(RequestHandlerSelectors.basePackage("cn.ibizlab.odoo.odoo_repair"))
				//.paths(or(regex("/rest/odoo_repair/.*")))
				.paths(PathSelectors.any())
				.build()
				;
	}
	@Bean
	public Docket odoo_fetchmailDocket() {
		return new Docket(DocumentationType.SWAGGER_2)
				.groupName("[ODOO标准服务]EMail 网关")
				.pathMapping("/")
				.apiInfo(
						new ApiInfoBuilder()
						.title("[ODOO标准服务]EMail 网关")
						.version("1")
						.build()
                    )
				.select()
				.apis(RequestHandlerSelectors.basePackage("cn.ibizlab.odoo.odoo_fetchmail"))
				//.paths(or(regex("/rest/odoo_fetchmail/.*")))
				.paths(PathSelectors.any())
				.build()
				;
	}
	@Bean
	public Docket odoo_projectDocket() {
		return new Docket(DocumentationType.SWAGGER_2)
				.groupName("[ODOO标准服务]项目")
				.pathMapping("/")
				.apiInfo(
						new ApiInfoBuilder()
						.title("[ODOO标准服务]项目")
						.version("1")
						.build()
                    )
				.select()
				.apis(RequestHandlerSelectors.basePackage("cn.ibizlab.odoo.odoo_project"))
				//.paths(or(regex("/rest/odoo_project/.*")))
				.paths(PathSelectors.any())
				.build()
				;
	}
	@Bean
	public Docket odoo_boardDocket() {
		return new Docket(DocumentationType.SWAGGER_2)
				.groupName("[ODOO标准服务]仪表板")
				.pathMapping("/")
				.apiInfo(
						new ApiInfoBuilder()
						.title("[ODOO标准服务]仪表板")
						.version("1")
						.build()
                    )
				.select()
				.apis(RequestHandlerSelectors.basePackage("cn.ibizlab.odoo.odoo_board"))
				//.paths(or(regex("/rest/odoo_board/.*")))
				.paths(PathSelectors.any())
				.build()
				;
	}
	@Bean
	public Docket odoo_iapDocket() {
		return new Docket(DocumentationType.SWAGGER_2)
				.groupName("[ODOO标准服务]应用内购买")
				.pathMapping("/")
				.apiInfo(
						new ApiInfoBuilder()
						.title("[ODOO标准服务]应用内购买")
						.version("1")
						.build()
                    )
				.select()
				.apis(RequestHandlerSelectors.basePackage("cn.ibizlab.odoo.odoo_iap"))
				//.paths(or(regex("/rest/odoo_iap/.*")))
				.paths(PathSelectors.any())
				.build()
				;
	}
	@Bean
	public Docket odoo_eventDocket() {
		return new Docket(DocumentationType.SWAGGER_2)
				.groupName("[ODOO标准服务]活动组织")
				.pathMapping("/")
				.apiInfo(
						new ApiInfoBuilder()
						.title("[ODOO标准服务]活动组织")
						.version("1")
						.build()
                    )
				.select()
				.apis(RequestHandlerSelectors.basePackage("cn.ibizlab.odoo.odoo_event"))
				//.paths(or(regex("/rest/odoo_event/.*")))
				.paths(PathSelectors.any())
				.build()
				;
	}
	@Bean
	public Docket odoo_base_importDocket() {
		return new Docket(DocumentationType.SWAGGER_2)
				.groupName("[ODOO标准服务]基础导入")
				.pathMapping("/")
				.apiInfo(
						new ApiInfoBuilder()
						.title("[ODOO标准服务]基础导入")
						.version("1")
						.build()
                    )
				.select()
				.apis(RequestHandlerSelectors.basePackage("cn.ibizlab.odoo.odoo_base_import"))
				//.paths(or(regex("/rest/odoo_base_import/.*")))
				.paths(PathSelectors.any())
				.build()
				;
	}
	@Bean
	public Docket odoo_snailmailDocket() {
		return new Docket(DocumentationType.SWAGGER_2)
				.groupName("[ODOO标准服务]Snail Mail")
				.pathMapping("/")
				.apiInfo(
						new ApiInfoBuilder()
						.title("[ODOO标准服务]Snail Mail")
						.version("1")
						.build()
                    )
				.select()
				.apis(RequestHandlerSelectors.basePackage("cn.ibizlab.odoo.odoo_snailmail"))
				//.paths(or(regex("/rest/odoo_snailmail/.*")))
				.paths(PathSelectors.any())
				.build()
				;
	}
	@Bean
	public Docket odoo_barcodesDocket() {
		return new Docket(DocumentationType.SWAGGER_2)
				.groupName("[ODOO标准服务]条码")
				.pathMapping("/")
				.apiInfo(
						new ApiInfoBuilder()
						.title("[ODOO标准服务]条码")
						.version("1")
						.build()
                    )
				.select()
				.apis(RequestHandlerSelectors.basePackage("cn.ibizlab.odoo.odoo_barcodes"))
				//.paths(or(regex("/rest/odoo_barcodes/.*")))
				.paths(PathSelectors.any())
				.build()
				;
	}
	@Bean
	public Docket odoo_busDocket() {
		return new Docket(DocumentationType.SWAGGER_2)
				.groupName("[ODOO标准服务]IM 总线")
				.pathMapping("/")
				.apiInfo(
						new ApiInfoBuilder()
						.title("[ODOO标准服务]IM 总线")
						.version("1")
						.build()
                    )
				.select()
				.apis(RequestHandlerSelectors.basePackage("cn.ibizlab.odoo.odoo_bus"))
				//.paths(or(regex("/rest/odoo_bus/.*")))
				.paths(PathSelectors.any())
				.build()
				;
	}
	@Bean
	public Docket odoo_saleDocket() {
		return new Docket(DocumentationType.SWAGGER_2)
				.groupName("[ODOO标准服务]销售")
				.pathMapping("/")
				.apiInfo(
						new ApiInfoBuilder()
						.title("[ODOO标准服务]销售")
						.version("1")
						.build()
                    )
				.select()
				.apis(RequestHandlerSelectors.basePackage("cn.ibizlab.odoo.odoo_sale"))
				//.paths(or(regex("/rest/odoo_sale/.*")))
				.paths(PathSelectors.any())
				.build()
				;
	}
	@Bean
	public Docket odoo_web_tourDocket() {
		return new Docket(DocumentationType.SWAGGER_2)
				.groupName("[ODOO标准服务]游览")
				.pathMapping("/")
				.apiInfo(
						new ApiInfoBuilder()
						.title("[ODOO标准服务]游览")
						.version("1")
						.build()
                    )
				.select()
				.apis(RequestHandlerSelectors.basePackage("cn.ibizlab.odoo.odoo_web_tour"))
				//.paths(or(regex("/rest/odoo_web_tour/.*")))
				.paths(PathSelectors.any())
				.build()
				;
	}
	@Bean
	public Docket odoo_digestDocket() {
		return new Docket(DocumentationType.SWAGGER_2)
				.groupName("[ODOO标准服务]KPI 摘要")
				.pathMapping("/")
				.apiInfo(
						new ApiInfoBuilder()
						.title("[ODOO标准服务]KPI 摘要")
						.version("1")
						.build()
                    )
				.select()
				.apis(RequestHandlerSelectors.basePackage("cn.ibizlab.odoo.odoo_digest"))
				//.paths(or(regex("/rest/odoo_digest/.*")))
				.paths(PathSelectors.any())
				.build()
				;
	}
	@Bean
	public Docket odoo_noteDocket() {
		return new Docket(DocumentationType.SWAGGER_2)
				.groupName("[ODOO标准服务]便签")
				.pathMapping("/")
				.apiInfo(
						new ApiInfoBuilder()
						.title("[ODOO标准服务]便签")
						.version("1")
						.build()
                    )
				.select()
				.apis(RequestHandlerSelectors.basePackage("cn.ibizlab.odoo.odoo_note"))
				//.paths(or(regex("/rest/odoo_note/.*")))
				.paths(PathSelectors.any())
				.build()
				;
	}
	@Bean
	public Docket odoo_utmDocket() {
		return new Docket(DocumentationType.SWAGGER_2)
				.groupName("[ODOO标准服务]UTM 跟踪器")
				.pathMapping("/")
				.apiInfo(
						new ApiInfoBuilder()
						.title("[ODOO标准服务]UTM 跟踪器")
						.version("1")
						.build()
                    )
				.select()
				.apis(RequestHandlerSelectors.basePackage("cn.ibizlab.odoo.odoo_utm"))
				//.paths(or(regex("/rest/odoo_utm/.*")))
				.paths(PathSelectors.any())
				.build()
				;
	}
	@Bean
	public Docket odoo_resourceDocket() {
		return new Docket(DocumentationType.SWAGGER_2)
				.groupName("[ODOO标准服务]资源")
				.pathMapping("/")
				.apiInfo(
						new ApiInfoBuilder()
						.title("[ODOO标准服务]资源")
						.version("1")
						.build()
                    )
				.select()
				.apis(RequestHandlerSelectors.basePackage("cn.ibizlab.odoo.odoo_resource"))
				//.paths(or(regex("/rest/odoo_resource/.*")))
				.paths(PathSelectors.any())
				.build()
				;
	}
	@Bean
	public Docket odoo_ratingDocket() {
		return new Docket(DocumentationType.SWAGGER_2)
				.groupName("[ODOO标准服务]客户点评")
				.pathMapping("/")
				.apiInfo(
						new ApiInfoBuilder()
						.title("[ODOO标准服务]客户点评")
						.version("1")
						.build()
                    )
				.select()
				.apis(RequestHandlerSelectors.basePackage("cn.ibizlab.odoo.odoo_rating"))
				//.paths(or(regex("/rest/odoo_rating/.*")))
				.paths(PathSelectors.any())
				.build()
				;
	}
	@Bean
	public Docket odoo_gamificationDocket() {
		return new Docket(DocumentationType.SWAGGER_2)
				.groupName("[ODOO标准服务]游戏化")
				.pathMapping("/")
				.apiInfo(
						new ApiInfoBuilder()
						.title("[ODOO标准服务]游戏化")
						.version("1")
						.build()
                    )
				.select()
				.apis(RequestHandlerSelectors.basePackage("cn.ibizlab.odoo.odoo_gamification"))
				//.paths(or(regex("/rest/odoo_gamification/.*")))
				.paths(PathSelectors.any())
				.build()
				;
	}
	@Bean
	public Docket odoo_fleetDocket() {
		return new Docket(DocumentationType.SWAGGER_2)
				.groupName("[ODOO标准服务]车队")
				.pathMapping("/")
				.apiInfo(
						new ApiInfoBuilder()
						.title("[ODOO标准服务]车队")
						.version("1")
						.build()
                    )
				.select()
				.apis(RequestHandlerSelectors.basePackage("cn.ibizlab.odoo.odoo_fleet"))
				//.paths(or(regex("/rest/odoo_fleet/.*")))
				.paths(PathSelectors.any())
				.build()
				;
	}
	@Bean
	public Docket odoo_baseDocket() {
		return new Docket(DocumentationType.SWAGGER_2)
				.groupName("[ODOO标准服务]基础")
				.pathMapping("/")
				.apiInfo(
						new ApiInfoBuilder()
						.title("[ODOO标准服务]基础")
						.version("1")
						.build()
                    )
				.select()
				.apis(RequestHandlerSelectors.basePackage("cn.ibizlab.odoo.odoo_base"))
				//.paths(or(regex("/rest/odoo_base/.*")))
				.paths(PathSelectors.any())
				.build()
				;
	}
	@Bean
	public Docket odoo_crmDocket() {
		return new Docket(DocumentationType.SWAGGER_2)
				.groupName("[ODOO标准服务]CRM")
				.pathMapping("/")
				.apiInfo(
						new ApiInfoBuilder()
						.title("[ODOO标准服务]CRM")
						.version("1")
						.build()
                    )
				.select()
				.apis(RequestHandlerSelectors.basePackage("cn.ibizlab.odoo.odoo_crm"))
				//.paths(or(regex("/rest/odoo_crm/.*")))
				.paths(PathSelectors.any())
				.build()
				;
	}
	@Bean
	public Docket odoo_hrDocket() {
		return new Docket(DocumentationType.SWAGGER_2)
				.groupName("[ODOO标准服务]员工")
				.pathMapping("/")
				.apiInfo(
						new ApiInfoBuilder()
						.title("[ODOO标准服务]员工")
						.version("1")
						.build()
                    )
				.select()
				.apis(RequestHandlerSelectors.basePackage("cn.ibizlab.odoo.odoo_hr"))
				//.paths(or(regex("/rest/odoo_hr/.*")))
				.paths(PathSelectors.any())
				.build()
				;
	}
	@Bean
	public Docket odoo_uomDocket() {
		return new Docket(DocumentationType.SWAGGER_2)
				.groupName("[ODOO标准服务]计量单位")
				.pathMapping("/")
				.apiInfo(
						new ApiInfoBuilder()
						.title("[ODOO标准服务]计量单位")
						.version("1")
						.build()
                    )
				.select()
				.apis(RequestHandlerSelectors.basePackage("cn.ibizlab.odoo.odoo_uom"))
				//.paths(or(regex("/rest/odoo_uom/.*")))
				.paths(PathSelectors.any())
				.build()
				;
	}
	@Bean
	public Docket odoo_maintenanceDocket() {
		return new Docket(DocumentationType.SWAGGER_2)
				.groupName("[ODOO标准服务]保养")
				.pathMapping("/")
				.apiInfo(
						new ApiInfoBuilder()
						.title("[ODOO标准服务]保养")
						.version("1")
						.build()
                    )
				.select()
				.apis(RequestHandlerSelectors.basePackage("cn.ibizlab.odoo.odoo_maintenance"))
				//.paths(or(regex("/rest/odoo_maintenance/.*")))
				.paths(PathSelectors.any())
				.build()
				;
	}
	@Bean
	public Docket odoo_purchaseDocket() {
		return new Docket(DocumentationType.SWAGGER_2)
				.groupName("[ODOO标准服务]采购")
				.pathMapping("/")
				.apiInfo(
						new ApiInfoBuilder()
						.title("[ODOO标准服务]采购")
						.version("1")
						.build()
                    )
				.select()
				.apis(RequestHandlerSelectors.basePackage("cn.ibizlab.odoo.odoo_purchase"))
				//.paths(or(regex("/rest/odoo_purchase/.*")))
				.paths(PathSelectors.any())
				.build()
				;
	}
	@Bean
	public Docket odoo_assetDocket() {
		return new Docket(DocumentationType.SWAGGER_2)
				.groupName("[ODOO标准服务]Assets")
				.pathMapping("/")
				.apiInfo(
						new ApiInfoBuilder()
						.title("[ODOO标准服务]Assets")
						.version("1")
						.build()
                    )
				.select()
				.apis(RequestHandlerSelectors.basePackage("cn.ibizlab.odoo.odoo_asset"))
				//.paths(or(regex("/rest/odoo_asset/.*")))
				.paths(PathSelectors.any())
				.build()
				;
	}
	@Bean
	public Docket odoo_smsDocket() {
		return new Docket(DocumentationType.SWAGGER_2)
				.groupName("[ODOO标准服务]短信息网关")
				.pathMapping("/")
				.apiInfo(
						new ApiInfoBuilder()
						.title("[ODOO标准服务]短信息网关")
						.version("1")
						.build()
                    )
				.select()
				.apis(RequestHandlerSelectors.basePackage("cn.ibizlab.odoo.odoo_sms"))
				//.paths(or(regex("/rest/odoo_sms/.*")))
				.paths(PathSelectors.any())
				.build()
				;
	}
	@Bean
	public Docket odoo_portalDocket() {
		return new Docket(DocumentationType.SWAGGER_2)
				.groupName("[ODOO标准服务]客户门户")
				.pathMapping("/")
				.apiInfo(
						new ApiInfoBuilder()
						.title("[ODOO标准服务]客户门户")
						.version("1")
						.build()
                    )
				.select()
				.apis(RequestHandlerSelectors.basePackage("cn.ibizlab.odoo.odoo_portal"))
				//.paths(or(regex("/rest/odoo_portal/.*")))
				.paths(PathSelectors.any())
				.build()
				;
	}
	@Bean
	public Docket odoo_mrpDocket() {
		return new Docket(DocumentationType.SWAGGER_2)
				.groupName("[ODOO标准服务]制造")
				.pathMapping("/")
				.apiInfo(
						new ApiInfoBuilder()
						.title("[ODOO标准服务]制造")
						.version("1")
						.build()
                    )
				.select()
				.apis(RequestHandlerSelectors.basePackage("cn.ibizlab.odoo.odoo_mrp"))
				//.paths(or(regex("/rest/odoo_mrp/.*")))
				.paths(PathSelectors.any())
				.build()
				;
	}
	@Bean
	public Docket odoo_productDocket() {
		return new Docket(DocumentationType.SWAGGER_2)
				.groupName("[ODOO标准服务]产品及价格表")
				.pathMapping("/")
				.apiInfo(
						new ApiInfoBuilder()
						.title("[ODOO标准服务]产品及价格表")
						.version("1")
						.build()
                    )
				.select()
				.apis(RequestHandlerSelectors.basePackage("cn.ibizlab.odoo.odoo_product"))
				//.paths(or(regex("/rest/odoo_product/.*")))
				.paths(PathSelectors.any())
				.build()
				;
	}
	@Bean
	public Docket odoo_im_livechatDocket() {
		return new Docket(DocumentationType.SWAGGER_2)
				.groupName("[ODOO标准服务]在线聊天")
				.pathMapping("/")
				.apiInfo(
						new ApiInfoBuilder()
						.title("[ODOO标准服务]在线聊天")
						.version("1")
						.build()
                    )
				.select()
				.apis(RequestHandlerSelectors.basePackage("cn.ibizlab.odoo.odoo_im_livechat"))
				//.paths(or(regex("/rest/odoo_im_livechat/.*")))
				.paths(PathSelectors.any())
				.build()
				;
	}
	@Bean
	public Docket odoo_accountDocket() {
		return new Docket(DocumentationType.SWAGGER_2)
				.groupName("[ODOO标准服务]开票")
				.pathMapping("/")
				.apiInfo(
						new ApiInfoBuilder()
						.title("[ODOO标准服务]开票")
						.version("1")
						.build()
                    )
				.select()
				.apis(RequestHandlerSelectors.basePackage("cn.ibizlab.odoo.odoo_account"))
				//.paths(or(regex("/rest/odoo_account/.*")))
				.paths(PathSelectors.any())
				.build()
				;
	}
	@Bean
	public Docket odoo_web_editorDocket() {
		return new Docket(DocumentationType.SWAGGER_2)
				.groupName("[ODOO标准服务]网页编辑器")
				.pathMapping("/")
				.apiInfo(
						new ApiInfoBuilder()
						.title("[ODOO标准服务]网页编辑器")
						.version("1")
						.build()
                    )
				.select()
				.apis(RequestHandlerSelectors.basePackage("cn.ibizlab.odoo.odoo_web_editor"))
				//.paths(or(regex("/rest/odoo_web_editor/.*")))
				.paths(PathSelectors.any())
				.build()
				;
	}
	@Bean
	public Docket odoo_lunchDocket() {
		return new Docket(DocumentationType.SWAGGER_2)
				.groupName("[ODOO标准服务]工作餐")
				.pathMapping("/")
				.apiInfo(
						new ApiInfoBuilder()
						.title("[ODOO标准服务]工作餐")
						.version("1")
						.build()
                    )
				.select()
				.apis(RequestHandlerSelectors.basePackage("cn.ibizlab.odoo.odoo_lunch"))
				//.paths(or(regex("/rest/odoo_lunch/.*")))
				.paths(PathSelectors.any())
				.build()
				;
	}
	@Bean
	public Docket odoo_websiteDocket() {
		return new Docket(DocumentationType.SWAGGER_2)
				.groupName("[ODOO标准服务]网站")
				.pathMapping("/")
				.apiInfo(
						new ApiInfoBuilder()
						.title("[ODOO标准服务]网站")
						.version("1")
						.build()
                    )
				.select()
				.apis(RequestHandlerSelectors.basePackage("cn.ibizlab.odoo.odoo_website"))
				//.paths(or(regex("/rest/odoo_website/.*")))
				.paths(PathSelectors.any())
				.build()
				;
	}
	@Bean
	public Docket odoo_surveyDocket() {
		return new Docket(DocumentationType.SWAGGER_2)
				.groupName("[ODOO标准服务]调查")
				.pathMapping("/")
				.apiInfo(
						new ApiInfoBuilder()
						.title("[ODOO标准服务]调查")
						.version("1")
						.build()
                    )
				.select()
				.apis(RequestHandlerSelectors.basePackage("cn.ibizlab.odoo.odoo_survey"))
				//.paths(or(regex("/rest/odoo_survey/.*")))
				.paths(PathSelectors.any())
				.build()
				;
	}
	@Bean
	public Docket odoo_paymentDocket() {
		return new Docket(DocumentationType.SWAGGER_2)
				.groupName("[ODOO标准服务]付款收单方")
				.pathMapping("/")
				.apiInfo(
						new ApiInfoBuilder()
						.title("[ODOO标准服务]付款收单方")
						.version("1")
						.build()
                    )
				.select()
				.apis(RequestHandlerSelectors.basePackage("cn.ibizlab.odoo.odoo_payment"))
				//.paths(or(regex("/rest/odoo_payment/.*")))
				.paths(PathSelectors.any())
				.build()
				;
	}
	@Bean
	public Docket odoo_stockDocket() {
		return new Docket(DocumentationType.SWAGGER_2)
				.groupName("[ODOO标准服务]库存")
				.pathMapping("/")
				.apiInfo(
						new ApiInfoBuilder()
						.title("[ODOO标准服务]库存")
						.version("1")
						.build()
                    )
				.select()
				.apis(RequestHandlerSelectors.basePackage("cn.ibizlab.odoo.odoo_stock"))
				//.paths(or(regex("/rest/odoo_stock/.*")))
				.paths(PathSelectors.any())
				.build()
				;
	}
	@Bean
	public Docket odoo_mroDocket() {
		return new Docket(DocumentationType.SWAGGER_2)
				.groupName("[ODOO标准服务]MRO")
				.pathMapping("/")
				.apiInfo(
						new ApiInfoBuilder()
						.title("[ODOO标准服务]MRO")
						.version("1")
						.build()
                    )
				.select()
				.apis(RequestHandlerSelectors.basePackage("cn.ibizlab.odoo.odoo_mro"))
				//.paths(or(regex("/rest/odoo_mro/.*")))
				.paths(PathSelectors.any())
				.build()
				;
	}
	@Bean
	public Docket odoo_mailDocket() {
		return new Docket(DocumentationType.SWAGGER_2)
				.groupName("[ODOO标准服务]讨论")
				.pathMapping("/")
				.apiInfo(
						new ApiInfoBuilder()
						.title("[ODOO标准服务]讨论")
						.version("1")
						.build()
                    )
				.select()
				.apis(RequestHandlerSelectors.basePackage("cn.ibizlab.odoo.odoo_mail"))
				//.paths(or(regex("/rest/odoo_mail/.*")))
				.paths(PathSelectors.any())
				.build()
				;
	}
	@Bean
	public Docket odoo_calendarDocket() {
		return new Docket(DocumentationType.SWAGGER_2)
				.groupName("[ODOO标准服务]日历")
				.pathMapping("/")
				.apiInfo(
						new ApiInfoBuilder()
						.title("[ODOO标准服务]日历")
						.version("1")
						.build()
                    )
				.select()
				.apis(RequestHandlerSelectors.basePackage("cn.ibizlab.odoo.odoo_calendar"))
				//.paths(or(regex("/rest/odoo_calendar/.*")))
				.paths(PathSelectors.any())
				.build()
				;
	}

	
	private void removeDefaultPlugin() {
		// 从spring容器中获取swagger插件注册表
		PluginRegistry<OperationBuilderPlugin, DocumentationType> pluginRegistry = SpringContextHolder.getBean("operationBuilderPluginRegistry");
		// 插件集合
		List<OperationBuilderPlugin> plugins = pluginRegistry.getPlugins();
		// 从spring容器中获取需要删除的插件
		OperationParameterReader operationParameterReader = SpringContextHolder.getBean(OperationParameterReader.class);
		if(operationParameterReader==null)
			return ;
		// 原plugins集合不能修改，创建新集合，通过反射替换
		if (pluginRegistry.contains(operationParameterReader)) {
			List<OperationBuilderPlugin> plugins_new = new ArrayList<OperationBuilderPlugin>(plugins);
			plugins_new.remove(operationParameterReader);
			try {
				Field field = PluginRegistrySupport.class.getDeclaredField("plugins");
				field.setAccessible(true);
				field.set(pluginRegistry, plugins_new);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	
	@Bean
	public IBZOperationParameterReader iBZOperationParameterReader(ModelAttributeParameterExpander expander,EnumTypeDeterminer enumTypeDeterminer){
		IBZOperationParameterReader iBZOperationParameterReader = new IBZOperationParameterReader(expander, enumTypeDeterminer) ;
		return iBZOperationParameterReader ;
	}
}
