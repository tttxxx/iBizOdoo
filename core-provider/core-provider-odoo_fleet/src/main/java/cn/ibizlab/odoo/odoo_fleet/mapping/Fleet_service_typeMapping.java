package cn.ibizlab.odoo.odoo_fleet.mapping;

import org.mapstruct.*;
import cn.ibizlab.odoo.core.odoo_fleet.domain.Fleet_service_type;
import cn.ibizlab.odoo.odoo_fleet.dto.Fleet_service_typeDTO;
import cn.ibizlab.odoo.util.domain.MappingBase;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring", uses = {},
    nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
    nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface Fleet_service_typeMapping extends MappingBase<Fleet_service_typeDTO, Fleet_service_type> {


}

