package cn.ibizlab.odoo.odoo_fleet.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_fleet.dto.*;
import cn.ibizlab.odoo.odoo_fleet.mapping.*;
import cn.ibizlab.odoo.core.odoo_fleet.domain.Fleet_vehicle_cost;
import cn.ibizlab.odoo.core.odoo_fleet.service.IFleet_vehicle_costService;
import cn.ibizlab.odoo.core.odoo_fleet.filter.Fleet_vehicle_costSearchContext;




@Slf4j
@Api(tags = {"Fleet_vehicle_cost" })
@RestController("odoo_fleet-fleet_vehicle_cost")
@RequestMapping("")
public class Fleet_vehicle_costResource {

    @Autowired
    private IFleet_vehicle_costService fleet_vehicle_costService;

    @Autowired
    @Lazy
    private Fleet_vehicle_costMapping fleet_vehicle_costMapping;




    @PreAuthorize("hasPermission(#fleet_vehicle_cost_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Fleet_vehicle_cost" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/fleet_vehicle_costs/{fleet_vehicle_cost_id}")
    public ResponseEntity<Fleet_vehicle_costDTO> get(@PathVariable("fleet_vehicle_cost_id") Integer fleet_vehicle_cost_id) {
        Fleet_vehicle_cost domain = fleet_vehicle_costService.get(fleet_vehicle_cost_id);
        Fleet_vehicle_costDTO dto = fleet_vehicle_costMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }




    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Fleet_vehicle_cost" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/fleet_vehicle_costs")

    public ResponseEntity<Fleet_vehicle_costDTO> create(@RequestBody Fleet_vehicle_costDTO fleet_vehicle_costdto) {
        Fleet_vehicle_cost domain = fleet_vehicle_costMapping.toDomain(fleet_vehicle_costdto);
		fleet_vehicle_costService.create(domain);
        Fleet_vehicle_costDTO dto = fleet_vehicle_costMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Fleet_vehicle_cost" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/fleet_vehicle_costs/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Fleet_vehicle_costDTO> fleet_vehicle_costdtos) {
        fleet_vehicle_costService.createBatch(fleet_vehicle_costMapping.toDomain(fleet_vehicle_costdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }







    @PreAuthorize("hasPermission('Remove',{#fleet_vehicle_cost_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Fleet_vehicle_cost" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/fleet_vehicle_costs/{fleet_vehicle_cost_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("fleet_vehicle_cost_id") Integer fleet_vehicle_cost_id) {
         return ResponseEntity.status(HttpStatus.OK).body(fleet_vehicle_costService.remove(fleet_vehicle_cost_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Fleet_vehicle_cost" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/fleet_vehicle_costs/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        fleet_vehicle_costService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }







    @PreAuthorize("hasPermission(#fleet_vehicle_cost_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Fleet_vehicle_cost" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/fleet_vehicle_costs/{fleet_vehicle_cost_id}")

    public ResponseEntity<Fleet_vehicle_costDTO> update(@PathVariable("fleet_vehicle_cost_id") Integer fleet_vehicle_cost_id, @RequestBody Fleet_vehicle_costDTO fleet_vehicle_costdto) {
		Fleet_vehicle_cost domain = fleet_vehicle_costMapping.toDomain(fleet_vehicle_costdto);
        domain.setId(fleet_vehicle_cost_id);
		fleet_vehicle_costService.update(domain);
		Fleet_vehicle_costDTO dto = fleet_vehicle_costMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#fleet_vehicle_cost_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Fleet_vehicle_cost" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/fleet_vehicle_costs/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Fleet_vehicle_costDTO> fleet_vehicle_costdtos) {
        fleet_vehicle_costService.updateBatch(fleet_vehicle_costMapping.toDomain(fleet_vehicle_costdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Fleet_vehicle_cost" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/fleet_vehicle_costs/fetchdefault")
	public ResponseEntity<List<Fleet_vehicle_costDTO>> fetchDefault(Fleet_vehicle_costSearchContext context) {
        Page<Fleet_vehicle_cost> domains = fleet_vehicle_costService.searchDefault(context) ;
        List<Fleet_vehicle_costDTO> list = fleet_vehicle_costMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Fleet_vehicle_cost" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/fleet_vehicle_costs/searchdefault")
	public ResponseEntity<Page<Fleet_vehicle_costDTO>> searchDefault(Fleet_vehicle_costSearchContext context) {
        Page<Fleet_vehicle_cost> domains = fleet_vehicle_costService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(fleet_vehicle_costMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Fleet_vehicle_cost getEntity(){
        return new Fleet_vehicle_cost();
    }

}
