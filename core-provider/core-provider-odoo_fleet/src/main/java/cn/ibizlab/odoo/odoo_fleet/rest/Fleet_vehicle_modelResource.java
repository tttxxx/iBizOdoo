package cn.ibizlab.odoo.odoo_fleet.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_fleet.dto.*;
import cn.ibizlab.odoo.odoo_fleet.mapping.*;
import cn.ibizlab.odoo.core.odoo_fleet.domain.Fleet_vehicle_model;
import cn.ibizlab.odoo.core.odoo_fleet.service.IFleet_vehicle_modelService;
import cn.ibizlab.odoo.core.odoo_fleet.filter.Fleet_vehicle_modelSearchContext;




@Slf4j
@Api(tags = {"Fleet_vehicle_model" })
@RestController("odoo_fleet-fleet_vehicle_model")
@RequestMapping("")
public class Fleet_vehicle_modelResource {

    @Autowired
    private IFleet_vehicle_modelService fleet_vehicle_modelService;

    @Autowired
    @Lazy
    private Fleet_vehicle_modelMapping fleet_vehicle_modelMapping;




    @PreAuthorize("hasPermission(#fleet_vehicle_model_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Fleet_vehicle_model" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/fleet_vehicle_models/{fleet_vehicle_model_id}")

    public ResponseEntity<Fleet_vehicle_modelDTO> update(@PathVariable("fleet_vehicle_model_id") Integer fleet_vehicle_model_id, @RequestBody Fleet_vehicle_modelDTO fleet_vehicle_modeldto) {
		Fleet_vehicle_model domain = fleet_vehicle_modelMapping.toDomain(fleet_vehicle_modeldto);
        domain.setId(fleet_vehicle_model_id);
		fleet_vehicle_modelService.update(domain);
		Fleet_vehicle_modelDTO dto = fleet_vehicle_modelMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#fleet_vehicle_model_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Fleet_vehicle_model" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/fleet_vehicle_models/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Fleet_vehicle_modelDTO> fleet_vehicle_modeldtos) {
        fleet_vehicle_modelService.updateBatch(fleet_vehicle_modelMapping.toDomain(fleet_vehicle_modeldtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission(#fleet_vehicle_model_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Fleet_vehicle_model" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/fleet_vehicle_models/{fleet_vehicle_model_id}")
    public ResponseEntity<Fleet_vehicle_modelDTO> get(@PathVariable("fleet_vehicle_model_id") Integer fleet_vehicle_model_id) {
        Fleet_vehicle_model domain = fleet_vehicle_modelService.get(fleet_vehicle_model_id);
        Fleet_vehicle_modelDTO dto = fleet_vehicle_modelMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }










    @PreAuthorize("hasPermission('Remove',{#fleet_vehicle_model_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Fleet_vehicle_model" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/fleet_vehicle_models/{fleet_vehicle_model_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("fleet_vehicle_model_id") Integer fleet_vehicle_model_id) {
         return ResponseEntity.status(HttpStatus.OK).body(fleet_vehicle_modelService.remove(fleet_vehicle_model_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Fleet_vehicle_model" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/fleet_vehicle_models/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        fleet_vehicle_modelService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }







    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Fleet_vehicle_model" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/fleet_vehicle_models")

    public ResponseEntity<Fleet_vehicle_modelDTO> create(@RequestBody Fleet_vehicle_modelDTO fleet_vehicle_modeldto) {
        Fleet_vehicle_model domain = fleet_vehicle_modelMapping.toDomain(fleet_vehicle_modeldto);
		fleet_vehicle_modelService.create(domain);
        Fleet_vehicle_modelDTO dto = fleet_vehicle_modelMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Fleet_vehicle_model" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/fleet_vehicle_models/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Fleet_vehicle_modelDTO> fleet_vehicle_modeldtos) {
        fleet_vehicle_modelService.createBatch(fleet_vehicle_modelMapping.toDomain(fleet_vehicle_modeldtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Fleet_vehicle_model" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/fleet_vehicle_models/fetchdefault")
	public ResponseEntity<List<Fleet_vehicle_modelDTO>> fetchDefault(Fleet_vehicle_modelSearchContext context) {
        Page<Fleet_vehicle_model> domains = fleet_vehicle_modelService.searchDefault(context) ;
        List<Fleet_vehicle_modelDTO> list = fleet_vehicle_modelMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Fleet_vehicle_model" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/fleet_vehicle_models/searchdefault")
	public ResponseEntity<Page<Fleet_vehicle_modelDTO>> searchDefault(Fleet_vehicle_modelSearchContext context) {
        Page<Fleet_vehicle_model> domains = fleet_vehicle_modelService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(fleet_vehicle_modelMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Fleet_vehicle_model getEntity(){
        return new Fleet_vehicle_model();
    }

}
