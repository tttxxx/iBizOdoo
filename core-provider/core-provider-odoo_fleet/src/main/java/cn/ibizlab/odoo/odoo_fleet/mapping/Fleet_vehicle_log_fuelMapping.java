package cn.ibizlab.odoo.odoo_fleet.mapping;

import org.mapstruct.*;
import cn.ibizlab.odoo.core.odoo_fleet.domain.Fleet_vehicle_log_fuel;
import cn.ibizlab.odoo.odoo_fleet.dto.Fleet_vehicle_log_fuelDTO;
import cn.ibizlab.odoo.util.domain.MappingBase;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring", uses = {},
    nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
    nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface Fleet_vehicle_log_fuelMapping extends MappingBase<Fleet_vehicle_log_fuelDTO, Fleet_vehicle_log_fuel> {


}

