package cn.ibizlab.odoo.odoo_fleet.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_fleet.dto.*;
import cn.ibizlab.odoo.odoo_fleet.mapping.*;
import cn.ibizlab.odoo.core.odoo_fleet.domain.Fleet_vehicle_log_fuel;
import cn.ibizlab.odoo.core.odoo_fleet.service.IFleet_vehicle_log_fuelService;
import cn.ibizlab.odoo.core.odoo_fleet.filter.Fleet_vehicle_log_fuelSearchContext;




@Slf4j
@Api(tags = {"Fleet_vehicle_log_fuel" })
@RestController("odoo_fleet-fleet_vehicle_log_fuel")
@RequestMapping("")
public class Fleet_vehicle_log_fuelResource {

    @Autowired
    private IFleet_vehicle_log_fuelService fleet_vehicle_log_fuelService;

    @Autowired
    @Lazy
    private Fleet_vehicle_log_fuelMapping fleet_vehicle_log_fuelMapping;







    @PreAuthorize("hasPermission('Remove',{#fleet_vehicle_log_fuel_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Fleet_vehicle_log_fuel" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/fleet_vehicle_log_fuels/{fleet_vehicle_log_fuel_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("fleet_vehicle_log_fuel_id") Integer fleet_vehicle_log_fuel_id) {
         return ResponseEntity.status(HttpStatus.OK).body(fleet_vehicle_log_fuelService.remove(fleet_vehicle_log_fuel_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Fleet_vehicle_log_fuel" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/fleet_vehicle_log_fuels/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        fleet_vehicle_log_fuelService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Fleet_vehicle_log_fuel" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/fleet_vehicle_log_fuels")

    public ResponseEntity<Fleet_vehicle_log_fuelDTO> create(@RequestBody Fleet_vehicle_log_fuelDTO fleet_vehicle_log_fueldto) {
        Fleet_vehicle_log_fuel domain = fleet_vehicle_log_fuelMapping.toDomain(fleet_vehicle_log_fueldto);
		fleet_vehicle_log_fuelService.create(domain);
        Fleet_vehicle_log_fuelDTO dto = fleet_vehicle_log_fuelMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Fleet_vehicle_log_fuel" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/fleet_vehicle_log_fuels/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Fleet_vehicle_log_fuelDTO> fleet_vehicle_log_fueldtos) {
        fleet_vehicle_log_fuelService.createBatch(fleet_vehicle_log_fuelMapping.toDomain(fleet_vehicle_log_fueldtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission(#fleet_vehicle_log_fuel_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Fleet_vehicle_log_fuel" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/fleet_vehicle_log_fuels/{fleet_vehicle_log_fuel_id}")

    public ResponseEntity<Fleet_vehicle_log_fuelDTO> update(@PathVariable("fleet_vehicle_log_fuel_id") Integer fleet_vehicle_log_fuel_id, @RequestBody Fleet_vehicle_log_fuelDTO fleet_vehicle_log_fueldto) {
		Fleet_vehicle_log_fuel domain = fleet_vehicle_log_fuelMapping.toDomain(fleet_vehicle_log_fueldto);
        domain.setId(fleet_vehicle_log_fuel_id);
		fleet_vehicle_log_fuelService.update(domain);
		Fleet_vehicle_log_fuelDTO dto = fleet_vehicle_log_fuelMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#fleet_vehicle_log_fuel_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Fleet_vehicle_log_fuel" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/fleet_vehicle_log_fuels/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Fleet_vehicle_log_fuelDTO> fleet_vehicle_log_fueldtos) {
        fleet_vehicle_log_fuelService.updateBatch(fleet_vehicle_log_fuelMapping.toDomain(fleet_vehicle_log_fueldtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }







    @PreAuthorize("hasPermission(#fleet_vehicle_log_fuel_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Fleet_vehicle_log_fuel" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/fleet_vehicle_log_fuels/{fleet_vehicle_log_fuel_id}")
    public ResponseEntity<Fleet_vehicle_log_fuelDTO> get(@PathVariable("fleet_vehicle_log_fuel_id") Integer fleet_vehicle_log_fuel_id) {
        Fleet_vehicle_log_fuel domain = fleet_vehicle_log_fuelService.get(fleet_vehicle_log_fuel_id);
        Fleet_vehicle_log_fuelDTO dto = fleet_vehicle_log_fuelMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }




    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Fleet_vehicle_log_fuel" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/fleet_vehicle_log_fuels/fetchdefault")
	public ResponseEntity<List<Fleet_vehicle_log_fuelDTO>> fetchDefault(Fleet_vehicle_log_fuelSearchContext context) {
        Page<Fleet_vehicle_log_fuel> domains = fleet_vehicle_log_fuelService.searchDefault(context) ;
        List<Fleet_vehicle_log_fuelDTO> list = fleet_vehicle_log_fuelMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Fleet_vehicle_log_fuel" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/fleet_vehicle_log_fuels/searchdefault")
	public ResponseEntity<Page<Fleet_vehicle_log_fuelDTO>> searchDefault(Fleet_vehicle_log_fuelSearchContext context) {
        Page<Fleet_vehicle_log_fuel> domains = fleet_vehicle_log_fuelService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(fleet_vehicle_log_fuelMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Fleet_vehicle_log_fuel getEntity(){
        return new Fleet_vehicle_log_fuel();
    }

}
