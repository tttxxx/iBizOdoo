package cn.ibizlab.odoo.odoo_fleet.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_fleet.dto.*;
import cn.ibizlab.odoo.odoo_fleet.mapping.*;
import cn.ibizlab.odoo.core.odoo_fleet.domain.Fleet_service_type;
import cn.ibizlab.odoo.core.odoo_fleet.service.IFleet_service_typeService;
import cn.ibizlab.odoo.core.odoo_fleet.filter.Fleet_service_typeSearchContext;




@Slf4j
@Api(tags = {"Fleet_service_type" })
@RestController("odoo_fleet-fleet_service_type")
@RequestMapping("")
public class Fleet_service_typeResource {

    @Autowired
    private IFleet_service_typeService fleet_service_typeService;

    @Autowired
    @Lazy
    private Fleet_service_typeMapping fleet_service_typeMapping;










    @PreAuthorize("hasPermission(#fleet_service_type_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Fleet_service_type" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/fleet_service_types/{fleet_service_type_id}")

    public ResponseEntity<Fleet_service_typeDTO> update(@PathVariable("fleet_service_type_id") Integer fleet_service_type_id, @RequestBody Fleet_service_typeDTO fleet_service_typedto) {
		Fleet_service_type domain = fleet_service_typeMapping.toDomain(fleet_service_typedto);
        domain.setId(fleet_service_type_id);
		fleet_service_typeService.update(domain);
		Fleet_service_typeDTO dto = fleet_service_typeMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#fleet_service_type_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Fleet_service_type" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/fleet_service_types/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Fleet_service_typeDTO> fleet_service_typedtos) {
        fleet_service_typeService.updateBatch(fleet_service_typeMapping.toDomain(fleet_service_typedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission(#fleet_service_type_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Fleet_service_type" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/fleet_service_types/{fleet_service_type_id}")
    public ResponseEntity<Fleet_service_typeDTO> get(@PathVariable("fleet_service_type_id") Integer fleet_service_type_id) {
        Fleet_service_type domain = fleet_service_typeService.get(fleet_service_type_id);
        Fleet_service_typeDTO dto = fleet_service_typeMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }




    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Fleet_service_type" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/fleet_service_types")

    public ResponseEntity<Fleet_service_typeDTO> create(@RequestBody Fleet_service_typeDTO fleet_service_typedto) {
        Fleet_service_type domain = fleet_service_typeMapping.toDomain(fleet_service_typedto);
		fleet_service_typeService.create(domain);
        Fleet_service_typeDTO dto = fleet_service_typeMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Fleet_service_type" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/fleet_service_types/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Fleet_service_typeDTO> fleet_service_typedtos) {
        fleet_service_typeService.createBatch(fleet_service_typeMapping.toDomain(fleet_service_typedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }







    @PreAuthorize("hasPermission('Remove',{#fleet_service_type_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Fleet_service_type" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/fleet_service_types/{fleet_service_type_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("fleet_service_type_id") Integer fleet_service_type_id) {
         return ResponseEntity.status(HttpStatus.OK).body(fleet_service_typeService.remove(fleet_service_type_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Fleet_service_type" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/fleet_service_types/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        fleet_service_typeService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Fleet_service_type" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/fleet_service_types/fetchdefault")
	public ResponseEntity<List<Fleet_service_typeDTO>> fetchDefault(Fleet_service_typeSearchContext context) {
        Page<Fleet_service_type> domains = fleet_service_typeService.searchDefault(context) ;
        List<Fleet_service_typeDTO> list = fleet_service_typeMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Fleet_service_type" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/fleet_service_types/searchdefault")
	public ResponseEntity<Page<Fleet_service_typeDTO>> searchDefault(Fleet_service_typeSearchContext context) {
        Page<Fleet_service_type> domains = fleet_service_typeService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(fleet_service_typeMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Fleet_service_type getEntity(){
        return new Fleet_service_type();
    }

}
