package cn.ibizlab.odoo.odoo_fleet.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_fleet.dto.*;
import cn.ibizlab.odoo.odoo_fleet.mapping.*;
import cn.ibizlab.odoo.core.odoo_fleet.domain.Fleet_vehicle_state;
import cn.ibizlab.odoo.core.odoo_fleet.service.IFleet_vehicle_stateService;
import cn.ibizlab.odoo.core.odoo_fleet.filter.Fleet_vehicle_stateSearchContext;




@Slf4j
@Api(tags = {"Fleet_vehicle_state" })
@RestController("odoo_fleet-fleet_vehicle_state")
@RequestMapping("")
public class Fleet_vehicle_stateResource {

    @Autowired
    private IFleet_vehicle_stateService fleet_vehicle_stateService;

    @Autowired
    @Lazy
    private Fleet_vehicle_stateMapping fleet_vehicle_stateMapping;







    @PreAuthorize("hasPermission(#fleet_vehicle_state_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Fleet_vehicle_state" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/fleet_vehicle_states/{fleet_vehicle_state_id}")
    public ResponseEntity<Fleet_vehicle_stateDTO> get(@PathVariable("fleet_vehicle_state_id") Integer fleet_vehicle_state_id) {
        Fleet_vehicle_state domain = fleet_vehicle_stateService.get(fleet_vehicle_state_id);
        Fleet_vehicle_stateDTO dto = fleet_vehicle_stateMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }




    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Fleet_vehicle_state" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/fleet_vehicle_states")

    public ResponseEntity<Fleet_vehicle_stateDTO> create(@RequestBody Fleet_vehicle_stateDTO fleet_vehicle_statedto) {
        Fleet_vehicle_state domain = fleet_vehicle_stateMapping.toDomain(fleet_vehicle_statedto);
		fleet_vehicle_stateService.create(domain);
        Fleet_vehicle_stateDTO dto = fleet_vehicle_stateMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Fleet_vehicle_state" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/fleet_vehicle_states/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Fleet_vehicle_stateDTO> fleet_vehicle_statedtos) {
        fleet_vehicle_stateService.createBatch(fleet_vehicle_stateMapping.toDomain(fleet_vehicle_statedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission(#fleet_vehicle_state_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Fleet_vehicle_state" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/fleet_vehicle_states/{fleet_vehicle_state_id}")

    public ResponseEntity<Fleet_vehicle_stateDTO> update(@PathVariable("fleet_vehicle_state_id") Integer fleet_vehicle_state_id, @RequestBody Fleet_vehicle_stateDTO fleet_vehicle_statedto) {
		Fleet_vehicle_state domain = fleet_vehicle_stateMapping.toDomain(fleet_vehicle_statedto);
        domain.setId(fleet_vehicle_state_id);
		fleet_vehicle_stateService.update(domain);
		Fleet_vehicle_stateDTO dto = fleet_vehicle_stateMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#fleet_vehicle_state_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Fleet_vehicle_state" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/fleet_vehicle_states/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Fleet_vehicle_stateDTO> fleet_vehicle_statedtos) {
        fleet_vehicle_stateService.updateBatch(fleet_vehicle_stateMapping.toDomain(fleet_vehicle_statedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }










    @PreAuthorize("hasPermission('Remove',{#fleet_vehicle_state_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Fleet_vehicle_state" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/fleet_vehicle_states/{fleet_vehicle_state_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("fleet_vehicle_state_id") Integer fleet_vehicle_state_id) {
         return ResponseEntity.status(HttpStatus.OK).body(fleet_vehicle_stateService.remove(fleet_vehicle_state_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Fleet_vehicle_state" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/fleet_vehicle_states/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        fleet_vehicle_stateService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Fleet_vehicle_state" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/fleet_vehicle_states/fetchdefault")
	public ResponseEntity<List<Fleet_vehicle_stateDTO>> fetchDefault(Fleet_vehicle_stateSearchContext context) {
        Page<Fleet_vehicle_state> domains = fleet_vehicle_stateService.searchDefault(context) ;
        List<Fleet_vehicle_stateDTO> list = fleet_vehicle_stateMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Fleet_vehicle_state" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/fleet_vehicle_states/searchdefault")
	public ResponseEntity<Page<Fleet_vehicle_stateDTO>> searchDefault(Fleet_vehicle_stateSearchContext context) {
        Page<Fleet_vehicle_state> domains = fleet_vehicle_stateService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(fleet_vehicle_stateMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Fleet_vehicle_state getEntity(){
        return new Fleet_vehicle_state();
    }

}
