package cn.ibizlab.odoo.odoo_fleet.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_fleet.dto.*;
import cn.ibizlab.odoo.odoo_fleet.mapping.*;
import cn.ibizlab.odoo.core.odoo_fleet.domain.Fleet_vehicle_log_contract;
import cn.ibizlab.odoo.core.odoo_fleet.service.IFleet_vehicle_log_contractService;
import cn.ibizlab.odoo.core.odoo_fleet.filter.Fleet_vehicle_log_contractSearchContext;




@Slf4j
@Api(tags = {"Fleet_vehicle_log_contract" })
@RestController("odoo_fleet-fleet_vehicle_log_contract")
@RequestMapping("")
public class Fleet_vehicle_log_contractResource {

    @Autowired
    private IFleet_vehicle_log_contractService fleet_vehicle_log_contractService;

    @Autowired
    @Lazy
    private Fleet_vehicle_log_contractMapping fleet_vehicle_log_contractMapping;




    @PreAuthorize("hasPermission(#fleet_vehicle_log_contract_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Fleet_vehicle_log_contract" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/fleet_vehicle_log_contracts/{fleet_vehicle_log_contract_id}")
    public ResponseEntity<Fleet_vehicle_log_contractDTO> get(@PathVariable("fleet_vehicle_log_contract_id") Integer fleet_vehicle_log_contract_id) {
        Fleet_vehicle_log_contract domain = fleet_vehicle_log_contractService.get(fleet_vehicle_log_contract_id);
        Fleet_vehicle_log_contractDTO dto = fleet_vehicle_log_contractMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }







    @PreAuthorize("hasPermission(#fleet_vehicle_log_contract_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Fleet_vehicle_log_contract" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/fleet_vehicle_log_contracts/{fleet_vehicle_log_contract_id}")

    public ResponseEntity<Fleet_vehicle_log_contractDTO> update(@PathVariable("fleet_vehicle_log_contract_id") Integer fleet_vehicle_log_contract_id, @RequestBody Fleet_vehicle_log_contractDTO fleet_vehicle_log_contractdto) {
		Fleet_vehicle_log_contract domain = fleet_vehicle_log_contractMapping.toDomain(fleet_vehicle_log_contractdto);
        domain.setId(fleet_vehicle_log_contract_id);
		fleet_vehicle_log_contractService.update(domain);
		Fleet_vehicle_log_contractDTO dto = fleet_vehicle_log_contractMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#fleet_vehicle_log_contract_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Fleet_vehicle_log_contract" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/fleet_vehicle_log_contracts/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Fleet_vehicle_log_contractDTO> fleet_vehicle_log_contractdtos) {
        fleet_vehicle_log_contractService.updateBatch(fleet_vehicle_log_contractMapping.toDomain(fleet_vehicle_log_contractdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission('Remove',{#fleet_vehicle_log_contract_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Fleet_vehicle_log_contract" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/fleet_vehicle_log_contracts/{fleet_vehicle_log_contract_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("fleet_vehicle_log_contract_id") Integer fleet_vehicle_log_contract_id) {
         return ResponseEntity.status(HttpStatus.OK).body(fleet_vehicle_log_contractService.remove(fleet_vehicle_log_contract_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Fleet_vehicle_log_contract" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/fleet_vehicle_log_contracts/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        fleet_vehicle_log_contractService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }







    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Fleet_vehicle_log_contract" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/fleet_vehicle_log_contracts")

    public ResponseEntity<Fleet_vehicle_log_contractDTO> create(@RequestBody Fleet_vehicle_log_contractDTO fleet_vehicle_log_contractdto) {
        Fleet_vehicle_log_contract domain = fleet_vehicle_log_contractMapping.toDomain(fleet_vehicle_log_contractdto);
		fleet_vehicle_log_contractService.create(domain);
        Fleet_vehicle_log_contractDTO dto = fleet_vehicle_log_contractMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Fleet_vehicle_log_contract" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/fleet_vehicle_log_contracts/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Fleet_vehicle_log_contractDTO> fleet_vehicle_log_contractdtos) {
        fleet_vehicle_log_contractService.createBatch(fleet_vehicle_log_contractMapping.toDomain(fleet_vehicle_log_contractdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Fleet_vehicle_log_contract" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/fleet_vehicle_log_contracts/fetchdefault")
	public ResponseEntity<List<Fleet_vehicle_log_contractDTO>> fetchDefault(Fleet_vehicle_log_contractSearchContext context) {
        Page<Fleet_vehicle_log_contract> domains = fleet_vehicle_log_contractService.searchDefault(context) ;
        List<Fleet_vehicle_log_contractDTO> list = fleet_vehicle_log_contractMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Fleet_vehicle_log_contract" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/fleet_vehicle_log_contracts/searchdefault")
	public ResponseEntity<Page<Fleet_vehicle_log_contractDTO>> searchDefault(Fleet_vehicle_log_contractSearchContext context) {
        Page<Fleet_vehicle_log_contract> domains = fleet_vehicle_log_contractService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(fleet_vehicle_log_contractMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Fleet_vehicle_log_contract getEntity(){
        return new Fleet_vehicle_log_contract();
    }

}
