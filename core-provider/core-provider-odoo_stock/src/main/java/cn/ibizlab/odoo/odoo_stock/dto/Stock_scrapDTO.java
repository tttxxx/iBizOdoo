package cn.ibizlab.odoo.odoo_stock.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;
import cn.ibizlab.odoo.util.domain.DTOBase;
import lombok.Data;

/**
 * 服务DTO对象[Stock_scrapDTO]
 */
@Data
public class Stock_scrapDTO extends DTOBase implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [WRITE_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;

    /**
     * 属性 [DATE_EXPECTED]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "date_expected" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("date_expected")
    private Timestamp dateExpected;

    /**
     * 属性 [SCRAP_QTY]
     *
     */
    @JSONField(name = "scrap_qty")
    @JsonProperty("scrap_qty")
    private Double scrapQty;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;

    /**
     * 属性 [ORIGIN]
     *
     */
    @JSONField(name = "origin")
    @JsonProperty("origin")
    private String origin;

    /**
     * 属性 [STATE]
     *
     */
    @JSONField(name = "state")
    @JsonProperty("state")
    private String state;

    /**
     * 属性 [ID]
     *
     */
    @JSONField(name = "id")
    @JsonProperty("id")
    private Integer id;

    /**
     * 属性 [CREATE_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    private String displayName;

    /**
     * 属性 [NAME]
     *
     */
    @JSONField(name = "name")
    @JsonProperty("name")
    private String name;

    /**
     * 属性 [WORKORDER_ID_TEXT]
     *
     */
    @JSONField(name = "workorder_id_text")
    @JsonProperty("workorder_id_text")
    private String workorderIdText;

    /**
     * 属性 [SCRAP_LOCATION_ID_TEXT]
     *
     */
    @JSONField(name = "scrap_location_id_text")
    @JsonProperty("scrap_location_id_text")
    private String scrapLocationIdText;

    /**
     * 属性 [LOT_ID_TEXT]
     *
     */
    @JSONField(name = "lot_id_text")
    @JsonProperty("lot_id_text")
    private String lotIdText;

    /**
     * 属性 [WRITE_UID_TEXT]
     *
     */
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    private String writeUidText;

    /**
     * 属性 [MOVE_ID_TEXT]
     *
     */
    @JSONField(name = "move_id_text")
    @JsonProperty("move_id_text")
    private String moveIdText;

    /**
     * 属性 [PICKING_ID_TEXT]
     *
     */
    @JSONField(name = "picking_id_text")
    @JsonProperty("picking_id_text")
    private String pickingIdText;

    /**
     * 属性 [PRODUCTION_ID_TEXT]
     *
     */
    @JSONField(name = "production_id_text")
    @JsonProperty("production_id_text")
    private String productionIdText;

    /**
     * 属性 [LOCATION_ID_TEXT]
     *
     */
    @JSONField(name = "location_id_text")
    @JsonProperty("location_id_text")
    private String locationIdText;

    /**
     * 属性 [PRODUCT_UOM_ID_TEXT]
     *
     */
    @JSONField(name = "product_uom_id_text")
    @JsonProperty("product_uom_id_text")
    private String productUomIdText;

    /**
     * 属性 [TRACKING]
     *
     */
    @JSONField(name = "tracking")
    @JsonProperty("tracking")
    private String tracking;

    /**
     * 属性 [PRODUCT_ID_TEXT]
     *
     */
    @JSONField(name = "product_id_text")
    @JsonProperty("product_id_text")
    private String productIdText;

    /**
     * 属性 [PACKAGE_ID_TEXT]
     *
     */
    @JSONField(name = "package_id_text")
    @JsonProperty("package_id_text")
    private String packageIdText;

    /**
     * 属性 [CREATE_UID_TEXT]
     *
     */
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    private String createUidText;

    /**
     * 属性 [OWNER_ID_TEXT]
     *
     */
    @JSONField(name = "owner_id_text")
    @JsonProperty("owner_id_text")
    private String ownerIdText;

    /**
     * 属性 [CREATE_UID]
     *
     */
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    private Integer createUid;

    /**
     * 属性 [LOT_ID]
     *
     */
    @JSONField(name = "lot_id")
    @JsonProperty("lot_id")
    private Integer lotId;

    /**
     * 属性 [PRODUCTION_ID]
     *
     */
    @JSONField(name = "production_id")
    @JsonProperty("production_id")
    private Integer productionId;

    /**
     * 属性 [PICKING_ID]
     *
     */
    @JSONField(name = "picking_id")
    @JsonProperty("picking_id")
    private Integer pickingId;

    /**
     * 属性 [SCRAP_LOCATION_ID]
     *
     */
    @JSONField(name = "scrap_location_id")
    @JsonProperty("scrap_location_id")
    private Integer scrapLocationId;

    /**
     * 属性 [MOVE_ID]
     *
     */
    @JSONField(name = "move_id")
    @JsonProperty("move_id")
    private Integer moveId;

    /**
     * 属性 [LOCATION_ID]
     *
     */
    @JSONField(name = "location_id")
    @JsonProperty("location_id")
    private Integer locationId;

    /**
     * 属性 [WRITE_UID]
     *
     */
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    private Integer writeUid;

    /**
     * 属性 [WORKORDER_ID]
     *
     */
    @JSONField(name = "workorder_id")
    @JsonProperty("workorder_id")
    private Integer workorderId;

    /**
     * 属性 [PRODUCT_UOM_ID]
     *
     */
    @JSONField(name = "product_uom_id")
    @JsonProperty("product_uom_id")
    private Integer productUomId;

    /**
     * 属性 [PACKAGE_ID]
     *
     */
    @JSONField(name = "package_id")
    @JsonProperty("package_id")
    private Integer packageId;

    /**
     * 属性 [PRODUCT_ID]
     *
     */
    @JSONField(name = "product_id")
    @JsonProperty("product_id")
    private Integer productId;

    /**
     * 属性 [OWNER_ID]
     *
     */
    @JSONField(name = "owner_id")
    @JsonProperty("owner_id")
    private Integer ownerId;


    /**
     * 设置 [DATE_EXPECTED]
     */
    public void setDateExpected(Timestamp  dateExpected){
        this.dateExpected = dateExpected ;
        this.modify("date_expected",dateExpected);
    }

    /**
     * 设置 [SCRAP_QTY]
     */
    public void setScrapQty(Double  scrapQty){
        this.scrapQty = scrapQty ;
        this.modify("scrap_qty",scrapQty);
    }

    /**
     * 设置 [ORIGIN]
     */
    public void setOrigin(String  origin){
        this.origin = origin ;
        this.modify("origin",origin);
    }

    /**
     * 设置 [STATE]
     */
    public void setState(String  state){
        this.state = state ;
        this.modify("state",state);
    }

    /**
     * 设置 [NAME]
     */
    public void setName(String  name){
        this.name = name ;
        this.modify("name",name);
    }

    /**
     * 设置 [LOT_ID]
     */
    public void setLotId(Integer  lotId){
        this.lotId = lotId ;
        this.modify("lot_id",lotId);
    }

    /**
     * 设置 [PRODUCTION_ID]
     */
    public void setProductionId(Integer  productionId){
        this.productionId = productionId ;
        this.modify("production_id",productionId);
    }

    /**
     * 设置 [PICKING_ID]
     */
    public void setPickingId(Integer  pickingId){
        this.pickingId = pickingId ;
        this.modify("picking_id",pickingId);
    }

    /**
     * 设置 [SCRAP_LOCATION_ID]
     */
    public void setScrapLocationId(Integer  scrapLocationId){
        this.scrapLocationId = scrapLocationId ;
        this.modify("scrap_location_id",scrapLocationId);
    }

    /**
     * 设置 [MOVE_ID]
     */
    public void setMoveId(Integer  moveId){
        this.moveId = moveId ;
        this.modify("move_id",moveId);
    }

    /**
     * 设置 [LOCATION_ID]
     */
    public void setLocationId(Integer  locationId){
        this.locationId = locationId ;
        this.modify("location_id",locationId);
    }

    /**
     * 设置 [WORKORDER_ID]
     */
    public void setWorkorderId(Integer  workorderId){
        this.workorderId = workorderId ;
        this.modify("workorder_id",workorderId);
    }

    /**
     * 设置 [PRODUCT_UOM_ID]
     */
    public void setProductUomId(Integer  productUomId){
        this.productUomId = productUomId ;
        this.modify("product_uom_id",productUomId);
    }

    /**
     * 设置 [PACKAGE_ID]
     */
    public void setPackageId(Integer  packageId){
        this.packageId = packageId ;
        this.modify("package_id",packageId);
    }

    /**
     * 设置 [PRODUCT_ID]
     */
    public void setProductId(Integer  productId){
        this.productId = productId ;
        this.modify("product_id",productId);
    }

    /**
     * 设置 [OWNER_ID]
     */
    public void setOwnerId(Integer  ownerId){
        this.ownerId = ownerId ;
        this.modify("owner_id",ownerId);
    }


}

