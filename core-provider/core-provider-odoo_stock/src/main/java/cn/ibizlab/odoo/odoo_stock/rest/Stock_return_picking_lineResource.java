package cn.ibizlab.odoo.odoo_stock.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_stock.dto.*;
import cn.ibizlab.odoo.odoo_stock.mapping.*;
import cn.ibizlab.odoo.core.odoo_stock.domain.Stock_return_picking_line;
import cn.ibizlab.odoo.core.odoo_stock.service.IStock_return_picking_lineService;
import cn.ibizlab.odoo.core.odoo_stock.filter.Stock_return_picking_lineSearchContext;




@Slf4j
@Api(tags = {"Stock_return_picking_line" })
@RestController("odoo_stock-stock_return_picking_line")
@RequestMapping("")
public class Stock_return_picking_lineResource {

    @Autowired
    private IStock_return_picking_lineService stock_return_picking_lineService;

    @Autowired
    @Lazy
    private Stock_return_picking_lineMapping stock_return_picking_lineMapping;




    @PreAuthorize("hasPermission(#stock_return_picking_line_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Stock_return_picking_line" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/stock_return_picking_lines/{stock_return_picking_line_id}")

    public ResponseEntity<Stock_return_picking_lineDTO> update(@PathVariable("stock_return_picking_line_id") Integer stock_return_picking_line_id, @RequestBody Stock_return_picking_lineDTO stock_return_picking_linedto) {
		Stock_return_picking_line domain = stock_return_picking_lineMapping.toDomain(stock_return_picking_linedto);
        domain.setId(stock_return_picking_line_id);
		stock_return_picking_lineService.update(domain);
		Stock_return_picking_lineDTO dto = stock_return_picking_lineMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#stock_return_picking_line_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Stock_return_picking_line" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/stock_return_picking_lines/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Stock_return_picking_lineDTO> stock_return_picking_linedtos) {
        stock_return_picking_lineService.updateBatch(stock_return_picking_lineMapping.toDomain(stock_return_picking_linedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }










    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Stock_return_picking_line" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/stock_return_picking_lines")

    public ResponseEntity<Stock_return_picking_lineDTO> create(@RequestBody Stock_return_picking_lineDTO stock_return_picking_linedto) {
        Stock_return_picking_line domain = stock_return_picking_lineMapping.toDomain(stock_return_picking_linedto);
		stock_return_picking_lineService.create(domain);
        Stock_return_picking_lineDTO dto = stock_return_picking_lineMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Stock_return_picking_line" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/stock_return_picking_lines/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Stock_return_picking_lineDTO> stock_return_picking_linedtos) {
        stock_return_picking_lineService.createBatch(stock_return_picking_lineMapping.toDomain(stock_return_picking_linedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission(#stock_return_picking_line_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Stock_return_picking_line" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/stock_return_picking_lines/{stock_return_picking_line_id}")
    public ResponseEntity<Stock_return_picking_lineDTO> get(@PathVariable("stock_return_picking_line_id") Integer stock_return_picking_line_id) {
        Stock_return_picking_line domain = stock_return_picking_lineService.get(stock_return_picking_line_id);
        Stock_return_picking_lineDTO dto = stock_return_picking_lineMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }




    @PreAuthorize("hasPermission('Remove',{#stock_return_picking_line_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Stock_return_picking_line" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/stock_return_picking_lines/{stock_return_picking_line_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("stock_return_picking_line_id") Integer stock_return_picking_line_id) {
         return ResponseEntity.status(HttpStatus.OK).body(stock_return_picking_lineService.remove(stock_return_picking_line_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Stock_return_picking_line" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/stock_return_picking_lines/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        stock_return_picking_lineService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Stock_return_picking_line" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/stock_return_picking_lines/fetchdefault")
	public ResponseEntity<List<Stock_return_picking_lineDTO>> fetchDefault(Stock_return_picking_lineSearchContext context) {
        Page<Stock_return_picking_line> domains = stock_return_picking_lineService.searchDefault(context) ;
        List<Stock_return_picking_lineDTO> list = stock_return_picking_lineMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Stock_return_picking_line" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/stock_return_picking_lines/searchdefault")
	public ResponseEntity<Page<Stock_return_picking_lineDTO>> searchDefault(Stock_return_picking_lineSearchContext context) {
        Page<Stock_return_picking_line> domains = stock_return_picking_lineService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(stock_return_picking_lineMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Stock_return_picking_line getEntity(){
        return new Stock_return_picking_line();
    }

}
