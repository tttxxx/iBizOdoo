package cn.ibizlab.odoo.odoo_stock.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_stock.dto.*;
import cn.ibizlab.odoo.odoo_stock.mapping.*;
import cn.ibizlab.odoo.core.odoo_stock.domain.Stock_inventory;
import cn.ibizlab.odoo.core.odoo_stock.service.IStock_inventoryService;
import cn.ibizlab.odoo.core.odoo_stock.filter.Stock_inventorySearchContext;




@Slf4j
@Api(tags = {"Stock_inventory" })
@RestController("odoo_stock-stock_inventory")
@RequestMapping("")
public class Stock_inventoryResource {

    @Autowired
    private IStock_inventoryService stock_inventoryService;

    @Autowired
    @Lazy
    private Stock_inventoryMapping stock_inventoryMapping;




    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Stock_inventory" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/stock_inventories")

    public ResponseEntity<Stock_inventoryDTO> create(@RequestBody Stock_inventoryDTO stock_inventorydto) {
        Stock_inventory domain = stock_inventoryMapping.toDomain(stock_inventorydto);
		stock_inventoryService.create(domain);
        Stock_inventoryDTO dto = stock_inventoryMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Stock_inventory" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/stock_inventories/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Stock_inventoryDTO> stock_inventorydtos) {
        stock_inventoryService.createBatch(stock_inventoryMapping.toDomain(stock_inventorydtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission(#stock_inventory_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Stock_inventory" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/stock_inventories/{stock_inventory_id}")
    public ResponseEntity<Stock_inventoryDTO> get(@PathVariable("stock_inventory_id") Integer stock_inventory_id) {
        Stock_inventory domain = stock_inventoryService.get(stock_inventory_id);
        Stock_inventoryDTO dto = stock_inventoryMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }




    @PreAuthorize("hasPermission(#stock_inventory_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Stock_inventory" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/stock_inventories/{stock_inventory_id}")

    public ResponseEntity<Stock_inventoryDTO> update(@PathVariable("stock_inventory_id") Integer stock_inventory_id, @RequestBody Stock_inventoryDTO stock_inventorydto) {
		Stock_inventory domain = stock_inventoryMapping.toDomain(stock_inventorydto);
        domain.setId(stock_inventory_id);
		stock_inventoryService.update(domain);
		Stock_inventoryDTO dto = stock_inventoryMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#stock_inventory_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Stock_inventory" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/stock_inventories/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Stock_inventoryDTO> stock_inventorydtos) {
        stock_inventoryService.updateBatch(stock_inventoryMapping.toDomain(stock_inventorydtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }










    @PreAuthorize("hasPermission('Remove',{#stock_inventory_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Stock_inventory" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/stock_inventories/{stock_inventory_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("stock_inventory_id") Integer stock_inventory_id) {
         return ResponseEntity.status(HttpStatus.OK).body(stock_inventoryService.remove(stock_inventory_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Stock_inventory" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/stock_inventories/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        stock_inventoryService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Stock_inventory" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/stock_inventories/fetchdefault")
	public ResponseEntity<List<Stock_inventoryDTO>> fetchDefault(Stock_inventorySearchContext context) {
        Page<Stock_inventory> domains = stock_inventoryService.searchDefault(context) ;
        List<Stock_inventoryDTO> list = stock_inventoryMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Stock_inventory" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/stock_inventories/searchdefault")
	public ResponseEntity<Page<Stock_inventoryDTO>> searchDefault(Stock_inventorySearchContext context) {
        Page<Stock_inventory> domains = stock_inventoryService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(stock_inventoryMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Stock_inventory getEntity(){
        return new Stock_inventory();
    }

}
