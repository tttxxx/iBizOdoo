package cn.ibizlab.odoo.odoo_stock.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_stock.dto.*;
import cn.ibizlab.odoo.odoo_stock.mapping.*;
import cn.ibizlab.odoo.core.odoo_stock.domain.Stock_warn_insufficient_qty_repair;
import cn.ibizlab.odoo.core.odoo_stock.service.IStock_warn_insufficient_qty_repairService;
import cn.ibizlab.odoo.core.odoo_stock.filter.Stock_warn_insufficient_qty_repairSearchContext;




@Slf4j
@Api(tags = {"Stock_warn_insufficient_qty_repair" })
@RestController("odoo_stock-stock_warn_insufficient_qty_repair")
@RequestMapping("")
public class Stock_warn_insufficient_qty_repairResource {

    @Autowired
    private IStock_warn_insufficient_qty_repairService stock_warn_insufficient_qty_repairService;

    @Autowired
    @Lazy
    private Stock_warn_insufficient_qty_repairMapping stock_warn_insufficient_qty_repairMapping;




    @PreAuthorize("hasPermission('Remove',{#stock_warn_insufficient_qty_repair_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Stock_warn_insufficient_qty_repair" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/stock_warn_insufficient_qty_repairs/{stock_warn_insufficient_qty_repair_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("stock_warn_insufficient_qty_repair_id") Integer stock_warn_insufficient_qty_repair_id) {
         return ResponseEntity.status(HttpStatus.OK).body(stock_warn_insufficient_qty_repairService.remove(stock_warn_insufficient_qty_repair_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Stock_warn_insufficient_qty_repair" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/stock_warn_insufficient_qty_repairs/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        stock_warn_insufficient_qty_repairService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission(#stock_warn_insufficient_qty_repair_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Stock_warn_insufficient_qty_repair" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/stock_warn_insufficient_qty_repairs/{stock_warn_insufficient_qty_repair_id}")
    public ResponseEntity<Stock_warn_insufficient_qty_repairDTO> get(@PathVariable("stock_warn_insufficient_qty_repair_id") Integer stock_warn_insufficient_qty_repair_id) {
        Stock_warn_insufficient_qty_repair domain = stock_warn_insufficient_qty_repairService.get(stock_warn_insufficient_qty_repair_id);
        Stock_warn_insufficient_qty_repairDTO dto = stock_warn_insufficient_qty_repairMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }













    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Stock_warn_insufficient_qty_repair" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/stock_warn_insufficient_qty_repairs")

    public ResponseEntity<Stock_warn_insufficient_qty_repairDTO> create(@RequestBody Stock_warn_insufficient_qty_repairDTO stock_warn_insufficient_qty_repairdto) {
        Stock_warn_insufficient_qty_repair domain = stock_warn_insufficient_qty_repairMapping.toDomain(stock_warn_insufficient_qty_repairdto);
		stock_warn_insufficient_qty_repairService.create(domain);
        Stock_warn_insufficient_qty_repairDTO dto = stock_warn_insufficient_qty_repairMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Stock_warn_insufficient_qty_repair" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/stock_warn_insufficient_qty_repairs/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Stock_warn_insufficient_qty_repairDTO> stock_warn_insufficient_qty_repairdtos) {
        stock_warn_insufficient_qty_repairService.createBatch(stock_warn_insufficient_qty_repairMapping.toDomain(stock_warn_insufficient_qty_repairdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission(#stock_warn_insufficient_qty_repair_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Stock_warn_insufficient_qty_repair" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/stock_warn_insufficient_qty_repairs/{stock_warn_insufficient_qty_repair_id}")

    public ResponseEntity<Stock_warn_insufficient_qty_repairDTO> update(@PathVariable("stock_warn_insufficient_qty_repair_id") Integer stock_warn_insufficient_qty_repair_id, @RequestBody Stock_warn_insufficient_qty_repairDTO stock_warn_insufficient_qty_repairdto) {
		Stock_warn_insufficient_qty_repair domain = stock_warn_insufficient_qty_repairMapping.toDomain(stock_warn_insufficient_qty_repairdto);
        domain.setId(stock_warn_insufficient_qty_repair_id);
		stock_warn_insufficient_qty_repairService.update(domain);
		Stock_warn_insufficient_qty_repairDTO dto = stock_warn_insufficient_qty_repairMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#stock_warn_insufficient_qty_repair_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Stock_warn_insufficient_qty_repair" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/stock_warn_insufficient_qty_repairs/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Stock_warn_insufficient_qty_repairDTO> stock_warn_insufficient_qty_repairdtos) {
        stock_warn_insufficient_qty_repairService.updateBatch(stock_warn_insufficient_qty_repairMapping.toDomain(stock_warn_insufficient_qty_repairdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Stock_warn_insufficient_qty_repair" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/stock_warn_insufficient_qty_repairs/fetchdefault")
	public ResponseEntity<List<Stock_warn_insufficient_qty_repairDTO>> fetchDefault(Stock_warn_insufficient_qty_repairSearchContext context) {
        Page<Stock_warn_insufficient_qty_repair> domains = stock_warn_insufficient_qty_repairService.searchDefault(context) ;
        List<Stock_warn_insufficient_qty_repairDTO> list = stock_warn_insufficient_qty_repairMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Stock_warn_insufficient_qty_repair" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/stock_warn_insufficient_qty_repairs/searchdefault")
	public ResponseEntity<Page<Stock_warn_insufficient_qty_repairDTO>> searchDefault(Stock_warn_insufficient_qty_repairSearchContext context) {
        Page<Stock_warn_insufficient_qty_repair> domains = stock_warn_insufficient_qty_repairService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(stock_warn_insufficient_qty_repairMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Stock_warn_insufficient_qty_repair getEntity(){
        return new Stock_warn_insufficient_qty_repair();
    }

}
