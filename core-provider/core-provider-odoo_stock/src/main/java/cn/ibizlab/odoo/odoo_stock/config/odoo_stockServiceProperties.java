package cn.ibizlab.odoo.odoo_stock.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import lombok.Data;

@ConfigurationProperties(prefix = "service.odoo-stock")
@Data
public class odoo_stockServiceProperties {

	private boolean enabled;

	private boolean auth;


}