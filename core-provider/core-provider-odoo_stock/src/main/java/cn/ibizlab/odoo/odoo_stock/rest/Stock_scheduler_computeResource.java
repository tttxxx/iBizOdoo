package cn.ibizlab.odoo.odoo_stock.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_stock.dto.*;
import cn.ibizlab.odoo.odoo_stock.mapping.*;
import cn.ibizlab.odoo.core.odoo_stock.domain.Stock_scheduler_compute;
import cn.ibizlab.odoo.core.odoo_stock.service.IStock_scheduler_computeService;
import cn.ibizlab.odoo.core.odoo_stock.filter.Stock_scheduler_computeSearchContext;




@Slf4j
@Api(tags = {"Stock_scheduler_compute" })
@RestController("odoo_stock-stock_scheduler_compute")
@RequestMapping("")
public class Stock_scheduler_computeResource {

    @Autowired
    private IStock_scheduler_computeService stock_scheduler_computeService;

    @Autowired
    @Lazy
    private Stock_scheduler_computeMapping stock_scheduler_computeMapping;




    @PreAuthorize("hasPermission('Remove',{#stock_scheduler_compute_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Stock_scheduler_compute" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/stock_scheduler_computes/{stock_scheduler_compute_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("stock_scheduler_compute_id") Integer stock_scheduler_compute_id) {
         return ResponseEntity.status(HttpStatus.OK).body(stock_scheduler_computeService.remove(stock_scheduler_compute_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Stock_scheduler_compute" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/stock_scheduler_computes/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        stock_scheduler_computeService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }







    @PreAuthorize("hasPermission(#stock_scheduler_compute_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Stock_scheduler_compute" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/stock_scheduler_computes/{stock_scheduler_compute_id}")

    public ResponseEntity<Stock_scheduler_computeDTO> update(@PathVariable("stock_scheduler_compute_id") Integer stock_scheduler_compute_id, @RequestBody Stock_scheduler_computeDTO stock_scheduler_computedto) {
		Stock_scheduler_compute domain = stock_scheduler_computeMapping.toDomain(stock_scheduler_computedto);
        domain.setId(stock_scheduler_compute_id);
		stock_scheduler_computeService.update(domain);
		Stock_scheduler_computeDTO dto = stock_scheduler_computeMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#stock_scheduler_compute_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Stock_scheduler_compute" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/stock_scheduler_computes/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Stock_scheduler_computeDTO> stock_scheduler_computedtos) {
        stock_scheduler_computeService.updateBatch(stock_scheduler_computeMapping.toDomain(stock_scheduler_computedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Stock_scheduler_compute" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/stock_scheduler_computes")

    public ResponseEntity<Stock_scheduler_computeDTO> create(@RequestBody Stock_scheduler_computeDTO stock_scheduler_computedto) {
        Stock_scheduler_compute domain = stock_scheduler_computeMapping.toDomain(stock_scheduler_computedto);
		stock_scheduler_computeService.create(domain);
        Stock_scheduler_computeDTO dto = stock_scheduler_computeMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Stock_scheduler_compute" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/stock_scheduler_computes/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Stock_scheduler_computeDTO> stock_scheduler_computedtos) {
        stock_scheduler_computeService.createBatch(stock_scheduler_computeMapping.toDomain(stock_scheduler_computedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }










    @PreAuthorize("hasPermission(#stock_scheduler_compute_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Stock_scheduler_compute" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/stock_scheduler_computes/{stock_scheduler_compute_id}")
    public ResponseEntity<Stock_scheduler_computeDTO> get(@PathVariable("stock_scheduler_compute_id") Integer stock_scheduler_compute_id) {
        Stock_scheduler_compute domain = stock_scheduler_computeService.get(stock_scheduler_compute_id);
        Stock_scheduler_computeDTO dto = stock_scheduler_computeMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Stock_scheduler_compute" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/stock_scheduler_computes/fetchdefault")
	public ResponseEntity<List<Stock_scheduler_computeDTO>> fetchDefault(Stock_scheduler_computeSearchContext context) {
        Page<Stock_scheduler_compute> domains = stock_scheduler_computeService.searchDefault(context) ;
        List<Stock_scheduler_computeDTO> list = stock_scheduler_computeMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Stock_scheduler_compute" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/stock_scheduler_computes/searchdefault")
	public ResponseEntity<Page<Stock_scheduler_computeDTO>> searchDefault(Stock_scheduler_computeSearchContext context) {
        Page<Stock_scheduler_compute> domains = stock_scheduler_computeService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(stock_scheduler_computeMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Stock_scheduler_compute getEntity(){
        return new Stock_scheduler_compute();
    }

}
