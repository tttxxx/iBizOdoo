package cn.ibizlab.odoo.odoo_stock.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_stock.dto.*;
import cn.ibizlab.odoo.odoo_stock.mapping.*;
import cn.ibizlab.odoo.core.odoo_stock.domain.Stock_inventory_line;
import cn.ibizlab.odoo.core.odoo_stock.service.IStock_inventory_lineService;
import cn.ibizlab.odoo.core.odoo_stock.filter.Stock_inventory_lineSearchContext;




@Slf4j
@Api(tags = {"Stock_inventory_line" })
@RestController("odoo_stock-stock_inventory_line")
@RequestMapping("")
public class Stock_inventory_lineResource {

    @Autowired
    private IStock_inventory_lineService stock_inventory_lineService;

    @Autowired
    @Lazy
    private Stock_inventory_lineMapping stock_inventory_lineMapping;




    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Stock_inventory_line" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/stock_inventory_lines")

    public ResponseEntity<Stock_inventory_lineDTO> create(@RequestBody Stock_inventory_lineDTO stock_inventory_linedto) {
        Stock_inventory_line domain = stock_inventory_lineMapping.toDomain(stock_inventory_linedto);
		stock_inventory_lineService.create(domain);
        Stock_inventory_lineDTO dto = stock_inventory_lineMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Stock_inventory_line" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/stock_inventory_lines/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Stock_inventory_lineDTO> stock_inventory_linedtos) {
        stock_inventory_lineService.createBatch(stock_inventory_lineMapping.toDomain(stock_inventory_linedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission(#stock_inventory_line_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Stock_inventory_line" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/stock_inventory_lines/{stock_inventory_line_id}")
    public ResponseEntity<Stock_inventory_lineDTO> get(@PathVariable("stock_inventory_line_id") Integer stock_inventory_line_id) {
        Stock_inventory_line domain = stock_inventory_lineService.get(stock_inventory_line_id);
        Stock_inventory_lineDTO dto = stock_inventory_lineMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }




    @PreAuthorize("hasPermission(#stock_inventory_line_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Stock_inventory_line" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/stock_inventory_lines/{stock_inventory_line_id}")

    public ResponseEntity<Stock_inventory_lineDTO> update(@PathVariable("stock_inventory_line_id") Integer stock_inventory_line_id, @RequestBody Stock_inventory_lineDTO stock_inventory_linedto) {
		Stock_inventory_line domain = stock_inventory_lineMapping.toDomain(stock_inventory_linedto);
        domain.setId(stock_inventory_line_id);
		stock_inventory_lineService.update(domain);
		Stock_inventory_lineDTO dto = stock_inventory_lineMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#stock_inventory_line_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Stock_inventory_line" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/stock_inventory_lines/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Stock_inventory_lineDTO> stock_inventory_linedtos) {
        stock_inventory_lineService.updateBatch(stock_inventory_lineMapping.toDomain(stock_inventory_linedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }







    @PreAuthorize("hasPermission('Remove',{#stock_inventory_line_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Stock_inventory_line" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/stock_inventory_lines/{stock_inventory_line_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("stock_inventory_line_id") Integer stock_inventory_line_id) {
         return ResponseEntity.status(HttpStatus.OK).body(stock_inventory_lineService.remove(stock_inventory_line_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Stock_inventory_line" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/stock_inventory_lines/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        stock_inventory_lineService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }







    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Stock_inventory_line" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/stock_inventory_lines/fetchdefault")
	public ResponseEntity<List<Stock_inventory_lineDTO>> fetchDefault(Stock_inventory_lineSearchContext context) {
        Page<Stock_inventory_line> domains = stock_inventory_lineService.searchDefault(context) ;
        List<Stock_inventory_lineDTO> list = stock_inventory_lineMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Stock_inventory_line" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/stock_inventory_lines/searchdefault")
	public ResponseEntity<Page<Stock_inventory_lineDTO>> searchDefault(Stock_inventory_lineSearchContext context) {
        Page<Stock_inventory_line> domains = stock_inventory_lineService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(stock_inventory_lineMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Stock_inventory_line getEntity(){
        return new Stock_inventory_line();
    }

}
