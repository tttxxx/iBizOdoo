package cn.ibizlab.odoo.odoo_stock.mapping;

import org.mapstruct.*;
import cn.ibizlab.odoo.core.odoo_stock.domain.Stock_picking_type;
import cn.ibizlab.odoo.odoo_stock.dto.Stock_picking_typeDTO;
import cn.ibizlab.odoo.util.domain.MappingBase;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring", uses = {},
    nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
    nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface Stock_picking_typeMapping extends MappingBase<Stock_picking_typeDTO, Stock_picking_type> {


}

