package cn.ibizlab.odoo.odoo_stock.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_stock.dto.*;
import cn.ibizlab.odoo.odoo_stock.mapping.*;
import cn.ibizlab.odoo.core.odoo_stock.domain.Stock_rules_report;
import cn.ibizlab.odoo.core.odoo_stock.service.IStock_rules_reportService;
import cn.ibizlab.odoo.core.odoo_stock.filter.Stock_rules_reportSearchContext;




@Slf4j
@Api(tags = {"Stock_rules_report" })
@RestController("odoo_stock-stock_rules_report")
@RequestMapping("")
public class Stock_rules_reportResource {

    @Autowired
    private IStock_rules_reportService stock_rules_reportService;

    @Autowired
    @Lazy
    private Stock_rules_reportMapping stock_rules_reportMapping;







    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Stock_rules_report" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/stock_rules_reports")

    public ResponseEntity<Stock_rules_reportDTO> create(@RequestBody Stock_rules_reportDTO stock_rules_reportdto) {
        Stock_rules_report domain = stock_rules_reportMapping.toDomain(stock_rules_reportdto);
		stock_rules_reportService.create(domain);
        Stock_rules_reportDTO dto = stock_rules_reportMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Stock_rules_report" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/stock_rules_reports/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Stock_rules_reportDTO> stock_rules_reportdtos) {
        stock_rules_reportService.createBatch(stock_rules_reportMapping.toDomain(stock_rules_reportdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }







    @PreAuthorize("hasPermission(#stock_rules_report_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Stock_rules_report" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/stock_rules_reports/{stock_rules_report_id}")
    public ResponseEntity<Stock_rules_reportDTO> get(@PathVariable("stock_rules_report_id") Integer stock_rules_report_id) {
        Stock_rules_report domain = stock_rules_reportService.get(stock_rules_report_id);
        Stock_rules_reportDTO dto = stock_rules_reportMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }




    @PreAuthorize("hasPermission('Remove',{#stock_rules_report_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Stock_rules_report" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/stock_rules_reports/{stock_rules_report_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("stock_rules_report_id") Integer stock_rules_report_id) {
         return ResponseEntity.status(HttpStatus.OK).body(stock_rules_reportService.remove(stock_rules_report_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Stock_rules_report" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/stock_rules_reports/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        stock_rules_reportService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission(#stock_rules_report_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Stock_rules_report" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/stock_rules_reports/{stock_rules_report_id}")

    public ResponseEntity<Stock_rules_reportDTO> update(@PathVariable("stock_rules_report_id") Integer stock_rules_report_id, @RequestBody Stock_rules_reportDTO stock_rules_reportdto) {
		Stock_rules_report domain = stock_rules_reportMapping.toDomain(stock_rules_reportdto);
        domain.setId(stock_rules_report_id);
		stock_rules_reportService.update(domain);
		Stock_rules_reportDTO dto = stock_rules_reportMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#stock_rules_report_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Stock_rules_report" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/stock_rules_reports/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Stock_rules_reportDTO> stock_rules_reportdtos) {
        stock_rules_reportService.updateBatch(stock_rules_reportMapping.toDomain(stock_rules_reportdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Stock_rules_report" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/stock_rules_reports/fetchdefault")
	public ResponseEntity<List<Stock_rules_reportDTO>> fetchDefault(Stock_rules_reportSearchContext context) {
        Page<Stock_rules_report> domains = stock_rules_reportService.searchDefault(context) ;
        List<Stock_rules_reportDTO> list = stock_rules_reportMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Stock_rules_report" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/stock_rules_reports/searchdefault")
	public ResponseEntity<Page<Stock_rules_reportDTO>> searchDefault(Stock_rules_reportSearchContext context) {
        Page<Stock_rules_report> domains = stock_rules_reportService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(stock_rules_reportMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Stock_rules_report getEntity(){
        return new Stock_rules_report();
    }

}
