package cn.ibizlab.odoo.odoo_stock.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;
import cn.ibizlab.odoo.util.domain.DTOBase;
import lombok.Data;

/**
 * 服务DTO对象[Stock_pickingDTO]
 */
@Data
public class Stock_pickingDTO extends DTOBase implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [PACKAGE_LEVEL_IDS]
     *
     */
    @JSONField(name = "package_level_ids")
    @JsonProperty("package_level_ids")
    private String packageLevelIds;

    /**
     * 属性 [NOTE]
     *
     */
    @JSONField(name = "note")
    @JsonProperty("note")
    private String note;

    /**
     * 属性 [WEBSITE_ID]
     *
     */
    @JSONField(name = "website_id")
    @JsonProperty("website_id")
    private Integer websiteId;

    /**
     * 属性 [ACTIVITY_TYPE_ID]
     *
     */
    @JSONField(name = "activity_type_id")
    @JsonProperty("activity_type_id")
    private Integer activityTypeId;

    /**
     * 属性 [PRINTED]
     *
     */
    @JSONField(name = "printed")
    @JsonProperty("printed")
    private String printed;

    /**
     * 属性 [MESSAGE_NEEDACTION_COUNTER]
     *
     */
    @JSONField(name = "message_needaction_counter")
    @JsonProperty("message_needaction_counter")
    private Integer messageNeedactionCounter;

    /**
     * 属性 [PRODUCT_ID]
     *
     */
    @JSONField(name = "product_id")
    @JsonProperty("product_id")
    private Integer productId;

    /**
     * 属性 [MESSAGE_PARTNER_IDS]
     *
     */
    @JSONField(name = "message_partner_ids")
    @JsonProperty("message_partner_ids")
    private String messagePartnerIds;

    /**
     * 属性 [MESSAGE_UNREAD_COUNTER]
     *
     */
    @JSONField(name = "message_unread_counter")
    @JsonProperty("message_unread_counter")
    private Integer messageUnreadCounter;

    /**
     * 属性 [MOVE_TYPE]
     *
     */
    @JSONField(name = "move_type")
    @JsonProperty("move_type")
    private String moveType;

    /**
     * 属性 [BACKORDER_IDS]
     *
     */
    @JSONField(name = "backorder_ids")
    @JsonProperty("backorder_ids")
    private String backorderIds;

    /**
     * 属性 [MESSAGE_NEEDACTION]
     *
     */
    @JSONField(name = "message_needaction")
    @JsonProperty("message_needaction")
    private String messageNeedaction;

    /**
     * 属性 [MESSAGE_IDS]
     *
     */
    @JSONField(name = "message_ids")
    @JsonProperty("message_ids")
    private String messageIds;

    /**
     * 属性 [HAS_PACKAGES]
     *
     */
    @JSONField(name = "has_packages")
    @JsonProperty("has_packages")
    private String hasPackages;

    /**
     * 属性 [ORIGIN]
     *
     */
    @JSONField(name = "origin")
    @JsonProperty("origin")
    private String origin;

    /**
     * 属性 [IS_LOCKED]
     *
     */
    @JSONField(name = "is_locked")
    @JsonProperty("is_locked")
    private String isLocked;

    /**
     * 属性 [SHOW_OPERATIONS]
     *
     */
    @JSONField(name = "show_operations")
    @JsonProperty("show_operations")
    private String showOperations;

    /**
     * 属性 [GROUP_ID]
     *
     */
    @JSONField(name = "group_id")
    @JsonProperty("group_id")
    private Integer groupId;

    /**
     * 属性 [MESSAGE_HAS_ERROR_COUNTER]
     *
     */
    @JSONField(name = "message_has_error_counter")
    @JsonProperty("message_has_error_counter")
    private Integer messageHasErrorCounter;

    /**
     * 属性 [MOVE_LINE_IDS_WITHOUT_PACKAGE]
     *
     */
    @JSONField(name = "move_line_ids_without_package")
    @JsonProperty("move_line_ids_without_package")
    private String moveLineIdsWithoutPackage;

    /**
     * 属性 [ACTIVITY_DATE_DEADLINE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "activity_date_deadline" , format="yyyy-MM-dd")
    @JsonProperty("activity_date_deadline")
    private Timestamp activityDateDeadline;

    /**
     * 属性 [ACTIVITY_USER_ID]
     *
     */
    @JSONField(name = "activity_user_id")
    @JsonProperty("activity_user_id")
    private Integer activityUserId;

    /**
     * 属性 [ACTIVITY_STATE]
     *
     */
    @JSONField(name = "activity_state")
    @JsonProperty("activity_state")
    private String activityState;

    /**
     * 属性 [ACTIVITY_IDS]
     *
     */
    @JSONField(name = "activity_ids")
    @JsonProperty("activity_ids")
    private String activityIds;

    /**
     * 属性 [ID]
     *
     */
    @JSONField(name = "id")
    @JsonProperty("id")
    private Integer id;

    /**
     * 属性 [ACTIVITY_SUMMARY]
     *
     */
    @JSONField(name = "activity_summary")
    @JsonProperty("activity_summary")
    private String activitySummary;

    /**
     * 属性 [MESSAGE_HAS_ERROR]
     *
     */
    @JSONField(name = "message_has_error")
    @JsonProperty("message_has_error")
    private String messageHasError;

    /**
     * 属性 [MOVE_LINE_IDS]
     *
     */
    @JSONField(name = "move_line_ids")
    @JsonProperty("move_line_ids")
    private String moveLineIds;

    /**
     * 属性 [PACKAGE_LEVEL_IDS_DETAILS]
     *
     */
    @JSONField(name = "package_level_ids_details")
    @JsonProperty("package_level_ids_details")
    private String packageLevelIdsDetails;

    /**
     * 属性 [MESSAGE_UNREAD]
     *
     */
    @JSONField(name = "message_unread")
    @JsonProperty("message_unread")
    private String messageUnread;

    /**
     * 属性 [DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("date")
    private Timestamp date;

    /**
     * 属性 [DATE_DONE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "date_done" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("date_done")
    private Timestamp dateDone;

    /**
     * 属性 [HAS_TRACKING]
     *
     */
    @JSONField(name = "has_tracking")
    @JsonProperty("has_tracking")
    private String hasTracking;

    /**
     * 属性 [MESSAGE_MAIN_ATTACHMENT_ID]
     *
     */
    @JSONField(name = "message_main_attachment_id")
    @JsonProperty("message_main_attachment_id")
    private Integer messageMainAttachmentId;

    /**
     * 属性 [STATE]
     *
     */
    @JSONField(name = "state")
    @JsonProperty("state")
    private String state;

    /**
     * 属性 [IMMEDIATE_TRANSFER]
     *
     */
    @JSONField(name = "immediate_transfer")
    @JsonProperty("immediate_transfer")
    private String immediateTransfer;

    /**
     * 属性 [HAS_SCRAP_MOVE]
     *
     */
    @JSONField(name = "has_scrap_move")
    @JsonProperty("has_scrap_move")
    private String hasScrapMove;

    /**
     * 属性 [MOVE_IDS_WITHOUT_PACKAGE]
     *
     */
    @JSONField(name = "move_ids_without_package")
    @JsonProperty("move_ids_without_package")
    private String moveIdsWithoutPackage;

    /**
     * 属性 [CREATE_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;

    /**
     * 属性 [SHOW_VALIDATE]
     *
     */
    @JSONField(name = "show_validate")
    @JsonProperty("show_validate")
    private String showValidate;

    /**
     * 属性 [NAME]
     *
     */
    @JSONField(name = "name")
    @JsonProperty("name")
    private String name;

    /**
     * 属性 [MESSAGE_CHANNEL_IDS]
     *
     */
    @JSONField(name = "message_channel_ids")
    @JsonProperty("message_channel_ids")
    private String messageChannelIds;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    private String displayName;

    /**
     * 属性 [PRIORITY]
     *
     */
    @JSONField(name = "priority")
    @JsonProperty("priority")
    private String priority;

    /**
     * 属性 [MOVE_LINES]
     *
     */
    @JSONField(name = "move_lines")
    @JsonProperty("move_lines")
    private String moveLines;

    /**
     * 属性 [PURCHASE_ID]
     *
     */
    @JSONField(name = "purchase_id")
    @JsonProperty("purchase_id")
    private Integer purchaseId;

    /**
     * 属性 [WRITE_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;

    /**
     * 属性 [WEBSITE_MESSAGE_IDS]
     *
     */
    @JSONField(name = "website_message_ids")
    @JsonProperty("website_message_ids")
    private String websiteMessageIds;

    /**
     * 属性 [SHOW_MARK_AS_TODO]
     *
     */
    @JSONField(name = "show_mark_as_todo")
    @JsonProperty("show_mark_as_todo")
    private String showMarkAsTodo;

    /**
     * 属性 [MESSAGE_FOLLOWER_IDS]
     *
     */
    @JSONField(name = "message_follower_ids")
    @JsonProperty("message_follower_ids")
    private String messageFollowerIds;

    /**
     * 属性 [MOVE_LINE_EXIST]
     *
     */
    @JSONField(name = "move_line_exist")
    @JsonProperty("move_line_exist")
    private String moveLineExist;

    /**
     * 属性 [MESSAGE_ATTACHMENT_COUNT]
     *
     */
    @JSONField(name = "message_attachment_count")
    @JsonProperty("message_attachment_count")
    private Integer messageAttachmentCount;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;

    /**
     * 属性 [SCHEDULED_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "scheduled_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("scheduled_date")
    private Timestamp scheduledDate;

    /**
     * 属性 [MESSAGE_IS_FOLLOWER]
     *
     */
    @JSONField(name = "message_is_follower")
    @JsonProperty("message_is_follower")
    private String messageIsFollower;

    /**
     * 属性 [SHOW_CHECK_AVAILABILITY]
     *
     */
    @JSONField(name = "show_check_availability")
    @JsonProperty("show_check_availability")
    private String showCheckAvailability;

    /**
     * 属性 [SHOW_LOTS_TEXT]
     *
     */
    @JSONField(name = "show_lots_text")
    @JsonProperty("show_lots_text")
    private String showLotsText;

    /**
     * 属性 [WRITE_UID_TEXT]
     *
     */
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    private String writeUidText;

    /**
     * 属性 [BACKORDER_ID_TEXT]
     *
     */
    @JSONField(name = "backorder_id_text")
    @JsonProperty("backorder_id_text")
    private String backorderIdText;

    /**
     * 属性 [PICKING_TYPE_ID_TEXT]
     *
     */
    @JSONField(name = "picking_type_id_text")
    @JsonProperty("picking_type_id_text")
    private String pickingTypeIdText;

    /**
     * 属性 [PARTNER_ID_TEXT]
     *
     */
    @JSONField(name = "partner_id_text")
    @JsonProperty("partner_id_text")
    private String partnerIdText;

    /**
     * 属性 [CREATE_UID_TEXT]
     *
     */
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    private String createUidText;

    /**
     * 属性 [LOCATION_ID_TEXT]
     *
     */
    @JSONField(name = "location_id_text")
    @JsonProperty("location_id_text")
    private String locationIdText;

    /**
     * 属性 [LOCATION_DEST_ID_TEXT]
     *
     */
    @JSONField(name = "location_dest_id_text")
    @JsonProperty("location_dest_id_text")
    private String locationDestIdText;

    /**
     * 属性 [PICKING_TYPE_CODE]
     *
     */
    @JSONField(name = "picking_type_code")
    @JsonProperty("picking_type_code")
    private String pickingTypeCode;

    /**
     * 属性 [PICKING_TYPE_ENTIRE_PACKS]
     *
     */
    @JSONField(name = "picking_type_entire_packs")
    @JsonProperty("picking_type_entire_packs")
    private String pickingTypeEntirePacks;

    /**
     * 属性 [OWNER_ID_TEXT]
     *
     */
    @JSONField(name = "owner_id_text")
    @JsonProperty("owner_id_text")
    private String ownerIdText;

    /**
     * 属性 [SALE_ID_TEXT]
     *
     */
    @JSONField(name = "sale_id_text")
    @JsonProperty("sale_id_text")
    private String saleIdText;

    /**
     * 属性 [COMPANY_ID_TEXT]
     *
     */
    @JSONField(name = "company_id_text")
    @JsonProperty("company_id_text")
    private String companyIdText;

    /**
     * 属性 [PICKING_TYPE_ID]
     *
     */
    @JSONField(name = "picking_type_id")
    @JsonProperty("picking_type_id")
    private Integer pickingTypeId;

    /**
     * 属性 [SALE_ID]
     *
     */
    @JSONField(name = "sale_id")
    @JsonProperty("sale_id")
    private Integer saleId;

    /**
     * 属性 [LOCATION_DEST_ID]
     *
     */
    @JSONField(name = "location_dest_id")
    @JsonProperty("location_dest_id")
    private Integer locationDestId;

    /**
     * 属性 [CREATE_UID]
     *
     */
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    private Integer createUid;

    /**
     * 属性 [PARTNER_ID]
     *
     */
    @JSONField(name = "partner_id")
    @JsonProperty("partner_id")
    private Integer partnerId;

    /**
     * 属性 [OWNER_ID]
     *
     */
    @JSONField(name = "owner_id")
    @JsonProperty("owner_id")
    private Integer ownerId;

    /**
     * 属性 [WRITE_UID]
     *
     */
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    private Integer writeUid;

    /**
     * 属性 [COMPANY_ID]
     *
     */
    @JSONField(name = "company_id")
    @JsonProperty("company_id")
    private Integer companyId;

    /**
     * 属性 [LOCATION_ID]
     *
     */
    @JSONField(name = "location_id")
    @JsonProperty("location_id")
    private Integer locationId;

    /**
     * 属性 [BACKORDER_ID]
     *
     */
    @JSONField(name = "backorder_id")
    @JsonProperty("backorder_id")
    private Integer backorderId;


    /**
     * 设置 [NOTE]
     */
    public void setNote(String  note){
        this.note = note ;
        this.modify("note",note);
    }

    /**
     * 设置 [WEBSITE_ID]
     */
    public void setWebsiteId(Integer  websiteId){
        this.websiteId = websiteId ;
        this.modify("website_id",websiteId);
    }

    /**
     * 设置 [PRINTED]
     */
    public void setPrinted(String  printed){
        this.printed = printed ;
        this.modify("printed",printed);
    }

    /**
     * 设置 [MOVE_TYPE]
     */
    public void setMoveType(String  moveType){
        this.moveType = moveType ;
        this.modify("move_type",moveType);
    }

    /**
     * 设置 [ORIGIN]
     */
    public void setOrigin(String  origin){
        this.origin = origin ;
        this.modify("origin",origin);
    }

    /**
     * 设置 [IS_LOCKED]
     */
    public void setIsLocked(String  isLocked){
        this.isLocked = isLocked ;
        this.modify("is_locked",isLocked);
    }

    /**
     * 设置 [GROUP_ID]
     */
    public void setGroupId(Integer  groupId){
        this.groupId = groupId ;
        this.modify("group_id",groupId);
    }

    /**
     * 设置 [DATE]
     */
    public void setDate(Timestamp  date){
        this.date = date ;
        this.modify("date",date);
    }

    /**
     * 设置 [DATE_DONE]
     */
    public void setDateDone(Timestamp  dateDone){
        this.dateDone = dateDone ;
        this.modify("date_done",dateDone);
    }

    /**
     * 设置 [MESSAGE_MAIN_ATTACHMENT_ID]
     */
    public void setMessageMainAttachmentId(Integer  messageMainAttachmentId){
        this.messageMainAttachmentId = messageMainAttachmentId ;
        this.modify("message_main_attachment_id",messageMainAttachmentId);
    }

    /**
     * 设置 [STATE]
     */
    public void setState(String  state){
        this.state = state ;
        this.modify("state",state);
    }

    /**
     * 设置 [IMMEDIATE_TRANSFER]
     */
    public void setImmediateTransfer(String  immediateTransfer){
        this.immediateTransfer = immediateTransfer ;
        this.modify("immediate_transfer",immediateTransfer);
    }

    /**
     * 设置 [NAME]
     */
    public void setName(String  name){
        this.name = name ;
        this.modify("name",name);
    }

    /**
     * 设置 [PRIORITY]
     */
    public void setPriority(String  priority){
        this.priority = priority ;
        this.modify("priority",priority);
    }

    /**
     * 设置 [SCHEDULED_DATE]
     */
    public void setScheduledDate(Timestamp  scheduledDate){
        this.scheduledDate = scheduledDate ;
        this.modify("scheduled_date",scheduledDate);
    }

    /**
     * 设置 [PICKING_TYPE_ID]
     */
    public void setPickingTypeId(Integer  pickingTypeId){
        this.pickingTypeId = pickingTypeId ;
        this.modify("picking_type_id",pickingTypeId);
    }

    /**
     * 设置 [SALE_ID]
     */
    public void setSaleId(Integer  saleId){
        this.saleId = saleId ;
        this.modify("sale_id",saleId);
    }

    /**
     * 设置 [LOCATION_DEST_ID]
     */
    public void setLocationDestId(Integer  locationDestId){
        this.locationDestId = locationDestId ;
        this.modify("location_dest_id",locationDestId);
    }

    /**
     * 设置 [PARTNER_ID]
     */
    public void setPartnerId(Integer  partnerId){
        this.partnerId = partnerId ;
        this.modify("partner_id",partnerId);
    }

    /**
     * 设置 [OWNER_ID]
     */
    public void setOwnerId(Integer  ownerId){
        this.ownerId = ownerId ;
        this.modify("owner_id",ownerId);
    }

    /**
     * 设置 [COMPANY_ID]
     */
    public void setCompanyId(Integer  companyId){
        this.companyId = companyId ;
        this.modify("company_id",companyId);
    }

    /**
     * 设置 [LOCATION_ID]
     */
    public void setLocationId(Integer  locationId){
        this.locationId = locationId ;
        this.modify("location_id",locationId);
    }

    /**
     * 设置 [BACKORDER_ID]
     */
    public void setBackorderId(Integer  backorderId){
        this.backorderId = backorderId ;
        this.modify("backorder_id",backorderId);
    }


}

