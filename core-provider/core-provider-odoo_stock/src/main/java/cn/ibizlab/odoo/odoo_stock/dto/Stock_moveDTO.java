package cn.ibizlab.odoo.odoo_stock.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;
import cn.ibizlab.odoo.util.domain.DTOBase;
import lombok.Data;

/**
 * 服务DTO对象[Stock_moveDTO]
 */
@Data
public class Stock_moveDTO extends DTOBase implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [RETURNED_MOVE_IDS]
     *
     */
    @JSONField(name = "returned_move_ids")
    @JsonProperty("returned_move_ids")
    private String returnedMoveIds;

    /**
     * 属性 [ADDITIONAL]
     *
     */
    @JSONField(name = "additional")
    @JsonProperty("additional")
    private String additional;

    /**
     * 属性 [PRICE_UNIT]
     *
     */
    @JSONField(name = "price_unit")
    @JsonProperty("price_unit")
    private Double priceUnit;

    /**
     * 属性 [GROUP_ID]
     *
     */
    @JSONField(name = "group_id")
    @JsonProperty("group_id")
    private Integer groupId;

    /**
     * 属性 [SCRAP_IDS]
     *
     */
    @JSONField(name = "scrap_ids")
    @JsonProperty("scrap_ids")
    private String scrapIds;

    /**
     * 属性 [ACCOUNT_MOVE_IDS]
     *
     */
    @JSONField(name = "account_move_ids")
    @JsonProperty("account_move_ids")
    private String accountMoveIds;

    /**
     * 属性 [MOVE_DEST_IDS]
     *
     */
    @JSONField(name = "move_dest_ids")
    @JsonProperty("move_dest_ids")
    private String moveDestIds;

    /**
     * 属性 [ID]
     *
     */
    @JSONField(name = "id")
    @JsonProperty("id")
    private Integer id;

    /**
     * 属性 [TO_REFUND]
     *
     */
    @JSONField(name = "to_refund")
    @JsonProperty("to_refund")
    private String toRefund;

    /**
     * 属性 [UNIT_FACTOR]
     *
     */
    @JSONField(name = "unit_factor")
    @JsonProperty("unit_factor")
    private Double unitFactor;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;

    /**
     * 属性 [AVAILABILITY]
     *
     */
    @JSONField(name = "availability")
    @JsonProperty("availability")
    private Double availability;

    /**
     * 属性 [REMAINING_QTY]
     *
     */
    @JSONField(name = "remaining_qty")
    @JsonProperty("remaining_qty")
    private Double remainingQty;

    /**
     * 属性 [PROPAGATE]
     *
     */
    @JSONField(name = "propagate")
    @JsonProperty("propagate")
    private String propagate;

    /**
     * 属性 [REFERENCE]
     *
     */
    @JSONField(name = "reference")
    @JsonProperty("reference")
    private String reference;

    /**
     * 属性 [ACTIVE_MOVE_LINE_IDS]
     *
     */
    @JSONField(name = "active_move_line_ids")
    @JsonProperty("active_move_line_ids")
    private String activeMoveLineIds;

    /**
     * 属性 [MOVE_LINE_NOSUGGEST_IDS]
     *
     */
    @JSONField(name = "move_line_nosuggest_ids")
    @JsonProperty("move_line_nosuggest_ids")
    private String moveLineNosuggestIds;

    /**
     * 属性 [SHOW_DETAILS_VISIBLE]
     *
     */
    @JSONField(name = "show_details_visible")
    @JsonProperty("show_details_visible")
    private String showDetailsVisible;

    /**
     * 属性 [PRODUCT_QTY]
     *
     */
    @JSONField(name = "product_qty")
    @JsonProperty("product_qty")
    private Double productQty;

    /**
     * 属性 [WRITE_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;

    /**
     * 属性 [ORIGIN]
     *
     */
    @JSONField(name = "origin")
    @JsonProperty("origin")
    private String origin;

    /**
     * 属性 [PRODUCT_UOM_QTY]
     *
     */
    @JSONField(name = "product_uom_qty")
    @JsonProperty("product_uom_qty")
    private Double productUomQty;

    /**
     * 属性 [NEEDS_LOTS]
     *
     */
    @JSONField(name = "needs_lots")
    @JsonProperty("needs_lots")
    private String needsLots;

    /**
     * 属性 [PRIORITY]
     *
     */
    @JSONField(name = "priority")
    @JsonProperty("priority")
    private String priority;

    /**
     * 属性 [DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("date")
    private Timestamp date;

    /**
     * 属性 [REMAINING_VALUE]
     *
     */
    @JSONField(name = "remaining_value")
    @JsonProperty("remaining_value")
    private Double remainingValue;

    /**
     * 属性 [ORDER_FINISHED_LOT_IDS]
     *
     */
    @JSONField(name = "order_finished_lot_ids")
    @JsonProperty("order_finished_lot_ids")
    private String orderFinishedLotIds;

    /**
     * 属性 [SEQUENCE]
     *
     */
    @JSONField(name = "sequence")
    @JsonProperty("sequence")
    private Integer sequence;

    /**
     * 属性 [CREATE_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;

    /**
     * 属性 [MOVE_ORIG_IDS]
     *
     */
    @JSONField(name = "move_orig_ids")
    @JsonProperty("move_orig_ids")
    private String moveOrigIds;

    /**
     * 属性 [MOVE_LINE_IDS]
     *
     */
    @JSONField(name = "move_line_ids")
    @JsonProperty("move_line_ids")
    private String moveLineIds;

    /**
     * 属性 [NOTE]
     *
     */
    @JSONField(name = "note")
    @JsonProperty("note")
    private String note;

    /**
     * 属性 [RESERVED_AVAILABILITY]
     *
     */
    @JSONField(name = "reserved_availability")
    @JsonProperty("reserved_availability")
    private Double reservedAvailability;

    /**
     * 属性 [IS_INITIAL_DEMAND_EDITABLE]
     *
     */
    @JSONField(name = "is_initial_demand_editable")
    @JsonProperty("is_initial_demand_editable")
    private String isInitialDemandEditable;

    /**
     * 属性 [QUANTITY_DONE]
     *
     */
    @JSONField(name = "quantity_done")
    @JsonProperty("quantity_done")
    private Double quantityDone;

    /**
     * 属性 [IS_LOCKED]
     *
     */
    @JSONField(name = "is_locked")
    @JsonProperty("is_locked")
    private String isLocked;

    /**
     * 属性 [STRING_AVAILABILITY_INFO]
     *
     */
    @JSONField(name = "string_availability_info")
    @JsonProperty("string_availability_info")
    private String stringAvailabilityInfo;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    private String displayName;

    /**
     * 属性 [SHOW_RESERVED_AVAILABILITY]
     *
     */
    @JSONField(name = "show_reserved_availability")
    @JsonProperty("show_reserved_availability")
    private String showReservedAvailability;

    /**
     * 属性 [ROUTE_IDS]
     *
     */
    @JSONField(name = "route_ids")
    @JsonProperty("route_ids")
    private String routeIds;

    /**
     * 属性 [STATE]
     *
     */
    @JSONField(name = "state")
    @JsonProperty("state")
    private String state;

    /**
     * 属性 [NAME]
     *
     */
    @JSONField(name = "name")
    @JsonProperty("name")
    private String name;

    /**
     * 属性 [VALUE]
     *
     */
    @JSONField(name = "value")
    @JsonProperty("value")
    private Double value;

    /**
     * 属性 [FINISHED_LOTS_EXIST]
     *
     */
    @JSONField(name = "finished_lots_exist")
    @JsonProperty("finished_lots_exist")
    private String finishedLotsExist;

    /**
     * 属性 [IS_DONE]
     *
     */
    @JSONField(name = "is_done")
    @JsonProperty("is_done")
    private String isDone;

    /**
     * 属性 [IS_QUANTITY_DONE_EDITABLE]
     *
     */
    @JSONField(name = "is_quantity_done_editable")
    @JsonProperty("is_quantity_done_editable")
    private String isQuantityDoneEditable;

    /**
     * 属性 [HAS_MOVE_LINES]
     *
     */
    @JSONField(name = "has_move_lines")
    @JsonProperty("has_move_lines")
    private String hasMoveLines;

    /**
     * 属性 [DATE_EXPECTED]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "date_expected" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("date_expected")
    private Timestamp dateExpected;

    /**
     * 属性 [PROCURE_METHOD]
     *
     */
    @JSONField(name = "procure_method")
    @JsonProperty("procure_method")
    private String procureMethod;

    /**
     * 属性 [PRODUCT_TYPE]
     *
     */
    @JSONField(name = "product_type")
    @JsonProperty("product_type")
    private String productType;

    /**
     * 属性 [UNBUILD_ID_TEXT]
     *
     */
    @JSONField(name = "unbuild_id_text")
    @JsonProperty("unbuild_id_text")
    private String unbuildIdText;

    /**
     * 属性 [SCRAPPED]
     *
     */
    @JSONField(name = "scrapped")
    @JsonProperty("scrapped")
    private String scrapped;

    /**
     * 属性 [COMPANY_ID_TEXT]
     *
     */
    @JSONField(name = "company_id_text")
    @JsonProperty("company_id_text")
    private String companyIdText;

    /**
     * 属性 [PRODUCTION_ID_TEXT]
     *
     */
    @JSONField(name = "production_id_text")
    @JsonProperty("production_id_text")
    private String productionIdText;

    /**
     * 属性 [WAREHOUSE_ID_TEXT]
     *
     */
    @JSONField(name = "warehouse_id_text")
    @JsonProperty("warehouse_id_text")
    private String warehouseIdText;

    /**
     * 属性 [SHOW_OPERATIONS]
     *
     */
    @JSONField(name = "show_operations")
    @JsonProperty("show_operations")
    private String showOperations;

    /**
     * 属性 [CREATED_PRODUCTION_ID_TEXT]
     *
     */
    @JSONField(name = "created_production_id_text")
    @JsonProperty("created_production_id_text")
    private String createdProductionIdText;

    /**
     * 属性 [CREATED_PURCHASE_LINE_ID_TEXT]
     *
     */
    @JSONField(name = "created_purchase_line_id_text")
    @JsonProperty("created_purchase_line_id_text")
    private String createdPurchaseLineIdText;

    /**
     * 属性 [LOCATION_DEST_ID_TEXT]
     *
     */
    @JSONField(name = "location_dest_id_text")
    @JsonProperty("location_dest_id_text")
    private String locationDestIdText;

    /**
     * 属性 [HAS_TRACKING]
     *
     */
    @JSONField(name = "has_tracking")
    @JsonProperty("has_tracking")
    private String hasTracking;

    /**
     * 属性 [PICKING_TYPE_ID_TEXT]
     *
     */
    @JSONField(name = "picking_type_id_text")
    @JsonProperty("picking_type_id_text")
    private String pickingTypeIdText;

    /**
     * 属性 [SALE_LINE_ID_TEXT]
     *
     */
    @JSONField(name = "sale_line_id_text")
    @JsonProperty("sale_line_id_text")
    private String saleLineIdText;

    /**
     * 属性 [PARTNER_ID_TEXT]
     *
     */
    @JSONField(name = "partner_id_text")
    @JsonProperty("partner_id_text")
    private String partnerIdText;

    /**
     * 属性 [PICKING_PARTNER_ID]
     *
     */
    @JSONField(name = "picking_partner_id")
    @JsonProperty("picking_partner_id")
    private Integer pickingPartnerId;

    /**
     * 属性 [RAW_MATERIAL_PRODUCTION_ID_TEXT]
     *
     */
    @JSONField(name = "raw_material_production_id_text")
    @JsonProperty("raw_material_production_id_text")
    private String rawMaterialProductionIdText;

    /**
     * 属性 [ORIGIN_RETURNED_MOVE_ID_TEXT]
     *
     */
    @JSONField(name = "origin_returned_move_id_text")
    @JsonProperty("origin_returned_move_id_text")
    private String originReturnedMoveIdText;

    /**
     * 属性 [REPAIR_ID_TEXT]
     *
     */
    @JSONField(name = "repair_id_text")
    @JsonProperty("repair_id_text")
    private String repairIdText;

    /**
     * 属性 [BACKORDER_ID]
     *
     */
    @JSONField(name = "backorder_id")
    @JsonProperty("backorder_id")
    private Integer backorderId;

    /**
     * 属性 [PURCHASE_LINE_ID_TEXT]
     *
     */
    @JSONField(name = "purchase_line_id_text")
    @JsonProperty("purchase_line_id_text")
    private String purchaseLineIdText;

    /**
     * 属性 [PICKING_ID_TEXT]
     *
     */
    @JSONField(name = "picking_id_text")
    @JsonProperty("picking_id_text")
    private String pickingIdText;

    /**
     * 属性 [PRODUCT_ID_TEXT]
     *
     */
    @JSONField(name = "product_id_text")
    @JsonProperty("product_id_text")
    private String productIdText;

    /**
     * 属性 [LOCATION_ID_TEXT]
     *
     */
    @JSONField(name = "location_id_text")
    @JsonProperty("location_id_text")
    private String locationIdText;

    /**
     * 属性 [CREATE_UID_TEXT]
     *
     */
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    private String createUidText;

    /**
     * 属性 [PRODUCT_UOM_TEXT]
     *
     */
    @JSONField(name = "product_uom_text")
    @JsonProperty("product_uom_text")
    private String productUomText;

    /**
     * 属性 [OPERATION_ID_TEXT]
     *
     */
    @JSONField(name = "operation_id_text")
    @JsonProperty("operation_id_text")
    private String operationIdText;

    /**
     * 属性 [WORKORDER_ID_TEXT]
     *
     */
    @JSONField(name = "workorder_id_text")
    @JsonProperty("workorder_id_text")
    private String workorderIdText;

    /**
     * 属性 [CONSUME_UNBUILD_ID_TEXT]
     *
     */
    @JSONField(name = "consume_unbuild_id_text")
    @JsonProperty("consume_unbuild_id_text")
    private String consumeUnbuildIdText;

    /**
     * 属性 [PRODUCT_PACKAGING_TEXT]
     *
     */
    @JSONField(name = "product_packaging_text")
    @JsonProperty("product_packaging_text")
    private String productPackagingText;

    /**
     * 属性 [RESTRICT_PARTNER_ID_TEXT]
     *
     */
    @JSONField(name = "restrict_partner_id_text")
    @JsonProperty("restrict_partner_id_text")
    private String restrictPartnerIdText;

    /**
     * 属性 [INVENTORY_ID_TEXT]
     *
     */
    @JSONField(name = "inventory_id_text")
    @JsonProperty("inventory_id_text")
    private String inventoryIdText;

    /**
     * 属性 [PRODUCT_TMPL_ID]
     *
     */
    @JSONField(name = "product_tmpl_id")
    @JsonProperty("product_tmpl_id")
    private Integer productTmplId;

    /**
     * 属性 [RULE_ID_TEXT]
     *
     */
    @JSONField(name = "rule_id_text")
    @JsonProperty("rule_id_text")
    private String ruleIdText;

    /**
     * 属性 [WRITE_UID_TEXT]
     *
     */
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    private String writeUidText;

    /**
     * 属性 [PICKING_CODE]
     *
     */
    @JSONField(name = "picking_code")
    @JsonProperty("picking_code")
    private String pickingCode;

    /**
     * 属性 [PICKING_TYPE_ENTIRE_PACKS]
     *
     */
    @JSONField(name = "picking_type_entire_packs")
    @JsonProperty("picking_type_entire_packs")
    private String pickingTypeEntirePacks;

    /**
     * 属性 [CREATED_PRODUCTION_ID]
     *
     */
    @JSONField(name = "created_production_id")
    @JsonProperty("created_production_id")
    private Integer createdProductionId;

    /**
     * 属性 [CONSUME_UNBUILD_ID]
     *
     */
    @JSONField(name = "consume_unbuild_id")
    @JsonProperty("consume_unbuild_id")
    private Integer consumeUnbuildId;

    /**
     * 属性 [SALE_LINE_ID]
     *
     */
    @JSONField(name = "sale_line_id")
    @JsonProperty("sale_line_id")
    private Integer saleLineId;

    /**
     * 属性 [ORIGIN_RETURNED_MOVE_ID]
     *
     */
    @JSONField(name = "origin_returned_move_id")
    @JsonProperty("origin_returned_move_id")
    private Integer originReturnedMoveId;

    /**
     * 属性 [PICKING_TYPE_ID]
     *
     */
    @JSONField(name = "picking_type_id")
    @JsonProperty("picking_type_id")
    private Integer pickingTypeId;

    /**
     * 属性 [OPERATION_ID]
     *
     */
    @JSONField(name = "operation_id")
    @JsonProperty("operation_id")
    private Integer operationId;

    /**
     * 属性 [BOM_LINE_ID]
     *
     */
    @JSONField(name = "bom_line_id")
    @JsonProperty("bom_line_id")
    private Integer bomLineId;

    /**
     * 属性 [REPAIR_ID]
     *
     */
    @JSONField(name = "repair_id")
    @JsonProperty("repair_id")
    private Integer repairId;

    /**
     * 属性 [PRODUCT_ID]
     *
     */
    @JSONField(name = "product_id")
    @JsonProperty("product_id")
    private Integer productId;

    /**
     * 属性 [UNBUILD_ID]
     *
     */
    @JSONField(name = "unbuild_id")
    @JsonProperty("unbuild_id")
    private Integer unbuildId;

    /**
     * 属性 [PRODUCTION_ID]
     *
     */
    @JSONField(name = "production_id")
    @JsonProperty("production_id")
    private Integer productionId;

    /**
     * 属性 [CREATED_PURCHASE_LINE_ID]
     *
     */
    @JSONField(name = "created_purchase_line_id")
    @JsonProperty("created_purchase_line_id")
    private Integer createdPurchaseLineId;

    /**
     * 属性 [WORKORDER_ID]
     *
     */
    @JSONField(name = "workorder_id")
    @JsonProperty("workorder_id")
    private Integer workorderId;

    /**
     * 属性 [PRODUCT_UOM]
     *
     */
    @JSONField(name = "product_uom")
    @JsonProperty("product_uom")
    private Integer productUom;

    /**
     * 属性 [PICKING_ID]
     *
     */
    @JSONField(name = "picking_id")
    @JsonProperty("picking_id")
    private Integer pickingId;

    /**
     * 属性 [PARTNER_ID]
     *
     */
    @JSONField(name = "partner_id")
    @JsonProperty("partner_id")
    private Integer partnerId;

    /**
     * 属性 [CREATE_UID]
     *
     */
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    private Integer createUid;

    /**
     * 属性 [PACKAGE_LEVEL_ID]
     *
     */
    @JSONField(name = "package_level_id")
    @JsonProperty("package_level_id")
    private Integer packageLevelId;

    /**
     * 属性 [PRODUCT_PACKAGING]
     *
     */
    @JSONField(name = "product_packaging")
    @JsonProperty("product_packaging")
    private Integer productPackaging;

    /**
     * 属性 [RESTRICT_PARTNER_ID]
     *
     */
    @JSONField(name = "restrict_partner_id")
    @JsonProperty("restrict_partner_id")
    private Integer restrictPartnerId;

    /**
     * 属性 [RULE_ID]
     *
     */
    @JSONField(name = "rule_id")
    @JsonProperty("rule_id")
    private Integer ruleId;

    /**
     * 属性 [WAREHOUSE_ID]
     *
     */
    @JSONField(name = "warehouse_id")
    @JsonProperty("warehouse_id")
    private Integer warehouseId;

    /**
     * 属性 [LOCATION_DEST_ID]
     *
     */
    @JSONField(name = "location_dest_id")
    @JsonProperty("location_dest_id")
    private Integer locationDestId;

    /**
     * 属性 [RAW_MATERIAL_PRODUCTION_ID]
     *
     */
    @JSONField(name = "raw_material_production_id")
    @JsonProperty("raw_material_production_id")
    private Integer rawMaterialProductionId;

    /**
     * 属性 [INVENTORY_ID]
     *
     */
    @JSONField(name = "inventory_id")
    @JsonProperty("inventory_id")
    private Integer inventoryId;

    /**
     * 属性 [PURCHASE_LINE_ID]
     *
     */
    @JSONField(name = "purchase_line_id")
    @JsonProperty("purchase_line_id")
    private Integer purchaseLineId;

    /**
     * 属性 [LOCATION_ID]
     *
     */
    @JSONField(name = "location_id")
    @JsonProperty("location_id")
    private Integer locationId;

    /**
     * 属性 [COMPANY_ID]
     *
     */
    @JSONField(name = "company_id")
    @JsonProperty("company_id")
    private Integer companyId;

    /**
     * 属性 [WRITE_UID]
     *
     */
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    private Integer writeUid;


    /**
     * 设置 [ADDITIONAL]
     */
    public void setAdditional(String  additional){
        this.additional = additional ;
        this.modify("additional",additional);
    }

    /**
     * 设置 [PRICE_UNIT]
     */
    public void setPriceUnit(Double  priceUnit){
        this.priceUnit = priceUnit ;
        this.modify("price_unit",priceUnit);
    }

    /**
     * 设置 [GROUP_ID]
     */
    public void setGroupId(Integer  groupId){
        this.groupId = groupId ;
        this.modify("group_id",groupId);
    }

    /**
     * 设置 [TO_REFUND]
     */
    public void setToRefund(String  toRefund){
        this.toRefund = toRefund ;
        this.modify("to_refund",toRefund);
    }

    /**
     * 设置 [UNIT_FACTOR]
     */
    public void setUnitFactor(Double  unitFactor){
        this.unitFactor = unitFactor ;
        this.modify("unit_factor",unitFactor);
    }

    /**
     * 设置 [REMAINING_QTY]
     */
    public void setRemainingQty(Double  remainingQty){
        this.remainingQty = remainingQty ;
        this.modify("remaining_qty",remainingQty);
    }

    /**
     * 设置 [PROPAGATE]
     */
    public void setPropagate(String  propagate){
        this.propagate = propagate ;
        this.modify("propagate",propagate);
    }

    /**
     * 设置 [REFERENCE]
     */
    public void setReference(String  reference){
        this.reference = reference ;
        this.modify("reference",reference);
    }

    /**
     * 设置 [PRODUCT_QTY]
     */
    public void setProductQty(Double  productQty){
        this.productQty = productQty ;
        this.modify("product_qty",productQty);
    }

    /**
     * 设置 [ORIGIN]
     */
    public void setOrigin(String  origin){
        this.origin = origin ;
        this.modify("origin",origin);
    }

    /**
     * 设置 [PRODUCT_UOM_QTY]
     */
    public void setProductUomQty(Double  productUomQty){
        this.productUomQty = productUomQty ;
        this.modify("product_uom_qty",productUomQty);
    }

    /**
     * 设置 [PRIORITY]
     */
    public void setPriority(String  priority){
        this.priority = priority ;
        this.modify("priority",priority);
    }

    /**
     * 设置 [DATE]
     */
    public void setDate(Timestamp  date){
        this.date = date ;
        this.modify("date",date);
    }

    /**
     * 设置 [REMAINING_VALUE]
     */
    public void setRemainingValue(Double  remainingValue){
        this.remainingValue = remainingValue ;
        this.modify("remaining_value",remainingValue);
    }

    /**
     * 设置 [SEQUENCE]
     */
    public void setSequence(Integer  sequence){
        this.sequence = sequence ;
        this.modify("sequence",sequence);
    }

    /**
     * 设置 [NOTE]
     */
    public void setNote(String  note){
        this.note = note ;
        this.modify("note",note);
    }

    /**
     * 设置 [STATE]
     */
    public void setState(String  state){
        this.state = state ;
        this.modify("state",state);
    }

    /**
     * 设置 [NAME]
     */
    public void setName(String  name){
        this.name = name ;
        this.modify("name",name);
    }

    /**
     * 设置 [VALUE]
     */
    public void setValue(Double  value){
        this.value = value ;
        this.modify("value",value);
    }

    /**
     * 设置 [IS_DONE]
     */
    public void setIsDone(String  isDone){
        this.isDone = isDone ;
        this.modify("is_done",isDone);
    }

    /**
     * 设置 [DATE_EXPECTED]
     */
    public void setDateExpected(Timestamp  dateExpected){
        this.dateExpected = dateExpected ;
        this.modify("date_expected",dateExpected);
    }

    /**
     * 设置 [PROCURE_METHOD]
     */
    public void setProcureMethod(String  procureMethod){
        this.procureMethod = procureMethod ;
        this.modify("procure_method",procureMethod);
    }

    /**
     * 设置 [CREATED_PRODUCTION_ID]
     */
    public void setCreatedProductionId(Integer  createdProductionId){
        this.createdProductionId = createdProductionId ;
        this.modify("created_production_id",createdProductionId);
    }

    /**
     * 设置 [CONSUME_UNBUILD_ID]
     */
    public void setConsumeUnbuildId(Integer  consumeUnbuildId){
        this.consumeUnbuildId = consumeUnbuildId ;
        this.modify("consume_unbuild_id",consumeUnbuildId);
    }

    /**
     * 设置 [SALE_LINE_ID]
     */
    public void setSaleLineId(Integer  saleLineId){
        this.saleLineId = saleLineId ;
        this.modify("sale_line_id",saleLineId);
    }

    /**
     * 设置 [ORIGIN_RETURNED_MOVE_ID]
     */
    public void setOriginReturnedMoveId(Integer  originReturnedMoveId){
        this.originReturnedMoveId = originReturnedMoveId ;
        this.modify("origin_returned_move_id",originReturnedMoveId);
    }

    /**
     * 设置 [PICKING_TYPE_ID]
     */
    public void setPickingTypeId(Integer  pickingTypeId){
        this.pickingTypeId = pickingTypeId ;
        this.modify("picking_type_id",pickingTypeId);
    }

    /**
     * 设置 [OPERATION_ID]
     */
    public void setOperationId(Integer  operationId){
        this.operationId = operationId ;
        this.modify("operation_id",operationId);
    }

    /**
     * 设置 [BOM_LINE_ID]
     */
    public void setBomLineId(Integer  bomLineId){
        this.bomLineId = bomLineId ;
        this.modify("bom_line_id",bomLineId);
    }

    /**
     * 设置 [REPAIR_ID]
     */
    public void setRepairId(Integer  repairId){
        this.repairId = repairId ;
        this.modify("repair_id",repairId);
    }

    /**
     * 设置 [PRODUCT_ID]
     */
    public void setProductId(Integer  productId){
        this.productId = productId ;
        this.modify("product_id",productId);
    }

    /**
     * 设置 [UNBUILD_ID]
     */
    public void setUnbuildId(Integer  unbuildId){
        this.unbuildId = unbuildId ;
        this.modify("unbuild_id",unbuildId);
    }

    /**
     * 设置 [PRODUCTION_ID]
     */
    public void setProductionId(Integer  productionId){
        this.productionId = productionId ;
        this.modify("production_id",productionId);
    }

    /**
     * 设置 [CREATED_PURCHASE_LINE_ID]
     */
    public void setCreatedPurchaseLineId(Integer  createdPurchaseLineId){
        this.createdPurchaseLineId = createdPurchaseLineId ;
        this.modify("created_purchase_line_id",createdPurchaseLineId);
    }

    /**
     * 设置 [WORKORDER_ID]
     */
    public void setWorkorderId(Integer  workorderId){
        this.workorderId = workorderId ;
        this.modify("workorder_id",workorderId);
    }

    /**
     * 设置 [PRODUCT_UOM]
     */
    public void setProductUom(Integer  productUom){
        this.productUom = productUom ;
        this.modify("product_uom",productUom);
    }

    /**
     * 设置 [PICKING_ID]
     */
    public void setPickingId(Integer  pickingId){
        this.pickingId = pickingId ;
        this.modify("picking_id",pickingId);
    }

    /**
     * 设置 [PARTNER_ID]
     */
    public void setPartnerId(Integer  partnerId){
        this.partnerId = partnerId ;
        this.modify("partner_id",partnerId);
    }

    /**
     * 设置 [PACKAGE_LEVEL_ID]
     */
    public void setPackageLevelId(Integer  packageLevelId){
        this.packageLevelId = packageLevelId ;
        this.modify("package_level_id",packageLevelId);
    }

    /**
     * 设置 [PRODUCT_PACKAGING]
     */
    public void setProductPackaging(Integer  productPackaging){
        this.productPackaging = productPackaging ;
        this.modify("product_packaging",productPackaging);
    }

    /**
     * 设置 [RESTRICT_PARTNER_ID]
     */
    public void setRestrictPartnerId(Integer  restrictPartnerId){
        this.restrictPartnerId = restrictPartnerId ;
        this.modify("restrict_partner_id",restrictPartnerId);
    }

    /**
     * 设置 [RULE_ID]
     */
    public void setRuleId(Integer  ruleId){
        this.ruleId = ruleId ;
        this.modify("rule_id",ruleId);
    }

    /**
     * 设置 [WAREHOUSE_ID]
     */
    public void setWarehouseId(Integer  warehouseId){
        this.warehouseId = warehouseId ;
        this.modify("warehouse_id",warehouseId);
    }

    /**
     * 设置 [LOCATION_DEST_ID]
     */
    public void setLocationDestId(Integer  locationDestId){
        this.locationDestId = locationDestId ;
        this.modify("location_dest_id",locationDestId);
    }

    /**
     * 设置 [RAW_MATERIAL_PRODUCTION_ID]
     */
    public void setRawMaterialProductionId(Integer  rawMaterialProductionId){
        this.rawMaterialProductionId = rawMaterialProductionId ;
        this.modify("raw_material_production_id",rawMaterialProductionId);
    }

    /**
     * 设置 [INVENTORY_ID]
     */
    public void setInventoryId(Integer  inventoryId){
        this.inventoryId = inventoryId ;
        this.modify("inventory_id",inventoryId);
    }

    /**
     * 设置 [PURCHASE_LINE_ID]
     */
    public void setPurchaseLineId(Integer  purchaseLineId){
        this.purchaseLineId = purchaseLineId ;
        this.modify("purchase_line_id",purchaseLineId);
    }

    /**
     * 设置 [LOCATION_ID]
     */
    public void setLocationId(Integer  locationId){
        this.locationId = locationId ;
        this.modify("location_id",locationId);
    }

    /**
     * 设置 [COMPANY_ID]
     */
    public void setCompanyId(Integer  companyId){
        this.companyId = companyId ;
        this.modify("company_id",companyId);
    }


}

