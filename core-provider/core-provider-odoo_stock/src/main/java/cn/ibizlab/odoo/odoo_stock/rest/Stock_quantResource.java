package cn.ibizlab.odoo.odoo_stock.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_stock.dto.*;
import cn.ibizlab.odoo.odoo_stock.mapping.*;
import cn.ibizlab.odoo.core.odoo_stock.domain.Stock_quant;
import cn.ibizlab.odoo.core.odoo_stock.service.IStock_quantService;
import cn.ibizlab.odoo.core.odoo_stock.filter.Stock_quantSearchContext;




@Slf4j
@Api(tags = {"Stock_quant" })
@RestController("odoo_stock-stock_quant")
@RequestMapping("")
public class Stock_quantResource {

    @Autowired
    private IStock_quantService stock_quantService;

    @Autowired
    @Lazy
    private Stock_quantMapping stock_quantMapping;










    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Stock_quant" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/stock_quants")

    public ResponseEntity<Stock_quantDTO> create(@RequestBody Stock_quantDTO stock_quantdto) {
        Stock_quant domain = stock_quantMapping.toDomain(stock_quantdto);
		stock_quantService.create(domain);
        Stock_quantDTO dto = stock_quantMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Stock_quant" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/stock_quants/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Stock_quantDTO> stock_quantdtos) {
        stock_quantService.createBatch(stock_quantMapping.toDomain(stock_quantdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission('Remove',{#stock_quant_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Stock_quant" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/stock_quants/{stock_quant_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("stock_quant_id") Integer stock_quant_id) {
         return ResponseEntity.status(HttpStatus.OK).body(stock_quantService.remove(stock_quant_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Stock_quant" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/stock_quants/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        stock_quantService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission(#stock_quant_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Stock_quant" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/stock_quants/{stock_quant_id}")

    public ResponseEntity<Stock_quantDTO> update(@PathVariable("stock_quant_id") Integer stock_quant_id, @RequestBody Stock_quantDTO stock_quantdto) {
		Stock_quant domain = stock_quantMapping.toDomain(stock_quantdto);
        domain.setId(stock_quant_id);
		stock_quantService.update(domain);
		Stock_quantDTO dto = stock_quantMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#stock_quant_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Stock_quant" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/stock_quants/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Stock_quantDTO> stock_quantdtos) {
        stock_quantService.updateBatch(stock_quantMapping.toDomain(stock_quantdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }







    @PreAuthorize("hasPermission(#stock_quant_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Stock_quant" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/stock_quants/{stock_quant_id}")
    public ResponseEntity<Stock_quantDTO> get(@PathVariable("stock_quant_id") Integer stock_quant_id) {
        Stock_quant domain = stock_quantService.get(stock_quant_id);
        Stock_quantDTO dto = stock_quantMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Stock_quant" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/stock_quants/fetchdefault")
	public ResponseEntity<List<Stock_quantDTO>> fetchDefault(Stock_quantSearchContext context) {
        Page<Stock_quant> domains = stock_quantService.searchDefault(context) ;
        List<Stock_quantDTO> list = stock_quantMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Stock_quant" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/stock_quants/searchdefault")
	public ResponseEntity<Page<Stock_quantDTO>> searchDefault(Stock_quantSearchContext context) {
        Page<Stock_quant> domains = stock_quantService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(stock_quantMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Stock_quant getEntity(){
        return new Stock_quant();
    }

}
