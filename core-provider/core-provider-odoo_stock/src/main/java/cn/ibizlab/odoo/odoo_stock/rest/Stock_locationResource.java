package cn.ibizlab.odoo.odoo_stock.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_stock.dto.*;
import cn.ibizlab.odoo.odoo_stock.mapping.*;
import cn.ibizlab.odoo.core.odoo_stock.domain.Stock_location;
import cn.ibizlab.odoo.core.odoo_stock.service.IStock_locationService;
import cn.ibizlab.odoo.core.odoo_stock.filter.Stock_locationSearchContext;




@Slf4j
@Api(tags = {"Stock_location" })
@RestController("odoo_stock-stock_location")
@RequestMapping("")
public class Stock_locationResource {

    @Autowired
    private IStock_locationService stock_locationService;

    @Autowired
    @Lazy
    private Stock_locationMapping stock_locationMapping;




    @PreAuthorize("hasPermission('Remove',{#stock_location_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Stock_location" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/stock_locations/{stock_location_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("stock_location_id") Integer stock_location_id) {
         return ResponseEntity.status(HttpStatus.OK).body(stock_locationService.remove(stock_location_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Stock_location" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/stock_locations/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        stock_locationService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }










    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Stock_location" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/stock_locations")

    public ResponseEntity<Stock_locationDTO> create(@RequestBody Stock_locationDTO stock_locationdto) {
        Stock_location domain = stock_locationMapping.toDomain(stock_locationdto);
		stock_locationService.create(domain);
        Stock_locationDTO dto = stock_locationMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Stock_location" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/stock_locations/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Stock_locationDTO> stock_locationdtos) {
        stock_locationService.createBatch(stock_locationMapping.toDomain(stock_locationdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission(#stock_location_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Stock_location" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/stock_locations/{stock_location_id}")
    public ResponseEntity<Stock_locationDTO> get(@PathVariable("stock_location_id") Integer stock_location_id) {
        Stock_location domain = stock_locationService.get(stock_location_id);
        Stock_locationDTO dto = stock_locationMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }




    @PreAuthorize("hasPermission(#stock_location_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Stock_location" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/stock_locations/{stock_location_id}")

    public ResponseEntity<Stock_locationDTO> update(@PathVariable("stock_location_id") Integer stock_location_id, @RequestBody Stock_locationDTO stock_locationdto) {
		Stock_location domain = stock_locationMapping.toDomain(stock_locationdto);
        domain.setId(stock_location_id);
		stock_locationService.update(domain);
		Stock_locationDTO dto = stock_locationMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#stock_location_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Stock_location" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/stock_locations/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Stock_locationDTO> stock_locationdtos) {
        stock_locationService.updateBatch(stock_locationMapping.toDomain(stock_locationdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Stock_location" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/stock_locations/fetchdefault")
	public ResponseEntity<List<Stock_locationDTO>> fetchDefault(Stock_locationSearchContext context) {
        Page<Stock_location> domains = stock_locationService.searchDefault(context) ;
        List<Stock_locationDTO> list = stock_locationMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Stock_location" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/stock_locations/searchdefault")
	public ResponseEntity<Page<Stock_locationDTO>> searchDefault(Stock_locationSearchContext context) {
        Page<Stock_location> domains = stock_locationService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(stock_locationMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Stock_location getEntity(){
        return new Stock_location();
    }

}
