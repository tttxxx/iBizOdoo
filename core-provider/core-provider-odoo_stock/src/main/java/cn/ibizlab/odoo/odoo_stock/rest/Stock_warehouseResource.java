package cn.ibizlab.odoo.odoo_stock.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_stock.dto.*;
import cn.ibizlab.odoo.odoo_stock.mapping.*;
import cn.ibizlab.odoo.core.odoo_stock.domain.Stock_warehouse;
import cn.ibizlab.odoo.core.odoo_stock.service.IStock_warehouseService;
import cn.ibizlab.odoo.core.odoo_stock.filter.Stock_warehouseSearchContext;




@Slf4j
@Api(tags = {"Stock_warehouse" })
@RestController("odoo_stock-stock_warehouse")
@RequestMapping("")
public class Stock_warehouseResource {

    @Autowired
    private IStock_warehouseService stock_warehouseService;

    @Autowired
    @Lazy
    private Stock_warehouseMapping stock_warehouseMapping;




    @PreAuthorize("hasPermission(#stock_warehouse_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Stock_warehouse" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/stock_warehouses/{stock_warehouse_id}")
    public ResponseEntity<Stock_warehouseDTO> get(@PathVariable("stock_warehouse_id") Integer stock_warehouse_id) {
        Stock_warehouse domain = stock_warehouseService.get(stock_warehouse_id);
        Stock_warehouseDTO dto = stock_warehouseMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }







    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Stock_warehouse" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/stock_warehouses")

    public ResponseEntity<Stock_warehouseDTO> create(@RequestBody Stock_warehouseDTO stock_warehousedto) {
        Stock_warehouse domain = stock_warehouseMapping.toDomain(stock_warehousedto);
		stock_warehouseService.create(domain);
        Stock_warehouseDTO dto = stock_warehouseMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Stock_warehouse" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/stock_warehouses/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Stock_warehouseDTO> stock_warehousedtos) {
        stock_warehouseService.createBatch(stock_warehouseMapping.toDomain(stock_warehousedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission(#stock_warehouse_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Stock_warehouse" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/stock_warehouses/{stock_warehouse_id}")

    public ResponseEntity<Stock_warehouseDTO> update(@PathVariable("stock_warehouse_id") Integer stock_warehouse_id, @RequestBody Stock_warehouseDTO stock_warehousedto) {
		Stock_warehouse domain = stock_warehouseMapping.toDomain(stock_warehousedto);
        domain.setId(stock_warehouse_id);
		stock_warehouseService.update(domain);
		Stock_warehouseDTO dto = stock_warehouseMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#stock_warehouse_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Stock_warehouse" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/stock_warehouses/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Stock_warehouseDTO> stock_warehousedtos) {
        stock_warehouseService.updateBatch(stock_warehouseMapping.toDomain(stock_warehousedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }










    @PreAuthorize("hasPermission('Remove',{#stock_warehouse_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Stock_warehouse" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/stock_warehouses/{stock_warehouse_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("stock_warehouse_id") Integer stock_warehouse_id) {
         return ResponseEntity.status(HttpStatus.OK).body(stock_warehouseService.remove(stock_warehouse_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Stock_warehouse" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/stock_warehouses/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        stock_warehouseService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Stock_warehouse" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/stock_warehouses/fetchdefault")
	public ResponseEntity<List<Stock_warehouseDTO>> fetchDefault(Stock_warehouseSearchContext context) {
        Page<Stock_warehouse> domains = stock_warehouseService.searchDefault(context) ;
        List<Stock_warehouseDTO> list = stock_warehouseMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Stock_warehouse" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/stock_warehouses/searchdefault")
	public ResponseEntity<Page<Stock_warehouseDTO>> searchDefault(Stock_warehouseSearchContext context) {
        Page<Stock_warehouse> domains = stock_warehouseService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(stock_warehouseMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Stock_warehouse getEntity(){
        return new Stock_warehouse();
    }

}
