package cn.ibizlab.odoo.odoo_stock.mapping;

import org.mapstruct.*;
import cn.ibizlab.odoo.core.odoo_stock.domain.Stock_warehouse_orderpoint;
import cn.ibizlab.odoo.odoo_stock.dto.Stock_warehouse_orderpointDTO;
import cn.ibizlab.odoo.util.domain.MappingBase;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring", uses = {},
    nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
    nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface Stock_warehouse_orderpointMapping extends MappingBase<Stock_warehouse_orderpointDTO, Stock_warehouse_orderpoint> {


}

