package cn.ibizlab.odoo.odoo_stock.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_stock.dto.*;
import cn.ibizlab.odoo.odoo_stock.mapping.*;
import cn.ibizlab.odoo.core.odoo_stock.domain.Stock_quant_package;
import cn.ibizlab.odoo.core.odoo_stock.service.IStock_quant_packageService;
import cn.ibizlab.odoo.core.odoo_stock.filter.Stock_quant_packageSearchContext;




@Slf4j
@Api(tags = {"Stock_quant_package" })
@RestController("odoo_stock-stock_quant_package")
@RequestMapping("")
public class Stock_quant_packageResource {

    @Autowired
    private IStock_quant_packageService stock_quant_packageService;

    @Autowired
    @Lazy
    private Stock_quant_packageMapping stock_quant_packageMapping;




    @PreAuthorize("hasPermission(#stock_quant_package_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Stock_quant_package" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/stock_quant_packages/{stock_quant_package_id}")

    public ResponseEntity<Stock_quant_packageDTO> update(@PathVariable("stock_quant_package_id") Integer stock_quant_package_id, @RequestBody Stock_quant_packageDTO stock_quant_packagedto) {
		Stock_quant_package domain = stock_quant_packageMapping.toDomain(stock_quant_packagedto);
        domain.setId(stock_quant_package_id);
		stock_quant_packageService.update(domain);
		Stock_quant_packageDTO dto = stock_quant_packageMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#stock_quant_package_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Stock_quant_package" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/stock_quant_packages/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Stock_quant_packageDTO> stock_quant_packagedtos) {
        stock_quant_packageService.updateBatch(stock_quant_packageMapping.toDomain(stock_quant_packagedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission('Remove',{#stock_quant_package_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Stock_quant_package" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/stock_quant_packages/{stock_quant_package_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("stock_quant_package_id") Integer stock_quant_package_id) {
         return ResponseEntity.status(HttpStatus.OK).body(stock_quant_packageService.remove(stock_quant_package_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Stock_quant_package" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/stock_quant_packages/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        stock_quant_packageService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }







    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Stock_quant_package" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/stock_quant_packages")

    public ResponseEntity<Stock_quant_packageDTO> create(@RequestBody Stock_quant_packageDTO stock_quant_packagedto) {
        Stock_quant_package domain = stock_quant_packageMapping.toDomain(stock_quant_packagedto);
		stock_quant_packageService.create(domain);
        Stock_quant_packageDTO dto = stock_quant_packageMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Stock_quant_package" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/stock_quant_packages/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Stock_quant_packageDTO> stock_quant_packagedtos) {
        stock_quant_packageService.createBatch(stock_quant_packageMapping.toDomain(stock_quant_packagedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }







    @PreAuthorize("hasPermission(#stock_quant_package_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Stock_quant_package" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/stock_quant_packages/{stock_quant_package_id}")
    public ResponseEntity<Stock_quant_packageDTO> get(@PathVariable("stock_quant_package_id") Integer stock_quant_package_id) {
        Stock_quant_package domain = stock_quant_packageService.get(stock_quant_package_id);
        Stock_quant_packageDTO dto = stock_quant_packageMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }




    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Stock_quant_package" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/stock_quant_packages/fetchdefault")
	public ResponseEntity<List<Stock_quant_packageDTO>> fetchDefault(Stock_quant_packageSearchContext context) {
        Page<Stock_quant_package> domains = stock_quant_packageService.searchDefault(context) ;
        List<Stock_quant_packageDTO> list = stock_quant_packageMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Stock_quant_package" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/stock_quant_packages/searchdefault")
	public ResponseEntity<Page<Stock_quant_packageDTO>> searchDefault(Stock_quant_packageSearchContext context) {
        Page<Stock_quant_package> domains = stock_quant_packageService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(stock_quant_packageMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Stock_quant_package getEntity(){
        return new Stock_quant_package();
    }

}
