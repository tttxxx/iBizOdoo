package cn.ibizlab.odoo.odoo_stock.mapping;

import org.mapstruct.*;
import cn.ibizlab.odoo.core.odoo_stock.domain.Stock_move_line;
import cn.ibizlab.odoo.odoo_stock.dto.Stock_move_lineDTO;
import cn.ibizlab.odoo.util.domain.MappingBase;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring", uses = {},
    nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
    nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface Stock_move_lineMapping extends MappingBase<Stock_move_lineDTO, Stock_move_line> {


}

