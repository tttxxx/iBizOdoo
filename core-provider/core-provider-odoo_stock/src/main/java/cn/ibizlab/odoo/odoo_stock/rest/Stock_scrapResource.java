package cn.ibizlab.odoo.odoo_stock.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_stock.dto.*;
import cn.ibizlab.odoo.odoo_stock.mapping.*;
import cn.ibizlab.odoo.core.odoo_stock.domain.Stock_scrap;
import cn.ibizlab.odoo.core.odoo_stock.service.IStock_scrapService;
import cn.ibizlab.odoo.core.odoo_stock.filter.Stock_scrapSearchContext;




@Slf4j
@Api(tags = {"Stock_scrap" })
@RestController("odoo_stock-stock_scrap")
@RequestMapping("")
public class Stock_scrapResource {

    @Autowired
    private IStock_scrapService stock_scrapService;

    @Autowired
    @Lazy
    private Stock_scrapMapping stock_scrapMapping;







    @PreAuthorize("hasPermission('Remove',{#stock_scrap_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Stock_scrap" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/stock_scraps/{stock_scrap_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("stock_scrap_id") Integer stock_scrap_id) {
         return ResponseEntity.status(HttpStatus.OK).body(stock_scrapService.remove(stock_scrap_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Stock_scrap" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/stock_scraps/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        stock_scrapService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Stock_scrap" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/stock_scraps")

    public ResponseEntity<Stock_scrapDTO> create(@RequestBody Stock_scrapDTO stock_scrapdto) {
        Stock_scrap domain = stock_scrapMapping.toDomain(stock_scrapdto);
		stock_scrapService.create(domain);
        Stock_scrapDTO dto = stock_scrapMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Stock_scrap" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/stock_scraps/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Stock_scrapDTO> stock_scrapdtos) {
        stock_scrapService.createBatch(stock_scrapMapping.toDomain(stock_scrapdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }







    @PreAuthorize("hasPermission(#stock_scrap_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Stock_scrap" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/stock_scraps/{stock_scrap_id}")

    public ResponseEntity<Stock_scrapDTO> update(@PathVariable("stock_scrap_id") Integer stock_scrap_id, @RequestBody Stock_scrapDTO stock_scrapdto) {
		Stock_scrap domain = stock_scrapMapping.toDomain(stock_scrapdto);
        domain.setId(stock_scrap_id);
		stock_scrapService.update(domain);
		Stock_scrapDTO dto = stock_scrapMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#stock_scrap_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Stock_scrap" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/stock_scraps/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Stock_scrapDTO> stock_scrapdtos) {
        stock_scrapService.updateBatch(stock_scrapMapping.toDomain(stock_scrapdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission(#stock_scrap_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Stock_scrap" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/stock_scraps/{stock_scrap_id}")
    public ResponseEntity<Stock_scrapDTO> get(@PathVariable("stock_scrap_id") Integer stock_scrap_id) {
        Stock_scrap domain = stock_scrapService.get(stock_scrap_id);
        Stock_scrapDTO dto = stock_scrapMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }




    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Stock_scrap" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/stock_scraps/fetchdefault")
	public ResponseEntity<List<Stock_scrapDTO>> fetchDefault(Stock_scrapSearchContext context) {
        Page<Stock_scrap> domains = stock_scrapService.searchDefault(context) ;
        List<Stock_scrapDTO> list = stock_scrapMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Stock_scrap" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/stock_scraps/searchdefault")
	public ResponseEntity<Page<Stock_scrapDTO>> searchDefault(Stock_scrapSearchContext context) {
        Page<Stock_scrap> domains = stock_scrapService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(stock_scrapMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Stock_scrap getEntity(){
        return new Stock_scrap();
    }

}
