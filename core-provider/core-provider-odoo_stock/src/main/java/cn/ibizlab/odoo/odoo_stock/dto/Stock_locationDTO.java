package cn.ibizlab.odoo.odoo_stock.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;
import cn.ibizlab.odoo.util.domain.DTOBase;
import lombok.Data;

/**
 * 服务DTO对象[Stock_locationDTO]
 */
@Data
public class Stock_locationDTO extends DTOBase implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [POSX]
     *
     */
    @JSONField(name = "posx")
    @JsonProperty("posx")
    private Integer posx;

    /**
     * 属性 [POSY]
     *
     */
    @JSONField(name = "posy")
    @JsonProperty("posy")
    private Integer posy;

    /**
     * 属性 [ACTIVE]
     *
     */
    @JSONField(name = "active")
    @JsonProperty("active")
    private String active;

    /**
     * 属性 [CREATE_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;

    /**
     * 属性 [BARCODE]
     *
     */
    @JSONField(name = "barcode")
    @JsonProperty("barcode")
    private String barcode;

    /**
     * 属性 [WRITE_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    private String displayName;

    /**
     * 属性 [POSZ]
     *
     */
    @JSONField(name = "posz")
    @JsonProperty("posz")
    private Integer posz;

    /**
     * 属性 [COMPLETE_NAME]
     *
     */
    @JSONField(name = "complete_name")
    @JsonProperty("complete_name")
    private String completeName;

    /**
     * 属性 [SCRAP_LOCATION]
     *
     */
    @JSONField(name = "scrap_location")
    @JsonProperty("scrap_location")
    private String scrapLocation;

    /**
     * 属性 [RETURN_LOCATION]
     *
     */
    @JSONField(name = "return_location")
    @JsonProperty("return_location")
    private String returnLocation;

    /**
     * 属性 [NAME]
     *
     */
    @JSONField(name = "name")
    @JsonProperty("name")
    private String name;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;

    /**
     * 属性 [PARENT_PATH]
     *
     */
    @JSONField(name = "parent_path")
    @JsonProperty("parent_path")
    private String parentPath;

    /**
     * 属性 [QUANT_IDS]
     *
     */
    @JSONField(name = "quant_ids")
    @JsonProperty("quant_ids")
    private String quantIds;

    /**
     * 属性 [USAGE]
     *
     */
    @JSONField(name = "usage")
    @JsonProperty("usage")
    private String usage;

    /**
     * 属性 [COMMENT]
     *
     */
    @JSONField(name = "comment")
    @JsonProperty("comment")
    private String comment;

    /**
     * 属性 [ID]
     *
     */
    @JSONField(name = "id")
    @JsonProperty("id")
    private Integer id;

    /**
     * 属性 [CHILD_IDS]
     *
     */
    @JSONField(name = "child_ids")
    @JsonProperty("child_ids")
    private String childIds;

    /**
     * 属性 [LOCATION_ID_TEXT]
     *
     */
    @JSONField(name = "location_id_text")
    @JsonProperty("location_id_text")
    private String locationIdText;

    /**
     * 属性 [COMPANY_ID_TEXT]
     *
     */
    @JSONField(name = "company_id_text")
    @JsonProperty("company_id_text")
    private String companyIdText;

    /**
     * 属性 [CREATE_UID_TEXT]
     *
     */
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    private String createUidText;

    /**
     * 属性 [VALUATION_OUT_ACCOUNT_ID_TEXT]
     *
     */
    @JSONField(name = "valuation_out_account_id_text")
    @JsonProperty("valuation_out_account_id_text")
    private String valuationOutAccountIdText;

    /**
     * 属性 [PARTNER_ID_TEXT]
     *
     */
    @JSONField(name = "partner_id_text")
    @JsonProperty("partner_id_text")
    private String partnerIdText;

    /**
     * 属性 [REMOVAL_STRATEGY_ID_TEXT]
     *
     */
    @JSONField(name = "removal_strategy_id_text")
    @JsonProperty("removal_strategy_id_text")
    private String removalStrategyIdText;

    /**
     * 属性 [PUTAWAY_STRATEGY_ID_TEXT]
     *
     */
    @JSONField(name = "putaway_strategy_id_text")
    @JsonProperty("putaway_strategy_id_text")
    private String putawayStrategyIdText;

    /**
     * 属性 [WRITE_UID_TEXT]
     *
     */
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    private String writeUidText;

    /**
     * 属性 [VALUATION_IN_ACCOUNT_ID_TEXT]
     *
     */
    @JSONField(name = "valuation_in_account_id_text")
    @JsonProperty("valuation_in_account_id_text")
    private String valuationInAccountIdText;

    /**
     * 属性 [WRITE_UID]
     *
     */
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    private Integer writeUid;

    /**
     * 属性 [CREATE_UID]
     *
     */
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    private Integer createUid;

    /**
     * 属性 [VALUATION_IN_ACCOUNT_ID]
     *
     */
    @JSONField(name = "valuation_in_account_id")
    @JsonProperty("valuation_in_account_id")
    private Integer valuationInAccountId;

    /**
     * 属性 [COMPANY_ID]
     *
     */
    @JSONField(name = "company_id")
    @JsonProperty("company_id")
    private Integer companyId;

    /**
     * 属性 [LOCATION_ID]
     *
     */
    @JSONField(name = "location_id")
    @JsonProperty("location_id")
    private Integer locationId;

    /**
     * 属性 [VALUATION_OUT_ACCOUNT_ID]
     *
     */
    @JSONField(name = "valuation_out_account_id")
    @JsonProperty("valuation_out_account_id")
    private Integer valuationOutAccountId;

    /**
     * 属性 [PARTNER_ID]
     *
     */
    @JSONField(name = "partner_id")
    @JsonProperty("partner_id")
    private Integer partnerId;

    /**
     * 属性 [PUTAWAY_STRATEGY_ID]
     *
     */
    @JSONField(name = "putaway_strategy_id")
    @JsonProperty("putaway_strategy_id")
    private Integer putawayStrategyId;

    /**
     * 属性 [REMOVAL_STRATEGY_ID]
     *
     */
    @JSONField(name = "removal_strategy_id")
    @JsonProperty("removal_strategy_id")
    private Integer removalStrategyId;


    /**
     * 设置 [POSX]
     */
    public void setPosx(Integer  posx){
        this.posx = posx ;
        this.modify("posx",posx);
    }

    /**
     * 设置 [POSY]
     */
    public void setPosy(Integer  posy){
        this.posy = posy ;
        this.modify("posy",posy);
    }

    /**
     * 设置 [ACTIVE]
     */
    public void setActive(String  active){
        this.active = active ;
        this.modify("active",active);
    }

    /**
     * 设置 [BARCODE]
     */
    public void setBarcode(String  barcode){
        this.barcode = barcode ;
        this.modify("barcode",barcode);
    }

    /**
     * 设置 [POSZ]
     */
    public void setPosz(Integer  posz){
        this.posz = posz ;
        this.modify("posz",posz);
    }

    /**
     * 设置 [COMPLETE_NAME]
     */
    public void setCompleteName(String  completeName){
        this.completeName = completeName ;
        this.modify("complete_name",completeName);
    }

    /**
     * 设置 [SCRAP_LOCATION]
     */
    public void setScrapLocation(String  scrapLocation){
        this.scrapLocation = scrapLocation ;
        this.modify("scrap_location",scrapLocation);
    }

    /**
     * 设置 [RETURN_LOCATION]
     */
    public void setReturnLocation(String  returnLocation){
        this.returnLocation = returnLocation ;
        this.modify("return_location",returnLocation);
    }

    /**
     * 设置 [NAME]
     */
    public void setName(String  name){
        this.name = name ;
        this.modify("name",name);
    }

    /**
     * 设置 [PARENT_PATH]
     */
    public void setParentPath(String  parentPath){
        this.parentPath = parentPath ;
        this.modify("parent_path",parentPath);
    }

    /**
     * 设置 [USAGE]
     */
    public void setUsage(String  usage){
        this.usage = usage ;
        this.modify("usage",usage);
    }

    /**
     * 设置 [COMMENT]
     */
    public void setComment(String  comment){
        this.comment = comment ;
        this.modify("comment",comment);
    }

    /**
     * 设置 [VALUATION_IN_ACCOUNT_ID]
     */
    public void setValuationInAccountId(Integer  valuationInAccountId){
        this.valuationInAccountId = valuationInAccountId ;
        this.modify("valuation_in_account_id",valuationInAccountId);
    }

    /**
     * 设置 [COMPANY_ID]
     */
    public void setCompanyId(Integer  companyId){
        this.companyId = companyId ;
        this.modify("company_id",companyId);
    }

    /**
     * 设置 [LOCATION_ID]
     */
    public void setLocationId(Integer  locationId){
        this.locationId = locationId ;
        this.modify("location_id",locationId);
    }

    /**
     * 设置 [VALUATION_OUT_ACCOUNT_ID]
     */
    public void setValuationOutAccountId(Integer  valuationOutAccountId){
        this.valuationOutAccountId = valuationOutAccountId ;
        this.modify("valuation_out_account_id",valuationOutAccountId);
    }

    /**
     * 设置 [PARTNER_ID]
     */
    public void setPartnerId(Integer  partnerId){
        this.partnerId = partnerId ;
        this.modify("partner_id",partnerId);
    }

    /**
     * 设置 [PUTAWAY_STRATEGY_ID]
     */
    public void setPutawayStrategyId(Integer  putawayStrategyId){
        this.putawayStrategyId = putawayStrategyId ;
        this.modify("putaway_strategy_id",putawayStrategyId);
    }

    /**
     * 设置 [REMOVAL_STRATEGY_ID]
     */
    public void setRemovalStrategyId(Integer  removalStrategyId){
        this.removalStrategyId = removalStrategyId ;
        this.modify("removal_strategy_id",removalStrategyId);
    }


}

