package cn.ibizlab.odoo.odoo_note.config;

import org.springframework.context.annotation.ComponentScan;

@ComponentScan("cn.ibizlab.odoo.odoo_note")
public class odoo_noteRestConfiguration {

}
