package cn.ibizlab.odoo.odoo_note.mapping;

import org.mapstruct.*;
import cn.ibizlab.odoo.core.odoo_note.domain.Note_tag;
import cn.ibizlab.odoo.odoo_note.dto.Note_tagDTO;
import cn.ibizlab.odoo.util.domain.MappingBase;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring", uses = {},
    nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
    nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface Note_tagMapping extends MappingBase<Note_tagDTO, Note_tag> {


}

