package cn.ibizlab.odoo.odoo_note.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import lombok.Data;

@ConfigurationProperties(prefix = "service.odoo-note")
@Data
public class odoo_noteServiceProperties {

	private boolean enabled;

	private boolean auth;


}