package cn.ibizlab.odoo.odoo_uom.mapping;

import org.mapstruct.*;
import cn.ibizlab.odoo.core.odoo_uom.domain.Uom_uom;
import cn.ibizlab.odoo.odoo_uom.dto.Uom_uomDTO;
import cn.ibizlab.odoo.util.domain.MappingBase;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring", uses = {},
    nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
    nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface Uom_uomMapping extends MappingBase<Uom_uomDTO, Uom_uom> {


}

