package cn.ibizlab.odoo.odoo_payment.mapping;

import org.mapstruct.*;
import cn.ibizlab.odoo.core.odoo_payment.domain.Payment_transaction;
import cn.ibizlab.odoo.odoo_payment.dto.Payment_transactionDTO;
import cn.ibizlab.odoo.util.domain.MappingBase;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring", uses = {},
    nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
    nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface Payment_transactionMapping extends MappingBase<Payment_transactionDTO, Payment_transaction> {


}

