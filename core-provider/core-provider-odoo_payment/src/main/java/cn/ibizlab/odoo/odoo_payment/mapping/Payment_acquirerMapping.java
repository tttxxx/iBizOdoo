package cn.ibizlab.odoo.odoo_payment.mapping;

import org.mapstruct.*;
import cn.ibizlab.odoo.core.odoo_payment.domain.Payment_acquirer;
import cn.ibizlab.odoo.odoo_payment.dto.Payment_acquirerDTO;
import cn.ibizlab.odoo.util.domain.MappingBase;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring", uses = {},
    nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
    nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface Payment_acquirerMapping extends MappingBase<Payment_acquirerDTO, Payment_acquirer> {


}

