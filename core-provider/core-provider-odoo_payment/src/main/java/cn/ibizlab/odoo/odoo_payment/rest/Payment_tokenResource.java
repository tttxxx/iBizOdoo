package cn.ibizlab.odoo.odoo_payment.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_payment.dto.*;
import cn.ibizlab.odoo.odoo_payment.mapping.*;
import cn.ibizlab.odoo.core.odoo_payment.domain.Payment_token;
import cn.ibizlab.odoo.core.odoo_payment.service.IPayment_tokenService;
import cn.ibizlab.odoo.core.odoo_payment.filter.Payment_tokenSearchContext;




@Slf4j
@Api(tags = {"Payment_token" })
@RestController("odoo_payment-payment_token")
@RequestMapping("")
public class Payment_tokenResource {

    @Autowired
    private IPayment_tokenService payment_tokenService;

    @Autowired
    @Lazy
    private Payment_tokenMapping payment_tokenMapping;




    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Payment_token" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/payment_tokens")

    public ResponseEntity<Payment_tokenDTO> create(@RequestBody Payment_tokenDTO payment_tokendto) {
        Payment_token domain = payment_tokenMapping.toDomain(payment_tokendto);
		payment_tokenService.create(domain);
        Payment_tokenDTO dto = payment_tokenMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Payment_token" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/payment_tokens/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Payment_tokenDTO> payment_tokendtos) {
        payment_tokenService.createBatch(payment_tokenMapping.toDomain(payment_tokendtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }










    @PreAuthorize("hasPermission(#payment_token_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Payment_token" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/payment_tokens/{payment_token_id}")

    public ResponseEntity<Payment_tokenDTO> update(@PathVariable("payment_token_id") Integer payment_token_id, @RequestBody Payment_tokenDTO payment_tokendto) {
		Payment_token domain = payment_tokenMapping.toDomain(payment_tokendto);
        domain.setId(payment_token_id);
		payment_tokenService.update(domain);
		Payment_tokenDTO dto = payment_tokenMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#payment_token_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Payment_token" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/payment_tokens/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Payment_tokenDTO> payment_tokendtos) {
        payment_tokenService.updateBatch(payment_tokenMapping.toDomain(payment_tokendtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }







    @PreAuthorize("hasPermission('Remove',{#payment_token_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Payment_token" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/payment_tokens/{payment_token_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("payment_token_id") Integer payment_token_id) {
         return ResponseEntity.status(HttpStatus.OK).body(payment_tokenService.remove(payment_token_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Payment_token" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/payment_tokens/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        payment_tokenService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission(#payment_token_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Payment_token" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/payment_tokens/{payment_token_id}")
    public ResponseEntity<Payment_tokenDTO> get(@PathVariable("payment_token_id") Integer payment_token_id) {
        Payment_token domain = payment_tokenService.get(payment_token_id);
        Payment_tokenDTO dto = payment_tokenMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Payment_token" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/payment_tokens/fetchdefault")
	public ResponseEntity<List<Payment_tokenDTO>> fetchDefault(Payment_tokenSearchContext context) {
        Page<Payment_token> domains = payment_tokenService.searchDefault(context) ;
        List<Payment_tokenDTO> list = payment_tokenMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Payment_token" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/payment_tokens/searchdefault")
	public ResponseEntity<Page<Payment_tokenDTO>> searchDefault(Payment_tokenSearchContext context) {
        Page<Payment_token> domains = payment_tokenService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(payment_tokenMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Payment_token getEntity(){
        return new Payment_token();
    }

}
