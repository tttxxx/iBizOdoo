package cn.ibizlab.odoo.odoo_payment.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_payment.dto.*;
import cn.ibizlab.odoo.odoo_payment.mapping.*;
import cn.ibizlab.odoo.core.odoo_payment.domain.Payment_transaction;
import cn.ibizlab.odoo.core.odoo_payment.service.IPayment_transactionService;
import cn.ibizlab.odoo.core.odoo_payment.filter.Payment_transactionSearchContext;




@Slf4j
@Api(tags = {"Payment_transaction" })
@RestController("odoo_payment-payment_transaction")
@RequestMapping("")
public class Payment_transactionResource {

    @Autowired
    private IPayment_transactionService payment_transactionService;

    @Autowired
    @Lazy
    private Payment_transactionMapping payment_transactionMapping;




    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Payment_transaction" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/payment_transactions")

    public ResponseEntity<Payment_transactionDTO> create(@RequestBody Payment_transactionDTO payment_transactiondto) {
        Payment_transaction domain = payment_transactionMapping.toDomain(payment_transactiondto);
		payment_transactionService.create(domain);
        Payment_transactionDTO dto = payment_transactionMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Payment_transaction" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/payment_transactions/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Payment_transactionDTO> payment_transactiondtos) {
        payment_transactionService.createBatch(payment_transactionMapping.toDomain(payment_transactiondtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }







    @PreAuthorize("hasPermission('Remove',{#payment_transaction_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Payment_transaction" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/payment_transactions/{payment_transaction_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("payment_transaction_id") Integer payment_transaction_id) {
         return ResponseEntity.status(HttpStatus.OK).body(payment_transactionService.remove(payment_transaction_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Payment_transaction" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/payment_transactions/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        payment_transactionService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }







    @PreAuthorize("hasPermission(#payment_transaction_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Payment_transaction" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/payment_transactions/{payment_transaction_id}")
    public ResponseEntity<Payment_transactionDTO> get(@PathVariable("payment_transaction_id") Integer payment_transaction_id) {
        Payment_transaction domain = payment_transactionService.get(payment_transaction_id);
        Payment_transactionDTO dto = payment_transactionMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }







    @PreAuthorize("hasPermission(#payment_transaction_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Payment_transaction" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/payment_transactions/{payment_transaction_id}")

    public ResponseEntity<Payment_transactionDTO> update(@PathVariable("payment_transaction_id") Integer payment_transaction_id, @RequestBody Payment_transactionDTO payment_transactiondto) {
		Payment_transaction domain = payment_transactionMapping.toDomain(payment_transactiondto);
        domain.setId(payment_transaction_id);
		payment_transactionService.update(domain);
		Payment_transactionDTO dto = payment_transactionMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#payment_transaction_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Payment_transaction" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/payment_transactions/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Payment_transactionDTO> payment_transactiondtos) {
        payment_transactionService.updateBatch(payment_transactionMapping.toDomain(payment_transactiondtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Payment_transaction" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/payment_transactions/fetchdefault")
	public ResponseEntity<List<Payment_transactionDTO>> fetchDefault(Payment_transactionSearchContext context) {
        Page<Payment_transaction> domains = payment_transactionService.searchDefault(context) ;
        List<Payment_transactionDTO> list = payment_transactionMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Payment_transaction" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/payment_transactions/searchdefault")
	public ResponseEntity<Page<Payment_transactionDTO>> searchDefault(Payment_transactionSearchContext context) {
        Page<Payment_transaction> domains = payment_transactionService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(payment_transactionMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Payment_transaction getEntity(){
        return new Payment_transaction();
    }

}
