package cn.ibizlab.odoo.odoo_payment.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_payment.dto.*;
import cn.ibizlab.odoo.odoo_payment.mapping.*;
import cn.ibizlab.odoo.core.odoo_payment.domain.Payment_icon;
import cn.ibizlab.odoo.core.odoo_payment.service.IPayment_iconService;
import cn.ibizlab.odoo.core.odoo_payment.filter.Payment_iconSearchContext;




@Slf4j
@Api(tags = {"Payment_icon" })
@RestController("odoo_payment-payment_icon")
@RequestMapping("")
public class Payment_iconResource {

    @Autowired
    private IPayment_iconService payment_iconService;

    @Autowired
    @Lazy
    private Payment_iconMapping payment_iconMapping;




    @PreAuthorize("hasPermission(#payment_icon_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Payment_icon" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/payment_icons/{payment_icon_id}")

    public ResponseEntity<Payment_iconDTO> update(@PathVariable("payment_icon_id") Integer payment_icon_id, @RequestBody Payment_iconDTO payment_icondto) {
		Payment_icon domain = payment_iconMapping.toDomain(payment_icondto);
        domain.setId(payment_icon_id);
		payment_iconService.update(domain);
		Payment_iconDTO dto = payment_iconMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#payment_icon_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Payment_icon" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/payment_icons/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Payment_iconDTO> payment_icondtos) {
        payment_iconService.updateBatch(payment_iconMapping.toDomain(payment_icondtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }







    @PreAuthorize("hasPermission('Remove',{#payment_icon_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Payment_icon" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/payment_icons/{payment_icon_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("payment_icon_id") Integer payment_icon_id) {
         return ResponseEntity.status(HttpStatus.OK).body(payment_iconService.remove(payment_icon_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Payment_icon" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/payment_icons/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        payment_iconService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission(#payment_icon_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Payment_icon" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/payment_icons/{payment_icon_id}")
    public ResponseEntity<Payment_iconDTO> get(@PathVariable("payment_icon_id") Integer payment_icon_id) {
        Payment_icon domain = payment_iconService.get(payment_icon_id);
        Payment_iconDTO dto = payment_iconMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }







    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Payment_icon" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/payment_icons")

    public ResponseEntity<Payment_iconDTO> create(@RequestBody Payment_iconDTO payment_icondto) {
        Payment_icon domain = payment_iconMapping.toDomain(payment_icondto);
		payment_iconService.create(domain);
        Payment_iconDTO dto = payment_iconMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Payment_icon" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/payment_icons/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Payment_iconDTO> payment_icondtos) {
        payment_iconService.createBatch(payment_iconMapping.toDomain(payment_icondtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Payment_icon" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/payment_icons/fetchdefault")
	public ResponseEntity<List<Payment_iconDTO>> fetchDefault(Payment_iconSearchContext context) {
        Page<Payment_icon> domains = payment_iconService.searchDefault(context) ;
        List<Payment_iconDTO> list = payment_iconMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Payment_icon" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/payment_icons/searchdefault")
	public ResponseEntity<Page<Payment_iconDTO>> searchDefault(Payment_iconSearchContext context) {
        Page<Payment_icon> domains = payment_iconService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(payment_iconMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Payment_icon getEntity(){
        return new Payment_icon();
    }

}
