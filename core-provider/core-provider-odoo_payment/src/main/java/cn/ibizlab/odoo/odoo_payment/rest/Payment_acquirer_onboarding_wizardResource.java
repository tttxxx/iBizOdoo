package cn.ibizlab.odoo.odoo_payment.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_payment.dto.*;
import cn.ibizlab.odoo.odoo_payment.mapping.*;
import cn.ibizlab.odoo.core.odoo_payment.domain.Payment_acquirer_onboarding_wizard;
import cn.ibizlab.odoo.core.odoo_payment.service.IPayment_acquirer_onboarding_wizardService;
import cn.ibizlab.odoo.core.odoo_payment.filter.Payment_acquirer_onboarding_wizardSearchContext;




@Slf4j
@Api(tags = {"Payment_acquirer_onboarding_wizard" })
@RestController("odoo_payment-payment_acquirer_onboarding_wizard")
@RequestMapping("")
public class Payment_acquirer_onboarding_wizardResource {

    @Autowired
    private IPayment_acquirer_onboarding_wizardService payment_acquirer_onboarding_wizardService;

    @Autowired
    @Lazy
    private Payment_acquirer_onboarding_wizardMapping payment_acquirer_onboarding_wizardMapping;




    @PreAuthorize("hasPermission('Remove',{#payment_acquirer_onboarding_wizard_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Payment_acquirer_onboarding_wizard" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/payment_acquirer_onboarding_wizards/{payment_acquirer_onboarding_wizard_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("payment_acquirer_onboarding_wizard_id") Integer payment_acquirer_onboarding_wizard_id) {
         return ResponseEntity.status(HttpStatus.OK).body(payment_acquirer_onboarding_wizardService.remove(payment_acquirer_onboarding_wizard_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Payment_acquirer_onboarding_wizard" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/payment_acquirer_onboarding_wizards/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        payment_acquirer_onboarding_wizardService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Payment_acquirer_onboarding_wizard" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/payment_acquirer_onboarding_wizards")

    public ResponseEntity<Payment_acquirer_onboarding_wizardDTO> create(@RequestBody Payment_acquirer_onboarding_wizardDTO payment_acquirer_onboarding_wizarddto) {
        Payment_acquirer_onboarding_wizard domain = payment_acquirer_onboarding_wizardMapping.toDomain(payment_acquirer_onboarding_wizarddto);
		payment_acquirer_onboarding_wizardService.create(domain);
        Payment_acquirer_onboarding_wizardDTO dto = payment_acquirer_onboarding_wizardMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Payment_acquirer_onboarding_wizard" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/payment_acquirer_onboarding_wizards/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Payment_acquirer_onboarding_wizardDTO> payment_acquirer_onboarding_wizarddtos) {
        payment_acquirer_onboarding_wizardService.createBatch(payment_acquirer_onboarding_wizardMapping.toDomain(payment_acquirer_onboarding_wizarddtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }













    @PreAuthorize("hasPermission(#payment_acquirer_onboarding_wizard_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Payment_acquirer_onboarding_wizard" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/payment_acquirer_onboarding_wizards/{payment_acquirer_onboarding_wizard_id}")
    public ResponseEntity<Payment_acquirer_onboarding_wizardDTO> get(@PathVariable("payment_acquirer_onboarding_wizard_id") Integer payment_acquirer_onboarding_wizard_id) {
        Payment_acquirer_onboarding_wizard domain = payment_acquirer_onboarding_wizardService.get(payment_acquirer_onboarding_wizard_id);
        Payment_acquirer_onboarding_wizardDTO dto = payment_acquirer_onboarding_wizardMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }




    @PreAuthorize("hasPermission(#payment_acquirer_onboarding_wizard_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Payment_acquirer_onboarding_wizard" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/payment_acquirer_onboarding_wizards/{payment_acquirer_onboarding_wizard_id}")

    public ResponseEntity<Payment_acquirer_onboarding_wizardDTO> update(@PathVariable("payment_acquirer_onboarding_wizard_id") Integer payment_acquirer_onboarding_wizard_id, @RequestBody Payment_acquirer_onboarding_wizardDTO payment_acquirer_onboarding_wizarddto) {
		Payment_acquirer_onboarding_wizard domain = payment_acquirer_onboarding_wizardMapping.toDomain(payment_acquirer_onboarding_wizarddto);
        domain.setId(payment_acquirer_onboarding_wizard_id);
		payment_acquirer_onboarding_wizardService.update(domain);
		Payment_acquirer_onboarding_wizardDTO dto = payment_acquirer_onboarding_wizardMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#payment_acquirer_onboarding_wizard_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Payment_acquirer_onboarding_wizard" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/payment_acquirer_onboarding_wizards/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Payment_acquirer_onboarding_wizardDTO> payment_acquirer_onboarding_wizarddtos) {
        payment_acquirer_onboarding_wizardService.updateBatch(payment_acquirer_onboarding_wizardMapping.toDomain(payment_acquirer_onboarding_wizarddtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Payment_acquirer_onboarding_wizard" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/payment_acquirer_onboarding_wizards/fetchdefault")
	public ResponseEntity<List<Payment_acquirer_onboarding_wizardDTO>> fetchDefault(Payment_acquirer_onboarding_wizardSearchContext context) {
        Page<Payment_acquirer_onboarding_wizard> domains = payment_acquirer_onboarding_wizardService.searchDefault(context) ;
        List<Payment_acquirer_onboarding_wizardDTO> list = payment_acquirer_onboarding_wizardMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Payment_acquirer_onboarding_wizard" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/payment_acquirer_onboarding_wizards/searchdefault")
	public ResponseEntity<Page<Payment_acquirer_onboarding_wizardDTO>> searchDefault(Payment_acquirer_onboarding_wizardSearchContext context) {
        Page<Payment_acquirer_onboarding_wizard> domains = payment_acquirer_onboarding_wizardService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(payment_acquirer_onboarding_wizardMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Payment_acquirer_onboarding_wizard getEntity(){
        return new Payment_acquirer_onboarding_wizard();
    }

}
