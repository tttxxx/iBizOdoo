package cn.ibizlab.odoo.odoo_payment.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import lombok.Data;

@ConfigurationProperties(prefix = "service.odoo-payment")
@Data
public class odoo_paymentServiceProperties {

	private boolean enabled;

	private boolean auth;


}