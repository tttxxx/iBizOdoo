package cn.ibizlab.odoo.odoo_sale.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_sale.dto.*;
import cn.ibizlab.odoo.odoo_sale.mapping.*;
import cn.ibizlab.odoo.core.odoo_sale.domain.Sale_payment_acquirer_onboarding_wizard;
import cn.ibizlab.odoo.core.odoo_sale.service.ISale_payment_acquirer_onboarding_wizardService;
import cn.ibizlab.odoo.core.odoo_sale.filter.Sale_payment_acquirer_onboarding_wizardSearchContext;




@Slf4j
@Api(tags = {"Sale_payment_acquirer_onboarding_wizard" })
@RestController("odoo_sale-sale_payment_acquirer_onboarding_wizard")
@RequestMapping("")
public class Sale_payment_acquirer_onboarding_wizardResource {

    @Autowired
    private ISale_payment_acquirer_onboarding_wizardService sale_payment_acquirer_onboarding_wizardService;

    @Autowired
    @Lazy
    private Sale_payment_acquirer_onboarding_wizardMapping sale_payment_acquirer_onboarding_wizardMapping;










    @PreAuthorize("hasPermission(#sale_payment_acquirer_onboarding_wizard_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Sale_payment_acquirer_onboarding_wizard" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/sale_payment_acquirer_onboarding_wizards/{sale_payment_acquirer_onboarding_wizard_id}")
    public ResponseEntity<Sale_payment_acquirer_onboarding_wizardDTO> get(@PathVariable("sale_payment_acquirer_onboarding_wizard_id") Integer sale_payment_acquirer_onboarding_wizard_id) {
        Sale_payment_acquirer_onboarding_wizard domain = sale_payment_acquirer_onboarding_wizardService.get(sale_payment_acquirer_onboarding_wizard_id);
        Sale_payment_acquirer_onboarding_wizardDTO dto = sale_payment_acquirer_onboarding_wizardMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }




    @PreAuthorize("hasPermission('Remove',{#sale_payment_acquirer_onboarding_wizard_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Sale_payment_acquirer_onboarding_wizard" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/sale_payment_acquirer_onboarding_wizards/{sale_payment_acquirer_onboarding_wizard_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("sale_payment_acquirer_onboarding_wizard_id") Integer sale_payment_acquirer_onboarding_wizard_id) {
         return ResponseEntity.status(HttpStatus.OK).body(sale_payment_acquirer_onboarding_wizardService.remove(sale_payment_acquirer_onboarding_wizard_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Sale_payment_acquirer_onboarding_wizard" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/sale_payment_acquirer_onboarding_wizards/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        sale_payment_acquirer_onboarding_wizardService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }







    @PreAuthorize("hasPermission(#sale_payment_acquirer_onboarding_wizard_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Sale_payment_acquirer_onboarding_wizard" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/sale_payment_acquirer_onboarding_wizards/{sale_payment_acquirer_onboarding_wizard_id}")

    public ResponseEntity<Sale_payment_acquirer_onboarding_wizardDTO> update(@PathVariable("sale_payment_acquirer_onboarding_wizard_id") Integer sale_payment_acquirer_onboarding_wizard_id, @RequestBody Sale_payment_acquirer_onboarding_wizardDTO sale_payment_acquirer_onboarding_wizarddto) {
		Sale_payment_acquirer_onboarding_wizard domain = sale_payment_acquirer_onboarding_wizardMapping.toDomain(sale_payment_acquirer_onboarding_wizarddto);
        domain.setId(sale_payment_acquirer_onboarding_wizard_id);
		sale_payment_acquirer_onboarding_wizardService.update(domain);
		Sale_payment_acquirer_onboarding_wizardDTO dto = sale_payment_acquirer_onboarding_wizardMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#sale_payment_acquirer_onboarding_wizard_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Sale_payment_acquirer_onboarding_wizard" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/sale_payment_acquirer_onboarding_wizards/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Sale_payment_acquirer_onboarding_wizardDTO> sale_payment_acquirer_onboarding_wizarddtos) {
        sale_payment_acquirer_onboarding_wizardService.updateBatch(sale_payment_acquirer_onboarding_wizardMapping.toDomain(sale_payment_acquirer_onboarding_wizarddtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Sale_payment_acquirer_onboarding_wizard" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/sale_payment_acquirer_onboarding_wizards")

    public ResponseEntity<Sale_payment_acquirer_onboarding_wizardDTO> create(@RequestBody Sale_payment_acquirer_onboarding_wizardDTO sale_payment_acquirer_onboarding_wizarddto) {
        Sale_payment_acquirer_onboarding_wizard domain = sale_payment_acquirer_onboarding_wizardMapping.toDomain(sale_payment_acquirer_onboarding_wizarddto);
		sale_payment_acquirer_onboarding_wizardService.create(domain);
        Sale_payment_acquirer_onboarding_wizardDTO dto = sale_payment_acquirer_onboarding_wizardMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Sale_payment_acquirer_onboarding_wizard" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/sale_payment_acquirer_onboarding_wizards/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Sale_payment_acquirer_onboarding_wizardDTO> sale_payment_acquirer_onboarding_wizarddtos) {
        sale_payment_acquirer_onboarding_wizardService.createBatch(sale_payment_acquirer_onboarding_wizardMapping.toDomain(sale_payment_acquirer_onboarding_wizarddtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Sale_payment_acquirer_onboarding_wizard" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/sale_payment_acquirer_onboarding_wizards/fetchdefault")
	public ResponseEntity<List<Sale_payment_acquirer_onboarding_wizardDTO>> fetchDefault(Sale_payment_acquirer_onboarding_wizardSearchContext context) {
        Page<Sale_payment_acquirer_onboarding_wizard> domains = sale_payment_acquirer_onboarding_wizardService.searchDefault(context) ;
        List<Sale_payment_acquirer_onboarding_wizardDTO> list = sale_payment_acquirer_onboarding_wizardMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Sale_payment_acquirer_onboarding_wizard" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/sale_payment_acquirer_onboarding_wizards/searchdefault")
	public ResponseEntity<Page<Sale_payment_acquirer_onboarding_wizardDTO>> searchDefault(Sale_payment_acquirer_onboarding_wizardSearchContext context) {
        Page<Sale_payment_acquirer_onboarding_wizard> domains = sale_payment_acquirer_onboarding_wizardService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(sale_payment_acquirer_onboarding_wizardMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Sale_payment_acquirer_onboarding_wizard getEntity(){
        return new Sale_payment_acquirer_onboarding_wizard();
    }

}
