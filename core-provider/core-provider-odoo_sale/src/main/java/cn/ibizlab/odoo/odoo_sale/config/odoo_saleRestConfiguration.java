package cn.ibizlab.odoo.odoo_sale.config;

import org.springframework.context.annotation.ComponentScan;

@ComponentScan("cn.ibizlab.odoo.odoo_sale")
public class odoo_saleRestConfiguration {

}
