package cn.ibizlab.odoo.odoo_sale.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_sale.dto.*;
import cn.ibizlab.odoo.odoo_sale.mapping.*;
import cn.ibizlab.odoo.core.odoo_sale.domain.Sale_product_configurator;
import cn.ibizlab.odoo.core.odoo_sale.service.ISale_product_configuratorService;
import cn.ibizlab.odoo.core.odoo_sale.filter.Sale_product_configuratorSearchContext;




@Slf4j
@Api(tags = {"Sale_product_configurator" })
@RestController("odoo_sale-sale_product_configurator")
@RequestMapping("")
public class Sale_product_configuratorResource {

    @Autowired
    private ISale_product_configuratorService sale_product_configuratorService;

    @Autowired
    @Lazy
    private Sale_product_configuratorMapping sale_product_configuratorMapping;




    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Sale_product_configurator" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/sale_product_configurators")

    public ResponseEntity<Sale_product_configuratorDTO> create(@RequestBody Sale_product_configuratorDTO sale_product_configuratordto) {
        Sale_product_configurator domain = sale_product_configuratorMapping.toDomain(sale_product_configuratordto);
		sale_product_configuratorService.create(domain);
        Sale_product_configuratorDTO dto = sale_product_configuratorMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Sale_product_configurator" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/sale_product_configurators/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Sale_product_configuratorDTO> sale_product_configuratordtos) {
        sale_product_configuratorService.createBatch(sale_product_configuratorMapping.toDomain(sale_product_configuratordtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission('Remove',{#sale_product_configurator_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Sale_product_configurator" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/sale_product_configurators/{sale_product_configurator_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("sale_product_configurator_id") Integer sale_product_configurator_id) {
         return ResponseEntity.status(HttpStatus.OK).body(sale_product_configuratorService.remove(sale_product_configurator_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Sale_product_configurator" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/sale_product_configurators/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        sale_product_configuratorService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission(#sale_product_configurator_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Sale_product_configurator" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/sale_product_configurators/{sale_product_configurator_id}")
    public ResponseEntity<Sale_product_configuratorDTO> get(@PathVariable("sale_product_configurator_id") Integer sale_product_configurator_id) {
        Sale_product_configurator domain = sale_product_configuratorService.get(sale_product_configurator_id);
        Sale_product_configuratorDTO dto = sale_product_configuratorMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }




    @PreAuthorize("hasPermission(#sale_product_configurator_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Sale_product_configurator" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/sale_product_configurators/{sale_product_configurator_id}")

    public ResponseEntity<Sale_product_configuratorDTO> update(@PathVariable("sale_product_configurator_id") Integer sale_product_configurator_id, @RequestBody Sale_product_configuratorDTO sale_product_configuratordto) {
		Sale_product_configurator domain = sale_product_configuratorMapping.toDomain(sale_product_configuratordto);
        domain.setId(sale_product_configurator_id);
		sale_product_configuratorService.update(domain);
		Sale_product_configuratorDTO dto = sale_product_configuratorMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#sale_product_configurator_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Sale_product_configurator" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/sale_product_configurators/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Sale_product_configuratorDTO> sale_product_configuratordtos) {
        sale_product_configuratorService.updateBatch(sale_product_configuratorMapping.toDomain(sale_product_configuratordtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }










    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Sale_product_configurator" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/sale_product_configurators/fetchdefault")
	public ResponseEntity<List<Sale_product_configuratorDTO>> fetchDefault(Sale_product_configuratorSearchContext context) {
        Page<Sale_product_configurator> domains = sale_product_configuratorService.searchDefault(context) ;
        List<Sale_product_configuratorDTO> list = sale_product_configuratorMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Sale_product_configurator" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/sale_product_configurators/searchdefault")
	public ResponseEntity<Page<Sale_product_configuratorDTO>> searchDefault(Sale_product_configuratorSearchContext context) {
        Page<Sale_product_configurator> domains = sale_product_configuratorService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(sale_product_configuratorMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Sale_product_configurator getEntity(){
        return new Sale_product_configurator();
    }

}
