package cn.ibizlab.odoo.odoo_sale.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_sale.dto.*;
import cn.ibizlab.odoo.odoo_sale.mapping.*;
import cn.ibizlab.odoo.core.odoo_sale.domain.Sale_report;
import cn.ibizlab.odoo.core.odoo_sale.service.ISale_reportService;
import cn.ibizlab.odoo.core.odoo_sale.filter.Sale_reportSearchContext;




@Slf4j
@Api(tags = {"Sale_report" })
@RestController("odoo_sale-sale_report")
@RequestMapping("")
public class Sale_reportResource {

    @Autowired
    private ISale_reportService sale_reportService;

    @Autowired
    @Lazy
    private Sale_reportMapping sale_reportMapping;







    @PreAuthorize("hasPermission(#sale_report_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Sale_report" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/sale_reports/{sale_report_id}")
    public ResponseEntity<Sale_reportDTO> get(@PathVariable("sale_report_id") Integer sale_report_id) {
        Sale_report domain = sale_reportService.get(sale_report_id);
        Sale_reportDTO dto = sale_reportMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }




    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Sale_report" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/sale_reports")

    public ResponseEntity<Sale_reportDTO> create(@RequestBody Sale_reportDTO sale_reportdto) {
        Sale_report domain = sale_reportMapping.toDomain(sale_reportdto);
		sale_reportService.create(domain);
        Sale_reportDTO dto = sale_reportMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Sale_report" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/sale_reports/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Sale_reportDTO> sale_reportdtos) {
        sale_reportService.createBatch(sale_reportMapping.toDomain(sale_reportdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission(#sale_report_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Sale_report" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/sale_reports/{sale_report_id}")

    public ResponseEntity<Sale_reportDTO> update(@PathVariable("sale_report_id") Integer sale_report_id, @RequestBody Sale_reportDTO sale_reportdto) {
		Sale_report domain = sale_reportMapping.toDomain(sale_reportdto);
        domain.setId(sale_report_id);
		sale_reportService.update(domain);
		Sale_reportDTO dto = sale_reportMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#sale_report_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Sale_report" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/sale_reports/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Sale_reportDTO> sale_reportdtos) {
        sale_reportService.updateBatch(sale_reportMapping.toDomain(sale_reportdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission('Remove',{#sale_report_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Sale_report" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/sale_reports/{sale_report_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("sale_report_id") Integer sale_report_id) {
         return ResponseEntity.status(HttpStatus.OK).body(sale_reportService.remove(sale_report_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Sale_report" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/sale_reports/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        sale_reportService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }







    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Sale_report" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/sale_reports/fetchdefault")
	public ResponseEntity<List<Sale_reportDTO>> fetchDefault(Sale_reportSearchContext context) {
        Page<Sale_report> domains = sale_reportService.searchDefault(context) ;
        List<Sale_reportDTO> list = sale_reportMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Sale_report" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/sale_reports/searchdefault")
	public ResponseEntity<Page<Sale_reportDTO>> searchDefault(Sale_reportSearchContext context) {
        Page<Sale_report> domains = sale_reportService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(sale_reportMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Sale_report getEntity(){
        return new Sale_report();
    }

}
