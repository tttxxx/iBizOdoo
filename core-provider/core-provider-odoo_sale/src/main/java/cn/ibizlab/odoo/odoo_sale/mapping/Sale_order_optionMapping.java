package cn.ibizlab.odoo.odoo_sale.mapping;

import org.mapstruct.*;
import cn.ibizlab.odoo.core.odoo_sale.domain.Sale_order_option;
import cn.ibizlab.odoo.odoo_sale.dto.Sale_order_optionDTO;
import cn.ibizlab.odoo.util.domain.MappingBase;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring", uses = {},
    nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
    nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface Sale_order_optionMapping extends MappingBase<Sale_order_optionDTO, Sale_order_option> {


}

