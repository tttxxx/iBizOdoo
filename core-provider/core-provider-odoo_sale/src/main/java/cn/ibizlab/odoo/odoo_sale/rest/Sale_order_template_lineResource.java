package cn.ibizlab.odoo.odoo_sale.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_sale.dto.*;
import cn.ibizlab.odoo.odoo_sale.mapping.*;
import cn.ibizlab.odoo.core.odoo_sale.domain.Sale_order_template_line;
import cn.ibizlab.odoo.core.odoo_sale.service.ISale_order_template_lineService;
import cn.ibizlab.odoo.core.odoo_sale.filter.Sale_order_template_lineSearchContext;




@Slf4j
@Api(tags = {"Sale_order_template_line" })
@RestController("odoo_sale-sale_order_template_line")
@RequestMapping("")
public class Sale_order_template_lineResource {

    @Autowired
    private ISale_order_template_lineService sale_order_template_lineService;

    @Autowired
    @Lazy
    private Sale_order_template_lineMapping sale_order_template_lineMapping;




    @PreAuthorize("hasPermission('Remove',{#sale_order_template_line_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Sale_order_template_line" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/sale_order_template_lines/{sale_order_template_line_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("sale_order_template_line_id") Integer sale_order_template_line_id) {
         return ResponseEntity.status(HttpStatus.OK).body(sale_order_template_lineService.remove(sale_order_template_line_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Sale_order_template_line" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/sale_order_template_lines/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        sale_order_template_lineService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }










    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Sale_order_template_line" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/sale_order_template_lines")

    public ResponseEntity<Sale_order_template_lineDTO> create(@RequestBody Sale_order_template_lineDTO sale_order_template_linedto) {
        Sale_order_template_line domain = sale_order_template_lineMapping.toDomain(sale_order_template_linedto);
		sale_order_template_lineService.create(domain);
        Sale_order_template_lineDTO dto = sale_order_template_lineMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Sale_order_template_line" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/sale_order_template_lines/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Sale_order_template_lineDTO> sale_order_template_linedtos) {
        sale_order_template_lineService.createBatch(sale_order_template_lineMapping.toDomain(sale_order_template_linedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission(#sale_order_template_line_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Sale_order_template_line" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/sale_order_template_lines/{sale_order_template_line_id}")
    public ResponseEntity<Sale_order_template_lineDTO> get(@PathVariable("sale_order_template_line_id") Integer sale_order_template_line_id) {
        Sale_order_template_line domain = sale_order_template_lineService.get(sale_order_template_line_id);
        Sale_order_template_lineDTO dto = sale_order_template_lineMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }




    @PreAuthorize("hasPermission(#sale_order_template_line_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Sale_order_template_line" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/sale_order_template_lines/{sale_order_template_line_id}")

    public ResponseEntity<Sale_order_template_lineDTO> update(@PathVariable("sale_order_template_line_id") Integer sale_order_template_line_id, @RequestBody Sale_order_template_lineDTO sale_order_template_linedto) {
		Sale_order_template_line domain = sale_order_template_lineMapping.toDomain(sale_order_template_linedto);
        domain.setId(sale_order_template_line_id);
		sale_order_template_lineService.update(domain);
		Sale_order_template_lineDTO dto = sale_order_template_lineMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#sale_order_template_line_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Sale_order_template_line" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/sale_order_template_lines/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Sale_order_template_lineDTO> sale_order_template_linedtos) {
        sale_order_template_lineService.updateBatch(sale_order_template_lineMapping.toDomain(sale_order_template_linedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Sale_order_template_line" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/sale_order_template_lines/fetchdefault")
	public ResponseEntity<List<Sale_order_template_lineDTO>> fetchDefault(Sale_order_template_lineSearchContext context) {
        Page<Sale_order_template_line> domains = sale_order_template_lineService.searchDefault(context) ;
        List<Sale_order_template_lineDTO> list = sale_order_template_lineMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Sale_order_template_line" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/sale_order_template_lines/searchdefault")
	public ResponseEntity<Page<Sale_order_template_lineDTO>> searchDefault(Sale_order_template_lineSearchContext context) {
        Page<Sale_order_template_line> domains = sale_order_template_lineService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(sale_order_template_lineMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Sale_order_template_line getEntity(){
        return new Sale_order_template_line();
    }

}
