package cn.ibizlab.odoo.odoo_bus.mapping;

import org.mapstruct.*;
import cn.ibizlab.odoo.core.odoo_bus.domain.Bus_presence;
import cn.ibizlab.odoo.odoo_bus.dto.Bus_presenceDTO;
import cn.ibizlab.odoo.util.domain.MappingBase;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring", uses = {},
    nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
    nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface Bus_presenceMapping extends MappingBase<Bus_presenceDTO, Bus_presence> {


}

