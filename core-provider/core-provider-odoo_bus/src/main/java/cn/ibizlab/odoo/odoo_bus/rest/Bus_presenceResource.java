package cn.ibizlab.odoo.odoo_bus.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_bus.dto.*;
import cn.ibizlab.odoo.odoo_bus.mapping.*;
import cn.ibizlab.odoo.core.odoo_bus.domain.Bus_presence;
import cn.ibizlab.odoo.core.odoo_bus.service.IBus_presenceService;
import cn.ibizlab.odoo.core.odoo_bus.filter.Bus_presenceSearchContext;




@Slf4j
@Api(tags = {"Bus_presence" })
@RestController("odoo_bus-bus_presence")
@RequestMapping("")
public class Bus_presenceResource {

    @Autowired
    private IBus_presenceService bus_presenceService;

    @Autowired
    @Lazy
    private Bus_presenceMapping bus_presenceMapping;




    @PreAuthorize("hasPermission(#bus_presence_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Bus_presence" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/bus_presences/{bus_presence_id}")
    public ResponseEntity<Bus_presenceDTO> get(@PathVariable("bus_presence_id") Integer bus_presence_id) {
        Bus_presence domain = bus_presenceService.get(bus_presence_id);
        Bus_presenceDTO dto = bus_presenceMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }







    @PreAuthorize("hasPermission('Remove',{#bus_presence_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Bus_presence" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/bus_presences/{bus_presence_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("bus_presence_id") Integer bus_presence_id) {
         return ResponseEntity.status(HttpStatus.OK).body(bus_presenceService.remove(bus_presence_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Bus_presence" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/bus_presences/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        bus_presenceService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }










    @PreAuthorize("hasPermission(#bus_presence_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Bus_presence" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/bus_presences/{bus_presence_id}")

    public ResponseEntity<Bus_presenceDTO> update(@PathVariable("bus_presence_id") Integer bus_presence_id, @RequestBody Bus_presenceDTO bus_presencedto) {
		Bus_presence domain = bus_presenceMapping.toDomain(bus_presencedto);
        domain.setId(bus_presence_id);
		bus_presenceService.update(domain);
		Bus_presenceDTO dto = bus_presenceMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#bus_presence_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Bus_presence" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/bus_presences/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Bus_presenceDTO> bus_presencedtos) {
        bus_presenceService.updateBatch(bus_presenceMapping.toDomain(bus_presencedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Bus_presence" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/bus_presences")

    public ResponseEntity<Bus_presenceDTO> create(@RequestBody Bus_presenceDTO bus_presencedto) {
        Bus_presence domain = bus_presenceMapping.toDomain(bus_presencedto);
		bus_presenceService.create(domain);
        Bus_presenceDTO dto = bus_presenceMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Bus_presence" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/bus_presences/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Bus_presenceDTO> bus_presencedtos) {
        bus_presenceService.createBatch(bus_presenceMapping.toDomain(bus_presencedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Bus_presence" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/bus_presences/fetchdefault")
	public ResponseEntity<List<Bus_presenceDTO>> fetchDefault(Bus_presenceSearchContext context) {
        Page<Bus_presence> domains = bus_presenceService.searchDefault(context) ;
        List<Bus_presenceDTO> list = bus_presenceMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Bus_presence" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/bus_presences/searchdefault")
	public ResponseEntity<Page<Bus_presenceDTO>> searchDefault(Bus_presenceSearchContext context) {
        Page<Bus_presence> domains = bus_presenceService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(bus_presenceMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Bus_presence getEntity(){
        return new Bus_presence();
    }

}
