package cn.ibizlab.odoo.odoo_website.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_website.dto.*;
import cn.ibizlab.odoo.odoo_website.mapping.*;
import cn.ibizlab.odoo.core.odoo_website.domain.Website_menu;
import cn.ibizlab.odoo.core.odoo_website.service.IWebsite_menuService;
import cn.ibizlab.odoo.core.odoo_website.filter.Website_menuSearchContext;




@Slf4j
@Api(tags = {"Website_menu" })
@RestController("odoo_website-website_menu")
@RequestMapping("")
public class Website_menuResource {

    @Autowired
    private IWebsite_menuService website_menuService;

    @Autowired
    @Lazy
    private Website_menuMapping website_menuMapping;




    @PreAuthorize("hasPermission(#website_menu_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Website_menu" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/website_menus/{website_menu_id}")

    public ResponseEntity<Website_menuDTO> update(@PathVariable("website_menu_id") Integer website_menu_id, @RequestBody Website_menuDTO website_menudto) {
		Website_menu domain = website_menuMapping.toDomain(website_menudto);
        domain.setId(website_menu_id);
		website_menuService.update(domain);
		Website_menuDTO dto = website_menuMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#website_menu_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Website_menu" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/website_menus/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Website_menuDTO> website_menudtos) {
        website_menuService.updateBatch(website_menuMapping.toDomain(website_menudtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission(#website_menu_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Website_menu" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/website_menus/{website_menu_id}")
    public ResponseEntity<Website_menuDTO> get(@PathVariable("website_menu_id") Integer website_menu_id) {
        Website_menu domain = website_menuService.get(website_menu_id);
        Website_menuDTO dto = website_menuMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }




    @PreAuthorize("hasPermission('Remove',{#website_menu_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Website_menu" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/website_menus/{website_menu_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("website_menu_id") Integer website_menu_id) {
         return ResponseEntity.status(HttpStatus.OK).body(website_menuService.remove(website_menu_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Website_menu" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/website_menus/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        website_menuService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }













    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Website_menu" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/website_menus")

    public ResponseEntity<Website_menuDTO> create(@RequestBody Website_menuDTO website_menudto) {
        Website_menu domain = website_menuMapping.toDomain(website_menudto);
		website_menuService.create(domain);
        Website_menuDTO dto = website_menuMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Website_menu" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/website_menus/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Website_menuDTO> website_menudtos) {
        website_menuService.createBatch(website_menuMapping.toDomain(website_menudtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Website_menu" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/website_menus/fetchdefault")
	public ResponseEntity<List<Website_menuDTO>> fetchDefault(Website_menuSearchContext context) {
        Page<Website_menu> domains = website_menuService.searchDefault(context) ;
        List<Website_menuDTO> list = website_menuMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Website_menu" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/website_menus/searchdefault")
	public ResponseEntity<Page<Website_menuDTO>> searchDefault(Website_menuSearchContext context) {
        Page<Website_menu> domains = website_menuService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(website_menuMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Website_menu getEntity(){
        return new Website_menu();
    }

}
