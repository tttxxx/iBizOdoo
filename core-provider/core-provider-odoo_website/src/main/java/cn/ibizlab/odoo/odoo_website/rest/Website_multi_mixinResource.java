package cn.ibizlab.odoo.odoo_website.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_website.dto.*;
import cn.ibizlab.odoo.odoo_website.mapping.*;
import cn.ibizlab.odoo.core.odoo_website.domain.Website_multi_mixin;
import cn.ibizlab.odoo.core.odoo_website.service.IWebsite_multi_mixinService;
import cn.ibizlab.odoo.core.odoo_website.filter.Website_multi_mixinSearchContext;




@Slf4j
@Api(tags = {"Website_multi_mixin" })
@RestController("odoo_website-website_multi_mixin")
@RequestMapping("")
public class Website_multi_mixinResource {

    @Autowired
    private IWebsite_multi_mixinService website_multi_mixinService;

    @Autowired
    @Lazy
    private Website_multi_mixinMapping website_multi_mixinMapping;







    @PreAuthorize("hasPermission(#website_multi_mixin_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Website_multi_mixin" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/website_multi_mixins/{website_multi_mixin_id}")

    public ResponseEntity<Website_multi_mixinDTO> update(@PathVariable("website_multi_mixin_id") Integer website_multi_mixin_id, @RequestBody Website_multi_mixinDTO website_multi_mixindto) {
		Website_multi_mixin domain = website_multi_mixinMapping.toDomain(website_multi_mixindto);
        domain.setId(website_multi_mixin_id);
		website_multi_mixinService.update(domain);
		Website_multi_mixinDTO dto = website_multi_mixinMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#website_multi_mixin_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Website_multi_mixin" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/website_multi_mixins/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Website_multi_mixinDTO> website_multi_mixindtos) {
        website_multi_mixinService.updateBatch(website_multi_mixinMapping.toDomain(website_multi_mixindtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission(#website_multi_mixin_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Website_multi_mixin" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/website_multi_mixins/{website_multi_mixin_id}")
    public ResponseEntity<Website_multi_mixinDTO> get(@PathVariable("website_multi_mixin_id") Integer website_multi_mixin_id) {
        Website_multi_mixin domain = website_multi_mixinService.get(website_multi_mixin_id);
        Website_multi_mixinDTO dto = website_multi_mixinMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }







    @PreAuthorize("hasPermission('Remove',{#website_multi_mixin_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Website_multi_mixin" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/website_multi_mixins/{website_multi_mixin_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("website_multi_mixin_id") Integer website_multi_mixin_id) {
         return ResponseEntity.status(HttpStatus.OK).body(website_multi_mixinService.remove(website_multi_mixin_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Website_multi_mixin" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/website_multi_mixins/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        website_multi_mixinService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }







    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Website_multi_mixin" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/website_multi_mixins")

    public ResponseEntity<Website_multi_mixinDTO> create(@RequestBody Website_multi_mixinDTO website_multi_mixindto) {
        Website_multi_mixin domain = website_multi_mixinMapping.toDomain(website_multi_mixindto);
		website_multi_mixinService.create(domain);
        Website_multi_mixinDTO dto = website_multi_mixinMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Website_multi_mixin" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/website_multi_mixins/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Website_multi_mixinDTO> website_multi_mixindtos) {
        website_multi_mixinService.createBatch(website_multi_mixinMapping.toDomain(website_multi_mixindtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Website_multi_mixin" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/website_multi_mixins/fetchdefault")
	public ResponseEntity<List<Website_multi_mixinDTO>> fetchDefault(Website_multi_mixinSearchContext context) {
        Page<Website_multi_mixin> domains = website_multi_mixinService.searchDefault(context) ;
        List<Website_multi_mixinDTO> list = website_multi_mixinMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Website_multi_mixin" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/website_multi_mixins/searchdefault")
	public ResponseEntity<Page<Website_multi_mixinDTO>> searchDefault(Website_multi_mixinSearchContext context) {
        Page<Website_multi_mixin> domains = website_multi_mixinService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(website_multi_mixinMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Website_multi_mixin getEntity(){
        return new Website_multi_mixin();
    }

}
