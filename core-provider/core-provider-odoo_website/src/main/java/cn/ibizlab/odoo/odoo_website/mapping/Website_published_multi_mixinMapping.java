package cn.ibizlab.odoo.odoo_website.mapping;

import org.mapstruct.*;
import cn.ibizlab.odoo.core.odoo_website.domain.Website_published_multi_mixin;
import cn.ibizlab.odoo.odoo_website.dto.Website_published_multi_mixinDTO;
import cn.ibizlab.odoo.util.domain.MappingBase;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring", uses = {},
    nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
    nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface Website_published_multi_mixinMapping extends MappingBase<Website_published_multi_mixinDTO, Website_published_multi_mixin> {


}

