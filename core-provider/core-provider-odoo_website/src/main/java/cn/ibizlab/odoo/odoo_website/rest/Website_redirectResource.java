package cn.ibizlab.odoo.odoo_website.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_website.dto.*;
import cn.ibizlab.odoo.odoo_website.mapping.*;
import cn.ibizlab.odoo.core.odoo_website.domain.Website_redirect;
import cn.ibizlab.odoo.core.odoo_website.service.IWebsite_redirectService;
import cn.ibizlab.odoo.core.odoo_website.filter.Website_redirectSearchContext;




@Slf4j
@Api(tags = {"Website_redirect" })
@RestController("odoo_website-website_redirect")
@RequestMapping("")
public class Website_redirectResource {

    @Autowired
    private IWebsite_redirectService website_redirectService;

    @Autowired
    @Lazy
    private Website_redirectMapping website_redirectMapping;




    @PreAuthorize("hasPermission(#website_redirect_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Website_redirect" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/website_redirects/{website_redirect_id}")

    public ResponseEntity<Website_redirectDTO> update(@PathVariable("website_redirect_id") Integer website_redirect_id, @RequestBody Website_redirectDTO website_redirectdto) {
		Website_redirect domain = website_redirectMapping.toDomain(website_redirectdto);
        domain.setId(website_redirect_id);
		website_redirectService.update(domain);
		Website_redirectDTO dto = website_redirectMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#website_redirect_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Website_redirect" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/website_redirects/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Website_redirectDTO> website_redirectdtos) {
        website_redirectService.updateBatch(website_redirectMapping.toDomain(website_redirectdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }







    @PreAuthorize("hasPermission(#website_redirect_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Website_redirect" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/website_redirects/{website_redirect_id}")
    public ResponseEntity<Website_redirectDTO> get(@PathVariable("website_redirect_id") Integer website_redirect_id) {
        Website_redirect domain = website_redirectService.get(website_redirect_id);
        Website_redirectDTO dto = website_redirectMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }







    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Website_redirect" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/website_redirects")

    public ResponseEntity<Website_redirectDTO> create(@RequestBody Website_redirectDTO website_redirectdto) {
        Website_redirect domain = website_redirectMapping.toDomain(website_redirectdto);
		website_redirectService.create(domain);
        Website_redirectDTO dto = website_redirectMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Website_redirect" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/website_redirects/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Website_redirectDTO> website_redirectdtos) {
        website_redirectService.createBatch(website_redirectMapping.toDomain(website_redirectdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission('Remove',{#website_redirect_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Website_redirect" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/website_redirects/{website_redirect_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("website_redirect_id") Integer website_redirect_id) {
         return ResponseEntity.status(HttpStatus.OK).body(website_redirectService.remove(website_redirect_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Website_redirect" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/website_redirects/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        website_redirectService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Website_redirect" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/website_redirects/fetchdefault")
	public ResponseEntity<List<Website_redirectDTO>> fetchDefault(Website_redirectSearchContext context) {
        Page<Website_redirect> domains = website_redirectService.searchDefault(context) ;
        List<Website_redirectDTO> list = website_redirectMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Website_redirect" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/website_redirects/searchdefault")
	public ResponseEntity<Page<Website_redirectDTO>> searchDefault(Website_redirectSearchContext context) {
        Page<Website_redirect> domains = website_redirectService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(website_redirectMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Website_redirect getEntity(){
        return new Website_redirect();
    }

}
