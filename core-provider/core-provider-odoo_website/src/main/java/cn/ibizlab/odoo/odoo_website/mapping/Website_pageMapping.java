package cn.ibizlab.odoo.odoo_website.mapping;

import org.mapstruct.*;
import cn.ibizlab.odoo.core.odoo_website.domain.Website_page;
import cn.ibizlab.odoo.odoo_website.dto.Website_pageDTO;
import cn.ibizlab.odoo.util.domain.MappingBase;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring", uses = {},
    nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
    nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface Website_pageMapping extends MappingBase<Website_pageDTO, Website_page> {


}

