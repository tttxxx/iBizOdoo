package cn.ibizlab.odoo.odoo_website.mapping;

import org.mapstruct.*;
import cn.ibizlab.odoo.core.odoo_website.domain.Website_menu;
import cn.ibizlab.odoo.odoo_website.dto.Website_menuDTO;
import cn.ibizlab.odoo.util.domain.MappingBase;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring", uses = {},
    nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
    nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface Website_menuMapping extends MappingBase<Website_menuDTO, Website_menu> {


}

