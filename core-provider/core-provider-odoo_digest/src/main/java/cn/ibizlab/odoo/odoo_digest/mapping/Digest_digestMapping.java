package cn.ibizlab.odoo.odoo_digest.mapping;

import org.mapstruct.*;
import cn.ibizlab.odoo.core.odoo_digest.domain.Digest_digest;
import cn.ibizlab.odoo.odoo_digest.dto.Digest_digestDTO;
import cn.ibizlab.odoo.util.domain.MappingBase;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring", uses = {},
    nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
    nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface Digest_digestMapping extends MappingBase<Digest_digestDTO, Digest_digest> {


}

