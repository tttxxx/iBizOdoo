package cn.ibizlab.odoo.odoo_account.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_account.dto.*;
import cn.ibizlab.odoo.odoo_account.mapping.*;
import cn.ibizlab.odoo.core.odoo_account.domain.Account_unreconcile;
import cn.ibizlab.odoo.core.odoo_account.service.IAccount_unreconcileService;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_unreconcileSearchContext;




@Slf4j
@Api(tags = {"Account_unreconcile" })
@RestController("odoo_account-account_unreconcile")
@RequestMapping("")
public class Account_unreconcileResource {

    @Autowired
    private IAccount_unreconcileService account_unreconcileService;

    @Autowired
    @Lazy
    private Account_unreconcileMapping account_unreconcileMapping;




    @PreAuthorize("hasPermission('Remove',{#account_unreconcile_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Account_unreconcile" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/account_unreconciles/{account_unreconcile_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("account_unreconcile_id") Integer account_unreconcile_id) {
         return ResponseEntity.status(HttpStatus.OK).body(account_unreconcileService.remove(account_unreconcile_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Account_unreconcile" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/account_unreconciles/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        account_unreconcileService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }







    @PreAuthorize("hasPermission(#account_unreconcile_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Account_unreconcile" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/account_unreconciles/{account_unreconcile_id}")
    public ResponseEntity<Account_unreconcileDTO> get(@PathVariable("account_unreconcile_id") Integer account_unreconcile_id) {
        Account_unreconcile domain = account_unreconcileService.get(account_unreconcile_id);
        Account_unreconcileDTO dto = account_unreconcileMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }




    @PreAuthorize("hasPermission(#account_unreconcile_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Account_unreconcile" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/account_unreconciles/{account_unreconcile_id}")

    public ResponseEntity<Account_unreconcileDTO> update(@PathVariable("account_unreconcile_id") Integer account_unreconcile_id, @RequestBody Account_unreconcileDTO account_unreconciledto) {
		Account_unreconcile domain = account_unreconcileMapping.toDomain(account_unreconciledto);
        domain.setId(account_unreconcile_id);
		account_unreconcileService.update(domain);
		Account_unreconcileDTO dto = account_unreconcileMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#account_unreconcile_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Account_unreconcile" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/account_unreconciles/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Account_unreconcileDTO> account_unreconciledtos) {
        account_unreconcileService.updateBatch(account_unreconcileMapping.toDomain(account_unreconciledtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Account_unreconcile" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/account_unreconciles")

    public ResponseEntity<Account_unreconcileDTO> create(@RequestBody Account_unreconcileDTO account_unreconciledto) {
        Account_unreconcile domain = account_unreconcileMapping.toDomain(account_unreconciledto);
		account_unreconcileService.create(domain);
        Account_unreconcileDTO dto = account_unreconcileMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Account_unreconcile" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/account_unreconciles/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Account_unreconcileDTO> account_unreconciledtos) {
        account_unreconcileService.createBatch(account_unreconcileMapping.toDomain(account_unreconciledtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }







    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Account_unreconcile" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/account_unreconciles/fetchdefault")
	public ResponseEntity<List<Account_unreconcileDTO>> fetchDefault(Account_unreconcileSearchContext context) {
        Page<Account_unreconcile> domains = account_unreconcileService.searchDefault(context) ;
        List<Account_unreconcileDTO> list = account_unreconcileMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Account_unreconcile" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/account_unreconciles/searchdefault")
	public ResponseEntity<Page<Account_unreconcileDTO>> searchDefault(Account_unreconcileSearchContext context) {
        Page<Account_unreconcile> domains = account_unreconcileService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(account_unreconcileMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Account_unreconcile getEntity(){
        return new Account_unreconcile();
    }

}
