package cn.ibizlab.odoo.odoo_account.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_account.dto.*;
import cn.ibizlab.odoo.odoo_account.mapping.*;
import cn.ibizlab.odoo.core.odoo_account.domain.Account_analytic_line;
import cn.ibizlab.odoo.core.odoo_account.service.IAccount_analytic_lineService;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_analytic_lineSearchContext;




@Slf4j
@Api(tags = {"Account_analytic_line" })
@RestController("odoo_account-account_analytic_line")
@RequestMapping("")
public class Account_analytic_lineResource {

    @Autowired
    private IAccount_analytic_lineService account_analytic_lineService;

    @Autowired
    @Lazy
    private Account_analytic_lineMapping account_analytic_lineMapping;







    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Account_analytic_line" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/account_analytic_lines")

    public ResponseEntity<Account_analytic_lineDTO> create(@RequestBody Account_analytic_lineDTO account_analytic_linedto) {
        Account_analytic_line domain = account_analytic_lineMapping.toDomain(account_analytic_linedto);
		account_analytic_lineService.create(domain);
        Account_analytic_lineDTO dto = account_analytic_lineMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Account_analytic_line" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/account_analytic_lines/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Account_analytic_lineDTO> account_analytic_linedtos) {
        account_analytic_lineService.createBatch(account_analytic_lineMapping.toDomain(account_analytic_linedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission(#account_analytic_line_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Account_analytic_line" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/account_analytic_lines/{account_analytic_line_id}")
    public ResponseEntity<Account_analytic_lineDTO> get(@PathVariable("account_analytic_line_id") Integer account_analytic_line_id) {
        Account_analytic_line domain = account_analytic_lineService.get(account_analytic_line_id);
        Account_analytic_lineDTO dto = account_analytic_lineMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }










    @PreAuthorize("hasPermission('Remove',{#account_analytic_line_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Account_analytic_line" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/account_analytic_lines/{account_analytic_line_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("account_analytic_line_id") Integer account_analytic_line_id) {
         return ResponseEntity.status(HttpStatus.OK).body(account_analytic_lineService.remove(account_analytic_line_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Account_analytic_line" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/account_analytic_lines/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        account_analytic_lineService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission(#account_analytic_line_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Account_analytic_line" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/account_analytic_lines/{account_analytic_line_id}")

    public ResponseEntity<Account_analytic_lineDTO> update(@PathVariable("account_analytic_line_id") Integer account_analytic_line_id, @RequestBody Account_analytic_lineDTO account_analytic_linedto) {
		Account_analytic_line domain = account_analytic_lineMapping.toDomain(account_analytic_linedto);
        domain.setId(account_analytic_line_id);
		account_analytic_lineService.update(domain);
		Account_analytic_lineDTO dto = account_analytic_lineMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#account_analytic_line_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Account_analytic_line" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/account_analytic_lines/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Account_analytic_lineDTO> account_analytic_linedtos) {
        account_analytic_lineService.updateBatch(account_analytic_lineMapping.toDomain(account_analytic_linedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Account_analytic_line" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/account_analytic_lines/fetchdefault")
	public ResponseEntity<List<Account_analytic_lineDTO>> fetchDefault(Account_analytic_lineSearchContext context) {
        Page<Account_analytic_line> domains = account_analytic_lineService.searchDefault(context) ;
        List<Account_analytic_lineDTO> list = account_analytic_lineMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Account_analytic_line" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/account_analytic_lines/searchdefault")
	public ResponseEntity<Page<Account_analytic_lineDTO>> searchDefault(Account_analytic_lineSearchContext context) {
        Page<Account_analytic_line> domains = account_analytic_lineService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(account_analytic_lineMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Account_analytic_line getEntity(){
        return new Account_analytic_line();
    }

}
