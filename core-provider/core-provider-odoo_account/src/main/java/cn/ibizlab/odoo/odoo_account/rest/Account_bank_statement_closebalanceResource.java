package cn.ibizlab.odoo.odoo_account.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_account.dto.*;
import cn.ibizlab.odoo.odoo_account.mapping.*;
import cn.ibizlab.odoo.core.odoo_account.domain.Account_bank_statement_closebalance;
import cn.ibizlab.odoo.core.odoo_account.service.IAccount_bank_statement_closebalanceService;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_bank_statement_closebalanceSearchContext;




@Slf4j
@Api(tags = {"Account_bank_statement_closebalance" })
@RestController("odoo_account-account_bank_statement_closebalance")
@RequestMapping("")
public class Account_bank_statement_closebalanceResource {

    @Autowired
    private IAccount_bank_statement_closebalanceService account_bank_statement_closebalanceService;

    @Autowired
    @Lazy
    private Account_bank_statement_closebalanceMapping account_bank_statement_closebalanceMapping;




    @PreAuthorize("hasPermission('Remove',{#account_bank_statement_closebalance_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Account_bank_statement_closebalance" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/account_bank_statement_closebalances/{account_bank_statement_closebalance_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("account_bank_statement_closebalance_id") Integer account_bank_statement_closebalance_id) {
         return ResponseEntity.status(HttpStatus.OK).body(account_bank_statement_closebalanceService.remove(account_bank_statement_closebalance_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Account_bank_statement_closebalance" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/account_bank_statement_closebalances/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        account_bank_statement_closebalanceService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }







    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Account_bank_statement_closebalance" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/account_bank_statement_closebalances")

    public ResponseEntity<Account_bank_statement_closebalanceDTO> create(@RequestBody Account_bank_statement_closebalanceDTO account_bank_statement_closebalancedto) {
        Account_bank_statement_closebalance domain = account_bank_statement_closebalanceMapping.toDomain(account_bank_statement_closebalancedto);
		account_bank_statement_closebalanceService.create(domain);
        Account_bank_statement_closebalanceDTO dto = account_bank_statement_closebalanceMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Account_bank_statement_closebalance" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/account_bank_statement_closebalances/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Account_bank_statement_closebalanceDTO> account_bank_statement_closebalancedtos) {
        account_bank_statement_closebalanceService.createBatch(account_bank_statement_closebalanceMapping.toDomain(account_bank_statement_closebalancedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission(#account_bank_statement_closebalance_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Account_bank_statement_closebalance" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/account_bank_statement_closebalances/{account_bank_statement_closebalance_id}")
    public ResponseEntity<Account_bank_statement_closebalanceDTO> get(@PathVariable("account_bank_statement_closebalance_id") Integer account_bank_statement_closebalance_id) {
        Account_bank_statement_closebalance domain = account_bank_statement_closebalanceService.get(account_bank_statement_closebalance_id);
        Account_bank_statement_closebalanceDTO dto = account_bank_statement_closebalanceMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }










    @PreAuthorize("hasPermission(#account_bank_statement_closebalance_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Account_bank_statement_closebalance" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/account_bank_statement_closebalances/{account_bank_statement_closebalance_id}")

    public ResponseEntity<Account_bank_statement_closebalanceDTO> update(@PathVariable("account_bank_statement_closebalance_id") Integer account_bank_statement_closebalance_id, @RequestBody Account_bank_statement_closebalanceDTO account_bank_statement_closebalancedto) {
		Account_bank_statement_closebalance domain = account_bank_statement_closebalanceMapping.toDomain(account_bank_statement_closebalancedto);
        domain.setId(account_bank_statement_closebalance_id);
		account_bank_statement_closebalanceService.update(domain);
		Account_bank_statement_closebalanceDTO dto = account_bank_statement_closebalanceMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#account_bank_statement_closebalance_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Account_bank_statement_closebalance" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/account_bank_statement_closebalances/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Account_bank_statement_closebalanceDTO> account_bank_statement_closebalancedtos) {
        account_bank_statement_closebalanceService.updateBatch(account_bank_statement_closebalanceMapping.toDomain(account_bank_statement_closebalancedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Account_bank_statement_closebalance" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/account_bank_statement_closebalances/fetchdefault")
	public ResponseEntity<List<Account_bank_statement_closebalanceDTO>> fetchDefault(Account_bank_statement_closebalanceSearchContext context) {
        Page<Account_bank_statement_closebalance> domains = account_bank_statement_closebalanceService.searchDefault(context) ;
        List<Account_bank_statement_closebalanceDTO> list = account_bank_statement_closebalanceMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Account_bank_statement_closebalance" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/account_bank_statement_closebalances/searchdefault")
	public ResponseEntity<Page<Account_bank_statement_closebalanceDTO>> searchDefault(Account_bank_statement_closebalanceSearchContext context) {
        Page<Account_bank_statement_closebalance> domains = account_bank_statement_closebalanceService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(account_bank_statement_closebalanceMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Account_bank_statement_closebalance getEntity(){
        return new Account_bank_statement_closebalance();
    }

}
