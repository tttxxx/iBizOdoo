package cn.ibizlab.odoo.odoo_account.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;
import cn.ibizlab.odoo.util.domain.DTOBase;
import lombok.Data;

/**
 * 服务DTO对象[Account_bank_statementDTO]
 */
@Data
public class Account_bank_statementDTO extends DTOBase implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [MESSAGE_IDS]
     *
     */
    @JSONField(name = "message_ids")
    @JsonProperty("message_ids")
    private String messageIds;

    /**
     * 属性 [MOVE_LINE_IDS]
     *
     */
    @JSONField(name = "move_line_ids")
    @JsonProperty("move_line_ids")
    private String moveLineIds;

    /**
     * 属性 [NAME]
     *
     */
    @JSONField(name = "name")
    @JsonProperty("name")
    private String name;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    private String displayName;

    /**
     * 属性 [BALANCE_END_REAL]
     *
     */
    @JSONField(name = "balance_end_real")
    @JsonProperty("balance_end_real")
    private Double balanceEndReal;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;

    /**
     * 属性 [POS_SESSION_ID]
     *
     */
    @JSONField(name = "pos_session_id")
    @JsonProperty("pos_session_id")
    private Integer posSessionId;

    /**
     * 属性 [BALANCE_END]
     *
     */
    @JSONField(name = "balance_end")
    @JsonProperty("balance_end")
    private Double balanceEnd;

    /**
     * 属性 [TOTAL_ENTRY_ENCODING]
     *
     */
    @JSONField(name = "total_entry_encoding")
    @JsonProperty("total_entry_encoding")
    private Double totalEntryEncoding;

    /**
     * 属性 [DATE_DONE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "date_done" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("date_done")
    private Timestamp dateDone;

    /**
     * 属性 [MESSAGE_CHANNEL_IDS]
     *
     */
    @JSONField(name = "message_channel_ids")
    @JsonProperty("message_channel_ids")
    private String messageChannelIds;

    /**
     * 属性 [ID]
     *
     */
    @JSONField(name = "id")
    @JsonProperty("id")
    private Integer id;

    /**
     * 属性 [ALL_LINES_RECONCILED]
     *
     */
    @JSONField(name = "all_lines_reconciled")
    @JsonProperty("all_lines_reconciled")
    private String allLinesReconciled;

    /**
     * 属性 [MESSAGE_IS_FOLLOWER]
     *
     */
    @JSONField(name = "message_is_follower")
    @JsonProperty("message_is_follower")
    private String messageIsFollower;

    /**
     * 属性 [WEBSITE_MESSAGE_IDS]
     *
     */
    @JSONField(name = "website_message_ids")
    @JsonProperty("website_message_ids")
    private String websiteMessageIds;

    /**
     * 属性 [MESSAGE_NEEDACTION_COUNTER]
     *
     */
    @JSONField(name = "message_needaction_counter")
    @JsonProperty("message_needaction_counter")
    private Integer messageNeedactionCounter;

    /**
     * 属性 [LINE_IDS]
     *
     */
    @JSONField(name = "line_ids")
    @JsonProperty("line_ids")
    private String lineIds;

    /**
     * 属性 [MESSAGE_ATTACHMENT_COUNT]
     *
     */
    @JSONField(name = "message_attachment_count")
    @JsonProperty("message_attachment_count")
    private Integer messageAttachmentCount;

    /**
     * 属性 [MESSAGE_HAS_ERROR]
     *
     */
    @JSONField(name = "message_has_error")
    @JsonProperty("message_has_error")
    private String messageHasError;

    /**
     * 属性 [MESSAGE_UNREAD]
     *
     */
    @JSONField(name = "message_unread")
    @JsonProperty("message_unread")
    private String messageUnread;

    /**
     * 属性 [MOVE_LINE_COUNT]
     *
     */
    @JSONField(name = "move_line_count")
    @JsonProperty("move_line_count")
    private Integer moveLineCount;

    /**
     * 属性 [STATE]
     *
     */
    @JSONField(name = "state")
    @JsonProperty("state")
    private String state;

    /**
     * 属性 [CURRENCY_ID]
     *
     */
    @JSONField(name = "currency_id")
    @JsonProperty("currency_id")
    private Integer currencyId;

    /**
     * 属性 [DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "date" , format="yyyy-MM-dd")
    @JsonProperty("date")
    private Timestamp date;

    /**
     * 属性 [IS_DIFFERENCE_ZERO]
     *
     */
    @JSONField(name = "is_difference_zero")
    @JsonProperty("is_difference_zero")
    private String isDifferenceZero;

    /**
     * 属性 [MESSAGE_UNREAD_COUNTER]
     *
     */
    @JSONField(name = "message_unread_counter")
    @JsonProperty("message_unread_counter")
    private Integer messageUnreadCounter;

    /**
     * 属性 [CREATE_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;

    /**
     * 属性 [MESSAGE_MAIN_ATTACHMENT_ID]
     *
     */
    @JSONField(name = "message_main_attachment_id")
    @JsonProperty("message_main_attachment_id")
    private Integer messageMainAttachmentId;

    /**
     * 属性 [MESSAGE_NEEDACTION]
     *
     */
    @JSONField(name = "message_needaction")
    @JsonProperty("message_needaction")
    private String messageNeedaction;

    /**
     * 属性 [MESSAGE_HAS_ERROR_COUNTER]
     *
     */
    @JSONField(name = "message_has_error_counter")
    @JsonProperty("message_has_error_counter")
    private Integer messageHasErrorCounter;

    /**
     * 属性 [MESSAGE_PARTNER_IDS]
     *
     */
    @JSONField(name = "message_partner_ids")
    @JsonProperty("message_partner_ids")
    private String messagePartnerIds;

    /**
     * 属性 [MESSAGE_FOLLOWER_IDS]
     *
     */
    @JSONField(name = "message_follower_ids")
    @JsonProperty("message_follower_ids")
    private String messageFollowerIds;

    /**
     * 属性 [DIFFERENCE]
     *
     */
    @JSONField(name = "difference")
    @JsonProperty("difference")
    private Double difference;

    /**
     * 属性 [WRITE_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;

    /**
     * 属性 [REFERENCE]
     *
     */
    @JSONField(name = "reference")
    @JsonProperty("reference")
    private String reference;

    /**
     * 属性 [BALANCE_START]
     *
     */
    @JSONField(name = "balance_start")
    @JsonProperty("balance_start")
    private Double balanceStart;

    /**
     * 属性 [ACCOUNTING_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "accounting_date" , format="yyyy-MM-dd")
    @JsonProperty("accounting_date")
    private Timestamp accountingDate;

    /**
     * 属性 [USER_ID_TEXT]
     *
     */
    @JSONField(name = "user_id_text")
    @JsonProperty("user_id_text")
    private String userIdText;

    /**
     * 属性 [WRITE_UID_TEXT]
     *
     */
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    private String writeUidText;

    /**
     * 属性 [COMPANY_ID_TEXT]
     *
     */
    @JSONField(name = "company_id_text")
    @JsonProperty("company_id_text")
    private String companyIdText;

    /**
     * 属性 [JOURNAL_ID_TEXT]
     *
     */
    @JSONField(name = "journal_id_text")
    @JsonProperty("journal_id_text")
    private String journalIdText;

    /**
     * 属性 [JOURNAL_TYPE]
     *
     */
    @JSONField(name = "journal_type")
    @JsonProperty("journal_type")
    private String journalType;

    /**
     * 属性 [CREATE_UID_TEXT]
     *
     */
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    private String createUidText;

    /**
     * 属性 [ACCOUNT_ID]
     *
     */
    @JSONField(name = "account_id")
    @JsonProperty("account_id")
    private Integer accountId;

    /**
     * 属性 [CASHBOX_START_ID]
     *
     */
    @JSONField(name = "cashbox_start_id")
    @JsonProperty("cashbox_start_id")
    private Integer cashboxStartId;

    /**
     * 属性 [CREATE_UID]
     *
     */
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    private Integer createUid;

    /**
     * 属性 [JOURNAL_ID]
     *
     */
    @JSONField(name = "journal_id")
    @JsonProperty("journal_id")
    private Integer journalId;

    /**
     * 属性 [COMPANY_ID]
     *
     */
    @JSONField(name = "company_id")
    @JsonProperty("company_id")
    private Integer companyId;

    /**
     * 属性 [CASHBOX_END_ID]
     *
     */
    @JSONField(name = "cashbox_end_id")
    @JsonProperty("cashbox_end_id")
    private Integer cashboxEndId;

    /**
     * 属性 [USER_ID]
     *
     */
    @JSONField(name = "user_id")
    @JsonProperty("user_id")
    private Integer userId;

    /**
     * 属性 [WRITE_UID]
     *
     */
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    private Integer writeUid;


    /**
     * 设置 [NAME]
     */
    public void setName(String  name){
        this.name = name ;
        this.modify("name",name);
    }

    /**
     * 设置 [BALANCE_END_REAL]
     */
    public void setBalanceEndReal(Double  balanceEndReal){
        this.balanceEndReal = balanceEndReal ;
        this.modify("balance_end_real",balanceEndReal);
    }

    /**
     * 设置 [POS_SESSION_ID]
     */
    public void setPosSessionId(Integer  posSessionId){
        this.posSessionId = posSessionId ;
        this.modify("pos_session_id",posSessionId);
    }

    /**
     * 设置 [BALANCE_END]
     */
    public void setBalanceEnd(Double  balanceEnd){
        this.balanceEnd = balanceEnd ;
        this.modify("balance_end",balanceEnd);
    }

    /**
     * 设置 [TOTAL_ENTRY_ENCODING]
     */
    public void setTotalEntryEncoding(Double  totalEntryEncoding){
        this.totalEntryEncoding = totalEntryEncoding ;
        this.modify("total_entry_encoding",totalEntryEncoding);
    }

    /**
     * 设置 [DATE_DONE]
     */
    public void setDateDone(Timestamp  dateDone){
        this.dateDone = dateDone ;
        this.modify("date_done",dateDone);
    }

    /**
     * 设置 [STATE]
     */
    public void setState(String  state){
        this.state = state ;
        this.modify("state",state);
    }

    /**
     * 设置 [DATE]
     */
    public void setDate(Timestamp  date){
        this.date = date ;
        this.modify("date",date);
    }

    /**
     * 设置 [MESSAGE_MAIN_ATTACHMENT_ID]
     */
    public void setMessageMainAttachmentId(Integer  messageMainAttachmentId){
        this.messageMainAttachmentId = messageMainAttachmentId ;
        this.modify("message_main_attachment_id",messageMainAttachmentId);
    }

    /**
     * 设置 [DIFFERENCE]
     */
    public void setDifference(Double  difference){
        this.difference = difference ;
        this.modify("difference",difference);
    }

    /**
     * 设置 [REFERENCE]
     */
    public void setReference(String  reference){
        this.reference = reference ;
        this.modify("reference",reference);
    }

    /**
     * 设置 [BALANCE_START]
     */
    public void setBalanceStart(Double  balanceStart){
        this.balanceStart = balanceStart ;
        this.modify("balance_start",balanceStart);
    }

    /**
     * 设置 [ACCOUNTING_DATE]
     */
    public void setAccountingDate(Timestamp  accountingDate){
        this.accountingDate = accountingDate ;
        this.modify("accounting_date",accountingDate);
    }

    /**
     * 设置 [CASHBOX_START_ID]
     */
    public void setCashboxStartId(Integer  cashboxStartId){
        this.cashboxStartId = cashboxStartId ;
        this.modify("cashbox_start_id",cashboxStartId);
    }

    /**
     * 设置 [JOURNAL_ID]
     */
    public void setJournalId(Integer  journalId){
        this.journalId = journalId ;
        this.modify("journal_id",journalId);
    }

    /**
     * 设置 [COMPANY_ID]
     */
    public void setCompanyId(Integer  companyId){
        this.companyId = companyId ;
        this.modify("company_id",companyId);
    }

    /**
     * 设置 [CASHBOX_END_ID]
     */
    public void setCashboxEndId(Integer  cashboxEndId){
        this.cashboxEndId = cashboxEndId ;
        this.modify("cashbox_end_id",cashboxEndId);
    }

    /**
     * 设置 [USER_ID]
     */
    public void setUserId(Integer  userId){
        this.userId = userId ;
        this.modify("user_id",userId);
    }


}

