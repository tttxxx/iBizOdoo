package cn.ibizlab.odoo.odoo_account.mapping;

import org.mapstruct.*;
import cn.ibizlab.odoo.core.odoo_account.domain.Account_account_template;
import cn.ibizlab.odoo.odoo_account.dto.Account_account_templateDTO;
import cn.ibizlab.odoo.util.domain.MappingBase;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring", uses = {},
    nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
    nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface Account_account_templateMapping extends MappingBase<Account_account_templateDTO, Account_account_template> {


}

