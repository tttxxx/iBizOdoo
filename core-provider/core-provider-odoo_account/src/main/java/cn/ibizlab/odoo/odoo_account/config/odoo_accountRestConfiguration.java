package cn.ibizlab.odoo.odoo_account.config;

import org.springframework.context.annotation.ComponentScan;

@ComponentScan("cn.ibizlab.odoo.odoo_account")
public class odoo_accountRestConfiguration {

}
