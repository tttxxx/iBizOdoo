package cn.ibizlab.odoo.odoo_account.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;
import cn.ibizlab.odoo.util.domain.DTOBase;
import lombok.Data;

/**
 * 服务DTO对象[Account_invoice_taxDTO]
 */
@Data
public class Account_invoice_taxDTO extends DTOBase implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [MANUAL]
     *
     */
    @JSONField(name = "manual")
    @JsonProperty("manual")
    private String manual;

    /**
     * 属性 [AMOUNT_TOTAL]
     *
     */
    @JSONField(name = "amount_total")
    @JsonProperty("amount_total")
    private Double amountTotal;

    /**
     * 属性 [CREATE_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;

    /**
     * 属性 [BASE]
     *
     */
    @JSONField(name = "base")
    @JsonProperty("base")
    private Double base;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;

    /**
     * 属性 [ID]
     *
     */
    @JSONField(name = "id")
    @JsonProperty("id")
    private Integer id;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    private String displayName;

    /**
     * 属性 [AMOUNT]
     *
     */
    @JSONField(name = "amount")
    @JsonProperty("amount")
    private Double amount;

    /**
     * 属性 [NAME]
     *
     */
    @JSONField(name = "name")
    @JsonProperty("name")
    private String name;

    /**
     * 属性 [SEQUENCE]
     *
     */
    @JSONField(name = "sequence")
    @JsonProperty("sequence")
    private Integer sequence;

    /**
     * 属性 [WRITE_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;

    /**
     * 属性 [ANALYTIC_TAG_IDS]
     *
     */
    @JSONField(name = "analytic_tag_ids")
    @JsonProperty("analytic_tag_ids")
    private String analyticTagIds;

    /**
     * 属性 [AMOUNT_ROUNDING]
     *
     */
    @JSONField(name = "amount_rounding")
    @JsonProperty("amount_rounding")
    private Double amountRounding;

    /**
     * 属性 [TAX_ID_TEXT]
     *
     */
    @JSONField(name = "tax_id_text")
    @JsonProperty("tax_id_text")
    private String taxIdText;

    /**
     * 属性 [CREATE_UID_TEXT]
     *
     */
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    private String createUidText;

    /**
     * 属性 [INVOICE_ID_TEXT]
     *
     */
    @JSONField(name = "invoice_id_text")
    @JsonProperty("invoice_id_text")
    private String invoiceIdText;

    /**
     * 属性 [CURRENCY_ID_TEXT]
     *
     */
    @JSONField(name = "currency_id_text")
    @JsonProperty("currency_id_text")
    private String currencyIdText;

    /**
     * 属性 [WRITE_UID_TEXT]
     *
     */
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    private String writeUidText;

    /**
     * 属性 [COMPANY_ID_TEXT]
     *
     */
    @JSONField(name = "company_id_text")
    @JsonProperty("company_id_text")
    private String companyIdText;

    /**
     * 属性 [ACCOUNT_ANALYTIC_ID_TEXT]
     *
     */
    @JSONField(name = "account_analytic_id_text")
    @JsonProperty("account_analytic_id_text")
    private String accountAnalyticIdText;

    /**
     * 属性 [ACCOUNT_ID_TEXT]
     *
     */
    @JSONField(name = "account_id_text")
    @JsonProperty("account_id_text")
    private String accountIdText;

    /**
     * 属性 [CREATE_UID]
     *
     */
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    private Integer createUid;

    /**
     * 属性 [ACCOUNT_ID]
     *
     */
    @JSONField(name = "account_id")
    @JsonProperty("account_id")
    private Integer accountId;

    /**
     * 属性 [INVOICE_ID]
     *
     */
    @JSONField(name = "invoice_id")
    @JsonProperty("invoice_id")
    private Integer invoiceId;

    /**
     * 属性 [CURRENCY_ID]
     *
     */
    @JSONField(name = "currency_id")
    @JsonProperty("currency_id")
    private Integer currencyId;

    /**
     * 属性 [ACCOUNT_ANALYTIC_ID]
     *
     */
    @JSONField(name = "account_analytic_id")
    @JsonProperty("account_analytic_id")
    private Integer accountAnalyticId;

    /**
     * 属性 [WRITE_UID]
     *
     */
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    private Integer writeUid;

    /**
     * 属性 [COMPANY_ID]
     *
     */
    @JSONField(name = "company_id")
    @JsonProperty("company_id")
    private Integer companyId;

    /**
     * 属性 [TAX_ID]
     *
     */
    @JSONField(name = "tax_id")
    @JsonProperty("tax_id")
    private Integer taxId;


    /**
     * 设置 [MANUAL]
     */
    public void setManual(String  manual){
        this.manual = manual ;
        this.modify("manual",manual);
    }

    /**
     * 设置 [BASE]
     */
    public void setBase(Double  base){
        this.base = base ;
        this.modify("base",base);
    }

    /**
     * 设置 [AMOUNT]
     */
    public void setAmount(Double  amount){
        this.amount = amount ;
        this.modify("amount",amount);
    }

    /**
     * 设置 [NAME]
     */
    public void setName(String  name){
        this.name = name ;
        this.modify("name",name);
    }

    /**
     * 设置 [SEQUENCE]
     */
    public void setSequence(Integer  sequence){
        this.sequence = sequence ;
        this.modify("sequence",sequence);
    }

    /**
     * 设置 [AMOUNT_ROUNDING]
     */
    public void setAmountRounding(Double  amountRounding){
        this.amountRounding = amountRounding ;
        this.modify("amount_rounding",amountRounding);
    }

    /**
     * 设置 [ACCOUNT_ID]
     */
    public void setAccountId(Integer  accountId){
        this.accountId = accountId ;
        this.modify("account_id",accountId);
    }

    /**
     * 设置 [INVOICE_ID]
     */
    public void setInvoiceId(Integer  invoiceId){
        this.invoiceId = invoiceId ;
        this.modify("invoice_id",invoiceId);
    }

    /**
     * 设置 [CURRENCY_ID]
     */
    public void setCurrencyId(Integer  currencyId){
        this.currencyId = currencyId ;
        this.modify("currency_id",currencyId);
    }

    /**
     * 设置 [ACCOUNT_ANALYTIC_ID]
     */
    public void setAccountAnalyticId(Integer  accountAnalyticId){
        this.accountAnalyticId = accountAnalyticId ;
        this.modify("account_analytic_id",accountAnalyticId);
    }

    /**
     * 设置 [COMPANY_ID]
     */
    public void setCompanyId(Integer  companyId){
        this.companyId = companyId ;
        this.modify("company_id",companyId);
    }

    /**
     * 设置 [TAX_ID]
     */
    public void setTaxId(Integer  taxId){
        this.taxId = taxId ;
        this.modify("tax_id",taxId);
    }


}

