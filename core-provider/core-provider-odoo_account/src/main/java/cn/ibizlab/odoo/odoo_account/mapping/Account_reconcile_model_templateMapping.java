package cn.ibizlab.odoo.odoo_account.mapping;

import org.mapstruct.*;
import cn.ibizlab.odoo.core.odoo_account.domain.Account_reconcile_model_template;
import cn.ibizlab.odoo.odoo_account.dto.Account_reconcile_model_templateDTO;
import cn.ibizlab.odoo.util.domain.MappingBase;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring", uses = {},
    nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
    nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface Account_reconcile_model_templateMapping extends MappingBase<Account_reconcile_model_templateDTO, Account_reconcile_model_template> {


}

