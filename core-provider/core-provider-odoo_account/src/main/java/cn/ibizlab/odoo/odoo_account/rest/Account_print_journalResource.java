package cn.ibizlab.odoo.odoo_account.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_account.dto.*;
import cn.ibizlab.odoo.odoo_account.mapping.*;
import cn.ibizlab.odoo.core.odoo_account.domain.Account_print_journal;
import cn.ibizlab.odoo.core.odoo_account.service.IAccount_print_journalService;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_print_journalSearchContext;




@Slf4j
@Api(tags = {"Account_print_journal" })
@RestController("odoo_account-account_print_journal")
@RequestMapping("")
public class Account_print_journalResource {

    @Autowired
    private IAccount_print_journalService account_print_journalService;

    @Autowired
    @Lazy
    private Account_print_journalMapping account_print_journalMapping;







    @PreAuthorize("hasPermission('Remove',{#account_print_journal_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Account_print_journal" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/account_print_journals/{account_print_journal_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("account_print_journal_id") Integer account_print_journal_id) {
         return ResponseEntity.status(HttpStatus.OK).body(account_print_journalService.remove(account_print_journal_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Account_print_journal" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/account_print_journals/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        account_print_journalService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }







    @PreAuthorize("hasPermission(#account_print_journal_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Account_print_journal" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/account_print_journals/{account_print_journal_id}")

    public ResponseEntity<Account_print_journalDTO> update(@PathVariable("account_print_journal_id") Integer account_print_journal_id, @RequestBody Account_print_journalDTO account_print_journaldto) {
		Account_print_journal domain = account_print_journalMapping.toDomain(account_print_journaldto);
        domain.setId(account_print_journal_id);
		account_print_journalService.update(domain);
		Account_print_journalDTO dto = account_print_journalMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#account_print_journal_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Account_print_journal" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/account_print_journals/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Account_print_journalDTO> account_print_journaldtos) {
        account_print_journalService.updateBatch(account_print_journalMapping.toDomain(account_print_journaldtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission(#account_print_journal_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Account_print_journal" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/account_print_journals/{account_print_journal_id}")
    public ResponseEntity<Account_print_journalDTO> get(@PathVariable("account_print_journal_id") Integer account_print_journal_id) {
        Account_print_journal domain = account_print_journalService.get(account_print_journal_id);
        Account_print_journalDTO dto = account_print_journalMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }




    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Account_print_journal" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/account_print_journals")

    public ResponseEntity<Account_print_journalDTO> create(@RequestBody Account_print_journalDTO account_print_journaldto) {
        Account_print_journal domain = account_print_journalMapping.toDomain(account_print_journaldto);
		account_print_journalService.create(domain);
        Account_print_journalDTO dto = account_print_journalMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Account_print_journal" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/account_print_journals/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Account_print_journalDTO> account_print_journaldtos) {
        account_print_journalService.createBatch(account_print_journalMapping.toDomain(account_print_journaldtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Account_print_journal" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/account_print_journals/fetchdefault")
	public ResponseEntity<List<Account_print_journalDTO>> fetchDefault(Account_print_journalSearchContext context) {
        Page<Account_print_journal> domains = account_print_journalService.searchDefault(context) ;
        List<Account_print_journalDTO> list = account_print_journalMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Account_print_journal" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/account_print_journals/searchdefault")
	public ResponseEntity<Page<Account_print_journalDTO>> searchDefault(Account_print_journalSearchContext context) {
        Page<Account_print_journal> domains = account_print_journalService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(account_print_journalMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Account_print_journal getEntity(){
        return new Account_print_journal();
    }

}
