package cn.ibizlab.odoo.odoo_account.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;
import cn.ibizlab.odoo.util.domain.DTOBase;
import lombok.Data;

/**
 * 服务DTO对象[Account_payment_term_lineDTO]
 */
@Data
public class Account_payment_term_lineDTO extends DTOBase implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [DAYS]
     *
     */
    @JSONField(name = "days")
    @JsonProperty("days")
    private Integer days;

    /**
     * 属性 [SEQUENCE]
     *
     */
    @JSONField(name = "sequence")
    @JsonProperty("sequence")
    private Integer sequence;

    /**
     * 属性 [WRITE_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;

    /**
     * 属性 [VALUE]
     *
     */
    @JSONField(name = "value")
    @JsonProperty("value")
    private String value;

    /**
     * 属性 [VALUE_AMOUNT]
     *
     */
    @JSONField(name = "value_amount")
    @JsonProperty("value_amount")
    private Double valueAmount;

    /**
     * 属性 [DAY_OF_THE_MONTH]
     *
     */
    @JSONField(name = "day_of_the_month")
    @JsonProperty("day_of_the_month")
    private Integer dayOfTheMonth;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    private String displayName;

    /**
     * 属性 [OPTION]
     *
     */
    @JSONField(name = "option")
    @JsonProperty("option")
    private String option;

    /**
     * 属性 [CREATE_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;

    /**
     * 属性 [ID]
     *
     */
    @JSONField(name = "id")
    @JsonProperty("id")
    private Integer id;

    /**
     * 属性 [PAYMENT_ID_TEXT]
     *
     */
    @JSONField(name = "payment_id_text")
    @JsonProperty("payment_id_text")
    private String paymentIdText;

    /**
     * 属性 [CREATE_UID_TEXT]
     *
     */
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    private String createUidText;

    /**
     * 属性 [WRITE_UID_TEXT]
     *
     */
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    private String writeUidText;

    /**
     * 属性 [CREATE_UID]
     *
     */
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    private Integer createUid;

    /**
     * 属性 [PAYMENT_ID]
     *
     */
    @JSONField(name = "payment_id")
    @JsonProperty("payment_id")
    private Integer paymentId;

    /**
     * 属性 [WRITE_UID]
     *
     */
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    private Integer writeUid;


    /**
     * 设置 [DAYS]
     */
    public void setDays(Integer  days){
        this.days = days ;
        this.modify("days",days);
    }

    /**
     * 设置 [SEQUENCE]
     */
    public void setSequence(Integer  sequence){
        this.sequence = sequence ;
        this.modify("sequence",sequence);
    }

    /**
     * 设置 [VALUE]
     */
    public void setValue(String  value){
        this.value = value ;
        this.modify("value",value);
    }

    /**
     * 设置 [VALUE_AMOUNT]
     */
    public void setValueAmount(Double  valueAmount){
        this.valueAmount = valueAmount ;
        this.modify("value_amount",valueAmount);
    }

    /**
     * 设置 [DAY_OF_THE_MONTH]
     */
    public void setDayOfTheMonth(Integer  dayOfTheMonth){
        this.dayOfTheMonth = dayOfTheMonth ;
        this.modify("day_of_the_month",dayOfTheMonth);
    }

    /**
     * 设置 [OPTION]
     */
    public void setOption(String  option){
        this.option = option ;
        this.modify("option",option);
    }

    /**
     * 设置 [PAYMENT_ID]
     */
    public void setPaymentId(Integer  paymentId){
        this.paymentId = paymentId ;
        this.modify("payment_id",paymentId);
    }


}

