package cn.ibizlab.odoo.odoo_account.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import lombok.Data;

@ConfigurationProperties(prefix = "service.odoo-account")
@Data
public class odoo_accountServiceProperties {

	private boolean enabled;

	private boolean auth;


}