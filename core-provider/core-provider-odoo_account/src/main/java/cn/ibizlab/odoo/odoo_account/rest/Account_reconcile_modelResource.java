package cn.ibizlab.odoo.odoo_account.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_account.dto.*;
import cn.ibizlab.odoo.odoo_account.mapping.*;
import cn.ibizlab.odoo.core.odoo_account.domain.Account_reconcile_model;
import cn.ibizlab.odoo.core.odoo_account.service.IAccount_reconcile_modelService;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_reconcile_modelSearchContext;




@Slf4j
@Api(tags = {"Account_reconcile_model" })
@RestController("odoo_account-account_reconcile_model")
@RequestMapping("")
public class Account_reconcile_modelResource {

    @Autowired
    private IAccount_reconcile_modelService account_reconcile_modelService;

    @Autowired
    @Lazy
    private Account_reconcile_modelMapping account_reconcile_modelMapping;




    @PreAuthorize("hasPermission(#account_reconcile_model_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Account_reconcile_model" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/account_reconcile_models/{account_reconcile_model_id}")
    public ResponseEntity<Account_reconcile_modelDTO> get(@PathVariable("account_reconcile_model_id") Integer account_reconcile_model_id) {
        Account_reconcile_model domain = account_reconcile_modelService.get(account_reconcile_model_id);
        Account_reconcile_modelDTO dto = account_reconcile_modelMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }













    @PreAuthorize("hasPermission('Remove',{#account_reconcile_model_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Account_reconcile_model" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/account_reconcile_models/{account_reconcile_model_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("account_reconcile_model_id") Integer account_reconcile_model_id) {
         return ResponseEntity.status(HttpStatus.OK).body(account_reconcile_modelService.remove(account_reconcile_model_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Account_reconcile_model" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/account_reconcile_models/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        account_reconcile_modelService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Account_reconcile_model" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/account_reconcile_models")

    public ResponseEntity<Account_reconcile_modelDTO> create(@RequestBody Account_reconcile_modelDTO account_reconcile_modeldto) {
        Account_reconcile_model domain = account_reconcile_modelMapping.toDomain(account_reconcile_modeldto);
		account_reconcile_modelService.create(domain);
        Account_reconcile_modelDTO dto = account_reconcile_modelMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Account_reconcile_model" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/account_reconcile_models/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Account_reconcile_modelDTO> account_reconcile_modeldtos) {
        account_reconcile_modelService.createBatch(account_reconcile_modelMapping.toDomain(account_reconcile_modeldtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission(#account_reconcile_model_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Account_reconcile_model" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/account_reconcile_models/{account_reconcile_model_id}")

    public ResponseEntity<Account_reconcile_modelDTO> update(@PathVariable("account_reconcile_model_id") Integer account_reconcile_model_id, @RequestBody Account_reconcile_modelDTO account_reconcile_modeldto) {
		Account_reconcile_model domain = account_reconcile_modelMapping.toDomain(account_reconcile_modeldto);
        domain.setId(account_reconcile_model_id);
		account_reconcile_modelService.update(domain);
		Account_reconcile_modelDTO dto = account_reconcile_modelMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#account_reconcile_model_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Account_reconcile_model" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/account_reconcile_models/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Account_reconcile_modelDTO> account_reconcile_modeldtos) {
        account_reconcile_modelService.updateBatch(account_reconcile_modelMapping.toDomain(account_reconcile_modeldtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Account_reconcile_model" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/account_reconcile_models/fetchdefault")
	public ResponseEntity<List<Account_reconcile_modelDTO>> fetchDefault(Account_reconcile_modelSearchContext context) {
        Page<Account_reconcile_model> domains = account_reconcile_modelService.searchDefault(context) ;
        List<Account_reconcile_modelDTO> list = account_reconcile_modelMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Account_reconcile_model" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/account_reconcile_models/searchdefault")
	public ResponseEntity<Page<Account_reconcile_modelDTO>> searchDefault(Account_reconcile_modelSearchContext context) {
        Page<Account_reconcile_model> domains = account_reconcile_modelService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(account_reconcile_modelMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Account_reconcile_model getEntity(){
        return new Account_reconcile_model();
    }

}
