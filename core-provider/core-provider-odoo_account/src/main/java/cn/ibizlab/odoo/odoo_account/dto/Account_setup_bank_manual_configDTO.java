package cn.ibizlab.odoo.odoo_account.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;
import cn.ibizlab.odoo.util.domain.DTOBase;
import lombok.Data;

/**
 * 服务DTO对象[Account_setup_bank_manual_configDTO]
 */
@Data
public class Account_setup_bank_manual_configDTO extends DTOBase implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [JOURNAL_ID]
     *
     */
    @JSONField(name = "journal_id")
    @JsonProperty("journal_id")
    private String journalId;

    /**
     * 属性 [LINKED_JOURNAL_ID]
     *
     */
    @JSONField(name = "linked_journal_id")
    @JsonProperty("linked_journal_id")
    private Integer linkedJournalId;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    private String displayName;

    /**
     * 属性 [WRITE_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;

    /**
     * 属性 [NEW_JOURNAL_NAME]
     *
     */
    @JSONField(name = "new_journal_name")
    @JsonProperty("new_journal_name")
    private String newJournalName;

    /**
     * 属性 [NEW_JOURNAL_CODE]
     *
     */
    @JSONField(name = "new_journal_code")
    @JsonProperty("new_journal_code")
    private String newJournalCode;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;

    /**
     * 属性 [CREATE_OR_LINK_OPTION]
     *
     */
    @JSONField(name = "create_or_link_option")
    @JsonProperty("create_or_link_option")
    private String createOrLinkOption;

    /**
     * 属性 [CREATE_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;

    /**
     * 属性 [RELATED_ACC_TYPE]
     *
     */
    @JSONField(name = "related_acc_type")
    @JsonProperty("related_acc_type")
    private String relatedAccType;

    /**
     * 属性 [ID]
     *
     */
    @JSONField(name = "id")
    @JsonProperty("id")
    private Integer id;

    /**
     * 属性 [PARTNER_ID]
     *
     */
    @JSONField(name = "partner_id")
    @JsonProperty("partner_id")
    private Integer partnerId;

    /**
     * 属性 [BANK_NAME]
     *
     */
    @JSONField(name = "bank_name")
    @JsonProperty("bank_name")
    private String bankName;

    /**
     * 属性 [WRITE_UID_TEXT]
     *
     */
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    private String writeUidText;

    /**
     * 属性 [BANK_ID]
     *
     */
    @JSONField(name = "bank_id")
    @JsonProperty("bank_id")
    private Integer bankId;

    /**
     * 属性 [QR_CODE_VALID]
     *
     */
    @JSONField(name = "qr_code_valid")
    @JsonProperty("qr_code_valid")
    private String qrCodeValid;

    /**
     * 属性 [COMPANY_ID]
     *
     */
    @JSONField(name = "company_id")
    @JsonProperty("company_id")
    private Integer companyId;

    /**
     * 属性 [SANITIZED_ACC_NUMBER]
     *
     */
    @JSONField(name = "sanitized_acc_number")
    @JsonProperty("sanitized_acc_number")
    private String sanitizedAccNumber;

    /**
     * 属性 [ACC_TYPE]
     *
     */
    @JSONField(name = "acc_type")
    @JsonProperty("acc_type")
    private String accType;

    /**
     * 属性 [SEQUENCE]
     *
     */
    @JSONField(name = "sequence")
    @JsonProperty("sequence")
    private Integer sequence;

    /**
     * 属性 [CURRENCY_ID]
     *
     */
    @JSONField(name = "currency_id")
    @JsonProperty("currency_id")
    private Integer currencyId;

    /**
     * 属性 [CREATE_UID_TEXT]
     *
     */
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    private String createUidText;

    /**
     * 属性 [ACC_NUMBER]
     *
     */
    @JSONField(name = "acc_number")
    @JsonProperty("acc_number")
    private String accNumber;

    /**
     * 属性 [BANK_BIC]
     *
     */
    @JSONField(name = "bank_bic")
    @JsonProperty("bank_bic")
    private String bankBic;

    /**
     * 属性 [ACC_HOLDER_NAME]
     *
     */
    @JSONField(name = "acc_holder_name")
    @JsonProperty("acc_holder_name")
    private String accHolderName;

    /**
     * 属性 [WRITE_UID]
     *
     */
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    private Integer writeUid;

    /**
     * 属性 [CREATE_UID]
     *
     */
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    private Integer createUid;

    /**
     * 属性 [RES_PARTNER_BANK_ID]
     *
     */
    @JSONField(name = "res_partner_bank_id")
    @JsonProperty("res_partner_bank_id")
    private Integer resPartnerBankId;


    /**
     * 设置 [NEW_JOURNAL_CODE]
     */
    public void setNewJournalCode(String  newJournalCode){
        this.newJournalCode = newJournalCode ;
        this.modify("new_journal_code",newJournalCode);
    }

    /**
     * 设置 [CREATE_OR_LINK_OPTION]
     */
    public void setCreateOrLinkOption(String  createOrLinkOption){
        this.createOrLinkOption = createOrLinkOption ;
        this.modify("create_or_link_option",createOrLinkOption);
    }

    /**
     * 设置 [RES_PARTNER_BANK_ID]
     */
    public void setResPartnerBankId(Integer  resPartnerBankId){
        this.resPartnerBankId = resPartnerBankId ;
        this.modify("res_partner_bank_id",resPartnerBankId);
    }


}

