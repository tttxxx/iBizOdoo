package cn.ibizlab.odoo.odoo_account.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_account.dto.*;
import cn.ibizlab.odoo.odoo_account.mapping.*;
import cn.ibizlab.odoo.core.odoo_account.domain.Account_invoice_tax;
import cn.ibizlab.odoo.core.odoo_account.service.IAccount_invoice_taxService;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_invoice_taxSearchContext;




@Slf4j
@Api(tags = {"Account_invoice_tax" })
@RestController("odoo_account-account_invoice_tax")
@RequestMapping("")
public class Account_invoice_taxResource {

    @Autowired
    private IAccount_invoice_taxService account_invoice_taxService;

    @Autowired
    @Lazy
    private Account_invoice_taxMapping account_invoice_taxMapping;













    @PreAuthorize("hasPermission(#account_invoice_tax_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Account_invoice_tax" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/account_invoice_taxes/{account_invoice_tax_id}")

    public ResponseEntity<Account_invoice_taxDTO> update(@PathVariable("account_invoice_tax_id") Integer account_invoice_tax_id, @RequestBody Account_invoice_taxDTO account_invoice_taxdto) {
		Account_invoice_tax domain = account_invoice_taxMapping.toDomain(account_invoice_taxdto);
        domain.setId(account_invoice_tax_id);
		account_invoice_taxService.update(domain);
		Account_invoice_taxDTO dto = account_invoice_taxMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#account_invoice_tax_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Account_invoice_tax" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/account_invoice_taxes/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Account_invoice_taxDTO> account_invoice_taxdtos) {
        account_invoice_taxService.updateBatch(account_invoice_taxMapping.toDomain(account_invoice_taxdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission(#account_invoice_tax_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Account_invoice_tax" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/account_invoice_taxes/{account_invoice_tax_id}")
    public ResponseEntity<Account_invoice_taxDTO> get(@PathVariable("account_invoice_tax_id") Integer account_invoice_tax_id) {
        Account_invoice_tax domain = account_invoice_taxService.get(account_invoice_tax_id);
        Account_invoice_taxDTO dto = account_invoice_taxMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }




    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Account_invoice_tax" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/account_invoice_taxes")

    public ResponseEntity<Account_invoice_taxDTO> create(@RequestBody Account_invoice_taxDTO account_invoice_taxdto) {
        Account_invoice_tax domain = account_invoice_taxMapping.toDomain(account_invoice_taxdto);
		account_invoice_taxService.create(domain);
        Account_invoice_taxDTO dto = account_invoice_taxMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Account_invoice_tax" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/account_invoice_taxes/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Account_invoice_taxDTO> account_invoice_taxdtos) {
        account_invoice_taxService.createBatch(account_invoice_taxMapping.toDomain(account_invoice_taxdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission('Remove',{#account_invoice_tax_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Account_invoice_tax" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/account_invoice_taxes/{account_invoice_tax_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("account_invoice_tax_id") Integer account_invoice_tax_id) {
         return ResponseEntity.status(HttpStatus.OK).body(account_invoice_taxService.remove(account_invoice_tax_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Account_invoice_tax" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/account_invoice_taxes/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        account_invoice_taxService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Account_invoice_tax" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/account_invoice_taxes/fetchdefault")
	public ResponseEntity<List<Account_invoice_taxDTO>> fetchDefault(Account_invoice_taxSearchContext context) {
        Page<Account_invoice_tax> domains = account_invoice_taxService.searchDefault(context) ;
        List<Account_invoice_taxDTO> list = account_invoice_taxMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Account_invoice_tax" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/account_invoice_taxes/searchdefault")
	public ResponseEntity<Page<Account_invoice_taxDTO>> searchDefault(Account_invoice_taxSearchContext context) {
        Page<Account_invoice_tax> domains = account_invoice_taxService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(account_invoice_taxMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Account_invoice_tax getEntity(){
        return new Account_invoice_tax();
    }

}
