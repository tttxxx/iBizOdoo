package cn.ibizlab.odoo.odoo_account.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_account.dto.*;
import cn.ibizlab.odoo.odoo_account.mapping.*;
import cn.ibizlab.odoo.core.odoo_account.domain.Account_invoice;
import cn.ibizlab.odoo.core.odoo_account.service.IAccount_invoiceService;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_invoiceSearchContext;




@Slf4j
@Api(tags = {"Account_invoice" })
@RestController("odoo_account-account_invoice")
@RequestMapping("")
public class Account_invoiceResource {

    @Autowired
    private IAccount_invoiceService account_invoiceService;

    @Autowired
    @Lazy
    private Account_invoiceMapping account_invoiceMapping;




    @PreAuthorize("hasPermission('Remove',{#account_invoice_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Account_invoice" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/account_invoices/{account_invoice_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("account_invoice_id") Integer account_invoice_id) {
         return ResponseEntity.status(HttpStatus.OK).body(account_invoiceService.remove(account_invoice_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Account_invoice" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/account_invoices/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        account_invoiceService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Account_invoice" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/account_invoices")

    public ResponseEntity<Account_invoiceDTO> create(@RequestBody Account_invoiceDTO account_invoicedto) {
        Account_invoice domain = account_invoiceMapping.toDomain(account_invoicedto);
		account_invoiceService.create(domain);
        Account_invoiceDTO dto = account_invoiceMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Account_invoice" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/account_invoices/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Account_invoiceDTO> account_invoicedtos) {
        account_invoiceService.createBatch(account_invoiceMapping.toDomain(account_invoicedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission(#account_invoice_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Account_invoice" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/account_invoices/{account_invoice_id}")

    public ResponseEntity<Account_invoiceDTO> update(@PathVariable("account_invoice_id") Integer account_invoice_id, @RequestBody Account_invoiceDTO account_invoicedto) {
		Account_invoice domain = account_invoiceMapping.toDomain(account_invoicedto);
        domain.setId(account_invoice_id);
		account_invoiceService.update(domain);
		Account_invoiceDTO dto = account_invoiceMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#account_invoice_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Account_invoice" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/account_invoices/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Account_invoiceDTO> account_invoicedtos) {
        account_invoiceService.updateBatch(account_invoiceMapping.toDomain(account_invoicedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }







    @PreAuthorize("hasPermission(#account_invoice_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Account_invoice" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/account_invoices/{account_invoice_id}")
    public ResponseEntity<Account_invoiceDTO> get(@PathVariable("account_invoice_id") Integer account_invoice_id) {
        Account_invoice domain = account_invoiceService.get(account_invoice_id);
        Account_invoiceDTO dto = account_invoiceMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }







    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Account_invoice" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/account_invoices/fetchdefault")
	public ResponseEntity<List<Account_invoiceDTO>> fetchDefault(Account_invoiceSearchContext context) {
        Page<Account_invoice> domains = account_invoiceService.searchDefault(context) ;
        List<Account_invoiceDTO> list = account_invoiceMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Account_invoice" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/account_invoices/searchdefault")
	public ResponseEntity<Page<Account_invoiceDTO>> searchDefault(Account_invoiceSearchContext context) {
        Page<Account_invoice> domains = account_invoiceService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(account_invoiceMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Account_invoice getEntity(){
        return new Account_invoice();
    }

}
