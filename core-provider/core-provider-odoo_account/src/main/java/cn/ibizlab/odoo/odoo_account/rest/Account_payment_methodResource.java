package cn.ibizlab.odoo.odoo_account.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_account.dto.*;
import cn.ibizlab.odoo.odoo_account.mapping.*;
import cn.ibizlab.odoo.core.odoo_account.domain.Account_payment_method;
import cn.ibizlab.odoo.core.odoo_account.service.IAccount_payment_methodService;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_payment_methodSearchContext;




@Slf4j
@Api(tags = {"Account_payment_method" })
@RestController("odoo_account-account_payment_method")
@RequestMapping("")
public class Account_payment_methodResource {

    @Autowired
    private IAccount_payment_methodService account_payment_methodService;

    @Autowired
    @Lazy
    private Account_payment_methodMapping account_payment_methodMapping;










    @PreAuthorize("hasPermission('Remove',{#account_payment_method_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Account_payment_method" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/account_payment_methods/{account_payment_method_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("account_payment_method_id") Integer account_payment_method_id) {
         return ResponseEntity.status(HttpStatus.OK).body(account_payment_methodService.remove(account_payment_method_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Account_payment_method" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/account_payment_methods/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        account_payment_methodService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }







    @PreAuthorize("hasPermission(#account_payment_method_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Account_payment_method" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/account_payment_methods/{account_payment_method_id}")

    public ResponseEntity<Account_payment_methodDTO> update(@PathVariable("account_payment_method_id") Integer account_payment_method_id, @RequestBody Account_payment_methodDTO account_payment_methoddto) {
		Account_payment_method domain = account_payment_methodMapping.toDomain(account_payment_methoddto);
        domain.setId(account_payment_method_id);
		account_payment_methodService.update(domain);
		Account_payment_methodDTO dto = account_payment_methodMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#account_payment_method_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Account_payment_method" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/account_payment_methods/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Account_payment_methodDTO> account_payment_methoddtos) {
        account_payment_methodService.updateBatch(account_payment_methodMapping.toDomain(account_payment_methoddtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Account_payment_method" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/account_payment_methods")

    public ResponseEntity<Account_payment_methodDTO> create(@RequestBody Account_payment_methodDTO account_payment_methoddto) {
        Account_payment_method domain = account_payment_methodMapping.toDomain(account_payment_methoddto);
		account_payment_methodService.create(domain);
        Account_payment_methodDTO dto = account_payment_methodMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Account_payment_method" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/account_payment_methods/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Account_payment_methodDTO> account_payment_methoddtos) {
        account_payment_methodService.createBatch(account_payment_methodMapping.toDomain(account_payment_methoddtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission(#account_payment_method_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Account_payment_method" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/account_payment_methods/{account_payment_method_id}")
    public ResponseEntity<Account_payment_methodDTO> get(@PathVariable("account_payment_method_id") Integer account_payment_method_id) {
        Account_payment_method domain = account_payment_methodService.get(account_payment_method_id);
        Account_payment_methodDTO dto = account_payment_methodMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Account_payment_method" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/account_payment_methods/fetchdefault")
	public ResponseEntity<List<Account_payment_methodDTO>> fetchDefault(Account_payment_methodSearchContext context) {
        Page<Account_payment_method> domains = account_payment_methodService.searchDefault(context) ;
        List<Account_payment_methodDTO> list = account_payment_methodMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Account_payment_method" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/account_payment_methods/searchdefault")
	public ResponseEntity<Page<Account_payment_methodDTO>> searchDefault(Account_payment_methodSearchContext context) {
        Page<Account_payment_method> domains = account_payment_methodService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(account_payment_methodMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Account_payment_method getEntity(){
        return new Account_payment_method();
    }

}
