package cn.ibizlab.odoo.odoo_account.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;
import cn.ibizlab.odoo.util.domain.DTOBase;
import lombok.Data;

/**
 * 服务DTO对象[Account_fiscal_position_templateDTO]
 */
@Data
public class Account_fiscal_position_templateDTO extends DTOBase implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [NAME]
     *
     */
    @JSONField(name = "name")
    @JsonProperty("name")
    private String name;

    /**
     * 属性 [STATE_IDS]
     *
     */
    @JSONField(name = "state_ids")
    @JsonProperty("state_ids")
    private String stateIds;

    /**
     * 属性 [NOTE]
     *
     */
    @JSONField(name = "note")
    @JsonProperty("note")
    private String note;

    /**
     * 属性 [SEQUENCE]
     *
     */
    @JSONField(name = "sequence")
    @JsonProperty("sequence")
    private Integer sequence;

    /**
     * 属性 [AUTO_APPLY]
     *
     */
    @JSONField(name = "auto_apply")
    @JsonProperty("auto_apply")
    private String autoApply;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;

    /**
     * 属性 [WRITE_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;

    /**
     * 属性 [ACCOUNT_IDS]
     *
     */
    @JSONField(name = "account_ids")
    @JsonProperty("account_ids")
    private String accountIds;

    /**
     * 属性 [CREATE_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;

    /**
     * 属性 [ID]
     *
     */
    @JSONField(name = "id")
    @JsonProperty("id")
    private Integer id;

    /**
     * 属性 [ZIP_TO]
     *
     */
    @JSONField(name = "zip_to")
    @JsonProperty("zip_to")
    private Integer zipTo;

    /**
     * 属性 [ZIP_FROM]
     *
     */
    @JSONField(name = "zip_from")
    @JsonProperty("zip_from")
    private Integer zipFrom;

    /**
     * 属性 [TAX_IDS]
     *
     */
    @JSONField(name = "tax_ids")
    @JsonProperty("tax_ids")
    private String taxIds;

    /**
     * 属性 [VAT_REQUIRED]
     *
     */
    @JSONField(name = "vat_required")
    @JsonProperty("vat_required")
    private String vatRequired;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    private String displayName;

    /**
     * 属性 [WRITE_UID_TEXT]
     *
     */
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    private String writeUidText;

    /**
     * 属性 [CHART_TEMPLATE_ID_TEXT]
     *
     */
    @JSONField(name = "chart_template_id_text")
    @JsonProperty("chart_template_id_text")
    private String chartTemplateIdText;

    /**
     * 属性 [COUNTRY_ID_TEXT]
     *
     */
    @JSONField(name = "country_id_text")
    @JsonProperty("country_id_text")
    private String countryIdText;

    /**
     * 属性 [CREATE_UID_TEXT]
     *
     */
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    private String createUidText;

    /**
     * 属性 [COUNTRY_GROUP_ID_TEXT]
     *
     */
    @JSONField(name = "country_group_id_text")
    @JsonProperty("country_group_id_text")
    private String countryGroupIdText;

    /**
     * 属性 [COUNTRY_GROUP_ID]
     *
     */
    @JSONField(name = "country_group_id")
    @JsonProperty("country_group_id")
    private Integer countryGroupId;

    /**
     * 属性 [COUNTRY_ID]
     *
     */
    @JSONField(name = "country_id")
    @JsonProperty("country_id")
    private Integer countryId;

    /**
     * 属性 [CREATE_UID]
     *
     */
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    private Integer createUid;

    /**
     * 属性 [WRITE_UID]
     *
     */
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    private Integer writeUid;

    /**
     * 属性 [CHART_TEMPLATE_ID]
     *
     */
    @JSONField(name = "chart_template_id")
    @JsonProperty("chart_template_id")
    private Integer chartTemplateId;


    /**
     * 设置 [NAME]
     */
    public void setName(String  name){
        this.name = name ;
        this.modify("name",name);
    }

    /**
     * 设置 [NOTE]
     */
    public void setNote(String  note){
        this.note = note ;
        this.modify("note",note);
    }

    /**
     * 设置 [SEQUENCE]
     */
    public void setSequence(Integer  sequence){
        this.sequence = sequence ;
        this.modify("sequence",sequence);
    }

    /**
     * 设置 [AUTO_APPLY]
     */
    public void setAutoApply(String  autoApply){
        this.autoApply = autoApply ;
        this.modify("auto_apply",autoApply);
    }

    /**
     * 设置 [ZIP_TO]
     */
    public void setZipTo(Integer  zipTo){
        this.zipTo = zipTo ;
        this.modify("zip_to",zipTo);
    }

    /**
     * 设置 [ZIP_FROM]
     */
    public void setZipFrom(Integer  zipFrom){
        this.zipFrom = zipFrom ;
        this.modify("zip_from",zipFrom);
    }

    /**
     * 设置 [VAT_REQUIRED]
     */
    public void setVatRequired(String  vatRequired){
        this.vatRequired = vatRequired ;
        this.modify("vat_required",vatRequired);
    }

    /**
     * 设置 [COUNTRY_GROUP_ID]
     */
    public void setCountryGroupId(Integer  countryGroupId){
        this.countryGroupId = countryGroupId ;
        this.modify("country_group_id",countryGroupId);
    }

    /**
     * 设置 [COUNTRY_ID]
     */
    public void setCountryId(Integer  countryId){
        this.countryId = countryId ;
        this.modify("country_id",countryId);
    }

    /**
     * 设置 [CHART_TEMPLATE_ID]
     */
    public void setChartTemplateId(Integer  chartTemplateId){
        this.chartTemplateId = chartTemplateId ;
        this.modify("chart_template_id",chartTemplateId);
    }


}

