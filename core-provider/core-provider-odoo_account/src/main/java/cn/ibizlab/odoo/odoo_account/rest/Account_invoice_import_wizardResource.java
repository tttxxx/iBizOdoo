package cn.ibizlab.odoo.odoo_account.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_account.dto.*;
import cn.ibizlab.odoo.odoo_account.mapping.*;
import cn.ibizlab.odoo.core.odoo_account.domain.Account_invoice_import_wizard;
import cn.ibizlab.odoo.core.odoo_account.service.IAccount_invoice_import_wizardService;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_invoice_import_wizardSearchContext;




@Slf4j
@Api(tags = {"Account_invoice_import_wizard" })
@RestController("odoo_account-account_invoice_import_wizard")
@RequestMapping("")
public class Account_invoice_import_wizardResource {

    @Autowired
    private IAccount_invoice_import_wizardService account_invoice_import_wizardService;

    @Autowired
    @Lazy
    private Account_invoice_import_wizardMapping account_invoice_import_wizardMapping;







    @PreAuthorize("hasPermission('Remove',{#account_invoice_import_wizard_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Account_invoice_import_wizard" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/account_invoice_import_wizards/{account_invoice_import_wizard_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("account_invoice_import_wizard_id") Integer account_invoice_import_wizard_id) {
         return ResponseEntity.status(HttpStatus.OK).body(account_invoice_import_wizardService.remove(account_invoice_import_wizard_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Account_invoice_import_wizard" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/account_invoice_import_wizards/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        account_invoice_import_wizardService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission(#account_invoice_import_wizard_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Account_invoice_import_wizard" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/account_invoice_import_wizards/{account_invoice_import_wizard_id}")

    public ResponseEntity<Account_invoice_import_wizardDTO> update(@PathVariable("account_invoice_import_wizard_id") Integer account_invoice_import_wizard_id, @RequestBody Account_invoice_import_wizardDTO account_invoice_import_wizarddto) {
		Account_invoice_import_wizard domain = account_invoice_import_wizardMapping.toDomain(account_invoice_import_wizarddto);
        domain.setId(account_invoice_import_wizard_id);
		account_invoice_import_wizardService.update(domain);
		Account_invoice_import_wizardDTO dto = account_invoice_import_wizardMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#account_invoice_import_wizard_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Account_invoice_import_wizard" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/account_invoice_import_wizards/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Account_invoice_import_wizardDTO> account_invoice_import_wizarddtos) {
        account_invoice_import_wizardService.updateBatch(account_invoice_import_wizardMapping.toDomain(account_invoice_import_wizarddtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }










    @PreAuthorize("hasPermission(#account_invoice_import_wizard_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Account_invoice_import_wizard" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/account_invoice_import_wizards/{account_invoice_import_wizard_id}")
    public ResponseEntity<Account_invoice_import_wizardDTO> get(@PathVariable("account_invoice_import_wizard_id") Integer account_invoice_import_wizard_id) {
        Account_invoice_import_wizard domain = account_invoice_import_wizardService.get(account_invoice_import_wizard_id);
        Account_invoice_import_wizardDTO dto = account_invoice_import_wizardMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }




    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Account_invoice_import_wizard" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/account_invoice_import_wizards")

    public ResponseEntity<Account_invoice_import_wizardDTO> create(@RequestBody Account_invoice_import_wizardDTO account_invoice_import_wizarddto) {
        Account_invoice_import_wizard domain = account_invoice_import_wizardMapping.toDomain(account_invoice_import_wizarddto);
		account_invoice_import_wizardService.create(domain);
        Account_invoice_import_wizardDTO dto = account_invoice_import_wizardMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Account_invoice_import_wizard" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/account_invoice_import_wizards/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Account_invoice_import_wizardDTO> account_invoice_import_wizarddtos) {
        account_invoice_import_wizardService.createBatch(account_invoice_import_wizardMapping.toDomain(account_invoice_import_wizarddtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Account_invoice_import_wizard" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/account_invoice_import_wizards/fetchdefault")
	public ResponseEntity<List<Account_invoice_import_wizardDTO>> fetchDefault(Account_invoice_import_wizardSearchContext context) {
        Page<Account_invoice_import_wizard> domains = account_invoice_import_wizardService.searchDefault(context) ;
        List<Account_invoice_import_wizardDTO> list = account_invoice_import_wizardMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Account_invoice_import_wizard" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/account_invoice_import_wizards/searchdefault")
	public ResponseEntity<Page<Account_invoice_import_wizardDTO>> searchDefault(Account_invoice_import_wizardSearchContext context) {
        Page<Account_invoice_import_wizard> domains = account_invoice_import_wizardService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(account_invoice_import_wizardMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Account_invoice_import_wizard getEntity(){
        return new Account_invoice_import_wizard();
    }

}
