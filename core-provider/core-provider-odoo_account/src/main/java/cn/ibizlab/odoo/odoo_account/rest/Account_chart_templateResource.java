package cn.ibizlab.odoo.odoo_account.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_account.dto.*;
import cn.ibizlab.odoo.odoo_account.mapping.*;
import cn.ibizlab.odoo.core.odoo_account.domain.Account_chart_template;
import cn.ibizlab.odoo.core.odoo_account.service.IAccount_chart_templateService;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_chart_templateSearchContext;




@Slf4j
@Api(tags = {"Account_chart_template" })
@RestController("odoo_account-account_chart_template")
@RequestMapping("")
public class Account_chart_templateResource {

    @Autowired
    private IAccount_chart_templateService account_chart_templateService;

    @Autowired
    @Lazy
    private Account_chart_templateMapping account_chart_templateMapping;







    @PreAuthorize("hasPermission(#account_chart_template_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Account_chart_template" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/account_chart_templates/{account_chart_template_id}")
    public ResponseEntity<Account_chart_templateDTO> get(@PathVariable("account_chart_template_id") Integer account_chart_template_id) {
        Account_chart_template domain = account_chart_templateService.get(account_chart_template_id);
        Account_chart_templateDTO dto = account_chart_templateMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }




    @PreAuthorize("hasPermission('Remove',{#account_chart_template_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Account_chart_template" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/account_chart_templates/{account_chart_template_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("account_chart_template_id") Integer account_chart_template_id) {
         return ResponseEntity.status(HttpStatus.OK).body(account_chart_templateService.remove(account_chart_template_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Account_chart_template" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/account_chart_templates/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        account_chart_templateService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission(#account_chart_template_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Account_chart_template" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/account_chart_templates/{account_chart_template_id}")

    public ResponseEntity<Account_chart_templateDTO> update(@PathVariable("account_chart_template_id") Integer account_chart_template_id, @RequestBody Account_chart_templateDTO account_chart_templatedto) {
		Account_chart_template domain = account_chart_templateMapping.toDomain(account_chart_templatedto);
        domain.setId(account_chart_template_id);
		account_chart_templateService.update(domain);
		Account_chart_templateDTO dto = account_chart_templateMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#account_chart_template_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Account_chart_template" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/account_chart_templates/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Account_chart_templateDTO> account_chart_templatedtos) {
        account_chart_templateService.updateBatch(account_chart_templateMapping.toDomain(account_chart_templatedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }










    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Account_chart_template" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/account_chart_templates")

    public ResponseEntity<Account_chart_templateDTO> create(@RequestBody Account_chart_templateDTO account_chart_templatedto) {
        Account_chart_template domain = account_chart_templateMapping.toDomain(account_chart_templatedto);
		account_chart_templateService.create(domain);
        Account_chart_templateDTO dto = account_chart_templateMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Account_chart_template" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/account_chart_templates/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Account_chart_templateDTO> account_chart_templatedtos) {
        account_chart_templateService.createBatch(account_chart_templateMapping.toDomain(account_chart_templatedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Account_chart_template" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/account_chart_templates/fetchdefault")
	public ResponseEntity<List<Account_chart_templateDTO>> fetchDefault(Account_chart_templateSearchContext context) {
        Page<Account_chart_template> domains = account_chart_templateService.searchDefault(context) ;
        List<Account_chart_templateDTO> list = account_chart_templateMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Account_chart_template" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/account_chart_templates/searchdefault")
	public ResponseEntity<Page<Account_chart_templateDTO>> searchDefault(Account_chart_templateSearchContext context) {
        Page<Account_chart_template> domains = account_chart_templateService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(account_chart_templateMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Account_chart_template getEntity(){
        return new Account_chart_template();
    }

}
