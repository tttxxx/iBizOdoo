package cn.ibizlab.odoo.odoo_account.mapping;

import org.mapstruct.*;
import cn.ibizlab.odoo.core.odoo_account.domain.Account_abstract_payment;
import cn.ibizlab.odoo.odoo_account.dto.Account_abstract_paymentDTO;
import cn.ibizlab.odoo.util.domain.MappingBase;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring", uses = {},
    nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
    nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface Account_abstract_paymentMapping extends MappingBase<Account_abstract_paymentDTO, Account_abstract_payment> {


}

