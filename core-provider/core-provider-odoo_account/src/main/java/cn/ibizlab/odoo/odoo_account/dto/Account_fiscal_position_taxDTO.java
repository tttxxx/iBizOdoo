package cn.ibizlab.odoo.odoo_account.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;
import cn.ibizlab.odoo.util.domain.DTOBase;
import lombok.Data;

/**
 * 服务DTO对象[Account_fiscal_position_taxDTO]
 */
@Data
public class Account_fiscal_position_taxDTO extends DTOBase implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [CREATE_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    private String displayName;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;

    /**
     * 属性 [WRITE_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;

    /**
     * 属性 [ID]
     *
     */
    @JSONField(name = "id")
    @JsonProperty("id")
    private Integer id;

    /**
     * 属性 [CREATE_UID_TEXT]
     *
     */
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    private String createUidText;

    /**
     * 属性 [TAX_SRC_ID_TEXT]
     *
     */
    @JSONField(name = "tax_src_id_text")
    @JsonProperty("tax_src_id_text")
    private String taxSrcIdText;

    /**
     * 属性 [POSITION_ID_TEXT]
     *
     */
    @JSONField(name = "position_id_text")
    @JsonProperty("position_id_text")
    private String positionIdText;

    /**
     * 属性 [TAX_DEST_ID_TEXT]
     *
     */
    @JSONField(name = "tax_dest_id_text")
    @JsonProperty("tax_dest_id_text")
    private String taxDestIdText;

    /**
     * 属性 [WRITE_UID_TEXT]
     *
     */
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    private String writeUidText;

    /**
     * 属性 [WRITE_UID]
     *
     */
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    private Integer writeUid;

    /**
     * 属性 [POSITION_ID]
     *
     */
    @JSONField(name = "position_id")
    @JsonProperty("position_id")
    private Integer positionId;

    /**
     * 属性 [TAX_DEST_ID]
     *
     */
    @JSONField(name = "tax_dest_id")
    @JsonProperty("tax_dest_id")
    private Integer taxDestId;

    /**
     * 属性 [TAX_SRC_ID]
     *
     */
    @JSONField(name = "tax_src_id")
    @JsonProperty("tax_src_id")
    private Integer taxSrcId;

    /**
     * 属性 [CREATE_UID]
     *
     */
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    private Integer createUid;


    /**
     * 设置 [POSITION_ID]
     */
    public void setPositionId(Integer  positionId){
        this.positionId = positionId ;
        this.modify("position_id",positionId);
    }

    /**
     * 设置 [TAX_DEST_ID]
     */
    public void setTaxDestId(Integer  taxDestId){
        this.taxDestId = taxDestId ;
        this.modify("tax_dest_id",taxDestId);
    }

    /**
     * 设置 [TAX_SRC_ID]
     */
    public void setTaxSrcId(Integer  taxSrcId){
        this.taxSrcId = taxSrcId ;
        this.modify("tax_src_id",taxSrcId);
    }


}

