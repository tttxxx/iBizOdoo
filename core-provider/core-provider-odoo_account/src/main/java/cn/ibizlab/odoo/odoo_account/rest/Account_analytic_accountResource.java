package cn.ibizlab.odoo.odoo_account.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_account.dto.*;
import cn.ibizlab.odoo.odoo_account.mapping.*;
import cn.ibizlab.odoo.core.odoo_account.domain.Account_analytic_account;
import cn.ibizlab.odoo.core.odoo_account.service.IAccount_analytic_accountService;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_analytic_accountSearchContext;




@Slf4j
@Api(tags = {"Account_analytic_account" })
@RestController("odoo_account-account_analytic_account")
@RequestMapping("")
public class Account_analytic_accountResource {

    @Autowired
    private IAccount_analytic_accountService account_analytic_accountService;

    @Autowired
    @Lazy
    private Account_analytic_accountMapping account_analytic_accountMapping;













    @PreAuthorize("hasPermission('Remove',{#account_analytic_account_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Account_analytic_account" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/account_analytic_accounts/{account_analytic_account_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("account_analytic_account_id") Integer account_analytic_account_id) {
         return ResponseEntity.status(HttpStatus.OK).body(account_analytic_accountService.remove(account_analytic_account_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Account_analytic_account" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/account_analytic_accounts/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        account_analytic_accountService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Account_analytic_account" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/account_analytic_accounts")

    public ResponseEntity<Account_analytic_accountDTO> create(@RequestBody Account_analytic_accountDTO account_analytic_accountdto) {
        Account_analytic_account domain = account_analytic_accountMapping.toDomain(account_analytic_accountdto);
		account_analytic_accountService.create(domain);
        Account_analytic_accountDTO dto = account_analytic_accountMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Account_analytic_account" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/account_analytic_accounts/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Account_analytic_accountDTO> account_analytic_accountdtos) {
        account_analytic_accountService.createBatch(account_analytic_accountMapping.toDomain(account_analytic_accountdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission(#account_analytic_account_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Account_analytic_account" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/account_analytic_accounts/{account_analytic_account_id}")

    public ResponseEntity<Account_analytic_accountDTO> update(@PathVariable("account_analytic_account_id") Integer account_analytic_account_id, @RequestBody Account_analytic_accountDTO account_analytic_accountdto) {
		Account_analytic_account domain = account_analytic_accountMapping.toDomain(account_analytic_accountdto);
        domain.setId(account_analytic_account_id);
		account_analytic_accountService.update(domain);
		Account_analytic_accountDTO dto = account_analytic_accountMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#account_analytic_account_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Account_analytic_account" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/account_analytic_accounts/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Account_analytic_accountDTO> account_analytic_accountdtos) {
        account_analytic_accountService.updateBatch(account_analytic_accountMapping.toDomain(account_analytic_accountdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission(#account_analytic_account_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Account_analytic_account" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/account_analytic_accounts/{account_analytic_account_id}")
    public ResponseEntity<Account_analytic_accountDTO> get(@PathVariable("account_analytic_account_id") Integer account_analytic_account_id) {
        Account_analytic_account domain = account_analytic_accountService.get(account_analytic_account_id);
        Account_analytic_accountDTO dto = account_analytic_accountMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Account_analytic_account" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/account_analytic_accounts/fetchdefault")
	public ResponseEntity<List<Account_analytic_accountDTO>> fetchDefault(Account_analytic_accountSearchContext context) {
        Page<Account_analytic_account> domains = account_analytic_accountService.searchDefault(context) ;
        List<Account_analytic_accountDTO> list = account_analytic_accountMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Account_analytic_account" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/account_analytic_accounts/searchdefault")
	public ResponseEntity<Page<Account_analytic_accountDTO>> searchDefault(Account_analytic_accountSearchContext context) {
        Page<Account_analytic_account> domains = account_analytic_accountService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(account_analytic_accountMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Account_analytic_account getEntity(){
        return new Account_analytic_account();
    }

}
