package cn.ibizlab.odoo.odoo_account.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;
import cn.ibizlab.odoo.util.domain.DTOBase;
import lombok.Data;

/**
 * 服务DTO对象[Account_partial_reconcileDTO]
 */
@Data
public class Account_partial_reconcileDTO extends DTOBase implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [AMOUNT_CURRENCY]
     *
     */
    @JSONField(name = "amount_currency")
    @JsonProperty("amount_currency")
    private Double amountCurrency;

    /**
     * 属性 [AMOUNT]
     *
     */
    @JSONField(name = "amount")
    @JsonProperty("amount")
    private Double amount;

    /**
     * 属性 [WRITE_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;

    /**
     * 属性 [ID]
     *
     */
    @JSONField(name = "id")
    @JsonProperty("id")
    private Integer id;

    /**
     * 属性 [__LAST_UPDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;

    /**
     * 属性 [DISPLAY_NAME]
     *
     */
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    private String displayName;

    /**
     * 属性 [CREATE_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;

    /**
     * 属性 [MAX_DATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "max_date" , format="yyyy-MM-dd")
    @JsonProperty("max_date")
    private Timestamp maxDate;

    /**
     * 属性 [COMPANY_ID_TEXT]
     *
     */
    @JSONField(name = "company_id_text")
    @JsonProperty("company_id_text")
    private String companyIdText;

    /**
     * 属性 [CREDIT_MOVE_ID_TEXT]
     *
     */
    @JSONField(name = "credit_move_id_text")
    @JsonProperty("credit_move_id_text")
    private String creditMoveIdText;

    /**
     * 属性 [COMPANY_CURRENCY_ID]
     *
     */
    @JSONField(name = "company_currency_id")
    @JsonProperty("company_currency_id")
    private Integer companyCurrencyId;

    /**
     * 属性 [CREATE_UID_TEXT]
     *
     */
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    private String createUidText;

    /**
     * 属性 [DEBIT_MOVE_ID_TEXT]
     *
     */
    @JSONField(name = "debit_move_id_text")
    @JsonProperty("debit_move_id_text")
    private String debitMoveIdText;

    /**
     * 属性 [CURRENCY_ID_TEXT]
     *
     */
    @JSONField(name = "currency_id_text")
    @JsonProperty("currency_id_text")
    private String currencyIdText;

    /**
     * 属性 [WRITE_UID_TEXT]
     *
     */
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    private String writeUidText;

    /**
     * 属性 [FULL_RECONCILE_ID_TEXT]
     *
     */
    @JSONField(name = "full_reconcile_id_text")
    @JsonProperty("full_reconcile_id_text")
    private String fullReconcileIdText;

    /**
     * 属性 [COMPANY_ID]
     *
     */
    @JSONField(name = "company_id")
    @JsonProperty("company_id")
    private Integer companyId;

    /**
     * 属性 [CURRENCY_ID]
     *
     */
    @JSONField(name = "currency_id")
    @JsonProperty("currency_id")
    private Integer currencyId;

    /**
     * 属性 [FULL_RECONCILE_ID]
     *
     */
    @JSONField(name = "full_reconcile_id")
    @JsonProperty("full_reconcile_id")
    private Integer fullReconcileId;

    /**
     * 属性 [CREATE_UID]
     *
     */
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    private Integer createUid;

    /**
     * 属性 [DEBIT_MOVE_ID]
     *
     */
    @JSONField(name = "debit_move_id")
    @JsonProperty("debit_move_id")
    private Integer debitMoveId;

    /**
     * 属性 [CREDIT_MOVE_ID]
     *
     */
    @JSONField(name = "credit_move_id")
    @JsonProperty("credit_move_id")
    private Integer creditMoveId;

    /**
     * 属性 [WRITE_UID]
     *
     */
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    private Integer writeUid;


    /**
     * 设置 [AMOUNT_CURRENCY]
     */
    public void setAmountCurrency(Double  amountCurrency){
        this.amountCurrency = amountCurrency ;
        this.modify("amount_currency",amountCurrency);
    }

    /**
     * 设置 [AMOUNT]
     */
    public void setAmount(Double  amount){
        this.amount = amount ;
        this.modify("amount",amount);
    }

    /**
     * 设置 [MAX_DATE]
     */
    public void setMaxDate(Timestamp  maxDate){
        this.maxDate = maxDate ;
        this.modify("max_date",maxDate);
    }

    /**
     * 设置 [COMPANY_ID]
     */
    public void setCompanyId(Integer  companyId){
        this.companyId = companyId ;
        this.modify("company_id",companyId);
    }

    /**
     * 设置 [CURRENCY_ID]
     */
    public void setCurrencyId(Integer  currencyId){
        this.currencyId = currencyId ;
        this.modify("currency_id",currencyId);
    }

    /**
     * 设置 [FULL_RECONCILE_ID]
     */
    public void setFullReconcileId(Integer  fullReconcileId){
        this.fullReconcileId = fullReconcileId ;
        this.modify("full_reconcile_id",fullReconcileId);
    }

    /**
     * 设置 [DEBIT_MOVE_ID]
     */
    public void setDebitMoveId(Integer  debitMoveId){
        this.debitMoveId = debitMoveId ;
        this.modify("debit_move_id",debitMoveId);
    }

    /**
     * 设置 [CREDIT_MOVE_ID]
     */
    public void setCreditMoveId(Integer  creditMoveId){
        this.creditMoveId = creditMoveId ;
        this.modify("credit_move_id",creditMoveId);
    }


}

