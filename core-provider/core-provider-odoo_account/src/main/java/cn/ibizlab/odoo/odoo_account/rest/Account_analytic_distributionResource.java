package cn.ibizlab.odoo.odoo_account.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_account.dto.*;
import cn.ibizlab.odoo.odoo_account.mapping.*;
import cn.ibizlab.odoo.core.odoo_account.domain.Account_analytic_distribution;
import cn.ibizlab.odoo.core.odoo_account.service.IAccount_analytic_distributionService;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_analytic_distributionSearchContext;




@Slf4j
@Api(tags = {"Account_analytic_distribution" })
@RestController("odoo_account-account_analytic_distribution")
@RequestMapping("")
public class Account_analytic_distributionResource {

    @Autowired
    private IAccount_analytic_distributionService account_analytic_distributionService;

    @Autowired
    @Lazy
    private Account_analytic_distributionMapping account_analytic_distributionMapping;










    @PreAuthorize("hasPermission('Remove',{#account_analytic_distribution_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Account_analytic_distribution" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/account_analytic_distributions/{account_analytic_distribution_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("account_analytic_distribution_id") Integer account_analytic_distribution_id) {
         return ResponseEntity.status(HttpStatus.OK).body(account_analytic_distributionService.remove(account_analytic_distribution_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Account_analytic_distribution" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/account_analytic_distributions/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        account_analytic_distributionService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }







    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Account_analytic_distribution" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/account_analytic_distributions")

    public ResponseEntity<Account_analytic_distributionDTO> create(@RequestBody Account_analytic_distributionDTO account_analytic_distributiondto) {
        Account_analytic_distribution domain = account_analytic_distributionMapping.toDomain(account_analytic_distributiondto);
		account_analytic_distributionService.create(domain);
        Account_analytic_distributionDTO dto = account_analytic_distributionMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Account_analytic_distribution" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/account_analytic_distributions/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Account_analytic_distributionDTO> account_analytic_distributiondtos) {
        account_analytic_distributionService.createBatch(account_analytic_distributionMapping.toDomain(account_analytic_distributiondtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission(#account_analytic_distribution_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Account_analytic_distribution" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/account_analytic_distributions/{account_analytic_distribution_id}")

    public ResponseEntity<Account_analytic_distributionDTO> update(@PathVariable("account_analytic_distribution_id") Integer account_analytic_distribution_id, @RequestBody Account_analytic_distributionDTO account_analytic_distributiondto) {
		Account_analytic_distribution domain = account_analytic_distributionMapping.toDomain(account_analytic_distributiondto);
        domain.setId(account_analytic_distribution_id);
		account_analytic_distributionService.update(domain);
		Account_analytic_distributionDTO dto = account_analytic_distributionMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#account_analytic_distribution_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Account_analytic_distribution" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/account_analytic_distributions/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Account_analytic_distributionDTO> account_analytic_distributiondtos) {
        account_analytic_distributionService.updateBatch(account_analytic_distributionMapping.toDomain(account_analytic_distributiondtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission(#account_analytic_distribution_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Account_analytic_distribution" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/account_analytic_distributions/{account_analytic_distribution_id}")
    public ResponseEntity<Account_analytic_distributionDTO> get(@PathVariable("account_analytic_distribution_id") Integer account_analytic_distribution_id) {
        Account_analytic_distribution domain = account_analytic_distributionService.get(account_analytic_distribution_id);
        Account_analytic_distributionDTO dto = account_analytic_distributionMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Account_analytic_distribution" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/account_analytic_distributions/fetchdefault")
	public ResponseEntity<List<Account_analytic_distributionDTO>> fetchDefault(Account_analytic_distributionSearchContext context) {
        Page<Account_analytic_distribution> domains = account_analytic_distributionService.searchDefault(context) ;
        List<Account_analytic_distributionDTO> list = account_analytic_distributionMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Account_analytic_distribution" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/account_analytic_distributions/searchdefault")
	public ResponseEntity<Page<Account_analytic_distributionDTO>> searchDefault(Account_analytic_distributionSearchContext context) {
        Page<Account_analytic_distribution> domains = account_analytic_distributionService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(account_analytic_distributionMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Account_analytic_distribution getEntity(){
        return new Account_analytic_distribution();
    }

}
