package cn.ibizlab.odoo.odoo_account.mapping;

import org.mapstruct.*;
import cn.ibizlab.odoo.core.odoo_account.domain.Account_reconciliation_widget;
import cn.ibizlab.odoo.odoo_account.dto.Account_reconciliation_widgetDTO;
import cn.ibizlab.odoo.util.domain.MappingBase;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring", uses = {},
    nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
    nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface Account_reconciliation_widgetMapping extends MappingBase<Account_reconciliation_widgetDTO, Account_reconciliation_widget> {


}

