package cn.ibizlab.odoo.odoo_account.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_account.dto.*;
import cn.ibizlab.odoo.odoo_account.mapping.*;
import cn.ibizlab.odoo.core.odoo_account.domain.Account_invoice_report;
import cn.ibizlab.odoo.core.odoo_account.service.IAccount_invoice_reportService;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_invoice_reportSearchContext;




@Slf4j
@Api(tags = {"Account_invoice_report" })
@RestController("odoo_account-account_invoice_report")
@RequestMapping("")
public class Account_invoice_reportResource {

    @Autowired
    private IAccount_invoice_reportService account_invoice_reportService;

    @Autowired
    @Lazy
    private Account_invoice_reportMapping account_invoice_reportMapping;







    @PreAuthorize("hasPermission(#account_invoice_report_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Account_invoice_report" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/account_invoice_reports/{account_invoice_report_id}")
    public ResponseEntity<Account_invoice_reportDTO> get(@PathVariable("account_invoice_report_id") Integer account_invoice_report_id) {
        Account_invoice_report domain = account_invoice_reportService.get(account_invoice_report_id);
        Account_invoice_reportDTO dto = account_invoice_reportMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }










    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Account_invoice_report" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/account_invoice_reports")

    public ResponseEntity<Account_invoice_reportDTO> create(@RequestBody Account_invoice_reportDTO account_invoice_reportdto) {
        Account_invoice_report domain = account_invoice_reportMapping.toDomain(account_invoice_reportdto);
		account_invoice_reportService.create(domain);
        Account_invoice_reportDTO dto = account_invoice_reportMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Account_invoice_report" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/account_invoice_reports/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Account_invoice_reportDTO> account_invoice_reportdtos) {
        account_invoice_reportService.createBatch(account_invoice_reportMapping.toDomain(account_invoice_reportdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission('Remove',{#account_invoice_report_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Account_invoice_report" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/account_invoice_reports/{account_invoice_report_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("account_invoice_report_id") Integer account_invoice_report_id) {
         return ResponseEntity.status(HttpStatus.OK).body(account_invoice_reportService.remove(account_invoice_report_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Account_invoice_report" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/account_invoice_reports/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        account_invoice_reportService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission(#account_invoice_report_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Account_invoice_report" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/account_invoice_reports/{account_invoice_report_id}")

    public ResponseEntity<Account_invoice_reportDTO> update(@PathVariable("account_invoice_report_id") Integer account_invoice_report_id, @RequestBody Account_invoice_reportDTO account_invoice_reportdto) {
		Account_invoice_report domain = account_invoice_reportMapping.toDomain(account_invoice_reportdto);
        domain.setId(account_invoice_report_id);
		account_invoice_reportService.update(domain);
		Account_invoice_reportDTO dto = account_invoice_reportMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#account_invoice_report_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Account_invoice_report" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/account_invoice_reports/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Account_invoice_reportDTO> account_invoice_reportdtos) {
        account_invoice_reportService.updateBatch(account_invoice_reportMapping.toDomain(account_invoice_reportdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Account_invoice_report" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/account_invoice_reports/fetchdefault")
	public ResponseEntity<List<Account_invoice_reportDTO>> fetchDefault(Account_invoice_reportSearchContext context) {
        Page<Account_invoice_report> domains = account_invoice_reportService.searchDefault(context) ;
        List<Account_invoice_reportDTO> list = account_invoice_reportMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Account_invoice_report" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/account_invoice_reports/searchdefault")
	public ResponseEntity<Page<Account_invoice_reportDTO>> searchDefault(Account_invoice_reportSearchContext context) {
        Page<Account_invoice_report> domains = account_invoice_reportService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(account_invoice_reportMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Account_invoice_report getEntity(){
        return new Account_invoice_report();
    }

}
