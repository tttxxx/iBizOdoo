package cn.ibizlab.odoo.odoo_account.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_account.dto.*;
import cn.ibizlab.odoo.odoo_account.mapping.*;
import cn.ibizlab.odoo.core.odoo_account.domain.Account_fiscal_position;
import cn.ibizlab.odoo.core.odoo_account.service.IAccount_fiscal_positionService;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_fiscal_positionSearchContext;




@Slf4j
@Api(tags = {"Account_fiscal_position" })
@RestController("odoo_account-account_fiscal_position")
@RequestMapping("")
public class Account_fiscal_positionResource {

    @Autowired
    private IAccount_fiscal_positionService account_fiscal_positionService;

    @Autowired
    @Lazy
    private Account_fiscal_positionMapping account_fiscal_positionMapping;




    @PreAuthorize("hasPermission('Remove',{#account_fiscal_position_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Account_fiscal_position" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/account_fiscal_positions/{account_fiscal_position_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("account_fiscal_position_id") Integer account_fiscal_position_id) {
         return ResponseEntity.status(HttpStatus.OK).body(account_fiscal_positionService.remove(account_fiscal_position_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Account_fiscal_position" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/account_fiscal_positions/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        account_fiscal_positionService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }







    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Account_fiscal_position" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/account_fiscal_positions")

    public ResponseEntity<Account_fiscal_positionDTO> create(@RequestBody Account_fiscal_positionDTO account_fiscal_positiondto) {
        Account_fiscal_position domain = account_fiscal_positionMapping.toDomain(account_fiscal_positiondto);
		account_fiscal_positionService.create(domain);
        Account_fiscal_positionDTO dto = account_fiscal_positionMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Account_fiscal_position" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/account_fiscal_positions/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Account_fiscal_positionDTO> account_fiscal_positiondtos) {
        account_fiscal_positionService.createBatch(account_fiscal_positionMapping.toDomain(account_fiscal_positiondtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }







    @PreAuthorize("hasPermission(#account_fiscal_position_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Account_fiscal_position" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/account_fiscal_positions/{account_fiscal_position_id}")

    public ResponseEntity<Account_fiscal_positionDTO> update(@PathVariable("account_fiscal_position_id") Integer account_fiscal_position_id, @RequestBody Account_fiscal_positionDTO account_fiscal_positiondto) {
		Account_fiscal_position domain = account_fiscal_positionMapping.toDomain(account_fiscal_positiondto);
        domain.setId(account_fiscal_position_id);
		account_fiscal_positionService.update(domain);
		Account_fiscal_positionDTO dto = account_fiscal_positionMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#account_fiscal_position_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Account_fiscal_position" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/account_fiscal_positions/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Account_fiscal_positionDTO> account_fiscal_positiondtos) {
        account_fiscal_positionService.updateBatch(account_fiscal_positionMapping.toDomain(account_fiscal_positiondtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }







    @PreAuthorize("hasPermission(#account_fiscal_position_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Account_fiscal_position" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/account_fiscal_positions/{account_fiscal_position_id}")
    public ResponseEntity<Account_fiscal_positionDTO> get(@PathVariable("account_fiscal_position_id") Integer account_fiscal_position_id) {
        Account_fiscal_position domain = account_fiscal_positionService.get(account_fiscal_position_id);
        Account_fiscal_positionDTO dto = account_fiscal_positionMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Account_fiscal_position" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/account_fiscal_positions/fetchdefault")
	public ResponseEntity<List<Account_fiscal_positionDTO>> fetchDefault(Account_fiscal_positionSearchContext context) {
        Page<Account_fiscal_position> domains = account_fiscal_positionService.searchDefault(context) ;
        List<Account_fiscal_positionDTO> list = account_fiscal_positionMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Account_fiscal_position" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/account_fiscal_positions/searchdefault")
	public ResponseEntity<Page<Account_fiscal_positionDTO>> searchDefault(Account_fiscal_positionSearchContext context) {
        Page<Account_fiscal_position> domains = account_fiscal_positionService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(account_fiscal_positionMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Account_fiscal_position getEntity(){
        return new Account_fiscal_position();
    }

}
