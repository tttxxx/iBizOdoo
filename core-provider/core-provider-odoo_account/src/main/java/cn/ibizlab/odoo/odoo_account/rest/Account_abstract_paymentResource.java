package cn.ibizlab.odoo.odoo_account.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_account.dto.*;
import cn.ibizlab.odoo.odoo_account.mapping.*;
import cn.ibizlab.odoo.core.odoo_account.domain.Account_abstract_payment;
import cn.ibizlab.odoo.core.odoo_account.service.IAccount_abstract_paymentService;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_abstract_paymentSearchContext;




@Slf4j
@Api(tags = {"Account_abstract_payment" })
@RestController("odoo_account-account_abstract_payment")
@RequestMapping("")
public class Account_abstract_paymentResource {

    @Autowired
    private IAccount_abstract_paymentService account_abstract_paymentService;

    @Autowired
    @Lazy
    private Account_abstract_paymentMapping account_abstract_paymentMapping;










    @PreAuthorize("hasPermission(#account_abstract_payment_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Account_abstract_payment" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/account_abstract_payments/{account_abstract_payment_id}")

    public ResponseEntity<Account_abstract_paymentDTO> update(@PathVariable("account_abstract_payment_id") Integer account_abstract_payment_id, @RequestBody Account_abstract_paymentDTO account_abstract_paymentdto) {
		Account_abstract_payment domain = account_abstract_paymentMapping.toDomain(account_abstract_paymentdto);
        domain.setId(account_abstract_payment_id);
		account_abstract_paymentService.update(domain);
		Account_abstract_paymentDTO dto = account_abstract_paymentMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#account_abstract_payment_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Account_abstract_payment" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/account_abstract_payments/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Account_abstract_paymentDTO> account_abstract_paymentdtos) {
        account_abstract_paymentService.updateBatch(account_abstract_paymentMapping.toDomain(account_abstract_paymentdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }







    @PreAuthorize("hasPermission(#account_abstract_payment_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Account_abstract_payment" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/account_abstract_payments/{account_abstract_payment_id}")
    public ResponseEntity<Account_abstract_paymentDTO> get(@PathVariable("account_abstract_payment_id") Integer account_abstract_payment_id) {
        Account_abstract_payment domain = account_abstract_paymentService.get(account_abstract_payment_id);
        Account_abstract_paymentDTO dto = account_abstract_paymentMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }




    @PreAuthorize("hasPermission('Remove',{#account_abstract_payment_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Account_abstract_payment" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/account_abstract_payments/{account_abstract_payment_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("account_abstract_payment_id") Integer account_abstract_payment_id) {
         return ResponseEntity.status(HttpStatus.OK).body(account_abstract_paymentService.remove(account_abstract_payment_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Account_abstract_payment" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/account_abstract_payments/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        account_abstract_paymentService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Account_abstract_payment" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/account_abstract_payments")

    public ResponseEntity<Account_abstract_paymentDTO> create(@RequestBody Account_abstract_paymentDTO account_abstract_paymentdto) {
        Account_abstract_payment domain = account_abstract_paymentMapping.toDomain(account_abstract_paymentdto);
		account_abstract_paymentService.create(domain);
        Account_abstract_paymentDTO dto = account_abstract_paymentMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Account_abstract_payment" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/account_abstract_payments/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Account_abstract_paymentDTO> account_abstract_paymentdtos) {
        account_abstract_paymentService.createBatch(account_abstract_paymentMapping.toDomain(account_abstract_paymentdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Account_abstract_payment" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/account_abstract_payments/fetchdefault")
	public ResponseEntity<List<Account_abstract_paymentDTO>> fetchDefault(Account_abstract_paymentSearchContext context) {
        Page<Account_abstract_payment> domains = account_abstract_paymentService.searchDefault(context) ;
        List<Account_abstract_paymentDTO> list = account_abstract_paymentMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Account_abstract_payment" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/account_abstract_payments/searchdefault")
	public ResponseEntity<Page<Account_abstract_paymentDTO>> searchDefault(Account_abstract_paymentSearchContext context) {
        Page<Account_abstract_payment> domains = account_abstract_paymentService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(account_abstract_paymentMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Account_abstract_payment getEntity(){
        return new Account_abstract_payment();
    }

}
