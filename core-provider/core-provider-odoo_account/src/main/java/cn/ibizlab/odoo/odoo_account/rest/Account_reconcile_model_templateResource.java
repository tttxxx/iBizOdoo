package cn.ibizlab.odoo.odoo_account.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_account.dto.*;
import cn.ibizlab.odoo.odoo_account.mapping.*;
import cn.ibizlab.odoo.core.odoo_account.domain.Account_reconcile_model_template;
import cn.ibizlab.odoo.core.odoo_account.service.IAccount_reconcile_model_templateService;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_reconcile_model_templateSearchContext;




@Slf4j
@Api(tags = {"Account_reconcile_model_template" })
@RestController("odoo_account-account_reconcile_model_template")
@RequestMapping("")
public class Account_reconcile_model_templateResource {

    @Autowired
    private IAccount_reconcile_model_templateService account_reconcile_model_templateService;

    @Autowired
    @Lazy
    private Account_reconcile_model_templateMapping account_reconcile_model_templateMapping;







    @PreAuthorize("hasPermission(#account_reconcile_model_template_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Account_reconcile_model_template" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/account_reconcile_model_templates/{account_reconcile_model_template_id}")
    public ResponseEntity<Account_reconcile_model_templateDTO> get(@PathVariable("account_reconcile_model_template_id") Integer account_reconcile_model_template_id) {
        Account_reconcile_model_template domain = account_reconcile_model_templateService.get(account_reconcile_model_template_id);
        Account_reconcile_model_templateDTO dto = account_reconcile_model_templateMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }




    @PreAuthorize("hasPermission(#account_reconcile_model_template_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Account_reconcile_model_template" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/account_reconcile_model_templates/{account_reconcile_model_template_id}")

    public ResponseEntity<Account_reconcile_model_templateDTO> update(@PathVariable("account_reconcile_model_template_id") Integer account_reconcile_model_template_id, @RequestBody Account_reconcile_model_templateDTO account_reconcile_model_templatedto) {
		Account_reconcile_model_template domain = account_reconcile_model_templateMapping.toDomain(account_reconcile_model_templatedto);
        domain.setId(account_reconcile_model_template_id);
		account_reconcile_model_templateService.update(domain);
		Account_reconcile_model_templateDTO dto = account_reconcile_model_templateMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#account_reconcile_model_template_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Account_reconcile_model_template" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/account_reconcile_model_templates/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Account_reconcile_model_templateDTO> account_reconcile_model_templatedtos) {
        account_reconcile_model_templateService.updateBatch(account_reconcile_model_templateMapping.toDomain(account_reconcile_model_templatedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }










    @PreAuthorize("hasPermission('Remove',{#account_reconcile_model_template_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Account_reconcile_model_template" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/account_reconcile_model_templates/{account_reconcile_model_template_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("account_reconcile_model_template_id") Integer account_reconcile_model_template_id) {
         return ResponseEntity.status(HttpStatus.OK).body(account_reconcile_model_templateService.remove(account_reconcile_model_template_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Account_reconcile_model_template" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/account_reconcile_model_templates/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        account_reconcile_model_templateService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Account_reconcile_model_template" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/account_reconcile_model_templates")

    public ResponseEntity<Account_reconcile_model_templateDTO> create(@RequestBody Account_reconcile_model_templateDTO account_reconcile_model_templatedto) {
        Account_reconcile_model_template domain = account_reconcile_model_templateMapping.toDomain(account_reconcile_model_templatedto);
		account_reconcile_model_templateService.create(domain);
        Account_reconcile_model_templateDTO dto = account_reconcile_model_templateMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Account_reconcile_model_template" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/account_reconcile_model_templates/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Account_reconcile_model_templateDTO> account_reconcile_model_templatedtos) {
        account_reconcile_model_templateService.createBatch(account_reconcile_model_templateMapping.toDomain(account_reconcile_model_templatedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Account_reconcile_model_template" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/account_reconcile_model_templates/fetchdefault")
	public ResponseEntity<List<Account_reconcile_model_templateDTO>> fetchDefault(Account_reconcile_model_templateSearchContext context) {
        Page<Account_reconcile_model_template> domains = account_reconcile_model_templateService.searchDefault(context) ;
        List<Account_reconcile_model_templateDTO> list = account_reconcile_model_templateMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Account_reconcile_model_template" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/account_reconcile_model_templates/searchdefault")
	public ResponseEntity<Page<Account_reconcile_model_templateDTO>> searchDefault(Account_reconcile_model_templateSearchContext context) {
        Page<Account_reconcile_model_template> domains = account_reconcile_model_templateService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(account_reconcile_model_templateMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Account_reconcile_model_template getEntity(){
        return new Account_reconcile_model_template();
    }

}
