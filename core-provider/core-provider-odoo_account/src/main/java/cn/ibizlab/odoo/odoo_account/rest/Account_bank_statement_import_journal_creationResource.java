package cn.ibizlab.odoo.odoo_account.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_account.dto.*;
import cn.ibizlab.odoo.odoo_account.mapping.*;
import cn.ibizlab.odoo.core.odoo_account.domain.Account_bank_statement_import_journal_creation;
import cn.ibizlab.odoo.core.odoo_account.service.IAccount_bank_statement_import_journal_creationService;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_bank_statement_import_journal_creationSearchContext;




@Slf4j
@Api(tags = {"Account_bank_statement_import_journal_creation" })
@RestController("odoo_account-account_bank_statement_import_journal_creation")
@RequestMapping("")
public class Account_bank_statement_import_journal_creationResource {

    @Autowired
    private IAccount_bank_statement_import_journal_creationService account_bank_statement_import_journal_creationService;

    @Autowired
    @Lazy
    private Account_bank_statement_import_journal_creationMapping account_bank_statement_import_journal_creationMapping;




    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Account_bank_statement_import_journal_creation" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/account_bank_statement_import_journal_creations")

    public ResponseEntity<Account_bank_statement_import_journal_creationDTO> create(@RequestBody Account_bank_statement_import_journal_creationDTO account_bank_statement_import_journal_creationdto) {
        Account_bank_statement_import_journal_creation domain = account_bank_statement_import_journal_creationMapping.toDomain(account_bank_statement_import_journal_creationdto);
		account_bank_statement_import_journal_creationService.create(domain);
        Account_bank_statement_import_journal_creationDTO dto = account_bank_statement_import_journal_creationMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Account_bank_statement_import_journal_creation" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/account_bank_statement_import_journal_creations/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Account_bank_statement_import_journal_creationDTO> account_bank_statement_import_journal_creationdtos) {
        account_bank_statement_import_journal_creationService.createBatch(account_bank_statement_import_journal_creationMapping.toDomain(account_bank_statement_import_journal_creationdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission('Remove',{#account_bank_statement_import_journal_creation_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Account_bank_statement_import_journal_creation" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/account_bank_statement_import_journal_creations/{account_bank_statement_import_journal_creation_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("account_bank_statement_import_journal_creation_id") Integer account_bank_statement_import_journal_creation_id) {
         return ResponseEntity.status(HttpStatus.OK).body(account_bank_statement_import_journal_creationService.remove(account_bank_statement_import_journal_creation_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Account_bank_statement_import_journal_creation" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/account_bank_statement_import_journal_creations/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        account_bank_statement_import_journal_creationService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }







    @PreAuthorize("hasPermission(#account_bank_statement_import_journal_creation_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Account_bank_statement_import_journal_creation" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/account_bank_statement_import_journal_creations/{account_bank_statement_import_journal_creation_id}")

    public ResponseEntity<Account_bank_statement_import_journal_creationDTO> update(@PathVariable("account_bank_statement_import_journal_creation_id") Integer account_bank_statement_import_journal_creation_id, @RequestBody Account_bank_statement_import_journal_creationDTO account_bank_statement_import_journal_creationdto) {
		Account_bank_statement_import_journal_creation domain = account_bank_statement_import_journal_creationMapping.toDomain(account_bank_statement_import_journal_creationdto);
        domain.setId(account_bank_statement_import_journal_creation_id);
		account_bank_statement_import_journal_creationService.update(domain);
		Account_bank_statement_import_journal_creationDTO dto = account_bank_statement_import_journal_creationMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#account_bank_statement_import_journal_creation_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Account_bank_statement_import_journal_creation" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/account_bank_statement_import_journal_creations/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Account_bank_statement_import_journal_creationDTO> account_bank_statement_import_journal_creationdtos) {
        account_bank_statement_import_journal_creationService.updateBatch(account_bank_statement_import_journal_creationMapping.toDomain(account_bank_statement_import_journal_creationdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission(#account_bank_statement_import_journal_creation_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Account_bank_statement_import_journal_creation" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/account_bank_statement_import_journal_creations/{account_bank_statement_import_journal_creation_id}")
    public ResponseEntity<Account_bank_statement_import_journal_creationDTO> get(@PathVariable("account_bank_statement_import_journal_creation_id") Integer account_bank_statement_import_journal_creation_id) {
        Account_bank_statement_import_journal_creation domain = account_bank_statement_import_journal_creationService.get(account_bank_statement_import_journal_creation_id);
        Account_bank_statement_import_journal_creationDTO dto = account_bank_statement_import_journal_creationMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }







    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Account_bank_statement_import_journal_creation" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/account_bank_statement_import_journal_creations/fetchdefault")
	public ResponseEntity<List<Account_bank_statement_import_journal_creationDTO>> fetchDefault(Account_bank_statement_import_journal_creationSearchContext context) {
        Page<Account_bank_statement_import_journal_creation> domains = account_bank_statement_import_journal_creationService.searchDefault(context) ;
        List<Account_bank_statement_import_journal_creationDTO> list = account_bank_statement_import_journal_creationMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Account_bank_statement_import_journal_creation" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/account_bank_statement_import_journal_creations/searchdefault")
	public ResponseEntity<Page<Account_bank_statement_import_journal_creationDTO>> searchDefault(Account_bank_statement_import_journal_creationSearchContext context) {
        Page<Account_bank_statement_import_journal_creation> domains = account_bank_statement_import_journal_creationService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(account_bank_statement_import_journal_creationMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Account_bank_statement_import_journal_creation getEntity(){
        return new Account_bank_statement_import_journal_creation();
    }

}
