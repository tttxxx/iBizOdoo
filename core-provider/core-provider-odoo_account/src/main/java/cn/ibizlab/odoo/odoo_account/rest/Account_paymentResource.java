package cn.ibizlab.odoo.odoo_account.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_account.dto.*;
import cn.ibizlab.odoo.odoo_account.mapping.*;
import cn.ibizlab.odoo.core.odoo_account.domain.Account_payment;
import cn.ibizlab.odoo.core.odoo_account.service.IAccount_paymentService;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_paymentSearchContext;




@Slf4j
@Api(tags = {"Account_payment" })
@RestController("odoo_account-account_payment")
@RequestMapping("")
public class Account_paymentResource {

    @Autowired
    private IAccount_paymentService account_paymentService;

    @Autowired
    @Lazy
    private Account_paymentMapping account_paymentMapping;




    @PreAuthorize("hasPermission(#account_payment_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Account_payment" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/account_payments/{account_payment_id}")

    public ResponseEntity<Account_paymentDTO> update(@PathVariable("account_payment_id") Integer account_payment_id, @RequestBody Account_paymentDTO account_paymentdto) {
		Account_payment domain = account_paymentMapping.toDomain(account_paymentdto);
        domain.setId(account_payment_id);
		account_paymentService.update(domain);
		Account_paymentDTO dto = account_paymentMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#account_payment_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Account_payment" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/account_payments/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Account_paymentDTO> account_paymentdtos) {
        account_paymentService.updateBatch(account_paymentMapping.toDomain(account_paymentdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission('Remove',{#account_payment_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Account_payment" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/account_payments/{account_payment_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("account_payment_id") Integer account_payment_id) {
         return ResponseEntity.status(HttpStatus.OK).body(account_paymentService.remove(account_payment_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Account_payment" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/account_payments/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        account_paymentService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }










    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Account_payment" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/account_payments")

    public ResponseEntity<Account_paymentDTO> create(@RequestBody Account_paymentDTO account_paymentdto) {
        Account_payment domain = account_paymentMapping.toDomain(account_paymentdto);
		account_paymentService.create(domain);
        Account_paymentDTO dto = account_paymentMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Account_payment" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/account_payments/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Account_paymentDTO> account_paymentdtos) {
        account_paymentService.createBatch(account_paymentMapping.toDomain(account_paymentdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission(#account_payment_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Account_payment" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/account_payments/{account_payment_id}")
    public ResponseEntity<Account_paymentDTO> get(@PathVariable("account_payment_id") Integer account_payment_id) {
        Account_payment domain = account_paymentService.get(account_payment_id);
        Account_paymentDTO dto = account_paymentMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }




    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Account_payment" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/account_payments/fetchdefault")
	public ResponseEntity<List<Account_paymentDTO>> fetchDefault(Account_paymentSearchContext context) {
        Page<Account_payment> domains = account_paymentService.searchDefault(context) ;
        List<Account_paymentDTO> list = account_paymentMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Account_payment" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/account_payments/searchdefault")
	public ResponseEntity<Page<Account_paymentDTO>> searchDefault(Account_paymentSearchContext context) {
        Page<Account_payment> domains = account_paymentService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(account_paymentMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Account_payment getEntity(){
        return new Account_payment();
    }

}
