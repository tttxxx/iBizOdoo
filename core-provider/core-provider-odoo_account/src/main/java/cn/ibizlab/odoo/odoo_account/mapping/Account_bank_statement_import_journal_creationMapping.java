package cn.ibizlab.odoo.odoo_account.mapping;

import org.mapstruct.*;
import cn.ibizlab.odoo.core.odoo_account.domain.Account_bank_statement_import_journal_creation;
import cn.ibizlab.odoo.odoo_account.dto.Account_bank_statement_import_journal_creationDTO;
import cn.ibizlab.odoo.util.domain.MappingBase;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring", uses = {},
    nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
    nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface Account_bank_statement_import_journal_creationMapping extends MappingBase<Account_bank_statement_import_journal_creationDTO, Account_bank_statement_import_journal_creation> {


}

