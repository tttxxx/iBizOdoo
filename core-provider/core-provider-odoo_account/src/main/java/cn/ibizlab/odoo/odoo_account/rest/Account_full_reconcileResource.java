package cn.ibizlab.odoo.odoo_account.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_account.dto.*;
import cn.ibizlab.odoo.odoo_account.mapping.*;
import cn.ibizlab.odoo.core.odoo_account.domain.Account_full_reconcile;
import cn.ibizlab.odoo.core.odoo_account.service.IAccount_full_reconcileService;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_full_reconcileSearchContext;




@Slf4j
@Api(tags = {"Account_full_reconcile" })
@RestController("odoo_account-account_full_reconcile")
@RequestMapping("")
public class Account_full_reconcileResource {

    @Autowired
    private IAccount_full_reconcileService account_full_reconcileService;

    @Autowired
    @Lazy
    private Account_full_reconcileMapping account_full_reconcileMapping;




    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Account_full_reconcile" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/account_full_reconciles")

    public ResponseEntity<Account_full_reconcileDTO> create(@RequestBody Account_full_reconcileDTO account_full_reconciledto) {
        Account_full_reconcile domain = account_full_reconcileMapping.toDomain(account_full_reconciledto);
		account_full_reconcileService.create(domain);
        Account_full_reconcileDTO dto = account_full_reconcileMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Account_full_reconcile" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/account_full_reconciles/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Account_full_reconcileDTO> account_full_reconciledtos) {
        account_full_reconcileService.createBatch(account_full_reconcileMapping.toDomain(account_full_reconciledtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }







    @PreAuthorize("hasPermission('Remove',{#account_full_reconcile_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Account_full_reconcile" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/account_full_reconciles/{account_full_reconcile_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("account_full_reconcile_id") Integer account_full_reconcile_id) {
         return ResponseEntity.status(HttpStatus.OK).body(account_full_reconcileService.remove(account_full_reconcile_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Account_full_reconcile" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/account_full_reconciles/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        account_full_reconcileService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }







    @PreAuthorize("hasPermission(#account_full_reconcile_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Account_full_reconcile" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/account_full_reconciles/{account_full_reconcile_id}")
    public ResponseEntity<Account_full_reconcileDTO> get(@PathVariable("account_full_reconcile_id") Integer account_full_reconcile_id) {
        Account_full_reconcile domain = account_full_reconcileService.get(account_full_reconcile_id);
        Account_full_reconcileDTO dto = account_full_reconcileMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }




    @PreAuthorize("hasPermission(#account_full_reconcile_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Account_full_reconcile" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/account_full_reconciles/{account_full_reconcile_id}")

    public ResponseEntity<Account_full_reconcileDTO> update(@PathVariable("account_full_reconcile_id") Integer account_full_reconcile_id, @RequestBody Account_full_reconcileDTO account_full_reconciledto) {
		Account_full_reconcile domain = account_full_reconcileMapping.toDomain(account_full_reconciledto);
        domain.setId(account_full_reconcile_id);
		account_full_reconcileService.update(domain);
		Account_full_reconcileDTO dto = account_full_reconcileMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#account_full_reconcile_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Account_full_reconcile" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/account_full_reconciles/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Account_full_reconcileDTO> account_full_reconciledtos) {
        account_full_reconcileService.updateBatch(account_full_reconcileMapping.toDomain(account_full_reconciledtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Account_full_reconcile" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/account_full_reconciles/fetchdefault")
	public ResponseEntity<List<Account_full_reconcileDTO>> fetchDefault(Account_full_reconcileSearchContext context) {
        Page<Account_full_reconcile> domains = account_full_reconcileService.searchDefault(context) ;
        List<Account_full_reconcileDTO> list = account_full_reconcileMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Account_full_reconcile" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/account_full_reconciles/searchdefault")
	public ResponseEntity<Page<Account_full_reconcileDTO>> searchDefault(Account_full_reconcileSearchContext context) {
        Page<Account_full_reconcile> domains = account_full_reconcileService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(account_full_reconcileMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Account_full_reconcile getEntity(){
        return new Account_full_reconcile();
    }

}
