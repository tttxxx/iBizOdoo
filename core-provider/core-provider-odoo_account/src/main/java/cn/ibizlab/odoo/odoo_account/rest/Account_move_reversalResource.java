package cn.ibizlab.odoo.odoo_account.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_account.dto.*;
import cn.ibizlab.odoo.odoo_account.mapping.*;
import cn.ibizlab.odoo.core.odoo_account.domain.Account_move_reversal;
import cn.ibizlab.odoo.core.odoo_account.service.IAccount_move_reversalService;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_move_reversalSearchContext;




@Slf4j
@Api(tags = {"Account_move_reversal" })
@RestController("odoo_account-account_move_reversal")
@RequestMapping("")
public class Account_move_reversalResource {

    @Autowired
    private IAccount_move_reversalService account_move_reversalService;

    @Autowired
    @Lazy
    private Account_move_reversalMapping account_move_reversalMapping;




    @PreAuthorize("hasPermission(#account_move_reversal_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Account_move_reversal" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/account_move_reversals/{account_move_reversal_id}")
    public ResponseEntity<Account_move_reversalDTO> get(@PathVariable("account_move_reversal_id") Integer account_move_reversal_id) {
        Account_move_reversal domain = account_move_reversalService.get(account_move_reversal_id);
        Account_move_reversalDTO dto = account_move_reversalMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }













    @PreAuthorize("hasPermission(#account_move_reversal_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Account_move_reversal" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/account_move_reversals/{account_move_reversal_id}")

    public ResponseEntity<Account_move_reversalDTO> update(@PathVariable("account_move_reversal_id") Integer account_move_reversal_id, @RequestBody Account_move_reversalDTO account_move_reversaldto) {
		Account_move_reversal domain = account_move_reversalMapping.toDomain(account_move_reversaldto);
        domain.setId(account_move_reversal_id);
		account_move_reversalService.update(domain);
		Account_move_reversalDTO dto = account_move_reversalMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#account_move_reversal_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Account_move_reversal" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/account_move_reversals/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Account_move_reversalDTO> account_move_reversaldtos) {
        account_move_reversalService.updateBatch(account_move_reversalMapping.toDomain(account_move_reversaldtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission('Remove',{#account_move_reversal_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Account_move_reversal" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/account_move_reversals/{account_move_reversal_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("account_move_reversal_id") Integer account_move_reversal_id) {
         return ResponseEntity.status(HttpStatus.OK).body(account_move_reversalService.remove(account_move_reversal_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Account_move_reversal" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/account_move_reversals/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        account_move_reversalService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Account_move_reversal" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/account_move_reversals")

    public ResponseEntity<Account_move_reversalDTO> create(@RequestBody Account_move_reversalDTO account_move_reversaldto) {
        Account_move_reversal domain = account_move_reversalMapping.toDomain(account_move_reversaldto);
		account_move_reversalService.create(domain);
        Account_move_reversalDTO dto = account_move_reversalMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Account_move_reversal" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/account_move_reversals/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Account_move_reversalDTO> account_move_reversaldtos) {
        account_move_reversalService.createBatch(account_move_reversalMapping.toDomain(account_move_reversaldtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Account_move_reversal" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/account_move_reversals/fetchdefault")
	public ResponseEntity<List<Account_move_reversalDTO>> fetchDefault(Account_move_reversalSearchContext context) {
        Page<Account_move_reversal> domains = account_move_reversalService.searchDefault(context) ;
        List<Account_move_reversalDTO> list = account_move_reversalMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Account_move_reversal" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/account_move_reversals/searchdefault")
	public ResponseEntity<Page<Account_move_reversalDTO>> searchDefault(Account_move_reversalSearchContext context) {
        Page<Account_move_reversal> domains = account_move_reversalService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(account_move_reversalMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Account_move_reversal getEntity(){
        return new Account_move_reversal();
    }

}
