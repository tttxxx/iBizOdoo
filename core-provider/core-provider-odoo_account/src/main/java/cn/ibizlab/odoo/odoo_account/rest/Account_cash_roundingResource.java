package cn.ibizlab.odoo.odoo_account.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_account.dto.*;
import cn.ibizlab.odoo.odoo_account.mapping.*;
import cn.ibizlab.odoo.core.odoo_account.domain.Account_cash_rounding;
import cn.ibizlab.odoo.core.odoo_account.service.IAccount_cash_roundingService;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_cash_roundingSearchContext;




@Slf4j
@Api(tags = {"Account_cash_rounding" })
@RestController("odoo_account-account_cash_rounding")
@RequestMapping("")
public class Account_cash_roundingResource {

    @Autowired
    private IAccount_cash_roundingService account_cash_roundingService;

    @Autowired
    @Lazy
    private Account_cash_roundingMapping account_cash_roundingMapping;




    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Account_cash_rounding" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/account_cash_roundings")

    public ResponseEntity<Account_cash_roundingDTO> create(@RequestBody Account_cash_roundingDTO account_cash_roundingdto) {
        Account_cash_rounding domain = account_cash_roundingMapping.toDomain(account_cash_roundingdto);
		account_cash_roundingService.create(domain);
        Account_cash_roundingDTO dto = account_cash_roundingMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Account_cash_rounding" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/account_cash_roundings/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Account_cash_roundingDTO> account_cash_roundingdtos) {
        account_cash_roundingService.createBatch(account_cash_roundingMapping.toDomain(account_cash_roundingdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission(#account_cash_rounding_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Account_cash_rounding" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/account_cash_roundings/{account_cash_rounding_id}")
    public ResponseEntity<Account_cash_roundingDTO> get(@PathVariable("account_cash_rounding_id") Integer account_cash_rounding_id) {
        Account_cash_rounding domain = account_cash_roundingService.get(account_cash_rounding_id);
        Account_cash_roundingDTO dto = account_cash_roundingMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }




    @PreAuthorize("hasPermission('Remove',{#account_cash_rounding_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Account_cash_rounding" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/account_cash_roundings/{account_cash_rounding_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("account_cash_rounding_id") Integer account_cash_rounding_id) {
         return ResponseEntity.status(HttpStatus.OK).body(account_cash_roundingService.remove(account_cash_rounding_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Account_cash_rounding" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/account_cash_roundings/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        account_cash_roundingService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }










    @PreAuthorize("hasPermission(#account_cash_rounding_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Account_cash_rounding" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/account_cash_roundings/{account_cash_rounding_id}")

    public ResponseEntity<Account_cash_roundingDTO> update(@PathVariable("account_cash_rounding_id") Integer account_cash_rounding_id, @RequestBody Account_cash_roundingDTO account_cash_roundingdto) {
		Account_cash_rounding domain = account_cash_roundingMapping.toDomain(account_cash_roundingdto);
        domain.setId(account_cash_rounding_id);
		account_cash_roundingService.update(domain);
		Account_cash_roundingDTO dto = account_cash_roundingMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#account_cash_rounding_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Account_cash_rounding" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/account_cash_roundings/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Account_cash_roundingDTO> account_cash_roundingdtos) {
        account_cash_roundingService.updateBatch(account_cash_roundingMapping.toDomain(account_cash_roundingdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Account_cash_rounding" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/account_cash_roundings/fetchdefault")
	public ResponseEntity<List<Account_cash_roundingDTO>> fetchDefault(Account_cash_roundingSearchContext context) {
        Page<Account_cash_rounding> domains = account_cash_roundingService.searchDefault(context) ;
        List<Account_cash_roundingDTO> list = account_cash_roundingMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Account_cash_rounding" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/account_cash_roundings/searchdefault")
	public ResponseEntity<Page<Account_cash_roundingDTO>> searchDefault(Account_cash_roundingSearchContext context) {
        Page<Account_cash_rounding> domains = account_cash_roundingService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(account_cash_roundingMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Account_cash_rounding getEntity(){
        return new Account_cash_rounding();
    }

}
