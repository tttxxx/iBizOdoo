package cn.ibizlab.odoo.odoo_account.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;

import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import cn.ibizlab.odoo.odoo_account.dto.*;
import cn.ibizlab.odoo.odoo_account.mapping.*;
import cn.ibizlab.odoo.core.odoo_account.domain.Account_common_journal_report;
import cn.ibizlab.odoo.core.odoo_account.service.IAccount_common_journal_reportService;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_common_journal_reportSearchContext;




@Slf4j
@Api(tags = {"Account_common_journal_report" })
@RestController("odoo_account-account_common_journal_report")
@RequestMapping("")
public class Account_common_journal_reportResource {

    @Autowired
    private IAccount_common_journal_reportService account_common_journal_reportService;

    @Autowired
    @Lazy
    private Account_common_journal_reportMapping account_common_journal_reportMapping;







    @PreAuthorize("hasPermission(#account_common_journal_report_id,'Get',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "获取数据", tags = {"Account_common_journal_report" },  notes = "获取数据")
	@RequestMapping(method = RequestMethod.GET, value = "/account_common_journal_reports/{account_common_journal_report_id}")
    public ResponseEntity<Account_common_journal_reportDTO> get(@PathVariable("account_common_journal_report_id") Integer account_common_journal_report_id) {
        Account_common_journal_report domain = account_common_journal_reportService.get(account_common_journal_report_id);
        Account_common_journal_reportDTO dto = account_common_journal_reportMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }




    @PreAuthorize("hasPermission(#account_common_journal_report_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "更新数据", tags = {"Account_common_journal_report" },  notes = "更新数据")
	@RequestMapping(method = RequestMethod.PUT, value = "/account_common_journal_reports/{account_common_journal_report_id}")

    public ResponseEntity<Account_common_journal_reportDTO> update(@PathVariable("account_common_journal_report_id") Integer account_common_journal_report_id, @RequestBody Account_common_journal_reportDTO account_common_journal_reportdto) {
		Account_common_journal_report domain = account_common_journal_reportMapping.toDomain(account_common_journal_reportdto);
        domain.setId(account_common_journal_report_id);
		account_common_journal_reportService.update(domain);
		Account_common_journal_reportDTO dto = account_common_journal_reportMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(#account_common_journal_report_id,'Update',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "UpdateBatch", tags = {"Account_common_journal_report" },  notes = "UpdateBatch")
	@RequestMapping(method = RequestMethod.PUT, value = "/account_common_journal_reports/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<Account_common_journal_reportDTO> account_common_journal_reportdtos) {
        account_common_journal_reportService.updateBatch(account_common_journal_reportMapping.toDomain(account_common_journal_reportdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }










    @PreAuthorize("hasPermission('Remove',{#account_common_journal_report_id,{this.getEntity(),'ServiceApi'}})")
    @ApiOperation(value = "删除数据", tags = {"Account_common_journal_report" },  notes = "删除数据")
	@RequestMapping(method = RequestMethod.DELETE, value = "/account_common_journal_reports/{account_common_journal_report_id}")

    public ResponseEntity<Boolean> remove(@PathVariable("account_common_journal_report_id") Integer account_common_journal_report_id) {
         return ResponseEntity.status(HttpStatus.OK).body(account_common_journal_reportService.remove(account_common_journal_report_id));
    }

    @ApiOperation(value = "RemoveBatch", tags = {"Account_common_journal_report" },  notes = "RemoveBatch")
	@RequestMapping(method = RequestMethod.DELETE, value = "/account_common_journal_reports/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<Integer> ids) {
        account_common_journal_reportService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }




    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "建立数据", tags = {"Account_common_journal_report" },  notes = "建立数据")
	@RequestMapping(method = RequestMethod.POST, value = "/account_common_journal_reports")

    public ResponseEntity<Account_common_journal_reportDTO> create(@RequestBody Account_common_journal_reportDTO account_common_journal_reportdto) {
        Account_common_journal_report domain = account_common_journal_reportMapping.toDomain(account_common_journal_reportdto);
		account_common_journal_reportService.create(domain);
        Account_common_journal_reportDTO dto = account_common_journal_reportMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission('','Create',{this.getEntity(),'ServiceApi'})")
    @ApiOperation(value = "createBatch", tags = {"Account_common_journal_report" },  notes = "createBatch")
	@RequestMapping(method = RequestMethod.POST, value = "/account_common_journal_reports/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<Account_common_journal_reportDTO> account_common_journal_reportdtos) {
        account_common_journal_reportService.createBatch(account_common_journal_reportMapping.toDomain(account_common_journal_reportdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "fetch默认查询", tags = {"Account_common_journal_report" } ,notes = "fetch默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/account_common_journal_reports/fetchdefault")
	public ResponseEntity<List<Account_common_journal_reportDTO>> fetchDefault(Account_common_journal_reportSearchContext context) {
        Page<Account_common_journal_report> domains = account_common_journal_reportService.searchDefault(context) ;
        List<Account_common_journal_reportDTO> list = account_common_journal_reportMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasPermission('Get',{#context,'Default',this.getEntity(),'ServiceApi'})")
	@ApiOperation(value = "search默认查询", tags = {"Account_common_journal_report" } ,notes = "search默认查询")
    @RequestMapping(method= RequestMethod.GET , value="/account_common_journal_reports/searchdefault")
	public ResponseEntity<Page<Account_common_journal_reportDTO>> searchDefault(Account_common_journal_reportSearchContext context) {
        Page<Account_common_journal_report> domains = account_common_journal_reportService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(account_common_journal_reportMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}


    /**
     * 用户权限校验
     * @return
     */
	public Account_common_journal_report getEntity(){
        return new Account_common_journal_report();
    }

}
