package cn.ibizlab.odoo.odoo_account.mapping;

import org.mapstruct.*;
import cn.ibizlab.odoo.core.odoo_account.domain.Account_fiscal_position_tax_template;
import cn.ibizlab.odoo.odoo_account.dto.Account_fiscal_position_tax_templateDTO;
import cn.ibizlab.odoo.util.domain.MappingBase;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring", uses = {},
    nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
    nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface Account_fiscal_position_tax_templateMapping extends MappingBase<Account_fiscal_position_tax_templateDTO, Account_fiscal_position_tax_template> {


}

