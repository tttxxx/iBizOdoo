package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Base_import_tests_models_m2o;
import cn.ibizlab.odoo.core.odoo_base_import.filter.Base_import_tests_models_m2oSearchContext;

/**
 * 实体 [测试:基本导入模型，多对一] 存储对象
 */
public interface Base_import_tests_models_m2oRepository extends Repository<Base_import_tests_models_m2o> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Base_import_tests_models_m2o> searchDefault(Base_import_tests_models_m2oSearchContext context);

    Base_import_tests_models_m2o convert2PO(cn.ibizlab.odoo.core.odoo_base_import.domain.Base_import_tests_models_m2o domain , Base_import_tests_models_m2o po) ;

    cn.ibizlab.odoo.core.odoo_base_import.domain.Base_import_tests_models_m2o convert2Domain( Base_import_tests_models_m2o po ,cn.ibizlab.odoo.core.odoo_base_import.domain.Base_import_tests_models_m2o domain) ;

}
