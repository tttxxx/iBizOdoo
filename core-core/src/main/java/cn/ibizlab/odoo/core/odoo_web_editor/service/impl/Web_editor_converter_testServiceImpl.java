package cn.ibizlab.odoo.core.odoo_web_editor.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_web_editor.domain.Web_editor_converter_test;
import cn.ibizlab.odoo.core.odoo_web_editor.filter.Web_editor_converter_testSearchContext;
import cn.ibizlab.odoo.core.odoo_web_editor.service.IWeb_editor_converter_testService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;



import cn.ibizlab.odoo.core.odoo_web_editor.client.web_editor_converter_testOdooClient;
import cn.ibizlab.odoo.core.odoo_web_editor.clientmodel.web_editor_converter_testClientModel;

/**
 * 实体[Web编辑器转换器测试] 服务对象接口实现
 */
@Slf4j
@Service
public class Web_editor_converter_testServiceImpl implements IWeb_editor_converter_testService {

    @Autowired
    web_editor_converter_testOdooClient web_editor_converter_testOdooClient;


    @Override
    public boolean create(Web_editor_converter_test et) {
        web_editor_converter_testClientModel clientModel = convert2Model(et,null);
		web_editor_converter_testOdooClient.create(clientModel);
        Web_editor_converter_test rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Web_editor_converter_test> list){
    }

    @Override
    public Web_editor_converter_test get(Integer id) {
        web_editor_converter_testClientModel clientModel = new web_editor_converter_testClientModel();
        clientModel.setId(id);
		web_editor_converter_testOdooClient.get(clientModel);
        Web_editor_converter_test et = convert2Domain(clientModel,null);
        if(et==null){
            et=new Web_editor_converter_test();
            et.setId(id);
        }
        else{
        }

        return  et;
    }

    @Override
    public boolean remove(Integer id) {
        web_editor_converter_testClientModel clientModel = new web_editor_converter_testClientModel();
        clientModel.setId(id);
		web_editor_converter_testOdooClient.remove(clientModel);
        return true; 
    }

    public void removeBatch(Collection<Integer> idList){
    }

    @Override
    public boolean update(Web_editor_converter_test et) {
        web_editor_converter_testClientModel clientModel = convert2Model(et,null);
		web_editor_converter_testOdooClient.update(clientModel);
        Web_editor_converter_test rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }
    
    public void updateBatch(List<Web_editor_converter_test> list){
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Web_editor_converter_test> searchDefault(Web_editor_converter_testSearchContext context) {
        List<Web_editor_converter_test> list = new ArrayList<Web_editor_converter_test>();
        Page<web_editor_converter_testClientModel> clientModelList = web_editor_converter_testOdooClient.search(context);
        for(web_editor_converter_testClientModel clientModel : clientModelList){
            list.add( this.convert2Domain(clientModel,null));
        }
        return new PageImpl<Web_editor_converter_test>(list, context.getPageable(), clientModelList.getTotalElements());
    }

    public web_editor_converter_testClientModel convert2Model(Web_editor_converter_test domain , web_editor_converter_testClientModel model) {
        if(domain == null)
            return model ;
        if(model==null){
            model = new web_editor_converter_testClientModel();

        }
        if((Boolean) domain.getExtensionparams().get("dirtyflagenable")){
            if((Boolean) domain.getExtensionparams().get("textdirtyflag"))
                model.setText(domain.getText());
            if((Boolean) domain.getExtensionparams().get("create_datedirtyflag"))
                model.setCreate_date(domain.getCreateDate());
            if((Boolean) domain.getExtensionparams().get("selection_strdirtyflag"))
                model.setSelection_str(domain.getSelectionStr());
            if((Boolean) domain.getExtensionparams().get("numericdirtyflag"))
                model.setNumeric(domain.getNumeric());
            if((Boolean) domain.getExtensionparams().get("integerdirtyflag"))
                model.setInteger(domain.getInteger());
            if((Boolean) domain.getExtensionparams().get("binarydirtyflag"))
                model.setBinary(domain.getBinary());
            if((Boolean) domain.getExtensionparams().get("datedirtyflag"))
                model.setDate(domain.getDate());
            if((Boolean) domain.getExtensionparams().get("ibizfloatdirtyflag"))
                model.setIbizfloat(domain.getIbizfloat());
            if((Boolean) domain.getExtensionparams().get("__last_updatedirtyflag"))
                model.set__last_update(domain.getLastUpdate());
            if((Boolean) domain.getExtensionparams().get("datetimedirtyflag"))
                model.setDatetime(domain.getDatetime());
            if((Boolean) domain.getExtensionparams().get("iddirtyflag"))
                model.setId(domain.getId());
            if((Boolean) domain.getExtensionparams().get("write_datedirtyflag"))
                model.setWrite_date(domain.getWriteDate());
            if((Boolean) domain.getExtensionparams().get("htmldirtyflag"))
                model.setHtml(domain.getHtml());
            if((Boolean) domain.getExtensionparams().get("display_namedirtyflag"))
                model.setDisplay_name(domain.getDisplayName());
            if((Boolean) domain.getExtensionparams().get("ibizchardirtyflag"))
                model.setIbizchar(domain.getIbizchar());
            if((Boolean) domain.getExtensionparams().get("selectiondirtyflag"))
                model.setSelection(domain.getSelection());
            if((Boolean) domain.getExtensionparams().get("create_uid_textdirtyflag"))
                model.setCreate_uid_text(domain.getCreateUidText());
            if((Boolean) domain.getExtensionparams().get("write_uid_textdirtyflag"))
                model.setWrite_uid_text(domain.getWriteUidText());
            if((Boolean) domain.getExtensionparams().get("many2one_textdirtyflag"))
                model.setMany2one_text(domain.getMany2oneText());
            if((Boolean) domain.getExtensionparams().get("write_uiddirtyflag"))
                model.setWrite_uid(domain.getWriteUid());
            if((Boolean) domain.getExtensionparams().get("many2onedirtyflag"))
                model.setMany2one(domain.getMany2one());
            if((Boolean) domain.getExtensionparams().get("create_uiddirtyflag"))
                model.setCreate_uid(domain.getCreateUid());
            domain.getFocusNull().clear();
        }
        return model ;
    }

    public Web_editor_converter_test convert2Domain( web_editor_converter_testClientModel model ,Web_editor_converter_test domain) {
        if(model == null)
            return domain ;
        if(domain == null){
            domain = new Web_editor_converter_test();
        }

        if(model.getTextDirtyFlag())
            domain.setText(model.getText());
        if(model.getCreate_dateDirtyFlag())
            domain.setCreateDate(model.getCreate_date());
        if(model.getSelection_strDirtyFlag())
            domain.setSelectionStr(model.getSelection_str());
        if(model.getNumericDirtyFlag())
            domain.setNumeric(model.getNumeric());
        if(model.getIntegerDirtyFlag())
            domain.setInteger(model.getInteger());
        if(model.getBinaryDirtyFlag())
            domain.setBinary(model.getBinary());
        if(model.getDateDirtyFlag())
            domain.setDate(model.getDate());
        if(model.getIbizfloatDirtyFlag())
            domain.setIbizfloat(model.getIbizfloat());
        if(model.get__last_updateDirtyFlag())
            domain.setLastUpdate(model.get__last_update());
        if(model.getDatetimeDirtyFlag())
            domain.setDatetime(model.getDatetime());
        if(model.getIdDirtyFlag())
            domain.setId(model.getId());
        if(model.getWrite_dateDirtyFlag())
            domain.setWriteDate(model.getWrite_date());
        if(model.getHtmlDirtyFlag())
            domain.setHtml(model.getHtml());
        if(model.getDisplay_nameDirtyFlag())
            domain.setDisplayName(model.getDisplay_name());
        if(model.getIbizcharDirtyFlag())
            domain.setIbizchar(model.getIbizchar());
        if(model.getSelectionDirtyFlag())
            domain.setSelection(model.getSelection());
        if(model.getCreate_uid_textDirtyFlag())
            domain.setCreateUidText(model.getCreate_uid_text());
        if(model.getWrite_uid_textDirtyFlag())
            domain.setWriteUidText(model.getWrite_uid_text());
        if(model.getMany2one_textDirtyFlag())
            domain.setMany2oneText(model.getMany2one_text());
        if(model.getWrite_uidDirtyFlag())
            domain.setWriteUid(model.getWrite_uid());
        if(model.getMany2oneDirtyFlag())
            domain.setMany2one(model.getMany2one());
        if(model.getCreate_uidDirtyFlag())
            domain.setCreateUid(model.getCreate_uid());
        return domain ;
    }

}

    



