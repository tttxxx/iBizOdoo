package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Account_payment_method;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_payment_methodSearchContext;

/**
 * 实体 [付款方法] 存储对象
 */
public interface Account_payment_methodRepository extends Repository<Account_payment_method> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Account_payment_method> searchDefault(Account_payment_methodSearchContext context);

    Account_payment_method convert2PO(cn.ibizlab.odoo.core.odoo_account.domain.Account_payment_method domain , Account_payment_method po) ;

    cn.ibizlab.odoo.core.odoo_account.domain.Account_payment_method convert2Domain( Account_payment_method po ,cn.ibizlab.odoo.core.odoo_account.domain.Account_payment_method domain) ;

}
