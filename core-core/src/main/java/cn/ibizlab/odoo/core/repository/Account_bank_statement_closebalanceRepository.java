package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Account_bank_statement_closebalance;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_bank_statement_closebalanceSearchContext;

/**
 * 实体 [银行对账单期末余额] 存储对象
 */
public interface Account_bank_statement_closebalanceRepository extends Repository<Account_bank_statement_closebalance> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Account_bank_statement_closebalance> searchDefault(Account_bank_statement_closebalanceSearchContext context);

    Account_bank_statement_closebalance convert2PO(cn.ibizlab.odoo.core.odoo_account.domain.Account_bank_statement_closebalance domain , Account_bank_statement_closebalance po) ;

    cn.ibizlab.odoo.core.odoo_account.domain.Account_bank_statement_closebalance convert2Domain( Account_bank_statement_closebalance po ,cn.ibizlab.odoo.core.odoo_account.domain.Account_bank_statement_closebalance domain) ;

}
