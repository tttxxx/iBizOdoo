package cn.ibizlab.odoo.core.odoo_base.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_base.domain.Res_users_log;
import cn.ibizlab.odoo.core.odoo_base.filter.Res_users_logSearchContext;
import cn.ibizlab.odoo.core.odoo_base.service.IRes_users_logService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;



import cn.ibizlab.odoo.core.odoo_base.client.res_users_logOdooClient;
import cn.ibizlab.odoo.core.odoo_base.clientmodel.res_users_logClientModel;

/**
 * 实体[用户日志] 服务对象接口实现
 */
@Slf4j
@Service
public class Res_users_logServiceImpl implements IRes_users_logService {

    @Autowired
    res_users_logOdooClient res_users_logOdooClient;


    @Override
    public boolean update(Res_users_log et) {
        res_users_logClientModel clientModel = convert2Model(et,null);
		res_users_logOdooClient.update(clientModel);
        Res_users_log rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }
    
    public void updateBatch(List<Res_users_log> list){
    }

    @Override
    public Res_users_log get(Integer id) {
        res_users_logClientModel clientModel = new res_users_logClientModel();
        clientModel.setId(id);
		res_users_logOdooClient.get(clientModel);
        Res_users_log et = convert2Domain(clientModel,null);
        if(et==null){
            et=new Res_users_log();
            et.setId(id);
        }
        else{
        }

        return  et;
    }

    @Override
    public boolean create(Res_users_log et) {
        res_users_logClientModel clientModel = convert2Model(et,null);
		res_users_logOdooClient.create(clientModel);
        Res_users_log rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Res_users_log> list){
    }

    @Override
    public boolean remove(Integer id) {
        res_users_logClientModel clientModel = new res_users_logClientModel();
        clientModel.setId(id);
		res_users_logOdooClient.remove(clientModel);
        return true; 
    }

    public void removeBatch(Collection<Integer> idList){
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Res_users_log> searchDefault(Res_users_logSearchContext context) {
        List<Res_users_log> list = new ArrayList<Res_users_log>();
        Page<res_users_logClientModel> clientModelList = res_users_logOdooClient.search(context);
        for(res_users_logClientModel clientModel : clientModelList){
            list.add( this.convert2Domain(clientModel,null));
        }
        return new PageImpl<Res_users_log>(list, context.getPageable(), clientModelList.getTotalElements());
    }

    public res_users_logClientModel convert2Model(Res_users_log domain , res_users_logClientModel model) {
        if(domain == null)
            return model ;
        if(model==null){
            model = new res_users_logClientModel();

        }
        if((Boolean) domain.getExtensionparams().get("dirtyflagenable")){
            if((Boolean) domain.getExtensionparams().get("write_datedirtyflag"))
                model.setWrite_date(domain.getWriteDate());
            if((Boolean) domain.getExtensionparams().get("iddirtyflag"))
                model.setId(domain.getId());
            if((Boolean) domain.getExtensionparams().get("display_namedirtyflag"))
                model.setDisplay_name(domain.getDisplayName());
            if((Boolean) domain.getExtensionparams().get("create_datedirtyflag"))
                model.setCreate_date(domain.getCreateDate());
            if((Boolean) domain.getExtensionparams().get("__last_updatedirtyflag"))
                model.set__last_update(domain.getLastUpdate());
            if((Boolean) domain.getExtensionparams().get("write_uid_textdirtyflag"))
                model.setWrite_uid_text(domain.getWriteUidText());
            if((Boolean) domain.getExtensionparams().get("create_uid_textdirtyflag"))
                model.setCreate_uid_text(domain.getCreateUidText());
            if((Boolean) domain.getExtensionparams().get("write_uiddirtyflag"))
                model.setWrite_uid(domain.getWriteUid());
            if((Boolean) domain.getExtensionparams().get("create_uiddirtyflag"))
                model.setCreate_uid(domain.getCreateUid());
            domain.getFocusNull().clear();
        }
        return model ;
    }

    public Res_users_log convert2Domain( res_users_logClientModel model ,Res_users_log domain) {
        if(model == null)
            return domain ;
        if(domain == null){
            domain = new Res_users_log();
        }

        if(model.getWrite_dateDirtyFlag())
            domain.setWriteDate(model.getWrite_date());
        if(model.getIdDirtyFlag())
            domain.setId(model.getId());
        if(model.getDisplay_nameDirtyFlag())
            domain.setDisplayName(model.getDisplay_name());
        if(model.getCreate_dateDirtyFlag())
            domain.setCreateDate(model.getCreate_date());
        if(model.get__last_updateDirtyFlag())
            domain.setLastUpdate(model.get__last_update());
        if(model.getWrite_uid_textDirtyFlag())
            domain.setWriteUidText(model.getWrite_uid_text());
        if(model.getCreate_uid_textDirtyFlag())
            domain.setCreateUidText(model.getCreate_uid_text());
        if(model.getWrite_uidDirtyFlag())
            domain.setWriteUid(model.getWrite_uid());
        if(model.getCreate_uidDirtyFlag())
            domain.setCreateUid(model.getCreate_uid());
        return domain ;
    }

}

    



