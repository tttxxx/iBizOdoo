package cn.ibizlab.odoo.core.odoo_hr.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_hr.domain.Hr_employee;
import cn.ibizlab.odoo.core.odoo_hr.filter.Hr_employeeSearchContext;
import cn.ibizlab.odoo.core.odoo_hr.service.IHr_employeeService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;



import cn.ibizlab.odoo.core.odoo_hr.client.hr_employeeOdooClient;
import cn.ibizlab.odoo.core.odoo_hr.clientmodel.hr_employeeClientModel;

/**
 * 实体[员工] 服务对象接口实现
 */
@Slf4j
@Service
public class Hr_employeeServiceImpl implements IHr_employeeService {

    @Autowired
    hr_employeeOdooClient hr_employeeOdooClient;


    @Override
    public boolean create(Hr_employee et) {
        hr_employeeClientModel clientModel = convert2Model(et,null);
		hr_employeeOdooClient.create(clientModel);
        Hr_employee rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Hr_employee> list){
    }

    @Override
    public boolean remove(Integer id) {
        hr_employeeClientModel clientModel = new hr_employeeClientModel();
        clientModel.setId(id);
		hr_employeeOdooClient.remove(clientModel);
        return true; 
    }

    public void removeBatch(Collection<Integer> idList){
    }

    @Override
    public boolean update(Hr_employee et) {
        hr_employeeClientModel clientModel = convert2Model(et,null);
		hr_employeeOdooClient.update(clientModel);
        Hr_employee rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }
    
    public void updateBatch(List<Hr_employee> list){
    }

    @Override
    public Hr_employee get(Integer id) {
        hr_employeeClientModel clientModel = new hr_employeeClientModel();
        clientModel.setId(id);
		hr_employeeOdooClient.get(clientModel);
        Hr_employee et = convert2Domain(clientModel,null);
        if(et==null){
            et=new Hr_employee();
            et.setId(id);
        }
        else{
        }

        return  et;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Hr_employee> searchDefault(Hr_employeeSearchContext context) {
        List<Hr_employee> list = new ArrayList<Hr_employee>();
        Page<hr_employeeClientModel> clientModelList = hr_employeeOdooClient.search(context);
        for(hr_employeeClientModel clientModel : clientModelList){
            list.add( this.convert2Domain(clientModel,null));
        }
        return new PageImpl<Hr_employee>(list, context.getPageable(), clientModelList.getTotalElements());
    }

    public hr_employeeClientModel convert2Model(Hr_employee domain , hr_employeeClientModel model) {
        if(domain == null)
            return model ;
        if(model==null){
            model = new hr_employeeClientModel();

        }
        if((Boolean) domain.getExtensionparams().get("dirtyflagenable")){
            if((Boolean) domain.getExtensionparams().get("mobile_phonedirtyflag"))
                model.setMobile_phone(domain.getMobilePhone());
            if((Boolean) domain.getExtensionparams().get("message_has_error_counterdirtyflag"))
                model.setMessage_has_error_counter(domain.getMessageHasErrorCounter());
            if((Boolean) domain.getExtensionparams().get("message_needaction_counterdirtyflag"))
                model.setMessage_needaction_counter(domain.getMessageNeedactionCounter());
            if((Boolean) domain.getExtensionparams().get("leave_date_fromdirtyflag"))
                model.setLeave_date_from(domain.getLeaveDateFrom());
            if((Boolean) domain.getExtensionparams().get("message_unreaddirtyflag"))
                model.setMessage_unread(domain.getMessageUnread());
            if((Boolean) domain.getExtensionparams().get("childrendirtyflag"))
                model.setChildren(domain.getChildren());
            if((Boolean) domain.getExtensionparams().get("image_smalldirtyflag"))
                model.setImage_small(domain.getImageSmall());
            if((Boolean) domain.getExtensionparams().get("message_needactiondirtyflag"))
                model.setMessage_needaction(domain.getMessageNeedaction());
            if((Boolean) domain.getExtensionparams().get("pindirtyflag"))
                model.setPin(domain.getPin());
            if((Boolean) domain.getExtensionparams().get("message_main_attachment_iddirtyflag"))
                model.setMessage_main_attachment_id(domain.getMessageMainAttachmentId());
            if((Boolean) domain.getExtensionparams().get("study_schooldirtyflag"))
                model.setStudy_school(domain.getStudySchool());
            if((Boolean) domain.getExtensionparams().get("activity_idsdirtyflag"))
                model.setActivity_ids(domain.getActivityIds());
            if((Boolean) domain.getExtensionparams().get("maritaldirtyflag"))
                model.setMarital(domain.getMarital());
            if((Boolean) domain.getExtensionparams().get("direct_badge_idsdirtyflag"))
                model.setDirect_badge_ids(domain.getDirectBadgeIds());
            if((Boolean) domain.getExtensionparams().get("image_mediumdirtyflag"))
                model.setImage_medium(domain.getImageMedium());
            if((Boolean) domain.getExtensionparams().get("__last_updatedirtyflag"))
                model.set__last_update(domain.getLastUpdate());
            if((Boolean) domain.getExtensionparams().get("emergency_phonedirtyflag"))
                model.setEmergency_phone(domain.getEmergencyPhone());
            if((Boolean) domain.getExtensionparams().get("activity_date_deadlinedirtyflag"))
                model.setActivity_date_deadline(domain.getActivityDateDeadline());
            if((Boolean) domain.getExtensionparams().get("km_home_workdirtyflag"))
                model.setKm_home_work(domain.getKmHomeWork());
            if((Boolean) domain.getExtensionparams().get("visa_nodirtyflag"))
                model.setVisa_no(domain.getVisaNo());
            if((Boolean) domain.getExtensionparams().get("place_of_birthdirtyflag"))
                model.setPlace_of_birth(domain.getPlaceOfBirth());
            if((Boolean) domain.getExtensionparams().get("spouse_complete_namedirtyflag"))
                model.setSpouse_complete_name(domain.getSpouseCompleteName());
            if((Boolean) domain.getExtensionparams().get("siniddirtyflag"))
                model.setSinid(domain.getSinid());
            if((Boolean) domain.getExtensionparams().get("badge_idsdirtyflag"))
                model.setBadge_ids(domain.getBadgeIds());
            if((Boolean) domain.getExtensionparams().get("work_emaildirtyflag"))
                model.setWork_email(domain.getWorkEmail());
            if((Boolean) domain.getExtensionparams().get("has_badgesdirtyflag"))
                model.setHas_badges(domain.getHasBadges());
            if((Boolean) domain.getExtensionparams().get("current_leave_iddirtyflag"))
                model.setCurrent_leave_id(domain.getCurrentLeaveId());
            if((Boolean) domain.getExtensionparams().get("contract_idsdirtyflag"))
                model.setContract_ids(domain.getContractIds());
            if((Boolean) domain.getExtensionparams().get("message_is_followerdirtyflag"))
                model.setMessage_is_follower(domain.getMessageIsFollower());
            if((Boolean) domain.getExtensionparams().get("message_channel_idsdirtyflag"))
                model.setMessage_channel_ids(domain.getMessageChannelIds());
            if((Boolean) domain.getExtensionparams().get("barcodedirtyflag"))
                model.setBarcode(domain.getBarcode());
            if((Boolean) domain.getExtensionparams().get("birthdaydirtyflag"))
                model.setBirthday(domain.getBirthday());
            if((Boolean) domain.getExtensionparams().get("certificatedirtyflag"))
                model.setCertificate(domain.getCertificate());
            if((Boolean) domain.getExtensionparams().get("activity_user_iddirtyflag"))
                model.setActivity_user_id(domain.getActivityUserId());
            if((Boolean) domain.getExtensionparams().get("study_fielddirtyflag"))
                model.setStudy_field(domain.getStudyField());
            if((Boolean) domain.getExtensionparams().get("website_message_idsdirtyflag"))
                model.setWebsite_message_ids(domain.getWebsiteMessageIds());
            if((Boolean) domain.getExtensionparams().get("activity_summarydirtyflag"))
                model.setActivity_summary(domain.getActivitySummary());
            if((Boolean) domain.getExtensionparams().get("attendance_statedirtyflag"))
                model.setAttendance_state(domain.getAttendanceState());
            if((Boolean) domain.getExtensionparams().get("contracts_countdirtyflag"))
                model.setContracts_count(domain.getContractsCount());
            if((Boolean) domain.getExtensionparams().get("genderdirtyflag"))
                model.setGender(domain.getGender());
            if((Boolean) domain.getExtensionparams().get("imagedirtyflag"))
                model.setImage(domain.getImage());
            if((Boolean) domain.getExtensionparams().get("spouse_birthdatedirtyflag"))
                model.setSpouse_birthdate(domain.getSpouseBirthdate());
            if((Boolean) domain.getExtensionparams().get("message_unread_counterdirtyflag"))
                model.setMessage_unread_counter(domain.getMessageUnreadCounter());
            if((Boolean) domain.getExtensionparams().get("leaves_countdirtyflag"))
                model.setLeaves_count(domain.getLeavesCount());
            if((Boolean) domain.getExtensionparams().get("notesdirtyflag"))
                model.setNotes(domain.getNotes());
            if((Boolean) domain.getExtensionparams().get("additional_notedirtyflag"))
                model.setAdditional_note(domain.getAdditionalNote());
            if((Boolean) domain.getExtensionparams().get("write_datedirtyflag"))
                model.setWrite_date(domain.getWriteDate());
            if((Boolean) domain.getExtensionparams().get("medic_examdirtyflag"))
                model.setMedic_exam(domain.getMedicExam());
            if((Boolean) domain.getExtensionparams().get("visa_expiredirtyflag"))
                model.setVisa_expire(domain.getVisaExpire());
            if((Boolean) domain.getExtensionparams().get("work_phonedirtyflag"))
                model.setWork_phone(domain.getWorkPhone());
            if((Boolean) domain.getExtensionparams().get("emergency_contactdirtyflag"))
                model.setEmergency_contact(domain.getEmergencyContact());
            if((Boolean) domain.getExtensionparams().get("newly_hired_employeedirtyflag"))
                model.setNewly_hired_employee(domain.getNewlyHiredEmployee());
            if((Boolean) domain.getExtensionparams().get("display_namedirtyflag"))
                model.setDisplay_name(domain.getDisplayName());
            if((Boolean) domain.getExtensionparams().get("is_address_home_a_companydirtyflag"))
                model.setIs_address_home_a_company(domain.getIsAddressHomeACompany());
            if((Boolean) domain.getExtensionparams().get("attendance_idsdirtyflag"))
                model.setAttendance_ids(domain.getAttendanceIds());
            if((Boolean) domain.getExtensionparams().get("activity_statedirtyflag"))
                model.setActivity_state(domain.getActivityState());
            if((Boolean) domain.getExtensionparams().get("message_has_errordirtyflag"))
                model.setMessage_has_error(domain.getMessageHasError());
            if((Boolean) domain.getExtensionparams().get("current_leave_statedirtyflag"))
                model.setCurrent_leave_state(domain.getCurrentLeaveState());
            if((Boolean) domain.getExtensionparams().get("child_idsdirtyflag"))
                model.setChild_ids(domain.getChildIds());
            if((Boolean) domain.getExtensionparams().get("message_idsdirtyflag"))
                model.setMessage_ids(domain.getMessageIds());
            if((Boolean) domain.getExtensionparams().get("colordirtyflag"))
                model.setColor(domain.getColor());
            if((Boolean) domain.getExtensionparams().get("manual_attendancedirtyflag"))
                model.setManual_attendance(domain.getManualAttendance());
            if((Boolean) domain.getExtensionparams().get("remaining_leavesdirtyflag"))
                model.setRemaining_leaves(domain.getRemainingLeaves());
            if((Boolean) domain.getExtensionparams().get("ssniddirtyflag"))
                model.setSsnid(domain.getSsnid());
            if((Boolean) domain.getExtensionparams().get("job_titledirtyflag"))
                model.setJob_title(domain.getJobTitle());
            if((Boolean) domain.getExtensionparams().get("activity_type_iddirtyflag"))
                model.setActivity_type_id(domain.getActivityTypeId());
            if((Boolean) domain.getExtensionparams().get("is_absent_totaydirtyflag"))
                model.setIs_absent_totay(domain.getIsAbsentTotay());
            if((Boolean) domain.getExtensionparams().get("message_partner_idsdirtyflag"))
                model.setMessage_partner_ids(domain.getMessagePartnerIds());
            if((Boolean) domain.getExtensionparams().get("passport_iddirtyflag"))
                model.setPassport_id(domain.getPassportId());
            if((Boolean) domain.getExtensionparams().get("goal_idsdirtyflag"))
                model.setGoal_ids(domain.getGoalIds());
            if((Boolean) domain.getExtensionparams().get("leave_date_todirtyflag"))
                model.setLeave_date_to(domain.getLeaveDateTo());
            if((Boolean) domain.getExtensionparams().get("managerdirtyflag"))
                model.setManager(domain.getManager());
            if((Boolean) domain.getExtensionparams().get("work_locationdirtyflag"))
                model.setWork_location(domain.getWorkLocation());
            if((Boolean) domain.getExtensionparams().get("message_attachment_countdirtyflag"))
                model.setMessage_attachment_count(domain.getMessageAttachmentCount());
            if((Boolean) domain.getExtensionparams().get("contract_iddirtyflag"))
                model.setContract_id(domain.getContractId());
            if((Boolean) domain.getExtensionparams().get("google_drive_linkdirtyflag"))
                model.setGoogle_drive_link(domain.getGoogleDriveLink());
            if((Boolean) domain.getExtensionparams().get("permit_nodirtyflag"))
                model.setPermit_no(domain.getPermitNo());
            if((Boolean) domain.getExtensionparams().get("message_follower_idsdirtyflag"))
                model.setMessage_follower_ids(domain.getMessageFollowerIds());
            if((Boolean) domain.getExtensionparams().get("iddirtyflag"))
                model.setId(domain.getId());
            if((Boolean) domain.getExtensionparams().get("identification_iddirtyflag"))
                model.setIdentification_id(domain.getIdentificationId());
            if((Boolean) domain.getExtensionparams().get("vehicledirtyflag"))
                model.setVehicle(domain.getVehicle());
            if((Boolean) domain.getExtensionparams().get("category_idsdirtyflag"))
                model.setCategory_ids(domain.getCategoryIds());
            if((Boolean) domain.getExtensionparams().get("show_leavesdirtyflag"))
                model.setShow_leaves(domain.getShowLeaves());
            if((Boolean) domain.getExtensionparams().get("create_datedirtyflag"))
                model.setCreate_date(domain.getCreateDate());
            if((Boolean) domain.getExtensionparams().get("parent_id_textdirtyflag"))
                model.setParent_id_text(domain.getParentIdText());
            if((Boolean) domain.getExtensionparams().get("write_uid_textdirtyflag"))
                model.setWrite_uid_text(domain.getWriteUidText());
            if((Boolean) domain.getExtensionparams().get("namedirtyflag"))
                model.setName(domain.getName());
            if((Boolean) domain.getExtensionparams().get("create_uid_textdirtyflag"))
                model.setCreate_uid_text(domain.getCreateUidText());
            if((Boolean) domain.getExtensionparams().get("address_home_id_textdirtyflag"))
                model.setAddress_home_id_text(domain.getAddressHomeIdText());
            if((Boolean) domain.getExtensionparams().get("tzdirtyflag"))
                model.setTz(domain.getTz());
            if((Boolean) domain.getExtensionparams().get("resource_calendar_id_textdirtyflag"))
                model.setResource_calendar_id_text(domain.getResourceCalendarIdText());
            if((Boolean) domain.getExtensionparams().get("user_id_textdirtyflag"))
                model.setUser_id_text(domain.getUserIdText());
            if((Boolean) domain.getExtensionparams().get("department_id_textdirtyflag"))
                model.setDepartment_id_text(domain.getDepartmentIdText());
            if((Boolean) domain.getExtensionparams().get("country_of_birth_textdirtyflag"))
                model.setCountry_of_birth_text(domain.getCountryOfBirthText());
            if((Boolean) domain.getExtensionparams().get("expense_manager_id_textdirtyflag"))
                model.setExpense_manager_id_text(domain.getExpenseManagerIdText());
            if((Boolean) domain.getExtensionparams().get("job_id_textdirtyflag"))
                model.setJob_id_text(domain.getJobIdText());
            if((Boolean) domain.getExtensionparams().get("address_id_textdirtyflag"))
                model.setAddress_id_text(domain.getAddressIdText());
            if((Boolean) domain.getExtensionparams().get("company_id_textdirtyflag"))
                model.setCompany_id_text(domain.getCompanyIdText());
            if((Boolean) domain.getExtensionparams().get("activedirtyflag"))
                model.setActive(domain.getActive());
            if((Boolean) domain.getExtensionparams().get("coach_id_textdirtyflag"))
                model.setCoach_id_text(domain.getCoachIdText());
            if((Boolean) domain.getExtensionparams().get("country_id_textdirtyflag"))
                model.setCountry_id_text(domain.getCountryIdText());
            if((Boolean) domain.getExtensionparams().get("address_home_iddirtyflag"))
                model.setAddress_home_id(domain.getAddressHomeId());
            if((Boolean) domain.getExtensionparams().get("expense_manager_iddirtyflag"))
                model.setExpense_manager_id(domain.getExpenseManagerId());
            if((Boolean) domain.getExtensionparams().get("bank_account_iddirtyflag"))
                model.setBank_account_id(domain.getBankAccountId());
            if((Boolean) domain.getExtensionparams().get("write_uiddirtyflag"))
                model.setWrite_uid(domain.getWriteUid());
            if((Boolean) domain.getExtensionparams().get("create_uiddirtyflag"))
                model.setCreate_uid(domain.getCreateUid());
            if((Boolean) domain.getExtensionparams().get("country_iddirtyflag"))
                model.setCountry_id(domain.getCountryId());
            if((Boolean) domain.getExtensionparams().get("company_iddirtyflag"))
                model.setCompany_id(domain.getCompanyId());
            if((Boolean) domain.getExtensionparams().get("job_iddirtyflag"))
                model.setJob_id(domain.getJobId());
            if((Boolean) domain.getExtensionparams().get("resource_iddirtyflag"))
                model.setResource_id(domain.getResourceId());
            if((Boolean) domain.getExtensionparams().get("user_iddirtyflag"))
                model.setUser_id(domain.getUserId());
            if((Boolean) domain.getExtensionparams().get("department_iddirtyflag"))
                model.setDepartment_id(domain.getDepartmentId());
            if((Boolean) domain.getExtensionparams().get("parent_iddirtyflag"))
                model.setParent_id(domain.getParentId());
            if((Boolean) domain.getExtensionparams().get("last_attendance_iddirtyflag"))
                model.setLast_attendance_id(domain.getLastAttendanceId());
            if((Boolean) domain.getExtensionparams().get("coach_iddirtyflag"))
                model.setCoach_id(domain.getCoachId());
            if((Boolean) domain.getExtensionparams().get("address_iddirtyflag"))
                model.setAddress_id(domain.getAddressId());
            if((Boolean) domain.getExtensionparams().get("country_of_birthdirtyflag"))
                model.setCountry_of_birth(domain.getCountryOfBirth());
            if((Boolean) domain.getExtensionparams().get("resource_calendar_iddirtyflag"))
                model.setResource_calendar_id(domain.getResourceCalendarId());
            domain.getFocusNull().clear();
        }
        return model ;
    }

    public Hr_employee convert2Domain( hr_employeeClientModel model ,Hr_employee domain) {
        if(model == null)
            return domain ;
        if(domain == null){
            domain = new Hr_employee();
        }

        if(model.getMobile_phoneDirtyFlag())
            domain.setMobilePhone(model.getMobile_phone());
        if(model.getMessage_has_error_counterDirtyFlag())
            domain.setMessageHasErrorCounter(model.getMessage_has_error_counter());
        if(model.getMessage_needaction_counterDirtyFlag())
            domain.setMessageNeedactionCounter(model.getMessage_needaction_counter());
        if(model.getLeave_date_fromDirtyFlag())
            domain.setLeaveDateFrom(model.getLeave_date_from());
        if(model.getMessage_unreadDirtyFlag())
            domain.setMessageUnread(model.getMessage_unread());
        if(model.getChildrenDirtyFlag())
            domain.setChildren(model.getChildren());
        if(model.getImage_smallDirtyFlag())
            domain.setImageSmall(model.getImage_small());
        if(model.getMessage_needactionDirtyFlag())
            domain.setMessageNeedaction(model.getMessage_needaction());
        if(model.getPinDirtyFlag())
            domain.setPin(model.getPin());
        if(model.getMessage_main_attachment_idDirtyFlag())
            domain.setMessageMainAttachmentId(model.getMessage_main_attachment_id());
        if(model.getStudy_schoolDirtyFlag())
            domain.setStudySchool(model.getStudy_school());
        if(model.getActivity_idsDirtyFlag())
            domain.setActivityIds(model.getActivity_ids());
        if(model.getMaritalDirtyFlag())
            domain.setMarital(model.getMarital());
        if(model.getDirect_badge_idsDirtyFlag())
            domain.setDirectBadgeIds(model.getDirect_badge_ids());
        if(model.getImage_mediumDirtyFlag())
            domain.setImageMedium(model.getImage_medium());
        if(model.get__last_updateDirtyFlag())
            domain.setLastUpdate(model.get__last_update());
        if(model.getEmergency_phoneDirtyFlag())
            domain.setEmergencyPhone(model.getEmergency_phone());
        if(model.getActivity_date_deadlineDirtyFlag())
            domain.setActivityDateDeadline(model.getActivity_date_deadline());
        if(model.getKm_home_workDirtyFlag())
            domain.setKmHomeWork(model.getKm_home_work());
        if(model.getVisa_noDirtyFlag())
            domain.setVisaNo(model.getVisa_no());
        if(model.getPlace_of_birthDirtyFlag())
            domain.setPlaceOfBirth(model.getPlace_of_birth());
        if(model.getSpouse_complete_nameDirtyFlag())
            domain.setSpouseCompleteName(model.getSpouse_complete_name());
        if(model.getSinidDirtyFlag())
            domain.setSinid(model.getSinid());
        if(model.getBadge_idsDirtyFlag())
            domain.setBadgeIds(model.getBadge_ids());
        if(model.getWork_emailDirtyFlag())
            domain.setWorkEmail(model.getWork_email());
        if(model.getHas_badgesDirtyFlag())
            domain.setHasBadges(model.getHas_badges());
        if(model.getCurrent_leave_idDirtyFlag())
            domain.setCurrentLeaveId(model.getCurrent_leave_id());
        if(model.getContract_idsDirtyFlag())
            domain.setContractIds(model.getContract_ids());
        if(model.getMessage_is_followerDirtyFlag())
            domain.setMessageIsFollower(model.getMessage_is_follower());
        if(model.getMessage_channel_idsDirtyFlag())
            domain.setMessageChannelIds(model.getMessage_channel_ids());
        if(model.getBarcodeDirtyFlag())
            domain.setBarcode(model.getBarcode());
        if(model.getBirthdayDirtyFlag())
            domain.setBirthday(model.getBirthday());
        if(model.getCertificateDirtyFlag())
            domain.setCertificate(model.getCertificate());
        if(model.getActivity_user_idDirtyFlag())
            domain.setActivityUserId(model.getActivity_user_id());
        if(model.getStudy_fieldDirtyFlag())
            domain.setStudyField(model.getStudy_field());
        if(model.getWebsite_message_idsDirtyFlag())
            domain.setWebsiteMessageIds(model.getWebsite_message_ids());
        if(model.getActivity_summaryDirtyFlag())
            domain.setActivitySummary(model.getActivity_summary());
        if(model.getAttendance_stateDirtyFlag())
            domain.setAttendanceState(model.getAttendance_state());
        if(model.getContracts_countDirtyFlag())
            domain.setContractsCount(model.getContracts_count());
        if(model.getGenderDirtyFlag())
            domain.setGender(model.getGender());
        if(model.getImageDirtyFlag())
            domain.setImage(model.getImage());
        if(model.getSpouse_birthdateDirtyFlag())
            domain.setSpouseBirthdate(model.getSpouse_birthdate());
        if(model.getMessage_unread_counterDirtyFlag())
            domain.setMessageUnreadCounter(model.getMessage_unread_counter());
        if(model.getLeaves_countDirtyFlag())
            domain.setLeavesCount(model.getLeaves_count());
        if(model.getNotesDirtyFlag())
            domain.setNotes(model.getNotes());
        if(model.getAdditional_noteDirtyFlag())
            domain.setAdditionalNote(model.getAdditional_note());
        if(model.getWrite_dateDirtyFlag())
            domain.setWriteDate(model.getWrite_date());
        if(model.getMedic_examDirtyFlag())
            domain.setMedicExam(model.getMedic_exam());
        if(model.getVisa_expireDirtyFlag())
            domain.setVisaExpire(model.getVisa_expire());
        if(model.getWork_phoneDirtyFlag())
            domain.setWorkPhone(model.getWork_phone());
        if(model.getEmergency_contactDirtyFlag())
            domain.setEmergencyContact(model.getEmergency_contact());
        if(model.getNewly_hired_employeeDirtyFlag())
            domain.setNewlyHiredEmployee(model.getNewly_hired_employee());
        if(model.getDisplay_nameDirtyFlag())
            domain.setDisplayName(model.getDisplay_name());
        if(model.getIs_address_home_a_companyDirtyFlag())
            domain.setIsAddressHomeACompany(model.getIs_address_home_a_company());
        if(model.getAttendance_idsDirtyFlag())
            domain.setAttendanceIds(model.getAttendance_ids());
        if(model.getActivity_stateDirtyFlag())
            domain.setActivityState(model.getActivity_state());
        if(model.getMessage_has_errorDirtyFlag())
            domain.setMessageHasError(model.getMessage_has_error());
        if(model.getCurrent_leave_stateDirtyFlag())
            domain.setCurrentLeaveState(model.getCurrent_leave_state());
        if(model.getChild_idsDirtyFlag())
            domain.setChildIds(model.getChild_ids());
        if(model.getMessage_idsDirtyFlag())
            domain.setMessageIds(model.getMessage_ids());
        if(model.getColorDirtyFlag())
            domain.setColor(model.getColor());
        if(model.getManual_attendanceDirtyFlag())
            domain.setManualAttendance(model.getManual_attendance());
        if(model.getRemaining_leavesDirtyFlag())
            domain.setRemainingLeaves(model.getRemaining_leaves());
        if(model.getSsnidDirtyFlag())
            domain.setSsnid(model.getSsnid());
        if(model.getJob_titleDirtyFlag())
            domain.setJobTitle(model.getJob_title());
        if(model.getActivity_type_idDirtyFlag())
            domain.setActivityTypeId(model.getActivity_type_id());
        if(model.getIs_absent_totayDirtyFlag())
            domain.setIsAbsentTotay(model.getIs_absent_totay());
        if(model.getMessage_partner_idsDirtyFlag())
            domain.setMessagePartnerIds(model.getMessage_partner_ids());
        if(model.getPassport_idDirtyFlag())
            domain.setPassportId(model.getPassport_id());
        if(model.getGoal_idsDirtyFlag())
            domain.setGoalIds(model.getGoal_ids());
        if(model.getLeave_date_toDirtyFlag())
            domain.setLeaveDateTo(model.getLeave_date_to());
        if(model.getManagerDirtyFlag())
            domain.setManager(model.getManager());
        if(model.getWork_locationDirtyFlag())
            domain.setWorkLocation(model.getWork_location());
        if(model.getMessage_attachment_countDirtyFlag())
            domain.setMessageAttachmentCount(model.getMessage_attachment_count());
        if(model.getContract_idDirtyFlag())
            domain.setContractId(model.getContract_id());
        if(model.getGoogle_drive_linkDirtyFlag())
            domain.setGoogleDriveLink(model.getGoogle_drive_link());
        if(model.getPermit_noDirtyFlag())
            domain.setPermitNo(model.getPermit_no());
        if(model.getMessage_follower_idsDirtyFlag())
            domain.setMessageFollowerIds(model.getMessage_follower_ids());
        if(model.getIdDirtyFlag())
            domain.setId(model.getId());
        if(model.getIdentification_idDirtyFlag())
            domain.setIdentificationId(model.getIdentification_id());
        if(model.getVehicleDirtyFlag())
            domain.setVehicle(model.getVehicle());
        if(model.getCategory_idsDirtyFlag())
            domain.setCategoryIds(model.getCategory_ids());
        if(model.getShow_leavesDirtyFlag())
            domain.setShowLeaves(model.getShow_leaves());
        if(model.getCreate_dateDirtyFlag())
            domain.setCreateDate(model.getCreate_date());
        if(model.getParent_id_textDirtyFlag())
            domain.setParentIdText(model.getParent_id_text());
        if(model.getWrite_uid_textDirtyFlag())
            domain.setWriteUidText(model.getWrite_uid_text());
        if(model.getNameDirtyFlag())
            domain.setName(model.getName());
        if(model.getCreate_uid_textDirtyFlag())
            domain.setCreateUidText(model.getCreate_uid_text());
        if(model.getAddress_home_id_textDirtyFlag())
            domain.setAddressHomeIdText(model.getAddress_home_id_text());
        if(model.getTzDirtyFlag())
            domain.setTz(model.getTz());
        if(model.getResource_calendar_id_textDirtyFlag())
            domain.setResourceCalendarIdText(model.getResource_calendar_id_text());
        if(model.getUser_id_textDirtyFlag())
            domain.setUserIdText(model.getUser_id_text());
        if(model.getDepartment_id_textDirtyFlag())
            domain.setDepartmentIdText(model.getDepartment_id_text());
        if(model.getCountry_of_birth_textDirtyFlag())
            domain.setCountryOfBirthText(model.getCountry_of_birth_text());
        if(model.getExpense_manager_id_textDirtyFlag())
            domain.setExpenseManagerIdText(model.getExpense_manager_id_text());
        if(model.getJob_id_textDirtyFlag())
            domain.setJobIdText(model.getJob_id_text());
        if(model.getAddress_id_textDirtyFlag())
            domain.setAddressIdText(model.getAddress_id_text());
        if(model.getCompany_id_textDirtyFlag())
            domain.setCompanyIdText(model.getCompany_id_text());
        if(model.getActiveDirtyFlag())
            domain.setActive(model.getActive());
        if(model.getCoach_id_textDirtyFlag())
            domain.setCoachIdText(model.getCoach_id_text());
        if(model.getCountry_id_textDirtyFlag())
            domain.setCountryIdText(model.getCountry_id_text());
        if(model.getAddress_home_idDirtyFlag())
            domain.setAddressHomeId(model.getAddress_home_id());
        if(model.getExpense_manager_idDirtyFlag())
            domain.setExpenseManagerId(model.getExpense_manager_id());
        if(model.getBank_account_idDirtyFlag())
            domain.setBankAccountId(model.getBank_account_id());
        if(model.getWrite_uidDirtyFlag())
            domain.setWriteUid(model.getWrite_uid());
        if(model.getCreate_uidDirtyFlag())
            domain.setCreateUid(model.getCreate_uid());
        if(model.getCountry_idDirtyFlag())
            domain.setCountryId(model.getCountry_id());
        if(model.getCompany_idDirtyFlag())
            domain.setCompanyId(model.getCompany_id());
        if(model.getJob_idDirtyFlag())
            domain.setJobId(model.getJob_id());
        if(model.getResource_idDirtyFlag())
            domain.setResourceId(model.getResource_id());
        if(model.getUser_idDirtyFlag())
            domain.setUserId(model.getUser_id());
        if(model.getDepartment_idDirtyFlag())
            domain.setDepartmentId(model.getDepartment_id());
        if(model.getParent_idDirtyFlag())
            domain.setParentId(model.getParent_id());
        if(model.getLast_attendance_idDirtyFlag())
            domain.setLastAttendanceId(model.getLast_attendance_id());
        if(model.getCoach_idDirtyFlag())
            domain.setCoachId(model.getCoach_id());
        if(model.getAddress_idDirtyFlag())
            domain.setAddressId(model.getAddress_id());
        if(model.getCountry_of_birthDirtyFlag())
            domain.setCountryOfBirth(model.getCountry_of_birth());
        if(model.getResource_calendar_idDirtyFlag())
            domain.setResourceCalendarId(model.getResource_calendar_id());
        return domain ;
    }

}

    



