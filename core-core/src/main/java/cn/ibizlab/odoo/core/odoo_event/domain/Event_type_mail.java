package cn.ibizlab.odoo.core.odoo_event.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.odoo.util.domain.EntityBase;
import cn.ibizlab.odoo.util.annotation.DEField;
import cn.ibizlab.odoo.util.enums.DEPredefinedFieldType;
import cn.ibizlab.odoo.util.enums.DEFieldDefaultValueType;
import java.io.Serializable;
import lombok.Data;
import org.springframework.data.annotation.Transient;

import cn.ibizlab.odoo.util.domain.EntityClient;

/**
 * ServiceApi [基于活动分类的邮件调度] 对象
 */
@Data
public class Event_type_mail extends EntityClient implements Serializable {

    /**
     * ID
     */
    @DEField(isKeyField=true)
    @JSONField(name = "id")
    @JsonProperty("id")
    private Integer id;

    /**
     * 显示名称
     */
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    private String displayName;

    /**
     * 触发器
     */
    @DEField(name = "interval_type")
    @JSONField(name = "interval_type")
    @JsonProperty("interval_type")
    private String intervalType;

    /**
     * 单位
     */
    @DEField(name = "interval_unit")
    @JSONField(name = "interval_unit")
    @JsonProperty("interval_unit")
    private String intervalUnit;

    /**
     * 创建时间
     */
    @DEField(name = "create_date" , preType = DEPredefinedFieldType.CREATEDATE)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;

    /**
     * 间隔
     */
    @DEField(name = "interval_nbr")
    @JSONField(name = "interval_nbr")
    @JsonProperty("interval_nbr")
    private Integer intervalNbr;

    /**
     * 最后更新时间
     */
    @DEField(name = "write_date" , preType = DEPredefinedFieldType.UPDATEDATE)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;

    /**
     * 最后修改日
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;

    /**
     * 活动类型
     */
    @JSONField(name = "event_type_id_text")
    @JsonProperty("event_type_id_text")
    private String eventTypeIdText;

    /**
     * 创建人
     */
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    private String createUidText;

    /**
     * 最后更新人
     */
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    private String writeUidText;

    /**
     * EMail模板
     */
    @JSONField(name = "template_id_text")
    @JsonProperty("template_id_text")
    private String templateIdText;

    /**
     * 活动类型
     */
    @DEField(name = "event_type_id")
    @JSONField(name = "event_type_id")
    @JsonProperty("event_type_id")
    private Integer eventTypeId;

    /**
     * EMail模板
     */
    @DEField(name = "template_id")
    @JSONField(name = "template_id")
    @JsonProperty("template_id")
    private Integer templateId;

    /**
     * 创建人
     */
    @DEField(name = "create_uid" , preType = DEPredefinedFieldType.CREATEMAN)
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    private Integer createUid;

    /**
     * 最后更新人
     */
    @DEField(name = "write_uid" , preType = DEPredefinedFieldType.UPDATEMAN)
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    private Integer writeUid;


    /**
     * 
     */
    @JSONField(name = "odooeventtype")
    @JsonProperty("odooeventtype")
    private cn.ibizlab.odoo.core.odoo_event.domain.Event_type odooEventType;

    /**
     * 
     */
    @JSONField(name = "odootemplate")
    @JsonProperty("odootemplate")
    private cn.ibizlab.odoo.core.odoo_mail.domain.Mail_template odooTemplate;

    /**
     * 
     */
    @JSONField(name = "odoocreate")
    @JsonProperty("odoocreate")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_users odooCreate;

    /**
     * 
     */
    @JSONField(name = "odoowrite")
    @JsonProperty("odoowrite")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_users odooWrite;




    /**
     * 设置 [触发器]
     */
    public void setIntervalType(String intervalType){
        this.intervalType = intervalType ;
        this.modify("interval_type",intervalType);
    }
    /**
     * 设置 [单位]
     */
    public void setIntervalUnit(String intervalUnit){
        this.intervalUnit = intervalUnit ;
        this.modify("interval_unit",intervalUnit);
    }
    /**
     * 设置 [间隔]
     */
    public void setIntervalNbr(Integer intervalNbr){
        this.intervalNbr = intervalNbr ;
        this.modify("interval_nbr",intervalNbr);
    }
    /**
     * 设置 [活动类型]
     */
    public void setEventTypeId(Integer eventTypeId){
        this.eventTypeId = eventTypeId ;
        this.modify("event_type_id",eventTypeId);
    }
    /**
     * 设置 [EMail模板]
     */
    public void setTemplateId(Integer templateId){
        this.templateId = templateId ;
        this.modify("template_id",templateId);
    }

}


