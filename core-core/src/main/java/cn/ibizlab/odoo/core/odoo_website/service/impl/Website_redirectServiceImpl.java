package cn.ibizlab.odoo.core.odoo_website.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_website.domain.Website_redirect;
import cn.ibizlab.odoo.core.odoo_website.filter.Website_redirectSearchContext;
import cn.ibizlab.odoo.core.odoo_website.service.IWebsite_redirectService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;



import cn.ibizlab.odoo.core.odoo_website.client.website_redirectOdooClient;
import cn.ibizlab.odoo.core.odoo_website.clientmodel.website_redirectClientModel;

/**
 * 实体[网站重定向] 服务对象接口实现
 */
@Slf4j
@Service
public class Website_redirectServiceImpl implements IWebsite_redirectService {

    @Autowired
    website_redirectOdooClient website_redirectOdooClient;


    @Override
    public boolean update(Website_redirect et) {
        website_redirectClientModel clientModel = convert2Model(et,null);
		website_redirectOdooClient.update(clientModel);
        Website_redirect rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }
    
    public void updateBatch(List<Website_redirect> list){
    }

    @Override
    public Website_redirect get(Integer id) {
        website_redirectClientModel clientModel = new website_redirectClientModel();
        clientModel.setId(id);
		website_redirectOdooClient.get(clientModel);
        Website_redirect et = convert2Domain(clientModel,null);
        if(et==null){
            et=new Website_redirect();
            et.setId(id);
        }
        else{
        }

        return  et;
    }

    @Override
    public boolean create(Website_redirect et) {
        website_redirectClientModel clientModel = convert2Model(et,null);
		website_redirectOdooClient.create(clientModel);
        Website_redirect rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Website_redirect> list){
    }

    @Override
    public boolean remove(Integer id) {
        website_redirectClientModel clientModel = new website_redirectClientModel();
        clientModel.setId(id);
		website_redirectOdooClient.remove(clientModel);
        return true; 
    }

    public void removeBatch(Collection<Integer> idList){
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Website_redirect> searchDefault(Website_redirectSearchContext context) {
        List<Website_redirect> list = new ArrayList<Website_redirect>();
        Page<website_redirectClientModel> clientModelList = website_redirectOdooClient.search(context);
        for(website_redirectClientModel clientModel : clientModelList){
            list.add( this.convert2Domain(clientModel,null));
        }
        return new PageImpl<Website_redirect>(list, context.getPageable(), clientModelList.getTotalElements());
    }

    public website_redirectClientModel convert2Model(Website_redirect domain , website_redirectClientModel model) {
        if(domain == null)
            return model ;
        if(model==null){
            model = new website_redirectClientModel();

        }
        if((Boolean) domain.getExtensionparams().get("dirtyflagenable")){
            if((Boolean) domain.getExtensionparams().get("website_iddirtyflag"))
                model.setWebsite_id(domain.getWebsiteId());
            if((Boolean) domain.getExtensionparams().get("create_datedirtyflag"))
                model.setCreate_date(domain.getCreateDate());
            if((Boolean) domain.getExtensionparams().get("iddirtyflag"))
                model.setId(domain.getId());
            if((Boolean) domain.getExtensionparams().get("activedirtyflag"))
                model.setActive(domain.getActive());
            if((Boolean) domain.getExtensionparams().get("__last_updatedirtyflag"))
                model.set__last_update(domain.getLastUpdate());
            if((Boolean) domain.getExtensionparams().get("url_fromdirtyflag"))
                model.setUrl_from(domain.getUrlFrom());
            if((Boolean) domain.getExtensionparams().get("display_namedirtyflag"))
                model.setDisplay_name(domain.getDisplayName());
            if((Boolean) domain.getExtensionparams().get("typedirtyflag"))
                model.setType(domain.getType());
            if((Boolean) domain.getExtensionparams().get("write_datedirtyflag"))
                model.setWrite_date(domain.getWriteDate());
            if((Boolean) domain.getExtensionparams().get("sequencedirtyflag"))
                model.setSequence(domain.getSequence());
            if((Boolean) domain.getExtensionparams().get("url_todirtyflag"))
                model.setUrl_to(domain.getUrlTo());
            if((Boolean) domain.getExtensionparams().get("create_uid_textdirtyflag"))
                model.setCreate_uid_text(domain.getCreateUidText());
            if((Boolean) domain.getExtensionparams().get("write_uid_textdirtyflag"))
                model.setWrite_uid_text(domain.getWriteUidText());
            if((Boolean) domain.getExtensionparams().get("write_uiddirtyflag"))
                model.setWrite_uid(domain.getWriteUid());
            if((Boolean) domain.getExtensionparams().get("create_uiddirtyflag"))
                model.setCreate_uid(domain.getCreateUid());
            domain.getFocusNull().clear();
        }
        return model ;
    }

    public Website_redirect convert2Domain( website_redirectClientModel model ,Website_redirect domain) {
        if(model == null)
            return domain ;
        if(domain == null){
            domain = new Website_redirect();
        }

        if(model.getWebsite_idDirtyFlag())
            domain.setWebsiteId(model.getWebsite_id());
        if(model.getCreate_dateDirtyFlag())
            domain.setCreateDate(model.getCreate_date());
        if(model.getIdDirtyFlag())
            domain.setId(model.getId());
        if(model.getActiveDirtyFlag())
            domain.setActive(model.getActive());
        if(model.get__last_updateDirtyFlag())
            domain.setLastUpdate(model.get__last_update());
        if(model.getUrl_fromDirtyFlag())
            domain.setUrlFrom(model.getUrl_from());
        if(model.getDisplay_nameDirtyFlag())
            domain.setDisplayName(model.getDisplay_name());
        if(model.getTypeDirtyFlag())
            domain.setType(model.getType());
        if(model.getWrite_dateDirtyFlag())
            domain.setWriteDate(model.getWrite_date());
        if(model.getSequenceDirtyFlag())
            domain.setSequence(model.getSequence());
        if(model.getUrl_toDirtyFlag())
            domain.setUrlTo(model.getUrl_to());
        if(model.getCreate_uid_textDirtyFlag())
            domain.setCreateUidText(model.getCreate_uid_text());
        if(model.getWrite_uid_textDirtyFlag())
            domain.setWriteUidText(model.getWrite_uid_text());
        if(model.getWrite_uidDirtyFlag())
            domain.setWriteUid(model.getWrite_uid());
        if(model.getCreate_uidDirtyFlag())
            domain.setCreateUid(model.getCreate_uid());
        return domain ;
    }

}

    



