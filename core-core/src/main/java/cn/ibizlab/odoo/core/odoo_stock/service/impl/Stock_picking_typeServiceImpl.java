package cn.ibizlab.odoo.core.odoo_stock.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_stock.domain.Stock_picking_type;
import cn.ibizlab.odoo.core.odoo_stock.filter.Stock_picking_typeSearchContext;
import cn.ibizlab.odoo.core.odoo_stock.service.IStock_picking_typeService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;



import cn.ibizlab.odoo.core.odoo_stock.client.stock_picking_typeOdooClient;
import cn.ibizlab.odoo.core.odoo_stock.clientmodel.stock_picking_typeClientModel;

/**
 * 实体[拣货类型] 服务对象接口实现
 */
@Slf4j
@Service
public class Stock_picking_typeServiceImpl implements IStock_picking_typeService {

    @Autowired
    stock_picking_typeOdooClient stock_picking_typeOdooClient;


    @Override
    public boolean remove(Integer id) {
        stock_picking_typeClientModel clientModel = new stock_picking_typeClientModel();
        clientModel.setId(id);
		stock_picking_typeOdooClient.remove(clientModel);
        return true; 
    }

    public void removeBatch(Collection<Integer> idList){
    }

    @Override
    public boolean update(Stock_picking_type et) {
        stock_picking_typeClientModel clientModel = convert2Model(et,null);
		stock_picking_typeOdooClient.update(clientModel);
        Stock_picking_type rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }
    
    public void updateBatch(List<Stock_picking_type> list){
    }

    @Override
    public boolean create(Stock_picking_type et) {
        stock_picking_typeClientModel clientModel = convert2Model(et,null);
		stock_picking_typeOdooClient.create(clientModel);
        Stock_picking_type rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Stock_picking_type> list){
    }

    @Override
    public Stock_picking_type get(Integer id) {
        stock_picking_typeClientModel clientModel = new stock_picking_typeClientModel();
        clientModel.setId(id);
		stock_picking_typeOdooClient.get(clientModel);
        Stock_picking_type et = convert2Domain(clientModel,null);
        if(et==null){
            et=new Stock_picking_type();
            et.setId(id);
        }
        else{
        }

        return  et;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Stock_picking_type> searchDefault(Stock_picking_typeSearchContext context) {
        List<Stock_picking_type> list = new ArrayList<Stock_picking_type>();
        Page<stock_picking_typeClientModel> clientModelList = stock_picking_typeOdooClient.search(context);
        for(stock_picking_typeClientModel clientModel : clientModelList){
            list.add( this.convert2Domain(clientModel,null));
        }
        return new PageImpl<Stock_picking_type>(list, context.getPageable(), clientModelList.getTotalElements());
    }

    public stock_picking_typeClientModel convert2Model(Stock_picking_type domain , stock_picking_typeClientModel model) {
        if(domain == null)
            return model ;
        if(model==null){
            model = new stock_picking_typeClientModel();

        }
        if((Boolean) domain.getExtensionparams().get("dirtyflagenable")){
            if((Boolean) domain.getExtensionparams().get("show_operationsdirtyflag"))
                model.setShow_operations(domain.getShowOperations());
            if((Boolean) domain.getExtensionparams().get("iddirtyflag"))
                model.setId(domain.getId());
            if((Boolean) domain.getExtensionparams().get("sequence_iddirtyflag"))
                model.setSequence_id(domain.getSequenceId());
            if((Boolean) domain.getExtensionparams().get("use_create_lotsdirtyflag"))
                model.setUse_create_lots(domain.getUseCreateLots());
            if((Boolean) domain.getExtensionparams().get("create_datedirtyflag"))
                model.setCreate_date(domain.getCreateDate());
            if((Boolean) domain.getExtensionparams().get("barcodedirtyflag"))
                model.setBarcode(domain.getBarcode());
            if((Boolean) domain.getExtensionparams().get("show_reserveddirtyflag"))
                model.setShow_reserved(domain.getShowReserved());
            if((Boolean) domain.getExtensionparams().get("rate_picking_backordersdirtyflag"))
                model.setRate_picking_backorders(domain.getRatePickingBackorders());
            if((Boolean) domain.getExtensionparams().get("sequencedirtyflag"))
                model.setSequence(domain.getSequence());
            if((Boolean) domain.getExtensionparams().get("count_picking_backordersdirtyflag"))
                model.setCount_picking_backorders(domain.getCountPickingBackorders());
            if((Boolean) domain.getExtensionparams().get("count_mo_tododirtyflag"))
                model.setCount_mo_todo(domain.getCountMoTodo());
            if((Boolean) domain.getExtensionparams().get("colordirtyflag"))
                model.setColor(domain.getColor());
            if((Boolean) domain.getExtensionparams().get("show_entire_packsdirtyflag"))
                model.setShow_entire_packs(domain.getShowEntirePacks());
            if((Boolean) domain.getExtensionparams().get("display_namedirtyflag"))
                model.setDisplay_name(domain.getDisplayName());
            if((Boolean) domain.getExtensionparams().get("count_mo_waitingdirtyflag"))
                model.setCount_mo_waiting(domain.getCountMoWaiting());
            if((Boolean) domain.getExtensionparams().get("count_picking_draftdirtyflag"))
                model.setCount_picking_draft(domain.getCountPickingDraft());
            if((Boolean) domain.getExtensionparams().get("activedirtyflag"))
                model.setActive(domain.getActive());
            if((Boolean) domain.getExtensionparams().get("count_picking_latedirtyflag"))
                model.setCount_picking_late(domain.getCountPickingLate());
            if((Boolean) domain.getExtensionparams().get("count_pickingdirtyflag"))
                model.setCount_picking(domain.getCountPicking());
            if((Boolean) domain.getExtensionparams().get("count_picking_readydirtyflag"))
                model.setCount_picking_ready(domain.getCountPickingReady());
            if((Boolean) domain.getExtensionparams().get("last_done_pickingdirtyflag"))
                model.setLast_done_picking(domain.getLastDonePicking());
            if((Boolean) domain.getExtensionparams().get("__last_updatedirtyflag"))
                model.set__last_update(domain.getLastUpdate());
            if((Boolean) domain.getExtensionparams().get("count_picking_waitingdirtyflag"))
                model.setCount_picking_waiting(domain.getCountPickingWaiting());
            if((Boolean) domain.getExtensionparams().get("codedirtyflag"))
                model.setCode(domain.getCode());
            if((Boolean) domain.getExtensionparams().get("write_datedirtyflag"))
                model.setWrite_date(domain.getWriteDate());
            if((Boolean) domain.getExtensionparams().get("rate_picking_latedirtyflag"))
                model.setRate_picking_late(domain.getRatePickingLate());
            if((Boolean) domain.getExtensionparams().get("namedirtyflag"))
                model.setName(domain.getName());
            if((Boolean) domain.getExtensionparams().get("count_mo_latedirtyflag"))
                model.setCount_mo_late(domain.getCountMoLate());
            if((Boolean) domain.getExtensionparams().get("use_existing_lotsdirtyflag"))
                model.setUse_existing_lots(domain.getUseExistingLots());
            if((Boolean) domain.getExtensionparams().get("create_uid_textdirtyflag"))
                model.setCreate_uid_text(domain.getCreateUidText());
            if((Boolean) domain.getExtensionparams().get("default_location_src_id_textdirtyflag"))
                model.setDefault_location_src_id_text(domain.getDefaultLocationSrcIdText());
            if((Boolean) domain.getExtensionparams().get("return_picking_type_id_textdirtyflag"))
                model.setReturn_picking_type_id_text(domain.getReturnPickingTypeIdText());
            if((Boolean) domain.getExtensionparams().get("warehouse_id_textdirtyflag"))
                model.setWarehouse_id_text(domain.getWarehouseIdText());
            if((Boolean) domain.getExtensionparams().get("default_location_dest_id_textdirtyflag"))
                model.setDefault_location_dest_id_text(domain.getDefaultLocationDestIdText());
            if((Boolean) domain.getExtensionparams().get("write_uid_textdirtyflag"))
                model.setWrite_uid_text(domain.getWriteUidText());
            if((Boolean) domain.getExtensionparams().get("write_uiddirtyflag"))
                model.setWrite_uid(domain.getWriteUid());
            if((Boolean) domain.getExtensionparams().get("default_location_dest_iddirtyflag"))
                model.setDefault_location_dest_id(domain.getDefaultLocationDestId());
            if((Boolean) domain.getExtensionparams().get("create_uiddirtyflag"))
                model.setCreate_uid(domain.getCreateUid());
            if((Boolean) domain.getExtensionparams().get("warehouse_iddirtyflag"))
                model.setWarehouse_id(domain.getWarehouseId());
            if((Boolean) domain.getExtensionparams().get("default_location_src_iddirtyflag"))
                model.setDefault_location_src_id(domain.getDefaultLocationSrcId());
            if((Boolean) domain.getExtensionparams().get("return_picking_type_iddirtyflag"))
                model.setReturn_picking_type_id(domain.getReturnPickingTypeId());
            domain.getFocusNull().clear();
        }
        return model ;
    }

    public Stock_picking_type convert2Domain( stock_picking_typeClientModel model ,Stock_picking_type domain) {
        if(model == null)
            return domain ;
        if(domain == null){
            domain = new Stock_picking_type();
        }

        if(model.getShow_operationsDirtyFlag())
            domain.setShowOperations(model.getShow_operations());
        if(model.getIdDirtyFlag())
            domain.setId(model.getId());
        if(model.getSequence_idDirtyFlag())
            domain.setSequenceId(model.getSequence_id());
        if(model.getUse_create_lotsDirtyFlag())
            domain.setUseCreateLots(model.getUse_create_lots());
        if(model.getCreate_dateDirtyFlag())
            domain.setCreateDate(model.getCreate_date());
        if(model.getBarcodeDirtyFlag())
            domain.setBarcode(model.getBarcode());
        if(model.getShow_reservedDirtyFlag())
            domain.setShowReserved(model.getShow_reserved());
        if(model.getRate_picking_backordersDirtyFlag())
            domain.setRatePickingBackorders(model.getRate_picking_backorders());
        if(model.getSequenceDirtyFlag())
            domain.setSequence(model.getSequence());
        if(model.getCount_picking_backordersDirtyFlag())
            domain.setCountPickingBackorders(model.getCount_picking_backorders());
        if(model.getCount_mo_todoDirtyFlag())
            domain.setCountMoTodo(model.getCount_mo_todo());
        if(model.getColorDirtyFlag())
            domain.setColor(model.getColor());
        if(model.getShow_entire_packsDirtyFlag())
            domain.setShowEntirePacks(model.getShow_entire_packs());
        if(model.getDisplay_nameDirtyFlag())
            domain.setDisplayName(model.getDisplay_name());
        if(model.getCount_mo_waitingDirtyFlag())
            domain.setCountMoWaiting(model.getCount_mo_waiting());
        if(model.getCount_picking_draftDirtyFlag())
            domain.setCountPickingDraft(model.getCount_picking_draft());
        if(model.getActiveDirtyFlag())
            domain.setActive(model.getActive());
        if(model.getCount_picking_lateDirtyFlag())
            domain.setCountPickingLate(model.getCount_picking_late());
        if(model.getCount_pickingDirtyFlag())
            domain.setCountPicking(model.getCount_picking());
        if(model.getCount_picking_readyDirtyFlag())
            domain.setCountPickingReady(model.getCount_picking_ready());
        if(model.getLast_done_pickingDirtyFlag())
            domain.setLastDonePicking(model.getLast_done_picking());
        if(model.get__last_updateDirtyFlag())
            domain.setLastUpdate(model.get__last_update());
        if(model.getCount_picking_waitingDirtyFlag())
            domain.setCountPickingWaiting(model.getCount_picking_waiting());
        if(model.getCodeDirtyFlag())
            domain.setCode(model.getCode());
        if(model.getWrite_dateDirtyFlag())
            domain.setWriteDate(model.getWrite_date());
        if(model.getRate_picking_lateDirtyFlag())
            domain.setRatePickingLate(model.getRate_picking_late());
        if(model.getNameDirtyFlag())
            domain.setName(model.getName());
        if(model.getCount_mo_lateDirtyFlag())
            domain.setCountMoLate(model.getCount_mo_late());
        if(model.getUse_existing_lotsDirtyFlag())
            domain.setUseExistingLots(model.getUse_existing_lots());
        if(model.getCreate_uid_textDirtyFlag())
            domain.setCreateUidText(model.getCreate_uid_text());
        if(model.getDefault_location_src_id_textDirtyFlag())
            domain.setDefaultLocationSrcIdText(model.getDefault_location_src_id_text());
        if(model.getReturn_picking_type_id_textDirtyFlag())
            domain.setReturnPickingTypeIdText(model.getReturn_picking_type_id_text());
        if(model.getWarehouse_id_textDirtyFlag())
            domain.setWarehouseIdText(model.getWarehouse_id_text());
        if(model.getDefault_location_dest_id_textDirtyFlag())
            domain.setDefaultLocationDestIdText(model.getDefault_location_dest_id_text());
        if(model.getWrite_uid_textDirtyFlag())
            domain.setWriteUidText(model.getWrite_uid_text());
        if(model.getWrite_uidDirtyFlag())
            domain.setWriteUid(model.getWrite_uid());
        if(model.getDefault_location_dest_idDirtyFlag())
            domain.setDefaultLocationDestId(model.getDefault_location_dest_id());
        if(model.getCreate_uidDirtyFlag())
            domain.setCreateUid(model.getCreate_uid());
        if(model.getWarehouse_idDirtyFlag())
            domain.setWarehouseId(model.getWarehouse_id());
        if(model.getDefault_location_src_idDirtyFlag())
            domain.setDefaultLocationSrcId(model.getDefault_location_src_id());
        if(model.getReturn_picking_type_idDirtyFlag())
            domain.setReturnPickingTypeId(model.getReturn_picking_type_id());
        return domain ;
    }

}

    



