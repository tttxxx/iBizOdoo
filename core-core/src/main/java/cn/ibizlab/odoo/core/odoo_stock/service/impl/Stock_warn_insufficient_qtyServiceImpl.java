package cn.ibizlab.odoo.core.odoo_stock.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_stock.domain.Stock_warn_insufficient_qty;
import cn.ibizlab.odoo.core.odoo_stock.filter.Stock_warn_insufficient_qtySearchContext;
import cn.ibizlab.odoo.core.odoo_stock.service.IStock_warn_insufficient_qtyService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;



import cn.ibizlab.odoo.core.odoo_stock.client.stock_warn_insufficient_qtyOdooClient;
import cn.ibizlab.odoo.core.odoo_stock.clientmodel.stock_warn_insufficient_qtyClientModel;

/**
 * 实体[库存不足时发出警告] 服务对象接口实现
 */
@Slf4j
@Service
public class Stock_warn_insufficient_qtyServiceImpl implements IStock_warn_insufficient_qtyService {

    @Autowired
    stock_warn_insufficient_qtyOdooClient stock_warn_insufficient_qtyOdooClient;


    @Override
    public boolean update(Stock_warn_insufficient_qty et) {
        stock_warn_insufficient_qtyClientModel clientModel = convert2Model(et,null);
		stock_warn_insufficient_qtyOdooClient.update(clientModel);
        Stock_warn_insufficient_qty rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }
    
    public void updateBatch(List<Stock_warn_insufficient_qty> list){
    }

    @Override
    public Stock_warn_insufficient_qty get(Integer id) {
        stock_warn_insufficient_qtyClientModel clientModel = new stock_warn_insufficient_qtyClientModel();
        clientModel.setId(id);
		stock_warn_insufficient_qtyOdooClient.get(clientModel);
        Stock_warn_insufficient_qty et = convert2Domain(clientModel,null);
        if(et==null){
            et=new Stock_warn_insufficient_qty();
            et.setId(id);
        }
        else{
        }

        return  et;
    }

    @Override
    public boolean remove(Integer id) {
        stock_warn_insufficient_qtyClientModel clientModel = new stock_warn_insufficient_qtyClientModel();
        clientModel.setId(id);
		stock_warn_insufficient_qtyOdooClient.remove(clientModel);
        return true; 
    }

    public void removeBatch(Collection<Integer> idList){
    }

    @Override
    public boolean create(Stock_warn_insufficient_qty et) {
        stock_warn_insufficient_qtyClientModel clientModel = convert2Model(et,null);
		stock_warn_insufficient_qtyOdooClient.create(clientModel);
        Stock_warn_insufficient_qty rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Stock_warn_insufficient_qty> list){
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Stock_warn_insufficient_qty> searchDefault(Stock_warn_insufficient_qtySearchContext context) {
        List<Stock_warn_insufficient_qty> list = new ArrayList<Stock_warn_insufficient_qty>();
        Page<stock_warn_insufficient_qtyClientModel> clientModelList = stock_warn_insufficient_qtyOdooClient.search(context);
        for(stock_warn_insufficient_qtyClientModel clientModel : clientModelList){
            list.add( this.convert2Domain(clientModel,null));
        }
        return new PageImpl<Stock_warn_insufficient_qty>(list, context.getPageable(), clientModelList.getTotalElements());
    }

    public stock_warn_insufficient_qtyClientModel convert2Model(Stock_warn_insufficient_qty domain , stock_warn_insufficient_qtyClientModel model) {
        if(domain == null)
            return model ;
        if(model==null){
            model = new stock_warn_insufficient_qtyClientModel();

        }
        if((Boolean) domain.getExtensionparams().get("dirtyflagenable")){
            if((Boolean) domain.getExtensionparams().get("iddirtyflag"))
                model.setId(domain.getId());
            if((Boolean) domain.getExtensionparams().get("display_namedirtyflag"))
                model.setDisplay_name(domain.getDisplayName());
            if((Boolean) domain.getExtensionparams().get("quant_idsdirtyflag"))
                model.setQuant_ids(domain.getQuantIds());
            if((Boolean) domain.getExtensionparams().get("__last_updatedirtyflag"))
                model.set__last_update(domain.getLastUpdate());
            if((Boolean) domain.getExtensionparams().get("location_id_textdirtyflag"))
                model.setLocation_id_text(domain.getLocationIdText());
            if((Boolean) domain.getExtensionparams().get("product_id_textdirtyflag"))
                model.setProduct_id_text(domain.getProductIdText());
            if((Boolean) domain.getExtensionparams().get("location_iddirtyflag"))
                model.setLocation_id(domain.getLocationId());
            if((Boolean) domain.getExtensionparams().get("product_iddirtyflag"))
                model.setProduct_id(domain.getProductId());
            domain.getFocusNull().clear();
        }
        return model ;
    }

    public Stock_warn_insufficient_qty convert2Domain( stock_warn_insufficient_qtyClientModel model ,Stock_warn_insufficient_qty domain) {
        if(model == null)
            return domain ;
        if(domain == null){
            domain = new Stock_warn_insufficient_qty();
        }

        if(model.getIdDirtyFlag())
            domain.setId(model.getId());
        if(model.getDisplay_nameDirtyFlag())
            domain.setDisplayName(model.getDisplay_name());
        if(model.getQuant_idsDirtyFlag())
            domain.setQuantIds(model.getQuant_ids());
        if(model.get__last_updateDirtyFlag())
            domain.setLastUpdate(model.get__last_update());
        if(model.getLocation_id_textDirtyFlag())
            domain.setLocationIdText(model.getLocation_id_text());
        if(model.getProduct_id_textDirtyFlag())
            domain.setProductIdText(model.getProduct_id_text());
        if(model.getLocation_idDirtyFlag())
            domain.setLocationId(model.getLocation_id());
        if(model.getProduct_idDirtyFlag())
            domain.setProductId(model.getProduct_id());
        return domain ;
    }

}

    



