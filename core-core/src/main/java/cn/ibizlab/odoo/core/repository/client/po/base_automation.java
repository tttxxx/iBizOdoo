package cn.ibizlab.odoo.core.repository.client.po;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigDecimal;

/**
 * [base_automation] 对象
 */
public interface base_automation {

    public String getActive();

    public void setActive(String active);

    public Integer getActivity_date_deadline_range();

    public void setActivity_date_deadline_range(Integer activity_date_deadline_range);

    public String getActivity_date_deadline_range_type();

    public void setActivity_date_deadline_range_type(String activity_date_deadline_range_type);

    public String getActivity_note();

    public void setActivity_note(String activity_note);

    public String getActivity_summary();

    public void setActivity_summary(String activity_summary);

    public Integer getActivity_type_id();

    public void setActivity_type_id(Integer activity_type_id);

    public String getActivity_type_id_text();

    public void setActivity_type_id_text(String activity_type_id_text);

    public String getActivity_user_field_name();

    public void setActivity_user_field_name(String activity_user_field_name);

    public Integer getActivity_user_id();

    public void setActivity_user_id(Integer activity_user_id);

    public String getActivity_user_id_text();

    public void setActivity_user_id_text(String activity_user_id_text);

    public String getActivity_user_type();

    public void setActivity_user_type(String activity_user_type);

    public String getBinding_type();

    public void setBinding_type(String binding_type);

    public String getChannel_ids();

    public void setChannel_ids(String channel_ids);

    public String getChild_ids();

    public void setChild_ids(String child_ids);

    public String getCode();

    public void setCode(String code);

    public Timestamp getCreate_date();

    public void setCreate_date(Timestamp create_date);

    public Integer getCreate_uid();

    public void setCreate_uid(Integer create_uid);

    public String getCreate_uid_text();

    public void setCreate_uid_text(String create_uid_text);

    public String getCrud_model_name();

    public void setCrud_model_name(String crud_model_name);

    public String getDisplay_name();

    public void setDisplay_name(String display_name);

    public String getFields_lines();

    public void setFields_lines(String fields_lines);

    public String getFilter_domain();

    public void setFilter_domain(String filter_domain);

    public String getFilter_pre_domain();

    public void setFilter_pre_domain(String filter_pre_domain);

    public String getHelp();

    public void setHelp(String help);

    public Integer getId();

    public void setId(Integer id);

    public Timestamp getLast_run();

    public void setLast_run(Timestamp last_run);

    public String getModel_name();

    public void setModel_name(String model_name);

    public String getName();

    public void setName(String name);

    public String getOn_change_fields();

    public void setOn_change_fields(String on_change_fields);

    public String getPartner_ids();

    public void setPartner_ids(String partner_ids);

    public Integer getSequence();

    public void setSequence(Integer sequence);

    public String getState();

    public void setState(String state);

    public Integer getTemplate_id();

    public void setTemplate_id(Integer template_id);

    public String getTemplate_id_text();

    public void setTemplate_id_text(String template_id_text);

    public Integer getTrg_date_calendar_id();

    public void setTrg_date_calendar_id(Integer trg_date_calendar_id);

    public String getTrg_date_calendar_id_text();

    public void setTrg_date_calendar_id_text(String trg_date_calendar_id_text);

    public Integer getTrg_date_range();

    public void setTrg_date_range(Integer trg_date_range);

    public String getTrg_date_range_type();

    public void setTrg_date_range_type(String trg_date_range_type);

    public String getTrigger();

    public void setTrigger(String trigger);

    public String getType();

    public void setType(String type);

    public String getUsage();

    public void setUsage(String usage);

    public String getWebsite_path();

    public void setWebsite_path(String website_path);

    public String getWebsite_published();

    public void setWebsite_published(String website_published);

    public String getWebsite_url();

    public void setWebsite_url(String website_url);

    public Timestamp getWrite_date();

    public void setWrite_date(Timestamp write_date);

    public Integer getWrite_uid();

    public void setWrite_uid(Integer write_uid);

    public String getWrite_uid_text();

    public void setWrite_uid_text(String write_uid_text);

    public String getXml_id();

    public void setXml_id(String xml_id);

    public Timestamp get__last_update();

    public void set__last_update(Timestamp __last_update);

}
