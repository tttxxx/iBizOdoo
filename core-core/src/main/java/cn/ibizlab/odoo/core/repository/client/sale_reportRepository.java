package cn.ibizlab.odoo.core.repository.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import cn.ibizlab.odoo.core.repository.client.po.sale_report;

/**
 * 实体[sale_report] 服务对象接口
 */
public interface sale_reportRepository{


    public sale_report createPO() ;
        public List<sale_report> search();

        public void get(String id);

        public void removeBatch(String id);

        public void update(sale_report sale_report);

        public void remove(String id);

        public void create(sale_report sale_report);

        public void createBatch(sale_report sale_report);

        public void updateBatch(sale_report sale_report);


}
