package cn.ibizlab.odoo.core.repository.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import cn.ibizlab.odoo.core.repository.client.po.hr_recruitment_stage;

/**
 * 实体[hr_recruitment_stage] 服务对象接口
 */
public interface hr_recruitment_stageRepository{


    public hr_recruitment_stage createPO() ;
        public void updateBatch(hr_recruitment_stage hr_recruitment_stage);

        public List<hr_recruitment_stage> search();

        public void create(hr_recruitment_stage hr_recruitment_stage);

        public void remove(String id);

        public void get(String id);

        public void createBatch(hr_recruitment_stage hr_recruitment_stage);

        public void update(hr_recruitment_stage hr_recruitment_stage);

        public void removeBatch(String id);


}
