package cn.ibizlab.odoo.core.repository.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import cn.ibizlab.odoo.core.repository.client.po.hr_employee_category;

/**
 * 实体[hr_employee_category] 服务对象接口
 */
public interface hr_employee_categoryRepository{


    public hr_employee_category createPO() ;
        public void updateBatch(hr_employee_category hr_employee_category);

        public List<hr_employee_category> search();

        public void get(String id);

        public void createBatch(hr_employee_category hr_employee_category);

        public void update(hr_employee_category hr_employee_category);

        public void removeBatch(String id);

        public void create(hr_employee_category hr_employee_category);

        public void remove(String id);


}
