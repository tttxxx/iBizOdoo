package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Iproduct_removal;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[product_removal] 服务对象接口
 */
public interface Iproduct_removalClientService{

    public Iproduct_removal createModel() ;

    public void remove(Iproduct_removal product_removal);

    public void removeBatch(List<Iproduct_removal> product_removals);

    public void createBatch(List<Iproduct_removal> product_removals);

    public Page<Iproduct_removal> search(SearchContext context);

    public void create(Iproduct_removal product_removal);

    public void updateBatch(List<Iproduct_removal> product_removals);

    public void get(Iproduct_removal product_removal);

    public void update(Iproduct_removal product_removal);

    public Page<Iproduct_removal> select(SearchContext context);

}
