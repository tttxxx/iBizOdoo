package cn.ibizlab.odoo.core.odoo_mail.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_mail.domain.Mail_mass_mailing_stage;
import cn.ibizlab.odoo.core.odoo_mail.filter.Mail_mass_mailing_stageSearchContext;
import cn.ibizlab.odoo.core.odoo_mail.service.IMail_mass_mailing_stageService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;



import cn.ibizlab.odoo.core.odoo_mail.client.mail_mass_mailing_stageOdooClient;
import cn.ibizlab.odoo.core.odoo_mail.clientmodel.mail_mass_mailing_stageClientModel;

/**
 * 实体[群发邮件营销阶段] 服务对象接口实现
 */
@Slf4j
@Service
public class Mail_mass_mailing_stageServiceImpl implements IMail_mass_mailing_stageService {

    @Autowired
    mail_mass_mailing_stageOdooClient mail_mass_mailing_stageOdooClient;


    @Override
    public boolean update(Mail_mass_mailing_stage et) {
        mail_mass_mailing_stageClientModel clientModel = convert2Model(et,null);
		mail_mass_mailing_stageOdooClient.update(clientModel);
        Mail_mass_mailing_stage rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }
    
    public void updateBatch(List<Mail_mass_mailing_stage> list){
    }

    @Override
    public boolean create(Mail_mass_mailing_stage et) {
        mail_mass_mailing_stageClientModel clientModel = convert2Model(et,null);
		mail_mass_mailing_stageOdooClient.create(clientModel);
        Mail_mass_mailing_stage rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Mail_mass_mailing_stage> list){
    }

    @Override
    public Mail_mass_mailing_stage get(Integer id) {
        mail_mass_mailing_stageClientModel clientModel = new mail_mass_mailing_stageClientModel();
        clientModel.setId(id);
		mail_mass_mailing_stageOdooClient.get(clientModel);
        Mail_mass_mailing_stage et = convert2Domain(clientModel,null);
        if(et==null){
            et=new Mail_mass_mailing_stage();
            et.setId(id);
        }
        else{
        }

        return  et;
    }

    @Override
    public boolean remove(Integer id) {
        mail_mass_mailing_stageClientModel clientModel = new mail_mass_mailing_stageClientModel();
        clientModel.setId(id);
		mail_mass_mailing_stageOdooClient.remove(clientModel);
        return true; 
    }

    public void removeBatch(Collection<Integer> idList){
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Mail_mass_mailing_stage> searchDefault(Mail_mass_mailing_stageSearchContext context) {
        List<Mail_mass_mailing_stage> list = new ArrayList<Mail_mass_mailing_stage>();
        Page<mail_mass_mailing_stageClientModel> clientModelList = mail_mass_mailing_stageOdooClient.search(context);
        for(mail_mass_mailing_stageClientModel clientModel : clientModelList){
            list.add( this.convert2Domain(clientModel,null));
        }
        return new PageImpl<Mail_mass_mailing_stage>(list, context.getPageable(), clientModelList.getTotalElements());
    }

    public mail_mass_mailing_stageClientModel convert2Model(Mail_mass_mailing_stage domain , mail_mass_mailing_stageClientModel model) {
        if(domain == null)
            return model ;
        if(model==null){
            model = new mail_mass_mailing_stageClientModel();

        }
        if((Boolean) domain.getExtensionparams().get("dirtyflagenable")){
            if((Boolean) domain.getExtensionparams().get("create_datedirtyflag"))
                model.setCreate_date(domain.getCreateDate());
            if((Boolean) domain.getExtensionparams().get("__last_updatedirtyflag"))
                model.set__last_update(domain.getLastUpdate());
            if((Boolean) domain.getExtensionparams().get("write_datedirtyflag"))
                model.setWrite_date(domain.getWriteDate());
            if((Boolean) domain.getExtensionparams().get("namedirtyflag"))
                model.setName(domain.getName());
            if((Boolean) domain.getExtensionparams().get("iddirtyflag"))
                model.setId(domain.getId());
            if((Boolean) domain.getExtensionparams().get("sequencedirtyflag"))
                model.setSequence(domain.getSequence());
            if((Boolean) domain.getExtensionparams().get("display_namedirtyflag"))
                model.setDisplay_name(domain.getDisplayName());
            if((Boolean) domain.getExtensionparams().get("create_uid_textdirtyflag"))
                model.setCreate_uid_text(domain.getCreateUidText());
            if((Boolean) domain.getExtensionparams().get("write_uid_textdirtyflag"))
                model.setWrite_uid_text(domain.getWriteUidText());
            if((Boolean) domain.getExtensionparams().get("write_uiddirtyflag"))
                model.setWrite_uid(domain.getWriteUid());
            if((Boolean) domain.getExtensionparams().get("create_uiddirtyflag"))
                model.setCreate_uid(domain.getCreateUid());
            domain.getFocusNull().clear();
        }
        return model ;
    }

    public Mail_mass_mailing_stage convert2Domain( mail_mass_mailing_stageClientModel model ,Mail_mass_mailing_stage domain) {
        if(model == null)
            return domain ;
        if(domain == null){
            domain = new Mail_mass_mailing_stage();
        }

        if(model.getCreate_dateDirtyFlag())
            domain.setCreateDate(model.getCreate_date());
        if(model.get__last_updateDirtyFlag())
            domain.setLastUpdate(model.get__last_update());
        if(model.getWrite_dateDirtyFlag())
            domain.setWriteDate(model.getWrite_date());
        if(model.getNameDirtyFlag())
            domain.setName(model.getName());
        if(model.getIdDirtyFlag())
            domain.setId(model.getId());
        if(model.getSequenceDirtyFlag())
            domain.setSequence(model.getSequence());
        if(model.getDisplay_nameDirtyFlag())
            domain.setDisplayName(model.getDisplay_name());
        if(model.getCreate_uid_textDirtyFlag())
            domain.setCreateUidText(model.getCreate_uid_text());
        if(model.getWrite_uid_textDirtyFlag())
            domain.setWriteUidText(model.getWrite_uid_text());
        if(model.getWrite_uidDirtyFlag())
            domain.setWriteUid(model.getWrite_uid());
        if(model.getCreate_uidDirtyFlag())
            domain.setCreateUid(model.getCreate_uid());
        return domain ;
    }

}

    



