package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Base_update_translations;
import cn.ibizlab.odoo.core.odoo_base.filter.Base_update_translationsSearchContext;

/**
 * 实体 [更新翻译] 存储对象
 */
public interface Base_update_translationsRepository extends Repository<Base_update_translations> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Base_update_translations> searchDefault(Base_update_translationsSearchContext context);

    Base_update_translations convert2PO(cn.ibizlab.odoo.core.odoo_base.domain.Base_update_translations domain , Base_update_translations po) ;

    cn.ibizlab.odoo.core.odoo_base.domain.Base_update_translations convert2Domain( Base_update_translations po ,cn.ibizlab.odoo.core.odoo_base.domain.Base_update_translations domain) ;

}
