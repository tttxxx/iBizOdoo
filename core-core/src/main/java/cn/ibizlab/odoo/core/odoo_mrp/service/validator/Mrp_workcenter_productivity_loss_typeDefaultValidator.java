package cn.ibizlab.odoo.core.odoo_mrp.service.validator;

import java.sql.Timestamp;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import cn.ibizlab.odoo.util.ISearchFilter;
import java.math.BigDecimal;
/**
 * 实体[Mrp_workcenter_productivity_loss_type]的实体值规则[Default] 对象
 */
public class Mrp_workcenter_productivity_loss_typeDefaultValidator implements Validator {
    @Override
    public boolean supports(Class<?> clazz) {
		return false;
	}
    @Override
	public void validate(Object target, Errors errors) {

	}
}
