package cn.ibizlab.odoo.core.odoo_utm.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_utm.domain.Utm_source;
import cn.ibizlab.odoo.core.odoo_utm.filter.Utm_sourceSearchContext;


/**
 * 实体[Utm_source] 服务对象接口
 */
public interface IUtm_sourceService{

    Utm_source get(Integer key) ;
    boolean create(Utm_source et) ;
    void createBatch(List<Utm_source> list) ;
    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    boolean update(Utm_source et) ;
    void updateBatch(List<Utm_source> list) ;
    Page<Utm_source> searchDefault(Utm_sourceSearchContext context) ;

}



