package cn.ibizlab.odoo.core.odoo_account.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_account.domain.Account_invoice_import_wizard;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_invoice_import_wizardSearchContext;
import cn.ibizlab.odoo.core.odoo_account.service.IAccount_invoice_import_wizardService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;



import cn.ibizlab.odoo.core.odoo_account.client.account_invoice_import_wizardOdooClient;
import cn.ibizlab.odoo.core.odoo_account.clientmodel.account_invoice_import_wizardClientModel;

/**
 * 实体[从文件中导入你的供应商账单。] 服务对象接口实现
 */
@Slf4j
@Service
public class Account_invoice_import_wizardServiceImpl implements IAccount_invoice_import_wizardService {

    @Autowired
    account_invoice_import_wizardOdooClient account_invoice_import_wizardOdooClient;


    @Override
    public boolean remove(Integer id) {
        account_invoice_import_wizardClientModel clientModel = new account_invoice_import_wizardClientModel();
        clientModel.setId(id);
		account_invoice_import_wizardOdooClient.remove(clientModel);
        return true; 
    }

    public void removeBatch(Collection<Integer> idList){
    }

    @Override
    public boolean update(Account_invoice_import_wizard et) {
        account_invoice_import_wizardClientModel clientModel = convert2Model(et,null);
		account_invoice_import_wizardOdooClient.update(clientModel);
        Account_invoice_import_wizard rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }
    
    public void updateBatch(List<Account_invoice_import_wizard> list){
    }

    @Override
    public Account_invoice_import_wizard get(Integer id) {
        account_invoice_import_wizardClientModel clientModel = new account_invoice_import_wizardClientModel();
        clientModel.setId(id);
		account_invoice_import_wizardOdooClient.get(clientModel);
        Account_invoice_import_wizard et = convert2Domain(clientModel,null);
        if(et==null){
            et=new Account_invoice_import_wizard();
            et.setId(id);
        }
        else{
        }

        return  et;
    }

    @Override
    public boolean create(Account_invoice_import_wizard et) {
        account_invoice_import_wizardClientModel clientModel = convert2Model(et,null);
		account_invoice_import_wizardOdooClient.create(clientModel);
        Account_invoice_import_wizard rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Account_invoice_import_wizard> list){
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Account_invoice_import_wizard> searchDefault(Account_invoice_import_wizardSearchContext context) {
        List<Account_invoice_import_wizard> list = new ArrayList<Account_invoice_import_wizard>();
        Page<account_invoice_import_wizardClientModel> clientModelList = account_invoice_import_wizardOdooClient.search(context);
        for(account_invoice_import_wizardClientModel clientModel : clientModelList){
            list.add( this.convert2Domain(clientModel,null));
        }
        return new PageImpl<Account_invoice_import_wizard>(list, context.getPageable(), clientModelList.getTotalElements());
    }

    public account_invoice_import_wizardClientModel convert2Model(Account_invoice_import_wizard domain , account_invoice_import_wizardClientModel model) {
        if(domain == null)
            return model ;
        if(model==null){
            model = new account_invoice_import_wizardClientModel();

        }
        if((Boolean) domain.getExtensionparams().get("dirtyflagenable")){
            if((Boolean) domain.getExtensionparams().get("iddirtyflag"))
                model.setId(domain.getId());
            if((Boolean) domain.getExtensionparams().get("write_datedirtyflag"))
                model.setWrite_date(domain.getWriteDate());
            if((Boolean) domain.getExtensionparams().get("create_datedirtyflag"))
                model.setCreate_date(domain.getCreateDate());
            if((Boolean) domain.getExtensionparams().get("display_namedirtyflag"))
                model.setDisplay_name(domain.getDisplayName());
            if((Boolean) domain.getExtensionparams().get("attachment_idsdirtyflag"))
                model.setAttachment_ids(domain.getAttachmentIds());
            if((Boolean) domain.getExtensionparams().get("__last_updatedirtyflag"))
                model.set__last_update(domain.getLastUpdate());
            if((Boolean) domain.getExtensionparams().get("create_uid_textdirtyflag"))
                model.setCreate_uid_text(domain.getCreateUidText());
            if((Boolean) domain.getExtensionparams().get("write_uid_textdirtyflag"))
                model.setWrite_uid_text(domain.getWriteUidText());
            if((Boolean) domain.getExtensionparams().get("journal_id_textdirtyflag"))
                model.setJournal_id_text(domain.getJournalIdText());
            if((Boolean) domain.getExtensionparams().get("journal_iddirtyflag"))
                model.setJournal_id(domain.getJournalId());
            if((Boolean) domain.getExtensionparams().get("write_uiddirtyflag"))
                model.setWrite_uid(domain.getWriteUid());
            if((Boolean) domain.getExtensionparams().get("create_uiddirtyflag"))
                model.setCreate_uid(domain.getCreateUid());
            domain.getFocusNull().clear();
        }
        return model ;
    }

    public Account_invoice_import_wizard convert2Domain( account_invoice_import_wizardClientModel model ,Account_invoice_import_wizard domain) {
        if(model == null)
            return domain ;
        if(domain == null){
            domain = new Account_invoice_import_wizard();
        }

        if(model.getIdDirtyFlag())
            domain.setId(model.getId());
        if(model.getWrite_dateDirtyFlag())
            domain.setWriteDate(model.getWrite_date());
        if(model.getCreate_dateDirtyFlag())
            domain.setCreateDate(model.getCreate_date());
        if(model.getDisplay_nameDirtyFlag())
            domain.setDisplayName(model.getDisplay_name());
        if(model.getAttachment_idsDirtyFlag())
            domain.setAttachmentIds(model.getAttachment_ids());
        if(model.get__last_updateDirtyFlag())
            domain.setLastUpdate(model.get__last_update());
        if(model.getCreate_uid_textDirtyFlag())
            domain.setCreateUidText(model.getCreate_uid_text());
        if(model.getWrite_uid_textDirtyFlag())
            domain.setWriteUidText(model.getWrite_uid_text());
        if(model.getJournal_id_textDirtyFlag())
            domain.setJournalIdText(model.getJournal_id_text());
        if(model.getJournal_idDirtyFlag())
            domain.setJournalId(model.getJournal_id());
        if(model.getWrite_uidDirtyFlag())
            domain.setWriteUid(model.getWrite_uid());
        if(model.getCreate_uidDirtyFlag())
            domain.setCreateUid(model.getCreate_uid());
        return domain ;
    }

}

    



