package cn.ibizlab.odoo.core.odoo_stock.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_stock.domain.Stock_change_standard_price;
import cn.ibizlab.odoo.core.odoo_stock.filter.Stock_change_standard_priceSearchContext;
import cn.ibizlab.odoo.core.odoo_stock.service.IStock_change_standard_priceService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;



import cn.ibizlab.odoo.core.odoo_stock.client.stock_change_standard_priceOdooClient;
import cn.ibizlab.odoo.core.odoo_stock.clientmodel.stock_change_standard_priceClientModel;

/**
 * 实体[更改标准价] 服务对象接口实现
 */
@Slf4j
@Service
public class Stock_change_standard_priceServiceImpl implements IStock_change_standard_priceService {

    @Autowired
    stock_change_standard_priceOdooClient stock_change_standard_priceOdooClient;


    @Override
    public boolean remove(Integer id) {
        stock_change_standard_priceClientModel clientModel = new stock_change_standard_priceClientModel();
        clientModel.setId(id);
		stock_change_standard_priceOdooClient.remove(clientModel);
        return true; 
    }

    public void removeBatch(Collection<Integer> idList){
    }

    @Override
    public Stock_change_standard_price get(Integer id) {
        stock_change_standard_priceClientModel clientModel = new stock_change_standard_priceClientModel();
        clientModel.setId(id);
		stock_change_standard_priceOdooClient.get(clientModel);
        Stock_change_standard_price et = convert2Domain(clientModel,null);
        if(et==null){
            et=new Stock_change_standard_price();
            et.setId(id);
        }
        else{
        }

        return  et;
    }

    @Override
    public boolean create(Stock_change_standard_price et) {
        stock_change_standard_priceClientModel clientModel = convert2Model(et,null);
		stock_change_standard_priceOdooClient.create(clientModel);
        Stock_change_standard_price rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Stock_change_standard_price> list){
    }

    @Override
    public boolean update(Stock_change_standard_price et) {
        stock_change_standard_priceClientModel clientModel = convert2Model(et,null);
		stock_change_standard_priceOdooClient.update(clientModel);
        Stock_change_standard_price rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }
    
    public void updateBatch(List<Stock_change_standard_price> list){
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Stock_change_standard_price> searchDefault(Stock_change_standard_priceSearchContext context) {
        List<Stock_change_standard_price> list = new ArrayList<Stock_change_standard_price>();
        Page<stock_change_standard_priceClientModel> clientModelList = stock_change_standard_priceOdooClient.search(context);
        for(stock_change_standard_priceClientModel clientModel : clientModelList){
            list.add( this.convert2Domain(clientModel,null));
        }
        return new PageImpl<Stock_change_standard_price>(list, context.getPageable(), clientModelList.getTotalElements());
    }

    public stock_change_standard_priceClientModel convert2Model(Stock_change_standard_price domain , stock_change_standard_priceClientModel model) {
        if(domain == null)
            return model ;
        if(model==null){
            model = new stock_change_standard_priceClientModel();

        }
        if((Boolean) domain.getExtensionparams().get("dirtyflagenable")){
            if((Boolean) domain.getExtensionparams().get("iddirtyflag"))
                model.setId(domain.getId());
            if((Boolean) domain.getExtensionparams().get("counterpart_account_id_requireddirtyflag"))
                model.setCounterpart_account_id_required(domain.getCounterpartAccountIdRequired());
            if((Boolean) domain.getExtensionparams().get("create_datedirtyflag"))
                model.setCreate_date(domain.getCreateDate());
            if((Boolean) domain.getExtensionparams().get("new_pricedirtyflag"))
                model.setNew_price(domain.getNewPrice());
            if((Boolean) domain.getExtensionparams().get("__last_updatedirtyflag"))
                model.set__last_update(domain.getLastUpdate());
            if((Boolean) domain.getExtensionparams().get("display_namedirtyflag"))
                model.setDisplay_name(domain.getDisplayName());
            if((Boolean) domain.getExtensionparams().get("write_datedirtyflag"))
                model.setWrite_date(domain.getWriteDate());
            if((Boolean) domain.getExtensionparams().get("counterpart_account_id_textdirtyflag"))
                model.setCounterpart_account_id_text(domain.getCounterpartAccountIdText());
            if((Boolean) domain.getExtensionparams().get("create_uid_textdirtyflag"))
                model.setCreate_uid_text(domain.getCreateUidText());
            if((Boolean) domain.getExtensionparams().get("write_uid_textdirtyflag"))
                model.setWrite_uid_text(domain.getWriteUidText());
            if((Boolean) domain.getExtensionparams().get("write_uiddirtyflag"))
                model.setWrite_uid(domain.getWriteUid());
            if((Boolean) domain.getExtensionparams().get("create_uiddirtyflag"))
                model.setCreate_uid(domain.getCreateUid());
            if((Boolean) domain.getExtensionparams().get("counterpart_account_iddirtyflag"))
                model.setCounterpart_account_id(domain.getCounterpartAccountId());
            domain.getFocusNull().clear();
        }
        return model ;
    }

    public Stock_change_standard_price convert2Domain( stock_change_standard_priceClientModel model ,Stock_change_standard_price domain) {
        if(model == null)
            return domain ;
        if(domain == null){
            domain = new Stock_change_standard_price();
        }

        if(model.getIdDirtyFlag())
            domain.setId(model.getId());
        if(model.getCounterpart_account_id_requiredDirtyFlag())
            domain.setCounterpartAccountIdRequired(model.getCounterpart_account_id_required());
        if(model.getCreate_dateDirtyFlag())
            domain.setCreateDate(model.getCreate_date());
        if(model.getNew_priceDirtyFlag())
            domain.setNewPrice(model.getNew_price());
        if(model.get__last_updateDirtyFlag())
            domain.setLastUpdate(model.get__last_update());
        if(model.getDisplay_nameDirtyFlag())
            domain.setDisplayName(model.getDisplay_name());
        if(model.getWrite_dateDirtyFlag())
            domain.setWriteDate(model.getWrite_date());
        if(model.getCounterpart_account_id_textDirtyFlag())
            domain.setCounterpartAccountIdText(model.getCounterpart_account_id_text());
        if(model.getCreate_uid_textDirtyFlag())
            domain.setCreateUidText(model.getCreate_uid_text());
        if(model.getWrite_uid_textDirtyFlag())
            domain.setWriteUidText(model.getWrite_uid_text());
        if(model.getWrite_uidDirtyFlag())
            domain.setWriteUid(model.getWrite_uid());
        if(model.getCreate_uidDirtyFlag())
            domain.setCreateUid(model.getCreate_uid());
        if(model.getCounterpart_account_idDirtyFlag())
            domain.setCounterpartAccountId(model.getCounterpart_account_id());
        return domain ;
    }

}

    



