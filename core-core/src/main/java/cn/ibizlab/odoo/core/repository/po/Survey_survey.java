package cn.ibizlab.odoo.core.repository.po ;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.math.BigInteger;

import cn.ibizlab.odoo.core.odoo_survey.filter.Survey_surveySearchContext;

/**
 * 实体 [问卷] 存储模型
 */
public interface Survey_survey{

    /**
     * 关注者(业务伙伴)
     */
    String getMessage_partner_ids();

    void setMessage_partner_ids(String message_partner_ids);

    /**
     * 获取 [关注者(业务伙伴)]脏标记
     */
    boolean getMessage_partner_idsDirtyFlag();

    /**
     * 创建时间
     */
    Timestamp getCreate_date();

    void setCreate_date(Timestamp create_date);

    /**
     * 获取 [创建时间]脏标记
     */
    boolean getCreate_dateDirtyFlag();

    /**
     * 显示名称
     */
    String getDisplay_name();

    void setDisplay_name(String display_name);

    /**
     * 获取 [显示名称]脏标记
     */
    boolean getDisplay_nameDirtyFlag();

    /**
     * 网站信息
     */
    String getWebsite_message_ids();

    void setWebsite_message_ids(String website_message_ids);

    /**
     * 获取 [网站信息]脏标记
     */
    boolean getWebsite_message_idsDirtyFlag();

    /**
     * 用户可返回
     */
    String getUsers_can_go_back();

    void setUsers_can_go_back(String users_can_go_back);

    /**
     * 获取 [用户可返回]脏标记
     */
    boolean getUsers_can_go_backDirtyFlag();

    /**
     * 关注者(渠道)
     */
    String getMessage_channel_ids();

    void setMessage_channel_ids(String message_channel_ids);

    /**
     * 获取 [关注者(渠道)]脏标记
     */
    boolean getMessage_channel_idsDirtyFlag();

    /**
     * 未读消息
     */
    String getMessage_unread();

    void setMessage_unread(String message_unread);

    /**
     * 获取 [未读消息]脏标记
     */
    boolean getMessage_unreadDirtyFlag();

    /**
     * 消息递送错误
     */
    String getMessage_has_error();

    void setMessage_has_error(String message_has_error);

    /**
     * 获取 [消息递送错误]脏标记
     */
    boolean getMessage_has_errorDirtyFlag();

    /**
     * 消息
     */
    String getMessage_ids();

    void setMessage_ids(String message_ids);

    /**
     * 获取 [消息]脏标记
     */
    boolean getMessage_idsDirtyFlag();

    /**
     * ID
     */
    Integer getId();

    void setId(Integer id);

    /**
     * 获取 [ID]脏标记
     */
    boolean getIdDirtyFlag();

    /**
     * 已开始的调查数量
     */
    Integer getTot_start_survey();

    void setTot_start_survey(Integer tot_start_survey);

    /**
     * 获取 [已开始的调查数量]脏标记
     */
    boolean getTot_start_surveyDirtyFlag();

    /**
     * 结果链接
     */
    String getResult_url();

    void setResult_url(String result_url);

    /**
     * 获取 [结果链接]脏标记
     */
    boolean getResult_urlDirtyFlag();

    /**
     * 打印链接
     */
    String getPrint_url();

    void setPrint_url(String print_url);

    /**
     * 获取 [打印链接]脏标记
     */
    boolean getPrint_urlDirtyFlag();

    /**
     * 测验模式
     */
    String getQuizz_mode();

    void setQuizz_mode(String quizz_mode);

    /**
     * 获取 [测验模式]脏标记
     */
    boolean getQuizz_modeDirtyFlag();

    /**
     * 最后修改日
     */
    Timestamp get__last_update();

    void set__last_update(Timestamp __last_update);

    /**
     * 获取 [最后修改日]脏标记
     */
    boolean get__last_updateDirtyFlag();

    /**
     * 已完成的调查数量
     */
    Integer getTot_comp_survey();

    void setTot_comp_survey(Integer tot_comp_survey);

    /**
     * 获取 [已完成的调查数量]脏标记
     */
    boolean getTot_comp_surveyDirtyFlag();

    /**
     * 最后更新时间
     */
    Timestamp getWrite_date();

    void setWrite_date(Timestamp write_date);

    /**
     * 获取 [最后更新时间]脏标记
     */
    boolean getWrite_dateDirtyFlag();

    /**
     * 未读消息计数器
     */
    Integer getMessage_unread_counter();

    void setMessage_unread_counter(Integer message_unread_counter);

    /**
     * 获取 [未读消息计数器]脏标记
     */
    boolean getMessage_unread_counterDirtyFlag();

    /**
     * 下一活动摘要
     */
    String getActivity_summary();

    void setActivity_summary(String activity_summary);

    /**
     * 获取 [下一活动摘要]脏标记
     */
    boolean getActivity_summaryDirtyFlag();

    /**
     * 公开链接（HTML版）
     */
    String getPublic_url_html();

    void setPublic_url_html(String public_url_html);

    /**
     * 获取 [公开链接（HTML版）]脏标记
     */
    boolean getPublic_url_htmlDirtyFlag();

    /**
     * 下一活动类型
     */
    Integer getActivity_type_id();

    void setActivity_type_id(Integer activity_type_id);

    /**
     * 获取 [下一活动类型]脏标记
     */
    boolean getActivity_type_idDirtyFlag();

    /**
     * 附件
     */
    Integer getMessage_main_attachment_id();

    void setMessage_main_attachment_id(Integer message_main_attachment_id);

    /**
     * 获取 [附件]脏标记
     */
    boolean getMessage_main_attachment_idDirtyFlag();

    /**
     * 关注者
     */
    String getMessage_follower_ids();

    void setMessage_follower_ids(String message_follower_ids);

    /**
     * 获取 [关注者]脏标记
     */
    boolean getMessage_follower_idsDirtyFlag();

    /**
     * 是关注者
     */
    String getMessage_is_follower();

    void setMessage_is_follower(String message_is_follower);

    /**
     * 获取 [是关注者]脏标记
     */
    boolean getMessage_is_followerDirtyFlag();

    /**
     * 用户回应
     */
    String getUser_input_ids();

    void setUser_input_ids(String user_input_ids);

    /**
     * 获取 [用户回应]脏标记
     */
    boolean getUser_input_idsDirtyFlag();

    /**
     * 颜色索引
     */
    Integer getColor();

    void setColor(Integer color);

    /**
     * 获取 [颜色索引]脏标记
     */
    boolean getColorDirtyFlag();

    /**
     * 页面
     */
    String getPage_ids();

    void setPage_ids(String page_ids);

    /**
     * 获取 [页面]脏标记
     */
    boolean getPage_idsDirtyFlag();

    /**
     * 需要登录
     */
    String getAuth_required();

    void setAuth_required(String auth_required);

    /**
     * 获取 [需要登录]脏标记
     */
    boolean getAuth_requiredDirtyFlag();

    /**
     * 说明
     */
    String getDescription();

    void setDescription(String description);

    /**
     * 获取 [说明]脏标记
     */
    boolean getDescriptionDirtyFlag();

    /**
     * 行动数量
     */
    Integer getMessage_needaction_counter();

    void setMessage_needaction_counter(Integer message_needaction_counter);

    /**
     * 获取 [行动数量]脏标记
     */
    boolean getMessage_needaction_counterDirtyFlag();

    /**
     * 已发送调查者数量
     */
    Integer getTot_sent_survey();

    void setTot_sent_survey(Integer tot_sent_survey);

    /**
     * 获取 [已发送调查者数量]脏标记
     */
    boolean getTot_sent_surveyDirtyFlag();

    /**
     * 责任用户
     */
    Integer getActivity_user_id();

    void setActivity_user_id(Integer activity_user_id);

    /**
     * 获取 [责任用户]脏标记
     */
    boolean getActivity_user_idDirtyFlag();

    /**
     * 活动
     */
    String getActivity_ids();

    void setActivity_ids(String activity_ids);

    /**
     * 获取 [活动]脏标记
     */
    boolean getActivity_idsDirtyFlag();

    /**
     * 附件数量
     */
    Integer getMessage_attachment_count();

    void setMessage_attachment_count(Integer message_attachment_count);

    /**
     * 获取 [附件数量]脏标记
     */
    boolean getMessage_attachment_countDirtyFlag();

    /**
     * 已经设计了
     */
    String getDesigned();

    void setDesigned(String designed);

    /**
     * 获取 [已经设计了]脏标记
     */
    boolean getDesignedDirtyFlag();

    /**
     * 活动状态
     */
    String getActivity_state();

    void setActivity_state(String activity_state);

    /**
     * 获取 [活动状态]脏标记
     */
    boolean getActivity_stateDirtyFlag();

    /**
     * 错误数
     */
    Integer getMessage_has_error_counter();

    void setMessage_has_error_counter(Integer message_has_error_counter);

    /**
     * 获取 [错误数]脏标记
     */
    boolean getMessage_has_error_counterDirtyFlag();

    /**
     * 称谓
     */
    String getTitle();

    void setTitle(String title);

    /**
     * 获取 [称谓]脏标记
     */
    boolean getTitleDirtyFlag();

    /**
     * 感谢留言
     */
    String getThank_you_message();

    void setThank_you_message(String thank_you_message);

    /**
     * 获取 [感谢留言]脏标记
     */
    boolean getThank_you_messageDirtyFlag();

    /**
     * 下一活动截止日期
     */
    Timestamp getActivity_date_deadline();

    void setActivity_date_deadline(Timestamp activity_date_deadline);

    /**
     * 获取 [下一活动截止日期]脏标记
     */
    boolean getActivity_date_deadlineDirtyFlag();

    /**
     * 公开链接
     */
    String getPublic_url();

    void setPublic_url(String public_url);

    /**
     * 获取 [公开链接]脏标记
     */
    boolean getPublic_urlDirtyFlag();

    /**
     * 有效
     */
    String getActive();

    void setActive(String active);

    /**
     * 获取 [有效]脏标记
     */
    boolean getActiveDirtyFlag();

    /**
     * 采取行动
     */
    String getMessage_needaction();

    void setMessage_needaction(String message_needaction);

    /**
     * 获取 [采取行动]脏标记
     */
    boolean getMessage_needactionDirtyFlag();

    /**
     * 最后更新者
     */
    String getWrite_uid_text();

    void setWrite_uid_text(String write_uid_text);

    /**
     * 获取 [最后更新者]脏标记
     */
    boolean getWrite_uid_textDirtyFlag();

    /**
     * 已关闭
     */
    String getIs_closed();

    void setIs_closed(String is_closed);

    /**
     * 获取 [已关闭]脏标记
     */
    boolean getIs_closedDirtyFlag();

    /**
     * 创建人
     */
    String getCreate_uid_text();

    void setCreate_uid_text(String create_uid_text);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uid_textDirtyFlag();

    /**
     * 阶段
     */
    String getStage_id_text();

    void setStage_id_text(String stage_id_text);

    /**
     * 获取 [阶段]脏标记
     */
    boolean getStage_id_textDirtyFlag();

    /**
     * 邮件模板
     */
    String getEmail_template_id_text();

    void setEmail_template_id_text(String email_template_id_text);

    /**
     * 获取 [邮件模板]脏标记
     */
    boolean getEmail_template_id_textDirtyFlag();

    /**
     * 阶段
     */
    Integer getStage_id();

    void setStage_id(Integer stage_id);

    /**
     * 获取 [阶段]脏标记
     */
    boolean getStage_idDirtyFlag();

    /**
     * 创建人
     */
    Integer getCreate_uid();

    void setCreate_uid(Integer create_uid);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uidDirtyFlag();

    /**
     * 邮件模板
     */
    Integer getEmail_template_id();

    void setEmail_template_id(Integer email_template_id);

    /**
     * 获取 [邮件模板]脏标记
     */
    boolean getEmail_template_idDirtyFlag();

    /**
     * 最后更新者
     */
    Integer getWrite_uid();

    void setWrite_uid(Integer write_uid);

    /**
     * 获取 [最后更新者]脏标记
     */
    boolean getWrite_uidDirtyFlag();

}
