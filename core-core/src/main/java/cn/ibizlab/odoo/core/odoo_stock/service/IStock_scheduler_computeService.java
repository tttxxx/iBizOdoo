package cn.ibizlab.odoo.core.odoo_stock.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_stock.domain.Stock_scheduler_compute;
import cn.ibizlab.odoo.core.odoo_stock.filter.Stock_scheduler_computeSearchContext;


/**
 * 实体[Stock_scheduler_compute] 服务对象接口
 */
public interface IStock_scheduler_computeService{

    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    boolean update(Stock_scheduler_compute et) ;
    void updateBatch(List<Stock_scheduler_compute> list) ;
    boolean create(Stock_scheduler_compute et) ;
    void createBatch(List<Stock_scheduler_compute> list) ;
    Stock_scheduler_compute get(Integer key) ;
    Page<Stock_scheduler_compute> searchDefault(Stock_scheduler_computeSearchContext context) ;

}



