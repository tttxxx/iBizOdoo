package cn.ibizlab.odoo.core.repository.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import cn.ibizlab.odoo.core.repository.client.po.hr_contract_type;

/**
 * 实体[hr_contract_type] 服务对象接口
 */
public interface hr_contract_typeRepository{


    public hr_contract_type createPO() ;
        public void removeBatch(String id);

        public void remove(String id);

        public void createBatch(hr_contract_type hr_contract_type);

        public void updateBatch(hr_contract_type hr_contract_type);

        public void update(hr_contract_type hr_contract_type);

        public void create(hr_contract_type hr_contract_type);

        public void get(String id);

        public List<hr_contract_type> search();


}
