package cn.ibizlab.odoo.core.repository.po ;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.math.BigInteger;

import cn.ibizlab.odoo.core.odoo_mail.filter.Mail_mass_mailingSearchContext;

/**
 * 实体 [群发邮件] 存储模型
 */
public interface Mail_mass_mailing{

    /**
     * 发送日期
     */
    Timestamp getSent_date();

    void setSent_date(Timestamp sent_date);

    /**
     * 获取 [发送日期]脏标记
     */
    boolean getSent_dateDirtyFlag();

    /**
     * 报价个数
     */
    Integer getSale_quotation_count();

    void setSale_quotation_count(Integer sale_quotation_count);

    /**
     * 获取 [报价个数]脏标记
     */
    boolean getSale_quotation_countDirtyFlag();

    /**
     * 创建时间
     */
    Timestamp getCreate_date();

    void setCreate_date(Timestamp create_date);

    /**
     * 获取 [创建时间]脏标记
     */
    boolean getCreate_dateDirtyFlag();

    /**
     * 最后修改日
     */
    Timestamp get__last_update();

    void set__last_update(Timestamp __last_update);

    /**
     * 获取 [最后修改日]脏标记
     */
    boolean get__last_updateDirtyFlag();

    /**
     * 域
     */
    String getMailing_domain();

    void setMailing_domain(String mailing_domain);

    /**
     * 获取 [域]脏标记
     */
    boolean getMailing_domainDirtyFlag();

    /**
     * 已开启
     */
    Integer getOpened();

    void setOpened(Integer opened);

    /**
     * 获取 [已开启]脏标记
     */
    boolean getOpenedDirtyFlag();

    /**
     * 回复
     */
    String getReply_to();

    void setReply_to(String reply_to);

    /**
     * 获取 [回复]脏标记
     */
    boolean getReply_toDirtyFlag();

    /**
     * 已回复
     */
    Integer getReplied();

    void setReplied(Integer replied);

    /**
     * 获取 [已回复]脏标记
     */
    boolean getRepliedDirtyFlag();

    /**
     * 线索总数
     */
    Integer getCrm_lead_count();

    void setCrm_lead_count(Integer crm_lead_count);

    /**
     * 获取 [线索总数]脏标记
     */
    boolean getCrm_lead_countDirtyFlag();

    /**
     * 收件人实物模型
     */
    String getMailing_model_real();

    void setMailing_model_real(String mailing_model_real);

    /**
     * 获取 [收件人实物模型]脏标记
     */
    boolean getMailing_model_realDirtyFlag();

    /**
     * 点击数
     */
    Integer getClicks_ratio();

    void setClicks_ratio(Integer clicks_ratio);

    /**
     * 获取 [点击数]脏标记
     */
    boolean getClicks_ratioDirtyFlag();

    /**
     * 已送货
     */
    Integer getDelivered();

    void setDelivered(Integer delivered);

    /**
     * 获取 [已送货]脏标记
     */
    boolean getDeliveredDirtyFlag();

    /**
     * 收件人模型
     */
    Integer getMailing_model_id();

    void setMailing_model_id(Integer mailing_model_id);

    /**
     * 获取 [收件人模型]脏标记
     */
    boolean getMailing_model_idDirtyFlag();

    /**
     * 回复模式
     */
    String getReply_to_mode();

    void setReply_to_mode(String reply_to_mode);

    /**
     * 获取 [回复模式]脏标记
     */
    boolean getReply_to_modeDirtyFlag();

    /**
     * 在将来计划
     */
    Timestamp getSchedule_date();

    void setSchedule_date(Timestamp schedule_date);

    /**
     * 获取 [在将来计划]脏标记
     */
    boolean getSchedule_dateDirtyFlag();

    /**
     * 从
     */
    String getEmail_from();

    void setEmail_from(String email_from);

    /**
     * 获取 [从]脏标记
     */
    boolean getEmail_fromDirtyFlag();

    /**
     * 显示名称
     */
    String getDisplay_name();

    void setDisplay_name(String display_name);

    /**
     * 获取 [显示名称]脏标记
     */
    boolean getDisplay_nameDirtyFlag();

    /**
     * 开票金额
     */
    Integer getSale_invoiced_amount();

    void setSale_invoiced_amount(Integer sale_invoiced_amount);

    /**
     * 获取 [开票金额]脏标记
     */
    boolean getSale_invoiced_amountDirtyFlag();

    /**
     * 附件
     */
    String getAttachment_ids();

    void setAttachment_ids(String attachment_ids);

    /**
     * 获取 [附件]脏标记
     */
    boolean getAttachment_idsDirtyFlag();

    /**
     * 总计
     */
    Integer getTotal();

    void setTotal(Integer total);

    /**
     * 获取 [总计]脏标记
     */
    boolean getTotalDirtyFlag();

    /**
     * 有效
     */
    String getActive();

    void setActive(String active);

    /**
     * 获取 [有效]脏标记
     */
    boolean getActiveDirtyFlag();

    /**
     * 被退回的比率
     */
    Integer getBounced_ratio();

    void setBounced_ratio(Integer bounced_ratio);

    /**
     * 获取 [被退回的比率]脏标记
     */
    boolean getBounced_ratioDirtyFlag();

    /**
     * 安排的日期
     */
    Timestamp getNext_departure();

    void setNext_departure(Timestamp next_departure);

    /**
     * 获取 [安排的日期]脏标记
     */
    boolean getNext_departureDirtyFlag();

    /**
     * 邮件服务器
     */
    Integer getMail_server_id();

    void setMail_server_id(Integer mail_server_id);

    /**
     * 获取 [邮件服务器]脏标记
     */
    boolean getMail_server_idDirtyFlag();

    /**
     * 忽略
     */
    Integer getIgnored();

    void setIgnored(Integer ignored);

    /**
     * 获取 [忽略]脏标记
     */
    boolean getIgnoredDirtyFlag();

    /**
     * 安排
     */
    Integer getScheduled();

    void setScheduled(Integer scheduled);

    /**
     * 获取 [安排]脏标记
     */
    boolean getScheduledDirtyFlag();

    /**
     * 使用线索
     */
    String getCrm_lead_activated();

    void setCrm_lead_activated(String crm_lead_activated);

    /**
     * 获取 [使用线索]脏标记
     */
    boolean getCrm_lead_activatedDirtyFlag();

    /**
     * 点击率
     */
    Integer getClicked();

    void setClicked(Integer clicked);

    /**
     * 获取 [点击率]脏标记
     */
    boolean getClickedDirtyFlag();

    /**
     * 回复比例
     */
    Integer getReplied_ratio();

    void setReplied_ratio(Integer replied_ratio);

    /**
     * 获取 [回复比例]脏标记
     */
    boolean getReplied_ratioDirtyFlag();

    /**
     * 最后更新时间
     */
    Timestamp getWrite_date();

    void setWrite_date(Timestamp write_date);

    /**
     * 获取 [最后更新时间]脏标记
     */
    boolean getWrite_dateDirtyFlag();

    /**
     * 状态
     */
    String getState();

    void setState(String state);

    /**
     * 获取 [状态]脏标记
     */
    boolean getStateDirtyFlag();

    /**
     * 邮件统计
     */
    String getStatistics_ids();

    void setStatistics_ids(String statistics_ids);

    /**
     * 获取 [邮件统计]脏标记
     */
    boolean getStatistics_idsDirtyFlag();

    /**
     * 打开比例
     */
    Integer getOpened_ratio();

    void setOpened_ratio(Integer opened_ratio);

    /**
     * 获取 [打开比例]脏标记
     */
    boolean getOpened_ratioDirtyFlag();

    /**
     * 正文
     */
    String getBody_html();

    void setBody_html(String body_html);

    /**
     * 获取 [正文]脏标记
     */
    boolean getBody_htmlDirtyFlag();

    /**
     * 被退回
     */
    Integer getBounced();

    void setBounced(Integer bounced);

    /**
     * 获取 [被退回]脏标记
     */
    boolean getBouncedDirtyFlag();

    /**
     * 颜色索引
     */
    Integer getColor();

    void setColor(Integer color);

    /**
     * 获取 [颜色索引]脏标记
     */
    boolean getColorDirtyFlag();

    /**
     * 收件人模型
     */
    String getMailing_model_name();

    void setMailing_model_name(String mailing_model_name);

    /**
     * 获取 [收件人模型]脏标记
     */
    boolean getMailing_model_nameDirtyFlag();

    /**
     * 邮件列表
     */
    String getContact_list_ids();

    void setContact_list_ids(String contact_list_ids);

    /**
     * 获取 [邮件列表]脏标记
     */
    boolean getContact_list_idsDirtyFlag();

    /**
     * 预期
     */
    Integer getExpected();

    void setExpected(Integer expected);

    /**
     * 获取 [预期]脏标记
     */
    boolean getExpectedDirtyFlag();

    /**
     * 失败的
     */
    Integer getFailed();

    void setFailed(Integer failed);

    /**
     * 获取 [失败的]脏标记
     */
    boolean getFailedDirtyFlag();

    /**
     * ID
     */
    Integer getId();

    void setId(Integer id);

    /**
     * 获取 [ID]脏标记
     */
    boolean getIdDirtyFlag();

    /**
     * 已接收比例
     */
    Integer getReceived_ratio();

    void setReceived_ratio(Integer received_ratio);

    /**
     * 获取 [已接收比例]脏标记
     */
    boolean getReceived_ratioDirtyFlag();

    /**
     * 商机个数
     */
    Integer getCrm_opportunities_count();

    void setCrm_opportunities_count(Integer crm_opportunities_count);

    /**
     * 获取 [商机个数]脏标记
     */
    boolean getCrm_opportunities_countDirtyFlag();

    /**
     * 归档
     */
    String getKeep_archives();

    void setKeep_archives(String keep_archives);

    /**
     * 获取 [归档]脏标记
     */
    boolean getKeep_archivesDirtyFlag();

    /**
     * A/B 测试百分比
     */
    Integer getContact_ab_pc();

    void setContact_ab_pc(Integer contact_ab_pc);

    /**
     * 获取 [A/B 测试百分比]脏标记
     */
    boolean getContact_ab_pcDirtyFlag();

    /**
     * 已汇
     */
    Integer getSent();

    void setSent(Integer sent);

    /**
     * 获取 [已汇]脏标记
     */
    boolean getSentDirtyFlag();

    /**
     * 营销
     */
    String getCampaign_id_text();

    void setCampaign_id_text(String campaign_id_text);

    /**
     * 获取 [营销]脏标记
     */
    boolean getCampaign_id_textDirtyFlag();

    /**
     * 邮件管理器
     */
    String getUser_id_text();

    void setUser_id_text(String user_id_text);

    /**
     * 获取 [邮件管理器]脏标记
     */
    boolean getUser_id_textDirtyFlag();

    /**
     * 来源名称
     */
    String getName();

    void setName(String name);

    /**
     * 获取 [来源名称]脏标记
     */
    boolean getNameDirtyFlag();

    /**
     * 最后更新者
     */
    String getWrite_uid_text();

    void setWrite_uid_text(String write_uid_text);

    /**
     * 获取 [最后更新者]脏标记
     */
    boolean getWrite_uid_textDirtyFlag();

    /**
     * 群发邮件营销
     */
    String getMass_mailing_campaign_id_text();

    void setMass_mailing_campaign_id_text(String mass_mailing_campaign_id_text);

    /**
     * 获取 [群发邮件营销]脏标记
     */
    boolean getMass_mailing_campaign_id_textDirtyFlag();

    /**
     * 媒体
     */
    String getMedium_id_text();

    void setMedium_id_text(String medium_id_text);

    /**
     * 获取 [媒体]脏标记
     */
    boolean getMedium_id_textDirtyFlag();

    /**
     * 创建人
     */
    String getCreate_uid_text();

    void setCreate_uid_text(String create_uid_text);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uid_textDirtyFlag();

    /**
     * 创建人
     */
    Integer getCreate_uid();

    void setCreate_uid(Integer create_uid);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uidDirtyFlag();

    /**
     * 邮件管理器
     */
    Integer getUser_id();

    void setUser_id(Integer user_id);

    /**
     * 获取 [邮件管理器]脏标记
     */
    boolean getUser_idDirtyFlag();

    /**
     * 群发邮件营销
     */
    Integer getMass_mailing_campaign_id();

    void setMass_mailing_campaign_id(Integer mass_mailing_campaign_id);

    /**
     * 获取 [群发邮件营销]脏标记
     */
    boolean getMass_mailing_campaign_idDirtyFlag();

    /**
     * 营销
     */
    Integer getCampaign_id();

    void setCampaign_id(Integer campaign_id);

    /**
     * 获取 [营销]脏标记
     */
    boolean getCampaign_idDirtyFlag();

    /**
     * 最后更新者
     */
    Integer getWrite_uid();

    void setWrite_uid(Integer write_uid);

    /**
     * 获取 [最后更新者]脏标记
     */
    boolean getWrite_uidDirtyFlag();

    /**
     * 媒体
     */
    Integer getMedium_id();

    void setMedium_id(Integer medium_id);

    /**
     * 获取 [媒体]脏标记
     */
    boolean getMedium_idDirtyFlag();

    /**
     * 主题
     */
    Integer getSource_id();

    void setSource_id(Integer source_id);

    /**
     * 获取 [主题]脏标记
     */
    boolean getSource_idDirtyFlag();

}
