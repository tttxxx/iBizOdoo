package cn.ibizlab.odoo.core.repository.client.po;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigDecimal;

/**
 * [survey_user_input] 对象
 */
public interface survey_user_input {

    public Timestamp getCreate_date();

    public void setCreate_date(Timestamp create_date);

    public Integer getCreate_uid();

    public void setCreate_uid(Integer create_uid);

    public String getCreate_uid_text();

    public void setCreate_uid_text(String create_uid_text);

    public Timestamp getDate_create();

    public void setDate_create(Timestamp date_create);

    public Timestamp getDeadline();

    public void setDeadline(Timestamp deadline);

    public String getDisplay_name();

    public void setDisplay_name(String display_name);

    public String getEmail();

    public void setEmail(String email);

    public Integer getId();

    public void setId(Integer id);

    public Integer getLast_displayed_page_id();

    public void setLast_displayed_page_id(Integer last_displayed_page_id);

    public Integer getPartner_id();

    public void setPartner_id(Integer partner_id);

    public String getPartner_id_text();

    public void setPartner_id_text(String partner_id_text);

    public String getPrint_url();

    public void setPrint_url(String print_url);

    public Double getQuizz_score();

    public void setQuizz_score(Double quizz_score);

    public String getResult_url();

    public void setResult_url(String result_url);

    public String getState();

    public void setState(String state);

    public Integer getSurvey_id();

    public void setSurvey_id(Integer survey_id);

    public String getTest_entry();

    public void setTest_entry(String test_entry);

    public String getToken();

    public void setToken(String token);

    public String getType();

    public void setType(String type);

    public String getUser_input_line_ids();

    public void setUser_input_line_ids(String user_input_line_ids);

    public Timestamp getWrite_date();

    public void setWrite_date(Timestamp write_date);

    public Integer getWrite_uid();

    public void setWrite_uid(Integer write_uid);

    public String getWrite_uid_text();

    public void setWrite_uid_text(String write_uid_text);

    public Timestamp get__last_update();

    public void set__last_update(Timestamp __last_update);

}
