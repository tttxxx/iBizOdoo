package cn.ibizlab.odoo.core.odoo_project.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_project.domain.Project_tags;
import cn.ibizlab.odoo.core.odoo_project.filter.Project_tagsSearchContext;
import cn.ibizlab.odoo.core.odoo_project.service.IProject_tagsService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;



import cn.ibizlab.odoo.core.odoo_project.client.project_tagsOdooClient;
import cn.ibizlab.odoo.core.odoo_project.clientmodel.project_tagsClientModel;

/**
 * 实体[项目标签] 服务对象接口实现
 */
@Slf4j
@Service
public class Project_tagsServiceImpl implements IProject_tagsService {

    @Autowired
    project_tagsOdooClient project_tagsOdooClient;


    @Override
    public Project_tags get(Integer id) {
        project_tagsClientModel clientModel = new project_tagsClientModel();
        clientModel.setId(id);
		project_tagsOdooClient.get(clientModel);
        Project_tags et = convert2Domain(clientModel,null);
        if(et==null){
            et=new Project_tags();
            et.setId(id);
        }
        else{
        }

        return  et;
    }

    @Override
    public boolean remove(Integer id) {
        project_tagsClientModel clientModel = new project_tagsClientModel();
        clientModel.setId(id);
		project_tagsOdooClient.remove(clientModel);
        return true; 
    }

    public void removeBatch(Collection<Integer> idList){
    }

    @Override
    public boolean create(Project_tags et) {
        project_tagsClientModel clientModel = convert2Model(et,null);
		project_tagsOdooClient.create(clientModel);
        Project_tags rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Project_tags> list){
    }

    @Override
    public boolean update(Project_tags et) {
        project_tagsClientModel clientModel = convert2Model(et,null);
		project_tagsOdooClient.update(clientModel);
        Project_tags rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }
    
    public void updateBatch(List<Project_tags> list){
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Project_tags> searchDefault(Project_tagsSearchContext context) {
        List<Project_tags> list = new ArrayList<Project_tags>();
        Page<project_tagsClientModel> clientModelList = project_tagsOdooClient.search(context);
        for(project_tagsClientModel clientModel : clientModelList){
            list.add( this.convert2Domain(clientModel,null));
        }
        return new PageImpl<Project_tags>(list, context.getPageable(), clientModelList.getTotalElements());
    }

    public project_tagsClientModel convert2Model(Project_tags domain , project_tagsClientModel model) {
        if(domain == null)
            return model ;
        if(model==null){
            model = new project_tagsClientModel();

        }
        if((Boolean) domain.getExtensionparams().get("dirtyflagenable")){
            if((Boolean) domain.getExtensionparams().get("namedirtyflag"))
                model.setName(domain.getName());
            if((Boolean) domain.getExtensionparams().get("write_datedirtyflag"))
                model.setWrite_date(domain.getWriteDate());
            if((Boolean) domain.getExtensionparams().get("display_namedirtyflag"))
                model.setDisplay_name(domain.getDisplayName());
            if((Boolean) domain.getExtensionparams().get("colordirtyflag"))
                model.setColor(domain.getColor());
            if((Boolean) domain.getExtensionparams().get("iddirtyflag"))
                model.setId(domain.getId());
            if((Boolean) domain.getExtensionparams().get("__last_updatedirtyflag"))
                model.set__last_update(domain.getLastUpdate());
            if((Boolean) domain.getExtensionparams().get("create_datedirtyflag"))
                model.setCreate_date(domain.getCreateDate());
            if((Boolean) domain.getExtensionparams().get("write_uid_textdirtyflag"))
                model.setWrite_uid_text(domain.getWriteUidText());
            if((Boolean) domain.getExtensionparams().get("create_uid_textdirtyflag"))
                model.setCreate_uid_text(domain.getCreateUidText());
            if((Boolean) domain.getExtensionparams().get("create_uiddirtyflag"))
                model.setCreate_uid(domain.getCreateUid());
            if((Boolean) domain.getExtensionparams().get("write_uiddirtyflag"))
                model.setWrite_uid(domain.getWriteUid());
            domain.getFocusNull().clear();
        }
        return model ;
    }

    public Project_tags convert2Domain( project_tagsClientModel model ,Project_tags domain) {
        if(model == null)
            return domain ;
        if(domain == null){
            domain = new Project_tags();
        }

        if(model.getNameDirtyFlag())
            domain.setName(model.getName());
        if(model.getWrite_dateDirtyFlag())
            domain.setWriteDate(model.getWrite_date());
        if(model.getDisplay_nameDirtyFlag())
            domain.setDisplayName(model.getDisplay_name());
        if(model.getColorDirtyFlag())
            domain.setColor(model.getColor());
        if(model.getIdDirtyFlag())
            domain.setId(model.getId());
        if(model.get__last_updateDirtyFlag())
            domain.setLastUpdate(model.get__last_update());
        if(model.getCreate_dateDirtyFlag())
            domain.setCreateDate(model.getCreate_date());
        if(model.getWrite_uid_textDirtyFlag())
            domain.setWriteUidText(model.getWrite_uid_text());
        if(model.getCreate_uid_textDirtyFlag())
            domain.setCreateUidText(model.getCreate_uid_text());
        if(model.getCreate_uidDirtyFlag())
            domain.setCreateUid(model.getCreate_uid());
        if(model.getWrite_uidDirtyFlag())
            domain.setWriteUid(model.getWrite_uid());
        return domain ;
    }

}

    



