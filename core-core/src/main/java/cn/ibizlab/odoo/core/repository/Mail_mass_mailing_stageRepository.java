package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Mail_mass_mailing_stage;
import cn.ibizlab.odoo.core.odoo_mail.filter.Mail_mass_mailing_stageSearchContext;

/**
 * 实体 [群发邮件营销阶段] 存储对象
 */
public interface Mail_mass_mailing_stageRepository extends Repository<Mail_mass_mailing_stage> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Mail_mass_mailing_stage> searchDefault(Mail_mass_mailing_stageSearchContext context);

    Mail_mass_mailing_stage convert2PO(cn.ibizlab.odoo.core.odoo_mail.domain.Mail_mass_mailing_stage domain , Mail_mass_mailing_stage po) ;

    cn.ibizlab.odoo.core.odoo_mail.domain.Mail_mass_mailing_stage convert2Domain( Mail_mass_mailing_stage po ,cn.ibizlab.odoo.core.odoo_mail.domain.Mail_mass_mailing_stage domain) ;

}
