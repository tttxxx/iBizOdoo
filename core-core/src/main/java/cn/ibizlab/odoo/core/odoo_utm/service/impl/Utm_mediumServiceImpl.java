package cn.ibizlab.odoo.core.odoo_utm.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_utm.domain.Utm_medium;
import cn.ibizlab.odoo.core.odoo_utm.filter.Utm_mediumSearchContext;
import cn.ibizlab.odoo.core.odoo_utm.service.IUtm_mediumService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;



import cn.ibizlab.odoo.core.odoo_utm.client.utm_mediumOdooClient;
import cn.ibizlab.odoo.core.odoo_utm.clientmodel.utm_mediumClientModel;

/**
 * 实体[UTM媒体] 服务对象接口实现
 */
@Slf4j
@Service
public class Utm_mediumServiceImpl implements IUtm_mediumService {

    @Autowired
    utm_mediumOdooClient utm_mediumOdooClient;


    @Override
    public boolean remove(Integer id) {
        utm_mediumClientModel clientModel = new utm_mediumClientModel();
        clientModel.setId(id);
		utm_mediumOdooClient.remove(clientModel);
        return true; 
    }

    public void removeBatch(Collection<Integer> idList){
    }

    @Override
    public boolean create(Utm_medium et) {
        utm_mediumClientModel clientModel = convert2Model(et,null);
		utm_mediumOdooClient.create(clientModel);
        Utm_medium rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Utm_medium> list){
    }

    @Override
    public boolean update(Utm_medium et) {
        utm_mediumClientModel clientModel = convert2Model(et,null);
		utm_mediumOdooClient.update(clientModel);
        Utm_medium rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }
    
    public void updateBatch(List<Utm_medium> list){
    }

    @Override
    public Utm_medium get(Integer id) {
        utm_mediumClientModel clientModel = new utm_mediumClientModel();
        clientModel.setId(id);
		utm_mediumOdooClient.get(clientModel);
        Utm_medium et = convert2Domain(clientModel,null);
        if(et==null){
            et=new Utm_medium();
            et.setId(id);
        }
        else{
        }

        return  et;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Utm_medium> searchDefault(Utm_mediumSearchContext context) {
        List<Utm_medium> list = new ArrayList<Utm_medium>();
        Page<utm_mediumClientModel> clientModelList = utm_mediumOdooClient.search(context);
        for(utm_mediumClientModel clientModel : clientModelList){
            list.add( this.convert2Domain(clientModel,null));
        }
        return new PageImpl<Utm_medium>(list, context.getPageable(), clientModelList.getTotalElements());
    }

    public utm_mediumClientModel convert2Model(Utm_medium domain , utm_mediumClientModel model) {
        if(domain == null)
            return model ;
        if(model==null){
            model = new utm_mediumClientModel();

        }
        if((Boolean) domain.getExtensionparams().get("dirtyflagenable")){
            if((Boolean) domain.getExtensionparams().get("activedirtyflag"))
                model.setActive(domain.getActive());
            if((Boolean) domain.getExtensionparams().get("iddirtyflag"))
                model.setId(domain.getId());
            if((Boolean) domain.getExtensionparams().get("namedirtyflag"))
                model.setName(domain.getName());
            if((Boolean) domain.getExtensionparams().get("display_namedirtyflag"))
                model.setDisplay_name(domain.getDisplayName());
            if((Boolean) domain.getExtensionparams().get("create_datedirtyflag"))
                model.setCreate_date(domain.getCreateDate());
            if((Boolean) domain.getExtensionparams().get("__last_updatedirtyflag"))
                model.set__last_update(domain.getLastUpdate());
            if((Boolean) domain.getExtensionparams().get("write_datedirtyflag"))
                model.setWrite_date(domain.getWriteDate());
            if((Boolean) domain.getExtensionparams().get("write_uid_textdirtyflag"))
                model.setWrite_uid_text(domain.getWriteUidText());
            if((Boolean) domain.getExtensionparams().get("create_uid_textdirtyflag"))
                model.setCreate_uid_text(domain.getCreateUidText());
            if((Boolean) domain.getExtensionparams().get("write_uiddirtyflag"))
                model.setWrite_uid(domain.getWriteUid());
            if((Boolean) domain.getExtensionparams().get("create_uiddirtyflag"))
                model.setCreate_uid(domain.getCreateUid());
            domain.getFocusNull().clear();
        }
        return model ;
    }

    public Utm_medium convert2Domain( utm_mediumClientModel model ,Utm_medium domain) {
        if(model == null)
            return domain ;
        if(domain == null){
            domain = new Utm_medium();
        }

        if(model.getActiveDirtyFlag())
            domain.setActive(model.getActive());
        if(model.getIdDirtyFlag())
            domain.setId(model.getId());
        if(model.getNameDirtyFlag())
            domain.setName(model.getName());
        if(model.getDisplay_nameDirtyFlag())
            domain.setDisplayName(model.getDisplay_name());
        if(model.getCreate_dateDirtyFlag())
            domain.setCreateDate(model.getCreate_date());
        if(model.get__last_updateDirtyFlag())
            domain.setLastUpdate(model.get__last_update());
        if(model.getWrite_dateDirtyFlag())
            domain.setWriteDate(model.getWrite_date());
        if(model.getWrite_uid_textDirtyFlag())
            domain.setWriteUidText(model.getWrite_uid_text());
        if(model.getCreate_uid_textDirtyFlag())
            domain.setCreateUidText(model.getCreate_uid_text());
        if(model.getWrite_uidDirtyFlag())
            domain.setWriteUid(model.getWrite_uid());
        if(model.getCreate_uidDirtyFlag())
            domain.setCreateUid(model.getCreate_uid());
        return domain ;
    }

}

    



