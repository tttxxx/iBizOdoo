package cn.ibizlab.odoo.core.odoo_resource.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.odoo.util.domain.EntityBase;
import cn.ibizlab.odoo.util.annotation.DEField;
import cn.ibizlab.odoo.util.enums.DEPredefinedFieldType;
import cn.ibizlab.odoo.util.enums.DEFieldDefaultValueType;
import java.io.Serializable;
import lombok.Data;
import org.springframework.data.annotation.Transient;

import cn.ibizlab.odoo.util.domain.EntityClient;

/**
 * ServiceApi [资源装饰] 对象
 */
@Data
public class Resource_mixin extends EntityClient implements Serializable {

    /**
     * 最后修改日
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;

    /**
     * 显示名称
     */
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    private String displayName;

    /**
     * ID
     */
    @DEField(isKeyField=true)
    @JSONField(name = "id")
    @JsonProperty("id")
    private Integer id;

    /**
     * 时区
     */
    @JSONField(name = "tz")
    @JsonProperty("tz")
    private String tz;

    /**
     * 资源
     */
    @JSONField(name = "resource_id_text")
    @JsonProperty("resource_id_text")
    private String resourceIdText;

    /**
     * 工作时间
     */
    @JSONField(name = "resource_calendar_id_text")
    @JsonProperty("resource_calendar_id_text")
    private String resourceCalendarIdText;

    /**
     * 公司
     */
    @JSONField(name = "company_id_text")
    @JsonProperty("company_id_text")
    private String companyIdText;

    /**
     * 工作时间
     */
    @DEField(name = "resource_calendar_id")
    @JSONField(name = "resource_calendar_id")
    @JsonProperty("resource_calendar_id")
    private Integer resourceCalendarId;

    /**
     * 公司
     */
    @DEField(name = "company_id")
    @JSONField(name = "company_id")
    @JsonProperty("company_id")
    private Integer companyId;

    /**
     * 资源
     */
    @DEField(name = "resource_id")
    @JSONField(name = "resource_id")
    @JsonProperty("resource_id")
    private Integer resourceId;


    /**
     * 
     */
    @JSONField(name = "odooresourcecalendar")
    @JsonProperty("odooresourcecalendar")
    private cn.ibizlab.odoo.core.odoo_resource.domain.Resource_calendar odooResourceCalendar;

    /**
     * 
     */
    @JSONField(name = "odooresource")
    @JsonProperty("odooresource")
    private cn.ibizlab.odoo.core.odoo_resource.domain.Resource_resource odooResource;

    /**
     * 
     */
    @JSONField(name = "odoocompany")
    @JsonProperty("odoocompany")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_company odooCompany;




    /**
     * 设置 [工作时间]
     */
    public void setResourceCalendarId(Integer resourceCalendarId){
        this.resourceCalendarId = resourceCalendarId ;
        this.modify("resource_calendar_id",resourceCalendarId);
    }
    /**
     * 设置 [公司]
     */
    public void setCompanyId(Integer companyId){
        this.companyId = companyId ;
        this.modify("company_id",companyId);
    }
    /**
     * 设置 [资源]
     */
    public void setResourceId(Integer resourceId){
        this.resourceId = resourceId ;
        this.modify("resource_id",resourceId);
    }

}


