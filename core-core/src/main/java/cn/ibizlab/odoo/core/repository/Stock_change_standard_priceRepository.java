package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Stock_change_standard_price;
import cn.ibizlab.odoo.core.odoo_stock.filter.Stock_change_standard_priceSearchContext;

/**
 * 实体 [更改标准价] 存储对象
 */
public interface Stock_change_standard_priceRepository extends Repository<Stock_change_standard_price> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Stock_change_standard_price> searchDefault(Stock_change_standard_priceSearchContext context);

    Stock_change_standard_price convert2PO(cn.ibizlab.odoo.core.odoo_stock.domain.Stock_change_standard_price domain , Stock_change_standard_price po) ;

    cn.ibizlab.odoo.core.odoo_stock.domain.Stock_change_standard_price convert2Domain( Stock_change_standard_price po ,cn.ibizlab.odoo.core.odoo_stock.domain.Stock_change_standard_price domain) ;

}
