package cn.ibizlab.odoo.core.odoo_mail.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_mail.domain.Mail_shortcode;
import cn.ibizlab.odoo.core.odoo_mail.filter.Mail_shortcodeSearchContext;
import cn.ibizlab.odoo.core.odoo_mail.service.IMail_shortcodeService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;



import cn.ibizlab.odoo.core.odoo_mail.client.mail_shortcodeOdooClient;
import cn.ibizlab.odoo.core.odoo_mail.clientmodel.mail_shortcodeClientModel;

/**
 * 实体[自动回复] 服务对象接口实现
 */
@Slf4j
@Service
public class Mail_shortcodeServiceImpl implements IMail_shortcodeService {

    @Autowired
    mail_shortcodeOdooClient mail_shortcodeOdooClient;


    @Override
    public Mail_shortcode get(Integer id) {
        mail_shortcodeClientModel clientModel = new mail_shortcodeClientModel();
        clientModel.setId(id);
		mail_shortcodeOdooClient.get(clientModel);
        Mail_shortcode et = convert2Domain(clientModel,null);
        if(et==null){
            et=new Mail_shortcode();
            et.setId(id);
        }
        else{
        }

        return  et;
    }

    @Override
    public boolean create(Mail_shortcode et) {
        mail_shortcodeClientModel clientModel = convert2Model(et,null);
		mail_shortcodeOdooClient.create(clientModel);
        Mail_shortcode rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Mail_shortcode> list){
    }

    @Override
    public boolean remove(Integer id) {
        mail_shortcodeClientModel clientModel = new mail_shortcodeClientModel();
        clientModel.setId(id);
		mail_shortcodeOdooClient.remove(clientModel);
        return true; 
    }

    public void removeBatch(Collection<Integer> idList){
    }

    @Override
    public boolean update(Mail_shortcode et) {
        mail_shortcodeClientModel clientModel = convert2Model(et,null);
		mail_shortcodeOdooClient.update(clientModel);
        Mail_shortcode rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }
    
    public void updateBatch(List<Mail_shortcode> list){
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Mail_shortcode> searchDefault(Mail_shortcodeSearchContext context) {
        List<Mail_shortcode> list = new ArrayList<Mail_shortcode>();
        Page<mail_shortcodeClientModel> clientModelList = mail_shortcodeOdooClient.search(context);
        for(mail_shortcodeClientModel clientModel : clientModelList){
            list.add( this.convert2Domain(clientModel,null));
        }
        return new PageImpl<Mail_shortcode>(list, context.getPageable(), clientModelList.getTotalElements());
    }

    public mail_shortcodeClientModel convert2Model(Mail_shortcode domain , mail_shortcodeClientModel model) {
        if(domain == null)
            return model ;
        if(model==null){
            model = new mail_shortcodeClientModel();

        }
        if((Boolean) domain.getExtensionparams().get("dirtyflagenable")){
            if((Boolean) domain.getExtensionparams().get("iddirtyflag"))
                model.setId(domain.getId());
            if((Boolean) domain.getExtensionparams().get("display_namedirtyflag"))
                model.setDisplay_name(domain.getDisplayName());
            if((Boolean) domain.getExtensionparams().get("write_datedirtyflag"))
                model.setWrite_date(domain.getWriteDate());
            if((Boolean) domain.getExtensionparams().get("create_datedirtyflag"))
                model.setCreate_date(domain.getCreateDate());
            if((Boolean) domain.getExtensionparams().get("substitutiondirtyflag"))
                model.setSubstitution(domain.getSubstitution());
            if((Boolean) domain.getExtensionparams().get("sourcedirtyflag"))
                model.setSource(domain.getSource());
            if((Boolean) domain.getExtensionparams().get("descriptiondirtyflag"))
                model.setDescription(domain.getDescription());
            if((Boolean) domain.getExtensionparams().get("__last_updatedirtyflag"))
                model.set__last_update(domain.getLastUpdate());
            if((Boolean) domain.getExtensionparams().get("write_uid_textdirtyflag"))
                model.setWrite_uid_text(domain.getWriteUidText());
            if((Boolean) domain.getExtensionparams().get("create_uid_textdirtyflag"))
                model.setCreate_uid_text(domain.getCreateUidText());
            if((Boolean) domain.getExtensionparams().get("write_uiddirtyflag"))
                model.setWrite_uid(domain.getWriteUid());
            if((Boolean) domain.getExtensionparams().get("create_uiddirtyflag"))
                model.setCreate_uid(domain.getCreateUid());
            domain.getFocusNull().clear();
        }
        return model ;
    }

    public Mail_shortcode convert2Domain( mail_shortcodeClientModel model ,Mail_shortcode domain) {
        if(model == null)
            return domain ;
        if(domain == null){
            domain = new Mail_shortcode();
        }

        if(model.getIdDirtyFlag())
            domain.setId(model.getId());
        if(model.getDisplay_nameDirtyFlag())
            domain.setDisplayName(model.getDisplay_name());
        if(model.getWrite_dateDirtyFlag())
            domain.setWriteDate(model.getWrite_date());
        if(model.getCreate_dateDirtyFlag())
            domain.setCreateDate(model.getCreate_date());
        if(model.getSubstitutionDirtyFlag())
            domain.setSubstitution(model.getSubstitution());
        if(model.getSourceDirtyFlag())
            domain.setSource(model.getSource());
        if(model.getDescriptionDirtyFlag())
            domain.setDescription(model.getDescription());
        if(model.get__last_updateDirtyFlag())
            domain.setLastUpdate(model.get__last_update());
        if(model.getWrite_uid_textDirtyFlag())
            domain.setWriteUidText(model.getWrite_uid_text());
        if(model.getCreate_uid_textDirtyFlag())
            domain.setCreateUidText(model.getCreate_uid_text());
        if(model.getWrite_uidDirtyFlag())
            domain.setWriteUid(model.getWrite_uid());
        if(model.getCreate_uidDirtyFlag())
            domain.setCreateUid(model.getCreate_uid());
        return domain ;
    }

}

    



