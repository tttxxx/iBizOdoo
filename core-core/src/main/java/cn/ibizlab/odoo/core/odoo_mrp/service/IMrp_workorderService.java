package cn.ibizlab.odoo.core.odoo_mrp.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_mrp.domain.Mrp_workorder;
import cn.ibizlab.odoo.core.odoo_mrp.filter.Mrp_workorderSearchContext;


/**
 * 实体[Mrp_workorder] 服务对象接口
 */
public interface IMrp_workorderService{

    boolean update(Mrp_workorder et) ;
    void updateBatch(List<Mrp_workorder> list) ;
    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    Mrp_workorder get(Integer key) ;
    boolean create(Mrp_workorder et) ;
    void createBatch(List<Mrp_workorder> list) ;
    Page<Mrp_workorder> searchDefault(Mrp_workorderSearchContext context) ;

}



