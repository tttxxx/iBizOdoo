package cn.ibizlab.odoo.core.repository.po ;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.math.BigInteger;

import cn.ibizlab.odoo.core.odoo_account.filter.Account_invoice_taxSearchContext;

/**
 * 实体 [发票税率] 存储模型
 */
public interface Account_invoice_tax{

    /**
     * 手动
     */
    String getManual();

    void setManual(String manual);

    /**
     * 获取 [手动]脏标记
     */
    boolean getManualDirtyFlag();

    /**
     * 总金额
     */
    Double getAmount_total();

    void setAmount_total(Double amount_total);

    /**
     * 获取 [总金额]脏标记
     */
    boolean getAmount_totalDirtyFlag();

    /**
     * 创建时间
     */
    Timestamp getCreate_date();

    void setCreate_date(Timestamp create_date);

    /**
     * 获取 [创建时间]脏标记
     */
    boolean getCreate_dateDirtyFlag();

    /**
     * 基础
     */
    Double getBase();

    void setBase(Double base);

    /**
     * 获取 [基础]脏标记
     */
    boolean getBaseDirtyFlag();

    /**
     * 最后修改日
     */
    Timestamp get__last_update();

    void set__last_update(Timestamp __last_update);

    /**
     * 获取 [最后修改日]脏标记
     */
    boolean get__last_updateDirtyFlag();

    /**
     * ID
     */
    Integer getId();

    void setId(Integer id);

    /**
     * 获取 [ID]脏标记
     */
    boolean getIdDirtyFlag();

    /**
     * 显示名称
     */
    String getDisplay_name();

    void setDisplay_name(String display_name);

    /**
     * 获取 [显示名称]脏标记
     */
    boolean getDisplay_nameDirtyFlag();

    /**
     * 税率金额
     */
    Double getAmount();

    void setAmount(Double amount);

    /**
     * 获取 [税率金额]脏标记
     */
    boolean getAmountDirtyFlag();

    /**
     * 税说明
     */
    String getName();

    void setName(String name);

    /**
     * 获取 [税说明]脏标记
     */
    boolean getNameDirtyFlag();

    /**
     * 序号
     */
    Integer getSequence();

    void setSequence(Integer sequence);

    /**
     * 获取 [序号]脏标记
     */
    boolean getSequenceDirtyFlag();

    /**
     * 最后更新时间
     */
    Timestamp getWrite_date();

    void setWrite_date(Timestamp write_date);

    /**
     * 获取 [最后更新时间]脏标记
     */
    boolean getWrite_dateDirtyFlag();

    /**
     * 分析标签
     */
    String getAnalytic_tag_ids();

    void setAnalytic_tag_ids(String analytic_tag_ids);

    /**
     * 获取 [分析标签]脏标记
     */
    boolean getAnalytic_tag_idsDirtyFlag();

    /**
     * 金额差异
     */
    Double getAmount_rounding();

    void setAmount_rounding(Double amount_rounding);

    /**
     * 获取 [金额差异]脏标记
     */
    boolean getAmount_roundingDirtyFlag();

    /**
     * 税率
     */
    String getTax_id_text();

    void setTax_id_text(String tax_id_text);

    /**
     * 获取 [税率]脏标记
     */
    boolean getTax_id_textDirtyFlag();

    /**
     * 创建人
     */
    String getCreate_uid_text();

    void setCreate_uid_text(String create_uid_text);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uid_textDirtyFlag();

    /**
     * 发票
     */
    String getInvoice_id_text();

    void setInvoice_id_text(String invoice_id_text);

    /**
     * 获取 [发票]脏标记
     */
    boolean getInvoice_id_textDirtyFlag();

    /**
     * 币种
     */
    String getCurrency_id_text();

    void setCurrency_id_text(String currency_id_text);

    /**
     * 获取 [币种]脏标记
     */
    boolean getCurrency_id_textDirtyFlag();

    /**
     * 最后更新人
     */
    String getWrite_uid_text();

    void setWrite_uid_text(String write_uid_text);

    /**
     * 获取 [最后更新人]脏标记
     */
    boolean getWrite_uid_textDirtyFlag();

    /**
     * 公司
     */
    String getCompany_id_text();

    void setCompany_id_text(String company_id_text);

    /**
     * 获取 [公司]脏标记
     */
    boolean getCompany_id_textDirtyFlag();

    /**
     * 分析账户
     */
    String getAccount_analytic_id_text();

    void setAccount_analytic_id_text(String account_analytic_id_text);

    /**
     * 获取 [分析账户]脏标记
     */
    boolean getAccount_analytic_id_textDirtyFlag();

    /**
     * 税率科目
     */
    String getAccount_id_text();

    void setAccount_id_text(String account_id_text);

    /**
     * 获取 [税率科目]脏标记
     */
    boolean getAccount_id_textDirtyFlag();

    /**
     * 创建人
     */
    Integer getCreate_uid();

    void setCreate_uid(Integer create_uid);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uidDirtyFlag();

    /**
     * 税率科目
     */
    Integer getAccount_id();

    void setAccount_id(Integer account_id);

    /**
     * 获取 [税率科目]脏标记
     */
    boolean getAccount_idDirtyFlag();

    /**
     * 发票
     */
    Integer getInvoice_id();

    void setInvoice_id(Integer invoice_id);

    /**
     * 获取 [发票]脏标记
     */
    boolean getInvoice_idDirtyFlag();

    /**
     * 币种
     */
    Integer getCurrency_id();

    void setCurrency_id(Integer currency_id);

    /**
     * 获取 [币种]脏标记
     */
    boolean getCurrency_idDirtyFlag();

    /**
     * 分析账户
     */
    Integer getAccount_analytic_id();

    void setAccount_analytic_id(Integer account_analytic_id);

    /**
     * 获取 [分析账户]脏标记
     */
    boolean getAccount_analytic_idDirtyFlag();

    /**
     * 最后更新人
     */
    Integer getWrite_uid();

    void setWrite_uid(Integer write_uid);

    /**
     * 获取 [最后更新人]脏标记
     */
    boolean getWrite_uidDirtyFlag();

    /**
     * 公司
     */
    Integer getCompany_id();

    void setCompany_id(Integer company_id);

    /**
     * 获取 [公司]脏标记
     */
    boolean getCompany_idDirtyFlag();

    /**
     * 税率
     */
    Integer getTax_id();

    void setTax_id(Integer tax_id);

    /**
     * 获取 [税率]脏标记
     */
    boolean getTax_idDirtyFlag();

}
