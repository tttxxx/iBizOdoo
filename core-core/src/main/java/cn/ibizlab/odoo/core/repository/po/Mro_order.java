package cn.ibizlab.odoo.core.repository.po ;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.math.BigInteger;

import cn.ibizlab.odoo.core.odoo_mro.filter.Mro_orderSearchContext;

/**
 * 实体 [Maintenance Order] 存储模型
 */
public interface Mro_order{

    /**
     * 消息
     */
    String getMessage_ids();

    void setMessage_ids(String message_ids);

    /**
     * 获取 [消息]脏标记
     */
    boolean getMessage_idsDirtyFlag();

    /**
     * 最后修改日
     */
    Timestamp get__last_update();

    void set__last_update(Timestamp __last_update);

    /**
     * 获取 [最后修改日]脏标记
     */
    boolean get__last_updateDirtyFlag();

    /**
     * 关注者(业务伙伴)
     */
    String getMessage_partner_ids();

    void setMessage_partner_ids(String message_partner_ids);

    /**
     * 获取 [关注者(业务伙伴)]脏标记
     */
    boolean getMessage_partner_idsDirtyFlag();

    /**
     * 保养类型
     */
    String getMaintenance_type();

    void setMaintenance_type(String maintenance_type);

    /**
     * 获取 [保养类型]脏标记
     */
    boolean getMaintenance_typeDirtyFlag();

    /**
     * 未读消息计数器
     */
    Integer getMessage_unread_counter();

    void setMessage_unread_counter(Integer message_unread_counter);

    /**
     * 获取 [未读消息计数器]脏标记
     */
    boolean getMessage_unread_counterDirtyFlag();

    /**
     * Documentation Description
     */
    String getDocumentation_description();

    void setDocumentation_description(String documentation_description);

    /**
     * 获取 [Documentation Description]脏标记
     */
    boolean getDocumentation_descriptionDirtyFlag();

    /**
     * Parts Moved Lines
     */
    String getParts_moved_lines();

    void setParts_moved_lines(String parts_moved_lines);

    /**
     * 获取 [Parts Moved Lines]脏标记
     */
    boolean getParts_moved_linesDirtyFlag();

    /**
     * 关注者(渠道)
     */
    String getMessage_channel_ids();

    void setMessage_channel_ids(String message_channel_ids);

    /**
     * 获取 [关注者(渠道)]脏标记
     */
    boolean getMessage_channel_idsDirtyFlag();

    /**
     * Asset Category
     */
    String getCategory_ids();

    void setCategory_ids(String category_ids);

    /**
     * 获取 [Asset Category]脏标记
     */
    boolean getCategory_idsDirtyFlag();

    /**
     * 计划日期
     */
    Timestamp getDate_scheduled();

    void setDate_scheduled(Timestamp date_scheduled);

    /**
     * 获取 [计划日期]脏标记
     */
    boolean getDate_scheduledDirtyFlag();

    /**
     * 显示名称
     */
    String getDisplay_name();

    void setDisplay_name(String display_name);

    /**
     * 获取 [显示名称]脏标记
     */
    boolean getDisplay_nameDirtyFlag();

    /**
     * Labor Description
     */
    String getLabor_description();

    void setLabor_description(String labor_description);

    /**
     * 获取 [Labor Description]脏标记
     */
    boolean getLabor_descriptionDirtyFlag();

    /**
     * 未读消息
     */
    String getMessage_unread();

    void setMessage_unread(String message_unread);

    /**
     * 获取 [未读消息]脏标记
     */
    boolean getMessage_unreadDirtyFlag();

    /**
     * 关注者
     */
    String getMessage_follower_ids();

    void setMessage_follower_ids(String message_follower_ids);

    /**
     * 获取 [关注者]脏标记
     */
    boolean getMessage_follower_idsDirtyFlag();

    /**
     * 源文档
     */
    String getOrigin();

    void setOrigin(String origin);

    /**
     * 获取 [源文档]脏标记
     */
    boolean getOriginDirtyFlag();

    /**
     * 错误个数
     */
    Integer getMessage_has_error_counter();

    void setMessage_has_error_counter(Integer message_has_error_counter);

    /**
     * 获取 [错误个数]脏标记
     */
    boolean getMessage_has_error_counterDirtyFlag();

    /**
     * 最后更新时间
     */
    Timestamp getWrite_date();

    void setWrite_date(Timestamp write_date);

    /**
     * 获取 [最后更新时间]脏标记
     */
    boolean getWrite_dateDirtyFlag();

    /**
     * 说明
     */
    String getDescription();

    void setDescription(String description);

    /**
     * 获取 [说明]脏标记
     */
    boolean getDescriptionDirtyFlag();

    /**
     * Parts Ready Lines
     */
    String getParts_ready_lines();

    void setParts_ready_lines(String parts_ready_lines);

    /**
     * 获取 [Parts Ready Lines]脏标记
     */
    boolean getParts_ready_linesDirtyFlag();

    /**
     * 网站消息
     */
    String getWebsite_message_ids();

    void setWebsite_message_ids(String website_message_ids);

    /**
     * 获取 [网站消息]脏标记
     */
    boolean getWebsite_message_idsDirtyFlag();

    /**
     * Parts Move Lines
     */
    String getParts_move_lines();

    void setParts_move_lines(String parts_move_lines);

    /**
     * 获取 [Parts Move Lines]脏标记
     */
    boolean getParts_move_linesDirtyFlag();

    /**
     * 关注者
     */
    String getMessage_is_follower();

    void setMessage_is_follower(String message_is_follower);

    /**
     * 获取 [关注者]脏标记
     */
    boolean getMessage_is_followerDirtyFlag();

    /**
     * 操作次数
     */
    Integer getMessage_needaction_counter();

    void setMessage_needaction_counter(Integer message_needaction_counter);

    /**
     * 获取 [操作次数]脏标记
     */
    boolean getMessage_needaction_counterDirtyFlag();

    /**
     * Operations Description
     */
    String getOperations_description();

    void setOperations_description(String operations_description);

    /**
     * 获取 [Operations Description]脏标记
     */
    boolean getOperations_descriptionDirtyFlag();

    /**
     * Problem Description
     */
    String getProblem_description();

    void setProblem_description(String problem_description);

    /**
     * 获取 [Problem Description]脏标记
     */
    boolean getProblem_descriptionDirtyFlag();

    /**
     * 附件数量
     */
    Integer getMessage_attachment_count();

    void setMessage_attachment_count(Integer message_attachment_count);

    /**
     * 获取 [附件数量]脏标记
     */
    boolean getMessage_attachment_countDirtyFlag();

    /**
     * Planned Date
     */
    Timestamp getDate_planned();

    void setDate_planned(Timestamp date_planned);

    /**
     * 获取 [Planned Date]脏标记
     */
    boolean getDate_plannedDirtyFlag();

    /**
     * Procurement group
     */
    Integer getProcurement_group_id();

    void setProcurement_group_id(Integer procurement_group_id);

    /**
     * 获取 [Procurement group]脏标记
     */
    boolean getProcurement_group_idDirtyFlag();

    /**
     * Execution Date
     */
    Timestamp getDate_execution();

    void setDate_execution(Timestamp date_execution);

    /**
     * 获取 [Execution Date]脏标记
     */
    boolean getDate_executionDirtyFlag();

    /**
     * 消息递送错误
     */
    String getMessage_has_error();

    void setMessage_has_error(String message_has_error);

    /**
     * 获取 [消息递送错误]脏标记
     */
    boolean getMessage_has_errorDirtyFlag();

    /**
     * 附件
     */
    Integer getMessage_main_attachment_id();

    void setMessage_main_attachment_id(Integer message_main_attachment_id);

    /**
     * 获取 [附件]脏标记
     */
    boolean getMessage_main_attachment_idDirtyFlag();

    /**
     * ID
     */
    Integer getId();

    void setId(Integer id);

    /**
     * 获取 [ID]脏标记
     */
    boolean getIdDirtyFlag();

    /**
     * Planned parts
     */
    String getParts_lines();

    void setParts_lines(String parts_lines);

    /**
     * 获取 [Planned parts]脏标记
     */
    boolean getParts_linesDirtyFlag();

    /**
     * 编号
     */
    String getName();

    void setName(String name);

    /**
     * 获取 [编号]脏标记
     */
    boolean getNameDirtyFlag();

    /**
     * 状态
     */
    String getState();

    void setState(String state);

    /**
     * 获取 [状态]脏标记
     */
    boolean getStateDirtyFlag();

    /**
     * 创建时间
     */
    Timestamp getCreate_date();

    void setCreate_date(Timestamp create_date);

    /**
     * 获取 [创建时间]脏标记
     */
    boolean getCreate_dateDirtyFlag();

    /**
     * 前置操作
     */
    String getMessage_needaction();

    void setMessage_needaction(String message_needaction);

    /**
     * 获取 [前置操作]脏标记
     */
    boolean getMessage_needactionDirtyFlag();

    /**
     * Tools Description
     */
    String getTools_description();

    void setTools_description(String tools_description);

    /**
     * 获取 [Tools Description]脏标记
     */
    boolean getTools_descriptionDirtyFlag();

    /**
     * 创建人
     */
    String getCreate_uid_text();

    void setCreate_uid_text(String create_uid_text);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uid_textDirtyFlag();

    /**
     * 最后更新者
     */
    String getWrite_uid_text();

    void setWrite_uid_text(String write_uid_text);

    /**
     * 获取 [最后更新者]脏标记
     */
    boolean getWrite_uid_textDirtyFlag();

    /**
     * 工单
     */
    String getWo_id_text();

    void setWo_id_text(String wo_id_text);

    /**
     * 获取 [工单]脏标记
     */
    boolean getWo_id_textDirtyFlag();

    /**
     * Asset
     */
    String getAsset_id_text();

    void setAsset_id_text(String asset_id_text);

    /**
     * 获取 [Asset]脏标记
     */
    boolean getAsset_id_textDirtyFlag();

    /**
     * Task
     */
    String getTask_id_text();

    void setTask_id_text(String task_id_text);

    /**
     * 获取 [Task]脏标记
     */
    boolean getTask_id_textDirtyFlag();

    /**
     * 公司
     */
    String getCompany_id_text();

    void setCompany_id_text(String company_id_text);

    /**
     * 获取 [公司]脏标记
     */
    boolean getCompany_id_textDirtyFlag();

    /**
     * 请求
     */
    String getRequest_id_text();

    void setRequest_id_text(String request_id_text);

    /**
     * 获取 [请求]脏标记
     */
    boolean getRequest_id_textDirtyFlag();

    /**
     * 负责人
     */
    String getUser_id_text();

    void setUser_id_text(String user_id_text);

    /**
     * 获取 [负责人]脏标记
     */
    boolean getUser_id_textDirtyFlag();

    /**
     * 请求
     */
    Integer getRequest_id();

    void setRequest_id(Integer request_id);

    /**
     * 获取 [请求]脏标记
     */
    boolean getRequest_idDirtyFlag();

    /**
     * Task
     */
    Integer getTask_id();

    void setTask_id(Integer task_id);

    /**
     * 获取 [Task]脏标记
     */
    boolean getTask_idDirtyFlag();

    /**
     * 工单
     */
    Integer getWo_id();

    void setWo_id(Integer wo_id);

    /**
     * 获取 [工单]脏标记
     */
    boolean getWo_idDirtyFlag();

    /**
     * 公司
     */
    Integer getCompany_id();

    void setCompany_id(Integer company_id);

    /**
     * 获取 [公司]脏标记
     */
    boolean getCompany_idDirtyFlag();

    /**
     * 负责人
     */
    Integer getUser_id();

    void setUser_id(Integer user_id);

    /**
     * 获取 [负责人]脏标记
     */
    boolean getUser_idDirtyFlag();

    /**
     * Asset
     */
    Integer getAsset_id();

    void setAsset_id(Integer asset_id);

    /**
     * 获取 [Asset]脏标记
     */
    boolean getAsset_idDirtyFlag();

    /**
     * 创建人
     */
    Integer getCreate_uid();

    void setCreate_uid(Integer create_uid);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uidDirtyFlag();

    /**
     * 最后更新者
     */
    Integer getWrite_uid();

    void setWrite_uid(Integer write_uid);

    /**
     * 获取 [最后更新者]脏标记
     */
    boolean getWrite_uidDirtyFlag();

}
