package cn.ibizlab.odoo.core.repository.po ;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.math.BigInteger;

import cn.ibizlab.odoo.core.odoo_stock.filter.Stock_warehouseSearchContext;

/**
 * 实体 [仓库] 存储模型
 */
public interface Stock_warehouse{

    /**
     * 补充路线
     */
    String getResupply_route_ids();

    void setResupply_route_ids(String resupply_route_ids);

    /**
     * 获取 [补充路线]脏标记
     */
    boolean getResupply_route_idsDirtyFlag();

    /**
     * 有效
     */
    String getActive();

    void setActive(String active);

    /**
     * 获取 [有效]脏标记
     */
    boolean getActiveDirtyFlag();

    /**
     * 补给 自
     */
    String getResupply_wh_ids();

    void setResupply_wh_ids(String resupply_wh_ids);

    /**
     * 获取 [补给 自]脏标记
     */
    boolean getResupply_wh_idsDirtyFlag();

    /**
     * 最后更新时间
     */
    Timestamp getWrite_date();

    void setWrite_date(Timestamp write_date);

    /**
     * 获取 [最后更新时间]脏标记
     */
    boolean getWrite_dateDirtyFlag();

    /**
     * 最后修改日
     */
    Timestamp get__last_update();

    void set__last_update(Timestamp __last_update);

    /**
     * 获取 [最后修改日]脏标记
     */
    boolean get__last_updateDirtyFlag();

    /**
     * 仓库
     */
    String getName();

    void setName(String name);

    /**
     * 获取 [仓库]脏标记
     */
    boolean getNameDirtyFlag();

    /**
     * 出向运输
     */
    String getDelivery_steps();

    void setDelivery_steps(String delivery_steps);

    /**
     * 获取 [出向运输]脏标记
     */
    boolean getDelivery_stepsDirtyFlag();

    /**
     * 仓库个数
     */
    Integer getWarehouse_count();

    void setWarehouse_count(Integer warehouse_count);

    /**
     * 获取 [仓库个数]脏标记
     */
    boolean getWarehouse_countDirtyFlag();

    /**
     * 制造补给
     */
    String getManufacture_to_resupply();

    void setManufacture_to_resupply(String manufacture_to_resupply);

    /**
     * 获取 [制造补给]脏标记
     */
    boolean getManufacture_to_resupplyDirtyFlag();

    /**
     * 创建时间
     */
    Timestamp getCreate_date();

    void setCreate_date(Timestamp create_date);

    /**
     * 获取 [创建时间]脏标记
     */
    boolean getCreate_dateDirtyFlag();

    /**
     * 路线
     */
    String getRoute_ids();

    void setRoute_ids(String route_ids);

    /**
     * 获取 [路线]脏标记
     */
    boolean getRoute_idsDirtyFlag();

    /**
     * 显示名称
     */
    String getDisplay_name();

    void setDisplay_name(String display_name);

    /**
     * 获取 [显示名称]脏标记
     */
    boolean getDisplay_nameDirtyFlag();

    /**
     * 入库
     */
    String getReception_steps();

    void setReception_steps(String reception_steps);

    /**
     * 获取 [入库]脏标记
     */
    boolean getReception_stepsDirtyFlag();

    /**
     * 购买补给
     */
    String getBuy_to_resupply();

    void setBuy_to_resupply(String buy_to_resupply);

    /**
     * 获取 [购买补给]脏标记
     */
    boolean getBuy_to_resupplyDirtyFlag();

    /**
     * ID
     */
    Integer getId();

    void setId(Integer id);

    /**
     * 获取 [ID]脏标记
     */
    boolean getIdDirtyFlag();

    /**
     * 缩写
     */
    String getCode();

    void setCode(String code);

    /**
     * 获取 [缩写]脏标记
     */
    boolean getCodeDirtyFlag();

    /**
     * 制造
     */
    String getManufacture_steps();

    void setManufacture_steps(String manufacture_steps);

    /**
     * 获取 [制造]脏标记
     */
    boolean getManufacture_stepsDirtyFlag();

    /**
     * 显示补给
     */
    String getShow_resupply();

    void setShow_resupply(String show_resupply);

    /**
     * 获取 [显示补给]脏标记
     */
    boolean getShow_resupplyDirtyFlag();

    /**
     * 视图位置
     */
    String getView_location_id_text();

    void setView_location_id_text(String view_location_id_text);

    /**
     * 获取 [视图位置]脏标记
     */
    boolean getView_location_id_textDirtyFlag();

    /**
     * 进货位置
     */
    String getWh_input_stock_loc_id_text();

    void setWh_input_stock_loc_id_text(String wh_input_stock_loc_id_text);

    /**
     * 获取 [进货位置]脏标记
     */
    boolean getWh_input_stock_loc_id_textDirtyFlag();

    /**
     * 制造地点后的库存
     */
    String getSam_loc_id_text();

    void setSam_loc_id_text(String sam_loc_id_text);

    /**
     * 获取 [制造地点后的库存]脏标记
     */
    boolean getSam_loc_id_textDirtyFlag();

    /**
     * 越库路线
     */
    String getCrossdock_route_id_text();

    void setCrossdock_route_id_text(String crossdock_route_id_text);

    /**
     * 获取 [越库路线]脏标记
     */
    boolean getCrossdock_route_id_textDirtyFlag();

    /**
     * 在制造路线前拣货
     */
    String getPbm_route_id_text();

    void setPbm_route_id_text(String pbm_route_id_text);

    /**
     * 获取 [在制造路线前拣货]脏标记
     */
    boolean getPbm_route_id_textDirtyFlag();

    /**
     * 最后更新者
     */
    String getWrite_uid_text();

    void setWrite_uid_text(String write_uid_text);

    /**
     * 获取 [最后更新者]脏标记
     */
    boolean getWrite_uid_textDirtyFlag();

    /**
     * 在制造作业类型前拣货
     */
    String getPbm_type_id_text();

    void setPbm_type_id_text(String pbm_type_id_text);

    /**
     * 获取 [在制造作业类型前拣货]脏标记
     */
    boolean getPbm_type_id_textDirtyFlag();

    /**
     * 分拣类型
     */
    String getPick_type_id_text();

    void setPick_type_id_text(String pick_type_id_text);

    /**
     * 获取 [分拣类型]脏标记
     */
    boolean getPick_type_id_textDirtyFlag();

    /**
     * 内部类型
     */
    String getInt_type_id_text();

    void setInt_type_id_text(String int_type_id_text);

    /**
     * 获取 [内部类型]脏标记
     */
    boolean getInt_type_id_textDirtyFlag();

    /**
     * 生产操作类型
     */
    String getManu_type_id_text();

    void setManu_type_id_text(String manu_type_id_text);

    /**
     * 获取 [生产操作类型]脏标记
     */
    boolean getManu_type_id_textDirtyFlag();

    /**
     * 购买规则
     */
    String getBuy_pull_id_text();

    void setBuy_pull_id_text(String buy_pull_id_text);

    /**
     * 获取 [购买规则]脏标记
     */
    boolean getBuy_pull_id_textDirtyFlag();

    /**
     * 打包位置
     */
    String getWh_pack_stock_loc_id_text();

    void setWh_pack_stock_loc_id_text(String wh_pack_stock_loc_id_text);

    /**
     * 获取 [打包位置]脏标记
     */
    boolean getWh_pack_stock_loc_id_textDirtyFlag();

    /**
     * 制造规则
     */
    String getManufacture_pull_id_text();

    void setManufacture_pull_id_text(String manufacture_pull_id_text);

    /**
     * 获取 [制造规则]脏标记
     */
    boolean getManufacture_pull_id_textDirtyFlag();

    /**
     * 收货路线
     */
    String getReception_route_id_text();

    void setReception_route_id_text(String reception_route_id_text);

    /**
     * 获取 [收货路线]脏标记
     */
    boolean getReception_route_id_textDirtyFlag();

    /**
     * 交货路线
     */
    String getDelivery_route_id_text();

    void setDelivery_route_id_text(String delivery_route_id_text);

    /**
     * 获取 [交货路线]脏标记
     */
    boolean getDelivery_route_id_textDirtyFlag();

    /**
     * 制造运营类型后的库存
     */
    String getSam_type_id_text();

    void setSam_type_id_text(String sam_type_id_text);

    /**
     * 获取 [制造运营类型后的库存]脏标记
     */
    boolean getSam_type_id_textDirtyFlag();

    /**
     * 地址
     */
    String getPartner_id_text();

    void setPartner_id_text(String partner_id_text);

    /**
     * 获取 [地址]脏标记
     */
    boolean getPartner_id_textDirtyFlag();

    /**
     * 出库类型
     */
    String getOut_type_id_text();

    void setOut_type_id_text(String out_type_id_text);

    /**
     * 获取 [出库类型]脏标记
     */
    boolean getOut_type_id_textDirtyFlag();

    /**
     * MTO规则
     */
    String getMto_pull_id_text();

    void setMto_pull_id_text(String mto_pull_id_text);

    /**
     * 获取 [MTO规则]脏标记
     */
    boolean getMto_pull_id_textDirtyFlag();

    /**
     * 创建人
     */
    String getCreate_uid_text();

    void setCreate_uid_text(String create_uid_text);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uid_textDirtyFlag();

    /**
     * 库存位置
     */
    String getLot_stock_id_text();

    void setLot_stock_id_text(String lot_stock_id_text);

    /**
     * 获取 [库存位置]脏标记
     */
    boolean getLot_stock_id_textDirtyFlag();

    /**
     * 在制造（按订单补货）MTO规则之前拣货
     */
    String getPbm_mto_pull_id_text();

    void setPbm_mto_pull_id_text(String pbm_mto_pull_id_text);

    /**
     * 获取 [在制造（按订单补货）MTO规则之前拣货]脏标记
     */
    boolean getPbm_mto_pull_id_textDirtyFlag();

    /**
     * 公司
     */
    String getCompany_id_text();

    void setCompany_id_text(String company_id_text);

    /**
     * 获取 [公司]脏标记
     */
    boolean getCompany_id_textDirtyFlag();

    /**
     * 在制造位置前拣货
     */
    String getPbm_loc_id_text();

    void setPbm_loc_id_text(String pbm_loc_id_text);

    /**
     * 获取 [在制造位置前拣货]脏标记
     */
    boolean getPbm_loc_id_textDirtyFlag();

    /**
     * 包裹类型
     */
    String getPack_type_id_text();

    void setPack_type_id_text(String pack_type_id_text);

    /**
     * 获取 [包裹类型]脏标记
     */
    boolean getPack_type_id_textDirtyFlag();

    /**
     * 制造规则后的库存
     */
    String getSam_rule_id_text();

    void setSam_rule_id_text(String sam_rule_id_text);

    /**
     * 获取 [制造规则后的库存]脏标记
     */
    boolean getSam_rule_id_textDirtyFlag();

    /**
     * 入库类型
     */
    String getIn_type_id_text();

    void setIn_type_id_text(String in_type_id_text);

    /**
     * 获取 [入库类型]脏标记
     */
    boolean getIn_type_id_textDirtyFlag();

    /**
     * 出货位置
     */
    String getWh_output_stock_loc_id_text();

    void setWh_output_stock_loc_id_text(String wh_output_stock_loc_id_text);

    /**
     * 获取 [出货位置]脏标记
     */
    boolean getWh_output_stock_loc_id_textDirtyFlag();

    /**
     * 质量管理位置
     */
    String getWh_qc_stock_loc_id_text();

    void setWh_qc_stock_loc_id_text(String wh_qc_stock_loc_id_text);

    /**
     * 获取 [质量管理位置]脏标记
     */
    boolean getWh_qc_stock_loc_id_textDirtyFlag();

    /**
     * 出货位置
     */
    Integer getWh_output_stock_loc_id();

    void setWh_output_stock_loc_id(Integer wh_output_stock_loc_id);

    /**
     * 获取 [出货位置]脏标记
     */
    boolean getWh_output_stock_loc_idDirtyFlag();

    /**
     * 最后更新者
     */
    Integer getWrite_uid();

    void setWrite_uid(Integer write_uid);

    /**
     * 获取 [最后更新者]脏标记
     */
    boolean getWrite_uidDirtyFlag();

    /**
     * 在制造位置前拣货
     */
    Integer getPbm_loc_id();

    void setPbm_loc_id(Integer pbm_loc_id);

    /**
     * 获取 [在制造位置前拣货]脏标记
     */
    boolean getPbm_loc_idDirtyFlag();

    /**
     * 视图位置
     */
    Integer getView_location_id();

    void setView_location_id(Integer view_location_id);

    /**
     * 获取 [视图位置]脏标记
     */
    boolean getView_location_idDirtyFlag();

    /**
     * 出库类型
     */
    Integer getOut_type_id();

    void setOut_type_id(Integer out_type_id);

    /**
     * 获取 [出库类型]脏标记
     */
    boolean getOut_type_idDirtyFlag();

    /**
     * 生产操作类型
     */
    Integer getManu_type_id();

    void setManu_type_id(Integer manu_type_id);

    /**
     * 获取 [生产操作类型]脏标记
     */
    boolean getManu_type_idDirtyFlag();

    /**
     * 在制造（按订单补货）MTO规则之前拣货
     */
    Integer getPbm_mto_pull_id();

    void setPbm_mto_pull_id(Integer pbm_mto_pull_id);

    /**
     * 获取 [在制造（按订单补货）MTO规则之前拣货]脏标记
     */
    boolean getPbm_mto_pull_idDirtyFlag();

    /**
     * MTO规则
     */
    Integer getMto_pull_id();

    void setMto_pull_id(Integer mto_pull_id);

    /**
     * 获取 [MTO规则]脏标记
     */
    boolean getMto_pull_idDirtyFlag();

    /**
     * 制造运营类型后的库存
     */
    Integer getSam_type_id();

    void setSam_type_id(Integer sam_type_id);

    /**
     * 获取 [制造运营类型后的库存]脏标记
     */
    boolean getSam_type_idDirtyFlag();

    /**
     * 制造规则
     */
    Integer getManufacture_pull_id();

    void setManufacture_pull_id(Integer manufacture_pull_id);

    /**
     * 获取 [制造规则]脏标记
     */
    boolean getManufacture_pull_idDirtyFlag();

    /**
     * 制造地点后的库存
     */
    Integer getSam_loc_id();

    void setSam_loc_id(Integer sam_loc_id);

    /**
     * 获取 [制造地点后的库存]脏标记
     */
    boolean getSam_loc_idDirtyFlag();

    /**
     * 购买规则
     */
    Integer getBuy_pull_id();

    void setBuy_pull_id(Integer buy_pull_id);

    /**
     * 获取 [购买规则]脏标记
     */
    boolean getBuy_pull_idDirtyFlag();

    /**
     * 内部类型
     */
    Integer getInt_type_id();

    void setInt_type_id(Integer int_type_id);

    /**
     * 获取 [内部类型]脏标记
     */
    boolean getInt_type_idDirtyFlag();

    /**
     * 质量管理位置
     */
    Integer getWh_qc_stock_loc_id();

    void setWh_qc_stock_loc_id(Integer wh_qc_stock_loc_id);

    /**
     * 获取 [质量管理位置]脏标记
     */
    boolean getWh_qc_stock_loc_idDirtyFlag();

    /**
     * 入库类型
     */
    Integer getIn_type_id();

    void setIn_type_id(Integer in_type_id);

    /**
     * 获取 [入库类型]脏标记
     */
    boolean getIn_type_idDirtyFlag();

    /**
     * 在制造路线前拣货
     */
    Integer getPbm_route_id();

    void setPbm_route_id(Integer pbm_route_id);

    /**
     * 获取 [在制造路线前拣货]脏标记
     */
    boolean getPbm_route_idDirtyFlag();

    /**
     * 分拣类型
     */
    Integer getPick_type_id();

    void setPick_type_id(Integer pick_type_id);

    /**
     * 获取 [分拣类型]脏标记
     */
    boolean getPick_type_idDirtyFlag();

    /**
     * 创建人
     */
    Integer getCreate_uid();

    void setCreate_uid(Integer create_uid);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uidDirtyFlag();

    /**
     * 地址
     */
    Integer getPartner_id();

    void setPartner_id(Integer partner_id);

    /**
     * 获取 [地址]脏标记
     */
    boolean getPartner_idDirtyFlag();

    /**
     * 公司
     */
    Integer getCompany_id();

    void setCompany_id(Integer company_id);

    /**
     * 获取 [公司]脏标记
     */
    boolean getCompany_idDirtyFlag();

    /**
     * 收货路线
     */
    Integer getReception_route_id();

    void setReception_route_id(Integer reception_route_id);

    /**
     * 获取 [收货路线]脏标记
     */
    boolean getReception_route_idDirtyFlag();

    /**
     * 包裹类型
     */
    Integer getPack_type_id();

    void setPack_type_id(Integer pack_type_id);

    /**
     * 获取 [包裹类型]脏标记
     */
    boolean getPack_type_idDirtyFlag();

    /**
     * 在制造作业类型前拣货
     */
    Integer getPbm_type_id();

    void setPbm_type_id(Integer pbm_type_id);

    /**
     * 获取 [在制造作业类型前拣货]脏标记
     */
    boolean getPbm_type_idDirtyFlag();

    /**
     * 交货路线
     */
    Integer getDelivery_route_id();

    void setDelivery_route_id(Integer delivery_route_id);

    /**
     * 获取 [交货路线]脏标记
     */
    boolean getDelivery_route_idDirtyFlag();

    /**
     * 进货位置
     */
    Integer getWh_input_stock_loc_id();

    void setWh_input_stock_loc_id(Integer wh_input_stock_loc_id);

    /**
     * 获取 [进货位置]脏标记
     */
    boolean getWh_input_stock_loc_idDirtyFlag();

    /**
     * 库存位置
     */
    Integer getLot_stock_id();

    void setLot_stock_id(Integer lot_stock_id);

    /**
     * 获取 [库存位置]脏标记
     */
    boolean getLot_stock_idDirtyFlag();

    /**
     * 制造规则后的库存
     */
    Integer getSam_rule_id();

    void setSam_rule_id(Integer sam_rule_id);

    /**
     * 获取 [制造规则后的库存]脏标记
     */
    boolean getSam_rule_idDirtyFlag();

    /**
     * 越库路线
     */
    Integer getCrossdock_route_id();

    void setCrossdock_route_id(Integer crossdock_route_id);

    /**
     * 获取 [越库路线]脏标记
     */
    boolean getCrossdock_route_idDirtyFlag();

    /**
     * 打包位置
     */
    Integer getWh_pack_stock_loc_id();

    void setWh_pack_stock_loc_id(Integer wh_pack_stock_loc_id);

    /**
     * 获取 [打包位置]脏标记
     */
    boolean getWh_pack_stock_loc_idDirtyFlag();

}
