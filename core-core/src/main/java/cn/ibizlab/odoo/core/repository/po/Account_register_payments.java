package cn.ibizlab.odoo.core.repository.po ;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.math.BigInteger;

import cn.ibizlab.odoo.core.odoo_account.filter.Account_register_paymentsSearchContext;

/**
 * 实体 [登记付款] 存储模型
 */
public interface Account_register_payments{

    /**
     * 显示评论字段
     */
    String getShow_communication_field();

    void setShow_communication_field(String show_communication_field);

    /**
     * 获取 [显示评论字段]脏标记
     */
    boolean getShow_communication_fieldDirtyFlag();

    /**
     * 付款类型
     */
    String getPayment_type();

    void setPayment_type(String payment_type);

    /**
     * 获取 [付款类型]脏标记
     */
    boolean getPayment_typeDirtyFlag();

    /**
     * 显示名称
     */
    String getDisplay_name();

    void setDisplay_name(String display_name);

    /**
     * 获取 [显示名称]脏标记
     */
    boolean getDisplay_nameDirtyFlag();

    /**
     * 业务伙伴类型
     */
    String getPartner_type();

    void setPartner_type(String partner_type);

    /**
     * 获取 [业务伙伴类型]脏标记
     */
    boolean getPartner_typeDirtyFlag();

    /**
     * 备忘
     */
    String getCommunication();

    void setCommunication(String communication);

    /**
     * 获取 [备忘]脏标记
     */
    boolean getCommunicationDirtyFlag();

    /**
     * 付款差异
     */
    Double getPayment_difference();

    void setPayment_difference(Double payment_difference);

    /**
     * 获取 [付款差异]脏标记
     */
    boolean getPayment_differenceDirtyFlag();

    /**
     * 显示合作伙伴银行账户
     */
    String getShow_partner_bank_account();

    void setShow_partner_bank_account(String show_partner_bank_account);

    /**
     * 获取 [显示合作伙伴银行账户]脏标记
     */
    boolean getShow_partner_bank_accountDirtyFlag();

    /**
     * 发票分组
     */
    String getGroup_invoices();

    void setGroup_invoices(String group_invoices);

    /**
     * 获取 [发票分组]脏标记
     */
    boolean getGroup_invoicesDirtyFlag();

    /**
     * 隐藏付款方式
     */
    String getHide_payment_method();

    void setHide_payment_method(String hide_payment_method);

    /**
     * 获取 [隐藏付款方式]脏标记
     */
    boolean getHide_payment_methodDirtyFlag();

    /**
     * 发票
     */
    String getInvoice_ids();

    void setInvoice_ids(String invoice_ids);

    /**
     * 获取 [发票]脏标记
     */
    boolean getInvoice_idsDirtyFlag();

    /**
     * 最后修改日
     */
    Timestamp get__last_update();

    void set__last_update(Timestamp __last_update);

    /**
     * 获取 [最后修改日]脏标记
     */
    boolean get__last_updateDirtyFlag();

    /**
     * 付款日期
     */
    Timestamp getPayment_date();

    void setPayment_date(Timestamp payment_date);

    /**
     * 获取 [付款日期]脏标记
     */
    boolean getPayment_dateDirtyFlag();

    /**
     * 日记账项目标签
     */
    String getWriteoff_label();

    void setWriteoff_label(String writeoff_label);

    /**
     * 获取 [日记账项目标签]脏标记
     */
    boolean getWriteoff_labelDirtyFlag();

    /**
     * 多
     */
    String getMulti();

    void setMulti(String multi);

    /**
     * 获取 [多]脏标记
     */
    boolean getMultiDirtyFlag();

    /**
     * 最后更新时间
     */
    Timestamp getWrite_date();

    void setWrite_date(Timestamp write_date);

    /**
     * 获取 [最后更新时间]脏标记
     */
    boolean getWrite_dateDirtyFlag();

    /**
     * 创建时间
     */
    Timestamp getCreate_date();

    void setCreate_date(Timestamp create_date);

    /**
     * 获取 [创建时间]脏标记
     */
    boolean getCreate_dateDirtyFlag();

    /**
     * ID
     */
    Integer getId();

    void setId(Integer id);

    /**
     * 获取 [ID]脏标记
     */
    boolean getIdDirtyFlag();

    /**
     * 付款差异处理
     */
    String getPayment_difference_handling();

    void setPayment_difference_handling(String payment_difference_handling);

    /**
     * 获取 [付款差异处理]脏标记
     */
    boolean getPayment_difference_handlingDirtyFlag();

    /**
     * 付款金额
     */
    Double getAmount();

    void setAmount(Double amount);

    /**
     * 获取 [付款金额]脏标记
     */
    boolean getAmountDirtyFlag();

    /**
     * 币种
     */
    String getCurrency_id_text();

    void setCurrency_id_text(String currency_id_text);

    /**
     * 获取 [币种]脏标记
     */
    boolean getCurrency_id_textDirtyFlag();

    /**
     * 最后更新人
     */
    String getWrite_uid_text();

    void setWrite_uid_text(String write_uid_text);

    /**
     * 获取 [最后更新人]脏标记
     */
    boolean getWrite_uid_textDirtyFlag();

    /**
     * 代码
     */
    String getPayment_method_code();

    void setPayment_method_code(String payment_method_code);

    /**
     * 获取 [代码]脏标记
     */
    boolean getPayment_method_codeDirtyFlag();

    /**
     * 付款方法类型
     */
    String getPayment_method_id_text();

    void setPayment_method_id_text(String payment_method_id_text);

    /**
     * 获取 [付款方法类型]脏标记
     */
    boolean getPayment_method_id_textDirtyFlag();

    /**
     * 创建人
     */
    String getCreate_uid_text();

    void setCreate_uid_text(String create_uid_text);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uid_textDirtyFlag();

    /**
     * 付款日记账
     */
    String getJournal_id_text();

    void setJournal_id_text(String journal_id_text);

    /**
     * 获取 [付款日记账]脏标记
     */
    boolean getJournal_id_textDirtyFlag();

    /**
     * 业务伙伴
     */
    String getPartner_id_text();

    void setPartner_id_text(String partner_id_text);

    /**
     * 获取 [业务伙伴]脏标记
     */
    boolean getPartner_id_textDirtyFlag();

    /**
     * 差异科目
     */
    String getWriteoff_account_id_text();

    void setWriteoff_account_id_text(String writeoff_account_id_text);

    /**
     * 获取 [差异科目]脏标记
     */
    boolean getWriteoff_account_id_textDirtyFlag();

    /**
     * 差异科目
     */
    Integer getWriteoff_account_id();

    void setWriteoff_account_id(Integer writeoff_account_id);

    /**
     * 获取 [差异科目]脏标记
     */
    boolean getWriteoff_account_idDirtyFlag();

    /**
     * 收款银行账号
     */
    Integer getPartner_bank_account_id();

    void setPartner_bank_account_id(Integer partner_bank_account_id);

    /**
     * 获取 [收款银行账号]脏标记
     */
    boolean getPartner_bank_account_idDirtyFlag();

    /**
     * 最后更新人
     */
    Integer getWrite_uid();

    void setWrite_uid(Integer write_uid);

    /**
     * 获取 [最后更新人]脏标记
     */
    boolean getWrite_uidDirtyFlag();

    /**
     * 创建人
     */
    Integer getCreate_uid();

    void setCreate_uid(Integer create_uid);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uidDirtyFlag();

    /**
     * 业务伙伴
     */
    Integer getPartner_id();

    void setPartner_id(Integer partner_id);

    /**
     * 获取 [业务伙伴]脏标记
     */
    boolean getPartner_idDirtyFlag();

    /**
     * 付款日记账
     */
    Integer getJournal_id();

    void setJournal_id(Integer journal_id);

    /**
     * 获取 [付款日记账]脏标记
     */
    boolean getJournal_idDirtyFlag();

    /**
     * 币种
     */
    Integer getCurrency_id();

    void setCurrency_id(Integer currency_id);

    /**
     * 获取 [币种]脏标记
     */
    boolean getCurrency_idDirtyFlag();

    /**
     * 付款方法类型
     */
    Integer getPayment_method_id();

    void setPayment_method_id(Integer payment_method_id);

    /**
     * 获取 [付款方法类型]脏标记
     */
    boolean getPayment_method_idDirtyFlag();

}
