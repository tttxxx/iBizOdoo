package cn.ibizlab.odoo.core.odoo_account.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.odoo.util.domain.EntityBase;
import cn.ibizlab.odoo.util.annotation.DEField;
import cn.ibizlab.odoo.util.enums.DEPredefinedFieldType;
import cn.ibizlab.odoo.util.enums.DEFieldDefaultValueType;
import java.io.Serializable;
import lombok.Data;
import org.springframework.data.annotation.Transient;

import cn.ibizlab.odoo.util.domain.EntityClient;

/**
 * ServiceApi [日记账项目] 对象
 */
@Data
public class Account_move_line extends EntityClient implements Serializable {

    /**
     * 分析标签
     */
    @JSONField(name = "analytic_tag_ids")
    @JsonProperty("analytic_tag_ids")
    private String analyticTagIds;

    /**
     * 最后更新时间
     */
    @DEField(name = "write_date" , preType = DEPredefinedFieldType.UPDATEDATE)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;

    /**
     * 标签
     */
    @JSONField(name = "name")
    @JsonProperty("name")
    private String name;

    /**
     * 余额
     */
    @JSONField(name = "balance")
    @JsonProperty("balance")
    private Double balance;

    /**
     * 采用的税
     */
    @JSONField(name = "tax_ids")
    @JsonProperty("tax_ids")
    private String taxIds;

    /**
     * 货币金额
     */
    @DEField(name = "amount_currency")
    @JSONField(name = "amount_currency")
    @JsonProperty("amount_currency")
    private Double amountCurrency;

    /**
     * 旧税额
     */
    @JSONField(name = "tax_line_grouping_key")
    @JsonProperty("tax_line_grouping_key")
    private String taxLineGroupingKey;

    /**
     * ID
     */
    @DEField(isKeyField=true)
    @JSONField(name = "id")
    @JsonProperty("id")
    private Integer id;

    /**
     * 已核销
     */
    @JSONField(name = "reconciled")
    @JsonProperty("reconciled")
    private String reconciled;

    /**
     * 显示名称
     */
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    private String displayName;

    /**
     * 现金余额
     */
    @DEField(name = "balance_cash_basis")
    @JSONField(name = "balance_cash_basis")
    @JsonProperty("balance_cash_basis")
    private Double balanceCashBasis;

    /**
     * 贷方现金基础
     */
    @DEField(name = "credit_cash_basis")
    @JSONField(name = "credit_cash_basis")
    @JsonProperty("credit_cash_basis")
    private Double creditCashBasis;

    /**
     * 数量
     */
    @JSONField(name = "quantity")
    @JsonProperty("quantity")
    private Double quantity;

    /**
     * 残值金额
     */
    @DEField(name = "amount_residual")
    @JSONField(name = "amount_residual")
    @JsonProperty("amount_residual")
    private Double amountResidual;

    /**
     * 重新计算税额
     */
    @JSONField(name = "recompute_tax_line")
    @JsonProperty("recompute_tax_line")
    private String recomputeTaxLine;

    /**
     * 出现在VAT报告
     */
    @DEField(name = "tax_exigible")
    @JSONField(name = "tax_exigible")
    @JsonProperty("tax_exigible")
    private String taxExigible;

    /**
     * 外币残余金额
     */
    @DEField(name = "amount_residual_currency")
    @JSONField(name = "amount_residual_currency")
    @JsonProperty("amount_residual_currency")
    private Double amountResidualCurrency;

    /**
     * 基本金额
     */
    @DEField(name = "tax_base_amount")
    @JSONField(name = "tax_base_amount")
    @JsonProperty("tax_base_amount")
    private Double taxBaseAmount;

    /**
     * 上级状态
     */
    @JSONField(name = "parent_state")
    @JsonProperty("parent_state")
    private String parentState;

    /**
     * 无催款
     */
    @JSONField(name = "blocked")
    @JsonProperty("blocked")
    private String blocked;

    /**
     * 创建时间
     */
    @DEField(name = "create_date" , preType = DEPredefinedFieldType.CREATEDATE)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;

    /**
     * 匹配的贷方
     */
    @JSONField(name = "matched_credit_ids")
    @JsonProperty("matched_credit_ids")
    private String matchedCreditIds;

    /**
     * 分析明细行
     */
    @JSONField(name = "analytic_line_ids")
    @JsonProperty("analytic_line_ids")
    private String analyticLineIds;

    /**
     * 到期日期
     */
    @DEField(name = "date_maturity")
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "date_maturity" , format="yyyy-MM-dd")
    @JsonProperty("date_maturity")
    private Timestamp dateMaturity;

    /**
     * 借方
     */
    @JSONField(name = "debit")
    @JsonProperty("debit")
    private Double debit;

    /**
     * 最后修改日
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;

    /**
     * 对方
     */
    @JSONField(name = "counterpart")
    @JsonProperty("counterpart")
    private String counterpart;

    /**
     * 匹配的借记卡
     */
    @JSONField(name = "matched_debit_ids")
    @JsonProperty("matched_debit_ids")
    private String matchedDebitIds;

    /**
     * 借记现金基础
     */
    @DEField(name = "debit_cash_basis")
    @JSONField(name = "debit_cash_basis")
    @JsonProperty("debit_cash_basis")
    private Double debitCashBasis;

    /**
     * 贷方
     */
    @JSONField(name = "credit")
    @JsonProperty("credit")
    private Double credit;

    /**
     * 记叙
     */
    @JSONField(name = "narration")
    @JsonProperty("narration")
    private String narration;

    /**
     * 产品
     */
    @JSONField(name = "product_id_text")
    @JsonProperty("product_id_text")
    private String productIdText;

    /**
     * 类型
     */
    @JSONField(name = "user_type_id_text")
    @JsonProperty("user_type_id_text")
    private String userTypeIdText;

    /**
     * 发起人付款
     */
    @JSONField(name = "payment_id_text")
    @JsonProperty("payment_id_text")
    private String paymentIdText;

    /**
     * 发起人税
     */
    @JSONField(name = "tax_line_id_text")
    @JsonProperty("tax_line_id_text")
    private String taxLineIdText;

    /**
     * 匹配号码
     */
    @JSONField(name = "full_reconcile_id_text")
    @JsonProperty("full_reconcile_id_text")
    private String fullReconcileIdText;

    /**
     * 费用
     */
    @JSONField(name = "expense_id_text")
    @JsonProperty("expense_id_text")
    private String expenseIdText;

    /**
     * 日记账分录
     */
    @JSONField(name = "move_id_text")
    @JsonProperty("move_id_text")
    private String moveIdText;

    /**
     * 日记账
     */
    @JSONField(name = "journal_id_text")
    @JsonProperty("journal_id_text")
    private String journalIdText;

    /**
     * 公司货币
     */
    @JSONField(name = "company_currency_id_text")
    @JsonProperty("company_currency_id_text")
    private String companyCurrencyIdText;

    /**
     * 发票
     */
    @JSONField(name = "invoice_id_text")
    @JsonProperty("invoice_id_text")
    private String invoiceIdText;

    /**
     * 分析账户
     */
    @JSONField(name = "analytic_account_id_text")
    @JsonProperty("analytic_account_id_text")
    private String analyticAccountIdText;

    /**
     * 创建人
     */
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    private String createUidText;

    /**
     * 币种
     */
    @JSONField(name = "currency_id_text")
    @JsonProperty("currency_id_text")
    private String currencyIdText;

    /**
     * 参考
     */
    @JSONField(name = "ref")
    @JsonProperty("ref")
    private String ref;

    /**
     * 公司
     */
    @JSONField(name = "company_id_text")
    @JsonProperty("company_id_text")
    private String companyIdText;

    /**
     * 报告
     */
    @JSONField(name = "statement_id_text")
    @JsonProperty("statement_id_text")
    private String statementIdText;

    /**
     * 用该分录核销的银行核销单明细
     */
    @JSONField(name = "statement_line_id_text")
    @JsonProperty("statement_line_id_text")
    private String statementLineIdText;

    /**
     * 业务伙伴
     */
    @JSONField(name = "partner_id_text")
    @JsonProperty("partner_id_text")
    private String partnerIdText;

    /**
     * 科目
     */
    @JSONField(name = "account_id_text")
    @JsonProperty("account_id_text")
    private String accountIdText;

    /**
     * 日期
     */
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "date" , format="yyyy-MM-dd")
    @JsonProperty("date")
    private Timestamp date;

    /**
     * 计量单位
     */
    @JSONField(name = "product_uom_id_text")
    @JsonProperty("product_uom_id_text")
    private String productUomIdText;

    /**
     * 最后更新人
     */
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    private String writeUidText;

    /**
     * 日记账分录
     */
    @DEField(name = "move_id")
    @JSONField(name = "move_id")
    @JsonProperty("move_id")
    private Integer moveId;

    /**
     * 币种
     */
    @DEField(name = "currency_id")
    @JSONField(name = "currency_id")
    @JsonProperty("currency_id")
    private Integer currencyId;

    /**
     * 公司
     */
    @DEField(name = "company_id")
    @JSONField(name = "company_id")
    @JsonProperty("company_id")
    private Integer companyId;

    /**
     * 发起人付款
     */
    @DEField(name = "payment_id")
    @JSONField(name = "payment_id")
    @JsonProperty("payment_id")
    private Integer paymentId;

    /**
     * 公司货币
     */
    @DEField(name = "company_currency_id")
    @JSONField(name = "company_currency_id")
    @JsonProperty("company_currency_id")
    private Integer companyCurrencyId;

    /**
     * 日记账
     */
    @DEField(name = "journal_id")
    @JSONField(name = "journal_id")
    @JsonProperty("journal_id")
    private Integer journalId;

    /**
     * 分析账户
     */
    @DEField(name = "analytic_account_id")
    @JSONField(name = "analytic_account_id")
    @JsonProperty("analytic_account_id")
    private Integer analyticAccountId;

    /**
     * 发票
     */
    @DEField(name = "invoice_id")
    @JSONField(name = "invoice_id")
    @JsonProperty("invoice_id")
    private Integer invoiceId;

    /**
     * 发起人税
     */
    @DEField(name = "tax_line_id")
    @JSONField(name = "tax_line_id")
    @JsonProperty("tax_line_id")
    private Integer taxLineId;

    /**
     * 匹配号码
     */
    @DEField(name = "full_reconcile_id")
    @JSONField(name = "full_reconcile_id")
    @JsonProperty("full_reconcile_id")
    private Integer fullReconcileId;

    /**
     * 用该分录核销的银行核销单明细
     */
    @DEField(name = "statement_line_id")
    @JSONField(name = "statement_line_id")
    @JsonProperty("statement_line_id")
    private Integer statementLineId;

    /**
     * 创建人
     */
    @DEField(name = "create_uid" , preType = DEPredefinedFieldType.CREATEMAN)
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    private Integer createUid;

    /**
     * 产品
     */
    @DEField(name = "product_id")
    @JSONField(name = "product_id")
    @JsonProperty("product_id")
    private Integer productId;

    /**
     * 计量单位
     */
    @DEField(name = "product_uom_id")
    @JSONField(name = "product_uom_id")
    @JsonProperty("product_uom_id")
    private Integer productUomId;

    /**
     * 业务伙伴
     */
    @DEField(name = "partner_id")
    @JSONField(name = "partner_id")
    @JsonProperty("partner_id")
    private Integer partnerId;

    /**
     * 科目
     */
    @DEField(name = "account_id")
    @JSONField(name = "account_id")
    @JsonProperty("account_id")
    private Integer accountId;

    /**
     * 报告
     */
    @DEField(name = "statement_id")
    @JSONField(name = "statement_id")
    @JsonProperty("statement_id")
    private Integer statementId;

    /**
     * 最后更新人
     */
    @DEField(name = "write_uid" , preType = DEPredefinedFieldType.UPDATEMAN)
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    private Integer writeUid;

    /**
     * 类型
     */
    @DEField(name = "user_type_id")
    @JSONField(name = "user_type_id")
    @JsonProperty("user_type_id")
    private Integer userTypeId;

    /**
     * 费用
     */
    @DEField(name = "expense_id")
    @JSONField(name = "expense_id")
    @JsonProperty("expense_id")
    private Integer expenseId;


    /**
     * 
     */
    @JSONField(name = "odoousertype")
    @JsonProperty("odoousertype")
    private cn.ibizlab.odoo.core.odoo_account.domain.Account_account_type odooUserType;

    /**
     * 
     */
    @JSONField(name = "odooaccount")
    @JsonProperty("odooaccount")
    private cn.ibizlab.odoo.core.odoo_account.domain.Account_account odooAccount;

    /**
     * 
     */
    @JSONField(name = "odooanalyticaccount")
    @JsonProperty("odooanalyticaccount")
    private cn.ibizlab.odoo.core.odoo_account.domain.Account_analytic_account odooAnalyticAccount;

    /**
     * 
     */
    @JSONField(name = "odoostatementline")
    @JsonProperty("odoostatementline")
    private cn.ibizlab.odoo.core.odoo_account.domain.Account_bank_statement_line odooStatementLine;

    /**
     * 
     */
    @JSONField(name = "odoostatement")
    @JsonProperty("odoostatement")
    private cn.ibizlab.odoo.core.odoo_account.domain.Account_bank_statement odooStatement;

    /**
     * 
     */
    @JSONField(name = "odoofullreconcile")
    @JsonProperty("odoofullreconcile")
    private cn.ibizlab.odoo.core.odoo_account.domain.Account_full_reconcile odooFullReconcile;

    /**
     * 
     */
    @JSONField(name = "odooinvoice")
    @JsonProperty("odooinvoice")
    private cn.ibizlab.odoo.core.odoo_account.domain.Account_invoice odooInvoice;

    /**
     * 
     */
    @JSONField(name = "odoojournal")
    @JsonProperty("odoojournal")
    private cn.ibizlab.odoo.core.odoo_account.domain.Account_journal odooJournal;

    /**
     * 
     */
    @JSONField(name = "odoomove")
    @JsonProperty("odoomove")
    private cn.ibizlab.odoo.core.odoo_account.domain.Account_move odooMove;

    /**
     * 
     */
    @JSONField(name = "odoopayment")
    @JsonProperty("odoopayment")
    private cn.ibizlab.odoo.core.odoo_account.domain.Account_payment odooPayment;

    /**
     * 
     */
    @JSONField(name = "odootaxline")
    @JsonProperty("odootaxline")
    private cn.ibizlab.odoo.core.odoo_account.domain.Account_tax odooTaxLine;

    /**
     * 
     */
    @JSONField(name = "odooexpense")
    @JsonProperty("odooexpense")
    private cn.ibizlab.odoo.core.odoo_hr.domain.Hr_expense odooExpense;

    /**
     * 
     */
    @JSONField(name = "odooproduct")
    @JsonProperty("odooproduct")
    private cn.ibizlab.odoo.core.odoo_product.domain.Product_product odooProduct;

    /**
     * 
     */
    @JSONField(name = "odoocompany")
    @JsonProperty("odoocompany")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_company odooCompany;

    /**
     * 
     */
    @JSONField(name = "odoocompanycurrency")
    @JsonProperty("odoocompanycurrency")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_currency odooCompanyCurrency;

    /**
     * 
     */
    @JSONField(name = "odoocurrency")
    @JsonProperty("odoocurrency")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_currency odooCurrency;

    /**
     * 
     */
    @JSONField(name = "odoopartner")
    @JsonProperty("odoopartner")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_partner odooPartner;

    /**
     * 
     */
    @JSONField(name = "odoocreate")
    @JsonProperty("odoocreate")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_users odooCreate;

    /**
     * 
     */
    @JSONField(name = "odoowrite")
    @JsonProperty("odoowrite")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_users odooWrite;

    /**
     * 
     */
    @JSONField(name = "odooproductuom")
    @JsonProperty("odooproductuom")
    private cn.ibizlab.odoo.core.odoo_uom.domain.Uom_uom odooProductUom;




    /**
     * 设置 [标签]
     */
    public void setName(String name){
        this.name = name ;
        this.modify("name",name);
    }
    /**
     * 设置 [余额]
     */
    public void setBalance(Double balance){
        this.balance = balance ;
        this.modify("balance",balance);
    }
    /**
     * 设置 [货币金额]
     */
    public void setAmountCurrency(Double amountCurrency){
        this.amountCurrency = amountCurrency ;
        this.modify("amount_currency",amountCurrency);
    }
    /**
     * 设置 [已核销]
     */
    public void setReconciled(String reconciled){
        this.reconciled = reconciled ;
        this.modify("reconciled",reconciled);
    }
    /**
     * 设置 [现金余额]
     */
    public void setBalanceCashBasis(Double balanceCashBasis){
        this.balanceCashBasis = balanceCashBasis ;
        this.modify("balance_cash_basis",balanceCashBasis);
    }
    /**
     * 设置 [贷方现金基础]
     */
    public void setCreditCashBasis(Double creditCashBasis){
        this.creditCashBasis = creditCashBasis ;
        this.modify("credit_cash_basis",creditCashBasis);
    }
    /**
     * 设置 [数量]
     */
    public void setQuantity(Double quantity){
        this.quantity = quantity ;
        this.modify("quantity",quantity);
    }
    /**
     * 设置 [残值金额]
     */
    public void setAmountResidual(Double amountResidual){
        this.amountResidual = amountResidual ;
        this.modify("amount_residual",amountResidual);
    }
    /**
     * 设置 [出现在VAT报告]
     */
    public void setTaxExigible(String taxExigible){
        this.taxExigible = taxExigible ;
        this.modify("tax_exigible",taxExigible);
    }
    /**
     * 设置 [外币残余金额]
     */
    public void setAmountResidualCurrency(Double amountResidualCurrency){
        this.amountResidualCurrency = amountResidualCurrency ;
        this.modify("amount_residual_currency",amountResidualCurrency);
    }
    /**
     * 设置 [基本金额]
     */
    public void setTaxBaseAmount(Double taxBaseAmount){
        this.taxBaseAmount = taxBaseAmount ;
        this.modify("tax_base_amount",taxBaseAmount);
    }
    /**
     * 设置 [无催款]
     */
    public void setBlocked(String blocked){
        this.blocked = blocked ;
        this.modify("blocked",blocked);
    }
    /**
     * 设置 [到期日期]
     */
    public void setDateMaturity(Timestamp dateMaturity){
        this.dateMaturity = dateMaturity ;
        this.modify("date_maturity",dateMaturity);
    }
    /**
     * 设置 [借方]
     */
    public void setDebit(Double debit){
        this.debit = debit ;
        this.modify("debit",debit);
    }
    /**
     * 设置 [借记现金基础]
     */
    public void setDebitCashBasis(Double debitCashBasis){
        this.debitCashBasis = debitCashBasis ;
        this.modify("debit_cash_basis",debitCashBasis);
    }
    /**
     * 设置 [贷方]
     */
    public void setCredit(Double credit){
        this.credit = credit ;
        this.modify("credit",credit);
    }
    /**
     * 设置 [日记账分录]
     */
    public void setMoveId(Integer moveId){
        this.moveId = moveId ;
        this.modify("move_id",moveId);
    }
    /**
     * 设置 [币种]
     */
    public void setCurrencyId(Integer currencyId){
        this.currencyId = currencyId ;
        this.modify("currency_id",currencyId);
    }
    /**
     * 设置 [公司]
     */
    public void setCompanyId(Integer companyId){
        this.companyId = companyId ;
        this.modify("company_id",companyId);
    }
    /**
     * 设置 [发起人付款]
     */
    public void setPaymentId(Integer paymentId){
        this.paymentId = paymentId ;
        this.modify("payment_id",paymentId);
    }
    /**
     * 设置 [公司货币]
     */
    public void setCompanyCurrencyId(Integer companyCurrencyId){
        this.companyCurrencyId = companyCurrencyId ;
        this.modify("company_currency_id",companyCurrencyId);
    }
    /**
     * 设置 [日记账]
     */
    public void setJournalId(Integer journalId){
        this.journalId = journalId ;
        this.modify("journal_id",journalId);
    }
    /**
     * 设置 [分析账户]
     */
    public void setAnalyticAccountId(Integer analyticAccountId){
        this.analyticAccountId = analyticAccountId ;
        this.modify("analytic_account_id",analyticAccountId);
    }
    /**
     * 设置 [发票]
     */
    public void setInvoiceId(Integer invoiceId){
        this.invoiceId = invoiceId ;
        this.modify("invoice_id",invoiceId);
    }
    /**
     * 设置 [发起人税]
     */
    public void setTaxLineId(Integer taxLineId){
        this.taxLineId = taxLineId ;
        this.modify("tax_line_id",taxLineId);
    }
    /**
     * 设置 [匹配号码]
     */
    public void setFullReconcileId(Integer fullReconcileId){
        this.fullReconcileId = fullReconcileId ;
        this.modify("full_reconcile_id",fullReconcileId);
    }
    /**
     * 设置 [用该分录核销的银行核销单明细]
     */
    public void setStatementLineId(Integer statementLineId){
        this.statementLineId = statementLineId ;
        this.modify("statement_line_id",statementLineId);
    }
    /**
     * 设置 [产品]
     */
    public void setProductId(Integer productId){
        this.productId = productId ;
        this.modify("product_id",productId);
    }
    /**
     * 设置 [计量单位]
     */
    public void setProductUomId(Integer productUomId){
        this.productUomId = productUomId ;
        this.modify("product_uom_id",productUomId);
    }
    /**
     * 设置 [业务伙伴]
     */
    public void setPartnerId(Integer partnerId){
        this.partnerId = partnerId ;
        this.modify("partner_id",partnerId);
    }
    /**
     * 设置 [科目]
     */
    public void setAccountId(Integer accountId){
        this.accountId = accountId ;
        this.modify("account_id",accountId);
    }
    /**
     * 设置 [报告]
     */
    public void setStatementId(Integer statementId){
        this.statementId = statementId ;
        this.modify("statement_id",statementId);
    }
    /**
     * 设置 [类型]
     */
    public void setUserTypeId(Integer userTypeId){
        this.userTypeId = userTypeId ;
        this.modify("user_type_id",userTypeId);
    }
    /**
     * 设置 [费用]
     */
    public void setExpenseId(Integer expenseId){
        this.expenseId = expenseId ;
        this.modify("expense_id",expenseId);
    }

}


