package cn.ibizlab.odoo.core.odoo_website.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_website.domain.Website_published_multi_mixin;
import cn.ibizlab.odoo.core.odoo_website.filter.Website_published_multi_mixinSearchContext;
import cn.ibizlab.odoo.core.odoo_website.service.IWebsite_published_multi_mixinService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;



import cn.ibizlab.odoo.core.odoo_website.client.website_published_multi_mixinOdooClient;
import cn.ibizlab.odoo.core.odoo_website.clientmodel.website_published_multi_mixinClientModel;

/**
 * 实体[多网站发布的Mixin] 服务对象接口实现
 */
@Slf4j
@Service
public class Website_published_multi_mixinServiceImpl implements IWebsite_published_multi_mixinService {

    @Autowired
    website_published_multi_mixinOdooClient website_published_multi_mixinOdooClient;


    @Override
    public boolean create(Website_published_multi_mixin et) {
        website_published_multi_mixinClientModel clientModel = convert2Model(et,null);
		website_published_multi_mixinOdooClient.create(clientModel);
        Website_published_multi_mixin rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Website_published_multi_mixin> list){
    }

    @Override
    public Website_published_multi_mixin get(Integer id) {
        website_published_multi_mixinClientModel clientModel = new website_published_multi_mixinClientModel();
        clientModel.setId(id);
		website_published_multi_mixinOdooClient.get(clientModel);
        Website_published_multi_mixin et = convert2Domain(clientModel,null);
        if(et==null){
            et=new Website_published_multi_mixin();
            et.setId(id);
        }
        else{
        }

        return  et;
    }

    @Override
    public boolean remove(Integer id) {
        website_published_multi_mixinClientModel clientModel = new website_published_multi_mixinClientModel();
        clientModel.setId(id);
		website_published_multi_mixinOdooClient.remove(clientModel);
        return true; 
    }

    public void removeBatch(Collection<Integer> idList){
    }

    @Override
    public boolean update(Website_published_multi_mixin et) {
        website_published_multi_mixinClientModel clientModel = convert2Model(et,null);
		website_published_multi_mixinOdooClient.update(clientModel);
        Website_published_multi_mixin rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }
    
    public void updateBatch(List<Website_published_multi_mixin> list){
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Website_published_multi_mixin> searchDefault(Website_published_multi_mixinSearchContext context) {
        List<Website_published_multi_mixin> list = new ArrayList<Website_published_multi_mixin>();
        Page<website_published_multi_mixinClientModel> clientModelList = website_published_multi_mixinOdooClient.search(context);
        for(website_published_multi_mixinClientModel clientModel : clientModelList){
            list.add( this.convert2Domain(clientModel,null));
        }
        return new PageImpl<Website_published_multi_mixin>(list, context.getPageable(), clientModelList.getTotalElements());
    }

    public website_published_multi_mixinClientModel convert2Model(Website_published_multi_mixin domain , website_published_multi_mixinClientModel model) {
        if(domain == null)
            return model ;
        if(model==null){
            model = new website_published_multi_mixinClientModel();

        }
        if((Boolean) domain.getExtensionparams().get("dirtyflagenable")){
            if((Boolean) domain.getExtensionparams().get("is_publisheddirtyflag"))
                model.setIs_published(domain.getIsPublished());
            if((Boolean) domain.getExtensionparams().get("website_urldirtyflag"))
                model.setWebsite_url(domain.getWebsiteUrl());
            if((Boolean) domain.getExtensionparams().get("iddirtyflag"))
                model.setId(domain.getId());
            if((Boolean) domain.getExtensionparams().get("__last_updatedirtyflag"))
                model.set__last_update(domain.getLastUpdate());
            if((Boolean) domain.getExtensionparams().get("website_iddirtyflag"))
                model.setWebsite_id(domain.getWebsiteId());
            if((Boolean) domain.getExtensionparams().get("website_publisheddirtyflag"))
                model.setWebsite_published(domain.getWebsitePublished());
            if((Boolean) domain.getExtensionparams().get("display_namedirtyflag"))
                model.setDisplay_name(domain.getDisplayName());
            domain.getFocusNull().clear();
        }
        return model ;
    }

    public Website_published_multi_mixin convert2Domain( website_published_multi_mixinClientModel model ,Website_published_multi_mixin domain) {
        if(model == null)
            return domain ;
        if(domain == null){
            domain = new Website_published_multi_mixin();
        }

        if(model.getIs_publishedDirtyFlag())
            domain.setIsPublished(model.getIs_published());
        if(model.getWebsite_urlDirtyFlag())
            domain.setWebsiteUrl(model.getWebsite_url());
        if(model.getIdDirtyFlag())
            domain.setId(model.getId());
        if(model.get__last_updateDirtyFlag())
            domain.setLastUpdate(model.get__last_update());
        if(model.getWebsite_idDirtyFlag())
            domain.setWebsiteId(model.getWebsite_id());
        if(model.getWebsite_publishedDirtyFlag())
            domain.setWebsitePublished(model.getWebsite_published());
        if(model.getDisplay_nameDirtyFlag())
            domain.setDisplayName(model.getDisplay_name());
        return domain ;
    }

}

    



