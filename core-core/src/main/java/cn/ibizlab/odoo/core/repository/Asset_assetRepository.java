package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Asset_asset;
import cn.ibizlab.odoo.core.odoo_asset.filter.Asset_assetSearchContext;

/**
 * 实体 [Asset] 存储对象
 */
public interface Asset_assetRepository extends Repository<Asset_asset> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Asset_asset> searchDefault(Asset_assetSearchContext context);

    Asset_asset convert2PO(cn.ibizlab.odoo.core.odoo_asset.domain.Asset_asset domain , Asset_asset po) ;

    cn.ibizlab.odoo.core.odoo_asset.domain.Asset_asset convert2Domain( Asset_asset po ,cn.ibizlab.odoo.core.odoo_asset.domain.Asset_asset domain) ;

}
