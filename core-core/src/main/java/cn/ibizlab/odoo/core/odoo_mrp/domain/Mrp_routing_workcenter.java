package cn.ibizlab.odoo.core.odoo_mrp.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.odoo.util.domain.EntityBase;
import cn.ibizlab.odoo.util.annotation.DEField;
import cn.ibizlab.odoo.util.enums.DEPredefinedFieldType;
import cn.ibizlab.odoo.util.enums.DEFieldDefaultValueType;
import java.io.Serializable;
import lombok.Data;
import org.springframework.data.annotation.Transient;

import cn.ibizlab.odoo.util.domain.EntityClient;

/**
 * ServiceApi [工作中心使用情况] 对象
 */
@Data
public class Mrp_routing_workcenter extends EntityClient implements Serializable {

    /**
     * 基于
     */
    @DEField(name = "time_mode_batch")
    @JSONField(name = "time_mode_batch")
    @JsonProperty("time_mode_batch")
    private Integer timeModeBatch;

    /**
     * 待处理的数量
     */
    @DEField(name = "batch_size")
    @JSONField(name = "batch_size")
    @JsonProperty("batch_size")
    private Double batchSize;

    /**
     * 工单
     */
    @JSONField(name = "workorder_ids")
    @JsonProperty("workorder_ids")
    private String workorderIds;

    /**
     * # 工单
     */
    @JSONField(name = "workorder_count")
    @JsonProperty("workorder_count")
    private Integer workorderCount;

    /**
     * 序号
     */
    @JSONField(name = "sequence")
    @JsonProperty("sequence")
    private Integer sequence;

    /**
     * 最后更新时间
     */
    @DEField(name = "write_date" , preType = DEPredefinedFieldType.UPDATEDATE)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;

    /**
     * 下一作业
     */
    @JSONField(name = "batch")
    @JsonProperty("batch")
    private String batch;

    /**
     * 人工时长
     */
    @DEField(name = "time_cycle_manual")
    @JSONField(name = "time_cycle_manual")
    @JsonProperty("time_cycle_manual")
    private Double timeCycleManual;

    /**
     * ID
     */
    @DEField(isKeyField=true)
    @JSONField(name = "id")
    @JsonProperty("id")
    private Integer id;

    /**
     * 说明
     */
    @JSONField(name = "note")
    @JsonProperty("note")
    private String note;

    /**
     * 时长计算
     */
    @DEField(name = "time_mode")
    @JSONField(name = "time_mode")
    @JsonProperty("time_mode")
    private String timeMode;

    /**
     * 显示名称
     */
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    private String displayName;

    /**
     * 最后修改日
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;

    /**
     * 操作
     */
    @JSONField(name = "name")
    @JsonProperty("name")
    private String name;

    /**
     * 时长
     */
    @JSONField(name = "time_cycle")
    @JsonProperty("time_cycle")
    private Double timeCycle;

    /**
     * 工作记录表
     */
    @JSONField(name = "worksheet")
    @JsonProperty("worksheet")
    private byte[] worksheet;

    /**
     * 创建时间
     */
    @DEField(name = "create_date" , preType = DEPredefinedFieldType.CREATEDATE)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;

    /**
     * 公司
     */
    @JSONField(name = "company_id_text")
    @JsonProperty("company_id_text")
    private String companyIdText;

    /**
     * 工作中心
     */
    @JSONField(name = "workcenter_id_text")
    @JsonProperty("workcenter_id_text")
    private String workcenterIdText;

    /**
     * 父级工艺路线
     */
    @JSONField(name = "routing_id_text")
    @JsonProperty("routing_id_text")
    private String routingIdText;

    /**
     * 最后更新者
     */
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    private String writeUidText;

    /**
     * 创建人
     */
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    private String createUidText;

    /**
     * 父级工艺路线
     */
    @DEField(name = "routing_id")
    @JSONField(name = "routing_id")
    @JsonProperty("routing_id")
    private Integer routingId;

    /**
     * 最后更新者
     */
    @DEField(name = "write_uid" , preType = DEPredefinedFieldType.UPDATEMAN)
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    private Integer writeUid;

    /**
     * 公司
     */
    @DEField(name = "company_id")
    @JSONField(name = "company_id")
    @JsonProperty("company_id")
    private Integer companyId;

    /**
     * 工作中心
     */
    @DEField(name = "workcenter_id")
    @JSONField(name = "workcenter_id")
    @JsonProperty("workcenter_id")
    private Integer workcenterId;

    /**
     * 创建人
     */
    @DEField(name = "create_uid" , preType = DEPredefinedFieldType.CREATEMAN)
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    private Integer createUid;


    /**
     * 
     */
    @JSONField(name = "odoorouting")
    @JsonProperty("odoorouting")
    private cn.ibizlab.odoo.core.odoo_mrp.domain.Mrp_routing odooRouting;

    /**
     * 
     */
    @JSONField(name = "odooworkcenter")
    @JsonProperty("odooworkcenter")
    private cn.ibizlab.odoo.core.odoo_mrp.domain.Mrp_workcenter odooWorkcenter;

    /**
     * 
     */
    @JSONField(name = "odoocompany")
    @JsonProperty("odoocompany")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_company odooCompany;

    /**
     * 
     */
    @JSONField(name = "odoocreate")
    @JsonProperty("odoocreate")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_users odooCreate;

    /**
     * 
     */
    @JSONField(name = "odoowrite")
    @JsonProperty("odoowrite")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_users odooWrite;




    /**
     * 设置 [基于]
     */
    public void setTimeModeBatch(Integer timeModeBatch){
        this.timeModeBatch = timeModeBatch ;
        this.modify("time_mode_batch",timeModeBatch);
    }
    /**
     * 设置 [待处理的数量]
     */
    public void setBatchSize(Double batchSize){
        this.batchSize = batchSize ;
        this.modify("batch_size",batchSize);
    }
    /**
     * 设置 [序号]
     */
    public void setSequence(Integer sequence){
        this.sequence = sequence ;
        this.modify("sequence",sequence);
    }
    /**
     * 设置 [下一作业]
     */
    public void setBatch(String batch){
        this.batch = batch ;
        this.modify("batch",batch);
    }
    /**
     * 设置 [人工时长]
     */
    public void setTimeCycleManual(Double timeCycleManual){
        this.timeCycleManual = timeCycleManual ;
        this.modify("time_cycle_manual",timeCycleManual);
    }
    /**
     * 设置 [说明]
     */
    public void setNote(String note){
        this.note = note ;
        this.modify("note",note);
    }
    /**
     * 设置 [时长计算]
     */
    public void setTimeMode(String timeMode){
        this.timeMode = timeMode ;
        this.modify("time_mode",timeMode);
    }
    /**
     * 设置 [操作]
     */
    public void setName(String name){
        this.name = name ;
        this.modify("name",name);
    }
    /**
     * 设置 [父级工艺路线]
     */
    public void setRoutingId(Integer routingId){
        this.routingId = routingId ;
        this.modify("routing_id",routingId);
    }
    /**
     * 设置 [公司]
     */
    public void setCompanyId(Integer companyId){
        this.companyId = companyId ;
        this.modify("company_id",companyId);
    }
    /**
     * 设置 [工作中心]
     */
    public void setWorkcenterId(Integer workcenterId){
        this.workcenterId = workcenterId ;
        this.modify("workcenter_id",workcenterId);
    }

}


