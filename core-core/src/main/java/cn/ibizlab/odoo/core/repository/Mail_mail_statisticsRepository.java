package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Mail_mail_statistics;
import cn.ibizlab.odoo.core.odoo_mail.filter.Mail_mail_statisticsSearchContext;

/**
 * 实体 [邮件统计] 存储对象
 */
public interface Mail_mail_statisticsRepository extends Repository<Mail_mail_statistics> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Mail_mail_statistics> searchDefault(Mail_mail_statisticsSearchContext context);

    Mail_mail_statistics convert2PO(cn.ibizlab.odoo.core.odoo_mail.domain.Mail_mail_statistics domain , Mail_mail_statistics po) ;

    cn.ibizlab.odoo.core.odoo_mail.domain.Mail_mail_statistics convert2Domain( Mail_mail_statistics po ,cn.ibizlab.odoo.core.odoo_mail.domain.Mail_mail_statistics domain) ;

}
