package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Sale_payment_acquirer_onboarding_wizard;
import cn.ibizlab.odoo.core.odoo_sale.filter.Sale_payment_acquirer_onboarding_wizardSearchContext;

/**
 * 实体 [销售付款获得在线向导] 存储对象
 */
public interface Sale_payment_acquirer_onboarding_wizardRepository extends Repository<Sale_payment_acquirer_onboarding_wizard> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Sale_payment_acquirer_onboarding_wizard> searchDefault(Sale_payment_acquirer_onboarding_wizardSearchContext context);

    Sale_payment_acquirer_onboarding_wizard convert2PO(cn.ibizlab.odoo.core.odoo_sale.domain.Sale_payment_acquirer_onboarding_wizard domain , Sale_payment_acquirer_onboarding_wizard po) ;

    cn.ibizlab.odoo.core.odoo_sale.domain.Sale_payment_acquirer_onboarding_wizard convert2Domain( Sale_payment_acquirer_onboarding_wizard po ,cn.ibizlab.odoo.core.odoo_sale.domain.Sale_payment_acquirer_onboarding_wizard domain) ;

}
