package cn.ibizlab.odoo.core.repository.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import cn.ibizlab.odoo.core.repository.client.po.gamification_challenge;

/**
 * 实体[gamification_challenge] 服务对象接口
 */
public interface gamification_challengeRepository{


    public gamification_challenge createPO() ;
        public void create(gamification_challenge gamification_challenge);

        public void createBatch(gamification_challenge gamification_challenge);

        public List<gamification_challenge> search();

        public void update(gamification_challenge gamification_challenge);

        public void get(String id);

        public void remove(String id);

        public void updateBatch(gamification_challenge gamification_challenge);

        public void removeBatch(String id);


}
