package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Sms_send_sms;
import cn.ibizlab.odoo.core.odoo_sms.filter.Sms_send_smsSearchContext;

/**
 * 实体 [发送短信] 存储对象
 */
public interface Sms_send_smsRepository extends Repository<Sms_send_sms> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Sms_send_sms> searchDefault(Sms_send_smsSearchContext context);

    Sms_send_sms convert2PO(cn.ibizlab.odoo.core.odoo_sms.domain.Sms_send_sms domain , Sms_send_sms po) ;

    cn.ibizlab.odoo.core.odoo_sms.domain.Sms_send_sms convert2Domain( Sms_send_sms po ,cn.ibizlab.odoo.core.odoo_sms.domain.Sms_send_sms domain) ;

}
