package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Ires_company;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[res_company] 服务对象接口
 */
public interface Ires_companyClientService{

    public Ires_company createModel() ;

    public void removeBatch(List<Ires_company> res_companies);

    public Page<Ires_company> search(SearchContext context);

    public void create(Ires_company res_company);

    public void remove(Ires_company res_company);

    public void updateBatch(List<Ires_company> res_companies);

    public void update(Ires_company res_company);

    public void createBatch(List<Ires_company> res_companies);

    public void get(Ires_company res_company);

    public Page<Ires_company> select(SearchContext context);

}
