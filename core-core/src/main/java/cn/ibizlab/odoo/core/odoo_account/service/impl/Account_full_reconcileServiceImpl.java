package cn.ibizlab.odoo.core.odoo_account.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_account.domain.Account_full_reconcile;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_full_reconcileSearchContext;
import cn.ibizlab.odoo.core.odoo_account.service.IAccount_full_reconcileService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;



import cn.ibizlab.odoo.core.odoo_account.client.account_full_reconcileOdooClient;
import cn.ibizlab.odoo.core.odoo_account.clientmodel.account_full_reconcileClientModel;

/**
 * 实体[完全核销] 服务对象接口实现
 */
@Slf4j
@Service
public class Account_full_reconcileServiceImpl implements IAccount_full_reconcileService {

    @Autowired
    account_full_reconcileOdooClient account_full_reconcileOdooClient;


    @Override
    public boolean create(Account_full_reconcile et) {
        account_full_reconcileClientModel clientModel = convert2Model(et,null);
		account_full_reconcileOdooClient.create(clientModel);
        Account_full_reconcile rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Account_full_reconcile> list){
    }

    @Override
    public boolean remove(Integer id) {
        account_full_reconcileClientModel clientModel = new account_full_reconcileClientModel();
        clientModel.setId(id);
		account_full_reconcileOdooClient.remove(clientModel);
        return true; 
    }

    public void removeBatch(Collection<Integer> idList){
    }

    @Override
    public Account_full_reconcile get(Integer id) {
        account_full_reconcileClientModel clientModel = new account_full_reconcileClientModel();
        clientModel.setId(id);
		account_full_reconcileOdooClient.get(clientModel);
        Account_full_reconcile et = convert2Domain(clientModel,null);
        if(et==null){
            et=new Account_full_reconcile();
            et.setId(id);
        }
        else{
        }

        return  et;
    }

    @Override
    public boolean update(Account_full_reconcile et) {
        account_full_reconcileClientModel clientModel = convert2Model(et,null);
		account_full_reconcileOdooClient.update(clientModel);
        Account_full_reconcile rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }
    
    public void updateBatch(List<Account_full_reconcile> list){
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Account_full_reconcile> searchDefault(Account_full_reconcileSearchContext context) {
        List<Account_full_reconcile> list = new ArrayList<Account_full_reconcile>();
        Page<account_full_reconcileClientModel> clientModelList = account_full_reconcileOdooClient.search(context);
        for(account_full_reconcileClientModel clientModel : clientModelList){
            list.add( this.convert2Domain(clientModel,null));
        }
        return new PageImpl<Account_full_reconcile>(list, context.getPageable(), clientModelList.getTotalElements());
    }

    public account_full_reconcileClientModel convert2Model(Account_full_reconcile domain , account_full_reconcileClientModel model) {
        if(domain == null)
            return model ;
        if(model==null){
            model = new account_full_reconcileClientModel();

        }
        if((Boolean) domain.getExtensionparams().get("dirtyflagenable")){
            if((Boolean) domain.getExtensionparams().get("partial_reconcile_idsdirtyflag"))
                model.setPartial_reconcile_ids(domain.getPartialReconcileIds());
            if((Boolean) domain.getExtensionparams().get("write_datedirtyflag"))
                model.setWrite_date(domain.getWriteDate());
            if((Boolean) domain.getExtensionparams().get("iddirtyflag"))
                model.setId(domain.getId());
            if((Boolean) domain.getExtensionparams().get("__last_updatedirtyflag"))
                model.set__last_update(domain.getLastUpdate());
            if((Boolean) domain.getExtensionparams().get("namedirtyflag"))
                model.setName(domain.getName());
            if((Boolean) domain.getExtensionparams().get("create_datedirtyflag"))
                model.setCreate_date(domain.getCreateDate());
            if((Boolean) domain.getExtensionparams().get("reconciled_line_idsdirtyflag"))
                model.setReconciled_line_ids(domain.getReconciledLineIds());
            if((Boolean) domain.getExtensionparams().get("display_namedirtyflag"))
                model.setDisplay_name(domain.getDisplayName());
            if((Boolean) domain.getExtensionparams().get("write_uid_textdirtyflag"))
                model.setWrite_uid_text(domain.getWriteUidText());
            if((Boolean) domain.getExtensionparams().get("exchange_move_id_textdirtyflag"))
                model.setExchange_move_id_text(domain.getExchangeMoveIdText());
            if((Boolean) domain.getExtensionparams().get("create_uid_textdirtyflag"))
                model.setCreate_uid_text(domain.getCreateUidText());
            if((Boolean) domain.getExtensionparams().get("write_uiddirtyflag"))
                model.setWrite_uid(domain.getWriteUid());
            if((Boolean) domain.getExtensionparams().get("create_uiddirtyflag"))
                model.setCreate_uid(domain.getCreateUid());
            if((Boolean) domain.getExtensionparams().get("exchange_move_iddirtyflag"))
                model.setExchange_move_id(domain.getExchangeMoveId());
            domain.getFocusNull().clear();
        }
        return model ;
    }

    public Account_full_reconcile convert2Domain( account_full_reconcileClientModel model ,Account_full_reconcile domain) {
        if(model == null)
            return domain ;
        if(domain == null){
            domain = new Account_full_reconcile();
        }

        if(model.getPartial_reconcile_idsDirtyFlag())
            domain.setPartialReconcileIds(model.getPartial_reconcile_ids());
        if(model.getWrite_dateDirtyFlag())
            domain.setWriteDate(model.getWrite_date());
        if(model.getIdDirtyFlag())
            domain.setId(model.getId());
        if(model.get__last_updateDirtyFlag())
            domain.setLastUpdate(model.get__last_update());
        if(model.getNameDirtyFlag())
            domain.setName(model.getName());
        if(model.getCreate_dateDirtyFlag())
            domain.setCreateDate(model.getCreate_date());
        if(model.getReconciled_line_idsDirtyFlag())
            domain.setReconciledLineIds(model.getReconciled_line_ids());
        if(model.getDisplay_nameDirtyFlag())
            domain.setDisplayName(model.getDisplay_name());
        if(model.getWrite_uid_textDirtyFlag())
            domain.setWriteUidText(model.getWrite_uid_text());
        if(model.getExchange_move_id_textDirtyFlag())
            domain.setExchangeMoveIdText(model.getExchange_move_id_text());
        if(model.getCreate_uid_textDirtyFlag())
            domain.setCreateUidText(model.getCreate_uid_text());
        if(model.getWrite_uidDirtyFlag())
            domain.setWriteUid(model.getWrite_uid());
        if(model.getCreate_uidDirtyFlag())
            domain.setCreateUid(model.getCreate_uid());
        if(model.getExchange_move_idDirtyFlag())
            domain.setExchangeMoveId(model.getExchange_move_id());
        return domain ;
    }

}

    



