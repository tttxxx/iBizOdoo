package cn.ibizlab.odoo.core.repository.client.po;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigDecimal;

/**
 * [fleet_vehicle] 对象
 */
public interface fleet_vehicle {

    public Timestamp getAcquisition_date();

    public void setAcquisition_date(Timestamp acquisition_date);

    public String getActive();

    public void setActive(String active);

    public Timestamp getActivity_date_deadline();

    public void setActivity_date_deadline(Timestamp activity_date_deadline);

    public String getActivity_ids();

    public void setActivity_ids(String activity_ids);

    public String getActivity_state();

    public void setActivity_state(String activity_state);

    public String getActivity_summary();

    public void setActivity_summary(String activity_summary);

    public Integer getActivity_type_id();

    public void setActivity_type_id(Integer activity_type_id);

    public String getActivity_type_id_text();

    public void setActivity_type_id_text(String activity_type_id_text);

    public Integer getActivity_user_id();

    public void setActivity_user_id(Integer activity_user_id);

    public String getActivity_user_id_text();

    public void setActivity_user_id_text(String activity_user_id_text);

    public Integer getBrand_id();

    public void setBrand_id(Integer brand_id);

    public String getBrand_id_text();

    public void setBrand_id_text(String brand_id_text);

    public Double getCar_value();

    public void setCar_value(Double car_value);

    public Double getCo2();

    public void setCo2(Double co2);

    public String getColor();

    public void setColor(String color);

    public Integer getCompany_id();

    public void setCompany_id(Integer company_id);

    public String getCompany_id_text();

    public void setCompany_id_text(String company_id_text);

    public Integer getContract_count();

    public void setContract_count(Integer contract_count);

    public String getContract_renewal_due_soon();

    public void setContract_renewal_due_soon(String contract_renewal_due_soon);

    public String getContract_renewal_name();

    public void setContract_renewal_name(String contract_renewal_name);

    public String getContract_renewal_overdue();

    public void setContract_renewal_overdue(String contract_renewal_overdue);

    public String getContract_renewal_total();

    public void setContract_renewal_total(String contract_renewal_total);

    public Integer getCost_count();

    public void setCost_count(Integer cost_count);

    public Timestamp getCreate_date();

    public void setCreate_date(Timestamp create_date);

    public Integer getCreate_uid();

    public void setCreate_uid(Integer create_uid);

    public String getCreate_uid_text();

    public void setCreate_uid_text(String create_uid_text);

    public String getDisplay_name();

    public void setDisplay_name(String display_name);

    public Integer getDoors();

    public void setDoors(Integer doors);

    public Integer getDriver_id();

    public void setDriver_id(Integer driver_id);

    public String getDriver_id_text();

    public void setDriver_id_text(String driver_id_text);

    public Timestamp getFirst_contract_date();

    public void setFirst_contract_date(Timestamp first_contract_date);

    public Integer getFuel_logs_count();

    public void setFuel_logs_count(Integer fuel_logs_count);

    public String getFuel_type();

    public void setFuel_type(String fuel_type);

    public Integer getHorsepower();

    public void setHorsepower(Integer horsepower);

    public Double getHorsepower_tax();

    public void setHorsepower_tax(Double horsepower_tax);

    public Integer getId();

    public void setId(Integer id);

    public byte[] getImage();

    public void setImage(byte[] image);

    public byte[] getImage_medium();

    public void setImage_medium(byte[] image_medium);

    public byte[] getImage_small();

    public void setImage_small(byte[] image_small);

    public String getLicense_plate();

    public void setLicense_plate(String license_plate);

    public String getLocation();

    public void setLocation(String location);

    public String getLog_contracts();

    public void setLog_contracts(String log_contracts);

    public String getLog_drivers();

    public void setLog_drivers(String log_drivers);

    public String getLog_fuel();

    public void setLog_fuel(String log_fuel);

    public String getLog_services();

    public void setLog_services(String log_services);

    public Integer getMessage_attachment_count();

    public void setMessage_attachment_count(Integer message_attachment_count);

    public String getMessage_channel_ids();

    public void setMessage_channel_ids(String message_channel_ids);

    public String getMessage_follower_ids();

    public void setMessage_follower_ids(String message_follower_ids);

    public String getMessage_has_error();

    public void setMessage_has_error(String message_has_error);

    public Integer getMessage_has_error_counter();

    public void setMessage_has_error_counter(Integer message_has_error_counter);

    public String getMessage_ids();

    public void setMessage_ids(String message_ids);

    public String getMessage_is_follower();

    public void setMessage_is_follower(String message_is_follower);

    public String getMessage_needaction();

    public void setMessage_needaction(String message_needaction);

    public Integer getMessage_needaction_counter();

    public void setMessage_needaction_counter(Integer message_needaction_counter);

    public String getMessage_partner_ids();

    public void setMessage_partner_ids(String message_partner_ids);

    public String getMessage_unread();

    public void setMessage_unread(String message_unread);

    public Integer getMessage_unread_counter();

    public void setMessage_unread_counter(Integer message_unread_counter);

    public Integer getModel_id();

    public void setModel_id(Integer model_id);

    public String getModel_id_text();

    public void setModel_id_text(String model_id_text);

    public String getModel_year();

    public void setModel_year(String model_year);

    public String getName();

    public void setName(String name);

    public Double getOdometer();

    public void setOdometer(Double odometer);

    public Integer getOdometer_count();

    public void setOdometer_count(Integer odometer_count);

    public String getOdometer_unit();

    public void setOdometer_unit(String odometer_unit);

    public Integer getPower();

    public void setPower(Integer power);

    public Double getResidual_value();

    public void setResidual_value(Double residual_value);

    public Integer getSeats();

    public void setSeats(Integer seats);

    public Integer getService_count();

    public void setService_count(Integer service_count);

    public Integer getState_id();

    public void setState_id(Integer state_id);

    public String getState_id_text();

    public void setState_id_text(String state_id_text);

    public String getTag_ids();

    public void setTag_ids(String tag_ids);

    public String getTransmission();

    public void setTransmission(String transmission);

    public String getVin_sn();

    public void setVin_sn(String vin_sn);

    public String getWebsite_message_ids();

    public void setWebsite_message_ids(String website_message_ids);

    public Timestamp getWrite_date();

    public void setWrite_date(Timestamp write_date);

    public Integer getWrite_uid();

    public void setWrite_uid(Integer write_uid);

    public String getWrite_uid_text();

    public void setWrite_uid_text(String write_uid_text);

    public Timestamp get__last_update();

    public void set__last_update(Timestamp __last_update);

}
