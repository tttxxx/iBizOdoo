package cn.ibizlab.odoo.core.client.model;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigDecimal;
import java.math.BigInteger;

/**
 * Client 实体 [res_config_settings] 对象
 */
public interface Ires_config_settings {

    /**
     * 获取 [银行核销阈值]
     */
    public void setAccount_bank_reconciliation_start(Timestamp account_bank_reconciliation_start);
    
    /**
     * 设置 [银行核销阈值]
     */
    public Timestamp getAccount_bank_reconciliation_start();

    /**
     * 获取 [银行核销阈值]脏标记
     */
    public boolean getAccount_bank_reconciliation_startDirtyFlag();
    /**
     * 获取 [别名域]
     */
    public void setAlias_domain(String alias_domain);
    
    /**
     * 设置 [别名域]
     */
    public String getAlias_domain();

    /**
     * 获取 [别名域]脏标记
     */
    public boolean getAlias_domainDirtyFlag();
    /**
     * 获取 [允许在登录页开启密码重置功能]
     */
    public void setAuth_signup_reset_password(String auth_signup_reset_password);
    
    /**
     * 设置 [允许在登录页开启密码重置功能]
     */
    public String getAuth_signup_reset_password();

    /**
     * 获取 [允许在登录页开启密码重置功能]脏标记
     */
    public boolean getAuth_signup_reset_passwordDirtyFlag();
    /**
     * 获取 [用作通过注册创建的新用户的模版]
     */
    public void setAuth_signup_template_user_id(Integer auth_signup_template_user_id);
    
    /**
     * 设置 [用作通过注册创建的新用户的模版]
     */
    public Integer getAuth_signup_template_user_id();

    /**
     * 获取 [用作通过注册创建的新用户的模版]脏标记
     */
    public boolean getAuth_signup_template_user_idDirtyFlag();
    /**
     * 获取 [用作通过注册创建的新用户的模版]
     */
    public void setAuth_signup_template_user_id_text(String auth_signup_template_user_id_text);
    
    /**
     * 设置 [用作通过注册创建的新用户的模版]
     */
    public String getAuth_signup_template_user_id_text();

    /**
     * 获取 [用作通过注册创建的新用户的模版]脏标记
     */
    public boolean getAuth_signup_template_user_id_textDirtyFlag();
    /**
     * 获取 [顾客账号]
     */
    public void setAuth_signup_uninvited(String auth_signup_uninvited);
    
    /**
     * 设置 [顾客账号]
     */
    public String getAuth_signup_uninvited();

    /**
     * 获取 [顾客账号]脏标记
     */
    public boolean getAuth_signup_uninvitedDirtyFlag();
    /**
     * 获取 [自动开票]
     */
    public void setAutomatic_invoice(String automatic_invoice);
    
    /**
     * 设置 [自动开票]
     */
    public String getAutomatic_invoice();

    /**
     * 获取 [自动开票]脏标记
     */
    public boolean getAutomatic_invoiceDirtyFlag();
    /**
     * 获取 [锁定]
     */
    public void setAuto_done_setting(String auto_done_setting);
    
    /**
     * 设置 [锁定]
     */
    public String getAuto_done_setting();

    /**
     * 获取 [锁定]脏标记
     */
    public boolean getAuto_done_settingDirtyFlag();
    /**
     * 获取 [可用阈值]
     */
    public void setAvailable_threshold(Double available_threshold);
    
    /**
     * 设置 [可用阈值]
     */
    public Double getAvailable_threshold();

    /**
     * 获取 [可用阈值]脏标记
     */
    public boolean getAvailable_thresholdDirtyFlag();
    /**
     * 获取 [放弃时长]
     */
    public void setCart_abandoned_delay(Double cart_abandoned_delay);
    
    /**
     * 设置 [放弃时长]
     */
    public Double getCart_abandoned_delay();

    /**
     * 获取 [放弃时长]脏标记
     */
    public boolean getCart_abandoned_delayDirtyFlag();
    /**
     * 获取 [购物车恢复EMail]
     */
    public void setCart_recovery_mail_template(Integer cart_recovery_mail_template);
    
    /**
     * 设置 [购物车恢复EMail]
     */
    public Integer getCart_recovery_mail_template();

    /**
     * 获取 [购物车恢复EMail]脏标记
     */
    public boolean getCart_recovery_mail_templateDirtyFlag();
    /**
     * 获取 [内容发布网络 (CDN)]
     */
    public void setCdn_activated(String cdn_activated);
    
    /**
     * 设置 [内容发布网络 (CDN)]
     */
    public String getCdn_activated();

    /**
     * 获取 [内容发布网络 (CDN)]脏标记
     */
    public boolean getCdn_activatedDirtyFlag();
    /**
     * 获取 [CDN筛选]
     */
    public void setCdn_filters(String cdn_filters);
    
    /**
     * 设置 [CDN筛选]
     */
    public String getCdn_filters();

    /**
     * 获取 [CDN筛选]脏标记
     */
    public boolean getCdn_filtersDirtyFlag();
    /**
     * 获取 [CDN基本网址]
     */
    public void setCdn_url(String cdn_url);
    
    /**
     * 设置 [CDN基本网址]
     */
    public String getCdn_url();

    /**
     * 获取 [CDN基本网址]脏标记
     */
    public boolean getCdn_urlDirtyFlag();
    /**
     * 获取 [网站直播频道]
     */
    public void setChannel_id(Integer channel_id);
    
    /**
     * 设置 [网站直播频道]
     */
    public Integer getChannel_id();

    /**
     * 获取 [网站直播频道]脏标记
     */
    public boolean getChannel_idDirtyFlag();
    /**
     * 获取 [模板]
     */
    public void setChart_template_id(Integer chart_template_id);
    
    /**
     * 设置 [模板]
     */
    public Integer getChart_template_id();

    /**
     * 获取 [模板]脏标记
     */
    public boolean getChart_template_idDirtyFlag();
    /**
     * 获取 [模板]
     */
    public void setChart_template_id_text(String chart_template_id_text);
    
    /**
     * 设置 [模板]
     */
    public String getChart_template_id_text();

    /**
     * 获取 [模板]脏标记
     */
    public boolean getChart_template_id_textDirtyFlag();
    /**
     * 获取 [公司货币]
     */
    public void setCompany_currency_id(Integer company_currency_id);
    
    /**
     * 设置 [公司货币]
     */
    public Integer getCompany_currency_id();

    /**
     * 获取 [公司货币]脏标记
     */
    public boolean getCompany_currency_idDirtyFlag();
    /**
     * 获取 [公司]
     */
    public void setCompany_id(Integer company_id);
    
    /**
     * 设置 [公司]
     */
    public Integer getCompany_id();

    /**
     * 获取 [公司]脏标记
     */
    public boolean getCompany_idDirtyFlag();
    /**
     * 获取 [公司]
     */
    public void setCompany_id_text(String company_id_text);
    
    /**
     * 设置 [公司]
     */
    public String getCompany_id_text();

    /**
     * 获取 [公司]脏标记
     */
    public boolean getCompany_id_textDirtyFlag();
    /**
     * 获取 [多公司间所有公司的公共用户]
     */
    public void setCompany_share_partner(String company_share_partner);
    
    /**
     * 设置 [多公司间所有公司的公共用户]
     */
    public String getCompany_share_partner();

    /**
     * 获取 [多公司间所有公司的公共用户]脏标记
     */
    public boolean getCompany_share_partnerDirtyFlag();
    /**
     * 获取 [分享产品 给所有公司]
     */
    public void setCompany_share_product(String company_share_product);
    
    /**
     * 设置 [分享产品 给所有公司]
     */
    public String getCompany_share_product();

    /**
     * 获取 [分享产品 给所有公司]脏标记
     */
    public boolean getCompany_share_productDirtyFlag();
    /**
     * 获取 [创建时间]
     */
    public void setCreate_date(Timestamp create_date);
    
    /**
     * 设置 [创建时间]
     */
    public Timestamp getCreate_date();

    /**
     * 获取 [创建时间]脏标记
     */
    public boolean getCreate_dateDirtyFlag();
    /**
     * 获取 [创建人]
     */
    public void setCreate_uid(Integer create_uid);
    
    /**
     * 设置 [创建人]
     */
    public Integer getCreate_uid();

    /**
     * 获取 [创建人]脏标记
     */
    public boolean getCreate_uidDirtyFlag();
    /**
     * 获取 [创建人]
     */
    public void setCreate_uid_text(String create_uid_text);
    
    /**
     * 设置 [创建人]
     */
    public String getCreate_uid_text();

    /**
     * 获取 [创建人]脏标记
     */
    public boolean getCreate_uid_textDirtyFlag();
    /**
     * 获取 [默认线索别名]
     */
    public void setCrm_alias_prefix(String crm_alias_prefix);
    
    /**
     * 设置 [默认线索别名]
     */
    public String getCrm_alias_prefix();

    /**
     * 获取 [默认线索别名]脏标记
     */
    public boolean getCrm_alias_prefixDirtyFlag();
    /**
     * 获取 [默认销售团队]
     */
    public void setCrm_default_team_id(Integer crm_default_team_id);
    
    /**
     * 设置 [默认销售团队]
     */
    public Integer getCrm_default_team_id();

    /**
     * 获取 [默认销售团队]脏标记
     */
    public boolean getCrm_default_team_idDirtyFlag();
    /**
     * 获取 [默认销售人员]
     */
    public void setCrm_default_user_id(Integer crm_default_user_id);
    
    /**
     * 设置 [默认销售人员]
     */
    public Integer getCrm_default_user_id();

    /**
     * 获取 [默认销售人员]脏标记
     */
    public boolean getCrm_default_user_idDirtyFlag();
    /**
     * 获取 [汇兑损益]
     */
    public void setCurrency_exchange_journal_id(Integer currency_exchange_journal_id);
    
    /**
     * 设置 [汇兑损益]
     */
    public Integer getCurrency_exchange_journal_id();

    /**
     * 获取 [汇兑损益]脏标记
     */
    public boolean getCurrency_exchange_journal_idDirtyFlag();
    /**
     * 获取 [币种]
     */
    public void setCurrency_id(Integer currency_id);
    
    /**
     * 设置 [币种]
     */
    public Integer getCurrency_id();

    /**
     * 获取 [币种]脏标记
     */
    public boolean getCurrency_idDirtyFlag();
    /**
     * 获取 [开票策略]
     */
    public void setDefault_invoice_policy(String default_invoice_policy);
    
    /**
     * 设置 [开票策略]
     */
    public String getDefault_invoice_policy();

    /**
     * 获取 [开票策略]脏标记
     */
    public boolean getDefault_invoice_policyDirtyFlag();
    /**
     * 获取 [拣货策略]
     */
    public void setDefault_picking_policy(String default_picking_policy);
    
    /**
     * 设置 [拣货策略]
     */
    public String getDefault_picking_policy();

    /**
     * 获取 [拣货策略]脏标记
     */
    public boolean getDefault_picking_policyDirtyFlag();
    /**
     * 获取 [账单控制]
     */
    public void setDefault_purchase_method(String default_purchase_method);
    
    /**
     * 设置 [账单控制]
     */
    public String getDefault_purchase_method();

    /**
     * 获取 [账单控制]脏标记
     */
    public boolean getDefault_purchase_methodDirtyFlag();
    /**
     * 获取 [默认模板]
     */
    public void setDefault_sale_order_template_id(Integer default_sale_order_template_id);
    
    /**
     * 设置 [默认模板]
     */
    public Integer getDefault_sale_order_template_id();

    /**
     * 获取 [默认模板]脏标记
     */
    public boolean getDefault_sale_order_template_idDirtyFlag();
    /**
     * 获取 [默认模板]
     */
    public void setDefault_sale_order_template_id_text(String default_sale_order_template_id_text);
    
    /**
     * 设置 [默认模板]
     */
    public String getDefault_sale_order_template_id_text();

    /**
     * 获取 [默认模板]脏标记
     */
    public boolean getDefault_sale_order_template_id_textDirtyFlag();
    /**
     * 获取 [押金产品]
     */
    public void setDeposit_default_product_id(Integer deposit_default_product_id);
    
    /**
     * 设置 [押金产品]
     */
    public Integer getDeposit_default_product_id();

    /**
     * 获取 [押金产品]脏标记
     */
    public boolean getDeposit_default_product_idDirtyFlag();
    /**
     * 获取 [押金产品]
     */
    public void setDeposit_default_product_id_text(String deposit_default_product_id_text);
    
    /**
     * 设置 [押金产品]
     */
    public String getDeposit_default_product_id_text();

    /**
     * 获取 [押金产品]脏标记
     */
    public boolean getDeposit_default_product_id_textDirtyFlag();
    /**
     * 获取 [摘要邮件]
     */
    public void setDigest_emails(String digest_emails);
    
    /**
     * 设置 [摘要邮件]
     */
    public String getDigest_emails();

    /**
     * 获取 [摘要邮件]脏标记
     */
    public boolean getDigest_emailsDirtyFlag();
    /**
     * 获取 [摘要邮件]
     */
    public void setDigest_id(Integer digest_id);
    
    /**
     * 设置 [摘要邮件]
     */
    public Integer getDigest_id();

    /**
     * 获取 [摘要邮件]脏标记
     */
    public boolean getDigest_idDirtyFlag();
    /**
     * 获取 [摘要邮件]
     */
    public void setDigest_id_text(String digest_id_text);
    
    /**
     * 设置 [摘要邮件]
     */
    public String getDigest_id_text();

    /**
     * 获取 [摘要邮件]脏标记
     */
    public boolean getDigest_id_textDirtyFlag();
    /**
     * 获取 [显示名称]
     */
    public void setDisplay_name(String display_name);
    
    /**
     * 设置 [显示名称]
     */
    public String getDisplay_name();

    /**
     * 获取 [显示名称]脏标记
     */
    public boolean getDisplay_nameDirtyFlag();
    /**
     * 获取 [默认的费用别名]
     */
    public void setExpense_alias_prefix(String expense_alias_prefix);
    
    /**
     * 设置 [默认的费用别名]
     */
    public String getExpense_alias_prefix();

    /**
     * 获取 [默认的费用别名]脏标记
     */
    public boolean getExpense_alias_prefixDirtyFlag();
    /**
     * 获取 [外部邮件服务器]
     */
    public void setExternal_email_server_default(String external_email_server_default);
    
    /**
     * 设置 [外部邮件服务器]
     */
    public String getExternal_email_server_default();

    /**
     * 获取 [外部邮件服务器]脏标记
     */
    public boolean getExternal_email_server_defaultDirtyFlag();
    /**
     * 获取 [文档模板]
     */
    public void setExternal_report_layout_id(Integer external_report_layout_id);
    
    /**
     * 设置 [文档模板]
     */
    public Integer getExternal_report_layout_id();

    /**
     * 获取 [文档模板]脏标记
     */
    public boolean getExternal_report_layout_idDirtyFlag();
    /**
     * 获取 [失败的邮件]
     */
    public void setFail_counter(Integer fail_counter);
    
    /**
     * 设置 [失败的邮件]
     */
    public Integer getFail_counter();

    /**
     * 获取 [失败的邮件]脏标记
     */
    public boolean getFail_counterDirtyFlag();
    /**
     * 获取 [图标]
     */
    public void setFavicon(byte[] favicon);
    
    /**
     * 设置 [图标]
     */
    public byte[] getFavicon();

    /**
     * 获取 [图标]脏标记
     */
    public boolean getFaviconDirtyFlag();
    /**
     * 获取 [手动分配EMail]
     */
    public void setGenerate_lead_from_alias(String generate_lead_from_alias);
    
    /**
     * 设置 [手动分配EMail]
     */
    public String getGenerate_lead_from_alias();

    /**
     * 获取 [手动分配EMail]脏标记
     */
    public boolean getGenerate_lead_from_aliasDirtyFlag();
    /**
     * 获取 [谷歌分析密钥]
     */
    public void setGoogle_analytics_key(String google_analytics_key);
    
    /**
     * 设置 [谷歌分析密钥]
     */
    public String getGoogle_analytics_key();

    /**
     * 获取 [谷歌分析密钥]脏标记
     */
    public boolean getGoogle_analytics_keyDirtyFlag();
    /**
     * 获取 [Google 客户 ID]
     */
    public void setGoogle_management_client_id(String google_management_client_id);
    
    /**
     * 设置 [Google 客户 ID]
     */
    public String getGoogle_management_client_id();

    /**
     * 获取 [Google 客户 ID]脏标记
     */
    public boolean getGoogle_management_client_idDirtyFlag();
    /**
     * 获取 [Google 客户端密钥]
     */
    public void setGoogle_management_client_secret(String google_management_client_secret);
    
    /**
     * 设置 [Google 客户端密钥]
     */
    public String getGoogle_management_client_secret();

    /**
     * 获取 [Google 客户端密钥]脏标记
     */
    public boolean getGoogle_management_client_secretDirtyFlag();
    /**
     * 获取 [Google 地图 API 密钥]
     */
    public void setGoogle_maps_api_key(String google_maps_api_key);
    
    /**
     * 设置 [Google 地图 API 密钥]
     */
    public String getGoogle_maps_api_key();

    /**
     * 获取 [Google 地图 API 密钥]脏标记
     */
    public boolean getGoogle_maps_api_keyDirtyFlag();
    /**
     * 获取 [分析会计]
     */
    public void setGroup_analytic_accounting(String group_analytic_accounting);
    
    /**
     * 设置 [分析会计]
     */
    public String getGroup_analytic_accounting();

    /**
     * 获取 [分析会计]脏标记
     */
    public boolean getGroup_analytic_accountingDirtyFlag();
    /**
     * 获取 [分析标签]
     */
    public void setGroup_analytic_tags(String group_analytic_tags);
    
    /**
     * 设置 [分析标签]
     */
    public String getGroup_analytic_tags();

    /**
     * 获取 [分析标签]脏标记
     */
    public boolean getGroup_analytic_tagsDirtyFlag();
    /**
     * 获取 [员工 PIN]
     */
    public void setGroup_attendance_use_pin(String group_attendance_use_pin);
    
    /**
     * 设置 [员工 PIN]
     */
    public String getGroup_attendance_use_pin();

    /**
     * 获取 [员工 PIN]脏标记
     */
    public boolean getGroup_attendance_use_pinDirtyFlag();
    /**
     * 获取 [现金舍入]
     */
    public void setGroup_cash_rounding(String group_cash_rounding);
    
    /**
     * 设置 [现金舍入]
     */
    public String getGroup_cash_rounding();

    /**
     * 获取 [现金舍入]脏标记
     */
    public boolean getGroup_cash_roundingDirtyFlag();
    /**
     * 获取 [送货地址]
     */
    public void setGroup_delivery_invoice_address(String group_delivery_invoice_address);
    
    /**
     * 设置 [送货地址]
     */
    public String getGroup_delivery_invoice_address();

    /**
     * 获取 [送货地址]脏标记
     */
    public boolean getGroup_delivery_invoice_addressDirtyFlag();
    /**
     * 获取 [折扣]
     */
    public void setGroup_discount_per_so_line(String group_discount_per_so_line);
    
    /**
     * 设置 [折扣]
     */
    public String getGroup_discount_per_so_line();

    /**
     * 获取 [折扣]脏标记
     */
    public boolean getGroup_discount_per_so_lineDirtyFlag();
    /**
     * 获取 [贸易条款]
     */
    public void setGroup_display_incoterm(String group_display_incoterm);
    
    /**
     * 设置 [贸易条款]
     */
    public String getGroup_display_incoterm();

    /**
     * 获取 [贸易条款]脏标记
     */
    public boolean getGroup_display_incotermDirtyFlag();
    /**
     * 获取 [财年]
     */
    public void setGroup_fiscal_year(String group_fiscal_year);
    
    /**
     * 设置 [财年]
     */
    public String getGroup_fiscal_year();

    /**
     * 获取 [财年]脏标记
     */
    public boolean getGroup_fiscal_yearDirtyFlag();
    /**
     * 获取 [显示批次 / 序列号]
     */
    public void setGroup_lot_on_delivery_slip(String group_lot_on_delivery_slip);
    
    /**
     * 设置 [显示批次 / 序列号]
     */
    public String getGroup_lot_on_delivery_slip();

    /**
     * 获取 [显示批次 / 序列号]脏标记
     */
    public boolean getGroup_lot_on_delivery_slipDirtyFlag();
    /**
     * 获取 [供应商价格表]
     */
    public void setGroup_manage_vendor_price(String group_manage_vendor_price);
    
    /**
     * 设置 [供应商价格表]
     */
    public String getGroup_manage_vendor_price();

    /**
     * 获取 [供应商价格表]脏标记
     */
    public boolean getGroup_manage_vendor_priceDirtyFlag();
    /**
     * 获取 [群发邮件营销]
     */
    public void setGroup_mass_mailing_campaign(String group_mass_mailing_campaign);
    
    /**
     * 设置 [群发邮件营销]
     */
    public String getGroup_mass_mailing_campaign();

    /**
     * 获取 [群发邮件营销]脏标记
     */
    public boolean getGroup_mass_mailing_campaignDirtyFlag();
    /**
     * 获取 [MRP 工单]
     */
    public void setGroup_mrp_routings(String group_mrp_routings);
    
    /**
     * 设置 [MRP 工单]
     */
    public String getGroup_mrp_routings();

    /**
     * 获取 [MRP 工单]脏标记
     */
    public boolean getGroup_mrp_routingsDirtyFlag();
    /**
     * 获取 [管理多公司]
     */
    public void setGroup_multi_company(String group_multi_company);
    
    /**
     * 设置 [管理多公司]
     */
    public String getGroup_multi_company();

    /**
     * 获取 [管理多公司]脏标记
     */
    public boolean getGroup_multi_companyDirtyFlag();
    /**
     * 获取 [多币种]
     */
    public void setGroup_multi_currency(String group_multi_currency);
    
    /**
     * 设置 [多币种]
     */
    public String getGroup_multi_currency();

    /**
     * 获取 [多币种]脏标记
     */
    public boolean getGroup_multi_currencyDirtyFlag();
    /**
     * 获取 [多网站]
     */
    public void setGroup_multi_website(String group_multi_website);
    
    /**
     * 设置 [多网站]
     */
    public String getGroup_multi_website();

    /**
     * 获取 [多网站]脏标记
     */
    public boolean getGroup_multi_websiteDirtyFlag();
    /**
     * 获取 [给客户显示价目表]
     */
    public void setGroup_pricelist_item(String group_pricelist_item);
    
    /**
     * 设置 [给客户显示价目表]
     */
    public String getGroup_pricelist_item();

    /**
     * 获取 [给客户显示价目表]脏标记
     */
    public boolean getGroup_pricelist_itemDirtyFlag();
    /**
     * 获取 [使用供应商单据里的产品]
     */
    public void setGroup_products_in_bills(String group_products_in_bills);
    
    /**
     * 设置 [使用供应商单据里的产品]
     */
    public String getGroup_products_in_bills();

    /**
     * 获取 [使用供应商单据里的产品]脏标记
     */
    public boolean getGroup_products_in_billsDirtyFlag();
    /**
     * 获取 [显示产品的价目表]
     */
    public void setGroup_product_pricelist(String group_product_pricelist);
    
    /**
     * 设置 [显示产品的价目表]
     */
    public String getGroup_product_pricelist();

    /**
     * 获取 [显示产品的价目表]脏标记
     */
    public boolean getGroup_product_pricelistDirtyFlag();
    /**
     * 获取 [变体和选项]
     */
    public void setGroup_product_variant(String group_product_variant);
    
    /**
     * 设置 [变体和选项]
     */
    public String getGroup_product_variant();

    /**
     * 获取 [变体和选项]脏标记
     */
    public boolean getGroup_product_variantDirtyFlag();
    /**
     * 获取 [形式发票]
     */
    public void setGroup_proforma_sales(String group_proforma_sales);
    
    /**
     * 设置 [形式发票]
     */
    public String getGroup_proforma_sales();

    /**
     * 获取 [形式发票]脏标记
     */
    public boolean getGroup_proforma_salesDirtyFlag();
    /**
     * 获取 [使用项目评级]
     */
    public void setGroup_project_rating(String group_project_rating);
    
    /**
     * 设置 [使用项目评级]
     */
    public String getGroup_project_rating();

    /**
     * 获取 [使用项目评级]脏标记
     */
    public boolean getGroup_project_ratingDirtyFlag();
    /**
     * 获取 [订单特定路线]
     */
    public void setGroup_route_so_lines(String group_route_so_lines);
    
    /**
     * 设置 [订单特定路线]
     */
    public String getGroup_route_so_lines();

    /**
     * 获取 [订单特定路线]脏标记
     */
    public boolean getGroup_route_so_linesDirtyFlag();
    /**
     * 获取 [客户地址]
     */
    public void setGroup_sale_delivery_address(String group_sale_delivery_address);
    
    /**
     * 设置 [客户地址]
     */
    public String getGroup_sale_delivery_address();

    /**
     * 获取 [客户地址]脏标记
     */
    public boolean getGroup_sale_delivery_addressDirtyFlag();
    /**
     * 获取 [交货日期]
     */
    public void setGroup_sale_order_dates(String group_sale_order_dates);
    
    /**
     * 设置 [交货日期]
     */
    public String getGroup_sale_order_dates();

    /**
     * 获取 [交货日期]脏标记
     */
    public boolean getGroup_sale_order_datesDirtyFlag();
    /**
     * 获取 [报价单模板]
     */
    public void setGroup_sale_order_template(String group_sale_order_template);
    
    /**
     * 设置 [报价单模板]
     */
    public String getGroup_sale_order_template();

    /**
     * 获取 [报价单模板]脏标记
     */
    public boolean getGroup_sale_order_templateDirtyFlag();
    /**
     * 获取 [为每个客户使用价格表来适配您的价格]
     */
    public void setGroup_sale_pricelist(String group_sale_pricelist);
    
    /**
     * 设置 [为每个客户使用价格表来适配您的价格]
     */
    public String getGroup_sale_pricelist();

    /**
     * 获取 [为每个客户使用价格表来适配您的价格]脏标记
     */
    public boolean getGroup_sale_pricelistDirtyFlag();
    /**
     * 获取 [明细行汇总含税(B2B).]
     */
    public void setGroup_show_line_subtotals_tax_excluded(String group_show_line_subtotals_tax_excluded);
    
    /**
     * 设置 [明细行汇总含税(B2B).]
     */
    public String getGroup_show_line_subtotals_tax_excluded();

    /**
     * 获取 [明细行汇总含税(B2B).]脏标记
     */
    public boolean getGroup_show_line_subtotals_tax_excludedDirtyFlag();
    /**
     * 获取 [显示含税明细行在汇总表(B2B).]
     */
    public void setGroup_show_line_subtotals_tax_included(String group_show_line_subtotals_tax_included);
    
    /**
     * 设置 [显示含税明细行在汇总表(B2B).]
     */
    public String getGroup_show_line_subtotals_tax_included();

    /**
     * 获取 [显示含税明细行在汇总表(B2B).]脏标记
     */
    public boolean getGroup_show_line_subtotals_tax_includedDirtyFlag();
    /**
     * 获取 [多步路由]
     */
    public void setGroup_stock_adv_location(String group_stock_adv_location);
    
    /**
     * 设置 [多步路由]
     */
    public String getGroup_stock_adv_location();

    /**
     * 获取 [多步路由]脏标记
     */
    public boolean getGroup_stock_adv_locationDirtyFlag();
    /**
     * 获取 [储存位置]
     */
    public void setGroup_stock_multi_locations(String group_stock_multi_locations);
    
    /**
     * 设置 [储存位置]
     */
    public String getGroup_stock_multi_locations();

    /**
     * 获取 [储存位置]脏标记
     */
    public boolean getGroup_stock_multi_locationsDirtyFlag();
    /**
     * 获取 [多仓库]
     */
    public void setGroup_stock_multi_warehouses(String group_stock_multi_warehouses);
    
    /**
     * 设置 [多仓库]
     */
    public String getGroup_stock_multi_warehouses();

    /**
     * 获取 [多仓库]脏标记
     */
    public boolean getGroup_stock_multi_warehousesDirtyFlag();
    /**
     * 获取 [产品包装]
     */
    public void setGroup_stock_packaging(String group_stock_packaging);
    
    /**
     * 设置 [产品包装]
     */
    public String getGroup_stock_packaging();

    /**
     * 获取 [产品包装]脏标记
     */
    public boolean getGroup_stock_packagingDirtyFlag();
    /**
     * 获取 [批次和序列号]
     */
    public void setGroup_stock_production_lot(String group_stock_production_lot);
    
    /**
     * 设置 [批次和序列号]
     */
    public String getGroup_stock_production_lot();

    /**
     * 获取 [批次和序列号]脏标记
     */
    public boolean getGroup_stock_production_lotDirtyFlag();
    /**
     * 获取 [交货包裹]
     */
    public void setGroup_stock_tracking_lot(String group_stock_tracking_lot);
    
    /**
     * 设置 [交货包裹]
     */
    public String getGroup_stock_tracking_lot();

    /**
     * 获取 [交货包裹]脏标记
     */
    public boolean getGroup_stock_tracking_lotDirtyFlag();
    /**
     * 获取 [寄售]
     */
    public void setGroup_stock_tracking_owner(String group_stock_tracking_owner);
    
    /**
     * 设置 [寄售]
     */
    public String getGroup_stock_tracking_owner();

    /**
     * 获取 [寄售]脏标记
     */
    public boolean getGroup_stock_tracking_ownerDirtyFlag();
    /**
     * 获取 [子任务]
     */
    public void setGroup_subtask_project(String group_subtask_project);
    
    /**
     * 设置 [子任务]
     */
    public String getGroup_subtask_project();

    /**
     * 获取 [子任务]脏标记
     */
    public boolean getGroup_subtask_projectDirtyFlag();
    /**
     * 获取 [计量单位]
     */
    public void setGroup_uom(String group_uom);
    
    /**
     * 设置 [计量单位]
     */
    public String getGroup_uom();

    /**
     * 获取 [计量单位]脏标记
     */
    public boolean getGroup_uomDirtyFlag();
    /**
     * 获取 [线索]
     */
    public void setGroup_use_lead(String group_use_lead);
    
    /**
     * 设置 [线索]
     */
    public String getGroup_use_lead();

    /**
     * 获取 [线索]脏标记
     */
    public boolean getGroup_use_leadDirtyFlag();
    /**
     * 获取 [发票警告]
     */
    public void setGroup_warning_account(String group_warning_account);
    
    /**
     * 设置 [发票警告]
     */
    public String getGroup_warning_account();

    /**
     * 获取 [发票警告]脏标记
     */
    public boolean getGroup_warning_accountDirtyFlag();
    /**
     * 获取 [采购警告]
     */
    public void setGroup_warning_purchase(String group_warning_purchase);
    
    /**
     * 设置 [采购警告]
     */
    public String getGroup_warning_purchase();

    /**
     * 获取 [采购警告]脏标记
     */
    public boolean getGroup_warning_purchaseDirtyFlag();
    /**
     * 获取 [销售订单警告]
     */
    public void setGroup_warning_sale(String group_warning_sale);
    
    /**
     * 设置 [销售订单警告]
     */
    public String getGroup_warning_sale();

    /**
     * 获取 [销售订单警告]脏标记
     */
    public boolean getGroup_warning_saleDirtyFlag();
    /**
     * 获取 [库存警报]
     */
    public void setGroup_warning_stock(String group_warning_stock);
    
    /**
     * 设置 [库存警报]
     */
    public String getGroup_warning_stock();

    /**
     * 获取 [库存警报]脏标记
     */
    public boolean getGroup_warning_stockDirtyFlag();
    /**
     * 获取 [网站弹出窗口]
     */
    public void setGroup_website_popup_on_exit(String group_website_popup_on_exit);
    
    /**
     * 设置 [网站弹出窗口]
     */
    public String getGroup_website_popup_on_exit();

    /**
     * 获取 [网站弹出窗口]脏标记
     */
    public boolean getGroup_website_popup_on_exitDirtyFlag();
    /**
     * 获取 [有会计分录]
     */
    public void setHas_accounting_entries(String has_accounting_entries);
    
    /**
     * 设置 [有会计分录]
     */
    public String getHas_accounting_entries();

    /**
     * 获取 [有会计分录]脏标记
     */
    public boolean getHas_accounting_entriesDirtyFlag();
    /**
     * 获取 [公司有科目表]
     */
    public void setHas_chart_of_accounts(String has_chart_of_accounts);
    
    /**
     * 设置 [公司有科目表]
     */
    public String getHas_chart_of_accounts();

    /**
     * 获取 [公司有科目表]脏标记
     */
    public boolean getHas_chart_of_accountsDirtyFlag();
    /**
     * 获取 [Google 分析]
     */
    public void setHas_google_analytics(String has_google_analytics);
    
    /**
     * 设置 [Google 分析]
     */
    public String getHas_google_analytics();

    /**
     * 获取 [Google 分析]脏标记
     */
    public boolean getHas_google_analyticsDirtyFlag();
    /**
     * 获取 [谷歌分析仪表板]
     */
    public void setHas_google_analytics_dashboard(String has_google_analytics_dashboard);
    
    /**
     * 设置 [谷歌分析仪表板]
     */
    public String getHas_google_analytics_dashboard();

    /**
     * 获取 [谷歌分析仪表板]脏标记
     */
    public boolean getHas_google_analytics_dashboardDirtyFlag();
    /**
     * 获取 [Google 地图]
     */
    public void setHas_google_maps(String has_google_maps);
    
    /**
     * 设置 [Google 地图]
     */
    public String getHas_google_maps();

    /**
     * 获取 [Google 地图]脏标记
     */
    public boolean getHas_google_mapsDirtyFlag();
    /**
     * 获取 [设置社交平台]
     */
    public void setHas_social_network(String has_social_network);
    
    /**
     * 设置 [设置社交平台]
     */
    public String getHas_social_network();

    /**
     * 获取 [设置社交平台]脏标记
     */
    public boolean getHas_social_networkDirtyFlag();
    /**
     * 获取 [ID]
     */
    public void setId(Integer id);
    
    /**
     * 设置 [ID]
     */
    public Integer getId();

    /**
     * 获取 [ID]脏标记
     */
    public boolean getIdDirtyFlag();
    /**
     * 获取 [库存可用性]
     */
    public void setInventory_availability(String inventory_availability);
    
    /**
     * 设置 [库存可用性]
     */
    public String getInventory_availability();

    /**
     * 获取 [库存可用性]脏标记
     */
    public boolean getInventory_availabilityDirtyFlag();
    /**
     * 获取 [发送EMail]
     */
    public void setInvoice_is_email(String invoice_is_email);
    
    /**
     * 设置 [发送EMail]
     */
    public String getInvoice_is_email();

    /**
     * 获取 [发送EMail]脏标记
     */
    public boolean getInvoice_is_emailDirtyFlag();
    /**
     * 获取 [打印]
     */
    public void setInvoice_is_print(String invoice_is_print);
    
    /**
     * 设置 [打印]
     */
    public String getInvoice_is_print();

    /**
     * 获取 [打印]脏标记
     */
    public boolean getInvoice_is_printDirtyFlag();
    /**
     * 获取 [透过邮递]
     */
    public void setInvoice_is_snailmail(String invoice_is_snailmail);
    
    /**
     * 设置 [透过邮递]
     */
    public String getInvoice_is_snailmail();

    /**
     * 获取 [透过邮递]脏标记
     */
    public boolean getInvoice_is_snailmailDirtyFlag();
    /**
     * 获取 [交流]
     */
    public void setInvoice_reference_type(String invoice_reference_type);
    
    /**
     * 设置 [交流]
     */
    public String getInvoice_reference_type();

    /**
     * 获取 [交流]脏标记
     */
    public boolean getInvoice_reference_typeDirtyFlag();
    /**
     * 获取 [销售模块是否已安装]
     */
    public void setIs_installed_sale(String is_installed_sale);
    
    /**
     * 设置 [销售模块是否已安装]
     */
    public String getIs_installed_sale();

    /**
     * 获取 [销售模块是否已安装]脏标记
     */
    public boolean getIs_installed_saleDirtyFlag();
    /**
     * 获取 [语言数量]
     */
    public void setLanguage_count(Integer language_count);
    
    /**
     * 设置 [语言数量]
     */
    public Integer getLanguage_count();

    /**
     * 获取 [语言数量]脏标记
     */
    public boolean getLanguage_countDirtyFlag();
    /**
     * 获取 [语言]
     */
    public void setLanguage_ids(String language_ids);
    
    /**
     * 设置 [语言]
     */
    public String getLanguage_ids();

    /**
     * 获取 [语言]脏标记
     */
    public boolean getLanguage_idsDirtyFlag();
    /**
     * 获取 [锁定确认订单]
     */
    public void setLock_confirmed_po(String lock_confirmed_po);
    
    /**
     * 设置 [锁定确认订单]
     */
    public String getLock_confirmed_po();

    /**
     * 获取 [锁定确认订单]脏标记
     */
    public boolean getLock_confirmed_poDirtyFlag();
    /**
     * 获取 [制造提前期(日)]
     */
    public void setManufacturing_lead(Double manufacturing_lead);
    
    /**
     * 设置 [制造提前期(日)]
     */
    public Double getManufacturing_lead();

    /**
     * 获取 [制造提前期(日)]脏标记
     */
    public boolean getManufacturing_leadDirtyFlag();
    /**
     * 获取 [邮件服务器]
     */
    public void setMass_mailing_mail_server_id(Integer mass_mailing_mail_server_id);
    
    /**
     * 设置 [邮件服务器]
     */
    public Integer getMass_mailing_mail_server_id();

    /**
     * 获取 [邮件服务器]脏标记
     */
    public boolean getMass_mailing_mail_server_idDirtyFlag();
    /**
     * 获取 [指定邮件服务器]
     */
    public void setMass_mailing_outgoing_mail_server(String mass_mailing_outgoing_mail_server);
    
    /**
     * 设置 [指定邮件服务器]
     */
    public String getMass_mailing_outgoing_mail_server();

    /**
     * 获取 [指定邮件服务器]脏标记
     */
    public boolean getMass_mailing_outgoing_mail_serverDirtyFlag();
    /**
     * 获取 [开票]
     */
    public void setModule_account(String module_account);
    
    /**
     * 设置 [开票]
     */
    public String getModule_account();

    /**
     * 获取 [开票]脏标记
     */
    public boolean getModule_accountDirtyFlag();
    /**
     * 获取 [三方匹配:采购，收货和发票]
     */
    public void setModule_account_3way_match(String module_account_3way_match);
    
    /**
     * 设置 [三方匹配:采购，收货和发票]
     */
    public String getModule_account_3way_match();

    /**
     * 获取 [三方匹配:采购，收货和发票]脏标记
     */
    public boolean getModule_account_3way_matchDirtyFlag();
    /**
     * 获取 [Accounting]
     */
    public void setModule_account_accountant(String module_account_accountant);
    
    /**
     * 设置 [Accounting]
     */
    public String getModule_account_accountant();

    /**
     * 获取 [Accounting]脏标记
     */
    public boolean getModule_account_accountantDirtyFlag();
    /**
     * 获取 [资产管理]
     */
    public void setModule_account_asset(String module_account_asset);
    
    /**
     * 设置 [资产管理]
     */
    public String getModule_account_asset();

    /**
     * 获取 [资产管理]脏标记
     */
    public boolean getModule_account_assetDirtyFlag();
    /**
     * 获取 [用 CAMT.053 格式导入]
     */
    public void setModule_account_bank_statement_import_camt(String module_account_bank_statement_import_camt);
    
    /**
     * 设置 [用 CAMT.053 格式导入]
     */
    public String getModule_account_bank_statement_import_camt();

    /**
     * 获取 [用 CAMT.053 格式导入]脏标记
     */
    public boolean getModule_account_bank_statement_import_camtDirtyFlag();
    /**
     * 获取 [以 CSV 格式导入]
     */
    public void setModule_account_bank_statement_import_csv(String module_account_bank_statement_import_csv);
    
    /**
     * 设置 [以 CSV 格式导入]
     */
    public String getModule_account_bank_statement_import_csv();

    /**
     * 获取 [以 CSV 格式导入]脏标记
     */
    public boolean getModule_account_bank_statement_import_csvDirtyFlag();
    /**
     * 获取 [导入.ofx格式]
     */
    public void setModule_account_bank_statement_import_ofx(String module_account_bank_statement_import_ofx);
    
    /**
     * 设置 [导入.ofx格式]
     */
    public String getModule_account_bank_statement_import_ofx();

    /**
     * 获取 [导入.ofx格式]脏标记
     */
    public boolean getModule_account_bank_statement_import_ofxDirtyFlag();
    /**
     * 获取 [导入.qif 文件]
     */
    public void setModule_account_bank_statement_import_qif(String module_account_bank_statement_import_qif);
    
    /**
     * 设置 [导入.qif 文件]
     */
    public String getModule_account_bank_statement_import_qif();

    /**
     * 获取 [导入.qif 文件]脏标记
     */
    public boolean getModule_account_bank_statement_import_qifDirtyFlag();
    /**
     * 获取 [使用批量付款]
     */
    public void setModule_account_batch_payment(String module_account_batch_payment);
    
    /**
     * 设置 [使用批量付款]
     */
    public String getModule_account_batch_payment();

    /**
     * 获取 [使用批量付款]脏标记
     */
    public boolean getModule_account_batch_paymentDirtyFlag();
    /**
     * 获取 [预算管理]
     */
    public void setModule_account_budget(String module_account_budget);
    
    /**
     * 设置 [预算管理]
     */
    public String getModule_account_budget();

    /**
     * 获取 [预算管理]脏标记
     */
    public boolean getModule_account_budgetDirtyFlag();
    /**
     * 获取 [允许支票打印和存款]
     */
    public void setModule_account_check_printing(String module_account_check_printing);
    
    /**
     * 设置 [允许支票打印和存款]
     */
    public String getModule_account_check_printing();

    /**
     * 获取 [允许支票打印和存款]脏标记
     */
    public boolean getModule_account_check_printingDirtyFlag();
    /**
     * 获取 [收入识别]
     */
    public void setModule_account_deferred_revenue(String module_account_deferred_revenue);
    
    /**
     * 设置 [收入识别]
     */
    public String getModule_account_deferred_revenue();

    /**
     * 获取 [收入识别]脏标记
     */
    public boolean getModule_account_deferred_revenueDirtyFlag();
    /**
     * 获取 [国际贸易统计组织]
     */
    public void setModule_account_intrastat(String module_account_intrastat);
    
    /**
     * 设置 [国际贸易统计组织]
     */
    public String getModule_account_intrastat();

    /**
     * 获取 [国际贸易统计组织]脏标记
     */
    public boolean getModule_account_intrastatDirtyFlag();
    /**
     * 获取 [自动票据处理]
     */
    public void setModule_account_invoice_extract(String module_account_invoice_extract);
    
    /**
     * 设置 [自动票据处理]
     */
    public String getModule_account_invoice_extract();

    /**
     * 获取 [自动票据处理]脏标记
     */
    public boolean getModule_account_invoice_extractDirtyFlag();
    /**
     * 获取 [发票在线付款]
     */
    public void setModule_account_payment(String module_account_payment);
    
    /**
     * 设置 [发票在线付款]
     */
    public String getModule_account_payment();

    /**
     * 获取 [发票在线付款]脏标记
     */
    public boolean getModule_account_paymentDirtyFlag();
    /**
     * 获取 [Plaid 接口]
     */
    public void setModule_account_plaid(String module_account_plaid);
    
    /**
     * 设置 [Plaid 接口]
     */
    public String getModule_account_plaid();

    /**
     * 获取 [Plaid 接口]脏标记
     */
    public boolean getModule_account_plaidDirtyFlag();
    /**
     * 获取 [动态报告]
     */
    public void setModule_account_reports(String module_account_reports);
    
    /**
     * 设置 [动态报告]
     */
    public String getModule_account_reports();

    /**
     * 获取 [动态报告]脏标记
     */
    public boolean getModule_account_reportsDirtyFlag();
    /**
     * 获取 [催款等级]
     */
    public void setModule_account_reports_followup(String module_account_reports_followup);
    
    /**
     * 设置 [催款等级]
     */
    public String getModule_account_reports_followup();

    /**
     * 获取 [催款等级]脏标记
     */
    public boolean getModule_account_reports_followupDirtyFlag();
    /**
     * 获取 [SEPA贷记交易]
     */
    public void setModule_account_sepa(String module_account_sepa);
    
    /**
     * 设置 [SEPA贷记交易]
     */
    public String getModule_account_sepa();

    /**
     * 获取 [SEPA贷记交易]脏标记
     */
    public boolean getModule_account_sepaDirtyFlag();
    /**
     * 获取 [使用SEPA直接计入借方]
     */
    public void setModule_account_sepa_direct_debit(String module_account_sepa_direct_debit);
    
    /**
     * 设置 [使用SEPA直接计入借方]
     */
    public String getModule_account_sepa_direct_debit();

    /**
     * 获取 [使用SEPA直接计入借方]脏标记
     */
    public boolean getModule_account_sepa_direct_debitDirtyFlag();
    /**
     * 获取 [科目税]
     */
    public void setModule_account_taxcloud(String module_account_taxcloud);
    
    /**
     * 设置 [科目税]
     */
    public String getModule_account_taxcloud();

    /**
     * 获取 [科目税]脏标记
     */
    public boolean getModule_account_taxcloudDirtyFlag();
    /**
     * 获取 [银行接口－自动同步银行费用]
     */
    public void setModule_account_yodlee(String module_account_yodlee);
    
    /**
     * 设置 [银行接口－自动同步银行费用]
     */
    public String getModule_account_yodlee();

    /**
     * 获取 [银行接口－自动同步银行费用]脏标记
     */
    public boolean getModule_account_yodleeDirtyFlag();
    /**
     * 获取 [LDAP认证]
     */
    public void setModule_auth_ldap(String module_auth_ldap);
    
    /**
     * 设置 [LDAP认证]
     */
    public String getModule_auth_ldap();

    /**
     * 获取 [LDAP认证]脏标记
     */
    public boolean getModule_auth_ldapDirtyFlag();
    /**
     * 获取 [使用外部验证提供者 (OAuth)]
     */
    public void setModule_auth_oauth(String module_auth_oauth);
    
    /**
     * 设置 [使用外部验证提供者 (OAuth)]
     */
    public String getModule_auth_oauth();

    /**
     * 获取 [使用外部验证提供者 (OAuth)]脏标记
     */
    public boolean getModule_auth_oauthDirtyFlag();
    /**
     * 获取 [用Gengo翻译您的网站]
     */
    public void setModule_base_gengo(String module_base_gengo);
    
    /**
     * 设置 [用Gengo翻译您的网站]
     */
    public String getModule_base_gengo();

    /**
     * 获取 [用Gengo翻译您的网站]脏标记
     */
    public boolean getModule_base_gengoDirtyFlag();
    /**
     * 获取 [允许用户导入 CSV/XLS/XLSX/ODS格式的文档数据]
     */
    public void setModule_base_import(String module_base_import);
    
    /**
     * 设置 [允许用户导入 CSV/XLS/XLSX/ODS格式的文档数据]
     */
    public String getModule_base_import();

    /**
     * 获取 [允许用户导入 CSV/XLS/XLSX/ODS格式的文档数据]脏标记
     */
    public boolean getModule_base_importDirtyFlag();
    /**
     * 获取 [号码格式]
     */
    public void setModule_crm_phone_validation(String module_crm_phone_validation);
    
    /**
     * 设置 [号码格式]
     */
    public String getModule_crm_phone_validation();

    /**
     * 获取 [号码格式]脏标记
     */
    public boolean getModule_crm_phone_validationDirtyFlag();
    /**
     * 获取 [从你的网站流量创建线索/商机]
     */
    public void setModule_crm_reveal(String module_crm_reveal);
    
    /**
     * 设置 [从你的网站流量创建线索/商机]
     */
    public String getModule_crm_reveal();

    /**
     * 获取 [从你的网站流量创建线索/商机]脏标记
     */
    public boolean getModule_crm_revealDirtyFlag();
    /**
     * 获取 [自动汇率]
     */
    public void setModule_currency_rate_live(String module_currency_rate_live);
    
    /**
     * 设置 [自动汇率]
     */
    public String getModule_currency_rate_live();

    /**
     * 获取 [自动汇率]脏标记
     */
    public boolean getModule_currency_rate_liveDirtyFlag();
    /**
     * 获取 [运输成本]
     */
    public void setModule_delivery(String module_delivery);
    
    /**
     * 设置 [运输成本]
     */
    public String getModule_delivery();

    /**
     * 获取 [运输成本]脏标记
     */
    public boolean getModule_deliveryDirtyFlag();
    /**
     * 获取 [bpost 接口]
     */
    public void setModule_delivery_bpost(String module_delivery_bpost);
    
    /**
     * 设置 [bpost 接口]
     */
    public String getModule_delivery_bpost();

    /**
     * 获取 [bpost 接口]脏标记
     */
    public boolean getModule_delivery_bpostDirtyFlag();
    /**
     * 获取 [DHL 接口]
     */
    public void setModule_delivery_dhl(String module_delivery_dhl);
    
    /**
     * 设置 [DHL 接口]
     */
    public String getModule_delivery_dhl();

    /**
     * 获取 [DHL 接口]脏标记
     */
    public boolean getModule_delivery_dhlDirtyFlag();
    /**
     * 获取 [Easypost 接口]
     */
    public void setModule_delivery_easypost(String module_delivery_easypost);
    
    /**
     * 设置 [Easypost 接口]
     */
    public String getModule_delivery_easypost();

    /**
     * 获取 [Easypost 接口]脏标记
     */
    public boolean getModule_delivery_easypostDirtyFlag();
    /**
     * 获取 [FedEx 接口]
     */
    public void setModule_delivery_fedex(String module_delivery_fedex);
    
    /**
     * 设置 [FedEx 接口]
     */
    public String getModule_delivery_fedex();

    /**
     * 获取 [FedEx 接口]脏标记
     */
    public boolean getModule_delivery_fedexDirtyFlag();
    /**
     * 获取 [UPS 接口]
     */
    public void setModule_delivery_ups(String module_delivery_ups);
    
    /**
     * 设置 [UPS 接口]
     */
    public String getModule_delivery_ups();

    /**
     * 获取 [UPS 接口]脏标记
     */
    public boolean getModule_delivery_upsDirtyFlag();
    /**
     * 获取 [USPS 接口]
     */
    public void setModule_delivery_usps(String module_delivery_usps);
    
    /**
     * 设置 [USPS 接口]
     */
    public String getModule_delivery_usps();

    /**
     * 获取 [USPS 接口]脏标记
     */
    public boolean getModule_delivery_uspsDirtyFlag();
    /**
     * 获取 [条码]
     */
    public void setModule_event_barcode(String module_event_barcode);
    
    /**
     * 设置 [条码]
     */
    public String getModule_event_barcode();

    /**
     * 获取 [条码]脏标记
     */
    public boolean getModule_event_barcodeDirtyFlag();
    /**
     * 获取 [入场券]
     */
    public void setModule_event_sale(String module_event_sale);
    
    /**
     * 设置 [入场券]
     */
    public String getModule_event_sale();

    /**
     * 获取 [入场券]脏标记
     */
    public boolean getModule_event_saleDirtyFlag();
    /**
     * 获取 [允许用户同步Google日历]
     */
    public void setModule_google_calendar(String module_google_calendar);
    
    /**
     * 设置 [允许用户同步Google日历]
     */
    public String getModule_google_calendar();

    /**
     * 获取 [允许用户同步Google日历]脏标记
     */
    public boolean getModule_google_calendarDirtyFlag();
    /**
     * 获取 [附加Google文档到记录]
     */
    public void setModule_google_drive(String module_google_drive);
    
    /**
     * 设置 [附加Google文档到记录]
     */
    public String getModule_google_drive();

    /**
     * 获取 [附加Google文档到记录]脏标记
     */
    public boolean getModule_google_driveDirtyFlag();
    /**
     * 获取 [Google 电子表格]
     */
    public void setModule_google_spreadsheet(String module_google_spreadsheet);
    
    /**
     * 设置 [Google 电子表格]
     */
    public String getModule_google_spreadsheet();

    /**
     * 获取 [Google 电子表格]脏标记
     */
    public boolean getModule_google_spreadsheetDirtyFlag();
    /**
     * 获取 [显示组织架构图]
     */
    public void setModule_hr_org_chart(String module_hr_org_chart);
    
    /**
     * 设置 [显示组织架构图]
     */
    public String getModule_hr_org_chart();

    /**
     * 获取 [显示组织架构图]脏标记
     */
    public boolean getModule_hr_org_chartDirtyFlag();
    /**
     * 获取 [面试表单]
     */
    public void setModule_hr_recruitment_survey(String module_hr_recruitment_survey);
    
    /**
     * 设置 [面试表单]
     */
    public String getModule_hr_recruitment_survey();

    /**
     * 获取 [面试表单]脏标记
     */
    public boolean getModule_hr_recruitment_surveyDirtyFlag();
    /**
     * 获取 [任务日志]
     */
    public void setModule_hr_timesheet(String module_hr_timesheet);
    
    /**
     * 设置 [任务日志]
     */
    public String getModule_hr_timesheet();

    /**
     * 获取 [任务日志]脏标记
     */
    public boolean getModule_hr_timesheetDirtyFlag();
    /**
     * 获取 [管理公司间交易]
     */
    public void setModule_inter_company_rules(String module_inter_company_rules);
    
    /**
     * 设置 [管理公司间交易]
     */
    public String getModule_inter_company_rules();

    /**
     * 获取 [管理公司间交易]脏标记
     */
    public boolean getModule_inter_company_rulesDirtyFlag();
    /**
     * 获取 [欧盟数字商品增值税]
     */
    public void setModule_l10n_eu_service(String module_l10n_eu_service);
    
    /**
     * 设置 [欧盟数字商品增值税]
     */
    public String getModule_l10n_eu_service();

    /**
     * 获取 [欧盟数字商品增值税]脏标记
     */
    public boolean getModule_l10n_eu_serviceDirtyFlag();
    /**
     * 获取 [副产品]
     */
    public void setModule_mrp_byproduct(String module_mrp_byproduct);
    
    /**
     * 设置 [副产品]
     */
    public String getModule_mrp_byproduct();

    /**
     * 获取 [副产品]脏标记
     */
    public boolean getModule_mrp_byproductDirtyFlag();
    /**
     * 获取 [主生产排程]
     */
    public void setModule_mrp_mps(String module_mrp_mps);
    
    /**
     * 设置 [主生产排程]
     */
    public String getModule_mrp_mps();

    /**
     * 获取 [主生产排程]脏标记
     */
    public boolean getModule_mrp_mpsDirtyFlag();
    /**
     * 获取 [产品生命周期管理 (PLM)]
     */
    public void setModule_mrp_plm(String module_mrp_plm);
    
    /**
     * 设置 [产品生命周期管理 (PLM)]
     */
    public String getModule_mrp_plm();

    /**
     * 获取 [产品生命周期管理 (PLM)]脏标记
     */
    public boolean getModule_mrp_plmDirtyFlag();
    /**
     * 获取 [工单]
     */
    public void setModule_mrp_workorder(String module_mrp_workorder);
    
    /**
     * 设置 [工单]
     */
    public String getModule_mrp_workorder();

    /**
     * 获取 [工单]脏标记
     */
    public boolean getModule_mrp_workorderDirtyFlag();
    /**
     * 获取 [协作pad]
     */
    public void setModule_pad(String module_pad);
    
    /**
     * 设置 [协作pad]
     */
    public String getModule_pad();

    /**
     * 获取 [协作pad]脏标记
     */
    public boolean getModule_padDirtyFlag();
    /**
     * 获取 [自动填充公司数据]
     */
    public void setModule_partner_autocomplete(String module_partner_autocomplete);
    
    /**
     * 设置 [自动填充公司数据]
     */
    public String getModule_partner_autocomplete();

    /**
     * 获取 [自动填充公司数据]脏标记
     */
    public boolean getModule_partner_autocompleteDirtyFlag();
    /**
     * 获取 [集成卡支付]
     */
    public void setModule_pos_mercury(String module_pos_mercury);
    
    /**
     * 设置 [集成卡支付]
     */
    public String getModule_pos_mercury();

    /**
     * 获取 [集成卡支付]脏标记
     */
    public boolean getModule_pos_mercuryDirtyFlag();
    /**
     * 获取 [保留]
     */
    public void setModule_procurement_jit(String module_procurement_jit);
    
    /**
     * 设置 [保留]
     */
    public String getModule_procurement_jit();

    /**
     * 获取 [保留]脏标记
     */
    public boolean getModule_procurement_jitDirtyFlag();
    /**
     * 获取 [特定的EMail]
     */
    public void setModule_product_email_template(String module_product_email_template);
    
    /**
     * 设置 [特定的EMail]
     */
    public String getModule_product_email_template();

    /**
     * 获取 [特定的EMail]脏标记
     */
    public boolean getModule_product_email_templateDirtyFlag();
    /**
     * 获取 [到期日]
     */
    public void setModule_product_expiry(String module_product_expiry);
    
    /**
     * 设置 [到期日]
     */
    public String getModule_product_expiry();

    /**
     * 获取 [到期日]脏标记
     */
    public boolean getModule_product_expiryDirtyFlag();
    /**
     * 获取 [允许产品毛利]
     */
    public void setModule_product_margin(String module_product_margin);
    
    /**
     * 设置 [允许产品毛利]
     */
    public String getModule_product_margin();

    /**
     * 获取 [允许产品毛利]脏标记
     */
    public boolean getModule_product_marginDirtyFlag();
    /**
     * 获取 [预测]
     */
    public void setModule_project_forecast(String module_project_forecast);
    
    /**
     * 设置 [预测]
     */
    public String getModule_project_forecast();

    /**
     * 获取 [预测]脏标记
     */
    public boolean getModule_project_forecastDirtyFlag();
    /**
     * 获取 [采购招标]
     */
    public void setModule_purchase_requisition(String module_purchase_requisition);
    
    /**
     * 设置 [采购招标]
     */
    public String getModule_purchase_requisition();

    /**
     * 获取 [采购招标]脏标记
     */
    public boolean getModule_purchase_requisitionDirtyFlag();
    /**
     * 获取 [质量]
     */
    public void setModule_quality_control(String module_quality_control);
    
    /**
     * 设置 [质量]
     */
    public String getModule_quality_control();

    /**
     * 获取 [质量]脏标记
     */
    public boolean getModule_quality_controlDirtyFlag();
    /**
     * 获取 [优惠券和促销]
     */
    public void setModule_sale_coupon(String module_sale_coupon);
    
    /**
     * 设置 [优惠券和促销]
     */
    public String getModule_sale_coupon();

    /**
     * 获取 [优惠券和促销]脏标记
     */
    public boolean getModule_sale_couponDirtyFlag();
    /**
     * 获取 [毛利]
     */
    public void setModule_sale_margin(String module_sale_margin);
    
    /**
     * 设置 [毛利]
     */
    public String getModule_sale_margin();

    /**
     * 获取 [毛利]脏标记
     */
    public boolean getModule_sale_marginDirtyFlag();
    /**
     * 获取 [报价单生成器]
     */
    public void setModule_sale_quotation_builder(String module_sale_quotation_builder);
    
    /**
     * 设置 [报价单生成器]
     */
    public String getModule_sale_quotation_builder();

    /**
     * 获取 [报价单生成器]脏标记
     */
    public boolean getModule_sale_quotation_builderDirtyFlag();
    /**
     * 获取 [条码扫描器]
     */
    public void setModule_stock_barcode(String module_stock_barcode);
    
    /**
     * 设置 [条码扫描器]
     */
    public String getModule_stock_barcode();

    /**
     * 获取 [条码扫描器]脏标记
     */
    public boolean getModule_stock_barcodeDirtyFlag();
    /**
     * 获取 [代发货]
     */
    public void setModule_stock_dropshipping(String module_stock_dropshipping);
    
    /**
     * 设置 [代发货]
     */
    public String getModule_stock_dropshipping();

    /**
     * 获取 [代发货]脏标记
     */
    public boolean getModule_stock_dropshippingDirtyFlag();
    /**
     * 获取 [到岸成本]
     */
    public void setModule_stock_landed_costs(String module_stock_landed_costs);
    
    /**
     * 设置 [到岸成本]
     */
    public String getModule_stock_landed_costs();

    /**
     * 获取 [到岸成本]脏标记
     */
    public boolean getModule_stock_landed_costsDirtyFlag();
    /**
     * 获取 [批量拣货]
     */
    public void setModule_stock_picking_batch(String module_stock_picking_batch);
    
    /**
     * 设置 [批量拣货]
     */
    public String getModule_stock_picking_batch();

    /**
     * 获取 [批量拣货]脏标记
     */
    public boolean getModule_stock_picking_batchDirtyFlag();
    /**
     * 获取 [Asterisk (开源VoIP平台)]
     */
    public void setModule_voip(String module_voip);
    
    /**
     * 设置 [Asterisk (开源VoIP平台)]
     */
    public String getModule_voip();

    /**
     * 获取 [Asterisk (开源VoIP平台)]脏标记
     */
    public boolean getModule_voipDirtyFlag();
    /**
     * 获取 [调查登记]
     */
    public void setModule_website_event_questions(String module_website_event_questions);
    
    /**
     * 设置 [调查登记]
     */
    public String getModule_website_event_questions();

    /**
     * 获取 [调查登记]脏标记
     */
    public boolean getModule_website_event_questionsDirtyFlag();
    /**
     * 获取 [在线票务]
     */
    public void setModule_website_event_sale(String module_website_event_sale);
    
    /**
     * 设置 [在线票务]
     */
    public String getModule_website_event_sale();

    /**
     * 获取 [在线票务]脏标记
     */
    public boolean getModule_website_event_saleDirtyFlag();
    /**
     * 获取 [耿宗并计划]
     */
    public void setModule_website_event_track(String module_website_event_track);
    
    /**
     * 设置 [耿宗并计划]
     */
    public String getModule_website_event_track();

    /**
     * 获取 [耿宗并计划]脏标记
     */
    public boolean getModule_website_event_trackDirtyFlag();
    /**
     * 获取 [在线发布]
     */
    public void setModule_website_hr_recruitment(String module_website_hr_recruitment);
    
    /**
     * 设置 [在线发布]
     */
    public String getModule_website_hr_recruitment();

    /**
     * 获取 [在线发布]脏标记
     */
    public boolean getModule_website_hr_recruitmentDirtyFlag();
    /**
     * 获取 [链接跟踪器]
     */
    public void setModule_website_links(String module_website_links);
    
    /**
     * 设置 [链接跟踪器]
     */
    public String getModule_website_links();

    /**
     * 获取 [链接跟踪器]脏标记
     */
    public boolean getModule_website_linksDirtyFlag();
    /**
     * 获取 [产品比较工具]
     */
    public void setModule_website_sale_comparison(String module_website_sale_comparison);
    
    /**
     * 设置 [产品比较工具]
     */
    public String getModule_website_sale_comparison();

    /**
     * 获取 [产品比较工具]脏标记
     */
    public boolean getModule_website_sale_comparisonDirtyFlag();
    /**
     * 获取 [电商物流成本]
     */
    public void setModule_website_sale_delivery(String module_website_sale_delivery);
    
    /**
     * 设置 [电商物流成本]
     */
    public String getModule_website_sale_delivery();

    /**
     * 获取 [电商物流成本]脏标记
     */
    public boolean getModule_website_sale_deliveryDirtyFlag();
    /**
     * 获取 [数字内容]
     */
    public void setModule_website_sale_digital(String module_website_sale_digital);
    
    /**
     * 设置 [数字内容]
     */
    public String getModule_website_sale_digital();

    /**
     * 获取 [数字内容]脏标记
     */
    public boolean getModule_website_sale_digitalDirtyFlag();
    /**
     * 获取 [库存]
     */
    public void setModule_website_sale_stock(String module_website_sale_stock);
    
    /**
     * 设置 [库存]
     */
    public String getModule_website_sale_stock();

    /**
     * 获取 [库存]脏标记
     */
    public boolean getModule_website_sale_stockDirtyFlag();
    /**
     * 获取 [心愿单]
     */
    public void setModule_website_sale_wishlist(String module_website_sale_wishlist);
    
    /**
     * 设置 [心愿单]
     */
    public String getModule_website_sale_wishlist();

    /**
     * 获取 [心愿单]脏标记
     */
    public boolean getModule_website_sale_wishlistDirtyFlag();
    /**
     * 获取 [A / B测试]
     */
    public void setModule_website_version(String module_website_version);
    
    /**
     * 设置 [A / B测试]
     */
    public String getModule_website_version();

    /**
     * 获取 [A / B测试]脏标记
     */
    public boolean getModule_website_versionDirtyFlag();
    /**
     * 获取 [Unsplash图像库]
     */
    public void setModule_web_unsplash(String module_web_unsplash);
    
    /**
     * 设置 [Unsplash图像库]
     */
    public String getModule_web_unsplash();

    /**
     * 获取 [Unsplash图像库]脏标记
     */
    public boolean getModule_web_unsplashDirtyFlag();
    /**
     * 获取 [每个产品的多种销售价格]
     */
    public void setMulti_sales_price(String multi_sales_price);
    
    /**
     * 设置 [每个产品的多种销售价格]
     */
    public String getMulti_sales_price();

    /**
     * 获取 [每个产品的多种销售价格]脏标记
     */
    public boolean getMulti_sales_priceDirtyFlag();
    /**
     * 获取 [计价方法]
     */
    public void setMulti_sales_price_method(String multi_sales_price_method);
    
    /**
     * 设置 [计价方法]
     */
    public String getMulti_sales_price_method();

    /**
     * 获取 [计价方法]脏标记
     */
    public boolean getMulti_sales_price_methodDirtyFlag();
    /**
     * 获取 [纸张格式]
     */
    public void setPaperformat_id(Integer paperformat_id);
    
    /**
     * 设置 [纸张格式]
     */
    public Integer getPaperformat_id();

    /**
     * 获取 [纸张格式]脏标记
     */
    public boolean getPaperformat_idDirtyFlag();
    /**
     * 获取 [信用不足]
     */
    public void setPartner_autocomplete_insufficient_credit(String partner_autocomplete_insufficient_credit);
    
    /**
     * 设置 [信用不足]
     */
    public String getPartner_autocomplete_insufficient_credit();

    /**
     * 获取 [信用不足]脏标记
     */
    public boolean getPartner_autocomplete_insufficient_creditDirtyFlag();
    /**
     * 获取 [在线支付]
     */
    public void setPortal_confirmation_pay(String portal_confirmation_pay);
    
    /**
     * 设置 [在线支付]
     */
    public String getPortal_confirmation_pay();

    /**
     * 获取 [在线支付]脏标记
     */
    public boolean getPortal_confirmation_payDirtyFlag();
    /**
     * 获取 [在线签名]
     */
    public void setPortal_confirmation_sign(String portal_confirmation_sign);
    
    /**
     * 设置 [在线签名]
     */
    public String getPortal_confirmation_sign();

    /**
     * 获取 [在线签名]脏标记
     */
    public boolean getPortal_confirmation_signDirtyFlag();
    /**
     * 获取 [POS价格表]
     */
    public void setPos_pricelist_setting(String pos_pricelist_setting);
    
    /**
     * 设置 [POS价格表]
     */
    public String getPos_pricelist_setting();

    /**
     * 获取 [POS价格表]脏标记
     */
    public boolean getPos_pricelist_settingDirtyFlag();
    /**
     * 获取 [产品复价]
     */
    public void setPos_sales_price(String pos_sales_price);
    
    /**
     * 设置 [产品复价]
     */
    public String getPos_sales_price();

    /**
     * 获取 [产品复价]脏标记
     */
    public boolean getPos_sales_priceDirtyFlag();
    /**
     * 获取 [审批层级 *]
     */
    public void setPo_double_validation(String po_double_validation);
    
    /**
     * 设置 [审批层级 *]
     */
    public String getPo_double_validation();

    /**
     * 获取 [审批层级 *]脏标记
     */
    public boolean getPo_double_validationDirtyFlag();
    /**
     * 获取 [最小金额]
     */
    public void setPo_double_validation_amount(Double po_double_validation_amount);
    
    /**
     * 设置 [最小金额]
     */
    public Double getPo_double_validation_amount();

    /**
     * 获取 [最小金额]脏标记
     */
    public boolean getPo_double_validation_amountDirtyFlag();
    /**
     * 获取 [采购提前时间]
     */
    public void setPo_lead(Double po_lead);
    
    /**
     * 设置 [采购提前时间]
     */
    public Double getPo_lead();

    /**
     * 获取 [采购提前时间]脏标记
     */
    public boolean getPo_leadDirtyFlag();
    /**
     * 获取 [采购订单修改 *]
     */
    public void setPo_lock(String po_lock);
    
    /**
     * 设置 [采购订单修改 *]
     */
    public String getPo_lock();

    /**
     * 获取 [采购订单修改 *]脏标记
     */
    public boolean getPo_lockDirtyFlag();
    /**
     * 获取 [采购订单批准]
     */
    public void setPo_order_approval(String po_order_approval);
    
    /**
     * 设置 [采购订单批准]
     */
    public String getPo_order_approval();

    /**
     * 获取 [采购订单批准]脏标记
     */
    public boolean getPo_order_approvalDirtyFlag();
    /**
     * 获取 [重量单位]
     */
    public void setProduct_weight_in_lbs(String product_weight_in_lbs);
    
    /**
     * 设置 [重量单位]
     */
    public String getProduct_weight_in_lbs();

    /**
     * 获取 [重量单位]脏标记
     */
    public boolean getProduct_weight_in_lbsDirtyFlag();
    /**
     * 获取 [传播的最小差值]
     */
    public void setPropagation_minimum_delta(Integer propagation_minimum_delta);
    
    /**
     * 设置 [传播的最小差值]
     */
    public Integer getPropagation_minimum_delta();

    /**
     * 获取 [传播的最小差值]脏标记
     */
    public boolean getPropagation_minimum_deltaDirtyFlag();
    /**
     * 获取 [默认进项税]
     */
    public void setPurchase_tax_id(Integer purchase_tax_id);
    
    /**
     * 设置 [默认进项税]
     */
    public Integer getPurchase_tax_id();

    /**
     * 获取 [默认进项税]脏标记
     */
    public boolean getPurchase_tax_idDirtyFlag();
    /**
     * 获取 [显示SEPA QR码]
     */
    public void setQr_code(String qr_code);
    
    /**
     * 设置 [显示SEPA QR码]
     */
    public String getQr_code();

    /**
     * 获取 [显示SEPA QR码]脏标记
     */
    public boolean getQr_codeDirtyFlag();
    /**
     * 获取 [默认报价有效期（日）]
     */
    public void setQuotation_validity_days(Integer quotation_validity_days);
    
    /**
     * 设置 [默认报价有效期（日）]
     */
    public Integer getQuotation_validity_days();

    /**
     * 获取 [默认报价有效期（日）]脏标记
     */
    public boolean getQuotation_validity_daysDirtyFlag();
    /**
     * 获取 [自定义报表页脚]
     */
    public void setReport_footer(String report_footer);
    
    /**
     * 设置 [自定义报表页脚]
     */
    public String getReport_footer();

    /**
     * 获取 [自定义报表页脚]脏标记
     */
    public boolean getReport_footerDirtyFlag();
    /**
     * 获取 [公司的上班时间]
     */
    public void setResource_calendar_id(Integer resource_calendar_id);
    
    /**
     * 设置 [公司的上班时间]
     */
    public Integer getResource_calendar_id();

    /**
     * 获取 [公司的上班时间]脏标记
     */
    public boolean getResource_calendar_idDirtyFlag();
    /**
     * 获取 [销售员]
     */
    public void setSalesperson_id(Integer salesperson_id);
    
    /**
     * 设置 [销售员]
     */
    public Integer getSalesperson_id();

    /**
     * 获取 [销售员]脏标记
     */
    public boolean getSalesperson_idDirtyFlag();
    /**
     * 获取 [销售团队]
     */
    public void setSalesteam_id(Integer salesteam_id);
    
    /**
     * 设置 [销售团队]
     */
    public Integer getSalesteam_id();

    /**
     * 获取 [销售团队]脏标记
     */
    public boolean getSalesteam_idDirtyFlag();
    /**
     * 获取 [送货管理]
     */
    public void setSale_delivery_settings(String sale_delivery_settings);
    
    /**
     * 设置 [送货管理]
     */
    public String getSale_delivery_settings();

    /**
     * 获取 [送货管理]脏标记
     */
    public boolean getSale_delivery_settingsDirtyFlag();
    /**
     * 获取 [条款及条件]
     */
    public void setSale_note(String sale_note);
    
    /**
     * 设置 [条款及条件]
     */
    public String getSale_note();

    /**
     * 获取 [条款及条件]脏标记
     */
    public boolean getSale_noteDirtyFlag();
    /**
     * 获取 [价格表]
     */
    public void setSale_pricelist_setting(String sale_pricelist_setting);
    
    /**
     * 设置 [价格表]
     */
    public String getSale_pricelist_setting();

    /**
     * 获取 [价格表]脏标记
     */
    public boolean getSale_pricelist_settingDirtyFlag();
    /**
     * 获取 [默认销售税]
     */
    public void setSale_tax_id(Integer sale_tax_id);
    
    /**
     * 设置 [默认销售税]
     */
    public Integer getSale_tax_id();

    /**
     * 获取 [默认销售税]脏标记
     */
    public boolean getSale_tax_idDirtyFlag();
    /**
     * 获取 [安全时间]
     */
    public void setSecurity_lead(Double security_lead);
    
    /**
     * 设置 [安全时间]
     */
    public Double getSecurity_lead();

    /**
     * 获取 [安全时间]脏标记
     */
    public boolean getSecurity_leadDirtyFlag();
    /**
     * 获取 [在取消订阅页面上显示黑名单按钮]
     */
    public void setShow_blacklist_buttons(String show_blacklist_buttons);
    
    /**
     * 设置 [在取消订阅页面上显示黑名单按钮]
     */
    public String getShow_blacklist_buttons();

    /**
     * 获取 [在取消订阅页面上显示黑名单按钮]脏标记
     */
    public boolean getShow_blacklist_buttonsDirtyFlag();
    /**
     * 获取 [显示效果]
     */
    public void setShow_effect(String show_effect);
    
    /**
     * 设置 [显示效果]
     */
    public String getShow_effect();

    /**
     * 获取 [显示效果]脏标记
     */
    public boolean getShow_effectDirtyFlag();
    /**
     * 获取 [税目汇总表]
     */
    public void setShow_line_subtotals_tax_selection(String show_line_subtotals_tax_selection);
    
    /**
     * 设置 [税目汇总表]
     */
    public String getShow_line_subtotals_tax_selection();

    /**
     * 获取 [税目汇总表]脏标记
     */
    public boolean getShow_line_subtotals_tax_selectionDirtyFlag();
    /**
     * 获取 [彩色打印]
     */
    public void setSnailmail_color(String snailmail_color);
    
    /**
     * 设置 [彩色打印]
     */
    public String getSnailmail_color();

    /**
     * 获取 [彩色打印]脏标记
     */
    public boolean getSnailmail_colorDirtyFlag();
    /**
     * 获取 [双面打印]
     */
    public void setSnailmail_duplex(String snailmail_duplex);
    
    /**
     * 设置 [双面打印]
     */
    public String getSnailmail_duplex();

    /**
     * 获取 [双面打印]脏标记
     */
    public boolean getSnailmail_duplexDirtyFlag();
    /**
     * 获取 [默认社交分享图片]
     */
    public void setSocial_default_image(byte[] social_default_image);
    
    /**
     * 设置 [默认社交分享图片]
     */
    public byte[] getSocial_default_image();

    /**
     * 获取 [默认社交分享图片]脏标记
     */
    public boolean getSocial_default_imageDirtyFlag();
    /**
     * 获取 [脸书账号]
     */
    public void setSocial_facebook(String social_facebook);
    
    /**
     * 设置 [脸书账号]
     */
    public String getSocial_facebook();

    /**
     * 获取 [脸书账号]脏标记
     */
    public boolean getSocial_facebookDirtyFlag();
    /**
     * 获取 [GitHub账户]
     */
    public void setSocial_github(String social_github);
    
    /**
     * 设置 [GitHub账户]
     */
    public String getSocial_github();

    /**
     * 获取 [GitHub账户]脏标记
     */
    public boolean getSocial_githubDirtyFlag();
    /**
     * 获取 [Google+账户]
     */
    public void setSocial_googleplus(String social_googleplus);
    
    /**
     * 设置 [Google+账户]
     */
    public String getSocial_googleplus();

    /**
     * 获取 [Google+账户]脏标记
     */
    public boolean getSocial_googleplusDirtyFlag();
    /**
     * 获取 [Instagram 账号]
     */
    public void setSocial_instagram(String social_instagram);
    
    /**
     * 设置 [Instagram 账号]
     */
    public String getSocial_instagram();

    /**
     * 获取 [Instagram 账号]脏标记
     */
    public boolean getSocial_instagramDirtyFlag();
    /**
     * 获取 [领英账号]
     */
    public void setSocial_linkedin(String social_linkedin);
    
    /**
     * 设置 [领英账号]
     */
    public String getSocial_linkedin();

    /**
     * 获取 [领英账号]脏标记
     */
    public boolean getSocial_linkedinDirtyFlag();
    /**
     * 获取 [Twitter账号]
     */
    public void setSocial_twitter(String social_twitter);
    
    /**
     * 设置 [Twitter账号]
     */
    public String getSocial_twitter();

    /**
     * 获取 [Twitter账号]脏标记
     */
    public boolean getSocial_twitterDirtyFlag();
    /**
     * 获取 [Youtube账号]
     */
    public void setSocial_youtube(String social_youtube);
    
    /**
     * 设置 [Youtube账号]
     */
    public String getSocial_youtube();

    /**
     * 获取 [Youtube账号]脏标记
     */
    public boolean getSocial_youtubeDirtyFlag();
    /**
     * 获取 [具体用户账号]
     */
    public void setSpecific_user_account(String specific_user_account);
    
    /**
     * 设置 [具体用户账号]
     */
    public String getSpecific_user_account();

    /**
     * 获取 [具体用户账号]脏标记
     */
    public boolean getSpecific_user_accountDirtyFlag();
    /**
     * 获取 [税率计算的舍入方法]
     */
    public void setTax_calculation_rounding_method(String tax_calculation_rounding_method);
    
    /**
     * 设置 [税率计算的舍入方法]
     */
    public String getTax_calculation_rounding_method();

    /**
     * 获取 [税率计算的舍入方法]脏标记
     */
    public boolean getTax_calculation_rounding_methodDirtyFlag();
    /**
     * 获取 [税率现金收付制日记账]
     */
    public void setTax_cash_basis_journal_id(Integer tax_cash_basis_journal_id);
    
    /**
     * 设置 [税率现金收付制日记账]
     */
    public Integer getTax_cash_basis_journal_id();

    /**
     * 获取 [税率现金收付制日记账]脏标记
     */
    public boolean getTax_cash_basis_journal_idDirtyFlag();
    /**
     * 获取 [现金收付制]
     */
    public void setTax_exigibility(String tax_exigibility);
    
    /**
     * 设置 [现金收付制]
     */
    public String getTax_exigibility();

    /**
     * 获取 [现金收付制]脏标记
     */
    public boolean getTax_exigibilityDirtyFlag();
    /**
     * 获取 [EMail模板]
     */
    public void setTemplate_id(Integer template_id);
    
    /**
     * 设置 [EMail模板]
     */
    public Integer getTemplate_id();

    /**
     * 获取 [EMail模板]脏标记
     */
    public boolean getTemplate_idDirtyFlag();
    /**
     * 获取 [EMail模板]
     */
    public void setTemplate_id_text(String template_id_text);
    
    /**
     * 设置 [EMail模板]
     */
    public String getTemplate_id_text();

    /**
     * 获取 [EMail模板]脏标记
     */
    public boolean getTemplate_id_textDirtyFlag();
    /**
     * 获取 [访问秘钥]
     */
    public void setUnsplash_access_key(String unsplash_access_key);
    
    /**
     * 设置 [访问秘钥]
     */
    public String getUnsplash_access_key();

    /**
     * 获取 [访问秘钥]脏标记
     */
    public boolean getUnsplash_access_keyDirtyFlag();
    /**
     * 获取 [默认访问权限]
     */
    public void setUser_default_rights(String user_default_rights);
    
    /**
     * 设置 [默认访问权限]
     */
    public String getUser_default_rights();

    /**
     * 获取 [默认访问权限]脏标记
     */
    public boolean getUser_default_rightsDirtyFlag();
    /**
     * 获取 [允许员工通过EMail记录费用]
     */
    public void setUse_mailgateway(String use_mailgateway);
    
    /**
     * 设置 [允许员工通过EMail记录费用]
     */
    public String getUse_mailgateway();

    /**
     * 获取 [允许员工通过EMail记录费用]脏标记
     */
    public boolean getUse_mailgatewayDirtyFlag();
    /**
     * 获取 [默认制造提前期]
     */
    public void setUse_manufacturing_lead(String use_manufacturing_lead);
    
    /**
     * 设置 [默认制造提前期]
     */
    public String getUse_manufacturing_lead();

    /**
     * 获取 [默认制造提前期]脏标记
     */
    public boolean getUse_manufacturing_leadDirtyFlag();
    /**
     * 获取 [安全交货时间]
     */
    public void setUse_po_lead(String use_po_lead);
    
    /**
     * 设置 [安全交货时间]
     */
    public String getUse_po_lead();

    /**
     * 获取 [安全交货时间]脏标记
     */
    public boolean getUse_po_leadDirtyFlag();
    /**
     * 获取 [无重新排程传播]
     */
    public void setUse_propagation_minimum_delta(String use_propagation_minimum_delta);
    
    /**
     * 设置 [无重新排程传播]
     */
    public String getUse_propagation_minimum_delta();

    /**
     * 获取 [无重新排程传播]脏标记
     */
    public boolean getUse_propagation_minimum_deltaDirtyFlag();
    /**
     * 获取 [默认报价有效期]
     */
    public void setUse_quotation_validity_days(String use_quotation_validity_days);
    
    /**
     * 设置 [默认报价有效期]
     */
    public String getUse_quotation_validity_days();

    /**
     * 获取 [默认报价有效期]脏标记
     */
    public boolean getUse_quotation_validity_daysDirtyFlag();
    /**
     * 获取 [默认条款和条件]
     */
    public void setUse_sale_note(String use_sale_note);
    
    /**
     * 设置 [默认条款和条件]
     */
    public String getUse_sale_note();

    /**
     * 获取 [默认条款和条件]脏标记
     */
    public boolean getUse_sale_noteDirtyFlag();
    /**
     * 获取 [销售的安全提前期]
     */
    public void setUse_security_lead(String use_security_lead);
    
    /**
     * 设置 [销售的安全提前期]
     */
    public String getUse_security_lead();

    /**
     * 获取 [销售的安全提前期]脏标记
     */
    public boolean getUse_security_leadDirtyFlag();
    /**
     * 获取 [网站公司]
     */
    public void setWebsite_company_id(Integer website_company_id);
    
    /**
     * 设置 [网站公司]
     */
    public Integer getWebsite_company_id();

    /**
     * 获取 [网站公司]脏标记
     */
    public boolean getWebsite_company_idDirtyFlag();
    /**
     * 获取 [国家/地区分组]
     */
    public void setWebsite_country_group_ids(String website_country_group_ids);
    
    /**
     * 设置 [国家/地区分组]
     */
    public String getWebsite_country_group_ids();

    /**
     * 获取 [国家/地区分组]脏标记
     */
    public boolean getWebsite_country_group_idsDirtyFlag();
    /**
     * 获取 [默认语言代码]
     */
    public void setWebsite_default_lang_code(String website_default_lang_code);
    
    /**
     * 设置 [默认语言代码]
     */
    public String getWebsite_default_lang_code();

    /**
     * 获取 [默认语言代码]脏标记
     */
    public boolean getWebsite_default_lang_codeDirtyFlag();
    /**
     * 获取 [默认语言]
     */
    public void setWebsite_default_lang_id(Integer website_default_lang_id);
    
    /**
     * 设置 [默认语言]
     */
    public Integer getWebsite_default_lang_id();

    /**
     * 获取 [默认语言]脏标记
     */
    public boolean getWebsite_default_lang_idDirtyFlag();
    /**
     * 获取 [网站域名]
     */
    public void setWebsite_domain(String website_domain);
    
    /**
     * 设置 [网站域名]
     */
    public String getWebsite_domain();

    /**
     * 获取 [网站域名]脏标记
     */
    public boolean getWebsite_domainDirtyFlag();
    /**
     * 获取 [联系表单上的技术数据]
     */
    public void setWebsite_form_enable_metadata(String website_form_enable_metadata);
    
    /**
     * 设置 [联系表单上的技术数据]
     */
    public String getWebsite_form_enable_metadata();

    /**
     * 获取 [联系表单上的技术数据]脏标记
     */
    public boolean getWebsite_form_enable_metadataDirtyFlag();
    /**
     * 获取 [网站]
     */
    public void setWebsite_id(Integer website_id);
    
    /**
     * 设置 [网站]
     */
    public Integer getWebsite_id();

    /**
     * 获取 [网站]脏标记
     */
    public boolean getWebsite_idDirtyFlag();
    /**
     * 获取 [网站名称]
     */
    public void setWebsite_name(String website_name);
    
    /**
     * 设置 [网站名称]
     */
    public String getWebsite_name();

    /**
     * 获取 [网站名称]脏标记
     */
    public boolean getWebsite_nameDirtyFlag();
    /**
     * 获取 [谷歌文档密钥]
     */
    public void setWebsite_slide_google_app_key(String website_slide_google_app_key);
    
    /**
     * 设置 [谷歌文档密钥]
     */
    public String getWebsite_slide_google_app_key();

    /**
     * 获取 [谷歌文档密钥]脏标记
     */
    public boolean getWebsite_slide_google_app_keyDirtyFlag();
    /**
     * 获取 [最后更新时间]
     */
    public void setWrite_date(Timestamp write_date);
    
    /**
     * 设置 [最后更新时间]
     */
    public Timestamp getWrite_date();

    /**
     * 获取 [最后更新时间]脏标记
     */
    public boolean getWrite_dateDirtyFlag();
    /**
     * 获取 [最后更新者]
     */
    public void setWrite_uid(Integer write_uid);
    
    /**
     * 设置 [最后更新者]
     */
    public Integer getWrite_uid();

    /**
     * 获取 [最后更新者]脏标记
     */
    public boolean getWrite_uidDirtyFlag();
    /**
     * 获取 [最后更新者]
     */
    public void setWrite_uid_text(String write_uid_text);
    
    /**
     * 设置 [最后更新者]
     */
    public String getWrite_uid_text();

    /**
     * 获取 [最后更新者]脏标记
     */
    public boolean getWrite_uid_textDirtyFlag();
    /**
     * 获取 [最后修改日]
     */
    public void set__last_update(Timestamp __last_update);
    
    /**
     * 设置 [最后修改日]
     */
    public Timestamp get__last_update();

    /**
     * 获取 [最后修改日]脏标记
     */
    public boolean get__last_updateDirtyFlag();


    /**
     *  转换为Map
     */
    public Map<String, Object> toMap() throws Exception ;

    /**
     *  从Map构建
     */
   public void fromMap(Map<String, Object> map) throws Exception;
}
