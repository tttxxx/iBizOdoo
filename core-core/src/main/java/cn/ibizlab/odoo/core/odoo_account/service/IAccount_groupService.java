package cn.ibizlab.odoo.core.odoo_account.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_account.domain.Account_group;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_groupSearchContext;


/**
 * 实体[Account_group] 服务对象接口
 */
public interface IAccount_groupService{

    Account_group get(Integer key) ;
    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    boolean update(Account_group et) ;
    void updateBatch(List<Account_group> list) ;
    boolean create(Account_group et) ;
    void createBatch(List<Account_group> list) ;
    Page<Account_group> searchDefault(Account_groupSearchContext context) ;

}



