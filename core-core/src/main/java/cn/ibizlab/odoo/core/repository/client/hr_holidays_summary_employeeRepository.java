package cn.ibizlab.odoo.core.repository.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import cn.ibizlab.odoo.core.repository.client.po.hr_holidays_summary_employee;

/**
 * 实体[hr_holidays_summary_employee] 服务对象接口
 */
public interface hr_holidays_summary_employeeRepository{


    public hr_holidays_summary_employee createPO() ;
        public void remove(String id);

        public void updateBatch(hr_holidays_summary_employee hr_holidays_summary_employee);

        public void removeBatch(String id);

        public void createBatch(hr_holidays_summary_employee hr_holidays_summary_employee);

        public void update(hr_holidays_summary_employee hr_holidays_summary_employee);

        public void get(String id);

        public List<hr_holidays_summary_employee> search();

        public void create(hr_holidays_summary_employee hr_holidays_summary_employee);


}
