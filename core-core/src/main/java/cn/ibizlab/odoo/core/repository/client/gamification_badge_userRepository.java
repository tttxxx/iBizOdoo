package cn.ibizlab.odoo.core.repository.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import cn.ibizlab.odoo.core.repository.client.po.gamification_badge_user;

/**
 * 实体[gamification_badge_user] 服务对象接口
 */
public interface gamification_badge_userRepository{


    public gamification_badge_user createPO() ;
        public void createBatch(gamification_badge_user gamification_badge_user);

        public void get(String id);

        public void updateBatch(gamification_badge_user gamification_badge_user);

        public void update(gamification_badge_user gamification_badge_user);

        public void removeBatch(String id);

        public void create(gamification_badge_user gamification_badge_user);

        public List<gamification_badge_user> search();

        public void remove(String id);


}
