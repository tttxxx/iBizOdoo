package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Utm_campaign;
import cn.ibizlab.odoo.core.odoo_utm.filter.Utm_campaignSearchContext;

/**
 * 实体 [UTM 营销活动] 存储对象
 */
public interface Utm_campaignRepository extends Repository<Utm_campaign> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Utm_campaign> searchDefault(Utm_campaignSearchContext context);

    Utm_campaign convert2PO(cn.ibizlab.odoo.core.odoo_utm.domain.Utm_campaign domain , Utm_campaign po) ;

    cn.ibizlab.odoo.core.odoo_utm.domain.Utm_campaign convert2Domain( Utm_campaign po ,cn.ibizlab.odoo.core.odoo_utm.domain.Utm_campaign domain) ;

}
