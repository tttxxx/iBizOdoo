package cn.ibizlab.odoo.core.odoo_product.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_product.domain.Product_public_category;
import cn.ibizlab.odoo.core.odoo_product.filter.Product_public_categorySearchContext;
import cn.ibizlab.odoo.core.odoo_product.service.IProduct_public_categoryService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;



import cn.ibizlab.odoo.core.odoo_product.client.product_public_categoryOdooClient;
import cn.ibizlab.odoo.core.odoo_product.clientmodel.product_public_categoryClientModel;

/**
 * 实体[网站产品目录] 服务对象接口实现
 */
@Slf4j
@Service
public class Product_public_categoryServiceImpl implements IProduct_public_categoryService {

    @Autowired
    product_public_categoryOdooClient product_public_categoryOdooClient;


    @Override
    public Product_public_category get(Integer id) {
        product_public_categoryClientModel clientModel = new product_public_categoryClientModel();
        clientModel.setId(id);
		product_public_categoryOdooClient.get(clientModel);
        Product_public_category et = convert2Domain(clientModel,null);
        if(et==null){
            et=new Product_public_category();
            et.setId(id);
        }
        else{
        }

        return  et;
    }

    @Override
    public boolean create(Product_public_category et) {
        product_public_categoryClientModel clientModel = convert2Model(et,null);
		product_public_categoryOdooClient.create(clientModel);
        Product_public_category rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Product_public_category> list){
    }

    @Override
    public boolean remove(Integer id) {
        product_public_categoryClientModel clientModel = new product_public_categoryClientModel();
        clientModel.setId(id);
		product_public_categoryOdooClient.remove(clientModel);
        return true; 
    }

    public void removeBatch(Collection<Integer> idList){
    }

    @Override
    public boolean update(Product_public_category et) {
        product_public_categoryClientModel clientModel = convert2Model(et,null);
		product_public_categoryOdooClient.update(clientModel);
        Product_public_category rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }
    
    public void updateBatch(List<Product_public_category> list){
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Product_public_category> searchDefault(Product_public_categorySearchContext context) {
        List<Product_public_category> list = new ArrayList<Product_public_category>();
        Page<product_public_categoryClientModel> clientModelList = product_public_categoryOdooClient.search(context);
        for(product_public_categoryClientModel clientModel : clientModelList){
            list.add( this.convert2Domain(clientModel,null));
        }
        return new PageImpl<Product_public_category>(list, context.getPageable(), clientModelList.getTotalElements());
    }

    public product_public_categoryClientModel convert2Model(Product_public_category domain , product_public_categoryClientModel model) {
        if(domain == null)
            return model ;
        if(model==null){
            model = new product_public_categoryClientModel();

        }
        if((Boolean) domain.getExtensionparams().get("dirtyflagenable")){
            if((Boolean) domain.getExtensionparams().get("image_mediumdirtyflag"))
                model.setImage_medium(domain.getImageMedium());
            if((Boolean) domain.getExtensionparams().get("image_smalldirtyflag"))
                model.setImage_small(domain.getImageSmall());
            if((Boolean) domain.getExtensionparams().get("iddirtyflag"))
                model.setId(domain.getId());
            if((Boolean) domain.getExtensionparams().get("website_meta_titledirtyflag"))
                model.setWebsite_meta_title(domain.getWebsiteMetaTitle());
            if((Boolean) domain.getExtensionparams().get("is_seo_optimizeddirtyflag"))
                model.setIs_seo_optimized(domain.getIsSeoOptimized());
            if((Boolean) domain.getExtensionparams().get("sequencedirtyflag"))
                model.setSequence(domain.getSequence());
            if((Boolean) domain.getExtensionparams().get("website_meta_descriptiondirtyflag"))
                model.setWebsite_meta_description(domain.getWebsiteMetaDescription());
            if((Boolean) domain.getExtensionparams().get("website_meta_og_imgdirtyflag"))
                model.setWebsite_meta_og_img(domain.getWebsiteMetaOgImg());
            if((Boolean) domain.getExtensionparams().get("display_namedirtyflag"))
                model.setDisplay_name(domain.getDisplayName());
            if((Boolean) domain.getExtensionparams().get("website_meta_keywordsdirtyflag"))
                model.setWebsite_meta_keywords(domain.getWebsiteMetaKeywords());
            if((Boolean) domain.getExtensionparams().get("create_datedirtyflag"))
                model.setCreate_date(domain.getCreateDate());
            if((Boolean) domain.getExtensionparams().get("write_datedirtyflag"))
                model.setWrite_date(domain.getWriteDate());
            if((Boolean) domain.getExtensionparams().get("__last_updatedirtyflag"))
                model.set__last_update(domain.getLastUpdate());
            if((Boolean) domain.getExtensionparams().get("imagedirtyflag"))
                model.setImage(domain.getImage());
            if((Boolean) domain.getExtensionparams().get("website_iddirtyflag"))
                model.setWebsite_id(domain.getWebsiteId());
            if((Boolean) domain.getExtensionparams().get("child_iddirtyflag"))
                model.setChild_id(domain.getChildId());
            if((Boolean) domain.getExtensionparams().get("namedirtyflag"))
                model.setName(domain.getName());
            if((Boolean) domain.getExtensionparams().get("create_uid_textdirtyflag"))
                model.setCreate_uid_text(domain.getCreateUidText());
            if((Boolean) domain.getExtensionparams().get("write_uid_textdirtyflag"))
                model.setWrite_uid_text(domain.getWriteUidText());
            if((Boolean) domain.getExtensionparams().get("parent_id_textdirtyflag"))
                model.setParent_id_text(domain.getParentIdText());
            if((Boolean) domain.getExtensionparams().get("write_uiddirtyflag"))
                model.setWrite_uid(domain.getWriteUid());
            if((Boolean) domain.getExtensionparams().get("parent_iddirtyflag"))
                model.setParent_id(domain.getParentId());
            if((Boolean) domain.getExtensionparams().get("create_uiddirtyflag"))
                model.setCreate_uid(domain.getCreateUid());
            domain.getFocusNull().clear();
        }
        return model ;
    }

    public Product_public_category convert2Domain( product_public_categoryClientModel model ,Product_public_category domain) {
        if(model == null)
            return domain ;
        if(domain == null){
            domain = new Product_public_category();
        }

        if(model.getImage_mediumDirtyFlag())
            domain.setImageMedium(model.getImage_medium());
        if(model.getImage_smallDirtyFlag())
            domain.setImageSmall(model.getImage_small());
        if(model.getIdDirtyFlag())
            domain.setId(model.getId());
        if(model.getWebsite_meta_titleDirtyFlag())
            domain.setWebsiteMetaTitle(model.getWebsite_meta_title());
        if(model.getIs_seo_optimizedDirtyFlag())
            domain.setIsSeoOptimized(model.getIs_seo_optimized());
        if(model.getSequenceDirtyFlag())
            domain.setSequence(model.getSequence());
        if(model.getWebsite_meta_descriptionDirtyFlag())
            domain.setWebsiteMetaDescription(model.getWebsite_meta_description());
        if(model.getWebsite_meta_og_imgDirtyFlag())
            domain.setWebsiteMetaOgImg(model.getWebsite_meta_og_img());
        if(model.getDisplay_nameDirtyFlag())
            domain.setDisplayName(model.getDisplay_name());
        if(model.getWebsite_meta_keywordsDirtyFlag())
            domain.setWebsiteMetaKeywords(model.getWebsite_meta_keywords());
        if(model.getCreate_dateDirtyFlag())
            domain.setCreateDate(model.getCreate_date());
        if(model.getWrite_dateDirtyFlag())
            domain.setWriteDate(model.getWrite_date());
        if(model.get__last_updateDirtyFlag())
            domain.setLastUpdate(model.get__last_update());
        if(model.getImageDirtyFlag())
            domain.setImage(model.getImage());
        if(model.getWebsite_idDirtyFlag())
            domain.setWebsiteId(model.getWebsite_id());
        if(model.getChild_idDirtyFlag())
            domain.setChildId(model.getChild_id());
        if(model.getNameDirtyFlag())
            domain.setName(model.getName());
        if(model.getCreate_uid_textDirtyFlag())
            domain.setCreateUidText(model.getCreate_uid_text());
        if(model.getWrite_uid_textDirtyFlag())
            domain.setWriteUidText(model.getWrite_uid_text());
        if(model.getParent_id_textDirtyFlag())
            domain.setParentIdText(model.getParent_id_text());
        if(model.getWrite_uidDirtyFlag())
            domain.setWriteUid(model.getWrite_uid());
        if(model.getParent_idDirtyFlag())
            domain.setParentId(model.getParent_id());
        if(model.getCreate_uidDirtyFlag())
            domain.setCreateUid(model.getCreate_uid());
        return domain ;
    }

}

    



