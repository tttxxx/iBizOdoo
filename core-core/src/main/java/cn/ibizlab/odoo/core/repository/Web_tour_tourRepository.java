package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Web_tour_tour;
import cn.ibizlab.odoo.core.odoo_web_tour.filter.Web_tour_tourSearchContext;

/**
 * 实体 [向导] 存储对象
 */
public interface Web_tour_tourRepository extends Repository<Web_tour_tour> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Web_tour_tour> searchDefault(Web_tour_tourSearchContext context);

    Web_tour_tour convert2PO(cn.ibizlab.odoo.core.odoo_web_tour.domain.Web_tour_tour domain , Web_tour_tour po) ;

    cn.ibizlab.odoo.core.odoo_web_tour.domain.Web_tour_tour convert2Domain( Web_tour_tour po ,cn.ibizlab.odoo.core.odoo_web_tour.domain.Web_tour_tour domain) ;

}
