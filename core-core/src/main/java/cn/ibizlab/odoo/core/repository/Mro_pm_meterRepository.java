package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Mro_pm_meter;
import cn.ibizlab.odoo.core.odoo_mro.filter.Mro_pm_meterSearchContext;

/**
 * 实体 [Asset Meters] 存储对象
 */
public interface Mro_pm_meterRepository extends Repository<Mro_pm_meter> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Mro_pm_meter> searchDefault(Mro_pm_meterSearchContext context);

    Mro_pm_meter convert2PO(cn.ibizlab.odoo.core.odoo_mro.domain.Mro_pm_meter domain , Mro_pm_meter po) ;

    cn.ibizlab.odoo.core.odoo_mro.domain.Mro_pm_meter convert2Domain( Mro_pm_meter po ,cn.ibizlab.odoo.core.odoo_mro.domain.Mro_pm_meter domain) ;

}
