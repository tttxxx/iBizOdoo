package cn.ibizlab.odoo.core.odoo_fleet.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_fleet.domain.Fleet_vehicle_model;
import cn.ibizlab.odoo.core.odoo_fleet.filter.Fleet_vehicle_modelSearchContext;
import cn.ibizlab.odoo.core.odoo_fleet.service.IFleet_vehicle_modelService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;



import cn.ibizlab.odoo.core.odoo_fleet.client.fleet_vehicle_modelOdooClient;
import cn.ibizlab.odoo.core.odoo_fleet.clientmodel.fleet_vehicle_modelClientModel;

/**
 * 实体[车辆型号] 服务对象接口实现
 */
@Slf4j
@Service
public class Fleet_vehicle_modelServiceImpl implements IFleet_vehicle_modelService {

    @Autowired
    fleet_vehicle_modelOdooClient fleet_vehicle_modelOdooClient;


    @Override
    public boolean update(Fleet_vehicle_model et) {
        fleet_vehicle_modelClientModel clientModel = convert2Model(et,null);
		fleet_vehicle_modelOdooClient.update(clientModel);
        Fleet_vehicle_model rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }
    
    public void updateBatch(List<Fleet_vehicle_model> list){
    }

    @Override
    public Fleet_vehicle_model get(Integer id) {
        fleet_vehicle_modelClientModel clientModel = new fleet_vehicle_modelClientModel();
        clientModel.setId(id);
		fleet_vehicle_modelOdooClient.get(clientModel);
        Fleet_vehicle_model et = convert2Domain(clientModel,null);
        if(et==null){
            et=new Fleet_vehicle_model();
            et.setId(id);
        }
        else{
        }

        return  et;
    }

    @Override
    public boolean remove(Integer id) {
        fleet_vehicle_modelClientModel clientModel = new fleet_vehicle_modelClientModel();
        clientModel.setId(id);
		fleet_vehicle_modelOdooClient.remove(clientModel);
        return true; 
    }

    public void removeBatch(Collection<Integer> idList){
    }

    @Override
    public boolean create(Fleet_vehicle_model et) {
        fleet_vehicle_modelClientModel clientModel = convert2Model(et,null);
		fleet_vehicle_modelOdooClient.create(clientModel);
        Fleet_vehicle_model rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Fleet_vehicle_model> list){
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Fleet_vehicle_model> searchDefault(Fleet_vehicle_modelSearchContext context) {
        List<Fleet_vehicle_model> list = new ArrayList<Fleet_vehicle_model>();
        Page<fleet_vehicle_modelClientModel> clientModelList = fleet_vehicle_modelOdooClient.search(context);
        for(fleet_vehicle_modelClientModel clientModel : clientModelList){
            list.add( this.convert2Domain(clientModel,null));
        }
        return new PageImpl<Fleet_vehicle_model>(list, context.getPageable(), clientModelList.getTotalElements());
    }

    public fleet_vehicle_modelClientModel convert2Model(Fleet_vehicle_model domain , fleet_vehicle_modelClientModel model) {
        if(domain == null)
            return model ;
        if(model==null){
            model = new fleet_vehicle_modelClientModel();

        }
        if((Boolean) domain.getExtensionparams().get("dirtyflagenable")){
            if((Boolean) domain.getExtensionparams().get("iddirtyflag"))
                model.setId(domain.getId());
            if((Boolean) domain.getExtensionparams().get("vendorsdirtyflag"))
                model.setVendors(domain.getVendors());
            if((Boolean) domain.getExtensionparams().get("display_namedirtyflag"))
                model.setDisplay_name(domain.getDisplayName());
            if((Boolean) domain.getExtensionparams().get("namedirtyflag"))
                model.setName(domain.getName());
            if((Boolean) domain.getExtensionparams().get("__last_updatedirtyflag"))
                model.set__last_update(domain.getLastUpdate());
            if((Boolean) domain.getExtensionparams().get("write_datedirtyflag"))
                model.setWrite_date(domain.getWriteDate());
            if((Boolean) domain.getExtensionparams().get("create_datedirtyflag"))
                model.setCreate_date(domain.getCreateDate());
            if((Boolean) domain.getExtensionparams().get("brand_id_textdirtyflag"))
                model.setBrand_id_text(domain.getBrandIdText());
            if((Boolean) domain.getExtensionparams().get("create_uid_textdirtyflag"))
                model.setCreate_uid_text(domain.getCreateUidText());
            if((Boolean) domain.getExtensionparams().get("image_smalldirtyflag"))
                model.setImage_small(domain.getImageSmall());
            if((Boolean) domain.getExtensionparams().get("write_uid_textdirtyflag"))
                model.setWrite_uid_text(domain.getWriteUidText());
            if((Boolean) domain.getExtensionparams().get("image_mediumdirtyflag"))
                model.setImage_medium(domain.getImageMedium());
            if((Boolean) domain.getExtensionparams().get("imagedirtyflag"))
                model.setImage(domain.getImage());
            if((Boolean) domain.getExtensionparams().get("brand_iddirtyflag"))
                model.setBrand_id(domain.getBrandId());
            if((Boolean) domain.getExtensionparams().get("create_uiddirtyflag"))
                model.setCreate_uid(domain.getCreateUid());
            if((Boolean) domain.getExtensionparams().get("write_uiddirtyflag"))
                model.setWrite_uid(domain.getWriteUid());
            domain.getFocusNull().clear();
        }
        return model ;
    }

    public Fleet_vehicle_model convert2Domain( fleet_vehicle_modelClientModel model ,Fleet_vehicle_model domain) {
        if(model == null)
            return domain ;
        if(domain == null){
            domain = new Fleet_vehicle_model();
        }

        if(model.getIdDirtyFlag())
            domain.setId(model.getId());
        if(model.getVendorsDirtyFlag())
            domain.setVendors(model.getVendors());
        if(model.getDisplay_nameDirtyFlag())
            domain.setDisplayName(model.getDisplay_name());
        if(model.getNameDirtyFlag())
            domain.setName(model.getName());
        if(model.get__last_updateDirtyFlag())
            domain.setLastUpdate(model.get__last_update());
        if(model.getWrite_dateDirtyFlag())
            domain.setWriteDate(model.getWrite_date());
        if(model.getCreate_dateDirtyFlag())
            domain.setCreateDate(model.getCreate_date());
        if(model.getBrand_id_textDirtyFlag())
            domain.setBrandIdText(model.getBrand_id_text());
        if(model.getCreate_uid_textDirtyFlag())
            domain.setCreateUidText(model.getCreate_uid_text());
        if(model.getImage_smallDirtyFlag())
            domain.setImageSmall(model.getImage_small());
        if(model.getWrite_uid_textDirtyFlag())
            domain.setWriteUidText(model.getWrite_uid_text());
        if(model.getImage_mediumDirtyFlag())
            domain.setImageMedium(model.getImage_medium());
        if(model.getImageDirtyFlag())
            domain.setImage(model.getImage());
        if(model.getBrand_idDirtyFlag())
            domain.setBrandId(model.getBrand_id());
        if(model.getCreate_uidDirtyFlag())
            domain.setCreateUid(model.getCreate_uid());
        if(model.getWrite_uidDirtyFlag())
            domain.setWriteUid(model.getWrite_uid());
        return domain ;
    }

}

    



