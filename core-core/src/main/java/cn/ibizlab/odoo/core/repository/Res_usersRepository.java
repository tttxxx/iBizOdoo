package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Res_users;
import cn.ibizlab.odoo.core.odoo_base.filter.Res_usersSearchContext;

/**
 * 实体 [用户] 存储对象
 */
public interface Res_usersRepository extends Repository<Res_users> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Res_users> searchDefault(Res_usersSearchContext context);

    Res_users convert2PO(cn.ibizlab.odoo.core.odoo_base.domain.Res_users domain , Res_users po) ;

    cn.ibizlab.odoo.core.odoo_base.domain.Res_users convert2Domain( Res_users po ,cn.ibizlab.odoo.core.odoo_base.domain.Res_users domain) ;

}
