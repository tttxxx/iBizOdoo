package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Isale_order_template;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[sale_order_template] 服务对象接口
 */
public interface Isale_order_templateClientService{

    public Isale_order_template createModel() ;

    public Page<Isale_order_template> search(SearchContext context);

    public void create(Isale_order_template sale_order_template);

    public void remove(Isale_order_template sale_order_template);

    public void updateBatch(List<Isale_order_template> sale_order_templates);

    public void update(Isale_order_template sale_order_template);

    public void get(Isale_order_template sale_order_template);

    public void removeBatch(List<Isale_order_template> sale_order_templates);

    public void createBatch(List<Isale_order_template> sale_order_templates);

    public Page<Isale_order_template> select(SearchContext context);

}
