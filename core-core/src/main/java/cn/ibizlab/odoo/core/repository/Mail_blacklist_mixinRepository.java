package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Mail_blacklist_mixin;
import cn.ibizlab.odoo.core.odoo_mail.filter.Mail_blacklist_mixinSearchContext;

/**
 * 实体 [mixin邮件黑名单] 存储对象
 */
public interface Mail_blacklist_mixinRepository extends Repository<Mail_blacklist_mixin> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Mail_blacklist_mixin> searchDefault(Mail_blacklist_mixinSearchContext context);

    Mail_blacklist_mixin convert2PO(cn.ibizlab.odoo.core.odoo_mail.domain.Mail_blacklist_mixin domain , Mail_blacklist_mixin po) ;

    cn.ibizlab.odoo.core.odoo_mail.domain.Mail_blacklist_mixin convert2Domain( Mail_blacklist_mixin po ,cn.ibizlab.odoo.core.odoo_mail.domain.Mail_blacklist_mixin domain) ;

}
