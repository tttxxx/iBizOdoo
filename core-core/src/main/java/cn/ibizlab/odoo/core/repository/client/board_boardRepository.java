package cn.ibizlab.odoo.core.repository.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import cn.ibizlab.odoo.core.repository.client.po.board_board;

/**
 * 实体[board_board] 服务对象接口
 */
public interface board_boardRepository{


    public board_board createPO() ;
        public void removeBatch(String id);

        public void remove(String id);

        public void create(board_board board_board);

        public void update(board_board board_board);

        public List<board_board> search();

        public void createBatch(board_board board_board);

        public void get(String id);

        public void updateBatch(board_board board_board);


}
