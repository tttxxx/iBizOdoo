package cn.ibizlab.odoo.core.repository.po ;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.math.BigInteger;

import cn.ibizlab.odoo.core.odoo_mail.filter.Mail_notificationSearchContext;

/**
 * 实体 [消息通知] 存储模型
 */
public interface Mail_notification{

    /**
     * 最后修改日
     */
    Timestamp get__last_update();

    void set__last_update(Timestamp __last_update);

    /**
     * 获取 [最后修改日]脏标记
     */
    boolean get__last_updateDirtyFlag();

    /**
     * ID
     */
    Integer getId();

    void setId(Integer id);

    /**
     * 获取 [ID]脏标记
     */
    boolean getIdDirtyFlag();

    /**
     * 失败原因
     */
    String getFailure_reason();

    void setFailure_reason(String failure_reason);

    /**
     * 获取 [失败原因]脏标记
     */
    boolean getFailure_reasonDirtyFlag();

    /**
     * 以邮件发送
     */
    String getIs_email();

    void setIs_email(String is_email);

    /**
     * 获取 [以邮件发送]脏标记
     */
    boolean getIs_emailDirtyFlag();

    /**
     * 已读
     */
    String getIs_read();

    void setIs_read(String is_read);

    /**
     * 获取 [已读]脏标记
     */
    boolean getIs_readDirtyFlag();

    /**
     * EMail状态
     */
    String getEmail_status();

    void setEmail_status(String email_status);

    /**
     * 获取 [EMail状态]脏标记
     */
    boolean getEmail_statusDirtyFlag();

    /**
     * 显示名称
     */
    String getDisplay_name();

    void setDisplay_name(String display_name);

    /**
     * 获取 [显示名称]脏标记
     */
    boolean getDisplay_nameDirtyFlag();

    /**
     * 失败类型
     */
    String getFailure_type();

    void setFailure_type(String failure_type);

    /**
     * 获取 [失败类型]脏标记
     */
    boolean getFailure_typeDirtyFlag();

    /**
     * 需收件人
     */
    String getRes_partner_id_text();

    void setRes_partner_id_text(String res_partner_id_text);

    /**
     * 获取 [需收件人]脏标记
     */
    boolean getRes_partner_id_textDirtyFlag();

    /**
     * 邮件
     */
    Integer getMail_id();

    void setMail_id(Integer mail_id);

    /**
     * 获取 [邮件]脏标记
     */
    boolean getMail_idDirtyFlag();

    /**
     * 消息
     */
    Integer getMail_message_id();

    void setMail_message_id(Integer mail_message_id);

    /**
     * 获取 [消息]脏标记
     */
    boolean getMail_message_idDirtyFlag();

    /**
     * 需收件人
     */
    Integer getRes_partner_id();

    void setRes_partner_id(Integer res_partner_id);

    /**
     * 获取 [需收件人]脏标记
     */
    boolean getRes_partner_idDirtyFlag();

}
