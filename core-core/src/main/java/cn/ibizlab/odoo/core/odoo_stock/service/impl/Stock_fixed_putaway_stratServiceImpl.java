package cn.ibizlab.odoo.core.odoo_stock.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_stock.domain.Stock_fixed_putaway_strat;
import cn.ibizlab.odoo.core.odoo_stock.filter.Stock_fixed_putaway_stratSearchContext;
import cn.ibizlab.odoo.core.odoo_stock.service.IStock_fixed_putaway_stratService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;



import cn.ibizlab.odoo.core.odoo_stock.client.stock_fixed_putaway_stratOdooClient;
import cn.ibizlab.odoo.core.odoo_stock.clientmodel.stock_fixed_putaway_stratClientModel;

/**
 * 实体[位置固定上架策略] 服务对象接口实现
 */
@Slf4j
@Service
public class Stock_fixed_putaway_stratServiceImpl implements IStock_fixed_putaway_stratService {

    @Autowired
    stock_fixed_putaway_stratOdooClient stock_fixed_putaway_stratOdooClient;


    @Override
    public boolean remove(Integer id) {
        stock_fixed_putaway_stratClientModel clientModel = new stock_fixed_putaway_stratClientModel();
        clientModel.setId(id);
		stock_fixed_putaway_stratOdooClient.remove(clientModel);
        return true; 
    }

    public void removeBatch(Collection<Integer> idList){
    }

    @Override
    public Stock_fixed_putaway_strat get(Integer id) {
        stock_fixed_putaway_stratClientModel clientModel = new stock_fixed_putaway_stratClientModel();
        clientModel.setId(id);
		stock_fixed_putaway_stratOdooClient.get(clientModel);
        Stock_fixed_putaway_strat et = convert2Domain(clientModel,null);
        if(et==null){
            et=new Stock_fixed_putaway_strat();
            et.setId(id);
        }
        else{
        }

        return  et;
    }

    @Override
    public boolean update(Stock_fixed_putaway_strat et) {
        stock_fixed_putaway_stratClientModel clientModel = convert2Model(et,null);
		stock_fixed_putaway_stratOdooClient.update(clientModel);
        Stock_fixed_putaway_strat rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }
    
    public void updateBatch(List<Stock_fixed_putaway_strat> list){
    }

    @Override
    public boolean create(Stock_fixed_putaway_strat et) {
        stock_fixed_putaway_stratClientModel clientModel = convert2Model(et,null);
		stock_fixed_putaway_stratOdooClient.create(clientModel);
        Stock_fixed_putaway_strat rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Stock_fixed_putaway_strat> list){
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Stock_fixed_putaway_strat> searchDefault(Stock_fixed_putaway_stratSearchContext context) {
        List<Stock_fixed_putaway_strat> list = new ArrayList<Stock_fixed_putaway_strat>();
        Page<stock_fixed_putaway_stratClientModel> clientModelList = stock_fixed_putaway_stratOdooClient.search(context);
        for(stock_fixed_putaway_stratClientModel clientModel : clientModelList){
            list.add( this.convert2Domain(clientModel,null));
        }
        return new PageImpl<Stock_fixed_putaway_strat>(list, context.getPageable(), clientModelList.getTotalElements());
    }

    public stock_fixed_putaway_stratClientModel convert2Model(Stock_fixed_putaway_strat domain , stock_fixed_putaway_stratClientModel model) {
        if(domain == null)
            return model ;
        if(model==null){
            model = new stock_fixed_putaway_stratClientModel();

        }
        if((Boolean) domain.getExtensionparams().get("dirtyflagenable")){
            if((Boolean) domain.getExtensionparams().get("write_datedirtyflag"))
                model.setWrite_date(domain.getWriteDate());
            if((Boolean) domain.getExtensionparams().get("iddirtyflag"))
                model.setId(domain.getId());
            if((Boolean) domain.getExtensionparams().get("create_datedirtyflag"))
                model.setCreate_date(domain.getCreateDate());
            if((Boolean) domain.getExtensionparams().get("sequencedirtyflag"))
                model.setSequence(domain.getSequence());
            if((Boolean) domain.getExtensionparams().get("display_namedirtyflag"))
                model.setDisplay_name(domain.getDisplayName());
            if((Boolean) domain.getExtensionparams().get("__last_updatedirtyflag"))
                model.set__last_update(domain.getLastUpdate());
            if((Boolean) domain.getExtensionparams().get("category_id_textdirtyflag"))
                model.setCategory_id_text(domain.getCategoryIdText());
            if((Boolean) domain.getExtensionparams().get("fixed_location_id_textdirtyflag"))
                model.setFixed_location_id_text(domain.getFixedLocationIdText());
            if((Boolean) domain.getExtensionparams().get("write_uid_textdirtyflag"))
                model.setWrite_uid_text(domain.getWriteUidText());
            if((Boolean) domain.getExtensionparams().get("product_id_textdirtyflag"))
                model.setProduct_id_text(domain.getProductIdText());
            if((Boolean) domain.getExtensionparams().get("putaway_id_textdirtyflag"))
                model.setPutaway_id_text(domain.getPutawayIdText());
            if((Boolean) domain.getExtensionparams().get("create_uid_textdirtyflag"))
                model.setCreate_uid_text(domain.getCreateUidText());
            if((Boolean) domain.getExtensionparams().get("putaway_iddirtyflag"))
                model.setPutaway_id(domain.getPutawayId());
            if((Boolean) domain.getExtensionparams().get("category_iddirtyflag"))
                model.setCategory_id(domain.getCategoryId());
            if((Boolean) domain.getExtensionparams().get("write_uiddirtyflag"))
                model.setWrite_uid(domain.getWriteUid());
            if((Boolean) domain.getExtensionparams().get("product_iddirtyflag"))
                model.setProduct_id(domain.getProductId());
            if((Boolean) domain.getExtensionparams().get("create_uiddirtyflag"))
                model.setCreate_uid(domain.getCreateUid());
            if((Boolean) domain.getExtensionparams().get("fixed_location_iddirtyflag"))
                model.setFixed_location_id(domain.getFixedLocationId());
            domain.getFocusNull().clear();
        }
        return model ;
    }

    public Stock_fixed_putaway_strat convert2Domain( stock_fixed_putaway_stratClientModel model ,Stock_fixed_putaway_strat domain) {
        if(model == null)
            return domain ;
        if(domain == null){
            domain = new Stock_fixed_putaway_strat();
        }

        if(model.getWrite_dateDirtyFlag())
            domain.setWriteDate(model.getWrite_date());
        if(model.getIdDirtyFlag())
            domain.setId(model.getId());
        if(model.getCreate_dateDirtyFlag())
            domain.setCreateDate(model.getCreate_date());
        if(model.getSequenceDirtyFlag())
            domain.setSequence(model.getSequence());
        if(model.getDisplay_nameDirtyFlag())
            domain.setDisplayName(model.getDisplay_name());
        if(model.get__last_updateDirtyFlag())
            domain.setLastUpdate(model.get__last_update());
        if(model.getCategory_id_textDirtyFlag())
            domain.setCategoryIdText(model.getCategory_id_text());
        if(model.getFixed_location_id_textDirtyFlag())
            domain.setFixedLocationIdText(model.getFixed_location_id_text());
        if(model.getWrite_uid_textDirtyFlag())
            domain.setWriteUidText(model.getWrite_uid_text());
        if(model.getProduct_id_textDirtyFlag())
            domain.setProductIdText(model.getProduct_id_text());
        if(model.getPutaway_id_textDirtyFlag())
            domain.setPutawayIdText(model.getPutaway_id_text());
        if(model.getCreate_uid_textDirtyFlag())
            domain.setCreateUidText(model.getCreate_uid_text());
        if(model.getPutaway_idDirtyFlag())
            domain.setPutawayId(model.getPutaway_id());
        if(model.getCategory_idDirtyFlag())
            domain.setCategoryId(model.getCategory_id());
        if(model.getWrite_uidDirtyFlag())
            domain.setWriteUid(model.getWrite_uid());
        if(model.getProduct_idDirtyFlag())
            domain.setProductId(model.getProduct_id());
        if(model.getCreate_uidDirtyFlag())
            domain.setCreateUid(model.getCreate_uid());
        if(model.getFixed_location_idDirtyFlag())
            domain.setFixedLocationId(model.getFixed_location_id());
        return domain ;
    }

}

    



