package cn.ibizlab.odoo.core.repository.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import cn.ibizlab.odoo.core.repository.client.po.event_type;

/**
 * 实体[event_type] 服务对象接口
 */
public interface event_typeRepository{


    public event_type createPO() ;
        public void updateBatch(event_type event_type);

        public void createBatch(event_type event_type);

        public void removeBatch(String id);

        public List<event_type> search();

        public void get(String id);

        public void create(event_type event_type);

        public void remove(String id);

        public void update(event_type event_type);


}
