package cn.ibizlab.odoo.core.repository.po ;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.math.BigInteger;

import cn.ibizlab.odoo.core.odoo_repair.filter.Repair_feeSearchContext;

/**
 * 实体 [修理费] 存储模型
 */
public interface Repair_fee{

    /**
     * 创建时间
     */
    Timestamp getCreate_date();

    void setCreate_date(Timestamp create_date);

    /**
     * 获取 [创建时间]脏标记
     */
    boolean getCreate_dateDirtyFlag();

    /**
     * 显示名称
     */
    String getDisplay_name();

    void setDisplay_name(String display_name);

    /**
     * 获取 [显示名称]脏标记
     */
    boolean getDisplay_nameDirtyFlag();

    /**
     * 单价
     */
    Double getPrice_unit();

    void setPrice_unit(Double price_unit);

    /**
     * 获取 [单价]脏标记
     */
    boolean getPrice_unitDirtyFlag();

    /**
     * 税
     */
    String getTax_id();

    void setTax_id(String tax_id);

    /**
     * 获取 [税]脏标记
     */
    boolean getTax_idDirtyFlag();

    /**
     * 描述
     */
    String getName();

    void setName(String name);

    /**
     * 获取 [描述]脏标记
     */
    boolean getNameDirtyFlag();

    /**
     * ID
     */
    Integer getId();

    void setId(Integer id);

    /**
     * 获取 [ID]脏标记
     */
    boolean getIdDirtyFlag();

    /**
     * 最后修改时间
     */
    Timestamp get__last_update();

    void set__last_update(Timestamp __last_update);

    /**
     * 获取 [最后修改时间]脏标记
     */
    boolean get__last_updateDirtyFlag();

    /**
     * 最后更新时间
     */
    Timestamp getWrite_date();

    void setWrite_date(Timestamp write_date);

    /**
     * 获取 [最后更新时间]脏标记
     */
    boolean getWrite_dateDirtyFlag();

    /**
     * 数量
     */
    Double getProduct_uom_qty();

    void setProduct_uom_qty(Double product_uom_qty);

    /**
     * 获取 [数量]脏标记
     */
    boolean getProduct_uom_qtyDirtyFlag();

    /**
     * 已开票
     */
    String getInvoiced();

    void setInvoiced(String invoiced);

    /**
     * 获取 [已开票]脏标记
     */
    boolean getInvoicedDirtyFlag();

    /**
     * 小计
     */
    Double getPrice_subtotal();

    void setPrice_subtotal(Double price_subtotal);

    /**
     * 获取 [小计]脏标记
     */
    boolean getPrice_subtotalDirtyFlag();

    /**
     * 产品
     */
    String getProduct_id_text();

    void setProduct_id_text(String product_id_text);

    /**
     * 获取 [产品]脏标记
     */
    boolean getProduct_id_textDirtyFlag();

    /**
     * 最后更新人
     */
    String getWrite_uid_text();

    void setWrite_uid_text(String write_uid_text);

    /**
     * 获取 [最后更新人]脏标记
     */
    boolean getWrite_uid_textDirtyFlag();

    /**
     * 产品量度单位
     */
    String getProduct_uom_text();

    void setProduct_uom_text(String product_uom_text);

    /**
     * 获取 [产品量度单位]脏标记
     */
    boolean getProduct_uom_textDirtyFlag();

    /**
     * 发票明细
     */
    String getInvoice_line_id_text();

    void setInvoice_line_id_text(String invoice_line_id_text);

    /**
     * 获取 [发票明细]脏标记
     */
    boolean getInvoice_line_id_textDirtyFlag();

    /**
     * 维修单编＃
     */
    String getRepair_id_text();

    void setRepair_id_text(String repair_id_text);

    /**
     * 获取 [维修单编＃]脏标记
     */
    boolean getRepair_id_textDirtyFlag();

    /**
     * 创建者
     */
    String getCreate_uid_text();

    void setCreate_uid_text(String create_uid_text);

    /**
     * 获取 [创建者]脏标记
     */
    boolean getCreate_uid_textDirtyFlag();

    /**
     * 发票明细
     */
    Integer getInvoice_line_id();

    void setInvoice_line_id(Integer invoice_line_id);

    /**
     * 获取 [发票明细]脏标记
     */
    boolean getInvoice_line_idDirtyFlag();

    /**
     * 产品量度单位
     */
    Integer getProduct_uom();

    void setProduct_uom(Integer product_uom);

    /**
     * 获取 [产品量度单位]脏标记
     */
    boolean getProduct_uomDirtyFlag();

    /**
     * 维修单编＃
     */
    Integer getRepair_id();

    void setRepair_id(Integer repair_id);

    /**
     * 获取 [维修单编＃]脏标记
     */
    boolean getRepair_idDirtyFlag();

    /**
     * 创建者
     */
    Integer getCreate_uid();

    void setCreate_uid(Integer create_uid);

    /**
     * 获取 [创建者]脏标记
     */
    boolean getCreate_uidDirtyFlag();

    /**
     * 最后更新人
     */
    Integer getWrite_uid();

    void setWrite_uid(Integer write_uid);

    /**
     * 获取 [最后更新人]脏标记
     */
    boolean getWrite_uidDirtyFlag();

    /**
     * 产品
     */
    Integer getProduct_id();

    void setProduct_id(Integer product_id);

    /**
     * 获取 [产品]脏标记
     */
    boolean getProduct_idDirtyFlag();

}
