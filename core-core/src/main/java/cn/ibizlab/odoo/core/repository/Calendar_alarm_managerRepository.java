package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Calendar_alarm_manager;
import cn.ibizlab.odoo.core.odoo_calendar.filter.Calendar_alarm_managerSearchContext;

/**
 * 实体 [活动提醒管理] 存储对象
 */
public interface Calendar_alarm_managerRepository extends Repository<Calendar_alarm_manager> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Calendar_alarm_manager> searchDefault(Calendar_alarm_managerSearchContext context);

    Calendar_alarm_manager convert2PO(cn.ibizlab.odoo.core.odoo_calendar.domain.Calendar_alarm_manager domain , Calendar_alarm_manager po) ;

    cn.ibizlab.odoo.core.odoo_calendar.domain.Calendar_alarm_manager convert2Domain( Calendar_alarm_manager po ,cn.ibizlab.odoo.core.odoo_calendar.domain.Calendar_alarm_manager domain) ;

}
