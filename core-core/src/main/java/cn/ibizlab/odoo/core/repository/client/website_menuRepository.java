package cn.ibizlab.odoo.core.repository.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import cn.ibizlab.odoo.core.repository.client.po.website_menu;

/**
 * 实体[website_menu] 服务对象接口
 */
public interface website_menuRepository{


    public website_menu createPO() ;
        public void get(String id);

        public void removeBatch(String id);

        public void updateBatch(website_menu website_menu);

        public void create(website_menu website_menu);

        public void remove(String id);

        public void update(website_menu website_menu);

        public List<website_menu> search();

        public void createBatch(website_menu website_menu);


}
