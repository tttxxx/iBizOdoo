package cn.ibizlab.odoo.core.odoo_account.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_account.domain.Account_analytic_group;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_analytic_groupSearchContext;
import cn.ibizlab.odoo.core.odoo_account.service.IAccount_analytic_groupService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;



import cn.ibizlab.odoo.core.odoo_account.client.account_analytic_groupOdooClient;
import cn.ibizlab.odoo.core.odoo_account.clientmodel.account_analytic_groupClientModel;

/**
 * 实体[分析类别] 服务对象接口实现
 */
@Slf4j
@Service
public class Account_analytic_groupServiceImpl implements IAccount_analytic_groupService {

    @Autowired
    account_analytic_groupOdooClient account_analytic_groupOdooClient;


    @Override
    public boolean remove(Integer id) {
        account_analytic_groupClientModel clientModel = new account_analytic_groupClientModel();
        clientModel.setId(id);
		account_analytic_groupOdooClient.remove(clientModel);
        return true; 
    }

    public void removeBatch(Collection<Integer> idList){
    }

    @Override
    public boolean create(Account_analytic_group et) {
        account_analytic_groupClientModel clientModel = convert2Model(et,null);
		account_analytic_groupOdooClient.create(clientModel);
        Account_analytic_group rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Account_analytic_group> list){
    }

    @Override
    public boolean update(Account_analytic_group et) {
        account_analytic_groupClientModel clientModel = convert2Model(et,null);
		account_analytic_groupOdooClient.update(clientModel);
        Account_analytic_group rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }
    
    public void updateBatch(List<Account_analytic_group> list){
    }

    @Override
    public Account_analytic_group get(Integer id) {
        account_analytic_groupClientModel clientModel = new account_analytic_groupClientModel();
        clientModel.setId(id);
		account_analytic_groupOdooClient.get(clientModel);
        Account_analytic_group et = convert2Domain(clientModel,null);
        if(et==null){
            et=new Account_analytic_group();
            et.setId(id);
        }
        else{
        }

        return  et;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Account_analytic_group> searchDefault(Account_analytic_groupSearchContext context) {
        List<Account_analytic_group> list = new ArrayList<Account_analytic_group>();
        Page<account_analytic_groupClientModel> clientModelList = account_analytic_groupOdooClient.search(context);
        for(account_analytic_groupClientModel clientModel : clientModelList){
            list.add( this.convert2Domain(clientModel,null));
        }
        return new PageImpl<Account_analytic_group>(list, context.getPageable(), clientModelList.getTotalElements());
    }

    public account_analytic_groupClientModel convert2Model(Account_analytic_group domain , account_analytic_groupClientModel model) {
        if(domain == null)
            return model ;
        if(model==null){
            model = new account_analytic_groupClientModel();

        }
        if((Boolean) domain.getExtensionparams().get("dirtyflagenable")){
            if((Boolean) domain.getExtensionparams().get("iddirtyflag"))
                model.setId(domain.getId());
            if((Boolean) domain.getExtensionparams().get("namedirtyflag"))
                model.setName(domain.getName());
            if((Boolean) domain.getExtensionparams().get("__last_updatedirtyflag"))
                model.set__last_update(domain.getLastUpdate());
            if((Boolean) domain.getExtensionparams().get("parent_pathdirtyflag"))
                model.setParent_path(domain.getParentPath());
            if((Boolean) domain.getExtensionparams().get("create_datedirtyflag"))
                model.setCreate_date(domain.getCreateDate());
            if((Boolean) domain.getExtensionparams().get("display_namedirtyflag"))
                model.setDisplay_name(domain.getDisplayName());
            if((Boolean) domain.getExtensionparams().get("complete_namedirtyflag"))
                model.setComplete_name(domain.getCompleteName());
            if((Boolean) domain.getExtensionparams().get("descriptiondirtyflag"))
                model.setDescription(domain.getDescription());
            if((Boolean) domain.getExtensionparams().get("children_idsdirtyflag"))
                model.setChildren_ids(domain.getChildrenIds());
            if((Boolean) domain.getExtensionparams().get("write_datedirtyflag"))
                model.setWrite_date(domain.getWriteDate());
            if((Boolean) domain.getExtensionparams().get("parent_id_textdirtyflag"))
                model.setParent_id_text(domain.getParentIdText());
            if((Boolean) domain.getExtensionparams().get("write_uid_textdirtyflag"))
                model.setWrite_uid_text(domain.getWriteUidText());
            if((Boolean) domain.getExtensionparams().get("create_uid_textdirtyflag"))
                model.setCreate_uid_text(domain.getCreateUidText());
            if((Boolean) domain.getExtensionparams().get("company_id_textdirtyflag"))
                model.setCompany_id_text(domain.getCompanyIdText());
            if((Boolean) domain.getExtensionparams().get("write_uiddirtyflag"))
                model.setWrite_uid(domain.getWriteUid());
            if((Boolean) domain.getExtensionparams().get("create_uiddirtyflag"))
                model.setCreate_uid(domain.getCreateUid());
            if((Boolean) domain.getExtensionparams().get("company_iddirtyflag"))
                model.setCompany_id(domain.getCompanyId());
            if((Boolean) domain.getExtensionparams().get("parent_iddirtyflag"))
                model.setParent_id(domain.getParentId());
            domain.getFocusNull().clear();
        }
        return model ;
    }

    public Account_analytic_group convert2Domain( account_analytic_groupClientModel model ,Account_analytic_group domain) {
        if(model == null)
            return domain ;
        if(domain == null){
            domain = new Account_analytic_group();
        }

        if(model.getIdDirtyFlag())
            domain.setId(model.getId());
        if(model.getNameDirtyFlag())
            domain.setName(model.getName());
        if(model.get__last_updateDirtyFlag())
            domain.setLastUpdate(model.get__last_update());
        if(model.getParent_pathDirtyFlag())
            domain.setParentPath(model.getParent_path());
        if(model.getCreate_dateDirtyFlag())
            domain.setCreateDate(model.getCreate_date());
        if(model.getDisplay_nameDirtyFlag())
            domain.setDisplayName(model.getDisplay_name());
        if(model.getComplete_nameDirtyFlag())
            domain.setCompleteName(model.getComplete_name());
        if(model.getDescriptionDirtyFlag())
            domain.setDescription(model.getDescription());
        if(model.getChildren_idsDirtyFlag())
            domain.setChildrenIds(model.getChildren_ids());
        if(model.getWrite_dateDirtyFlag())
            domain.setWriteDate(model.getWrite_date());
        if(model.getParent_id_textDirtyFlag())
            domain.setParentIdText(model.getParent_id_text());
        if(model.getWrite_uid_textDirtyFlag())
            domain.setWriteUidText(model.getWrite_uid_text());
        if(model.getCreate_uid_textDirtyFlag())
            domain.setCreateUidText(model.getCreate_uid_text());
        if(model.getCompany_id_textDirtyFlag())
            domain.setCompanyIdText(model.getCompany_id_text());
        if(model.getWrite_uidDirtyFlag())
            domain.setWriteUid(model.getWrite_uid());
        if(model.getCreate_uidDirtyFlag())
            domain.setCreateUid(model.getCreate_uid());
        if(model.getCompany_idDirtyFlag())
            domain.setCompanyId(model.getCompany_id());
        if(model.getParent_idDirtyFlag())
            domain.setParentId(model.getParent_id());
        return domain ;
    }

}

    



