package cn.ibizlab.odoo.core.odoo_base.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_base.domain.Base_partner_merge_line;
import cn.ibizlab.odoo.core.odoo_base.filter.Base_partner_merge_lineSearchContext;
import cn.ibizlab.odoo.core.odoo_base.service.IBase_partner_merge_lineService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;



import cn.ibizlab.odoo.core.odoo_base.client.base_partner_merge_lineOdooClient;
import cn.ibizlab.odoo.core.odoo_base.clientmodel.base_partner_merge_lineClientModel;

/**
 * 实体[合并合作伙伴明细行] 服务对象接口实现
 */
@Slf4j
@Service
public class Base_partner_merge_lineServiceImpl implements IBase_partner_merge_lineService {

    @Autowired
    base_partner_merge_lineOdooClient base_partner_merge_lineOdooClient;


    @Override
    public Base_partner_merge_line get(Integer id) {
        base_partner_merge_lineClientModel clientModel = new base_partner_merge_lineClientModel();
        clientModel.setId(id);
		base_partner_merge_lineOdooClient.get(clientModel);
        Base_partner_merge_line et = convert2Domain(clientModel,null);
        if(et==null){
            et=new Base_partner_merge_line();
            et.setId(id);
        }
        else{
        }

        return  et;
    }

    @Override
    public boolean update(Base_partner_merge_line et) {
        base_partner_merge_lineClientModel clientModel = convert2Model(et,null);
		base_partner_merge_lineOdooClient.update(clientModel);
        Base_partner_merge_line rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }
    
    public void updateBatch(List<Base_partner_merge_line> list){
    }

    @Override
    public boolean create(Base_partner_merge_line et) {
        base_partner_merge_lineClientModel clientModel = convert2Model(et,null);
		base_partner_merge_lineOdooClient.create(clientModel);
        Base_partner_merge_line rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Base_partner_merge_line> list){
    }

    @Override
    public boolean remove(Integer id) {
        base_partner_merge_lineClientModel clientModel = new base_partner_merge_lineClientModel();
        clientModel.setId(id);
		base_partner_merge_lineOdooClient.remove(clientModel);
        return true; 
    }

    public void removeBatch(Collection<Integer> idList){
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Base_partner_merge_line> searchDefault(Base_partner_merge_lineSearchContext context) {
        List<Base_partner_merge_line> list = new ArrayList<Base_partner_merge_line>();
        Page<base_partner_merge_lineClientModel> clientModelList = base_partner_merge_lineOdooClient.search(context);
        for(base_partner_merge_lineClientModel clientModel : clientModelList){
            list.add( this.convert2Domain(clientModel,null));
        }
        return new PageImpl<Base_partner_merge_line>(list, context.getPageable(), clientModelList.getTotalElements());
    }

    public base_partner_merge_lineClientModel convert2Model(Base_partner_merge_line domain , base_partner_merge_lineClientModel model) {
        if(domain == null)
            return model ;
        if(model==null){
            model = new base_partner_merge_lineClientModel();

        }
        if((Boolean) domain.getExtensionparams().get("dirtyflagenable")){
            if((Boolean) domain.getExtensionparams().get("write_datedirtyflag"))
                model.setWrite_date(domain.getWriteDate());
            if((Boolean) domain.getExtensionparams().get("aggr_idsdirtyflag"))
                model.setAggr_ids(domain.getAggrIds());
            if((Boolean) domain.getExtensionparams().get("iddirtyflag"))
                model.setId(domain.getId());
            if((Boolean) domain.getExtensionparams().get("create_datedirtyflag"))
                model.setCreate_date(domain.getCreateDate());
            if((Boolean) domain.getExtensionparams().get("min_iddirtyflag"))
                model.setMin_id(domain.getMinId());
            if((Boolean) domain.getExtensionparams().get("display_namedirtyflag"))
                model.setDisplay_name(domain.getDisplayName());
            if((Boolean) domain.getExtensionparams().get("__last_updatedirtyflag"))
                model.set__last_update(domain.getLastUpdate());
            if((Boolean) domain.getExtensionparams().get("create_uid_textdirtyflag"))
                model.setCreate_uid_text(domain.getCreateUidText());
            if((Boolean) domain.getExtensionparams().get("write_uid_textdirtyflag"))
                model.setWrite_uid_text(domain.getWriteUidText());
            if((Boolean) domain.getExtensionparams().get("write_uiddirtyflag"))
                model.setWrite_uid(domain.getWriteUid());
            if((Boolean) domain.getExtensionparams().get("wizard_iddirtyflag"))
                model.setWizard_id(domain.getWizardId());
            if((Boolean) domain.getExtensionparams().get("create_uiddirtyflag"))
                model.setCreate_uid(domain.getCreateUid());
            domain.getFocusNull().clear();
        }
        return model ;
    }

    public Base_partner_merge_line convert2Domain( base_partner_merge_lineClientModel model ,Base_partner_merge_line domain) {
        if(model == null)
            return domain ;
        if(domain == null){
            domain = new Base_partner_merge_line();
        }

        if(model.getWrite_dateDirtyFlag())
            domain.setWriteDate(model.getWrite_date());
        if(model.getAggr_idsDirtyFlag())
            domain.setAggrIds(model.getAggr_ids());
        if(model.getIdDirtyFlag())
            domain.setId(model.getId());
        if(model.getCreate_dateDirtyFlag())
            domain.setCreateDate(model.getCreate_date());
        if(model.getMin_idDirtyFlag())
            domain.setMinId(model.getMin_id());
        if(model.getDisplay_nameDirtyFlag())
            domain.setDisplayName(model.getDisplay_name());
        if(model.get__last_updateDirtyFlag())
            domain.setLastUpdate(model.get__last_update());
        if(model.getCreate_uid_textDirtyFlag())
            domain.setCreateUidText(model.getCreate_uid_text());
        if(model.getWrite_uid_textDirtyFlag())
            domain.setWriteUidText(model.getWrite_uid_text());
        if(model.getWrite_uidDirtyFlag())
            domain.setWriteUid(model.getWrite_uid());
        if(model.getWizard_idDirtyFlag())
            domain.setWizardId(model.getWizard_id());
        if(model.getCreate_uidDirtyFlag())
            domain.setCreateUid(model.getCreate_uid());
        return domain ;
    }

}

    



