package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Crm_merge_opportunity;
import cn.ibizlab.odoo.core.odoo_crm.filter.Crm_merge_opportunitySearchContext;

/**
 * 实体 [合并商机] 存储对象
 */
public interface Crm_merge_opportunityRepository extends Repository<Crm_merge_opportunity> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Crm_merge_opportunity> searchDefault(Crm_merge_opportunitySearchContext context);

    Crm_merge_opportunity convert2PO(cn.ibizlab.odoo.core.odoo_crm.domain.Crm_merge_opportunity domain , Crm_merge_opportunity po) ;

    cn.ibizlab.odoo.core.odoo_crm.domain.Crm_merge_opportunity convert2Domain( Crm_merge_opportunity po ,cn.ibizlab.odoo.core.odoo_crm.domain.Crm_merge_opportunity domain) ;

}
