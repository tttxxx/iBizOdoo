package cn.ibizlab.odoo.core.repository.po ;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.math.BigInteger;

import cn.ibizlab.odoo.core.odoo_account.filter.Account_invoiceSearchContext;

/**
 * 实体 [发票] 存储模型
 */
public interface Account_invoice{

    /**
     * 未到期贷项
     */
    String getOutstanding_credits_debits_widget();

    void setOutstanding_credits_debits_widget(String outstanding_credits_debits_widget);

    /**
     * 获取 [未到期贷项]脏标记
     */
    boolean getOutstanding_credits_debits_widgetDirtyFlag();

    /**
     * 下一活动截止日期
     */
    Timestamp getActivity_date_deadline();

    void setActivity_date_deadline(Timestamp activity_date_deadline);

    /**
     * 获取 [下一活动截止日期]脏标记
     */
    boolean getActivity_date_deadlineDirtyFlag();

    /**
     * 关注者(渠道)
     */
    String getMessage_channel_ids();

    void setMessage_channel_ids(String message_channel_ids);

    /**
     * 获取 [关注者(渠道)]脏标记
     */
    boolean getMessage_channel_idsDirtyFlag();

    /**
     * 日记账分录名称
     */
    String getMove_name();

    void setMove_name(String move_name);

    /**
     * 获取 [日记账分录名称]脏标记
     */
    boolean getMove_nameDirtyFlag();

    /**
     * 下一活动摘要
     */
    String getActivity_summary();

    void setActivity_summary(String activity_summary);

    /**
     * 获取 [下一活动摘要]脏标记
     */
    boolean getActivity_summaryDirtyFlag();

    /**
     * 门户访问网址
     */
    String getAccess_url();

    void setAccess_url(String access_url);

    /**
     * 获取 [门户访问网址]脏标记
     */
    boolean getAccess_urlDirtyFlag();

    /**
     * 付款凭证明细
     */
    String getPayment_move_line_ids();

    void setPayment_move_line_ids(String payment_move_line_ids);

    /**
     * 获取 [付款凭证明细]脏标记
     */
    boolean getPayment_move_line_idsDirtyFlag();

    /**
     * 源文档
     */
    String getOrigin();

    void setOrigin(String origin);

    /**
     * 获取 [源文档]脏标记
     */
    boolean getOriginDirtyFlag();

    /**
     * 会计日期
     */
    Timestamp getDate();

    void setDate(Timestamp date);

    /**
     * 获取 [会计日期]脏标记
     */
    boolean getDateDirtyFlag();

    /**
     * 公司货币的合计
     */
    Double getAmount_total_company_signed();

    void setAmount_total_company_signed(Double amount_total_company_signed);

    /**
     * 获取 [公司货币的合计]脏标记
     */
    boolean getAmount_total_company_signedDirtyFlag();

    /**
     * 操作编号
     */
    Integer getMessage_needaction_counter();

    void setMessage_needaction_counter(Integer message_needaction_counter);

    /**
     * 获取 [操作编号]脏标记
     */
    boolean getMessage_needaction_counterDirtyFlag();

    /**
     * 按公司本位币计的不含税金额
     */
    Double getAmount_untaxed_signed();

    void setAmount_untaxed_signed(Double amount_untaxed_signed);

    /**
     * 获取 [按公司本位币计的不含税金额]脏标记
     */
    boolean getAmount_untaxed_signedDirtyFlag();

    /**
     * 到期金额
     */
    Double getResidual();

    void setResidual(Double residual);

    /**
     * 获取 [到期金额]脏标记
     */
    boolean getResidualDirtyFlag();

    /**
     * 附件
     */
    Integer getMessage_main_attachment_id();

    void setMessage_main_attachment_id(Integer message_main_attachment_id);

    /**
     * 获取 [附件]脏标记
     */
    boolean getMessage_main_attachment_idDirtyFlag();

    /**
     * 有未清项
     */
    String getHas_outstanding();

    void setHas_outstanding(String has_outstanding);

    /**
     * 获取 [有未清项]脏标记
     */
    boolean getHas_outstandingDirtyFlag();

    /**
     * 额外的信息
     */
    String getComment();

    void setComment(String comment);

    /**
     * 获取 [额外的信息]脏标记
     */
    boolean getCommentDirtyFlag();

    /**
     * 未读消息
     */
    String getMessage_unread();

    void setMessage_unread(String message_unread);

    /**
     * 获取 [未读消息]脏标记
     */
    boolean getMessage_unreadDirtyFlag();

    /**
     * 下一活动类型
     */
    Integer getActivity_type_id();

    void setActivity_type_id(Integer activity_type_id);

    /**
     * 获取 [下一活动类型]脏标记
     */
    boolean getActivity_type_idDirtyFlag();

    /**
     * 按组分配税额
     */
    byte[] getAmount_by_group();

    void setAmount_by_group(byte[] amount_by_group);

    /**
     * 获取 [按组分配税额]脏标记
     */
    boolean getAmount_by_groupDirtyFlag();

    /**
     * 支付挂件
     */
    String getPayments_widget();

    void setPayments_widget(String payments_widget);

    /**
     * 获取 [支付挂件]脏标记
     */
    boolean getPayments_widgetDirtyFlag();

    /**
     * 已付／已核销
     */
    String getReconciled();

    void setReconciled(String reconciled);

    /**
     * 获取 [已付／已核销]脏标记
     */
    boolean getReconciledDirtyFlag();

    /**
     * 显示名称
     */
    String getDisplay_name();

    void setDisplay_name(String display_name);

    /**
     * 获取 [显示名称]脏标记
     */
    boolean getDisplay_nameDirtyFlag();

    /**
     * 已汇
     */
    String getSent();

    void setSent(String sent);

    /**
     * 获取 [已汇]脏标记
     */
    boolean getSentDirtyFlag();

    /**
     * 以发票币种总计
     */
    Double getAmount_total_signed();

    void setAmount_total_signed(Double amount_total_signed);

    /**
     * 获取 [以发票币种总计]脏标记
     */
    boolean getAmount_total_signedDirtyFlag();

    /**
     * 发票行
     */
    String getInvoice_line_ids();

    void setInvoice_line_ids(String invoice_line_ids);

    /**
     * 获取 [发票行]脏标记
     */
    boolean getInvoice_line_idsDirtyFlag();

    /**
     * 最后更新时间
     */
    Timestamp getWrite_date();

    void setWrite_date(Timestamp write_date);

    /**
     * 获取 [最后更新时间]脏标记
     */
    boolean getWrite_dateDirtyFlag();

    /**
     * 访问警告
     */
    String getAccess_warning();

    void setAccess_warning(String access_warning);

    /**
     * 获取 [访问警告]脏标记
     */
    boolean getAccess_warningDirtyFlag();

    /**
     * 创建时间
     */
    Timestamp getCreate_date();

    void setCreate_date(Timestamp create_date);

    /**
     * 获取 [创建时间]脏标记
     */
    boolean getCreate_dateDirtyFlag();

    /**
     * 活动状态
     */
    String getActivity_state();

    void setActivity_state(String activity_state);

    /**
     * 获取 [活动状态]脏标记
     */
    boolean getActivity_stateDirtyFlag();

    /**
     * 发票使用币种的逾期金额
     */
    Double getResidual_signed();

    void setResidual_signed(Double residual_signed);

    /**
     * 获取 [发票使用币种的逾期金额]脏标记
     */
    boolean getResidual_signedDirtyFlag();

    /**
     * 未读消息计数器
     */
    Integer getMessage_unread_counter();

    void setMessage_unread_counter(Integer message_unread_counter);

    /**
     * 获取 [未读消息计数器]脏标记
     */
    boolean getMessage_unread_counterDirtyFlag();

    /**
     * 安全令牌
     */
    String getAccess_token();

    void setAccess_token(String access_token);

    /**
     * 获取 [安全令牌]脏标记
     */
    boolean getAccess_tokenDirtyFlag();

    /**
     * 供应商名称
     */
    String getVendor_display_name();

    void setVendor_display_name(String vendor_display_name);

    /**
     * 获取 [供应商名称]脏标记
     */
    boolean getVendor_display_nameDirtyFlag();

    /**
     * 附件数量
     */
    Integer getMessage_attachment_count();

    void setMessage_attachment_count(Integer message_attachment_count);

    /**
     * 获取 [附件数量]脏标记
     */
    boolean getMessage_attachment_countDirtyFlag();

    /**
     * 消息
     */
    String getMessage_ids();

    void setMessage_ids(String message_ids);

    /**
     * 获取 [消息]脏标记
     */
    boolean getMessage_idsDirtyFlag();

    /**
     * 未税金额
     */
    Double getAmount_untaxed();

    void setAmount_untaxed(Double amount_untaxed);

    /**
     * 获取 [未税金额]脏标记
     */
    boolean getAmount_untaxedDirtyFlag();

    /**
     * 交易
     */
    String getTransaction_ids();

    void setTransaction_ids(String transaction_ids);

    /**
     * 获取 [交易]脏标记
     */
    boolean getTransaction_idsDirtyFlag();

    /**
     * 到期日期
     */
    Timestamp getDate_due();

    void setDate_due(Timestamp date_due);

    /**
     * 获取 [到期日期]脏标记
     */
    boolean getDate_dueDirtyFlag();

    /**
     * 关注者(业务伙伴)
     */
    String getMessage_partner_ids();

    void setMessage_partner_ids(String message_partner_ids);

    /**
     * 获取 [关注者(业务伙伴)]脏标记
     */
    boolean getMessage_partner_idsDirtyFlag();

    /**
     * 最后修改日
     */
    Timestamp get__last_update();

    void set__last_update(Timestamp __last_update);

    /**
     * 获取 [最后修改日]脏标记
     */
    boolean get__last_updateDirtyFlag();

    /**
     * 已授权的交易
     */
    String getAuthorized_transaction_ids();

    void setAuthorized_transaction_ids(String authorized_transaction_ids);

    /**
     * 获取 [已授权的交易]脏标记
     */
    boolean getAuthorized_transaction_idsDirtyFlag();

    /**
     * 税率明细行
     */
    String getTax_line_ids();

    void setTax_line_ids(String tax_line_ids);

    /**
     * 获取 [税率明细行]脏标记
     */
    boolean getTax_line_idsDirtyFlag();

    /**
     * 关注者
     */
    String getMessage_follower_ids();

    void setMessage_follower_ids(String message_follower_ids);

    /**
     * 获取 [关注者]脏标记
     */
    boolean getMessage_follower_idsDirtyFlag();

    /**
     * 源邮箱
     */
    String getSource_email();

    void setSource_email(String source_email);

    /**
     * 获取 [源邮箱]脏标记
     */
    boolean getSource_emailDirtyFlag();

    /**
     * 税率
     */
    Double getAmount_tax();

    void setAmount_tax(Double amount_tax);

    /**
     * 获取 [税率]脏标记
     */
    boolean getAmount_taxDirtyFlag();

    /**
     * 是关注者
     */
    String getMessage_is_follower();

    void setMessage_is_follower(String message_is_follower);

    /**
     * 获取 [是关注者]脏标记
     */
    boolean getMessage_is_followerDirtyFlag();

    /**
     * 状态
     */
    String getState();

    void setState(String state);

    /**
     * 获取 [状态]脏标记
     */
    boolean getStateDirtyFlag();

    /**
     * 消息递送错误
     */
    String getMessage_has_error();

    void setMessage_has_error(String message_has_error);

    /**
     * 获取 [消息递送错误]脏标记
     */
    boolean getMessage_has_errorDirtyFlag();

    /**
     * Untaxed Amount in Invoice Currency
     */
    Double getAmount_untaxed_invoice_signed();

    void setAmount_untaxed_invoice_signed(Double amount_untaxed_invoice_signed);

    /**
     * 获取 [Untaxed Amount in Invoice Currency]脏标记
     */
    boolean getAmount_untaxed_invoice_signedDirtyFlag();

    /**
     * 需要采取行动
     */
    String getMessage_needaction();

    void setMessage_needaction(String message_needaction);

    /**
     * 获取 [需要采取行动]脏标记
     */
    boolean getMessage_needactionDirtyFlag();

    /**
     * 下一个号码前缀
     */
    String getSequence_number_next_prefix();

    void setSequence_number_next_prefix(String sequence_number_next_prefix);

    /**
     * 获取 [下一个号码前缀]脏标记
     */
    boolean getSequence_number_next_prefixDirtyFlag();

    /**
     * ID
     */
    Integer getId();

    void setId(Integer id);

    /**
     * 获取 [ID]脏标记
     */
    boolean getIdDirtyFlag();

    /**
     * 退款发票
     */
    String getRefund_invoice_ids();

    void setRefund_invoice_ids(String refund_invoice_ids);

    /**
     * 获取 [退款发票]脏标记
     */
    boolean getRefund_invoice_idsDirtyFlag();

    /**
     * 公司使用币种的逾期金额
     */
    Double getResidual_company_signed();

    void setResidual_company_signed(Double residual_company_signed);

    /**
     * 获取 [公司使用币种的逾期金额]脏标记
     */
    boolean getResidual_company_signedDirtyFlag();

    /**
     * 开票日期
     */
    Timestamp getDate_invoice();

    void setDate_invoice(Timestamp date_invoice);

    /**
     * 获取 [开票日期]脏标记
     */
    boolean getDate_invoiceDirtyFlag();

    /**
     * 参考/说明
     */
    String getName();

    void setName(String name);

    /**
     * 获取 [参考/说明]脏标记
     */
    boolean getNameDirtyFlag();

    /**
     * 责任用户
     */
    Integer getActivity_user_id();

    void setActivity_user_id(Integer activity_user_id);

    /**
     * 获取 [责任用户]脏标记
     */
    boolean getActivity_user_idDirtyFlag();

    /**
     * 付款参考:
     */
    String getReference();

    void setReference(String reference);

    /**
     * 获取 [付款参考:]脏标记
     */
    boolean getReferenceDirtyFlag();

    /**
     * 网站
     */
    Integer getWebsite_id();

    void setWebsite_id(Integer website_id);

    /**
     * 获取 [网站]脏标记
     */
    boolean getWebsite_idDirtyFlag();

    /**
     * 下一号码
     */
    String getSequence_number_next();

    void setSequence_number_next(String sequence_number_next);

    /**
     * 获取 [下一号码]脏标记
     */
    boolean getSequence_number_nextDirtyFlag();

    /**
     * 网站信息
     */
    String getWebsite_message_ids();

    void setWebsite_message_ids(String website_message_ids);

    /**
     * 获取 [网站信息]脏标记
     */
    boolean getWebsite_message_idsDirtyFlag();

    /**
     * Tax in Invoice Currency
     */
    Double getAmount_tax_signed();

    void setAmount_tax_signed(Double amount_tax_signed);

    /**
     * 获取 [Tax in Invoice Currency]脏标记
     */
    boolean getAmount_tax_signedDirtyFlag();

    /**
     * 发票标示
     */
    String getInvoice_icon();

    void setInvoice_icon(String invoice_icon);

    /**
     * 获取 [发票标示]脏标记
     */
    boolean getInvoice_iconDirtyFlag();

    /**
     * 付款
     */
    String getPayment_ids();

    void setPayment_ids(String payment_ids);

    /**
     * 获取 [付款]脏标记
     */
    boolean getPayment_idsDirtyFlag();

    /**
     * 需要一个动作消息的编码
     */
    Integer getMessage_has_error_counter();

    void setMessage_has_error_counter(Integer message_has_error_counter);

    /**
     * 获取 [需要一个动作消息的编码]脏标记
     */
    boolean getMessage_has_error_counterDirtyFlag();

    /**
     * 总计
     */
    Double getAmount_total();

    void setAmount_total(Double amount_total);

    /**
     * 获取 [总计]脏标记
     */
    boolean getAmount_totalDirtyFlag();

    /**
     * 活动
     */
    String getActivity_ids();

    void setActivity_ids(String activity_ids);

    /**
     * 获取 [活动]脏标记
     */
    boolean getActivity_idsDirtyFlag();

    /**
     * 类型
     */
    String getType();

    void setType(String type);

    /**
     * 获取 [类型]脏标记
     */
    boolean getTypeDirtyFlag();

    /**
     * 日记账
     */
    String getJournal_id_text();

    void setJournal_id_text(String journal_id_text);

    /**
     * 获取 [日记账]脏标记
     */
    boolean getJournal_id_textDirtyFlag();

    /**
     * 添加采购订单
     */
    String getPurchase_id_text();

    void setPurchase_id_text(String purchase_id_text);

    /**
     * 获取 [添加采购订单]脏标记
     */
    boolean getPurchase_id_textDirtyFlag();

    /**
     * 自动完成
     */
    String getVendor_bill_purchase_id_text();

    void setVendor_bill_purchase_id_text(String vendor_bill_purchase_id_text);

    /**
     * 获取 [自动完成]脏标记
     */
    boolean getVendor_bill_purchase_id_textDirtyFlag();

    /**
     * 来源
     */
    String getSource_id_text();

    void setSource_id_text(String source_id_text);

    /**
     * 获取 [来源]脏标记
     */
    boolean getSource_id_textDirtyFlag();

    /**
     * 营销
     */
    String getCampaign_id_text();

    void setCampaign_id_text(String campaign_id_text);

    /**
     * 获取 [营销]脏标记
     */
    boolean getCampaign_id_textDirtyFlag();

    /**
     * 送货地址
     */
    String getPartner_shipping_id_text();

    void setPartner_shipping_id_text(String partner_shipping_id_text);

    /**
     * 获取 [送货地址]脏标记
     */
    boolean getPartner_shipping_id_textDirtyFlag();

    /**
     * 币种
     */
    String getCurrency_id_text();

    void setCurrency_id_text(String currency_id_text);

    /**
     * 获取 [币种]脏标记
     */
    boolean getCurrency_id_textDirtyFlag();

    /**
     * 最后更新人
     */
    String getWrite_uid_text();

    void setWrite_uid_text(String write_uid_text);

    /**
     * 获取 [最后更新人]脏标记
     */
    boolean getWrite_uid_textDirtyFlag();

    /**
     * 贸易条款
     */
    String getIncoterms_id_text();

    void setIncoterms_id_text(String incoterms_id_text);

    /**
     * 获取 [贸易条款]脏标记
     */
    boolean getIncoterms_id_textDirtyFlag();

    /**
     * 公司货币
     */
    Integer getCompany_currency_id();

    void setCompany_currency_id(Integer company_currency_id);

    /**
     * 获取 [公司货币]脏标记
     */
    boolean getCompany_currency_idDirtyFlag();

    /**
     * 科目
     */
    String getAccount_id_text();

    void setAccount_id_text(String account_id_text);

    /**
     * 获取 [科目]脏标记
     */
    boolean getAccount_id_textDirtyFlag();

    /**
     * 国际贸易术语
     */
    String getIncoterm_id_text();

    void setIncoterm_id_text(String incoterm_id_text);

    /**
     * 获取 [国际贸易术语]脏标记
     */
    boolean getIncoterm_id_textDirtyFlag();

    /**
     * 业务伙伴
     */
    String getPartner_id_text();

    void setPartner_id_text(String partner_id_text);

    /**
     * 获取 [业务伙伴]脏标记
     */
    boolean getPartner_id_textDirtyFlag();

    /**
     * 媒体
     */
    String getMedium_id_text();

    void setMedium_id_text(String medium_id_text);

    /**
     * 获取 [媒体]脏标记
     */
    boolean getMedium_id_textDirtyFlag();

    /**
     * 创建人
     */
    String getCreate_uid_text();

    void setCreate_uid_text(String create_uid_text);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uid_textDirtyFlag();

    /**
     * 公司
     */
    String getCompany_id_text();

    void setCompany_id_text(String company_id_text);

    /**
     * 获取 [公司]脏标记
     */
    boolean getCompany_id_textDirtyFlag();

    /**
     * 销售员
     */
    String getUser_id_text();

    void setUser_id_text(String user_id_text);

    /**
     * 获取 [销售员]脏标记
     */
    boolean getUser_id_textDirtyFlag();

    /**
     * 现金舍入方式
     */
    String getCash_rounding_id_text();

    void setCash_rounding_id_text(String cash_rounding_id_text);

    /**
     * 获取 [现金舍入方式]脏标记
     */
    boolean getCash_rounding_id_textDirtyFlag();

    /**
     * 供应商账单
     */
    String getVendor_bill_id_text();

    void setVendor_bill_id_text(String vendor_bill_id_text);

    /**
     * 获取 [供应商账单]脏标记
     */
    boolean getVendor_bill_id_textDirtyFlag();

    /**
     * 此发票为信用票的发票
     */
    String getRefund_invoice_id_text();

    void setRefund_invoice_id_text(String refund_invoice_id_text);

    /**
     * 获取 [此发票为信用票的发票]脏标记
     */
    boolean getRefund_invoice_id_textDirtyFlag();

    /**
     * 税科目调整
     */
    String getFiscal_position_id_text();

    void setFiscal_position_id_text(String fiscal_position_id_text);

    /**
     * 获取 [税科目调整]脏标记
     */
    boolean getFiscal_position_id_textDirtyFlag();

    /**
     * 号码
     */
    String getNumber();

    void setNumber(String number);

    /**
     * 获取 [号码]脏标记
     */
    boolean getNumberDirtyFlag();

    /**
     * 商业实体
     */
    String getCommercial_partner_id_text();

    void setCommercial_partner_id_text(String commercial_partner_id_text);

    /**
     * 获取 [商业实体]脏标记
     */
    boolean getCommercial_partner_id_textDirtyFlag();

    /**
     * 销售团队
     */
    String getTeam_id_text();

    void setTeam_id_text(String team_id_text);

    /**
     * 获取 [销售团队]脏标记
     */
    boolean getTeam_id_textDirtyFlag();

    /**
     * 付款条款
     */
    String getPayment_term_id_text();

    void setPayment_term_id_text(String payment_term_id_text);

    /**
     * 获取 [付款条款]脏标记
     */
    boolean getPayment_term_id_textDirtyFlag();

    /**
     * 销售员
     */
    Integer getUser_id();

    void setUser_id(Integer user_id);

    /**
     * 获取 [销售员]脏标记
     */
    boolean getUser_idDirtyFlag();

    /**
     * 销售团队
     */
    Integer getTeam_id();

    void setTeam_id(Integer team_id);

    /**
     * 获取 [销售团队]脏标记
     */
    boolean getTeam_idDirtyFlag();

    /**
     * 创建人
     */
    Integer getCreate_uid();

    void setCreate_uid(Integer create_uid);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uidDirtyFlag();

    /**
     * 科目
     */
    Integer getAccount_id();

    void setAccount_id(Integer account_id);

    /**
     * 获取 [科目]脏标记
     */
    boolean getAccount_idDirtyFlag();

    /**
     * 媒体
     */
    Integer getMedium_id();

    void setMedium_id(Integer medium_id);

    /**
     * 获取 [媒体]脏标记
     */
    boolean getMedium_idDirtyFlag();

    /**
     * 公司
     */
    Integer getCompany_id();

    void setCompany_id(Integer company_id);

    /**
     * 获取 [公司]脏标记
     */
    boolean getCompany_idDirtyFlag();

    /**
     * 日记账分录
     */
    Integer getMove_id();

    void setMove_id(Integer move_id);

    /**
     * 获取 [日记账分录]脏标记
     */
    boolean getMove_idDirtyFlag();

    /**
     * 付款条款
     */
    Integer getPayment_term_id();

    void setPayment_term_id(Integer payment_term_id);

    /**
     * 获取 [付款条款]脏标记
     */
    boolean getPayment_term_idDirtyFlag();

    /**
     * 日记账
     */
    Integer getJournal_id();

    void setJournal_id(Integer journal_id);

    /**
     * 获取 [日记账]脏标记
     */
    boolean getJournal_idDirtyFlag();

    /**
     * 国际贸易术语
     */
    Integer getIncoterm_id();

    void setIncoterm_id(Integer incoterm_id);

    /**
     * 获取 [国际贸易术语]脏标记
     */
    boolean getIncoterm_idDirtyFlag();

    /**
     * 添加采购订单
     */
    Integer getPurchase_id();

    void setPurchase_id(Integer purchase_id);

    /**
     * 获取 [添加采购订单]脏标记
     */
    boolean getPurchase_idDirtyFlag();

    /**
     * 税科目调整
     */
    Integer getFiscal_position_id();

    void setFiscal_position_id(Integer fiscal_position_id);

    /**
     * 获取 [税科目调整]脏标记
     */
    boolean getFiscal_position_idDirtyFlag();

    /**
     * 此发票为信用票的发票
     */
    Integer getRefund_invoice_id();

    void setRefund_invoice_id(Integer refund_invoice_id);

    /**
     * 获取 [此发票为信用票的发票]脏标记
     */
    boolean getRefund_invoice_idDirtyFlag();

    /**
     * 币种
     */
    Integer getCurrency_id();

    void setCurrency_id(Integer currency_id);

    /**
     * 获取 [币种]脏标记
     */
    boolean getCurrency_idDirtyFlag();

    /**
     * 业务伙伴
     */
    Integer getPartner_id();

    void setPartner_id(Integer partner_id);

    /**
     * 获取 [业务伙伴]脏标记
     */
    boolean getPartner_idDirtyFlag();

    /**
     * 银行账户
     */
    Integer getPartner_bank_id();

    void setPartner_bank_id(Integer partner_bank_id);

    /**
     * 获取 [银行账户]脏标记
     */
    boolean getPartner_bank_idDirtyFlag();

    /**
     * 现金舍入方式
     */
    Integer getCash_rounding_id();

    void setCash_rounding_id(Integer cash_rounding_id);

    /**
     * 获取 [现金舍入方式]脏标记
     */
    boolean getCash_rounding_idDirtyFlag();

    /**
     * 商业实体
     */
    Integer getCommercial_partner_id();

    void setCommercial_partner_id(Integer commercial_partner_id);

    /**
     * 获取 [商业实体]脏标记
     */
    boolean getCommercial_partner_idDirtyFlag();

    /**
     * 送货地址
     */
    Integer getPartner_shipping_id();

    void setPartner_shipping_id(Integer partner_shipping_id);

    /**
     * 获取 [送货地址]脏标记
     */
    boolean getPartner_shipping_idDirtyFlag();

    /**
     * 贸易条款
     */
    Integer getIncoterms_id();

    void setIncoterms_id(Integer incoterms_id);

    /**
     * 获取 [贸易条款]脏标记
     */
    boolean getIncoterms_idDirtyFlag();

    /**
     * 来源
     */
    Integer getSource_id();

    void setSource_id(Integer source_id);

    /**
     * 获取 [来源]脏标记
     */
    boolean getSource_idDirtyFlag();

    /**
     * 供应商账单
     */
    Integer getVendor_bill_id();

    void setVendor_bill_id(Integer vendor_bill_id);

    /**
     * 获取 [供应商账单]脏标记
     */
    boolean getVendor_bill_idDirtyFlag();

    /**
     * 最后更新人
     */
    Integer getWrite_uid();

    void setWrite_uid(Integer write_uid);

    /**
     * 获取 [最后更新人]脏标记
     */
    boolean getWrite_uidDirtyFlag();

    /**
     * 自动完成
     */
    Integer getVendor_bill_purchase_id();

    void setVendor_bill_purchase_id(Integer vendor_bill_purchase_id);

    /**
     * 获取 [自动完成]脏标记
     */
    boolean getVendor_bill_purchase_idDirtyFlag();

    /**
     * 营销
     */
    Integer getCampaign_id();

    void setCampaign_id(Integer campaign_id);

    /**
     * 获取 [营销]脏标记
     */
    boolean getCampaign_idDirtyFlag();

}
