package cn.ibizlab.odoo.core.odoo_stock.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_stock.domain.Stock_rules_report;
import cn.ibizlab.odoo.core.odoo_stock.filter.Stock_rules_reportSearchContext;
import cn.ibizlab.odoo.core.odoo_stock.service.IStock_rules_reportService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;



import cn.ibizlab.odoo.core.odoo_stock.client.stock_rules_reportOdooClient;
import cn.ibizlab.odoo.core.odoo_stock.clientmodel.stock_rules_reportClientModel;

/**
 * 实体[库存规则报告] 服务对象接口实现
 */
@Slf4j
@Service
public class Stock_rules_reportServiceImpl implements IStock_rules_reportService {

    @Autowired
    stock_rules_reportOdooClient stock_rules_reportOdooClient;


    @Override
    public boolean create(Stock_rules_report et) {
        stock_rules_reportClientModel clientModel = convert2Model(et,null);
		stock_rules_reportOdooClient.create(clientModel);
        Stock_rules_report rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Stock_rules_report> list){
    }

    @Override
    public Stock_rules_report get(Integer id) {
        stock_rules_reportClientModel clientModel = new stock_rules_reportClientModel();
        clientModel.setId(id);
		stock_rules_reportOdooClient.get(clientModel);
        Stock_rules_report et = convert2Domain(clientModel,null);
        if(et==null){
            et=new Stock_rules_report();
            et.setId(id);
        }
        else{
        }

        return  et;
    }

    @Override
    public boolean remove(Integer id) {
        stock_rules_reportClientModel clientModel = new stock_rules_reportClientModel();
        clientModel.setId(id);
		stock_rules_reportOdooClient.remove(clientModel);
        return true; 
    }

    public void removeBatch(Collection<Integer> idList){
    }

    @Override
    public boolean update(Stock_rules_report et) {
        stock_rules_reportClientModel clientModel = convert2Model(et,null);
		stock_rules_reportOdooClient.update(clientModel);
        Stock_rules_report rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }
    
    public void updateBatch(List<Stock_rules_report> list){
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Stock_rules_report> searchDefault(Stock_rules_reportSearchContext context) {
        List<Stock_rules_report> list = new ArrayList<Stock_rules_report>();
        Page<stock_rules_reportClientModel> clientModelList = stock_rules_reportOdooClient.search(context);
        for(stock_rules_reportClientModel clientModel : clientModelList){
            list.add( this.convert2Domain(clientModel,null));
        }
        return new PageImpl<Stock_rules_report>(list, context.getPageable(), clientModelList.getTotalElements());
    }

    public stock_rules_reportClientModel convert2Model(Stock_rules_report domain , stock_rules_reportClientModel model) {
        if(domain == null)
            return model ;
        if(model==null){
            model = new stock_rules_reportClientModel();

        }
        if((Boolean) domain.getExtensionparams().get("dirtyflagenable")){
            if((Boolean) domain.getExtensionparams().get("__last_updatedirtyflag"))
                model.set__last_update(domain.getLastUpdate());
            if((Boolean) domain.getExtensionparams().get("display_namedirtyflag"))
                model.setDisplay_name(domain.getDisplayName());
            if((Boolean) domain.getExtensionparams().get("create_datedirtyflag"))
                model.setCreate_date(domain.getCreateDate());
            if((Boolean) domain.getExtensionparams().get("write_datedirtyflag"))
                model.setWrite_date(domain.getWriteDate());
            if((Boolean) domain.getExtensionparams().get("warehouse_idsdirtyflag"))
                model.setWarehouse_ids(domain.getWarehouseIds());
            if((Boolean) domain.getExtensionparams().get("product_has_variantsdirtyflag"))
                model.setProduct_has_variants(domain.getProductHasVariants());
            if((Boolean) domain.getExtensionparams().get("so_route_idsdirtyflag"))
                model.setSo_route_ids(domain.getSoRouteIds());
            if((Boolean) domain.getExtensionparams().get("iddirtyflag"))
                model.setId(domain.getId());
            if((Boolean) domain.getExtensionparams().get("product_id_textdirtyflag"))
                model.setProduct_id_text(domain.getProductIdText());
            if((Boolean) domain.getExtensionparams().get("product_tmpl_id_textdirtyflag"))
                model.setProduct_tmpl_id_text(domain.getProductTmplIdText());
            if((Boolean) domain.getExtensionparams().get("create_uid_textdirtyflag"))
                model.setCreate_uid_text(domain.getCreateUidText());
            if((Boolean) domain.getExtensionparams().get("write_uid_textdirtyflag"))
                model.setWrite_uid_text(domain.getWriteUidText());
            if((Boolean) domain.getExtensionparams().get("write_uiddirtyflag"))
                model.setWrite_uid(domain.getWriteUid());
            if((Boolean) domain.getExtensionparams().get("product_iddirtyflag"))
                model.setProduct_id(domain.getProductId());
            if((Boolean) domain.getExtensionparams().get("product_tmpl_iddirtyflag"))
                model.setProduct_tmpl_id(domain.getProductTmplId());
            if((Boolean) domain.getExtensionparams().get("create_uiddirtyflag"))
                model.setCreate_uid(domain.getCreateUid());
            domain.getFocusNull().clear();
        }
        return model ;
    }

    public Stock_rules_report convert2Domain( stock_rules_reportClientModel model ,Stock_rules_report domain) {
        if(model == null)
            return domain ;
        if(domain == null){
            domain = new Stock_rules_report();
        }

        if(model.get__last_updateDirtyFlag())
            domain.setLastUpdate(model.get__last_update());
        if(model.getDisplay_nameDirtyFlag())
            domain.setDisplayName(model.getDisplay_name());
        if(model.getCreate_dateDirtyFlag())
            domain.setCreateDate(model.getCreate_date());
        if(model.getWrite_dateDirtyFlag())
            domain.setWriteDate(model.getWrite_date());
        if(model.getWarehouse_idsDirtyFlag())
            domain.setWarehouseIds(model.getWarehouse_ids());
        if(model.getProduct_has_variantsDirtyFlag())
            domain.setProductHasVariants(model.getProduct_has_variants());
        if(model.getSo_route_idsDirtyFlag())
            domain.setSoRouteIds(model.getSo_route_ids());
        if(model.getIdDirtyFlag())
            domain.setId(model.getId());
        if(model.getProduct_id_textDirtyFlag())
            domain.setProductIdText(model.getProduct_id_text());
        if(model.getProduct_tmpl_id_textDirtyFlag())
            domain.setProductTmplIdText(model.getProduct_tmpl_id_text());
        if(model.getCreate_uid_textDirtyFlag())
            domain.setCreateUidText(model.getCreate_uid_text());
        if(model.getWrite_uid_textDirtyFlag())
            domain.setWriteUidText(model.getWrite_uid_text());
        if(model.getWrite_uidDirtyFlag())
            domain.setWriteUid(model.getWrite_uid());
        if(model.getProduct_idDirtyFlag())
            domain.setProductId(model.getProduct_id());
        if(model.getProduct_tmpl_idDirtyFlag())
            domain.setProductTmplId(model.getProduct_tmpl_id());
        if(model.getCreate_uidDirtyFlag())
            domain.setCreateUid(model.getCreate_uid());
        return domain ;
    }

}

    



