package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Account_unreconcile;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_unreconcileSearchContext;

/**
 * 实体 [会计取消核销] 存储对象
 */
public interface Account_unreconcileRepository extends Repository<Account_unreconcile> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Account_unreconcile> searchDefault(Account_unreconcileSearchContext context);

    Account_unreconcile convert2PO(cn.ibizlab.odoo.core.odoo_account.domain.Account_unreconcile domain , Account_unreconcile po) ;

    cn.ibizlab.odoo.core.odoo_account.domain.Account_unreconcile convert2Domain( Account_unreconcile po ,cn.ibizlab.odoo.core.odoo_account.domain.Account_unreconcile domain) ;

}
