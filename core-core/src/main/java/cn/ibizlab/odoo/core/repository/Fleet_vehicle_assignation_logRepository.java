package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Fleet_vehicle_assignation_log;
import cn.ibizlab.odoo.core.odoo_fleet.filter.Fleet_vehicle_assignation_logSearchContext;

/**
 * 实体 [交通工具驾驶历史] 存储对象
 */
public interface Fleet_vehicle_assignation_logRepository extends Repository<Fleet_vehicle_assignation_log> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Fleet_vehicle_assignation_log> searchDefault(Fleet_vehicle_assignation_logSearchContext context);

    Fleet_vehicle_assignation_log convert2PO(cn.ibizlab.odoo.core.odoo_fleet.domain.Fleet_vehicle_assignation_log domain , Fleet_vehicle_assignation_log po) ;

    cn.ibizlab.odoo.core.odoo_fleet.domain.Fleet_vehicle_assignation_log convert2Domain( Fleet_vehicle_assignation_log po ,cn.ibizlab.odoo.core.odoo_fleet.domain.Fleet_vehicle_assignation_log domain) ;

}
