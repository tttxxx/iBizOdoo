package cn.ibizlab.odoo.core.odoo_bus.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_bus.domain.Bus_presence;
import cn.ibizlab.odoo.core.odoo_bus.filter.Bus_presenceSearchContext;
import cn.ibizlab.odoo.core.odoo_bus.service.IBus_presenceService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;



import cn.ibizlab.odoo.core.odoo_bus.client.bus_presenceOdooClient;
import cn.ibizlab.odoo.core.odoo_bus.clientmodel.bus_presenceClientModel;

/**
 * 实体[用户上线] 服务对象接口实现
 */
@Slf4j
@Service
public class Bus_presenceServiceImpl implements IBus_presenceService {

    @Autowired
    bus_presenceOdooClient bus_presenceOdooClient;


    @Override
    public Bus_presence get(Integer id) {
        bus_presenceClientModel clientModel = new bus_presenceClientModel();
        clientModel.setId(id);
		bus_presenceOdooClient.get(clientModel);
        Bus_presence et = convert2Domain(clientModel,null);
        if(et==null){
            et=new Bus_presence();
            et.setId(id);
        }
        else{
        }

        return  et;
    }

    @Override
    public boolean remove(Integer id) {
        bus_presenceClientModel clientModel = new bus_presenceClientModel();
        clientModel.setId(id);
		bus_presenceOdooClient.remove(clientModel);
        return true; 
    }

    public void removeBatch(Collection<Integer> idList){
    }

    @Override
    public boolean update(Bus_presence et) {
        bus_presenceClientModel clientModel = convert2Model(et,null);
		bus_presenceOdooClient.update(clientModel);
        Bus_presence rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }
    
    public void updateBatch(List<Bus_presence> list){
    }

    @Override
    public boolean create(Bus_presence et) {
        bus_presenceClientModel clientModel = convert2Model(et,null);
		bus_presenceOdooClient.create(clientModel);
        Bus_presence rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Bus_presence> list){
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Bus_presence> searchDefault(Bus_presenceSearchContext context) {
        List<Bus_presence> list = new ArrayList<Bus_presence>();
        Page<bus_presenceClientModel> clientModelList = bus_presenceOdooClient.search(context);
        for(bus_presenceClientModel clientModel : clientModelList){
            list.add( this.convert2Domain(clientModel,null));
        }
        return new PageImpl<Bus_presence>(list, context.getPageable(), clientModelList.getTotalElements());
    }

    public bus_presenceClientModel convert2Model(Bus_presence domain , bus_presenceClientModel model) {
        if(domain == null)
            return model ;
        if(model==null){
            model = new bus_presenceClientModel();

        }
        if((Boolean) domain.getExtensionparams().get("dirtyflagenable")){
            if((Boolean) domain.getExtensionparams().get("display_namedirtyflag"))
                model.setDisplay_name(domain.getDisplayName());
            if((Boolean) domain.getExtensionparams().get("last_presencedirtyflag"))
                model.setLast_presence(domain.getLastPresence());
            if((Boolean) domain.getExtensionparams().get("last_polldirtyflag"))
                model.setLast_poll(domain.getLastPoll());
            if((Boolean) domain.getExtensionparams().get("iddirtyflag"))
                model.setId(domain.getId());
            if((Boolean) domain.getExtensionparams().get("statusdirtyflag"))
                model.setStatus(domain.getStatus());
            if((Boolean) domain.getExtensionparams().get("__last_updatedirtyflag"))
                model.set__last_update(domain.getLastUpdate());
            if((Boolean) domain.getExtensionparams().get("user_id_textdirtyflag"))
                model.setUser_id_text(domain.getUserIdText());
            if((Boolean) domain.getExtensionparams().get("user_iddirtyflag"))
                model.setUser_id(domain.getUserId());
            domain.getFocusNull().clear();
        }
        return model ;
    }

    public Bus_presence convert2Domain( bus_presenceClientModel model ,Bus_presence domain) {
        if(model == null)
            return domain ;
        if(domain == null){
            domain = new Bus_presence();
        }

        if(model.getDisplay_nameDirtyFlag())
            domain.setDisplayName(model.getDisplay_name());
        if(model.getLast_presenceDirtyFlag())
            domain.setLastPresence(model.getLast_presence());
        if(model.getLast_pollDirtyFlag())
            domain.setLastPoll(model.getLast_poll());
        if(model.getIdDirtyFlag())
            domain.setId(model.getId());
        if(model.getStatusDirtyFlag())
            domain.setStatus(model.getStatus());
        if(model.get__last_updateDirtyFlag())
            domain.setLastUpdate(model.get__last_update());
        if(model.getUser_id_textDirtyFlag())
            domain.setUserIdText(model.getUser_id_text());
        if(model.getUser_idDirtyFlag())
            domain.setUserId(model.getUser_id());
        return domain ;
    }

}

    



