package cn.ibizlab.odoo.core.odoo_mail.clientmodel;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.math.BigInteger;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.util.helper.OdooClientHelper;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang.ArrayUtils;
import org.springframework.util.StringUtils;

/**
 * 接口实体[mail_mass_mailing] 对象
 */
public class mail_mass_mailingClientModel implements Serializable{

    /**
     * 有效
     */
    public String active;

    @JsonIgnore
    public boolean activeDirtyFlag;
    
    /**
     * 附件
     */
    public String attachment_ids;

    @JsonIgnore
    public boolean attachment_idsDirtyFlag;
    
    /**
     * 正文
     */
    public String body_html;

    @JsonIgnore
    public boolean body_htmlDirtyFlag;
    
    /**
     * 被退回
     */
    public Integer bounced;

    @JsonIgnore
    public boolean bouncedDirtyFlag;
    
    /**
     * 被退回的比率
     */
    public Integer bounced_ratio;

    @JsonIgnore
    public boolean bounced_ratioDirtyFlag;
    
    /**
     * 营销
     */
    public Integer campaign_id;

    @JsonIgnore
    public boolean campaign_idDirtyFlag;
    
    /**
     * 营销
     */
    public String campaign_id_text;

    @JsonIgnore
    public boolean campaign_id_textDirtyFlag;
    
    /**
     * 点击率
     */
    public Integer clicked;

    @JsonIgnore
    public boolean clickedDirtyFlag;
    
    /**
     * 点击数
     */
    public Integer clicks_ratio;

    @JsonIgnore
    public boolean clicks_ratioDirtyFlag;
    
    /**
     * 颜色索引
     */
    public Integer color;

    @JsonIgnore
    public boolean colorDirtyFlag;
    
    /**
     * A/B 测试百分比
     */
    public Integer contact_ab_pc;

    @JsonIgnore
    public boolean contact_ab_pcDirtyFlag;
    
    /**
     * 邮件列表
     */
    public String contact_list_ids;

    @JsonIgnore
    public boolean contact_list_idsDirtyFlag;
    
    /**
     * 创建时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp create_date;

    @JsonIgnore
    public boolean create_dateDirtyFlag;
    
    /**
     * 创建人
     */
    public Integer create_uid;

    @JsonIgnore
    public boolean create_uidDirtyFlag;
    
    /**
     * 创建人
     */
    public String create_uid_text;

    @JsonIgnore
    public boolean create_uid_textDirtyFlag;
    
    /**
     * 使用线索
     */
    public String crm_lead_activated;

    @JsonIgnore
    public boolean crm_lead_activatedDirtyFlag;
    
    /**
     * 线索总数
     */
    public Integer crm_lead_count;

    @JsonIgnore
    public boolean crm_lead_countDirtyFlag;
    
    /**
     * 商机个数
     */
    public Integer crm_opportunities_count;

    @JsonIgnore
    public boolean crm_opportunities_countDirtyFlag;
    
    /**
     * 已送货
     */
    public Integer delivered;

    @JsonIgnore
    public boolean deliveredDirtyFlag;
    
    /**
     * 显示名称
     */
    public String display_name;

    @JsonIgnore
    public boolean display_nameDirtyFlag;
    
    /**
     * 从
     */
    public String email_from;

    @JsonIgnore
    public boolean email_fromDirtyFlag;
    
    /**
     * 预期
     */
    public Integer expected;

    @JsonIgnore
    public boolean expectedDirtyFlag;
    
    /**
     * 失败的
     */
    public Integer failed;

    @JsonIgnore
    public boolean failedDirtyFlag;
    
    /**
     * ID
     */
    public Integer id;

    @JsonIgnore
    public boolean idDirtyFlag;
    
    /**
     * 忽略
     */
    public Integer ignored;

    @JsonIgnore
    public boolean ignoredDirtyFlag;
    
    /**
     * 归档
     */
    public String keep_archives;

    @JsonIgnore
    public boolean keep_archivesDirtyFlag;
    
    /**
     * 域
     */
    public String mailing_domain;

    @JsonIgnore
    public boolean mailing_domainDirtyFlag;
    
    /**
     * 收件人模型
     */
    public Integer mailing_model_id;

    @JsonIgnore
    public boolean mailing_model_idDirtyFlag;
    
    /**
     * 收件人模型
     */
    public String mailing_model_name;

    @JsonIgnore
    public boolean mailing_model_nameDirtyFlag;
    
    /**
     * 收件人实物模型
     */
    public String mailing_model_real;

    @JsonIgnore
    public boolean mailing_model_realDirtyFlag;
    
    /**
     * 邮件服务器
     */
    public Integer mail_server_id;

    @JsonIgnore
    public boolean mail_server_idDirtyFlag;
    
    /**
     * 群发邮件营销
     */
    public Integer mass_mailing_campaign_id;

    @JsonIgnore
    public boolean mass_mailing_campaign_idDirtyFlag;
    
    /**
     * 群发邮件营销
     */
    public String mass_mailing_campaign_id_text;

    @JsonIgnore
    public boolean mass_mailing_campaign_id_textDirtyFlag;
    
    /**
     * 媒体
     */
    public Integer medium_id;

    @JsonIgnore
    public boolean medium_idDirtyFlag;
    
    /**
     * 媒体
     */
    public String medium_id_text;

    @JsonIgnore
    public boolean medium_id_textDirtyFlag;
    
    /**
     * 来源名称
     */
    public String name;

    @JsonIgnore
    public boolean nameDirtyFlag;
    
    /**
     * 安排的日期
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp next_departure;

    @JsonIgnore
    public boolean next_departureDirtyFlag;
    
    /**
     * 已开启
     */
    public Integer opened;

    @JsonIgnore
    public boolean openedDirtyFlag;
    
    /**
     * 打开比例
     */
    public Integer opened_ratio;

    @JsonIgnore
    public boolean opened_ratioDirtyFlag;
    
    /**
     * 已接收比例
     */
    public Integer received_ratio;

    @JsonIgnore
    public boolean received_ratioDirtyFlag;
    
    /**
     * 已回复
     */
    public Integer replied;

    @JsonIgnore
    public boolean repliedDirtyFlag;
    
    /**
     * 回复比例
     */
    public Integer replied_ratio;

    @JsonIgnore
    public boolean replied_ratioDirtyFlag;
    
    /**
     * 回复
     */
    public String reply_to;

    @JsonIgnore
    public boolean reply_toDirtyFlag;
    
    /**
     * 回复模式
     */
    public String reply_to_mode;

    @JsonIgnore
    public boolean reply_to_modeDirtyFlag;
    
    /**
     * 开票金额
     */
    public Integer sale_invoiced_amount;

    @JsonIgnore
    public boolean sale_invoiced_amountDirtyFlag;
    
    /**
     * 报价个数
     */
    public Integer sale_quotation_count;

    @JsonIgnore
    public boolean sale_quotation_countDirtyFlag;
    
    /**
     * 安排
     */
    public Integer scheduled;

    @JsonIgnore
    public boolean scheduledDirtyFlag;
    
    /**
     * 在将来计划
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp schedule_date;

    @JsonIgnore
    public boolean schedule_dateDirtyFlag;
    
    /**
     * 已汇
     */
    public Integer sent;

    @JsonIgnore
    public boolean sentDirtyFlag;
    
    /**
     * 发送日期
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp sent_date;

    @JsonIgnore
    public boolean sent_dateDirtyFlag;
    
    /**
     * 主题
     */
    public Integer source_id;

    @JsonIgnore
    public boolean source_idDirtyFlag;
    
    /**
     * 状态
     */
    public String state;

    @JsonIgnore
    public boolean stateDirtyFlag;
    
    /**
     * 邮件统计
     */
    public String statistics_ids;

    @JsonIgnore
    public boolean statistics_idsDirtyFlag;
    
    /**
     * 总计
     */
    public Integer total;

    @JsonIgnore
    public boolean totalDirtyFlag;
    
    /**
     * 邮件管理器
     */
    public Integer user_id;

    @JsonIgnore
    public boolean user_idDirtyFlag;
    
    /**
     * 邮件管理器
     */
    public String user_id_text;

    @JsonIgnore
    public boolean user_id_textDirtyFlag;
    
    /**
     * 最后更新时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp write_date;

    @JsonIgnore
    public boolean write_dateDirtyFlag;
    
    /**
     * 最后更新者
     */
    public Integer write_uid;

    @JsonIgnore
    public boolean write_uidDirtyFlag;
    
    /**
     * 最后更新者
     */
    public String write_uid_text;

    @JsonIgnore
    public boolean write_uid_textDirtyFlag;
    
    /**
     * 最后修改日
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp __last_update;

    @JsonIgnore
    public boolean __last_updateDirtyFlag;
    
    /**
     * 获取 [有效]
     */
    @JsonProperty("active")
    public String getActive(){
        return this.active ;
    }

    /**
     * 设置 [有效]
     */
    @JsonProperty("active")
    public void setActive(String  active){
        this.active = active ;
        this.activeDirtyFlag = true ;
    }

     /**
     * 获取 [有效]脏标记
     */
    @JsonIgnore
    public boolean getActiveDirtyFlag(){
        return this.activeDirtyFlag ;
    }   

    /**
     * 获取 [附件]
     */
    @JsonProperty("attachment_ids")
    public String getAttachment_ids(){
        return this.attachment_ids ;
    }

    /**
     * 设置 [附件]
     */
    @JsonProperty("attachment_ids")
    public void setAttachment_ids(String  attachment_ids){
        this.attachment_ids = attachment_ids ;
        this.attachment_idsDirtyFlag = true ;
    }

     /**
     * 获取 [附件]脏标记
     */
    @JsonIgnore
    public boolean getAttachment_idsDirtyFlag(){
        return this.attachment_idsDirtyFlag ;
    }   

    /**
     * 获取 [正文]
     */
    @JsonProperty("body_html")
    public String getBody_html(){
        return this.body_html ;
    }

    /**
     * 设置 [正文]
     */
    @JsonProperty("body_html")
    public void setBody_html(String  body_html){
        this.body_html = body_html ;
        this.body_htmlDirtyFlag = true ;
    }

     /**
     * 获取 [正文]脏标记
     */
    @JsonIgnore
    public boolean getBody_htmlDirtyFlag(){
        return this.body_htmlDirtyFlag ;
    }   

    /**
     * 获取 [被退回]
     */
    @JsonProperty("bounced")
    public Integer getBounced(){
        return this.bounced ;
    }

    /**
     * 设置 [被退回]
     */
    @JsonProperty("bounced")
    public void setBounced(Integer  bounced){
        this.bounced = bounced ;
        this.bouncedDirtyFlag = true ;
    }

     /**
     * 获取 [被退回]脏标记
     */
    @JsonIgnore
    public boolean getBouncedDirtyFlag(){
        return this.bouncedDirtyFlag ;
    }   

    /**
     * 获取 [被退回的比率]
     */
    @JsonProperty("bounced_ratio")
    public Integer getBounced_ratio(){
        return this.bounced_ratio ;
    }

    /**
     * 设置 [被退回的比率]
     */
    @JsonProperty("bounced_ratio")
    public void setBounced_ratio(Integer  bounced_ratio){
        this.bounced_ratio = bounced_ratio ;
        this.bounced_ratioDirtyFlag = true ;
    }

     /**
     * 获取 [被退回的比率]脏标记
     */
    @JsonIgnore
    public boolean getBounced_ratioDirtyFlag(){
        return this.bounced_ratioDirtyFlag ;
    }   

    /**
     * 获取 [营销]
     */
    @JsonProperty("campaign_id")
    public Integer getCampaign_id(){
        return this.campaign_id ;
    }

    /**
     * 设置 [营销]
     */
    @JsonProperty("campaign_id")
    public void setCampaign_id(Integer  campaign_id){
        this.campaign_id = campaign_id ;
        this.campaign_idDirtyFlag = true ;
    }

     /**
     * 获取 [营销]脏标记
     */
    @JsonIgnore
    public boolean getCampaign_idDirtyFlag(){
        return this.campaign_idDirtyFlag ;
    }   

    /**
     * 获取 [营销]
     */
    @JsonProperty("campaign_id_text")
    public String getCampaign_id_text(){
        return this.campaign_id_text ;
    }

    /**
     * 设置 [营销]
     */
    @JsonProperty("campaign_id_text")
    public void setCampaign_id_text(String  campaign_id_text){
        this.campaign_id_text = campaign_id_text ;
        this.campaign_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [营销]脏标记
     */
    @JsonIgnore
    public boolean getCampaign_id_textDirtyFlag(){
        return this.campaign_id_textDirtyFlag ;
    }   

    /**
     * 获取 [点击率]
     */
    @JsonProperty("clicked")
    public Integer getClicked(){
        return this.clicked ;
    }

    /**
     * 设置 [点击率]
     */
    @JsonProperty("clicked")
    public void setClicked(Integer  clicked){
        this.clicked = clicked ;
        this.clickedDirtyFlag = true ;
    }

     /**
     * 获取 [点击率]脏标记
     */
    @JsonIgnore
    public boolean getClickedDirtyFlag(){
        return this.clickedDirtyFlag ;
    }   

    /**
     * 获取 [点击数]
     */
    @JsonProperty("clicks_ratio")
    public Integer getClicks_ratio(){
        return this.clicks_ratio ;
    }

    /**
     * 设置 [点击数]
     */
    @JsonProperty("clicks_ratio")
    public void setClicks_ratio(Integer  clicks_ratio){
        this.clicks_ratio = clicks_ratio ;
        this.clicks_ratioDirtyFlag = true ;
    }

     /**
     * 获取 [点击数]脏标记
     */
    @JsonIgnore
    public boolean getClicks_ratioDirtyFlag(){
        return this.clicks_ratioDirtyFlag ;
    }   

    /**
     * 获取 [颜色索引]
     */
    @JsonProperty("color")
    public Integer getColor(){
        return this.color ;
    }

    /**
     * 设置 [颜色索引]
     */
    @JsonProperty("color")
    public void setColor(Integer  color){
        this.color = color ;
        this.colorDirtyFlag = true ;
    }

     /**
     * 获取 [颜色索引]脏标记
     */
    @JsonIgnore
    public boolean getColorDirtyFlag(){
        return this.colorDirtyFlag ;
    }   

    /**
     * 获取 [A/B 测试百分比]
     */
    @JsonProperty("contact_ab_pc")
    public Integer getContact_ab_pc(){
        return this.contact_ab_pc ;
    }

    /**
     * 设置 [A/B 测试百分比]
     */
    @JsonProperty("contact_ab_pc")
    public void setContact_ab_pc(Integer  contact_ab_pc){
        this.contact_ab_pc = contact_ab_pc ;
        this.contact_ab_pcDirtyFlag = true ;
    }

     /**
     * 获取 [A/B 测试百分比]脏标记
     */
    @JsonIgnore
    public boolean getContact_ab_pcDirtyFlag(){
        return this.contact_ab_pcDirtyFlag ;
    }   

    /**
     * 获取 [邮件列表]
     */
    @JsonProperty("contact_list_ids")
    public String getContact_list_ids(){
        return this.contact_list_ids ;
    }

    /**
     * 设置 [邮件列表]
     */
    @JsonProperty("contact_list_ids")
    public void setContact_list_ids(String  contact_list_ids){
        this.contact_list_ids = contact_list_ids ;
        this.contact_list_idsDirtyFlag = true ;
    }

     /**
     * 获取 [邮件列表]脏标记
     */
    @JsonIgnore
    public boolean getContact_list_idsDirtyFlag(){
        return this.contact_list_idsDirtyFlag ;
    }   

    /**
     * 获取 [创建时间]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return this.create_date ;
    }

    /**
     * 设置 [创建时间]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

     /**
     * 获取 [创建时间]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return this.create_dateDirtyFlag ;
    }   

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return this.create_uid ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

     /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return this.create_uidDirtyFlag ;
    }   

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return this.create_uid_text ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

     /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return this.create_uid_textDirtyFlag ;
    }   

    /**
     * 获取 [使用线索]
     */
    @JsonProperty("crm_lead_activated")
    public String getCrm_lead_activated(){
        return this.crm_lead_activated ;
    }

    /**
     * 设置 [使用线索]
     */
    @JsonProperty("crm_lead_activated")
    public void setCrm_lead_activated(String  crm_lead_activated){
        this.crm_lead_activated = crm_lead_activated ;
        this.crm_lead_activatedDirtyFlag = true ;
    }

     /**
     * 获取 [使用线索]脏标记
     */
    @JsonIgnore
    public boolean getCrm_lead_activatedDirtyFlag(){
        return this.crm_lead_activatedDirtyFlag ;
    }   

    /**
     * 获取 [线索总数]
     */
    @JsonProperty("crm_lead_count")
    public Integer getCrm_lead_count(){
        return this.crm_lead_count ;
    }

    /**
     * 设置 [线索总数]
     */
    @JsonProperty("crm_lead_count")
    public void setCrm_lead_count(Integer  crm_lead_count){
        this.crm_lead_count = crm_lead_count ;
        this.crm_lead_countDirtyFlag = true ;
    }

     /**
     * 获取 [线索总数]脏标记
     */
    @JsonIgnore
    public boolean getCrm_lead_countDirtyFlag(){
        return this.crm_lead_countDirtyFlag ;
    }   

    /**
     * 获取 [商机个数]
     */
    @JsonProperty("crm_opportunities_count")
    public Integer getCrm_opportunities_count(){
        return this.crm_opportunities_count ;
    }

    /**
     * 设置 [商机个数]
     */
    @JsonProperty("crm_opportunities_count")
    public void setCrm_opportunities_count(Integer  crm_opportunities_count){
        this.crm_opportunities_count = crm_opportunities_count ;
        this.crm_opportunities_countDirtyFlag = true ;
    }

     /**
     * 获取 [商机个数]脏标记
     */
    @JsonIgnore
    public boolean getCrm_opportunities_countDirtyFlag(){
        return this.crm_opportunities_countDirtyFlag ;
    }   

    /**
     * 获取 [已送货]
     */
    @JsonProperty("delivered")
    public Integer getDelivered(){
        return this.delivered ;
    }

    /**
     * 设置 [已送货]
     */
    @JsonProperty("delivered")
    public void setDelivered(Integer  delivered){
        this.delivered = delivered ;
        this.deliveredDirtyFlag = true ;
    }

     /**
     * 获取 [已送货]脏标记
     */
    @JsonIgnore
    public boolean getDeliveredDirtyFlag(){
        return this.deliveredDirtyFlag ;
    }   

    /**
     * 获取 [显示名称]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return this.display_name ;
    }

    /**
     * 设置 [显示名称]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

     /**
     * 获取 [显示名称]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return this.display_nameDirtyFlag ;
    }   

    /**
     * 获取 [从]
     */
    @JsonProperty("email_from")
    public String getEmail_from(){
        return this.email_from ;
    }

    /**
     * 设置 [从]
     */
    @JsonProperty("email_from")
    public void setEmail_from(String  email_from){
        this.email_from = email_from ;
        this.email_fromDirtyFlag = true ;
    }

     /**
     * 获取 [从]脏标记
     */
    @JsonIgnore
    public boolean getEmail_fromDirtyFlag(){
        return this.email_fromDirtyFlag ;
    }   

    /**
     * 获取 [预期]
     */
    @JsonProperty("expected")
    public Integer getExpected(){
        return this.expected ;
    }

    /**
     * 设置 [预期]
     */
    @JsonProperty("expected")
    public void setExpected(Integer  expected){
        this.expected = expected ;
        this.expectedDirtyFlag = true ;
    }

     /**
     * 获取 [预期]脏标记
     */
    @JsonIgnore
    public boolean getExpectedDirtyFlag(){
        return this.expectedDirtyFlag ;
    }   

    /**
     * 获取 [失败的]
     */
    @JsonProperty("failed")
    public Integer getFailed(){
        return this.failed ;
    }

    /**
     * 设置 [失败的]
     */
    @JsonProperty("failed")
    public void setFailed(Integer  failed){
        this.failed = failed ;
        this.failedDirtyFlag = true ;
    }

     /**
     * 获取 [失败的]脏标记
     */
    @JsonIgnore
    public boolean getFailedDirtyFlag(){
        return this.failedDirtyFlag ;
    }   

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return this.id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

     /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return this.idDirtyFlag ;
    }   

    /**
     * 获取 [忽略]
     */
    @JsonProperty("ignored")
    public Integer getIgnored(){
        return this.ignored ;
    }

    /**
     * 设置 [忽略]
     */
    @JsonProperty("ignored")
    public void setIgnored(Integer  ignored){
        this.ignored = ignored ;
        this.ignoredDirtyFlag = true ;
    }

     /**
     * 获取 [忽略]脏标记
     */
    @JsonIgnore
    public boolean getIgnoredDirtyFlag(){
        return this.ignoredDirtyFlag ;
    }   

    /**
     * 获取 [归档]
     */
    @JsonProperty("keep_archives")
    public String getKeep_archives(){
        return this.keep_archives ;
    }

    /**
     * 设置 [归档]
     */
    @JsonProperty("keep_archives")
    public void setKeep_archives(String  keep_archives){
        this.keep_archives = keep_archives ;
        this.keep_archivesDirtyFlag = true ;
    }

     /**
     * 获取 [归档]脏标记
     */
    @JsonIgnore
    public boolean getKeep_archivesDirtyFlag(){
        return this.keep_archivesDirtyFlag ;
    }   

    /**
     * 获取 [域]
     */
    @JsonProperty("mailing_domain")
    public String getMailing_domain(){
        return this.mailing_domain ;
    }

    /**
     * 设置 [域]
     */
    @JsonProperty("mailing_domain")
    public void setMailing_domain(String  mailing_domain){
        this.mailing_domain = mailing_domain ;
        this.mailing_domainDirtyFlag = true ;
    }

     /**
     * 获取 [域]脏标记
     */
    @JsonIgnore
    public boolean getMailing_domainDirtyFlag(){
        return this.mailing_domainDirtyFlag ;
    }   

    /**
     * 获取 [收件人模型]
     */
    @JsonProperty("mailing_model_id")
    public Integer getMailing_model_id(){
        return this.mailing_model_id ;
    }

    /**
     * 设置 [收件人模型]
     */
    @JsonProperty("mailing_model_id")
    public void setMailing_model_id(Integer  mailing_model_id){
        this.mailing_model_id = mailing_model_id ;
        this.mailing_model_idDirtyFlag = true ;
    }

     /**
     * 获取 [收件人模型]脏标记
     */
    @JsonIgnore
    public boolean getMailing_model_idDirtyFlag(){
        return this.mailing_model_idDirtyFlag ;
    }   

    /**
     * 获取 [收件人模型]
     */
    @JsonProperty("mailing_model_name")
    public String getMailing_model_name(){
        return this.mailing_model_name ;
    }

    /**
     * 设置 [收件人模型]
     */
    @JsonProperty("mailing_model_name")
    public void setMailing_model_name(String  mailing_model_name){
        this.mailing_model_name = mailing_model_name ;
        this.mailing_model_nameDirtyFlag = true ;
    }

     /**
     * 获取 [收件人模型]脏标记
     */
    @JsonIgnore
    public boolean getMailing_model_nameDirtyFlag(){
        return this.mailing_model_nameDirtyFlag ;
    }   

    /**
     * 获取 [收件人实物模型]
     */
    @JsonProperty("mailing_model_real")
    public String getMailing_model_real(){
        return this.mailing_model_real ;
    }

    /**
     * 设置 [收件人实物模型]
     */
    @JsonProperty("mailing_model_real")
    public void setMailing_model_real(String  mailing_model_real){
        this.mailing_model_real = mailing_model_real ;
        this.mailing_model_realDirtyFlag = true ;
    }

     /**
     * 获取 [收件人实物模型]脏标记
     */
    @JsonIgnore
    public boolean getMailing_model_realDirtyFlag(){
        return this.mailing_model_realDirtyFlag ;
    }   

    /**
     * 获取 [邮件服务器]
     */
    @JsonProperty("mail_server_id")
    public Integer getMail_server_id(){
        return this.mail_server_id ;
    }

    /**
     * 设置 [邮件服务器]
     */
    @JsonProperty("mail_server_id")
    public void setMail_server_id(Integer  mail_server_id){
        this.mail_server_id = mail_server_id ;
        this.mail_server_idDirtyFlag = true ;
    }

     /**
     * 获取 [邮件服务器]脏标记
     */
    @JsonIgnore
    public boolean getMail_server_idDirtyFlag(){
        return this.mail_server_idDirtyFlag ;
    }   

    /**
     * 获取 [群发邮件营销]
     */
    @JsonProperty("mass_mailing_campaign_id")
    public Integer getMass_mailing_campaign_id(){
        return this.mass_mailing_campaign_id ;
    }

    /**
     * 设置 [群发邮件营销]
     */
    @JsonProperty("mass_mailing_campaign_id")
    public void setMass_mailing_campaign_id(Integer  mass_mailing_campaign_id){
        this.mass_mailing_campaign_id = mass_mailing_campaign_id ;
        this.mass_mailing_campaign_idDirtyFlag = true ;
    }

     /**
     * 获取 [群发邮件营销]脏标记
     */
    @JsonIgnore
    public boolean getMass_mailing_campaign_idDirtyFlag(){
        return this.mass_mailing_campaign_idDirtyFlag ;
    }   

    /**
     * 获取 [群发邮件营销]
     */
    @JsonProperty("mass_mailing_campaign_id_text")
    public String getMass_mailing_campaign_id_text(){
        return this.mass_mailing_campaign_id_text ;
    }

    /**
     * 设置 [群发邮件营销]
     */
    @JsonProperty("mass_mailing_campaign_id_text")
    public void setMass_mailing_campaign_id_text(String  mass_mailing_campaign_id_text){
        this.mass_mailing_campaign_id_text = mass_mailing_campaign_id_text ;
        this.mass_mailing_campaign_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [群发邮件营销]脏标记
     */
    @JsonIgnore
    public boolean getMass_mailing_campaign_id_textDirtyFlag(){
        return this.mass_mailing_campaign_id_textDirtyFlag ;
    }   

    /**
     * 获取 [媒体]
     */
    @JsonProperty("medium_id")
    public Integer getMedium_id(){
        return this.medium_id ;
    }

    /**
     * 设置 [媒体]
     */
    @JsonProperty("medium_id")
    public void setMedium_id(Integer  medium_id){
        this.medium_id = medium_id ;
        this.medium_idDirtyFlag = true ;
    }

     /**
     * 获取 [媒体]脏标记
     */
    @JsonIgnore
    public boolean getMedium_idDirtyFlag(){
        return this.medium_idDirtyFlag ;
    }   

    /**
     * 获取 [媒体]
     */
    @JsonProperty("medium_id_text")
    public String getMedium_id_text(){
        return this.medium_id_text ;
    }

    /**
     * 设置 [媒体]
     */
    @JsonProperty("medium_id_text")
    public void setMedium_id_text(String  medium_id_text){
        this.medium_id_text = medium_id_text ;
        this.medium_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [媒体]脏标记
     */
    @JsonIgnore
    public boolean getMedium_id_textDirtyFlag(){
        return this.medium_id_textDirtyFlag ;
    }   

    /**
     * 获取 [来源名称]
     */
    @JsonProperty("name")
    public String getName(){
        return this.name ;
    }

    /**
     * 设置 [来源名称]
     */
    @JsonProperty("name")
    public void setName(String  name){
        this.name = name ;
        this.nameDirtyFlag = true ;
    }

     /**
     * 获取 [来源名称]脏标记
     */
    @JsonIgnore
    public boolean getNameDirtyFlag(){
        return this.nameDirtyFlag ;
    }   

    /**
     * 获取 [安排的日期]
     */
    @JsonProperty("next_departure")
    public Timestamp getNext_departure(){
        return this.next_departure ;
    }

    /**
     * 设置 [安排的日期]
     */
    @JsonProperty("next_departure")
    public void setNext_departure(Timestamp  next_departure){
        this.next_departure = next_departure ;
        this.next_departureDirtyFlag = true ;
    }

     /**
     * 获取 [安排的日期]脏标记
     */
    @JsonIgnore
    public boolean getNext_departureDirtyFlag(){
        return this.next_departureDirtyFlag ;
    }   

    /**
     * 获取 [已开启]
     */
    @JsonProperty("opened")
    public Integer getOpened(){
        return this.opened ;
    }

    /**
     * 设置 [已开启]
     */
    @JsonProperty("opened")
    public void setOpened(Integer  opened){
        this.opened = opened ;
        this.openedDirtyFlag = true ;
    }

     /**
     * 获取 [已开启]脏标记
     */
    @JsonIgnore
    public boolean getOpenedDirtyFlag(){
        return this.openedDirtyFlag ;
    }   

    /**
     * 获取 [打开比例]
     */
    @JsonProperty("opened_ratio")
    public Integer getOpened_ratio(){
        return this.opened_ratio ;
    }

    /**
     * 设置 [打开比例]
     */
    @JsonProperty("opened_ratio")
    public void setOpened_ratio(Integer  opened_ratio){
        this.opened_ratio = opened_ratio ;
        this.opened_ratioDirtyFlag = true ;
    }

     /**
     * 获取 [打开比例]脏标记
     */
    @JsonIgnore
    public boolean getOpened_ratioDirtyFlag(){
        return this.opened_ratioDirtyFlag ;
    }   

    /**
     * 获取 [已接收比例]
     */
    @JsonProperty("received_ratio")
    public Integer getReceived_ratio(){
        return this.received_ratio ;
    }

    /**
     * 设置 [已接收比例]
     */
    @JsonProperty("received_ratio")
    public void setReceived_ratio(Integer  received_ratio){
        this.received_ratio = received_ratio ;
        this.received_ratioDirtyFlag = true ;
    }

     /**
     * 获取 [已接收比例]脏标记
     */
    @JsonIgnore
    public boolean getReceived_ratioDirtyFlag(){
        return this.received_ratioDirtyFlag ;
    }   

    /**
     * 获取 [已回复]
     */
    @JsonProperty("replied")
    public Integer getReplied(){
        return this.replied ;
    }

    /**
     * 设置 [已回复]
     */
    @JsonProperty("replied")
    public void setReplied(Integer  replied){
        this.replied = replied ;
        this.repliedDirtyFlag = true ;
    }

     /**
     * 获取 [已回复]脏标记
     */
    @JsonIgnore
    public boolean getRepliedDirtyFlag(){
        return this.repliedDirtyFlag ;
    }   

    /**
     * 获取 [回复比例]
     */
    @JsonProperty("replied_ratio")
    public Integer getReplied_ratio(){
        return this.replied_ratio ;
    }

    /**
     * 设置 [回复比例]
     */
    @JsonProperty("replied_ratio")
    public void setReplied_ratio(Integer  replied_ratio){
        this.replied_ratio = replied_ratio ;
        this.replied_ratioDirtyFlag = true ;
    }

     /**
     * 获取 [回复比例]脏标记
     */
    @JsonIgnore
    public boolean getReplied_ratioDirtyFlag(){
        return this.replied_ratioDirtyFlag ;
    }   

    /**
     * 获取 [回复]
     */
    @JsonProperty("reply_to")
    public String getReply_to(){
        return this.reply_to ;
    }

    /**
     * 设置 [回复]
     */
    @JsonProperty("reply_to")
    public void setReply_to(String  reply_to){
        this.reply_to = reply_to ;
        this.reply_toDirtyFlag = true ;
    }

     /**
     * 获取 [回复]脏标记
     */
    @JsonIgnore
    public boolean getReply_toDirtyFlag(){
        return this.reply_toDirtyFlag ;
    }   

    /**
     * 获取 [回复模式]
     */
    @JsonProperty("reply_to_mode")
    public String getReply_to_mode(){
        return this.reply_to_mode ;
    }

    /**
     * 设置 [回复模式]
     */
    @JsonProperty("reply_to_mode")
    public void setReply_to_mode(String  reply_to_mode){
        this.reply_to_mode = reply_to_mode ;
        this.reply_to_modeDirtyFlag = true ;
    }

     /**
     * 获取 [回复模式]脏标记
     */
    @JsonIgnore
    public boolean getReply_to_modeDirtyFlag(){
        return this.reply_to_modeDirtyFlag ;
    }   

    /**
     * 获取 [开票金额]
     */
    @JsonProperty("sale_invoiced_amount")
    public Integer getSale_invoiced_amount(){
        return this.sale_invoiced_amount ;
    }

    /**
     * 设置 [开票金额]
     */
    @JsonProperty("sale_invoiced_amount")
    public void setSale_invoiced_amount(Integer  sale_invoiced_amount){
        this.sale_invoiced_amount = sale_invoiced_amount ;
        this.sale_invoiced_amountDirtyFlag = true ;
    }

     /**
     * 获取 [开票金额]脏标记
     */
    @JsonIgnore
    public boolean getSale_invoiced_amountDirtyFlag(){
        return this.sale_invoiced_amountDirtyFlag ;
    }   

    /**
     * 获取 [报价个数]
     */
    @JsonProperty("sale_quotation_count")
    public Integer getSale_quotation_count(){
        return this.sale_quotation_count ;
    }

    /**
     * 设置 [报价个数]
     */
    @JsonProperty("sale_quotation_count")
    public void setSale_quotation_count(Integer  sale_quotation_count){
        this.sale_quotation_count = sale_quotation_count ;
        this.sale_quotation_countDirtyFlag = true ;
    }

     /**
     * 获取 [报价个数]脏标记
     */
    @JsonIgnore
    public boolean getSale_quotation_countDirtyFlag(){
        return this.sale_quotation_countDirtyFlag ;
    }   

    /**
     * 获取 [安排]
     */
    @JsonProperty("scheduled")
    public Integer getScheduled(){
        return this.scheduled ;
    }

    /**
     * 设置 [安排]
     */
    @JsonProperty("scheduled")
    public void setScheduled(Integer  scheduled){
        this.scheduled = scheduled ;
        this.scheduledDirtyFlag = true ;
    }

     /**
     * 获取 [安排]脏标记
     */
    @JsonIgnore
    public boolean getScheduledDirtyFlag(){
        return this.scheduledDirtyFlag ;
    }   

    /**
     * 获取 [在将来计划]
     */
    @JsonProperty("schedule_date")
    public Timestamp getSchedule_date(){
        return this.schedule_date ;
    }

    /**
     * 设置 [在将来计划]
     */
    @JsonProperty("schedule_date")
    public void setSchedule_date(Timestamp  schedule_date){
        this.schedule_date = schedule_date ;
        this.schedule_dateDirtyFlag = true ;
    }

     /**
     * 获取 [在将来计划]脏标记
     */
    @JsonIgnore
    public boolean getSchedule_dateDirtyFlag(){
        return this.schedule_dateDirtyFlag ;
    }   

    /**
     * 获取 [已汇]
     */
    @JsonProperty("sent")
    public Integer getSent(){
        return this.sent ;
    }

    /**
     * 设置 [已汇]
     */
    @JsonProperty("sent")
    public void setSent(Integer  sent){
        this.sent = sent ;
        this.sentDirtyFlag = true ;
    }

     /**
     * 获取 [已汇]脏标记
     */
    @JsonIgnore
    public boolean getSentDirtyFlag(){
        return this.sentDirtyFlag ;
    }   

    /**
     * 获取 [发送日期]
     */
    @JsonProperty("sent_date")
    public Timestamp getSent_date(){
        return this.sent_date ;
    }

    /**
     * 设置 [发送日期]
     */
    @JsonProperty("sent_date")
    public void setSent_date(Timestamp  sent_date){
        this.sent_date = sent_date ;
        this.sent_dateDirtyFlag = true ;
    }

     /**
     * 获取 [发送日期]脏标记
     */
    @JsonIgnore
    public boolean getSent_dateDirtyFlag(){
        return this.sent_dateDirtyFlag ;
    }   

    /**
     * 获取 [主题]
     */
    @JsonProperty("source_id")
    public Integer getSource_id(){
        return this.source_id ;
    }

    /**
     * 设置 [主题]
     */
    @JsonProperty("source_id")
    public void setSource_id(Integer  source_id){
        this.source_id = source_id ;
        this.source_idDirtyFlag = true ;
    }

     /**
     * 获取 [主题]脏标记
     */
    @JsonIgnore
    public boolean getSource_idDirtyFlag(){
        return this.source_idDirtyFlag ;
    }   

    /**
     * 获取 [状态]
     */
    @JsonProperty("state")
    public String getState(){
        return this.state ;
    }

    /**
     * 设置 [状态]
     */
    @JsonProperty("state")
    public void setState(String  state){
        this.state = state ;
        this.stateDirtyFlag = true ;
    }

     /**
     * 获取 [状态]脏标记
     */
    @JsonIgnore
    public boolean getStateDirtyFlag(){
        return this.stateDirtyFlag ;
    }   

    /**
     * 获取 [邮件统计]
     */
    @JsonProperty("statistics_ids")
    public String getStatistics_ids(){
        return this.statistics_ids ;
    }

    /**
     * 设置 [邮件统计]
     */
    @JsonProperty("statistics_ids")
    public void setStatistics_ids(String  statistics_ids){
        this.statistics_ids = statistics_ids ;
        this.statistics_idsDirtyFlag = true ;
    }

     /**
     * 获取 [邮件统计]脏标记
     */
    @JsonIgnore
    public boolean getStatistics_idsDirtyFlag(){
        return this.statistics_idsDirtyFlag ;
    }   

    /**
     * 获取 [总计]
     */
    @JsonProperty("total")
    public Integer getTotal(){
        return this.total ;
    }

    /**
     * 设置 [总计]
     */
    @JsonProperty("total")
    public void setTotal(Integer  total){
        this.total = total ;
        this.totalDirtyFlag = true ;
    }

     /**
     * 获取 [总计]脏标记
     */
    @JsonIgnore
    public boolean getTotalDirtyFlag(){
        return this.totalDirtyFlag ;
    }   

    /**
     * 获取 [邮件管理器]
     */
    @JsonProperty("user_id")
    public Integer getUser_id(){
        return this.user_id ;
    }

    /**
     * 设置 [邮件管理器]
     */
    @JsonProperty("user_id")
    public void setUser_id(Integer  user_id){
        this.user_id = user_id ;
        this.user_idDirtyFlag = true ;
    }

     /**
     * 获取 [邮件管理器]脏标记
     */
    @JsonIgnore
    public boolean getUser_idDirtyFlag(){
        return this.user_idDirtyFlag ;
    }   

    /**
     * 获取 [邮件管理器]
     */
    @JsonProperty("user_id_text")
    public String getUser_id_text(){
        return this.user_id_text ;
    }

    /**
     * 设置 [邮件管理器]
     */
    @JsonProperty("user_id_text")
    public void setUser_id_text(String  user_id_text){
        this.user_id_text = user_id_text ;
        this.user_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [邮件管理器]脏标记
     */
    @JsonIgnore
    public boolean getUser_id_textDirtyFlag(){
        return this.user_id_textDirtyFlag ;
    }   

    /**
     * 获取 [最后更新时间]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return this.write_date ;
    }

    /**
     * 设置 [最后更新时间]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新时间]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return this.write_dateDirtyFlag ;
    }   

    /**
     * 获取 [最后更新者]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return this.write_uid ;
    }

    /**
     * 设置 [最后更新者]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新者]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return this.write_uidDirtyFlag ;
    }   

    /**
     * 获取 [最后更新者]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return this.write_uid_text ;
    }

    /**
     * 设置 [最后更新者]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新者]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return this.write_uid_textDirtyFlag ;
    }   

    /**
     * 获取 [最后修改日]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return this.__last_update ;
    }

    /**
     * 设置 [最后修改日]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

     /**
     * 获取 [最后修改日]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return this.__last_updateDirtyFlag ;
    }   



    public void fromMap(Map<String, Object> map) throws Exception {
		if(map.get("active") instanceof Boolean){
			this.setActive(((Boolean)map.get("active"))? "true" : "false");
		}
		if(!(map.get("attachment_ids") instanceof Boolean)&& map.get("attachment_ids")!=null){
			Object[] objs = (Object[])map.get("attachment_ids");
			if(objs.length > 0){
				Integer[] attachment_ids = Arrays.copyOfRange(objs,0,objs.length,Integer[].class);
				this.setAttachment_ids(Arrays.toString(attachment_ids).replace(" ",""));
			}
		}
		if(!(map.get("body_html") instanceof Boolean)&& map.get("body_html")!=null){
			this.setBody_html((String)map.get("body_html"));
		}
		if(!(map.get("bounced") instanceof Boolean)&& map.get("bounced")!=null){
			this.setBounced((Integer)map.get("bounced"));
		}
		if(!(map.get("bounced_ratio") instanceof Boolean)&& map.get("bounced_ratio")!=null){
			this.setBounced_ratio((Integer)map.get("bounced_ratio"));
		}
		if(!(map.get("campaign_id") instanceof Boolean)&& map.get("campaign_id")!=null){
			Object[] objs = (Object[])map.get("campaign_id");
			if(objs.length > 0){
				this.setCampaign_id((Integer)objs[0]);
			}
		}
		if(!(map.get("campaign_id") instanceof Boolean)&& map.get("campaign_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("campaign_id");
			if(objs.length > 1){
				this.setCampaign_id_text((String)objs[1]);
			}
		}
		if(!(map.get("clicked") instanceof Boolean)&& map.get("clicked")!=null){
			this.setClicked((Integer)map.get("clicked"));
		}
		if(!(map.get("clicks_ratio") instanceof Boolean)&& map.get("clicks_ratio")!=null){
			this.setClicks_ratio((Integer)map.get("clicks_ratio"));
		}
		if(!(map.get("color") instanceof Boolean)&& map.get("color")!=null){
			this.setColor((Integer)map.get("color"));
		}
		if(!(map.get("contact_ab_pc") instanceof Boolean)&& map.get("contact_ab_pc")!=null){
			this.setContact_ab_pc((Integer)map.get("contact_ab_pc"));
		}
		if(!(map.get("contact_list_ids") instanceof Boolean)&& map.get("contact_list_ids")!=null){
			Object[] objs = (Object[])map.get("contact_list_ids");
			if(objs.length > 0){
				Integer[] contact_list_ids = Arrays.copyOfRange(objs,0,objs.length,Integer[].class);
				this.setContact_list_ids(Arrays.toString(contact_list_ids).replace(" ",""));
			}
		}
		if(!(map.get("create_date") instanceof Boolean)&& map.get("create_date")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse((String)map.get("create_date"));
   			this.setCreate_date(new Timestamp(parse.getTime()));
		}
		if(!(map.get("create_uid") instanceof Boolean)&& map.get("create_uid")!=null){
			Object[] objs = (Object[])map.get("create_uid");
			if(objs.length > 0){
				this.setCreate_uid((Integer)objs[0]);
			}
		}
		if(!(map.get("create_uid") instanceof Boolean)&& map.get("create_uid")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("create_uid");
			if(objs.length > 1){
				this.setCreate_uid_text((String)objs[1]);
			}
		}
		if(map.get("crm_lead_activated") instanceof Boolean){
			this.setCrm_lead_activated(((Boolean)map.get("crm_lead_activated"))? "true" : "false");
		}
		if(!(map.get("crm_lead_count") instanceof Boolean)&& map.get("crm_lead_count")!=null){
			this.setCrm_lead_count((Integer)map.get("crm_lead_count"));
		}
		if(!(map.get("crm_opportunities_count") instanceof Boolean)&& map.get("crm_opportunities_count")!=null){
			this.setCrm_opportunities_count((Integer)map.get("crm_opportunities_count"));
		}
		if(!(map.get("delivered") instanceof Boolean)&& map.get("delivered")!=null){
			this.setDelivered((Integer)map.get("delivered"));
		}
		if(!(map.get("display_name") instanceof Boolean)&& map.get("display_name")!=null){
			this.setDisplay_name((String)map.get("display_name"));
		}
		if(!(map.get("email_from") instanceof Boolean)&& map.get("email_from")!=null){
			this.setEmail_from((String)map.get("email_from"));
		}
		if(!(map.get("expected") instanceof Boolean)&& map.get("expected")!=null){
			this.setExpected((Integer)map.get("expected"));
		}
		if(!(map.get("failed") instanceof Boolean)&& map.get("failed")!=null){
			this.setFailed((Integer)map.get("failed"));
		}
		if(!(map.get("id") instanceof Boolean)&& map.get("id")!=null){
			this.setId((Integer)map.get("id"));
		}
		if(!(map.get("ignored") instanceof Boolean)&& map.get("ignored")!=null){
			this.setIgnored((Integer)map.get("ignored"));
		}
		if(map.get("keep_archives") instanceof Boolean){
			this.setKeep_archives(((Boolean)map.get("keep_archives"))? "true" : "false");
		}
		if(!(map.get("mailing_domain") instanceof Boolean)&& map.get("mailing_domain")!=null){
			this.setMailing_domain((String)map.get("mailing_domain"));
		}
		if(!(map.get("mailing_model_id") instanceof Boolean)&& map.get("mailing_model_id")!=null){
			Object[] objs = (Object[])map.get("mailing_model_id");
			if(objs.length > 0){
				this.setMailing_model_id((Integer)objs[0]);
			}
		}
		if(!(map.get("mailing_model_name") instanceof Boolean)&& map.get("mailing_model_name")!=null){
			this.setMailing_model_name((String)map.get("mailing_model_name"));
		}
		if(!(map.get("mailing_model_real") instanceof Boolean)&& map.get("mailing_model_real")!=null){
			this.setMailing_model_real((String)map.get("mailing_model_real"));
		}
		if(!(map.get("mail_server_id") instanceof Boolean)&& map.get("mail_server_id")!=null){
			Object[] objs = (Object[])map.get("mail_server_id");
			if(objs.length > 0){
				this.setMail_server_id((Integer)objs[0]);
			}
		}
		if(!(map.get("mass_mailing_campaign_id") instanceof Boolean)&& map.get("mass_mailing_campaign_id")!=null){
			Object[] objs = (Object[])map.get("mass_mailing_campaign_id");
			if(objs.length > 0){
				this.setMass_mailing_campaign_id((Integer)objs[0]);
			}
		}
		if(!(map.get("mass_mailing_campaign_id") instanceof Boolean)&& map.get("mass_mailing_campaign_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("mass_mailing_campaign_id");
			if(objs.length > 1){
				this.setMass_mailing_campaign_id_text((String)objs[1]);
			}
		}
		if(!(map.get("medium_id") instanceof Boolean)&& map.get("medium_id")!=null){
			Object[] objs = (Object[])map.get("medium_id");
			if(objs.length > 0){
				this.setMedium_id((Integer)objs[0]);
			}
		}
		if(!(map.get("medium_id") instanceof Boolean)&& map.get("medium_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("medium_id");
			if(objs.length > 1){
				this.setMedium_id_text((String)objs[1]);
			}
		}
		if(!(map.get("name") instanceof Boolean)&& map.get("name")!=null){
			this.setName((String)map.get("name"));
		}
		if(!(map.get("next_departure") instanceof Boolean)&& map.get("next_departure")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse((String)map.get("next_departure"));
   			this.setNext_departure(new Timestamp(parse.getTime()));
		}
		if(!(map.get("opened") instanceof Boolean)&& map.get("opened")!=null){
			this.setOpened((Integer)map.get("opened"));
		}
		if(!(map.get("opened_ratio") instanceof Boolean)&& map.get("opened_ratio")!=null){
			this.setOpened_ratio((Integer)map.get("opened_ratio"));
		}
		if(!(map.get("received_ratio") instanceof Boolean)&& map.get("received_ratio")!=null){
			this.setReceived_ratio((Integer)map.get("received_ratio"));
		}
		if(!(map.get("replied") instanceof Boolean)&& map.get("replied")!=null){
			this.setReplied((Integer)map.get("replied"));
		}
		if(!(map.get("replied_ratio") instanceof Boolean)&& map.get("replied_ratio")!=null){
			this.setReplied_ratio((Integer)map.get("replied_ratio"));
		}
		if(!(map.get("reply_to") instanceof Boolean)&& map.get("reply_to")!=null){
			this.setReply_to((String)map.get("reply_to"));
		}
		if(!(map.get("reply_to_mode") instanceof Boolean)&& map.get("reply_to_mode")!=null){
			this.setReply_to_mode((String)map.get("reply_to_mode"));
		}
		if(!(map.get("sale_invoiced_amount") instanceof Boolean)&& map.get("sale_invoiced_amount")!=null){
			this.setSale_invoiced_amount((Integer)map.get("sale_invoiced_amount"));
		}
		if(!(map.get("sale_quotation_count") instanceof Boolean)&& map.get("sale_quotation_count")!=null){
			this.setSale_quotation_count((Integer)map.get("sale_quotation_count"));
		}
		if(!(map.get("scheduled") instanceof Boolean)&& map.get("scheduled")!=null){
			this.setScheduled((Integer)map.get("scheduled"));
		}
		if(!(map.get("schedule_date") instanceof Boolean)&& map.get("schedule_date")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse((String)map.get("schedule_date"));
   			this.setSchedule_date(new Timestamp(parse.getTime()));
		}
		if(!(map.get("sent") instanceof Boolean)&& map.get("sent")!=null){
			this.setSent((Integer)map.get("sent"));
		}
		if(!(map.get("sent_date") instanceof Boolean)&& map.get("sent_date")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse((String)map.get("sent_date"));
   			this.setSent_date(new Timestamp(parse.getTime()));
		}
		if(!(map.get("source_id") instanceof Boolean)&& map.get("source_id")!=null){
			Object[] objs = (Object[])map.get("source_id");
			if(objs.length > 0){
				this.setSource_id((Integer)objs[0]);
			}
		}
		if(!(map.get("state") instanceof Boolean)&& map.get("state")!=null){
			this.setState((String)map.get("state"));
		}
		if(!(map.get("statistics_ids") instanceof Boolean)&& map.get("statistics_ids")!=null){
			Object[] objs = (Object[])map.get("statistics_ids");
			if(objs.length > 0){
				Integer[] statistics_ids = Arrays.copyOfRange(objs,0,objs.length,Integer[].class);
				this.setStatistics_ids(Arrays.toString(statistics_ids).replace(" ",""));
			}
		}
		if(!(map.get("total") instanceof Boolean)&& map.get("total")!=null){
			this.setTotal((Integer)map.get("total"));
		}
		if(!(map.get("user_id") instanceof Boolean)&& map.get("user_id")!=null){
			Object[] objs = (Object[])map.get("user_id");
			if(objs.length > 0){
				this.setUser_id((Integer)objs[0]);
			}
		}
		if(!(map.get("user_id") instanceof Boolean)&& map.get("user_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("user_id");
			if(objs.length > 1){
				this.setUser_id_text((String)objs[1]);
			}
		}
		if(!(map.get("write_date") instanceof Boolean)&& map.get("write_date")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse((String)map.get("write_date"));
   			this.setWrite_date(new Timestamp(parse.getTime()));
		}
		if(!(map.get("write_uid") instanceof Boolean)&& map.get("write_uid")!=null){
			Object[] objs = (Object[])map.get("write_uid");
			if(objs.length > 0){
				this.setWrite_uid((Integer)objs[0]);
			}
		}
		if(!(map.get("write_uid") instanceof Boolean)&& map.get("write_uid")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("write_uid");
			if(objs.length > 1){
				this.setWrite_uid_text((String)objs[1]);
			}
		}
		if(!(map.get("__last_update") instanceof Boolean)&& map.get("__last_update")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse((String)map.get("__last_update"));
   			this.set__last_update(new Timestamp(parse.getTime()));
		}
	}

	public Map<String, Object> toMap() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		if(this.getActive()!=null&&this.getActiveDirtyFlag()){
			map.put("active",Boolean.parseBoolean(this.getActive()));		
		}		if(this.getAttachment_ids()!=null&&this.getAttachment_idsDirtyFlag()){
			map.put("attachment_ids",this.getAttachment_ids());
		}else if(this.getAttachment_idsDirtyFlag()){
			map.put("attachment_ids",false);
		}
		if(this.getBody_html()!=null&&this.getBody_htmlDirtyFlag()){
			map.put("body_html",this.getBody_html());
		}else if(this.getBody_htmlDirtyFlag()){
			map.put("body_html",false);
		}
		if(this.getBounced()!=null&&this.getBouncedDirtyFlag()){
			map.put("bounced",this.getBounced());
		}else if(this.getBouncedDirtyFlag()){
			map.put("bounced",false);
		}
		if(this.getBounced_ratio()!=null&&this.getBounced_ratioDirtyFlag()){
			map.put("bounced_ratio",this.getBounced_ratio());
		}else if(this.getBounced_ratioDirtyFlag()){
			map.put("bounced_ratio",false);
		}
		if(this.getCampaign_id()!=null&&this.getCampaign_idDirtyFlag()){
			map.put("campaign_id",this.getCampaign_id());
		}else if(this.getCampaign_idDirtyFlag()){
			map.put("campaign_id",false);
		}
		if(this.getCampaign_id_text()!=null&&this.getCampaign_id_textDirtyFlag()){
			//忽略文本外键campaign_id_text
		}else if(this.getCampaign_id_textDirtyFlag()){
			map.put("campaign_id",false);
		}
		if(this.getClicked()!=null&&this.getClickedDirtyFlag()){
			map.put("clicked",this.getClicked());
		}else if(this.getClickedDirtyFlag()){
			map.put("clicked",false);
		}
		if(this.getClicks_ratio()!=null&&this.getClicks_ratioDirtyFlag()){
			map.put("clicks_ratio",this.getClicks_ratio());
		}else if(this.getClicks_ratioDirtyFlag()){
			map.put("clicks_ratio",false);
		}
		if(this.getColor()!=null&&this.getColorDirtyFlag()){
			map.put("color",this.getColor());
		}else if(this.getColorDirtyFlag()){
			map.put("color",false);
		}
		if(this.getContact_ab_pc()!=null&&this.getContact_ab_pcDirtyFlag()){
			map.put("contact_ab_pc",this.getContact_ab_pc());
		}else if(this.getContact_ab_pcDirtyFlag()){
			map.put("contact_ab_pc",false);
		}
		if(this.getContact_list_ids()!=null&&this.getContact_list_idsDirtyFlag()){
			map.put("contact_list_ids",this.getContact_list_ids());
		}else if(this.getContact_list_idsDirtyFlag()){
			map.put("contact_list_ids",false);
		}
		if(this.getCreate_date()!=null&&this.getCreate_dateDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String datetimeStr = sdf.format(this.getCreate_date());
			map.put("create_date",datetimeStr);
		}else if(this.getCreate_dateDirtyFlag()){
			map.put("create_date",false);
		}
		if(this.getCreate_uid()!=null&&this.getCreate_uidDirtyFlag()){
			map.put("create_uid",this.getCreate_uid());
		}else if(this.getCreate_uidDirtyFlag()){
			map.put("create_uid",false);
		}
		if(this.getCreate_uid_text()!=null&&this.getCreate_uid_textDirtyFlag()){
			//忽略文本外键create_uid_text
		}else if(this.getCreate_uid_textDirtyFlag()){
			map.put("create_uid",false);
		}
		if(this.getCrm_lead_activated()!=null&&this.getCrm_lead_activatedDirtyFlag()){
			map.put("crm_lead_activated",Boolean.parseBoolean(this.getCrm_lead_activated()));		
		}		if(this.getCrm_lead_count()!=null&&this.getCrm_lead_countDirtyFlag()){
			map.put("crm_lead_count",this.getCrm_lead_count());
		}else if(this.getCrm_lead_countDirtyFlag()){
			map.put("crm_lead_count",false);
		}
		if(this.getCrm_opportunities_count()!=null&&this.getCrm_opportunities_countDirtyFlag()){
			map.put("crm_opportunities_count",this.getCrm_opportunities_count());
		}else if(this.getCrm_opportunities_countDirtyFlag()){
			map.put("crm_opportunities_count",false);
		}
		if(this.getDelivered()!=null&&this.getDeliveredDirtyFlag()){
			map.put("delivered",this.getDelivered());
		}else if(this.getDeliveredDirtyFlag()){
			map.put("delivered",false);
		}
		if(this.getDisplay_name()!=null&&this.getDisplay_nameDirtyFlag()){
			map.put("display_name",this.getDisplay_name());
		}else if(this.getDisplay_nameDirtyFlag()){
			map.put("display_name",false);
		}
		if(this.getEmail_from()!=null&&this.getEmail_fromDirtyFlag()){
			map.put("email_from",this.getEmail_from());
		}else if(this.getEmail_fromDirtyFlag()){
			map.put("email_from",false);
		}
		if(this.getExpected()!=null&&this.getExpectedDirtyFlag()){
			map.put("expected",this.getExpected());
		}else if(this.getExpectedDirtyFlag()){
			map.put("expected",false);
		}
		if(this.getFailed()!=null&&this.getFailedDirtyFlag()){
			map.put("failed",this.getFailed());
		}else if(this.getFailedDirtyFlag()){
			map.put("failed",false);
		}
		if(this.getId()!=null&&this.getIdDirtyFlag()){
			map.put("id",this.getId());
		}else if(this.getIdDirtyFlag()){
			map.put("id",false);
		}
		if(this.getIgnored()!=null&&this.getIgnoredDirtyFlag()){
			map.put("ignored",this.getIgnored());
		}else if(this.getIgnoredDirtyFlag()){
			map.put("ignored",false);
		}
		if(this.getKeep_archives()!=null&&this.getKeep_archivesDirtyFlag()){
			map.put("keep_archives",Boolean.parseBoolean(this.getKeep_archives()));		
		}		if(this.getMailing_domain()!=null&&this.getMailing_domainDirtyFlag()){
			map.put("mailing_domain",this.getMailing_domain());
		}else if(this.getMailing_domainDirtyFlag()){
			map.put("mailing_domain",false);
		}
		if(this.getMailing_model_id()!=null&&this.getMailing_model_idDirtyFlag()){
			map.put("mailing_model_id",this.getMailing_model_id());
		}else if(this.getMailing_model_idDirtyFlag()){
			map.put("mailing_model_id",false);
		}
		if(this.getMailing_model_name()!=null&&this.getMailing_model_nameDirtyFlag()){
			map.put("mailing_model_name",this.getMailing_model_name());
		}else if(this.getMailing_model_nameDirtyFlag()){
			map.put("mailing_model_name",false);
		}
		if(this.getMailing_model_real()!=null&&this.getMailing_model_realDirtyFlag()){
			map.put("mailing_model_real",this.getMailing_model_real());
		}else if(this.getMailing_model_realDirtyFlag()){
			map.put("mailing_model_real",false);
		}
		if(this.getMail_server_id()!=null&&this.getMail_server_idDirtyFlag()){
			map.put("mail_server_id",this.getMail_server_id());
		}else if(this.getMail_server_idDirtyFlag()){
			map.put("mail_server_id",false);
		}
		if(this.getMass_mailing_campaign_id()!=null&&this.getMass_mailing_campaign_idDirtyFlag()){
			map.put("mass_mailing_campaign_id",this.getMass_mailing_campaign_id());
		}else if(this.getMass_mailing_campaign_idDirtyFlag()){
			map.put("mass_mailing_campaign_id",false);
		}
		if(this.getMass_mailing_campaign_id_text()!=null&&this.getMass_mailing_campaign_id_textDirtyFlag()){
			//忽略文本外键mass_mailing_campaign_id_text
		}else if(this.getMass_mailing_campaign_id_textDirtyFlag()){
			map.put("mass_mailing_campaign_id",false);
		}
		if(this.getMedium_id()!=null&&this.getMedium_idDirtyFlag()){
			map.put("medium_id",this.getMedium_id());
		}else if(this.getMedium_idDirtyFlag()){
			map.put("medium_id",false);
		}
		if(this.getMedium_id_text()!=null&&this.getMedium_id_textDirtyFlag()){
			//忽略文本外键medium_id_text
		}else if(this.getMedium_id_textDirtyFlag()){
			map.put("medium_id",false);
		}
		if(this.getName()!=null&&this.getNameDirtyFlag()){
			map.put("name",this.getName());
		}else if(this.getNameDirtyFlag()){
			map.put("name",false);
		}
		if(this.getNext_departure()!=null&&this.getNext_departureDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String datetimeStr = sdf.format(this.getNext_departure());
			map.put("next_departure",datetimeStr);
		}else if(this.getNext_departureDirtyFlag()){
			map.put("next_departure",false);
		}
		if(this.getOpened()!=null&&this.getOpenedDirtyFlag()){
			map.put("opened",this.getOpened());
		}else if(this.getOpenedDirtyFlag()){
			map.put("opened",false);
		}
		if(this.getOpened_ratio()!=null&&this.getOpened_ratioDirtyFlag()){
			map.put("opened_ratio",this.getOpened_ratio());
		}else if(this.getOpened_ratioDirtyFlag()){
			map.put("opened_ratio",false);
		}
		if(this.getReceived_ratio()!=null&&this.getReceived_ratioDirtyFlag()){
			map.put("received_ratio",this.getReceived_ratio());
		}else if(this.getReceived_ratioDirtyFlag()){
			map.put("received_ratio",false);
		}
		if(this.getReplied()!=null&&this.getRepliedDirtyFlag()){
			map.put("replied",this.getReplied());
		}else if(this.getRepliedDirtyFlag()){
			map.put("replied",false);
		}
		if(this.getReplied_ratio()!=null&&this.getReplied_ratioDirtyFlag()){
			map.put("replied_ratio",this.getReplied_ratio());
		}else if(this.getReplied_ratioDirtyFlag()){
			map.put("replied_ratio",false);
		}
		if(this.getReply_to()!=null&&this.getReply_toDirtyFlag()){
			map.put("reply_to",this.getReply_to());
		}else if(this.getReply_toDirtyFlag()){
			map.put("reply_to",false);
		}
		if(this.getReply_to_mode()!=null&&this.getReply_to_modeDirtyFlag()){
			map.put("reply_to_mode",this.getReply_to_mode());
		}else if(this.getReply_to_modeDirtyFlag()){
			map.put("reply_to_mode",false);
		}
		if(this.getSale_invoiced_amount()!=null&&this.getSale_invoiced_amountDirtyFlag()){
			map.put("sale_invoiced_amount",this.getSale_invoiced_amount());
		}else if(this.getSale_invoiced_amountDirtyFlag()){
			map.put("sale_invoiced_amount",false);
		}
		if(this.getSale_quotation_count()!=null&&this.getSale_quotation_countDirtyFlag()){
			map.put("sale_quotation_count",this.getSale_quotation_count());
		}else if(this.getSale_quotation_countDirtyFlag()){
			map.put("sale_quotation_count",false);
		}
		if(this.getScheduled()!=null&&this.getScheduledDirtyFlag()){
			map.put("scheduled",this.getScheduled());
		}else if(this.getScheduledDirtyFlag()){
			map.put("scheduled",false);
		}
		if(this.getSchedule_date()!=null&&this.getSchedule_dateDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String datetimeStr = sdf.format(this.getSchedule_date());
			map.put("schedule_date",datetimeStr);
		}else if(this.getSchedule_dateDirtyFlag()){
			map.put("schedule_date",false);
		}
		if(this.getSent()!=null&&this.getSentDirtyFlag()){
			map.put("sent",this.getSent());
		}else if(this.getSentDirtyFlag()){
			map.put("sent",false);
		}
		if(this.getSent_date()!=null&&this.getSent_dateDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String datetimeStr = sdf.format(this.getSent_date());
			map.put("sent_date",datetimeStr);
		}else if(this.getSent_dateDirtyFlag()){
			map.put("sent_date",false);
		}
		if(this.getSource_id()!=null&&this.getSource_idDirtyFlag()){
			map.put("source_id",this.getSource_id());
		}else if(this.getSource_idDirtyFlag()){
			map.put("source_id",false);
		}
		if(this.getState()!=null&&this.getStateDirtyFlag()){
			map.put("state",this.getState());
		}else if(this.getStateDirtyFlag()){
			map.put("state",false);
		}
		if(this.getStatistics_ids()!=null&&this.getStatistics_idsDirtyFlag()){
			map.put("statistics_ids",this.getStatistics_ids());
		}else if(this.getStatistics_idsDirtyFlag()){
			map.put("statistics_ids",false);
		}
		if(this.getTotal()!=null&&this.getTotalDirtyFlag()){
			map.put("total",this.getTotal());
		}else if(this.getTotalDirtyFlag()){
			map.put("total",false);
		}
		if(this.getUser_id()!=null&&this.getUser_idDirtyFlag()){
			map.put("user_id",this.getUser_id());
		}else if(this.getUser_idDirtyFlag()){
			map.put("user_id",false);
		}
		if(this.getUser_id_text()!=null&&this.getUser_id_textDirtyFlag()){
			//忽略文本外键user_id_text
		}else if(this.getUser_id_textDirtyFlag()){
			map.put("user_id",false);
		}
		if(this.getWrite_date()!=null&&this.getWrite_dateDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String datetimeStr = sdf.format(this.getWrite_date());
			map.put("write_date",datetimeStr);
		}else if(this.getWrite_dateDirtyFlag()){
			map.put("write_date",false);
		}
		if(this.getWrite_uid()!=null&&this.getWrite_uidDirtyFlag()){
			map.put("write_uid",this.getWrite_uid());
		}else if(this.getWrite_uidDirtyFlag()){
			map.put("write_uid",false);
		}
		if(this.getWrite_uid_text()!=null&&this.getWrite_uid_textDirtyFlag()){
			//忽略文本外键write_uid_text
		}else if(this.getWrite_uid_textDirtyFlag()){
			map.put("write_uid",false);
		}
		if(this.get__last_update()!=null&&this.get__last_updateDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String datetimeStr = sdf.format(this.get__last_update());
			map.put("__last_update",datetimeStr);
		}else if(this.get__last_updateDirtyFlag()){
			map.put("__last_update",false);
		}
		return map;
	}

}
