package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Mro_pm_parameter;
import cn.ibizlab.odoo.core.odoo_mro.filter.Mro_pm_parameterSearchContext;

/**
 * 实体 [Asset Parameters] 存储对象
 */
public interface Mro_pm_parameterRepository extends Repository<Mro_pm_parameter> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Mro_pm_parameter> searchDefault(Mro_pm_parameterSearchContext context);

    Mro_pm_parameter convert2PO(cn.ibizlab.odoo.core.odoo_mro.domain.Mro_pm_parameter domain , Mro_pm_parameter po) ;

    cn.ibizlab.odoo.core.odoo_mro.domain.Mro_pm_parameter convert2Domain( Mro_pm_parameter po ,cn.ibizlab.odoo.core.odoo_mro.domain.Mro_pm_parameter domain) ;

}
