package cn.ibizlab.odoo.core.repository.po ;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.math.BigInteger;

import cn.ibizlab.odoo.core.odoo_purchase.filter.Purchase_order_lineSearchContext;

/**
 * 实体 [采购订单行] 存储模型
 */
public interface Purchase_order_line{

    /**
     * 单价
     */
    Double getPrice_unit();

    void setPrice_unit(Double price_unit);

    /**
     * 获取 [单价]脏标记
     */
    boolean getPrice_unitDirtyFlag();

    /**
     * 已接收数量
     */
    Double getQty_received();

    void setQty_received(Double qty_received);

    /**
     * 获取 [已接收数量]脏标记
     */
    boolean getQty_receivedDirtyFlag();

    /**
     * 总计
     */
    Double getPrice_total();

    void setPrice_total(Double price_total);

    /**
     * 获取 [总计]脏标记
     */
    boolean getPrice_totalDirtyFlag();

    /**
     * 说明
     */
    String getName();

    void setName(String name);

    /**
     * 获取 [说明]脏标记
     */
    boolean getNameDirtyFlag();

    /**
     * 数量
     */
    Double getProduct_qty();

    void setProduct_qty(Double product_qty);

    /**
     * 获取 [数量]脏标记
     */
    boolean getProduct_qtyDirtyFlag();

    /**
     * 税率
     */
    Double getPrice_tax();

    void setPrice_tax(Double price_tax);

    /**
     * 获取 [税率]脏标记
     */
    boolean getPrice_taxDirtyFlag();

    /**
     * 保留
     */
    String getMove_ids();

    void setMove_ids(String move_ids);

    /**
     * 获取 [保留]脏标记
     */
    boolean getMove_idsDirtyFlag();

    /**
     * 小计
     */
    Double getPrice_subtotal();

    void setPrice_subtotal(Double price_subtotal);

    /**
     * 获取 [小计]脏标记
     */
    boolean getPrice_subtotalDirtyFlag();

    /**
     * 最后修改日
     */
    Timestamp get__last_update();

    void set__last_update(Timestamp __last_update);

    /**
     * 获取 [最后修改日]脏标记
     */
    boolean get__last_updateDirtyFlag();

    /**
     * 序列
     */
    Integer getSequence();

    void setSequence(Integer sequence);

    /**
     * 获取 [序列]脏标记
     */
    boolean getSequenceDirtyFlag();

    /**
     * 显示名称
     */
    String getDisplay_name();

    void setDisplay_name(String display_name);

    /**
     * 获取 [显示名称]脏标记
     */
    boolean getDisplay_nameDirtyFlag();

    /**
     * 数量总计
     */
    Double getProduct_uom_qty();

    void setProduct_uom_qty(Double product_uom_qty);

    /**
     * 获取 [数量总计]脏标记
     */
    boolean getProduct_uom_qtyDirtyFlag();

    /**
     * 税率
     */
    String getTaxes_id();

    void setTaxes_id(String taxes_id);

    /**
     * 获取 [税率]脏标记
     */
    boolean getTaxes_idDirtyFlag();

    /**
     * 账单明细行
     */
    String getInvoice_lines();

    void setInvoice_lines(String invoice_lines);

    /**
     * 获取 [账单明细行]脏标记
     */
    boolean getInvoice_linesDirtyFlag();

    /**
     * 创建时间
     */
    Timestamp getCreate_date();

    void setCreate_date(Timestamp create_date);

    /**
     * 获取 [创建时间]脏标记
     */
    boolean getCreate_dateDirtyFlag();

    /**
     * 最后更新时间
     */
    Timestamp getWrite_date();

    void setWrite_date(Timestamp write_date);

    /**
     * 获取 [最后更新时间]脏标记
     */
    boolean getWrite_dateDirtyFlag();

    /**
     * 计划日期
     */
    Timestamp getDate_planned();

    void setDate_planned(Timestamp date_planned);

    /**
     * 获取 [计划日期]脏标记
     */
    boolean getDate_plannedDirtyFlag();

    /**
     * 开票数量
     */
    Double getQty_invoiced();

    void setQty_invoiced(Double qty_invoiced);

    /**
     * 获取 [开票数量]脏标记
     */
    boolean getQty_invoicedDirtyFlag();

    /**
     * 分析标签
     */
    String getAnalytic_tag_ids();

    void setAnalytic_tag_ids(String analytic_tag_ids);

    /**
     * 获取 [分析标签]脏标记
     */
    boolean getAnalytic_tag_idsDirtyFlag();

    /**
     * 下游移动
     */
    String getMove_dest_ids();

    void setMove_dest_ids(String move_dest_ids);

    /**
     * 获取 [下游移动]脏标记
     */
    boolean getMove_dest_idsDirtyFlag();

    /**
     * ID
     */
    Integer getId();

    void setId(Integer id);

    /**
     * 获取 [ID]脏标记
     */
    boolean getIdDirtyFlag();

    /**
     * 业务伙伴
     */
    String getPartner_id_text();

    void setPartner_id_text(String partner_id_text);

    /**
     * 获取 [业务伙伴]脏标记
     */
    boolean getPartner_id_textDirtyFlag();

    /**
     * 创建人
     */
    String getCreate_uid_text();

    void setCreate_uid_text(String create_uid_text);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uid_textDirtyFlag();

    /**
     * 订货点
     */
    String getOrderpoint_id_text();

    void setOrderpoint_id_text(String orderpoint_id_text);

    /**
     * 获取 [订货点]脏标记
     */
    boolean getOrderpoint_id_textDirtyFlag();

    /**
     * 销售订单
     */
    String getSale_order_id_text();

    void setSale_order_id_text(String sale_order_id_text);

    /**
     * 获取 [销售订单]脏标记
     */
    boolean getSale_order_id_textDirtyFlag();

    /**
     * 产品
     */
    String getProduct_id_text();

    void setProduct_id_text(String product_id_text);

    /**
     * 获取 [产品]脏标记
     */
    boolean getProduct_id_textDirtyFlag();

    /**
     * 产品类型
     */
    String getProduct_type();

    void setProduct_type(String product_type);

    /**
     * 获取 [产品类型]脏标记
     */
    boolean getProduct_typeDirtyFlag();

    /**
     * 最后更新人
     */
    String getWrite_uid_text();

    void setWrite_uid_text(String write_uid_text);

    /**
     * 获取 [最后更新人]脏标记
     */
    boolean getWrite_uid_textDirtyFlag();

    /**
     * 公司
     */
    String getCompany_id_text();

    void setCompany_id_text(String company_id_text);

    /**
     * 获取 [公司]脏标记
     */
    boolean getCompany_id_textDirtyFlag();

    /**
     * 状态
     */
    String getState();

    void setState(String state);

    /**
     * 获取 [状态]脏标记
     */
    boolean getStateDirtyFlag();

    /**
     * 分析账户
     */
    String getAccount_analytic_id_text();

    void setAccount_analytic_id_text(String account_analytic_id_text);

    /**
     * 获取 [分析账户]脏标记
     */
    boolean getAccount_analytic_id_textDirtyFlag();

    /**
     * 产品图片
     */
    byte[] getProduct_image();

    void setProduct_image(byte[] product_image);

    /**
     * 获取 [产品图片]脏标记
     */
    boolean getProduct_imageDirtyFlag();

    /**
     * 计量单位
     */
    String getProduct_uom_text();

    void setProduct_uom_text(String product_uom_text);

    /**
     * 获取 [计量单位]脏标记
     */
    boolean getProduct_uom_textDirtyFlag();

    /**
     * 订单关联
     */
    String getOrder_id_text();

    void setOrder_id_text(String order_id_text);

    /**
     * 获取 [订单关联]脏标记
     */
    boolean getOrder_id_textDirtyFlag();

    /**
     * 币种
     */
    String getCurrency_id_text();

    void setCurrency_id_text(String currency_id_text);

    /**
     * 获取 [币种]脏标记
     */
    boolean getCurrency_id_textDirtyFlag();

    /**
     * 原销售项
     */
    String getSale_line_id_text();

    void setSale_line_id_text(String sale_line_id_text);

    /**
     * 获取 [原销售项]脏标记
     */
    boolean getSale_line_id_textDirtyFlag();

    /**
     * 单据日期
     */
    Timestamp getDate_order();

    void setDate_order(Timestamp date_order);

    /**
     * 获取 [单据日期]脏标记
     */
    boolean getDate_orderDirtyFlag();

    /**
     * 公司
     */
    Integer getCompany_id();

    void setCompany_id(Integer company_id);

    /**
     * 获取 [公司]脏标记
     */
    boolean getCompany_idDirtyFlag();

    /**
     * 分析账户
     */
    Integer getAccount_analytic_id();

    void setAccount_analytic_id(Integer account_analytic_id);

    /**
     * 获取 [分析账户]脏标记
     */
    boolean getAccount_analytic_idDirtyFlag();

    /**
     * 币种
     */
    Integer getCurrency_id();

    void setCurrency_id(Integer currency_id);

    /**
     * 获取 [币种]脏标记
     */
    boolean getCurrency_idDirtyFlag();

    /**
     * 销售订单
     */
    Integer getSale_order_id();

    void setSale_order_id(Integer sale_order_id);

    /**
     * 获取 [销售订单]脏标记
     */
    boolean getSale_order_idDirtyFlag();

    /**
     * 计量单位
     */
    Integer getProduct_uom();

    void setProduct_uom(Integer product_uom);

    /**
     * 获取 [计量单位]脏标记
     */
    boolean getProduct_uomDirtyFlag();

    /**
     * 原销售项
     */
    Integer getSale_line_id();

    void setSale_line_id(Integer sale_line_id);

    /**
     * 获取 [原销售项]脏标记
     */
    boolean getSale_line_idDirtyFlag();

    /**
     * 订单关联
     */
    Integer getOrder_id();

    void setOrder_id(Integer order_id);

    /**
     * 获取 [订单关联]脏标记
     */
    boolean getOrder_idDirtyFlag();

    /**
     * 订货点
     */
    Integer getOrderpoint_id();

    void setOrderpoint_id(Integer orderpoint_id);

    /**
     * 获取 [订货点]脏标记
     */
    boolean getOrderpoint_idDirtyFlag();

    /**
     * 创建人
     */
    Integer getCreate_uid();

    void setCreate_uid(Integer create_uid);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uidDirtyFlag();

    /**
     * 最后更新人
     */
    Integer getWrite_uid();

    void setWrite_uid(Integer write_uid);

    /**
     * 获取 [最后更新人]脏标记
     */
    boolean getWrite_uidDirtyFlag();

    /**
     * 产品
     */
    Integer getProduct_id();

    void setProduct_id(Integer product_id);

    /**
     * 获取 [产品]脏标记
     */
    boolean getProduct_idDirtyFlag();

    /**
     * 业务伙伴
     */
    Integer getPartner_id();

    void setPartner_id(Integer partner_id);

    /**
     * 获取 [业务伙伴]脏标记
     */
    boolean getPartner_idDirtyFlag();

}
