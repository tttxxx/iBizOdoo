package cn.ibizlab.odoo.core.odoo_mail.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_mail.domain.Mail_activity_type;
import cn.ibizlab.odoo.core.odoo_mail.filter.Mail_activity_typeSearchContext;
import cn.ibizlab.odoo.core.odoo_mail.service.IMail_activity_typeService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;



import cn.ibizlab.odoo.core.odoo_mail.client.mail_activity_typeOdooClient;
import cn.ibizlab.odoo.core.odoo_mail.clientmodel.mail_activity_typeClientModel;

/**
 * 实体[活动类型] 服务对象接口实现
 */
@Slf4j
@Service
public class Mail_activity_typeServiceImpl implements IMail_activity_typeService {

    @Autowired
    mail_activity_typeOdooClient mail_activity_typeOdooClient;


    @Override
    public boolean remove(Integer id) {
        mail_activity_typeClientModel clientModel = new mail_activity_typeClientModel();
        clientModel.setId(id);
		mail_activity_typeOdooClient.remove(clientModel);
        return true; 
    }

    public void removeBatch(Collection<Integer> idList){
    }

    @Override
    public Mail_activity_type get(Integer id) {
        mail_activity_typeClientModel clientModel = new mail_activity_typeClientModel();
        clientModel.setId(id);
		mail_activity_typeOdooClient.get(clientModel);
        Mail_activity_type et = convert2Domain(clientModel,null);
        if(et==null){
            et=new Mail_activity_type();
            et.setId(id);
        }
        else{
        }

        return  et;
    }

    @Override
    public boolean create(Mail_activity_type et) {
        mail_activity_typeClientModel clientModel = convert2Model(et,null);
		mail_activity_typeOdooClient.create(clientModel);
        Mail_activity_type rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Mail_activity_type> list){
    }

    @Override
    public boolean update(Mail_activity_type et) {
        mail_activity_typeClientModel clientModel = convert2Model(et,null);
		mail_activity_typeOdooClient.update(clientModel);
        Mail_activity_type rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }
    
    public void updateBatch(List<Mail_activity_type> list){
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Mail_activity_type> searchDefault(Mail_activity_typeSearchContext context) {
        List<Mail_activity_type> list = new ArrayList<Mail_activity_type>();
        Page<mail_activity_typeClientModel> clientModelList = mail_activity_typeOdooClient.search(context);
        for(mail_activity_typeClientModel clientModel : clientModelList){
            list.add( this.convert2Domain(clientModel,null));
        }
        return new PageImpl<Mail_activity_type>(list, context.getPageable(), clientModelList.getTotalElements());
    }

    public mail_activity_typeClientModel convert2Model(Mail_activity_type domain , mail_activity_typeClientModel model) {
        if(domain == null)
            return model ;
        if(model==null){
            model = new mail_activity_typeClientModel();

        }
        if((Boolean) domain.getExtensionparams().get("dirtyflagenable")){
            if((Boolean) domain.getExtensionparams().get("res_model_changedirtyflag"))
                model.setRes_model_change(domain.getResModelChange());
            if((Boolean) domain.getExtensionparams().get("summarydirtyflag"))
                model.setSummary(domain.getSummary());
            if((Boolean) domain.getExtensionparams().get("force_nextdirtyflag"))
                model.setForce_next(domain.getForceNext());
            if((Boolean) domain.getExtensionparams().get("sequencedirtyflag"))
                model.setSequence(domain.getSequence());
            if((Boolean) domain.getExtensionparams().get("iddirtyflag"))
                model.setId(domain.getId());
            if((Boolean) domain.getExtensionparams().get("categorydirtyflag"))
                model.setCategory(domain.getCategory());
            if((Boolean) domain.getExtensionparams().get("icondirtyflag"))
                model.setIcon(domain.getIcon());
            if((Boolean) domain.getExtensionparams().get("delay_countdirtyflag"))
                model.setDelay_count(domain.getDelayCount());
            if((Boolean) domain.getExtensionparams().get("previous_type_idsdirtyflag"))
                model.setPrevious_type_ids(domain.getPreviousTypeIds());
            if((Boolean) domain.getExtensionparams().get("display_namedirtyflag"))
                model.setDisplay_name(domain.getDisplayName());
            if((Boolean) domain.getExtensionparams().get("activedirtyflag"))
                model.setActive(domain.getActive());
            if((Boolean) domain.getExtensionparams().get("delay_fromdirtyflag"))
                model.setDelay_from(domain.getDelayFrom());
            if((Boolean) domain.getExtensionparams().get("write_datedirtyflag"))
                model.setWrite_date(domain.getWriteDate());
            if((Boolean) domain.getExtensionparams().get("mail_template_idsdirtyflag"))
                model.setMail_template_ids(domain.getMailTemplateIds());
            if((Boolean) domain.getExtensionparams().get("namedirtyflag"))
                model.setName(domain.getName());
            if((Boolean) domain.getExtensionparams().get("create_datedirtyflag"))
                model.setCreate_date(domain.getCreateDate());
            if((Boolean) domain.getExtensionparams().get("res_model_iddirtyflag"))
                model.setRes_model_id(domain.getResModelId());
            if((Boolean) domain.getExtensionparams().get("__last_updatedirtyflag"))
                model.set__last_update(domain.getLastUpdate());
            if((Boolean) domain.getExtensionparams().get("decoration_typedirtyflag"))
                model.setDecoration_type(domain.getDecorationType());
            if((Boolean) domain.getExtensionparams().get("next_type_idsdirtyflag"))
                model.setNext_type_ids(domain.getNextTypeIds());
            if((Boolean) domain.getExtensionparams().get("delay_unitdirtyflag"))
                model.setDelay_unit(domain.getDelayUnit());
            if((Boolean) domain.getExtensionparams().get("initial_res_model_iddirtyflag"))
                model.setInitial_res_model_id(domain.getInitialResModelId());
            if((Boolean) domain.getExtensionparams().get("default_next_type_id_textdirtyflag"))
                model.setDefault_next_type_id_text(domain.getDefaultNextTypeIdText());
            if((Boolean) domain.getExtensionparams().get("write_uid_textdirtyflag"))
                model.setWrite_uid_text(domain.getWriteUidText());
            if((Boolean) domain.getExtensionparams().get("create_uid_textdirtyflag"))
                model.setCreate_uid_text(domain.getCreateUidText());
            if((Boolean) domain.getExtensionparams().get("create_uiddirtyflag"))
                model.setCreate_uid(domain.getCreateUid());
            if((Boolean) domain.getExtensionparams().get("write_uiddirtyflag"))
                model.setWrite_uid(domain.getWriteUid());
            if((Boolean) domain.getExtensionparams().get("default_next_type_iddirtyflag"))
                model.setDefault_next_type_id(domain.getDefaultNextTypeId());
            domain.getFocusNull().clear();
        }
        return model ;
    }

    public Mail_activity_type convert2Domain( mail_activity_typeClientModel model ,Mail_activity_type domain) {
        if(model == null)
            return domain ;
        if(domain == null){
            domain = new Mail_activity_type();
        }

        if(model.getRes_model_changeDirtyFlag())
            domain.setResModelChange(model.getRes_model_change());
        if(model.getSummaryDirtyFlag())
            domain.setSummary(model.getSummary());
        if(model.getForce_nextDirtyFlag())
            domain.setForceNext(model.getForce_next());
        if(model.getSequenceDirtyFlag())
            domain.setSequence(model.getSequence());
        if(model.getIdDirtyFlag())
            domain.setId(model.getId());
        if(model.getCategoryDirtyFlag())
            domain.setCategory(model.getCategory());
        if(model.getIconDirtyFlag())
            domain.setIcon(model.getIcon());
        if(model.getDelay_countDirtyFlag())
            domain.setDelayCount(model.getDelay_count());
        if(model.getPrevious_type_idsDirtyFlag())
            domain.setPreviousTypeIds(model.getPrevious_type_ids());
        if(model.getDisplay_nameDirtyFlag())
            domain.setDisplayName(model.getDisplay_name());
        if(model.getActiveDirtyFlag())
            domain.setActive(model.getActive());
        if(model.getDelay_fromDirtyFlag())
            domain.setDelayFrom(model.getDelay_from());
        if(model.getWrite_dateDirtyFlag())
            domain.setWriteDate(model.getWrite_date());
        if(model.getMail_template_idsDirtyFlag())
            domain.setMailTemplateIds(model.getMail_template_ids());
        if(model.getNameDirtyFlag())
            domain.setName(model.getName());
        if(model.getCreate_dateDirtyFlag())
            domain.setCreateDate(model.getCreate_date());
        if(model.getRes_model_idDirtyFlag())
            domain.setResModelId(model.getRes_model_id());
        if(model.get__last_updateDirtyFlag())
            domain.setLastUpdate(model.get__last_update());
        if(model.getDecoration_typeDirtyFlag())
            domain.setDecorationType(model.getDecoration_type());
        if(model.getNext_type_idsDirtyFlag())
            domain.setNextTypeIds(model.getNext_type_ids());
        if(model.getDelay_unitDirtyFlag())
            domain.setDelayUnit(model.getDelay_unit());
        if(model.getInitial_res_model_idDirtyFlag())
            domain.setInitialResModelId(model.getInitial_res_model_id());
        if(model.getDefault_next_type_id_textDirtyFlag())
            domain.setDefaultNextTypeIdText(model.getDefault_next_type_id_text());
        if(model.getWrite_uid_textDirtyFlag())
            domain.setWriteUidText(model.getWrite_uid_text());
        if(model.getCreate_uid_textDirtyFlag())
            domain.setCreateUidText(model.getCreate_uid_text());
        if(model.getCreate_uidDirtyFlag())
            domain.setCreateUid(model.getCreate_uid());
        if(model.getWrite_uidDirtyFlag())
            domain.setWriteUid(model.getWrite_uid());
        if(model.getDefault_next_type_idDirtyFlag())
            domain.setDefaultNextTypeId(model.getDefault_next_type_id());
        return domain ;
    }

}

    



