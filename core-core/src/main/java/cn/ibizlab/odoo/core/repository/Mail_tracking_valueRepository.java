package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Mail_tracking_value;
import cn.ibizlab.odoo.core.odoo_mail.filter.Mail_tracking_valueSearchContext;

/**
 * 实体 [邮件跟踪值] 存储对象
 */
public interface Mail_tracking_valueRepository extends Repository<Mail_tracking_value> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Mail_tracking_value> searchDefault(Mail_tracking_valueSearchContext context);

    Mail_tracking_value convert2PO(cn.ibizlab.odoo.core.odoo_mail.domain.Mail_tracking_value domain , Mail_tracking_value po) ;

    cn.ibizlab.odoo.core.odoo_mail.domain.Mail_tracking_value convert2Domain( Mail_tracking_value po ,cn.ibizlab.odoo.core.odoo_mail.domain.Mail_tracking_value domain) ;

}
