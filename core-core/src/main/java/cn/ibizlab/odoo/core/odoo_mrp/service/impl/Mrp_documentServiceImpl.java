package cn.ibizlab.odoo.core.odoo_mrp.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_mrp.domain.Mrp_document;
import cn.ibizlab.odoo.core.odoo_mrp.filter.Mrp_documentSearchContext;
import cn.ibizlab.odoo.core.odoo_mrp.service.IMrp_documentService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;



import cn.ibizlab.odoo.core.odoo_mrp.client.mrp_documentOdooClient;
import cn.ibizlab.odoo.core.odoo_mrp.clientmodel.mrp_documentClientModel;

/**
 * 实体[生产文档] 服务对象接口实现
 */
@Slf4j
@Service
public class Mrp_documentServiceImpl implements IMrp_documentService {

    @Autowired
    mrp_documentOdooClient mrp_documentOdooClient;


    @Override
    public boolean update(Mrp_document et) {
        mrp_documentClientModel clientModel = convert2Model(et,null);
		mrp_documentOdooClient.update(clientModel);
        Mrp_document rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }
    
    public void updateBatch(List<Mrp_document> list){
    }

    @Override
    public boolean create(Mrp_document et) {
        mrp_documentClientModel clientModel = convert2Model(et,null);
		mrp_documentOdooClient.create(clientModel);
        Mrp_document rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Mrp_document> list){
    }

    @Override
    public boolean remove(Integer id) {
        mrp_documentClientModel clientModel = new mrp_documentClientModel();
        clientModel.setId(id);
		mrp_documentOdooClient.remove(clientModel);
        return true; 
    }

    public void removeBatch(Collection<Integer> idList){
    }

    @Override
    public Mrp_document get(Integer id) {
        mrp_documentClientModel clientModel = new mrp_documentClientModel();
        clientModel.setId(id);
		mrp_documentOdooClient.get(clientModel);
        Mrp_document et = convert2Domain(clientModel,null);
        if(et==null){
            et=new Mrp_document();
            et.setId(id);
        }
        else{
        }

        return  et;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Mrp_document> searchDefault(Mrp_documentSearchContext context) {
        List<Mrp_document> list = new ArrayList<Mrp_document>();
        Page<mrp_documentClientModel> clientModelList = mrp_documentOdooClient.search(context);
        for(mrp_documentClientModel clientModel : clientModelList){
            list.add( this.convert2Domain(clientModel,null));
        }
        return new PageImpl<Mrp_document>(list, context.getPageable(), clientModelList.getTotalElements());
    }

    public mrp_documentClientModel convert2Model(Mrp_document domain , mrp_documentClientModel model) {
        if(domain == null)
            return model ;
        if(model==null){
            model = new mrp_documentClientModel();

        }
        if((Boolean) domain.getExtensionparams().get("dirtyflagenable")){
            if((Boolean) domain.getExtensionparams().get("create_datedirtyflag"))
                model.setCreate_date(domain.getCreateDate());
            if((Boolean) domain.getExtensionparams().get("activedirtyflag"))
                model.setActive(domain.getActive());
            if((Boolean) domain.getExtensionparams().get("res_fielddirtyflag"))
                model.setRes_field(domain.getResField());
            if((Boolean) domain.getExtensionparams().get("res_namedirtyflag"))
                model.setRes_name(domain.getResName());
            if((Boolean) domain.getExtensionparams().get("res_model_namedirtyflag"))
                model.setRes_model_name(domain.getResModelName());
            if((Boolean) domain.getExtensionparams().get("datas_fnamedirtyflag"))
                model.setDatas_fname(domain.getDatasFname());
            if((Boolean) domain.getExtensionparams().get("theme_template_iddirtyflag"))
                model.setTheme_template_id(domain.getThemeTemplateId());
            if((Boolean) domain.getExtensionparams().get("mimetypedirtyflag"))
                model.setMimetype(domain.getMimetype());
            if((Boolean) domain.getExtensionparams().get("res_iddirtyflag"))
                model.setRes_id(domain.getResId());
            if((Boolean) domain.getExtensionparams().get("store_fnamedirtyflag"))
                model.setStore_fname(domain.getStoreFname());
            if((Boolean) domain.getExtensionparams().get("write_datedirtyflag"))
                model.setWrite_date(domain.getWriteDate());
            if((Boolean) domain.getExtensionparams().get("local_urldirtyflag"))
                model.setLocal_url(domain.getLocalUrl());
            if((Boolean) domain.getExtensionparams().get("keydirtyflag"))
                model.setKey(domain.getKey());
            if((Boolean) domain.getExtensionparams().get("ibizpublicdirtyflag"))
                model.setIbizpublic(domain.getIbizpublic());
            if((Boolean) domain.getExtensionparams().get("res_modeldirtyflag"))
                model.setRes_model(domain.getResModel());
            if((Boolean) domain.getExtensionparams().get("__last_updatedirtyflag"))
                model.set__last_update(domain.getLastUpdate());
            if((Boolean) domain.getExtensionparams().get("thumbnaildirtyflag"))
                model.setThumbnail(domain.getThumbnail());
            if((Boolean) domain.getExtensionparams().get("urldirtyflag"))
                model.setUrl(domain.getUrl());
            if((Boolean) domain.getExtensionparams().get("file_sizedirtyflag"))
                model.setFile_size(domain.getFileSize());
            if((Boolean) domain.getExtensionparams().get("access_tokendirtyflag"))
                model.setAccess_token(domain.getAccessToken());
            if((Boolean) domain.getExtensionparams().get("company_iddirtyflag"))
                model.setCompany_id(domain.getCompanyId());
            if((Boolean) domain.getExtensionparams().get("ir_attachment_iddirtyflag"))
                model.setIr_attachment_id(domain.getIrAttachmentId());
            if((Boolean) domain.getExtensionparams().get("iddirtyflag"))
                model.setId(domain.getId());
            if((Boolean) domain.getExtensionparams().get("typedirtyflag"))
                model.setType(domain.getType());
            if((Boolean) domain.getExtensionparams().get("checksumdirtyflag"))
                model.setChecksum(domain.getChecksum());
            if((Boolean) domain.getExtensionparams().get("db_datasdirtyflag"))
                model.setDb_datas(domain.getDbDatas());
            if((Boolean) domain.getExtensionparams().get("index_contentdirtyflag"))
                model.setIndex_content(domain.getIndexContent());
            if((Boolean) domain.getExtensionparams().get("descriptiondirtyflag"))
                model.setDescription(domain.getDescription());
            if((Boolean) domain.getExtensionparams().get("namedirtyflag"))
                model.setName(domain.getName());
            if((Boolean) domain.getExtensionparams().get("display_namedirtyflag"))
                model.setDisplay_name(domain.getDisplayName());
            if((Boolean) domain.getExtensionparams().get("website_urldirtyflag"))
                model.setWebsite_url(domain.getWebsiteUrl());
            if((Boolean) domain.getExtensionparams().get("website_iddirtyflag"))
                model.setWebsite_id(domain.getWebsiteId());
            if((Boolean) domain.getExtensionparams().get("datasdirtyflag"))
                model.setDatas(domain.getDatas());
            if((Boolean) domain.getExtensionparams().get("prioritydirtyflag"))
                model.setPriority(domain.getPriority());
            if((Boolean) domain.getExtensionparams().get("write_uid_textdirtyflag"))
                model.setWrite_uid_text(domain.getWriteUidText());
            if((Boolean) domain.getExtensionparams().get("create_uid_textdirtyflag"))
                model.setCreate_uid_text(domain.getCreateUidText());
            if((Boolean) domain.getExtensionparams().get("create_uiddirtyflag"))
                model.setCreate_uid(domain.getCreateUid());
            if((Boolean) domain.getExtensionparams().get("write_uiddirtyflag"))
                model.setWrite_uid(domain.getWriteUid());
            domain.getFocusNull().clear();
        }
        return model ;
    }

    public Mrp_document convert2Domain( mrp_documentClientModel model ,Mrp_document domain) {
        if(model == null)
            return domain ;
        if(domain == null){
            domain = new Mrp_document();
        }

        if(model.getCreate_dateDirtyFlag())
            domain.setCreateDate(model.getCreate_date());
        if(model.getActiveDirtyFlag())
            domain.setActive(model.getActive());
        if(model.getRes_fieldDirtyFlag())
            domain.setResField(model.getRes_field());
        if(model.getRes_nameDirtyFlag())
            domain.setResName(model.getRes_name());
        if(model.getRes_model_nameDirtyFlag())
            domain.setResModelName(model.getRes_model_name());
        if(model.getDatas_fnameDirtyFlag())
            domain.setDatasFname(model.getDatas_fname());
        if(model.getTheme_template_idDirtyFlag())
            domain.setThemeTemplateId(model.getTheme_template_id());
        if(model.getMimetypeDirtyFlag())
            domain.setMimetype(model.getMimetype());
        if(model.getRes_idDirtyFlag())
            domain.setResId(model.getRes_id());
        if(model.getStore_fnameDirtyFlag())
            domain.setStoreFname(model.getStore_fname());
        if(model.getWrite_dateDirtyFlag())
            domain.setWriteDate(model.getWrite_date());
        if(model.getLocal_urlDirtyFlag())
            domain.setLocalUrl(model.getLocal_url());
        if(model.getKeyDirtyFlag())
            domain.setKey(model.getKey());
        if(model.getIbizpublicDirtyFlag())
            domain.setIbizpublic(model.getIbizpublic());
        if(model.getRes_modelDirtyFlag())
            domain.setResModel(model.getRes_model());
        if(model.get__last_updateDirtyFlag())
            domain.setLastUpdate(model.get__last_update());
        if(model.getThumbnailDirtyFlag())
            domain.setThumbnail(model.getThumbnail());
        if(model.getUrlDirtyFlag())
            domain.setUrl(model.getUrl());
        if(model.getFile_sizeDirtyFlag())
            domain.setFileSize(model.getFile_size());
        if(model.getAccess_tokenDirtyFlag())
            domain.setAccessToken(model.getAccess_token());
        if(model.getCompany_idDirtyFlag())
            domain.setCompanyId(model.getCompany_id());
        if(model.getIr_attachment_idDirtyFlag())
            domain.setIrAttachmentId(model.getIr_attachment_id());
        if(model.getIdDirtyFlag())
            domain.setId(model.getId());
        if(model.getTypeDirtyFlag())
            domain.setType(model.getType());
        if(model.getChecksumDirtyFlag())
            domain.setChecksum(model.getChecksum());
        if(model.getDb_datasDirtyFlag())
            domain.setDbDatas(model.getDb_datas());
        if(model.getIndex_contentDirtyFlag())
            domain.setIndexContent(model.getIndex_content());
        if(model.getDescriptionDirtyFlag())
            domain.setDescription(model.getDescription());
        if(model.getNameDirtyFlag())
            domain.setName(model.getName());
        if(model.getDisplay_nameDirtyFlag())
            domain.setDisplayName(model.getDisplay_name());
        if(model.getWebsite_urlDirtyFlag())
            domain.setWebsiteUrl(model.getWebsite_url());
        if(model.getWebsite_idDirtyFlag())
            domain.setWebsiteId(model.getWebsite_id());
        if(model.getDatasDirtyFlag())
            domain.setDatas(model.getDatas());
        if(model.getPriorityDirtyFlag())
            domain.setPriority(model.getPriority());
        if(model.getWrite_uid_textDirtyFlag())
            domain.setWriteUidText(model.getWrite_uid_text());
        if(model.getCreate_uid_textDirtyFlag())
            domain.setCreateUidText(model.getCreate_uid_text());
        if(model.getCreate_uidDirtyFlag())
            domain.setCreateUid(model.getCreate_uid());
        if(model.getWrite_uidDirtyFlag())
            domain.setWriteUid(model.getWrite_uid());
        return domain ;
    }

}

    



