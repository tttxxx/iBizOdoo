package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Res_country;
import cn.ibizlab.odoo.core.odoo_base.filter.Res_countrySearchContext;

/**
 * 实体 [国家/地区] 存储对象
 */
public interface Res_countryRepository extends Repository<Res_country> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Res_country> searchDefault(Res_countrySearchContext context);

    Res_country convert2PO(cn.ibizlab.odoo.core.odoo_base.domain.Res_country domain , Res_country po) ;

    cn.ibizlab.odoo.core.odoo_base.domain.Res_country convert2Domain( Res_country po ,cn.ibizlab.odoo.core.odoo_base.domain.Res_country domain) ;

}
