package cn.ibizlab.odoo.core.odoo_crm.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.odoo.util.domain.EntityBase;
import cn.ibizlab.odoo.util.annotation.DEField;
import cn.ibizlab.odoo.util.enums.DEPredefinedFieldType;
import cn.ibizlab.odoo.util.enums.DEFieldDefaultValueType;
import java.io.Serializable;
import lombok.Data;
import org.springframework.data.annotation.Transient;

import cn.ibizlab.odoo.util.domain.EntityClient;

/**
 * ServiceApi [转化线索为商机（批量）] 对象
 */
@Data
public class Crm_lead2opportunity_partner_mass extends EntityClient implements Serializable {

    /**
     * 显示名称
     */
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    private String displayName;

    /**
     * 商机
     */
    @JSONField(name = "opportunity_ids")
    @JsonProperty("opportunity_ids")
    private String opportunityIds;

    /**
     * 转换动作
     */
    @JSONField(name = "name")
    @JsonProperty("name")
    private String name;

    /**
     * 创建时间
     */
    @DEField(name = "create_date" , preType = DEPredefinedFieldType.CREATEDATE)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;

    /**
     * 最后修改日
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;

    /**
     * 相关客户
     */
    @JSONField(name = "action")
    @JsonProperty("action")
    private String action;

    /**
     * 最后更新时间
     */
    @DEField(name = "write_date" , preType = DEPredefinedFieldType.UPDATEDATE)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;

    /**
     * 强制分配
     */
    @DEField(name = "force_assignation")
    @JSONField(name = "force_assignation")
    @JsonProperty("force_assignation")
    private String forceAssignation;

    /**
     * 删除重复内容
     */
    @JSONField(name = "deduplicate")
    @JsonProperty("deduplicate")
    private String deduplicate;

    /**
     * ID
     */
    @DEField(isKeyField=true)
    @JSONField(name = "id")
    @JsonProperty("id")
    private Integer id;

    /**
     * 销售员
     */
    @JSONField(name = "user_ids")
    @JsonProperty("user_ids")
    private String userIds;

    /**
     * 销售团队
     */
    @JSONField(name = "team_id_text")
    @JsonProperty("team_id_text")
    private String teamIdText;

    /**
     * 创建人
     */
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    private String createUidText;

    /**
     * 客户
     */
    @JSONField(name = "partner_id_text")
    @JsonProperty("partner_id_text")
    private String partnerIdText;

    /**
     * 销售员
     */
    @JSONField(name = "user_id_text")
    @JsonProperty("user_id_text")
    private String userIdText;

    /**
     * 最后更新
     */
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    private String writeUidText;

    /**
     * 销售员
     */
    @DEField(name = "user_id")
    @JSONField(name = "user_id")
    @JsonProperty("user_id")
    private Integer userId;

    /**
     * 客户
     */
    @DEField(name = "partner_id")
    @JSONField(name = "partner_id")
    @JsonProperty("partner_id")
    private Integer partnerId;

    /**
     * 创建人
     */
    @DEField(name = "create_uid" , preType = DEPredefinedFieldType.CREATEMAN)
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    private Integer createUid;

    /**
     * 最后更新
     */
    @DEField(name = "write_uid" , preType = DEPredefinedFieldType.UPDATEMAN)
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    private Integer writeUid;

    /**
     * 销售团队
     */
    @DEField(name = "team_id")
    @JSONField(name = "team_id")
    @JsonProperty("team_id")
    private Integer teamId;


    /**
     * 
     */
    @JSONField(name = "odooteam")
    @JsonProperty("odooteam")
    private cn.ibizlab.odoo.core.odoo_crm.domain.Crm_team odooTeam;

    /**
     * 
     */
    @JSONField(name = "odoopartner")
    @JsonProperty("odoopartner")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_partner odooPartner;

    /**
     * 
     */
    @JSONField(name = "odoocreate")
    @JsonProperty("odoocreate")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_users odooCreate;

    /**
     * 
     */
    @JSONField(name = "odoouser")
    @JsonProperty("odoouser")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_users odooUser;

    /**
     * 
     */
    @JSONField(name = "odoowrite")
    @JsonProperty("odoowrite")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_users odooWrite;




    /**
     * 设置 [转换动作]
     */
    public void setName(String name){
        this.name = name ;
        this.modify("name",name);
    }
    /**
     * 设置 [相关客户]
     */
    public void setAction(String action){
        this.action = action ;
        this.modify("action",action);
    }
    /**
     * 设置 [强制分配]
     */
    public void setForceAssignation(String forceAssignation){
        this.forceAssignation = forceAssignation ;
        this.modify("force_assignation",forceAssignation);
    }
    /**
     * 设置 [删除重复内容]
     */
    public void setDeduplicate(String deduplicate){
        this.deduplicate = deduplicate ;
        this.modify("deduplicate",deduplicate);
    }
    /**
     * 设置 [销售员]
     */
    public void setUserId(Integer userId){
        this.userId = userId ;
        this.modify("user_id",userId);
    }
    /**
     * 设置 [客户]
     */
    public void setPartnerId(Integer partnerId){
        this.partnerId = partnerId ;
        this.modify("partner_id",partnerId);
    }
    /**
     * 设置 [销售团队]
     */
    public void setTeamId(Integer teamId){
        this.teamId = teamId ;
        this.modify("team_id",teamId);
    }

}


