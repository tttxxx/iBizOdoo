package cn.ibizlab.odoo.core.odoo_note.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_note.domain.Note_tag;
import cn.ibizlab.odoo.core.odoo_note.filter.Note_tagSearchContext;
import cn.ibizlab.odoo.core.odoo_note.service.INote_tagService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;



import cn.ibizlab.odoo.core.odoo_note.client.note_tagOdooClient;
import cn.ibizlab.odoo.core.odoo_note.clientmodel.note_tagClientModel;

/**
 * 实体[便签标签] 服务对象接口实现
 */
@Slf4j
@Service
public class Note_tagServiceImpl implements INote_tagService {

    @Autowired
    note_tagOdooClient note_tagOdooClient;


    @Override
    public boolean remove(Integer id) {
        note_tagClientModel clientModel = new note_tagClientModel();
        clientModel.setId(id);
		note_tagOdooClient.remove(clientModel);
        return true; 
    }

    public void removeBatch(Collection<Integer> idList){
    }

    @Override
    public boolean update(Note_tag et) {
        note_tagClientModel clientModel = convert2Model(et,null);
		note_tagOdooClient.update(clientModel);
        Note_tag rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }
    
    public void updateBatch(List<Note_tag> list){
    }

    @Override
    public boolean create(Note_tag et) {
        note_tagClientModel clientModel = convert2Model(et,null);
		note_tagOdooClient.create(clientModel);
        Note_tag rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Note_tag> list){
    }

    @Override
    public Note_tag get(Integer id) {
        note_tagClientModel clientModel = new note_tagClientModel();
        clientModel.setId(id);
		note_tagOdooClient.get(clientModel);
        Note_tag et = convert2Domain(clientModel,null);
        if(et==null){
            et=new Note_tag();
            et.setId(id);
        }
        else{
        }

        return  et;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Note_tag> searchDefault(Note_tagSearchContext context) {
        List<Note_tag> list = new ArrayList<Note_tag>();
        Page<note_tagClientModel> clientModelList = note_tagOdooClient.search(context);
        for(note_tagClientModel clientModel : clientModelList){
            list.add( this.convert2Domain(clientModel,null));
        }
        return new PageImpl<Note_tag>(list, context.getPageable(), clientModelList.getTotalElements());
    }

    public note_tagClientModel convert2Model(Note_tag domain , note_tagClientModel model) {
        if(domain == null)
            return model ;
        if(model==null){
            model = new note_tagClientModel();

        }
        if((Boolean) domain.getExtensionparams().get("dirtyflagenable")){
            if((Boolean) domain.getExtensionparams().get("write_datedirtyflag"))
                model.setWrite_date(domain.getWriteDate());
            if((Boolean) domain.getExtensionparams().get("create_datedirtyflag"))
                model.setCreate_date(domain.getCreateDate());
            if((Boolean) domain.getExtensionparams().get("__last_updatedirtyflag"))
                model.set__last_update(domain.getLastUpdate());
            if((Boolean) domain.getExtensionparams().get("colordirtyflag"))
                model.setColor(domain.getColor());
            if((Boolean) domain.getExtensionparams().get("display_namedirtyflag"))
                model.setDisplay_name(domain.getDisplayName());
            if((Boolean) domain.getExtensionparams().get("iddirtyflag"))
                model.setId(domain.getId());
            if((Boolean) domain.getExtensionparams().get("namedirtyflag"))
                model.setName(domain.getName());
            if((Boolean) domain.getExtensionparams().get("write_uid_textdirtyflag"))
                model.setWrite_uid_text(domain.getWriteUidText());
            if((Boolean) domain.getExtensionparams().get("create_uid_textdirtyflag"))
                model.setCreate_uid_text(domain.getCreateUidText());
            if((Boolean) domain.getExtensionparams().get("create_uiddirtyflag"))
                model.setCreate_uid(domain.getCreateUid());
            if((Boolean) domain.getExtensionparams().get("write_uiddirtyflag"))
                model.setWrite_uid(domain.getWriteUid());
            domain.getFocusNull().clear();
        }
        return model ;
    }

    public Note_tag convert2Domain( note_tagClientModel model ,Note_tag domain) {
        if(model == null)
            return domain ;
        if(domain == null){
            domain = new Note_tag();
        }

        if(model.getWrite_dateDirtyFlag())
            domain.setWriteDate(model.getWrite_date());
        if(model.getCreate_dateDirtyFlag())
            domain.setCreateDate(model.getCreate_date());
        if(model.get__last_updateDirtyFlag())
            domain.setLastUpdate(model.get__last_update());
        if(model.getColorDirtyFlag())
            domain.setColor(model.getColor());
        if(model.getDisplay_nameDirtyFlag())
            domain.setDisplayName(model.getDisplay_name());
        if(model.getIdDirtyFlag())
            domain.setId(model.getId());
        if(model.getNameDirtyFlag())
            domain.setName(model.getName());
        if(model.getWrite_uid_textDirtyFlag())
            domain.setWriteUidText(model.getWrite_uid_text());
        if(model.getCreate_uid_textDirtyFlag())
            domain.setCreateUidText(model.getCreate_uid_text());
        if(model.getCreate_uidDirtyFlag())
            domain.setCreateUid(model.getCreate_uid());
        if(model.getWrite_uidDirtyFlag())
            domain.setWriteUid(model.getWrite_uid());
        return domain ;
    }

}

    



