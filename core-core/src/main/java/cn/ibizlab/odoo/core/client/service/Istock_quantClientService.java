package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Istock_quant;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[stock_quant] 服务对象接口
 */
public interface Istock_quantClientService{

    public Istock_quant createModel() ;

    public void update(Istock_quant stock_quant);

    public Page<Istock_quant> search(SearchContext context);

    public void removeBatch(List<Istock_quant> stock_quants);

    public void get(Istock_quant stock_quant);

    public void create(Istock_quant stock_quant);

    public void createBatch(List<Istock_quant> stock_quants);

    public void remove(Istock_quant stock_quant);

    public void updateBatch(List<Istock_quant> stock_quants);

    public Page<Istock_quant> select(SearchContext context);

}
