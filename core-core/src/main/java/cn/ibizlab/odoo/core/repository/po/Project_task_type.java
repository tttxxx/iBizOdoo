package cn.ibizlab.odoo.core.repository.po ;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.math.BigInteger;

import cn.ibizlab.odoo.core.odoo_project.filter.Project_task_typeSearchContext;

/**
 * 实体 [任务阶段] 存储模型
 */
public interface Project_task_type{

    /**
     * 阶段名称
     */
    String getName();

    void setName(String name);

    /**
     * 获取 [阶段名称]脏标记
     */
    boolean getNameDirtyFlag();

    /**
     * 自动看板状态
     */
    String getAuto_validation_kanban_state();

    void setAuto_validation_kanban_state(String auto_validation_kanban_state);

    /**
     * 获取 [自动看板状态]脏标记
     */
    boolean getAuto_validation_kanban_stateDirtyFlag();

    /**
     * 最后更新时间
     */
    Timestamp getWrite_date();

    void setWrite_date(Timestamp write_date);

    /**
     * 获取 [最后更新时间]脏标记
     */
    boolean getWrite_dateDirtyFlag();

    /**
     * 显示名称
     */
    String getDisplay_name();

    void setDisplay_name(String display_name);

    /**
     * 获取 [显示名称]脏标记
     */
    boolean getDisplay_nameDirtyFlag();

    /**
     * ID
     */
    Integer getId();

    void setId(Integer id);

    /**
     * 获取 [ID]脏标记
     */
    boolean getIdDirtyFlag();

    /**
     * 项目
     */
    String getProject_ids();

    void setProject_ids(String project_ids);

    /**
     * 获取 [项目]脏标记
     */
    boolean getProject_idsDirtyFlag();

    /**
     * 序号
     */
    Integer getSequence();

    void setSequence(Integer sequence);

    /**
     * 获取 [序号]脏标记
     */
    boolean getSequenceDirtyFlag();

    /**
     * 说明
     */
    String getDescription();

    void setDescription(String description);

    /**
     * 获取 [说明]脏标记
     */
    boolean getDescriptionDirtyFlag();

    /**
     * 灰色看板标签
     */
    String getLegend_normal();

    void setLegend_normal(String legend_normal);

    /**
     * 获取 [灰色看板标签]脏标记
     */
    boolean getLegend_normalDirtyFlag();

    /**
     * 集中到看板中
     */
    String getFold();

    void setFold(String fold);

    /**
     * 获取 [集中到看板中]脏标记
     */
    boolean getFoldDirtyFlag();

    /**
     * 创建时间
     */
    Timestamp getCreate_date();

    void setCreate_date(Timestamp create_date);

    /**
     * 获取 [创建时间]脏标记
     */
    boolean getCreate_dateDirtyFlag();

    /**
     * 红色的看板标签
     */
    String getLegend_blocked();

    void setLegend_blocked(String legend_blocked);

    /**
     * 获取 [红色的看板标签]脏标记
     */
    boolean getLegend_blockedDirtyFlag();

    /**
     * 星标解释
     */
    String getLegend_priority();

    void setLegend_priority(String legend_priority);

    /**
     * 获取 [星标解释]脏标记
     */
    boolean getLegend_priorityDirtyFlag();

    /**
     * 最后修改日
     */
    Timestamp get__last_update();

    void set__last_update(Timestamp __last_update);

    /**
     * 获取 [最后修改日]脏标记
     */
    boolean get__last_updateDirtyFlag();

    /**
     * 绿色看板标签
     */
    String getLegend_done();

    void setLegend_done(String legend_done);

    /**
     * 获取 [绿色看板标签]脏标记
     */
    boolean getLegend_doneDirtyFlag();

    /**
     * 创建人
     */
    String getCreate_uid_text();

    void setCreate_uid_text(String create_uid_text);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uid_textDirtyFlag();

    /**
     * 点评邮件模板
     */
    String getRating_template_id_text();

    void setRating_template_id_text(String rating_template_id_text);

    /**
     * 获取 [点评邮件模板]脏标记
     */
    boolean getRating_template_id_textDirtyFlag();

    /**
     * 最后更新人
     */
    String getWrite_uid_text();

    void setWrite_uid_text(String write_uid_text);

    /**
     * 获取 [最后更新人]脏标记
     */
    boolean getWrite_uid_textDirtyFlag();

    /**
     * EMail模板
     */
    String getMail_template_id_text();

    void setMail_template_id_text(String mail_template_id_text);

    /**
     * 获取 [EMail模板]脏标记
     */
    boolean getMail_template_id_textDirtyFlag();

    /**
     * EMail模板
     */
    Integer getMail_template_id();

    void setMail_template_id(Integer mail_template_id);

    /**
     * 获取 [EMail模板]脏标记
     */
    boolean getMail_template_idDirtyFlag();

    /**
     * 创建人
     */
    Integer getCreate_uid();

    void setCreate_uid(Integer create_uid);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uidDirtyFlag();

    /**
     * 点评邮件模板
     */
    Integer getRating_template_id();

    void setRating_template_id(Integer rating_template_id);

    /**
     * 获取 [点评邮件模板]脏标记
     */
    boolean getRating_template_idDirtyFlag();

    /**
     * 最后更新人
     */
    Integer getWrite_uid();

    void setWrite_uid(Integer write_uid);

    /**
     * 获取 [最后更新人]脏标记
     */
    boolean getWrite_uidDirtyFlag();

}
