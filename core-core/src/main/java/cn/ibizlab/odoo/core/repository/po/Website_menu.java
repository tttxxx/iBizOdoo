package cn.ibizlab.odoo.core.repository.po ;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.math.BigInteger;

import cn.ibizlab.odoo.core.odoo_website.filter.Website_menuSearchContext;

/**
 * 实体 [网站菜单] 存储模型
 */
public interface Website_menu{

    /**
     * 主题模板
     */
    Integer getTheme_template_id();

    void setTheme_template_id(Integer theme_template_id);

    /**
     * 获取 [主题模板]脏标记
     */
    boolean getTheme_template_idDirtyFlag();

    /**
     * 可见
     */
    String getIs_visible();

    void setIs_visible(String is_visible);

    /**
     * 获取 [可见]脏标记
     */
    boolean getIs_visibleDirtyFlag();

    /**
     * Url
     */
    String getUrl();

    void setUrl(String url);

    /**
     * 获取 [Url]脏标记
     */
    boolean getUrlDirtyFlag();

    /**
     * 新窗口
     */
    String getNew_window();

    void setNew_window(String new_window);

    /**
     * 获取 [新窗口]脏标记
     */
    boolean getNew_windowDirtyFlag();

    /**
     * 最后修改日
     */
    Timestamp get__last_update();

    void set__last_update(Timestamp __last_update);

    /**
     * 获取 [最后修改日]脏标记
     */
    boolean get__last_updateDirtyFlag();

    /**
     * 上级路径
     */
    String getParent_path();

    void setParent_path(String parent_path);

    /**
     * 获取 [上级路径]脏标记
     */
    boolean getParent_pathDirtyFlag();

    /**
     * 创建时间
     */
    Timestamp getCreate_date();

    void setCreate_date(Timestamp create_date);

    /**
     * 获取 [创建时间]脏标记
     */
    boolean getCreate_dateDirtyFlag();

    /**
     * 网站
     */
    Integer getWebsite_id();

    void setWebsite_id(Integer website_id);

    /**
     * 获取 [网站]脏标记
     */
    boolean getWebsite_idDirtyFlag();

    /**
     * 序号
     */
    Integer getSequence();

    void setSequence(Integer sequence);

    /**
     * 获取 [序号]脏标记
     */
    boolean getSequenceDirtyFlag();

    /**
     * ID
     */
    Integer getId();

    void setId(Integer id);

    /**
     * 获取 [ID]脏标记
     */
    boolean getIdDirtyFlag();

    /**
     * 下级菜单
     */
    String getChild_id();

    void setChild_id(String child_id);

    /**
     * 获取 [下级菜单]脏标记
     */
    boolean getChild_idDirtyFlag();

    /**
     * 最后更新时间
     */
    Timestamp getWrite_date();

    void setWrite_date(Timestamp write_date);

    /**
     * 获取 [最后更新时间]脏标记
     */
    boolean getWrite_dateDirtyFlag();

    /**
     * 菜单
     */
    String getName();

    void setName(String name);

    /**
     * 获取 [菜单]脏标记
     */
    boolean getNameDirtyFlag();

    /**
     * 显示名称
     */
    String getDisplay_name();

    void setDisplay_name(String display_name);

    /**
     * 获取 [显示名称]脏标记
     */
    boolean getDisplay_nameDirtyFlag();

    /**
     * 创建人
     */
    String getCreate_uid_text();

    void setCreate_uid_text(String create_uid_text);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uid_textDirtyFlag();

    /**
     * 相关页面
     */
    String getPage_id_text();

    void setPage_id_text(String page_id_text);

    /**
     * 获取 [相关页面]脏标记
     */
    boolean getPage_id_textDirtyFlag();

    /**
     * 上级菜单
     */
    String getParent_id_text();

    void setParent_id_text(String parent_id_text);

    /**
     * 获取 [上级菜单]脏标记
     */
    boolean getParent_id_textDirtyFlag();

    /**
     * 最后更新者
     */
    String getWrite_uid_text();

    void setWrite_uid_text(String write_uid_text);

    /**
     * 获取 [最后更新者]脏标记
     */
    boolean getWrite_uid_textDirtyFlag();

    /**
     * 最后更新者
     */
    Integer getWrite_uid();

    void setWrite_uid(Integer write_uid);

    /**
     * 获取 [最后更新者]脏标记
     */
    boolean getWrite_uidDirtyFlag();

    /**
     * 相关页面
     */
    Integer getPage_id();

    void setPage_id(Integer page_id);

    /**
     * 获取 [相关页面]脏标记
     */
    boolean getPage_idDirtyFlag();

    /**
     * 上级菜单
     */
    Integer getParent_id();

    void setParent_id(Integer parent_id);

    /**
     * 获取 [上级菜单]脏标记
     */
    boolean getParent_idDirtyFlag();

    /**
     * 创建人
     */
    Integer getCreate_uid();

    void setCreate_uid(Integer create_uid);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uidDirtyFlag();

}
