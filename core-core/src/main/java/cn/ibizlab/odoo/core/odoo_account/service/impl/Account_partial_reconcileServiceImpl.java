package cn.ibizlab.odoo.core.odoo_account.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_account.domain.Account_partial_reconcile;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_partial_reconcileSearchContext;
import cn.ibizlab.odoo.core.odoo_account.service.IAccount_partial_reconcileService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;



import cn.ibizlab.odoo.core.odoo_account.client.account_partial_reconcileOdooClient;
import cn.ibizlab.odoo.core.odoo_account.clientmodel.account_partial_reconcileClientModel;

/**
 * 实体[部分核销] 服务对象接口实现
 */
@Slf4j
@Service
public class Account_partial_reconcileServiceImpl implements IAccount_partial_reconcileService {

    @Autowired
    account_partial_reconcileOdooClient account_partial_reconcileOdooClient;


    @Override
    public boolean create(Account_partial_reconcile et) {
        account_partial_reconcileClientModel clientModel = convert2Model(et,null);
		account_partial_reconcileOdooClient.create(clientModel);
        Account_partial_reconcile rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Account_partial_reconcile> list){
    }

    @Override
    public boolean remove(Integer id) {
        account_partial_reconcileClientModel clientModel = new account_partial_reconcileClientModel();
        clientModel.setId(id);
		account_partial_reconcileOdooClient.remove(clientModel);
        return true; 
    }

    public void removeBatch(Collection<Integer> idList){
    }

    @Override
    public boolean update(Account_partial_reconcile et) {
        account_partial_reconcileClientModel clientModel = convert2Model(et,null);
		account_partial_reconcileOdooClient.update(clientModel);
        Account_partial_reconcile rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }
    
    public void updateBatch(List<Account_partial_reconcile> list){
    }

    @Override
    public Account_partial_reconcile get(Integer id) {
        account_partial_reconcileClientModel clientModel = new account_partial_reconcileClientModel();
        clientModel.setId(id);
		account_partial_reconcileOdooClient.get(clientModel);
        Account_partial_reconcile et = convert2Domain(clientModel,null);
        if(et==null){
            et=new Account_partial_reconcile();
            et.setId(id);
        }
        else{
        }

        return  et;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Account_partial_reconcile> searchDefault(Account_partial_reconcileSearchContext context) {
        List<Account_partial_reconcile> list = new ArrayList<Account_partial_reconcile>();
        Page<account_partial_reconcileClientModel> clientModelList = account_partial_reconcileOdooClient.search(context);
        for(account_partial_reconcileClientModel clientModel : clientModelList){
            list.add( this.convert2Domain(clientModel,null));
        }
        return new PageImpl<Account_partial_reconcile>(list, context.getPageable(), clientModelList.getTotalElements());
    }

    public account_partial_reconcileClientModel convert2Model(Account_partial_reconcile domain , account_partial_reconcileClientModel model) {
        if(domain == null)
            return model ;
        if(model==null){
            model = new account_partial_reconcileClientModel();

        }
        if((Boolean) domain.getExtensionparams().get("dirtyflagenable")){
            if((Boolean) domain.getExtensionparams().get("amount_currencydirtyflag"))
                model.setAmount_currency(domain.getAmountCurrency());
            if((Boolean) domain.getExtensionparams().get("amountdirtyflag"))
                model.setAmount(domain.getAmount());
            if((Boolean) domain.getExtensionparams().get("write_datedirtyflag"))
                model.setWrite_date(domain.getWriteDate());
            if((Boolean) domain.getExtensionparams().get("iddirtyflag"))
                model.setId(domain.getId());
            if((Boolean) domain.getExtensionparams().get("__last_updatedirtyflag"))
                model.set__last_update(domain.getLastUpdate());
            if((Boolean) domain.getExtensionparams().get("display_namedirtyflag"))
                model.setDisplay_name(domain.getDisplayName());
            if((Boolean) domain.getExtensionparams().get("create_datedirtyflag"))
                model.setCreate_date(domain.getCreateDate());
            if((Boolean) domain.getExtensionparams().get("max_datedirtyflag"))
                model.setMax_date(domain.getMaxDate());
            if((Boolean) domain.getExtensionparams().get("company_id_textdirtyflag"))
                model.setCompany_id_text(domain.getCompanyIdText());
            if((Boolean) domain.getExtensionparams().get("credit_move_id_textdirtyflag"))
                model.setCredit_move_id_text(domain.getCreditMoveIdText());
            if((Boolean) domain.getExtensionparams().get("company_currency_iddirtyflag"))
                model.setCompany_currency_id(domain.getCompanyCurrencyId());
            if((Boolean) domain.getExtensionparams().get("create_uid_textdirtyflag"))
                model.setCreate_uid_text(domain.getCreateUidText());
            if((Boolean) domain.getExtensionparams().get("debit_move_id_textdirtyflag"))
                model.setDebit_move_id_text(domain.getDebitMoveIdText());
            if((Boolean) domain.getExtensionparams().get("currency_id_textdirtyflag"))
                model.setCurrency_id_text(domain.getCurrencyIdText());
            if((Boolean) domain.getExtensionparams().get("write_uid_textdirtyflag"))
                model.setWrite_uid_text(domain.getWriteUidText());
            if((Boolean) domain.getExtensionparams().get("full_reconcile_id_textdirtyflag"))
                model.setFull_reconcile_id_text(domain.getFullReconcileIdText());
            if((Boolean) domain.getExtensionparams().get("company_iddirtyflag"))
                model.setCompany_id(domain.getCompanyId());
            if((Boolean) domain.getExtensionparams().get("currency_iddirtyflag"))
                model.setCurrency_id(domain.getCurrencyId());
            if((Boolean) domain.getExtensionparams().get("full_reconcile_iddirtyflag"))
                model.setFull_reconcile_id(domain.getFullReconcileId());
            if((Boolean) domain.getExtensionparams().get("create_uiddirtyflag"))
                model.setCreate_uid(domain.getCreateUid());
            if((Boolean) domain.getExtensionparams().get("debit_move_iddirtyflag"))
                model.setDebit_move_id(domain.getDebitMoveId());
            if((Boolean) domain.getExtensionparams().get("credit_move_iddirtyflag"))
                model.setCredit_move_id(domain.getCreditMoveId());
            if((Boolean) domain.getExtensionparams().get("write_uiddirtyflag"))
                model.setWrite_uid(domain.getWriteUid());
            domain.getFocusNull().clear();
        }
        return model ;
    }

    public Account_partial_reconcile convert2Domain( account_partial_reconcileClientModel model ,Account_partial_reconcile domain) {
        if(model == null)
            return domain ;
        if(domain == null){
            domain = new Account_partial_reconcile();
        }

        if(model.getAmount_currencyDirtyFlag())
            domain.setAmountCurrency(model.getAmount_currency());
        if(model.getAmountDirtyFlag())
            domain.setAmount(model.getAmount());
        if(model.getWrite_dateDirtyFlag())
            domain.setWriteDate(model.getWrite_date());
        if(model.getIdDirtyFlag())
            domain.setId(model.getId());
        if(model.get__last_updateDirtyFlag())
            domain.setLastUpdate(model.get__last_update());
        if(model.getDisplay_nameDirtyFlag())
            domain.setDisplayName(model.getDisplay_name());
        if(model.getCreate_dateDirtyFlag())
            domain.setCreateDate(model.getCreate_date());
        if(model.getMax_dateDirtyFlag())
            domain.setMaxDate(model.getMax_date());
        if(model.getCompany_id_textDirtyFlag())
            domain.setCompanyIdText(model.getCompany_id_text());
        if(model.getCredit_move_id_textDirtyFlag())
            domain.setCreditMoveIdText(model.getCredit_move_id_text());
        if(model.getCompany_currency_idDirtyFlag())
            domain.setCompanyCurrencyId(model.getCompany_currency_id());
        if(model.getCreate_uid_textDirtyFlag())
            domain.setCreateUidText(model.getCreate_uid_text());
        if(model.getDebit_move_id_textDirtyFlag())
            domain.setDebitMoveIdText(model.getDebit_move_id_text());
        if(model.getCurrency_id_textDirtyFlag())
            domain.setCurrencyIdText(model.getCurrency_id_text());
        if(model.getWrite_uid_textDirtyFlag())
            domain.setWriteUidText(model.getWrite_uid_text());
        if(model.getFull_reconcile_id_textDirtyFlag())
            domain.setFullReconcileIdText(model.getFull_reconcile_id_text());
        if(model.getCompany_idDirtyFlag())
            domain.setCompanyId(model.getCompany_id());
        if(model.getCurrency_idDirtyFlag())
            domain.setCurrencyId(model.getCurrency_id());
        if(model.getFull_reconcile_idDirtyFlag())
            domain.setFullReconcileId(model.getFull_reconcile_id());
        if(model.getCreate_uidDirtyFlag())
            domain.setCreateUid(model.getCreate_uid());
        if(model.getDebit_move_idDirtyFlag())
            domain.setDebitMoveId(model.getDebit_move_id());
        if(model.getCredit_move_idDirtyFlag())
            domain.setCreditMoveId(model.getCredit_move_id());
        if(model.getWrite_uidDirtyFlag())
            domain.setWriteUid(model.getWrite_uid());
        return domain ;
    }

}

    



