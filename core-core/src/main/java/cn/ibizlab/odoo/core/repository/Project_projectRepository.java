package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Project_project;
import cn.ibizlab.odoo.core.odoo_project.filter.Project_projectSearchContext;

/**
 * 实体 [项目] 存储对象
 */
public interface Project_projectRepository extends Repository<Project_project> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Project_project> searchDefault(Project_projectSearchContext context);

    Project_project convert2PO(cn.ibizlab.odoo.core.odoo_project.domain.Project_project domain , Project_project po) ;

    cn.ibizlab.odoo.core.odoo_project.domain.Project_project convert2Domain( Project_project po ,cn.ibizlab.odoo.core.odoo_project.domain.Project_project domain) ;

}
