package cn.ibizlab.odoo.core.odoo_product.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_product.domain.Product_category;
import cn.ibizlab.odoo.core.odoo_product.filter.Product_categorySearchContext;


/**
 * 实体[Product_category] 服务对象接口
 */
public interface IProduct_categoryService{

    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    boolean create(Product_category et) ;
    void createBatch(List<Product_category> list) ;
    boolean update(Product_category et) ;
    void updateBatch(List<Product_category> list) ;
    Product_category get(Integer key) ;
    Page<Product_category> searchDefault(Product_categorySearchContext context) ;

}



