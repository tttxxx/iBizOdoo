package cn.ibizlab.odoo.core.repository.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import cn.ibizlab.odoo.core.repository.client.po.fleet_vehicle_cost;

/**
 * 实体[fleet_vehicle_cost] 服务对象接口
 */
public interface fleet_vehicle_costRepository{


    public fleet_vehicle_cost createPO() ;
        public void createBatch(fleet_vehicle_cost fleet_vehicle_cost);

        public List<fleet_vehicle_cost> search();

        public void create(fleet_vehicle_cost fleet_vehicle_cost);

        public void remove(String id);

        public void update(fleet_vehicle_cost fleet_vehicle_cost);

        public void get(String id);

        public void removeBatch(String id);

        public void updateBatch(fleet_vehicle_cost fleet_vehicle_cost);


}
