package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Account_analytic_tag;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_analytic_tagSearchContext;

/**
 * 实体 [分析标签] 存储对象
 */
public interface Account_analytic_tagRepository extends Repository<Account_analytic_tag> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Account_analytic_tag> searchDefault(Account_analytic_tagSearchContext context);

    Account_analytic_tag convert2PO(cn.ibizlab.odoo.core.odoo_account.domain.Account_analytic_tag domain , Account_analytic_tag po) ;

    cn.ibizlab.odoo.core.odoo_account.domain.Account_analytic_tag convert2Domain( Account_analytic_tag po ,cn.ibizlab.odoo.core.odoo_account.domain.Account_analytic_tag domain) ;

}
