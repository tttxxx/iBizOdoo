package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Istock_quant_package;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[stock_quant_package] 服务对象接口
 */
public interface Istock_quant_packageClientService{

    public Istock_quant_package createModel() ;

    public void removeBatch(List<Istock_quant_package> stock_quant_packages);

    public void updateBatch(List<Istock_quant_package> stock_quant_packages);

    public void update(Istock_quant_package stock_quant_package);

    public void get(Istock_quant_package stock_quant_package);

    public void create(Istock_quant_package stock_quant_package);

    public Page<Istock_quant_package> search(SearchContext context);

    public void remove(Istock_quant_package stock_quant_package);

    public void createBatch(List<Istock_quant_package> stock_quant_packages);

    public Page<Istock_quant_package> select(SearchContext context);

}
