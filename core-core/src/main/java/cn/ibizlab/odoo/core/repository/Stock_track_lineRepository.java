package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Stock_track_line;
import cn.ibizlab.odoo.core.odoo_stock.filter.Stock_track_lineSearchContext;

/**
 * 实体 [库存追溯行] 存储对象
 */
public interface Stock_track_lineRepository extends Repository<Stock_track_line> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Stock_track_line> searchDefault(Stock_track_lineSearchContext context);

    Stock_track_line convert2PO(cn.ibizlab.odoo.core.odoo_stock.domain.Stock_track_line domain , Stock_track_line po) ;

    cn.ibizlab.odoo.core.odoo_stock.domain.Stock_track_line convert2Domain( Stock_track_line po ,cn.ibizlab.odoo.core.odoo_stock.domain.Stock_track_line domain) ;

}
