package cn.ibizlab.odoo.core.repository.po ;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.math.BigInteger;

import cn.ibizlab.odoo.core.odoo_stock.filter.Stock_change_standard_priceSearchContext;

/**
 * 实体 [更改标准价] 存储模型
 */
public interface Stock_change_standard_price{

    /**
     * ID
     */
    Integer getId();

    void setId(Integer id);

    /**
     * 获取 [ID]脏标记
     */
    boolean getIdDirtyFlag();

    /**
     * 对方科目必填
     */
    String getCounterpart_account_id_required();

    void setCounterpart_account_id_required(String counterpart_account_id_required);

    /**
     * 获取 [对方科目必填]脏标记
     */
    boolean getCounterpart_account_id_requiredDirtyFlag();

    /**
     * 创建时间
     */
    Timestamp getCreate_date();

    void setCreate_date(Timestamp create_date);

    /**
     * 获取 [创建时间]脏标记
     */
    boolean getCreate_dateDirtyFlag();

    /**
     * 价格
     */
    Double getNew_price();

    void setNew_price(Double new_price);

    /**
     * 获取 [价格]脏标记
     */
    boolean getNew_priceDirtyFlag();

    /**
     * 最后修改日
     */
    Timestamp get__last_update();

    void set__last_update(Timestamp __last_update);

    /**
     * 获取 [最后修改日]脏标记
     */
    boolean get__last_updateDirtyFlag();

    /**
     * 显示名称
     */
    String getDisplay_name();

    void setDisplay_name(String display_name);

    /**
     * 获取 [显示名称]脏标记
     */
    boolean getDisplay_nameDirtyFlag();

    /**
     * 最后更新时间
     */
    Timestamp getWrite_date();

    void setWrite_date(Timestamp write_date);

    /**
     * 获取 [最后更新时间]脏标记
     */
    boolean getWrite_dateDirtyFlag();

    /**
     * 对方科目
     */
    String getCounterpart_account_id_text();

    void setCounterpart_account_id_text(String counterpart_account_id_text);

    /**
     * 获取 [对方科目]脏标记
     */
    boolean getCounterpart_account_id_textDirtyFlag();

    /**
     * 创建人
     */
    String getCreate_uid_text();

    void setCreate_uid_text(String create_uid_text);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uid_textDirtyFlag();

    /**
     * 最后更新者
     */
    String getWrite_uid_text();

    void setWrite_uid_text(String write_uid_text);

    /**
     * 获取 [最后更新者]脏标记
     */
    boolean getWrite_uid_textDirtyFlag();

    /**
     * 最后更新者
     */
    Integer getWrite_uid();

    void setWrite_uid(Integer write_uid);

    /**
     * 获取 [最后更新者]脏标记
     */
    boolean getWrite_uidDirtyFlag();

    /**
     * 创建人
     */
    Integer getCreate_uid();

    void setCreate_uid(Integer create_uid);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uidDirtyFlag();

    /**
     * 对方科目
     */
    Integer getCounterpart_account_id();

    void setCounterpart_account_id(Integer counterpart_account_id);

    /**
     * 获取 [对方科目]脏标记
     */
    boolean getCounterpart_account_idDirtyFlag();

}
