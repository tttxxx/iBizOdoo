package cn.ibizlab.odoo.core.repository.po ;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.math.BigInteger;

import cn.ibizlab.odoo.core.odoo_gamification.filter.Gamification_challenge_lineSearchContext;

/**
 * 实体 [游戏化挑战的一般目标] 存储模型
 */
public interface Gamification_challenge_line{

    /**
     * ID
     */
    Integer getId();

    void setId(Integer id);

    /**
     * 获取 [ID]脏标记
     */
    boolean getIdDirtyFlag();

    /**
     * 要达到的目标值
     */
    Double getTarget_goal();

    void setTarget_goal(Double target_goal);

    /**
     * 获取 [要达到的目标值]脏标记
     */
    boolean getTarget_goalDirtyFlag();

    /**
     * 最后更新时间
     */
    Timestamp getWrite_date();

    void setWrite_date(Timestamp write_date);

    /**
     * 获取 [最后更新时间]脏标记
     */
    boolean getWrite_dateDirtyFlag();

    /**
     * 最后修改日
     */
    Timestamp get__last_update();

    void set__last_update(Timestamp __last_update);

    /**
     * 获取 [最后修改日]脏标记
     */
    boolean get__last_updateDirtyFlag();

    /**
     * 创建时间
     */
    Timestamp getCreate_date();

    void setCreate_date(Timestamp create_date);

    /**
     * 获取 [创建时间]脏标记
     */
    boolean getCreate_dateDirtyFlag();

    /**
     * 显示名称
     */
    String getDisplay_name();

    void setDisplay_name(String display_name);

    /**
     * 获取 [显示名称]脏标记
     */
    boolean getDisplay_nameDirtyFlag();

    /**
     * 序号
     */
    Integer getSequence();

    void setSequence(Integer sequence);

    /**
     * 获取 [序号]脏标记
     */
    boolean getSequenceDirtyFlag();

    /**
     * 货币
     */
    String getDefinition_monetary();

    void setDefinition_monetary(String definition_monetary);

    /**
     * 获取 [货币]脏标记
     */
    boolean getDefinition_monetaryDirtyFlag();

    /**
     * 目标绩效
     */
    String getCondition();

    void setCondition(String condition);

    /**
     * 获取 [目标绩效]脏标记
     */
    boolean getConditionDirtyFlag();

    /**
     * 创建人
     */
    String getCreate_uid_text();

    void setCreate_uid_text(String create_uid_text);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uid_textDirtyFlag();

    /**
     * 最后更新人
     */
    String getWrite_uid_text();

    void setWrite_uid_text(String write_uid_text);

    /**
     * 获取 [最后更新人]脏标记
     */
    boolean getWrite_uid_textDirtyFlag();

    /**
     * 后缀
     */
    String getDefinition_full_suffix();

    void setDefinition_full_suffix(String definition_full_suffix);

    /**
     * 获取 [后缀]脏标记
     */
    boolean getDefinition_full_suffixDirtyFlag();

    /**
     * 挑战
     */
    String getChallenge_id_text();

    void setChallenge_id_text(String challenge_id_text);

    /**
     * 获取 [挑战]脏标记
     */
    boolean getChallenge_id_textDirtyFlag();

    /**
     * 名称
     */
    String getName();

    void setName(String name);

    /**
     * 获取 [名称]脏标记
     */
    boolean getNameDirtyFlag();

    /**
     * 单位
     */
    String getDefinition_suffix();

    void setDefinition_suffix(String definition_suffix);

    /**
     * 获取 [单位]脏标记
     */
    boolean getDefinition_suffixDirtyFlag();

    /**
     * 最后更新人
     */
    Integer getWrite_uid();

    void setWrite_uid(Integer write_uid);

    /**
     * 获取 [最后更新人]脏标记
     */
    boolean getWrite_uidDirtyFlag();

    /**
     * 挑战
     */
    Integer getChallenge_id();

    void setChallenge_id(Integer challenge_id);

    /**
     * 获取 [挑战]脏标记
     */
    boolean getChallenge_idDirtyFlag();

    /**
     * 创建人
     */
    Integer getCreate_uid();

    void setCreate_uid(Integer create_uid);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uidDirtyFlag();

    /**
     * 目标定义
     */
    Integer getDefinition_id();

    void setDefinition_id(Integer definition_id);

    /**
     * 获取 [目标定义]脏标记
     */
    boolean getDefinition_idDirtyFlag();

}
