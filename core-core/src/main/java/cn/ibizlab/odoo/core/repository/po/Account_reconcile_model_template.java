package cn.ibizlab.odoo.core.repository.po ;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.math.BigInteger;

import cn.ibizlab.odoo.core.odoo_account.filter.Account_reconcile_model_templateSearchContext;

/**
 * 实体 [核销模型模板] 存储模型
 */
public interface Account_reconcile_model_template{

    /**
     * 参数最小金额
     */
    Double getMatch_amount_min();

    void setMatch_amount_min(Double match_amount_min);

    /**
     * 获取 [参数最小金额]脏标记
     */
    boolean getMatch_amount_minDirtyFlag();

    /**
     * 含税价
     */
    String getForce_tax_included();

    void setForce_tax_included(String force_tax_included);

    /**
     * 获取 [含税价]脏标记
     */
    boolean getForce_tax_includedDirtyFlag();

    /**
     * 最后更新时间
     */
    Timestamp getWrite_date();

    void setWrite_date(Timestamp write_date);

    /**
     * 获取 [最后更新时间]脏标记
     */
    boolean getWrite_dateDirtyFlag();

    /**
     * 显示名称
     */
    String getDisplay_name();

    void setDisplay_name(String display_name);

    /**
     * 获取 [显示名称]脏标记
     */
    boolean getDisplay_nameDirtyFlag();

    /**
     * 限制合作伙伴类别为
     */
    String getMatch_partner_category_ids();

    void setMatch_partner_category_ids(String match_partner_category_ids);

    /**
     * 获取 [限制合作伙伴类别为]脏标记
     */
    boolean getMatch_partner_category_idsDirtyFlag();

    /**
     * 第二金额类型
     */
    String getSecond_amount_type();

    void setSecond_amount_type(String second_amount_type);

    /**
     * 获取 [第二金额类型]脏标记
     */
    boolean getSecond_amount_typeDirtyFlag();

    /**
     * 核销金额
     */
    Double getAmount();

    void setAmount(Double amount);

    /**
     * 获取 [核销金额]脏标记
     */
    boolean getAmountDirtyFlag();

    /**
     * 限制合作伙伴为
     */
    String getMatch_partner_ids();

    void setMatch_partner_ids(String match_partner_ids);

    /**
     * 获取 [限制合作伙伴为]脏标记
     */
    boolean getMatch_partner_idsDirtyFlag();

    /**
     * 凭证类型
     */
    String getMatch_journal_ids();

    void setMatch_journal_ids(String match_journal_ids);

    /**
     * 获取 [凭证类型]脏标记
     */
    boolean getMatch_journal_idsDirtyFlag();

    /**
     * 最后修改日
     */
    Timestamp get__last_update();

    void set__last_update(Timestamp __last_update);

    /**
     * 获取 [最后修改日]脏标记
     */
    boolean get__last_updateDirtyFlag();

    /**
     * 日记账项目标签
     */
    String getLabel();

    void setLabel(String label);

    /**
     * 获取 [日记账项目标签]脏标记
     */
    boolean getLabelDirtyFlag();

    /**
     * 会计匹配%
     */
    Double getMatch_total_amount_param();

    void setMatch_total_amount_param(Double match_total_amount_param);

    /**
     * 获取 [会计匹配%]脏标记
     */
    boolean getMatch_total_amount_paramDirtyFlag();

    /**
     * 类型
     */
    String getRule_type();

    void setRule_type(String rule_type);

    /**
     * 获取 [类型]脏标记
     */
    boolean getRule_typeDirtyFlag();

    /**
     * 金额
     */
    String getMatch_amount();

    void setMatch_amount(String match_amount);

    /**
     * 获取 [金额]脏标记
     */
    boolean getMatch_amountDirtyFlag();

    /**
     * 第二核销金额
     */
    Double getSecond_amount();

    void setSecond_amount(Double second_amount);

    /**
     * 获取 [第二核销金额]脏标记
     */
    boolean getSecond_amountDirtyFlag();

    /**
     * 金额类型
     */
    String getAmount_type();

    void setAmount_type(String amount_type);

    /**
     * 获取 [金额类型]脏标记
     */
    boolean getAmount_typeDirtyFlag();

    /**
     * 同币种匹配
     */
    String getMatch_same_currency();

    void setMatch_same_currency(String match_same_currency);

    /**
     * 获取 [同币种匹配]脏标记
     */
    boolean getMatch_same_currencyDirtyFlag();

    /**
     * 参数最大金额
     */
    Double getMatch_amount_max();

    void setMatch_amount_max(Double match_amount_max);

    /**
     * 获取 [参数最大金额]脏标记
     */
    boolean getMatch_amount_maxDirtyFlag();

    /**
     * 按钮标签
     */
    String getName();

    void setName(String name);

    /**
     * 获取 [按钮标签]脏标记
     */
    boolean getNameDirtyFlag();

    /**
     * 自动验证
     */
    String getAuto_reconcile();

    void setAuto_reconcile(String auto_reconcile);

    /**
     * 获取 [自动验证]脏标记
     */
    boolean getAuto_reconcileDirtyFlag();

    /**
     * 会计匹配
     */
    String getMatch_total_amount();

    void setMatch_total_amount(String match_total_amount);

    /**
     * 获取 [会计匹配]脏标记
     */
    boolean getMatch_total_amountDirtyFlag();

    /**
     * 添加第二行
     */
    String getHas_second_line();

    void setHas_second_line(String has_second_line);

    /**
     * 获取 [添加第二行]脏标记
     */
    boolean getHas_second_lineDirtyFlag();

    /**
     * 序号
     */
    Integer getSequence();

    void setSequence(Integer sequence);

    /**
     * 获取 [序号]脏标记
     */
    boolean getSequenceDirtyFlag();

    /**
     * 创建时间
     */
    Timestamp getCreate_date();

    void setCreate_date(Timestamp create_date);

    /**
     * 获取 [创建时间]脏标记
     */
    boolean getCreate_dateDirtyFlag();

    /**
     * ID
     */
    Integer getId();

    void setId(Integer id);

    /**
     * 获取 [ID]脏标记
     */
    boolean getIdDirtyFlag();

    /**
     * 数量性质
     */
    String getMatch_nature();

    void setMatch_nature(String match_nature);

    /**
     * 获取 [数量性质]脏标记
     */
    boolean getMatch_natureDirtyFlag();

    /**
     * 第二个分录项目标签
     */
    String getSecond_label();

    void setSecond_label(String second_label);

    /**
     * 获取 [第二个分录项目标签]脏标记
     */
    boolean getSecond_labelDirtyFlag();

    /**
     * 第二含税价‎
     */
    String getForce_second_tax_included();

    void setForce_second_tax_included(String force_second_tax_included);

    /**
     * 获取 [第二含税价‎]脏标记
     */
    boolean getForce_second_tax_includedDirtyFlag();

    /**
     * 标签参数
     */
    String getMatch_label_param();

    void setMatch_label_param(String match_label_param);

    /**
     * 获取 [标签参数]脏标记
     */
    boolean getMatch_label_paramDirtyFlag();

    /**
     * 标签
     */
    String getMatch_label();

    void setMatch_label(String match_label);

    /**
     * 获取 [标签]脏标记
     */
    boolean getMatch_labelDirtyFlag();

    /**
     * 已经匹配合作伙伴
     */
    String getMatch_partner();

    void setMatch_partner(String match_partner);

    /**
     * 获取 [已经匹配合作伙伴]脏标记
     */
    boolean getMatch_partnerDirtyFlag();

    /**
     * 最后更新人
     */
    String getWrite_uid_text();

    void setWrite_uid_text(String write_uid_text);

    /**
     * 获取 [最后更新人]脏标记
     */
    boolean getWrite_uid_textDirtyFlag();

    /**
     * 税率
     */
    String getTax_id_text();

    void setTax_id_text(String tax_id_text);

    /**
     * 获取 [税率]脏标记
     */
    boolean getTax_id_textDirtyFlag();

    /**
     * 第二个税
     */
    String getSecond_tax_id_text();

    void setSecond_tax_id_text(String second_tax_id_text);

    /**
     * 获取 [第二个税]脏标记
     */
    boolean getSecond_tax_id_textDirtyFlag();

    /**
     * 科目
     */
    String getAccount_id_text();

    void setAccount_id_text(String account_id_text);

    /**
     * 获取 [科目]脏标记
     */
    boolean getAccount_id_textDirtyFlag();

    /**
     * 表模板
     */
    String getChart_template_id_text();

    void setChart_template_id_text(String chart_template_id_text);

    /**
     * 获取 [表模板]脏标记
     */
    boolean getChart_template_id_textDirtyFlag();

    /**
     * 创建人
     */
    String getCreate_uid_text();

    void setCreate_uid_text(String create_uid_text);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uid_textDirtyFlag();

    /**
     * 第二科目
     */
    String getSecond_account_id_text();

    void setSecond_account_id_text(String second_account_id_text);

    /**
     * 获取 [第二科目]脏标记
     */
    boolean getSecond_account_id_textDirtyFlag();

    /**
     * 创建人
     */
    Integer getCreate_uid();

    void setCreate_uid(Integer create_uid);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uidDirtyFlag();

    /**
     * 第二科目
     */
    Integer getSecond_account_id();

    void setSecond_account_id(Integer second_account_id);

    /**
     * 获取 [第二科目]脏标记
     */
    boolean getSecond_account_idDirtyFlag();

    /**
     * 科目
     */
    Integer getAccount_id();

    void setAccount_id(Integer account_id);

    /**
     * 获取 [科目]脏标记
     */
    boolean getAccount_idDirtyFlag();

    /**
     * 最后更新人
     */
    Integer getWrite_uid();

    void setWrite_uid(Integer write_uid);

    /**
     * 获取 [最后更新人]脏标记
     */
    boolean getWrite_uidDirtyFlag();

    /**
     * 表模板
     */
    Integer getChart_template_id();

    void setChart_template_id(Integer chart_template_id);

    /**
     * 获取 [表模板]脏标记
     */
    boolean getChart_template_idDirtyFlag();

    /**
     * 第二个税
     */
    Integer getSecond_tax_id();

    void setSecond_tax_id(Integer second_tax_id);

    /**
     * 获取 [第二个税]脏标记
     */
    boolean getSecond_tax_idDirtyFlag();

    /**
     * 税率
     */
    Integer getTax_id();

    void setTax_id(Integer tax_id);

    /**
     * 获取 [税率]脏标记
     */
    boolean getTax_idDirtyFlag();

}
