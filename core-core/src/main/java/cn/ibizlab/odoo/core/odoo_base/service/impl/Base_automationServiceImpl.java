package cn.ibizlab.odoo.core.odoo_base.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_base.domain.Base_automation;
import cn.ibizlab.odoo.core.odoo_base.filter.Base_automationSearchContext;
import cn.ibizlab.odoo.core.odoo_base.service.IBase_automationService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;



import cn.ibizlab.odoo.core.odoo_base.client.base_automationOdooClient;
import cn.ibizlab.odoo.core.odoo_base.clientmodel.base_automationClientModel;

/**
 * 实体[自动动作] 服务对象接口实现
 */
@Slf4j
@Service
public class Base_automationServiceImpl implements IBase_automationService {

    @Autowired
    base_automationOdooClient base_automationOdooClient;


    @Override
    public Base_automation get(Integer id) {
        base_automationClientModel clientModel = new base_automationClientModel();
        clientModel.setId(id);
		base_automationOdooClient.get(clientModel);
        Base_automation et = convert2Domain(clientModel,null);
        if(et==null){
            et=new Base_automation();
            et.setId(id);
        }
        else{
        }

        return  et;
    }

    @Override
    public boolean create(Base_automation et) {
        base_automationClientModel clientModel = convert2Model(et,null);
		base_automationOdooClient.create(clientModel);
        Base_automation rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Base_automation> list){
    }

    @Override
    public boolean update(Base_automation et) {
        base_automationClientModel clientModel = convert2Model(et,null);
		base_automationOdooClient.update(clientModel);
        Base_automation rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }
    
    public void updateBatch(List<Base_automation> list){
    }

    @Override
    public boolean remove(Integer id) {
        base_automationClientModel clientModel = new base_automationClientModel();
        clientModel.setId(id);
		base_automationOdooClient.remove(clientModel);
        return true; 
    }

    public void removeBatch(Collection<Integer> idList){
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Base_automation> searchDefault(Base_automationSearchContext context) {
        List<Base_automation> list = new ArrayList<Base_automation>();
        Page<base_automationClientModel> clientModelList = base_automationOdooClient.search(context);
        for(base_automationClientModel clientModel : clientModelList){
            list.add( this.convert2Domain(clientModel,null));
        }
        return new PageImpl<Base_automation>(list, context.getPageable(), clientModelList.getTotalElements());
    }

    public base_automationClientModel convert2Model(Base_automation domain , base_automationClientModel model) {
        if(domain == null)
            return model ;
        if(model==null){
            model = new base_automationClientModel();

        }
        if((Boolean) domain.getExtensionparams().get("dirtyflagenable")){
            if((Boolean) domain.getExtensionparams().get("binding_model_iddirtyflag"))
                model.setBinding_model_id(domain.getBindingModelId());
            if((Boolean) domain.getExtensionparams().get("sequencedirtyflag"))
                model.setSequence(domain.getSequence());
            if((Boolean) domain.getExtensionparams().get("filter_pre_domaindirtyflag"))
                model.setFilter_pre_domain(domain.getFilterPreDomain());
            if((Boolean) domain.getExtensionparams().get("on_change_fieldsdirtyflag"))
                model.setOn_change_fields(domain.getOnChangeFields());
            if((Boolean) domain.getExtensionparams().get("activity_summarydirtyflag"))
                model.setActivity_summary(domain.getActivitySummary());
            if((Boolean) domain.getExtensionparams().get("activity_user_field_namedirtyflag"))
                model.setActivity_user_field_name(domain.getActivityUserFieldName());
            if((Boolean) domain.getExtensionparams().get("website_publisheddirtyflag"))
                model.setWebsite_published(domain.getWebsitePublished());
            if((Boolean) domain.getExtensionparams().get("usagedirtyflag"))
                model.setUsage(domain.getUsage());
            if((Boolean) domain.getExtensionparams().get("crud_model_namedirtyflag"))
                model.setCrud_model_name(domain.getCrudModelName());
            if((Boolean) domain.getExtensionparams().get("trg_date_range_typedirtyflag"))
                model.setTrg_date_range_type(domain.getTrgDateRangeType());
            if((Boolean) domain.getExtensionparams().get("iddirtyflag"))
                model.setId(domain.getId());
            if((Boolean) domain.getExtensionparams().get("fields_linesdirtyflag"))
                model.setFields_lines(domain.getFieldsLines());
            if((Boolean) domain.getExtensionparams().get("activity_notedirtyflag"))
                model.setActivity_note(domain.getActivityNote());
            if((Boolean) domain.getExtensionparams().get("activedirtyflag"))
                model.setActive(domain.getActive());
            if((Boolean) domain.getExtensionparams().get("activity_type_iddirtyflag"))
                model.setActivity_type_id(domain.getActivityTypeId());
            if((Boolean) domain.getExtensionparams().get("codedirtyflag"))
                model.setCode(domain.getCode());
            if((Boolean) domain.getExtensionparams().get("activity_user_typedirtyflag"))
                model.setActivity_user_type(domain.getActivityUserType());
            if((Boolean) domain.getExtensionparams().get("website_pathdirtyflag"))
                model.setWebsite_path(domain.getWebsitePath());
            if((Boolean) domain.getExtensionparams().get("partner_idsdirtyflag"))
                model.setPartner_ids(domain.getPartnerIds());
            if((Boolean) domain.getExtensionparams().get("__last_updatedirtyflag"))
                model.set__last_update(domain.getLastUpdate());
            if((Boolean) domain.getExtensionparams().get("trg_date_iddirtyflag"))
                model.setTrg_date_id(domain.getTrgDateId());
            if((Boolean) domain.getExtensionparams().get("xml_iddirtyflag"))
                model.setXml_id(domain.getXmlId());
            if((Boolean) domain.getExtensionparams().get("last_rundirtyflag"))
                model.setLast_run(domain.getLastRun());
            if((Boolean) domain.getExtensionparams().get("template_iddirtyflag"))
                model.setTemplate_id(domain.getTemplateId());
            if((Boolean) domain.getExtensionparams().get("binding_typedirtyflag"))
                model.setBinding_type(domain.getBindingType());
            if((Boolean) domain.getExtensionparams().get("namedirtyflag"))
                model.setName(domain.getName());
            if((Boolean) domain.getExtensionparams().get("model_namedirtyflag"))
                model.setModel_name(domain.getModelName());
            if((Boolean) domain.getExtensionparams().get("statedirtyflag"))
                model.setState(domain.getState());
            if((Boolean) domain.getExtensionparams().get("display_namedirtyflag"))
                model.setDisplay_name(domain.getDisplayName());
            if((Boolean) domain.getExtensionparams().get("channel_idsdirtyflag"))
                model.setChannel_ids(domain.getChannelIds());
            if((Boolean) domain.getExtensionparams().get("model_iddirtyflag"))
                model.setModel_id(domain.getModelId());
            if((Boolean) domain.getExtensionparams().get("helpdirtyflag"))
                model.setHelp(domain.getHelp());
            if((Boolean) domain.getExtensionparams().get("website_urldirtyflag"))
                model.setWebsite_url(domain.getWebsiteUrl());
            if((Boolean) domain.getExtensionparams().get("activity_user_iddirtyflag"))
                model.setActivity_user_id(domain.getActivityUserId());
            if((Boolean) domain.getExtensionparams().get("activity_date_deadline_rangedirtyflag"))
                model.setActivity_date_deadline_range(domain.getActivityDateDeadlineRange());
            if((Boolean) domain.getExtensionparams().get("trg_date_rangedirtyflag"))
                model.setTrg_date_range(domain.getTrgDateRange());
            if((Boolean) domain.getExtensionparams().get("typedirtyflag"))
                model.setType(domain.getType());
            if((Boolean) domain.getExtensionparams().get("filter_domaindirtyflag"))
                model.setFilter_domain(domain.getFilterDomain());
            if((Boolean) domain.getExtensionparams().get("activity_date_deadline_range_typedirtyflag"))
                model.setActivity_date_deadline_range_type(domain.getActivityDateDeadlineRangeType());
            if((Boolean) domain.getExtensionparams().get("write_datedirtyflag"))
                model.setWrite_date(domain.getWriteDate());
            if((Boolean) domain.getExtensionparams().get("child_idsdirtyflag"))
                model.setChild_ids(domain.getChildIds());
            if((Boolean) domain.getExtensionparams().get("link_field_iddirtyflag"))
                model.setLink_field_id(domain.getLinkFieldId());
            if((Boolean) domain.getExtensionparams().get("create_datedirtyflag"))
                model.setCreate_date(domain.getCreateDate());
            if((Boolean) domain.getExtensionparams().get("triggerdirtyflag"))
                model.setTrigger(domain.getTrigger());
            if((Boolean) domain.getExtensionparams().get("action_server_iddirtyflag"))
                model.setAction_server_id(domain.getActionServerId());
            if((Boolean) domain.getExtensionparams().get("crud_model_iddirtyflag"))
                model.setCrud_model_id(domain.getCrudModelId());
            if((Boolean) domain.getExtensionparams().get("create_uid_textdirtyflag"))
                model.setCreate_uid_text(domain.getCreateUidText());
            if((Boolean) domain.getExtensionparams().get("write_uid_textdirtyflag"))
                model.setWrite_uid_text(domain.getWriteUidText());
            if((Boolean) domain.getExtensionparams().get("trg_date_calendar_id_textdirtyflag"))
                model.setTrg_date_calendar_id_text(domain.getTrgDateCalendarIdText());
            if((Boolean) domain.getExtensionparams().get("write_uiddirtyflag"))
                model.setWrite_uid(domain.getWriteUid());
            if((Boolean) domain.getExtensionparams().get("trg_date_calendar_iddirtyflag"))
                model.setTrg_date_calendar_id(domain.getTrgDateCalendarId());
            if((Boolean) domain.getExtensionparams().get("create_uiddirtyflag"))
                model.setCreate_uid(domain.getCreateUid());
            domain.getFocusNull().clear();
        }
        return model ;
    }

    public Base_automation convert2Domain( base_automationClientModel model ,Base_automation domain) {
        if(model == null)
            return domain ;
        if(domain == null){
            domain = new Base_automation();
        }

        if(model.getBinding_model_idDirtyFlag())
            domain.setBindingModelId(model.getBinding_model_id());
        if(model.getSequenceDirtyFlag())
            domain.setSequence(model.getSequence());
        if(model.getFilter_pre_domainDirtyFlag())
            domain.setFilterPreDomain(model.getFilter_pre_domain());
        if(model.getOn_change_fieldsDirtyFlag())
            domain.setOnChangeFields(model.getOn_change_fields());
        if(model.getActivity_summaryDirtyFlag())
            domain.setActivitySummary(model.getActivity_summary());
        if(model.getActivity_user_field_nameDirtyFlag())
            domain.setActivityUserFieldName(model.getActivity_user_field_name());
        if(model.getWebsite_publishedDirtyFlag())
            domain.setWebsitePublished(model.getWebsite_published());
        if(model.getUsageDirtyFlag())
            domain.setUsage(model.getUsage());
        if(model.getCrud_model_nameDirtyFlag())
            domain.setCrudModelName(model.getCrud_model_name());
        if(model.getTrg_date_range_typeDirtyFlag())
            domain.setTrgDateRangeType(model.getTrg_date_range_type());
        if(model.getIdDirtyFlag())
            domain.setId(model.getId());
        if(model.getFields_linesDirtyFlag())
            domain.setFieldsLines(model.getFields_lines());
        if(model.getActivity_noteDirtyFlag())
            domain.setActivityNote(model.getActivity_note());
        if(model.getActiveDirtyFlag())
            domain.setActive(model.getActive());
        if(model.getActivity_type_idDirtyFlag())
            domain.setActivityTypeId(model.getActivity_type_id());
        if(model.getCodeDirtyFlag())
            domain.setCode(model.getCode());
        if(model.getActivity_user_typeDirtyFlag())
            domain.setActivityUserType(model.getActivity_user_type());
        if(model.getWebsite_pathDirtyFlag())
            domain.setWebsitePath(model.getWebsite_path());
        if(model.getPartner_idsDirtyFlag())
            domain.setPartnerIds(model.getPartner_ids());
        if(model.get__last_updateDirtyFlag())
            domain.setLastUpdate(model.get__last_update());
        if(model.getTrg_date_idDirtyFlag())
            domain.setTrgDateId(model.getTrg_date_id());
        if(model.getXml_idDirtyFlag())
            domain.setXmlId(model.getXml_id());
        if(model.getLast_runDirtyFlag())
            domain.setLastRun(model.getLast_run());
        if(model.getTemplate_idDirtyFlag())
            domain.setTemplateId(model.getTemplate_id());
        if(model.getBinding_typeDirtyFlag())
            domain.setBindingType(model.getBinding_type());
        if(model.getNameDirtyFlag())
            domain.setName(model.getName());
        if(model.getModel_nameDirtyFlag())
            domain.setModelName(model.getModel_name());
        if(model.getStateDirtyFlag())
            domain.setState(model.getState());
        if(model.getDisplay_nameDirtyFlag())
            domain.setDisplayName(model.getDisplay_name());
        if(model.getChannel_idsDirtyFlag())
            domain.setChannelIds(model.getChannel_ids());
        if(model.getModel_idDirtyFlag())
            domain.setModelId(model.getModel_id());
        if(model.getHelpDirtyFlag())
            domain.setHelp(model.getHelp());
        if(model.getWebsite_urlDirtyFlag())
            domain.setWebsiteUrl(model.getWebsite_url());
        if(model.getActivity_user_idDirtyFlag())
            domain.setActivityUserId(model.getActivity_user_id());
        if(model.getActivity_date_deadline_rangeDirtyFlag())
            domain.setActivityDateDeadlineRange(model.getActivity_date_deadline_range());
        if(model.getTrg_date_rangeDirtyFlag())
            domain.setTrgDateRange(model.getTrg_date_range());
        if(model.getTypeDirtyFlag())
            domain.setType(model.getType());
        if(model.getFilter_domainDirtyFlag())
            domain.setFilterDomain(model.getFilter_domain());
        if(model.getActivity_date_deadline_range_typeDirtyFlag())
            domain.setActivityDateDeadlineRangeType(model.getActivity_date_deadline_range_type());
        if(model.getWrite_dateDirtyFlag())
            domain.setWriteDate(model.getWrite_date());
        if(model.getChild_idsDirtyFlag())
            domain.setChildIds(model.getChild_ids());
        if(model.getLink_field_idDirtyFlag())
            domain.setLinkFieldId(model.getLink_field_id());
        if(model.getCreate_dateDirtyFlag())
            domain.setCreateDate(model.getCreate_date());
        if(model.getTriggerDirtyFlag())
            domain.setTrigger(model.getTrigger());
        if(model.getAction_server_idDirtyFlag())
            domain.setActionServerId(model.getAction_server_id());
        if(model.getCrud_model_idDirtyFlag())
            domain.setCrudModelId(model.getCrud_model_id());
        if(model.getCreate_uid_textDirtyFlag())
            domain.setCreateUidText(model.getCreate_uid_text());
        if(model.getWrite_uid_textDirtyFlag())
            domain.setWriteUidText(model.getWrite_uid_text());
        if(model.getTrg_date_calendar_id_textDirtyFlag())
            domain.setTrgDateCalendarIdText(model.getTrg_date_calendar_id_text());
        if(model.getWrite_uidDirtyFlag())
            domain.setWriteUid(model.getWrite_uid());
        if(model.getTrg_date_calendar_idDirtyFlag())
            domain.setTrgDateCalendarId(model.getTrg_date_calendar_id());
        if(model.getCreate_uidDirtyFlag())
            domain.setCreateUid(model.getCreate_uid());
        return domain ;
    }

}

    



