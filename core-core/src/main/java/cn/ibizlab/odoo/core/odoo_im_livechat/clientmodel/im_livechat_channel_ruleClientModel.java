package cn.ibizlab.odoo.core.odoo_im_livechat.clientmodel;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.math.BigInteger;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.util.helper.OdooClientHelper;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang.ArrayUtils;
import org.springframework.util.StringUtils;

/**
 * 接口实体[im_livechat_channel_rule] 对象
 */
public class im_livechat_channel_ruleClientModel implements Serializable{

    /**
     * 动作
     */
    public String action;

    @JsonIgnore
    public boolean actionDirtyFlag;
    
    /**
     * 自动弹出时间
     */
    public Integer auto_popup_timer;

    @JsonIgnore
    public boolean auto_popup_timerDirtyFlag;
    
    /**
     * 渠道
     */
    public Integer channel_id;

    @JsonIgnore
    public boolean channel_idDirtyFlag;
    
    /**
     * 渠道
     */
    public String channel_id_text;

    @JsonIgnore
    public boolean channel_id_textDirtyFlag;
    
    /**
     * 国家
     */
    public String country_ids;

    @JsonIgnore
    public boolean country_idsDirtyFlag;
    
    /**
     * 创建时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp create_date;

    @JsonIgnore
    public boolean create_dateDirtyFlag;
    
    /**
     * 创建人
     */
    public Integer create_uid;

    @JsonIgnore
    public boolean create_uidDirtyFlag;
    
    /**
     * 创建人
     */
    public String create_uid_text;

    @JsonIgnore
    public boolean create_uid_textDirtyFlag;
    
    /**
     * 显示名称
     */
    public String display_name;

    @JsonIgnore
    public boolean display_nameDirtyFlag;
    
    /**
     * ID
     */
    public Integer id;

    @JsonIgnore
    public boolean idDirtyFlag;
    
    /**
     * URL的正则表达式
     */
    public String regex_url;

    @JsonIgnore
    public boolean regex_urlDirtyFlag;
    
    /**
     * 匹配的订单
     */
    public Integer sequence;

    @JsonIgnore
    public boolean sequenceDirtyFlag;
    
    /**
     * 最后更新时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp write_date;

    @JsonIgnore
    public boolean write_dateDirtyFlag;
    
    /**
     * 最后更新人
     */
    public Integer write_uid;

    @JsonIgnore
    public boolean write_uidDirtyFlag;
    
    /**
     * 最后更新人
     */
    public String write_uid_text;

    @JsonIgnore
    public boolean write_uid_textDirtyFlag;
    
    /**
     * 最后修改日
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp __last_update;

    @JsonIgnore
    public boolean __last_updateDirtyFlag;
    
    /**
     * 获取 [动作]
     */
    @JsonProperty("action")
    public String getAction(){
        return this.action ;
    }

    /**
     * 设置 [动作]
     */
    @JsonProperty("action")
    public void setAction(String  action){
        this.action = action ;
        this.actionDirtyFlag = true ;
    }

     /**
     * 获取 [动作]脏标记
     */
    @JsonIgnore
    public boolean getActionDirtyFlag(){
        return this.actionDirtyFlag ;
    }   

    /**
     * 获取 [自动弹出时间]
     */
    @JsonProperty("auto_popup_timer")
    public Integer getAuto_popup_timer(){
        return this.auto_popup_timer ;
    }

    /**
     * 设置 [自动弹出时间]
     */
    @JsonProperty("auto_popup_timer")
    public void setAuto_popup_timer(Integer  auto_popup_timer){
        this.auto_popup_timer = auto_popup_timer ;
        this.auto_popup_timerDirtyFlag = true ;
    }

     /**
     * 获取 [自动弹出时间]脏标记
     */
    @JsonIgnore
    public boolean getAuto_popup_timerDirtyFlag(){
        return this.auto_popup_timerDirtyFlag ;
    }   

    /**
     * 获取 [渠道]
     */
    @JsonProperty("channel_id")
    public Integer getChannel_id(){
        return this.channel_id ;
    }

    /**
     * 设置 [渠道]
     */
    @JsonProperty("channel_id")
    public void setChannel_id(Integer  channel_id){
        this.channel_id = channel_id ;
        this.channel_idDirtyFlag = true ;
    }

     /**
     * 获取 [渠道]脏标记
     */
    @JsonIgnore
    public boolean getChannel_idDirtyFlag(){
        return this.channel_idDirtyFlag ;
    }   

    /**
     * 获取 [渠道]
     */
    @JsonProperty("channel_id_text")
    public String getChannel_id_text(){
        return this.channel_id_text ;
    }

    /**
     * 设置 [渠道]
     */
    @JsonProperty("channel_id_text")
    public void setChannel_id_text(String  channel_id_text){
        this.channel_id_text = channel_id_text ;
        this.channel_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [渠道]脏标记
     */
    @JsonIgnore
    public boolean getChannel_id_textDirtyFlag(){
        return this.channel_id_textDirtyFlag ;
    }   

    /**
     * 获取 [国家]
     */
    @JsonProperty("country_ids")
    public String getCountry_ids(){
        return this.country_ids ;
    }

    /**
     * 设置 [国家]
     */
    @JsonProperty("country_ids")
    public void setCountry_ids(String  country_ids){
        this.country_ids = country_ids ;
        this.country_idsDirtyFlag = true ;
    }

     /**
     * 获取 [国家]脏标记
     */
    @JsonIgnore
    public boolean getCountry_idsDirtyFlag(){
        return this.country_idsDirtyFlag ;
    }   

    /**
     * 获取 [创建时间]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return this.create_date ;
    }

    /**
     * 设置 [创建时间]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

     /**
     * 获取 [创建时间]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return this.create_dateDirtyFlag ;
    }   

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return this.create_uid ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

     /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return this.create_uidDirtyFlag ;
    }   

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return this.create_uid_text ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

     /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return this.create_uid_textDirtyFlag ;
    }   

    /**
     * 获取 [显示名称]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return this.display_name ;
    }

    /**
     * 设置 [显示名称]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

     /**
     * 获取 [显示名称]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return this.display_nameDirtyFlag ;
    }   

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return this.id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

     /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return this.idDirtyFlag ;
    }   

    /**
     * 获取 [URL的正则表达式]
     */
    @JsonProperty("regex_url")
    public String getRegex_url(){
        return this.regex_url ;
    }

    /**
     * 设置 [URL的正则表达式]
     */
    @JsonProperty("regex_url")
    public void setRegex_url(String  regex_url){
        this.regex_url = regex_url ;
        this.regex_urlDirtyFlag = true ;
    }

     /**
     * 获取 [URL的正则表达式]脏标记
     */
    @JsonIgnore
    public boolean getRegex_urlDirtyFlag(){
        return this.regex_urlDirtyFlag ;
    }   

    /**
     * 获取 [匹配的订单]
     */
    @JsonProperty("sequence")
    public Integer getSequence(){
        return this.sequence ;
    }

    /**
     * 设置 [匹配的订单]
     */
    @JsonProperty("sequence")
    public void setSequence(Integer  sequence){
        this.sequence = sequence ;
        this.sequenceDirtyFlag = true ;
    }

     /**
     * 获取 [匹配的订单]脏标记
     */
    @JsonIgnore
    public boolean getSequenceDirtyFlag(){
        return this.sequenceDirtyFlag ;
    }   

    /**
     * 获取 [最后更新时间]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return this.write_date ;
    }

    /**
     * 设置 [最后更新时间]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新时间]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return this.write_dateDirtyFlag ;
    }   

    /**
     * 获取 [最后更新人]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return this.write_uid ;
    }

    /**
     * 设置 [最后更新人]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新人]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return this.write_uidDirtyFlag ;
    }   

    /**
     * 获取 [最后更新人]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return this.write_uid_text ;
    }

    /**
     * 设置 [最后更新人]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新人]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return this.write_uid_textDirtyFlag ;
    }   

    /**
     * 获取 [最后修改日]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return this.__last_update ;
    }

    /**
     * 设置 [最后修改日]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

     /**
     * 获取 [最后修改日]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return this.__last_updateDirtyFlag ;
    }   



    public void fromMap(Map<String, Object> map) throws Exception {
		if(!(map.get("action") instanceof Boolean)&& map.get("action")!=null){
			this.setAction((String)map.get("action"));
		}
		if(!(map.get("auto_popup_timer") instanceof Boolean)&& map.get("auto_popup_timer")!=null){
			this.setAuto_popup_timer((Integer)map.get("auto_popup_timer"));
		}
		if(!(map.get("channel_id") instanceof Boolean)&& map.get("channel_id")!=null){
			Object[] objs = (Object[])map.get("channel_id");
			if(objs.length > 0){
				this.setChannel_id((Integer)objs[0]);
			}
		}
		if(!(map.get("channel_id") instanceof Boolean)&& map.get("channel_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("channel_id");
			if(objs.length > 1){
				this.setChannel_id_text((String)objs[1]);
			}
		}
		if(!(map.get("country_ids") instanceof Boolean)&& map.get("country_ids")!=null){
			Object[] objs = (Object[])map.get("country_ids");
			if(objs.length > 0){
				Integer[] country_ids = Arrays.copyOfRange(objs,0,objs.length,Integer[].class);
				this.setCountry_ids(Arrays.toString(country_ids).replace(" ",""));
			}
		}
		if(!(map.get("create_date") instanceof Boolean)&& map.get("create_date")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse((String)map.get("create_date"));
   			this.setCreate_date(new Timestamp(parse.getTime()));
		}
		if(!(map.get("create_uid") instanceof Boolean)&& map.get("create_uid")!=null){
			Object[] objs = (Object[])map.get("create_uid");
			if(objs.length > 0){
				this.setCreate_uid((Integer)objs[0]);
			}
		}
		if(!(map.get("create_uid") instanceof Boolean)&& map.get("create_uid")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("create_uid");
			if(objs.length > 1){
				this.setCreate_uid_text((String)objs[1]);
			}
		}
		if(!(map.get("display_name") instanceof Boolean)&& map.get("display_name")!=null){
			this.setDisplay_name((String)map.get("display_name"));
		}
		if(!(map.get("id") instanceof Boolean)&& map.get("id")!=null){
			this.setId((Integer)map.get("id"));
		}
		if(!(map.get("regex_url") instanceof Boolean)&& map.get("regex_url")!=null){
			this.setRegex_url((String)map.get("regex_url"));
		}
		if(!(map.get("sequence") instanceof Boolean)&& map.get("sequence")!=null){
			this.setSequence((Integer)map.get("sequence"));
		}
		if(!(map.get("write_date") instanceof Boolean)&& map.get("write_date")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse((String)map.get("write_date"));
   			this.setWrite_date(new Timestamp(parse.getTime()));
		}
		if(!(map.get("write_uid") instanceof Boolean)&& map.get("write_uid")!=null){
			Object[] objs = (Object[])map.get("write_uid");
			if(objs.length > 0){
				this.setWrite_uid((Integer)objs[0]);
			}
		}
		if(!(map.get("write_uid") instanceof Boolean)&& map.get("write_uid")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("write_uid");
			if(objs.length > 1){
				this.setWrite_uid_text((String)objs[1]);
			}
		}
		if(!(map.get("__last_update") instanceof Boolean)&& map.get("__last_update")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse((String)map.get("__last_update"));
   			this.set__last_update(new Timestamp(parse.getTime()));
		}
	}

	public Map<String, Object> toMap() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		if(this.getAction()!=null&&this.getActionDirtyFlag()){
			map.put("action",this.getAction());
		}else if(this.getActionDirtyFlag()){
			map.put("action",false);
		}
		if(this.getAuto_popup_timer()!=null&&this.getAuto_popup_timerDirtyFlag()){
			map.put("auto_popup_timer",this.getAuto_popup_timer());
		}else if(this.getAuto_popup_timerDirtyFlag()){
			map.put("auto_popup_timer",false);
		}
		if(this.getChannel_id()!=null&&this.getChannel_idDirtyFlag()){
			map.put("channel_id",this.getChannel_id());
		}else if(this.getChannel_idDirtyFlag()){
			map.put("channel_id",false);
		}
		if(this.getChannel_id_text()!=null&&this.getChannel_id_textDirtyFlag()){
			//忽略文本外键channel_id_text
		}else if(this.getChannel_id_textDirtyFlag()){
			map.put("channel_id",false);
		}
		if(this.getCountry_ids()!=null&&this.getCountry_idsDirtyFlag()){
			map.put("country_ids",this.getCountry_ids());
		}else if(this.getCountry_idsDirtyFlag()){
			map.put("country_ids",false);
		}
		if(this.getCreate_date()!=null&&this.getCreate_dateDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String datetimeStr = sdf.format(this.getCreate_date());
			map.put("create_date",datetimeStr);
		}else if(this.getCreate_dateDirtyFlag()){
			map.put("create_date",false);
		}
		if(this.getCreate_uid()!=null&&this.getCreate_uidDirtyFlag()){
			map.put("create_uid",this.getCreate_uid());
		}else if(this.getCreate_uidDirtyFlag()){
			map.put("create_uid",false);
		}
		if(this.getCreate_uid_text()!=null&&this.getCreate_uid_textDirtyFlag()){
			//忽略文本外键create_uid_text
		}else if(this.getCreate_uid_textDirtyFlag()){
			map.put("create_uid",false);
		}
		if(this.getDisplay_name()!=null&&this.getDisplay_nameDirtyFlag()){
			map.put("display_name",this.getDisplay_name());
		}else if(this.getDisplay_nameDirtyFlag()){
			map.put("display_name",false);
		}
		if(this.getId()!=null&&this.getIdDirtyFlag()){
			map.put("id",this.getId());
		}else if(this.getIdDirtyFlag()){
			map.put("id",false);
		}
		if(this.getRegex_url()!=null&&this.getRegex_urlDirtyFlag()){
			map.put("regex_url",this.getRegex_url());
		}else if(this.getRegex_urlDirtyFlag()){
			map.put("regex_url",false);
		}
		if(this.getSequence()!=null&&this.getSequenceDirtyFlag()){
			map.put("sequence",this.getSequence());
		}else if(this.getSequenceDirtyFlag()){
			map.put("sequence",false);
		}
		if(this.getWrite_date()!=null&&this.getWrite_dateDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String datetimeStr = sdf.format(this.getWrite_date());
			map.put("write_date",datetimeStr);
		}else if(this.getWrite_dateDirtyFlag()){
			map.put("write_date",false);
		}
		if(this.getWrite_uid()!=null&&this.getWrite_uidDirtyFlag()){
			map.put("write_uid",this.getWrite_uid());
		}else if(this.getWrite_uidDirtyFlag()){
			map.put("write_uid",false);
		}
		if(this.getWrite_uid_text()!=null&&this.getWrite_uid_textDirtyFlag()){
			//忽略文本外键write_uid_text
		}else if(this.getWrite_uid_textDirtyFlag()){
			map.put("write_uid",false);
		}
		if(this.get__last_update()!=null&&this.get__last_updateDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String datetimeStr = sdf.format(this.get__last_update());
			map.put("__last_update",datetimeStr);
		}else if(this.get__last_updateDirtyFlag()){
			map.put("__last_update",false);
		}
		return map;
	}

}
