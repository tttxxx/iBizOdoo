package cn.ibizlab.odoo.core.odoo_mail.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_mail.domain.Mail_mass_mailing_list_contact_rel;
import cn.ibizlab.odoo.core.odoo_mail.filter.Mail_mass_mailing_list_contact_relSearchContext;
import cn.ibizlab.odoo.core.odoo_mail.service.IMail_mass_mailing_list_contact_relService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;



import cn.ibizlab.odoo.core.odoo_mail.client.mail_mass_mailing_list_contact_relOdooClient;
import cn.ibizlab.odoo.core.odoo_mail.clientmodel.mail_mass_mailing_list_contact_relClientModel;

/**
 * 实体[群发邮件订阅信息] 服务对象接口实现
 */
@Slf4j
@Service
public class Mail_mass_mailing_list_contact_relServiceImpl implements IMail_mass_mailing_list_contact_relService {

    @Autowired
    mail_mass_mailing_list_contact_relOdooClient mail_mass_mailing_list_contact_relOdooClient;


    @Override
    public boolean remove(Integer id) {
        mail_mass_mailing_list_contact_relClientModel clientModel = new mail_mass_mailing_list_contact_relClientModel();
        clientModel.setId(id);
		mail_mass_mailing_list_contact_relOdooClient.remove(clientModel);
        return true; 
    }

    public void removeBatch(Collection<Integer> idList){
    }

    @Override
    public boolean create(Mail_mass_mailing_list_contact_rel et) {
        mail_mass_mailing_list_contact_relClientModel clientModel = convert2Model(et,null);
		mail_mass_mailing_list_contact_relOdooClient.create(clientModel);
        Mail_mass_mailing_list_contact_rel rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Mail_mass_mailing_list_contact_rel> list){
    }

    @Override
    public Mail_mass_mailing_list_contact_rel get(Integer id) {
        mail_mass_mailing_list_contact_relClientModel clientModel = new mail_mass_mailing_list_contact_relClientModel();
        clientModel.setId(id);
		mail_mass_mailing_list_contact_relOdooClient.get(clientModel);
        Mail_mass_mailing_list_contact_rel et = convert2Domain(clientModel,null);
        if(et==null){
            et=new Mail_mass_mailing_list_contact_rel();
            et.setId(id);
        }
        else{
        }

        return  et;
    }

    @Override
    public boolean update(Mail_mass_mailing_list_contact_rel et) {
        mail_mass_mailing_list_contact_relClientModel clientModel = convert2Model(et,null);
		mail_mass_mailing_list_contact_relOdooClient.update(clientModel);
        Mail_mass_mailing_list_contact_rel rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }
    
    public void updateBatch(List<Mail_mass_mailing_list_contact_rel> list){
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Mail_mass_mailing_list_contact_rel> searchDefault(Mail_mass_mailing_list_contact_relSearchContext context) {
        List<Mail_mass_mailing_list_contact_rel> list = new ArrayList<Mail_mass_mailing_list_contact_rel>();
        Page<mail_mass_mailing_list_contact_relClientModel> clientModelList = mail_mass_mailing_list_contact_relOdooClient.search(context);
        for(mail_mass_mailing_list_contact_relClientModel clientModel : clientModelList){
            list.add( this.convert2Domain(clientModel,null));
        }
        return new PageImpl<Mail_mass_mailing_list_contact_rel>(list, context.getPageable(), clientModelList.getTotalElements());
    }

    public mail_mass_mailing_list_contact_relClientModel convert2Model(Mail_mass_mailing_list_contact_rel domain , mail_mass_mailing_list_contact_relClientModel model) {
        if(domain == null)
            return model ;
        if(model==null){
            model = new mail_mass_mailing_list_contact_relClientModel();

        }
        if((Boolean) domain.getExtensionparams().get("dirtyflagenable")){
            if((Boolean) domain.getExtensionparams().get("display_namedirtyflag"))
                model.setDisplay_name(domain.getDisplayName());
            if((Boolean) domain.getExtensionparams().get("unsubscription_datedirtyflag"))
                model.setUnsubscription_date(domain.getUnsubscriptionDate());
            if((Boolean) domain.getExtensionparams().get("create_datedirtyflag"))
                model.setCreate_date(domain.getCreateDate());
            if((Boolean) domain.getExtensionparams().get("write_datedirtyflag"))
                model.setWrite_date(domain.getWriteDate());
            if((Boolean) domain.getExtensionparams().get("iddirtyflag"))
                model.setId(domain.getId());
            if((Boolean) domain.getExtensionparams().get("opt_outdirtyflag"))
                model.setOpt_out(domain.getOptOut());
            if((Boolean) domain.getExtensionparams().get("__last_updatedirtyflag"))
                model.set__last_update(domain.getLastUpdate());
            if((Boolean) domain.getExtensionparams().get("message_bouncedirtyflag"))
                model.setMessage_bounce(domain.getMessageBounce());
            if((Boolean) domain.getExtensionparams().get("is_blacklisteddirtyflag"))
                model.setIs_blacklisted(domain.getIsBlacklisted());
            if((Boolean) domain.getExtensionparams().get("list_id_textdirtyflag"))
                model.setList_id_text(domain.getListIdText());
            if((Boolean) domain.getExtensionparams().get("contact_id_textdirtyflag"))
                model.setContact_id_text(domain.getContactIdText());
            if((Boolean) domain.getExtensionparams().get("create_uid_textdirtyflag"))
                model.setCreate_uid_text(domain.getCreateUidText());
            if((Boolean) domain.getExtensionparams().get("contact_countdirtyflag"))
                model.setContact_count(domain.getContactCount());
            if((Boolean) domain.getExtensionparams().get("write_uid_textdirtyflag"))
                model.setWrite_uid_text(domain.getWriteUidText());
            if((Boolean) domain.getExtensionparams().get("write_uiddirtyflag"))
                model.setWrite_uid(domain.getWriteUid());
            if((Boolean) domain.getExtensionparams().get("create_uiddirtyflag"))
                model.setCreate_uid(domain.getCreateUid());
            if((Boolean) domain.getExtensionparams().get("contact_iddirtyflag"))
                model.setContact_id(domain.getContactId());
            if((Boolean) domain.getExtensionparams().get("list_iddirtyflag"))
                model.setList_id(domain.getListId());
            domain.getFocusNull().clear();
        }
        return model ;
    }

    public Mail_mass_mailing_list_contact_rel convert2Domain( mail_mass_mailing_list_contact_relClientModel model ,Mail_mass_mailing_list_contact_rel domain) {
        if(model == null)
            return domain ;
        if(domain == null){
            domain = new Mail_mass_mailing_list_contact_rel();
        }

        if(model.getDisplay_nameDirtyFlag())
            domain.setDisplayName(model.getDisplay_name());
        if(model.getUnsubscription_dateDirtyFlag())
            domain.setUnsubscriptionDate(model.getUnsubscription_date());
        if(model.getCreate_dateDirtyFlag())
            domain.setCreateDate(model.getCreate_date());
        if(model.getWrite_dateDirtyFlag())
            domain.setWriteDate(model.getWrite_date());
        if(model.getIdDirtyFlag())
            domain.setId(model.getId());
        if(model.getOpt_outDirtyFlag())
            domain.setOptOut(model.getOpt_out());
        if(model.get__last_updateDirtyFlag())
            domain.setLastUpdate(model.get__last_update());
        if(model.getMessage_bounceDirtyFlag())
            domain.setMessageBounce(model.getMessage_bounce());
        if(model.getIs_blacklistedDirtyFlag())
            domain.setIsBlacklisted(model.getIs_blacklisted());
        if(model.getList_id_textDirtyFlag())
            domain.setListIdText(model.getList_id_text());
        if(model.getContact_id_textDirtyFlag())
            domain.setContactIdText(model.getContact_id_text());
        if(model.getCreate_uid_textDirtyFlag())
            domain.setCreateUidText(model.getCreate_uid_text());
        if(model.getContact_countDirtyFlag())
            domain.setContactCount(model.getContact_count());
        if(model.getWrite_uid_textDirtyFlag())
            domain.setWriteUidText(model.getWrite_uid_text());
        if(model.getWrite_uidDirtyFlag())
            domain.setWriteUid(model.getWrite_uid());
        if(model.getCreate_uidDirtyFlag())
            domain.setCreateUid(model.getCreate_uid());
        if(model.getContact_idDirtyFlag())
            domain.setContactId(model.getContact_id());
        if(model.getList_idDirtyFlag())
            domain.setListId(model.getList_id());
        return domain ;
    }

}

    



