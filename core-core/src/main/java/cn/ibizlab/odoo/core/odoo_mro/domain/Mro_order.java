package cn.ibizlab.odoo.core.odoo_mro.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.odoo.util.domain.EntityBase;
import cn.ibizlab.odoo.util.annotation.DEField;
import cn.ibizlab.odoo.util.enums.DEPredefinedFieldType;
import cn.ibizlab.odoo.util.enums.DEFieldDefaultValueType;
import java.io.Serializable;
import lombok.Data;
import org.springframework.data.annotation.Transient;

import cn.ibizlab.odoo.util.domain.EntityClient;

/**
 * ServiceApi [Maintenance Order] 对象
 */
@Data
public class Mro_order extends EntityClient implements Serializable {

    /**
     * 消息
     */
    @JSONField(name = "message_ids")
    @JsonProperty("message_ids")
    private String messageIds;

    /**
     * 最后修改日
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;

    /**
     * 关注者(业务伙伴)
     */
    @JSONField(name = "message_partner_ids")
    @JsonProperty("message_partner_ids")
    private String messagePartnerIds;

    /**
     * 保养类型
     */
    @DEField(name = "maintenance_type")
    @JSONField(name = "maintenance_type")
    @JsonProperty("maintenance_type")
    private String maintenanceType;

    /**
     * 未读消息计数器
     */
    @JSONField(name = "message_unread_counter")
    @JsonProperty("message_unread_counter")
    private Integer messageUnreadCounter;

    /**
     * Documentation Description
     */
    @DEField(name = "documentation_description")
    @JSONField(name = "documentation_description")
    @JsonProperty("documentation_description")
    private String documentationDescription;

    /**
     * Parts Moved Lines
     */
    @JSONField(name = "parts_moved_lines")
    @JsonProperty("parts_moved_lines")
    private String partsMovedLines;

    /**
     * 关注者(渠道)
     */
    @JSONField(name = "message_channel_ids")
    @JsonProperty("message_channel_ids")
    private String messageChannelIds;

    /**
     * Asset Category
     */
    @JSONField(name = "category_ids")
    @JsonProperty("category_ids")
    private String categoryIds;

    /**
     * 计划日期
     */
    @DEField(name = "date_scheduled")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "date_scheduled" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("date_scheduled")
    private Timestamp dateScheduled;

    /**
     * 显示名称
     */
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    private String displayName;

    /**
     * Labor Description
     */
    @DEField(name = "labor_description")
    @JSONField(name = "labor_description")
    @JsonProperty("labor_description")
    private String laborDescription;

    /**
     * 未读消息
     */
    @JSONField(name = "message_unread")
    @JsonProperty("message_unread")
    private String messageUnread;

    /**
     * 关注者
     */
    @JSONField(name = "message_follower_ids")
    @JsonProperty("message_follower_ids")
    private String messageFollowerIds;

    /**
     * 源文档
     */
    @JSONField(name = "origin")
    @JsonProperty("origin")
    private String origin;

    /**
     * 错误个数
     */
    @JSONField(name = "message_has_error_counter")
    @JsonProperty("message_has_error_counter")
    private Integer messageHasErrorCounter;

    /**
     * 最后更新时间
     */
    @DEField(name = "write_date" , preType = DEPredefinedFieldType.UPDATEDATE)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;

    /**
     * 说明
     */
    @JSONField(name = "description")
    @JsonProperty("description")
    private String description;

    /**
     * Parts Ready Lines
     */
    @JSONField(name = "parts_ready_lines")
    @JsonProperty("parts_ready_lines")
    private String partsReadyLines;

    /**
     * 网站消息
     */
    @JSONField(name = "website_message_ids")
    @JsonProperty("website_message_ids")
    private String websiteMessageIds;

    /**
     * Parts Move Lines
     */
    @JSONField(name = "parts_move_lines")
    @JsonProperty("parts_move_lines")
    private String partsMoveLines;

    /**
     * 关注者
     */
    @JSONField(name = "message_is_follower")
    @JsonProperty("message_is_follower")
    private String messageIsFollower;

    /**
     * 操作次数
     */
    @JSONField(name = "message_needaction_counter")
    @JsonProperty("message_needaction_counter")
    private Integer messageNeedactionCounter;

    /**
     * Operations Description
     */
    @DEField(name = "operations_description")
    @JSONField(name = "operations_description")
    @JsonProperty("operations_description")
    private String operationsDescription;

    /**
     * Problem Description
     */
    @DEField(name = "problem_description")
    @JSONField(name = "problem_description")
    @JsonProperty("problem_description")
    private String problemDescription;

    /**
     * 附件数量
     */
    @JSONField(name = "message_attachment_count")
    @JsonProperty("message_attachment_count")
    private Integer messageAttachmentCount;

    /**
     * Planned Date
     */
    @DEField(name = "date_planned")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "date_planned" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("date_planned")
    private Timestamp datePlanned;

    /**
     * Procurement group
     */
    @DEField(name = "procurement_group_id")
    @JSONField(name = "procurement_group_id")
    @JsonProperty("procurement_group_id")
    private Integer procurementGroupId;

    /**
     * Execution Date
     */
    @DEField(name = "date_execution")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "date_execution" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("date_execution")
    private Timestamp dateExecution;

    /**
     * 消息递送错误
     */
    @JSONField(name = "message_has_error")
    @JsonProperty("message_has_error")
    private String messageHasError;

    /**
     * 附件
     */
    @DEField(name = "message_main_attachment_id")
    @JSONField(name = "message_main_attachment_id")
    @JsonProperty("message_main_attachment_id")
    private Integer messageMainAttachmentId;

    /**
     * ID
     */
    @DEField(isKeyField=true)
    @JSONField(name = "id")
    @JsonProperty("id")
    private Integer id;

    /**
     * Planned parts
     */
    @JSONField(name = "parts_lines")
    @JsonProperty("parts_lines")
    private String partsLines;

    /**
     * 编号
     */
    @JSONField(name = "name")
    @JsonProperty("name")
    private String name;

    /**
     * 状态
     */
    @JSONField(name = "state")
    @JsonProperty("state")
    private String state;

    /**
     * 创建时间
     */
    @DEField(name = "create_date" , preType = DEPredefinedFieldType.CREATEDATE)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;

    /**
     * 前置操作
     */
    @JSONField(name = "message_needaction")
    @JsonProperty("message_needaction")
    private String messageNeedaction;

    /**
     * Tools Description
     */
    @DEField(name = "tools_description")
    @JSONField(name = "tools_description")
    @JsonProperty("tools_description")
    private String toolsDescription;

    /**
     * 创建人
     */
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    private String createUidText;

    /**
     * 最后更新者
     */
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    private String writeUidText;

    /**
     * 工单
     */
    @JSONField(name = "wo_id_text")
    @JsonProperty("wo_id_text")
    private String woIdText;

    /**
     * Asset
     */
    @JSONField(name = "asset_id_text")
    @JsonProperty("asset_id_text")
    private String assetIdText;

    /**
     * Task
     */
    @JSONField(name = "task_id_text")
    @JsonProperty("task_id_text")
    private String taskIdText;

    /**
     * 公司
     */
    @JSONField(name = "company_id_text")
    @JsonProperty("company_id_text")
    private String companyIdText;

    /**
     * 请求
     */
    @JSONField(name = "request_id_text")
    @JsonProperty("request_id_text")
    private String requestIdText;

    /**
     * 负责人
     */
    @JSONField(name = "user_id_text")
    @JsonProperty("user_id_text")
    private String userIdText;

    /**
     * 请求
     */
    @DEField(name = "request_id")
    @JSONField(name = "request_id")
    @JsonProperty("request_id")
    private Integer requestId;

    /**
     * Task
     */
    @DEField(name = "task_id")
    @JSONField(name = "task_id")
    @JsonProperty("task_id")
    private Integer taskId;

    /**
     * 工单
     */
    @DEField(name = "wo_id")
    @JSONField(name = "wo_id")
    @JsonProperty("wo_id")
    private Integer woId;

    /**
     * 公司
     */
    @DEField(name = "company_id")
    @JSONField(name = "company_id")
    @JsonProperty("company_id")
    private Integer companyId;

    /**
     * 负责人
     */
    @DEField(name = "user_id")
    @JSONField(name = "user_id")
    @JsonProperty("user_id")
    private Integer userId;

    /**
     * Asset
     */
    @DEField(name = "asset_id")
    @JSONField(name = "asset_id")
    @JsonProperty("asset_id")
    private Integer assetId;

    /**
     * 创建人
     */
    @DEField(name = "create_uid" , preType = DEPredefinedFieldType.CREATEMAN)
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    private Integer createUid;

    /**
     * 最后更新者
     */
    @DEField(name = "write_uid" , preType = DEPredefinedFieldType.UPDATEMAN)
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    private Integer writeUid;


    /**
     * 
     */
    @JSONField(name = "odooasset")
    @JsonProperty("odooasset")
    private cn.ibizlab.odoo.core.odoo_asset.domain.Asset_asset odooAsset;

    /**
     * 
     */
    @JSONField(name = "odoorequest")
    @JsonProperty("odoorequest")
    private cn.ibizlab.odoo.core.odoo_mro.domain.Mro_request odooRequest;

    /**
     * 
     */
    @JSONField(name = "odootask")
    @JsonProperty("odootask")
    private cn.ibizlab.odoo.core.odoo_mro.domain.Mro_task odooTask;

    /**
     * 
     */
    @JSONField(name = "odoowo")
    @JsonProperty("odoowo")
    private cn.ibizlab.odoo.core.odoo_mro.domain.Mro_workorder odooWo;

    /**
     * 
     */
    @JSONField(name = "odoocompany")
    @JsonProperty("odoocompany")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_company odooCompany;

    /**
     * 
     */
    @JSONField(name = "odoocreate")
    @JsonProperty("odoocreate")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_users odooCreate;

    /**
     * 
     */
    @JSONField(name = "odoouser")
    @JsonProperty("odoouser")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_users odooUser;

    /**
     * 
     */
    @JSONField(name = "odoowrite")
    @JsonProperty("odoowrite")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_users odooWrite;




    /**
     * 设置 [保养类型]
     */
    public void setMaintenanceType(String maintenanceType){
        this.maintenanceType = maintenanceType ;
        this.modify("maintenance_type",maintenanceType);
    }
    /**
     * 设置 [Documentation Description]
     */
    public void setDocumentationDescription(String documentationDescription){
        this.documentationDescription = documentationDescription ;
        this.modify("documentation_description",documentationDescription);
    }
    /**
     * 设置 [计划日期]
     */
    public void setDateScheduled(Timestamp dateScheduled){
        this.dateScheduled = dateScheduled ;
        this.modify("date_scheduled",dateScheduled);
    }
    /**
     * 设置 [Labor Description]
     */
    public void setLaborDescription(String laborDescription){
        this.laborDescription = laborDescription ;
        this.modify("labor_description",laborDescription);
    }
    /**
     * 设置 [源文档]
     */
    public void setOrigin(String origin){
        this.origin = origin ;
        this.modify("origin",origin);
    }
    /**
     * 设置 [说明]
     */
    public void setDescription(String description){
        this.description = description ;
        this.modify("description",description);
    }
    /**
     * 设置 [Operations Description]
     */
    public void setOperationsDescription(String operationsDescription){
        this.operationsDescription = operationsDescription ;
        this.modify("operations_description",operationsDescription);
    }
    /**
     * 设置 [Problem Description]
     */
    public void setProblemDescription(String problemDescription){
        this.problemDescription = problemDescription ;
        this.modify("problem_description",problemDescription);
    }
    /**
     * 设置 [Planned Date]
     */
    public void setDatePlanned(Timestamp datePlanned){
        this.datePlanned = datePlanned ;
        this.modify("date_planned",datePlanned);
    }
    /**
     * 设置 [Procurement group]
     */
    public void setProcurementGroupId(Integer procurementGroupId){
        this.procurementGroupId = procurementGroupId ;
        this.modify("procurement_group_id",procurementGroupId);
    }
    /**
     * 设置 [Execution Date]
     */
    public void setDateExecution(Timestamp dateExecution){
        this.dateExecution = dateExecution ;
        this.modify("date_execution",dateExecution);
    }
    /**
     * 设置 [附件]
     */
    public void setMessageMainAttachmentId(Integer messageMainAttachmentId){
        this.messageMainAttachmentId = messageMainAttachmentId ;
        this.modify("message_main_attachment_id",messageMainAttachmentId);
    }
    /**
     * 设置 [编号]
     */
    public void setName(String name){
        this.name = name ;
        this.modify("name",name);
    }
    /**
     * 设置 [状态]
     */
    public void setState(String state){
        this.state = state ;
        this.modify("state",state);
    }
    /**
     * 设置 [Tools Description]
     */
    public void setToolsDescription(String toolsDescription){
        this.toolsDescription = toolsDescription ;
        this.modify("tools_description",toolsDescription);
    }
    /**
     * 设置 [请求]
     */
    public void setRequestId(Integer requestId){
        this.requestId = requestId ;
        this.modify("request_id",requestId);
    }
    /**
     * 设置 [Task]
     */
    public void setTaskId(Integer taskId){
        this.taskId = taskId ;
        this.modify("task_id",taskId);
    }
    /**
     * 设置 [工单]
     */
    public void setWoId(Integer woId){
        this.woId = woId ;
        this.modify("wo_id",woId);
    }
    /**
     * 设置 [公司]
     */
    public void setCompanyId(Integer companyId){
        this.companyId = companyId ;
        this.modify("company_id",companyId);
    }
    /**
     * 设置 [负责人]
     */
    public void setUserId(Integer userId){
        this.userId = userId ;
        this.modify("user_id",userId);
    }
    /**
     * 设置 [Asset]
     */
    public void setAssetId(Integer assetId){
        this.assetId = assetId ;
        this.modify("asset_id",assetId);
    }

}


