package cn.ibizlab.odoo.core.repository.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import cn.ibizlab.odoo.core.repository.client.po.project_task_type;

/**
 * 实体[project_task_type] 服务对象接口
 */
public interface project_task_typeRepository{


    public project_task_type createPO() ;
        public void createBatch(project_task_type project_task_type);

        public void get(String id);

        public List<project_task_type> search();

        public void update(project_task_type project_task_type);

        public void removeBatch(String id);

        public void create(project_task_type project_task_type);

        public void remove(String id);

        public void updateBatch(project_task_type project_task_type);


}
