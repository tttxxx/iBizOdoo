package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Maintenance_equipment;
import cn.ibizlab.odoo.core.odoo_maintenance.filter.Maintenance_equipmentSearchContext;

/**
 * 实体 [保养设备] 存储对象
 */
public interface Maintenance_equipmentRepository extends Repository<Maintenance_equipment> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Maintenance_equipment> searchDefault(Maintenance_equipmentSearchContext context);

    Maintenance_equipment convert2PO(cn.ibizlab.odoo.core.odoo_maintenance.domain.Maintenance_equipment domain , Maintenance_equipment po) ;

    cn.ibizlab.odoo.core.odoo_maintenance.domain.Maintenance_equipment convert2Domain( Maintenance_equipment po ,cn.ibizlab.odoo.core.odoo_maintenance.domain.Maintenance_equipment domain) ;

}
