package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Account_tax_template;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_tax_templateSearchContext;

/**
 * 实体 [税率模板] 存储对象
 */
public interface Account_tax_templateRepository extends Repository<Account_tax_template> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Account_tax_template> searchDefault(Account_tax_templateSearchContext context);

    Account_tax_template convert2PO(cn.ibizlab.odoo.core.odoo_account.domain.Account_tax_template domain , Account_tax_template po) ;

    cn.ibizlab.odoo.core.odoo_account.domain.Account_tax_template convert2Domain( Account_tax_template po ,cn.ibizlab.odoo.core.odoo_account.domain.Account_tax_template domain) ;

}
