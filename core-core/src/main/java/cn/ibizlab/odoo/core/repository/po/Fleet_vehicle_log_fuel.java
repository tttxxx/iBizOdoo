package cn.ibizlab.odoo.core.repository.po ;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.math.BigInteger;

import cn.ibizlab.odoo.core.odoo_fleet.filter.Fleet_vehicle_log_fuelSearchContext;

/**
 * 实体 [车辆燃油记录] 存储模型
 */
public interface Fleet_vehicle_log_fuel{

    /**
     * ID
     */
    Integer getId();

    void setId(Integer id);

    /**
     * 获取 [ID]脏标记
     */
    boolean getIdDirtyFlag();

    /**
     * 每公升价格
     */
    Double getPrice_per_liter();

    void setPrice_per_liter(Double price_per_liter);

    /**
     * 获取 [每公升价格]脏标记
     */
    boolean getPrice_per_literDirtyFlag();

    /**
     * 包括服务
     */
    String getCost_ids();

    void setCost_ids(String cost_ids);

    /**
     * 获取 [包括服务]脏标记
     */
    boolean getCost_idsDirtyFlag();

    /**
     * 显示名称
     */
    String getDisplay_name();

    void setDisplay_name(String display_name);

    /**
     * 获取 [显示名称]脏标记
     */
    boolean getDisplay_nameDirtyFlag();

    /**
     * 最后更新时间
     */
    Timestamp getWrite_date();

    void setWrite_date(Timestamp write_date);

    /**
     * 获取 [最后更新时间]脏标记
     */
    boolean getWrite_dateDirtyFlag();

    /**
     * 升
     */
    Double getLiter();

    void setLiter(Double liter);

    /**
     * 获取 [升]脏标记
     */
    boolean getLiterDirtyFlag();

    /**
     * 最后修改日
     */
    Timestamp get__last_update();

    void set__last_update(Timestamp __last_update);

    /**
     * 获取 [最后修改日]脏标记
     */
    boolean get__last_updateDirtyFlag();

    /**
     * 便签
     */
    String getNotes();

    void setNotes(String notes);

    /**
     * 获取 [便签]脏标记
     */
    boolean getNotesDirtyFlag();

    /**
     * 创建时间
     */
    Timestamp getCreate_date();

    void setCreate_date(Timestamp create_date);

    /**
     * 获取 [创建时间]脏标记
     */
    boolean getCreate_dateDirtyFlag();

    /**
     * 发票参考
     */
    String getInv_ref();

    void setInv_ref(String inv_ref);

    /**
     * 获取 [发票参考]脏标记
     */
    boolean getInv_refDirtyFlag();

    /**
     * 类型
     */
    Integer getCost_subtype_id();

    void setCost_subtype_id(Integer cost_subtype_id);

    /**
     * 获取 [类型]脏标记
     */
    boolean getCost_subtype_idDirtyFlag();

    /**
     * 创建人
     */
    String getCreate_uid_text();

    void setCreate_uid_text(String create_uid_text);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uid_textDirtyFlag();

    /**
     * 里程表
     */
    Integer getOdometer_id();

    void setOdometer_id(Integer odometer_id);

    /**
     * 获取 [里程表]脏标记
     */
    boolean getOdometer_idDirtyFlag();

    /**
     * 费用所属类别
     */
    String getCost_type();

    void setCost_type(String cost_type);

    /**
     * 获取 [费用所属类别]脏标记
     */
    boolean getCost_typeDirtyFlag();

    /**
     * 总价
     */
    Double getAmount();

    void setAmount(Double amount);

    /**
     * 获取 [总价]脏标记
     */
    boolean getAmountDirtyFlag();

    /**
     * 采购
     */
    String getPurchaser_id_text();

    void setPurchaser_id_text(String purchaser_id_text);

    /**
     * 获取 [采购]脏标记
     */
    boolean getPurchaser_id_textDirtyFlag();

    /**
     * 最后更新人
     */
    String getWrite_uid_text();

    void setWrite_uid_text(String write_uid_text);

    /**
     * 获取 [最后更新人]脏标记
     */
    boolean getWrite_uid_textDirtyFlag();

    /**
     * 总额
     */
    Double getCost_amount();

    void setCost_amount(Double cost_amount);

    /**
     * 获取 [总额]脏标记
     */
    boolean getCost_amountDirtyFlag();

    /**
     * 上级
     */
    Integer getParent_id();

    void setParent_id(Integer parent_id);

    /**
     * 获取 [上级]脏标记
     */
    boolean getParent_idDirtyFlag();

    /**
     * 车辆
     */
    Integer getVehicle_id();

    void setVehicle_id(Integer vehicle_id);

    /**
     * 获取 [车辆]脏标记
     */
    boolean getVehicle_idDirtyFlag();

    /**
     * 自动生成
     */
    String getAuto_generated();

    void setAuto_generated(String auto_generated);

    /**
     * 获取 [自动生成]脏标记
     */
    boolean getAuto_generatedDirtyFlag();

    /**
     * 合同
     */
    Integer getContract_id();

    void setContract_id(Integer contract_id);

    /**
     * 获取 [合同]脏标记
     */
    boolean getContract_idDirtyFlag();

    /**
     * 成本说明
     */
    String getDescription();

    void setDescription(String description);

    /**
     * 获取 [成本说明]脏标记
     */
    boolean getDescriptionDirtyFlag();

    /**
     * 名称
     */
    String getName();

    void setName(String name);

    /**
     * 获取 [名称]脏标记
     */
    boolean getNameDirtyFlag();

    /**
     * 供应商
     */
    String getVendor_id_text();

    void setVendor_id_text(String vendor_id_text);

    /**
     * 获取 [供应商]脏标记
     */
    boolean getVendor_id_textDirtyFlag();

    /**
     * 里程表数值
     */
    Double getOdometer();

    void setOdometer(Double odometer);

    /**
     * 获取 [里程表数值]脏标记
     */
    boolean getOdometerDirtyFlag();

    /**
     * 日期
     */
    Timestamp getDate();

    void setDate(Timestamp date);

    /**
     * 获取 [日期]脏标记
     */
    boolean getDateDirtyFlag();

    /**
     * 单位
     */
    String getOdometer_unit();

    void setOdometer_unit(String odometer_unit);

    /**
     * 获取 [单位]脏标记
     */
    boolean getOdometer_unitDirtyFlag();

    /**
     * 最后更新人
     */
    Integer getWrite_uid();

    void setWrite_uid(Integer write_uid);

    /**
     * 获取 [最后更新人]脏标记
     */
    boolean getWrite_uidDirtyFlag();

    /**
     * 成本
     */
    Integer getCost_id();

    void setCost_id(Integer cost_id);

    /**
     * 获取 [成本]脏标记
     */
    boolean getCost_idDirtyFlag();

    /**
     * 创建人
     */
    Integer getCreate_uid();

    void setCreate_uid(Integer create_uid);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uidDirtyFlag();

    /**
     * 供应商
     */
    Integer getVendor_id();

    void setVendor_id(Integer vendor_id);

    /**
     * 获取 [供应商]脏标记
     */
    boolean getVendor_idDirtyFlag();

    /**
     * 采购
     */
    Integer getPurchaser_id();

    void setPurchaser_id(Integer purchaser_id);

    /**
     * 获取 [采购]脏标记
     */
    boolean getPurchaser_idDirtyFlag();

}
