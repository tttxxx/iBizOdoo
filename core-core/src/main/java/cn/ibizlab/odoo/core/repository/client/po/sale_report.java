package cn.ibizlab.odoo.core.repository.client.po;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigDecimal;

/**
 * [sale_report] 对象
 */
public interface sale_report {

    public Integer getAnalytic_account_id();

    public void setAnalytic_account_id(Integer analytic_account_id);

    public String getAnalytic_account_id_text();

    public void setAnalytic_account_id_text(String analytic_account_id_text);

    public Integer getCampaign_id();

    public void setCampaign_id(Integer campaign_id);

    public String getCampaign_id_text();

    public void setCampaign_id_text(String campaign_id_text);

    public Integer getCateg_id();

    public void setCateg_id(Integer categ_id);

    public String getCateg_id_text();

    public void setCateg_id_text(String categ_id_text);

    public Integer getCommercial_partner_id();

    public void setCommercial_partner_id(Integer commercial_partner_id);

    public String getCommercial_partner_id_text();

    public void setCommercial_partner_id_text(String commercial_partner_id_text);

    public Integer getCompany_id();

    public void setCompany_id(Integer company_id);

    public String getCompany_id_text();

    public void setCompany_id_text(String company_id_text);

    public Timestamp getConfirmation_date();

    public void setConfirmation_date(Timestamp confirmation_date);

    public Integer getCountry_id();

    public void setCountry_id(Integer country_id);

    public String getCountry_id_text();

    public void setCountry_id_text(String country_id_text);

    public Timestamp getDate();

    public void setDate(Timestamp date);

    public Double getDiscount();

    public void setDiscount(Double discount);

    public Double getDiscount_amount();

    public void setDiscount_amount(Double discount_amount);

    public String getDisplay_name();

    public void setDisplay_name(String display_name);

    public Integer getId();

    public void setId(Integer id);

    public Integer getMedium_id();

    public void setMedium_id(Integer medium_id);

    public String getMedium_id_text();

    public void setMedium_id_text(String medium_id_text);

    public String getName();

    public void setName(String name);

    public Integer getNbr();

    public void setNbr(Integer nbr);

    public Integer getOrder_id();

    public void setOrder_id(Integer order_id);

    public String getOrder_id_text();

    public void setOrder_id_text(String order_id_text);

    public Integer getPartner_id();

    public void setPartner_id(Integer partner_id);

    public String getPartner_id_text();

    public void setPartner_id_text(String partner_id_text);

    public Integer getPricelist_id();

    public void setPricelist_id(Integer pricelist_id);

    public String getPricelist_id_text();

    public void setPricelist_id_text(String pricelist_id_text);

    public Double getPrice_subtotal();

    public void setPrice_subtotal(Double price_subtotal);

    public Double getPrice_total();

    public void setPrice_total(Double price_total);

    public Integer getProduct_id();

    public void setProduct_id(Integer product_id);

    public String getProduct_id_text();

    public void setProduct_id_text(String product_id_text);

    public Integer getProduct_tmpl_id();

    public void setProduct_tmpl_id(Integer product_tmpl_id);

    public String getProduct_tmpl_id_text();

    public void setProduct_tmpl_id_text(String product_tmpl_id_text);

    public Integer getProduct_uom();

    public void setProduct_uom(Integer product_uom);

    public Double getProduct_uom_qty();

    public void setProduct_uom_qty(Double product_uom_qty);

    public String getProduct_uom_text();

    public void setProduct_uom_text(String product_uom_text);

    public Double getQty_delivered();

    public void setQty_delivered(Double qty_delivered);

    public Double getQty_invoiced();

    public void setQty_invoiced(Double qty_invoiced);

    public Double getQty_to_invoice();

    public void setQty_to_invoice(Double qty_to_invoice);

    public Integer getSource_id();

    public void setSource_id(Integer source_id);

    public String getSource_id_text();

    public void setSource_id_text(String source_id_text);

    public String getState();

    public void setState(String state);

    public Integer getTeam_id();

    public void setTeam_id(Integer team_id);

    public String getTeam_id_text();

    public void setTeam_id_text(String team_id_text);

    public Double getUntaxed_amount_invoiced();

    public void setUntaxed_amount_invoiced(Double untaxed_amount_invoiced);

    public Double getUntaxed_amount_to_invoice();

    public void setUntaxed_amount_to_invoice(Double untaxed_amount_to_invoice);

    public Integer getUser_id();

    public void setUser_id(Integer user_id);

    public String getUser_id_text();

    public void setUser_id_text(String user_id_text);

    public Double getVolume();

    public void setVolume(Double volume);

    public Integer getWarehouse_id();

    public void setWarehouse_id(Integer warehouse_id);

    public String getWarehouse_id_text();

    public void setWarehouse_id_text(String warehouse_id_text);

    public Double getWeight();

    public void setWeight(Double weight);

    public Timestamp get__last_update();

    public void set__last_update(Timestamp __last_update);

}
