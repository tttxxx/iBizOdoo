package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Fleet_vehicle_model_brand;
import cn.ibizlab.odoo.core.odoo_fleet.filter.Fleet_vehicle_model_brandSearchContext;

/**
 * 实体 [车辆品牌] 存储对象
 */
public interface Fleet_vehicle_model_brandRepository extends Repository<Fleet_vehicle_model_brand> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Fleet_vehicle_model_brand> searchDefault(Fleet_vehicle_model_brandSearchContext context);

    Fleet_vehicle_model_brand convert2PO(cn.ibizlab.odoo.core.odoo_fleet.domain.Fleet_vehicle_model_brand domain , Fleet_vehicle_model_brand po) ;

    cn.ibizlab.odoo.core.odoo_fleet.domain.Fleet_vehicle_model_brand convert2Domain( Fleet_vehicle_model_brand po ,cn.ibizlab.odoo.core.odoo_fleet.domain.Fleet_vehicle_model_brand domain) ;

}
