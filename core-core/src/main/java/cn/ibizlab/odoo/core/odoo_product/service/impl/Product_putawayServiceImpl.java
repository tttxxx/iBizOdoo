package cn.ibizlab.odoo.core.odoo_product.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_product.domain.Product_putaway;
import cn.ibizlab.odoo.core.odoo_product.filter.Product_putawaySearchContext;
import cn.ibizlab.odoo.core.odoo_product.service.IProduct_putawayService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;



import cn.ibizlab.odoo.core.odoo_product.client.product_putawayOdooClient;
import cn.ibizlab.odoo.core.odoo_product.clientmodel.product_putawayClientModel;

/**
 * 实体[上架策略] 服务对象接口实现
 */
@Slf4j
@Service
public class Product_putawayServiceImpl implements IProduct_putawayService {

    @Autowired
    product_putawayOdooClient product_putawayOdooClient;


    @Override
    public boolean remove(Integer id) {
        product_putawayClientModel clientModel = new product_putawayClientModel();
        clientModel.setId(id);
		product_putawayOdooClient.remove(clientModel);
        return true; 
    }

    public void removeBatch(Collection<Integer> idList){
    }

    @Override
    public boolean create(Product_putaway et) {
        product_putawayClientModel clientModel = convert2Model(et,null);
		product_putawayOdooClient.create(clientModel);
        Product_putaway rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Product_putaway> list){
    }

    @Override
    public Product_putaway get(Integer id) {
        product_putawayClientModel clientModel = new product_putawayClientModel();
        clientModel.setId(id);
		product_putawayOdooClient.get(clientModel);
        Product_putaway et = convert2Domain(clientModel,null);
        if(et==null){
            et=new Product_putaway();
            et.setId(id);
        }
        else{
        }

        return  et;
    }

    @Override
    public boolean update(Product_putaway et) {
        product_putawayClientModel clientModel = convert2Model(et,null);
		product_putawayOdooClient.update(clientModel);
        Product_putaway rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }
    
    public void updateBatch(List<Product_putaway> list){
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Product_putaway> searchDefault(Product_putawaySearchContext context) {
        List<Product_putaway> list = new ArrayList<Product_putaway>();
        Page<product_putawayClientModel> clientModelList = product_putawayOdooClient.search(context);
        for(product_putawayClientModel clientModel : clientModelList){
            list.add( this.convert2Domain(clientModel,null));
        }
        return new PageImpl<Product_putaway>(list, context.getPageable(), clientModelList.getTotalElements());
    }

    public product_putawayClientModel convert2Model(Product_putaway domain , product_putawayClientModel model) {
        if(domain == null)
            return model ;
        if(model==null){
            model = new product_putawayClientModel();

        }
        if((Boolean) domain.getExtensionparams().get("dirtyflagenable")){
            if((Boolean) domain.getExtensionparams().get("product_location_idsdirtyflag"))
                model.setProduct_location_ids(domain.getProductLocationIds());
            if((Boolean) domain.getExtensionparams().get("namedirtyflag"))
                model.setName(domain.getName());
            if((Boolean) domain.getExtensionparams().get("iddirtyflag"))
                model.setId(domain.getId());
            if((Boolean) domain.getExtensionparams().get("create_datedirtyflag"))
                model.setCreate_date(domain.getCreateDate());
            if((Boolean) domain.getExtensionparams().get("fixed_location_idsdirtyflag"))
                model.setFixed_location_ids(domain.getFixedLocationIds());
            if((Boolean) domain.getExtensionparams().get("__last_updatedirtyflag"))
                model.set__last_update(domain.getLastUpdate());
            if((Boolean) domain.getExtensionparams().get("write_datedirtyflag"))
                model.setWrite_date(domain.getWriteDate());
            if((Boolean) domain.getExtensionparams().get("display_namedirtyflag"))
                model.setDisplay_name(domain.getDisplayName());
            if((Boolean) domain.getExtensionparams().get("write_uid_textdirtyflag"))
                model.setWrite_uid_text(domain.getWriteUidText());
            if((Boolean) domain.getExtensionparams().get("create_uid_textdirtyflag"))
                model.setCreate_uid_text(domain.getCreateUidText());
            if((Boolean) domain.getExtensionparams().get("create_uiddirtyflag"))
                model.setCreate_uid(domain.getCreateUid());
            if((Boolean) domain.getExtensionparams().get("write_uiddirtyflag"))
                model.setWrite_uid(domain.getWriteUid());
            domain.getFocusNull().clear();
        }
        return model ;
    }

    public Product_putaway convert2Domain( product_putawayClientModel model ,Product_putaway domain) {
        if(model == null)
            return domain ;
        if(domain == null){
            domain = new Product_putaway();
        }

        if(model.getProduct_location_idsDirtyFlag())
            domain.setProductLocationIds(model.getProduct_location_ids());
        if(model.getNameDirtyFlag())
            domain.setName(model.getName());
        if(model.getIdDirtyFlag())
            domain.setId(model.getId());
        if(model.getCreate_dateDirtyFlag())
            domain.setCreateDate(model.getCreate_date());
        if(model.getFixed_location_idsDirtyFlag())
            domain.setFixedLocationIds(model.getFixed_location_ids());
        if(model.get__last_updateDirtyFlag())
            domain.setLastUpdate(model.get__last_update());
        if(model.getWrite_dateDirtyFlag())
            domain.setWriteDate(model.getWrite_date());
        if(model.getDisplay_nameDirtyFlag())
            domain.setDisplayName(model.getDisplay_name());
        if(model.getWrite_uid_textDirtyFlag())
            domain.setWriteUidText(model.getWrite_uid_text());
        if(model.getCreate_uid_textDirtyFlag())
            domain.setCreateUidText(model.getCreate_uid_text());
        if(model.getCreate_uidDirtyFlag())
            domain.setCreateUid(model.getCreate_uid());
        if(model.getWrite_uidDirtyFlag())
            domain.setWriteUid(model.getWrite_uid());
        return domain ;
    }

}

    



