package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Mro_task_parts_line;
import cn.ibizlab.odoo.core.odoo_mro.filter.Mro_task_parts_lineSearchContext;

/**
 * 实体 [Maintenance Planned Parts] 存储对象
 */
public interface Mro_task_parts_lineRepository extends Repository<Mro_task_parts_line> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Mro_task_parts_line> searchDefault(Mro_task_parts_lineSearchContext context);

    Mro_task_parts_line convert2PO(cn.ibizlab.odoo.core.odoo_mro.domain.Mro_task_parts_line domain , Mro_task_parts_line po) ;

    cn.ibizlab.odoo.core.odoo_mro.domain.Mro_task_parts_line convert2Domain( Mro_task_parts_line po ,cn.ibizlab.odoo.core.odoo_mro.domain.Mro_task_parts_line domain) ;

}
