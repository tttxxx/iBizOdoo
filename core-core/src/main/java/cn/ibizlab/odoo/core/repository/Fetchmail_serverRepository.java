package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Fetchmail_server;
import cn.ibizlab.odoo.core.odoo_fetchmail.filter.Fetchmail_serverSearchContext;

/**
 * 实体 [Incoming Mail Server] 存储对象
 */
public interface Fetchmail_serverRepository extends Repository<Fetchmail_server> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Fetchmail_server> searchDefault(Fetchmail_serverSearchContext context);

    Fetchmail_server convert2PO(cn.ibizlab.odoo.core.odoo_fetchmail.domain.Fetchmail_server domain , Fetchmail_server po) ;

    cn.ibizlab.odoo.core.odoo_fetchmail.domain.Fetchmail_server convert2Domain( Fetchmail_server po ,cn.ibizlab.odoo.core.odoo_fetchmail.domain.Fetchmail_server domain) ;

}
