package cn.ibizlab.odoo.core.odoo_base.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_base.domain.Res_country_state;
import cn.ibizlab.odoo.core.odoo_base.filter.Res_country_stateSearchContext;
import cn.ibizlab.odoo.core.odoo_base.service.IRes_country_stateService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;



import cn.ibizlab.odoo.core.odoo_base.client.res_country_stateOdooClient;
import cn.ibizlab.odoo.core.odoo_base.clientmodel.res_country_stateClientModel;

/**
 * 实体[国家/地区州/省] 服务对象接口实现
 */
@Slf4j
@Service
public class Res_country_stateServiceImpl implements IRes_country_stateService {

    @Autowired
    res_country_stateOdooClient res_country_stateOdooClient;


    @Override
    public Res_country_state get(Integer id) {
        res_country_stateClientModel clientModel = new res_country_stateClientModel();
        clientModel.setId(id);
		res_country_stateOdooClient.get(clientModel);
        Res_country_state et = convert2Domain(clientModel,null);
        if(et==null){
            et=new Res_country_state();
            et.setId(id);
        }
        else{
        }

        return  et;
    }

    @Override
    public boolean update(Res_country_state et) {
        res_country_stateClientModel clientModel = convert2Model(et,null);
		res_country_stateOdooClient.update(clientModel);
        Res_country_state rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }
    
    public void updateBatch(List<Res_country_state> list){
    }

    @Override
    public boolean create(Res_country_state et) {
        res_country_stateClientModel clientModel = convert2Model(et,null);
		res_country_stateOdooClient.create(clientModel);
        Res_country_state rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Res_country_state> list){
    }

    @Override
    public boolean remove(Integer id) {
        res_country_stateClientModel clientModel = new res_country_stateClientModel();
        clientModel.setId(id);
		res_country_stateOdooClient.remove(clientModel);
        return true; 
    }

    public void removeBatch(Collection<Integer> idList){
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Res_country_state> searchDefault(Res_country_stateSearchContext context) {
        List<Res_country_state> list = new ArrayList<Res_country_state>();
        Page<res_country_stateClientModel> clientModelList = res_country_stateOdooClient.search(context);
        for(res_country_stateClientModel clientModel : clientModelList){
            list.add( this.convert2Domain(clientModel,null));
        }
        return new PageImpl<Res_country_state>(list, context.getPageable(), clientModelList.getTotalElements());
    }

    public res_country_stateClientModel convert2Model(Res_country_state domain , res_country_stateClientModel model) {
        if(domain == null)
            return model ;
        if(model==null){
            model = new res_country_stateClientModel();

        }
        if((Boolean) domain.getExtensionparams().get("dirtyflagenable")){
            if((Boolean) domain.getExtensionparams().get("__last_updatedirtyflag"))
                model.set__last_update(domain.getLastUpdate());
            if((Boolean) domain.getExtensionparams().get("iddirtyflag"))
                model.setId(domain.getId());
            if((Boolean) domain.getExtensionparams().get("namedirtyflag"))
                model.setName(domain.getName());
            if((Boolean) domain.getExtensionparams().get("codedirtyflag"))
                model.setCode(domain.getCode());
            if((Boolean) domain.getExtensionparams().get("create_datedirtyflag"))
                model.setCreate_date(domain.getCreateDate());
            if((Boolean) domain.getExtensionparams().get("write_datedirtyflag"))
                model.setWrite_date(domain.getWriteDate());
            if((Boolean) domain.getExtensionparams().get("display_namedirtyflag"))
                model.setDisplay_name(domain.getDisplayName());
            if((Boolean) domain.getExtensionparams().get("country_id_textdirtyflag"))
                model.setCountry_id_text(domain.getCountryIdText());
            if((Boolean) domain.getExtensionparams().get("create_uid_textdirtyflag"))
                model.setCreate_uid_text(domain.getCreateUidText());
            if((Boolean) domain.getExtensionparams().get("write_uid_textdirtyflag"))
                model.setWrite_uid_text(domain.getWriteUidText());
            if((Boolean) domain.getExtensionparams().get("write_uiddirtyflag"))
                model.setWrite_uid(domain.getWriteUid());
            if((Boolean) domain.getExtensionparams().get("create_uiddirtyflag"))
                model.setCreate_uid(domain.getCreateUid());
            if((Boolean) domain.getExtensionparams().get("country_iddirtyflag"))
                model.setCountry_id(domain.getCountryId());
            domain.getFocusNull().clear();
        }
        return model ;
    }

    public Res_country_state convert2Domain( res_country_stateClientModel model ,Res_country_state domain) {
        if(model == null)
            return domain ;
        if(domain == null){
            domain = new Res_country_state();
        }

        if(model.get__last_updateDirtyFlag())
            domain.setLastUpdate(model.get__last_update());
        if(model.getIdDirtyFlag())
            domain.setId(model.getId());
        if(model.getNameDirtyFlag())
            domain.setName(model.getName());
        if(model.getCodeDirtyFlag())
            domain.setCode(model.getCode());
        if(model.getCreate_dateDirtyFlag())
            domain.setCreateDate(model.getCreate_date());
        if(model.getWrite_dateDirtyFlag())
            domain.setWriteDate(model.getWrite_date());
        if(model.getDisplay_nameDirtyFlag())
            domain.setDisplayName(model.getDisplay_name());
        if(model.getCountry_id_textDirtyFlag())
            domain.setCountryIdText(model.getCountry_id_text());
        if(model.getCreate_uid_textDirtyFlag())
            domain.setCreateUidText(model.getCreate_uid_text());
        if(model.getWrite_uid_textDirtyFlag())
            domain.setWriteUidText(model.getWrite_uid_text());
        if(model.getWrite_uidDirtyFlag())
            domain.setWriteUid(model.getWrite_uid());
        if(model.getCreate_uidDirtyFlag())
            domain.setCreateUid(model.getCreate_uid());
        if(model.getCountry_idDirtyFlag())
            domain.setCountryId(model.getCountry_id());
        return domain ;
    }

}

    



