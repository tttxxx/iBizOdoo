package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Ihr_applicant;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[hr_applicant] 服务对象接口
 */
public interface Ihr_applicantClientService{

    public Ihr_applicant createModel() ;

    public void get(Ihr_applicant hr_applicant);

    public void createBatch(List<Ihr_applicant> hr_applicants);

    public void create(Ihr_applicant hr_applicant);

    public void remove(Ihr_applicant hr_applicant);

    public Page<Ihr_applicant> search(SearchContext context);

    public void updateBatch(List<Ihr_applicant> hr_applicants);

    public void update(Ihr_applicant hr_applicant);

    public void removeBatch(List<Ihr_applicant> hr_applicants);

    public Page<Ihr_applicant> select(SearchContext context);

}
