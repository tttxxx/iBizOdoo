package cn.ibizlab.odoo.core.repository.client.po;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigDecimal;

/**
 * [rating_rating] 对象
 */
public interface rating_rating {

    public String getAccess_token();

    public void setAccess_token(String access_token);

    public String getConsumed();

    public void setConsumed(String consumed);

    public Timestamp getCreate_date();

    public void setCreate_date(Timestamp create_date);

    public Integer getCreate_uid();

    public void setCreate_uid(Integer create_uid);

    public String getCreate_uid_text();

    public void setCreate_uid_text(String create_uid_text);

    public String getDisplay_name();

    public void setDisplay_name(String display_name);

    public String getFeedback();

    public void setFeedback(String feedback);

    public Integer getId();

    public void setId(Integer id);

    public Integer getMessage_id();

    public void setMessage_id(Integer message_id);

    public Integer getParent_res_id();

    public void setParent_res_id(Integer parent_res_id);

    public String getParent_res_model();

    public void setParent_res_model(String parent_res_model);

    public String getParent_res_name();

    public void setParent_res_name(String parent_res_name);

    public Integer getPartner_id();

    public void setPartner_id(Integer partner_id);

    public String getPartner_id_text();

    public void setPartner_id_text(String partner_id_text);

    public Integer getRated_partner_id();

    public void setRated_partner_id(Integer rated_partner_id);

    public String getRated_partner_id_text();

    public void setRated_partner_id_text(String rated_partner_id_text);

    public Double getRating();

    public void setRating(Double rating);

    public byte[] getRating_image();

    public void setRating_image(byte[] rating_image);

    public String getRating_text();

    public void setRating_text(String rating_text);

    public Integer getRes_id();

    public void setRes_id(Integer res_id);

    public String getRes_model();

    public void setRes_model(String res_model);

    public String getRes_name();

    public void setRes_name(String res_name);

    public String getWebsite_published();

    public void setWebsite_published(String website_published);

    public Timestamp getWrite_date();

    public void setWrite_date(Timestamp write_date);

    public Integer getWrite_uid();

    public void setWrite_uid(Integer write_uid);

    public String getWrite_uid_text();

    public void setWrite_uid_text(String write_uid_text);

    public Timestamp get__last_update();

    public void set__last_update(Timestamp __last_update);

}
