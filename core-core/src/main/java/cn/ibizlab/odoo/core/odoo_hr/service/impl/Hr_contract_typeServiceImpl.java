package cn.ibizlab.odoo.core.odoo_hr.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_hr.domain.Hr_contract_type;
import cn.ibizlab.odoo.core.odoo_hr.filter.Hr_contract_typeSearchContext;
import cn.ibizlab.odoo.core.odoo_hr.service.IHr_contract_typeService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;



import cn.ibizlab.odoo.core.odoo_hr.client.hr_contract_typeOdooClient;
import cn.ibizlab.odoo.core.odoo_hr.clientmodel.hr_contract_typeClientModel;

/**
 * 实体[合同类型] 服务对象接口实现
 */
@Slf4j
@Service
public class Hr_contract_typeServiceImpl implements IHr_contract_typeService {

    @Autowired
    hr_contract_typeOdooClient hr_contract_typeOdooClient;


    @Override
    public boolean create(Hr_contract_type et) {
        hr_contract_typeClientModel clientModel = convert2Model(et,null);
		hr_contract_typeOdooClient.create(clientModel);
        Hr_contract_type rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Hr_contract_type> list){
    }

    @Override
    public boolean remove(Integer id) {
        hr_contract_typeClientModel clientModel = new hr_contract_typeClientModel();
        clientModel.setId(id);
		hr_contract_typeOdooClient.remove(clientModel);
        return true; 
    }

    public void removeBatch(Collection<Integer> idList){
    }

    @Override
    public Hr_contract_type get(Integer id) {
        hr_contract_typeClientModel clientModel = new hr_contract_typeClientModel();
        clientModel.setId(id);
		hr_contract_typeOdooClient.get(clientModel);
        Hr_contract_type et = convert2Domain(clientModel,null);
        if(et==null){
            et=new Hr_contract_type();
            et.setId(id);
        }
        else{
        }

        return  et;
    }

    @Override
    public boolean update(Hr_contract_type et) {
        hr_contract_typeClientModel clientModel = convert2Model(et,null);
		hr_contract_typeOdooClient.update(clientModel);
        Hr_contract_type rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }
    
    public void updateBatch(List<Hr_contract_type> list){
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Hr_contract_type> searchDefault(Hr_contract_typeSearchContext context) {
        List<Hr_contract_type> list = new ArrayList<Hr_contract_type>();
        Page<hr_contract_typeClientModel> clientModelList = hr_contract_typeOdooClient.search(context);
        for(hr_contract_typeClientModel clientModel : clientModelList){
            list.add( this.convert2Domain(clientModel,null));
        }
        return new PageImpl<Hr_contract_type>(list, context.getPageable(), clientModelList.getTotalElements());
    }

    public hr_contract_typeClientModel convert2Model(Hr_contract_type domain , hr_contract_typeClientModel model) {
        if(domain == null)
            return model ;
        if(model==null){
            model = new hr_contract_typeClientModel();

        }
        if((Boolean) domain.getExtensionparams().get("dirtyflagenable")){
            if((Boolean) domain.getExtensionparams().get("sequencedirtyflag"))
                model.setSequence(domain.getSequence());
            if((Boolean) domain.getExtensionparams().get("display_namedirtyflag"))
                model.setDisplay_name(domain.getDisplayName());
            if((Boolean) domain.getExtensionparams().get("__last_updatedirtyflag"))
                model.set__last_update(domain.getLastUpdate());
            if((Boolean) domain.getExtensionparams().get("create_datedirtyflag"))
                model.setCreate_date(domain.getCreateDate());
            if((Boolean) domain.getExtensionparams().get("namedirtyflag"))
                model.setName(domain.getName());
            if((Boolean) domain.getExtensionparams().get("write_datedirtyflag"))
                model.setWrite_date(domain.getWriteDate());
            if((Boolean) domain.getExtensionparams().get("iddirtyflag"))
                model.setId(domain.getId());
            if((Boolean) domain.getExtensionparams().get("write_uid_textdirtyflag"))
                model.setWrite_uid_text(domain.getWriteUidText());
            if((Boolean) domain.getExtensionparams().get("create_uid_textdirtyflag"))
                model.setCreate_uid_text(domain.getCreateUidText());
            if((Boolean) domain.getExtensionparams().get("write_uiddirtyflag"))
                model.setWrite_uid(domain.getWriteUid());
            if((Boolean) domain.getExtensionparams().get("create_uiddirtyflag"))
                model.setCreate_uid(domain.getCreateUid());
            domain.getFocusNull().clear();
        }
        return model ;
    }

    public Hr_contract_type convert2Domain( hr_contract_typeClientModel model ,Hr_contract_type domain) {
        if(model == null)
            return domain ;
        if(domain == null){
            domain = new Hr_contract_type();
        }

        if(model.getSequenceDirtyFlag())
            domain.setSequence(model.getSequence());
        if(model.getDisplay_nameDirtyFlag())
            domain.setDisplayName(model.getDisplay_name());
        if(model.get__last_updateDirtyFlag())
            domain.setLastUpdate(model.get__last_update());
        if(model.getCreate_dateDirtyFlag())
            domain.setCreateDate(model.getCreate_date());
        if(model.getNameDirtyFlag())
            domain.setName(model.getName());
        if(model.getWrite_dateDirtyFlag())
            domain.setWriteDate(model.getWrite_date());
        if(model.getIdDirtyFlag())
            domain.setId(model.getId());
        if(model.getWrite_uid_textDirtyFlag())
            domain.setWriteUidText(model.getWrite_uid_text());
        if(model.getCreate_uid_textDirtyFlag())
            domain.setCreateUidText(model.getCreate_uid_text());
        if(model.getWrite_uidDirtyFlag())
            domain.setWriteUid(model.getWrite_uid());
        if(model.getCreate_uidDirtyFlag())
            domain.setCreateUid(model.getCreate_uid());
        return domain ;
    }

}

    



