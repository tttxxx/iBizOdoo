package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Mro_task;
import cn.ibizlab.odoo.core.odoo_mro.filter.Mro_taskSearchContext;

/**
 * 实体 [Maintenance Task] 存储对象
 */
public interface Mro_taskRepository extends Repository<Mro_task> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Mro_task> searchDefault(Mro_taskSearchContext context);

    Mro_task convert2PO(cn.ibizlab.odoo.core.odoo_mro.domain.Mro_task domain , Mro_task po) ;

    cn.ibizlab.odoo.core.odoo_mro.domain.Mro_task convert2Domain( Mro_task po ,cn.ibizlab.odoo.core.odoo_mro.domain.Mro_task domain) ;

}
