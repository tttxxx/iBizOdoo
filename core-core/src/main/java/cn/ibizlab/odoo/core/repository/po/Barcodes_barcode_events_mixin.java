package cn.ibizlab.odoo.core.repository.po ;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.math.BigInteger;

import cn.ibizlab.odoo.core.odoo_barcodes.filter.Barcodes_barcode_events_mixinSearchContext;

/**
 * 实体 [条形码事件混合] 存储模型
 */
public interface Barcodes_barcode_events_mixin{

    /**
     * ID
     */
    Integer getId();

    void setId(Integer id);

    /**
     * 获取 [ID]脏标记
     */
    boolean getIdDirtyFlag();

    /**
     * 显示名称
     */
    String getDisplay_name();

    void setDisplay_name(String display_name);

    /**
     * 获取 [显示名称]脏标记
     */
    boolean getDisplay_nameDirtyFlag();

    /**
     * 最后修改时间
     */
    Timestamp get__last_update();

    void set__last_update(Timestamp __last_update);

    /**
     * 获取 [最后修改时间]脏标记
     */
    boolean get__last_updateDirtyFlag();

    /**
     * 条码扫描到
     */
    String get_barcode_scanned();

    void set_barcode_scanned(String _barcode_scanned);

    /**
     * 获取 [条码扫描到]脏标记
     */
    boolean get_barcode_scannedDirtyFlag();

}
