package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Base_module_upgrade;
import cn.ibizlab.odoo.core.odoo_base.filter.Base_module_upgradeSearchContext;

/**
 * 实体 [升级模块] 存储对象
 */
public interface Base_module_upgradeRepository extends Repository<Base_module_upgrade> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Base_module_upgrade> searchDefault(Base_module_upgradeSearchContext context);

    Base_module_upgrade convert2PO(cn.ibizlab.odoo.core.odoo_base.domain.Base_module_upgrade domain , Base_module_upgrade po) ;

    cn.ibizlab.odoo.core.odoo_base.domain.Base_module_upgrade convert2Domain( Base_module_upgrade po ,cn.ibizlab.odoo.core.odoo_base.domain.Base_module_upgrade domain) ;

}
