package cn.ibizlab.odoo.core.repository.po ;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.math.BigInteger;

import cn.ibizlab.odoo.core.odoo_digest.filter.Digest_digestSearchContext;

/**
 * 实体 [摘要] 存储模型
 */
public interface Digest_digest{

    /**
     * 名称
     */
    String getName();

    void setName(String name);

    /**
     * 获取 [名称]脏标记
     */
    boolean getNameDirtyFlag();

    /**
     * Kpi网站销售总价值
     */
    Double getKpi_website_sale_total_value();

    void setKpi_website_sale_total_value(Double kpi_website_sale_total_value);

    /**
     * 获取 [Kpi网站销售总价值]脏标记
     */
    boolean getKpi_website_sale_total_valueDirtyFlag();

    /**
     * 创建时间
     */
    Timestamp getCreate_date();

    void setCreate_date(Timestamp create_date);

    /**
     * 获取 [创建时间]脏标记
     */
    boolean getCreate_dateDirtyFlag();

    /**
     * KPI CRM预期收益
     */
    Integer getKpi_crm_lead_created_value();

    void setKpi_crm_lead_created_value(Integer kpi_crm_lead_created_value);

    /**
     * 获取 [KPI CRM预期收益]脏标记
     */
    boolean getKpi_crm_lead_created_valueDirtyFlag();

    /**
     * 已签单商机
     */
    String getKpi_crm_opportunities_won();

    void setKpi_crm_opportunities_won(String kpi_crm_opportunities_won);

    /**
     * 获取 [已签单商机]脏标记
     */
    boolean getKpi_crm_opportunities_wonDirtyFlag();

    /**
     * 可用字段
     */
    String getAvailable_fields();

    void setAvailable_fields(String available_fields);

    /**
     * 获取 [可用字段]脏标记
     */
    boolean getAvailable_fieldsDirtyFlag();

    /**
     * 状态
     */
    String getState();

    void setState(String state);

    /**
     * 获取 [状态]脏标记
     */
    boolean getStateDirtyFlag();

    /**
     * 收件人
     */
    String getUser_ids();

    void setUser_ids(String user_ids);

    /**
     * 获取 [收件人]脏标记
     */
    boolean getUser_idsDirtyFlag();

    /**
     * KPI CRM签单金额
     */
    Integer getKpi_crm_opportunities_won_value();

    void setKpi_crm_opportunities_won_value(Integer kpi_crm_opportunities_won_value);

    /**
     * 获取 [KPI CRM签单金额]脏标记
     */
    boolean getKpi_crm_opportunities_won_valueDirtyFlag();

    /**
     * 员工
     */
    String getKpi_hr_recruitment_new_colleagues();

    void setKpi_hr_recruitment_new_colleagues(String kpi_hr_recruitment_new_colleagues);

    /**
     * 获取 [员工]脏标记
     */
    boolean getKpi_hr_recruitment_new_colleaguesDirtyFlag();

    /**
     * 最后修改日
     */
    Timestamp get__last_update();

    void set__last_update(Timestamp __last_update);

    /**
     * 获取 [最后修改日]脏标记
     */
    boolean get__last_updateDirtyFlag();

    /**
     * POS 销售
     */
    String getKpi_pos_total();

    void setKpi_pos_total(String kpi_pos_total);

    /**
     * 获取 [POS 销售]脏标记
     */
    boolean getKpi_pos_totalDirtyFlag();

    /**
     * 最后更新时间
     */
    Timestamp getWrite_date();

    void setWrite_date(Timestamp write_date);

    /**
     * 获取 [最后更新时间]脏标记
     */
    boolean getWrite_dateDirtyFlag();

    /**
     * 终端销售总额的关键绩效指标
     */
    Double getKpi_pos_total_value();

    void setKpi_pos_total_value(Double kpi_pos_total_value);

    /**
     * 获取 [终端销售总额的关键绩效指标]脏标记
     */
    boolean getKpi_pos_total_valueDirtyFlag();

    /**
     * 电商销售
     */
    String getKpi_website_sale_total();

    void setKpi_website_sale_total(String kpi_website_sale_total);

    /**
     * 获取 [电商销售]脏标记
     */
    boolean getKpi_website_sale_totalDirtyFlag();

    /**
     * 所有销售
     */
    String getKpi_all_sale_total();

    void setKpi_all_sale_total(String kpi_all_sale_total);

    /**
     * 获取 [所有销售]脏标记
     */
    boolean getKpi_all_sale_totalDirtyFlag();

    /**
     * 下一发送日期
     */
    Timestamp getNext_run_date();

    void setNext_run_date(Timestamp next_run_date);

    /**
     * 获取 [下一发送日期]脏标记
     */
    boolean getNext_run_dateDirtyFlag();

    /**
     * 已连接用户
     */
    String getKpi_res_users_connected();

    void setKpi_res_users_connected(String kpi_res_users_connected);

    /**
     * 获取 [已连接用户]脏标记
     */
    boolean getKpi_res_users_connectedDirtyFlag();

    /**
     * Kpi Res 用户连接值
     */
    Integer getKpi_res_users_connected_value();

    void setKpi_res_users_connected_value(Integer kpi_res_users_connected_value);

    /**
     * 获取 [Kpi Res 用户连接值]脏标记
     */
    boolean getKpi_res_users_connected_valueDirtyFlag();

    /**
     * 周期
     */
    String getPeriodicity();

    void setPeriodicity(String periodicity);

    /**
     * 获取 [周期]脏标记
     */
    boolean getPeriodicityDirtyFlag();

    /**
     * ID
     */
    Integer getId();

    void setId(Integer id);

    /**
     * 获取 [ID]脏标记
     */
    boolean getIdDirtyFlag();

    /**
     * 人力资源新聘员工KPI指标
     */
    Integer getKpi_hr_recruitment_new_colleagues_value();

    void setKpi_hr_recruitment_new_colleagues_value(Integer kpi_hr_recruitment_new_colleagues_value);

    /**
     * 获取 [人力资源新聘员工KPI指标]脏标记
     */
    boolean getKpi_hr_recruitment_new_colleagues_valueDirtyFlag();

    /**
     * 收入
     */
    String getKpi_account_total_revenue();

    void setKpi_account_total_revenue(String kpi_account_total_revenue);

    /**
     * 获取 [收入]脏标记
     */
    boolean getKpi_account_total_revenueDirtyFlag();

    /**
     * 新的线索/商机
     */
    String getKpi_crm_lead_created();

    void setKpi_crm_lead_created(String kpi_crm_lead_created);

    /**
     * 获取 [新的线索/商机]脏标记
     */
    boolean getKpi_crm_lead_createdDirtyFlag();

    /**
     * 开放任务
     */
    String getKpi_project_task_opened();

    void setKpi_project_task_opened(String kpi_project_task_opened);

    /**
     * 获取 [开放任务]脏标记
     */
    boolean getKpi_project_task_openedDirtyFlag();

    /**
     * 所有销售总价值KPI
     */
    Double getKpi_all_sale_total_value();

    void setKpi_all_sale_total_value(Double kpi_all_sale_total_value);

    /**
     * 获取 [所有销售总价值KPI]脏标记
     */
    boolean getKpi_all_sale_total_valueDirtyFlag();

    /**
     * 已订阅
     */
    String getIs_subscribed();

    void setIs_subscribed(String is_subscribed);

    /**
     * 获取 [已订阅]脏标记
     */
    boolean getIs_subscribedDirtyFlag();

    /**
     * 显示名称
     */
    String getDisplay_name();

    void setDisplay_name(String display_name);

    /**
     * 获取 [显示名称]脏标记
     */
    boolean getDisplay_nameDirtyFlag();

    /**
     * 项目任务开放价值的关键绩效指标
     */
    Integer getKpi_project_task_opened_value();

    void setKpi_project_task_opened_value(Integer kpi_project_task_opened_value);

    /**
     * 获取 [项目任务开放价值的关键绩效指标]脏标记
     */
    boolean getKpi_project_task_opened_valueDirtyFlag();

    /**
     * KPI账户总收入
     */
    Double getKpi_account_total_revenue_value();

    void setKpi_account_total_revenue_value(Double kpi_account_total_revenue_value);

    /**
     * 获取 [KPI账户总收入]脏标记
     */
    boolean getKpi_account_total_revenue_valueDirtyFlag();

    /**
     * Kpi 邮件信息总计
     */
    Integer getKpi_mail_message_total_value();

    void setKpi_mail_message_total_value(Integer kpi_mail_message_total_value);

    /**
     * 获取 [Kpi 邮件信息总计]脏标记
     */
    boolean getKpi_mail_message_total_valueDirtyFlag();

    /**
     * 消息
     */
    String getKpi_mail_message_total();

    void setKpi_mail_message_total(String kpi_mail_message_total);

    /**
     * 获取 [消息]脏标记
     */
    boolean getKpi_mail_message_totalDirtyFlag();

    /**
     * 最后更新者
     */
    String getWrite_uid_text();

    void setWrite_uid_text(String write_uid_text);

    /**
     * 获取 [最后更新者]脏标记
     */
    boolean getWrite_uid_textDirtyFlag();

    /**
     * EMail模板
     */
    String getTemplate_id_text();

    void setTemplate_id_text(String template_id_text);

    /**
     * 获取 [EMail模板]脏标记
     */
    boolean getTemplate_id_textDirtyFlag();

    /**
     * 公司
     */
    String getCompany_id_text();

    void setCompany_id_text(String company_id_text);

    /**
     * 获取 [公司]脏标记
     */
    boolean getCompany_id_textDirtyFlag();

    /**
     * 币种
     */
    Integer getCurrency_id();

    void setCurrency_id(Integer currency_id);

    /**
     * 获取 [币种]脏标记
     */
    boolean getCurrency_idDirtyFlag();

    /**
     * 创建人
     */
    String getCreate_uid_text();

    void setCreate_uid_text(String create_uid_text);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uid_textDirtyFlag();

    /**
     * 创建人
     */
    Integer getCreate_uid();

    void setCreate_uid(Integer create_uid);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uidDirtyFlag();

    /**
     * 最后更新者
     */
    Integer getWrite_uid();

    void setWrite_uid(Integer write_uid);

    /**
     * 获取 [最后更新者]脏标记
     */
    boolean getWrite_uidDirtyFlag();

    /**
     * 公司
     */
    Integer getCompany_id();

    void setCompany_id(Integer company_id);

    /**
     * 获取 [公司]脏标记
     */
    boolean getCompany_idDirtyFlag();

    /**
     * EMail模板
     */
    Integer getTemplate_id();

    void setTemplate_id(Integer template_id);

    /**
     * 获取 [EMail模板]脏标记
     */
    boolean getTemplate_idDirtyFlag();

}
