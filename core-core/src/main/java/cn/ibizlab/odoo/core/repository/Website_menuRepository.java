package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Website_menu;
import cn.ibizlab.odoo.core.odoo_website.filter.Website_menuSearchContext;

/**
 * 实体 [网站菜单] 存储对象
 */
public interface Website_menuRepository extends Repository<Website_menu> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Website_menu> searchDefault(Website_menuSearchContext context);

    Website_menu convert2PO(cn.ibizlab.odoo.core.odoo_website.domain.Website_menu domain , Website_menu po) ;

    cn.ibizlab.odoo.core.odoo_website.domain.Website_menu convert2Domain( Website_menu po ,cn.ibizlab.odoo.core.odoo_website.domain.Website_menu domain) ;

}
