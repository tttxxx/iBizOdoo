package cn.ibizlab.odoo.core.odoo_event.service.validator;

import java.sql.Timestamp;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import cn.ibizlab.odoo.util.ISearchFilter;
import java.math.BigDecimal;
/**
 * 实体[Event_type_mail]的实体值规则[Default] 对象
 */
public class Event_type_mailDefaultValidator implements Validator {
    @Override
    public boolean supports(Class<?> clazz) {
		return false;
	}
    @Override
	public void validate(Object target, Errors errors) {

	}
}
