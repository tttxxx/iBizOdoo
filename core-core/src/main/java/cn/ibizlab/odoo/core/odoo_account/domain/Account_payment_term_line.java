package cn.ibizlab.odoo.core.odoo_account.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.odoo.util.domain.EntityBase;
import cn.ibizlab.odoo.util.annotation.DEField;
import cn.ibizlab.odoo.util.enums.DEPredefinedFieldType;
import cn.ibizlab.odoo.util.enums.DEFieldDefaultValueType;
import java.io.Serializable;
import lombok.Data;
import org.springframework.data.annotation.Transient;

import cn.ibizlab.odoo.util.domain.EntityClient;

/**
 * ServiceApi [付款条款行] 对象
 */
@Data
public class Account_payment_term_line extends EntityClient implements Serializable {

    /**
     * 天数
     */
    @JSONField(name = "days")
    @JsonProperty("days")
    private Integer days;

    /**
     * 序号
     */
    @JSONField(name = "sequence")
    @JsonProperty("sequence")
    private Integer sequence;

    /**
     * 最后更新时间
     */
    @DEField(name = "write_date" , preType = DEPredefinedFieldType.UPDATEDATE)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;

    /**
     * 最后修改日
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;

    /**
     * 类型
     */
    @JSONField(name = "value")
    @JsonProperty("value")
    private String value;

    /**
     * 值
     */
    @DEField(name = "value_amount")
    @JSONField(name = "value_amount")
    @JsonProperty("value_amount")
    private Double valueAmount;

    /**
     * 日期
     */
    @DEField(name = "day_of_the_month")
    @JSONField(name = "day_of_the_month")
    @JsonProperty("day_of_the_month")
    private Integer dayOfTheMonth;

    /**
     * 显示名称
     */
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    private String displayName;

    /**
     * 选项
     */
    @JSONField(name = "option")
    @JsonProperty("option")
    private String option;

    /**
     * 创建时间
     */
    @DEField(name = "create_date" , preType = DEPredefinedFieldType.CREATEDATE)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;

    /**
     * ID
     */
    @DEField(isKeyField=true)
    @JSONField(name = "id")
    @JsonProperty("id")
    private Integer id;

    /**
     * 付款条款
     */
    @JSONField(name = "payment_id_text")
    @JsonProperty("payment_id_text")
    private String paymentIdText;

    /**
     * 创建人
     */
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    private String createUidText;

    /**
     * 最后更新人
     */
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    private String writeUidText;

    /**
     * 创建人
     */
    @DEField(name = "create_uid" , preType = DEPredefinedFieldType.CREATEMAN)
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    private Integer createUid;

    /**
     * 付款条款
     */
    @DEField(name = "payment_id")
    @JSONField(name = "payment_id")
    @JsonProperty("payment_id")
    private Integer paymentId;

    /**
     * 最后更新人
     */
    @DEField(name = "write_uid" , preType = DEPredefinedFieldType.UPDATEMAN)
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    private Integer writeUid;


    /**
     * 
     */
    @JSONField(name = "odoopayment")
    @JsonProperty("odoopayment")
    private cn.ibizlab.odoo.core.odoo_account.domain.Account_payment_term odooPayment;

    /**
     * 
     */
    @JSONField(name = "odoocreate")
    @JsonProperty("odoocreate")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_users odooCreate;

    /**
     * 
     */
    @JSONField(name = "odoowrite")
    @JsonProperty("odoowrite")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_users odooWrite;




    /**
     * 设置 [天数]
     */
    public void setDays(Integer days){
        this.days = days ;
        this.modify("days",days);
    }
    /**
     * 设置 [序号]
     */
    public void setSequence(Integer sequence){
        this.sequence = sequence ;
        this.modify("sequence",sequence);
    }
    /**
     * 设置 [类型]
     */
    public void setValue(String value){
        this.value = value ;
        this.modify("value",value);
    }
    /**
     * 设置 [值]
     */
    public void setValueAmount(Double valueAmount){
        this.valueAmount = valueAmount ;
        this.modify("value_amount",valueAmount);
    }
    /**
     * 设置 [日期]
     */
    public void setDayOfTheMonth(Integer dayOfTheMonth){
        this.dayOfTheMonth = dayOfTheMonth ;
        this.modify("day_of_the_month",dayOfTheMonth);
    }
    /**
     * 设置 [选项]
     */
    public void setOption(String option){
        this.option = option ;
        this.modify("option",option);
    }
    /**
     * 设置 [付款条款]
     */
    public void setPaymentId(Integer paymentId){
        this.paymentId = paymentId ;
        this.modify("payment_id",paymentId);
    }

}


