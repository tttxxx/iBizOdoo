package cn.ibizlab.odoo.core.repository.client.po;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigDecimal;

/**
 * [mrp_workcenter_productivity] 对象
 */
public interface mrp_workcenter_productivity {

    public Timestamp getCreate_date();

    public void setCreate_date(Timestamp create_date);

    public Integer getCreate_uid();

    public void setCreate_uid(Integer create_uid);

    public String getCreate_uid_text();

    public void setCreate_uid_text(String create_uid_text);

    public Timestamp getDate_end();

    public void setDate_end(Timestamp date_end);

    public Timestamp getDate_start();

    public void setDate_start(Timestamp date_start);

    public String getDescription();

    public void setDescription(String description);

    public String getDisplay_name();

    public void setDisplay_name(String display_name);

    public Double getDuration();

    public void setDuration(Double duration);

    public Integer getId();

    public void setId(Integer id);

    public Integer getLoss_id();

    public void setLoss_id(Integer loss_id);

    public String getLoss_id_text();

    public void setLoss_id_text(String loss_id_text);

    public String getLoss_type();

    public void setLoss_type(String loss_type);

    public Integer getProduction_id();

    public void setProduction_id(Integer production_id);

    public String getProduction_id_text();

    public void setProduction_id_text(String production_id_text);

    public Integer getUser_id();

    public void setUser_id(Integer user_id);

    public String getUser_id_text();

    public void setUser_id_text(String user_id_text);

    public Integer getWorkcenter_id();

    public void setWorkcenter_id(Integer workcenter_id);

    public String getWorkcenter_id_text();

    public void setWorkcenter_id_text(String workcenter_id_text);

    public Integer getWorkorder_id();

    public void setWorkorder_id(Integer workorder_id);

    public String getWorkorder_id_text();

    public void setWorkorder_id_text(String workorder_id_text);

    public Timestamp getWrite_date();

    public void setWrite_date(Timestamp write_date);

    public Integer getWrite_uid();

    public void setWrite_uid(Integer write_uid);

    public String getWrite_uid_text();

    public void setWrite_uid_text(String write_uid_text);

    public Timestamp get__last_update();

    public void set__last_update(Timestamp __last_update);

}
