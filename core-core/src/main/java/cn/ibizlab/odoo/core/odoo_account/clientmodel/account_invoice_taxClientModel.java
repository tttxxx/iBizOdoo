package cn.ibizlab.odoo.core.odoo_account.clientmodel;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.math.BigInteger;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.util.helper.OdooClientHelper;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang.ArrayUtils;
import org.springframework.util.StringUtils;

/**
 * 接口实体[account_invoice_tax] 对象
 */
public class account_invoice_taxClientModel implements Serializable{

    /**
     * 分析账户
     */
    public Integer account_analytic_id;

    @JsonIgnore
    public boolean account_analytic_idDirtyFlag;
    
    /**
     * 分析账户
     */
    public String account_analytic_id_text;

    @JsonIgnore
    public boolean account_analytic_id_textDirtyFlag;
    
    /**
     * 税率科目
     */
    public Integer account_id;

    @JsonIgnore
    public boolean account_idDirtyFlag;
    
    /**
     * 税率科目
     */
    public String account_id_text;

    @JsonIgnore
    public boolean account_id_textDirtyFlag;
    
    /**
     * 税率金额
     */
    public Double amount;

    @JsonIgnore
    public boolean amountDirtyFlag;
    
    /**
     * 金额差异
     */
    public Double amount_rounding;

    @JsonIgnore
    public boolean amount_roundingDirtyFlag;
    
    /**
     * 总金额
     */
    public Double amount_total;

    @JsonIgnore
    public boolean amount_totalDirtyFlag;
    
    /**
     * 分析标签
     */
    public String analytic_tag_ids;

    @JsonIgnore
    public boolean analytic_tag_idsDirtyFlag;
    
    /**
     * 基础
     */
    public Double base;

    @JsonIgnore
    public boolean baseDirtyFlag;
    
    /**
     * 公司
     */
    public Integer company_id;

    @JsonIgnore
    public boolean company_idDirtyFlag;
    
    /**
     * 公司
     */
    public String company_id_text;

    @JsonIgnore
    public boolean company_id_textDirtyFlag;
    
    /**
     * 创建时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp create_date;

    @JsonIgnore
    public boolean create_dateDirtyFlag;
    
    /**
     * 创建人
     */
    public Integer create_uid;

    @JsonIgnore
    public boolean create_uidDirtyFlag;
    
    /**
     * 创建人
     */
    public String create_uid_text;

    @JsonIgnore
    public boolean create_uid_textDirtyFlag;
    
    /**
     * 币种
     */
    public Integer currency_id;

    @JsonIgnore
    public boolean currency_idDirtyFlag;
    
    /**
     * 币种
     */
    public String currency_id_text;

    @JsonIgnore
    public boolean currency_id_textDirtyFlag;
    
    /**
     * 显示名称
     */
    public String display_name;

    @JsonIgnore
    public boolean display_nameDirtyFlag;
    
    /**
     * ID
     */
    public Integer id;

    @JsonIgnore
    public boolean idDirtyFlag;
    
    /**
     * 发票
     */
    public Integer invoice_id;

    @JsonIgnore
    public boolean invoice_idDirtyFlag;
    
    /**
     * 发票
     */
    public String invoice_id_text;

    @JsonIgnore
    public boolean invoice_id_textDirtyFlag;
    
    /**
     * 手动
     */
    public String manual;

    @JsonIgnore
    public boolean manualDirtyFlag;
    
    /**
     * 税说明
     */
    public String name;

    @JsonIgnore
    public boolean nameDirtyFlag;
    
    /**
     * 序号
     */
    public Integer sequence;

    @JsonIgnore
    public boolean sequenceDirtyFlag;
    
    /**
     * 税率
     */
    public Integer tax_id;

    @JsonIgnore
    public boolean tax_idDirtyFlag;
    
    /**
     * 税率
     */
    public String tax_id_text;

    @JsonIgnore
    public boolean tax_id_textDirtyFlag;
    
    /**
     * 最后更新时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp write_date;

    @JsonIgnore
    public boolean write_dateDirtyFlag;
    
    /**
     * 最后更新人
     */
    public Integer write_uid;

    @JsonIgnore
    public boolean write_uidDirtyFlag;
    
    /**
     * 最后更新人
     */
    public String write_uid_text;

    @JsonIgnore
    public boolean write_uid_textDirtyFlag;
    
    /**
     * 最后修改日
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp __last_update;

    @JsonIgnore
    public boolean __last_updateDirtyFlag;
    
    /**
     * 获取 [分析账户]
     */
    @JsonProperty("account_analytic_id")
    public Integer getAccount_analytic_id(){
        return this.account_analytic_id ;
    }

    /**
     * 设置 [分析账户]
     */
    @JsonProperty("account_analytic_id")
    public void setAccount_analytic_id(Integer  account_analytic_id){
        this.account_analytic_id = account_analytic_id ;
        this.account_analytic_idDirtyFlag = true ;
    }

     /**
     * 获取 [分析账户]脏标记
     */
    @JsonIgnore
    public boolean getAccount_analytic_idDirtyFlag(){
        return this.account_analytic_idDirtyFlag ;
    }   

    /**
     * 获取 [分析账户]
     */
    @JsonProperty("account_analytic_id_text")
    public String getAccount_analytic_id_text(){
        return this.account_analytic_id_text ;
    }

    /**
     * 设置 [分析账户]
     */
    @JsonProperty("account_analytic_id_text")
    public void setAccount_analytic_id_text(String  account_analytic_id_text){
        this.account_analytic_id_text = account_analytic_id_text ;
        this.account_analytic_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [分析账户]脏标记
     */
    @JsonIgnore
    public boolean getAccount_analytic_id_textDirtyFlag(){
        return this.account_analytic_id_textDirtyFlag ;
    }   

    /**
     * 获取 [税率科目]
     */
    @JsonProperty("account_id")
    public Integer getAccount_id(){
        return this.account_id ;
    }

    /**
     * 设置 [税率科目]
     */
    @JsonProperty("account_id")
    public void setAccount_id(Integer  account_id){
        this.account_id = account_id ;
        this.account_idDirtyFlag = true ;
    }

     /**
     * 获取 [税率科目]脏标记
     */
    @JsonIgnore
    public boolean getAccount_idDirtyFlag(){
        return this.account_idDirtyFlag ;
    }   

    /**
     * 获取 [税率科目]
     */
    @JsonProperty("account_id_text")
    public String getAccount_id_text(){
        return this.account_id_text ;
    }

    /**
     * 设置 [税率科目]
     */
    @JsonProperty("account_id_text")
    public void setAccount_id_text(String  account_id_text){
        this.account_id_text = account_id_text ;
        this.account_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [税率科目]脏标记
     */
    @JsonIgnore
    public boolean getAccount_id_textDirtyFlag(){
        return this.account_id_textDirtyFlag ;
    }   

    /**
     * 获取 [税率金额]
     */
    @JsonProperty("amount")
    public Double getAmount(){
        return this.amount ;
    }

    /**
     * 设置 [税率金额]
     */
    @JsonProperty("amount")
    public void setAmount(Double  amount){
        this.amount = amount ;
        this.amountDirtyFlag = true ;
    }

     /**
     * 获取 [税率金额]脏标记
     */
    @JsonIgnore
    public boolean getAmountDirtyFlag(){
        return this.amountDirtyFlag ;
    }   

    /**
     * 获取 [金额差异]
     */
    @JsonProperty("amount_rounding")
    public Double getAmount_rounding(){
        return this.amount_rounding ;
    }

    /**
     * 设置 [金额差异]
     */
    @JsonProperty("amount_rounding")
    public void setAmount_rounding(Double  amount_rounding){
        this.amount_rounding = amount_rounding ;
        this.amount_roundingDirtyFlag = true ;
    }

     /**
     * 获取 [金额差异]脏标记
     */
    @JsonIgnore
    public boolean getAmount_roundingDirtyFlag(){
        return this.amount_roundingDirtyFlag ;
    }   

    /**
     * 获取 [总金额]
     */
    @JsonProperty("amount_total")
    public Double getAmount_total(){
        return this.amount_total ;
    }

    /**
     * 设置 [总金额]
     */
    @JsonProperty("amount_total")
    public void setAmount_total(Double  amount_total){
        this.amount_total = amount_total ;
        this.amount_totalDirtyFlag = true ;
    }

     /**
     * 获取 [总金额]脏标记
     */
    @JsonIgnore
    public boolean getAmount_totalDirtyFlag(){
        return this.amount_totalDirtyFlag ;
    }   

    /**
     * 获取 [分析标签]
     */
    @JsonProperty("analytic_tag_ids")
    public String getAnalytic_tag_ids(){
        return this.analytic_tag_ids ;
    }

    /**
     * 设置 [分析标签]
     */
    @JsonProperty("analytic_tag_ids")
    public void setAnalytic_tag_ids(String  analytic_tag_ids){
        this.analytic_tag_ids = analytic_tag_ids ;
        this.analytic_tag_idsDirtyFlag = true ;
    }

     /**
     * 获取 [分析标签]脏标记
     */
    @JsonIgnore
    public boolean getAnalytic_tag_idsDirtyFlag(){
        return this.analytic_tag_idsDirtyFlag ;
    }   

    /**
     * 获取 [基础]
     */
    @JsonProperty("base")
    public Double getBase(){
        return this.base ;
    }

    /**
     * 设置 [基础]
     */
    @JsonProperty("base")
    public void setBase(Double  base){
        this.base = base ;
        this.baseDirtyFlag = true ;
    }

     /**
     * 获取 [基础]脏标记
     */
    @JsonIgnore
    public boolean getBaseDirtyFlag(){
        return this.baseDirtyFlag ;
    }   

    /**
     * 获取 [公司]
     */
    @JsonProperty("company_id")
    public Integer getCompany_id(){
        return this.company_id ;
    }

    /**
     * 设置 [公司]
     */
    @JsonProperty("company_id")
    public void setCompany_id(Integer  company_id){
        this.company_id = company_id ;
        this.company_idDirtyFlag = true ;
    }

     /**
     * 获取 [公司]脏标记
     */
    @JsonIgnore
    public boolean getCompany_idDirtyFlag(){
        return this.company_idDirtyFlag ;
    }   

    /**
     * 获取 [公司]
     */
    @JsonProperty("company_id_text")
    public String getCompany_id_text(){
        return this.company_id_text ;
    }

    /**
     * 设置 [公司]
     */
    @JsonProperty("company_id_text")
    public void setCompany_id_text(String  company_id_text){
        this.company_id_text = company_id_text ;
        this.company_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [公司]脏标记
     */
    @JsonIgnore
    public boolean getCompany_id_textDirtyFlag(){
        return this.company_id_textDirtyFlag ;
    }   

    /**
     * 获取 [创建时间]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return this.create_date ;
    }

    /**
     * 设置 [创建时间]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

     /**
     * 获取 [创建时间]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return this.create_dateDirtyFlag ;
    }   

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return this.create_uid ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

     /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return this.create_uidDirtyFlag ;
    }   

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return this.create_uid_text ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

     /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return this.create_uid_textDirtyFlag ;
    }   

    /**
     * 获取 [币种]
     */
    @JsonProperty("currency_id")
    public Integer getCurrency_id(){
        return this.currency_id ;
    }

    /**
     * 设置 [币种]
     */
    @JsonProperty("currency_id")
    public void setCurrency_id(Integer  currency_id){
        this.currency_id = currency_id ;
        this.currency_idDirtyFlag = true ;
    }

     /**
     * 获取 [币种]脏标记
     */
    @JsonIgnore
    public boolean getCurrency_idDirtyFlag(){
        return this.currency_idDirtyFlag ;
    }   

    /**
     * 获取 [币种]
     */
    @JsonProperty("currency_id_text")
    public String getCurrency_id_text(){
        return this.currency_id_text ;
    }

    /**
     * 设置 [币种]
     */
    @JsonProperty("currency_id_text")
    public void setCurrency_id_text(String  currency_id_text){
        this.currency_id_text = currency_id_text ;
        this.currency_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [币种]脏标记
     */
    @JsonIgnore
    public boolean getCurrency_id_textDirtyFlag(){
        return this.currency_id_textDirtyFlag ;
    }   

    /**
     * 获取 [显示名称]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return this.display_name ;
    }

    /**
     * 设置 [显示名称]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

     /**
     * 获取 [显示名称]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return this.display_nameDirtyFlag ;
    }   

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return this.id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

     /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return this.idDirtyFlag ;
    }   

    /**
     * 获取 [发票]
     */
    @JsonProperty("invoice_id")
    public Integer getInvoice_id(){
        return this.invoice_id ;
    }

    /**
     * 设置 [发票]
     */
    @JsonProperty("invoice_id")
    public void setInvoice_id(Integer  invoice_id){
        this.invoice_id = invoice_id ;
        this.invoice_idDirtyFlag = true ;
    }

     /**
     * 获取 [发票]脏标记
     */
    @JsonIgnore
    public boolean getInvoice_idDirtyFlag(){
        return this.invoice_idDirtyFlag ;
    }   

    /**
     * 获取 [发票]
     */
    @JsonProperty("invoice_id_text")
    public String getInvoice_id_text(){
        return this.invoice_id_text ;
    }

    /**
     * 设置 [发票]
     */
    @JsonProperty("invoice_id_text")
    public void setInvoice_id_text(String  invoice_id_text){
        this.invoice_id_text = invoice_id_text ;
        this.invoice_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [发票]脏标记
     */
    @JsonIgnore
    public boolean getInvoice_id_textDirtyFlag(){
        return this.invoice_id_textDirtyFlag ;
    }   

    /**
     * 获取 [手动]
     */
    @JsonProperty("manual")
    public String getManual(){
        return this.manual ;
    }

    /**
     * 设置 [手动]
     */
    @JsonProperty("manual")
    public void setManual(String  manual){
        this.manual = manual ;
        this.manualDirtyFlag = true ;
    }

     /**
     * 获取 [手动]脏标记
     */
    @JsonIgnore
    public boolean getManualDirtyFlag(){
        return this.manualDirtyFlag ;
    }   

    /**
     * 获取 [税说明]
     */
    @JsonProperty("name")
    public String getName(){
        return this.name ;
    }

    /**
     * 设置 [税说明]
     */
    @JsonProperty("name")
    public void setName(String  name){
        this.name = name ;
        this.nameDirtyFlag = true ;
    }

     /**
     * 获取 [税说明]脏标记
     */
    @JsonIgnore
    public boolean getNameDirtyFlag(){
        return this.nameDirtyFlag ;
    }   

    /**
     * 获取 [序号]
     */
    @JsonProperty("sequence")
    public Integer getSequence(){
        return this.sequence ;
    }

    /**
     * 设置 [序号]
     */
    @JsonProperty("sequence")
    public void setSequence(Integer  sequence){
        this.sequence = sequence ;
        this.sequenceDirtyFlag = true ;
    }

     /**
     * 获取 [序号]脏标记
     */
    @JsonIgnore
    public boolean getSequenceDirtyFlag(){
        return this.sequenceDirtyFlag ;
    }   

    /**
     * 获取 [税率]
     */
    @JsonProperty("tax_id")
    public Integer getTax_id(){
        return this.tax_id ;
    }

    /**
     * 设置 [税率]
     */
    @JsonProperty("tax_id")
    public void setTax_id(Integer  tax_id){
        this.tax_id = tax_id ;
        this.tax_idDirtyFlag = true ;
    }

     /**
     * 获取 [税率]脏标记
     */
    @JsonIgnore
    public boolean getTax_idDirtyFlag(){
        return this.tax_idDirtyFlag ;
    }   

    /**
     * 获取 [税率]
     */
    @JsonProperty("tax_id_text")
    public String getTax_id_text(){
        return this.tax_id_text ;
    }

    /**
     * 设置 [税率]
     */
    @JsonProperty("tax_id_text")
    public void setTax_id_text(String  tax_id_text){
        this.tax_id_text = tax_id_text ;
        this.tax_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [税率]脏标记
     */
    @JsonIgnore
    public boolean getTax_id_textDirtyFlag(){
        return this.tax_id_textDirtyFlag ;
    }   

    /**
     * 获取 [最后更新时间]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return this.write_date ;
    }

    /**
     * 设置 [最后更新时间]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新时间]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return this.write_dateDirtyFlag ;
    }   

    /**
     * 获取 [最后更新人]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return this.write_uid ;
    }

    /**
     * 设置 [最后更新人]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新人]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return this.write_uidDirtyFlag ;
    }   

    /**
     * 获取 [最后更新人]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return this.write_uid_text ;
    }

    /**
     * 设置 [最后更新人]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新人]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return this.write_uid_textDirtyFlag ;
    }   

    /**
     * 获取 [最后修改日]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return this.__last_update ;
    }

    /**
     * 设置 [最后修改日]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

     /**
     * 获取 [最后修改日]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return this.__last_updateDirtyFlag ;
    }   



    public void fromMap(Map<String, Object> map) throws Exception {
		if(!(map.get("account_analytic_id") instanceof Boolean)&& map.get("account_analytic_id")!=null){
			Object[] objs = (Object[])map.get("account_analytic_id");
			if(objs.length > 0){
				this.setAccount_analytic_id((Integer)objs[0]);
			}
		}
		if(!(map.get("account_analytic_id") instanceof Boolean)&& map.get("account_analytic_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("account_analytic_id");
			if(objs.length > 1){
				this.setAccount_analytic_id_text((String)objs[1]);
			}
		}
		if(!(map.get("account_id") instanceof Boolean)&& map.get("account_id")!=null){
			Object[] objs = (Object[])map.get("account_id");
			if(objs.length > 0){
				this.setAccount_id((Integer)objs[0]);
			}
		}
		if(!(map.get("account_id") instanceof Boolean)&& map.get("account_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("account_id");
			if(objs.length > 1){
				this.setAccount_id_text((String)objs[1]);
			}
		}
		if(!(map.get("amount") instanceof Boolean)&& map.get("amount")!=null){
			this.setAmount((Double)map.get("amount"));
		}
		if(!(map.get("amount_rounding") instanceof Boolean)&& map.get("amount_rounding")!=null){
			this.setAmount_rounding((Double)map.get("amount_rounding"));
		}
		if(!(map.get("amount_total") instanceof Boolean)&& map.get("amount_total")!=null){
			this.setAmount_total((Double)map.get("amount_total"));
		}
		if(!(map.get("analytic_tag_ids") instanceof Boolean)&& map.get("analytic_tag_ids")!=null){
			Object[] objs = (Object[])map.get("analytic_tag_ids");
			if(objs.length > 0){
				Integer[] analytic_tag_ids = Arrays.copyOfRange(objs,0,objs.length,Integer[].class);
				this.setAnalytic_tag_ids(Arrays.toString(analytic_tag_ids).replace(" ",""));
			}
		}
		if(!(map.get("base") instanceof Boolean)&& map.get("base")!=null){
			this.setBase((Double)map.get("base"));
		}
		if(!(map.get("company_id") instanceof Boolean)&& map.get("company_id")!=null){
			Object[] objs = (Object[])map.get("company_id");
			if(objs.length > 0){
				this.setCompany_id((Integer)objs[0]);
			}
		}
		if(!(map.get("company_id") instanceof Boolean)&& map.get("company_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("company_id");
			if(objs.length > 1){
				this.setCompany_id_text((String)objs[1]);
			}
		}
		if(!(map.get("create_date") instanceof Boolean)&& map.get("create_date")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse((String)map.get("create_date"));
   			this.setCreate_date(new Timestamp(parse.getTime()));
		}
		if(!(map.get("create_uid") instanceof Boolean)&& map.get("create_uid")!=null){
			Object[] objs = (Object[])map.get("create_uid");
			if(objs.length > 0){
				this.setCreate_uid((Integer)objs[0]);
			}
		}
		if(!(map.get("create_uid") instanceof Boolean)&& map.get("create_uid")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("create_uid");
			if(objs.length > 1){
				this.setCreate_uid_text((String)objs[1]);
			}
		}
		if(!(map.get("currency_id") instanceof Boolean)&& map.get("currency_id")!=null){
			Object[] objs = (Object[])map.get("currency_id");
			if(objs.length > 0){
				this.setCurrency_id((Integer)objs[0]);
			}
		}
		if(!(map.get("currency_id") instanceof Boolean)&& map.get("currency_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("currency_id");
			if(objs.length > 1){
				this.setCurrency_id_text((String)objs[1]);
			}
		}
		if(!(map.get("display_name") instanceof Boolean)&& map.get("display_name")!=null){
			this.setDisplay_name((String)map.get("display_name"));
		}
		if(!(map.get("id") instanceof Boolean)&& map.get("id")!=null){
			this.setId((Integer)map.get("id"));
		}
		if(!(map.get("invoice_id") instanceof Boolean)&& map.get("invoice_id")!=null){
			Object[] objs = (Object[])map.get("invoice_id");
			if(objs.length > 0){
				this.setInvoice_id((Integer)objs[0]);
			}
		}
		if(!(map.get("invoice_id") instanceof Boolean)&& map.get("invoice_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("invoice_id");
			if(objs.length > 1){
				this.setInvoice_id_text((String)objs[1]);
			}
		}
		if(map.get("manual") instanceof Boolean){
			this.setManual(((Boolean)map.get("manual"))? "true" : "false");
		}
		if(!(map.get("name") instanceof Boolean)&& map.get("name")!=null){
			this.setName((String)map.get("name"));
		}
		if(!(map.get("sequence") instanceof Boolean)&& map.get("sequence")!=null){
			this.setSequence((Integer)map.get("sequence"));
		}
		if(!(map.get("tax_id") instanceof Boolean)&& map.get("tax_id")!=null){
			Object[] objs = (Object[])map.get("tax_id");
			if(objs.length > 0){
				this.setTax_id((Integer)objs[0]);
			}
		}
		if(!(map.get("tax_id") instanceof Boolean)&& map.get("tax_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("tax_id");
			if(objs.length > 1){
				this.setTax_id_text((String)objs[1]);
			}
		}
		if(!(map.get("write_date") instanceof Boolean)&& map.get("write_date")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse((String)map.get("write_date"));
   			this.setWrite_date(new Timestamp(parse.getTime()));
		}
		if(!(map.get("write_uid") instanceof Boolean)&& map.get("write_uid")!=null){
			Object[] objs = (Object[])map.get("write_uid");
			if(objs.length > 0){
				this.setWrite_uid((Integer)objs[0]);
			}
		}
		if(!(map.get("write_uid") instanceof Boolean)&& map.get("write_uid")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("write_uid");
			if(objs.length > 1){
				this.setWrite_uid_text((String)objs[1]);
			}
		}
		if(!(map.get("__last_update") instanceof Boolean)&& map.get("__last_update")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse((String)map.get("__last_update"));
   			this.set__last_update(new Timestamp(parse.getTime()));
		}
	}

	public Map<String, Object> toMap() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		if(this.getAccount_analytic_id()!=null&&this.getAccount_analytic_idDirtyFlag()){
			map.put("account_analytic_id",this.getAccount_analytic_id());
		}else if(this.getAccount_analytic_idDirtyFlag()){
			map.put("account_analytic_id",false);
		}
		if(this.getAccount_analytic_id_text()!=null&&this.getAccount_analytic_id_textDirtyFlag()){
			//忽略文本外键account_analytic_id_text
		}else if(this.getAccount_analytic_id_textDirtyFlag()){
			map.put("account_analytic_id",false);
		}
		if(this.getAccount_id()!=null&&this.getAccount_idDirtyFlag()){
			map.put("account_id",this.getAccount_id());
		}else if(this.getAccount_idDirtyFlag()){
			map.put("account_id",false);
		}
		if(this.getAccount_id_text()!=null&&this.getAccount_id_textDirtyFlag()){
			//忽略文本外键account_id_text
		}else if(this.getAccount_id_textDirtyFlag()){
			map.put("account_id",false);
		}
		if(this.getAmount()!=null&&this.getAmountDirtyFlag()){
			map.put("amount",this.getAmount());
		}else if(this.getAmountDirtyFlag()){
			map.put("amount",false);
		}
		if(this.getAmount_rounding()!=null&&this.getAmount_roundingDirtyFlag()){
			map.put("amount_rounding",this.getAmount_rounding());
		}else if(this.getAmount_roundingDirtyFlag()){
			map.put("amount_rounding",false);
		}
		if(this.getAmount_total()!=null&&this.getAmount_totalDirtyFlag()){
			map.put("amount_total",this.getAmount_total());
		}else if(this.getAmount_totalDirtyFlag()){
			map.put("amount_total",false);
		}
		if(this.getAnalytic_tag_ids()!=null&&this.getAnalytic_tag_idsDirtyFlag()){
			map.put("analytic_tag_ids",this.getAnalytic_tag_ids());
		}else if(this.getAnalytic_tag_idsDirtyFlag()){
			map.put("analytic_tag_ids",false);
		}
		if(this.getBase()!=null&&this.getBaseDirtyFlag()){
			map.put("base",this.getBase());
		}else if(this.getBaseDirtyFlag()){
			map.put("base",false);
		}
		if(this.getCompany_id()!=null&&this.getCompany_idDirtyFlag()){
			map.put("company_id",this.getCompany_id());
		}else if(this.getCompany_idDirtyFlag()){
			map.put("company_id",false);
		}
		if(this.getCompany_id_text()!=null&&this.getCompany_id_textDirtyFlag()){
			//忽略文本外键company_id_text
		}else if(this.getCompany_id_textDirtyFlag()){
			map.put("company_id",false);
		}
		if(this.getCreate_date()!=null&&this.getCreate_dateDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String datetimeStr = sdf.format(this.getCreate_date());
			map.put("create_date",datetimeStr);
		}else if(this.getCreate_dateDirtyFlag()){
			map.put("create_date",false);
		}
		if(this.getCreate_uid()!=null&&this.getCreate_uidDirtyFlag()){
			map.put("create_uid",this.getCreate_uid());
		}else if(this.getCreate_uidDirtyFlag()){
			map.put("create_uid",false);
		}
		if(this.getCreate_uid_text()!=null&&this.getCreate_uid_textDirtyFlag()){
			//忽略文本外键create_uid_text
		}else if(this.getCreate_uid_textDirtyFlag()){
			map.put("create_uid",false);
		}
		if(this.getCurrency_id()!=null&&this.getCurrency_idDirtyFlag()){
			map.put("currency_id",this.getCurrency_id());
		}else if(this.getCurrency_idDirtyFlag()){
			map.put("currency_id",false);
		}
		if(this.getCurrency_id_text()!=null&&this.getCurrency_id_textDirtyFlag()){
			//忽略文本外键currency_id_text
		}else if(this.getCurrency_id_textDirtyFlag()){
			map.put("currency_id",false);
		}
		if(this.getDisplay_name()!=null&&this.getDisplay_nameDirtyFlag()){
			map.put("display_name",this.getDisplay_name());
		}else if(this.getDisplay_nameDirtyFlag()){
			map.put("display_name",false);
		}
		if(this.getId()!=null&&this.getIdDirtyFlag()){
			map.put("id",this.getId());
		}else if(this.getIdDirtyFlag()){
			map.put("id",false);
		}
		if(this.getInvoice_id()!=null&&this.getInvoice_idDirtyFlag()){
			map.put("invoice_id",this.getInvoice_id());
		}else if(this.getInvoice_idDirtyFlag()){
			map.put("invoice_id",false);
		}
		if(this.getInvoice_id_text()!=null&&this.getInvoice_id_textDirtyFlag()){
			//忽略文本外键invoice_id_text
		}else if(this.getInvoice_id_textDirtyFlag()){
			map.put("invoice_id",false);
		}
		if(this.getManual()!=null&&this.getManualDirtyFlag()){
			map.put("manual",Boolean.parseBoolean(this.getManual()));		
		}		if(this.getName()!=null&&this.getNameDirtyFlag()){
			map.put("name",this.getName());
		}else if(this.getNameDirtyFlag()){
			map.put("name",false);
		}
		if(this.getSequence()!=null&&this.getSequenceDirtyFlag()){
			map.put("sequence",this.getSequence());
		}else if(this.getSequenceDirtyFlag()){
			map.put("sequence",false);
		}
		if(this.getTax_id()!=null&&this.getTax_idDirtyFlag()){
			map.put("tax_id",this.getTax_id());
		}else if(this.getTax_idDirtyFlag()){
			map.put("tax_id",false);
		}
		if(this.getTax_id_text()!=null&&this.getTax_id_textDirtyFlag()){
			//忽略文本外键tax_id_text
		}else if(this.getTax_id_textDirtyFlag()){
			map.put("tax_id",false);
		}
		if(this.getWrite_date()!=null&&this.getWrite_dateDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String datetimeStr = sdf.format(this.getWrite_date());
			map.put("write_date",datetimeStr);
		}else if(this.getWrite_dateDirtyFlag()){
			map.put("write_date",false);
		}
		if(this.getWrite_uid()!=null&&this.getWrite_uidDirtyFlag()){
			map.put("write_uid",this.getWrite_uid());
		}else if(this.getWrite_uidDirtyFlag()){
			map.put("write_uid",false);
		}
		if(this.getWrite_uid_text()!=null&&this.getWrite_uid_textDirtyFlag()){
			//忽略文本外键write_uid_text
		}else if(this.getWrite_uid_textDirtyFlag()){
			map.put("write_uid",false);
		}
		if(this.get__last_update()!=null&&this.get__last_updateDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String datetimeStr = sdf.format(this.get__last_update());
			map.put("__last_update",datetimeStr);
		}else if(this.get__last_updateDirtyFlag()){
			map.put("__last_update",false);
		}
		return map;
	}

}
