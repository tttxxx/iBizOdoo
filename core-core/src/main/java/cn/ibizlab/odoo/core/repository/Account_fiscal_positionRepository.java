package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Account_fiscal_position;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_fiscal_positionSearchContext;

/**
 * 实体 [税科目调整] 存储对象
 */
public interface Account_fiscal_positionRepository extends Repository<Account_fiscal_position> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Account_fiscal_position> searchDefault(Account_fiscal_positionSearchContext context);

    Account_fiscal_position convert2PO(cn.ibizlab.odoo.core.odoo_account.domain.Account_fiscal_position domain , Account_fiscal_position po) ;

    cn.ibizlab.odoo.core.odoo_account.domain.Account_fiscal_position convert2Domain( Account_fiscal_position po ,cn.ibizlab.odoo.core.odoo_account.domain.Account_fiscal_position domain) ;

}
