package cn.ibizlab.odoo.core.repository.po ;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.math.BigInteger;

import cn.ibizlab.odoo.core.odoo_account.filter.Account_invoice_lineSearchContext;

/**
 * 实体 [发票行] 存储模型
 */
public interface Account_invoice_line{

    /**
     * 显示类型
     */
    String getDisplay_type();

    void setDisplay_type(String display_type);

    /**
     * 获取 [显示类型]脏标记
     */
    boolean getDisplay_typeDirtyFlag();

    /**
     * 说明
     */
    String getName();

    void setName(String name);

    /**
     * 获取 [说明]脏标记
     */
    boolean getNameDirtyFlag();

    /**
     * 金额 (不含税)
     */
    Double getPrice_subtotal();

    void setPrice_subtotal(Double price_subtotal);

    /**
     * 获取 [金额 (不含税)]脏标记
     */
    boolean getPrice_subtotalDirtyFlag();

    /**
     * 显示名称
     */
    String getDisplay_name();

    void setDisplay_name(String display_name);

    /**
     * 获取 [显示名称]脏标记
     */
    boolean getDisplay_nameDirtyFlag();

    /**
     * 源文档
     */
    String getOrigin();

    void setOrigin(String origin);

    /**
     * 获取 [源文档]脏标记
     */
    boolean getOriginDirtyFlag();

    /**
     * 创建时间
     */
    Timestamp getCreate_date();

    void setCreate_date(Timestamp create_date);

    /**
     * 获取 [创建时间]脏标记
     */
    boolean getCreate_dateDirtyFlag();

    /**
     * 销售订单明细
     */
    String getSale_line_ids();

    void setSale_line_ids(String sale_line_ids);

    /**
     * 获取 [销售订单明细]脏标记
     */
    boolean getSale_line_idsDirtyFlag();

    /**
     * 折扣 (%)
     */
    Double getDiscount();

    void setDiscount(Double discount);

    /**
     * 获取 [折扣 (%)]脏标记
     */
    boolean getDiscountDirtyFlag();

    /**
     * 金额 (含税)
     */
    Double getPrice_total();

    void setPrice_total(Double price_total);

    /**
     * 获取 [金额 (含税)]脏标记
     */
    boolean getPrice_totalDirtyFlag();

    /**
     * 最后修改日
     */
    Timestamp get__last_update();

    void set__last_update(Timestamp __last_update);

    /**
     * 获取 [最后修改日]脏标记
     */
    boolean get__last_updateDirtyFlag();

    /**
     * 数量
     */
    Double getQuantity();

    void setQuantity(Double quantity);

    /**
     * 获取 [数量]脏标记
     */
    boolean getQuantityDirtyFlag();

    /**
     * 税率金额
     */
    Double getPrice_tax();

    void setPrice_tax(Double price_tax);

    /**
     * 获取 [税率金额]脏标记
     */
    boolean getPrice_taxDirtyFlag();

    /**
     * 签核的金额
     */
    Double getPrice_subtotal_signed();

    void setPrice_subtotal_signed(Double price_subtotal_signed);

    /**
     * 获取 [签核的金额]脏标记
     */
    boolean getPrice_subtotal_signedDirtyFlag();

    /**
     * 序号
     */
    Integer getSequence();

    void setSequence(Integer sequence);

    /**
     * 获取 [序号]脏标记
     */
    boolean getSequenceDirtyFlag();

    /**
     * 单价
     */
    Double getPrice_unit();

    void setPrice_unit(Double price_unit);

    /**
     * 获取 [单价]脏标记
     */
    boolean getPrice_unitDirtyFlag();

    /**
     * ID
     */
    Integer getId();

    void setId(Integer id);

    /**
     * 获取 [ID]脏标记
     */
    boolean getIdDirtyFlag();

    /**
     * 税率设置
     */
    String getInvoice_line_tax_ids();

    void setInvoice_line_tax_ids(String invoice_line_tax_ids);

    /**
     * 获取 [税率设置]脏标记
     */
    boolean getInvoice_line_tax_idsDirtyFlag();

    /**
     * 分析标签
     */
    String getAnalytic_tag_ids();

    void setAnalytic_tag_ids(String analytic_tag_ids);

    /**
     * 获取 [分析标签]脏标记
     */
    boolean getAnalytic_tag_idsDirtyFlag();

    /**
     * 舍入明细
     */
    String getIs_rounding_line();

    void setIs_rounding_line(String is_rounding_line);

    /**
     * 获取 [舍入明细]脏标记
     */
    boolean getIs_rounding_lineDirtyFlag();

    /**
     * 最后更新时间
     */
    Timestamp getWrite_date();

    void setWrite_date(Timestamp write_date);

    /**
     * 获取 [最后更新时间]脏标记
     */
    boolean getWrite_dateDirtyFlag();

    /**
     * 币种
     */
    String getCurrency_id_text();

    void setCurrency_id_text(String currency_id_text);

    /**
     * 获取 [币种]脏标记
     */
    boolean getCurrency_id_textDirtyFlag();

    /**
     * 分析账户
     */
    String getAccount_analytic_id_text();

    void setAccount_analytic_id_text(String account_analytic_id_text);

    /**
     * 获取 [分析账户]脏标记
     */
    boolean getAccount_analytic_id_textDirtyFlag();

    /**
     * 产品
     */
    String getProduct_id_text();

    void setProduct_id_text(String product_id_text);

    /**
     * 获取 [产品]脏标记
     */
    boolean getProduct_id_textDirtyFlag();

    /**
     * 发票参考
     */
    String getInvoice_id_text();

    void setInvoice_id_text(String invoice_id_text);

    /**
     * 获取 [发票参考]脏标记
     */
    boolean getInvoice_id_textDirtyFlag();

    /**
     * 业务伙伴
     */
    String getPartner_id_text();

    void setPartner_id_text(String partner_id_text);

    /**
     * 获取 [业务伙伴]脏标记
     */
    boolean getPartner_id_textDirtyFlag();

    /**
     * 创建人
     */
    String getCreate_uid_text();

    void setCreate_uid_text(String create_uid_text);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uid_textDirtyFlag();

    /**
     * 类型
     */
    String getInvoice_type();

    void setInvoice_type(String invoice_type);

    /**
     * 获取 [类型]脏标记
     */
    boolean getInvoice_typeDirtyFlag();

    /**
     * 产品图片
     */
    byte[] getProduct_image();

    void setProduct_image(byte[] product_image);

    /**
     * 获取 [产品图片]脏标记
     */
    boolean getProduct_imageDirtyFlag();

    /**
     * 计量单位
     */
    String getUom_id_text();

    void setUom_id_text(String uom_id_text);

    /**
     * 获取 [计量单位]脏标记
     */
    boolean getUom_id_textDirtyFlag();

    /**
     * 公司货币
     */
    Integer getCompany_currency_id();

    void setCompany_currency_id(Integer company_currency_id);

    /**
     * 获取 [公司货币]脏标记
     */
    boolean getCompany_currency_idDirtyFlag();

    /**
     * 采购订单
     */
    Integer getPurchase_id();

    void setPurchase_id(Integer purchase_id);

    /**
     * 获取 [采购订单]脏标记
     */
    boolean getPurchase_idDirtyFlag();

    /**
     * 科目
     */
    String getAccount_id_text();

    void setAccount_id_text(String account_id_text);

    /**
     * 获取 [科目]脏标记
     */
    boolean getAccount_id_textDirtyFlag();

    /**
     * 采购订单行
     */
    String getPurchase_line_id_text();

    void setPurchase_line_id_text(String purchase_line_id_text);

    /**
     * 获取 [采购订单行]脏标记
     */
    boolean getPurchase_line_id_textDirtyFlag();

    /**
     * 最后更新人
     */
    String getWrite_uid_text();

    void setWrite_uid_text(String write_uid_text);

    /**
     * 获取 [最后更新人]脏标记
     */
    boolean getWrite_uid_textDirtyFlag();

    /**
     * 公司
     */
    String getCompany_id_text();

    void setCompany_id_text(String company_id_text);

    /**
     * 获取 [公司]脏标记
     */
    boolean getCompany_id_textDirtyFlag();

    /**
     * 发票参考
     */
    Integer getInvoice_id();

    void setInvoice_id(Integer invoice_id);

    /**
     * 获取 [发票参考]脏标记
     */
    boolean getInvoice_idDirtyFlag();

    /**
     * 创建人
     */
    Integer getCreate_uid();

    void setCreate_uid(Integer create_uid);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uidDirtyFlag();

    /**
     * 产品
     */
    Integer getProduct_id();

    void setProduct_id(Integer product_id);

    /**
     * 获取 [产品]脏标记
     */
    boolean getProduct_idDirtyFlag();

    /**
     * 科目
     */
    Integer getAccount_id();

    void setAccount_id(Integer account_id);

    /**
     * 获取 [科目]脏标记
     */
    boolean getAccount_idDirtyFlag();

    /**
     * 采购订单行
     */
    Integer getPurchase_line_id();

    void setPurchase_line_id(Integer purchase_line_id);

    /**
     * 获取 [采购订单行]脏标记
     */
    boolean getPurchase_line_idDirtyFlag();

    /**
     * 分析账户
     */
    Integer getAccount_analytic_id();

    void setAccount_analytic_id(Integer account_analytic_id);

    /**
     * 获取 [分析账户]脏标记
     */
    boolean getAccount_analytic_idDirtyFlag();

    /**
     * 业务伙伴
     */
    Integer getPartner_id();

    void setPartner_id(Integer partner_id);

    /**
     * 获取 [业务伙伴]脏标记
     */
    boolean getPartner_idDirtyFlag();

    /**
     * 最后更新人
     */
    Integer getWrite_uid();

    void setWrite_uid(Integer write_uid);

    /**
     * 获取 [最后更新人]脏标记
     */
    boolean getWrite_uidDirtyFlag();

    /**
     * 币种
     */
    Integer getCurrency_id();

    void setCurrency_id(Integer currency_id);

    /**
     * 获取 [币种]脏标记
     */
    boolean getCurrency_idDirtyFlag();

    /**
     * 计量单位
     */
    Integer getUom_id();

    void setUom_id(Integer uom_id);

    /**
     * 获取 [计量单位]脏标记
     */
    boolean getUom_idDirtyFlag();

    /**
     * 公司
     */
    Integer getCompany_id();

    void setCompany_id(Integer company_id);

    /**
     * 获取 [公司]脏标记
     */
    boolean getCompany_idDirtyFlag();

}
