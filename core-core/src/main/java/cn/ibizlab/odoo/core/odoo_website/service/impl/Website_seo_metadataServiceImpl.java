package cn.ibizlab.odoo.core.odoo_website.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_website.domain.Website_seo_metadata;
import cn.ibizlab.odoo.core.odoo_website.filter.Website_seo_metadataSearchContext;
import cn.ibizlab.odoo.core.odoo_website.service.IWebsite_seo_metadataService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;



import cn.ibizlab.odoo.core.odoo_website.client.website_seo_metadataOdooClient;
import cn.ibizlab.odoo.core.odoo_website.clientmodel.website_seo_metadataClientModel;

/**
 * 实体[SEO元数据] 服务对象接口实现
 */
@Slf4j
@Service
public class Website_seo_metadataServiceImpl implements IWebsite_seo_metadataService {

    @Autowired
    website_seo_metadataOdooClient website_seo_metadataOdooClient;


    @Override
    public boolean create(Website_seo_metadata et) {
        website_seo_metadataClientModel clientModel = convert2Model(et,null);
		website_seo_metadataOdooClient.create(clientModel);
        Website_seo_metadata rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Website_seo_metadata> list){
    }

    @Override
    public boolean update(Website_seo_metadata et) {
        website_seo_metadataClientModel clientModel = convert2Model(et,null);
		website_seo_metadataOdooClient.update(clientModel);
        Website_seo_metadata rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }
    
    public void updateBatch(List<Website_seo_metadata> list){
    }

    @Override
    public Website_seo_metadata get(Integer id) {
        website_seo_metadataClientModel clientModel = new website_seo_metadataClientModel();
        clientModel.setId(id);
		website_seo_metadataOdooClient.get(clientModel);
        Website_seo_metadata et = convert2Domain(clientModel,null);
        if(et==null){
            et=new Website_seo_metadata();
            et.setId(id);
        }
        else{
        }

        return  et;
    }

    @Override
    public boolean remove(Integer id) {
        website_seo_metadataClientModel clientModel = new website_seo_metadataClientModel();
        clientModel.setId(id);
		website_seo_metadataOdooClient.remove(clientModel);
        return true; 
    }

    public void removeBatch(Collection<Integer> idList){
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Website_seo_metadata> searchDefault(Website_seo_metadataSearchContext context) {
        List<Website_seo_metadata> list = new ArrayList<Website_seo_metadata>();
        Page<website_seo_metadataClientModel> clientModelList = website_seo_metadataOdooClient.search(context);
        for(website_seo_metadataClientModel clientModel : clientModelList){
            list.add( this.convert2Domain(clientModel,null));
        }
        return new PageImpl<Website_seo_metadata>(list, context.getPageable(), clientModelList.getTotalElements());
    }

    public website_seo_metadataClientModel convert2Model(Website_seo_metadata domain , website_seo_metadataClientModel model) {
        if(domain == null)
            return model ;
        if(model==null){
            model = new website_seo_metadataClientModel();

        }
        if((Boolean) domain.getExtensionparams().get("dirtyflagenable")){
            if((Boolean) domain.getExtensionparams().get("display_namedirtyflag"))
                model.setDisplay_name(domain.getDisplayName());
            if((Boolean) domain.getExtensionparams().get("iddirtyflag"))
                model.setId(domain.getId());
            if((Boolean) domain.getExtensionparams().get("website_meta_og_imgdirtyflag"))
                model.setWebsite_meta_og_img(domain.getWebsiteMetaOgImg());
            if((Boolean) domain.getExtensionparams().get("website_meta_titledirtyflag"))
                model.setWebsite_meta_title(domain.getWebsiteMetaTitle());
            if((Boolean) domain.getExtensionparams().get("website_meta_keywordsdirtyflag"))
                model.setWebsite_meta_keywords(domain.getWebsiteMetaKeywords());
            if((Boolean) domain.getExtensionparams().get("__last_updatedirtyflag"))
                model.set__last_update(domain.getLastUpdate());
            if((Boolean) domain.getExtensionparams().get("website_meta_descriptiondirtyflag"))
                model.setWebsite_meta_description(domain.getWebsiteMetaDescription());
            if((Boolean) domain.getExtensionparams().get("is_seo_optimizeddirtyflag"))
                model.setIs_seo_optimized(domain.getIsSeoOptimized());
            domain.getFocusNull().clear();
        }
        return model ;
    }

    public Website_seo_metadata convert2Domain( website_seo_metadataClientModel model ,Website_seo_metadata domain) {
        if(model == null)
            return domain ;
        if(domain == null){
            domain = new Website_seo_metadata();
        }

        if(model.getDisplay_nameDirtyFlag())
            domain.setDisplayName(model.getDisplay_name());
        if(model.getIdDirtyFlag())
            domain.setId(model.getId());
        if(model.getWebsite_meta_og_imgDirtyFlag())
            domain.setWebsiteMetaOgImg(model.getWebsite_meta_og_img());
        if(model.getWebsite_meta_titleDirtyFlag())
            domain.setWebsiteMetaTitle(model.getWebsite_meta_title());
        if(model.getWebsite_meta_keywordsDirtyFlag())
            domain.setWebsiteMetaKeywords(model.getWebsite_meta_keywords());
        if(model.get__last_updateDirtyFlag())
            domain.setLastUpdate(model.get__last_update());
        if(model.getWebsite_meta_descriptionDirtyFlag())
            domain.setWebsiteMetaDescription(model.getWebsite_meta_description());
        if(model.getIs_seo_optimizedDirtyFlag())
            domain.setIsSeoOptimized(model.getIs_seo_optimized());
        return domain ;
    }

}

    



