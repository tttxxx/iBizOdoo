package cn.ibizlab.odoo.core.repository.po ;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.math.BigInteger;

import cn.ibizlab.odoo.core.odoo_account.filter.Account_chart_templateSearchContext;

/**
 * 实体 [科目表模版] 存储模型
 */
public interface Account_chart_template{

    /**
     * # 数字
     */
    Integer getCode_digits();

    void setCode_digits(Integer code_digits);

    /**
     * 获取 [# 数字]脏标记
     */
    boolean getCode_digitsDirtyFlag();

    /**
     * 主转账帐户的前缀
     */
    String getTransfer_account_code_prefix();

    void setTransfer_account_code_prefix(String transfer_account_code_prefix);

    /**
     * 获取 [主转账帐户的前缀]脏标记
     */
    boolean getTransfer_account_code_prefixDirtyFlag();

    /**
     * 银行科目的前缀
     */
    String getBank_account_code_prefix();

    void setBank_account_code_prefix(String bank_account_code_prefix);

    /**
     * 获取 [银行科目的前缀]脏标记
     */
    boolean getBank_account_code_prefixDirtyFlag();

    /**
     * 口语
     */
    String getSpoken_languages();

    void setSpoken_languages(String spoken_languages);

    /**
     * 获取 [口语]脏标记
     */
    boolean getSpoken_languagesDirtyFlag();

    /**
     * 税模板列表
     */
    String getTax_template_ids();

    void setTax_template_ids(String tax_template_ids);

    /**
     * 获取 [税模板列表]脏标记
     */
    boolean getTax_template_idsDirtyFlag();

    /**
     * 可显示？
     */
    String getVisible();

    void setVisible(String visible);

    /**
     * 获取 [可显示？]脏标记
     */
    boolean getVisibleDirtyFlag();

    /**
     * 关联的科目模板
     */
    String getAccount_ids();

    void setAccount_ids(String account_ids);

    /**
     * 获取 [关联的科目模板]脏标记
     */
    boolean getAccount_idsDirtyFlag();

    /**
     * 税率完整集合
     */
    String getComplete_tax_set();

    void setComplete_tax_set(String complete_tax_set);

    /**
     * 获取 [税率完整集合]脏标记
     */
    boolean getComplete_tax_setDirtyFlag();

    /**
     * ID
     */
    Integer getId();

    void setId(Integer id);

    /**
     * 获取 [ID]脏标记
     */
    boolean getIdDirtyFlag();

    /**
     * 主现金科目的前缀
     */
    String getCash_account_code_prefix();

    void setCash_account_code_prefix(String cash_account_code_prefix);

    /**
     * 获取 [主现金科目的前缀]脏标记
     */
    boolean getCash_account_code_prefixDirtyFlag();

    /**
     * 显示名称
     */
    String getDisplay_name();

    void setDisplay_name(String display_name);

    /**
     * 获取 [显示名称]脏标记
     */
    boolean getDisplay_nameDirtyFlag();

    /**
     * 创建时间
     */
    Timestamp getCreate_date();

    void setCreate_date(Timestamp create_date);

    /**
     * 获取 [创建时间]脏标记
     */
    boolean getCreate_dateDirtyFlag();

    /**
     * 使用anglo-saxon会计
     */
    String getUse_anglo_saxon();

    void setUse_anglo_saxon(String use_anglo_saxon);

    /**
     * 获取 [使用anglo-saxon会计]脏标记
     */
    boolean getUse_anglo_saxonDirtyFlag();

    /**
     * 最后修改日
     */
    Timestamp get__last_update();

    void set__last_update(Timestamp __last_update);

    /**
     * 获取 [最后修改日]脏标记
     */
    boolean get__last_updateDirtyFlag();

    /**
     * 最后更新时间
     */
    Timestamp getWrite_date();

    void setWrite_date(Timestamp write_date);

    /**
     * 获取 [最后更新时间]脏标记
     */
    boolean getWrite_dateDirtyFlag();

    /**
     * 名称
     */
    String getName();

    void setName(String name);

    /**
     * 获取 [名称]脏标记
     */
    boolean getNameDirtyFlag();

    /**
     * 汇率损失科目
     */
    String getExpense_currency_exchange_account_id_text();

    void setExpense_currency_exchange_account_id_text(String expense_currency_exchange_account_id_text);

    /**
     * 获取 [汇率损失科目]脏标记
     */
    boolean getExpense_currency_exchange_account_id_textDirtyFlag();

    /**
     * 应收科目
     */
    String getProperty_account_receivable_id_text();

    void setProperty_account_receivable_id_text(String property_account_receivable_id_text);

    /**
     * 获取 [应收科目]脏标记
     */
    boolean getProperty_account_receivable_id_textDirtyFlag();

    /**
     * 币种
     */
    String getCurrency_id_text();

    void setCurrency_id_text(String currency_id_text);

    /**
     * 获取 [币种]脏标记
     */
    boolean getCurrency_id_textDirtyFlag();

    /**
     * 产品模板的费用科目
     */
    String getProperty_account_expense_id_text();

    void setProperty_account_expense_id_text(String property_account_expense_id_text);

    /**
     * 获取 [产品模板的费用科目]脏标记
     */
    boolean getProperty_account_expense_id_textDirtyFlag();

    /**
     * 创建人
     */
    String getCreate_uid_text();

    void setCreate_uid_text(String create_uid_text);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uid_textDirtyFlag();

    /**
     * 上级表模板
     */
    String getParent_id_text();

    void setParent_id_text(String parent_id_text);

    /**
     * 获取 [上级表模板]脏标记
     */
    boolean getParent_id_textDirtyFlag();

    /**
     * 库存计价的入库科目
     */
    String getProperty_stock_account_input_categ_id_text();

    void setProperty_stock_account_input_categ_id_text(String property_stock_account_input_categ_id_text);

    /**
     * 获取 [库存计价的入库科目]脏标记
     */
    boolean getProperty_stock_account_input_categ_id_textDirtyFlag();

    /**
     * 产品模板的收入科目
     */
    String getProperty_account_income_id_text();

    void setProperty_account_income_id_text(String property_account_income_id_text);

    /**
     * 获取 [产品模板的收入科目]脏标记
     */
    boolean getProperty_account_income_id_textDirtyFlag();

    /**
     * 费用科目的类别
     */
    String getProperty_account_expense_categ_id_text();

    void setProperty_account_expense_categ_id_text(String property_account_expense_categ_id_text);

    /**
     * 获取 [费用科目的类别]脏标记
     */
    boolean getProperty_account_expense_categ_id_textDirtyFlag();

    /**
     * 最后更新人
     */
    String getWrite_uid_text();

    void setWrite_uid_text(String write_uid_text);

    /**
     * 获取 [最后更新人]脏标记
     */
    boolean getWrite_uid_textDirtyFlag();

    /**
     * 汇率增益科目
     */
    String getIncome_currency_exchange_account_id_text();

    void setIncome_currency_exchange_account_id_text(String income_currency_exchange_account_id_text);

    /**
     * 获取 [汇率增益科目]脏标记
     */
    boolean getIncome_currency_exchange_account_id_textDirtyFlag();

    /**
     * 应付科目
     */
    String getProperty_account_payable_id_text();

    void setProperty_account_payable_id_text(String property_account_payable_id_text);

    /**
     * 获取 [应付科目]脏标记
     */
    boolean getProperty_account_payable_id_textDirtyFlag();

    /**
     * 收入科目的类别
     */
    String getProperty_account_income_categ_id_text();

    void setProperty_account_income_categ_id_text(String property_account_income_categ_id_text);

    /**
     * 获取 [收入科目的类别]脏标记
     */
    boolean getProperty_account_income_categ_id_textDirtyFlag();

    /**
     * 库存计价的科目模板
     */
    String getProperty_stock_valuation_account_id_text();

    void setProperty_stock_valuation_account_id_text(String property_stock_valuation_account_id_text);

    /**
     * 获取 [库存计价的科目模板]脏标记
     */
    boolean getProperty_stock_valuation_account_id_textDirtyFlag();

    /**
     * 库存计价的出货科目
     */
    String getProperty_stock_account_output_categ_id_text();

    void setProperty_stock_account_output_categ_id_text(String property_stock_account_output_categ_id_text);

    /**
     * 获取 [库存计价的出货科目]脏标记
     */
    boolean getProperty_stock_account_output_categ_id_textDirtyFlag();

    /**
     * 库存计价的科目模板
     */
    Integer getProperty_stock_valuation_account_id();

    void setProperty_stock_valuation_account_id(Integer property_stock_valuation_account_id);

    /**
     * 获取 [库存计价的科目模板]脏标记
     */
    boolean getProperty_stock_valuation_account_idDirtyFlag();

    /**
     * 汇率损失科目
     */
    Integer getExpense_currency_exchange_account_id();

    void setExpense_currency_exchange_account_id(Integer expense_currency_exchange_account_id);

    /**
     * 获取 [汇率损失科目]脏标记
     */
    boolean getExpense_currency_exchange_account_idDirtyFlag();

    /**
     * 应付科目
     */
    Integer getProperty_account_payable_id();

    void setProperty_account_payable_id(Integer property_account_payable_id);

    /**
     * 获取 [应付科目]脏标记
     */
    boolean getProperty_account_payable_idDirtyFlag();

    /**
     * 收入科目的类别
     */
    Integer getProperty_account_income_categ_id();

    void setProperty_account_income_categ_id(Integer property_account_income_categ_id);

    /**
     * 获取 [收入科目的类别]脏标记
     */
    boolean getProperty_account_income_categ_idDirtyFlag();

    /**
     * 库存计价的出货科目
     */
    Integer getProperty_stock_account_output_categ_id();

    void setProperty_stock_account_output_categ_id(Integer property_stock_account_output_categ_id);

    /**
     * 获取 [库存计价的出货科目]脏标记
     */
    boolean getProperty_stock_account_output_categ_idDirtyFlag();

    /**
     * 产品模板的费用科目
     */
    Integer getProperty_account_expense_id();

    void setProperty_account_expense_id(Integer property_account_expense_id);

    /**
     * 获取 [产品模板的费用科目]脏标记
     */
    boolean getProperty_account_expense_idDirtyFlag();

    /**
     * 币种
     */
    Integer getCurrency_id();

    void setCurrency_id(Integer currency_id);

    /**
     * 获取 [币种]脏标记
     */
    boolean getCurrency_idDirtyFlag();

    /**
     * 上级表模板
     */
    Integer getParent_id();

    void setParent_id(Integer parent_id);

    /**
     * 获取 [上级表模板]脏标记
     */
    boolean getParent_idDirtyFlag();

    /**
     * 应收科目
     */
    Integer getProperty_account_receivable_id();

    void setProperty_account_receivable_id(Integer property_account_receivable_id);

    /**
     * 获取 [应收科目]脏标记
     */
    boolean getProperty_account_receivable_idDirtyFlag();

    /**
     * 最后更新人
     */
    Integer getWrite_uid();

    void setWrite_uid(Integer write_uid);

    /**
     * 获取 [最后更新人]脏标记
     */
    boolean getWrite_uidDirtyFlag();

    /**
     * 费用科目的类别
     */
    Integer getProperty_account_expense_categ_id();

    void setProperty_account_expense_categ_id(Integer property_account_expense_categ_id);

    /**
     * 获取 [费用科目的类别]脏标记
     */
    boolean getProperty_account_expense_categ_idDirtyFlag();

    /**
     * 汇率增益科目
     */
    Integer getIncome_currency_exchange_account_id();

    void setIncome_currency_exchange_account_id(Integer income_currency_exchange_account_id);

    /**
     * 获取 [汇率增益科目]脏标记
     */
    boolean getIncome_currency_exchange_account_idDirtyFlag();

    /**
     * 创建人
     */
    Integer getCreate_uid();

    void setCreate_uid(Integer create_uid);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uidDirtyFlag();

    /**
     * 库存计价的入库科目
     */
    Integer getProperty_stock_account_input_categ_id();

    void setProperty_stock_account_input_categ_id(Integer property_stock_account_input_categ_id);

    /**
     * 获取 [库存计价的入库科目]脏标记
     */
    boolean getProperty_stock_account_input_categ_idDirtyFlag();

    /**
     * 产品模板的收入科目
     */
    Integer getProperty_account_income_id();

    void setProperty_account_income_id(Integer property_account_income_id);

    /**
     * 获取 [产品模板的收入科目]脏标记
     */
    boolean getProperty_account_income_idDirtyFlag();

}
