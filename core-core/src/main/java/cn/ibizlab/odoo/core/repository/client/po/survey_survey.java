package cn.ibizlab.odoo.core.repository.client.po;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigDecimal;

/**
 * [survey_survey] 对象
 */
public interface survey_survey {

    public String getActive();

    public void setActive(String active);

    public Timestamp getActivity_date_deadline();

    public void setActivity_date_deadline(Timestamp activity_date_deadline);

    public String getActivity_ids();

    public void setActivity_ids(String activity_ids);

    public String getActivity_state();

    public void setActivity_state(String activity_state);

    public String getActivity_summary();

    public void setActivity_summary(String activity_summary);

    public Integer getActivity_type_id();

    public void setActivity_type_id(Integer activity_type_id);

    public String getActivity_type_id_text();

    public void setActivity_type_id_text(String activity_type_id_text);

    public Integer getActivity_user_id();

    public void setActivity_user_id(Integer activity_user_id);

    public String getActivity_user_id_text();

    public void setActivity_user_id_text(String activity_user_id_text);

    public String getAuth_required();

    public void setAuth_required(String auth_required);

    public Integer getColor();

    public void setColor(Integer color);

    public Timestamp getCreate_date();

    public void setCreate_date(Timestamp create_date);

    public Integer getCreate_uid();

    public void setCreate_uid(Integer create_uid);

    public String getCreate_uid_text();

    public void setCreate_uid_text(String create_uid_text);

    public String getDescription();

    public void setDescription(String description);

    public String getDesigned();

    public void setDesigned(String designed);

    public String getDisplay_name();

    public void setDisplay_name(String display_name);

    public Integer getEmail_template_id();

    public void setEmail_template_id(Integer email_template_id);

    public String getEmail_template_id_text();

    public void setEmail_template_id_text(String email_template_id_text);

    public Integer getId();

    public void setId(Integer id);

    public String getIs_closed();

    public void setIs_closed(String is_closed);

    public Integer getMessage_attachment_count();

    public void setMessage_attachment_count(Integer message_attachment_count);

    public String getMessage_channel_ids();

    public void setMessage_channel_ids(String message_channel_ids);

    public String getMessage_follower_ids();

    public void setMessage_follower_ids(String message_follower_ids);

    public String getMessage_has_error();

    public void setMessage_has_error(String message_has_error);

    public Integer getMessage_has_error_counter();

    public void setMessage_has_error_counter(Integer message_has_error_counter);

    public String getMessage_ids();

    public void setMessage_ids(String message_ids);

    public String getMessage_is_follower();

    public void setMessage_is_follower(String message_is_follower);

    public String getMessage_needaction();

    public void setMessage_needaction(String message_needaction);

    public Integer getMessage_needaction_counter();

    public void setMessage_needaction_counter(Integer message_needaction_counter);

    public String getMessage_partner_ids();

    public void setMessage_partner_ids(String message_partner_ids);

    public String getMessage_unread();

    public void setMessage_unread(String message_unread);

    public Integer getMessage_unread_counter();

    public void setMessage_unread_counter(Integer message_unread_counter);

    public String getPage_ids();

    public void setPage_ids(String page_ids);

    public String getPrint_url();

    public void setPrint_url(String print_url);

    public String getPublic_url();

    public void setPublic_url(String public_url);

    public String getPublic_url_html();

    public void setPublic_url_html(String public_url_html);

    public String getQuizz_mode();

    public void setQuizz_mode(String quizz_mode);

    public String getResult_url();

    public void setResult_url(String result_url);

    public Integer getStage_id();

    public void setStage_id(Integer stage_id);

    public String getStage_id_text();

    public void setStage_id_text(String stage_id_text);

    public String getThank_you_message();

    public void setThank_you_message(String thank_you_message);

    public String getTitle();

    public void setTitle(String title);

    public Integer getTot_comp_survey();

    public void setTot_comp_survey(Integer tot_comp_survey);

    public Integer getTot_sent_survey();

    public void setTot_sent_survey(Integer tot_sent_survey);

    public Integer getTot_start_survey();

    public void setTot_start_survey(Integer tot_start_survey);

    public String getUsers_can_go_back();

    public void setUsers_can_go_back(String users_can_go_back);

    public String getUser_input_ids();

    public void setUser_input_ids(String user_input_ids);

    public String getWebsite_message_ids();

    public void setWebsite_message_ids(String website_message_ids);

    public Timestamp getWrite_date();

    public void setWrite_date(Timestamp write_date);

    public Integer getWrite_uid();

    public void setWrite_uid(Integer write_uid);

    public String getWrite_uid_text();

    public void setWrite_uid_text(String write_uid_text);

    public Timestamp get__last_update();

    public void set__last_update(Timestamp __last_update);

}
