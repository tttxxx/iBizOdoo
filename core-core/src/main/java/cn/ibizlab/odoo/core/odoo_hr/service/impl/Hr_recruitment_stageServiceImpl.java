package cn.ibizlab.odoo.core.odoo_hr.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_hr.domain.Hr_recruitment_stage;
import cn.ibizlab.odoo.core.odoo_hr.filter.Hr_recruitment_stageSearchContext;
import cn.ibizlab.odoo.core.odoo_hr.service.IHr_recruitment_stageService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;



import cn.ibizlab.odoo.core.odoo_hr.client.hr_recruitment_stageOdooClient;
import cn.ibizlab.odoo.core.odoo_hr.clientmodel.hr_recruitment_stageClientModel;

/**
 * 实体[招聘阶段] 服务对象接口实现
 */
@Slf4j
@Service
public class Hr_recruitment_stageServiceImpl implements IHr_recruitment_stageService {

    @Autowired
    hr_recruitment_stageOdooClient hr_recruitment_stageOdooClient;


    @Override
    public Hr_recruitment_stage get(Integer id) {
        hr_recruitment_stageClientModel clientModel = new hr_recruitment_stageClientModel();
        clientModel.setId(id);
		hr_recruitment_stageOdooClient.get(clientModel);
        Hr_recruitment_stage et = convert2Domain(clientModel,null);
        if(et==null){
            et=new Hr_recruitment_stage();
            et.setId(id);
        }
        else{
        }

        return  et;
    }

    @Override
    public boolean create(Hr_recruitment_stage et) {
        hr_recruitment_stageClientModel clientModel = convert2Model(et,null);
		hr_recruitment_stageOdooClient.create(clientModel);
        Hr_recruitment_stage rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Hr_recruitment_stage> list){
    }

    @Override
    public boolean update(Hr_recruitment_stage et) {
        hr_recruitment_stageClientModel clientModel = convert2Model(et,null);
		hr_recruitment_stageOdooClient.update(clientModel);
        Hr_recruitment_stage rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }
    
    public void updateBatch(List<Hr_recruitment_stage> list){
    }

    @Override
    public boolean remove(Integer id) {
        hr_recruitment_stageClientModel clientModel = new hr_recruitment_stageClientModel();
        clientModel.setId(id);
		hr_recruitment_stageOdooClient.remove(clientModel);
        return true; 
    }

    public void removeBatch(Collection<Integer> idList){
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Hr_recruitment_stage> searchDefault(Hr_recruitment_stageSearchContext context) {
        List<Hr_recruitment_stage> list = new ArrayList<Hr_recruitment_stage>();
        Page<hr_recruitment_stageClientModel> clientModelList = hr_recruitment_stageOdooClient.search(context);
        for(hr_recruitment_stageClientModel clientModel : clientModelList){
            list.add( this.convert2Domain(clientModel,null));
        }
        return new PageImpl<Hr_recruitment_stage>(list, context.getPageable(), clientModelList.getTotalElements());
    }

    public hr_recruitment_stageClientModel convert2Model(Hr_recruitment_stage domain , hr_recruitment_stageClientModel model) {
        if(domain == null)
            return model ;
        if(model==null){
            model = new hr_recruitment_stageClientModel();

        }
        if((Boolean) domain.getExtensionparams().get("dirtyflagenable")){
            if((Boolean) domain.getExtensionparams().get("write_datedirtyflag"))
                model.setWrite_date(domain.getWriteDate());
            if((Boolean) domain.getExtensionparams().get("requirementsdirtyflag"))
                model.setRequirements(domain.getRequirements());
            if((Boolean) domain.getExtensionparams().get("legend_normaldirtyflag"))
                model.setLegend_normal(domain.getLegendNormal());
            if((Boolean) domain.getExtensionparams().get("__last_updatedirtyflag"))
                model.set__last_update(domain.getLastUpdate());
            if((Boolean) domain.getExtensionparams().get("namedirtyflag"))
                model.setName(domain.getName());
            if((Boolean) domain.getExtensionparams().get("folddirtyflag"))
                model.setFold(domain.getFold());
            if((Boolean) domain.getExtensionparams().get("sequencedirtyflag"))
                model.setSequence(domain.getSequence());
            if((Boolean) domain.getExtensionparams().get("legend_donedirtyflag"))
                model.setLegend_done(domain.getLegendDone());
            if((Boolean) domain.getExtensionparams().get("display_namedirtyflag"))
                model.setDisplay_name(domain.getDisplayName());
            if((Boolean) domain.getExtensionparams().get("legend_blockeddirtyflag"))
                model.setLegend_blocked(domain.getLegendBlocked());
            if((Boolean) domain.getExtensionparams().get("iddirtyflag"))
                model.setId(domain.getId());
            if((Boolean) domain.getExtensionparams().get("create_datedirtyflag"))
                model.setCreate_date(domain.getCreateDate());
            if((Boolean) domain.getExtensionparams().get("template_id_textdirtyflag"))
                model.setTemplate_id_text(domain.getTemplateIdText());
            if((Boolean) domain.getExtensionparams().get("write_uid_textdirtyflag"))
                model.setWrite_uid_text(domain.getWriteUidText());
            if((Boolean) domain.getExtensionparams().get("job_id_textdirtyflag"))
                model.setJob_id_text(domain.getJobIdText());
            if((Boolean) domain.getExtensionparams().get("create_uid_textdirtyflag"))
                model.setCreate_uid_text(domain.getCreateUidText());
            if((Boolean) domain.getExtensionparams().get("write_uiddirtyflag"))
                model.setWrite_uid(domain.getWriteUid());
            if((Boolean) domain.getExtensionparams().get("create_uiddirtyflag"))
                model.setCreate_uid(domain.getCreateUid());
            if((Boolean) domain.getExtensionparams().get("job_iddirtyflag"))
                model.setJob_id(domain.getJobId());
            if((Boolean) domain.getExtensionparams().get("template_iddirtyflag"))
                model.setTemplate_id(domain.getTemplateId());
            domain.getFocusNull().clear();
        }
        return model ;
    }

    public Hr_recruitment_stage convert2Domain( hr_recruitment_stageClientModel model ,Hr_recruitment_stage domain) {
        if(model == null)
            return domain ;
        if(domain == null){
            domain = new Hr_recruitment_stage();
        }

        if(model.getWrite_dateDirtyFlag())
            domain.setWriteDate(model.getWrite_date());
        if(model.getRequirementsDirtyFlag())
            domain.setRequirements(model.getRequirements());
        if(model.getLegend_normalDirtyFlag())
            domain.setLegendNormal(model.getLegend_normal());
        if(model.get__last_updateDirtyFlag())
            domain.setLastUpdate(model.get__last_update());
        if(model.getNameDirtyFlag())
            domain.setName(model.getName());
        if(model.getFoldDirtyFlag())
            domain.setFold(model.getFold());
        if(model.getSequenceDirtyFlag())
            domain.setSequence(model.getSequence());
        if(model.getLegend_doneDirtyFlag())
            domain.setLegendDone(model.getLegend_done());
        if(model.getDisplay_nameDirtyFlag())
            domain.setDisplayName(model.getDisplay_name());
        if(model.getLegend_blockedDirtyFlag())
            domain.setLegendBlocked(model.getLegend_blocked());
        if(model.getIdDirtyFlag())
            domain.setId(model.getId());
        if(model.getCreate_dateDirtyFlag())
            domain.setCreateDate(model.getCreate_date());
        if(model.getTemplate_id_textDirtyFlag())
            domain.setTemplateIdText(model.getTemplate_id_text());
        if(model.getWrite_uid_textDirtyFlag())
            domain.setWriteUidText(model.getWrite_uid_text());
        if(model.getJob_id_textDirtyFlag())
            domain.setJobIdText(model.getJob_id_text());
        if(model.getCreate_uid_textDirtyFlag())
            domain.setCreateUidText(model.getCreate_uid_text());
        if(model.getWrite_uidDirtyFlag())
            domain.setWriteUid(model.getWrite_uid());
        if(model.getCreate_uidDirtyFlag())
            domain.setCreateUid(model.getCreate_uid());
        if(model.getJob_idDirtyFlag())
            domain.setJobId(model.getJob_id());
        if(model.getTemplate_idDirtyFlag())
            domain.setTemplateId(model.getTemplate_id());
        return domain ;
    }

}

    



