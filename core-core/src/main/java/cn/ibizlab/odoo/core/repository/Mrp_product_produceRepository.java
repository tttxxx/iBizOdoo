package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Mrp_product_produce;
import cn.ibizlab.odoo.core.odoo_mrp.filter.Mrp_product_produceSearchContext;

/**
 * 实体 [记录生产] 存储对象
 */
public interface Mrp_product_produceRepository extends Repository<Mrp_product_produce> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Mrp_product_produce> searchDefault(Mrp_product_produceSearchContext context);

    Mrp_product_produce convert2PO(cn.ibizlab.odoo.core.odoo_mrp.domain.Mrp_product_produce domain , Mrp_product_produce po) ;

    cn.ibizlab.odoo.core.odoo_mrp.domain.Mrp_product_produce convert2Domain( Mrp_product_produce po ,cn.ibizlab.odoo.core.odoo_mrp.domain.Mrp_product_produce domain) ;

}
