package cn.ibizlab.odoo.core.repository.client.po;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigDecimal;

/**
 * [hr_recruitment_source] 对象
 */
public interface hr_recruitment_source {

    public Integer getAlias_id();

    public void setAlias_id(Integer alias_id);

    public Timestamp getCreate_date();

    public void setCreate_date(Timestamp create_date);

    public Integer getCreate_uid();

    public void setCreate_uid(Integer create_uid);

    public String getCreate_uid_text();

    public void setCreate_uid_text(String create_uid_text);

    public String getDisplay_name();

    public void setDisplay_name(String display_name);

    public String getEmail();

    public void setEmail(String email);

    public Integer getId();

    public void setId(Integer id);

    public Integer getJob_id();

    public void setJob_id(Integer job_id);

    public String getJob_id_text();

    public void setJob_id_text(String job_id_text);

    public String getName();

    public void setName(String name);

    public Integer getSource_id();

    public void setSource_id(Integer source_id);

    public String getSource_id_text();

    public void setSource_id_text(String source_id_text);

    public String getUrl();

    public void setUrl(String url);

    public Timestamp getWrite_date();

    public void setWrite_date(Timestamp write_date);

    public Integer getWrite_uid();

    public void setWrite_uid(Integer write_uid);

    public String getWrite_uid_text();

    public void setWrite_uid_text(String write_uid_text);

    public Timestamp get__last_update();

    public void set__last_update(Timestamp __last_update);

}
