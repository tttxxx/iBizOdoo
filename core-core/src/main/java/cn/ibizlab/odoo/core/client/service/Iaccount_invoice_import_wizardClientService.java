package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Iaccount_invoice_import_wizard;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[account_invoice_import_wizard] 服务对象接口
 */
public interface Iaccount_invoice_import_wizardClientService{

    public Iaccount_invoice_import_wizard createModel() ;

    public void updateBatch(List<Iaccount_invoice_import_wizard> account_invoice_import_wizards);

    public void remove(Iaccount_invoice_import_wizard account_invoice_import_wizard);

    public void create(Iaccount_invoice_import_wizard account_invoice_import_wizard);

    public void removeBatch(List<Iaccount_invoice_import_wizard> account_invoice_import_wizards);

    public void createBatch(List<Iaccount_invoice_import_wizard> account_invoice_import_wizards);

    public void get(Iaccount_invoice_import_wizard account_invoice_import_wizard);

    public void update(Iaccount_invoice_import_wizard account_invoice_import_wizard);

    public Page<Iaccount_invoice_import_wizard> search(SearchContext context);

    public Page<Iaccount_invoice_import_wizard> select(SearchContext context);

}
