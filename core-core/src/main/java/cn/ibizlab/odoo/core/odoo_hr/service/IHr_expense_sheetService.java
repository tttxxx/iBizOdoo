package cn.ibizlab.odoo.core.odoo_hr.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_hr.domain.Hr_expense_sheet;
import cn.ibizlab.odoo.core.odoo_hr.filter.Hr_expense_sheetSearchContext;


/**
 * 实体[Hr_expense_sheet] 服务对象接口
 */
public interface IHr_expense_sheetService{

    boolean update(Hr_expense_sheet et) ;
    void updateBatch(List<Hr_expense_sheet> list) ;
    Hr_expense_sheet get(Integer key) ;
    boolean create(Hr_expense_sheet et) ;
    void createBatch(List<Hr_expense_sheet> list) ;
    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    Page<Hr_expense_sheet> searchDefault(Hr_expense_sheetSearchContext context) ;

}



