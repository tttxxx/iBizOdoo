package cn.ibizlab.odoo.core.repository.po ;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.math.BigInteger;

import cn.ibizlab.odoo.core.odoo_fetchmail.filter.Fetchmail_serverSearchContext;

/**
 * 实体 [Incoming Mail Server] 存储模型
 */
public interface Fetchmail_server{

    /**
     * 用户名
     */
    String getUser();

    void setUser(String user);

    /**
     * 获取 [用户名]脏标记
     */
    boolean getUserDirtyFlag();

    /**
     * 显示名称
     */
    String getDisplay_name();

    void setDisplay_name(String display_name);

    /**
     * 获取 [显示名称]脏标记
     */
    boolean getDisplay_nameDirtyFlag();

    /**
     * 脚本
     */
    String getScript();

    void setScript(String script);

    /**
     * 获取 [脚本]脏标记
     */
    boolean getScriptDirtyFlag();

    /**
     * 配置
     */
    String getConfiguration();

    void setConfiguration(String configuration);

    /**
     * 获取 [配置]脏标记
     */
    boolean getConfigurationDirtyFlag();

    /**
     * 最后收取日期
     */
    Timestamp getDate();

    void setDate(Timestamp date);

    /**
     * 获取 [最后收取日期]脏标记
     */
    boolean getDateDirtyFlag();

    /**
     * 服务器类型
     */
    String getType();

    void setType(String type);

    /**
     * 获取 [服务器类型]脏标记
     */
    boolean getTypeDirtyFlag();

    /**
     * 密码
     */
    String getPassword();

    void setPassword(String password);

    /**
     * 获取 [密码]脏标记
     */
    boolean getPasswordDirtyFlag();

    /**
     * 消息
     */
    String getMessage_ids();

    void setMessage_ids(String message_ids);

    /**
     * 获取 [消息]脏标记
     */
    boolean getMessage_idsDirtyFlag();

    /**
     * 创建时间
     */
    Timestamp getCreate_date();

    void setCreate_date(Timestamp create_date);

    /**
     * 获取 [创建时间]脏标记
     */
    boolean getCreate_dateDirtyFlag();

    /**
     * 服务器名称
     */
    String getServer();

    void setServer(String server);

    /**
     * 获取 [服务器名称]脏标记
     */
    boolean getServerDirtyFlag();

    /**
     * 端口
     */
    Integer getPort();

    void setPort(Integer port);

    /**
     * 获取 [端口]脏标记
     */
    boolean getPortDirtyFlag();

    /**
     * SSL/TLS
     */
    String getIs_ssl();

    void setIs_ssl(String is_ssl);

    /**
     * 获取 [SSL/TLS]脏标记
     */
    boolean getIs_sslDirtyFlag();

    /**
     * 名称
     */
    String getName();

    void setName(String name);

    /**
     * 获取 [名称]脏标记
     */
    boolean getNameDirtyFlag();

    /**
     * 服务器优先级
     */
    Integer getPriority();

    void setPriority(Integer priority);

    /**
     * 获取 [服务器优先级]脏标记
     */
    boolean getPriorityDirtyFlag();

    /**
     * 最后修改日
     */
    Timestamp get__last_update();

    void set__last_update(Timestamp __last_update);

    /**
     * 获取 [最后修改日]脏标记
     */
    boolean get__last_updateDirtyFlag();

    /**
     * 创建新记录
     */
    Integer getObject_id();

    void setObject_id(Integer object_id);

    /**
     * 获取 [创建新记录]脏标记
     */
    boolean getObject_idDirtyFlag();

    /**
     * ID
     */
    Integer getId();

    void setId(Integer id);

    /**
     * 获取 [ID]脏标记
     */
    boolean getIdDirtyFlag();

    /**
     * 状态
     */
    String getState();

    void setState(String state);

    /**
     * 获取 [状态]脏标记
     */
    boolean getStateDirtyFlag();

    /**
     * 有效
     */
    String getActive();

    void setActive(String active);

    /**
     * 获取 [有效]脏标记
     */
    boolean getActiveDirtyFlag();

    /**
     * 最后更新时间
     */
    Timestamp getWrite_date();

    void setWrite_date(Timestamp write_date);

    /**
     * 获取 [最后更新时间]脏标记
     */
    boolean getWrite_dateDirtyFlag();

    /**
     * 保存附件
     */
    String getAttach();

    void setAttach(String attach);

    /**
     * 获取 [保存附件]脏标记
     */
    boolean getAttachDirtyFlag();

    /**
     * 保留原件
     */
    String getOriginal();

    void setOriginal(String original);

    /**
     * 获取 [保留原件]脏标记
     */
    boolean getOriginalDirtyFlag();

    /**
     * 创建人
     */
    String getCreate_uid_text();

    void setCreate_uid_text(String create_uid_text);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uid_textDirtyFlag();

    /**
     * 最后更新人
     */
    String getWrite_uid_text();

    void setWrite_uid_text(String write_uid_text);

    /**
     * 获取 [最后更新人]脏标记
     */
    boolean getWrite_uid_textDirtyFlag();

    /**
     * 创建人
     */
    Integer getCreate_uid();

    void setCreate_uid(Integer create_uid);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uidDirtyFlag();

    /**
     * 最后更新人
     */
    Integer getWrite_uid();

    void setWrite_uid(Integer write_uid);

    /**
     * 获取 [最后更新人]脏标记
     */
    boolean getWrite_uidDirtyFlag();

}
