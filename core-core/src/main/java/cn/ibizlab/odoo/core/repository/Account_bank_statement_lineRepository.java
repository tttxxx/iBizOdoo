package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Account_bank_statement_line;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_bank_statement_lineSearchContext;

/**
 * 实体 [银行对账单明细] 存储对象
 */
public interface Account_bank_statement_lineRepository extends Repository<Account_bank_statement_line> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Account_bank_statement_line> searchDefault(Account_bank_statement_lineSearchContext context);

    Account_bank_statement_line convert2PO(cn.ibizlab.odoo.core.odoo_account.domain.Account_bank_statement_line domain , Account_bank_statement_line po) ;

    cn.ibizlab.odoo.core.odoo_account.domain.Account_bank_statement_line convert2Domain( Account_bank_statement_line po ,cn.ibizlab.odoo.core.odoo_account.domain.Account_bank_statement_line domain) ;

}
