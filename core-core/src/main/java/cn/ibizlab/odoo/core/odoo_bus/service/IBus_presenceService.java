package cn.ibizlab.odoo.core.odoo_bus.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_bus.domain.Bus_presence;
import cn.ibizlab.odoo.core.odoo_bus.filter.Bus_presenceSearchContext;


/**
 * 实体[Bus_presence] 服务对象接口
 */
public interface IBus_presenceService{

    Bus_presence get(Integer key) ;
    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    boolean update(Bus_presence et) ;
    void updateBatch(List<Bus_presence> list) ;
    boolean create(Bus_presence et) ;
    void createBatch(List<Bus_presence> list) ;
    Page<Bus_presence> searchDefault(Bus_presenceSearchContext context) ;

}



