package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Account_account;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_accountSearchContext;

/**
 * 实体 [科目] 存储对象
 */
public interface Account_accountRepository extends Repository<Account_account> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Account_account> searchDefault(Account_accountSearchContext context);

    Account_account convert2PO(cn.ibizlab.odoo.core.odoo_account.domain.Account_account domain , Account_account po) ;

    cn.ibizlab.odoo.core.odoo_account.domain.Account_account convert2Domain( Account_account po ,cn.ibizlab.odoo.core.odoo_account.domain.Account_account domain) ;

}
