package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Stock_warn_insufficient_qty_repair;
import cn.ibizlab.odoo.core.odoo_stock.filter.Stock_warn_insufficient_qty_repairSearchContext;

/**
 * 实体 [警告维修数量不足] 存储对象
 */
public interface Stock_warn_insufficient_qty_repairRepository extends Repository<Stock_warn_insufficient_qty_repair> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Stock_warn_insufficient_qty_repair> searchDefault(Stock_warn_insufficient_qty_repairSearchContext context);

    Stock_warn_insufficient_qty_repair convert2PO(cn.ibizlab.odoo.core.odoo_stock.domain.Stock_warn_insufficient_qty_repair domain , Stock_warn_insufficient_qty_repair po) ;

    cn.ibizlab.odoo.core.odoo_stock.domain.Stock_warn_insufficient_qty_repair convert2Domain( Stock_warn_insufficient_qty_repair po ,cn.ibizlab.odoo.core.odoo_stock.domain.Stock_warn_insufficient_qty_repair domain) ;

}
