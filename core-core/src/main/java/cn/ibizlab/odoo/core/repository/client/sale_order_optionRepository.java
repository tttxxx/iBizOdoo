package cn.ibizlab.odoo.core.repository.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import cn.ibizlab.odoo.core.repository.client.po.sale_order_option;

/**
 * 实体[sale_order_option] 服务对象接口
 */
public interface sale_order_optionRepository{


    public sale_order_option createPO() ;
        public void remove(String id);

        public void createBatch(sale_order_option sale_order_option);

        public void removeBatch(String id);

        public void update(sale_order_option sale_order_option);

        public void updateBatch(sale_order_option sale_order_option);

        public void get(String id);

        public List<sale_order_option> search();

        public void create(sale_order_option sale_order_option);


}
