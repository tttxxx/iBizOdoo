package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Event_event;
import cn.ibizlab.odoo.core.odoo_event.filter.Event_eventSearchContext;

/**
 * 实体 [活动] 存储对象
 */
public interface Event_eventRepository extends Repository<Event_event> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Event_event> searchDefault(Event_eventSearchContext context);

    Event_event convert2PO(cn.ibizlab.odoo.core.odoo_event.domain.Event_event domain , Event_event po) ;

    cn.ibizlab.odoo.core.odoo_event.domain.Event_event convert2Domain( Event_event po ,cn.ibizlab.odoo.core.odoo_event.domain.Event_event domain) ;

}
