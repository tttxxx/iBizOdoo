package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Gamification_goal_wizard;
import cn.ibizlab.odoo.core.odoo_gamification.filter.Gamification_goal_wizardSearchContext;

/**
 * 实体 [游戏化目标向导] 存储对象
 */
public interface Gamification_goal_wizardRepository extends Repository<Gamification_goal_wizard> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Gamification_goal_wizard> searchDefault(Gamification_goal_wizardSearchContext context);

    Gamification_goal_wizard convert2PO(cn.ibizlab.odoo.core.odoo_gamification.domain.Gamification_goal_wizard domain , Gamification_goal_wizard po) ;

    cn.ibizlab.odoo.core.odoo_gamification.domain.Gamification_goal_wizard convert2Domain( Gamification_goal_wizard po ,cn.ibizlab.odoo.core.odoo_gamification.domain.Gamification_goal_wizard domain) ;

}
