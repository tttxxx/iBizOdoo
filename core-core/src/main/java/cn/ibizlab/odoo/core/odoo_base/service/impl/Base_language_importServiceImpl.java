package cn.ibizlab.odoo.core.odoo_base.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_base.domain.Base_language_import;
import cn.ibizlab.odoo.core.odoo_base.filter.Base_language_importSearchContext;
import cn.ibizlab.odoo.core.odoo_base.service.IBase_language_importService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;



import cn.ibizlab.odoo.core.odoo_base.client.base_language_importOdooClient;
import cn.ibizlab.odoo.core.odoo_base.clientmodel.base_language_importClientModel;

/**
 * 实体[语言导入] 服务对象接口实现
 */
@Slf4j
@Service
public class Base_language_importServiceImpl implements IBase_language_importService {

    @Autowired
    base_language_importOdooClient base_language_importOdooClient;


    @Override
    public boolean create(Base_language_import et) {
        base_language_importClientModel clientModel = convert2Model(et,null);
		base_language_importOdooClient.create(clientModel);
        Base_language_import rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Base_language_import> list){
    }

    @Override
    public boolean remove(Integer id) {
        base_language_importClientModel clientModel = new base_language_importClientModel();
        clientModel.setId(id);
		base_language_importOdooClient.remove(clientModel);
        return true; 
    }

    public void removeBatch(Collection<Integer> idList){
    }

    @Override
    public boolean update(Base_language_import et) {
        base_language_importClientModel clientModel = convert2Model(et,null);
		base_language_importOdooClient.update(clientModel);
        Base_language_import rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }
    
    public void updateBatch(List<Base_language_import> list){
    }

    @Override
    public Base_language_import get(Integer id) {
        base_language_importClientModel clientModel = new base_language_importClientModel();
        clientModel.setId(id);
		base_language_importOdooClient.get(clientModel);
        Base_language_import et = convert2Domain(clientModel,null);
        if(et==null){
            et=new Base_language_import();
            et.setId(id);
        }
        else{
        }

        return  et;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Base_language_import> searchDefault(Base_language_importSearchContext context) {
        List<Base_language_import> list = new ArrayList<Base_language_import>();
        Page<base_language_importClientModel> clientModelList = base_language_importOdooClient.search(context);
        for(base_language_importClientModel clientModel : clientModelList){
            list.add( this.convert2Domain(clientModel,null));
        }
        return new PageImpl<Base_language_import>(list, context.getPageable(), clientModelList.getTotalElements());
    }

    public base_language_importClientModel convert2Model(Base_language_import domain , base_language_importClientModel model) {
        if(domain == null)
            return model ;
        if(model==null){
            model = new base_language_importClientModel();

        }
        if((Boolean) domain.getExtensionparams().get("dirtyflagenable")){
            if((Boolean) domain.getExtensionparams().get("display_namedirtyflag"))
                model.setDisplay_name(domain.getDisplayName());
            if((Boolean) domain.getExtensionparams().get("filenamedirtyflag"))
                model.setFilename(domain.getFilename());
            if((Boolean) domain.getExtensionparams().get("datadirtyflag"))
                model.setData(domain.getData());
            if((Boolean) domain.getExtensionparams().get("namedirtyflag"))
                model.setName(domain.getName());
            if((Boolean) domain.getExtensionparams().get("write_datedirtyflag"))
                model.setWrite_date(domain.getWriteDate());
            if((Boolean) domain.getExtensionparams().get("__last_updatedirtyflag"))
                model.set__last_update(domain.getLastUpdate());
            if((Boolean) domain.getExtensionparams().get("codedirtyflag"))
                model.setCode(domain.getCode());
            if((Boolean) domain.getExtensionparams().get("create_datedirtyflag"))
                model.setCreate_date(domain.getCreateDate());
            if((Boolean) domain.getExtensionparams().get("iddirtyflag"))
                model.setId(domain.getId());
            if((Boolean) domain.getExtensionparams().get("overwritedirtyflag"))
                model.setOverwrite(domain.getOverwrite());
            if((Boolean) domain.getExtensionparams().get("write_uid_textdirtyflag"))
                model.setWrite_uid_text(domain.getWriteUidText());
            if((Boolean) domain.getExtensionparams().get("create_uid_textdirtyflag"))
                model.setCreate_uid_text(domain.getCreateUidText());
            if((Boolean) domain.getExtensionparams().get("create_uiddirtyflag"))
                model.setCreate_uid(domain.getCreateUid());
            if((Boolean) domain.getExtensionparams().get("write_uiddirtyflag"))
                model.setWrite_uid(domain.getWriteUid());
            domain.getFocusNull().clear();
        }
        return model ;
    }

    public Base_language_import convert2Domain( base_language_importClientModel model ,Base_language_import domain) {
        if(model == null)
            return domain ;
        if(domain == null){
            domain = new Base_language_import();
        }

        if(model.getDisplay_nameDirtyFlag())
            domain.setDisplayName(model.getDisplay_name());
        if(model.getFilenameDirtyFlag())
            domain.setFilename(model.getFilename());
        if(model.getDataDirtyFlag())
            domain.setData(model.getData());
        if(model.getNameDirtyFlag())
            domain.setName(model.getName());
        if(model.getWrite_dateDirtyFlag())
            domain.setWriteDate(model.getWrite_date());
        if(model.get__last_updateDirtyFlag())
            domain.setLastUpdate(model.get__last_update());
        if(model.getCodeDirtyFlag())
            domain.setCode(model.getCode());
        if(model.getCreate_dateDirtyFlag())
            domain.setCreateDate(model.getCreate_date());
        if(model.getIdDirtyFlag())
            domain.setId(model.getId());
        if(model.getOverwriteDirtyFlag())
            domain.setOverwrite(model.getOverwrite());
        if(model.getWrite_uid_textDirtyFlag())
            domain.setWriteUidText(model.getWrite_uid_text());
        if(model.getCreate_uid_textDirtyFlag())
            domain.setCreateUidText(model.getCreate_uid_text());
        if(model.getCreate_uidDirtyFlag())
            domain.setCreateUid(model.getCreate_uid());
        if(model.getWrite_uidDirtyFlag())
            domain.setWriteUid(model.getWrite_uid());
        return domain ;
    }

}

    



