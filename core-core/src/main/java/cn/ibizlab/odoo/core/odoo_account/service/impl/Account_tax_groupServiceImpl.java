package cn.ibizlab.odoo.core.odoo_account.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_account.domain.Account_tax_group;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_tax_groupSearchContext;
import cn.ibizlab.odoo.core.odoo_account.service.IAccount_tax_groupService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;



import cn.ibizlab.odoo.core.odoo_account.client.account_tax_groupOdooClient;
import cn.ibizlab.odoo.core.odoo_account.clientmodel.account_tax_groupClientModel;

/**
 * 实体[税组] 服务对象接口实现
 */
@Slf4j
@Service
public class Account_tax_groupServiceImpl implements IAccount_tax_groupService {

    @Autowired
    account_tax_groupOdooClient account_tax_groupOdooClient;


    @Override
    public boolean update(Account_tax_group et) {
        account_tax_groupClientModel clientModel = convert2Model(et,null);
		account_tax_groupOdooClient.update(clientModel);
        Account_tax_group rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }
    
    public void updateBatch(List<Account_tax_group> list){
    }

    @Override
    public boolean remove(Integer id) {
        account_tax_groupClientModel clientModel = new account_tax_groupClientModel();
        clientModel.setId(id);
		account_tax_groupOdooClient.remove(clientModel);
        return true; 
    }

    public void removeBatch(Collection<Integer> idList){
    }

    @Override
    public Account_tax_group get(Integer id) {
        account_tax_groupClientModel clientModel = new account_tax_groupClientModel();
        clientModel.setId(id);
		account_tax_groupOdooClient.get(clientModel);
        Account_tax_group et = convert2Domain(clientModel,null);
        if(et==null){
            et=new Account_tax_group();
            et.setId(id);
        }
        else{
        }

        return  et;
    }

    @Override
    public boolean create(Account_tax_group et) {
        account_tax_groupClientModel clientModel = convert2Model(et,null);
		account_tax_groupOdooClient.create(clientModel);
        Account_tax_group rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Account_tax_group> list){
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Account_tax_group> searchDefault(Account_tax_groupSearchContext context) {
        List<Account_tax_group> list = new ArrayList<Account_tax_group>();
        Page<account_tax_groupClientModel> clientModelList = account_tax_groupOdooClient.search(context);
        for(account_tax_groupClientModel clientModel : clientModelList){
            list.add( this.convert2Domain(clientModel,null));
        }
        return new PageImpl<Account_tax_group>(list, context.getPageable(), clientModelList.getTotalElements());
    }

    public account_tax_groupClientModel convert2Model(Account_tax_group domain , account_tax_groupClientModel model) {
        if(domain == null)
            return model ;
        if(model==null){
            model = new account_tax_groupClientModel();

        }
        if((Boolean) domain.getExtensionparams().get("dirtyflagenable")){
            if((Boolean) domain.getExtensionparams().get("write_datedirtyflag"))
                model.setWrite_date(domain.getWriteDate());
            if((Boolean) domain.getExtensionparams().get("sequencedirtyflag"))
                model.setSequence(domain.getSequence());
            if((Boolean) domain.getExtensionparams().get("namedirtyflag"))
                model.setName(domain.getName());
            if((Boolean) domain.getExtensionparams().get("__last_updatedirtyflag"))
                model.set__last_update(domain.getLastUpdate());
            if((Boolean) domain.getExtensionparams().get("iddirtyflag"))
                model.setId(domain.getId());
            if((Boolean) domain.getExtensionparams().get("create_datedirtyflag"))
                model.setCreate_date(domain.getCreateDate());
            if((Boolean) domain.getExtensionparams().get("display_namedirtyflag"))
                model.setDisplay_name(domain.getDisplayName());
            if((Boolean) domain.getExtensionparams().get("create_uid_textdirtyflag"))
                model.setCreate_uid_text(domain.getCreateUidText());
            if((Boolean) domain.getExtensionparams().get("write_uid_textdirtyflag"))
                model.setWrite_uid_text(domain.getWriteUidText());
            if((Boolean) domain.getExtensionparams().get("create_uiddirtyflag"))
                model.setCreate_uid(domain.getCreateUid());
            if((Boolean) domain.getExtensionparams().get("write_uiddirtyflag"))
                model.setWrite_uid(domain.getWriteUid());
            domain.getFocusNull().clear();
        }
        return model ;
    }

    public Account_tax_group convert2Domain( account_tax_groupClientModel model ,Account_tax_group domain) {
        if(model == null)
            return domain ;
        if(domain == null){
            domain = new Account_tax_group();
        }

        if(model.getWrite_dateDirtyFlag())
            domain.setWriteDate(model.getWrite_date());
        if(model.getSequenceDirtyFlag())
            domain.setSequence(model.getSequence());
        if(model.getNameDirtyFlag())
            domain.setName(model.getName());
        if(model.get__last_updateDirtyFlag())
            domain.setLastUpdate(model.get__last_update());
        if(model.getIdDirtyFlag())
            domain.setId(model.getId());
        if(model.getCreate_dateDirtyFlag())
            domain.setCreateDate(model.getCreate_date());
        if(model.getDisplay_nameDirtyFlag())
            domain.setDisplayName(model.getDisplay_name());
        if(model.getCreate_uid_textDirtyFlag())
            domain.setCreateUidText(model.getCreate_uid_text());
        if(model.getWrite_uid_textDirtyFlag())
            domain.setWriteUidText(model.getWrite_uid_text());
        if(model.getCreate_uidDirtyFlag())
            domain.setCreateUid(model.getCreate_uid());
        if(model.getWrite_uidDirtyFlag())
            domain.setWriteUid(model.getWrite_uid());
        return domain ;
    }

}

    



