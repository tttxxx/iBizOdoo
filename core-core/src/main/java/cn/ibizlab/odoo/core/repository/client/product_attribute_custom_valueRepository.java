package cn.ibizlab.odoo.core.repository.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import cn.ibizlab.odoo.core.repository.client.po.product_attribute_custom_value;

/**
 * 实体[product_attribute_custom_value] 服务对象接口
 */
public interface product_attribute_custom_valueRepository{


    public product_attribute_custom_value createPO() ;
        public void get(String id);

        public List<product_attribute_custom_value> search();

        public void createBatch(product_attribute_custom_value product_attribute_custom_value);

        public void removeBatch(String id);

        public void create(product_attribute_custom_value product_attribute_custom_value);

        public void updateBatch(product_attribute_custom_value product_attribute_custom_value);

        public void update(product_attribute_custom_value product_attribute_custom_value);

        public void remove(String id);


}
