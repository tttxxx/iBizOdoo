package cn.ibizlab.odoo.core.odoo_hr.clientmodel;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.math.BigInteger;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.util.helper.OdooClientHelper;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang.ArrayUtils;
import org.springframework.util.StringUtils;

/**
 * 接口实体[hr_leave_allocation] 对象
 */
public class hr_leave_allocationClientModel implements Serializable{

    /**
     * 权责发生制
     */
    public String accrual;

    @JsonIgnore
    public boolean accrualDirtyFlag;
    
    /**
     * 余额限额
     */
    public Integer accrual_limit;

    @JsonIgnore
    public boolean accrual_limitDirtyFlag;
    
    /**
     * 下一活动截止日期
     */
    public Timestamp activity_date_deadline;

    @JsonIgnore
    public boolean activity_date_deadlineDirtyFlag;
    
    /**
     * 活动
     */
    public String activity_ids;

    @JsonIgnore
    public boolean activity_idsDirtyFlag;
    
    /**
     * 活动状态
     */
    public String activity_state;

    @JsonIgnore
    public boolean activity_stateDirtyFlag;
    
    /**
     * 下一活动摘要
     */
    public String activity_summary;

    @JsonIgnore
    public boolean activity_summaryDirtyFlag;
    
    /**
     * 下一活动类型
     */
    public Integer activity_type_id;

    @JsonIgnore
    public boolean activity_type_idDirtyFlag;
    
    /**
     * 责任用户
     */
    public Integer activity_user_id;

    @JsonIgnore
    public boolean activity_user_idDirtyFlag;
    
    /**
     * 能批准
     */
    public String can_approve;

    @JsonIgnore
    public boolean can_approveDirtyFlag;
    
    /**
     * 能重置
     */
    public String can_reset;

    @JsonIgnore
    public boolean can_resetDirtyFlag;
    
    /**
     * 员工标签
     */
    public Integer category_id;

    @JsonIgnore
    public boolean category_idDirtyFlag;
    
    /**
     * 员工标签
     */
    public String category_id_text;

    @JsonIgnore
    public boolean category_id_textDirtyFlag;
    
    /**
     * 创建时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp create_date;

    @JsonIgnore
    public boolean create_dateDirtyFlag;
    
    /**
     * 创建人
     */
    public Integer create_uid;

    @JsonIgnore
    public boolean create_uidDirtyFlag;
    
    /**
     * 创建人
     */
    public String create_uid_text;

    @JsonIgnore
    public boolean create_uid_textDirtyFlag;
    
    /**
     * 开始日期
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp date_from;

    @JsonIgnore
    public boolean date_fromDirtyFlag;
    
    /**
     * 结束日期
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp date_to;

    @JsonIgnore
    public boolean date_toDirtyFlag;
    
    /**
     * 部门
     */
    public Integer department_id;

    @JsonIgnore
    public boolean department_idDirtyFlag;
    
    /**
     * 部门
     */
    public String department_id_text;

    @JsonIgnore
    public boolean department_id_textDirtyFlag;
    
    /**
     * 显示名称
     */
    public String display_name;

    @JsonIgnore
    public boolean display_nameDirtyFlag;
    
    /**
     * 分配 （天/小时）
     */
    public String duration_display;

    @JsonIgnore
    public boolean duration_displayDirtyFlag;
    
    /**
     * 员工
     */
    public Integer employee_id;

    @JsonIgnore
    public boolean employee_idDirtyFlag;
    
    /**
     * 员工
     */
    public String employee_id_text;

    @JsonIgnore
    public boolean employee_id_textDirtyFlag;
    
    /**
     * 首次审批
     */
    public Integer first_approver_id;

    @JsonIgnore
    public boolean first_approver_idDirtyFlag;
    
    /**
     * 首次审批
     */
    public String first_approver_id_text;

    @JsonIgnore
    public boolean first_approver_id_textDirtyFlag;
    
    /**
     * 休假类型
     */
    public Integer holiday_status_id;

    @JsonIgnore
    public boolean holiday_status_idDirtyFlag;
    
    /**
     * 休假类型
     */
    public String holiday_status_id_text;

    @JsonIgnore
    public boolean holiday_status_id_textDirtyFlag;
    
    /**
     * 分配模式
     */
    public String holiday_type;

    @JsonIgnore
    public boolean holiday_typeDirtyFlag;
    
    /**
     * ID
     */
    public Integer id;

    @JsonIgnore
    public boolean idDirtyFlag;
    
    /**
     * 两个间隔之间的单位数
     */
    public Integer interval_number;

    @JsonIgnore
    public boolean interval_numberDirtyFlag;
    
    /**
     * 两个区间之间的时间单位
     */
    public String interval_unit;

    @JsonIgnore
    public boolean interval_unitDirtyFlag;
    
    /**
     * 链接申请
     */
    public String linked_request_ids;

    @JsonIgnore
    public boolean linked_request_idsDirtyFlag;
    
    /**
     * 附件数量
     */
    public Integer message_attachment_count;

    @JsonIgnore
    public boolean message_attachment_countDirtyFlag;
    
    /**
     * 关注者(渠道)
     */
    public String message_channel_ids;

    @JsonIgnore
    public boolean message_channel_idsDirtyFlag;
    
    /**
     * 关注者
     */
    public String message_follower_ids;

    @JsonIgnore
    public boolean message_follower_idsDirtyFlag;
    
    /**
     * 消息递送错误
     */
    public String message_has_error;

    @JsonIgnore
    public boolean message_has_errorDirtyFlag;
    
    /**
     * 错误数
     */
    public Integer message_has_error_counter;

    @JsonIgnore
    public boolean message_has_error_counterDirtyFlag;
    
    /**
     * 信息
     */
    public String message_ids;

    @JsonIgnore
    public boolean message_idsDirtyFlag;
    
    /**
     * 是关注者
     */
    public String message_is_follower;

    @JsonIgnore
    public boolean message_is_followerDirtyFlag;
    
    /**
     * 附件
     */
    public Integer message_main_attachment_id;

    @JsonIgnore
    public boolean message_main_attachment_idDirtyFlag;
    
    /**
     * 需要采取行动
     */
    public String message_needaction;

    @JsonIgnore
    public boolean message_needactionDirtyFlag;
    
    /**
     * 行动数量
     */
    public Integer message_needaction_counter;

    @JsonIgnore
    public boolean message_needaction_counterDirtyFlag;
    
    /**
     * 关注者(业务伙伴)
     */
    public String message_partner_ids;

    @JsonIgnore
    public boolean message_partner_idsDirtyFlag;
    
    /**
     * 未读消息
     */
    public String message_unread;

    @JsonIgnore
    public boolean message_unreadDirtyFlag;
    
    /**
     * 未读消息计数器
     */
    public Integer message_unread_counter;

    @JsonIgnore
    public boolean message_unread_counterDirtyFlag;
    
    /**
     * 公司
     */
    public Integer mode_company_id;

    @JsonIgnore
    public boolean mode_company_idDirtyFlag;
    
    /**
     * 公司
     */
    public String mode_company_id_text;

    @JsonIgnore
    public boolean mode_company_id_textDirtyFlag;
    
    /**
     * 说明
     */
    public String name;

    @JsonIgnore
    public boolean nameDirtyFlag;
    
    /**
     * 下一次应计分配的日期
     */
    public Timestamp nextcall;

    @JsonIgnore
    public boolean nextcallDirtyFlag;
    
    /**
     * 理由
     */
    public String notes;

    @JsonIgnore
    public boolean notesDirtyFlag;
    
    /**
     * 天数
     */
    public Double number_of_days;

    @JsonIgnore
    public boolean number_of_daysDirtyFlag;
    
    /**
     * 持续时间（天）
     */
    public Double number_of_days_display;

    @JsonIgnore
    public boolean number_of_days_displayDirtyFlag;
    
    /**
     * 时长(小时)
     */
    public Double number_of_hours_display;

    @JsonIgnore
    public boolean number_of_hours_displayDirtyFlag;
    
    /**
     * 每个间隔的单位数
     */
    public Double number_per_interval;

    @JsonIgnore
    public boolean number_per_intervalDirtyFlag;
    
    /**
     * 上级
     */
    public Integer parent_id;

    @JsonIgnore
    public boolean parent_idDirtyFlag;
    
    /**
     * 上级
     */
    public String parent_id_text;

    @JsonIgnore
    public boolean parent_id_textDirtyFlag;
    
    /**
     * 第二次审批
     */
    public Integer second_approver_id;

    @JsonIgnore
    public boolean second_approver_idDirtyFlag;
    
    /**
     * 第二次审批
     */
    public String second_approver_id_text;

    @JsonIgnore
    public boolean second_approver_id_textDirtyFlag;
    
    /**
     * 状态
     */
    public String state;

    @JsonIgnore
    public boolean stateDirtyFlag;
    
    /**
     * 休假
     */
    public String type_request_unit;

    @JsonIgnore
    public boolean type_request_unitDirtyFlag;
    
    /**
     * 在每个区间添加的时间单位
     */
    public String unit_per_interval;

    @JsonIgnore
    public boolean unit_per_intervalDirtyFlag;
    
    /**
     * 验证人
     */
    public String validation_type;

    @JsonIgnore
    public boolean validation_typeDirtyFlag;
    
    /**
     * 网站消息
     */
    public String website_message_ids;

    @JsonIgnore
    public boolean website_message_idsDirtyFlag;
    
    /**
     * 最后更新时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp write_date;

    @JsonIgnore
    public boolean write_dateDirtyFlag;
    
    /**
     * 最后更新人
     */
    public Integer write_uid;

    @JsonIgnore
    public boolean write_uidDirtyFlag;
    
    /**
     * 最后更新人
     */
    public String write_uid_text;

    @JsonIgnore
    public boolean write_uid_textDirtyFlag;
    
    /**
     * 最后修改日
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp __last_update;

    @JsonIgnore
    public boolean __last_updateDirtyFlag;
    
    /**
     * 获取 [权责发生制]
     */
    @JsonProperty("accrual")
    public String getAccrual(){
        return this.accrual ;
    }

    /**
     * 设置 [权责发生制]
     */
    @JsonProperty("accrual")
    public void setAccrual(String  accrual){
        this.accrual = accrual ;
        this.accrualDirtyFlag = true ;
    }

     /**
     * 获取 [权责发生制]脏标记
     */
    @JsonIgnore
    public boolean getAccrualDirtyFlag(){
        return this.accrualDirtyFlag ;
    }   

    /**
     * 获取 [余额限额]
     */
    @JsonProperty("accrual_limit")
    public Integer getAccrual_limit(){
        return this.accrual_limit ;
    }

    /**
     * 设置 [余额限额]
     */
    @JsonProperty("accrual_limit")
    public void setAccrual_limit(Integer  accrual_limit){
        this.accrual_limit = accrual_limit ;
        this.accrual_limitDirtyFlag = true ;
    }

     /**
     * 获取 [余额限额]脏标记
     */
    @JsonIgnore
    public boolean getAccrual_limitDirtyFlag(){
        return this.accrual_limitDirtyFlag ;
    }   

    /**
     * 获取 [下一活动截止日期]
     */
    @JsonProperty("activity_date_deadline")
    public Timestamp getActivity_date_deadline(){
        return this.activity_date_deadline ;
    }

    /**
     * 设置 [下一活动截止日期]
     */
    @JsonProperty("activity_date_deadline")
    public void setActivity_date_deadline(Timestamp  activity_date_deadline){
        this.activity_date_deadline = activity_date_deadline ;
        this.activity_date_deadlineDirtyFlag = true ;
    }

     /**
     * 获取 [下一活动截止日期]脏标记
     */
    @JsonIgnore
    public boolean getActivity_date_deadlineDirtyFlag(){
        return this.activity_date_deadlineDirtyFlag ;
    }   

    /**
     * 获取 [活动]
     */
    @JsonProperty("activity_ids")
    public String getActivity_ids(){
        return this.activity_ids ;
    }

    /**
     * 设置 [活动]
     */
    @JsonProperty("activity_ids")
    public void setActivity_ids(String  activity_ids){
        this.activity_ids = activity_ids ;
        this.activity_idsDirtyFlag = true ;
    }

     /**
     * 获取 [活动]脏标记
     */
    @JsonIgnore
    public boolean getActivity_idsDirtyFlag(){
        return this.activity_idsDirtyFlag ;
    }   

    /**
     * 获取 [活动状态]
     */
    @JsonProperty("activity_state")
    public String getActivity_state(){
        return this.activity_state ;
    }

    /**
     * 设置 [活动状态]
     */
    @JsonProperty("activity_state")
    public void setActivity_state(String  activity_state){
        this.activity_state = activity_state ;
        this.activity_stateDirtyFlag = true ;
    }

     /**
     * 获取 [活动状态]脏标记
     */
    @JsonIgnore
    public boolean getActivity_stateDirtyFlag(){
        return this.activity_stateDirtyFlag ;
    }   

    /**
     * 获取 [下一活动摘要]
     */
    @JsonProperty("activity_summary")
    public String getActivity_summary(){
        return this.activity_summary ;
    }

    /**
     * 设置 [下一活动摘要]
     */
    @JsonProperty("activity_summary")
    public void setActivity_summary(String  activity_summary){
        this.activity_summary = activity_summary ;
        this.activity_summaryDirtyFlag = true ;
    }

     /**
     * 获取 [下一活动摘要]脏标记
     */
    @JsonIgnore
    public boolean getActivity_summaryDirtyFlag(){
        return this.activity_summaryDirtyFlag ;
    }   

    /**
     * 获取 [下一活动类型]
     */
    @JsonProperty("activity_type_id")
    public Integer getActivity_type_id(){
        return this.activity_type_id ;
    }

    /**
     * 设置 [下一活动类型]
     */
    @JsonProperty("activity_type_id")
    public void setActivity_type_id(Integer  activity_type_id){
        this.activity_type_id = activity_type_id ;
        this.activity_type_idDirtyFlag = true ;
    }

     /**
     * 获取 [下一活动类型]脏标记
     */
    @JsonIgnore
    public boolean getActivity_type_idDirtyFlag(){
        return this.activity_type_idDirtyFlag ;
    }   

    /**
     * 获取 [责任用户]
     */
    @JsonProperty("activity_user_id")
    public Integer getActivity_user_id(){
        return this.activity_user_id ;
    }

    /**
     * 设置 [责任用户]
     */
    @JsonProperty("activity_user_id")
    public void setActivity_user_id(Integer  activity_user_id){
        this.activity_user_id = activity_user_id ;
        this.activity_user_idDirtyFlag = true ;
    }

     /**
     * 获取 [责任用户]脏标记
     */
    @JsonIgnore
    public boolean getActivity_user_idDirtyFlag(){
        return this.activity_user_idDirtyFlag ;
    }   

    /**
     * 获取 [能批准]
     */
    @JsonProperty("can_approve")
    public String getCan_approve(){
        return this.can_approve ;
    }

    /**
     * 设置 [能批准]
     */
    @JsonProperty("can_approve")
    public void setCan_approve(String  can_approve){
        this.can_approve = can_approve ;
        this.can_approveDirtyFlag = true ;
    }

     /**
     * 获取 [能批准]脏标记
     */
    @JsonIgnore
    public boolean getCan_approveDirtyFlag(){
        return this.can_approveDirtyFlag ;
    }   

    /**
     * 获取 [能重置]
     */
    @JsonProperty("can_reset")
    public String getCan_reset(){
        return this.can_reset ;
    }

    /**
     * 设置 [能重置]
     */
    @JsonProperty("can_reset")
    public void setCan_reset(String  can_reset){
        this.can_reset = can_reset ;
        this.can_resetDirtyFlag = true ;
    }

     /**
     * 获取 [能重置]脏标记
     */
    @JsonIgnore
    public boolean getCan_resetDirtyFlag(){
        return this.can_resetDirtyFlag ;
    }   

    /**
     * 获取 [员工标签]
     */
    @JsonProperty("category_id")
    public Integer getCategory_id(){
        return this.category_id ;
    }

    /**
     * 设置 [员工标签]
     */
    @JsonProperty("category_id")
    public void setCategory_id(Integer  category_id){
        this.category_id = category_id ;
        this.category_idDirtyFlag = true ;
    }

     /**
     * 获取 [员工标签]脏标记
     */
    @JsonIgnore
    public boolean getCategory_idDirtyFlag(){
        return this.category_idDirtyFlag ;
    }   

    /**
     * 获取 [员工标签]
     */
    @JsonProperty("category_id_text")
    public String getCategory_id_text(){
        return this.category_id_text ;
    }

    /**
     * 设置 [员工标签]
     */
    @JsonProperty("category_id_text")
    public void setCategory_id_text(String  category_id_text){
        this.category_id_text = category_id_text ;
        this.category_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [员工标签]脏标记
     */
    @JsonIgnore
    public boolean getCategory_id_textDirtyFlag(){
        return this.category_id_textDirtyFlag ;
    }   

    /**
     * 获取 [创建时间]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return this.create_date ;
    }

    /**
     * 设置 [创建时间]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

     /**
     * 获取 [创建时间]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return this.create_dateDirtyFlag ;
    }   

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return this.create_uid ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

     /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return this.create_uidDirtyFlag ;
    }   

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return this.create_uid_text ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

     /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return this.create_uid_textDirtyFlag ;
    }   

    /**
     * 获取 [开始日期]
     */
    @JsonProperty("date_from")
    public Timestamp getDate_from(){
        return this.date_from ;
    }

    /**
     * 设置 [开始日期]
     */
    @JsonProperty("date_from")
    public void setDate_from(Timestamp  date_from){
        this.date_from = date_from ;
        this.date_fromDirtyFlag = true ;
    }

     /**
     * 获取 [开始日期]脏标记
     */
    @JsonIgnore
    public boolean getDate_fromDirtyFlag(){
        return this.date_fromDirtyFlag ;
    }   

    /**
     * 获取 [结束日期]
     */
    @JsonProperty("date_to")
    public Timestamp getDate_to(){
        return this.date_to ;
    }

    /**
     * 设置 [结束日期]
     */
    @JsonProperty("date_to")
    public void setDate_to(Timestamp  date_to){
        this.date_to = date_to ;
        this.date_toDirtyFlag = true ;
    }

     /**
     * 获取 [结束日期]脏标记
     */
    @JsonIgnore
    public boolean getDate_toDirtyFlag(){
        return this.date_toDirtyFlag ;
    }   

    /**
     * 获取 [部门]
     */
    @JsonProperty("department_id")
    public Integer getDepartment_id(){
        return this.department_id ;
    }

    /**
     * 设置 [部门]
     */
    @JsonProperty("department_id")
    public void setDepartment_id(Integer  department_id){
        this.department_id = department_id ;
        this.department_idDirtyFlag = true ;
    }

     /**
     * 获取 [部门]脏标记
     */
    @JsonIgnore
    public boolean getDepartment_idDirtyFlag(){
        return this.department_idDirtyFlag ;
    }   

    /**
     * 获取 [部门]
     */
    @JsonProperty("department_id_text")
    public String getDepartment_id_text(){
        return this.department_id_text ;
    }

    /**
     * 设置 [部门]
     */
    @JsonProperty("department_id_text")
    public void setDepartment_id_text(String  department_id_text){
        this.department_id_text = department_id_text ;
        this.department_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [部门]脏标记
     */
    @JsonIgnore
    public boolean getDepartment_id_textDirtyFlag(){
        return this.department_id_textDirtyFlag ;
    }   

    /**
     * 获取 [显示名称]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return this.display_name ;
    }

    /**
     * 设置 [显示名称]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

     /**
     * 获取 [显示名称]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return this.display_nameDirtyFlag ;
    }   

    /**
     * 获取 [分配 （天/小时）]
     */
    @JsonProperty("duration_display")
    public String getDuration_display(){
        return this.duration_display ;
    }

    /**
     * 设置 [分配 （天/小时）]
     */
    @JsonProperty("duration_display")
    public void setDuration_display(String  duration_display){
        this.duration_display = duration_display ;
        this.duration_displayDirtyFlag = true ;
    }

     /**
     * 获取 [分配 （天/小时）]脏标记
     */
    @JsonIgnore
    public boolean getDuration_displayDirtyFlag(){
        return this.duration_displayDirtyFlag ;
    }   

    /**
     * 获取 [员工]
     */
    @JsonProperty("employee_id")
    public Integer getEmployee_id(){
        return this.employee_id ;
    }

    /**
     * 设置 [员工]
     */
    @JsonProperty("employee_id")
    public void setEmployee_id(Integer  employee_id){
        this.employee_id = employee_id ;
        this.employee_idDirtyFlag = true ;
    }

     /**
     * 获取 [员工]脏标记
     */
    @JsonIgnore
    public boolean getEmployee_idDirtyFlag(){
        return this.employee_idDirtyFlag ;
    }   

    /**
     * 获取 [员工]
     */
    @JsonProperty("employee_id_text")
    public String getEmployee_id_text(){
        return this.employee_id_text ;
    }

    /**
     * 设置 [员工]
     */
    @JsonProperty("employee_id_text")
    public void setEmployee_id_text(String  employee_id_text){
        this.employee_id_text = employee_id_text ;
        this.employee_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [员工]脏标记
     */
    @JsonIgnore
    public boolean getEmployee_id_textDirtyFlag(){
        return this.employee_id_textDirtyFlag ;
    }   

    /**
     * 获取 [首次审批]
     */
    @JsonProperty("first_approver_id")
    public Integer getFirst_approver_id(){
        return this.first_approver_id ;
    }

    /**
     * 设置 [首次审批]
     */
    @JsonProperty("first_approver_id")
    public void setFirst_approver_id(Integer  first_approver_id){
        this.first_approver_id = first_approver_id ;
        this.first_approver_idDirtyFlag = true ;
    }

     /**
     * 获取 [首次审批]脏标记
     */
    @JsonIgnore
    public boolean getFirst_approver_idDirtyFlag(){
        return this.first_approver_idDirtyFlag ;
    }   

    /**
     * 获取 [首次审批]
     */
    @JsonProperty("first_approver_id_text")
    public String getFirst_approver_id_text(){
        return this.first_approver_id_text ;
    }

    /**
     * 设置 [首次审批]
     */
    @JsonProperty("first_approver_id_text")
    public void setFirst_approver_id_text(String  first_approver_id_text){
        this.first_approver_id_text = first_approver_id_text ;
        this.first_approver_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [首次审批]脏标记
     */
    @JsonIgnore
    public boolean getFirst_approver_id_textDirtyFlag(){
        return this.first_approver_id_textDirtyFlag ;
    }   

    /**
     * 获取 [休假类型]
     */
    @JsonProperty("holiday_status_id")
    public Integer getHoliday_status_id(){
        return this.holiday_status_id ;
    }

    /**
     * 设置 [休假类型]
     */
    @JsonProperty("holiday_status_id")
    public void setHoliday_status_id(Integer  holiday_status_id){
        this.holiday_status_id = holiday_status_id ;
        this.holiday_status_idDirtyFlag = true ;
    }

     /**
     * 获取 [休假类型]脏标记
     */
    @JsonIgnore
    public boolean getHoliday_status_idDirtyFlag(){
        return this.holiday_status_idDirtyFlag ;
    }   

    /**
     * 获取 [休假类型]
     */
    @JsonProperty("holiday_status_id_text")
    public String getHoliday_status_id_text(){
        return this.holiday_status_id_text ;
    }

    /**
     * 设置 [休假类型]
     */
    @JsonProperty("holiday_status_id_text")
    public void setHoliday_status_id_text(String  holiday_status_id_text){
        this.holiday_status_id_text = holiday_status_id_text ;
        this.holiday_status_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [休假类型]脏标记
     */
    @JsonIgnore
    public boolean getHoliday_status_id_textDirtyFlag(){
        return this.holiday_status_id_textDirtyFlag ;
    }   

    /**
     * 获取 [分配模式]
     */
    @JsonProperty("holiday_type")
    public String getHoliday_type(){
        return this.holiday_type ;
    }

    /**
     * 设置 [分配模式]
     */
    @JsonProperty("holiday_type")
    public void setHoliday_type(String  holiday_type){
        this.holiday_type = holiday_type ;
        this.holiday_typeDirtyFlag = true ;
    }

     /**
     * 获取 [分配模式]脏标记
     */
    @JsonIgnore
    public boolean getHoliday_typeDirtyFlag(){
        return this.holiday_typeDirtyFlag ;
    }   

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return this.id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

     /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return this.idDirtyFlag ;
    }   

    /**
     * 获取 [两个间隔之间的单位数]
     */
    @JsonProperty("interval_number")
    public Integer getInterval_number(){
        return this.interval_number ;
    }

    /**
     * 设置 [两个间隔之间的单位数]
     */
    @JsonProperty("interval_number")
    public void setInterval_number(Integer  interval_number){
        this.interval_number = interval_number ;
        this.interval_numberDirtyFlag = true ;
    }

     /**
     * 获取 [两个间隔之间的单位数]脏标记
     */
    @JsonIgnore
    public boolean getInterval_numberDirtyFlag(){
        return this.interval_numberDirtyFlag ;
    }   

    /**
     * 获取 [两个区间之间的时间单位]
     */
    @JsonProperty("interval_unit")
    public String getInterval_unit(){
        return this.interval_unit ;
    }

    /**
     * 设置 [两个区间之间的时间单位]
     */
    @JsonProperty("interval_unit")
    public void setInterval_unit(String  interval_unit){
        this.interval_unit = interval_unit ;
        this.interval_unitDirtyFlag = true ;
    }

     /**
     * 获取 [两个区间之间的时间单位]脏标记
     */
    @JsonIgnore
    public boolean getInterval_unitDirtyFlag(){
        return this.interval_unitDirtyFlag ;
    }   

    /**
     * 获取 [链接申请]
     */
    @JsonProperty("linked_request_ids")
    public String getLinked_request_ids(){
        return this.linked_request_ids ;
    }

    /**
     * 设置 [链接申请]
     */
    @JsonProperty("linked_request_ids")
    public void setLinked_request_ids(String  linked_request_ids){
        this.linked_request_ids = linked_request_ids ;
        this.linked_request_idsDirtyFlag = true ;
    }

     /**
     * 获取 [链接申请]脏标记
     */
    @JsonIgnore
    public boolean getLinked_request_idsDirtyFlag(){
        return this.linked_request_idsDirtyFlag ;
    }   

    /**
     * 获取 [附件数量]
     */
    @JsonProperty("message_attachment_count")
    public Integer getMessage_attachment_count(){
        return this.message_attachment_count ;
    }

    /**
     * 设置 [附件数量]
     */
    @JsonProperty("message_attachment_count")
    public void setMessage_attachment_count(Integer  message_attachment_count){
        this.message_attachment_count = message_attachment_count ;
        this.message_attachment_countDirtyFlag = true ;
    }

     /**
     * 获取 [附件数量]脏标记
     */
    @JsonIgnore
    public boolean getMessage_attachment_countDirtyFlag(){
        return this.message_attachment_countDirtyFlag ;
    }   

    /**
     * 获取 [关注者(渠道)]
     */
    @JsonProperty("message_channel_ids")
    public String getMessage_channel_ids(){
        return this.message_channel_ids ;
    }

    /**
     * 设置 [关注者(渠道)]
     */
    @JsonProperty("message_channel_ids")
    public void setMessage_channel_ids(String  message_channel_ids){
        this.message_channel_ids = message_channel_ids ;
        this.message_channel_idsDirtyFlag = true ;
    }

     /**
     * 获取 [关注者(渠道)]脏标记
     */
    @JsonIgnore
    public boolean getMessage_channel_idsDirtyFlag(){
        return this.message_channel_idsDirtyFlag ;
    }   

    /**
     * 获取 [关注者]
     */
    @JsonProperty("message_follower_ids")
    public String getMessage_follower_ids(){
        return this.message_follower_ids ;
    }

    /**
     * 设置 [关注者]
     */
    @JsonProperty("message_follower_ids")
    public void setMessage_follower_ids(String  message_follower_ids){
        this.message_follower_ids = message_follower_ids ;
        this.message_follower_idsDirtyFlag = true ;
    }

     /**
     * 获取 [关注者]脏标记
     */
    @JsonIgnore
    public boolean getMessage_follower_idsDirtyFlag(){
        return this.message_follower_idsDirtyFlag ;
    }   

    /**
     * 获取 [消息递送错误]
     */
    @JsonProperty("message_has_error")
    public String getMessage_has_error(){
        return this.message_has_error ;
    }

    /**
     * 设置 [消息递送错误]
     */
    @JsonProperty("message_has_error")
    public void setMessage_has_error(String  message_has_error){
        this.message_has_error = message_has_error ;
        this.message_has_errorDirtyFlag = true ;
    }

     /**
     * 获取 [消息递送错误]脏标记
     */
    @JsonIgnore
    public boolean getMessage_has_errorDirtyFlag(){
        return this.message_has_errorDirtyFlag ;
    }   

    /**
     * 获取 [错误数]
     */
    @JsonProperty("message_has_error_counter")
    public Integer getMessage_has_error_counter(){
        return this.message_has_error_counter ;
    }

    /**
     * 设置 [错误数]
     */
    @JsonProperty("message_has_error_counter")
    public void setMessage_has_error_counter(Integer  message_has_error_counter){
        this.message_has_error_counter = message_has_error_counter ;
        this.message_has_error_counterDirtyFlag = true ;
    }

     /**
     * 获取 [错误数]脏标记
     */
    @JsonIgnore
    public boolean getMessage_has_error_counterDirtyFlag(){
        return this.message_has_error_counterDirtyFlag ;
    }   

    /**
     * 获取 [信息]
     */
    @JsonProperty("message_ids")
    public String getMessage_ids(){
        return this.message_ids ;
    }

    /**
     * 设置 [信息]
     */
    @JsonProperty("message_ids")
    public void setMessage_ids(String  message_ids){
        this.message_ids = message_ids ;
        this.message_idsDirtyFlag = true ;
    }

     /**
     * 获取 [信息]脏标记
     */
    @JsonIgnore
    public boolean getMessage_idsDirtyFlag(){
        return this.message_idsDirtyFlag ;
    }   

    /**
     * 获取 [是关注者]
     */
    @JsonProperty("message_is_follower")
    public String getMessage_is_follower(){
        return this.message_is_follower ;
    }

    /**
     * 设置 [是关注者]
     */
    @JsonProperty("message_is_follower")
    public void setMessage_is_follower(String  message_is_follower){
        this.message_is_follower = message_is_follower ;
        this.message_is_followerDirtyFlag = true ;
    }

     /**
     * 获取 [是关注者]脏标记
     */
    @JsonIgnore
    public boolean getMessage_is_followerDirtyFlag(){
        return this.message_is_followerDirtyFlag ;
    }   

    /**
     * 获取 [附件]
     */
    @JsonProperty("message_main_attachment_id")
    public Integer getMessage_main_attachment_id(){
        return this.message_main_attachment_id ;
    }

    /**
     * 设置 [附件]
     */
    @JsonProperty("message_main_attachment_id")
    public void setMessage_main_attachment_id(Integer  message_main_attachment_id){
        this.message_main_attachment_id = message_main_attachment_id ;
        this.message_main_attachment_idDirtyFlag = true ;
    }

     /**
     * 获取 [附件]脏标记
     */
    @JsonIgnore
    public boolean getMessage_main_attachment_idDirtyFlag(){
        return this.message_main_attachment_idDirtyFlag ;
    }   

    /**
     * 获取 [需要采取行动]
     */
    @JsonProperty("message_needaction")
    public String getMessage_needaction(){
        return this.message_needaction ;
    }

    /**
     * 设置 [需要采取行动]
     */
    @JsonProperty("message_needaction")
    public void setMessage_needaction(String  message_needaction){
        this.message_needaction = message_needaction ;
        this.message_needactionDirtyFlag = true ;
    }

     /**
     * 获取 [需要采取行动]脏标记
     */
    @JsonIgnore
    public boolean getMessage_needactionDirtyFlag(){
        return this.message_needactionDirtyFlag ;
    }   

    /**
     * 获取 [行动数量]
     */
    @JsonProperty("message_needaction_counter")
    public Integer getMessage_needaction_counter(){
        return this.message_needaction_counter ;
    }

    /**
     * 设置 [行动数量]
     */
    @JsonProperty("message_needaction_counter")
    public void setMessage_needaction_counter(Integer  message_needaction_counter){
        this.message_needaction_counter = message_needaction_counter ;
        this.message_needaction_counterDirtyFlag = true ;
    }

     /**
     * 获取 [行动数量]脏标记
     */
    @JsonIgnore
    public boolean getMessage_needaction_counterDirtyFlag(){
        return this.message_needaction_counterDirtyFlag ;
    }   

    /**
     * 获取 [关注者(业务伙伴)]
     */
    @JsonProperty("message_partner_ids")
    public String getMessage_partner_ids(){
        return this.message_partner_ids ;
    }

    /**
     * 设置 [关注者(业务伙伴)]
     */
    @JsonProperty("message_partner_ids")
    public void setMessage_partner_ids(String  message_partner_ids){
        this.message_partner_ids = message_partner_ids ;
        this.message_partner_idsDirtyFlag = true ;
    }

     /**
     * 获取 [关注者(业务伙伴)]脏标记
     */
    @JsonIgnore
    public boolean getMessage_partner_idsDirtyFlag(){
        return this.message_partner_idsDirtyFlag ;
    }   

    /**
     * 获取 [未读消息]
     */
    @JsonProperty("message_unread")
    public String getMessage_unread(){
        return this.message_unread ;
    }

    /**
     * 设置 [未读消息]
     */
    @JsonProperty("message_unread")
    public void setMessage_unread(String  message_unread){
        this.message_unread = message_unread ;
        this.message_unreadDirtyFlag = true ;
    }

     /**
     * 获取 [未读消息]脏标记
     */
    @JsonIgnore
    public boolean getMessage_unreadDirtyFlag(){
        return this.message_unreadDirtyFlag ;
    }   

    /**
     * 获取 [未读消息计数器]
     */
    @JsonProperty("message_unread_counter")
    public Integer getMessage_unread_counter(){
        return this.message_unread_counter ;
    }

    /**
     * 设置 [未读消息计数器]
     */
    @JsonProperty("message_unread_counter")
    public void setMessage_unread_counter(Integer  message_unread_counter){
        this.message_unread_counter = message_unread_counter ;
        this.message_unread_counterDirtyFlag = true ;
    }

     /**
     * 获取 [未读消息计数器]脏标记
     */
    @JsonIgnore
    public boolean getMessage_unread_counterDirtyFlag(){
        return this.message_unread_counterDirtyFlag ;
    }   

    /**
     * 获取 [公司]
     */
    @JsonProperty("mode_company_id")
    public Integer getMode_company_id(){
        return this.mode_company_id ;
    }

    /**
     * 设置 [公司]
     */
    @JsonProperty("mode_company_id")
    public void setMode_company_id(Integer  mode_company_id){
        this.mode_company_id = mode_company_id ;
        this.mode_company_idDirtyFlag = true ;
    }

     /**
     * 获取 [公司]脏标记
     */
    @JsonIgnore
    public boolean getMode_company_idDirtyFlag(){
        return this.mode_company_idDirtyFlag ;
    }   

    /**
     * 获取 [公司]
     */
    @JsonProperty("mode_company_id_text")
    public String getMode_company_id_text(){
        return this.mode_company_id_text ;
    }

    /**
     * 设置 [公司]
     */
    @JsonProperty("mode_company_id_text")
    public void setMode_company_id_text(String  mode_company_id_text){
        this.mode_company_id_text = mode_company_id_text ;
        this.mode_company_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [公司]脏标记
     */
    @JsonIgnore
    public boolean getMode_company_id_textDirtyFlag(){
        return this.mode_company_id_textDirtyFlag ;
    }   

    /**
     * 获取 [说明]
     */
    @JsonProperty("name")
    public String getName(){
        return this.name ;
    }

    /**
     * 设置 [说明]
     */
    @JsonProperty("name")
    public void setName(String  name){
        this.name = name ;
        this.nameDirtyFlag = true ;
    }

     /**
     * 获取 [说明]脏标记
     */
    @JsonIgnore
    public boolean getNameDirtyFlag(){
        return this.nameDirtyFlag ;
    }   

    /**
     * 获取 [下一次应计分配的日期]
     */
    @JsonProperty("nextcall")
    public Timestamp getNextcall(){
        return this.nextcall ;
    }

    /**
     * 设置 [下一次应计分配的日期]
     */
    @JsonProperty("nextcall")
    public void setNextcall(Timestamp  nextcall){
        this.nextcall = nextcall ;
        this.nextcallDirtyFlag = true ;
    }

     /**
     * 获取 [下一次应计分配的日期]脏标记
     */
    @JsonIgnore
    public boolean getNextcallDirtyFlag(){
        return this.nextcallDirtyFlag ;
    }   

    /**
     * 获取 [理由]
     */
    @JsonProperty("notes")
    public String getNotes(){
        return this.notes ;
    }

    /**
     * 设置 [理由]
     */
    @JsonProperty("notes")
    public void setNotes(String  notes){
        this.notes = notes ;
        this.notesDirtyFlag = true ;
    }

     /**
     * 获取 [理由]脏标记
     */
    @JsonIgnore
    public boolean getNotesDirtyFlag(){
        return this.notesDirtyFlag ;
    }   

    /**
     * 获取 [天数]
     */
    @JsonProperty("number_of_days")
    public Double getNumber_of_days(){
        return this.number_of_days ;
    }

    /**
     * 设置 [天数]
     */
    @JsonProperty("number_of_days")
    public void setNumber_of_days(Double  number_of_days){
        this.number_of_days = number_of_days ;
        this.number_of_daysDirtyFlag = true ;
    }

     /**
     * 获取 [天数]脏标记
     */
    @JsonIgnore
    public boolean getNumber_of_daysDirtyFlag(){
        return this.number_of_daysDirtyFlag ;
    }   

    /**
     * 获取 [持续时间（天）]
     */
    @JsonProperty("number_of_days_display")
    public Double getNumber_of_days_display(){
        return this.number_of_days_display ;
    }

    /**
     * 设置 [持续时间（天）]
     */
    @JsonProperty("number_of_days_display")
    public void setNumber_of_days_display(Double  number_of_days_display){
        this.number_of_days_display = number_of_days_display ;
        this.number_of_days_displayDirtyFlag = true ;
    }

     /**
     * 获取 [持续时间（天）]脏标记
     */
    @JsonIgnore
    public boolean getNumber_of_days_displayDirtyFlag(){
        return this.number_of_days_displayDirtyFlag ;
    }   

    /**
     * 获取 [时长(小时)]
     */
    @JsonProperty("number_of_hours_display")
    public Double getNumber_of_hours_display(){
        return this.number_of_hours_display ;
    }

    /**
     * 设置 [时长(小时)]
     */
    @JsonProperty("number_of_hours_display")
    public void setNumber_of_hours_display(Double  number_of_hours_display){
        this.number_of_hours_display = number_of_hours_display ;
        this.number_of_hours_displayDirtyFlag = true ;
    }

     /**
     * 获取 [时长(小时)]脏标记
     */
    @JsonIgnore
    public boolean getNumber_of_hours_displayDirtyFlag(){
        return this.number_of_hours_displayDirtyFlag ;
    }   

    /**
     * 获取 [每个间隔的单位数]
     */
    @JsonProperty("number_per_interval")
    public Double getNumber_per_interval(){
        return this.number_per_interval ;
    }

    /**
     * 设置 [每个间隔的单位数]
     */
    @JsonProperty("number_per_interval")
    public void setNumber_per_interval(Double  number_per_interval){
        this.number_per_interval = number_per_interval ;
        this.number_per_intervalDirtyFlag = true ;
    }

     /**
     * 获取 [每个间隔的单位数]脏标记
     */
    @JsonIgnore
    public boolean getNumber_per_intervalDirtyFlag(){
        return this.number_per_intervalDirtyFlag ;
    }   

    /**
     * 获取 [上级]
     */
    @JsonProperty("parent_id")
    public Integer getParent_id(){
        return this.parent_id ;
    }

    /**
     * 设置 [上级]
     */
    @JsonProperty("parent_id")
    public void setParent_id(Integer  parent_id){
        this.parent_id = parent_id ;
        this.parent_idDirtyFlag = true ;
    }

     /**
     * 获取 [上级]脏标记
     */
    @JsonIgnore
    public boolean getParent_idDirtyFlag(){
        return this.parent_idDirtyFlag ;
    }   

    /**
     * 获取 [上级]
     */
    @JsonProperty("parent_id_text")
    public String getParent_id_text(){
        return this.parent_id_text ;
    }

    /**
     * 设置 [上级]
     */
    @JsonProperty("parent_id_text")
    public void setParent_id_text(String  parent_id_text){
        this.parent_id_text = parent_id_text ;
        this.parent_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [上级]脏标记
     */
    @JsonIgnore
    public boolean getParent_id_textDirtyFlag(){
        return this.parent_id_textDirtyFlag ;
    }   

    /**
     * 获取 [第二次审批]
     */
    @JsonProperty("second_approver_id")
    public Integer getSecond_approver_id(){
        return this.second_approver_id ;
    }

    /**
     * 设置 [第二次审批]
     */
    @JsonProperty("second_approver_id")
    public void setSecond_approver_id(Integer  second_approver_id){
        this.second_approver_id = second_approver_id ;
        this.second_approver_idDirtyFlag = true ;
    }

     /**
     * 获取 [第二次审批]脏标记
     */
    @JsonIgnore
    public boolean getSecond_approver_idDirtyFlag(){
        return this.second_approver_idDirtyFlag ;
    }   

    /**
     * 获取 [第二次审批]
     */
    @JsonProperty("second_approver_id_text")
    public String getSecond_approver_id_text(){
        return this.second_approver_id_text ;
    }

    /**
     * 设置 [第二次审批]
     */
    @JsonProperty("second_approver_id_text")
    public void setSecond_approver_id_text(String  second_approver_id_text){
        this.second_approver_id_text = second_approver_id_text ;
        this.second_approver_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [第二次审批]脏标记
     */
    @JsonIgnore
    public boolean getSecond_approver_id_textDirtyFlag(){
        return this.second_approver_id_textDirtyFlag ;
    }   

    /**
     * 获取 [状态]
     */
    @JsonProperty("state")
    public String getState(){
        return this.state ;
    }

    /**
     * 设置 [状态]
     */
    @JsonProperty("state")
    public void setState(String  state){
        this.state = state ;
        this.stateDirtyFlag = true ;
    }

     /**
     * 获取 [状态]脏标记
     */
    @JsonIgnore
    public boolean getStateDirtyFlag(){
        return this.stateDirtyFlag ;
    }   

    /**
     * 获取 [休假]
     */
    @JsonProperty("type_request_unit")
    public String getType_request_unit(){
        return this.type_request_unit ;
    }

    /**
     * 设置 [休假]
     */
    @JsonProperty("type_request_unit")
    public void setType_request_unit(String  type_request_unit){
        this.type_request_unit = type_request_unit ;
        this.type_request_unitDirtyFlag = true ;
    }

     /**
     * 获取 [休假]脏标记
     */
    @JsonIgnore
    public boolean getType_request_unitDirtyFlag(){
        return this.type_request_unitDirtyFlag ;
    }   

    /**
     * 获取 [在每个区间添加的时间单位]
     */
    @JsonProperty("unit_per_interval")
    public String getUnit_per_interval(){
        return this.unit_per_interval ;
    }

    /**
     * 设置 [在每个区间添加的时间单位]
     */
    @JsonProperty("unit_per_interval")
    public void setUnit_per_interval(String  unit_per_interval){
        this.unit_per_interval = unit_per_interval ;
        this.unit_per_intervalDirtyFlag = true ;
    }

     /**
     * 获取 [在每个区间添加的时间单位]脏标记
     */
    @JsonIgnore
    public boolean getUnit_per_intervalDirtyFlag(){
        return this.unit_per_intervalDirtyFlag ;
    }   

    /**
     * 获取 [验证人]
     */
    @JsonProperty("validation_type")
    public String getValidation_type(){
        return this.validation_type ;
    }

    /**
     * 设置 [验证人]
     */
    @JsonProperty("validation_type")
    public void setValidation_type(String  validation_type){
        this.validation_type = validation_type ;
        this.validation_typeDirtyFlag = true ;
    }

     /**
     * 获取 [验证人]脏标记
     */
    @JsonIgnore
    public boolean getValidation_typeDirtyFlag(){
        return this.validation_typeDirtyFlag ;
    }   

    /**
     * 获取 [网站消息]
     */
    @JsonProperty("website_message_ids")
    public String getWebsite_message_ids(){
        return this.website_message_ids ;
    }

    /**
     * 设置 [网站消息]
     */
    @JsonProperty("website_message_ids")
    public void setWebsite_message_ids(String  website_message_ids){
        this.website_message_ids = website_message_ids ;
        this.website_message_idsDirtyFlag = true ;
    }

     /**
     * 获取 [网站消息]脏标记
     */
    @JsonIgnore
    public boolean getWebsite_message_idsDirtyFlag(){
        return this.website_message_idsDirtyFlag ;
    }   

    /**
     * 获取 [最后更新时间]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return this.write_date ;
    }

    /**
     * 设置 [最后更新时间]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新时间]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return this.write_dateDirtyFlag ;
    }   

    /**
     * 获取 [最后更新人]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return this.write_uid ;
    }

    /**
     * 设置 [最后更新人]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新人]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return this.write_uidDirtyFlag ;
    }   

    /**
     * 获取 [最后更新人]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return this.write_uid_text ;
    }

    /**
     * 设置 [最后更新人]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新人]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return this.write_uid_textDirtyFlag ;
    }   

    /**
     * 获取 [最后修改日]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return this.__last_update ;
    }

    /**
     * 设置 [最后修改日]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

     /**
     * 获取 [最后修改日]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return this.__last_updateDirtyFlag ;
    }   



    public void fromMap(Map<String, Object> map) throws Exception {
		if(map.get("accrual") instanceof Boolean){
			this.setAccrual(((Boolean)map.get("accrual"))? "true" : "false");
		}
		if(!(map.get("accrual_limit") instanceof Boolean)&& map.get("accrual_limit")!=null){
			this.setAccrual_limit((Integer)map.get("accrual_limit"));
		}
		if(!(map.get("activity_date_deadline") instanceof Boolean)&& map.get("activity_date_deadline")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd").parse((String)map.get("activity_date_deadline"));
   			this.setActivity_date_deadline(new Timestamp(parse.getTime()));
		}
		if(!(map.get("activity_ids") instanceof Boolean)&& map.get("activity_ids")!=null){
			Object[] objs = (Object[])map.get("activity_ids");
			if(objs.length > 0){
				Integer[] activity_ids = Arrays.copyOfRange(objs,0,objs.length,Integer[].class);
				this.setActivity_ids(Arrays.toString(activity_ids).replace(" ",""));
			}
		}
		if(!(map.get("activity_state") instanceof Boolean)&& map.get("activity_state")!=null){
			this.setActivity_state((String)map.get("activity_state"));
		}
		if(!(map.get("activity_summary") instanceof Boolean)&& map.get("activity_summary")!=null){
			this.setActivity_summary((String)map.get("activity_summary"));
		}
		if(!(map.get("activity_type_id") instanceof Boolean)&& map.get("activity_type_id")!=null){
			Object[] objs = (Object[])map.get("activity_type_id");
			if(objs.length > 0){
				this.setActivity_type_id((Integer)objs[0]);
			}
		}
		if(!(map.get("activity_user_id") instanceof Boolean)&& map.get("activity_user_id")!=null){
			Object[] objs = (Object[])map.get("activity_user_id");
			if(objs.length > 0){
				this.setActivity_user_id((Integer)objs[0]);
			}
		}
		if(map.get("can_approve") instanceof Boolean){
			this.setCan_approve(((Boolean)map.get("can_approve"))? "true" : "false");
		}
		if(map.get("can_reset") instanceof Boolean){
			this.setCan_reset(((Boolean)map.get("can_reset"))? "true" : "false");
		}
		if(!(map.get("category_id") instanceof Boolean)&& map.get("category_id")!=null){
			Object[] objs = (Object[])map.get("category_id");
			if(objs.length > 0){
				this.setCategory_id((Integer)objs[0]);
			}
		}
		if(!(map.get("category_id") instanceof Boolean)&& map.get("category_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("category_id");
			if(objs.length > 1){
				this.setCategory_id_text((String)objs[1]);
			}
		}
		if(!(map.get("create_date") instanceof Boolean)&& map.get("create_date")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse((String)map.get("create_date"));
   			this.setCreate_date(new Timestamp(parse.getTime()));
		}
		if(!(map.get("create_uid") instanceof Boolean)&& map.get("create_uid")!=null){
			Object[] objs = (Object[])map.get("create_uid");
			if(objs.length > 0){
				this.setCreate_uid((Integer)objs[0]);
			}
		}
		if(!(map.get("create_uid") instanceof Boolean)&& map.get("create_uid")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("create_uid");
			if(objs.length > 1){
				this.setCreate_uid_text((String)objs[1]);
			}
		}
		if(!(map.get("date_from") instanceof Boolean)&& map.get("date_from")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse((String)map.get("date_from"));
   			this.setDate_from(new Timestamp(parse.getTime()));
		}
		if(!(map.get("date_to") instanceof Boolean)&& map.get("date_to")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse((String)map.get("date_to"));
   			this.setDate_to(new Timestamp(parse.getTime()));
		}
		if(!(map.get("department_id") instanceof Boolean)&& map.get("department_id")!=null){
			Object[] objs = (Object[])map.get("department_id");
			if(objs.length > 0){
				this.setDepartment_id((Integer)objs[0]);
			}
		}
		if(!(map.get("department_id") instanceof Boolean)&& map.get("department_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("department_id");
			if(objs.length > 1){
				this.setDepartment_id_text((String)objs[1]);
			}
		}
		if(!(map.get("display_name") instanceof Boolean)&& map.get("display_name")!=null){
			this.setDisplay_name((String)map.get("display_name"));
		}
		if(!(map.get("duration_display") instanceof Boolean)&& map.get("duration_display")!=null){
			this.setDuration_display((String)map.get("duration_display"));
		}
		if(!(map.get("employee_id") instanceof Boolean)&& map.get("employee_id")!=null){
			Object[] objs = (Object[])map.get("employee_id");
			if(objs.length > 0){
				this.setEmployee_id((Integer)objs[0]);
			}
		}
		if(!(map.get("employee_id") instanceof Boolean)&& map.get("employee_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("employee_id");
			if(objs.length > 1){
				this.setEmployee_id_text((String)objs[1]);
			}
		}
		if(!(map.get("first_approver_id") instanceof Boolean)&& map.get("first_approver_id")!=null){
			Object[] objs = (Object[])map.get("first_approver_id");
			if(objs.length > 0){
				this.setFirst_approver_id((Integer)objs[0]);
			}
		}
		if(!(map.get("first_approver_id") instanceof Boolean)&& map.get("first_approver_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("first_approver_id");
			if(objs.length > 1){
				this.setFirst_approver_id_text((String)objs[1]);
			}
		}
		if(!(map.get("holiday_status_id") instanceof Boolean)&& map.get("holiday_status_id")!=null){
			Object[] objs = (Object[])map.get("holiday_status_id");
			if(objs.length > 0){
				this.setHoliday_status_id((Integer)objs[0]);
			}
		}
		if(!(map.get("holiday_status_id") instanceof Boolean)&& map.get("holiday_status_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("holiday_status_id");
			if(objs.length > 1){
				this.setHoliday_status_id_text((String)objs[1]);
			}
		}
		if(!(map.get("holiday_type") instanceof Boolean)&& map.get("holiday_type")!=null){
			this.setHoliday_type((String)map.get("holiday_type"));
		}
		if(!(map.get("id") instanceof Boolean)&& map.get("id")!=null){
			this.setId((Integer)map.get("id"));
		}
		if(!(map.get("interval_number") instanceof Boolean)&& map.get("interval_number")!=null){
			this.setInterval_number((Integer)map.get("interval_number"));
		}
		if(!(map.get("interval_unit") instanceof Boolean)&& map.get("interval_unit")!=null){
			this.setInterval_unit((String)map.get("interval_unit"));
		}
		if(!(map.get("linked_request_ids") instanceof Boolean)&& map.get("linked_request_ids")!=null){
			Object[] objs = (Object[])map.get("linked_request_ids");
			if(objs.length > 0){
				Integer[] linked_request_ids = Arrays.copyOfRange(objs,0,objs.length,Integer[].class);
				this.setLinked_request_ids(Arrays.toString(linked_request_ids).replace(" ",""));
			}
		}
		if(!(map.get("message_attachment_count") instanceof Boolean)&& map.get("message_attachment_count")!=null){
			this.setMessage_attachment_count((Integer)map.get("message_attachment_count"));
		}
		if(!(map.get("message_channel_ids") instanceof Boolean)&& map.get("message_channel_ids")!=null){
			Object[] objs = (Object[])map.get("message_channel_ids");
			if(objs.length > 0){
				Integer[] message_channel_ids = Arrays.copyOfRange(objs,0,objs.length,Integer[].class);
				this.setMessage_channel_ids(Arrays.toString(message_channel_ids).replace(" ",""));
			}
		}
		if(!(map.get("message_follower_ids") instanceof Boolean)&& map.get("message_follower_ids")!=null){
			Object[] objs = (Object[])map.get("message_follower_ids");
			if(objs.length > 0){
				Integer[] message_follower_ids = Arrays.copyOfRange(objs,0,objs.length,Integer[].class);
				this.setMessage_follower_ids(Arrays.toString(message_follower_ids).replace(" ",""));
			}
		}
		if(map.get("message_has_error") instanceof Boolean){
			this.setMessage_has_error(((Boolean)map.get("message_has_error"))? "true" : "false");
		}
		if(!(map.get("message_has_error_counter") instanceof Boolean)&& map.get("message_has_error_counter")!=null){
			this.setMessage_has_error_counter((Integer)map.get("message_has_error_counter"));
		}
		if(!(map.get("message_ids") instanceof Boolean)&& map.get("message_ids")!=null){
			Object[] objs = (Object[])map.get("message_ids");
			if(objs.length > 0){
				Integer[] message_ids = Arrays.copyOfRange(objs,0,objs.length,Integer[].class);
				this.setMessage_ids(Arrays.toString(message_ids).replace(" ",""));
			}
		}
		if(map.get("message_is_follower") instanceof Boolean){
			this.setMessage_is_follower(((Boolean)map.get("message_is_follower"))? "true" : "false");
		}
		if(!(map.get("message_main_attachment_id") instanceof Boolean)&& map.get("message_main_attachment_id")!=null){
			Object[] objs = (Object[])map.get("message_main_attachment_id");
			if(objs.length > 0){
				this.setMessage_main_attachment_id((Integer)objs[0]);
			}
		}
		if(map.get("message_needaction") instanceof Boolean){
			this.setMessage_needaction(((Boolean)map.get("message_needaction"))? "true" : "false");
		}
		if(!(map.get("message_needaction_counter") instanceof Boolean)&& map.get("message_needaction_counter")!=null){
			this.setMessage_needaction_counter((Integer)map.get("message_needaction_counter"));
		}
		if(!(map.get("message_partner_ids") instanceof Boolean)&& map.get("message_partner_ids")!=null){
			Object[] objs = (Object[])map.get("message_partner_ids");
			if(objs.length > 0){
				Integer[] message_partner_ids = Arrays.copyOfRange(objs,0,objs.length,Integer[].class);
				this.setMessage_partner_ids(Arrays.toString(message_partner_ids).replace(" ",""));
			}
		}
		if(map.get("message_unread") instanceof Boolean){
			this.setMessage_unread(((Boolean)map.get("message_unread"))? "true" : "false");
		}
		if(!(map.get("message_unread_counter") instanceof Boolean)&& map.get("message_unread_counter")!=null){
			this.setMessage_unread_counter((Integer)map.get("message_unread_counter"));
		}
		if(!(map.get("mode_company_id") instanceof Boolean)&& map.get("mode_company_id")!=null){
			Object[] objs = (Object[])map.get("mode_company_id");
			if(objs.length > 0){
				this.setMode_company_id((Integer)objs[0]);
			}
		}
		if(!(map.get("mode_company_id") instanceof Boolean)&& map.get("mode_company_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("mode_company_id");
			if(objs.length > 1){
				this.setMode_company_id_text((String)objs[1]);
			}
		}
		if(!(map.get("name") instanceof Boolean)&& map.get("name")!=null){
			this.setName((String)map.get("name"));
		}
		if(!(map.get("nextcall") instanceof Boolean)&& map.get("nextcall")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd").parse((String)map.get("nextcall"));
   			this.setNextcall(new Timestamp(parse.getTime()));
		}
		if(!(map.get("notes") instanceof Boolean)&& map.get("notes")!=null){
			this.setNotes((String)map.get("notes"));
		}
		if(!(map.get("number_of_days") instanceof Boolean)&& map.get("number_of_days")!=null){
			this.setNumber_of_days((Double)map.get("number_of_days"));
		}
		if(!(map.get("number_of_days_display") instanceof Boolean)&& map.get("number_of_days_display")!=null){
			this.setNumber_of_days_display((Double)map.get("number_of_days_display"));
		}
		if(!(map.get("number_of_hours_display") instanceof Boolean)&& map.get("number_of_hours_display")!=null){
			this.setNumber_of_hours_display((Double)map.get("number_of_hours_display"));
		}
		if(!(map.get("number_per_interval") instanceof Boolean)&& map.get("number_per_interval")!=null){
			this.setNumber_per_interval((Double)map.get("number_per_interval"));
		}
		if(!(map.get("parent_id") instanceof Boolean)&& map.get("parent_id")!=null){
			Object[] objs = (Object[])map.get("parent_id");
			if(objs.length > 0){
				this.setParent_id((Integer)objs[0]);
			}
		}
		if(!(map.get("parent_id") instanceof Boolean)&& map.get("parent_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("parent_id");
			if(objs.length > 1){
				this.setParent_id_text((String)objs[1]);
			}
		}
		if(!(map.get("second_approver_id") instanceof Boolean)&& map.get("second_approver_id")!=null){
			Object[] objs = (Object[])map.get("second_approver_id");
			if(objs.length > 0){
				this.setSecond_approver_id((Integer)objs[0]);
			}
		}
		if(!(map.get("second_approver_id") instanceof Boolean)&& map.get("second_approver_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("second_approver_id");
			if(objs.length > 1){
				this.setSecond_approver_id_text((String)objs[1]);
			}
		}
		if(!(map.get("state") instanceof Boolean)&& map.get("state")!=null){
			this.setState((String)map.get("state"));
		}
		if(!(map.get("type_request_unit") instanceof Boolean)&& map.get("type_request_unit")!=null){
			this.setType_request_unit((String)map.get("type_request_unit"));
		}
		if(!(map.get("unit_per_interval") instanceof Boolean)&& map.get("unit_per_interval")!=null){
			this.setUnit_per_interval((String)map.get("unit_per_interval"));
		}
		if(!(map.get("validation_type") instanceof Boolean)&& map.get("validation_type")!=null){
			this.setValidation_type((String)map.get("validation_type"));
		}
		if(!(map.get("website_message_ids") instanceof Boolean)&& map.get("website_message_ids")!=null){
			Object[] objs = (Object[])map.get("website_message_ids");
			if(objs.length > 0){
				Integer[] website_message_ids = Arrays.copyOfRange(objs,0,objs.length,Integer[].class);
				this.setWebsite_message_ids(Arrays.toString(website_message_ids).replace(" ",""));
			}
		}
		if(!(map.get("write_date") instanceof Boolean)&& map.get("write_date")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse((String)map.get("write_date"));
   			this.setWrite_date(new Timestamp(parse.getTime()));
		}
		if(!(map.get("write_uid") instanceof Boolean)&& map.get("write_uid")!=null){
			Object[] objs = (Object[])map.get("write_uid");
			if(objs.length > 0){
				this.setWrite_uid((Integer)objs[0]);
			}
		}
		if(!(map.get("write_uid") instanceof Boolean)&& map.get("write_uid")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("write_uid");
			if(objs.length > 1){
				this.setWrite_uid_text((String)objs[1]);
			}
		}
		if(!(map.get("__last_update") instanceof Boolean)&& map.get("__last_update")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse((String)map.get("__last_update"));
   			this.set__last_update(new Timestamp(parse.getTime()));
		}
	}

	public Map<String, Object> toMap() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		if(this.getAccrual()!=null&&this.getAccrualDirtyFlag()){
			map.put("accrual",Boolean.parseBoolean(this.getAccrual()));		
		}		if(this.getAccrual_limit()!=null&&this.getAccrual_limitDirtyFlag()){
			map.put("accrual_limit",this.getAccrual_limit());
		}else if(this.getAccrual_limitDirtyFlag()){
			map.put("accrual_limit",false);
		}
		if(this.getActivity_date_deadline()!=null&&this.getActivity_date_deadlineDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			String datetimeStr = sdf.format(this.getActivity_date_deadline());
			map.put("activity_date_deadline",datetimeStr);
		}else if(this.getActivity_date_deadlineDirtyFlag()){
			map.put("activity_date_deadline",false);
		}
		if(this.getActivity_ids()!=null&&this.getActivity_idsDirtyFlag()){
			map.put("activity_ids",this.getActivity_ids());
		}else if(this.getActivity_idsDirtyFlag()){
			map.put("activity_ids",false);
		}
		if(this.getActivity_state()!=null&&this.getActivity_stateDirtyFlag()){
			map.put("activity_state",this.getActivity_state());
		}else if(this.getActivity_stateDirtyFlag()){
			map.put("activity_state",false);
		}
		if(this.getActivity_summary()!=null&&this.getActivity_summaryDirtyFlag()){
			map.put("activity_summary",this.getActivity_summary());
		}else if(this.getActivity_summaryDirtyFlag()){
			map.put("activity_summary",false);
		}
		if(this.getActivity_type_id()!=null&&this.getActivity_type_idDirtyFlag()){
			map.put("activity_type_id",this.getActivity_type_id());
		}else if(this.getActivity_type_idDirtyFlag()){
			map.put("activity_type_id",false);
		}
		if(this.getActivity_user_id()!=null&&this.getActivity_user_idDirtyFlag()){
			map.put("activity_user_id",this.getActivity_user_id());
		}else if(this.getActivity_user_idDirtyFlag()){
			map.put("activity_user_id",false);
		}
		if(this.getCan_approve()!=null&&this.getCan_approveDirtyFlag()){
			map.put("can_approve",Boolean.parseBoolean(this.getCan_approve()));		
		}		if(this.getCan_reset()!=null&&this.getCan_resetDirtyFlag()){
			map.put("can_reset",Boolean.parseBoolean(this.getCan_reset()));		
		}		if(this.getCategory_id()!=null&&this.getCategory_idDirtyFlag()){
			map.put("category_id",this.getCategory_id());
		}else if(this.getCategory_idDirtyFlag()){
			map.put("category_id",false);
		}
		if(this.getCategory_id_text()!=null&&this.getCategory_id_textDirtyFlag()){
			//忽略文本外键category_id_text
		}else if(this.getCategory_id_textDirtyFlag()){
			map.put("category_id",false);
		}
		if(this.getCreate_date()!=null&&this.getCreate_dateDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String datetimeStr = sdf.format(this.getCreate_date());
			map.put("create_date",datetimeStr);
		}else if(this.getCreate_dateDirtyFlag()){
			map.put("create_date",false);
		}
		if(this.getCreate_uid()!=null&&this.getCreate_uidDirtyFlag()){
			map.put("create_uid",this.getCreate_uid());
		}else if(this.getCreate_uidDirtyFlag()){
			map.put("create_uid",false);
		}
		if(this.getCreate_uid_text()!=null&&this.getCreate_uid_textDirtyFlag()){
			//忽略文本外键create_uid_text
		}else if(this.getCreate_uid_textDirtyFlag()){
			map.put("create_uid",false);
		}
		if(this.getDate_from()!=null&&this.getDate_fromDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String datetimeStr = sdf.format(this.getDate_from());
			map.put("date_from",datetimeStr);
		}else if(this.getDate_fromDirtyFlag()){
			map.put("date_from",false);
		}
		if(this.getDate_to()!=null&&this.getDate_toDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String datetimeStr = sdf.format(this.getDate_to());
			map.put("date_to",datetimeStr);
		}else if(this.getDate_toDirtyFlag()){
			map.put("date_to",false);
		}
		if(this.getDepartment_id()!=null&&this.getDepartment_idDirtyFlag()){
			map.put("department_id",this.getDepartment_id());
		}else if(this.getDepartment_idDirtyFlag()){
			map.put("department_id",false);
		}
		if(this.getDepartment_id_text()!=null&&this.getDepartment_id_textDirtyFlag()){
			//忽略文本外键department_id_text
		}else if(this.getDepartment_id_textDirtyFlag()){
			map.put("department_id",false);
		}
		if(this.getDisplay_name()!=null&&this.getDisplay_nameDirtyFlag()){
			map.put("display_name",this.getDisplay_name());
		}else if(this.getDisplay_nameDirtyFlag()){
			map.put("display_name",false);
		}
		if(this.getDuration_display()!=null&&this.getDuration_displayDirtyFlag()){
			map.put("duration_display",this.getDuration_display());
		}else if(this.getDuration_displayDirtyFlag()){
			map.put("duration_display",false);
		}
		if(this.getEmployee_id()!=null&&this.getEmployee_idDirtyFlag()){
			map.put("employee_id",this.getEmployee_id());
		}else if(this.getEmployee_idDirtyFlag()){
			map.put("employee_id",false);
		}
		if(this.getEmployee_id_text()!=null&&this.getEmployee_id_textDirtyFlag()){
			//忽略文本外键employee_id_text
		}else if(this.getEmployee_id_textDirtyFlag()){
			map.put("employee_id",false);
		}
		if(this.getFirst_approver_id()!=null&&this.getFirst_approver_idDirtyFlag()){
			map.put("first_approver_id",this.getFirst_approver_id());
		}else if(this.getFirst_approver_idDirtyFlag()){
			map.put("first_approver_id",false);
		}
		if(this.getFirst_approver_id_text()!=null&&this.getFirst_approver_id_textDirtyFlag()){
			//忽略文本外键first_approver_id_text
		}else if(this.getFirst_approver_id_textDirtyFlag()){
			map.put("first_approver_id",false);
		}
		if(this.getHoliday_status_id()!=null&&this.getHoliday_status_idDirtyFlag()){
			map.put("holiday_status_id",this.getHoliday_status_id());
		}else if(this.getHoliday_status_idDirtyFlag()){
			map.put("holiday_status_id",false);
		}
		if(this.getHoliday_status_id_text()!=null&&this.getHoliday_status_id_textDirtyFlag()){
			//忽略文本外键holiday_status_id_text
		}else if(this.getHoliday_status_id_textDirtyFlag()){
			map.put("holiday_status_id",false);
		}
		if(this.getHoliday_type()!=null&&this.getHoliday_typeDirtyFlag()){
			map.put("holiday_type",this.getHoliday_type());
		}else if(this.getHoliday_typeDirtyFlag()){
			map.put("holiday_type",false);
		}
		if(this.getId()!=null&&this.getIdDirtyFlag()){
			map.put("id",this.getId());
		}else if(this.getIdDirtyFlag()){
			map.put("id",false);
		}
		if(this.getInterval_number()!=null&&this.getInterval_numberDirtyFlag()){
			map.put("interval_number",this.getInterval_number());
		}else if(this.getInterval_numberDirtyFlag()){
			map.put("interval_number",false);
		}
		if(this.getInterval_unit()!=null&&this.getInterval_unitDirtyFlag()){
			map.put("interval_unit",this.getInterval_unit());
		}else if(this.getInterval_unitDirtyFlag()){
			map.put("interval_unit",false);
		}
		if(this.getLinked_request_ids()!=null&&this.getLinked_request_idsDirtyFlag()){
			map.put("linked_request_ids",this.getLinked_request_ids());
		}else if(this.getLinked_request_idsDirtyFlag()){
			map.put("linked_request_ids",false);
		}
		if(this.getMessage_attachment_count()!=null&&this.getMessage_attachment_countDirtyFlag()){
			map.put("message_attachment_count",this.getMessage_attachment_count());
		}else if(this.getMessage_attachment_countDirtyFlag()){
			map.put("message_attachment_count",false);
		}
		if(this.getMessage_channel_ids()!=null&&this.getMessage_channel_idsDirtyFlag()){
			map.put("message_channel_ids",this.getMessage_channel_ids());
		}else if(this.getMessage_channel_idsDirtyFlag()){
			map.put("message_channel_ids",false);
		}
		if(this.getMessage_follower_ids()!=null&&this.getMessage_follower_idsDirtyFlag()){
			map.put("message_follower_ids",this.getMessage_follower_ids());
		}else if(this.getMessage_follower_idsDirtyFlag()){
			map.put("message_follower_ids",false);
		}
		if(this.getMessage_has_error()!=null&&this.getMessage_has_errorDirtyFlag()){
			map.put("message_has_error",Boolean.parseBoolean(this.getMessage_has_error()));		
		}		if(this.getMessage_has_error_counter()!=null&&this.getMessage_has_error_counterDirtyFlag()){
			map.put("message_has_error_counter",this.getMessage_has_error_counter());
		}else if(this.getMessage_has_error_counterDirtyFlag()){
			map.put("message_has_error_counter",false);
		}
		if(this.getMessage_ids()!=null&&this.getMessage_idsDirtyFlag()){
			map.put("message_ids",this.getMessage_ids());
		}else if(this.getMessage_idsDirtyFlag()){
			map.put("message_ids",false);
		}
		if(this.getMessage_is_follower()!=null&&this.getMessage_is_followerDirtyFlag()){
			map.put("message_is_follower",Boolean.parseBoolean(this.getMessage_is_follower()));		
		}		if(this.getMessage_main_attachment_id()!=null&&this.getMessage_main_attachment_idDirtyFlag()){
			map.put("message_main_attachment_id",this.getMessage_main_attachment_id());
		}else if(this.getMessage_main_attachment_idDirtyFlag()){
			map.put("message_main_attachment_id",false);
		}
		if(this.getMessage_needaction()!=null&&this.getMessage_needactionDirtyFlag()){
			map.put("message_needaction",Boolean.parseBoolean(this.getMessage_needaction()));		
		}		if(this.getMessage_needaction_counter()!=null&&this.getMessage_needaction_counterDirtyFlag()){
			map.put("message_needaction_counter",this.getMessage_needaction_counter());
		}else if(this.getMessage_needaction_counterDirtyFlag()){
			map.put("message_needaction_counter",false);
		}
		if(this.getMessage_partner_ids()!=null&&this.getMessage_partner_idsDirtyFlag()){
			map.put("message_partner_ids",this.getMessage_partner_ids());
		}else if(this.getMessage_partner_idsDirtyFlag()){
			map.put("message_partner_ids",false);
		}
		if(this.getMessage_unread()!=null&&this.getMessage_unreadDirtyFlag()){
			map.put("message_unread",Boolean.parseBoolean(this.getMessage_unread()));		
		}		if(this.getMessage_unread_counter()!=null&&this.getMessage_unread_counterDirtyFlag()){
			map.put("message_unread_counter",this.getMessage_unread_counter());
		}else if(this.getMessage_unread_counterDirtyFlag()){
			map.put("message_unread_counter",false);
		}
		if(this.getMode_company_id()!=null&&this.getMode_company_idDirtyFlag()){
			map.put("mode_company_id",this.getMode_company_id());
		}else if(this.getMode_company_idDirtyFlag()){
			map.put("mode_company_id",false);
		}
		if(this.getMode_company_id_text()!=null&&this.getMode_company_id_textDirtyFlag()){
			//忽略文本外键mode_company_id_text
		}else if(this.getMode_company_id_textDirtyFlag()){
			map.put("mode_company_id",false);
		}
		if(this.getName()!=null&&this.getNameDirtyFlag()){
			map.put("name",this.getName());
		}else if(this.getNameDirtyFlag()){
			map.put("name",false);
		}
		if(this.getNextcall()!=null&&this.getNextcallDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			String datetimeStr = sdf.format(this.getNextcall());
			map.put("nextcall",datetimeStr);
		}else if(this.getNextcallDirtyFlag()){
			map.put("nextcall",false);
		}
		if(this.getNotes()!=null&&this.getNotesDirtyFlag()){
			map.put("notes",this.getNotes());
		}else if(this.getNotesDirtyFlag()){
			map.put("notes",false);
		}
		if(this.getNumber_of_days()!=null&&this.getNumber_of_daysDirtyFlag()){
			map.put("number_of_days",this.getNumber_of_days());
		}else if(this.getNumber_of_daysDirtyFlag()){
			map.put("number_of_days",false);
		}
		if(this.getNumber_of_days_display()!=null&&this.getNumber_of_days_displayDirtyFlag()){
			map.put("number_of_days_display",this.getNumber_of_days_display());
		}else if(this.getNumber_of_days_displayDirtyFlag()){
			map.put("number_of_days_display",false);
		}
		if(this.getNumber_of_hours_display()!=null&&this.getNumber_of_hours_displayDirtyFlag()){
			map.put("number_of_hours_display",this.getNumber_of_hours_display());
		}else if(this.getNumber_of_hours_displayDirtyFlag()){
			map.put("number_of_hours_display",false);
		}
		if(this.getNumber_per_interval()!=null&&this.getNumber_per_intervalDirtyFlag()){
			map.put("number_per_interval",this.getNumber_per_interval());
		}else if(this.getNumber_per_intervalDirtyFlag()){
			map.put("number_per_interval",false);
		}
		if(this.getParent_id()!=null&&this.getParent_idDirtyFlag()){
			map.put("parent_id",this.getParent_id());
		}else if(this.getParent_idDirtyFlag()){
			map.put("parent_id",false);
		}
		if(this.getParent_id_text()!=null&&this.getParent_id_textDirtyFlag()){
			//忽略文本外键parent_id_text
		}else if(this.getParent_id_textDirtyFlag()){
			map.put("parent_id",false);
		}
		if(this.getSecond_approver_id()!=null&&this.getSecond_approver_idDirtyFlag()){
			map.put("second_approver_id",this.getSecond_approver_id());
		}else if(this.getSecond_approver_idDirtyFlag()){
			map.put("second_approver_id",false);
		}
		if(this.getSecond_approver_id_text()!=null&&this.getSecond_approver_id_textDirtyFlag()){
			//忽略文本外键second_approver_id_text
		}else if(this.getSecond_approver_id_textDirtyFlag()){
			map.put("second_approver_id",false);
		}
		if(this.getState()!=null&&this.getStateDirtyFlag()){
			map.put("state",this.getState());
		}else if(this.getStateDirtyFlag()){
			map.put("state",false);
		}
		if(this.getType_request_unit()!=null&&this.getType_request_unitDirtyFlag()){
			map.put("type_request_unit",this.getType_request_unit());
		}else if(this.getType_request_unitDirtyFlag()){
			map.put("type_request_unit",false);
		}
		if(this.getUnit_per_interval()!=null&&this.getUnit_per_intervalDirtyFlag()){
			map.put("unit_per_interval",this.getUnit_per_interval());
		}else if(this.getUnit_per_intervalDirtyFlag()){
			map.put("unit_per_interval",false);
		}
		if(this.getValidation_type()!=null&&this.getValidation_typeDirtyFlag()){
			map.put("validation_type",this.getValidation_type());
		}else if(this.getValidation_typeDirtyFlag()){
			map.put("validation_type",false);
		}
		if(this.getWebsite_message_ids()!=null&&this.getWebsite_message_idsDirtyFlag()){
			map.put("website_message_ids",this.getWebsite_message_ids());
		}else if(this.getWebsite_message_idsDirtyFlag()){
			map.put("website_message_ids",false);
		}
		if(this.getWrite_date()!=null&&this.getWrite_dateDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String datetimeStr = sdf.format(this.getWrite_date());
			map.put("write_date",datetimeStr);
		}else if(this.getWrite_dateDirtyFlag()){
			map.put("write_date",false);
		}
		if(this.getWrite_uid()!=null&&this.getWrite_uidDirtyFlag()){
			map.put("write_uid",this.getWrite_uid());
		}else if(this.getWrite_uidDirtyFlag()){
			map.put("write_uid",false);
		}
		if(this.getWrite_uid_text()!=null&&this.getWrite_uid_textDirtyFlag()){
			//忽略文本外键write_uid_text
		}else if(this.getWrite_uid_textDirtyFlag()){
			map.put("write_uid",false);
		}
		if(this.get__last_update()!=null&&this.get__last_updateDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String datetimeStr = sdf.format(this.get__last_update());
			map.put("__last_update",datetimeStr);
		}else if(this.get__last_updateDirtyFlag()){
			map.put("__last_update",false);
		}
		return map;
	}

}
