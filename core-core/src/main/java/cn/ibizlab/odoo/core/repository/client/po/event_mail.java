package cn.ibizlab.odoo.core.repository.client.po;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigDecimal;

/**
 * [event_mail] 对象
 */
public interface event_mail {

    public Timestamp getCreate_date();

    public void setCreate_date(Timestamp create_date);

    public Integer getCreate_uid();

    public void setCreate_uid(Integer create_uid);

    public String getCreate_uid_text();

    public void setCreate_uid_text(String create_uid_text);

    public String getDisplay_name();

    public void setDisplay_name(String display_name);

    public String getDone();

    public void setDone(String done);

    public Integer getEvent_id();

    public void setEvent_id(Integer event_id);

    public String getEvent_id_text();

    public void setEvent_id_text(String event_id_text);

    public Integer getId();

    public void setId(Integer id);

    public Integer getInterval_nbr();

    public void setInterval_nbr(Integer interval_nbr);

    public String getInterval_type();

    public void setInterval_type(String interval_type);

    public String getInterval_unit();

    public void setInterval_unit(String interval_unit);

    public String getMail_registration_ids();

    public void setMail_registration_ids(String mail_registration_ids);

    public String getMail_sent();

    public void setMail_sent(String mail_sent);

    public Timestamp getScheduled_date();

    public void setScheduled_date(Timestamp scheduled_date);

    public Integer getSequence();

    public void setSequence(Integer sequence);

    public Integer getTemplate_id();

    public void setTemplate_id(Integer template_id);

    public String getTemplate_id_text();

    public void setTemplate_id_text(String template_id_text);

    public Timestamp getWrite_date();

    public void setWrite_date(Timestamp write_date);

    public Integer getWrite_uid();

    public void setWrite_uid(Integer write_uid);

    public String getWrite_uid_text();

    public void setWrite_uid_text(String write_uid_text);

    public Timestamp get__last_update();

    public void set__last_update(Timestamp __last_update);

}
