package cn.ibizlab.odoo.core.odoo_mail.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_mail.domain.Mail_mass_mailing_list;
import cn.ibizlab.odoo.core.odoo_mail.filter.Mail_mass_mailing_listSearchContext;
import cn.ibizlab.odoo.core.odoo_mail.service.IMail_mass_mailing_listService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;



import cn.ibizlab.odoo.core.odoo_mail.client.mail_mass_mailing_listOdooClient;
import cn.ibizlab.odoo.core.odoo_mail.clientmodel.mail_mass_mailing_listClientModel;

/**
 * 实体[邮件列表] 服务对象接口实现
 */
@Slf4j
@Service
public class Mail_mass_mailing_listServiceImpl implements IMail_mass_mailing_listService {

    @Autowired
    mail_mass_mailing_listOdooClient mail_mass_mailing_listOdooClient;


    @Override
    public boolean update(Mail_mass_mailing_list et) {
        mail_mass_mailing_listClientModel clientModel = convert2Model(et,null);
		mail_mass_mailing_listOdooClient.update(clientModel);
        Mail_mass_mailing_list rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }
    
    public void updateBatch(List<Mail_mass_mailing_list> list){
    }

    @Override
    public boolean create(Mail_mass_mailing_list et) {
        mail_mass_mailing_listClientModel clientModel = convert2Model(et,null);
		mail_mass_mailing_listOdooClient.create(clientModel);
        Mail_mass_mailing_list rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Mail_mass_mailing_list> list){
    }

    @Override
    public Mail_mass_mailing_list get(Integer id) {
        mail_mass_mailing_listClientModel clientModel = new mail_mass_mailing_listClientModel();
        clientModel.setId(id);
		mail_mass_mailing_listOdooClient.get(clientModel);
        Mail_mass_mailing_list et = convert2Domain(clientModel,null);
        if(et==null){
            et=new Mail_mass_mailing_list();
            et.setId(id);
        }
        else{
        }

        return  et;
    }

    @Override
    public boolean remove(Integer id) {
        mail_mass_mailing_listClientModel clientModel = new mail_mass_mailing_listClientModel();
        clientModel.setId(id);
		mail_mass_mailing_listOdooClient.remove(clientModel);
        return true; 
    }

    public void removeBatch(Collection<Integer> idList){
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Mail_mass_mailing_list> searchDefault(Mail_mass_mailing_listSearchContext context) {
        List<Mail_mass_mailing_list> list = new ArrayList<Mail_mass_mailing_list>();
        Page<mail_mass_mailing_listClientModel> clientModelList = mail_mass_mailing_listOdooClient.search(context);
        for(mail_mass_mailing_listClientModel clientModel : clientModelList){
            list.add( this.convert2Domain(clientModel,null));
        }
        return new PageImpl<Mail_mass_mailing_list>(list, context.getPageable(), clientModelList.getTotalElements());
    }

    public mail_mass_mailing_listClientModel convert2Model(Mail_mass_mailing_list domain , mail_mass_mailing_listClientModel model) {
        if(domain == null)
            return model ;
        if(model==null){
            model = new mail_mass_mailing_listClientModel();

        }
        if((Boolean) domain.getExtensionparams().get("dirtyflagenable")){
            if((Boolean) domain.getExtensionparams().get("write_datedirtyflag"))
                model.setWrite_date(domain.getWriteDate());
            if((Boolean) domain.getExtensionparams().get("iddirtyflag"))
                model.setId(domain.getId());
            if((Boolean) domain.getExtensionparams().get("contact_nbrdirtyflag"))
                model.setContact_nbr(domain.getContactNbr());
            if((Boolean) domain.getExtensionparams().get("__last_updatedirtyflag"))
                model.set__last_update(domain.getLastUpdate());
            if((Boolean) domain.getExtensionparams().get("create_datedirtyflag"))
                model.setCreate_date(domain.getCreateDate());
            if((Boolean) domain.getExtensionparams().get("subscription_contact_idsdirtyflag"))
                model.setSubscription_contact_ids(domain.getSubscriptionContactIds());
            if((Boolean) domain.getExtensionparams().get("display_namedirtyflag"))
                model.setDisplay_name(domain.getDisplayName());
            if((Boolean) domain.getExtensionparams().get("contact_idsdirtyflag"))
                model.setContact_ids(domain.getContactIds());
            if((Boolean) domain.getExtensionparams().get("popup_contentdirtyflag"))
                model.setPopup_content(domain.getPopupContent());
            if((Boolean) domain.getExtensionparams().get("is_publicdirtyflag"))
                model.setIs_public(domain.getIsPublic());
            if((Boolean) domain.getExtensionparams().get("namedirtyflag"))
                model.setName(domain.getName());
            if((Boolean) domain.getExtensionparams().get("popup_redirect_urldirtyflag"))
                model.setPopup_redirect_url(domain.getPopupRedirectUrl());
            if((Boolean) domain.getExtensionparams().get("activedirtyflag"))
                model.setActive(domain.getActive());
            if((Boolean) domain.getExtensionparams().get("create_uid_textdirtyflag"))
                model.setCreate_uid_text(domain.getCreateUidText());
            if((Boolean) domain.getExtensionparams().get("write_uid_textdirtyflag"))
                model.setWrite_uid_text(domain.getWriteUidText());
            if((Boolean) domain.getExtensionparams().get("write_uiddirtyflag"))
                model.setWrite_uid(domain.getWriteUid());
            if((Boolean) domain.getExtensionparams().get("create_uiddirtyflag"))
                model.setCreate_uid(domain.getCreateUid());
            domain.getFocusNull().clear();
        }
        return model ;
    }

    public Mail_mass_mailing_list convert2Domain( mail_mass_mailing_listClientModel model ,Mail_mass_mailing_list domain) {
        if(model == null)
            return domain ;
        if(domain == null){
            domain = new Mail_mass_mailing_list();
        }

        if(model.getWrite_dateDirtyFlag())
            domain.setWriteDate(model.getWrite_date());
        if(model.getIdDirtyFlag())
            domain.setId(model.getId());
        if(model.getContact_nbrDirtyFlag())
            domain.setContactNbr(model.getContact_nbr());
        if(model.get__last_updateDirtyFlag())
            domain.setLastUpdate(model.get__last_update());
        if(model.getCreate_dateDirtyFlag())
            domain.setCreateDate(model.getCreate_date());
        if(model.getSubscription_contact_idsDirtyFlag())
            domain.setSubscriptionContactIds(model.getSubscription_contact_ids());
        if(model.getDisplay_nameDirtyFlag())
            domain.setDisplayName(model.getDisplay_name());
        if(model.getContact_idsDirtyFlag())
            domain.setContactIds(model.getContact_ids());
        if(model.getPopup_contentDirtyFlag())
            domain.setPopupContent(model.getPopup_content());
        if(model.getIs_publicDirtyFlag())
            domain.setIsPublic(model.getIs_public());
        if(model.getNameDirtyFlag())
            domain.setName(model.getName());
        if(model.getPopup_redirect_urlDirtyFlag())
            domain.setPopupRedirectUrl(model.getPopup_redirect_url());
        if(model.getActiveDirtyFlag())
            domain.setActive(model.getActive());
        if(model.getCreate_uid_textDirtyFlag())
            domain.setCreateUidText(model.getCreate_uid_text());
        if(model.getWrite_uid_textDirtyFlag())
            domain.setWriteUidText(model.getWrite_uid_text());
        if(model.getWrite_uidDirtyFlag())
            domain.setWriteUid(model.getWrite_uid());
        if(model.getCreate_uidDirtyFlag())
            domain.setCreateUid(model.getCreate_uid());
        return domain ;
    }

}

    



