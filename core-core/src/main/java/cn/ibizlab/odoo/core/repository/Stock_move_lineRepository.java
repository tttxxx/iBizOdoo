package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Stock_move_line;
import cn.ibizlab.odoo.core.odoo_stock.filter.Stock_move_lineSearchContext;

/**
 * 实体 [产品移动(移库明细)] 存储对象
 */
public interface Stock_move_lineRepository extends Repository<Stock_move_line> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Stock_move_line> searchDefault(Stock_move_lineSearchContext context);

    Stock_move_line convert2PO(cn.ibizlab.odoo.core.odoo_stock.domain.Stock_move_line domain , Stock_move_line po) ;

    cn.ibizlab.odoo.core.odoo_stock.domain.Stock_move_line convert2Domain( Stock_move_line po ,cn.ibizlab.odoo.core.odoo_stock.domain.Stock_move_line domain) ;

}
