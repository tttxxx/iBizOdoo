package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Stock_location_route;
import cn.ibizlab.odoo.core.odoo_stock.filter.Stock_location_routeSearchContext;

/**
 * 实体 [库存路线] 存储对象
 */
public interface Stock_location_routeRepository extends Repository<Stock_location_route> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Stock_location_route> searchDefault(Stock_location_routeSearchContext context);

    Stock_location_route convert2PO(cn.ibizlab.odoo.core.odoo_stock.domain.Stock_location_route domain , Stock_location_route po) ;

    cn.ibizlab.odoo.core.odoo_stock.domain.Stock_location_route convert2Domain( Stock_location_route po ,cn.ibizlab.odoo.core.odoo_stock.domain.Stock_location_route domain) ;

}
