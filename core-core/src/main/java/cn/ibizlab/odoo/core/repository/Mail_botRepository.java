package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Mail_bot;
import cn.ibizlab.odoo.core.odoo_mail.filter.Mail_botSearchContext;

/**
 * 实体 [邮件机器人] 存储对象
 */
public interface Mail_botRepository extends Repository<Mail_bot> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Mail_bot> searchDefault(Mail_botSearchContext context);

    Mail_bot convert2PO(cn.ibizlab.odoo.core.odoo_mail.domain.Mail_bot domain , Mail_bot po) ;

    cn.ibizlab.odoo.core.odoo_mail.domain.Mail_bot convert2Domain( Mail_bot po ,cn.ibizlab.odoo.core.odoo_mail.domain.Mail_bot domain) ;

}
