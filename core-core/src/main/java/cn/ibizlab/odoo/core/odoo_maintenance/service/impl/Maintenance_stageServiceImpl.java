package cn.ibizlab.odoo.core.odoo_maintenance.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_maintenance.domain.Maintenance_stage;
import cn.ibizlab.odoo.core.odoo_maintenance.filter.Maintenance_stageSearchContext;
import cn.ibizlab.odoo.core.odoo_maintenance.service.IMaintenance_stageService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;



import cn.ibizlab.odoo.core.odoo_maintenance.client.maintenance_stageOdooClient;
import cn.ibizlab.odoo.core.odoo_maintenance.clientmodel.maintenance_stageClientModel;

/**
 * 实体[保养阶段] 服务对象接口实现
 */
@Slf4j
@Service
public class Maintenance_stageServiceImpl implements IMaintenance_stageService {

    @Autowired
    maintenance_stageOdooClient maintenance_stageOdooClient;


    @Override
    public boolean create(Maintenance_stage et) {
        maintenance_stageClientModel clientModel = convert2Model(et,null);
		maintenance_stageOdooClient.create(clientModel);
        Maintenance_stage rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Maintenance_stage> list){
    }

    @Override
    public Maintenance_stage get(Integer id) {
        maintenance_stageClientModel clientModel = new maintenance_stageClientModel();
        clientModel.setId(id);
		maintenance_stageOdooClient.get(clientModel);
        Maintenance_stage et = convert2Domain(clientModel,null);
        if(et==null){
            et=new Maintenance_stage();
            et.setId(id);
        }
        else{
        }

        return  et;
    }

    @Override
    public boolean update(Maintenance_stage et) {
        maintenance_stageClientModel clientModel = convert2Model(et,null);
		maintenance_stageOdooClient.update(clientModel);
        Maintenance_stage rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }
    
    public void updateBatch(List<Maintenance_stage> list){
    }

    @Override
    public boolean remove(Integer id) {
        maintenance_stageClientModel clientModel = new maintenance_stageClientModel();
        clientModel.setId(id);
		maintenance_stageOdooClient.remove(clientModel);
        return true; 
    }

    public void removeBatch(Collection<Integer> idList){
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Maintenance_stage> searchDefault(Maintenance_stageSearchContext context) {
        List<Maintenance_stage> list = new ArrayList<Maintenance_stage>();
        Page<maintenance_stageClientModel> clientModelList = maintenance_stageOdooClient.search(context);
        for(maintenance_stageClientModel clientModel : clientModelList){
            list.add( this.convert2Domain(clientModel,null));
        }
        return new PageImpl<Maintenance_stage>(list, context.getPageable(), clientModelList.getTotalElements());
    }

    public maintenance_stageClientModel convert2Model(Maintenance_stage domain , maintenance_stageClientModel model) {
        if(domain == null)
            return model ;
        if(model==null){
            model = new maintenance_stageClientModel();

        }
        if((Boolean) domain.getExtensionparams().get("dirtyflagenable")){
            if((Boolean) domain.getExtensionparams().get("iddirtyflag"))
                model.setId(domain.getId());
            if((Boolean) domain.getExtensionparams().get("__last_updatedirtyflag"))
                model.set__last_update(domain.getLastUpdate());
            if((Boolean) domain.getExtensionparams().get("sequencedirtyflag"))
                model.setSequence(domain.getSequence());
            if((Boolean) domain.getExtensionparams().get("donedirtyflag"))
                model.setDone(domain.getDone());
            if((Boolean) domain.getExtensionparams().get("create_datedirtyflag"))
                model.setCreate_date(domain.getCreateDate());
            if((Boolean) domain.getExtensionparams().get("write_datedirtyflag"))
                model.setWrite_date(domain.getWriteDate());
            if((Boolean) domain.getExtensionparams().get("folddirtyflag"))
                model.setFold(domain.getFold());
            if((Boolean) domain.getExtensionparams().get("namedirtyflag"))
                model.setName(domain.getName());
            if((Boolean) domain.getExtensionparams().get("display_namedirtyflag"))
                model.setDisplay_name(domain.getDisplayName());
            if((Boolean) domain.getExtensionparams().get("write_uid_textdirtyflag"))
                model.setWrite_uid_text(domain.getWriteUidText());
            if((Boolean) domain.getExtensionparams().get("create_uid_textdirtyflag"))
                model.setCreate_uid_text(domain.getCreateUidText());
            if((Boolean) domain.getExtensionparams().get("create_uiddirtyflag"))
                model.setCreate_uid(domain.getCreateUid());
            if((Boolean) domain.getExtensionparams().get("write_uiddirtyflag"))
                model.setWrite_uid(domain.getWriteUid());
            domain.getFocusNull().clear();
        }
        return model ;
    }

    public Maintenance_stage convert2Domain( maintenance_stageClientModel model ,Maintenance_stage domain) {
        if(model == null)
            return domain ;
        if(domain == null){
            domain = new Maintenance_stage();
        }

        if(model.getIdDirtyFlag())
            domain.setId(model.getId());
        if(model.get__last_updateDirtyFlag())
            domain.setLastUpdate(model.get__last_update());
        if(model.getSequenceDirtyFlag())
            domain.setSequence(model.getSequence());
        if(model.getDoneDirtyFlag())
            domain.setDone(model.getDone());
        if(model.getCreate_dateDirtyFlag())
            domain.setCreateDate(model.getCreate_date());
        if(model.getWrite_dateDirtyFlag())
            domain.setWriteDate(model.getWrite_date());
        if(model.getFoldDirtyFlag())
            domain.setFold(model.getFold());
        if(model.getNameDirtyFlag())
            domain.setName(model.getName());
        if(model.getDisplay_nameDirtyFlag())
            domain.setDisplayName(model.getDisplay_name());
        if(model.getWrite_uid_textDirtyFlag())
            domain.setWriteUidText(model.getWrite_uid_text());
        if(model.getCreate_uid_textDirtyFlag())
            domain.setCreateUidText(model.getCreate_uid_text());
        if(model.getCreate_uidDirtyFlag())
            domain.setCreateUid(model.getCreate_uid());
        if(model.getWrite_uidDirtyFlag())
            domain.setWriteUid(model.getWrite_uid());
        return domain ;
    }

}

    



