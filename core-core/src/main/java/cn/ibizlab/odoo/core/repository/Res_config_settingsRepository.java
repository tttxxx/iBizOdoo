package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Res_config_settings;
import cn.ibizlab.odoo.core.odoo_base.filter.Res_config_settingsSearchContext;

/**
 * 实体 [配置设定] 存储对象
 */
public interface Res_config_settingsRepository extends Repository<Res_config_settings> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Res_config_settings> searchDefault(Res_config_settingsSearchContext context);

    Res_config_settings convert2PO(cn.ibizlab.odoo.core.odoo_base.domain.Res_config_settings domain , Res_config_settings po) ;

    cn.ibizlab.odoo.core.odoo_base.domain.Res_config_settings convert2Domain( Res_config_settings po ,cn.ibizlab.odoo.core.odoo_base.domain.Res_config_settings domain) ;

}
