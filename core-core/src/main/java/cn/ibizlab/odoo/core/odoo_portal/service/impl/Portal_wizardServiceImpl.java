package cn.ibizlab.odoo.core.odoo_portal.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_portal.domain.Portal_wizard;
import cn.ibizlab.odoo.core.odoo_portal.filter.Portal_wizardSearchContext;
import cn.ibizlab.odoo.core.odoo_portal.service.IPortal_wizardService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;



import cn.ibizlab.odoo.core.odoo_portal.client.portal_wizardOdooClient;
import cn.ibizlab.odoo.core.odoo_portal.clientmodel.portal_wizardClientModel;

/**
 * 实体[授予门户访问] 服务对象接口实现
 */
@Slf4j
@Service
public class Portal_wizardServiceImpl implements IPortal_wizardService {

    @Autowired
    portal_wizardOdooClient portal_wizardOdooClient;


    @Override
    public boolean update(Portal_wizard et) {
        portal_wizardClientModel clientModel = convert2Model(et,null);
		portal_wizardOdooClient.update(clientModel);
        Portal_wizard rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }
    
    public void updateBatch(List<Portal_wizard> list){
    }

    @Override
    public boolean remove(Integer id) {
        portal_wizardClientModel clientModel = new portal_wizardClientModel();
        clientModel.setId(id);
		portal_wizardOdooClient.remove(clientModel);
        return true; 
    }

    public void removeBatch(Collection<Integer> idList){
    }

    @Override
    public boolean create(Portal_wizard et) {
        portal_wizardClientModel clientModel = convert2Model(et,null);
		portal_wizardOdooClient.create(clientModel);
        Portal_wizard rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Portal_wizard> list){
    }

    @Override
    public Portal_wizard get(Integer id) {
        portal_wizardClientModel clientModel = new portal_wizardClientModel();
        clientModel.setId(id);
		portal_wizardOdooClient.get(clientModel);
        Portal_wizard et = convert2Domain(clientModel,null);
        if(et==null){
            et=new Portal_wizard();
            et.setId(id);
        }
        else{
        }

        return  et;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Portal_wizard> searchDefault(Portal_wizardSearchContext context) {
        List<Portal_wizard> list = new ArrayList<Portal_wizard>();
        Page<portal_wizardClientModel> clientModelList = portal_wizardOdooClient.search(context);
        for(portal_wizardClientModel clientModel : clientModelList){
            list.add( this.convert2Domain(clientModel,null));
        }
        return new PageImpl<Portal_wizard>(list, context.getPageable(), clientModelList.getTotalElements());
    }

    public portal_wizardClientModel convert2Model(Portal_wizard domain , portal_wizardClientModel model) {
        if(domain == null)
            return model ;
        if(model==null){
            model = new portal_wizardClientModel();

        }
        if((Boolean) domain.getExtensionparams().get("dirtyflagenable")){
            if((Boolean) domain.getExtensionparams().get("display_namedirtyflag"))
                model.setDisplay_name(domain.getDisplayName());
            if((Boolean) domain.getExtensionparams().get("write_datedirtyflag"))
                model.setWrite_date(domain.getWriteDate());
            if((Boolean) domain.getExtensionparams().get("user_idsdirtyflag"))
                model.setUser_ids(domain.getUserIds());
            if((Boolean) domain.getExtensionparams().get("iddirtyflag"))
                model.setId(domain.getId());
            if((Boolean) domain.getExtensionparams().get("__last_updatedirtyflag"))
                model.set__last_update(domain.getLastUpdate());
            if((Boolean) domain.getExtensionparams().get("welcome_messagedirtyflag"))
                model.setWelcome_message(domain.getWelcomeMessage());
            if((Boolean) domain.getExtensionparams().get("create_datedirtyflag"))
                model.setCreate_date(domain.getCreateDate());
            if((Boolean) domain.getExtensionparams().get("create_uid_textdirtyflag"))
                model.setCreate_uid_text(domain.getCreateUidText());
            if((Boolean) domain.getExtensionparams().get("write_uid_textdirtyflag"))
                model.setWrite_uid_text(domain.getWriteUidText());
            if((Boolean) domain.getExtensionparams().get("create_uiddirtyflag"))
                model.setCreate_uid(domain.getCreateUid());
            if((Boolean) domain.getExtensionparams().get("write_uiddirtyflag"))
                model.setWrite_uid(domain.getWriteUid());
            domain.getFocusNull().clear();
        }
        return model ;
    }

    public Portal_wizard convert2Domain( portal_wizardClientModel model ,Portal_wizard domain) {
        if(model == null)
            return domain ;
        if(domain == null){
            domain = new Portal_wizard();
        }

        if(model.getDisplay_nameDirtyFlag())
            domain.setDisplayName(model.getDisplay_name());
        if(model.getWrite_dateDirtyFlag())
            domain.setWriteDate(model.getWrite_date());
        if(model.getUser_idsDirtyFlag())
            domain.setUserIds(model.getUser_ids());
        if(model.getIdDirtyFlag())
            domain.setId(model.getId());
        if(model.get__last_updateDirtyFlag())
            domain.setLastUpdate(model.get__last_update());
        if(model.getWelcome_messageDirtyFlag())
            domain.setWelcomeMessage(model.getWelcome_message());
        if(model.getCreate_dateDirtyFlag())
            domain.setCreateDate(model.getCreate_date());
        if(model.getCreate_uid_textDirtyFlag())
            domain.setCreateUidText(model.getCreate_uid_text());
        if(model.getWrite_uid_textDirtyFlag())
            domain.setWriteUidText(model.getWrite_uid_text());
        if(model.getCreate_uidDirtyFlag())
            domain.setCreateUid(model.getCreate_uid());
        if(model.getWrite_uidDirtyFlag())
            domain.setWriteUid(model.getWrite_uid());
        return domain ;
    }

}

    



