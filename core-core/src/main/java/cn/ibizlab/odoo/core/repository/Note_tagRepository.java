package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Note_tag;
import cn.ibizlab.odoo.core.odoo_note.filter.Note_tagSearchContext;

/**
 * 实体 [便签标签] 存储对象
 */
public interface Note_tagRepository extends Repository<Note_tag> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Note_tag> searchDefault(Note_tagSearchContext context);

    Note_tag convert2PO(cn.ibizlab.odoo.core.odoo_note.domain.Note_tag domain , Note_tag po) ;

    cn.ibizlab.odoo.core.odoo_note.domain.Note_tag convert2Domain( Note_tag po ,cn.ibizlab.odoo.core.odoo_note.domain.Note_tag domain) ;

}
