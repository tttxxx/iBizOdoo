package cn.ibizlab.odoo.core.repository.po ;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.math.BigInteger;

import cn.ibizlab.odoo.core.odoo_im_livechat.filter.Im_livechat_channelSearchContext;

/**
 * 实体 [即时聊天] 存储模型
 */
public interface Im_livechat_channel{

    /**
     * 操作员
     */
    String getUser_ids();

    void setUser_ids(String user_ids);

    /**
     * 获取 [操作员]脏标记
     */
    boolean getUser_idsDirtyFlag();

    /**
     * 网站网址
     */
    String getWebsite_url();

    void setWebsite_url(String website_url);

    /**
     * 获取 [网站网址]脏标记
     */
    boolean getWebsite_urlDirtyFlag();

    /**
     * 规则
     */
    String getRule_ids();

    void setRule_ids(String rule_ids);

    /**
     * 获取 [规则]脏标记
     */
    boolean getRule_idsDirtyFlag();

    /**
     * 图像
     */
    byte[] getImage();

    void setImage(byte[] image);

    /**
     * 获取 [图像]脏标记
     */
    boolean getImageDirtyFlag();

    /**
     * Web页
     */
    String getWeb_page();

    void setWeb_page(String web_page);

    /**
     * 获取 [Web页]脏标记
     */
    boolean getWeb_pageDirtyFlag();

    /**
     * 已发布
     */
    String getIs_published();

    void setIs_published(String is_published);

    /**
     * 获取 [已发布]脏标记
     */
    boolean getIs_publishedDirtyFlag();

    /**
     * 脚本（外部）
     */
    String getScript_external();

    void setScript_external(String script_external);

    /**
     * 获取 [脚本（外部）]脏标记
     */
    boolean getScript_externalDirtyFlag();

    /**
     * 对话数
     */
    Integer getNbr_channel();

    void setNbr_channel(Integer nbr_channel);

    /**
     * 获取 [对话数]脏标记
     */
    boolean getNbr_channelDirtyFlag();

    /**
     * 按钮的文本
     */
    String getButton_text();

    void setButton_text(String button_text);

    /**
     * 获取 [按钮的文本]脏标记
     */
    boolean getButton_textDirtyFlag();

    /**
     * 网站说明
     */
    String getWebsite_description();

    void setWebsite_description(String website_description);

    /**
     * 获取 [网站说明]脏标记
     */
    boolean getWebsite_descriptionDirtyFlag();

    /**
     * 会话
     */
    String getChannel_ids();

    void setChannel_ids(String channel_ids);

    /**
     * 获取 [会话]脏标记
     */
    boolean getChannel_idsDirtyFlag();

    /**
     * 名称
     */
    String getName();

    void setName(String name);

    /**
     * 获取 [名称]脏标记
     */
    boolean getNameDirtyFlag();

    /**
     * 在当前网站显示
     */
    String getWebsite_published();

    void setWebsite_published(String website_published);

    /**
     * 获取 [在当前网站显示]脏标记
     */
    boolean getWebsite_publishedDirtyFlag();

    /**
     * 最后修改日
     */
    Timestamp get__last_update();

    void set__last_update(Timestamp __last_update);

    /**
     * 获取 [最后修改日]脏标记
     */
    boolean get__last_updateDirtyFlag();

    /**
     * 创建时间
     */
    Timestamp getCreate_date();

    void setCreate_date(Timestamp create_date);

    /**
     * 获取 [创建时间]脏标记
     */
    boolean getCreate_dateDirtyFlag();

    /**
     * 显示名称
     */
    String getDisplay_name();

    void setDisplay_name(String display_name);

    /**
     * 获取 [显示名称]脏标记
     */
    boolean getDisplay_nameDirtyFlag();

    /**
     * ID
     */
    Integer getId();

    void setId(Integer id);

    /**
     * 获取 [ID]脏标记
     */
    boolean getIdDirtyFlag();

    /**
     * 普通
     */
    byte[] getImage_medium();

    void setImage_medium(byte[] image_medium);

    /**
     * 获取 [普通]脏标记
     */
    boolean getImage_mediumDirtyFlag();

    /**
     * % 高兴
     */
    Integer getRating_percentage_satisfaction();

    void setRating_percentage_satisfaction(Integer rating_percentage_satisfaction);

    /**
     * 获取 [% 高兴]脏标记
     */
    boolean getRating_percentage_satisfactionDirtyFlag();

    /**
     * 聊天输入为空时显示
     */
    String getInput_placeholder();

    void setInput_placeholder(String input_placeholder);

    /**
     * 获取 [聊天输入为空时显示]脏标记
     */
    boolean getInput_placeholderDirtyFlag();

    /**
     * 缩略图
     */
    byte[] getImage_small();

    void setImage_small(byte[] image_small);

    /**
     * 获取 [缩略图]脏标记
     */
    boolean getImage_smallDirtyFlag();

    /**
     * 您是否在频道中？
     */
    String getAre_you_inside();

    void setAre_you_inside(String are_you_inside);

    /**
     * 获取 [您是否在频道中？]脏标记
     */
    boolean getAre_you_insideDirtyFlag();

    /**
     * 欢迎信息
     */
    String getDefault_message();

    void setDefault_message(String default_message);

    /**
     * 获取 [欢迎信息]脏标记
     */
    boolean getDefault_messageDirtyFlag();

    /**
     * 最后更新时间
     */
    Timestamp getWrite_date();

    void setWrite_date(Timestamp write_date);

    /**
     * 获取 [最后更新时间]脏标记
     */
    boolean getWrite_dateDirtyFlag();

    /**
     * 最后更新人
     */
    String getWrite_uid_text();

    void setWrite_uid_text(String write_uid_text);

    /**
     * 获取 [最后更新人]脏标记
     */
    boolean getWrite_uid_textDirtyFlag();

    /**
     * 创建人
     */
    String getCreate_uid_text();

    void setCreate_uid_text(String create_uid_text);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uid_textDirtyFlag();

    /**
     * 最后更新人
     */
    Integer getWrite_uid();

    void setWrite_uid(Integer write_uid);

    /**
     * 获取 [最后更新人]脏标记
     */
    boolean getWrite_uidDirtyFlag();

    /**
     * 创建人
     */
    Integer getCreate_uid();

    void setCreate_uid(Integer create_uid);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uidDirtyFlag();

}
