package cn.ibizlab.odoo.core.repository.client.po;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigDecimal;

/**
 * [product_public_category] 对象
 */
public interface product_public_category {

    public String getChild_id();

    public void setChild_id(String child_id);

    public Timestamp getCreate_date();

    public void setCreate_date(Timestamp create_date);

    public Integer getCreate_uid();

    public void setCreate_uid(Integer create_uid);

    public String getCreate_uid_text();

    public void setCreate_uid_text(String create_uid_text);

    public String getDisplay_name();

    public void setDisplay_name(String display_name);

    public Integer getId();

    public void setId(Integer id);

    public byte[] getImage();

    public void setImage(byte[] image);

    public byte[] getImage_medium();

    public void setImage_medium(byte[] image_medium);

    public byte[] getImage_small();

    public void setImage_small(byte[] image_small);

    public String getIs_seo_optimized();

    public void setIs_seo_optimized(String is_seo_optimized);

    public String getName();

    public void setName(String name);

    public Integer getParent_id();

    public void setParent_id(Integer parent_id);

    public String getParent_id_text();

    public void setParent_id_text(String parent_id_text);

    public Integer getSequence();

    public void setSequence(Integer sequence);

    public String getWebsite_meta_description();

    public void setWebsite_meta_description(String website_meta_description);

    public String getWebsite_meta_keywords();

    public void setWebsite_meta_keywords(String website_meta_keywords);

    public String getWebsite_meta_og_img();

    public void setWebsite_meta_og_img(String website_meta_og_img);

    public String getWebsite_meta_title();

    public void setWebsite_meta_title(String website_meta_title);

    public Timestamp getWrite_date();

    public void setWrite_date(Timestamp write_date);

    public Integer getWrite_uid();

    public void setWrite_uid(Integer write_uid);

    public String getWrite_uid_text();

    public void setWrite_uid_text(String write_uid_text);

    public Timestamp get__last_update();

    public void set__last_update(Timestamp __last_update);

}
