package cn.ibizlab.odoo.core.odoo_fleet.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_fleet.domain.Fleet_vehicle_state;
import cn.ibizlab.odoo.core.odoo_fleet.filter.Fleet_vehicle_stateSearchContext;
import cn.ibizlab.odoo.core.odoo_fleet.service.IFleet_vehicle_stateService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;



import cn.ibizlab.odoo.core.odoo_fleet.client.fleet_vehicle_stateOdooClient;
import cn.ibizlab.odoo.core.odoo_fleet.clientmodel.fleet_vehicle_stateClientModel;

/**
 * 实体[车辆状态] 服务对象接口实现
 */
@Slf4j
@Service
public class Fleet_vehicle_stateServiceImpl implements IFleet_vehicle_stateService {

    @Autowired
    fleet_vehicle_stateOdooClient fleet_vehicle_stateOdooClient;


    @Override
    public Fleet_vehicle_state get(Integer id) {
        fleet_vehicle_stateClientModel clientModel = new fleet_vehicle_stateClientModel();
        clientModel.setId(id);
		fleet_vehicle_stateOdooClient.get(clientModel);
        Fleet_vehicle_state et = convert2Domain(clientModel,null);
        if(et==null){
            et=new Fleet_vehicle_state();
            et.setId(id);
        }
        else{
        }

        return  et;
    }

    @Override
    public boolean create(Fleet_vehicle_state et) {
        fleet_vehicle_stateClientModel clientModel = convert2Model(et,null);
		fleet_vehicle_stateOdooClient.create(clientModel);
        Fleet_vehicle_state rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Fleet_vehicle_state> list){
    }

    @Override
    public boolean update(Fleet_vehicle_state et) {
        fleet_vehicle_stateClientModel clientModel = convert2Model(et,null);
		fleet_vehicle_stateOdooClient.update(clientModel);
        Fleet_vehicle_state rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }
    
    public void updateBatch(List<Fleet_vehicle_state> list){
    }

    @Override
    public boolean remove(Integer id) {
        fleet_vehicle_stateClientModel clientModel = new fleet_vehicle_stateClientModel();
        clientModel.setId(id);
		fleet_vehicle_stateOdooClient.remove(clientModel);
        return true; 
    }

    public void removeBatch(Collection<Integer> idList){
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Fleet_vehicle_state> searchDefault(Fleet_vehicle_stateSearchContext context) {
        List<Fleet_vehicle_state> list = new ArrayList<Fleet_vehicle_state>();
        Page<fleet_vehicle_stateClientModel> clientModelList = fleet_vehicle_stateOdooClient.search(context);
        for(fleet_vehicle_stateClientModel clientModel : clientModelList){
            list.add( this.convert2Domain(clientModel,null));
        }
        return new PageImpl<Fleet_vehicle_state>(list, context.getPageable(), clientModelList.getTotalElements());
    }

    public fleet_vehicle_stateClientModel convert2Model(Fleet_vehicle_state domain , fleet_vehicle_stateClientModel model) {
        if(domain == null)
            return model ;
        if(model==null){
            model = new fleet_vehicle_stateClientModel();

        }
        if((Boolean) domain.getExtensionparams().get("dirtyflagenable")){
            if((Boolean) domain.getExtensionparams().get("create_datedirtyflag"))
                model.setCreate_date(domain.getCreateDate());
            if((Boolean) domain.getExtensionparams().get("write_datedirtyflag"))
                model.setWrite_date(domain.getWriteDate());
            if((Boolean) domain.getExtensionparams().get("__last_updatedirtyflag"))
                model.set__last_update(domain.getLastUpdate());
            if((Boolean) domain.getExtensionparams().get("iddirtyflag"))
                model.setId(domain.getId());
            if((Boolean) domain.getExtensionparams().get("namedirtyflag"))
                model.setName(domain.getName());
            if((Boolean) domain.getExtensionparams().get("sequencedirtyflag"))
                model.setSequence(domain.getSequence());
            if((Boolean) domain.getExtensionparams().get("display_namedirtyflag"))
                model.setDisplay_name(domain.getDisplayName());
            if((Boolean) domain.getExtensionparams().get("create_uid_textdirtyflag"))
                model.setCreate_uid_text(domain.getCreateUidText());
            if((Boolean) domain.getExtensionparams().get("write_uid_textdirtyflag"))
                model.setWrite_uid_text(domain.getWriteUidText());
            if((Boolean) domain.getExtensionparams().get("write_uiddirtyflag"))
                model.setWrite_uid(domain.getWriteUid());
            if((Boolean) domain.getExtensionparams().get("create_uiddirtyflag"))
                model.setCreate_uid(domain.getCreateUid());
            domain.getFocusNull().clear();
        }
        return model ;
    }

    public Fleet_vehicle_state convert2Domain( fleet_vehicle_stateClientModel model ,Fleet_vehicle_state domain) {
        if(model == null)
            return domain ;
        if(domain == null){
            domain = new Fleet_vehicle_state();
        }

        if(model.getCreate_dateDirtyFlag())
            domain.setCreateDate(model.getCreate_date());
        if(model.getWrite_dateDirtyFlag())
            domain.setWriteDate(model.getWrite_date());
        if(model.get__last_updateDirtyFlag())
            domain.setLastUpdate(model.get__last_update());
        if(model.getIdDirtyFlag())
            domain.setId(model.getId());
        if(model.getNameDirtyFlag())
            domain.setName(model.getName());
        if(model.getSequenceDirtyFlag())
            domain.setSequence(model.getSequence());
        if(model.getDisplay_nameDirtyFlag())
            domain.setDisplayName(model.getDisplay_name());
        if(model.getCreate_uid_textDirtyFlag())
            domain.setCreateUidText(model.getCreate_uid_text());
        if(model.getWrite_uid_textDirtyFlag())
            domain.setWriteUidText(model.getWrite_uid_text());
        if(model.getWrite_uidDirtyFlag())
            domain.setWriteUid(model.getWrite_uid());
        if(model.getCreate_uidDirtyFlag())
            domain.setCreateUid(model.getCreate_uid());
        return domain ;
    }

}

    



