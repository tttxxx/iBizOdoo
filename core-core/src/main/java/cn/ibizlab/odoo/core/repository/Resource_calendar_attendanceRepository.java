package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Resource_calendar_attendance;
import cn.ibizlab.odoo.core.odoo_resource.filter.Resource_calendar_attendanceSearchContext;

/**
 * 实体 [工作细节] 存储对象
 */
public interface Resource_calendar_attendanceRepository extends Repository<Resource_calendar_attendance> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Resource_calendar_attendance> searchDefault(Resource_calendar_attendanceSearchContext context);

    Resource_calendar_attendance convert2PO(cn.ibizlab.odoo.core.odoo_resource.domain.Resource_calendar_attendance domain , Resource_calendar_attendance po) ;

    cn.ibizlab.odoo.core.odoo_resource.domain.Resource_calendar_attendance convert2Domain( Resource_calendar_attendance po ,cn.ibizlab.odoo.core.odoo_resource.domain.Resource_calendar_attendance domain) ;

}
