package cn.ibizlab.odoo.core.odoo_iap.valuerule.anno.iap_account;

import cn.ibizlab.odoo.core.odoo_iap.valuerule.validator.iap_account.Iap_accountAccount_tokenDefaultValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

/**
 * 值规则注解
 * 实体：Iap_account
 * 属性：Account_token
 * 值规则：Default
 */
@Target({ElementType.METHOD, ElementType.FIELD, ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Constraint(validatedBy = {Iap_accountAccount_tokenDefaultValidator.class})
public @interface Iap_accountAccount_tokenDefault {
    //是否默认检查
    boolean flag() default true;

    //值规则名称
    String info() default "默认规则";

    //值规则信息，如果为空，使用值规则名称。
    String message() default "内容长度必须小于等于[100]";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
