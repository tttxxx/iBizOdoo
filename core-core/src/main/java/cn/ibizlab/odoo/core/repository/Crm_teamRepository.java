package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Crm_team;
import cn.ibizlab.odoo.core.odoo_crm.filter.Crm_teamSearchContext;

/**
 * 实体 [销售渠道] 存储对象
 */
public interface Crm_teamRepository extends Repository<Crm_team> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Crm_team> searchDefault(Crm_teamSearchContext context);

    Crm_team convert2PO(cn.ibizlab.odoo.core.odoo_crm.domain.Crm_team domain , Crm_team po) ;

    cn.ibizlab.odoo.core.odoo_crm.domain.Crm_team convert2Domain( Crm_team po ,cn.ibizlab.odoo.core.odoo_crm.domain.Crm_team domain) ;

}
