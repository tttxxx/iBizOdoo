package cn.ibizlab.odoo.core.odoo_account.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_account.domain.Account_fiscal_position_tax;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_fiscal_position_taxSearchContext;
import cn.ibizlab.odoo.core.odoo_account.service.IAccount_fiscal_position_taxService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;



import cn.ibizlab.odoo.core.odoo_account.client.account_fiscal_position_taxOdooClient;
import cn.ibizlab.odoo.core.odoo_account.clientmodel.account_fiscal_position_taxClientModel;

/**
 * 实体[税率的科目调整] 服务对象接口实现
 */
@Slf4j
@Service
public class Account_fiscal_position_taxServiceImpl implements IAccount_fiscal_position_taxService {

    @Autowired
    account_fiscal_position_taxOdooClient account_fiscal_position_taxOdooClient;


    @Override
    public boolean create(Account_fiscal_position_tax et) {
        account_fiscal_position_taxClientModel clientModel = convert2Model(et,null);
		account_fiscal_position_taxOdooClient.create(clientModel);
        Account_fiscal_position_tax rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Account_fiscal_position_tax> list){
    }

    @Override
    public boolean update(Account_fiscal_position_tax et) {
        account_fiscal_position_taxClientModel clientModel = convert2Model(et,null);
		account_fiscal_position_taxOdooClient.update(clientModel);
        Account_fiscal_position_tax rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }
    
    public void updateBatch(List<Account_fiscal_position_tax> list){
    }

    @Override
    public boolean remove(Integer id) {
        account_fiscal_position_taxClientModel clientModel = new account_fiscal_position_taxClientModel();
        clientModel.setId(id);
		account_fiscal_position_taxOdooClient.remove(clientModel);
        return true; 
    }

    public void removeBatch(Collection<Integer> idList){
    }

    @Override
    public Account_fiscal_position_tax get(Integer id) {
        account_fiscal_position_taxClientModel clientModel = new account_fiscal_position_taxClientModel();
        clientModel.setId(id);
		account_fiscal_position_taxOdooClient.get(clientModel);
        Account_fiscal_position_tax et = convert2Domain(clientModel,null);
        if(et==null){
            et=new Account_fiscal_position_tax();
            et.setId(id);
        }
        else{
        }

        return  et;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Account_fiscal_position_tax> searchDefault(Account_fiscal_position_taxSearchContext context) {
        List<Account_fiscal_position_tax> list = new ArrayList<Account_fiscal_position_tax>();
        Page<account_fiscal_position_taxClientModel> clientModelList = account_fiscal_position_taxOdooClient.search(context);
        for(account_fiscal_position_taxClientModel clientModel : clientModelList){
            list.add( this.convert2Domain(clientModel,null));
        }
        return new PageImpl<Account_fiscal_position_tax>(list, context.getPageable(), clientModelList.getTotalElements());
    }

    public account_fiscal_position_taxClientModel convert2Model(Account_fiscal_position_tax domain , account_fiscal_position_taxClientModel model) {
        if(domain == null)
            return model ;
        if(model==null){
            model = new account_fiscal_position_taxClientModel();

        }
        if((Boolean) domain.getExtensionparams().get("dirtyflagenable")){
            if((Boolean) domain.getExtensionparams().get("create_datedirtyflag"))
                model.setCreate_date(domain.getCreateDate());
            if((Boolean) domain.getExtensionparams().get("display_namedirtyflag"))
                model.setDisplay_name(domain.getDisplayName());
            if((Boolean) domain.getExtensionparams().get("__last_updatedirtyflag"))
                model.set__last_update(domain.getLastUpdate());
            if((Boolean) domain.getExtensionparams().get("write_datedirtyflag"))
                model.setWrite_date(domain.getWriteDate());
            if((Boolean) domain.getExtensionparams().get("iddirtyflag"))
                model.setId(domain.getId());
            if((Boolean) domain.getExtensionparams().get("create_uid_textdirtyflag"))
                model.setCreate_uid_text(domain.getCreateUidText());
            if((Boolean) domain.getExtensionparams().get("tax_src_id_textdirtyflag"))
                model.setTax_src_id_text(domain.getTaxSrcIdText());
            if((Boolean) domain.getExtensionparams().get("position_id_textdirtyflag"))
                model.setPosition_id_text(domain.getPositionIdText());
            if((Boolean) domain.getExtensionparams().get("tax_dest_id_textdirtyflag"))
                model.setTax_dest_id_text(domain.getTaxDestIdText());
            if((Boolean) domain.getExtensionparams().get("write_uid_textdirtyflag"))
                model.setWrite_uid_text(domain.getWriteUidText());
            if((Boolean) domain.getExtensionparams().get("write_uiddirtyflag"))
                model.setWrite_uid(domain.getWriteUid());
            if((Boolean) domain.getExtensionparams().get("position_iddirtyflag"))
                model.setPosition_id(domain.getPositionId());
            if((Boolean) domain.getExtensionparams().get("tax_dest_iddirtyflag"))
                model.setTax_dest_id(domain.getTaxDestId());
            if((Boolean) domain.getExtensionparams().get("tax_src_iddirtyflag"))
                model.setTax_src_id(domain.getTaxSrcId());
            if((Boolean) domain.getExtensionparams().get("create_uiddirtyflag"))
                model.setCreate_uid(domain.getCreateUid());
            domain.getFocusNull().clear();
        }
        return model ;
    }

    public Account_fiscal_position_tax convert2Domain( account_fiscal_position_taxClientModel model ,Account_fiscal_position_tax domain) {
        if(model == null)
            return domain ;
        if(domain == null){
            domain = new Account_fiscal_position_tax();
        }

        if(model.getCreate_dateDirtyFlag())
            domain.setCreateDate(model.getCreate_date());
        if(model.getDisplay_nameDirtyFlag())
            domain.setDisplayName(model.getDisplay_name());
        if(model.get__last_updateDirtyFlag())
            domain.setLastUpdate(model.get__last_update());
        if(model.getWrite_dateDirtyFlag())
            domain.setWriteDate(model.getWrite_date());
        if(model.getIdDirtyFlag())
            domain.setId(model.getId());
        if(model.getCreate_uid_textDirtyFlag())
            domain.setCreateUidText(model.getCreate_uid_text());
        if(model.getTax_src_id_textDirtyFlag())
            domain.setTaxSrcIdText(model.getTax_src_id_text());
        if(model.getPosition_id_textDirtyFlag())
            domain.setPositionIdText(model.getPosition_id_text());
        if(model.getTax_dest_id_textDirtyFlag())
            domain.setTaxDestIdText(model.getTax_dest_id_text());
        if(model.getWrite_uid_textDirtyFlag())
            domain.setWriteUidText(model.getWrite_uid_text());
        if(model.getWrite_uidDirtyFlag())
            domain.setWriteUid(model.getWrite_uid());
        if(model.getPosition_idDirtyFlag())
            domain.setPositionId(model.getPosition_id());
        if(model.getTax_dest_idDirtyFlag())
            domain.setTaxDestId(model.getTax_dest_id());
        if(model.getTax_src_idDirtyFlag())
            domain.setTaxSrcId(model.getTax_src_id());
        if(model.getCreate_uidDirtyFlag())
            domain.setCreateUid(model.getCreate_uid());
        return domain ;
    }

}

    



