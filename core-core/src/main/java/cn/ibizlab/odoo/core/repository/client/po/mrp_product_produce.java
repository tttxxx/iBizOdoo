package cn.ibizlab.odoo.core.repository.client.po;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigDecimal;

/**
 * [mrp_product_produce] 对象
 */
public interface mrp_product_produce {

    public Timestamp getCreate_date();

    public void setCreate_date(Timestamp create_date);

    public Integer getCreate_uid();

    public void setCreate_uid(Integer create_uid);

    public String getCreate_uid_text();

    public void setCreate_uid_text(String create_uid_text);

    public String getDisplay_name();

    public void setDisplay_name(String display_name);

    public Integer getId();

    public void setId(Integer id);

    public Integer getLot_id();

    public void setLot_id(Integer lot_id);

    public String getLot_id_text();

    public void setLot_id_text(String lot_id_text);

    public String getProduce_line_ids();

    public void setProduce_line_ids(String produce_line_ids);

    public Integer getProduction_id();

    public void setProduction_id(Integer production_id);

    public String getProduction_id_text();

    public void setProduction_id_text(String production_id_text);

    public Integer getProduct_id();

    public void setProduct_id(Integer product_id);

    public String getProduct_id_text();

    public void setProduct_id_text(String product_id_text);

    public Double getProduct_qty();

    public void setProduct_qty(Double product_qty);

    public String getProduct_tracking();

    public void setProduct_tracking(String product_tracking);

    public Integer getProduct_uom_id();

    public void setProduct_uom_id(Integer product_uom_id);

    public String getProduct_uom_id_text();

    public void setProduct_uom_id_text(String product_uom_id_text);

    public String getSerial();

    public void setSerial(String serial);

    public Timestamp getWrite_date();

    public void setWrite_date(Timestamp write_date);

    public Integer getWrite_uid();

    public void setWrite_uid(Integer write_uid);

    public String getWrite_uid_text();

    public void setWrite_uid_text(String write_uid_text);

    public Timestamp get__last_update();

    public void set__last_update(Timestamp __last_update);

}
