package cn.ibizlab.odoo.core.odoo_stock.valuerule.anno.stock_rules_report;

import cn.ibizlab.odoo.core.odoo_stock.valuerule.validator.stock_rules_report.Stock_rules_reportProduct_has_variantsDefaultValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

/**
 * 值规则注解
 * 实体：Stock_rules_report
 * 属性：Product_has_variants
 * 值规则：Default
 */
@Target({ElementType.METHOD, ElementType.FIELD, ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Constraint(validatedBy = {Stock_rules_reportProduct_has_variantsDefaultValidator.class})
public @interface Stock_rules_reportProduct_has_variantsDefault {
    //是否默认检查
    boolean flag() default true;

    //值规则名称
    String info() default "默认规则";

    //值规则信息，如果为空，使用值规则名称。
    String message() default "内容长度必须小于等于[200]";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
