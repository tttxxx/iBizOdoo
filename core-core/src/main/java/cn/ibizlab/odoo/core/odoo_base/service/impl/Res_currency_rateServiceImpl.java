package cn.ibizlab.odoo.core.odoo_base.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_base.domain.Res_currency_rate;
import cn.ibizlab.odoo.core.odoo_base.filter.Res_currency_rateSearchContext;
import cn.ibizlab.odoo.core.odoo_base.service.IRes_currency_rateService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;



import cn.ibizlab.odoo.core.odoo_base.client.res_currency_rateOdooClient;
import cn.ibizlab.odoo.core.odoo_base.clientmodel.res_currency_rateClientModel;

/**
 * 实体[汇率] 服务对象接口实现
 */
@Slf4j
@Service
public class Res_currency_rateServiceImpl implements IRes_currency_rateService {

    @Autowired
    res_currency_rateOdooClient res_currency_rateOdooClient;


    @Override
    public boolean create(Res_currency_rate et) {
        res_currency_rateClientModel clientModel = convert2Model(et,null);
		res_currency_rateOdooClient.create(clientModel);
        Res_currency_rate rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Res_currency_rate> list){
    }

    @Override
    public Res_currency_rate get(Integer id) {
        res_currency_rateClientModel clientModel = new res_currency_rateClientModel();
        clientModel.setId(id);
		res_currency_rateOdooClient.get(clientModel);
        Res_currency_rate et = convert2Domain(clientModel,null);
        if(et==null){
            et=new Res_currency_rate();
            et.setId(id);
        }
        else{
        }

        return  et;
    }

    @Override
    public boolean remove(Integer id) {
        res_currency_rateClientModel clientModel = new res_currency_rateClientModel();
        clientModel.setId(id);
		res_currency_rateOdooClient.remove(clientModel);
        return true; 
    }

    public void removeBatch(Collection<Integer> idList){
    }

    @Override
    public boolean update(Res_currency_rate et) {
        res_currency_rateClientModel clientModel = convert2Model(et,null);
		res_currency_rateOdooClient.update(clientModel);
        Res_currency_rate rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }
    
    public void updateBatch(List<Res_currency_rate> list){
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Res_currency_rate> searchDefault(Res_currency_rateSearchContext context) {
        List<Res_currency_rate> list = new ArrayList<Res_currency_rate>();
        Page<res_currency_rateClientModel> clientModelList = res_currency_rateOdooClient.search(context);
        for(res_currency_rateClientModel clientModel : clientModelList){
            list.add( this.convert2Domain(clientModel,null));
        }
        return new PageImpl<Res_currency_rate>(list, context.getPageable(), clientModelList.getTotalElements());
    }

    public res_currency_rateClientModel convert2Model(Res_currency_rate domain , res_currency_rateClientModel model) {
        if(domain == null)
            return model ;
        if(model==null){
            model = new res_currency_rateClientModel();

        }
        if((Boolean) domain.getExtensionparams().get("dirtyflagenable")){
            if((Boolean) domain.getExtensionparams().get("create_datedirtyflag"))
                model.setCreate_date(domain.getCreateDate());
            if((Boolean) domain.getExtensionparams().get("iddirtyflag"))
                model.setId(domain.getId());
            if((Boolean) domain.getExtensionparams().get("write_datedirtyflag"))
                model.setWrite_date(domain.getWriteDate());
            if((Boolean) domain.getExtensionparams().get("display_namedirtyflag"))
                model.setDisplay_name(domain.getDisplayName());
            if((Boolean) domain.getExtensionparams().get("__last_updatedirtyflag"))
                model.set__last_update(domain.getLastUpdate());
            if((Boolean) domain.getExtensionparams().get("ratedirtyflag"))
                model.setRate(domain.getRate());
            if((Boolean) domain.getExtensionparams().get("namedirtyflag"))
                model.setName(domain.getName());
            if((Boolean) domain.getExtensionparams().get("company_id_textdirtyflag"))
                model.setCompany_id_text(domain.getCompanyIdText());
            if((Boolean) domain.getExtensionparams().get("currency_id_textdirtyflag"))
                model.setCurrency_id_text(domain.getCurrencyIdText());
            if((Boolean) domain.getExtensionparams().get("create_uid_textdirtyflag"))
                model.setCreate_uid_text(domain.getCreateUidText());
            if((Boolean) domain.getExtensionparams().get("write_uid_textdirtyflag"))
                model.setWrite_uid_text(domain.getWriteUidText());
            if((Boolean) domain.getExtensionparams().get("write_uiddirtyflag"))
                model.setWrite_uid(domain.getWriteUid());
            if((Boolean) domain.getExtensionparams().get("currency_iddirtyflag"))
                model.setCurrency_id(domain.getCurrencyId());
            if((Boolean) domain.getExtensionparams().get("company_iddirtyflag"))
                model.setCompany_id(domain.getCompanyId());
            if((Boolean) domain.getExtensionparams().get("create_uiddirtyflag"))
                model.setCreate_uid(domain.getCreateUid());
            domain.getFocusNull().clear();
        }
        return model ;
    }

    public Res_currency_rate convert2Domain( res_currency_rateClientModel model ,Res_currency_rate domain) {
        if(model == null)
            return domain ;
        if(domain == null){
            domain = new Res_currency_rate();
        }

        if(model.getCreate_dateDirtyFlag())
            domain.setCreateDate(model.getCreate_date());
        if(model.getIdDirtyFlag())
            domain.setId(model.getId());
        if(model.getWrite_dateDirtyFlag())
            domain.setWriteDate(model.getWrite_date());
        if(model.getDisplay_nameDirtyFlag())
            domain.setDisplayName(model.getDisplay_name());
        if(model.get__last_updateDirtyFlag())
            domain.setLastUpdate(model.get__last_update());
        if(model.getRateDirtyFlag())
            domain.setRate(model.getRate());
        if(model.getNameDirtyFlag())
            domain.setName(model.getName());
        if(model.getCompany_id_textDirtyFlag())
            domain.setCompanyIdText(model.getCompany_id_text());
        if(model.getCurrency_id_textDirtyFlag())
            domain.setCurrencyIdText(model.getCurrency_id_text());
        if(model.getCreate_uid_textDirtyFlag())
            domain.setCreateUidText(model.getCreate_uid_text());
        if(model.getWrite_uid_textDirtyFlag())
            domain.setWriteUidText(model.getWrite_uid_text());
        if(model.getWrite_uidDirtyFlag())
            domain.setWriteUid(model.getWrite_uid());
        if(model.getCurrency_idDirtyFlag())
            domain.setCurrencyId(model.getCurrency_id());
        if(model.getCompany_idDirtyFlag())
            domain.setCompanyId(model.getCompany_id());
        if(model.getCreate_uidDirtyFlag())
            domain.setCreateUid(model.getCreate_uid());
        return domain ;
    }

}

    



