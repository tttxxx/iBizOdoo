package cn.ibizlab.odoo.core.repository.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import cn.ibizlab.odoo.core.repository.client.po.sale_order_template;

/**
 * 实体[sale_order_template] 服务对象接口
 */
public interface sale_order_templateRepository{


    public sale_order_template createPO() ;
        public List<sale_order_template> search();

        public void create(sale_order_template sale_order_template);

        public void remove(String id);

        public void updateBatch(sale_order_template sale_order_template);

        public void update(sale_order_template sale_order_template);

        public void get(String id);

        public void removeBatch(String id);

        public void createBatch(sale_order_template sale_order_template);


}
