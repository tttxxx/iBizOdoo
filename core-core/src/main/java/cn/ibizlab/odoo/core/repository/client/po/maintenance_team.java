package cn.ibizlab.odoo.core.repository.client.po;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigDecimal;

/**
 * [maintenance_team] 对象
 */
public interface maintenance_team {

    public String getActive();

    public void setActive(String active);

    public Integer getColor();

    public void setColor(Integer color);

    public Integer getCompany_id();

    public void setCompany_id(Integer company_id);

    public String getCompany_id_text();

    public void setCompany_id_text(String company_id_text);

    public Timestamp getCreate_date();

    public void setCreate_date(Timestamp create_date);

    public Integer getCreate_uid();

    public void setCreate_uid(Integer create_uid);

    public String getCreate_uid_text();

    public void setCreate_uid_text(String create_uid_text);

    public String getDisplay_name();

    public void setDisplay_name(String display_name);

    public String getEquipment_ids();

    public void setEquipment_ids(String equipment_ids);

    public Integer getId();

    public void setId(Integer id);

    public String getMember_ids();

    public void setMember_ids(String member_ids);

    public String getName();

    public void setName(String name);

    public String getRequest_ids();

    public void setRequest_ids(String request_ids);

    public Integer getTodo_request_count();

    public void setTodo_request_count(Integer todo_request_count);

    public Integer getTodo_request_count_block();

    public void setTodo_request_count_block(Integer todo_request_count_block);

    public Integer getTodo_request_count_date();

    public void setTodo_request_count_date(Integer todo_request_count_date);

    public Integer getTodo_request_count_high_priority();

    public void setTodo_request_count_high_priority(Integer todo_request_count_high_priority);

    public Integer getTodo_request_count_unscheduled();

    public void setTodo_request_count_unscheduled(Integer todo_request_count_unscheduled);

    public String getTodo_request_ids();

    public void setTodo_request_ids(String todo_request_ids);

    public Timestamp getWrite_date();

    public void setWrite_date(Timestamp write_date);

    public Integer getWrite_uid();

    public void setWrite_uid(Integer write_uid);

    public String getWrite_uid_text();

    public void setWrite_uid_text(String write_uid_text);

    public Timestamp get__last_update();

    public void set__last_update(Timestamp __last_update);

}
