package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Mail_thread;
import cn.ibizlab.odoo.core.odoo_mail.filter.Mail_threadSearchContext;

/**
 * 实体 [EMail线程] 存储对象
 */
public interface Mail_threadRepository extends Repository<Mail_thread> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Mail_thread> searchDefault(Mail_threadSearchContext context);

    Mail_thread convert2PO(cn.ibizlab.odoo.core.odoo_mail.domain.Mail_thread domain , Mail_thread po) ;

    cn.ibizlab.odoo.core.odoo_mail.domain.Mail_thread convert2Domain( Mail_thread po ,cn.ibizlab.odoo.core.odoo_mail.domain.Mail_thread domain) ;

}
