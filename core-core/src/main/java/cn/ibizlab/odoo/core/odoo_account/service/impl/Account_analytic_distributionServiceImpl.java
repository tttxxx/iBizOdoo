package cn.ibizlab.odoo.core.odoo_account.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_account.domain.Account_analytic_distribution;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_analytic_distributionSearchContext;
import cn.ibizlab.odoo.core.odoo_account.service.IAccount_analytic_distributionService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;



import cn.ibizlab.odoo.core.odoo_account.client.account_analytic_distributionOdooClient;
import cn.ibizlab.odoo.core.odoo_account.clientmodel.account_analytic_distributionClientModel;

/**
 * 实体[分析账户分配] 服务对象接口实现
 */
@Slf4j
@Service
public class Account_analytic_distributionServiceImpl implements IAccount_analytic_distributionService {

    @Autowired
    account_analytic_distributionOdooClient account_analytic_distributionOdooClient;


    @Override
    public boolean remove(Integer id) {
        account_analytic_distributionClientModel clientModel = new account_analytic_distributionClientModel();
        clientModel.setId(id);
		account_analytic_distributionOdooClient.remove(clientModel);
        return true; 
    }

    public void removeBatch(Collection<Integer> idList){
    }

    @Override
    public boolean create(Account_analytic_distribution et) {
        account_analytic_distributionClientModel clientModel = convert2Model(et,null);
		account_analytic_distributionOdooClient.create(clientModel);
        Account_analytic_distribution rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Account_analytic_distribution> list){
    }

    @Override
    public boolean update(Account_analytic_distribution et) {
        account_analytic_distributionClientModel clientModel = convert2Model(et,null);
		account_analytic_distributionOdooClient.update(clientModel);
        Account_analytic_distribution rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }
    
    public void updateBatch(List<Account_analytic_distribution> list){
    }

    @Override
    public Account_analytic_distribution get(Integer id) {
        account_analytic_distributionClientModel clientModel = new account_analytic_distributionClientModel();
        clientModel.setId(id);
		account_analytic_distributionOdooClient.get(clientModel);
        Account_analytic_distribution et = convert2Domain(clientModel,null);
        if(et==null){
            et=new Account_analytic_distribution();
            et.setId(id);
        }
        else{
        }

        return  et;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Account_analytic_distribution> searchDefault(Account_analytic_distributionSearchContext context) {
        List<Account_analytic_distribution> list = new ArrayList<Account_analytic_distribution>();
        Page<account_analytic_distributionClientModel> clientModelList = account_analytic_distributionOdooClient.search(context);
        for(account_analytic_distributionClientModel clientModel : clientModelList){
            list.add( this.convert2Domain(clientModel,null));
        }
        return new PageImpl<Account_analytic_distribution>(list, context.getPageable(), clientModelList.getTotalElements());
    }

    public account_analytic_distributionClientModel convert2Model(Account_analytic_distribution domain , account_analytic_distributionClientModel model) {
        if(domain == null)
            return model ;
        if(model==null){
            model = new account_analytic_distributionClientModel();

        }
        if((Boolean) domain.getExtensionparams().get("dirtyflagenable")){
            if((Boolean) domain.getExtensionparams().get("percentagedirtyflag"))
                model.setPercentage(domain.getPercentage());
            if((Boolean) domain.getExtensionparams().get("display_namedirtyflag"))
                model.setDisplay_name(domain.getDisplayName());
            if((Boolean) domain.getExtensionparams().get("write_datedirtyflag"))
                model.setWrite_date(domain.getWriteDate());
            if((Boolean) domain.getExtensionparams().get("__last_updatedirtyflag"))
                model.set__last_update(domain.getLastUpdate());
            if((Boolean) domain.getExtensionparams().get("iddirtyflag"))
                model.setId(domain.getId());
            if((Boolean) domain.getExtensionparams().get("create_datedirtyflag"))
                model.setCreate_date(domain.getCreateDate());
            if((Boolean) domain.getExtensionparams().get("create_uid_textdirtyflag"))
                model.setCreate_uid_text(domain.getCreateUidText());
            if((Boolean) domain.getExtensionparams().get("namedirtyflag"))
                model.setName(domain.getName());
            if((Boolean) domain.getExtensionparams().get("tag_id_textdirtyflag"))
                model.setTag_id_text(domain.getTagIdText());
            if((Boolean) domain.getExtensionparams().get("write_uid_textdirtyflag"))
                model.setWrite_uid_text(domain.getWriteUidText());
            if((Boolean) domain.getExtensionparams().get("create_uiddirtyflag"))
                model.setCreate_uid(domain.getCreateUid());
            if((Boolean) domain.getExtensionparams().get("write_uiddirtyflag"))
                model.setWrite_uid(domain.getWriteUid());
            if((Boolean) domain.getExtensionparams().get("tag_iddirtyflag"))
                model.setTag_id(domain.getTagId());
            if((Boolean) domain.getExtensionparams().get("account_iddirtyflag"))
                model.setAccount_id(domain.getAccountId());
            domain.getFocusNull().clear();
        }
        return model ;
    }

    public Account_analytic_distribution convert2Domain( account_analytic_distributionClientModel model ,Account_analytic_distribution domain) {
        if(model == null)
            return domain ;
        if(domain == null){
            domain = new Account_analytic_distribution();
        }

        if(model.getPercentageDirtyFlag())
            domain.setPercentage(model.getPercentage());
        if(model.getDisplay_nameDirtyFlag())
            domain.setDisplayName(model.getDisplay_name());
        if(model.getWrite_dateDirtyFlag())
            domain.setWriteDate(model.getWrite_date());
        if(model.get__last_updateDirtyFlag())
            domain.setLastUpdate(model.get__last_update());
        if(model.getIdDirtyFlag())
            domain.setId(model.getId());
        if(model.getCreate_dateDirtyFlag())
            domain.setCreateDate(model.getCreate_date());
        if(model.getCreate_uid_textDirtyFlag())
            domain.setCreateUidText(model.getCreate_uid_text());
        if(model.getNameDirtyFlag())
            domain.setName(model.getName());
        if(model.getTag_id_textDirtyFlag())
            domain.setTagIdText(model.getTag_id_text());
        if(model.getWrite_uid_textDirtyFlag())
            domain.setWriteUidText(model.getWrite_uid_text());
        if(model.getCreate_uidDirtyFlag())
            domain.setCreateUid(model.getCreate_uid());
        if(model.getWrite_uidDirtyFlag())
            domain.setWriteUid(model.getWrite_uid());
        if(model.getTag_idDirtyFlag())
            domain.setTagId(model.getTag_id());
        if(model.getAccount_idDirtyFlag())
            domain.setAccountId(model.getAccount_id());
        return domain ;
    }

}

    



