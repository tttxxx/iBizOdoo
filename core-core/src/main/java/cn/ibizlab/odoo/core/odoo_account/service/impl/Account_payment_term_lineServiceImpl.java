package cn.ibizlab.odoo.core.odoo_account.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_account.domain.Account_payment_term_line;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_payment_term_lineSearchContext;
import cn.ibizlab.odoo.core.odoo_account.service.IAccount_payment_term_lineService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;



import cn.ibizlab.odoo.core.odoo_account.client.account_payment_term_lineOdooClient;
import cn.ibizlab.odoo.core.odoo_account.clientmodel.account_payment_term_lineClientModel;

/**
 * 实体[付款条款行] 服务对象接口实现
 */
@Slf4j
@Service
public class Account_payment_term_lineServiceImpl implements IAccount_payment_term_lineService {

    @Autowired
    account_payment_term_lineOdooClient account_payment_term_lineOdooClient;


    @Override
    public boolean update(Account_payment_term_line et) {
        account_payment_term_lineClientModel clientModel = convert2Model(et,null);
		account_payment_term_lineOdooClient.update(clientModel);
        Account_payment_term_line rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }
    
    public void updateBatch(List<Account_payment_term_line> list){
    }

    @Override
    public boolean create(Account_payment_term_line et) {
        account_payment_term_lineClientModel clientModel = convert2Model(et,null);
		account_payment_term_lineOdooClient.create(clientModel);
        Account_payment_term_line rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Account_payment_term_line> list){
    }

    @Override
    public boolean remove(Integer id) {
        account_payment_term_lineClientModel clientModel = new account_payment_term_lineClientModel();
        clientModel.setId(id);
		account_payment_term_lineOdooClient.remove(clientModel);
        return true; 
    }

    public void removeBatch(Collection<Integer> idList){
    }

    @Override
    public Account_payment_term_line get(Integer id) {
        account_payment_term_lineClientModel clientModel = new account_payment_term_lineClientModel();
        clientModel.setId(id);
		account_payment_term_lineOdooClient.get(clientModel);
        Account_payment_term_line et = convert2Domain(clientModel,null);
        if(et==null){
            et=new Account_payment_term_line();
            et.setId(id);
        }
        else{
        }

        return  et;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Account_payment_term_line> searchDefault(Account_payment_term_lineSearchContext context) {
        List<Account_payment_term_line> list = new ArrayList<Account_payment_term_line>();
        Page<account_payment_term_lineClientModel> clientModelList = account_payment_term_lineOdooClient.search(context);
        for(account_payment_term_lineClientModel clientModel : clientModelList){
            list.add( this.convert2Domain(clientModel,null));
        }
        return new PageImpl<Account_payment_term_line>(list, context.getPageable(), clientModelList.getTotalElements());
    }

    public account_payment_term_lineClientModel convert2Model(Account_payment_term_line domain , account_payment_term_lineClientModel model) {
        if(domain == null)
            return model ;
        if(model==null){
            model = new account_payment_term_lineClientModel();

        }
        if((Boolean) domain.getExtensionparams().get("dirtyflagenable")){
            if((Boolean) domain.getExtensionparams().get("daysdirtyflag"))
                model.setDays(domain.getDays());
            if((Boolean) domain.getExtensionparams().get("sequencedirtyflag"))
                model.setSequence(domain.getSequence());
            if((Boolean) domain.getExtensionparams().get("write_datedirtyflag"))
                model.setWrite_date(domain.getWriteDate());
            if((Boolean) domain.getExtensionparams().get("__last_updatedirtyflag"))
                model.set__last_update(domain.getLastUpdate());
            if((Boolean) domain.getExtensionparams().get("valuedirtyflag"))
                model.setValue(domain.getValue());
            if((Boolean) domain.getExtensionparams().get("value_amountdirtyflag"))
                model.setValue_amount(domain.getValueAmount());
            if((Boolean) domain.getExtensionparams().get("day_of_the_monthdirtyflag"))
                model.setDay_of_the_month(domain.getDayOfTheMonth());
            if((Boolean) domain.getExtensionparams().get("display_namedirtyflag"))
                model.setDisplay_name(domain.getDisplayName());
            if((Boolean) domain.getExtensionparams().get("optiondirtyflag"))
                model.setOption(domain.getOption());
            if((Boolean) domain.getExtensionparams().get("create_datedirtyflag"))
                model.setCreate_date(domain.getCreateDate());
            if((Boolean) domain.getExtensionparams().get("iddirtyflag"))
                model.setId(domain.getId());
            if((Boolean) domain.getExtensionparams().get("payment_id_textdirtyflag"))
                model.setPayment_id_text(domain.getPaymentIdText());
            if((Boolean) domain.getExtensionparams().get("create_uid_textdirtyflag"))
                model.setCreate_uid_text(domain.getCreateUidText());
            if((Boolean) domain.getExtensionparams().get("write_uid_textdirtyflag"))
                model.setWrite_uid_text(domain.getWriteUidText());
            if((Boolean) domain.getExtensionparams().get("create_uiddirtyflag"))
                model.setCreate_uid(domain.getCreateUid());
            if((Boolean) domain.getExtensionparams().get("payment_iddirtyflag"))
                model.setPayment_id(domain.getPaymentId());
            if((Boolean) domain.getExtensionparams().get("write_uiddirtyflag"))
                model.setWrite_uid(domain.getWriteUid());
            domain.getFocusNull().clear();
        }
        return model ;
    }

    public Account_payment_term_line convert2Domain( account_payment_term_lineClientModel model ,Account_payment_term_line domain) {
        if(model == null)
            return domain ;
        if(domain == null){
            domain = new Account_payment_term_line();
        }

        if(model.getDaysDirtyFlag())
            domain.setDays(model.getDays());
        if(model.getSequenceDirtyFlag())
            domain.setSequence(model.getSequence());
        if(model.getWrite_dateDirtyFlag())
            domain.setWriteDate(model.getWrite_date());
        if(model.get__last_updateDirtyFlag())
            domain.setLastUpdate(model.get__last_update());
        if(model.getValueDirtyFlag())
            domain.setValue(model.getValue());
        if(model.getValue_amountDirtyFlag())
            domain.setValueAmount(model.getValue_amount());
        if(model.getDay_of_the_monthDirtyFlag())
            domain.setDayOfTheMonth(model.getDay_of_the_month());
        if(model.getDisplay_nameDirtyFlag())
            domain.setDisplayName(model.getDisplay_name());
        if(model.getOptionDirtyFlag())
            domain.setOption(model.getOption());
        if(model.getCreate_dateDirtyFlag())
            domain.setCreateDate(model.getCreate_date());
        if(model.getIdDirtyFlag())
            domain.setId(model.getId());
        if(model.getPayment_id_textDirtyFlag())
            domain.setPaymentIdText(model.getPayment_id_text());
        if(model.getCreate_uid_textDirtyFlag())
            domain.setCreateUidText(model.getCreate_uid_text());
        if(model.getWrite_uid_textDirtyFlag())
            domain.setWriteUidText(model.getWrite_uid_text());
        if(model.getCreate_uidDirtyFlag())
            domain.setCreateUid(model.getCreate_uid());
        if(model.getPayment_idDirtyFlag())
            domain.setPaymentId(model.getPayment_id());
        if(model.getWrite_uidDirtyFlag())
            domain.setWriteUid(model.getWrite_uid());
        return domain ;
    }

}

    



