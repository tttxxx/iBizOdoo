package cn.ibizlab.odoo.core.repository.po ;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.math.BigInteger;

import cn.ibizlab.odoo.core.odoo_mail.filter.Mail_statistics_reportSearchContext;

/**
 * 实体 [群发邮件阶段] 存储模型
 */
public interface Mail_statistics_report{

    /**
     * 点击率
     */
    Integer getClicked();

    void setClicked(Integer clicked);

    /**
     * 获取 [点击率]脏标记
     */
    boolean getClickedDirtyFlag();

    /**
     * 最后修改日
     */
    Timestamp get__last_update();

    void set__last_update(Timestamp __last_update);

    /**
     * 获取 [最后修改日]脏标记
     */
    boolean get__last_updateDirtyFlag();

    /**
     * ID
     */
    Integer getId();

    void setId(Integer id);

    /**
     * 获取 [ID]脏标记
     */
    boolean getIdDirtyFlag();

    /**
     * 显示名称
     */
    String getDisplay_name();

    void setDisplay_name(String display_name);

    /**
     * 获取 [显示名称]脏标记
     */
    boolean getDisplay_nameDirtyFlag();

    /**
     * 被退回
     */
    Integer getBounced();

    void setBounced(Integer bounced);

    /**
     * 获取 [被退回]脏标记
     */
    boolean getBouncedDirtyFlag();

    /**
     * 状态
     */
    String getState();

    void setState(String state);

    /**
     * 获取 [状态]脏标记
     */
    boolean getStateDirtyFlag();

    /**
     * 已回复
     */
    Integer getReplied();

    void setReplied(Integer replied);

    /**
     * 获取 [已回复]脏标记
     */
    boolean getRepliedDirtyFlag();

    /**
     * 群发邮件营销
     */
    String getCampaign();

    void setCampaign(String campaign);

    /**
     * 获取 [群发邮件营销]脏标记
     */
    boolean getCampaignDirtyFlag();

    /**
     * 计划日期
     */
    Timestamp getScheduled_date();

    void setScheduled_date(Timestamp scheduled_date);

    /**
     * 获取 [计划日期]脏标记
     */
    boolean getScheduled_dateDirtyFlag();

    /**
     * 已汇
     */
    Integer getSent();

    void setSent(Integer sent);

    /**
     * 获取 [已汇]脏标记
     */
    boolean getSentDirtyFlag();

    /**
     * 从
     */
    String getEmail_from();

    void setEmail_from(String email_from);

    /**
     * 获取 [从]脏标记
     */
    boolean getEmail_fromDirtyFlag();

    /**
     * 已送货
     */
    Integer getDelivered();

    void setDelivered(Integer delivered);

    /**
     * 获取 [已送货]脏标记
     */
    boolean getDeliveredDirtyFlag();

    /**
     * 群发邮件
     */
    String getName();

    void setName(String name);

    /**
     * 获取 [群发邮件]脏标记
     */
    boolean getNameDirtyFlag();

    /**
     * 已开启
     */
    Integer getOpened();

    void setOpened(Integer opened);

    /**
     * 获取 [已开启]脏标记
     */
    boolean getOpenedDirtyFlag();

}
