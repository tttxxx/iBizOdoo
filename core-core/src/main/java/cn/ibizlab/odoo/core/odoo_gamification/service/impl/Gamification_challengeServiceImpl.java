package cn.ibizlab.odoo.core.odoo_gamification.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_gamification.domain.Gamification_challenge;
import cn.ibizlab.odoo.core.odoo_gamification.filter.Gamification_challengeSearchContext;
import cn.ibizlab.odoo.core.odoo_gamification.service.IGamification_challengeService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;



import cn.ibizlab.odoo.core.odoo_gamification.client.gamification_challengeOdooClient;
import cn.ibizlab.odoo.core.odoo_gamification.clientmodel.gamification_challengeClientModel;

/**
 * 实体[游戏化挑战] 服务对象接口实现
 */
@Slf4j
@Service
public class Gamification_challengeServiceImpl implements IGamification_challengeService {

    @Autowired
    gamification_challengeOdooClient gamification_challengeOdooClient;


    @Override
    public boolean remove(Integer id) {
        gamification_challengeClientModel clientModel = new gamification_challengeClientModel();
        clientModel.setId(id);
		gamification_challengeOdooClient.remove(clientModel);
        return true; 
    }

    public void removeBatch(Collection<Integer> idList){
    }

    @Override
    public Gamification_challenge get(Integer id) {
        gamification_challengeClientModel clientModel = new gamification_challengeClientModel();
        clientModel.setId(id);
		gamification_challengeOdooClient.get(clientModel);
        Gamification_challenge et = convert2Domain(clientModel,null);
        if(et==null){
            et=new Gamification_challenge();
            et.setId(id);
        }
        else{
        }

        return  et;
    }

    @Override
    public boolean update(Gamification_challenge et) {
        gamification_challengeClientModel clientModel = convert2Model(et,null);
		gamification_challengeOdooClient.update(clientModel);
        Gamification_challenge rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }
    
    public void updateBatch(List<Gamification_challenge> list){
    }

    @Override
    public boolean create(Gamification_challenge et) {
        gamification_challengeClientModel clientModel = convert2Model(et,null);
		gamification_challengeOdooClient.create(clientModel);
        Gamification_challenge rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Gamification_challenge> list){
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Gamification_challenge> searchDefault(Gamification_challengeSearchContext context) {
        List<Gamification_challenge> list = new ArrayList<Gamification_challenge>();
        Page<gamification_challengeClientModel> clientModelList = gamification_challengeOdooClient.search(context);
        for(gamification_challengeClientModel clientModel : clientModelList){
            list.add( this.convert2Domain(clientModel,null));
        }
        return new PageImpl<Gamification_challenge>(list, context.getPageable(), clientModelList.getTotalElements());
    }

    public gamification_challengeClientModel convert2Model(Gamification_challenge domain , gamification_challengeClientModel model) {
        if(domain == null)
            return model ;
        if(model==null){
            model = new gamification_challengeClientModel();

        }
        if((Boolean) domain.getExtensionparams().get("dirtyflagenable")){
            if((Boolean) domain.getExtensionparams().get("website_message_idsdirtyflag"))
                model.setWebsite_message_ids(domain.getWebsiteMessageIds());
            if((Boolean) domain.getExtensionparams().get("namedirtyflag"))
                model.setName(domain.getName());
            if((Boolean) domain.getExtensionparams().get("message_needactiondirtyflag"))
                model.setMessage_needaction(domain.getMessageNeedaction());
            if((Boolean) domain.getExtensionparams().get("iddirtyflag"))
                model.setId(domain.getId());
            if((Boolean) domain.getExtensionparams().get("create_datedirtyflag"))
                model.setCreate_date(domain.getCreateDate());
            if((Boolean) domain.getExtensionparams().get("message_unreaddirtyflag"))
                model.setMessage_unread(domain.getMessageUnread());
            if((Boolean) domain.getExtensionparams().get("start_datedirtyflag"))
                model.setStart_date(domain.getStartDate());
            if((Boolean) domain.getExtensionparams().get("message_has_error_counterdirtyflag"))
                model.setMessage_has_error_counter(domain.getMessageHasErrorCounter());
            if((Boolean) domain.getExtensionparams().get("message_main_attachment_iddirtyflag"))
                model.setMessage_main_attachment_id(domain.getMessageMainAttachmentId());
            if((Boolean) domain.getExtensionparams().get("message_has_errordirtyflag"))
                model.setMessage_has_error(domain.getMessageHasError());
            if((Boolean) domain.getExtensionparams().get("last_report_datedirtyflag"))
                model.setLast_report_date(domain.getLastReportDate());
            if((Boolean) domain.getExtensionparams().get("end_datedirtyflag"))
                model.setEnd_date(domain.getEndDate());
            if((Boolean) domain.getExtensionparams().get("message_follower_idsdirtyflag"))
                model.setMessage_follower_ids(domain.getMessageFollowerIds());
            if((Boolean) domain.getExtensionparams().get("message_attachment_countdirtyflag"))
                model.setMessage_attachment_count(domain.getMessageAttachmentCount());
            if((Boolean) domain.getExtensionparams().get("message_is_followerdirtyflag"))
                model.setMessage_is_follower(domain.getMessageIsFollower());
            if((Boolean) domain.getExtensionparams().get("user_domaindirtyflag"))
                model.setUser_domain(domain.getUserDomain());
            if((Boolean) domain.getExtensionparams().get("categorydirtyflag"))
                model.setCategory(domain.getCategory());
            if((Boolean) domain.getExtensionparams().get("descriptiondirtyflag"))
                model.setDescription(domain.getDescription());
            if((Boolean) domain.getExtensionparams().get("user_idsdirtyflag"))
                model.setUser_ids(domain.getUserIds());
            if((Boolean) domain.getExtensionparams().get("message_unread_counterdirtyflag"))
                model.setMessage_unread_counter(domain.getMessageUnreadCounter());
            if((Boolean) domain.getExtensionparams().get("line_idsdirtyflag"))
                model.setLine_ids(domain.getLineIds());
            if((Boolean) domain.getExtensionparams().get("report_message_frequencydirtyflag"))
                model.setReport_message_frequency(domain.getReportMessageFrequency());
            if((Boolean) domain.getExtensionparams().get("message_partner_idsdirtyflag"))
                model.setMessage_partner_ids(domain.getMessagePartnerIds());
            if((Boolean) domain.getExtensionparams().get("display_namedirtyflag"))
                model.setDisplay_name(domain.getDisplayName());
            if((Boolean) domain.getExtensionparams().get("message_channel_idsdirtyflag"))
                model.setMessage_channel_ids(domain.getMessageChannelIds());
            if((Boolean) domain.getExtensionparams().get("message_idsdirtyflag"))
                model.setMessage_ids(domain.getMessageIds());
            if((Boolean) domain.getExtensionparams().get("reward_realtimedirtyflag"))
                model.setReward_realtime(domain.getRewardRealtime());
            if((Boolean) domain.getExtensionparams().get("reward_failuredirtyflag"))
                model.setReward_failure(domain.getRewardFailure());
            if((Boolean) domain.getExtensionparams().get("perioddirtyflag"))
                model.setPeriod(domain.getPeriod());
            if((Boolean) domain.getExtensionparams().get("visibility_modedirtyflag"))
                model.setVisibility_mode(domain.getVisibilityMode());
            if((Boolean) domain.getExtensionparams().get("remind_update_delaydirtyflag"))
                model.setRemind_update_delay(domain.getRemindUpdateDelay());
            if((Boolean) domain.getExtensionparams().get("write_datedirtyflag"))
                model.setWrite_date(domain.getWriteDate());
            if((Boolean) domain.getExtensionparams().get("__last_updatedirtyflag"))
                model.set__last_update(domain.getLastUpdate());
            if((Boolean) domain.getExtensionparams().get("invited_user_idsdirtyflag"))
                model.setInvited_user_ids(domain.getInvitedUserIds());
            if((Boolean) domain.getExtensionparams().get("message_needaction_counterdirtyflag"))
                model.setMessage_needaction_counter(domain.getMessageNeedactionCounter());
            if((Boolean) domain.getExtensionparams().get("statedirtyflag"))
                model.setState(domain.getState());
            if((Boolean) domain.getExtensionparams().get("next_report_datedirtyflag"))
                model.setNext_report_date(domain.getNextReportDate());
            if((Boolean) domain.getExtensionparams().get("manager_id_textdirtyflag"))
                model.setManager_id_text(domain.getManagerIdText());
            if((Boolean) domain.getExtensionparams().get("report_message_group_id_textdirtyflag"))
                model.setReport_message_group_id_text(domain.getReportMessageGroupIdText());
            if((Boolean) domain.getExtensionparams().get("report_template_id_textdirtyflag"))
                model.setReport_template_id_text(domain.getReportTemplateIdText());
            if((Boolean) domain.getExtensionparams().get("reward_id_textdirtyflag"))
                model.setReward_id_text(domain.getRewardIdText());
            if((Boolean) domain.getExtensionparams().get("reward_first_id_textdirtyflag"))
                model.setReward_first_id_text(domain.getRewardFirstIdText());
            if((Boolean) domain.getExtensionparams().get("create_uid_textdirtyflag"))
                model.setCreate_uid_text(domain.getCreateUidText());
            if((Boolean) domain.getExtensionparams().get("write_uid_textdirtyflag"))
                model.setWrite_uid_text(domain.getWriteUidText());
            if((Boolean) domain.getExtensionparams().get("reward_second_id_textdirtyflag"))
                model.setReward_second_id_text(domain.getRewardSecondIdText());
            if((Boolean) domain.getExtensionparams().get("reward_third_id_textdirtyflag"))
                model.setReward_third_id_text(domain.getRewardThirdIdText());
            if((Boolean) domain.getExtensionparams().get("reward_first_iddirtyflag"))
                model.setReward_first_id(domain.getRewardFirstId());
            if((Boolean) domain.getExtensionparams().get("report_template_iddirtyflag"))
                model.setReport_template_id(domain.getReportTemplateId());
            if((Boolean) domain.getExtensionparams().get("reward_second_iddirtyflag"))
                model.setReward_second_id(domain.getRewardSecondId());
            if((Boolean) domain.getExtensionparams().get("reward_third_iddirtyflag"))
                model.setReward_third_id(domain.getRewardThirdId());
            if((Boolean) domain.getExtensionparams().get("report_message_group_iddirtyflag"))
                model.setReport_message_group_id(domain.getReportMessageGroupId());
            if((Boolean) domain.getExtensionparams().get("reward_iddirtyflag"))
                model.setReward_id(domain.getRewardId());
            if((Boolean) domain.getExtensionparams().get("manager_iddirtyflag"))
                model.setManager_id(domain.getManagerId());
            if((Boolean) domain.getExtensionparams().get("write_uiddirtyflag"))
                model.setWrite_uid(domain.getWriteUid());
            if((Boolean) domain.getExtensionparams().get("create_uiddirtyflag"))
                model.setCreate_uid(domain.getCreateUid());
            domain.getFocusNull().clear();
        }
        return model ;
    }

    public Gamification_challenge convert2Domain( gamification_challengeClientModel model ,Gamification_challenge domain) {
        if(model == null)
            return domain ;
        if(domain == null){
            domain = new Gamification_challenge();
        }

        if(model.getWebsite_message_idsDirtyFlag())
            domain.setWebsiteMessageIds(model.getWebsite_message_ids());
        if(model.getNameDirtyFlag())
            domain.setName(model.getName());
        if(model.getMessage_needactionDirtyFlag())
            domain.setMessageNeedaction(model.getMessage_needaction());
        if(model.getIdDirtyFlag())
            domain.setId(model.getId());
        if(model.getCreate_dateDirtyFlag())
            domain.setCreateDate(model.getCreate_date());
        if(model.getMessage_unreadDirtyFlag())
            domain.setMessageUnread(model.getMessage_unread());
        if(model.getStart_dateDirtyFlag())
            domain.setStartDate(model.getStart_date());
        if(model.getMessage_has_error_counterDirtyFlag())
            domain.setMessageHasErrorCounter(model.getMessage_has_error_counter());
        if(model.getMessage_main_attachment_idDirtyFlag())
            domain.setMessageMainAttachmentId(model.getMessage_main_attachment_id());
        if(model.getMessage_has_errorDirtyFlag())
            domain.setMessageHasError(model.getMessage_has_error());
        if(model.getLast_report_dateDirtyFlag())
            domain.setLastReportDate(model.getLast_report_date());
        if(model.getEnd_dateDirtyFlag())
            domain.setEndDate(model.getEnd_date());
        if(model.getMessage_follower_idsDirtyFlag())
            domain.setMessageFollowerIds(model.getMessage_follower_ids());
        if(model.getMessage_attachment_countDirtyFlag())
            domain.setMessageAttachmentCount(model.getMessage_attachment_count());
        if(model.getMessage_is_followerDirtyFlag())
            domain.setMessageIsFollower(model.getMessage_is_follower());
        if(model.getUser_domainDirtyFlag())
            domain.setUserDomain(model.getUser_domain());
        if(model.getCategoryDirtyFlag())
            domain.setCategory(model.getCategory());
        if(model.getDescriptionDirtyFlag())
            domain.setDescription(model.getDescription());
        if(model.getUser_idsDirtyFlag())
            domain.setUserIds(model.getUser_ids());
        if(model.getMessage_unread_counterDirtyFlag())
            domain.setMessageUnreadCounter(model.getMessage_unread_counter());
        if(model.getLine_idsDirtyFlag())
            domain.setLineIds(model.getLine_ids());
        if(model.getReport_message_frequencyDirtyFlag())
            domain.setReportMessageFrequency(model.getReport_message_frequency());
        if(model.getMessage_partner_idsDirtyFlag())
            domain.setMessagePartnerIds(model.getMessage_partner_ids());
        if(model.getDisplay_nameDirtyFlag())
            domain.setDisplayName(model.getDisplay_name());
        if(model.getMessage_channel_idsDirtyFlag())
            domain.setMessageChannelIds(model.getMessage_channel_ids());
        if(model.getMessage_idsDirtyFlag())
            domain.setMessageIds(model.getMessage_ids());
        if(model.getReward_realtimeDirtyFlag())
            domain.setRewardRealtime(model.getReward_realtime());
        if(model.getReward_failureDirtyFlag())
            domain.setRewardFailure(model.getReward_failure());
        if(model.getPeriodDirtyFlag())
            domain.setPeriod(model.getPeriod());
        if(model.getVisibility_modeDirtyFlag())
            domain.setVisibilityMode(model.getVisibility_mode());
        if(model.getRemind_update_delayDirtyFlag())
            domain.setRemindUpdateDelay(model.getRemind_update_delay());
        if(model.getWrite_dateDirtyFlag())
            domain.setWriteDate(model.getWrite_date());
        if(model.get__last_updateDirtyFlag())
            domain.setLastUpdate(model.get__last_update());
        if(model.getInvited_user_idsDirtyFlag())
            domain.setInvitedUserIds(model.getInvited_user_ids());
        if(model.getMessage_needaction_counterDirtyFlag())
            domain.setMessageNeedactionCounter(model.getMessage_needaction_counter());
        if(model.getStateDirtyFlag())
            domain.setState(model.getState());
        if(model.getNext_report_dateDirtyFlag())
            domain.setNextReportDate(model.getNext_report_date());
        if(model.getManager_id_textDirtyFlag())
            domain.setManagerIdText(model.getManager_id_text());
        if(model.getReport_message_group_id_textDirtyFlag())
            domain.setReportMessageGroupIdText(model.getReport_message_group_id_text());
        if(model.getReport_template_id_textDirtyFlag())
            domain.setReportTemplateIdText(model.getReport_template_id_text());
        if(model.getReward_id_textDirtyFlag())
            domain.setRewardIdText(model.getReward_id_text());
        if(model.getReward_first_id_textDirtyFlag())
            domain.setRewardFirstIdText(model.getReward_first_id_text());
        if(model.getCreate_uid_textDirtyFlag())
            domain.setCreateUidText(model.getCreate_uid_text());
        if(model.getWrite_uid_textDirtyFlag())
            domain.setWriteUidText(model.getWrite_uid_text());
        if(model.getReward_second_id_textDirtyFlag())
            domain.setRewardSecondIdText(model.getReward_second_id_text());
        if(model.getReward_third_id_textDirtyFlag())
            domain.setRewardThirdIdText(model.getReward_third_id_text());
        if(model.getReward_first_idDirtyFlag())
            domain.setRewardFirstId(model.getReward_first_id());
        if(model.getReport_template_idDirtyFlag())
            domain.setReportTemplateId(model.getReport_template_id());
        if(model.getReward_second_idDirtyFlag())
            domain.setRewardSecondId(model.getReward_second_id());
        if(model.getReward_third_idDirtyFlag())
            domain.setRewardThirdId(model.getReward_third_id());
        if(model.getReport_message_group_idDirtyFlag())
            domain.setReportMessageGroupId(model.getReport_message_group_id());
        if(model.getReward_idDirtyFlag())
            domain.setRewardId(model.getReward_id());
        if(model.getManager_idDirtyFlag())
            domain.setManagerId(model.getManager_id());
        if(model.getWrite_uidDirtyFlag())
            domain.setWriteUid(model.getWrite_uid());
        if(model.getCreate_uidDirtyFlag())
            domain.setCreateUid(model.getCreate_uid());
        return domain ;
    }

}

    



