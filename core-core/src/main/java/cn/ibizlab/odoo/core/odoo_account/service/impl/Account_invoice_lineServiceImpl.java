package cn.ibizlab.odoo.core.odoo_account.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_account.domain.Account_invoice_line;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_invoice_lineSearchContext;
import cn.ibizlab.odoo.core.odoo_account.service.IAccount_invoice_lineService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;



import cn.ibizlab.odoo.core.odoo_account.client.account_invoice_lineOdooClient;
import cn.ibizlab.odoo.core.odoo_account.clientmodel.account_invoice_lineClientModel;

/**
 * 实体[发票行] 服务对象接口实现
 */
@Slf4j
@Service
public class Account_invoice_lineServiceImpl implements IAccount_invoice_lineService {

    @Autowired
    account_invoice_lineOdooClient account_invoice_lineOdooClient;


    @Override
    public boolean update(Account_invoice_line et) {
        account_invoice_lineClientModel clientModel = convert2Model(et,null);
		account_invoice_lineOdooClient.update(clientModel);
        Account_invoice_line rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }
    
    public void updateBatch(List<Account_invoice_line> list){
    }

    @Override
    public Account_invoice_line get(Integer id) {
        account_invoice_lineClientModel clientModel = new account_invoice_lineClientModel();
        clientModel.setId(id);
		account_invoice_lineOdooClient.get(clientModel);
        Account_invoice_line et = convert2Domain(clientModel,null);
        if(et==null){
            et=new Account_invoice_line();
            et.setId(id);
        }
        else{
        }

        return  et;
    }

    @Override
    public boolean remove(Integer id) {
        account_invoice_lineClientModel clientModel = new account_invoice_lineClientModel();
        clientModel.setId(id);
		account_invoice_lineOdooClient.remove(clientModel);
        return true; 
    }

    public void removeBatch(Collection<Integer> idList){
    }

    @Override
    public boolean create(Account_invoice_line et) {
        account_invoice_lineClientModel clientModel = convert2Model(et,null);
		account_invoice_lineOdooClient.create(clientModel);
        Account_invoice_line rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Account_invoice_line> list){
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Account_invoice_line> searchDefault(Account_invoice_lineSearchContext context) {
        List<Account_invoice_line> list = new ArrayList<Account_invoice_line>();
        Page<account_invoice_lineClientModel> clientModelList = account_invoice_lineOdooClient.search(context);
        for(account_invoice_lineClientModel clientModel : clientModelList){
            list.add( this.convert2Domain(clientModel,null));
        }
        return new PageImpl<Account_invoice_line>(list, context.getPageable(), clientModelList.getTotalElements());
    }

    public account_invoice_lineClientModel convert2Model(Account_invoice_line domain , account_invoice_lineClientModel model) {
        if(domain == null)
            return model ;
        if(model==null){
            model = new account_invoice_lineClientModel();

        }
        if((Boolean) domain.getExtensionparams().get("dirtyflagenable")){
            if((Boolean) domain.getExtensionparams().get("display_typedirtyflag"))
                model.setDisplay_type(domain.getDisplayType());
            if((Boolean) domain.getExtensionparams().get("namedirtyflag"))
                model.setName(domain.getName());
            if((Boolean) domain.getExtensionparams().get("price_subtotaldirtyflag"))
                model.setPrice_subtotal(domain.getPriceSubtotal());
            if((Boolean) domain.getExtensionparams().get("display_namedirtyflag"))
                model.setDisplay_name(domain.getDisplayName());
            if((Boolean) domain.getExtensionparams().get("origindirtyflag"))
                model.setOrigin(domain.getOrigin());
            if((Boolean) domain.getExtensionparams().get("create_datedirtyflag"))
                model.setCreate_date(domain.getCreateDate());
            if((Boolean) domain.getExtensionparams().get("sale_line_idsdirtyflag"))
                model.setSale_line_ids(domain.getSaleLineIds());
            if((Boolean) domain.getExtensionparams().get("discountdirtyflag"))
                model.setDiscount(domain.getDiscount());
            if((Boolean) domain.getExtensionparams().get("price_totaldirtyflag"))
                model.setPrice_total(domain.getPriceTotal());
            if((Boolean) domain.getExtensionparams().get("__last_updatedirtyflag"))
                model.set__last_update(domain.getLastUpdate());
            if((Boolean) domain.getExtensionparams().get("quantitydirtyflag"))
                model.setQuantity(domain.getQuantity());
            if((Boolean) domain.getExtensionparams().get("price_taxdirtyflag"))
                model.setPrice_tax(domain.getPriceTax());
            if((Boolean) domain.getExtensionparams().get("price_subtotal_signeddirtyflag"))
                model.setPrice_subtotal_signed(domain.getPriceSubtotalSigned());
            if((Boolean) domain.getExtensionparams().get("sequencedirtyflag"))
                model.setSequence(domain.getSequence());
            if((Boolean) domain.getExtensionparams().get("price_unitdirtyflag"))
                model.setPrice_unit(domain.getPriceUnit());
            if((Boolean) domain.getExtensionparams().get("iddirtyflag"))
                model.setId(domain.getId());
            if((Boolean) domain.getExtensionparams().get("invoice_line_tax_idsdirtyflag"))
                model.setInvoice_line_tax_ids(domain.getInvoiceLineTaxIds());
            if((Boolean) domain.getExtensionparams().get("analytic_tag_idsdirtyflag"))
                model.setAnalytic_tag_ids(domain.getAnalyticTagIds());
            if((Boolean) domain.getExtensionparams().get("is_rounding_linedirtyflag"))
                model.setIs_rounding_line(domain.getIsRoundingLine());
            if((Boolean) domain.getExtensionparams().get("write_datedirtyflag"))
                model.setWrite_date(domain.getWriteDate());
            if((Boolean) domain.getExtensionparams().get("currency_id_textdirtyflag"))
                model.setCurrency_id_text(domain.getCurrencyIdText());
            if((Boolean) domain.getExtensionparams().get("account_analytic_id_textdirtyflag"))
                model.setAccount_analytic_id_text(domain.getAccountAnalyticIdText());
            if((Boolean) domain.getExtensionparams().get("product_id_textdirtyflag"))
                model.setProduct_id_text(domain.getProductIdText());
            if((Boolean) domain.getExtensionparams().get("invoice_id_textdirtyflag"))
                model.setInvoice_id_text(domain.getInvoiceIdText());
            if((Boolean) domain.getExtensionparams().get("partner_id_textdirtyflag"))
                model.setPartner_id_text(domain.getPartnerIdText());
            if((Boolean) domain.getExtensionparams().get("create_uid_textdirtyflag"))
                model.setCreate_uid_text(domain.getCreateUidText());
            if((Boolean) domain.getExtensionparams().get("invoice_typedirtyflag"))
                model.setInvoice_type(domain.getInvoiceType());
            if((Boolean) domain.getExtensionparams().get("product_imagedirtyflag"))
                model.setProduct_image(domain.getProductImage());
            if((Boolean) domain.getExtensionparams().get("uom_id_textdirtyflag"))
                model.setUom_id_text(domain.getUomIdText());
            if((Boolean) domain.getExtensionparams().get("company_currency_iddirtyflag"))
                model.setCompany_currency_id(domain.getCompanyCurrencyId());
            if((Boolean) domain.getExtensionparams().get("purchase_iddirtyflag"))
                model.setPurchase_id(domain.getPurchaseId());
            if((Boolean) domain.getExtensionparams().get("account_id_textdirtyflag"))
                model.setAccount_id_text(domain.getAccountIdText());
            if((Boolean) domain.getExtensionparams().get("purchase_line_id_textdirtyflag"))
                model.setPurchase_line_id_text(domain.getPurchaseLineIdText());
            if((Boolean) domain.getExtensionparams().get("write_uid_textdirtyflag"))
                model.setWrite_uid_text(domain.getWriteUidText());
            if((Boolean) domain.getExtensionparams().get("company_id_textdirtyflag"))
                model.setCompany_id_text(domain.getCompanyIdText());
            if((Boolean) domain.getExtensionparams().get("invoice_iddirtyflag"))
                model.setInvoice_id(domain.getInvoiceId());
            if((Boolean) domain.getExtensionparams().get("create_uiddirtyflag"))
                model.setCreate_uid(domain.getCreateUid());
            if((Boolean) domain.getExtensionparams().get("product_iddirtyflag"))
                model.setProduct_id(domain.getProductId());
            if((Boolean) domain.getExtensionparams().get("account_iddirtyflag"))
                model.setAccount_id(domain.getAccountId());
            if((Boolean) domain.getExtensionparams().get("purchase_line_iddirtyflag"))
                model.setPurchase_line_id(domain.getPurchaseLineId());
            if((Boolean) domain.getExtensionparams().get("account_analytic_iddirtyflag"))
                model.setAccount_analytic_id(domain.getAccountAnalyticId());
            if((Boolean) domain.getExtensionparams().get("partner_iddirtyflag"))
                model.setPartner_id(domain.getPartnerId());
            if((Boolean) domain.getExtensionparams().get("write_uiddirtyflag"))
                model.setWrite_uid(domain.getWriteUid());
            if((Boolean) domain.getExtensionparams().get("currency_iddirtyflag"))
                model.setCurrency_id(domain.getCurrencyId());
            if((Boolean) domain.getExtensionparams().get("uom_iddirtyflag"))
                model.setUom_id(domain.getUomId());
            if((Boolean) domain.getExtensionparams().get("company_iddirtyflag"))
                model.setCompany_id(domain.getCompanyId());
            domain.getFocusNull().clear();
        }
        return model ;
    }

    public Account_invoice_line convert2Domain( account_invoice_lineClientModel model ,Account_invoice_line domain) {
        if(model == null)
            return domain ;
        if(domain == null){
            domain = new Account_invoice_line();
        }

        if(model.getDisplay_typeDirtyFlag())
            domain.setDisplayType(model.getDisplay_type());
        if(model.getNameDirtyFlag())
            domain.setName(model.getName());
        if(model.getPrice_subtotalDirtyFlag())
            domain.setPriceSubtotal(model.getPrice_subtotal());
        if(model.getDisplay_nameDirtyFlag())
            domain.setDisplayName(model.getDisplay_name());
        if(model.getOriginDirtyFlag())
            domain.setOrigin(model.getOrigin());
        if(model.getCreate_dateDirtyFlag())
            domain.setCreateDate(model.getCreate_date());
        if(model.getSale_line_idsDirtyFlag())
            domain.setSaleLineIds(model.getSale_line_ids());
        if(model.getDiscountDirtyFlag())
            domain.setDiscount(model.getDiscount());
        if(model.getPrice_totalDirtyFlag())
            domain.setPriceTotal(model.getPrice_total());
        if(model.get__last_updateDirtyFlag())
            domain.setLastUpdate(model.get__last_update());
        if(model.getQuantityDirtyFlag())
            domain.setQuantity(model.getQuantity());
        if(model.getPrice_taxDirtyFlag())
            domain.setPriceTax(model.getPrice_tax());
        if(model.getPrice_subtotal_signedDirtyFlag())
            domain.setPriceSubtotalSigned(model.getPrice_subtotal_signed());
        if(model.getSequenceDirtyFlag())
            domain.setSequence(model.getSequence());
        if(model.getPrice_unitDirtyFlag())
            domain.setPriceUnit(model.getPrice_unit());
        if(model.getIdDirtyFlag())
            domain.setId(model.getId());
        if(model.getInvoice_line_tax_idsDirtyFlag())
            domain.setInvoiceLineTaxIds(model.getInvoice_line_tax_ids());
        if(model.getAnalytic_tag_idsDirtyFlag())
            domain.setAnalyticTagIds(model.getAnalytic_tag_ids());
        if(model.getIs_rounding_lineDirtyFlag())
            domain.setIsRoundingLine(model.getIs_rounding_line());
        if(model.getWrite_dateDirtyFlag())
            domain.setWriteDate(model.getWrite_date());
        if(model.getCurrency_id_textDirtyFlag())
            domain.setCurrencyIdText(model.getCurrency_id_text());
        if(model.getAccount_analytic_id_textDirtyFlag())
            domain.setAccountAnalyticIdText(model.getAccount_analytic_id_text());
        if(model.getProduct_id_textDirtyFlag())
            domain.setProductIdText(model.getProduct_id_text());
        if(model.getInvoice_id_textDirtyFlag())
            domain.setInvoiceIdText(model.getInvoice_id_text());
        if(model.getPartner_id_textDirtyFlag())
            domain.setPartnerIdText(model.getPartner_id_text());
        if(model.getCreate_uid_textDirtyFlag())
            domain.setCreateUidText(model.getCreate_uid_text());
        if(model.getInvoice_typeDirtyFlag())
            domain.setInvoiceType(model.getInvoice_type());
        if(model.getProduct_imageDirtyFlag())
            domain.setProductImage(model.getProduct_image());
        if(model.getUom_id_textDirtyFlag())
            domain.setUomIdText(model.getUom_id_text());
        if(model.getCompany_currency_idDirtyFlag())
            domain.setCompanyCurrencyId(model.getCompany_currency_id());
        if(model.getPurchase_idDirtyFlag())
            domain.setPurchaseId(model.getPurchase_id());
        if(model.getAccount_id_textDirtyFlag())
            domain.setAccountIdText(model.getAccount_id_text());
        if(model.getPurchase_line_id_textDirtyFlag())
            domain.setPurchaseLineIdText(model.getPurchase_line_id_text());
        if(model.getWrite_uid_textDirtyFlag())
            domain.setWriteUidText(model.getWrite_uid_text());
        if(model.getCompany_id_textDirtyFlag())
            domain.setCompanyIdText(model.getCompany_id_text());
        if(model.getInvoice_idDirtyFlag())
            domain.setInvoiceId(model.getInvoice_id());
        if(model.getCreate_uidDirtyFlag())
            domain.setCreateUid(model.getCreate_uid());
        if(model.getProduct_idDirtyFlag())
            domain.setProductId(model.getProduct_id());
        if(model.getAccount_idDirtyFlag())
            domain.setAccountId(model.getAccount_id());
        if(model.getPurchase_line_idDirtyFlag())
            domain.setPurchaseLineId(model.getPurchase_line_id());
        if(model.getAccount_analytic_idDirtyFlag())
            domain.setAccountAnalyticId(model.getAccount_analytic_id());
        if(model.getPartner_idDirtyFlag())
            domain.setPartnerId(model.getPartner_id());
        if(model.getWrite_uidDirtyFlag())
            domain.setWriteUid(model.getWrite_uid());
        if(model.getCurrency_idDirtyFlag())
            domain.setCurrencyId(model.getCurrency_id());
        if(model.getUom_idDirtyFlag())
            domain.setUomId(model.getUom_id());
        if(model.getCompany_idDirtyFlag())
            domain.setCompanyId(model.getCompany_id());
        return domain ;
    }

}

    



