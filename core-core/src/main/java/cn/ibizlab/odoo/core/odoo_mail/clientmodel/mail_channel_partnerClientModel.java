package cn.ibizlab.odoo.core.odoo_mail.clientmodel;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.math.BigInteger;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.util.helper.OdooClientHelper;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang.ArrayUtils;
import org.springframework.util.StringUtils;

/**
 * 接口实体[mail_channel_partner] 对象
 */
public class mail_channel_partnerClientModel implements Serializable{

    /**
     * 渠道
     */
    public Integer channel_id;

    @JsonIgnore
    public boolean channel_idDirtyFlag;
    
    /**
     * 渠道
     */
    public String channel_id_text;

    @JsonIgnore
    public boolean channel_id_textDirtyFlag;
    
    /**
     * 创建时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp create_date;

    @JsonIgnore
    public boolean create_dateDirtyFlag;
    
    /**
     * 创建人
     */
    public Integer create_uid;

    @JsonIgnore
    public boolean create_uidDirtyFlag;
    
    /**
     * 创建人
     */
    public String create_uid_text;

    @JsonIgnore
    public boolean create_uid_textDirtyFlag;
    
    /**
     * 显示名称
     */
    public String display_name;

    @JsonIgnore
    public boolean display_nameDirtyFlag;
    
    /**
     * 对话收拢状态
     */
    public String fold_state;

    @JsonIgnore
    public boolean fold_stateDirtyFlag;
    
    /**
     * ID
     */
    public Integer id;

    @JsonIgnore
    public boolean idDirtyFlag;
    
    /**
     * 黑名单
     */
    public String is_blacklisted;

    @JsonIgnore
    public boolean is_blacklistedDirtyFlag;
    
    /**
     * 对话已最小化
     */
    public String is_minimized;

    @JsonIgnore
    public boolean is_minimizedDirtyFlag;
    
    /**
     * 是否置顶
     */
    public String is_pinned;

    @JsonIgnore
    public boolean is_pinnedDirtyFlag;
    
    /**
     * EMail
     */
    public String partner_email;

    @JsonIgnore
    public boolean partner_emailDirtyFlag;
    
    /**
     * 收件人
     */
    public Integer partner_id;

    @JsonIgnore
    public boolean partner_idDirtyFlag;
    
    /**
     * 收件人
     */
    public String partner_id_text;

    @JsonIgnore
    public boolean partner_id_textDirtyFlag;
    
    /**
     * 最近一次查阅
     */
    public Integer seen_message_id;

    @JsonIgnore
    public boolean seen_message_idDirtyFlag;
    
    /**
     * 最后更新时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp write_date;

    @JsonIgnore
    public boolean write_dateDirtyFlag;
    
    /**
     * 最后更新者
     */
    public Integer write_uid;

    @JsonIgnore
    public boolean write_uidDirtyFlag;
    
    /**
     * 最后更新者
     */
    public String write_uid_text;

    @JsonIgnore
    public boolean write_uid_textDirtyFlag;
    
    /**
     * 最后修改日
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp __last_update;

    @JsonIgnore
    public boolean __last_updateDirtyFlag;
    
    /**
     * 获取 [渠道]
     */
    @JsonProperty("channel_id")
    public Integer getChannel_id(){
        return this.channel_id ;
    }

    /**
     * 设置 [渠道]
     */
    @JsonProperty("channel_id")
    public void setChannel_id(Integer  channel_id){
        this.channel_id = channel_id ;
        this.channel_idDirtyFlag = true ;
    }

     /**
     * 获取 [渠道]脏标记
     */
    @JsonIgnore
    public boolean getChannel_idDirtyFlag(){
        return this.channel_idDirtyFlag ;
    }   

    /**
     * 获取 [渠道]
     */
    @JsonProperty("channel_id_text")
    public String getChannel_id_text(){
        return this.channel_id_text ;
    }

    /**
     * 设置 [渠道]
     */
    @JsonProperty("channel_id_text")
    public void setChannel_id_text(String  channel_id_text){
        this.channel_id_text = channel_id_text ;
        this.channel_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [渠道]脏标记
     */
    @JsonIgnore
    public boolean getChannel_id_textDirtyFlag(){
        return this.channel_id_textDirtyFlag ;
    }   

    /**
     * 获取 [创建时间]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return this.create_date ;
    }

    /**
     * 设置 [创建时间]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

     /**
     * 获取 [创建时间]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return this.create_dateDirtyFlag ;
    }   

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return this.create_uid ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

     /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return this.create_uidDirtyFlag ;
    }   

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return this.create_uid_text ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

     /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return this.create_uid_textDirtyFlag ;
    }   

    /**
     * 获取 [显示名称]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return this.display_name ;
    }

    /**
     * 设置 [显示名称]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

     /**
     * 获取 [显示名称]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return this.display_nameDirtyFlag ;
    }   

    /**
     * 获取 [对话收拢状态]
     */
    @JsonProperty("fold_state")
    public String getFold_state(){
        return this.fold_state ;
    }

    /**
     * 设置 [对话收拢状态]
     */
    @JsonProperty("fold_state")
    public void setFold_state(String  fold_state){
        this.fold_state = fold_state ;
        this.fold_stateDirtyFlag = true ;
    }

     /**
     * 获取 [对话收拢状态]脏标记
     */
    @JsonIgnore
    public boolean getFold_stateDirtyFlag(){
        return this.fold_stateDirtyFlag ;
    }   

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return this.id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

     /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return this.idDirtyFlag ;
    }   

    /**
     * 获取 [黑名单]
     */
    @JsonProperty("is_blacklisted")
    public String getIs_blacklisted(){
        return this.is_blacklisted ;
    }

    /**
     * 设置 [黑名单]
     */
    @JsonProperty("is_blacklisted")
    public void setIs_blacklisted(String  is_blacklisted){
        this.is_blacklisted = is_blacklisted ;
        this.is_blacklistedDirtyFlag = true ;
    }

     /**
     * 获取 [黑名单]脏标记
     */
    @JsonIgnore
    public boolean getIs_blacklistedDirtyFlag(){
        return this.is_blacklistedDirtyFlag ;
    }   

    /**
     * 获取 [对话已最小化]
     */
    @JsonProperty("is_minimized")
    public String getIs_minimized(){
        return this.is_minimized ;
    }

    /**
     * 设置 [对话已最小化]
     */
    @JsonProperty("is_minimized")
    public void setIs_minimized(String  is_minimized){
        this.is_minimized = is_minimized ;
        this.is_minimizedDirtyFlag = true ;
    }

     /**
     * 获取 [对话已最小化]脏标记
     */
    @JsonIgnore
    public boolean getIs_minimizedDirtyFlag(){
        return this.is_minimizedDirtyFlag ;
    }   

    /**
     * 获取 [是否置顶]
     */
    @JsonProperty("is_pinned")
    public String getIs_pinned(){
        return this.is_pinned ;
    }

    /**
     * 设置 [是否置顶]
     */
    @JsonProperty("is_pinned")
    public void setIs_pinned(String  is_pinned){
        this.is_pinned = is_pinned ;
        this.is_pinnedDirtyFlag = true ;
    }

     /**
     * 获取 [是否置顶]脏标记
     */
    @JsonIgnore
    public boolean getIs_pinnedDirtyFlag(){
        return this.is_pinnedDirtyFlag ;
    }   

    /**
     * 获取 [EMail]
     */
    @JsonProperty("partner_email")
    public String getPartner_email(){
        return this.partner_email ;
    }

    /**
     * 设置 [EMail]
     */
    @JsonProperty("partner_email")
    public void setPartner_email(String  partner_email){
        this.partner_email = partner_email ;
        this.partner_emailDirtyFlag = true ;
    }

     /**
     * 获取 [EMail]脏标记
     */
    @JsonIgnore
    public boolean getPartner_emailDirtyFlag(){
        return this.partner_emailDirtyFlag ;
    }   

    /**
     * 获取 [收件人]
     */
    @JsonProperty("partner_id")
    public Integer getPartner_id(){
        return this.partner_id ;
    }

    /**
     * 设置 [收件人]
     */
    @JsonProperty("partner_id")
    public void setPartner_id(Integer  partner_id){
        this.partner_id = partner_id ;
        this.partner_idDirtyFlag = true ;
    }

     /**
     * 获取 [收件人]脏标记
     */
    @JsonIgnore
    public boolean getPartner_idDirtyFlag(){
        return this.partner_idDirtyFlag ;
    }   

    /**
     * 获取 [收件人]
     */
    @JsonProperty("partner_id_text")
    public String getPartner_id_text(){
        return this.partner_id_text ;
    }

    /**
     * 设置 [收件人]
     */
    @JsonProperty("partner_id_text")
    public void setPartner_id_text(String  partner_id_text){
        this.partner_id_text = partner_id_text ;
        this.partner_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [收件人]脏标记
     */
    @JsonIgnore
    public boolean getPartner_id_textDirtyFlag(){
        return this.partner_id_textDirtyFlag ;
    }   

    /**
     * 获取 [最近一次查阅]
     */
    @JsonProperty("seen_message_id")
    public Integer getSeen_message_id(){
        return this.seen_message_id ;
    }

    /**
     * 设置 [最近一次查阅]
     */
    @JsonProperty("seen_message_id")
    public void setSeen_message_id(Integer  seen_message_id){
        this.seen_message_id = seen_message_id ;
        this.seen_message_idDirtyFlag = true ;
    }

     /**
     * 获取 [最近一次查阅]脏标记
     */
    @JsonIgnore
    public boolean getSeen_message_idDirtyFlag(){
        return this.seen_message_idDirtyFlag ;
    }   

    /**
     * 获取 [最后更新时间]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return this.write_date ;
    }

    /**
     * 设置 [最后更新时间]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新时间]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return this.write_dateDirtyFlag ;
    }   

    /**
     * 获取 [最后更新者]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return this.write_uid ;
    }

    /**
     * 设置 [最后更新者]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新者]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return this.write_uidDirtyFlag ;
    }   

    /**
     * 获取 [最后更新者]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return this.write_uid_text ;
    }

    /**
     * 设置 [最后更新者]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新者]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return this.write_uid_textDirtyFlag ;
    }   

    /**
     * 获取 [最后修改日]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return this.__last_update ;
    }

    /**
     * 设置 [最后修改日]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

     /**
     * 获取 [最后修改日]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return this.__last_updateDirtyFlag ;
    }   



    public void fromMap(Map<String, Object> map) throws Exception {
		if(!(map.get("channel_id") instanceof Boolean)&& map.get("channel_id")!=null){
			Object[] objs = (Object[])map.get("channel_id");
			if(objs.length > 0){
				this.setChannel_id((Integer)objs[0]);
			}
		}
		if(!(map.get("channel_id") instanceof Boolean)&& map.get("channel_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("channel_id");
			if(objs.length > 1){
				this.setChannel_id_text((String)objs[1]);
			}
		}
		if(!(map.get("create_date") instanceof Boolean)&& map.get("create_date")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse((String)map.get("create_date"));
   			this.setCreate_date(new Timestamp(parse.getTime()));
		}
		if(!(map.get("create_uid") instanceof Boolean)&& map.get("create_uid")!=null){
			Object[] objs = (Object[])map.get("create_uid");
			if(objs.length > 0){
				this.setCreate_uid((Integer)objs[0]);
			}
		}
		if(!(map.get("create_uid") instanceof Boolean)&& map.get("create_uid")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("create_uid");
			if(objs.length > 1){
				this.setCreate_uid_text((String)objs[1]);
			}
		}
		if(!(map.get("display_name") instanceof Boolean)&& map.get("display_name")!=null){
			this.setDisplay_name((String)map.get("display_name"));
		}
		if(!(map.get("fold_state") instanceof Boolean)&& map.get("fold_state")!=null){
			this.setFold_state((String)map.get("fold_state"));
		}
		if(!(map.get("id") instanceof Boolean)&& map.get("id")!=null){
			this.setId((Integer)map.get("id"));
		}
		if(map.get("is_blacklisted") instanceof Boolean){
			this.setIs_blacklisted(((Boolean)map.get("is_blacklisted"))? "true" : "false");
		}
		if(map.get("is_minimized") instanceof Boolean){
			this.setIs_minimized(((Boolean)map.get("is_minimized"))? "true" : "false");
		}
		if(map.get("is_pinned") instanceof Boolean){
			this.setIs_pinned(((Boolean)map.get("is_pinned"))? "true" : "false");
		}
		if(!(map.get("partner_email") instanceof Boolean)&& map.get("partner_email")!=null){
			this.setPartner_email((String)map.get("partner_email"));
		}
		if(!(map.get("partner_id") instanceof Boolean)&& map.get("partner_id")!=null){
			Object[] objs = (Object[])map.get("partner_id");
			if(objs.length > 0){
				this.setPartner_id((Integer)objs[0]);
			}
		}
		if(!(map.get("partner_id") instanceof Boolean)&& map.get("partner_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("partner_id");
			if(objs.length > 1){
				this.setPartner_id_text((String)objs[1]);
			}
		}
		if(!(map.get("seen_message_id") instanceof Boolean)&& map.get("seen_message_id")!=null){
			Object[] objs = (Object[])map.get("seen_message_id");
			if(objs.length > 0){
				this.setSeen_message_id((Integer)objs[0]);
			}
		}
		if(!(map.get("write_date") instanceof Boolean)&& map.get("write_date")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse((String)map.get("write_date"));
   			this.setWrite_date(new Timestamp(parse.getTime()));
		}
		if(!(map.get("write_uid") instanceof Boolean)&& map.get("write_uid")!=null){
			Object[] objs = (Object[])map.get("write_uid");
			if(objs.length > 0){
				this.setWrite_uid((Integer)objs[0]);
			}
		}
		if(!(map.get("write_uid") instanceof Boolean)&& map.get("write_uid")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("write_uid");
			if(objs.length > 1){
				this.setWrite_uid_text((String)objs[1]);
			}
		}
		if(!(map.get("__last_update") instanceof Boolean)&& map.get("__last_update")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse((String)map.get("__last_update"));
   			this.set__last_update(new Timestamp(parse.getTime()));
		}
	}

	public Map<String, Object> toMap() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		if(this.getChannel_id()!=null&&this.getChannel_idDirtyFlag()){
			map.put("channel_id",this.getChannel_id());
		}else if(this.getChannel_idDirtyFlag()){
			map.put("channel_id",false);
		}
		if(this.getChannel_id_text()!=null&&this.getChannel_id_textDirtyFlag()){
			//忽略文本外键channel_id_text
		}else if(this.getChannel_id_textDirtyFlag()){
			map.put("channel_id",false);
		}
		if(this.getCreate_date()!=null&&this.getCreate_dateDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String datetimeStr = sdf.format(this.getCreate_date());
			map.put("create_date",datetimeStr);
		}else if(this.getCreate_dateDirtyFlag()){
			map.put("create_date",false);
		}
		if(this.getCreate_uid()!=null&&this.getCreate_uidDirtyFlag()){
			map.put("create_uid",this.getCreate_uid());
		}else if(this.getCreate_uidDirtyFlag()){
			map.put("create_uid",false);
		}
		if(this.getCreate_uid_text()!=null&&this.getCreate_uid_textDirtyFlag()){
			//忽略文本外键create_uid_text
		}else if(this.getCreate_uid_textDirtyFlag()){
			map.put("create_uid",false);
		}
		if(this.getDisplay_name()!=null&&this.getDisplay_nameDirtyFlag()){
			map.put("display_name",this.getDisplay_name());
		}else if(this.getDisplay_nameDirtyFlag()){
			map.put("display_name",false);
		}
		if(this.getFold_state()!=null&&this.getFold_stateDirtyFlag()){
			map.put("fold_state",this.getFold_state());
		}else if(this.getFold_stateDirtyFlag()){
			map.put("fold_state",false);
		}
		if(this.getId()!=null&&this.getIdDirtyFlag()){
			map.put("id",this.getId());
		}else if(this.getIdDirtyFlag()){
			map.put("id",false);
		}
		if(this.getIs_blacklisted()!=null&&this.getIs_blacklistedDirtyFlag()){
			map.put("is_blacklisted",Boolean.parseBoolean(this.getIs_blacklisted()));		
		}		if(this.getIs_minimized()!=null&&this.getIs_minimizedDirtyFlag()){
			map.put("is_minimized",Boolean.parseBoolean(this.getIs_minimized()));		
		}		if(this.getIs_pinned()!=null&&this.getIs_pinnedDirtyFlag()){
			map.put("is_pinned",Boolean.parseBoolean(this.getIs_pinned()));		
		}		if(this.getPartner_email()!=null&&this.getPartner_emailDirtyFlag()){
			map.put("partner_email",this.getPartner_email());
		}else if(this.getPartner_emailDirtyFlag()){
			map.put("partner_email",false);
		}
		if(this.getPartner_id()!=null&&this.getPartner_idDirtyFlag()){
			map.put("partner_id",this.getPartner_id());
		}else if(this.getPartner_idDirtyFlag()){
			map.put("partner_id",false);
		}
		if(this.getPartner_id_text()!=null&&this.getPartner_id_textDirtyFlag()){
			//忽略文本外键partner_id_text
		}else if(this.getPartner_id_textDirtyFlag()){
			map.put("partner_id",false);
		}
		if(this.getSeen_message_id()!=null&&this.getSeen_message_idDirtyFlag()){
			map.put("seen_message_id",this.getSeen_message_id());
		}else if(this.getSeen_message_idDirtyFlag()){
			map.put("seen_message_id",false);
		}
		if(this.getWrite_date()!=null&&this.getWrite_dateDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String datetimeStr = sdf.format(this.getWrite_date());
			map.put("write_date",datetimeStr);
		}else if(this.getWrite_dateDirtyFlag()){
			map.put("write_date",false);
		}
		if(this.getWrite_uid()!=null&&this.getWrite_uidDirtyFlag()){
			map.put("write_uid",this.getWrite_uid());
		}else if(this.getWrite_uidDirtyFlag()){
			map.put("write_uid",false);
		}
		if(this.getWrite_uid_text()!=null&&this.getWrite_uid_textDirtyFlag()){
			//忽略文本外键write_uid_text
		}else if(this.getWrite_uid_textDirtyFlag()){
			map.put("write_uid",false);
		}
		if(this.get__last_update()!=null&&this.get__last_updateDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String datetimeStr = sdf.format(this.get__last_update());
			map.put("__last_update",datetimeStr);
		}else if(this.get__last_updateDirtyFlag()){
			map.put("__last_update",false);
		}
		return map;
	}

}
