package cn.ibizlab.odoo.core.repository.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import cn.ibizlab.odoo.core.repository.client.po.gamification_goal_definition;

/**
 * 实体[gamification_goal_definition] 服务对象接口
 */
public interface gamification_goal_definitionRepository{


    public gamification_goal_definition createPO() ;
        public void updateBatch(gamification_goal_definition gamification_goal_definition);

        public void update(gamification_goal_definition gamification_goal_definition);

        public void get(String id);

        public List<gamification_goal_definition> search();

        public void removeBatch(String id);

        public void create(gamification_goal_definition gamification_goal_definition);

        public void remove(String id);

        public void createBatch(gamification_goal_definition gamification_goal_definition);


}
