package cn.ibizlab.odoo.core.odoo_base.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_base.domain.Base_module_update;
import cn.ibizlab.odoo.core.odoo_base.filter.Base_module_updateSearchContext;
import cn.ibizlab.odoo.core.odoo_base.service.IBase_module_updateService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;



import cn.ibizlab.odoo.core.odoo_base.client.base_module_updateOdooClient;
import cn.ibizlab.odoo.core.odoo_base.clientmodel.base_module_updateClientModel;

/**
 * 实体[更新模块] 服务对象接口实现
 */
@Slf4j
@Service
public class Base_module_updateServiceImpl implements IBase_module_updateService {

    @Autowired
    base_module_updateOdooClient base_module_updateOdooClient;


    @Override
    public Base_module_update get(Integer id) {
        base_module_updateClientModel clientModel = new base_module_updateClientModel();
        clientModel.setId(id);
		base_module_updateOdooClient.get(clientModel);
        Base_module_update et = convert2Domain(clientModel,null);
        if(et==null){
            et=new Base_module_update();
            et.setId(id);
        }
        else{
        }

        return  et;
    }

    @Override
    public boolean update(Base_module_update et) {
        base_module_updateClientModel clientModel = convert2Model(et,null);
		base_module_updateOdooClient.update(clientModel);
        Base_module_update rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }
    
    public void updateBatch(List<Base_module_update> list){
    }

    @Override
    public boolean create(Base_module_update et) {
        base_module_updateClientModel clientModel = convert2Model(et,null);
		base_module_updateOdooClient.create(clientModel);
        Base_module_update rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Base_module_update> list){
    }

    @Override
    public boolean remove(Integer id) {
        base_module_updateClientModel clientModel = new base_module_updateClientModel();
        clientModel.setId(id);
		base_module_updateOdooClient.remove(clientModel);
        return true; 
    }

    public void removeBatch(Collection<Integer> idList){
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Base_module_update> searchDefault(Base_module_updateSearchContext context) {
        List<Base_module_update> list = new ArrayList<Base_module_update>();
        Page<base_module_updateClientModel> clientModelList = base_module_updateOdooClient.search(context);
        for(base_module_updateClientModel clientModel : clientModelList){
            list.add( this.convert2Domain(clientModel,null));
        }
        return new PageImpl<Base_module_update>(list, context.getPageable(), clientModelList.getTotalElements());
    }

    public base_module_updateClientModel convert2Model(Base_module_update domain , base_module_updateClientModel model) {
        if(domain == null)
            return model ;
        if(model==null){
            model = new base_module_updateClientModel();

        }
        if((Boolean) domain.getExtensionparams().get("dirtyflagenable")){
            if((Boolean) domain.getExtensionparams().get("iddirtyflag"))
                model.setId(domain.getId());
            if((Boolean) domain.getExtensionparams().get("display_namedirtyflag"))
                model.setDisplay_name(domain.getDisplayName());
            if((Boolean) domain.getExtensionparams().get("statedirtyflag"))
                model.setState(domain.getState());
            if((Boolean) domain.getExtensionparams().get("__last_updatedirtyflag"))
                model.set__last_update(domain.getLastUpdate());
            if((Boolean) domain.getExtensionparams().get("updateddirtyflag"))
                model.setUpdated(domain.getUpdated());
            if((Boolean) domain.getExtensionparams().get("write_datedirtyflag"))
                model.setWrite_date(domain.getWriteDate());
            if((Boolean) domain.getExtensionparams().get("create_datedirtyflag"))
                model.setCreate_date(domain.getCreateDate());
            if((Boolean) domain.getExtensionparams().get("addeddirtyflag"))
                model.setAdded(domain.getAdded());
            if((Boolean) domain.getExtensionparams().get("write_uid_textdirtyflag"))
                model.setWrite_uid_text(domain.getWriteUidText());
            if((Boolean) domain.getExtensionparams().get("create_uid_textdirtyflag"))
                model.setCreate_uid_text(domain.getCreateUidText());
            if((Boolean) domain.getExtensionparams().get("create_uiddirtyflag"))
                model.setCreate_uid(domain.getCreateUid());
            if((Boolean) domain.getExtensionparams().get("write_uiddirtyflag"))
                model.setWrite_uid(domain.getWriteUid());
            domain.getFocusNull().clear();
        }
        return model ;
    }

    public Base_module_update convert2Domain( base_module_updateClientModel model ,Base_module_update domain) {
        if(model == null)
            return domain ;
        if(domain == null){
            domain = new Base_module_update();
        }

        if(model.getIdDirtyFlag())
            domain.setId(model.getId());
        if(model.getDisplay_nameDirtyFlag())
            domain.setDisplayName(model.getDisplay_name());
        if(model.getStateDirtyFlag())
            domain.setState(model.getState());
        if(model.get__last_updateDirtyFlag())
            domain.setLastUpdate(model.get__last_update());
        if(model.getUpdatedDirtyFlag())
            domain.setUpdated(model.getUpdated());
        if(model.getWrite_dateDirtyFlag())
            domain.setWriteDate(model.getWrite_date());
        if(model.getCreate_dateDirtyFlag())
            domain.setCreateDate(model.getCreate_date());
        if(model.getAddedDirtyFlag())
            domain.setAdded(model.getAdded());
        if(model.getWrite_uid_textDirtyFlag())
            domain.setWriteUidText(model.getWrite_uid_text());
        if(model.getCreate_uid_textDirtyFlag())
            domain.setCreateUidText(model.getCreate_uid_text());
        if(model.getCreate_uidDirtyFlag())
            domain.setCreateUid(model.getCreate_uid());
        if(model.getWrite_uidDirtyFlag())
            domain.setWriteUid(model.getWrite_uid());
        return domain ;
    }

}

    



