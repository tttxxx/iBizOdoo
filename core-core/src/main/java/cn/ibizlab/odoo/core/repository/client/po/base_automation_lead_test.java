package cn.ibizlab.odoo.core.repository.client.po;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigDecimal;

/**
 * [base_automation_lead_test] 对象
 */
public interface base_automation_lead_test {

    public String getActive();

    public void setActive(String active);

    public Timestamp getCreate_date();

    public void setCreate_date(Timestamp create_date);

    public Integer getCreate_uid();

    public void setCreate_uid(Integer create_uid);

    public String getCreate_uid_text();

    public void setCreate_uid_text(String create_uid_text);

    public String getCustomer();

    public void setCustomer(String customer);

    public Timestamp getDate_action_last();

    public void setDate_action_last(Timestamp date_action_last);

    public String getDeadline();

    public void setDeadline(String deadline);

    public String getDisplay_name();

    public void setDisplay_name(String display_name);

    public Integer getId();

    public void setId(Integer id);

    public String getIs_assigned_to_admin();

    public void setIs_assigned_to_admin(String is_assigned_to_admin);

    public String getLine_ids();

    public void setLine_ids(String line_ids);

    public String getName();

    public void setName(String name);

    public Integer getPartner_id();

    public void setPartner_id(Integer partner_id);

    public String getPartner_id_text();

    public void setPartner_id_text(String partner_id_text);

    public String getPriority();

    public void setPriority(String priority);

    public String getState();

    public void setState(String state);

    public Integer getUser_id();

    public void setUser_id(Integer user_id);

    public String getUser_id_text();

    public void setUser_id_text(String user_id_text);

    public Timestamp getWrite_date();

    public void setWrite_date(Timestamp write_date);

    public Integer getWrite_uid();

    public void setWrite_uid(Integer write_uid);

    public String getWrite_uid_text();

    public void setWrite_uid_text(String write_uid_text);

    public Timestamp get__last_update();

    public void set__last_update(Timestamp __last_update);

}
