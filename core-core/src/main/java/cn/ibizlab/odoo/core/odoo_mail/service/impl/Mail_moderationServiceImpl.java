package cn.ibizlab.odoo.core.odoo_mail.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_mail.domain.Mail_moderation;
import cn.ibizlab.odoo.core.odoo_mail.filter.Mail_moderationSearchContext;
import cn.ibizlab.odoo.core.odoo_mail.service.IMail_moderationService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;



import cn.ibizlab.odoo.core.odoo_mail.client.mail_moderationOdooClient;
import cn.ibizlab.odoo.core.odoo_mail.clientmodel.mail_moderationClientModel;

/**
 * 实体[渠道黑/白名单] 服务对象接口实现
 */
@Slf4j
@Service
public class Mail_moderationServiceImpl implements IMail_moderationService {

    @Autowired
    mail_moderationOdooClient mail_moderationOdooClient;


    @Override
    public boolean remove(Integer id) {
        mail_moderationClientModel clientModel = new mail_moderationClientModel();
        clientModel.setId(id);
		mail_moderationOdooClient.remove(clientModel);
        return true; 
    }

    public void removeBatch(Collection<Integer> idList){
    }

    @Override
    public boolean update(Mail_moderation et) {
        mail_moderationClientModel clientModel = convert2Model(et,null);
		mail_moderationOdooClient.update(clientModel);
        Mail_moderation rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }
    
    public void updateBatch(List<Mail_moderation> list){
    }

    @Override
    public boolean create(Mail_moderation et) {
        mail_moderationClientModel clientModel = convert2Model(et,null);
		mail_moderationOdooClient.create(clientModel);
        Mail_moderation rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Mail_moderation> list){
    }

    @Override
    public Mail_moderation get(Integer id) {
        mail_moderationClientModel clientModel = new mail_moderationClientModel();
        clientModel.setId(id);
		mail_moderationOdooClient.get(clientModel);
        Mail_moderation et = convert2Domain(clientModel,null);
        if(et==null){
            et=new Mail_moderation();
            et.setId(id);
        }
        else{
        }

        return  et;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Mail_moderation> searchDefault(Mail_moderationSearchContext context) {
        List<Mail_moderation> list = new ArrayList<Mail_moderation>();
        Page<mail_moderationClientModel> clientModelList = mail_moderationOdooClient.search(context);
        for(mail_moderationClientModel clientModel : clientModelList){
            list.add( this.convert2Domain(clientModel,null));
        }
        return new PageImpl<Mail_moderation>(list, context.getPageable(), clientModelList.getTotalElements());
    }

    public mail_moderationClientModel convert2Model(Mail_moderation domain , mail_moderationClientModel model) {
        if(domain == null)
            return model ;
        if(model==null){
            model = new mail_moderationClientModel();

        }
        if((Boolean) domain.getExtensionparams().get("dirtyflagenable")){
            if((Boolean) domain.getExtensionparams().get("write_datedirtyflag"))
                model.setWrite_date(domain.getWriteDate());
            if((Boolean) domain.getExtensionparams().get("create_datedirtyflag"))
                model.setCreate_date(domain.getCreateDate());
            if((Boolean) domain.getExtensionparams().get("statusdirtyflag"))
                model.setStatus(domain.getStatus());
            if((Boolean) domain.getExtensionparams().get("__last_updatedirtyflag"))
                model.set__last_update(domain.getLastUpdate());
            if((Boolean) domain.getExtensionparams().get("display_namedirtyflag"))
                model.setDisplay_name(domain.getDisplayName());
            if((Boolean) domain.getExtensionparams().get("iddirtyflag"))
                model.setId(domain.getId());
            if((Boolean) domain.getExtensionparams().get("emaildirtyflag"))
                model.setEmail(domain.getEmail());
            if((Boolean) domain.getExtensionparams().get("create_uid_textdirtyflag"))
                model.setCreate_uid_text(domain.getCreateUidText());
            if((Boolean) domain.getExtensionparams().get("write_uid_textdirtyflag"))
                model.setWrite_uid_text(domain.getWriteUidText());
            if((Boolean) domain.getExtensionparams().get("channel_id_textdirtyflag"))
                model.setChannel_id_text(domain.getChannelIdText());
            if((Boolean) domain.getExtensionparams().get("create_uiddirtyflag"))
                model.setCreate_uid(domain.getCreateUid());
            if((Boolean) domain.getExtensionparams().get("write_uiddirtyflag"))
                model.setWrite_uid(domain.getWriteUid());
            if((Boolean) domain.getExtensionparams().get("channel_iddirtyflag"))
                model.setChannel_id(domain.getChannelId());
            domain.getFocusNull().clear();
        }
        return model ;
    }

    public Mail_moderation convert2Domain( mail_moderationClientModel model ,Mail_moderation domain) {
        if(model == null)
            return domain ;
        if(domain == null){
            domain = new Mail_moderation();
        }

        if(model.getWrite_dateDirtyFlag())
            domain.setWriteDate(model.getWrite_date());
        if(model.getCreate_dateDirtyFlag())
            domain.setCreateDate(model.getCreate_date());
        if(model.getStatusDirtyFlag())
            domain.setStatus(model.getStatus());
        if(model.get__last_updateDirtyFlag())
            domain.setLastUpdate(model.get__last_update());
        if(model.getDisplay_nameDirtyFlag())
            domain.setDisplayName(model.getDisplay_name());
        if(model.getIdDirtyFlag())
            domain.setId(model.getId());
        if(model.getEmailDirtyFlag())
            domain.setEmail(model.getEmail());
        if(model.getCreate_uid_textDirtyFlag())
            domain.setCreateUidText(model.getCreate_uid_text());
        if(model.getWrite_uid_textDirtyFlag())
            domain.setWriteUidText(model.getWrite_uid_text());
        if(model.getChannel_id_textDirtyFlag())
            domain.setChannelIdText(model.getChannel_id_text());
        if(model.getCreate_uidDirtyFlag())
            domain.setCreateUid(model.getCreate_uid());
        if(model.getWrite_uidDirtyFlag())
            domain.setWriteUid(model.getWrite_uid());
        if(model.getChannel_idDirtyFlag())
            domain.setChannelId(model.getChannel_id());
        return domain ;
    }

}

    



