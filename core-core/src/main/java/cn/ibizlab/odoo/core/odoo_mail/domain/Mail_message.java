package cn.ibizlab.odoo.core.odoo_mail.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.odoo.util.domain.EntityBase;
import cn.ibizlab.odoo.util.annotation.DEField;
import cn.ibizlab.odoo.util.enums.DEPredefinedFieldType;
import cn.ibizlab.odoo.util.enums.DEFieldDefaultValueType;
import java.io.Serializable;
import lombok.Data;
import org.springframework.data.annotation.Transient;

import cn.ibizlab.odoo.util.domain.EntityClient;

/**
 * ServiceApi [消息] 对象
 */
@Data
public class Mail_message extends EntityClient implements Serializable {

    /**
     * 待处理
     */
    @JSONField(name = "needaction")
    @JsonProperty("needaction")
    private String needaction;

    /**
     * 布局
     */
    @JSONField(name = "layout")
    @JsonProperty("layout")
    private String layout;

    /**
     * 相关的文档模型
     */
    @JSONField(name = "model")
    @JsonProperty("model")
    private String model;

    /**
     * 无响应
     */
    @DEField(name = "no_auto_thread")
    @JSONField(name = "no_auto_thread")
    @JsonProperty("no_auto_thread")
    private String noAutoThread;

    /**
     * 收件人
     */
    @JSONField(name = "partner_ids")
    @JsonProperty("partner_ids")
    private String partnerIds;

    /**
     * 创建时间
     */
    @DEField(name = "create_date" , preType = DEPredefinedFieldType.CREATEDATE)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;

    /**
     * 消息记录名称
     */
    @DEField(name = "record_name")
    @JSONField(name = "record_name")
    @JsonProperty("record_name")
    private String recordName;

    /**
     * 消息ID
     */
    @DEField(name = "message_id")
    @JSONField(name = "message_id")
    @JsonProperty("message_id")
    private String messageId;

    /**
     * 追踪值
     */
    @JSONField(name = "tracking_value_ids")
    @JsonProperty("tracking_value_ids")
    private String trackingValueIds;

    /**
     * 管理状态
     */
    @DEField(name = "moderation_status")
    @JSONField(name = "moderation_status")
    @JsonProperty("moderation_status")
    private String moderationStatus;

    /**
     * 通知
     */
    @JSONField(name = "notification_ids")
    @JsonProperty("notification_ids")
    private String notificationIds;

    /**
     * 从
     */
    @DEField(name = "email_from")
    @JSONField(name = "email_from")
    @JsonProperty("email_from")
    private String emailFrom;

    /**
     * 渠道
     */
    @JSONField(name = "channel_ids")
    @JsonProperty("channel_ids")
    private String channelIds;

    /**
     * ID
     */
    @DEField(isKeyField=true)
    @JSONField(name = "id")
    @JsonProperty("id")
    private Integer id;

    /**
     * 说明
     */
    @JSONField(name = "description")
    @JsonProperty("description")
    private String description;

    /**
     * 加星的邮件
     */
    @JSONField(name = "starred")
    @JsonProperty("starred")
    private String starred;

    /**
     * 相关评级
     */
    @JSONField(name = "rating_ids")
    @JsonProperty("rating_ids")
    private String ratingIds;

    /**
     * 待处理的业务伙伴
     */
    @JSONField(name = "needaction_partner_ids")
    @JsonProperty("needaction_partner_ids")
    private String needactionPartnerIds;

    /**
     * 回复 至
     */
    @DEField(name = "reply_to")
    @JSONField(name = "reply_to")
    @JsonProperty("reply_to")
    private String replyTo;

    /**
     * 邮件发送服务器
     */
    @DEField(name = "mail_server_id")
    @JSONField(name = "mail_server_id")
    @JsonProperty("mail_server_id")
    private Integer mailServerId;

    /**
     * 相关文档编号
     */
    @DEField(name = "res_id")
    @JSONField(name = "res_id")
    @JsonProperty("res_id")
    private Integer resId;

    /**
     * 需审核
     */
    @JSONField(name = "need_moderation")
    @JsonProperty("need_moderation")
    private String needModeration;

    /**
     * 主题
     */
    @JSONField(name = "subject")
    @JsonProperty("subject")
    private String subject;

    /**
     * 内容
     */
    @JSONField(name = "body")
    @JsonProperty("body")
    private String body;

    /**
     * 显示名称
     */
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    private String displayName;

    /**
     * 已发布
     */
    @DEField(name = "website_published")
    @JSONField(name = "website_published")
    @JsonProperty("website_published")
    private String websitePublished;

    /**
     * 附件
     */
    @JSONField(name = "attachment_ids")
    @JsonProperty("attachment_ids")
    private String attachmentIds;

    /**
     * 下级消息
     */
    @JSONField(name = "child_ids")
    @JsonProperty("child_ids")
    private String childIds;

    /**
     * 评级值
     */
    @JSONField(name = "rating_value")
    @JsonProperty("rating_value")
    private Double ratingValue;

    /**
     * 添加签名
     */
    @DEField(name = "add_sign")
    @JSONField(name = "add_sign")
    @JsonProperty("add_sign")
    private String addSign;

    /**
     * 收藏夹
     */
    @JSONField(name = "starred_partner_ids")
    @JsonProperty("starred_partner_ids")
    private String starredPartnerIds;

    /**
     * 最后修改日
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;

    /**
     * 最后更新时间
     */
    @DEField(name = "write_date" , preType = DEPredefinedFieldType.UPDATEDATE)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;

    /**
     * 日期
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("date")
    private Timestamp date;

    /**
     * 有误差
     */
    @JSONField(name = "has_error")
    @JsonProperty("has_error")
    private String hasError;

    /**
     * 类型
     */
    @DEField(name = "message_type")
    @JSONField(name = "message_type")
    @JsonProperty("message_type")
    private String messageType;

    /**
     * 作者
     */
    @JSONField(name = "author_id_text")
    @JsonProperty("author_id_text")
    private String authorIdText;

    /**
     * 创建人
     */
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    private String createUidText;

    /**
     * 邮件活动类型
     */
    @JSONField(name = "mail_activity_type_id_text")
    @JsonProperty("mail_activity_type_id_text")
    private String mailActivityTypeIdText;

    /**
     * 管理员
     */
    @JSONField(name = "moderator_id_text")
    @JsonProperty("moderator_id_text")
    private String moderatorIdText;

    /**
     * 最后更新者
     */
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    private String writeUidText;

    /**
     * 子类型
     */
    @JSONField(name = "subtype_id_text")
    @JsonProperty("subtype_id_text")
    private String subtypeIdText;

    /**
     * 作者头像
     */
    @JSONField(name = "author_avatar")
    @JsonProperty("author_avatar")
    private byte[] authorAvatar;

    /**
     * 最后更新者
     */
    @DEField(name = "write_uid" , preType = DEPredefinedFieldType.UPDATEMAN)
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    private Integer writeUid;

    /**
     * 邮件活动类型
     */
    @DEField(name = "mail_activity_type_id")
    @JSONField(name = "mail_activity_type_id")
    @JsonProperty("mail_activity_type_id")
    private Integer mailActivityTypeId;

    /**
     * 管理员
     */
    @DEField(name = "moderator_id")
    @JSONField(name = "moderator_id")
    @JsonProperty("moderator_id")
    private Integer moderatorId;

    /**
     * 创建人
     */
    @DEField(name = "create_uid" , preType = DEPredefinedFieldType.CREATEMAN)
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    private Integer createUid;

    /**
     * 作者
     */
    @DEField(name = "author_id")
    @JSONField(name = "author_id")
    @JsonProperty("author_id")
    private Integer authorId;

    /**
     * 上级消息
     */
    @DEField(name = "parent_id")
    @JSONField(name = "parent_id")
    @JsonProperty("parent_id")
    private Integer parentId;

    /**
     * 子类型
     */
    @DEField(name = "subtype_id")
    @JSONField(name = "subtype_id")
    @JsonProperty("subtype_id")
    private Integer subtypeId;


    /**
     * 
     */
    @JSONField(name = "odoomailactivitytype")
    @JsonProperty("odoomailactivitytype")
    private cn.ibizlab.odoo.core.odoo_mail.domain.Mail_activity_type odooMailActivityType;

    /**
     * 
     */
    @JSONField(name = "odoosubtype")
    @JsonProperty("odoosubtype")
    private cn.ibizlab.odoo.core.odoo_mail.domain.Mail_message_subtype odooSubtype;

    /**
     * 
     */
    @JSONField(name = "odooparent")
    @JsonProperty("odooparent")
    private cn.ibizlab.odoo.core.odoo_mail.domain.Mail_message odooParent;

    /**
     * 
     */
    @JSONField(name = "odooauthor")
    @JsonProperty("odooauthor")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_partner odooAuthor;

    /**
     * 
     */
    @JSONField(name = "odoocreate")
    @JsonProperty("odoocreate")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_users odooCreate;

    /**
     * 
     */
    @JSONField(name = "odoomoderator")
    @JsonProperty("odoomoderator")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_users odooModerator;

    /**
     * 
     */
    @JSONField(name = "odoowrite")
    @JsonProperty("odoowrite")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_users odooWrite;




    /**
     * 设置 [布局]
     */
    public void setLayout(String layout){
        this.layout = layout ;
        this.modify("layout",layout);
    }
    /**
     * 设置 [相关的文档模型]
     */
    public void setModel(String model){
        this.model = model ;
        this.modify("model",model);
    }
    /**
     * 设置 [无响应]
     */
    public void setNoAutoThread(String noAutoThread){
        this.noAutoThread = noAutoThread ;
        this.modify("no_auto_thread",noAutoThread);
    }
    /**
     * 设置 [消息记录名称]
     */
    public void setRecordName(String recordName){
        this.recordName = recordName ;
        this.modify("record_name",recordName);
    }
    /**
     * 设置 [消息ID]
     */
    public void setMessageId(String messageId){
        this.messageId = messageId ;
        this.modify("message_id",messageId);
    }
    /**
     * 设置 [管理状态]
     */
    public void setModerationStatus(String moderationStatus){
        this.moderationStatus = moderationStatus ;
        this.modify("moderation_status",moderationStatus);
    }
    /**
     * 设置 [从]
     */
    public void setEmailFrom(String emailFrom){
        this.emailFrom = emailFrom ;
        this.modify("email_from",emailFrom);
    }
    /**
     * 设置 [回复 至]
     */
    public void setReplyTo(String replyTo){
        this.replyTo = replyTo ;
        this.modify("reply_to",replyTo);
    }
    /**
     * 设置 [邮件发送服务器]
     */
    public void setMailServerId(Integer mailServerId){
        this.mailServerId = mailServerId ;
        this.modify("mail_server_id",mailServerId);
    }
    /**
     * 设置 [相关文档编号]
     */
    public void setResId(Integer resId){
        this.resId = resId ;
        this.modify("res_id",resId);
    }
    /**
     * 设置 [主题]
     */
    public void setSubject(String subject){
        this.subject = subject ;
        this.modify("subject",subject);
    }
    /**
     * 设置 [内容]
     */
    public void setBody(String body){
        this.body = body ;
        this.modify("body",body);
    }
    /**
     * 设置 [已发布]
     */
    public void setWebsitePublished(String websitePublished){
        this.websitePublished = websitePublished ;
        this.modify("website_published",websitePublished);
    }
    /**
     * 设置 [添加签名]
     */
    public void setAddSign(String addSign){
        this.addSign = addSign ;
        this.modify("add_sign",addSign);
    }
    /**
     * 设置 [日期]
     */
    public void setDate(Timestamp date){
        this.date = date ;
        this.modify("date",date);
    }
    /**
     * 设置 [类型]
     */
    public void setMessageType(String messageType){
        this.messageType = messageType ;
        this.modify("message_type",messageType);
    }
    /**
     * 设置 [邮件活动类型]
     */
    public void setMailActivityTypeId(Integer mailActivityTypeId){
        this.mailActivityTypeId = mailActivityTypeId ;
        this.modify("mail_activity_type_id",mailActivityTypeId);
    }
    /**
     * 设置 [管理员]
     */
    public void setModeratorId(Integer moderatorId){
        this.moderatorId = moderatorId ;
        this.modify("moderator_id",moderatorId);
    }
    /**
     * 设置 [作者]
     */
    public void setAuthorId(Integer authorId){
        this.authorId = authorId ;
        this.modify("author_id",authorId);
    }
    /**
     * 设置 [上级消息]
     */
    public void setParentId(Integer parentId){
        this.parentId = parentId ;
        this.modify("parent_id",parentId);
    }
    /**
     * 设置 [子类型]
     */
    public void setSubtypeId(Integer subtypeId){
        this.subtypeId = subtypeId ;
        this.modify("subtype_id",subtypeId);
    }

}


