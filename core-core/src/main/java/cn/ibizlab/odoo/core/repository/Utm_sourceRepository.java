package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Utm_source;
import cn.ibizlab.odoo.core.odoo_utm.filter.Utm_sourceSearchContext;

/**
 * 实体 [UTM来源] 存储对象
 */
public interface Utm_sourceRepository extends Repository<Utm_source> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Utm_source> searchDefault(Utm_sourceSearchContext context);

    Utm_source convert2PO(cn.ibizlab.odoo.core.odoo_utm.domain.Utm_source domain , Utm_source po) ;

    cn.ibizlab.odoo.core.odoo_utm.domain.Utm_source convert2Domain( Utm_source po ,cn.ibizlab.odoo.core.odoo_utm.domain.Utm_source domain) ;

}
