package cn.ibizlab.odoo.core.repository.po ;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.math.BigInteger;

import cn.ibizlab.odoo.core.odoo_base.filter.Res_usersSearchContext;

/**
 * 实体 [用户] 存储模型
 */
public interface Res_users{

    /**
     * 管理员
     */
    String getIs_moderator();

    void setIs_moderator(String is_moderator);

    /**
     * 获取 [管理员]脏标记
     */
    boolean getIs_moderatorDirtyFlag();

    /**
     * 资源
     */
    String getResource_ids();

    void setResource_ids(String resource_ids);

    /**
     * 获取 [资源]脏标记
     */
    boolean getResource_idsDirtyFlag();

    /**
     * 创建日期
     */
    Timestamp getCreate_date();

    void setCreate_date(Timestamp create_date);

    /**
     * 获取 [创建日期]脏标记
     */
    boolean getCreate_dateDirtyFlag();

    /**
     * 标签
     */
    String getCategory_id();

    void setCategory_id(String category_id);

    /**
     * 获取 [标签]脏标记
     */
    boolean getCategory_idDirtyFlag();

    /**
     * 默认工作时间
     */
    Integer getResource_calendar_id();

    void setResource_calendar_id(Integer resource_calendar_id);

    /**
     * 获取 [默认工作时间]脏标记
     */
    boolean getResource_calendar_idDirtyFlag();

    /**
     * 用户登录记录
     */
    String getLog_ids();

    void setLog_ids(String log_ids);

    /**
     * 获取 [用户登录记录]脏标记
     */
    boolean getLog_idsDirtyFlag();

    /**
     * 消息
     */
    String getMessage_ids();

    void setMessage_ids(String message_ids);

    /**
     * 获取 [消息]脏标记
     */
    boolean getMessage_idsDirtyFlag();

    /**
     * 安全PIN
     */
    String getPos_security_pin();

    void setPos_security_pin(String pos_security_pin);

    /**
     * 获取 [安全PIN]脏标记
     */
    boolean getPos_security_pinDirtyFlag();

    /**
     * 徽章
     */
    String getBadge_ids();

    void setBadge_ids(String badge_ids);

    /**
     * 获取 [徽章]脏标记
     */
    boolean getBadge_idsDirtyFlag();

    /**
     * 最后更新时间
     */
    Timestamp getWrite_date();

    void setWrite_date(Timestamp write_date);

    /**
     * 获取 [最后更新时间]脏标记
     */
    boolean getWrite_dateDirtyFlag();

    /**
     * ID
     */
    Integer getId();

    void setId(Integer id);

    /**
     * 获取 [ID]脏标记
     */
    boolean getIdDirtyFlag();

    /**
     * 公司
     */
    String getCompany_ids();

    void setCompany_ids(String company_ids);

    /**
     * 获取 [公司]脏标记
     */
    boolean getCompany_idsDirtyFlag();

    /**
     * 联系人
     */
    String getChild_ids();

    void setChild_ids(String child_ids);

    /**
     * 获取 [联系人]脏标记
     */
    boolean getChild_idsDirtyFlag();

    /**
     * 时区偏移
     */
    String getTz_offset();

    void setTz_offset(String tz_offset);

    /**
     * 获取 [时区偏移]脏标记
     */
    boolean getTz_offsetDirtyFlag();

    /**
     * 活动达成
     */
    Integer getTarget_sales_done();

    void setTarget_sales_done(Integer target_sales_done);

    /**
     * 获取 [活动达成]脏标记
     */
    boolean getTarget_sales_doneDirtyFlag();

    /**
     * 关注者(业务伙伴)
     */
    String getMessage_partner_ids();

    void setMessage_partner_ids(String message_partner_ids);

    /**
     * 获取 [关注者(业务伙伴)]脏标记
     */
    boolean getMessage_partner_idsDirtyFlag();

    /**
     * 通知管理
     */
    String getNotification_type();

    void setNotification_type(String notification_type);

    /**
     * 获取 [通知管理]脏标记
     */
    boolean getNotification_typeDirtyFlag();

    /**
     * 有效
     */
    String getActive();

    void setActive(String active);

    /**
     * 获取 [有效]脏标记
     */
    boolean getActiveDirtyFlag();

    /**
     * IM的状态
     */
    String getIm_status();

    void setIm_status(String im_status);

    /**
     * 获取 [IM的状态]脏标记
     */
    boolean getIm_statusDirtyFlag();

    /**
     * 贡献值
     */
    Integer getKarma();

    void setKarma(Integer karma);

    /**
     * 获取 [贡献值]脏标记
     */
    boolean getKarmaDirtyFlag();

    /**
     * 登记网站
     */
    Integer getWebsite_id();

    void setWebsite_id(Integer website_id);

    /**
     * 获取 [登记网站]脏标记
     */
    boolean getWebsite_idDirtyFlag();

    /**
     * 活动
     */
    String getActivity_ids();

    void setActivity_ids(String activity_ids);

    /**
     * 获取 [活动]脏标记
     */
    boolean getActivity_idsDirtyFlag();

    /**
     * 金质徽章个数
     */
    Integer getGold_badge();

    void setGold_badge(Integer gold_badge);

    /**
     * 获取 [金质徽章个数]脏标记
     */
    boolean getGold_badgeDirtyFlag();

    /**
     * 相关的员工
     */
    String getEmployee_ids();

    void setEmployee_ids(String employee_ids);

    /**
     * 获取 [相关的员工]脏标记
     */
    boolean getEmployee_idsDirtyFlag();

    /**
     * 状态
     */
    String getState();

    void setState(String state);

    /**
     * 获取 [状态]脏标记
     */
    boolean getStateDirtyFlag();

    /**
     * 管理频道
     */
    String getModeration_channel_ids();

    void setModeration_channel_ids(String moderation_channel_ids);

    /**
     * 获取 [管理频道]脏标记
     */
    boolean getModeration_channel_idsDirtyFlag();

    /**
     * 关注者(渠道)
     */
    String getMessage_channel_ids();

    void setMessage_channel_ids(String message_channel_ids);

    /**
     * 获取 [关注者(渠道)]脏标记
     */
    boolean getMessage_channel_idsDirtyFlag();

    /**
     * 银质徽章个数
     */
    Integer getSilver_badge();

    void setSilver_badge(Integer silver_badge);

    /**
     * 获取 [银质徽章个数]脏标记
     */
    boolean getSilver_badgeDirtyFlag();

    /**
     * 付款令牌
     */
    String getPayment_token_ids();

    void setPayment_token_ids(String payment_token_ids);

    /**
     * 获取 [付款令牌]脏标记
     */
    boolean getPayment_token_idsDirtyFlag();

    /**
     * 显示名称
     */
    String getDisplay_name();

    void setDisplay_name(String display_name);

    /**
     * 获取 [显示名称]脏标记
     */
    boolean getDisplay_nameDirtyFlag();

    /**
     * 公司数量
     */
    Integer getCompanies_count();

    void setCompanies_count(Integer companies_count);

    /**
     * 获取 [公司数量]脏标记
     */
    boolean getCompanies_countDirtyFlag();

    /**
     * 销售订单目标发票
     */
    Integer getTarget_sales_invoiced();

    void setTarget_sales_invoiced(Integer target_sales_invoiced);

    /**
     * 获取 [销售订单目标发票]脏标记
     */
    boolean getTarget_sales_invoicedDirtyFlag();

    /**
     * 关注者
     */
    String getMessage_follower_ids();

    void setMessage_follower_ids(String message_follower_ids);

    /**
     * 获取 [关注者]脏标记
     */
    boolean getMessage_follower_idsDirtyFlag();

    /**
     * 最后连接
     */
    Timestamp getLogin_date();

    void setLogin_date(Timestamp login_date);

    /**
     * 获取 [最后连接]脏标记
     */
    boolean getLogin_dateDirtyFlag();

    /**
     * 渠道
     */
    String getChannel_ids();

    void setChannel_ids(String channel_ids);

    /**
     * 获取 [渠道]脏标记
     */
    boolean getChannel_idsDirtyFlag();

    /**
     * 群组
     */
    String getGroups_id();

    void setGroups_id(String groups_id);

    /**
     * 获取 [群组]脏标记
     */
    boolean getGroups_idDirtyFlag();

    /**
     * 共享用户
     */
    String getShare();

    void setShare(String share);

    /**
     * 获取 [共享用户]脏标记
     */
    boolean getShareDirtyFlag();

    /**
     * 银行
     */
    String getBank_ids();

    void setBank_ids(String bank_ids);

    /**
     * 获取 [银行]脏标记
     */
    boolean getBank_idsDirtyFlag();

    /**
     * 销售订单
     */
    String getSale_order_ids();

    void setSale_order_ids(String sale_order_ids);

    /**
     * 获取 [销售订单]脏标记
     */
    boolean getSale_order_idsDirtyFlag();

    /**
     * 设置密码
     */
    String getNew_password();

    void setNew_password(String new_password);

    /**
     * 获取 [设置密码]脏标记
     */
    boolean getNew_passwordDirtyFlag();

    /**
     * OdooBot 状态
     */
    String getOdoobot_state();

    void setOdoobot_state(String odoobot_state);

    /**
     * 获取 [OdooBot 状态]脏标记
     */
    boolean getOdoobot_stateDirtyFlag();

    /**
     * 公司是指业务伙伴
     */
    String getRef_company_ids();

    void setRef_company_ids(String ref_company_ids);

    /**
     * 获取 [公司是指业务伙伴]脏标记
     */
    boolean getRef_company_idsDirtyFlag();

    /**
     * 密码
     */
    String getPassword();

    void setPassword(String password);

    /**
     * 获取 [密码]脏标记
     */
    boolean getPasswordDirtyFlag();

    /**
     * 青铜徽章数目
     */
    Integer getBronze_badge();

    void setBronze_badge(Integer bronze_badge);

    /**
     * 获取 [青铜徽章数目]脏标记
     */
    boolean getBronze_badgeDirtyFlag();

    /**
     * 会议
     */
    String getMeeting_ids();

    void setMeeting_ids(String meeting_ids);

    /**
     * 获取 [会议]脏标记
     */
    boolean getMeeting_idsDirtyFlag();

    /**
     * 最后修改日
     */
    Timestamp get__last_update();

    void set__last_update(Timestamp __last_update);

    /**
     * 获取 [最后修改日]脏标记
     */
    boolean get__last_updateDirtyFlag();

    /**
     * 待发布的帖子
     */
    Integer getForum_waiting_posts_count();

    void setForum_waiting_posts_count(Integer forum_waiting_posts_count);

    /**
     * 获取 [待发布的帖子]脏标记
     */
    boolean getForum_waiting_posts_countDirtyFlag();

    /**
     * 目标
     */
    String getGoal_ids();

    void setGoal_ids(String goal_ids);

    /**
     * 获取 [目标]脏标记
     */
    boolean getGoal_idsDirtyFlag();

    /**
     * 签单是商机的最终目标
     */
    Integer getTarget_sales_won();

    void setTarget_sales_won(Integer target_sales_won);

    /**
     * 获取 [签单是商机的最终目标]脏标记
     */
    boolean getTarget_sales_wonDirtyFlag();

    /**
     * 网站消息
     */
    String getWebsite_message_ids();

    void setWebsite_message_ids(String website_message_ids);

    /**
     * 获取 [网站消息]脏标记
     */
    boolean getWebsite_message_idsDirtyFlag();

    /**
     * 主页动作
     */
    Integer getAction_id();

    void setAction_id(Integer action_id);

    /**
     * 获取 [主页动作]脏标记
     */
    boolean getAction_idDirtyFlag();

    /**
     * 登录
     */
    String getLogin();

    void setLogin(String login);

    /**
     * 获取 [登录]脏标记
     */
    boolean getLoginDirtyFlag();

    /**
     * 客户合同
     */
    String getContract_ids();

    void setContract_ids(String contract_ids);

    /**
     * 获取 [客户合同]脏标记
     */
    boolean getContract_idsDirtyFlag();

    /**
     * 审核数
     */
    Integer getModeration_counter();

    void setModeration_counter(Integer moderation_counter);

    /**
     * 获取 [审核数]脏标记
     */
    boolean getModeration_counterDirtyFlag();

    /**
     * 签名
     */
    String getSignature();

    void setSignature(String signature);

    /**
     * 获取 [签名]脏标记
     */
    boolean getSignatureDirtyFlag();

    /**
     * 用户
     */
    String getUser_ids();

    void setUser_ids(String user_ids);

    /**
     * 获取 [用户]脏标记
     */
    boolean getUser_idsDirtyFlag();

    /**
     * 商机
     */
    String getOpportunity_ids();

    void setOpportunity_ids(String opportunity_ids);

    /**
     * 获取 [商机]脏标记
     */
    boolean getOpportunity_idsDirtyFlag();

    /**
     * 任务
     */
    String getTask_ids();

    void setTask_ids(String task_ids);

    /**
     * 获取 [任务]脏标记
     */
    boolean getTask_idsDirtyFlag();

    /**
     * 发票
     */
    String getInvoice_ids();

    void setInvoice_ids(String invoice_ids);

    /**
     * 获取 [发票]脏标记
     */
    boolean getInvoice_idsDirtyFlag();

    /**
     * 内部参考
     */
    String getRef();

    void setRef(String ref);

    /**
     * 获取 [内部参考]脏标记
     */
    boolean getRefDirtyFlag();

    /**
     * 错误个数
     */
    Integer getMessage_has_error_counter();

    void setMessage_has_error_counter(Integer message_has_error_counter);

    /**
     * 获取 [错误个数]脏标记
     */
    boolean getMessage_has_error_counterDirtyFlag();

    /**
     * 最近的在线销售订单
     */
    Integer getLast_website_so_id();

    void setLast_website_so_id(Integer last_website_so_id);

    /**
     * 获取 [最近的在线销售订单]脏标记
     */
    boolean getLast_website_so_idDirtyFlag();

    /**
     * 日期
     */
    Timestamp getDate();

    void setDate(Timestamp date);

    /**
     * 获取 [日期]脏标记
     */
    boolean getDateDirtyFlag();

    /**
     * 关注者
     */
    String getMessage_is_follower();

    void setMessage_is_follower(String message_is_follower);

    /**
     * 获取 [关注者]脏标记
     */
    boolean getMessage_is_followerDirtyFlag();

    /**
     * 客户位置
     */
    Integer getProperty_stock_customer();

    void setProperty_stock_customer(Integer property_stock_customer);

    /**
     * 获取 [客户位置]脏标记
     */
    boolean getProperty_stock_customerDirtyFlag();

    /**
     * 最近的发票和付款匹配时间
     */
    Timestamp getLast_time_entries_checked();

    void setLast_time_entries_checked(Timestamp last_time_entries_checked);

    /**
     * 获取 [最近的发票和付款匹配时间]脏标记
     */
    boolean getLast_time_entries_checkedDirtyFlag();

    /**
     * 语言
     */
    String getLang();

    void setLang(String lang);

    /**
     * 获取 [语言]脏标记
     */
    boolean getLangDirtyFlag();

    /**
     * 销售警告
     */
    String getSale_warn();

    void setSale_warn(String sale_warn);

    /**
     * 获取 [销售警告]脏标记
     */
    boolean getSale_warnDirtyFlag();

    /**
     * #会议
     */
    Integer getMeeting_count();

    void setMeeting_count(Integer meeting_count);

    /**
     * 获取 [#会议]脏标记
     */
    boolean getMeeting_countDirtyFlag();

    /**
     * 街道
     */
    String getStreet();

    void setStreet(String street);

    /**
     * 获取 [街道]脏标记
     */
    boolean getStreetDirtyFlag();

    /**
     * 发票
     */
    String getInvoice_warn();

    void setInvoice_warn(String invoice_warn);

    /**
     * 获取 [发票]脏标记
     */
    boolean getInvoice_warnDirtyFlag();

    /**
     * 注册令牌 Token
     */
    String getSignup_token();

    void setSignup_token(String signup_token);

    /**
     * 获取 [注册令牌 Token]脏标记
     */
    boolean getSignup_tokenDirtyFlag();

    /**
     * # 任务
     */
    Integer getTask_count();

    void setTask_count(Integer task_count);

    /**
     * 获取 [# 任务]脏标记
     */
    boolean getTask_countDirtyFlag();

    /**
     * 注册令牌（ Token  ）是有效的
     */
    String getSignup_valid();

    void setSignup_valid(String signup_valid);

    /**
     * 获取 [注册令牌（ Token  ）是有效的]脏标记
     */
    boolean getSignup_validDirtyFlag();

    /**
     * 未读消息计数器
     */
    Integer getMessage_unread_counter();

    void setMessage_unread_counter(Integer message_unread_counter);

    /**
     * 获取 [未读消息计数器]脏标记
     */
    boolean getMessage_unread_counterDirtyFlag();

    /**
     * 注册令牌（Token）类型
     */
    String getSignup_type();

    void setSignup_type(String signup_type);

    /**
     * 获取 [注册令牌（Token）类型]脏标记
     */
    boolean getSignup_typeDirtyFlag();

    /**
     * 应收账款
     */
    Integer getProperty_account_receivable_id();

    void setProperty_account_receivable_id(Integer property_account_receivable_id);

    /**
     * 获取 [应收账款]脏标记
     */
    boolean getProperty_account_receivable_idDirtyFlag();

    /**
     * 网站opengraph图像
     */
    String getWebsite_meta_og_img();

    void setWebsite_meta_og_img(String website_meta_og_img);

    /**
     * 获取 [网站opengraph图像]脏标记
     */
    boolean getWebsite_meta_og_imgDirtyFlag();

    /**
     * 活动
     */
    Integer getEvent_count();

    void setEvent_count(Integer event_count);

    /**
     * 获取 [活动]脏标记
     */
    boolean getEvent_countDirtyFlag();

    /**
     * 日记账项目
     */
    Integer getJournal_item_count();

    void setJournal_item_count(Integer journal_item_count);

    /**
     * 获取 [日记账项目]脏标记
     */
    boolean getJournal_item_countDirtyFlag();

    /**
     * 上级名称
     */
    String getParent_name();

    void setParent_name(String parent_name);

    /**
     * 获取 [上级名称]脏标记
     */
    boolean getParent_nameDirtyFlag();

    /**
     * 用户的销售团队
     */
    String getSale_team_id_text();

    void setSale_team_id_text(String sale_team_id_text);

    /**
     * 获取 [用户的销售团队]脏标记
     */
    boolean getSale_team_id_textDirtyFlag();

    /**
     * 应收总计
     */
    Double getCredit();

    void setCredit(Double credit);

    /**
     * 获取 [应收总计]脏标记
     */
    boolean getCreditDirtyFlag();

    /**
     * 商机
     */
    Integer getOpportunity_count();

    void setOpportunity_count(Integer opportunity_count);

    /**
     * 获取 [商机]脏标记
     */
    boolean getOpportunity_countDirtyFlag();

    /**
     * 注册网址
     */
    String getSignup_url();

    void setSignup_url(String signup_url);

    /**
     * 获取 [注册网址]脏标记
     */
    boolean getSignup_urlDirtyFlag();

    /**
     * 应付账款
     */
    Integer getProperty_account_payable_id();

    void setProperty_account_payable_id(Integer property_account_payable_id);

    /**
     * 获取 [应付账款]脏标记
     */
    boolean getProperty_account_payable_idDirtyFlag();

    /**
     * 省/ 州
     */
    Integer getState_id();

    void setState_id(Integer state_id);

    /**
     * 获取 [省/ 州]脏标记
     */
    boolean getState_idDirtyFlag();

    /**
     * 消息递送错误
     */
    String getMessage_has_error();

    void setMessage_has_error(String message_has_error);

    /**
     * 获取 [消息递送错误]脏标记
     */
    boolean getMessage_has_errorDirtyFlag();

    /**
     * 销售订单消息
     */
    String getSale_warn_msg();

    void setSale_warn_msg(String sale_warn_msg);

    /**
     * 获取 [销售订单消息]脏标记
     */
    boolean getSale_warn_msgDirtyFlag();

    /**
     * 在当前网站显示
     */
    String getWebsite_published();

    void setWebsite_published(String website_published);

    /**
     * 获取 [在当前网站显示]脏标记
     */
    boolean getWebsite_publishedDirtyFlag();

    /**
     * 已开票总计
     */
    Double getTotal_invoiced();

    void setTotal_invoiced(Double total_invoiced);

    /**
     * 获取 [已开票总计]脏标记
     */
    boolean getTotal_invoicedDirtyFlag();

    /**
     * 应付总计
     */
    Double getDebit();

    void setDebit(Double debit);

    /**
     * 获取 [应付总计]脏标记
     */
    boolean getDebitDirtyFlag();

    /**
     * 销售点订单计数
     */
    Integer getPos_order_count();

    void setPos_order_count(Integer pos_order_count);

    /**
     * 获取 [销售点订单计数]脏标记
     */
    boolean getPos_order_countDirtyFlag();

    /**
     * 最后更新者
     */
    String getWrite_uid_text();

    void setWrite_uid_text(String write_uid_text);

    /**
     * 获取 [最后更新者]脏标记
     */
    boolean getWrite_uid_textDirtyFlag();

    /**
     * 称谓
     */
    Integer getTitle();

    void setTitle(Integer title);

    /**
     * 获取 [称谓]脏标记
     */
    boolean getTitleDirtyFlag();

    /**
     * 未读消息
     */
    String getMessage_unread();

    void setMessage_unread(String message_unread);

    /**
     * 获取 [未读消息]脏标记
     */
    boolean getMessage_unreadDirtyFlag();

    /**
     * ＃供应商账单
     */
    Integer getSupplier_invoice_count();

    void setSupplier_invoice_count(Integer supplier_invoice_count);

    /**
     * 获取 [＃供应商账单]脏标记
     */
    boolean getSupplier_invoice_countDirtyFlag();

    /**
     * 城市
     */
    String getCity();

    void setCity(String city);

    /**
     * 获取 [城市]脏标记
     */
    boolean getCityDirtyFlag();

    /**
     * 库存拣货
     */
    String getPicking_warn();

    void setPicking_warn(String picking_warn);

    /**
     * 获取 [库存拣货]脏标记
     */
    boolean getPicking_warnDirtyFlag();

    /**
     * 附件
     */
    Integer getMessage_main_attachment_id();

    void setMessage_main_attachment_id(Integer message_main_attachment_id);

    /**
     * 获取 [附件]脏标记
     */
    boolean getMessage_main_attachment_idDirtyFlag();

    /**
     * 责任用户
     */
    Integer getActivity_user_id();

    void setActivity_user_id(Integer activity_user_id);

    /**
     * 获取 [责任用户]脏标记
     */
    boolean getActivity_user_idDirtyFlag();

    /**
     * 附加信息
     */
    String getAdditional_info();

    void setAdditional_info(String additional_info);

    /**
     * 获取 [附加信息]脏标记
     */
    boolean getAdditional_infoDirtyFlag();

    /**
     * 工作岗位
     */
    String getIbizfunction();

    void setIbizfunction(String ibizfunction);

    /**
     * 获取 [工作岗位]脏标记
     */
    boolean getIbizfunctionDirtyFlag();

    /**
     * 网站网址
     */
    String getWebsite_url();

    void setWebsite_url(String website_url);

    /**
     * 获取 [网站网址]脏标记
     */
    boolean getWebsite_urlDirtyFlag();

    /**
     * 最后的提醒已经标志为已读
     */
    Timestamp getCalendar_last_notif_ack();

    void setCalendar_last_notif_ack(Timestamp calendar_last_notif_ack);

    /**
     * 获取 [最后的提醒已经标志为已读]脏标记
     */
    boolean getCalendar_last_notif_ackDirtyFlag();

    /**
     * 操作次数
     */
    Integer getMessage_needaction_counter();

    void setMessage_needaction_counter(Integer message_needaction_counter);

    /**
     * 获取 [操作次数]脏标记
     */
    boolean getMessage_needaction_counterDirtyFlag();

    /**
     * 下一活动截止日期
     */
    Timestamp getActivity_date_deadline();

    void setActivity_date_deadline(Timestamp activity_date_deadline);

    /**
     * 获取 [下一活动截止日期]脏标记
     */
    boolean getActivity_date_deadlineDirtyFlag();

    /**
     * 员工
     */
    String getEmployee();

    void setEmployee(String employee);

    /**
     * 获取 [员工]脏标记
     */
    boolean getEmployeeDirtyFlag();

    /**
     * 活动状态
     */
    String getActivity_state();

    void setActivity_state(String activity_state);

    /**
     * 获取 [活动状态]脏标记
     */
    boolean getActivity_stateDirtyFlag();

    /**
     * 条码
     */
    String getBarcode();

    void setBarcode(String barcode);

    /**
     * 获取 [条码]脏标记
     */
    boolean getBarcodeDirtyFlag();

    /**
     * 公司数据库ID
     */
    Integer getPartner_gid();

    void setPartner_gid(Integer partner_gid);

    /**
     * 获取 [公司数据库ID]脏标记
     */
    boolean getPartner_gidDirtyFlag();

    /**
     * 地址类型
     */
    String getType();

    void setType(String type);

    /**
     * 获取 [地址类型]脏标记
     */
    boolean getTypeDirtyFlag();

    /**
     * 税号
     */
    String getVat();

    void setVat(String vat);

    /**
     * 获取 [税号]脏标记
     */
    boolean getVatDirtyFlag();

    /**
     * 采购订单消息
     */
    String getPurchase_warn_msg();

    void setPurchase_warn_msg(String purchase_warn_msg);

    /**
     * 获取 [采购订单消息]脏标记
     */
    boolean getPurchase_warn_msgDirtyFlag();

    /**
     * 便签
     */
    String getComment();

    void setComment(String comment);

    /**
     * 获取 [便签]脏标记
     */
    boolean getCommentDirtyFlag();

    /**
     * 供应商
     */
    String getSupplier();

    void setSupplier(String supplier);

    /**
     * 获取 [供应商]脏标记
     */
    boolean getSupplierDirtyFlag();

    /**
     * 网站meta关键词
     */
    String getWebsite_meta_keywords();

    void setWebsite_meta_keywords(String website_meta_keywords);

    /**
     * 获取 [网站meta关键词]脏标记
     */
    boolean getWebsite_meta_keywordsDirtyFlag();

    /**
     * 关联公司
     */
    Integer getParent_id();

    void setParent_id(Integer parent_id);

    /**
     * 获取 [关联公司]脏标记
     */
    boolean getParent_idDirtyFlag();

    /**
     * 采购订单
     */
    String getPurchase_warn();

    void setPurchase_warn(String purchase_warn);

    /**
     * 获取 [采购订单]脏标记
     */
    boolean getPurchase_warnDirtyFlag();

    /**
     * 激活的合作伙伴
     */
    String getActive_partner();

    void setActive_partner(String active_partner);

    /**
     * 获取 [激活的合作伙伴]脏标记
     */
    boolean getActive_partnerDirtyFlag();

    /**
     * 币种
     */
    Integer getCurrency_id();

    void setCurrency_id(Integer currency_id);

    /**
     * 获取 [币种]脏标记
     */
    boolean getCurrency_idDirtyFlag();

    /**
     * 工业
     */
    Integer getIndustry_id();

    void setIndustry_id(Integer industry_id);

    /**
     * 获取 [工业]脏标记
     */
    boolean getIndustry_idDirtyFlag();

    /**
     * 供应商位置
     */
    Integer getProperty_stock_supplier();

    void setProperty_stock_supplier(Integer property_stock_supplier);

    /**
     * 获取 [供应商位置]脏标记
     */
    boolean getProperty_stock_supplierDirtyFlag();

    /**
     * 付款令牌计数
     */
    Integer getPayment_token_count();

    void setPayment_token_count(Integer payment_token_count);

    /**
     * 获取 [付款令牌计数]脏标记
     */
    boolean getPayment_token_countDirtyFlag();

    /**
     * 前置操作
     */
    String getMessage_needaction();

    void setMessage_needaction(String message_needaction);

    /**
     * 获取 [前置操作]脏标记
     */
    boolean getMessage_needactionDirtyFlag();

    /**
     * 销售员
     */
    Integer getUser_id();

    void setUser_id(Integer user_id);

    /**
     * 获取 [销售员]脏标记
     */
    boolean getUser_idDirtyFlag();

    /**
     * 客户
     */
    String getCustomer();

    void setCustomer(String customer);

    /**
     * 获取 [客户]脏标记
     */
    boolean getCustomerDirtyFlag();

    /**
     * 客户付款条款
     */
    Integer getProperty_payment_term_id();

    void setProperty_payment_term_id(Integer property_payment_term_id);

    /**
     * 获取 [客户付款条款]脏标记
     */
    boolean getProperty_payment_term_idDirtyFlag();

    /**
     * 下一活动类型
     */
    Integer getActivity_type_id();

    void setActivity_type_id(Integer activity_type_id);

    /**
     * 获取 [下一活动类型]脏标记
     */
    boolean getActivity_type_idDirtyFlag();

    /**
     * 合同统计
     */
    Integer getContracts_count();

    void setContracts_count(Integer contracts_count);

    /**
     * 获取 [合同统计]脏标记
     */
    boolean getContracts_countDirtyFlag();

    /**
     * 自己
     */
    Integer getSelf();

    void setSelf(Integer self);

    /**
     * 获取 [自己]脏标记
     */
    boolean getSelfDirtyFlag();

    /**
     * 网站元说明
     */
    String getWebsite_meta_description();

    void setWebsite_meta_description(String website_meta_description);

    /**
     * 获取 [网站元说明]脏标记
     */
    boolean getWebsite_meta_descriptionDirtyFlag();

    /**
     * 图像
     */
    byte[] getImage();

    void setImage(byte[] image);

    /**
     * 获取 [图像]脏标记
     */
    boolean getImageDirtyFlag();

    /**
     * EMail
     */
    String getEmail();

    void setEmail(String email);

    /**
     * 获取 [EMail]脏标记
     */
    boolean getEmailDirtyFlag();

    /**
     * 中等尺寸图像
     */
    byte[] getImage_medium();

    void setImage_medium(byte[] image_medium);

    /**
     * 获取 [中等尺寸图像]脏标记
     */
    boolean getImage_mediumDirtyFlag();

    /**
     * 下一个活动摘要
     */
    String getActivity_summary();

    void setActivity_summary(String activity_summary);

    /**
     * 获取 [下一个活动摘要]脏标记
     */
    boolean getActivity_summaryDirtyFlag();

    /**
     * 应付限额
     */
    Double getDebit_limit();

    void setDebit_limit(Double debit_limit);

    /**
     * 获取 [应付限额]脏标记
     */
    boolean getDebit_limitDirtyFlag();

    /**
     * 国家/地区
     */
    Integer getCountry_id();

    void setCountry_id(Integer country_id);

    /**
     * 获取 [国家/地区]脏标记
     */
    boolean getCountry_idDirtyFlag();

    /**
     * 信用额度
     */
    Double getCredit_limit();

    void setCredit_limit(Double credit_limit);

    /**
     * 获取 [信用额度]脏标记
     */
    boolean getCredit_limitDirtyFlag();

    /**
     * 公司名称实体
     */
    String getCommercial_company_name();

    void setCommercial_company_name(String commercial_company_name);

    /**
     * 获取 [公司名称实体]脏标记
     */
    boolean getCommercial_company_nameDirtyFlag();

    /**
     * 发票消息
     */
    String getInvoice_warn_msg();

    void setInvoice_warn_msg(String invoice_warn_msg);

    /**
     * 获取 [发票消息]脏标记
     */
    boolean getInvoice_warn_msgDirtyFlag();

    /**
     * 已发布
     */
    String getIs_published();

    void setIs_published(String is_published);

    /**
     * 获取 [已发布]脏标记
     */
    boolean getIs_publishedDirtyFlag();

    /**
     * 对此债务人的信任度
     */
    String getTrust();

    void setTrust(String trust);

    /**
     * 获取 [对此债务人的信任度]脏标记
     */
    boolean getTrustDirtyFlag();

    /**
     * 手机
     */
    String getMobile();

    void setMobile(String mobile);

    /**
     * 获取 [手机]脏标记
     */
    boolean getMobileDirtyFlag();

    /**
     * 格式化的邮件
     */
    String getEmail_formatted();

    void setEmail_formatted(String email_formatted);

    /**
     * 获取 [格式化的邮件]脏标记
     */
    boolean getEmail_formattedDirtyFlag();

    /**
     * 公司
     */
    String getIs_company();

    void setIs_company(String is_company);

    /**
     * 获取 [公司]脏标记
     */
    boolean getIs_companyDirtyFlag();

    /**
     * 创建人
     */
    String getCreate_uid_text();

    void setCreate_uid_text(String create_uid_text);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uid_textDirtyFlag();

    /**
     * 销售团队
     */
    Integer getTeam_id();

    void setTeam_id(Integer team_id);

    /**
     * 获取 [销售团队]脏标记
     */
    boolean getTeam_idDirtyFlag();

    /**
     * 黑名单
     */
    String getIs_blacklisted();

    void setIs_blacklisted(String is_blacklisted);

    /**
     * 获取 [黑名单]脏标记
     */
    boolean getIs_blacklistedDirtyFlag();

    /**
     * 银行
     */
    Integer getBank_account_count();

    void setBank_account_count(Integer bank_account_count);

    /**
     * 获取 [银行]脏标记
     */
    boolean getBank_account_countDirtyFlag();

    /**
     * 价格表
     */
    Integer getProperty_product_pricelist();

    void setProperty_product_pricelist(Integer property_product_pricelist);

    /**
     * 获取 [价格表]脏标记
     */
    boolean getProperty_product_pricelistDirtyFlag();

    /**
     * 名称
     */
    String getName();

    void setName(String name);

    /**
     * 获取 [名称]脏标记
     */
    boolean getNameDirtyFlag();

    /**
     * 库存拣货单消息
     */
    String getPicking_warn_msg();

    void setPicking_warn_msg(String picking_warn_msg);

    /**
     * 获取 [库存拣货单消息]脏标记
     */
    boolean getPicking_warn_msgDirtyFlag();

    /**
     * 附件数量
     */
    Integer getMessage_attachment_count();

    void setMessage_attachment_count(Integer message_attachment_count);

    /**
     * 获取 [附件数量]脏标记
     */
    boolean getMessage_attachment_countDirtyFlag();

    /**
     * 网站
     */
    String getWebsite();

    void setWebsite(String website);

    /**
     * 获取 [网站]脏标记
     */
    boolean getWebsiteDirtyFlag();

    /**
     * 电话
     */
    String getPhone();

    void setPhone(String phone);

    /**
     * 获取 [电话]脏标记
     */
    boolean getPhoneDirtyFlag();

    /**
     * 街道 2
     */
    String getStreet2();

    void setStreet2(String street2);

    /**
     * 获取 [街道 2]脏标记
     */
    boolean getStreet2DirtyFlag();

    /**
     * 有未核销的分录
     */
    String getHas_unreconciled_entries();

    void setHas_unreconciled_entries(String has_unreconciled_entries);

    /**
     * 获取 [有未核销的分录]脏标记
     */
    boolean getHas_unreconciled_entriesDirtyFlag();

    /**
     * 注册到期
     */
    Timestamp getSignup_expiration();

    void setSignup_expiration(Timestamp signup_expiration);

    /**
     * 获取 [注册到期]脏标记
     */
    boolean getSignup_expirationDirtyFlag();

    /**
     * 时区
     */
    String getTz();

    void setTz(String tz);

    /**
     * 获取 [时区]脏标记
     */
    boolean getTzDirtyFlag();

    /**
     * 完整地址
     */
    String getContact_address();

    void setContact_address(String contact_address);

    /**
     * 获取 [完整地址]脏标记
     */
    boolean getContact_addressDirtyFlag();

    /**
     * 网站业务伙伴简介
     */
    String getWebsite_short_description();

    void setWebsite_short_description(String website_short_description);

    /**
     * 获取 [网站业务伙伴简介]脏标记
     */
    boolean getWebsite_short_descriptionDirtyFlag();

    /**
     * 共享合作伙伴
     */
    String getPartner_share();

    void setPartner_share(String partner_share);

    /**
     * 获取 [共享合作伙伴]脏标记
     */
    boolean getPartner_shareDirtyFlag();

    /**
     * 公司
     */
    String getCompany_id_text();

    void setCompany_id_text(String company_id_text);

    /**
     * 获取 [公司]脏标记
     */
    boolean getCompany_id_textDirtyFlag();

    /**
     * 颜色索引
     */
    Integer getColor();

    void setColor(Integer color);

    /**
     * 获取 [颜色索引]脏标记
     */
    boolean getColorDirtyFlag();

    /**
     * 邮政编码
     */
    String getZip();

    void setZip(String zip);

    /**
     * 获取 [邮政编码]脏标记
     */
    boolean getZipDirtyFlag();

    /**
     * 销售订单个数
     */
    Integer getSale_order_count();

    void setSale_order_count(Integer sale_order_count);

    /**
     * 获取 [销售订单个数]脏标记
     */
    boolean getSale_order_countDirtyFlag();

    /**
     * 公司类别
     */
    String getCompany_type();

    void setCompany_type(String company_type);

    /**
     * 获取 [公司类别]脏标记
     */
    boolean getCompany_typeDirtyFlag();

    /**
     * 税科目调整
     */
    Integer getProperty_account_position_id();

    void setProperty_account_position_id(Integer property_account_position_id);

    /**
     * 获取 [税科目调整]脏标记
     */
    boolean getProperty_account_position_idDirtyFlag();

    /**
     * SEO优化
     */
    String getIs_seo_optimized();

    void setIs_seo_optimized(String is_seo_optimized);

    /**
     * 获取 [SEO优化]脏标记
     */
    boolean getIs_seo_optimizedDirtyFlag();

    /**
     * 退回
     */
    Integer getMessage_bounce();

    void setMessage_bounce(Integer message_bounce);

    /**
     * 获取 [退回]脏标记
     */
    boolean getMessage_bounceDirtyFlag();

    /**
     * 网站meta标题
     */
    String getWebsite_meta_title();

    void setWebsite_meta_title(String website_meta_title);

    /**
     * 获取 [网站meta标题]脏标记
     */
    boolean getWebsite_meta_titleDirtyFlag();

    /**
     * 小尺寸图像
     */
    byte[] getImage_small();

    void setImage_small(byte[] image_small);

    /**
     * 获取 [小尺寸图像]脏标记
     */
    boolean getImage_smallDirtyFlag();

    /**
     * 供应商货币
     */
    Integer getProperty_purchase_currency_id();

    void setProperty_purchase_currency_id(Integer property_purchase_currency_id);

    /**
     * 获取 [供应商货币]脏标记
     */
    boolean getProperty_purchase_currency_idDirtyFlag();

    /**
     * 采购订单数
     */
    Integer getPurchase_order_count();

    void setPurchase_order_count(Integer purchase_order_count);

    /**
     * 获取 [采购订单数]脏标记
     */
    boolean getPurchase_order_countDirtyFlag();

    /**
     * 网站业务伙伴的详细说明
     */
    String getWebsite_description();

    void setWebsite_description(String website_description);

    /**
     * 获取 [网站业务伙伴的详细说明]脏标记
     */
    boolean getWebsite_descriptionDirtyFlag();

    /**
     * 供应商付款条款
     */
    Integer getProperty_supplier_payment_term_id();

    void setProperty_supplier_payment_term_id(Integer property_supplier_payment_term_id);

    /**
     * 获取 [供应商付款条款]脏标记
     */
    boolean getProperty_supplier_payment_term_idDirtyFlag();

    /**
     * 商业实体
     */
    Integer getCommercial_partner_id();

    void setCommercial_partner_id(Integer commercial_partner_id);

    /**
     * 获取 [商业实体]脏标记
     */
    boolean getCommercial_partner_idDirtyFlag();

    /**
     * 安全联系人别名
     */
    String getAlias_contact();

    void setAlias_contact(String alias_contact);

    /**
     * 获取 [安全联系人别名]脏标记
     */
    boolean getAlias_contactDirtyFlag();

    /**
     * 公司名称
     */
    String getCompany_name();

    void setCompany_name(String company_name);

    /**
     * 获取 [公司名称]脏标记
     */
    boolean getCompany_nameDirtyFlag();

    /**
     * 公司
     */
    Integer getCompany_id();

    void setCompany_id(Integer company_id);

    /**
     * 获取 [公司]脏标记
     */
    boolean getCompany_idDirtyFlag();

    /**
     * 相关的业务伙伴
     */
    Integer getPartner_id();

    void setPartner_id(Integer partner_id);

    /**
     * 获取 [相关的业务伙伴]脏标记
     */
    boolean getPartner_idDirtyFlag();

    /**
     * 别名
     */
    Integer getAlias_id();

    void setAlias_id(Integer alias_id);

    /**
     * 获取 [别名]脏标记
     */
    boolean getAlias_idDirtyFlag();

    /**
     * 用户的销售团队
     */
    Integer getSale_team_id();

    void setSale_team_id(Integer sale_team_id);

    /**
     * 获取 [用户的销售团队]脏标记
     */
    boolean getSale_team_idDirtyFlag();

    /**
     * 最后更新者
     */
    Integer getWrite_uid();

    void setWrite_uid(Integer write_uid);

    /**
     * 获取 [最后更新者]脏标记
     */
    boolean getWrite_uidDirtyFlag();

    /**
     * 创建人
     */
    Integer getCreate_uid();

    void setCreate_uid(Integer create_uid);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uidDirtyFlag();

}
