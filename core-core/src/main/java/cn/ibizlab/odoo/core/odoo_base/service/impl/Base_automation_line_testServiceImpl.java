package cn.ibizlab.odoo.core.odoo_base.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_base.domain.Base_automation_line_test;
import cn.ibizlab.odoo.core.odoo_base.filter.Base_automation_line_testSearchContext;
import cn.ibizlab.odoo.core.odoo_base.service.IBase_automation_line_testService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;



import cn.ibizlab.odoo.core.odoo_base.client.base_automation_line_testOdooClient;
import cn.ibizlab.odoo.core.odoo_base.clientmodel.base_automation_line_testClientModel;

/**
 * 实体[自动化规则行测试] 服务对象接口实现
 */
@Slf4j
@Service
public class Base_automation_line_testServiceImpl implements IBase_automation_line_testService {

    @Autowired
    base_automation_line_testOdooClient base_automation_line_testOdooClient;


    @Override
    public boolean remove(Integer id) {
        base_automation_line_testClientModel clientModel = new base_automation_line_testClientModel();
        clientModel.setId(id);
		base_automation_line_testOdooClient.remove(clientModel);
        return true; 
    }

    public void removeBatch(Collection<Integer> idList){
    }

    @Override
    public boolean update(Base_automation_line_test et) {
        base_automation_line_testClientModel clientModel = convert2Model(et,null);
		base_automation_line_testOdooClient.update(clientModel);
        Base_automation_line_test rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }
    
    public void updateBatch(List<Base_automation_line_test> list){
    }

    @Override
    public Base_automation_line_test get(Integer id) {
        base_automation_line_testClientModel clientModel = new base_automation_line_testClientModel();
        clientModel.setId(id);
		base_automation_line_testOdooClient.get(clientModel);
        Base_automation_line_test et = convert2Domain(clientModel,null);
        if(et==null){
            et=new Base_automation_line_test();
            et.setId(id);
        }
        else{
        }

        return  et;
    }

    @Override
    public boolean create(Base_automation_line_test et) {
        base_automation_line_testClientModel clientModel = convert2Model(et,null);
		base_automation_line_testOdooClient.create(clientModel);
        Base_automation_line_test rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Base_automation_line_test> list){
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Base_automation_line_test> searchDefault(Base_automation_line_testSearchContext context) {
        List<Base_automation_line_test> list = new ArrayList<Base_automation_line_test>();
        Page<base_automation_line_testClientModel> clientModelList = base_automation_line_testOdooClient.search(context);
        for(base_automation_line_testClientModel clientModel : clientModelList){
            list.add( this.convert2Domain(clientModel,null));
        }
        return new PageImpl<Base_automation_line_test>(list, context.getPageable(), clientModelList.getTotalElements());
    }

    public base_automation_line_testClientModel convert2Model(Base_automation_line_test domain , base_automation_line_testClientModel model) {
        if(domain == null)
            return model ;
        if(model==null){
            model = new base_automation_line_testClientModel();

        }
        if((Boolean) domain.getExtensionparams().get("dirtyflagenable")){
            if((Boolean) domain.getExtensionparams().get("display_namedirtyflag"))
                model.setDisplay_name(domain.getDisplayName());
            if((Boolean) domain.getExtensionparams().get("write_datedirtyflag"))
                model.setWrite_date(domain.getWriteDate());
            if((Boolean) domain.getExtensionparams().get("__last_updatedirtyflag"))
                model.set__last_update(domain.getLastUpdate());
            if((Boolean) domain.getExtensionparams().get("namedirtyflag"))
                model.setName(domain.getName());
            if((Boolean) domain.getExtensionparams().get("iddirtyflag"))
                model.setId(domain.getId());
            if((Boolean) domain.getExtensionparams().get("create_datedirtyflag"))
                model.setCreate_date(domain.getCreateDate());
            if((Boolean) domain.getExtensionparams().get("create_uid_textdirtyflag"))
                model.setCreate_uid_text(domain.getCreateUidText());
            if((Boolean) domain.getExtensionparams().get("write_uid_textdirtyflag"))
                model.setWrite_uid_text(domain.getWriteUidText());
            if((Boolean) domain.getExtensionparams().get("user_id_textdirtyflag"))
                model.setUser_id_text(domain.getUserIdText());
            if((Boolean) domain.getExtensionparams().get("lead_id_textdirtyflag"))
                model.setLead_id_text(domain.getLeadIdText());
            if((Boolean) domain.getExtensionparams().get("create_uiddirtyflag"))
                model.setCreate_uid(domain.getCreateUid());
            if((Boolean) domain.getExtensionparams().get("write_uiddirtyflag"))
                model.setWrite_uid(domain.getWriteUid());
            if((Boolean) domain.getExtensionparams().get("lead_iddirtyflag"))
                model.setLead_id(domain.getLeadId());
            if((Boolean) domain.getExtensionparams().get("user_iddirtyflag"))
                model.setUser_id(domain.getUserId());
            domain.getFocusNull().clear();
        }
        return model ;
    }

    public Base_automation_line_test convert2Domain( base_automation_line_testClientModel model ,Base_automation_line_test domain) {
        if(model == null)
            return domain ;
        if(domain == null){
            domain = new Base_automation_line_test();
        }

        if(model.getDisplay_nameDirtyFlag())
            domain.setDisplayName(model.getDisplay_name());
        if(model.getWrite_dateDirtyFlag())
            domain.setWriteDate(model.getWrite_date());
        if(model.get__last_updateDirtyFlag())
            domain.setLastUpdate(model.get__last_update());
        if(model.getNameDirtyFlag())
            domain.setName(model.getName());
        if(model.getIdDirtyFlag())
            domain.setId(model.getId());
        if(model.getCreate_dateDirtyFlag())
            domain.setCreateDate(model.getCreate_date());
        if(model.getCreate_uid_textDirtyFlag())
            domain.setCreateUidText(model.getCreate_uid_text());
        if(model.getWrite_uid_textDirtyFlag())
            domain.setWriteUidText(model.getWrite_uid_text());
        if(model.getUser_id_textDirtyFlag())
            domain.setUserIdText(model.getUser_id_text());
        if(model.getLead_id_textDirtyFlag())
            domain.setLeadIdText(model.getLead_id_text());
        if(model.getCreate_uidDirtyFlag())
            domain.setCreateUid(model.getCreate_uid());
        if(model.getWrite_uidDirtyFlag())
            domain.setWriteUid(model.getWrite_uid());
        if(model.getLead_idDirtyFlag())
            domain.setLeadId(model.getLead_id());
        if(model.getUser_idDirtyFlag())
            domain.setUserId(model.getUser_id());
        return domain ;
    }

}

    



