package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Hr_expense_refuse_wizard;
import cn.ibizlab.odoo.core.odoo_hr.filter.Hr_expense_refuse_wizardSearchContext;

/**
 * 实体 [费用拒绝原因向导] 存储对象
 */
public interface Hr_expense_refuse_wizardRepository extends Repository<Hr_expense_refuse_wizard> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Hr_expense_refuse_wizard> searchDefault(Hr_expense_refuse_wizardSearchContext context);

    Hr_expense_refuse_wizard convert2PO(cn.ibizlab.odoo.core.odoo_hr.domain.Hr_expense_refuse_wizard domain , Hr_expense_refuse_wizard po) ;

    cn.ibizlab.odoo.core.odoo_hr.domain.Hr_expense_refuse_wizard convert2Domain( Hr_expense_refuse_wizard po ,cn.ibizlab.odoo.core.odoo_hr.domain.Hr_expense_refuse_wizard domain) ;

}
