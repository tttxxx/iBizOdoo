package cn.ibizlab.odoo.core.odoo_base.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_base.domain.Res_users;
import cn.ibizlab.odoo.core.odoo_base.filter.Res_usersSearchContext;
import cn.ibizlab.odoo.core.odoo_base.service.IRes_usersService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;



import cn.ibizlab.odoo.core.odoo_base.client.res_usersOdooClient;
import cn.ibizlab.odoo.core.odoo_base.clientmodel.res_usersClientModel;

/**
 * 实体[用户] 服务对象接口实现
 */
@Slf4j
@Service
public class Res_usersServiceImpl implements IRes_usersService {

    @Autowired
    res_usersOdooClient res_usersOdooClient;


    @Override
    public boolean update(Res_users et) {
        res_usersClientModel clientModel = convert2Model(et,null);
		res_usersOdooClient.update(clientModel);
        Res_users rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }
    
    public void updateBatch(List<Res_users> list){
    }

    @Override
    public boolean create(Res_users et) {
        res_usersClientModel clientModel = convert2Model(et,null);
		res_usersOdooClient.create(clientModel);
        Res_users rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Res_users> list){
    }

    @Override
    public Res_users get(Integer id) {
        res_usersClientModel clientModel = new res_usersClientModel();
        clientModel.setId(id);
		res_usersOdooClient.get(clientModel);
        Res_users et = convert2Domain(clientModel,null);
        if(et==null){
            et=new Res_users();
            et.setId(id);
        }
        else{
        }

        return  et;
    }

    @Override
    public boolean remove(Integer id) {
        res_usersClientModel clientModel = new res_usersClientModel();
        clientModel.setId(id);
		res_usersOdooClient.remove(clientModel);
        return true; 
    }

    public void removeBatch(Collection<Integer> idList){
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Res_users> searchDefault(Res_usersSearchContext context) {
        List<Res_users> list = new ArrayList<Res_users>();
        Page<res_usersClientModel> clientModelList = res_usersOdooClient.search(context);
        for(res_usersClientModel clientModel : clientModelList){
            list.add( this.convert2Domain(clientModel,null));
        }
        return new PageImpl<Res_users>(list, context.getPageable(), clientModelList.getTotalElements());
    }

    public res_usersClientModel convert2Model(Res_users domain , res_usersClientModel model) {
        if(domain == null)
            return model ;
        if(model==null){
            model = new res_usersClientModel();

        }
        if((Boolean) domain.getExtensionparams().get("dirtyflagenable")){
            if((Boolean) domain.getExtensionparams().get("is_moderatordirtyflag"))
                model.setIs_moderator(domain.getIsModerator());
            if((Boolean) domain.getExtensionparams().get("resource_idsdirtyflag"))
                model.setResource_ids(domain.getResourceIds());
            if((Boolean) domain.getExtensionparams().get("create_datedirtyflag"))
                model.setCreate_date(domain.getCreateDate());
            if((Boolean) domain.getExtensionparams().get("category_iddirtyflag"))
                model.setCategory_id(domain.getCategoryId());
            if((Boolean) domain.getExtensionparams().get("resource_calendar_iddirtyflag"))
                model.setResource_calendar_id(domain.getResourceCalendarId());
            if((Boolean) domain.getExtensionparams().get("log_idsdirtyflag"))
                model.setLog_ids(domain.getLogIds());
            if((Boolean) domain.getExtensionparams().get("message_idsdirtyflag"))
                model.setMessage_ids(domain.getMessageIds());
            if((Boolean) domain.getExtensionparams().get("pos_security_pindirtyflag"))
                model.setPos_security_pin(domain.getPosSecurityPin());
            if((Boolean) domain.getExtensionparams().get("badge_idsdirtyflag"))
                model.setBadge_ids(domain.getBadgeIds());
            if((Boolean) domain.getExtensionparams().get("write_datedirtyflag"))
                model.setWrite_date(domain.getWriteDate());
            if((Boolean) domain.getExtensionparams().get("iddirtyflag"))
                model.setId(domain.getId());
            if((Boolean) domain.getExtensionparams().get("company_idsdirtyflag"))
                model.setCompany_ids(domain.getCompanyIds());
            if((Boolean) domain.getExtensionparams().get("child_idsdirtyflag"))
                model.setChild_ids(domain.getChildIds());
            if((Boolean) domain.getExtensionparams().get("tz_offsetdirtyflag"))
                model.setTz_offset(domain.getTzOffset());
            if((Boolean) domain.getExtensionparams().get("target_sales_donedirtyflag"))
                model.setTarget_sales_done(domain.getTargetSalesDone());
            if((Boolean) domain.getExtensionparams().get("message_partner_idsdirtyflag"))
                model.setMessage_partner_ids(domain.getMessagePartnerIds());
            if((Boolean) domain.getExtensionparams().get("notification_typedirtyflag"))
                model.setNotification_type(domain.getNotificationType());
            if((Boolean) domain.getExtensionparams().get("activedirtyflag"))
                model.setActive(domain.getActive());
            if((Boolean) domain.getExtensionparams().get("im_statusdirtyflag"))
                model.setIm_status(domain.getImStatus());
            if((Boolean) domain.getExtensionparams().get("karmadirtyflag"))
                model.setKarma(domain.getKarma());
            if((Boolean) domain.getExtensionparams().get("website_iddirtyflag"))
                model.setWebsite_id(domain.getWebsiteId());
            if((Boolean) domain.getExtensionparams().get("activity_idsdirtyflag"))
                model.setActivity_ids(domain.getActivityIds());
            if((Boolean) domain.getExtensionparams().get("gold_badgedirtyflag"))
                model.setGold_badge(domain.getGoldBadge());
            if((Boolean) domain.getExtensionparams().get("employee_idsdirtyflag"))
                model.setEmployee_ids(domain.getEmployeeIds());
            if((Boolean) domain.getExtensionparams().get("statedirtyflag"))
                model.setState(domain.getState());
            if((Boolean) domain.getExtensionparams().get("moderation_channel_idsdirtyflag"))
                model.setModeration_channel_ids(domain.getModerationChannelIds());
            if((Boolean) domain.getExtensionparams().get("message_channel_idsdirtyflag"))
                model.setMessage_channel_ids(domain.getMessageChannelIds());
            if((Boolean) domain.getExtensionparams().get("silver_badgedirtyflag"))
                model.setSilver_badge(domain.getSilverBadge());
            if((Boolean) domain.getExtensionparams().get("payment_token_idsdirtyflag"))
                model.setPayment_token_ids(domain.getPaymentTokenIds());
            if((Boolean) domain.getExtensionparams().get("display_namedirtyflag"))
                model.setDisplay_name(domain.getDisplayName());
            if((Boolean) domain.getExtensionparams().get("companies_countdirtyflag"))
                model.setCompanies_count(domain.getCompaniesCount());
            if((Boolean) domain.getExtensionparams().get("target_sales_invoiceddirtyflag"))
                model.setTarget_sales_invoiced(domain.getTargetSalesInvoiced());
            if((Boolean) domain.getExtensionparams().get("message_follower_idsdirtyflag"))
                model.setMessage_follower_ids(domain.getMessageFollowerIds());
            if((Boolean) domain.getExtensionparams().get("login_datedirtyflag"))
                model.setLogin_date(domain.getLoginDate());
            if((Boolean) domain.getExtensionparams().get("channel_idsdirtyflag"))
                model.setChannel_ids(domain.getChannelIds());
            if((Boolean) domain.getExtensionparams().get("groups_iddirtyflag"))
                model.setGroups_id(domain.getGroupsId());
            if((Boolean) domain.getExtensionparams().get("sharedirtyflag"))
                model.setShare(domain.getShare());
            if((Boolean) domain.getExtensionparams().get("bank_idsdirtyflag"))
                model.setBank_ids(domain.getBankIds());
            if((Boolean) domain.getExtensionparams().get("sale_order_idsdirtyflag"))
                model.setSale_order_ids(domain.getSaleOrderIds());
            if((Boolean) domain.getExtensionparams().get("new_passworddirtyflag"))
                model.setNew_password(domain.getNewPassword());
            if((Boolean) domain.getExtensionparams().get("odoobot_statedirtyflag"))
                model.setOdoobot_state(domain.getOdoobotState());
            if((Boolean) domain.getExtensionparams().get("ref_company_idsdirtyflag"))
                model.setRef_company_ids(domain.getRefCompanyIds());
            if((Boolean) domain.getExtensionparams().get("passworddirtyflag"))
                model.setPassword(domain.getPassword());
            if((Boolean) domain.getExtensionparams().get("bronze_badgedirtyflag"))
                model.setBronze_badge(domain.getBronzeBadge());
            if((Boolean) domain.getExtensionparams().get("meeting_idsdirtyflag"))
                model.setMeeting_ids(domain.getMeetingIds());
            if((Boolean) domain.getExtensionparams().get("__last_updatedirtyflag"))
                model.set__last_update(domain.getLastUpdate());
            if((Boolean) domain.getExtensionparams().get("forum_waiting_posts_countdirtyflag"))
                model.setForum_waiting_posts_count(domain.getForumWaitingPostsCount());
            if((Boolean) domain.getExtensionparams().get("goal_idsdirtyflag"))
                model.setGoal_ids(domain.getGoalIds());
            if((Boolean) domain.getExtensionparams().get("target_sales_wondirtyflag"))
                model.setTarget_sales_won(domain.getTargetSalesWon());
            if((Boolean) domain.getExtensionparams().get("website_message_idsdirtyflag"))
                model.setWebsite_message_ids(domain.getWebsiteMessageIds());
            if((Boolean) domain.getExtensionparams().get("action_iddirtyflag"))
                model.setAction_id(domain.getActionId());
            if((Boolean) domain.getExtensionparams().get("logindirtyflag"))
                model.setLogin(domain.getLogin());
            if((Boolean) domain.getExtensionparams().get("contract_idsdirtyflag"))
                model.setContract_ids(domain.getContractIds());
            if((Boolean) domain.getExtensionparams().get("moderation_counterdirtyflag"))
                model.setModeration_counter(domain.getModerationCounter());
            if((Boolean) domain.getExtensionparams().get("signaturedirtyflag"))
                model.setSignature(domain.getSignature());
            if((Boolean) domain.getExtensionparams().get("user_idsdirtyflag"))
                model.setUser_ids(domain.getUserIds());
            if((Boolean) domain.getExtensionparams().get("opportunity_idsdirtyflag"))
                model.setOpportunity_ids(domain.getOpportunityIds());
            if((Boolean) domain.getExtensionparams().get("task_idsdirtyflag"))
                model.setTask_ids(domain.getTaskIds());
            if((Boolean) domain.getExtensionparams().get("invoice_idsdirtyflag"))
                model.setInvoice_ids(domain.getInvoiceIds());
            if((Boolean) domain.getExtensionparams().get("refdirtyflag"))
                model.setRef(domain.getRef());
            if((Boolean) domain.getExtensionparams().get("message_has_error_counterdirtyflag"))
                model.setMessage_has_error_counter(domain.getMessageHasErrorCounter());
            if((Boolean) domain.getExtensionparams().get("last_website_so_iddirtyflag"))
                model.setLast_website_so_id(domain.getLastWebsiteSoId());
            if((Boolean) domain.getExtensionparams().get("datedirtyflag"))
                model.setDate(domain.getDate());
            if((Boolean) domain.getExtensionparams().get("message_is_followerdirtyflag"))
                model.setMessage_is_follower(domain.getMessageIsFollower());
            if((Boolean) domain.getExtensionparams().get("property_stock_customerdirtyflag"))
                model.setProperty_stock_customer(domain.getPropertyStockCustomer());
            if((Boolean) domain.getExtensionparams().get("last_time_entries_checkeddirtyflag"))
                model.setLast_time_entries_checked(domain.getLastTimeEntriesChecked());
            if((Boolean) domain.getExtensionparams().get("langdirtyflag"))
                model.setLang(domain.getLang());
            if((Boolean) domain.getExtensionparams().get("sale_warndirtyflag"))
                model.setSale_warn(domain.getSaleWarn());
            if((Boolean) domain.getExtensionparams().get("meeting_countdirtyflag"))
                model.setMeeting_count(domain.getMeetingCount());
            if((Boolean) domain.getExtensionparams().get("streetdirtyflag"))
                model.setStreet(domain.getStreet());
            if((Boolean) domain.getExtensionparams().get("invoice_warndirtyflag"))
                model.setInvoice_warn(domain.getInvoiceWarn());
            if((Boolean) domain.getExtensionparams().get("signup_tokendirtyflag"))
                model.setSignup_token(domain.getSignupToken());
            if((Boolean) domain.getExtensionparams().get("task_countdirtyflag"))
                model.setTask_count(domain.getTaskCount());
            if((Boolean) domain.getExtensionparams().get("signup_validdirtyflag"))
                model.setSignup_valid(domain.getSignupValid());
            if((Boolean) domain.getExtensionparams().get("message_unread_counterdirtyflag"))
                model.setMessage_unread_counter(domain.getMessageUnreadCounter());
            if((Boolean) domain.getExtensionparams().get("signup_typedirtyflag"))
                model.setSignup_type(domain.getSignupType());
            if((Boolean) domain.getExtensionparams().get("property_account_receivable_iddirtyflag"))
                model.setProperty_account_receivable_id(domain.getPropertyAccountReceivableId());
            if((Boolean) domain.getExtensionparams().get("website_meta_og_imgdirtyflag"))
                model.setWebsite_meta_og_img(domain.getWebsiteMetaOgImg());
            if((Boolean) domain.getExtensionparams().get("event_countdirtyflag"))
                model.setEvent_count(domain.getEventCount());
            if((Boolean) domain.getExtensionparams().get("journal_item_countdirtyflag"))
                model.setJournal_item_count(domain.getJournalItemCount());
            if((Boolean) domain.getExtensionparams().get("parent_namedirtyflag"))
                model.setParent_name(domain.getParentName());
            if((Boolean) domain.getExtensionparams().get("sale_team_id_textdirtyflag"))
                model.setSale_team_id_text(domain.getSaleTeamIdText());
            if((Boolean) domain.getExtensionparams().get("creditdirtyflag"))
                model.setCredit(domain.getCredit());
            if((Boolean) domain.getExtensionparams().get("opportunity_countdirtyflag"))
                model.setOpportunity_count(domain.getOpportunityCount());
            if((Boolean) domain.getExtensionparams().get("signup_urldirtyflag"))
                model.setSignup_url(domain.getSignupUrl());
            if((Boolean) domain.getExtensionparams().get("property_account_payable_iddirtyflag"))
                model.setProperty_account_payable_id(domain.getPropertyAccountPayableId());
            if((Boolean) domain.getExtensionparams().get("state_iddirtyflag"))
                model.setState_id(domain.getStateId());
            if((Boolean) domain.getExtensionparams().get("message_has_errordirtyflag"))
                model.setMessage_has_error(domain.getMessageHasError());
            if((Boolean) domain.getExtensionparams().get("sale_warn_msgdirtyflag"))
                model.setSale_warn_msg(domain.getSaleWarnMsg());
            if((Boolean) domain.getExtensionparams().get("website_publisheddirtyflag"))
                model.setWebsite_published(domain.getWebsitePublished());
            if((Boolean) domain.getExtensionparams().get("total_invoiceddirtyflag"))
                model.setTotal_invoiced(domain.getTotalInvoiced());
            if((Boolean) domain.getExtensionparams().get("debitdirtyflag"))
                model.setDebit(domain.getDebit());
            if((Boolean) domain.getExtensionparams().get("pos_order_countdirtyflag"))
                model.setPos_order_count(domain.getPosOrderCount());
            if((Boolean) domain.getExtensionparams().get("write_uid_textdirtyflag"))
                model.setWrite_uid_text(domain.getWriteUidText());
            if((Boolean) domain.getExtensionparams().get("titledirtyflag"))
                model.setTitle(domain.getTitle());
            if((Boolean) domain.getExtensionparams().get("message_unreaddirtyflag"))
                model.setMessage_unread(domain.getMessageUnread());
            if((Boolean) domain.getExtensionparams().get("supplier_invoice_countdirtyflag"))
                model.setSupplier_invoice_count(domain.getSupplierInvoiceCount());
            if((Boolean) domain.getExtensionparams().get("citydirtyflag"))
                model.setCity(domain.getCity());
            if((Boolean) domain.getExtensionparams().get("picking_warndirtyflag"))
                model.setPicking_warn(domain.getPickingWarn());
            if((Boolean) domain.getExtensionparams().get("message_main_attachment_iddirtyflag"))
                model.setMessage_main_attachment_id(domain.getMessageMainAttachmentId());
            if((Boolean) domain.getExtensionparams().get("activity_user_iddirtyflag"))
                model.setActivity_user_id(domain.getActivityUserId());
            if((Boolean) domain.getExtensionparams().get("additional_infodirtyflag"))
                model.setAdditional_info(domain.getAdditionalInfo());
            if((Boolean) domain.getExtensionparams().get("ibizfunctiondirtyflag"))
                model.setIbizfunction(domain.getIbizfunction());
            if((Boolean) domain.getExtensionparams().get("website_urldirtyflag"))
                model.setWebsite_url(domain.getWebsiteUrl());
            if((Boolean) domain.getExtensionparams().get("calendar_last_notif_ackdirtyflag"))
                model.setCalendar_last_notif_ack(domain.getCalendarLastNotifAck());
            if((Boolean) domain.getExtensionparams().get("message_needaction_counterdirtyflag"))
                model.setMessage_needaction_counter(domain.getMessageNeedactionCounter());
            if((Boolean) domain.getExtensionparams().get("activity_date_deadlinedirtyflag"))
                model.setActivity_date_deadline(domain.getActivityDateDeadline());
            if((Boolean) domain.getExtensionparams().get("employeedirtyflag"))
                model.setEmployee(domain.getEmployee());
            if((Boolean) domain.getExtensionparams().get("activity_statedirtyflag"))
                model.setActivity_state(domain.getActivityState());
            if((Boolean) domain.getExtensionparams().get("barcodedirtyflag"))
                model.setBarcode(domain.getBarcode());
            if((Boolean) domain.getExtensionparams().get("partner_giddirtyflag"))
                model.setPartner_gid(domain.getPartnerGid());
            if((Boolean) domain.getExtensionparams().get("typedirtyflag"))
                model.setType(domain.getType());
            if((Boolean) domain.getExtensionparams().get("vatdirtyflag"))
                model.setVat(domain.getVat());
            if((Boolean) domain.getExtensionparams().get("purchase_warn_msgdirtyflag"))
                model.setPurchase_warn_msg(domain.getPurchaseWarnMsg());
            if((Boolean) domain.getExtensionparams().get("commentdirtyflag"))
                model.setComment(domain.getComment());
            if((Boolean) domain.getExtensionparams().get("supplierdirtyflag"))
                model.setSupplier(domain.getSupplier());
            if((Boolean) domain.getExtensionparams().get("website_meta_keywordsdirtyflag"))
                model.setWebsite_meta_keywords(domain.getWebsiteMetaKeywords());
            if((Boolean) domain.getExtensionparams().get("parent_iddirtyflag"))
                model.setParent_id(domain.getParentId());
            if((Boolean) domain.getExtensionparams().get("purchase_warndirtyflag"))
                model.setPurchase_warn(domain.getPurchaseWarn());
            if((Boolean) domain.getExtensionparams().get("active_partnerdirtyflag"))
                model.setActive_partner(domain.getActivePartner());
            if((Boolean) domain.getExtensionparams().get("currency_iddirtyflag"))
                model.setCurrency_id(domain.getCurrencyId());
            if((Boolean) domain.getExtensionparams().get("industry_iddirtyflag"))
                model.setIndustry_id(domain.getIndustryId());
            if((Boolean) domain.getExtensionparams().get("property_stock_supplierdirtyflag"))
                model.setProperty_stock_supplier(domain.getPropertyStockSupplier());
            if((Boolean) domain.getExtensionparams().get("payment_token_countdirtyflag"))
                model.setPayment_token_count(domain.getPaymentTokenCount());
            if((Boolean) domain.getExtensionparams().get("message_needactiondirtyflag"))
                model.setMessage_needaction(domain.getMessageNeedaction());
            if((Boolean) domain.getExtensionparams().get("user_iddirtyflag"))
                model.setUser_id(domain.getUserId());
            if((Boolean) domain.getExtensionparams().get("customerdirtyflag"))
                model.setCustomer(domain.getCustomer());
            if((Boolean) domain.getExtensionparams().get("property_payment_term_iddirtyflag"))
                model.setProperty_payment_term_id(domain.getPropertyPaymentTermId());
            if((Boolean) domain.getExtensionparams().get("activity_type_iddirtyflag"))
                model.setActivity_type_id(domain.getActivityTypeId());
            if((Boolean) domain.getExtensionparams().get("contracts_countdirtyflag"))
                model.setContracts_count(domain.getContractsCount());
            if((Boolean) domain.getExtensionparams().get("selfdirtyflag"))
                model.setSelf(domain.getSelf());
            if((Boolean) domain.getExtensionparams().get("website_meta_descriptiondirtyflag"))
                model.setWebsite_meta_description(domain.getWebsiteMetaDescription());
            if((Boolean) domain.getExtensionparams().get("imagedirtyflag"))
                model.setImage(domain.getImage());
            if((Boolean) domain.getExtensionparams().get("emaildirtyflag"))
                model.setEmail(domain.getEmail());
            if((Boolean) domain.getExtensionparams().get("image_mediumdirtyflag"))
                model.setImage_medium(domain.getImageMedium());
            if((Boolean) domain.getExtensionparams().get("activity_summarydirtyflag"))
                model.setActivity_summary(domain.getActivitySummary());
            if((Boolean) domain.getExtensionparams().get("debit_limitdirtyflag"))
                model.setDebit_limit(domain.getDebitLimit());
            if((Boolean) domain.getExtensionparams().get("country_iddirtyflag"))
                model.setCountry_id(domain.getCountryId());
            if((Boolean) domain.getExtensionparams().get("credit_limitdirtyflag"))
                model.setCredit_limit(domain.getCreditLimit());
            if((Boolean) domain.getExtensionparams().get("commercial_company_namedirtyflag"))
                model.setCommercial_company_name(domain.getCommercialCompanyName());
            if((Boolean) domain.getExtensionparams().get("invoice_warn_msgdirtyflag"))
                model.setInvoice_warn_msg(domain.getInvoiceWarnMsg());
            if((Boolean) domain.getExtensionparams().get("is_publisheddirtyflag"))
                model.setIs_published(domain.getIsPublished());
            if((Boolean) domain.getExtensionparams().get("trustdirtyflag"))
                model.setTrust(domain.getTrust());
            if((Boolean) domain.getExtensionparams().get("mobiledirtyflag"))
                model.setMobile(domain.getMobile());
            if((Boolean) domain.getExtensionparams().get("email_formatteddirtyflag"))
                model.setEmail_formatted(domain.getEmailFormatted());
            if((Boolean) domain.getExtensionparams().get("is_companydirtyflag"))
                model.setIs_company(domain.getIsCompany());
            if((Boolean) domain.getExtensionparams().get("create_uid_textdirtyflag"))
                model.setCreate_uid_text(domain.getCreateUidText());
            if((Boolean) domain.getExtensionparams().get("team_iddirtyflag"))
                model.setTeam_id(domain.getTeamId());
            if((Boolean) domain.getExtensionparams().get("is_blacklisteddirtyflag"))
                model.setIs_blacklisted(domain.getIsBlacklisted());
            if((Boolean) domain.getExtensionparams().get("bank_account_countdirtyflag"))
                model.setBank_account_count(domain.getBankAccountCount());
            if((Boolean) domain.getExtensionparams().get("property_product_pricelistdirtyflag"))
                model.setProperty_product_pricelist(domain.getPropertyProductPricelist());
            if((Boolean) domain.getExtensionparams().get("namedirtyflag"))
                model.setName(domain.getName());
            if((Boolean) domain.getExtensionparams().get("picking_warn_msgdirtyflag"))
                model.setPicking_warn_msg(domain.getPickingWarnMsg());
            if((Boolean) domain.getExtensionparams().get("message_attachment_countdirtyflag"))
                model.setMessage_attachment_count(domain.getMessageAttachmentCount());
            if((Boolean) domain.getExtensionparams().get("websitedirtyflag"))
                model.setWebsite(domain.getWebsite());
            if((Boolean) domain.getExtensionparams().get("phonedirtyflag"))
                model.setPhone(domain.getPhone());
            if((Boolean) domain.getExtensionparams().get("street2dirtyflag"))
                model.setStreet2(domain.getStreet2());
            if((Boolean) domain.getExtensionparams().get("has_unreconciled_entriesdirtyflag"))
                model.setHas_unreconciled_entries(domain.getHasUnreconciledEntries());
            if((Boolean) domain.getExtensionparams().get("signup_expirationdirtyflag"))
                model.setSignup_expiration(domain.getSignupExpiration());
            if((Boolean) domain.getExtensionparams().get("tzdirtyflag"))
                model.setTz(domain.getTz());
            if((Boolean) domain.getExtensionparams().get("contact_addressdirtyflag"))
                model.setContact_address(domain.getContactAddress());
            if((Boolean) domain.getExtensionparams().get("website_short_descriptiondirtyflag"))
                model.setWebsite_short_description(domain.getWebsiteShortDescription());
            if((Boolean) domain.getExtensionparams().get("partner_sharedirtyflag"))
                model.setPartner_share(domain.getPartnerShare());
            if((Boolean) domain.getExtensionparams().get("company_id_textdirtyflag"))
                model.setCompany_id_text(domain.getCompanyIdText());
            if((Boolean) domain.getExtensionparams().get("colordirtyflag"))
                model.setColor(domain.getColor());
            if((Boolean) domain.getExtensionparams().get("zipdirtyflag"))
                model.setZip(domain.getZip());
            if((Boolean) domain.getExtensionparams().get("sale_order_countdirtyflag"))
                model.setSale_order_count(domain.getSaleOrderCount());
            if((Boolean) domain.getExtensionparams().get("company_typedirtyflag"))
                model.setCompany_type(domain.getCompanyType());
            if((Boolean) domain.getExtensionparams().get("property_account_position_iddirtyflag"))
                model.setProperty_account_position_id(domain.getPropertyAccountPositionId());
            if((Boolean) domain.getExtensionparams().get("is_seo_optimizeddirtyflag"))
                model.setIs_seo_optimized(domain.getIsSeoOptimized());
            if((Boolean) domain.getExtensionparams().get("message_bouncedirtyflag"))
                model.setMessage_bounce(domain.getMessageBounce());
            if((Boolean) domain.getExtensionparams().get("website_meta_titledirtyflag"))
                model.setWebsite_meta_title(domain.getWebsiteMetaTitle());
            if((Boolean) domain.getExtensionparams().get("image_smalldirtyflag"))
                model.setImage_small(domain.getImageSmall());
            if((Boolean) domain.getExtensionparams().get("property_purchase_currency_iddirtyflag"))
                model.setProperty_purchase_currency_id(domain.getPropertyPurchaseCurrencyId());
            if((Boolean) domain.getExtensionparams().get("purchase_order_countdirtyflag"))
                model.setPurchase_order_count(domain.getPurchaseOrderCount());
            if((Boolean) domain.getExtensionparams().get("website_descriptiondirtyflag"))
                model.setWebsite_description(domain.getWebsiteDescription());
            if((Boolean) domain.getExtensionparams().get("property_supplier_payment_term_iddirtyflag"))
                model.setProperty_supplier_payment_term_id(domain.getPropertySupplierPaymentTermId());
            if((Boolean) domain.getExtensionparams().get("commercial_partner_iddirtyflag"))
                model.setCommercial_partner_id(domain.getCommercialPartnerId());
            if((Boolean) domain.getExtensionparams().get("alias_contactdirtyflag"))
                model.setAlias_contact(domain.getAliasContact());
            if((Boolean) domain.getExtensionparams().get("company_namedirtyflag"))
                model.setCompany_name(domain.getCompanyName());
            if((Boolean) domain.getExtensionparams().get("company_iddirtyflag"))
                model.setCompany_id(domain.getCompanyId());
            if((Boolean) domain.getExtensionparams().get("partner_iddirtyflag"))
                model.setPartner_id(domain.getPartnerId());
            if((Boolean) domain.getExtensionparams().get("alias_iddirtyflag"))
                model.setAlias_id(domain.getAliasId());
            if((Boolean) domain.getExtensionparams().get("sale_team_iddirtyflag"))
                model.setSale_team_id(domain.getSaleTeamId());
            if((Boolean) domain.getExtensionparams().get("write_uiddirtyflag"))
                model.setWrite_uid(domain.getWriteUid());
            if((Boolean) domain.getExtensionparams().get("create_uiddirtyflag"))
                model.setCreate_uid(domain.getCreateUid());
            domain.getFocusNull().clear();
        }
        return model ;
    }

    public Res_users convert2Domain( res_usersClientModel model ,Res_users domain) {
        if(model == null)
            return domain ;
        if(domain == null){
            domain = new Res_users();
        }

        if(model.getIs_moderatorDirtyFlag())
            domain.setIsModerator(model.getIs_moderator());
        if(model.getResource_idsDirtyFlag())
            domain.setResourceIds(model.getResource_ids());
        if(model.getCreate_dateDirtyFlag())
            domain.setCreateDate(model.getCreate_date());
        if(model.getCategory_idDirtyFlag())
            domain.setCategoryId(model.getCategory_id());
        if(model.getResource_calendar_idDirtyFlag())
            domain.setResourceCalendarId(model.getResource_calendar_id());
        if(model.getLog_idsDirtyFlag())
            domain.setLogIds(model.getLog_ids());
        if(model.getMessage_idsDirtyFlag())
            domain.setMessageIds(model.getMessage_ids());
        if(model.getPos_security_pinDirtyFlag())
            domain.setPosSecurityPin(model.getPos_security_pin());
        if(model.getBadge_idsDirtyFlag())
            domain.setBadgeIds(model.getBadge_ids());
        if(model.getWrite_dateDirtyFlag())
            domain.setWriteDate(model.getWrite_date());
        if(model.getIdDirtyFlag())
            domain.setId(model.getId());
        if(model.getCompany_idsDirtyFlag())
            domain.setCompanyIds(model.getCompany_ids());
        if(model.getChild_idsDirtyFlag())
            domain.setChildIds(model.getChild_ids());
        if(model.getTz_offsetDirtyFlag())
            domain.setTzOffset(model.getTz_offset());
        if(model.getTarget_sales_doneDirtyFlag())
            domain.setTargetSalesDone(model.getTarget_sales_done());
        if(model.getMessage_partner_idsDirtyFlag())
            domain.setMessagePartnerIds(model.getMessage_partner_ids());
        if(model.getNotification_typeDirtyFlag())
            domain.setNotificationType(model.getNotification_type());
        if(model.getActiveDirtyFlag())
            domain.setActive(model.getActive());
        if(model.getIm_statusDirtyFlag())
            domain.setImStatus(model.getIm_status());
        if(model.getKarmaDirtyFlag())
            domain.setKarma(model.getKarma());
        if(model.getWebsite_idDirtyFlag())
            domain.setWebsiteId(model.getWebsite_id());
        if(model.getActivity_idsDirtyFlag())
            domain.setActivityIds(model.getActivity_ids());
        if(model.getGold_badgeDirtyFlag())
            domain.setGoldBadge(model.getGold_badge());
        if(model.getEmployee_idsDirtyFlag())
            domain.setEmployeeIds(model.getEmployee_ids());
        if(model.getStateDirtyFlag())
            domain.setState(model.getState());
        if(model.getModeration_channel_idsDirtyFlag())
            domain.setModerationChannelIds(model.getModeration_channel_ids());
        if(model.getMessage_channel_idsDirtyFlag())
            domain.setMessageChannelIds(model.getMessage_channel_ids());
        if(model.getSilver_badgeDirtyFlag())
            domain.setSilverBadge(model.getSilver_badge());
        if(model.getPayment_token_idsDirtyFlag())
            domain.setPaymentTokenIds(model.getPayment_token_ids());
        if(model.getDisplay_nameDirtyFlag())
            domain.setDisplayName(model.getDisplay_name());
        if(model.getCompanies_countDirtyFlag())
            domain.setCompaniesCount(model.getCompanies_count());
        if(model.getTarget_sales_invoicedDirtyFlag())
            domain.setTargetSalesInvoiced(model.getTarget_sales_invoiced());
        if(model.getMessage_follower_idsDirtyFlag())
            domain.setMessageFollowerIds(model.getMessage_follower_ids());
        if(model.getLogin_dateDirtyFlag())
            domain.setLoginDate(model.getLogin_date());
        if(model.getChannel_idsDirtyFlag())
            domain.setChannelIds(model.getChannel_ids());
        if(model.getGroups_idDirtyFlag())
            domain.setGroupsId(model.getGroups_id());
        if(model.getShareDirtyFlag())
            domain.setShare(model.getShare());
        if(model.getBank_idsDirtyFlag())
            domain.setBankIds(model.getBank_ids());
        if(model.getSale_order_idsDirtyFlag())
            domain.setSaleOrderIds(model.getSale_order_ids());
        if(model.getNew_passwordDirtyFlag())
            domain.setNewPassword(model.getNew_password());
        if(model.getOdoobot_stateDirtyFlag())
            domain.setOdoobotState(model.getOdoobot_state());
        if(model.getRef_company_idsDirtyFlag())
            domain.setRefCompanyIds(model.getRef_company_ids());
        if(model.getPasswordDirtyFlag())
            domain.setPassword(model.getPassword());
        if(model.getBronze_badgeDirtyFlag())
            domain.setBronzeBadge(model.getBronze_badge());
        if(model.getMeeting_idsDirtyFlag())
            domain.setMeetingIds(model.getMeeting_ids());
        if(model.get__last_updateDirtyFlag())
            domain.setLastUpdate(model.get__last_update());
        if(model.getForum_waiting_posts_countDirtyFlag())
            domain.setForumWaitingPostsCount(model.getForum_waiting_posts_count());
        if(model.getGoal_idsDirtyFlag())
            domain.setGoalIds(model.getGoal_ids());
        if(model.getTarget_sales_wonDirtyFlag())
            domain.setTargetSalesWon(model.getTarget_sales_won());
        if(model.getWebsite_message_idsDirtyFlag())
            domain.setWebsiteMessageIds(model.getWebsite_message_ids());
        if(model.getAction_idDirtyFlag())
            domain.setActionId(model.getAction_id());
        if(model.getLoginDirtyFlag())
            domain.setLogin(model.getLogin());
        if(model.getContract_idsDirtyFlag())
            domain.setContractIds(model.getContract_ids());
        if(model.getModeration_counterDirtyFlag())
            domain.setModerationCounter(model.getModeration_counter());
        if(model.getSignatureDirtyFlag())
            domain.setSignature(model.getSignature());
        if(model.getUser_idsDirtyFlag())
            domain.setUserIds(model.getUser_ids());
        if(model.getOpportunity_idsDirtyFlag())
            domain.setOpportunityIds(model.getOpportunity_ids());
        if(model.getTask_idsDirtyFlag())
            domain.setTaskIds(model.getTask_ids());
        if(model.getInvoice_idsDirtyFlag())
            domain.setInvoiceIds(model.getInvoice_ids());
        if(model.getRefDirtyFlag())
            domain.setRef(model.getRef());
        if(model.getMessage_has_error_counterDirtyFlag())
            domain.setMessageHasErrorCounter(model.getMessage_has_error_counter());
        if(model.getLast_website_so_idDirtyFlag())
            domain.setLastWebsiteSoId(model.getLast_website_so_id());
        if(model.getDateDirtyFlag())
            domain.setDate(model.getDate());
        if(model.getMessage_is_followerDirtyFlag())
            domain.setMessageIsFollower(model.getMessage_is_follower());
        if(model.getProperty_stock_customerDirtyFlag())
            domain.setPropertyStockCustomer(model.getProperty_stock_customer());
        if(model.getLast_time_entries_checkedDirtyFlag())
            domain.setLastTimeEntriesChecked(model.getLast_time_entries_checked());
        if(model.getLangDirtyFlag())
            domain.setLang(model.getLang());
        if(model.getSale_warnDirtyFlag())
            domain.setSaleWarn(model.getSale_warn());
        if(model.getMeeting_countDirtyFlag())
            domain.setMeetingCount(model.getMeeting_count());
        if(model.getStreetDirtyFlag())
            domain.setStreet(model.getStreet());
        if(model.getInvoice_warnDirtyFlag())
            domain.setInvoiceWarn(model.getInvoice_warn());
        if(model.getSignup_tokenDirtyFlag())
            domain.setSignupToken(model.getSignup_token());
        if(model.getTask_countDirtyFlag())
            domain.setTaskCount(model.getTask_count());
        if(model.getSignup_validDirtyFlag())
            domain.setSignupValid(model.getSignup_valid());
        if(model.getMessage_unread_counterDirtyFlag())
            domain.setMessageUnreadCounter(model.getMessage_unread_counter());
        if(model.getSignup_typeDirtyFlag())
            domain.setSignupType(model.getSignup_type());
        if(model.getProperty_account_receivable_idDirtyFlag())
            domain.setPropertyAccountReceivableId(model.getProperty_account_receivable_id());
        if(model.getWebsite_meta_og_imgDirtyFlag())
            domain.setWebsiteMetaOgImg(model.getWebsite_meta_og_img());
        if(model.getEvent_countDirtyFlag())
            domain.setEventCount(model.getEvent_count());
        if(model.getJournal_item_countDirtyFlag())
            domain.setJournalItemCount(model.getJournal_item_count());
        if(model.getParent_nameDirtyFlag())
            domain.setParentName(model.getParent_name());
        if(model.getSale_team_id_textDirtyFlag())
            domain.setSaleTeamIdText(model.getSale_team_id_text());
        if(model.getCreditDirtyFlag())
            domain.setCredit(model.getCredit());
        if(model.getOpportunity_countDirtyFlag())
            domain.setOpportunityCount(model.getOpportunity_count());
        if(model.getSignup_urlDirtyFlag())
            domain.setSignupUrl(model.getSignup_url());
        if(model.getProperty_account_payable_idDirtyFlag())
            domain.setPropertyAccountPayableId(model.getProperty_account_payable_id());
        if(model.getState_idDirtyFlag())
            domain.setStateId(model.getState_id());
        if(model.getMessage_has_errorDirtyFlag())
            domain.setMessageHasError(model.getMessage_has_error());
        if(model.getSale_warn_msgDirtyFlag())
            domain.setSaleWarnMsg(model.getSale_warn_msg());
        if(model.getWebsite_publishedDirtyFlag())
            domain.setWebsitePublished(model.getWebsite_published());
        if(model.getTotal_invoicedDirtyFlag())
            domain.setTotalInvoiced(model.getTotal_invoiced());
        if(model.getDebitDirtyFlag())
            domain.setDebit(model.getDebit());
        if(model.getPos_order_countDirtyFlag())
            domain.setPosOrderCount(model.getPos_order_count());
        if(model.getWrite_uid_textDirtyFlag())
            domain.setWriteUidText(model.getWrite_uid_text());
        if(model.getTitleDirtyFlag())
            domain.setTitle(model.getTitle());
        if(model.getMessage_unreadDirtyFlag())
            domain.setMessageUnread(model.getMessage_unread());
        if(model.getSupplier_invoice_countDirtyFlag())
            domain.setSupplierInvoiceCount(model.getSupplier_invoice_count());
        if(model.getCityDirtyFlag())
            domain.setCity(model.getCity());
        if(model.getPicking_warnDirtyFlag())
            domain.setPickingWarn(model.getPicking_warn());
        if(model.getMessage_main_attachment_idDirtyFlag())
            domain.setMessageMainAttachmentId(model.getMessage_main_attachment_id());
        if(model.getActivity_user_idDirtyFlag())
            domain.setActivityUserId(model.getActivity_user_id());
        if(model.getAdditional_infoDirtyFlag())
            domain.setAdditionalInfo(model.getAdditional_info());
        if(model.getIbizfunctionDirtyFlag())
            domain.setIbizfunction(model.getIbizfunction());
        if(model.getWebsite_urlDirtyFlag())
            domain.setWebsiteUrl(model.getWebsite_url());
        if(model.getCalendar_last_notif_ackDirtyFlag())
            domain.setCalendarLastNotifAck(model.getCalendar_last_notif_ack());
        if(model.getMessage_needaction_counterDirtyFlag())
            domain.setMessageNeedactionCounter(model.getMessage_needaction_counter());
        if(model.getActivity_date_deadlineDirtyFlag())
            domain.setActivityDateDeadline(model.getActivity_date_deadline());
        if(model.getEmployeeDirtyFlag())
            domain.setEmployee(model.getEmployee());
        if(model.getActivity_stateDirtyFlag())
            domain.setActivityState(model.getActivity_state());
        if(model.getBarcodeDirtyFlag())
            domain.setBarcode(model.getBarcode());
        if(model.getPartner_gidDirtyFlag())
            domain.setPartnerGid(model.getPartner_gid());
        if(model.getTypeDirtyFlag())
            domain.setType(model.getType());
        if(model.getVatDirtyFlag())
            domain.setVat(model.getVat());
        if(model.getPurchase_warn_msgDirtyFlag())
            domain.setPurchaseWarnMsg(model.getPurchase_warn_msg());
        if(model.getCommentDirtyFlag())
            domain.setComment(model.getComment());
        if(model.getSupplierDirtyFlag())
            domain.setSupplier(model.getSupplier());
        if(model.getWebsite_meta_keywordsDirtyFlag())
            domain.setWebsiteMetaKeywords(model.getWebsite_meta_keywords());
        if(model.getParent_idDirtyFlag())
            domain.setParentId(model.getParent_id());
        if(model.getPurchase_warnDirtyFlag())
            domain.setPurchaseWarn(model.getPurchase_warn());
        if(model.getActive_partnerDirtyFlag())
            domain.setActivePartner(model.getActive_partner());
        if(model.getCurrency_idDirtyFlag())
            domain.setCurrencyId(model.getCurrency_id());
        if(model.getIndustry_idDirtyFlag())
            domain.setIndustryId(model.getIndustry_id());
        if(model.getProperty_stock_supplierDirtyFlag())
            domain.setPropertyStockSupplier(model.getProperty_stock_supplier());
        if(model.getPayment_token_countDirtyFlag())
            domain.setPaymentTokenCount(model.getPayment_token_count());
        if(model.getMessage_needactionDirtyFlag())
            domain.setMessageNeedaction(model.getMessage_needaction());
        if(model.getUser_idDirtyFlag())
            domain.setUserId(model.getUser_id());
        if(model.getCustomerDirtyFlag())
            domain.setCustomer(model.getCustomer());
        if(model.getProperty_payment_term_idDirtyFlag())
            domain.setPropertyPaymentTermId(model.getProperty_payment_term_id());
        if(model.getActivity_type_idDirtyFlag())
            domain.setActivityTypeId(model.getActivity_type_id());
        if(model.getContracts_countDirtyFlag())
            domain.setContractsCount(model.getContracts_count());
        if(model.getSelfDirtyFlag())
            domain.setSelf(model.getSelf());
        if(model.getWebsite_meta_descriptionDirtyFlag())
            domain.setWebsiteMetaDescription(model.getWebsite_meta_description());
        if(model.getImageDirtyFlag())
            domain.setImage(model.getImage());
        if(model.getEmailDirtyFlag())
            domain.setEmail(model.getEmail());
        if(model.getImage_mediumDirtyFlag())
            domain.setImageMedium(model.getImage_medium());
        if(model.getActivity_summaryDirtyFlag())
            domain.setActivitySummary(model.getActivity_summary());
        if(model.getDebit_limitDirtyFlag())
            domain.setDebitLimit(model.getDebit_limit());
        if(model.getCountry_idDirtyFlag())
            domain.setCountryId(model.getCountry_id());
        if(model.getCredit_limitDirtyFlag())
            domain.setCreditLimit(model.getCredit_limit());
        if(model.getCommercial_company_nameDirtyFlag())
            domain.setCommercialCompanyName(model.getCommercial_company_name());
        if(model.getInvoice_warn_msgDirtyFlag())
            domain.setInvoiceWarnMsg(model.getInvoice_warn_msg());
        if(model.getIs_publishedDirtyFlag())
            domain.setIsPublished(model.getIs_published());
        if(model.getTrustDirtyFlag())
            domain.setTrust(model.getTrust());
        if(model.getMobileDirtyFlag())
            domain.setMobile(model.getMobile());
        if(model.getEmail_formattedDirtyFlag())
            domain.setEmailFormatted(model.getEmail_formatted());
        if(model.getIs_companyDirtyFlag())
            domain.setIsCompany(model.getIs_company());
        if(model.getCreate_uid_textDirtyFlag())
            domain.setCreateUidText(model.getCreate_uid_text());
        if(model.getTeam_idDirtyFlag())
            domain.setTeamId(model.getTeam_id());
        if(model.getIs_blacklistedDirtyFlag())
            domain.setIsBlacklisted(model.getIs_blacklisted());
        if(model.getBank_account_countDirtyFlag())
            domain.setBankAccountCount(model.getBank_account_count());
        if(model.getProperty_product_pricelistDirtyFlag())
            domain.setPropertyProductPricelist(model.getProperty_product_pricelist());
        if(model.getNameDirtyFlag())
            domain.setName(model.getName());
        if(model.getPicking_warn_msgDirtyFlag())
            domain.setPickingWarnMsg(model.getPicking_warn_msg());
        if(model.getMessage_attachment_countDirtyFlag())
            domain.setMessageAttachmentCount(model.getMessage_attachment_count());
        if(model.getWebsiteDirtyFlag())
            domain.setWebsite(model.getWebsite());
        if(model.getPhoneDirtyFlag())
            domain.setPhone(model.getPhone());
        if(model.getStreet2DirtyFlag())
            domain.setStreet2(model.getStreet2());
        if(model.getHas_unreconciled_entriesDirtyFlag())
            domain.setHasUnreconciledEntries(model.getHas_unreconciled_entries());
        if(model.getSignup_expirationDirtyFlag())
            domain.setSignupExpiration(model.getSignup_expiration());
        if(model.getTzDirtyFlag())
            domain.setTz(model.getTz());
        if(model.getContact_addressDirtyFlag())
            domain.setContactAddress(model.getContact_address());
        if(model.getWebsite_short_descriptionDirtyFlag())
            domain.setWebsiteShortDescription(model.getWebsite_short_description());
        if(model.getPartner_shareDirtyFlag())
            domain.setPartnerShare(model.getPartner_share());
        if(model.getCompany_id_textDirtyFlag())
            domain.setCompanyIdText(model.getCompany_id_text());
        if(model.getColorDirtyFlag())
            domain.setColor(model.getColor());
        if(model.getZipDirtyFlag())
            domain.setZip(model.getZip());
        if(model.getSale_order_countDirtyFlag())
            domain.setSaleOrderCount(model.getSale_order_count());
        if(model.getCompany_typeDirtyFlag())
            domain.setCompanyType(model.getCompany_type());
        if(model.getProperty_account_position_idDirtyFlag())
            domain.setPropertyAccountPositionId(model.getProperty_account_position_id());
        if(model.getIs_seo_optimizedDirtyFlag())
            domain.setIsSeoOptimized(model.getIs_seo_optimized());
        if(model.getMessage_bounceDirtyFlag())
            domain.setMessageBounce(model.getMessage_bounce());
        if(model.getWebsite_meta_titleDirtyFlag())
            domain.setWebsiteMetaTitle(model.getWebsite_meta_title());
        if(model.getImage_smallDirtyFlag())
            domain.setImageSmall(model.getImage_small());
        if(model.getProperty_purchase_currency_idDirtyFlag())
            domain.setPropertyPurchaseCurrencyId(model.getProperty_purchase_currency_id());
        if(model.getPurchase_order_countDirtyFlag())
            domain.setPurchaseOrderCount(model.getPurchase_order_count());
        if(model.getWebsite_descriptionDirtyFlag())
            domain.setWebsiteDescription(model.getWebsite_description());
        if(model.getProperty_supplier_payment_term_idDirtyFlag())
            domain.setPropertySupplierPaymentTermId(model.getProperty_supplier_payment_term_id());
        if(model.getCommercial_partner_idDirtyFlag())
            domain.setCommercialPartnerId(model.getCommercial_partner_id());
        if(model.getAlias_contactDirtyFlag())
            domain.setAliasContact(model.getAlias_contact());
        if(model.getCompany_nameDirtyFlag())
            domain.setCompanyName(model.getCompany_name());
        if(model.getCompany_idDirtyFlag())
            domain.setCompanyId(model.getCompany_id());
        if(model.getPartner_idDirtyFlag())
            domain.setPartnerId(model.getPartner_id());
        if(model.getAlias_idDirtyFlag())
            domain.setAliasId(model.getAlias_id());
        if(model.getSale_team_idDirtyFlag())
            domain.setSaleTeamId(model.getSale_team_id());
        if(model.getWrite_uidDirtyFlag())
            domain.setWriteUid(model.getWrite_uid());
        if(model.getCreate_uidDirtyFlag())
            domain.setCreateUid(model.getCreate_uid());
        return domain ;
    }

}

    



