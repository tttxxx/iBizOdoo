package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Fleet_vehicle_odometer;
import cn.ibizlab.odoo.core.odoo_fleet.filter.Fleet_vehicle_odometerSearchContext;

/**
 * 实体 [车辆的里程表记录] 存储对象
 */
public interface Fleet_vehicle_odometerRepository extends Repository<Fleet_vehicle_odometer> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Fleet_vehicle_odometer> searchDefault(Fleet_vehicle_odometerSearchContext context);

    Fleet_vehicle_odometer convert2PO(cn.ibizlab.odoo.core.odoo_fleet.domain.Fleet_vehicle_odometer domain , Fleet_vehicle_odometer po) ;

    cn.ibizlab.odoo.core.odoo_fleet.domain.Fleet_vehicle_odometer convert2Domain( Fleet_vehicle_odometer po ,cn.ibizlab.odoo.core.odoo_fleet.domain.Fleet_vehicle_odometer domain) ;

}
