package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Maintenance_stage;
import cn.ibizlab.odoo.core.odoo_maintenance.filter.Maintenance_stageSearchContext;

/**
 * 实体 [保养阶段] 存储对象
 */
public interface Maintenance_stageRepository extends Repository<Maintenance_stage> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Maintenance_stage> searchDefault(Maintenance_stageSearchContext context);

    Maintenance_stage convert2PO(cn.ibizlab.odoo.core.odoo_maintenance.domain.Maintenance_stage domain , Maintenance_stage po) ;

    cn.ibizlab.odoo.core.odoo_maintenance.domain.Maintenance_stage convert2Domain( Maintenance_stage po ,cn.ibizlab.odoo.core.odoo_maintenance.domain.Maintenance_stage domain) ;

}
