package cn.ibizlab.odoo.core.odoo_account.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_account.domain.Account_move_line;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_move_lineSearchContext;


/**
 * 实体[Account_move_line] 服务对象接口
 */
public interface IAccount_move_lineService{

    boolean update(Account_move_line et) ;
    void updateBatch(List<Account_move_line> list) ;
    boolean create(Account_move_line et) ;
    void createBatch(List<Account_move_line> list) ;
    Account_move_line get(Integer key) ;
    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    Page<Account_move_line> searchDefault(Account_move_lineSearchContext context) ;

}



