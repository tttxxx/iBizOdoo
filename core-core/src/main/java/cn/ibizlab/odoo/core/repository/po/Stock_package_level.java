package cn.ibizlab.odoo.core.repository.po ;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.math.BigInteger;

import cn.ibizlab.odoo.core.odoo_stock.filter.Stock_package_levelSearchContext;

/**
 * 实体 [库存包装层级] 存储模型
 */
public interface Stock_package_level{

    /**
     * 凭证明细
     */
    String getMove_line_ids();

    void setMove_line_ids(String move_line_ids);

    /**
     * 获取 [凭证明细]脏标记
     */
    boolean getMove_line_idsDirtyFlag();

    /**
     * 移动
     */
    String getMove_ids();

    void setMove_ids(String move_ids);

    /**
     * 获取 [移动]脏标记
     */
    boolean getMove_idsDirtyFlag();

    /**
     * 创建时间
     */
    Timestamp getCreate_date();

    void setCreate_date(Timestamp create_date);

    /**
     * 获取 [创建时间]脏标记
     */
    boolean getCreate_dateDirtyFlag();

    /**
     * 从
     */
    Integer getLocation_id();

    void setLocation_id(Integer location_id);

    /**
     * 获取 [从]脏标记
     */
    boolean getLocation_idDirtyFlag();

    /**
     * 显示批次文本
     */
    String getShow_lots_text();

    void setShow_lots_text(String show_lots_text);

    /**
     * 获取 [显示批次文本]脏标记
     */
    boolean getShow_lots_textDirtyFlag();

    /**
     * 最后修改日
     */
    Timestamp get__last_update();

    void set__last_update(Timestamp __last_update);

    /**
     * 获取 [最后修改日]脏标记
     */
    boolean get__last_updateDirtyFlag();

    /**
     * 完成
     */
    String getIs_done();

    void setIs_done(String is_done);

    /**
     * 获取 [完成]脏标记
     */
    boolean getIs_doneDirtyFlag();

    /**
     * 显示批次M2O
     */
    String getShow_lots_m2o();

    void setShow_lots_m2o(String show_lots_m2o);

    /**
     * 获取 [显示批次M2O]脏标记
     */
    boolean getShow_lots_m2oDirtyFlag();

    /**
     * 状态
     */
    String getState();

    void setState(String state);

    /**
     * 获取 [状态]脏标记
     */
    boolean getStateDirtyFlag();

    /**
     * ID
     */
    Integer getId();

    void setId(Integer id);

    /**
     * 获取 [ID]脏标记
     */
    boolean getIdDirtyFlag();

    /**
     * 最后更新时间
     */
    Timestamp getWrite_date();

    void setWrite_date(Timestamp write_date);

    /**
     * 获取 [最后更新时间]脏标记
     */
    boolean getWrite_dateDirtyFlag();

    /**
     * 是新鲜包裹
     */
    String getIs_fresh_package();

    void setIs_fresh_package(String is_fresh_package);

    /**
     * 获取 [是新鲜包裹]脏标记
     */
    boolean getIs_fresh_packageDirtyFlag();

    /**
     * 显示名称
     */
    String getDisplay_name();

    void setDisplay_name(String display_name);

    /**
     * 获取 [显示名称]脏标记
     */
    boolean getDisplay_nameDirtyFlag();

    /**
     * 分拣
     */
    String getPicking_id_text();

    void setPicking_id_text(String picking_id_text);

    /**
     * 获取 [分拣]脏标记
     */
    boolean getPicking_id_textDirtyFlag();

    /**
     * 最后更新者
     */
    String getWrite_uid_text();

    void setWrite_uid_text(String write_uid_text);

    /**
     * 获取 [最后更新者]脏标记
     */
    boolean getWrite_uid_textDirtyFlag();

    /**
     * 包裹
     */
    String getPackage_id_text();

    void setPackage_id_text(String package_id_text);

    /**
     * 获取 [包裹]脏标记
     */
    boolean getPackage_id_textDirtyFlag();

    /**
     * 至
     */
    String getLocation_dest_id_text();

    void setLocation_dest_id_text(String location_dest_id_text);

    /**
     * 获取 [至]脏标记
     */
    boolean getLocation_dest_id_textDirtyFlag();

    /**
     * 创建人
     */
    String getCreate_uid_text();

    void setCreate_uid_text(String create_uid_text);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uid_textDirtyFlag();

    /**
     * 源位置
     */
    Integer getPicking_source_location();

    void setPicking_source_location(Integer picking_source_location);

    /**
     * 获取 [源位置]脏标记
     */
    boolean getPicking_source_locationDirtyFlag();

    /**
     * 最后更新者
     */
    Integer getWrite_uid();

    void setWrite_uid(Integer write_uid);

    /**
     * 获取 [最后更新者]脏标记
     */
    boolean getWrite_uidDirtyFlag();

    /**
     * 分拣
     */
    Integer getPicking_id();

    void setPicking_id(Integer picking_id);

    /**
     * 获取 [分拣]脏标记
     */
    boolean getPicking_idDirtyFlag();

    /**
     * 包裹
     */
    Integer getPackage_id();

    void setPackage_id(Integer package_id);

    /**
     * 获取 [包裹]脏标记
     */
    boolean getPackage_idDirtyFlag();

    /**
     * 创建人
     */
    Integer getCreate_uid();

    void setCreate_uid(Integer create_uid);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uidDirtyFlag();

    /**
     * 至
     */
    Integer getLocation_dest_id();

    void setLocation_dest_id(Integer location_dest_id);

    /**
     * 获取 [至]脏标记
     */
    boolean getLocation_dest_idDirtyFlag();

}
