package cn.ibizlab.odoo.core.odoo_stock.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_stock.domain.Stock_move;
import cn.ibizlab.odoo.core.odoo_stock.filter.Stock_moveSearchContext;
import cn.ibizlab.odoo.core.odoo_stock.service.IStock_moveService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;



import cn.ibizlab.odoo.core.odoo_stock.client.stock_moveOdooClient;
import cn.ibizlab.odoo.core.odoo_stock.clientmodel.stock_moveClientModel;

/**
 * 实体[库存移动] 服务对象接口实现
 */
@Slf4j
@Service
public class Stock_moveServiceImpl implements IStock_moveService {

    @Autowired
    stock_moveOdooClient stock_moveOdooClient;


    @Override
    public Stock_move get(Integer id) {
        stock_moveClientModel clientModel = new stock_moveClientModel();
        clientModel.setId(id);
		stock_moveOdooClient.get(clientModel);
        Stock_move et = convert2Domain(clientModel,null);
        if(et==null){
            et=new Stock_move();
            et.setId(id);
        }
        else{
        }

        return  et;
    }

    @Override
    public boolean update(Stock_move et) {
        stock_moveClientModel clientModel = convert2Model(et,null);
		stock_moveOdooClient.update(clientModel);
        Stock_move rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }
    
    public void updateBatch(List<Stock_move> list){
    }

    @Override
    public boolean create(Stock_move et) {
        stock_moveClientModel clientModel = convert2Model(et,null);
		stock_moveOdooClient.create(clientModel);
        Stock_move rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Stock_move> list){
    }

    @Override
    public boolean remove(Integer id) {
        stock_moveClientModel clientModel = new stock_moveClientModel();
        clientModel.setId(id);
		stock_moveOdooClient.remove(clientModel);
        return true; 
    }

    public void removeBatch(Collection<Integer> idList){
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Stock_move> searchDefault(Stock_moveSearchContext context) {
        List<Stock_move> list = new ArrayList<Stock_move>();
        Page<stock_moveClientModel> clientModelList = stock_moveOdooClient.search(context);
        for(stock_moveClientModel clientModel : clientModelList){
            list.add( this.convert2Domain(clientModel,null));
        }
        return new PageImpl<Stock_move>(list, context.getPageable(), clientModelList.getTotalElements());
    }

    public stock_moveClientModel convert2Model(Stock_move domain , stock_moveClientModel model) {
        if(domain == null)
            return model ;
        if(model==null){
            model = new stock_moveClientModel();

        }
        if((Boolean) domain.getExtensionparams().get("dirtyflagenable")){
            if((Boolean) domain.getExtensionparams().get("returned_move_idsdirtyflag"))
                model.setReturned_move_ids(domain.getReturnedMoveIds());
            if((Boolean) domain.getExtensionparams().get("additionaldirtyflag"))
                model.setAdditional(domain.getAdditional());
            if((Boolean) domain.getExtensionparams().get("price_unitdirtyflag"))
                model.setPrice_unit(domain.getPriceUnit());
            if((Boolean) domain.getExtensionparams().get("group_iddirtyflag"))
                model.setGroup_id(domain.getGroupId());
            if((Boolean) domain.getExtensionparams().get("scrap_idsdirtyflag"))
                model.setScrap_ids(domain.getScrapIds());
            if((Boolean) domain.getExtensionparams().get("account_move_idsdirtyflag"))
                model.setAccount_move_ids(domain.getAccountMoveIds());
            if((Boolean) domain.getExtensionparams().get("move_dest_idsdirtyflag"))
                model.setMove_dest_ids(domain.getMoveDestIds());
            if((Boolean) domain.getExtensionparams().get("iddirtyflag"))
                model.setId(domain.getId());
            if((Boolean) domain.getExtensionparams().get("to_refunddirtyflag"))
                model.setTo_refund(domain.getToRefund());
            if((Boolean) domain.getExtensionparams().get("unit_factordirtyflag"))
                model.setUnit_factor(domain.getUnitFactor());
            if((Boolean) domain.getExtensionparams().get("__last_updatedirtyflag"))
                model.set__last_update(domain.getLastUpdate());
            if((Boolean) domain.getExtensionparams().get("availabilitydirtyflag"))
                model.setAvailability(domain.getAvailability());
            if((Boolean) domain.getExtensionparams().get("remaining_qtydirtyflag"))
                model.setRemaining_qty(domain.getRemainingQty());
            if((Boolean) domain.getExtensionparams().get("propagatedirtyflag"))
                model.setPropagate(domain.getPropagate());
            if((Boolean) domain.getExtensionparams().get("referencedirtyflag"))
                model.setReference(domain.getReference());
            if((Boolean) domain.getExtensionparams().get("active_move_line_idsdirtyflag"))
                model.setActive_move_line_ids(domain.getActiveMoveLineIds());
            if((Boolean) domain.getExtensionparams().get("move_line_nosuggest_idsdirtyflag"))
                model.setMove_line_nosuggest_ids(domain.getMoveLineNosuggestIds());
            if((Boolean) domain.getExtensionparams().get("show_details_visibledirtyflag"))
                model.setShow_details_visible(domain.getShowDetailsVisible());
            if((Boolean) domain.getExtensionparams().get("product_qtydirtyflag"))
                model.setProduct_qty(domain.getProductQty());
            if((Boolean) domain.getExtensionparams().get("write_datedirtyflag"))
                model.setWrite_date(domain.getWriteDate());
            if((Boolean) domain.getExtensionparams().get("origindirtyflag"))
                model.setOrigin(domain.getOrigin());
            if((Boolean) domain.getExtensionparams().get("product_uom_qtydirtyflag"))
                model.setProduct_uom_qty(domain.getProductUomQty());
            if((Boolean) domain.getExtensionparams().get("needs_lotsdirtyflag"))
                model.setNeeds_lots(domain.getNeedsLots());
            if((Boolean) domain.getExtensionparams().get("prioritydirtyflag"))
                model.setPriority(domain.getPriority());
            if((Boolean) domain.getExtensionparams().get("datedirtyflag"))
                model.setDate(domain.getDate());
            if((Boolean) domain.getExtensionparams().get("remaining_valuedirtyflag"))
                model.setRemaining_value(domain.getRemainingValue());
            if((Boolean) domain.getExtensionparams().get("order_finished_lot_idsdirtyflag"))
                model.setOrder_finished_lot_ids(domain.getOrderFinishedLotIds());
            if((Boolean) domain.getExtensionparams().get("sequencedirtyflag"))
                model.setSequence(domain.getSequence());
            if((Boolean) domain.getExtensionparams().get("create_datedirtyflag"))
                model.setCreate_date(domain.getCreateDate());
            if((Boolean) domain.getExtensionparams().get("move_orig_idsdirtyflag"))
                model.setMove_orig_ids(domain.getMoveOrigIds());
            if((Boolean) domain.getExtensionparams().get("move_line_idsdirtyflag"))
                model.setMove_line_ids(domain.getMoveLineIds());
            if((Boolean) domain.getExtensionparams().get("notedirtyflag"))
                model.setNote(domain.getNote());
            if((Boolean) domain.getExtensionparams().get("reserved_availabilitydirtyflag"))
                model.setReserved_availability(domain.getReservedAvailability());
            if((Boolean) domain.getExtensionparams().get("is_initial_demand_editabledirtyflag"))
                model.setIs_initial_demand_editable(domain.getIsInitialDemandEditable());
            if((Boolean) domain.getExtensionparams().get("quantity_donedirtyflag"))
                model.setQuantity_done(domain.getQuantityDone());
            if((Boolean) domain.getExtensionparams().get("is_lockeddirtyflag"))
                model.setIs_locked(domain.getIsLocked());
            if((Boolean) domain.getExtensionparams().get("string_availability_infodirtyflag"))
                model.setString_availability_info(domain.getStringAvailabilityInfo());
            if((Boolean) domain.getExtensionparams().get("display_namedirtyflag"))
                model.setDisplay_name(domain.getDisplayName());
            if((Boolean) domain.getExtensionparams().get("show_reserved_availabilitydirtyflag"))
                model.setShow_reserved_availability(domain.getShowReservedAvailability());
            if((Boolean) domain.getExtensionparams().get("route_idsdirtyflag"))
                model.setRoute_ids(domain.getRouteIds());
            if((Boolean) domain.getExtensionparams().get("statedirtyflag"))
                model.setState(domain.getState());
            if((Boolean) domain.getExtensionparams().get("namedirtyflag"))
                model.setName(domain.getName());
            if((Boolean) domain.getExtensionparams().get("valuedirtyflag"))
                model.setValue(domain.getValue());
            if((Boolean) domain.getExtensionparams().get("finished_lots_existdirtyflag"))
                model.setFinished_lots_exist(domain.getFinishedLotsExist());
            if((Boolean) domain.getExtensionparams().get("is_donedirtyflag"))
                model.setIs_done(domain.getIsDone());
            if((Boolean) domain.getExtensionparams().get("is_quantity_done_editabledirtyflag"))
                model.setIs_quantity_done_editable(domain.getIsQuantityDoneEditable());
            if((Boolean) domain.getExtensionparams().get("has_move_linesdirtyflag"))
                model.setHas_move_lines(domain.getHasMoveLines());
            if((Boolean) domain.getExtensionparams().get("date_expecteddirtyflag"))
                model.setDate_expected(domain.getDateExpected());
            if((Boolean) domain.getExtensionparams().get("procure_methoddirtyflag"))
                model.setProcure_method(domain.getProcureMethod());
            if((Boolean) domain.getExtensionparams().get("product_typedirtyflag"))
                model.setProduct_type(domain.getProductType());
            if((Boolean) domain.getExtensionparams().get("unbuild_id_textdirtyflag"))
                model.setUnbuild_id_text(domain.getUnbuildIdText());
            if((Boolean) domain.getExtensionparams().get("scrappeddirtyflag"))
                model.setScrapped(domain.getScrapped());
            if((Boolean) domain.getExtensionparams().get("company_id_textdirtyflag"))
                model.setCompany_id_text(domain.getCompanyIdText());
            if((Boolean) domain.getExtensionparams().get("production_id_textdirtyflag"))
                model.setProduction_id_text(domain.getProductionIdText());
            if((Boolean) domain.getExtensionparams().get("warehouse_id_textdirtyflag"))
                model.setWarehouse_id_text(domain.getWarehouseIdText());
            if((Boolean) domain.getExtensionparams().get("show_operationsdirtyflag"))
                model.setShow_operations(domain.getShowOperations());
            if((Boolean) domain.getExtensionparams().get("created_production_id_textdirtyflag"))
                model.setCreated_production_id_text(domain.getCreatedProductionIdText());
            if((Boolean) domain.getExtensionparams().get("created_purchase_line_id_textdirtyflag"))
                model.setCreated_purchase_line_id_text(domain.getCreatedPurchaseLineIdText());
            if((Boolean) domain.getExtensionparams().get("location_dest_id_textdirtyflag"))
                model.setLocation_dest_id_text(domain.getLocationDestIdText());
            if((Boolean) domain.getExtensionparams().get("has_trackingdirtyflag"))
                model.setHas_tracking(domain.getHasTracking());
            if((Boolean) domain.getExtensionparams().get("picking_type_id_textdirtyflag"))
                model.setPicking_type_id_text(domain.getPickingTypeIdText());
            if((Boolean) domain.getExtensionparams().get("sale_line_id_textdirtyflag"))
                model.setSale_line_id_text(domain.getSaleLineIdText());
            if((Boolean) domain.getExtensionparams().get("partner_id_textdirtyflag"))
                model.setPartner_id_text(domain.getPartnerIdText());
            if((Boolean) domain.getExtensionparams().get("picking_partner_iddirtyflag"))
                model.setPicking_partner_id(domain.getPickingPartnerId());
            if((Boolean) domain.getExtensionparams().get("raw_material_production_id_textdirtyflag"))
                model.setRaw_material_production_id_text(domain.getRawMaterialProductionIdText());
            if((Boolean) domain.getExtensionparams().get("origin_returned_move_id_textdirtyflag"))
                model.setOrigin_returned_move_id_text(domain.getOriginReturnedMoveIdText());
            if((Boolean) domain.getExtensionparams().get("repair_id_textdirtyflag"))
                model.setRepair_id_text(domain.getRepairIdText());
            if((Boolean) domain.getExtensionparams().get("backorder_iddirtyflag"))
                model.setBackorder_id(domain.getBackorderId());
            if((Boolean) domain.getExtensionparams().get("purchase_line_id_textdirtyflag"))
                model.setPurchase_line_id_text(domain.getPurchaseLineIdText());
            if((Boolean) domain.getExtensionparams().get("picking_id_textdirtyflag"))
                model.setPicking_id_text(domain.getPickingIdText());
            if((Boolean) domain.getExtensionparams().get("product_id_textdirtyflag"))
                model.setProduct_id_text(domain.getProductIdText());
            if((Boolean) domain.getExtensionparams().get("location_id_textdirtyflag"))
                model.setLocation_id_text(domain.getLocationIdText());
            if((Boolean) domain.getExtensionparams().get("create_uid_textdirtyflag"))
                model.setCreate_uid_text(domain.getCreateUidText());
            if((Boolean) domain.getExtensionparams().get("product_uom_textdirtyflag"))
                model.setProduct_uom_text(domain.getProductUomText());
            if((Boolean) domain.getExtensionparams().get("operation_id_textdirtyflag"))
                model.setOperation_id_text(domain.getOperationIdText());
            if((Boolean) domain.getExtensionparams().get("workorder_id_textdirtyflag"))
                model.setWorkorder_id_text(domain.getWorkorderIdText());
            if((Boolean) domain.getExtensionparams().get("consume_unbuild_id_textdirtyflag"))
                model.setConsume_unbuild_id_text(domain.getConsumeUnbuildIdText());
            if((Boolean) domain.getExtensionparams().get("product_packaging_textdirtyflag"))
                model.setProduct_packaging_text(domain.getProductPackagingText());
            if((Boolean) domain.getExtensionparams().get("restrict_partner_id_textdirtyflag"))
                model.setRestrict_partner_id_text(domain.getRestrictPartnerIdText());
            if((Boolean) domain.getExtensionparams().get("inventory_id_textdirtyflag"))
                model.setInventory_id_text(domain.getInventoryIdText());
            if((Boolean) domain.getExtensionparams().get("product_tmpl_iddirtyflag"))
                model.setProduct_tmpl_id(domain.getProductTmplId());
            if((Boolean) domain.getExtensionparams().get("rule_id_textdirtyflag"))
                model.setRule_id_text(domain.getRuleIdText());
            if((Boolean) domain.getExtensionparams().get("write_uid_textdirtyflag"))
                model.setWrite_uid_text(domain.getWriteUidText());
            if((Boolean) domain.getExtensionparams().get("picking_codedirtyflag"))
                model.setPicking_code(domain.getPickingCode());
            if((Boolean) domain.getExtensionparams().get("picking_type_entire_packsdirtyflag"))
                model.setPicking_type_entire_packs(domain.getPickingTypeEntirePacks());
            if((Boolean) domain.getExtensionparams().get("created_production_iddirtyflag"))
                model.setCreated_production_id(domain.getCreatedProductionId());
            if((Boolean) domain.getExtensionparams().get("consume_unbuild_iddirtyflag"))
                model.setConsume_unbuild_id(domain.getConsumeUnbuildId());
            if((Boolean) domain.getExtensionparams().get("sale_line_iddirtyflag"))
                model.setSale_line_id(domain.getSaleLineId());
            if((Boolean) domain.getExtensionparams().get("origin_returned_move_iddirtyflag"))
                model.setOrigin_returned_move_id(domain.getOriginReturnedMoveId());
            if((Boolean) domain.getExtensionparams().get("picking_type_iddirtyflag"))
                model.setPicking_type_id(domain.getPickingTypeId());
            if((Boolean) domain.getExtensionparams().get("operation_iddirtyflag"))
                model.setOperation_id(domain.getOperationId());
            if((Boolean) domain.getExtensionparams().get("bom_line_iddirtyflag"))
                model.setBom_line_id(domain.getBomLineId());
            if((Boolean) domain.getExtensionparams().get("repair_iddirtyflag"))
                model.setRepair_id(domain.getRepairId());
            if((Boolean) domain.getExtensionparams().get("product_iddirtyflag"))
                model.setProduct_id(domain.getProductId());
            if((Boolean) domain.getExtensionparams().get("unbuild_iddirtyflag"))
                model.setUnbuild_id(domain.getUnbuildId());
            if((Boolean) domain.getExtensionparams().get("production_iddirtyflag"))
                model.setProduction_id(domain.getProductionId());
            if((Boolean) domain.getExtensionparams().get("created_purchase_line_iddirtyflag"))
                model.setCreated_purchase_line_id(domain.getCreatedPurchaseLineId());
            if((Boolean) domain.getExtensionparams().get("workorder_iddirtyflag"))
                model.setWorkorder_id(domain.getWorkorderId());
            if((Boolean) domain.getExtensionparams().get("product_uomdirtyflag"))
                model.setProduct_uom(domain.getProductUom());
            if((Boolean) domain.getExtensionparams().get("picking_iddirtyflag"))
                model.setPicking_id(domain.getPickingId());
            if((Boolean) domain.getExtensionparams().get("partner_iddirtyflag"))
                model.setPartner_id(domain.getPartnerId());
            if((Boolean) domain.getExtensionparams().get("create_uiddirtyflag"))
                model.setCreate_uid(domain.getCreateUid());
            if((Boolean) domain.getExtensionparams().get("package_level_iddirtyflag"))
                model.setPackage_level_id(domain.getPackageLevelId());
            if((Boolean) domain.getExtensionparams().get("product_packagingdirtyflag"))
                model.setProduct_packaging(domain.getProductPackaging());
            if((Boolean) domain.getExtensionparams().get("restrict_partner_iddirtyflag"))
                model.setRestrict_partner_id(domain.getRestrictPartnerId());
            if((Boolean) domain.getExtensionparams().get("rule_iddirtyflag"))
                model.setRule_id(domain.getRuleId());
            if((Boolean) domain.getExtensionparams().get("warehouse_iddirtyflag"))
                model.setWarehouse_id(domain.getWarehouseId());
            if((Boolean) domain.getExtensionparams().get("location_dest_iddirtyflag"))
                model.setLocation_dest_id(domain.getLocationDestId());
            if((Boolean) domain.getExtensionparams().get("raw_material_production_iddirtyflag"))
                model.setRaw_material_production_id(domain.getRawMaterialProductionId());
            if((Boolean) domain.getExtensionparams().get("inventory_iddirtyflag"))
                model.setInventory_id(domain.getInventoryId());
            if((Boolean) domain.getExtensionparams().get("purchase_line_iddirtyflag"))
                model.setPurchase_line_id(domain.getPurchaseLineId());
            if((Boolean) domain.getExtensionparams().get("location_iddirtyflag"))
                model.setLocation_id(domain.getLocationId());
            if((Boolean) domain.getExtensionparams().get("company_iddirtyflag"))
                model.setCompany_id(domain.getCompanyId());
            if((Boolean) domain.getExtensionparams().get("write_uiddirtyflag"))
                model.setWrite_uid(domain.getWriteUid());
            domain.getFocusNull().clear();
        }
        return model ;
    }

    public Stock_move convert2Domain( stock_moveClientModel model ,Stock_move domain) {
        if(model == null)
            return domain ;
        if(domain == null){
            domain = new Stock_move();
        }

        if(model.getReturned_move_idsDirtyFlag())
            domain.setReturnedMoveIds(model.getReturned_move_ids());
        if(model.getAdditionalDirtyFlag())
            domain.setAdditional(model.getAdditional());
        if(model.getPrice_unitDirtyFlag())
            domain.setPriceUnit(model.getPrice_unit());
        if(model.getGroup_idDirtyFlag())
            domain.setGroupId(model.getGroup_id());
        if(model.getScrap_idsDirtyFlag())
            domain.setScrapIds(model.getScrap_ids());
        if(model.getAccount_move_idsDirtyFlag())
            domain.setAccountMoveIds(model.getAccount_move_ids());
        if(model.getMove_dest_idsDirtyFlag())
            domain.setMoveDestIds(model.getMove_dest_ids());
        if(model.getIdDirtyFlag())
            domain.setId(model.getId());
        if(model.getTo_refundDirtyFlag())
            domain.setToRefund(model.getTo_refund());
        if(model.getUnit_factorDirtyFlag())
            domain.setUnitFactor(model.getUnit_factor());
        if(model.get__last_updateDirtyFlag())
            domain.setLastUpdate(model.get__last_update());
        if(model.getAvailabilityDirtyFlag())
            domain.setAvailability(model.getAvailability());
        if(model.getRemaining_qtyDirtyFlag())
            domain.setRemainingQty(model.getRemaining_qty());
        if(model.getPropagateDirtyFlag())
            domain.setPropagate(model.getPropagate());
        if(model.getReferenceDirtyFlag())
            domain.setReference(model.getReference());
        if(model.getActive_move_line_idsDirtyFlag())
            domain.setActiveMoveLineIds(model.getActive_move_line_ids());
        if(model.getMove_line_nosuggest_idsDirtyFlag())
            domain.setMoveLineNosuggestIds(model.getMove_line_nosuggest_ids());
        if(model.getShow_details_visibleDirtyFlag())
            domain.setShowDetailsVisible(model.getShow_details_visible());
        if(model.getProduct_qtyDirtyFlag())
            domain.setProductQty(model.getProduct_qty());
        if(model.getWrite_dateDirtyFlag())
            domain.setWriteDate(model.getWrite_date());
        if(model.getOriginDirtyFlag())
            domain.setOrigin(model.getOrigin());
        if(model.getProduct_uom_qtyDirtyFlag())
            domain.setProductUomQty(model.getProduct_uom_qty());
        if(model.getNeeds_lotsDirtyFlag())
            domain.setNeedsLots(model.getNeeds_lots());
        if(model.getPriorityDirtyFlag())
            domain.setPriority(model.getPriority());
        if(model.getDateDirtyFlag())
            domain.setDate(model.getDate());
        if(model.getRemaining_valueDirtyFlag())
            domain.setRemainingValue(model.getRemaining_value());
        if(model.getOrder_finished_lot_idsDirtyFlag())
            domain.setOrderFinishedLotIds(model.getOrder_finished_lot_ids());
        if(model.getSequenceDirtyFlag())
            domain.setSequence(model.getSequence());
        if(model.getCreate_dateDirtyFlag())
            domain.setCreateDate(model.getCreate_date());
        if(model.getMove_orig_idsDirtyFlag())
            domain.setMoveOrigIds(model.getMove_orig_ids());
        if(model.getMove_line_idsDirtyFlag())
            domain.setMoveLineIds(model.getMove_line_ids());
        if(model.getNoteDirtyFlag())
            domain.setNote(model.getNote());
        if(model.getReserved_availabilityDirtyFlag())
            domain.setReservedAvailability(model.getReserved_availability());
        if(model.getIs_initial_demand_editableDirtyFlag())
            domain.setIsInitialDemandEditable(model.getIs_initial_demand_editable());
        if(model.getQuantity_doneDirtyFlag())
            domain.setQuantityDone(model.getQuantity_done());
        if(model.getIs_lockedDirtyFlag())
            domain.setIsLocked(model.getIs_locked());
        if(model.getString_availability_infoDirtyFlag())
            domain.setStringAvailabilityInfo(model.getString_availability_info());
        if(model.getDisplay_nameDirtyFlag())
            domain.setDisplayName(model.getDisplay_name());
        if(model.getShow_reserved_availabilityDirtyFlag())
            domain.setShowReservedAvailability(model.getShow_reserved_availability());
        if(model.getRoute_idsDirtyFlag())
            domain.setRouteIds(model.getRoute_ids());
        if(model.getStateDirtyFlag())
            domain.setState(model.getState());
        if(model.getNameDirtyFlag())
            domain.setName(model.getName());
        if(model.getValueDirtyFlag())
            domain.setValue(model.getValue());
        if(model.getFinished_lots_existDirtyFlag())
            domain.setFinishedLotsExist(model.getFinished_lots_exist());
        if(model.getIs_doneDirtyFlag())
            domain.setIsDone(model.getIs_done());
        if(model.getIs_quantity_done_editableDirtyFlag())
            domain.setIsQuantityDoneEditable(model.getIs_quantity_done_editable());
        if(model.getHas_move_linesDirtyFlag())
            domain.setHasMoveLines(model.getHas_move_lines());
        if(model.getDate_expectedDirtyFlag())
            domain.setDateExpected(model.getDate_expected());
        if(model.getProcure_methodDirtyFlag())
            domain.setProcureMethod(model.getProcure_method());
        if(model.getProduct_typeDirtyFlag())
            domain.setProductType(model.getProduct_type());
        if(model.getUnbuild_id_textDirtyFlag())
            domain.setUnbuildIdText(model.getUnbuild_id_text());
        if(model.getScrappedDirtyFlag())
            domain.setScrapped(model.getScrapped());
        if(model.getCompany_id_textDirtyFlag())
            domain.setCompanyIdText(model.getCompany_id_text());
        if(model.getProduction_id_textDirtyFlag())
            domain.setProductionIdText(model.getProduction_id_text());
        if(model.getWarehouse_id_textDirtyFlag())
            domain.setWarehouseIdText(model.getWarehouse_id_text());
        if(model.getShow_operationsDirtyFlag())
            domain.setShowOperations(model.getShow_operations());
        if(model.getCreated_production_id_textDirtyFlag())
            domain.setCreatedProductionIdText(model.getCreated_production_id_text());
        if(model.getCreated_purchase_line_id_textDirtyFlag())
            domain.setCreatedPurchaseLineIdText(model.getCreated_purchase_line_id_text());
        if(model.getLocation_dest_id_textDirtyFlag())
            domain.setLocationDestIdText(model.getLocation_dest_id_text());
        if(model.getHas_trackingDirtyFlag())
            domain.setHasTracking(model.getHas_tracking());
        if(model.getPicking_type_id_textDirtyFlag())
            domain.setPickingTypeIdText(model.getPicking_type_id_text());
        if(model.getSale_line_id_textDirtyFlag())
            domain.setSaleLineIdText(model.getSale_line_id_text());
        if(model.getPartner_id_textDirtyFlag())
            domain.setPartnerIdText(model.getPartner_id_text());
        if(model.getPicking_partner_idDirtyFlag())
            domain.setPickingPartnerId(model.getPicking_partner_id());
        if(model.getRaw_material_production_id_textDirtyFlag())
            domain.setRawMaterialProductionIdText(model.getRaw_material_production_id_text());
        if(model.getOrigin_returned_move_id_textDirtyFlag())
            domain.setOriginReturnedMoveIdText(model.getOrigin_returned_move_id_text());
        if(model.getRepair_id_textDirtyFlag())
            domain.setRepairIdText(model.getRepair_id_text());
        if(model.getBackorder_idDirtyFlag())
            domain.setBackorderId(model.getBackorder_id());
        if(model.getPurchase_line_id_textDirtyFlag())
            domain.setPurchaseLineIdText(model.getPurchase_line_id_text());
        if(model.getPicking_id_textDirtyFlag())
            domain.setPickingIdText(model.getPicking_id_text());
        if(model.getProduct_id_textDirtyFlag())
            domain.setProductIdText(model.getProduct_id_text());
        if(model.getLocation_id_textDirtyFlag())
            domain.setLocationIdText(model.getLocation_id_text());
        if(model.getCreate_uid_textDirtyFlag())
            domain.setCreateUidText(model.getCreate_uid_text());
        if(model.getProduct_uom_textDirtyFlag())
            domain.setProductUomText(model.getProduct_uom_text());
        if(model.getOperation_id_textDirtyFlag())
            domain.setOperationIdText(model.getOperation_id_text());
        if(model.getWorkorder_id_textDirtyFlag())
            domain.setWorkorderIdText(model.getWorkorder_id_text());
        if(model.getConsume_unbuild_id_textDirtyFlag())
            domain.setConsumeUnbuildIdText(model.getConsume_unbuild_id_text());
        if(model.getProduct_packaging_textDirtyFlag())
            domain.setProductPackagingText(model.getProduct_packaging_text());
        if(model.getRestrict_partner_id_textDirtyFlag())
            domain.setRestrictPartnerIdText(model.getRestrict_partner_id_text());
        if(model.getInventory_id_textDirtyFlag())
            domain.setInventoryIdText(model.getInventory_id_text());
        if(model.getProduct_tmpl_idDirtyFlag())
            domain.setProductTmplId(model.getProduct_tmpl_id());
        if(model.getRule_id_textDirtyFlag())
            domain.setRuleIdText(model.getRule_id_text());
        if(model.getWrite_uid_textDirtyFlag())
            domain.setWriteUidText(model.getWrite_uid_text());
        if(model.getPicking_codeDirtyFlag())
            domain.setPickingCode(model.getPicking_code());
        if(model.getPicking_type_entire_packsDirtyFlag())
            domain.setPickingTypeEntirePacks(model.getPicking_type_entire_packs());
        if(model.getCreated_production_idDirtyFlag())
            domain.setCreatedProductionId(model.getCreated_production_id());
        if(model.getConsume_unbuild_idDirtyFlag())
            domain.setConsumeUnbuildId(model.getConsume_unbuild_id());
        if(model.getSale_line_idDirtyFlag())
            domain.setSaleLineId(model.getSale_line_id());
        if(model.getOrigin_returned_move_idDirtyFlag())
            domain.setOriginReturnedMoveId(model.getOrigin_returned_move_id());
        if(model.getPicking_type_idDirtyFlag())
            domain.setPickingTypeId(model.getPicking_type_id());
        if(model.getOperation_idDirtyFlag())
            domain.setOperationId(model.getOperation_id());
        if(model.getBom_line_idDirtyFlag())
            domain.setBomLineId(model.getBom_line_id());
        if(model.getRepair_idDirtyFlag())
            domain.setRepairId(model.getRepair_id());
        if(model.getProduct_idDirtyFlag())
            domain.setProductId(model.getProduct_id());
        if(model.getUnbuild_idDirtyFlag())
            domain.setUnbuildId(model.getUnbuild_id());
        if(model.getProduction_idDirtyFlag())
            domain.setProductionId(model.getProduction_id());
        if(model.getCreated_purchase_line_idDirtyFlag())
            domain.setCreatedPurchaseLineId(model.getCreated_purchase_line_id());
        if(model.getWorkorder_idDirtyFlag())
            domain.setWorkorderId(model.getWorkorder_id());
        if(model.getProduct_uomDirtyFlag())
            domain.setProductUom(model.getProduct_uom());
        if(model.getPicking_idDirtyFlag())
            domain.setPickingId(model.getPicking_id());
        if(model.getPartner_idDirtyFlag())
            domain.setPartnerId(model.getPartner_id());
        if(model.getCreate_uidDirtyFlag())
            domain.setCreateUid(model.getCreate_uid());
        if(model.getPackage_level_idDirtyFlag())
            domain.setPackageLevelId(model.getPackage_level_id());
        if(model.getProduct_packagingDirtyFlag())
            domain.setProductPackaging(model.getProduct_packaging());
        if(model.getRestrict_partner_idDirtyFlag())
            domain.setRestrictPartnerId(model.getRestrict_partner_id());
        if(model.getRule_idDirtyFlag())
            domain.setRuleId(model.getRule_id());
        if(model.getWarehouse_idDirtyFlag())
            domain.setWarehouseId(model.getWarehouse_id());
        if(model.getLocation_dest_idDirtyFlag())
            domain.setLocationDestId(model.getLocation_dest_id());
        if(model.getRaw_material_production_idDirtyFlag())
            domain.setRawMaterialProductionId(model.getRaw_material_production_id());
        if(model.getInventory_idDirtyFlag())
            domain.setInventoryId(model.getInventory_id());
        if(model.getPurchase_line_idDirtyFlag())
            domain.setPurchaseLineId(model.getPurchase_line_id());
        if(model.getLocation_idDirtyFlag())
            domain.setLocationId(model.getLocation_id());
        if(model.getCompany_idDirtyFlag())
            domain.setCompanyId(model.getCompany_id());
        if(model.getWrite_uidDirtyFlag())
            domain.setWriteUid(model.getWrite_uid());
        return domain ;
    }

}

    



