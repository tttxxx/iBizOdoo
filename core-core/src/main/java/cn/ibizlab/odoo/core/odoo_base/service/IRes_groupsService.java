package cn.ibizlab.odoo.core.odoo_base.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_base.domain.Res_groups;
import cn.ibizlab.odoo.core.odoo_base.filter.Res_groupsSearchContext;


/**
 * 实体[Res_groups] 服务对象接口
 */
public interface IRes_groupsService{

    Res_groups get(Integer key) ;
    boolean update(Res_groups et) ;
    void updateBatch(List<Res_groups> list) ;
    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    boolean create(Res_groups et) ;
    void createBatch(List<Res_groups> list) ;
    Page<Res_groups> searchDefault(Res_groupsSearchContext context) ;

}



