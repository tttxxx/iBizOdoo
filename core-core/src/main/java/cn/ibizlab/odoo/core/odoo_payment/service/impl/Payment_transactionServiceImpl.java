package cn.ibizlab.odoo.core.odoo_payment.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_payment.domain.Payment_transaction;
import cn.ibizlab.odoo.core.odoo_payment.filter.Payment_transactionSearchContext;
import cn.ibizlab.odoo.core.odoo_payment.service.IPayment_transactionService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;



import cn.ibizlab.odoo.core.odoo_payment.client.payment_transactionOdooClient;
import cn.ibizlab.odoo.core.odoo_payment.clientmodel.payment_transactionClientModel;

/**
 * 实体[付款交易] 服务对象接口实现
 */
@Slf4j
@Service
public class Payment_transactionServiceImpl implements IPayment_transactionService {

    @Autowired
    payment_transactionOdooClient payment_transactionOdooClient;


    @Override
    public boolean create(Payment_transaction et) {
        payment_transactionClientModel clientModel = convert2Model(et,null);
		payment_transactionOdooClient.create(clientModel);
        Payment_transaction rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Payment_transaction> list){
    }

    @Override
    public boolean remove(Integer id) {
        payment_transactionClientModel clientModel = new payment_transactionClientModel();
        clientModel.setId(id);
		payment_transactionOdooClient.remove(clientModel);
        return true; 
    }

    public void removeBatch(Collection<Integer> idList){
    }

    @Override
    public Payment_transaction get(Integer id) {
        payment_transactionClientModel clientModel = new payment_transactionClientModel();
        clientModel.setId(id);
		payment_transactionOdooClient.get(clientModel);
        Payment_transaction et = convert2Domain(clientModel,null);
        if(et==null){
            et=new Payment_transaction();
            et.setId(id);
        }
        else{
        }

        return  et;
    }

    @Override
    public boolean update(Payment_transaction et) {
        payment_transactionClientModel clientModel = convert2Model(et,null);
		payment_transactionOdooClient.update(clientModel);
        Payment_transaction rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }
    
    public void updateBatch(List<Payment_transaction> list){
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Payment_transaction> searchDefault(Payment_transactionSearchContext context) {
        List<Payment_transaction> list = new ArrayList<Payment_transaction>();
        Page<payment_transactionClientModel> clientModelList = payment_transactionOdooClient.search(context);
        for(payment_transactionClientModel clientModel : clientModelList){
            list.add( this.convert2Domain(clientModel,null));
        }
        return new PageImpl<Payment_transaction>(list, context.getPageable(), clientModelList.getTotalElements());
    }

    public payment_transactionClientModel convert2Model(Payment_transaction domain , payment_transactionClientModel model) {
        if(domain == null)
            return model ;
        if(model==null){
            model = new payment_transactionClientModel();

        }
        if((Boolean) domain.getExtensionparams().get("dirtyflagenable")){
            if((Boolean) domain.getExtensionparams().get("iddirtyflag"))
                model.setId(domain.getId());
            if((Boolean) domain.getExtensionparams().get("amountdirtyflag"))
                model.setAmount(domain.getAmount());
            if((Boolean) domain.getExtensionparams().get("partner_phonedirtyflag"))
                model.setPartner_phone(domain.getPartnerPhone());
            if((Boolean) domain.getExtensionparams().get("partner_emaildirtyflag"))
                model.setPartner_email(domain.getPartnerEmail());
            if((Boolean) domain.getExtensionparams().get("callback_methoddirtyflag"))
                model.setCallback_method(domain.getCallbackMethod());
            if((Boolean) domain.getExtensionparams().get("return_urldirtyflag"))
                model.setReturn_url(domain.getReturnUrl());
            if((Boolean) domain.getExtensionparams().get("sale_order_ids_nbrdirtyflag"))
                model.setSale_order_ids_nbr(domain.getSaleOrderIdsNbr());
            if((Boolean) domain.getExtensionparams().get("acquirer_referencedirtyflag"))
                model.setAcquirer_reference(domain.getAcquirerReference());
            if((Boolean) domain.getExtensionparams().get("partner_langdirtyflag"))
                model.setPartner_lang(domain.getPartnerLang());
            if((Boolean) domain.getExtensionparams().get("partner_zipdirtyflag"))
                model.setPartner_zip(domain.getPartnerZip());
            if((Boolean) domain.getExtensionparams().get("display_namedirtyflag"))
                model.setDisplay_name(domain.getDisplayName());
            if((Boolean) domain.getExtensionparams().get("statedirtyflag"))
                model.setState(domain.getState());
            if((Boolean) domain.getExtensionparams().get("datedirtyflag"))
                model.setDate(domain.getDate());
            if((Boolean) domain.getExtensionparams().get("callback_model_iddirtyflag"))
                model.setCallback_model_id(domain.getCallbackModelId());
            if((Boolean) domain.getExtensionparams().get("__last_updatedirtyflag"))
                model.set__last_update(domain.getLastUpdate());
            if((Boolean) domain.getExtensionparams().get("invoice_idsdirtyflag"))
                model.setInvoice_ids(domain.getInvoiceIds());
            if((Boolean) domain.getExtensionparams().get("sale_order_idsdirtyflag"))
                model.setSale_order_ids(domain.getSaleOrderIds());
            if((Boolean) domain.getExtensionparams().get("feesdirtyflag"))
                model.setFees(domain.getFees());
            if((Boolean) domain.getExtensionparams().get("state_messagedirtyflag"))
                model.setState_message(domain.getStateMessage());
            if((Boolean) domain.getExtensionparams().get("callback_res_iddirtyflag"))
                model.setCallback_res_id(domain.getCallbackResId());
            if((Boolean) domain.getExtensionparams().get("partner_citydirtyflag"))
                model.setPartner_city(domain.getPartnerCity());
            if((Boolean) domain.getExtensionparams().get("write_datedirtyflag"))
                model.setWrite_date(domain.getWriteDate());
            if((Boolean) domain.getExtensionparams().get("create_datedirtyflag"))
                model.setCreate_date(domain.getCreateDate());
            if((Boolean) domain.getExtensionparams().get("invoice_ids_nbrdirtyflag"))
                model.setInvoice_ids_nbr(domain.getInvoiceIdsNbr());
            if((Boolean) domain.getExtensionparams().get("typedirtyflag"))
                model.setType(domain.getType());
            if((Boolean) domain.getExtensionparams().get("html_3dsdirtyflag"))
                model.setHtml_3ds(domain.getHtml3ds());
            if((Boolean) domain.getExtensionparams().get("partner_namedirtyflag"))
                model.setPartner_name(domain.getPartnerName());
            if((Boolean) domain.getExtensionparams().get("referencedirtyflag"))
                model.setReference(domain.getReference());
            if((Boolean) domain.getExtensionparams().get("partner_addressdirtyflag"))
                model.setPartner_address(domain.getPartnerAddress());
            if((Boolean) domain.getExtensionparams().get("is_processeddirtyflag"))
                model.setIs_processed(domain.getIsProcessed());
            if((Boolean) domain.getExtensionparams().get("callback_hashdirtyflag"))
                model.setCallback_hash(domain.getCallbackHash());
            if((Boolean) domain.getExtensionparams().get("currency_id_textdirtyflag"))
                model.setCurrency_id_text(domain.getCurrencyIdText());
            if((Boolean) domain.getExtensionparams().get("write_uid_textdirtyflag"))
                model.setWrite_uid_text(domain.getWriteUidText());
            if((Boolean) domain.getExtensionparams().get("create_uid_textdirtyflag"))
                model.setCreate_uid_text(domain.getCreateUidText());
            if((Boolean) domain.getExtensionparams().get("payment_id_textdirtyflag"))
                model.setPayment_id_text(domain.getPaymentIdText());
            if((Boolean) domain.getExtensionparams().get("partner_id_textdirtyflag"))
                model.setPartner_id_text(domain.getPartnerIdText());
            if((Boolean) domain.getExtensionparams().get("providerdirtyflag"))
                model.setProvider(domain.getProvider());
            if((Boolean) domain.getExtensionparams().get("acquirer_id_textdirtyflag"))
                model.setAcquirer_id_text(domain.getAcquirerIdText());
            if((Boolean) domain.getExtensionparams().get("partner_country_id_textdirtyflag"))
                model.setPartner_country_id_text(domain.getPartnerCountryIdText());
            if((Boolean) domain.getExtensionparams().get("payment_token_id_textdirtyflag"))
                model.setPayment_token_id_text(domain.getPaymentTokenIdText());
            if((Boolean) domain.getExtensionparams().get("currency_iddirtyflag"))
                model.setCurrency_id(domain.getCurrencyId());
            if((Boolean) domain.getExtensionparams().get("write_uiddirtyflag"))
                model.setWrite_uid(domain.getWriteUid());
            if((Boolean) domain.getExtensionparams().get("partner_country_iddirtyflag"))
                model.setPartner_country_id(domain.getPartnerCountryId());
            if((Boolean) domain.getExtensionparams().get("payment_token_iddirtyflag"))
                model.setPayment_token_id(domain.getPaymentTokenId());
            if((Boolean) domain.getExtensionparams().get("partner_iddirtyflag"))
                model.setPartner_id(domain.getPartnerId());
            if((Boolean) domain.getExtensionparams().get("create_uiddirtyflag"))
                model.setCreate_uid(domain.getCreateUid());
            if((Boolean) domain.getExtensionparams().get("payment_iddirtyflag"))
                model.setPayment_id(domain.getPaymentId());
            if((Boolean) domain.getExtensionparams().get("acquirer_iddirtyflag"))
                model.setAcquirer_id(domain.getAcquirerId());
            domain.getFocusNull().clear();
        }
        return model ;
    }

    public Payment_transaction convert2Domain( payment_transactionClientModel model ,Payment_transaction domain) {
        if(model == null)
            return domain ;
        if(domain == null){
            domain = new Payment_transaction();
        }

        if(model.getIdDirtyFlag())
            domain.setId(model.getId());
        if(model.getAmountDirtyFlag())
            domain.setAmount(model.getAmount());
        if(model.getPartner_phoneDirtyFlag())
            domain.setPartnerPhone(model.getPartner_phone());
        if(model.getPartner_emailDirtyFlag())
            domain.setPartnerEmail(model.getPartner_email());
        if(model.getCallback_methodDirtyFlag())
            domain.setCallbackMethod(model.getCallback_method());
        if(model.getReturn_urlDirtyFlag())
            domain.setReturnUrl(model.getReturn_url());
        if(model.getSale_order_ids_nbrDirtyFlag())
            domain.setSaleOrderIdsNbr(model.getSale_order_ids_nbr());
        if(model.getAcquirer_referenceDirtyFlag())
            domain.setAcquirerReference(model.getAcquirer_reference());
        if(model.getPartner_langDirtyFlag())
            domain.setPartnerLang(model.getPartner_lang());
        if(model.getPartner_zipDirtyFlag())
            domain.setPartnerZip(model.getPartner_zip());
        if(model.getDisplay_nameDirtyFlag())
            domain.setDisplayName(model.getDisplay_name());
        if(model.getStateDirtyFlag())
            domain.setState(model.getState());
        if(model.getDateDirtyFlag())
            domain.setDate(model.getDate());
        if(model.getCallback_model_idDirtyFlag())
            domain.setCallbackModelId(model.getCallback_model_id());
        if(model.get__last_updateDirtyFlag())
            domain.setLastUpdate(model.get__last_update());
        if(model.getInvoice_idsDirtyFlag())
            domain.setInvoiceIds(model.getInvoice_ids());
        if(model.getSale_order_idsDirtyFlag())
            domain.setSaleOrderIds(model.getSale_order_ids());
        if(model.getFeesDirtyFlag())
            domain.setFees(model.getFees());
        if(model.getState_messageDirtyFlag())
            domain.setStateMessage(model.getState_message());
        if(model.getCallback_res_idDirtyFlag())
            domain.setCallbackResId(model.getCallback_res_id());
        if(model.getPartner_cityDirtyFlag())
            domain.setPartnerCity(model.getPartner_city());
        if(model.getWrite_dateDirtyFlag())
            domain.setWriteDate(model.getWrite_date());
        if(model.getCreate_dateDirtyFlag())
            domain.setCreateDate(model.getCreate_date());
        if(model.getInvoice_ids_nbrDirtyFlag())
            domain.setInvoiceIdsNbr(model.getInvoice_ids_nbr());
        if(model.getTypeDirtyFlag())
            domain.setType(model.getType());
        if(model.getHtml_3dsDirtyFlag())
            domain.setHtml3ds(model.getHtml_3ds());
        if(model.getPartner_nameDirtyFlag())
            domain.setPartnerName(model.getPartner_name());
        if(model.getReferenceDirtyFlag())
            domain.setReference(model.getReference());
        if(model.getPartner_addressDirtyFlag())
            domain.setPartnerAddress(model.getPartner_address());
        if(model.getIs_processedDirtyFlag())
            domain.setIsProcessed(model.getIs_processed());
        if(model.getCallback_hashDirtyFlag())
            domain.setCallbackHash(model.getCallback_hash());
        if(model.getCurrency_id_textDirtyFlag())
            domain.setCurrencyIdText(model.getCurrency_id_text());
        if(model.getWrite_uid_textDirtyFlag())
            domain.setWriteUidText(model.getWrite_uid_text());
        if(model.getCreate_uid_textDirtyFlag())
            domain.setCreateUidText(model.getCreate_uid_text());
        if(model.getPayment_id_textDirtyFlag())
            domain.setPaymentIdText(model.getPayment_id_text());
        if(model.getPartner_id_textDirtyFlag())
            domain.setPartnerIdText(model.getPartner_id_text());
        if(model.getProviderDirtyFlag())
            domain.setProvider(model.getProvider());
        if(model.getAcquirer_id_textDirtyFlag())
            domain.setAcquirerIdText(model.getAcquirer_id_text());
        if(model.getPartner_country_id_textDirtyFlag())
            domain.setPartnerCountryIdText(model.getPartner_country_id_text());
        if(model.getPayment_token_id_textDirtyFlag())
            domain.setPaymentTokenIdText(model.getPayment_token_id_text());
        if(model.getCurrency_idDirtyFlag())
            domain.setCurrencyId(model.getCurrency_id());
        if(model.getWrite_uidDirtyFlag())
            domain.setWriteUid(model.getWrite_uid());
        if(model.getPartner_country_idDirtyFlag())
            domain.setPartnerCountryId(model.getPartner_country_id());
        if(model.getPayment_token_idDirtyFlag())
            domain.setPaymentTokenId(model.getPayment_token_id());
        if(model.getPartner_idDirtyFlag())
            domain.setPartnerId(model.getPartner_id());
        if(model.getCreate_uidDirtyFlag())
            domain.setCreateUid(model.getCreate_uid());
        if(model.getPayment_idDirtyFlag())
            domain.setPaymentId(model.getPayment_id());
        if(model.getAcquirer_idDirtyFlag())
            domain.setAcquirerId(model.getAcquirer_id());
        return domain ;
    }

}

    



