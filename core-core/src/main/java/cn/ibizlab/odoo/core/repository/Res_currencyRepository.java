package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Res_currency;
import cn.ibizlab.odoo.core.odoo_base.filter.Res_currencySearchContext;

/**
 * 实体 [币种] 存储对象
 */
public interface Res_currencyRepository extends Repository<Res_currency> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Res_currency> searchDefault(Res_currencySearchContext context);

    Res_currency convert2PO(cn.ibizlab.odoo.core.odoo_base.domain.Res_currency domain , Res_currency po) ;

    cn.ibizlab.odoo.core.odoo_base.domain.Res_currency convert2Domain( Res_currency po ,cn.ibizlab.odoo.core.odoo_base.domain.Res_currency domain) ;

}
