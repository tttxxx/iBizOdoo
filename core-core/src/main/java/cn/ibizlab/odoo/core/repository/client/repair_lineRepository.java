package cn.ibizlab.odoo.core.repository.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import cn.ibizlab.odoo.core.repository.client.po.repair_line;

/**
 * 实体[repair_line] 服务对象接口
 */
public interface repair_lineRepository{


    public repair_line createPO() ;
        public void updateBatch(repair_line repair_line);

        public void createBatch(repair_line repair_line);

        public void get(String id);

        public void update(repair_line repair_line);

        public List<repair_line> search();

        public void removeBatch(String id);

        public void create(repair_line repair_line);

        public void remove(String id);


}
