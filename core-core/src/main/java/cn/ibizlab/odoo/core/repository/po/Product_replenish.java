package cn.ibizlab.odoo.core.repository.po ;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.math.BigInteger;

import cn.ibizlab.odoo.core.odoo_product.filter.Product_replenishSearchContext;

/**
 * 实体 [补料] 存储模型
 */
public interface Product_replenish{

    /**
     * 最后修改日
     */
    Timestamp get__last_update();

    void set__last_update(Timestamp __last_update);

    /**
     * 获取 [最后修改日]脏标记
     */
    boolean get__last_updateDirtyFlag();

    /**
     * 有变体
     */
    String getProduct_has_variants();

    void setProduct_has_variants(String product_has_variants);

    /**
     * 获取 [有变体]脏标记
     */
    boolean getProduct_has_variantsDirtyFlag();

    /**
     * 显示名称
     */
    String getDisplay_name();

    void setDisplay_name(String display_name);

    /**
     * 获取 [显示名称]脏标记
     */
    boolean getDisplay_nameDirtyFlag();

    /**
     * 数量
     */
    Double getQuantity();

    void setQuantity(Double quantity);

    /**
     * 获取 [数量]脏标记
     */
    boolean getQuantityDirtyFlag();

    /**
     * 最后更新时间
     */
    Timestamp getWrite_date();

    void setWrite_date(Timestamp write_date);

    /**
     * 获取 [最后更新时间]脏标记
     */
    boolean getWrite_dateDirtyFlag();

    /**
     * ID
     */
    Integer getId();

    void setId(Integer id);

    /**
     * 获取 [ID]脏标记
     */
    boolean getIdDirtyFlag();

    /**
     * 类别
     */
    Integer getProduct_uom_category_id();

    void setProduct_uom_category_id(Integer product_uom_category_id);

    /**
     * 获取 [类别]脏标记
     */
    boolean getProduct_uom_category_idDirtyFlag();

    /**
     * 计划日期
     */
    Timestamp getDate_planned();

    void setDate_planned(Timestamp date_planned);

    /**
     * 获取 [计划日期]脏标记
     */
    boolean getDate_plannedDirtyFlag();

    /**
     * 创建时间
     */
    Timestamp getCreate_date();

    void setCreate_date(Timestamp create_date);

    /**
     * 获取 [创建时间]脏标记
     */
    boolean getCreate_dateDirtyFlag();

    /**
     * 首选路线
     */
    String getRoute_ids();

    void setRoute_ids(String route_ids);

    /**
     * 获取 [首选路线]脏标记
     */
    boolean getRoute_idsDirtyFlag();

    /**
     * 创建人
     */
    String getCreate_uid_text();

    void setCreate_uid_text(String create_uid_text);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uid_textDirtyFlag();

    /**
     * 计量单位
     */
    String getProduct_uom_id_text();

    void setProduct_uom_id_text(String product_uom_id_text);

    /**
     * 获取 [计量单位]脏标记
     */
    boolean getProduct_uom_id_textDirtyFlag();

    /**
     * 产品模板
     */
    String getProduct_tmpl_id_text();

    void setProduct_tmpl_id_text(String product_tmpl_id_text);

    /**
     * 获取 [产品模板]脏标记
     */
    boolean getProduct_tmpl_id_textDirtyFlag();

    /**
     * 最后更新人
     */
    String getWrite_uid_text();

    void setWrite_uid_text(String write_uid_text);

    /**
     * 获取 [最后更新人]脏标记
     */
    boolean getWrite_uid_textDirtyFlag();

    /**
     * 仓库
     */
    String getWarehouse_id_text();

    void setWarehouse_id_text(String warehouse_id_text);

    /**
     * 获取 [仓库]脏标记
     */
    boolean getWarehouse_id_textDirtyFlag();

    /**
     * 产品
     */
    String getProduct_id_text();

    void setProduct_id_text(String product_id_text);

    /**
     * 获取 [产品]脏标记
     */
    boolean getProduct_id_textDirtyFlag();

    /**
     * 最后更新人
     */
    Integer getWrite_uid();

    void setWrite_uid(Integer write_uid);

    /**
     * 获取 [最后更新人]脏标记
     */
    boolean getWrite_uidDirtyFlag();

    /**
     * 产品模板
     */
    Integer getProduct_tmpl_id();

    void setProduct_tmpl_id(Integer product_tmpl_id);

    /**
     * 获取 [产品模板]脏标记
     */
    boolean getProduct_tmpl_idDirtyFlag();

    /**
     * 创建人
     */
    Integer getCreate_uid();

    void setCreate_uid(Integer create_uid);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uidDirtyFlag();

    /**
     * 计量单位
     */
    Integer getProduct_uom_id();

    void setProduct_uom_id(Integer product_uom_id);

    /**
     * 获取 [计量单位]脏标记
     */
    boolean getProduct_uom_idDirtyFlag();

    /**
     * 产品
     */
    Integer getProduct_id();

    void setProduct_id(Integer product_id);

    /**
     * 获取 [产品]脏标记
     */
    boolean getProduct_idDirtyFlag();

    /**
     * 仓库
     */
    Integer getWarehouse_id();

    void setWarehouse_id(Integer warehouse_id);

    /**
     * 获取 [仓库]脏标记
     */
    boolean getWarehouse_idDirtyFlag();

}
