package cn.ibizlab.odoo.core.repository.po ;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.math.BigInteger;

import cn.ibizlab.odoo.core.odoo_gamification.filter.Gamification_goal_definitionSearchContext;

/**
 * 实体 [游戏化目标定义] 存储模型
 */
public interface Gamification_goal_definition{

    /**
     * 后缀
     */
    String getSuffix();

    void setSuffix(String suffix);

    /**
     * 获取 [后缀]脏标记
     */
    boolean getSuffixDirtyFlag();

    /**
     * 批量用户的特有字段
     */
    Integer getBatch_distinctive_field();

    void setBatch_distinctive_field(Integer batch_distinctive_field);

    /**
     * 获取 [批量用户的特有字段]脏标记
     */
    boolean getBatch_distinctive_fieldDirtyFlag();

    /**
     * 金钱值
     */
    String getMonetary();

    void setMonetary(String monetary);

    /**
     * 获取 [金钱值]脏标记
     */
    boolean getMonetaryDirtyFlag();

    /**
     * 目标绩效
     */
    String getCondition();

    void setCondition(String condition);

    /**
     * 获取 [目标绩效]脏标记
     */
    boolean getConditionDirtyFlag();

    /**
     * Python 代码
     */
    String getCompute_code();

    void setCompute_code(String compute_code);

    /**
     * 获取 [Python 代码]脏标记
     */
    boolean getCompute_codeDirtyFlag();

    /**
     * 计算模式
     */
    String getComputation_mode();

    void setComputation_mode(String computation_mode);

    /**
     * 获取 [计算模式]脏标记
     */
    boolean getComputation_modeDirtyFlag();

    /**
     * 动作
     */
    Integer getAction_id();

    void setAction_id(Integer action_id);

    /**
     * 获取 [动作]脏标记
     */
    boolean getAction_idDirtyFlag();

    /**
     * ID
     */
    Integer getId();

    void setId(Integer id);

    /**
     * 获取 [ID]脏标记
     */
    boolean getIdDirtyFlag();

    /**
     * 最后修改日
     */
    Timestamp get__last_update();

    void set__last_update(Timestamp __last_update);

    /**
     * 获取 [最后修改日]脏标记
     */
    boolean get__last_updateDirtyFlag();

    /**
     * 筛选域
     */
    String getDomain();

    void setDomain(String domain);

    /**
     * 获取 [筛选域]脏标记
     */
    boolean getDomainDirtyFlag();

    /**
     * 最后更新时间
     */
    Timestamp getWrite_date();

    void setWrite_date(Timestamp write_date);

    /**
     * 获取 [最后更新时间]脏标记
     */
    boolean getWrite_dateDirtyFlag();

    /**
     * 字段总计
     */
    Integer getField_id();

    void setField_id(Integer field_id);

    /**
     * 获取 [字段总计]脏标记
     */
    boolean getField_idDirtyFlag();

    /**
     * 用户ID字段
     */
    String getRes_id_field();

    void setRes_id_field(String res_id_field);

    /**
     * 获取 [用户ID字段]脏标记
     */
    boolean getRes_id_fieldDirtyFlag();

    /**
     * 批量模式
     */
    String getBatch_mode();

    void setBatch_mode(String batch_mode);

    /**
     * 获取 [批量模式]脏标记
     */
    boolean getBatch_modeDirtyFlag();

    /**
     * 模型
     */
    Integer getModel_id();

    void setModel_id(Integer model_id);

    /**
     * 获取 [模型]脏标记
     */
    boolean getModel_idDirtyFlag();

    /**
     * 目标说明
     */
    String getDescription();

    void setDescription(String description);

    /**
     * 获取 [目标说明]脏标记
     */
    boolean getDescriptionDirtyFlag();

    /**
     * 日期字段
     */
    Integer getField_date_id();

    void setField_date_id(Integer field_date_id);

    /**
     * 获取 [日期字段]脏标记
     */
    boolean getField_date_idDirtyFlag();

    /**
     * 显示为
     */
    String getDisplay_mode();

    void setDisplay_mode(String display_mode);

    /**
     * 获取 [显示为]脏标记
     */
    boolean getDisplay_modeDirtyFlag();

    /**
     * 完整的后缀
     */
    String getFull_suffix();

    void setFull_suffix(String full_suffix);

    /**
     * 获取 [完整的后缀]脏标记
     */
    boolean getFull_suffixDirtyFlag();

    /**
     * 显示名称
     */
    String getDisplay_name();

    void setDisplay_name(String display_name);

    /**
     * 获取 [显示名称]脏标记
     */
    boolean getDisplay_nameDirtyFlag();

    /**
     * 批处理模式的求值表达式
     */
    String getBatch_user_expression();

    void setBatch_user_expression(String batch_user_expression);

    /**
     * 获取 [批处理模式的求值表达式]脏标记
     */
    boolean getBatch_user_expressionDirtyFlag();

    /**
     * 创建时间
     */
    Timestamp getCreate_date();

    void setCreate_date(Timestamp create_date);

    /**
     * 获取 [创建时间]脏标记
     */
    boolean getCreate_dateDirtyFlag();

    /**
     * 目标定义
     */
    String getName();

    void setName(String name);

    /**
     * 获取 [目标定义]脏标记
     */
    boolean getNameDirtyFlag();

    /**
     * 创建人
     */
    String getCreate_uid_text();

    void setCreate_uid_text(String create_uid_text);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uid_textDirtyFlag();

    /**
     * 最后更新人
     */
    String getWrite_uid_text();

    void setWrite_uid_text(String write_uid_text);

    /**
     * 获取 [最后更新人]脏标记
     */
    boolean getWrite_uid_textDirtyFlag();

    /**
     * 最后更新人
     */
    Integer getWrite_uid();

    void setWrite_uid(Integer write_uid);

    /**
     * 获取 [最后更新人]脏标记
     */
    boolean getWrite_uidDirtyFlag();

    /**
     * 创建人
     */
    Integer getCreate_uid();

    void setCreate_uid(Integer create_uid);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uidDirtyFlag();

}
