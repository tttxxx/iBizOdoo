package cn.ibizlab.odoo.core.odoo_base.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.odoo.util.domain.EntityBase;
import cn.ibizlab.odoo.util.annotation.DEField;
import cn.ibizlab.odoo.util.enums.DEPredefinedFieldType;
import cn.ibizlab.odoo.util.enums.DEFieldDefaultValueType;
import java.io.Serializable;
import lombok.Data;
import org.springframework.data.annotation.Transient;

import cn.ibizlab.odoo.util.domain.EntityClient;

/**
 * ServiceApi [联系人] 对象
 */
@Data
public class Res_partner extends EntityClient implements Serializable {

    /**
     * 图像
     */
    @JSONField(name = "image")
    @JsonProperty("image")
    private byte[] image;

    /**
     * 地址类型
     */
    @JSONField(name = "type")
    @JsonProperty("type")
    private String type;

    /**
     * 颜色索引
     */
    @JSONField(name = "color")
    @JsonProperty("color")
    private Integer color;

    /**
     * 付款令牌
     */
    @JSONField(name = "payment_token_ids")
    @JsonProperty("payment_token_ids")
    private String paymentTokenIds;

    /**
     * 发票
     */
    @JSONField(name = "invoice_ids")
    @JsonProperty("invoice_ids")
    private String invoiceIds;

    /**
     * #会议
     */
    @JSONField(name = "meeting_count")
    @JsonProperty("meeting_count")
    private Integer meetingCount;

    /**
     * ＃供应商账单
     */
    @JSONField(name = "supplier_invoice_count")
    @JsonProperty("supplier_invoice_count")
    private Integer supplierInvoiceCount;

    /**
     * 公司名称
     */
    @DEField(name = "company_name")
    @JSONField(name = "company_name")
    @JsonProperty("company_name")
    private String companyName;

    /**
     * 在当前网站显示
     */
    @JSONField(name = "website_published")
    @JsonProperty("website_published")
    private String websitePublished;

    /**
     * 最近的发票和付款匹配时间
     */
    @DEField(name = "last_time_entries_checked")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "last_time_entries_checked" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("last_time_entries_checked")
    private Timestamp lastTimeEntriesChecked;

    /**
     * 未读消息
     */
    @JSONField(name = "message_unread")
    @JsonProperty("message_unread")
    private String messageUnread;

    /**
     * 对此债务人的信任度
     */
    @JSONField(name = "trust")
    @JsonProperty("trust")
    private String trust;

    /**
     * 工作岗位
     */
    @JSONField(name = "ibizfunction")
    @JsonProperty("ibizfunction")
    private String ibizfunction;

    /**
     * 已开票总计
     */
    @JSONField(name = "total_invoiced")
    @JsonProperty("total_invoiced")
    private Double totalInvoiced;

    /**
     * 销售点订单计数
     */
    @JSONField(name = "pos_order_count")
    @JsonProperty("pos_order_count")
    private Integer posOrderCount;

    /**
     * 完整地址
     */
    @JSONField(name = "contact_address")
    @JsonProperty("contact_address")
    private String contactAddress;

    /**
     * 发票
     */
    @DEField(name = "invoice_warn")
    @JSONField(name = "invoice_warn")
    @JsonProperty("invoice_warn")
    private String invoiceWarn;

    /**
     * 银行
     */
    @JSONField(name = "bank_ids")
    @JsonProperty("bank_ids")
    private String bankIds;

    /**
     * 注册到期
     */
    @DEField(name = "signup_expiration")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "signup_expiration" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("signup_expiration")
    private Timestamp signupExpiration;

    /**
     * 采购订单数
     */
    @JSONField(name = "purchase_order_count")
    @JsonProperty("purchase_order_count")
    private Integer purchaseOrderCount;

    /**
     * 有未核销的分录
     */
    @JSONField(name = "has_unreconciled_entries")
    @JsonProperty("has_unreconciled_entries")
    private String hasUnreconciledEntries;

    /**
     * 标签
     */
    @JSONField(name = "category_id")
    @JsonProperty("category_id")
    private String categoryId;

    /**
     * 网站业务伙伴的详细说明
     */
    @DEField(name = "website_description")
    @JSONField(name = "website_description")
    @JsonProperty("website_description")
    private String websiteDescription;

    /**
     * 附件
     */
    @DEField(name = "message_main_attachment_id")
    @JSONField(name = "message_main_attachment_id")
    @JsonProperty("message_main_attachment_id")
    private Integer messageMainAttachmentId;

    /**
     * 会议
     */
    @JSONField(name = "meeting_ids")
    @JsonProperty("meeting_ids")
    private String meetingIds;

    /**
     * 员工
     */
    @JSONField(name = "employee")
    @JsonProperty("employee")
    private String employee;

    /**
     * 显示名称
     */
    @DEField(name = "display_name")
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    private String displayName;

    /**
     * 联系人
     */
    @JSONField(name = "child_ids")
    @JsonProperty("child_ids")
    private String childIds;

    /**
     * 网站元说明
     */
    @DEField(name = "website_meta_description")
    @JSONField(name = "website_meta_description")
    @JsonProperty("website_meta_description")
    private String websiteMetaDescription;

    /**
     * 黑名单
     */
    @JSONField(name = "is_blacklisted")
    @JsonProperty("is_blacklisted")
    private String isBlacklisted;

    /**
     * 价格表
     */
    @JSONField(name = "property_product_pricelist")
    @JsonProperty("property_product_pricelist")
    private Integer propertyProductPricelist;

    /**
     * 下一活动截止日期
     */
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "activity_date_deadline" , format="yyyy-MM-dd")
    @JsonProperty("activity_date_deadline")
    private Timestamp activityDateDeadline;

    /**
     * 下一活动类型
     */
    @JSONField(name = "activity_type_id")
    @JsonProperty("activity_type_id")
    private Integer activityTypeId;

    /**
     * 注册令牌 Token
     */
    @DEField(name = "signup_token")
    @JSONField(name = "signup_token")
    @JsonProperty("signup_token")
    private String signupToken;

    /**
     * 公司是指业务伙伴
     */
    @JSONField(name = "ref_company_ids")
    @JsonProperty("ref_company_ids")
    private String refCompanyIds;

    /**
     * 公司
     */
    @DEField(name = "is_company")
    @JSONField(name = "is_company")
    @JsonProperty("is_company")
    private String isCompany;

    /**
     * 电话
     */
    @JSONField(name = "phone")
    @JsonProperty("phone")
    private String phone;

    /**
     * 创建时间
     */
    @DEField(name = "create_date" , preType = DEPredefinedFieldType.CREATEDATE)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;

    /**
     * 时区
     */
    @JSONField(name = "tz")
    @JsonProperty("tz")
    private String tz;

    /**
     * 活动
     */
    @JSONField(name = "event_count")
    @JsonProperty("event_count")
    private Integer eventCount;

    /**
     * 消息递送错误
     */
    @JSONField(name = "message_has_error")
    @JsonProperty("message_has_error")
    private String messageHasError;

    /**
     * 最后的提醒已经标志为已读
     */
    @DEField(name = "calendar_last_notif_ack")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "calendar_last_notif_ack" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("calendar_last_notif_ack")
    private Timestamp calendarLastNotifAck;

    /**
     * 关注者(渠道)
     */
    @JSONField(name = "message_channel_ids")
    @JsonProperty("message_channel_ids")
    private String messageChannelIds;

    /**
     * 注册令牌（Token）类型
     */
    @DEField(name = "signup_type")
    @JSONField(name = "signup_type")
    @JsonProperty("signup_type")
    private String signupType;

    /**
     * 格式化的邮件
     */
    @JSONField(name = "email_formatted")
    @JsonProperty("email_formatted")
    private String emailFormatted;

    /**
     * 网站消息
     */
    @JSONField(name = "website_message_ids")
    @JsonProperty("website_message_ids")
    private String websiteMessageIds;

    /**
     * 共享合作伙伴
     */
    @DEField(name = "partner_share")
    @JSONField(name = "partner_share")
    @JsonProperty("partner_share")
    private String partnerShare;

    /**
     * 街道 2
     */
    @JSONField(name = "street2")
    @JsonProperty("street2")
    private String street2;

    /**
     * 应付总计
     */
    @JSONField(name = "debit")
    @JsonProperty("debit")
    private Double debit;

    /**
     * 付款令牌计数
     */
    @JSONField(name = "payment_token_count")
    @JsonProperty("payment_token_count")
    private Integer paymentTokenCount;

    /**
     * 内部参考
     */
    @JSONField(name = "ref")
    @JsonProperty("ref")
    private String ref;

    /**
     * 公司数据库ID
     */
    @DEField(name = "partner_gid")
    @JSONField(name = "partner_gid")
    @JsonProperty("partner_gid")
    private Integer partnerGid;

    /**
     * 注册令牌（ Token  ）是有效的
     */
    @JSONField(name = "signup_valid")
    @JsonProperty("signup_valid")
    private String signupValid;

    /**
     * 网站opengraph图像
     */
    @DEField(name = "website_meta_og_img")
    @JSONField(name = "website_meta_og_img")
    @JsonProperty("website_meta_og_img")
    private String websiteMetaOgImg;

    /**
     * 小尺寸图像
     */
    @JSONField(name = "image_small")
    @JsonProperty("image_small")
    private byte[] imageSmall;

    /**
     * 银行
     */
    @JSONField(name = "bank_account_count")
    @JsonProperty("bank_account_count")
    private Integer bankAccountCount;

    /**
     * 街道
     */
    @JSONField(name = "street")
    @JsonProperty("street")
    private String street;

    /**
     * 销售警告
     */
    @DEField(name = "sale_warn")
    @JSONField(name = "sale_warn")
    @JsonProperty("sale_warn")
    private String saleWarn;

    /**
     * 退回
     */
    @DEField(name = "message_bounce")
    @JSONField(name = "message_bounce")
    @JsonProperty("message_bounce")
    private Integer messageBounce;

    /**
     * 操作次数
     */
    @JSONField(name = "message_needaction_counter")
    @JsonProperty("message_needaction_counter")
    private Integer messageNeedactionCounter;

    /**
     * 关注者
     */
    @JSONField(name = "message_follower_ids")
    @JsonProperty("message_follower_ids")
    private String messageFollowerIds;

    /**
     * 商机
     */
    @JSONField(name = "opportunity_count")
    @JsonProperty("opportunity_count")
    private Integer opportunityCount;

    /**
     * 日期
     */
    @JsonFormat(pattern="yyyy-MM-dd", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "date" , format="yyyy-MM-dd")
    @JsonProperty("date")
    private Timestamp date;

    /**
     * 最后修改日
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;

    /**
     * 关注者(业务伙伴)
     */
    @JSONField(name = "message_partner_ids")
    @JsonProperty("message_partner_ids")
    private String messagePartnerIds;

    /**
     * 自己
     */
    @JSONField(name = "self")
    @JsonProperty("self")
    private Integer self;

    /**
     * IM的状态
     */
    @JSONField(name = "im_status")
    @JsonProperty("im_status")
    private String imStatus;

    /**
     * 客户
     */
    @JSONField(name = "customer")
    @JsonProperty("customer")
    private String customer;

    /**
     * 最后更新时间
     */
    @DEField(name = "write_date" , preType = DEPredefinedFieldType.UPDATEDATE)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;

    /**
     * 错误个数
     */
    @JSONField(name = "message_has_error_counter")
    @JsonProperty("message_has_error_counter")
    private Integer messageHasErrorCounter;

    /**
     * 发票消息
     */
    @DEField(name = "invoice_warn_msg")
    @JSONField(name = "invoice_warn_msg")
    @JsonProperty("invoice_warn_msg")
    private String invoiceWarnMsg;

    /**
     * 前置操作
     */
    @JSONField(name = "message_needaction")
    @JsonProperty("message_needaction")
    private String messageNeedaction;

    /**
     * 库存拣货
     */
    @DEField(name = "picking_warn")
    @JSONField(name = "picking_warn")
    @JsonProperty("picking_warn")
    private String pickingWarn;

    /**
     * 客户合同
     */
    @JSONField(name = "contract_ids")
    @JsonProperty("contract_ids")
    private String contractIds;

    /**
     * 币种
     */
    @JSONField(name = "currency_id")
    @JsonProperty("currency_id")
    private Integer currencyId;

    /**
     * 网站
     */
    @JSONField(name = "website")
    @JsonProperty("website")
    private String website;

    /**
     * 手机
     */
    @JSONField(name = "mobile")
    @JsonProperty("mobile")
    private String mobile;

    /**
     * 附件数量
     */
    @JSONField(name = "message_attachment_count")
    @JsonProperty("message_attachment_count")
    private Integer messageAttachmentCount;

    /**
     * 城市
     */
    @JSONField(name = "city")
    @JsonProperty("city")
    private String city;

    /**
     * 客户付款条款
     */
    @JSONField(name = "property_payment_term_id")
    @JsonProperty("property_payment_term_id")
    private Integer propertyPaymentTermId;

    /**
     * 用户
     */
    @JSONField(name = "user_ids")
    @JsonProperty("user_ids")
    private String userIds;

    /**
     * 网站meta关键词
     */
    @DEField(name = "website_meta_keywords")
    @JSONField(name = "website_meta_keywords")
    @JsonProperty("website_meta_keywords")
    private String websiteMetaKeywords;

    /**
     * 渠道
     */
    @JSONField(name = "channel_ids")
    @JsonProperty("channel_ids")
    private String channelIds;

    /**
     * 采购订单
     */
    @DEField(name = "purchase_warn")
    @JSONField(name = "purchase_warn")
    @JsonProperty("purchase_warn")
    private String purchaseWarn;

    /**
     * 日记账项目
     */
    @JSONField(name = "journal_item_count")
    @JsonProperty("journal_item_count")
    private Integer journalItemCount;

    /**
     * 供应商
     */
    @JSONField(name = "supplier")
    @JsonProperty("supplier")
    private String supplier;

    /**
     * 供应商位置
     */
    @JSONField(name = "property_stock_supplier")
    @JsonProperty("property_stock_supplier")
    private Integer propertyStockSupplier;

    /**
     * 应付账款
     */
    @JSONField(name = "property_account_payable_id")
    @JsonProperty("property_account_payable_id")
    private Integer propertyAccountPayableId;

    /**
     * 网站业务伙伴简介
     */
    @DEField(name = "website_short_description")
    @JSONField(name = "website_short_description")
    @JsonProperty("website_short_description")
    private String websiteShortDescription;

    /**
     * 销售订单消息
     */
    @DEField(name = "sale_warn_msg")
    @JSONField(name = "sale_warn_msg")
    @JsonProperty("sale_warn_msg")
    private String saleWarnMsg;

    /**
     * 应收总计
     */
    @JSONField(name = "credit")
    @JsonProperty("credit")
    private Double credit;

    /**
     * 活动状态
     */
    @JSONField(name = "activity_state")
    @JsonProperty("activity_state")
    private String activityState;

    /**
     * 活动
     */
    @JSONField(name = "activity_ids")
    @JsonProperty("activity_ids")
    private String activityIds;

    /**
     * 关注者
     */
    @JSONField(name = "message_is_follower")
    @JsonProperty("message_is_follower")
    private String messageIsFollower;

    /**
     * 名称
     */
    @JSONField(name = "name")
    @JsonProperty("name")
    private String name;

    /**
     * 税号
     */
    @JSONField(name = "vat")
    @JsonProperty("vat")
    private String vat;

    /**
     * 供应商付款条款
     */
    @JSONField(name = "property_supplier_payment_term_id")
    @JsonProperty("property_supplier_payment_term_id")
    private Integer propertySupplierPaymentTermId;

    /**
     * 客户位置
     */
    @JSONField(name = "property_stock_customer")
    @JsonProperty("property_stock_customer")
    private Integer propertyStockCustomer;

    /**
     * 便签
     */
    @JSONField(name = "comment")
    @JsonProperty("comment")
    private String comment;

    /**
     * 任务
     */
    @JSONField(name = "task_ids")
    @JsonProperty("task_ids")
    private String taskIds;

    /**
     * 未读消息计数器
     */
    @JSONField(name = "message_unread_counter")
    @JsonProperty("message_unread_counter")
    private Integer messageUnreadCounter;

    /**
     * EMail
     */
    @JSONField(name = "email")
    @JsonProperty("email")
    private String email;

    /**
     * 采购订单消息
     */
    @DEField(name = "purchase_warn_msg")
    @JSONField(name = "purchase_warn_msg")
    @JsonProperty("purchase_warn_msg")
    private String purchaseWarnMsg;

    /**
     * 网站meta标题
     */
    @DEField(name = "website_meta_title")
    @JSONField(name = "website_meta_title")
    @JsonProperty("website_meta_title")
    private String websiteMetaTitle;

    /**
     * 邮政编码
     */
    @JSONField(name = "zip")
    @JsonProperty("zip")
    private String zip;

    /**
     * 时区偏移
     */
    @JSONField(name = "tz_offset")
    @JsonProperty("tz_offset")
    private String tzOffset;

    /**
     * 公司类别
     */
    @JSONField(name = "company_type")
    @JsonProperty("company_type")
    private String companyType;

    /**
     * 下一个活动摘要
     */
    @JSONField(name = "activity_summary")
    @JsonProperty("activity_summary")
    private String activitySummary;

    /**
     * # 任务
     */
    @JSONField(name = "task_count")
    @JsonProperty("task_count")
    private Integer taskCount;

    /**
     * 信用额度
     */
    @DEField(name = "credit_limit")
    @JSONField(name = "credit_limit")
    @JsonProperty("credit_limit")
    private Double creditLimit;

    /**
     * 应收账款
     */
    @JSONField(name = "property_account_receivable_id")
    @JsonProperty("property_account_receivable_id")
    private Integer propertyAccountReceivableId;

    /**
     * 供应商货币
     */
    @JSONField(name = "property_purchase_currency_id")
    @JsonProperty("property_purchase_currency_id")
    private Integer propertyPurchaseCurrencyId;

    /**
     * 库存拣货单消息
     */
    @DEField(name = "picking_warn_msg")
    @JSONField(name = "picking_warn_msg")
    @JsonProperty("picking_warn_msg")
    private String pickingWarnMsg;

    /**
     * ID
     */
    @DEField(isKeyField=true)
    @JSONField(name = "id")
    @JsonProperty("id")
    private Integer id;

    /**
     * 注册网址
     */
    @JSONField(name = "signup_url")
    @JsonProperty("signup_url")
    private String signupUrl;

    /**
     * 语言
     */
    @JSONField(name = "lang")
    @JsonProperty("lang")
    private String lang;

    /**
     * 消息
     */
    @JSONField(name = "message_ids")
    @JsonProperty("message_ids")
    private String messageIds;

    /**
     * 税科目调整
     */
    @JSONField(name = "property_account_position_id")
    @JsonProperty("property_account_position_id")
    private Integer propertyAccountPositionId;

    /**
     * 登记网站
     */
    @DEField(name = "website_id")
    @JSONField(name = "website_id")
    @JsonProperty("website_id")
    private Integer websiteId;

    /**
     * 有效
     */
    @JSONField(name = "active")
    @JsonProperty("active")
    private String active;

    /**
     * 条码
     */
    @JSONField(name = "barcode")
    @JsonProperty("barcode")
    private String barcode;

    /**
     * 已发布
     */
    @DEField(name = "is_published")
    @JSONField(name = "is_published")
    @JsonProperty("is_published")
    private String isPublished;

    /**
     * 责任用户
     */
    @JSONField(name = "activity_user_id")
    @JsonProperty("activity_user_id")
    private Integer activityUserId;

    /**
     * 销售订单个数
     */
    @JSONField(name = "sale_order_count")
    @JsonProperty("sale_order_count")
    private Integer saleOrderCount;

    /**
     * 中等尺寸图像
     */
    @JSONField(name = "image_medium")
    @JsonProperty("image_medium")
    private byte[] imageMedium;

    /**
     * 附加信息
     */
    @DEField(name = "additional_info")
    @JSONField(name = "additional_info")
    @JsonProperty("additional_info")
    private String additionalInfo;

    /**
     * 商机
     */
    @JSONField(name = "opportunity_ids")
    @JsonProperty("opportunity_ids")
    private String opportunityIds;

    /**
     * 合同统计
     */
    @JSONField(name = "contracts_count")
    @JsonProperty("contracts_count")
    private Integer contractsCount;

    /**
     * 应付限额
     */
    @DEField(name = "debit_limit")
    @JSONField(name = "debit_limit")
    @JsonProperty("debit_limit")
    private Double debitLimit;

    /**
     * 网站网址
     */
    @JSONField(name = "website_url")
    @JsonProperty("website_url")
    private String websiteUrl;

    /**
     * 销售订单
     */
    @JSONField(name = "sale_order_ids")
    @JsonProperty("sale_order_ids")
    private String saleOrderIds;

    /**
     * 最近的在线销售订单
     */
    @JSONField(name = "last_website_so_id")
    @JsonProperty("last_website_so_id")
    private Integer lastWebsiteSoId;

    /**
     * SEO优化
     */
    @JSONField(name = "is_seo_optimized")
    @JsonProperty("is_seo_optimized")
    private String isSeoOptimized;

    /**
     * 公司名称实体
     */
    @DEField(name = "commercial_company_name")
    @JSONField(name = "commercial_company_name")
    @JsonProperty("commercial_company_name")
    private String commercialCompanyName;

    /**
     * 最后更新者
     */
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    private String writeUidText;

    /**
     * 称谓
     */
    @JSONField(name = "title_text")
    @JsonProperty("title_text")
    private String titleText;

    /**
     * 公司
     */
    @JSONField(name = "company_id_text")
    @JsonProperty("company_id_text")
    private String companyIdText;

    /**
     * 国家/地区
     */
    @JSONField(name = "country_id_text")
    @JsonProperty("country_id_text")
    private String countryIdText;

    /**
     * 省/ 州
     */
    @JSONField(name = "state_id_text")
    @JsonProperty("state_id_text")
    private String stateIdText;

    /**
     * 商业实体
     */
    @JSONField(name = "commercial_partner_id_text")
    @JsonProperty("commercial_partner_id_text")
    private String commercialPartnerIdText;

    /**
     * 上级名称
     */
    @JSONField(name = "parent_name")
    @JsonProperty("parent_name")
    private String parentName;

    /**
     * 销售员
     */
    @JSONField(name = "user_id_text")
    @JsonProperty("user_id_text")
    private String userIdText;

    /**
     * 创建人
     */
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    private String createUidText;

    /**
     * 工业
     */
    @JSONField(name = "industry_id_text")
    @JsonProperty("industry_id_text")
    private String industryIdText;

    /**
     * 销售团队
     */
    @JSONField(name = "team_id_text")
    @JsonProperty("team_id_text")
    private String teamIdText;

    /**
     * 销售团队
     */
    @DEField(name = "team_id")
    @JSONField(name = "team_id")
    @JsonProperty("team_id")
    private Integer teamId;

    /**
     * 省/ 州
     */
    @DEField(name = "state_id")
    @JSONField(name = "state_id")
    @JsonProperty("state_id")
    private Integer stateId;

    /**
     * 销售员
     */
    @DEField(name = "user_id")
    @JSONField(name = "user_id")
    @JsonProperty("user_id")
    private Integer userId;

    /**
     * 创建人
     */
    @DEField(name = "create_uid" , preType = DEPredefinedFieldType.CREATEMAN)
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    private Integer createUid;

    /**
     * 关联公司
     */
    @DEField(name = "parent_id")
    @JSONField(name = "parent_id")
    @JsonProperty("parent_id")
    private Integer parentId;

    /**
     * 称谓
     */
    @JSONField(name = "title")
    @JsonProperty("title")
    private Integer title;

    /**
     * 最后更新者
     */
    @DEField(name = "write_uid" , preType = DEPredefinedFieldType.UPDATEMAN)
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    private Integer writeUid;

    /**
     * 商业实体
     */
    @DEField(name = "commercial_partner_id")
    @JSONField(name = "commercial_partner_id")
    @JsonProperty("commercial_partner_id")
    private Integer commercialPartnerId;

    /**
     * 工业
     */
    @DEField(name = "industry_id")
    @JSONField(name = "industry_id")
    @JsonProperty("industry_id")
    private Integer industryId;

    /**
     * 公司
     */
    @DEField(name = "company_id")
    @JSONField(name = "company_id")
    @JsonProperty("company_id")
    private Integer companyId;

    /**
     * 国家/地区
     */
    @DEField(name = "country_id")
    @JSONField(name = "country_id")
    @JsonProperty("country_id")
    private Integer countryId;


    /**
     * 
     */
    @JSONField(name = "odooteam")
    @JsonProperty("odooteam")
    private cn.ibizlab.odoo.core.odoo_crm.domain.Crm_team odooTeam;

    /**
     * 
     */
    @JSONField(name = "odoocompany")
    @JsonProperty("odoocompany")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_company odooCompany;

    /**
     * 
     */
    @JSONField(name = "odoostate")
    @JsonProperty("odoostate")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_country_state odooState;

    /**
     * 
     */
    @JSONField(name = "odoocountry")
    @JsonProperty("odoocountry")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_country odooCountry;

    /**
     * 
     */
    @JSONField(name = "odooindustry")
    @JsonProperty("odooindustry")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_partner_industry odooIndustry;

    /**
     * 
     */
    @JSONField(name = "odootitle")
    @JsonProperty("odootitle")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_partner_title odooTitle;

    /**
     * 
     */
    @JSONField(name = "odoocommercialpartner")
    @JsonProperty("odoocommercialpartner")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_partner odooCommercialPartner;

    /**
     * 
     */
    @JSONField(name = "odooparent")
    @JsonProperty("odooparent")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_partner odooParent;

    /**
     * 
     */
    @JSONField(name = "odoocreate")
    @JsonProperty("odoocreate")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_users odooCreate;

    /**
     * 
     */
    @JSONField(name = "odoouser")
    @JsonProperty("odoouser")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_users odooUser;

    /**
     * 
     */
    @JSONField(name = "odoowrite")
    @JsonProperty("odoowrite")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_users odooWrite;




    /**
     * 设置 [地址类型]
     */
    public void setType(String type){
        this.type = type ;
        this.modify("type",type);
    }
    /**
     * 设置 [颜色索引]
     */
    public void setColor(Integer color){
        this.color = color ;
        this.modify("color",color);
    }
    /**
     * 设置 [公司名称]
     */
    public void setCompanyName(String companyName){
        this.companyName = companyName ;
        this.modify("company_name",companyName);
    }
    /**
     * 设置 [最近的发票和付款匹配时间]
     */
    public void setLastTimeEntriesChecked(Timestamp lastTimeEntriesChecked){
        this.lastTimeEntriesChecked = lastTimeEntriesChecked ;
        this.modify("last_time_entries_checked",lastTimeEntriesChecked);
    }
    /**
     * 设置 [工作岗位]
     */
    public void setIbizfunction(String ibizfunction){
        this.ibizfunction = ibizfunction ;
        this.modify("ibizfunction",ibizfunction);
    }
    /**
     * 设置 [发票]
     */
    public void setInvoiceWarn(String invoiceWarn){
        this.invoiceWarn = invoiceWarn ;
        this.modify("invoice_warn",invoiceWarn);
    }
    /**
     * 设置 [注册到期]
     */
    public void setSignupExpiration(Timestamp signupExpiration){
        this.signupExpiration = signupExpiration ;
        this.modify("signup_expiration",signupExpiration);
    }
    /**
     * 设置 [网站业务伙伴的详细说明]
     */
    public void setWebsiteDescription(String websiteDescription){
        this.websiteDescription = websiteDescription ;
        this.modify("website_description",websiteDescription);
    }
    /**
     * 设置 [附件]
     */
    public void setMessageMainAttachmentId(Integer messageMainAttachmentId){
        this.messageMainAttachmentId = messageMainAttachmentId ;
        this.modify("message_main_attachment_id",messageMainAttachmentId);
    }
    /**
     * 设置 [员工]
     */
    public void setEmployee(String employee){
        this.employee = employee ;
        this.modify("employee",employee);
    }
    /**
     * 设置 [显示名称]
     */
    public void setDisplayName(String displayName){
        this.displayName = displayName ;
        this.modify("display_name",displayName);
    }
    /**
     * 设置 [网站元说明]
     */
    public void setWebsiteMetaDescription(String websiteMetaDescription){
        this.websiteMetaDescription = websiteMetaDescription ;
        this.modify("website_meta_description",websiteMetaDescription);
    }
    /**
     * 设置 [注册令牌 Token]
     */
    public void setSignupToken(String signupToken){
        this.signupToken = signupToken ;
        this.modify("signup_token",signupToken);
    }
    /**
     * 设置 [公司]
     */
    public void setIsCompany(String isCompany){
        this.isCompany = isCompany ;
        this.modify("is_company",isCompany);
    }
    /**
     * 设置 [电话]
     */
    public void setPhone(String phone){
        this.phone = phone ;
        this.modify("phone",phone);
    }
    /**
     * 设置 [时区]
     */
    public void setTz(String tz){
        this.tz = tz ;
        this.modify("tz",tz);
    }
    /**
     * 设置 [最后的提醒已经标志为已读]
     */
    public void setCalendarLastNotifAck(Timestamp calendarLastNotifAck){
        this.calendarLastNotifAck = calendarLastNotifAck ;
        this.modify("calendar_last_notif_ack",calendarLastNotifAck);
    }
    /**
     * 设置 [注册令牌（Token）类型]
     */
    public void setSignupType(String signupType){
        this.signupType = signupType ;
        this.modify("signup_type",signupType);
    }
    /**
     * 设置 [共享合作伙伴]
     */
    public void setPartnerShare(String partnerShare){
        this.partnerShare = partnerShare ;
        this.modify("partner_share",partnerShare);
    }
    /**
     * 设置 [街道 2]
     */
    public void setStreet2(String street2){
        this.street2 = street2 ;
        this.modify("street2",street2);
    }
    /**
     * 设置 [内部参考]
     */
    public void setRef(String ref){
        this.ref = ref ;
        this.modify("ref",ref);
    }
    /**
     * 设置 [公司数据库ID]
     */
    public void setPartnerGid(Integer partnerGid){
        this.partnerGid = partnerGid ;
        this.modify("partner_gid",partnerGid);
    }
    /**
     * 设置 [网站opengraph图像]
     */
    public void setWebsiteMetaOgImg(String websiteMetaOgImg){
        this.websiteMetaOgImg = websiteMetaOgImg ;
        this.modify("website_meta_og_img",websiteMetaOgImg);
    }
    /**
     * 设置 [街道]
     */
    public void setStreet(String street){
        this.street = street ;
        this.modify("street",street);
    }
    /**
     * 设置 [销售警告]
     */
    public void setSaleWarn(String saleWarn){
        this.saleWarn = saleWarn ;
        this.modify("sale_warn",saleWarn);
    }
    /**
     * 设置 [退回]
     */
    public void setMessageBounce(Integer messageBounce){
        this.messageBounce = messageBounce ;
        this.modify("message_bounce",messageBounce);
    }
    /**
     * 设置 [日期]
     */
    public void setDate(Timestamp date){
        this.date = date ;
        this.modify("date",date);
    }
    /**
     * 设置 [客户]
     */
    public void setCustomer(String customer){
        this.customer = customer ;
        this.modify("customer",customer);
    }
    /**
     * 设置 [发票消息]
     */
    public void setInvoiceWarnMsg(String invoiceWarnMsg){
        this.invoiceWarnMsg = invoiceWarnMsg ;
        this.modify("invoice_warn_msg",invoiceWarnMsg);
    }
    /**
     * 设置 [库存拣货]
     */
    public void setPickingWarn(String pickingWarn){
        this.pickingWarn = pickingWarn ;
        this.modify("picking_warn",pickingWarn);
    }
    /**
     * 设置 [网站]
     */
    public void setWebsite(String website){
        this.website = website ;
        this.modify("website",website);
    }
    /**
     * 设置 [手机]
     */
    public void setMobile(String mobile){
        this.mobile = mobile ;
        this.modify("mobile",mobile);
    }
    /**
     * 设置 [城市]
     */
    public void setCity(String city){
        this.city = city ;
        this.modify("city",city);
    }
    /**
     * 设置 [网站meta关键词]
     */
    public void setWebsiteMetaKeywords(String websiteMetaKeywords){
        this.websiteMetaKeywords = websiteMetaKeywords ;
        this.modify("website_meta_keywords",websiteMetaKeywords);
    }
    /**
     * 设置 [采购订单]
     */
    public void setPurchaseWarn(String purchaseWarn){
        this.purchaseWarn = purchaseWarn ;
        this.modify("purchase_warn",purchaseWarn);
    }
    /**
     * 设置 [供应商]
     */
    public void setSupplier(String supplier){
        this.supplier = supplier ;
        this.modify("supplier",supplier);
    }
    /**
     * 设置 [网站业务伙伴简介]
     */
    public void setWebsiteShortDescription(String websiteShortDescription){
        this.websiteShortDescription = websiteShortDescription ;
        this.modify("website_short_description",websiteShortDescription);
    }
    /**
     * 设置 [销售订单消息]
     */
    public void setSaleWarnMsg(String saleWarnMsg){
        this.saleWarnMsg = saleWarnMsg ;
        this.modify("sale_warn_msg",saleWarnMsg);
    }
    /**
     * 设置 [名称]
     */
    public void setName(String name){
        this.name = name ;
        this.modify("name",name);
    }
    /**
     * 设置 [税号]
     */
    public void setVat(String vat){
        this.vat = vat ;
        this.modify("vat",vat);
    }
    /**
     * 设置 [便签]
     */
    public void setComment(String comment){
        this.comment = comment ;
        this.modify("comment",comment);
    }
    /**
     * 设置 [EMail]
     */
    public void setEmail(String email){
        this.email = email ;
        this.modify("email",email);
    }
    /**
     * 设置 [采购订单消息]
     */
    public void setPurchaseWarnMsg(String purchaseWarnMsg){
        this.purchaseWarnMsg = purchaseWarnMsg ;
        this.modify("purchase_warn_msg",purchaseWarnMsg);
    }
    /**
     * 设置 [网站meta标题]
     */
    public void setWebsiteMetaTitle(String websiteMetaTitle){
        this.websiteMetaTitle = websiteMetaTitle ;
        this.modify("website_meta_title",websiteMetaTitle);
    }
    /**
     * 设置 [邮政编码]
     */
    public void setZip(String zip){
        this.zip = zip ;
        this.modify("zip",zip);
    }
    /**
     * 设置 [信用额度]
     */
    public void setCreditLimit(Double creditLimit){
        this.creditLimit = creditLimit ;
        this.modify("credit_limit",creditLimit);
    }
    /**
     * 设置 [库存拣货单消息]
     */
    public void setPickingWarnMsg(String pickingWarnMsg){
        this.pickingWarnMsg = pickingWarnMsg ;
        this.modify("picking_warn_msg",pickingWarnMsg);
    }
    /**
     * 设置 [语言]
     */
    public void setLang(String lang){
        this.lang = lang ;
        this.modify("lang",lang);
    }
    /**
     * 设置 [登记网站]
     */
    public void setWebsiteId(Integer websiteId){
        this.websiteId = websiteId ;
        this.modify("website_id",websiteId);
    }
    /**
     * 设置 [有效]
     */
    public void setActive(String active){
        this.active = active ;
        this.modify("active",active);
    }
    /**
     * 设置 [条码]
     */
    public void setBarcode(String barcode){
        this.barcode = barcode ;
        this.modify("barcode",barcode);
    }
    /**
     * 设置 [已发布]
     */
    public void setIsPublished(String isPublished){
        this.isPublished = isPublished ;
        this.modify("is_published",isPublished);
    }
    /**
     * 设置 [附加信息]
     */
    public void setAdditionalInfo(String additionalInfo){
        this.additionalInfo = additionalInfo ;
        this.modify("additional_info",additionalInfo);
    }
    /**
     * 设置 [应付限额]
     */
    public void setDebitLimit(Double debitLimit){
        this.debitLimit = debitLimit ;
        this.modify("debit_limit",debitLimit);
    }
    /**
     * 设置 [公司名称实体]
     */
    public void setCommercialCompanyName(String commercialCompanyName){
        this.commercialCompanyName = commercialCompanyName ;
        this.modify("commercial_company_name",commercialCompanyName);
    }
    /**
     * 设置 [销售团队]
     */
    public void setTeamId(Integer teamId){
        this.teamId = teamId ;
        this.modify("team_id",teamId);
    }
    /**
     * 设置 [省/ 州]
     */
    public void setStateId(Integer stateId){
        this.stateId = stateId ;
        this.modify("state_id",stateId);
    }
    /**
     * 设置 [销售员]
     */
    public void setUserId(Integer userId){
        this.userId = userId ;
        this.modify("user_id",userId);
    }
    /**
     * 设置 [关联公司]
     */
    public void setParentId(Integer parentId){
        this.parentId = parentId ;
        this.modify("parent_id",parentId);
    }
    /**
     * 设置 [称谓]
     */
    public void setTitle(Integer title){
        this.title = title ;
        this.modify("title",title);
    }
    /**
     * 设置 [商业实体]
     */
    public void setCommercialPartnerId(Integer commercialPartnerId){
        this.commercialPartnerId = commercialPartnerId ;
        this.modify("commercial_partner_id",commercialPartnerId);
    }
    /**
     * 设置 [工业]
     */
    public void setIndustryId(Integer industryId){
        this.industryId = industryId ;
        this.modify("industry_id",industryId);
    }
    /**
     * 设置 [公司]
     */
    public void setCompanyId(Integer companyId){
        this.companyId = companyId ;
        this.modify("company_id",companyId);
    }
    /**
     * 设置 [国家/地区]
     */
    public void setCountryId(Integer countryId){
        this.countryId = countryId ;
        this.modify("country_id",countryId);
    }

}


