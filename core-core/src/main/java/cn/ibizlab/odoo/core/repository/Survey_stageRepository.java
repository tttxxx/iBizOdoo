package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Survey_stage;
import cn.ibizlab.odoo.core.odoo_survey.filter.Survey_stageSearchContext;

/**
 * 实体 [调查阶段] 存储对象
 */
public interface Survey_stageRepository extends Repository<Survey_stage> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Survey_stage> searchDefault(Survey_stageSearchContext context);

    Survey_stage convert2PO(cn.ibizlab.odoo.core.odoo_survey.domain.Survey_stage domain , Survey_stage po) ;

    cn.ibizlab.odoo.core.odoo_survey.domain.Survey_stage convert2Domain( Survey_stage po ,cn.ibizlab.odoo.core.odoo_survey.domain.Survey_stage domain) ;

}
