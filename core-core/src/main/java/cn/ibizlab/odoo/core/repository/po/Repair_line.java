package cn.ibizlab.odoo.core.repository.po ;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.math.BigInteger;

import cn.ibizlab.odoo.core.odoo_repair.filter.Repair_lineSearchContext;

/**
 * 实体 [修理明细行(零件)] 存储模型
 */
public interface Repair_line{

    /**
     * 显示名称
     */
    String getDisplay_name();

    void setDisplay_name(String display_name);

    /**
     * 获取 [显示名称]脏标记
     */
    boolean getDisplay_nameDirtyFlag();

    /**
     * 最后更新时间
     */
    Timestamp getWrite_date();

    void setWrite_date(Timestamp write_date);

    /**
     * 获取 [最后更新时间]脏标记
     */
    boolean getWrite_dateDirtyFlag();

    /**
     * 描述
     */
    String getName();

    void setName(String name);

    /**
     * 获取 [描述]脏标记
     */
    boolean getNameDirtyFlag();

    /**
     * 数量
     */
    Double getProduct_uom_qty();

    void setProduct_uom_qty(Double product_uom_qty);

    /**
     * 获取 [数量]脏标记
     */
    boolean getProduct_uom_qtyDirtyFlag();

    /**
     * 已开票
     */
    String getInvoiced();

    void setInvoiced(String invoiced);

    /**
     * 获取 [已开票]脏标记
     */
    boolean getInvoicedDirtyFlag();

    /**
     * 状态
     */
    String getState();

    void setState(String state);

    /**
     * 获取 [状态]脏标记
     */
    boolean getStateDirtyFlag();

    /**
     * 类型
     */
    String getType();

    void setType(String type);

    /**
     * 获取 [类型]脏标记
     */
    boolean getTypeDirtyFlag();

    /**
     * 单价
     */
    Double getPrice_unit();

    void setPrice_unit(Double price_unit);

    /**
     * 获取 [单价]脏标记
     */
    boolean getPrice_unitDirtyFlag();

    /**
     * 最后修改时间
     */
    Timestamp get__last_update();

    void set__last_update(Timestamp __last_update);

    /**
     * 获取 [最后修改时间]脏标记
     */
    boolean get__last_updateDirtyFlag();

    /**
     * 创建时间
     */
    Timestamp getCreate_date();

    void setCreate_date(Timestamp create_date);

    /**
     * 获取 [创建时间]脏标记
     */
    boolean getCreate_dateDirtyFlag();

    /**
     * ID
     */
    Integer getId();

    void setId(Integer id);

    /**
     * 获取 [ID]脏标记
     */
    boolean getIdDirtyFlag();

    /**
     * 税
     */
    String getTax_id();

    void setTax_id(String tax_id);

    /**
     * 获取 [税]脏标记
     */
    boolean getTax_idDirtyFlag();

    /**
     * 小计
     */
    Double getPrice_subtotal();

    void setPrice_subtotal(Double price_subtotal);

    /**
     * 获取 [小计]脏标记
     */
    boolean getPrice_subtotalDirtyFlag();

    /**
     * 目的地的位置
     */
    String getLocation_dest_id_text();

    void setLocation_dest_id_text(String location_dest_id_text);

    /**
     * 获取 [目的地的位置]脏标记
     */
    boolean getLocation_dest_id_textDirtyFlag();

    /**
     * 最后更新人
     */
    String getWrite_uid_text();

    void setWrite_uid_text(String write_uid_text);

    /**
     * 获取 [最后更新人]脏标记
     */
    boolean getWrite_uid_textDirtyFlag();

    /**
     * 库存移动
     */
    String getMove_id_text();

    void setMove_id_text(String move_id_text);

    /**
     * 获取 [库存移动]脏标记
     */
    boolean getMove_id_textDirtyFlag();

    /**
     * 源位置
     */
    String getLocation_id_text();

    void setLocation_id_text(String location_id_text);

    /**
     * 获取 [源位置]脏标记
     */
    boolean getLocation_id_textDirtyFlag();

    /**
     * 批次/序列号
     */
    String getLot_id_text();

    void setLot_id_text(String lot_id_text);

    /**
     * 获取 [批次/序列号]脏标记
     */
    boolean getLot_id_textDirtyFlag();

    /**
     * 产品量度单位
     */
    String getProduct_uom_text();

    void setProduct_uom_text(String product_uom_text);

    /**
     * 获取 [产品量度单位]脏标记
     */
    boolean getProduct_uom_textDirtyFlag();

    /**
     * 发票明细
     */
    String getInvoice_line_id_text();

    void setInvoice_line_id_text(String invoice_line_id_text);

    /**
     * 获取 [发票明细]脏标记
     */
    boolean getInvoice_line_id_textDirtyFlag();

    /**
     * 创建者
     */
    String getCreate_uid_text();

    void setCreate_uid_text(String create_uid_text);

    /**
     * 获取 [创建者]脏标记
     */
    boolean getCreate_uid_textDirtyFlag();

    /**
     * 产品
     */
    String getProduct_id_text();

    void setProduct_id_text(String product_id_text);

    /**
     * 获取 [产品]脏标记
     */
    boolean getProduct_id_textDirtyFlag();

    /**
     * 维修单编＃
     */
    String getRepair_id_text();

    void setRepair_id_text(String repair_id_text);

    /**
     * 获取 [维修单编＃]脏标记
     */
    boolean getRepair_id_textDirtyFlag();

    /**
     * 批次/序列号
     */
    Integer getLot_id();

    void setLot_id(Integer lot_id);

    /**
     * 获取 [批次/序列号]脏标记
     */
    boolean getLot_idDirtyFlag();

    /**
     * 维修单编＃
     */
    Integer getRepair_id();

    void setRepair_id(Integer repair_id);

    /**
     * 获取 [维修单编＃]脏标记
     */
    boolean getRepair_idDirtyFlag();

    /**
     * 库存移动
     */
    Integer getMove_id();

    void setMove_id(Integer move_id);

    /**
     * 获取 [库存移动]脏标记
     */
    boolean getMove_idDirtyFlag();

    /**
     * 发票明细
     */
    Integer getInvoice_line_id();

    void setInvoice_line_id(Integer invoice_line_id);

    /**
     * 获取 [发票明细]脏标记
     */
    boolean getInvoice_line_idDirtyFlag();

    /**
     * 产品
     */
    Integer getProduct_id();

    void setProduct_id(Integer product_id);

    /**
     * 获取 [产品]脏标记
     */
    boolean getProduct_idDirtyFlag();

    /**
     * 源位置
     */
    Integer getLocation_id();

    void setLocation_id(Integer location_id);

    /**
     * 获取 [源位置]脏标记
     */
    boolean getLocation_idDirtyFlag();

    /**
     * 创建者
     */
    Integer getCreate_uid();

    void setCreate_uid(Integer create_uid);

    /**
     * 获取 [创建者]脏标记
     */
    boolean getCreate_uidDirtyFlag();

    /**
     * 目的地的位置
     */
    Integer getLocation_dest_id();

    void setLocation_dest_id(Integer location_dest_id);

    /**
     * 获取 [目的地的位置]脏标记
     */
    boolean getLocation_dest_idDirtyFlag();

    /**
     * 最后更新人
     */
    Integer getWrite_uid();

    void setWrite_uid(Integer write_uid);

    /**
     * 获取 [最后更新人]脏标记
     */
    boolean getWrite_uidDirtyFlag();

    /**
     * 产品量度单位
     */
    Integer getProduct_uom();

    void setProduct_uom(Integer product_uom);

    /**
     * 获取 [产品量度单位]脏标记
     */
    boolean getProduct_uomDirtyFlag();

}
