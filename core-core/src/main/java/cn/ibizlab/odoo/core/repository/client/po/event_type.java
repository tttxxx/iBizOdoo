package cn.ibizlab.odoo.core.repository.client.po;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigDecimal;

/**
 * [event_type] 对象
 */
public interface event_type {

    public String getAuto_confirm();

    public void setAuto_confirm(String auto_confirm);

    public Timestamp getCreate_date();

    public void setCreate_date(Timestamp create_date);

    public Integer getCreate_uid();

    public void setCreate_uid(Integer create_uid);

    public String getCreate_uid_text();

    public void setCreate_uid_text(String create_uid_text);

    public String getDefault_hashtag();

    public void setDefault_hashtag(String default_hashtag);

    public Integer getDefault_registration_max();

    public void setDefault_registration_max(Integer default_registration_max);

    public Integer getDefault_registration_min();

    public void setDefault_registration_min(Integer default_registration_min);

    public String getDefault_timezone();

    public void setDefault_timezone(String default_timezone);

    public String getDisplay_name();

    public void setDisplay_name(String display_name);

    public String getEvent_ticket_ids();

    public void setEvent_ticket_ids(String event_ticket_ids);

    public String getEvent_type_mail_ids();

    public void setEvent_type_mail_ids(String event_type_mail_ids);

    public String getHas_seats_limitation();

    public void setHas_seats_limitation(String has_seats_limitation);

    public Integer getId();

    public void setId(Integer id);

    public String getIs_online();

    public void setIs_online(String is_online);

    public String getName();

    public void setName(String name);

    public String getUse_hashtag();

    public void setUse_hashtag(String use_hashtag);

    public String getUse_mail_schedule();

    public void setUse_mail_schedule(String use_mail_schedule);

    public String getUse_ticketing();

    public void setUse_ticketing(String use_ticketing);

    public String getUse_timezone();

    public void setUse_timezone(String use_timezone);

    public String getWebsite_menu();

    public void setWebsite_menu(String website_menu);

    public Timestamp getWrite_date();

    public void setWrite_date(Timestamp write_date);

    public Integer getWrite_uid();

    public void setWrite_uid(Integer write_uid);

    public String getWrite_uid_text();

    public void setWrite_uid_text(String write_uid_text);

    public Timestamp get__last_update();

    public void set__last_update(Timestamp __last_update);

}
