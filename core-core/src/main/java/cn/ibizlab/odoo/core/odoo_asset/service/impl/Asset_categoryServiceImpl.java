package cn.ibizlab.odoo.core.odoo_asset.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_asset.domain.Asset_category;
import cn.ibizlab.odoo.core.odoo_asset.filter.Asset_categorySearchContext;
import cn.ibizlab.odoo.core.odoo_asset.service.IAsset_categoryService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;



import cn.ibizlab.odoo.core.odoo_asset.client.asset_categoryOdooClient;
import cn.ibizlab.odoo.core.odoo_asset.clientmodel.asset_categoryClientModel;

/**
 * 实体[Asset Tags] 服务对象接口实现
 */
@Slf4j
@Service
public class Asset_categoryServiceImpl implements IAsset_categoryService {

    @Autowired
    asset_categoryOdooClient asset_categoryOdooClient;


    @Override
    public Asset_category get(Integer id) {
        asset_categoryClientModel clientModel = new asset_categoryClientModel();
        clientModel.setId(id);
		asset_categoryOdooClient.get(clientModel);
        Asset_category et = convert2Domain(clientModel,null);
        if(et==null){
            et=new Asset_category();
            et.setId(id);
        }
        else{
        }

        return  et;
    }

    @Override
    public boolean remove(Integer id) {
        asset_categoryClientModel clientModel = new asset_categoryClientModel();
        clientModel.setId(id);
		asset_categoryOdooClient.remove(clientModel);
        return true; 
    }

    public void removeBatch(Collection<Integer> idList){
    }

    @Override
    public boolean create(Asset_category et) {
        asset_categoryClientModel clientModel = convert2Model(et,null);
		asset_categoryOdooClient.create(clientModel);
        Asset_category rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Asset_category> list){
    }

    @Override
    public boolean update(Asset_category et) {
        asset_categoryClientModel clientModel = convert2Model(et,null);
		asset_categoryOdooClient.update(clientModel);
        Asset_category rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }
    
    public void updateBatch(List<Asset_category> list){
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Asset_category> searchDefault(Asset_categorySearchContext context) {
        List<Asset_category> list = new ArrayList<Asset_category>();
        Page<asset_categoryClientModel> clientModelList = asset_categoryOdooClient.search(context);
        for(asset_categoryClientModel clientModel : clientModelList){
            list.add( this.convert2Domain(clientModel,null));
        }
        return new PageImpl<Asset_category>(list, context.getPageable(), clientModelList.getTotalElements());
    }

    public asset_categoryClientModel convert2Model(Asset_category domain , asset_categoryClientModel model) {
        if(domain == null)
            return model ;
        if(model==null){
            model = new asset_categoryClientModel();

        }
        if((Boolean) domain.getExtensionparams().get("dirtyflagenable")){
            if((Boolean) domain.getExtensionparams().get("asset_idsdirtyflag"))
                model.setAsset_ids(domain.getAssetIds());
            if((Boolean) domain.getExtensionparams().get("write_datedirtyflag"))
                model.setWrite_date(domain.getWriteDate());
            if((Boolean) domain.getExtensionparams().get("namedirtyflag"))
                model.setName(domain.getName());
            if((Boolean) domain.getExtensionparams().get("display_namedirtyflag"))
                model.setDisplay_name(domain.getDisplayName());
            if((Boolean) domain.getExtensionparams().get("iddirtyflag"))
                model.setId(domain.getId());
            if((Boolean) domain.getExtensionparams().get("__last_updatedirtyflag"))
                model.set__last_update(domain.getLastUpdate());
            if((Boolean) domain.getExtensionparams().get("create_datedirtyflag"))
                model.setCreate_date(domain.getCreateDate());
            if((Boolean) domain.getExtensionparams().get("create_uid_textdirtyflag"))
                model.setCreate_uid_text(domain.getCreateUidText());
            if((Boolean) domain.getExtensionparams().get("write_uid_textdirtyflag"))
                model.setWrite_uid_text(domain.getWriteUidText());
            if((Boolean) domain.getExtensionparams().get("create_uiddirtyflag"))
                model.setCreate_uid(domain.getCreateUid());
            if((Boolean) domain.getExtensionparams().get("write_uiddirtyflag"))
                model.setWrite_uid(domain.getWriteUid());
            domain.getFocusNull().clear();
        }
        return model ;
    }

    public Asset_category convert2Domain( asset_categoryClientModel model ,Asset_category domain) {
        if(model == null)
            return domain ;
        if(domain == null){
            domain = new Asset_category();
        }

        if(model.getAsset_idsDirtyFlag())
            domain.setAssetIds(model.getAsset_ids());
        if(model.getWrite_dateDirtyFlag())
            domain.setWriteDate(model.getWrite_date());
        if(model.getNameDirtyFlag())
            domain.setName(model.getName());
        if(model.getDisplay_nameDirtyFlag())
            domain.setDisplayName(model.getDisplay_name());
        if(model.getIdDirtyFlag())
            domain.setId(model.getId());
        if(model.get__last_updateDirtyFlag())
            domain.setLastUpdate(model.get__last_update());
        if(model.getCreate_dateDirtyFlag())
            domain.setCreateDate(model.getCreate_date());
        if(model.getCreate_uid_textDirtyFlag())
            domain.setCreateUidText(model.getCreate_uid_text());
        if(model.getWrite_uid_textDirtyFlag())
            domain.setWriteUidText(model.getWrite_uid_text());
        if(model.getCreate_uidDirtyFlag())
            domain.setCreateUid(model.getCreate_uid());
        if(model.getWrite_uidDirtyFlag())
            domain.setWriteUid(model.getWrite_uid());
        return domain ;
    }

}

    



