package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Account_invoice_tax;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_invoice_taxSearchContext;

/**
 * 实体 [发票税率] 存储对象
 */
public interface Account_invoice_taxRepository extends Repository<Account_invoice_tax> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Account_invoice_tax> searchDefault(Account_invoice_taxSearchContext context);

    Account_invoice_tax convert2PO(cn.ibizlab.odoo.core.odoo_account.domain.Account_invoice_tax domain , Account_invoice_tax po) ;

    cn.ibizlab.odoo.core.odoo_account.domain.Account_invoice_tax convert2Domain( Account_invoice_tax po ,cn.ibizlab.odoo.core.odoo_account.domain.Account_invoice_tax domain) ;

}
