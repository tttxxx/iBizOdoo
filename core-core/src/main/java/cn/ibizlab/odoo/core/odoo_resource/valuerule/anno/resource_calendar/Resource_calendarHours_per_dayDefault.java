package cn.ibizlab.odoo.core.odoo_resource.valuerule.anno.resource_calendar;

import cn.ibizlab.odoo.core.odoo_resource.valuerule.validator.resource_calendar.Resource_calendarHours_per_dayDefaultValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

/**
 * 值规则注解
 * 实体：Resource_calendar
 * 属性：Hours_per_day
 * 值规则：Default
 */
@Target({ElementType.METHOD, ElementType.FIELD, ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Constraint(validatedBy = {Resource_calendarHours_per_dayDefaultValidator.class})
public @interface Resource_calendarHours_per_dayDefault {
    //是否默认检查
    boolean flag() default true;

    //值规则名称
    String info() default "默认规则";

    //值规则信息，如果为空，使用值规则名称。
    String message() default "默认规则";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
