package cn.ibizlab.odoo.core.odoo_iap.valuerule.anno.iap_account;

import cn.ibizlab.odoo.core.odoo_iap.valuerule.validator.iap_account.Iap_accountCreate_uid_textDefaultValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

/**
 * 值规则注解
 * 实体：Iap_account
 * 属性：Create_uid_text
 * 值规则：Default
 */
@Target({ElementType.METHOD, ElementType.FIELD, ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Constraint(validatedBy = {Iap_accountCreate_uid_textDefaultValidator.class})
public @interface Iap_accountCreate_uid_textDefault {
    //是否默认检查
    boolean flag() default true;

    //值规则名称
    String info() default "默认规则";

    //值规则信息，如果为空，使用值规则名称。
    String message() default "内容长度必须小于等于[200]";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
