package cn.ibizlab.odoo.core.odoo_maintenance.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_maintenance.domain.Maintenance_equipment;
import cn.ibizlab.odoo.core.odoo_maintenance.filter.Maintenance_equipmentSearchContext;


/**
 * 实体[Maintenance_equipment] 服务对象接口
 */
public interface IMaintenance_equipmentService{

    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    Maintenance_equipment get(Integer key) ;
    boolean create(Maintenance_equipment et) ;
    void createBatch(List<Maintenance_equipment> list) ;
    boolean update(Maintenance_equipment et) ;
    void updateBatch(List<Maintenance_equipment> list) ;
    Page<Maintenance_equipment> searchDefault(Maintenance_equipmentSearchContext context) ;

}



