package cn.ibizlab.odoo.core.repository.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import cn.ibizlab.odoo.core.repository.client.po.lunch_order_line;

/**
 * 实体[lunch_order_line] 服务对象接口
 */
public interface lunch_order_lineRepository{


    public lunch_order_line createPO() ;
        public void get(String id);

        public void createBatch(lunch_order_line lunch_order_line);

        public void remove(String id);

        public void removeBatch(String id);

        public void update(lunch_order_line lunch_order_line);

        public List<lunch_order_line> search();

        public void create(lunch_order_line lunch_order_line);

        public void updateBatch(lunch_order_line lunch_order_line);


}
