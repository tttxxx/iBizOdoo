package cn.ibizlab.odoo.core.repository.po ;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.math.BigInteger;

import cn.ibizlab.odoo.core.odoo_product.filter.Product_productSearchContext;

/**
 * 实体 [产品] 存储模型
 */
public interface Product_product{

    /**
     * 变种卖家
     */
    String getVariant_seller_ids();

    void setVariant_seller_ids(String variant_seller_ids);

    /**
     * 获取 [变种卖家]脏标记
     */
    boolean getVariant_seller_idsDirtyFlag();

    /**
     * 模板属性值
     */
    String getProduct_template_attribute_value_ids();

    void setProduct_template_attribute_value_ids(String product_template_attribute_value_ids);

    /**
     * 获取 [模板属性值]脏标记
     */
    boolean getProduct_template_attribute_value_idsDirtyFlag();

    /**
     * 产品
     */
    String getProduct_variant_ids();

    void setProduct_variant_ids(String product_variant_ids);

    /**
     * 获取 [产品]脏标记
     */
    boolean getProduct_variant_idsDirtyFlag();

    /**
     * 小尺寸图像
     */
    byte[] getImage_small();

    void setImage_small(byte[] image_small);

    /**
     * 获取 [小尺寸图像]脏标记
     */
    boolean getImage_smallDirtyFlag();

    /**
     * 未读消息
     */
    String getMessage_unread();

    void setMessage_unread(String message_unread);

    /**
     * 获取 [未读消息]脏标记
     */
    boolean getMessage_unreadDirtyFlag();

    /**
     * 体积
     */
    Double getVolume();

    void setVolume(Double volume);

    /**
     * 获取 [体积]脏标记
     */
    boolean getVolumeDirtyFlag();

    /**
     * 标价
     */
    Double getLst_price();

    void setLst_price(Double lst_price);

    /**
     * 获取 [标价]脏标记
     */
    boolean getLst_priceDirtyFlag();

    /**
     * 有效的产品属性
     */
    String getValid_product_attribute_ids();

    void setValid_product_attribute_ids(String valid_product_attribute_ids);

    /**
     * 获取 [有效的产品属性]脏标记
     */
    boolean getValid_product_attribute_idsDirtyFlag();

    /**
     * 库存FIFO手工凭证
     */
    String getStock_fifo_manual_move_ids();

    void setStock_fifo_manual_move_ids(String stock_fifo_manual_move_ids);

    /**
     * 获取 [库存FIFO手工凭证]脏标记
     */
    boolean getStock_fifo_manual_move_idsDirtyFlag();

    /**
     * 即时库存
     */
    String getStock_quant_ids();

    void setStock_quant_ids(String stock_quant_ids);

    /**
     * 获取 [即时库存]脏标记
     */
    boolean getStock_quant_idsDirtyFlag();

    /**
     * 进项税
     */
    String getSupplier_taxes_id();

    void setSupplier_taxes_id(String supplier_taxes_id);

    /**
     * 获取 [进项税]脏标记
     */
    boolean getSupplier_taxes_idDirtyFlag();

    /**
     * 价格表明细
     */
    String getPricelist_item_ids();

    void setPricelist_item_ids(String pricelist_item_ids);

    /**
     * 获取 [价格表明细]脏标记
     */
    boolean getPricelist_item_idsDirtyFlag();

    /**
     * 最后修改日
     */
    Timestamp get__last_update();

    void set__last_update(Timestamp __last_update);

    /**
     * 获取 [最后修改日]脏标记
     */
    boolean get__last_updateDirtyFlag();

    /**
     * 附件产品
     */
    String getAccessory_product_ids();

    void setAccessory_product_ids(String accessory_product_ids);

    /**
     * 获取 [附件产品]脏标记
     */
    boolean getAccessory_product_idsDirtyFlag();

    /**
     * 供应商
     */
    String getSeller_ids();

    void setSeller_ids(String seller_ids);

    /**
     * 获取 [供应商]脏标记
     */
    boolean getSeller_idsDirtyFlag();

    /**
     * Valid Product Attribute Values Without No Variant Attributes
     */
    String getValid_product_attribute_value_wnva_ids();

    void setValid_product_attribute_value_wnva_ids(String valid_product_attribute_value_wnva_ids);

    /**
     * 获取 [Valid Product Attribute Values Without No Variant Attributes]脏标记
     */
    boolean getValid_product_attribute_value_wnva_idsDirtyFlag();

    /**
     * 客户单号
     */
    String getPartner_ref();

    void setPartner_ref(String partner_ref);

    /**
     * 获取 [客户单号]脏标记
     */
    boolean getPartner_refDirtyFlag();

    /**
     * 图片
     */
    String getProduct_image_ids();

    void setProduct_image_ids(String product_image_ids);

    /**
     * 获取 [图片]脏标记
     */
    boolean getProduct_image_idsDirtyFlag();

    /**
     * 已生产
     */
    Double getMrp_product_qty();

    void setMrp_product_qty(Double mrp_product_qty);

    /**
     * 获取 [已生产]脏标记
     */
    boolean getMrp_product_qtyDirtyFlag();

    /**
     * 最后更新时间
     */
    Timestamp getWrite_date();

    void setWrite_date(Timestamp write_date);

    /**
     * 获取 [最后更新时间]脏标记
     */
    boolean getWrite_dateDirtyFlag();

    /**
     * Valid Product Attribute Lines
     */
    String getValid_product_template_attribute_line_ids();

    void setValid_product_template_attribute_line_ids(String valid_product_template_attribute_line_ids);

    /**
     * 获取 [Valid Product Attribute Lines]脏标记
     */
    boolean getValid_product_template_attribute_line_idsDirtyFlag();

    /**
     * 下一活动类型
     */
    Integer getActivity_type_id();

    void setActivity_type_id(Integer activity_type_id);

    /**
     * 获取 [下一活动类型]脏标记
     */
    boolean getActivity_type_idDirtyFlag();

    /**
     * 网站产品目录
     */
    String getPublic_categ_ids();

    void setPublic_categ_ids(String public_categ_ids);

    /**
     * 获取 [网站产品目录]脏标记
     */
    boolean getPublic_categ_idsDirtyFlag();

    /**
     * 活动入场券
     */
    String getEvent_ticket_ids();

    void setEvent_ticket_ids(String event_ticket_ids);

    /**
     * 获取 [活动入场券]脏标记
     */
    boolean getEvent_ticket_idsDirtyFlag();

    /**
     * 价格
     */
    Double getPrice();

    void setPrice(Double price);

    /**
     * 获取 [价格]脏标记
     */
    boolean getPriceDirtyFlag();

    /**
     * 产品属性
     */
    String getAttribute_line_ids();

    void setAttribute_line_ids(String attribute_line_ids);

    /**
     * 获取 [产品属性]脏标记
     */
    boolean getAttribute_line_idsDirtyFlag();

    /**
     * 预测数量
     */
    Double getVirtual_available();

    void setVirtual_available(Double virtual_available);

    /**
     * 获取 [预测数量]脏标记
     */
    boolean getVirtual_availableDirtyFlag();

    /**
     * 订货规则
     */
    Integer getNbr_reordering_rules();

    void setNbr_reordering_rules(Integer nbr_reordering_rules);

    /**
     * 获取 [订货规则]脏标记
     */
    boolean getNbr_reordering_rulesDirtyFlag();

    /**
     * 有效
     */
    String getActive();

    void setActive(String active);

    /**
     * 获取 [有效]脏标记
     */
    boolean getActiveDirtyFlag();

    /**
     * 是关注者
     */
    String getMessage_is_follower();

    void setMessage_is_follower(String message_is_follower);

    /**
     * 获取 [是关注者]脏标记
     */
    boolean getMessage_is_followerDirtyFlag();

    /**
     * 未读消息计数器
     */
    Integer getMessage_unread_counter();

    void setMessage_unread_counter(Integer message_unread_counter);

    /**
     * 获取 [未读消息计数器]脏标记
     */
    boolean getMessage_unread_counterDirtyFlag();

    /**
     * ID
     */
    Integer getId();

    void setId(Integer id);

    /**
     * 获取 [ID]脏标记
     */
    boolean getIdDirtyFlag();

    /**
     * 网站价格差异
     */
    String getWebsite_price_difference();

    void setWebsite_price_difference(String website_price_difference);

    /**
     * 获取 [网站价格差异]脏标记
     */
    boolean getWebsite_price_differenceDirtyFlag();

    /**
     * 关注者
     */
    String getMessage_follower_ids();

    void setMessage_follower_ids(String message_follower_ids);

    /**
     * 获取 [关注者]脏标记
     */
    boolean getMessage_follower_idsDirtyFlag();

    /**
     * 购物车数量
     */
    Integer getCart_qty();

    void setCart_qty(Integer cart_qty);

    /**
     * 获取 [购物车数量]脏标记
     */
    boolean getCart_qtyDirtyFlag();

    /**
     * 网站公开价格
     */
    Double getWebsite_public_price();

    void setWebsite_public_price(Double website_public_price);

    /**
     * 获取 [网站公开价格]脏标记
     */
    boolean getWebsite_public_priceDirtyFlag();

    /**
     * 评级
     */
    String getRating_ids();

    void setRating_ids(String rating_ids);

    /**
     * 获取 [评级]脏标记
     */
    boolean getRating_idsDirtyFlag();

    /**
     * BOM组件
     */
    String getBom_line_ids();

    void setBom_line_ids(String bom_line_ids);

    /**
     * 获取 [BOM组件]脏标记
     */
    boolean getBom_line_idsDirtyFlag();

    /**
     * 网站价格
     */
    Double getWebsite_price();

    void setWebsite_price(Double website_price);

    /**
     * 获取 [网站价格]脏标记
     */
    boolean getWebsite_priceDirtyFlag();

    /**
     * 出向
     */
    Double getOutgoing_qty();

    void setOutgoing_qty(Double outgoing_qty);

    /**
     * 获取 [出向]脏标记
     */
    boolean getOutgoing_qtyDirtyFlag();

    /**
     * 已售出
     */
    Double getSales_count();

    void setSales_count(Double sales_count);

    /**
     * 获取 [已售出]脏标记
     */
    boolean getSales_countDirtyFlag();

    /**
     * Valid Product Attributes Without No Variant Attributes
     */
    String getValid_product_attribute_wnva_ids();

    void setValid_product_attribute_wnva_ids(String valid_product_attribute_wnva_ids);

    /**
     * 获取 [Valid Product Attributes Without No Variant Attributes]脏标记
     */
    boolean getValid_product_attribute_wnva_idsDirtyFlag();

    /**
     * 中等尺寸图像
     */
    byte[] getImage_medium();

    void setImage_medium(byte[] image_medium);

    /**
     * 获取 [中等尺寸图像]脏标记
     */
    boolean getImage_mediumDirtyFlag();

    /**
     * Valid Existing Variants
     */
    String getValid_existing_variant_ids();

    void setValid_existing_variant_ids(String valid_existing_variant_ids);

    /**
     * 获取 [Valid Existing Variants]脏标记
     */
    boolean getValid_existing_variant_idsDirtyFlag();

    /**
     * 库存货币价值
     */
    Integer getStock_value_currency_id();

    void setStock_value_currency_id(Integer stock_value_currency_id);

    /**
     * 获取 [库存货币价值]脏标记
     */
    boolean getStock_value_currency_idDirtyFlag();

    /**
     * 值
     */
    Double getStock_value();

    void setStock_value(Double stock_value);

    /**
     * 获取 [值]脏标记
     */
    boolean getStock_valueDirtyFlag();

    /**
     * 样式
     */
    String getWebsite_style_ids();

    void setWebsite_style_ids(String website_style_ids);

    /**
     * 获取 [样式]脏标记
     */
    boolean getWebsite_style_idsDirtyFlag();

    /**
     * 关注者(渠道)
     */
    String getMessage_channel_ids();

    void setMessage_channel_ids(String message_channel_ids);

    /**
     * 获取 [关注者(渠道)]脏标记
     */
    boolean getMessage_channel_idsDirtyFlag();

    /**
     * 重量
     */
    Double getWeight();

    void setWeight(Double weight);

    /**
     * 获取 [重量]脏标记
     */
    boolean getWeightDirtyFlag();

    /**
     * 物料清单
     */
    String getBom_ids();

    void setBom_ids(String bom_ids);

    /**
     * 获取 [物料清单]脏标记
     */
    boolean getBom_idsDirtyFlag();

    /**
     * 错误数
     */
    Integer getMessage_has_error_counter();

    void setMessage_has_error_counter(Integer message_has_error_counter);

    /**
     * 获取 [错误数]脏标记
     */
    boolean getMessage_has_error_counterDirtyFlag();

    /**
     * 活动状态
     */
    String getActivity_state();

    void setActivity_state(String activity_state);

    /**
     * 获取 [活动状态]脏标记
     */
    boolean getActivity_stateDirtyFlag();

    /**
     * Valid Product Attribute Lines Without No Variant Attributes
     */
    String getValid_product_template_attribute_line_wnva_ids();

    void setValid_product_template_attribute_line_wnva_ids(String valid_product_template_attribute_line_wnva_ids);

    /**
     * 获取 [Valid Product Attribute Lines Without No Variant Attributes]脏标记
     */
    boolean getValid_product_template_attribute_line_wnva_idsDirtyFlag();

    /**
     * 附件
     */
    Integer getMessage_main_attachment_id();

    void setMessage_main_attachment_id(Integer message_main_attachment_id);

    /**
     * 获取 [附件]脏标记
     */
    boolean getMessage_main_attachment_idDirtyFlag();

    /**
     * 关注者(业务伙伴)
     */
    String getMessage_partner_ids();

    void setMessage_partner_ids(String message_partner_ids);

    /**
     * 获取 [关注者(业务伙伴)]脏标记
     */
    boolean getMessage_partner_idsDirtyFlag();

    /**
     * 创建时间
     */
    Timestamp getCreate_date();

    void setCreate_date(Timestamp create_date);

    /**
     * 获取 [创建时间]脏标记
     */
    boolean getCreate_dateDirtyFlag();

    /**
     * 有效的产品属性值
     */
    String getValid_product_attribute_value_ids();

    void setValid_product_attribute_value_ids(String valid_product_attribute_value_ids);

    /**
     * 获取 [有效的产品属性值]脏标记
     */
    boolean getValid_product_attribute_value_idsDirtyFlag();

    /**
     * 在手数量
     */
    Double getQty_available();

    void setQty_available(Double qty_available);

    /**
     * 获取 [在手数量]脏标记
     */
    boolean getQty_availableDirtyFlag();

    /**
     * 附件数量
     */
    Integer getMessage_attachment_count();

    void setMessage_attachment_count(Integer message_attachment_count);

    /**
     * 获取 [附件数量]脏标记
     */
    boolean getMessage_attachment_countDirtyFlag();

    /**
     * 变体图像
     */
    byte[] getImage_variant();

    void setImage_variant(byte[] image_variant);

    /**
     * 获取 [变体图像]脏标记
     */
    boolean getImage_variantDirtyFlag();

    /**
     * 库存移动
     */
    String getStock_move_ids();

    void setStock_move_ids(String stock_move_ids);

    /**
     * 获取 [库存移动]脏标记
     */
    boolean getStock_move_idsDirtyFlag();

    /**
     * 需要采取行动
     */
    String getMessage_needaction();

    void setMessage_needaction(String message_needaction);

    /**
     * 获取 [需要采取行动]脏标记
     */
    boolean getMessage_needactionDirtyFlag();

    /**
     * 网站信息
     */
    String getWebsite_message_ids();

    void setWebsite_message_ids(String website_message_ids);

    /**
     * 获取 [网站信息]脏标记
     */
    boolean getWebsite_message_idsDirtyFlag();

    /**
     * 库存FIFO实时计价
     */
    String getStock_fifo_real_time_aml_ids();

    void setStock_fifo_real_time_aml_ids(String stock_fifo_real_time_aml_ids);

    /**
     * 获取 [库存FIFO实时计价]脏标记
     */
    boolean getStock_fifo_real_time_aml_idsDirtyFlag();

    /**
     * 下一活动截止日期
     */
    Timestamp getActivity_date_deadline();

    void setActivity_date_deadline(Timestamp activity_date_deadline);

    /**
     * 获取 [下一活动截止日期]脏标记
     */
    boolean getActivity_date_deadlineDirtyFlag();

    /**
     * 参照
     */
    String getCode();

    void setCode(String code);

    /**
     * 获取 [参照]脏标记
     */
    boolean getCodeDirtyFlag();

    /**
     * 重订货最小数量
     */
    Double getReordering_min_qty();

    void setReordering_min_qty(Double reordering_min_qty);

    /**
     * 获取 [重订货最小数量]脏标记
     */
    boolean getReordering_min_qtyDirtyFlag();

    /**
     * 大尺寸图像
     */
    byte[] getImage();

    void setImage(byte[] image);

    /**
     * 获取 [大尺寸图像]脏标记
     */
    boolean getImageDirtyFlag();

    /**
     * 路线
     */
    String getRoute_ids();

    void setRoute_ids(String route_ids);

    /**
     * 获取 [路线]脏标记
     */
    boolean getRoute_idsDirtyFlag();

    /**
     * 销项税
     */
    String getTaxes_id();

    void setTaxes_id(String taxes_id);

    /**
     * 获取 [销项税]脏标记
     */
    boolean getTaxes_idDirtyFlag();

    /**
     * # 物料清单
     */
    Integer getBom_count();

    void setBom_count(Integer bom_count);

    /**
     * 获取 [# 物料清单]脏标记
     */
    boolean getBom_countDirtyFlag();

    /**
     * 动作数量
     */
    Integer getMessage_needaction_counter();

    void setMessage_needaction_counter(Integer message_needaction_counter);

    /**
     * 获取 [动作数量]脏标记
     */
    boolean getMessage_needaction_counterDirtyFlag();

    /**
     * 产品包裹
     */
    String getPackaging_ids();

    void setPackaging_ids(String packaging_ids);

    /**
     * 获取 [产品包裹]脏标记
     */
    boolean getPackaging_idsDirtyFlag();

    /**
     * Valid Archived Variants
     */
    String getValid_archived_variant_ids();

    void setValid_archived_variant_ids(String valid_archived_variant_ids);

    /**
     * 获取 [Valid Archived Variants]脏标记
     */
    boolean getValid_archived_variant_idsDirtyFlag();

    /**
     * 责任用户
     */
    Integer getActivity_user_id();

    void setActivity_user_id(Integer activity_user_id);

    /**
     * 获取 [责任用户]脏标记
     */
    boolean getActivity_user_idDirtyFlag();

    /**
     * 价格表项目
     */
    String getItem_ids();

    void setItem_ids(String item_ids);

    /**
     * 获取 [价格表项目]脏标记
     */
    boolean getItem_idsDirtyFlag();

    /**
     * 已采购
     */
    Double getPurchased_product_qty();

    void setPurchased_product_qty(Double purchased_product_qty);

    /**
     * 获取 [已采购]脏标记
     */
    boolean getPurchased_product_qtyDirtyFlag();

    /**
     * 重订货最大数量
     */
    Double getReordering_max_qty();

    void setReordering_max_qty(Double reordering_max_qty);

    /**
     * 获取 [重订货最大数量]脏标记
     */
    boolean getReordering_max_qtyDirtyFlag();

    /**
     * 最小库存规则
     */
    String getOrderpoint_ids();

    void setOrderpoint_ids(String orderpoint_ids);

    /**
     * 获取 [最小库存规则]脏标记
     */
    boolean getOrderpoint_idsDirtyFlag();

    /**
     * 可选产品
     */
    String getOptional_product_ids();

    void setOptional_product_ids(String optional_product_ids);

    /**
     * 获取 [可选产品]脏标记
     */
    boolean getOptional_product_idsDirtyFlag();

    /**
     * 是产品变体
     */
    String getIs_product_variant();

    void setIs_product_variant(String is_product_variant);

    /**
     * 获取 [是产品变体]脏标记
     */
    boolean getIs_product_variantDirtyFlag();

    /**
     * # BOM 使用的地方
     */
    Integer getUsed_in_bom_count();

    void setUsed_in_bom_count(Integer used_in_bom_count);

    /**
     * 获取 [# BOM 使用的地方]脏标记
     */
    boolean getUsed_in_bom_countDirtyFlag();

    /**
     * 数量
     */
    Double getQty_at_date();

    void setQty_at_date(Double qty_at_date);

    /**
     * 获取 [数量]脏标记
     */
    boolean getQty_at_dateDirtyFlag();

    /**
     * 消息递送错误
     */
    String getMessage_has_error();

    void setMessage_has_error(String message_has_error);

    /**
     * 获取 [消息递送错误]脏标记
     */
    boolean getMessage_has_errorDirtyFlag();

    /**
     * 活动
     */
    String getActivity_ids();

    void setActivity_ids(String activity_ids);

    /**
     * 获取 [活动]脏标记
     */
    boolean getActivity_idsDirtyFlag();

    /**
     * 消息
     */
    String getMessage_ids();

    void setMessage_ids(String message_ids);

    /**
     * 获取 [消息]脏标记
     */
    boolean getMessage_idsDirtyFlag();

    /**
     * 条码
     */
    String getBarcode();

    void setBarcode(String barcode);

    /**
     * 获取 [条码]脏标记
     */
    boolean getBarcodeDirtyFlag();

    /**
     * 显示名称
     */
    String getDisplay_name();

    void setDisplay_name(String display_name);

    /**
     * 获取 [显示名称]脏标记
     */
    boolean getDisplay_nameDirtyFlag();

    /**
     * 成本
     */
    Double getStandard_price();

    void setStandard_price(Double standard_price);

    /**
     * 获取 [成本]脏标记
     */
    boolean getStandard_priceDirtyFlag();

    /**
     * 属性值
     */
    String getAttribute_value_ids();

    void setAttribute_value_ids(String attribute_value_ids);

    /**
     * 获取 [属性值]脏标记
     */
    boolean getAttribute_value_idsDirtyFlag();

    /**
     * 变体价格额外
     */
    Double getPrice_extra();

    void setPrice_extra(Double price_extra);

    /**
     * 获取 [变体价格额外]脏标记
     */
    boolean getPrice_extraDirtyFlag();

    /**
     * BOM产品变体.
     */
    String getVariant_bom_ids();

    void setVariant_bom_ids(String variant_bom_ids);

    /**
     * 获取 [BOM产品变体.]脏标记
     */
    boolean getVariant_bom_idsDirtyFlag();

    /**
     * 替代产品
     */
    String getAlternative_product_ids();

    void setAlternative_product_ids(String alternative_product_ids);

    /**
     * 获取 [替代产品]脏标记
     */
    boolean getAlternative_product_idsDirtyFlag();

    /**
     * 内部参考
     */
    String getDefault_code();

    void setDefault_code(String default_code);

    /**
     * 获取 [内部参考]脏标记
     */
    boolean getDefault_codeDirtyFlag();

    /**
     * 类别路线
     */
    String getRoute_from_categ_ids();

    void setRoute_from_categ_ids(String route_from_categ_ids);

    /**
     * 获取 [类别路线]脏标记
     */
    boolean getRoute_from_categ_idsDirtyFlag();

    /**
     * 下一活动摘要
     */
    String getActivity_summary();

    void setActivity_summary(String activity_summary);

    /**
     * 获取 [下一活动摘要]脏标记
     */
    boolean getActivity_summaryDirtyFlag();

    /**
     * 入库
     */
    Double getIncoming_qty();

    void setIncoming_qty(Double incoming_qty);

    /**
     * 获取 [入库]脏标记
     */
    boolean getIncoming_qtyDirtyFlag();

    /**
     * 币种
     */
    Integer getCurrency_id();

    void setCurrency_id(Integer currency_id);

    /**
     * 获取 [币种]脏标记
     */
    boolean getCurrency_idDirtyFlag();

    /**
     * 追踪
     */
    String getTracking();

    void setTracking(String tracking);

    /**
     * 获取 [追踪]脏标记
     */
    boolean getTrackingDirtyFlag();

    /**
     * 拣货说明
     */
    String getDescription_picking();

    void setDescription_picking(String description_picking);

    /**
     * 获取 [拣货说明]脏标记
     */
    boolean getDescription_pickingDirtyFlag();

    /**
     * 库存出货科目
     */
    Integer getProperty_stock_account_output();

    void setProperty_stock_account_output(Integer property_stock_account_output);

    /**
     * 获取 [库存出货科目]脏标记
     */
    boolean getProperty_stock_account_outputDirtyFlag();

    /**
     * 销售
     */
    String getSale_ok();

    void setSale_ok(String sale_ok);

    /**
     * 获取 [销售]脏标记
     */
    boolean getSale_okDirtyFlag();

    /**
     * 网站的说明
     */
    String getWebsite_description();

    void setWebsite_description(String website_description);

    /**
     * 获取 [网站的说明]脏标记
     */
    boolean getWebsite_descriptionDirtyFlag();

    /**
     * 网站opengraph图像
     */
    String getWebsite_meta_og_img();

    void setWebsite_meta_og_img(String website_meta_og_img);

    /**
     * 获取 [网站opengraph图像]脏标记
     */
    boolean getWebsite_meta_og_imgDirtyFlag();

    /**
     * 公司
     */
    Integer getCompany_id();

    void setCompany_id(Integer company_id);

    /**
     * 获取 [公司]脏标记
     */
    boolean getCompany_idDirtyFlag();

    /**
     * 称重
     */
    String getTo_weight();

    void setTo_weight(String to_weight);

    /**
     * 获取 [称重]脏标记
     */
    boolean getTo_weightDirtyFlag();

    /**
     * 说明
     */
    String getDescription();

    void setDescription(String description);

    /**
     * 获取 [说明]脏标记
     */
    boolean getDescriptionDirtyFlag();

    /**
     * 收货说明
     */
    String getDescription_pickingin();

    void setDescription_pickingin(String description_pickingin);

    /**
     * 获取 [收货说明]脏标记
     */
    boolean getDescription_pickinginDirtyFlag();

    /**
     * 销售价格
     */
    Double getList_price();

    void setList_price(Double list_price);

    /**
     * 获取 [销售价格]脏标记
     */
    boolean getList_priceDirtyFlag();

    /**
     * 隐藏费用政策
     */
    String getHide_expense_policy();

    void setHide_expense_policy(String hide_expense_policy);

    /**
     * 获取 [隐藏费用政策]脏标记
     */
    boolean getHide_expense_policyDirtyFlag();

    /**
     * 销售说明
     */
    String getDescription_sale();

    void setDescription_sale(String description_sale);

    /**
     * 获取 [销售说明]脏标记
     */
    boolean getDescription_saleDirtyFlag();

    /**
     * 成本方法
     */
    String getCost_method();

    void setCost_method(String cost_method);

    /**
     * 获取 [成本方法]脏标记
     */
    boolean getCost_methodDirtyFlag();

    /**
     * 序号
     */
    Integer getSequence();

    void setSequence(Integer sequence);

    /**
     * 获取 [序号]脏标记
     */
    boolean getSequenceDirtyFlag();

    /**
     * 销售订单行消息
     */
    String getSale_line_warn_msg();

    void setSale_line_warn_msg(String sale_line_warn_msg);

    /**
     * 获取 [销售订单行消息]脏标记
     */
    boolean getSale_line_warn_msgDirtyFlag();

    /**
     * 仓库
     */
    Integer getWarehouse_id();

    void setWarehouse_id(Integer warehouse_id);

    /**
     * 获取 [仓库]脏标记
     */
    boolean getWarehouse_idDirtyFlag();

    /**
     * 出租
     */
    String getRental();

    void setRental(String rental);

    /**
     * 获取 [出租]脏标记
     */
    boolean getRentalDirtyFlag();

    /**
     * 价格差异科目
     */
    Integer getProperty_account_creditor_price_difference();

    void setProperty_account_creditor_price_difference(Integer property_account_creditor_price_difference);

    /**
     * 获取 [价格差异科目]脏标记
     */
    boolean getProperty_account_creditor_price_differenceDirtyFlag();

    /**
     * 重量计量单位标签
     */
    String getWeight_uom_name();

    void setWeight_uom_name(String weight_uom_name);

    /**
     * 获取 [重量计量单位标签]脏标记
     */
    boolean getWeight_uom_nameDirtyFlag();

    /**
     * 成本币种
     */
    Integer getCost_currency_id();

    void setCost_currency_id(Integer cost_currency_id);

    /**
     * 获取 [成本币种]脏标记
     */
    boolean getCost_currency_idDirtyFlag();

    /**
     * 库存进货科目
     */
    Integer getProperty_stock_account_input();

    void setProperty_stock_account_input(Integer property_stock_account_input);

    /**
     * 获取 [库存进货科目]脏标记
     */
    boolean getProperty_stock_account_inputDirtyFlag();

    /**
     * 名称
     */
    String getName();

    void setName(String name);

    /**
     * 获取 [名称]脏标记
     */
    boolean getNameDirtyFlag();

    /**
     * 制造提前期(日)
     */
    Double getProduce_delay();

    void setProduce_delay(Double produce_delay);

    /**
     * 获取 [制造提前期(日)]脏标记
     */
    boolean getProduce_delayDirtyFlag();

    /**
     * SEO优化
     */
    String getIs_seo_optimized();

    void setIs_seo_optimized(String is_seo_optimized);

    /**
     * 获取 [SEO优化]脏标记
     */
    boolean getIs_seo_optimizedDirtyFlag();

    /**
     * 网站网址
     */
    String getWebsite_url();

    void setWebsite_url(String website_url);

    /**
     * 获取 [网站网址]脏标记
     */
    boolean getWebsite_urlDirtyFlag();

    /**
     * 最新反馈评级
     */
    String getRating_last_feedback();

    void setRating_last_feedback(String rating_last_feedback);

    /**
     * 获取 [最新反馈评级]脏标记
     */
    boolean getRating_last_feedbackDirtyFlag();

    /**
     * 尺寸 Y
     */
    Integer getWebsite_size_y();

    void setWebsite_size_y(Integer website_size_y);

    /**
     * 获取 [尺寸 Y]脏标记
     */
    boolean getWebsite_size_yDirtyFlag();

    /**
     * 是一张活动票吗？
     */
    String getEvent_ok();

    void setEvent_ok(String event_ok);

    /**
     * 获取 [是一张活动票吗？]脏标记
     */
    boolean getEvent_okDirtyFlag();

    /**
     * 库存可用性
     */
    String getInventory_availability();

    void setInventory_availability(String inventory_availability);

    /**
     * 获取 [库存可用性]脏标记
     */
    boolean getInventory_availabilityDirtyFlag();

    /**
     * 采购
     */
    String getPurchase_ok();

    void setPurchase_ok(String purchase_ok);

    /**
     * 获取 [采购]脏标记
     */
    boolean getPurchase_okDirtyFlag();

    /**
     * 创建人
     */
    String getCreate_uid_text();

    void setCreate_uid_text(String create_uid_text);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uid_textDirtyFlag();

    /**
     * 最新值评级
     */
    Double getRating_last_value();

    void setRating_last_value(Double rating_last_value);

    /**
     * 获取 [最新值评级]脏标记
     */
    boolean getRating_last_valueDirtyFlag();

    /**
     * 网站meta标题
     */
    String getWebsite_meta_title();

    void setWebsite_meta_title(String website_meta_title);

    /**
     * 获取 [网站meta标题]脏标记
     */
    boolean getWebsite_meta_titleDirtyFlag();

    /**
     * 最新图像评级
     */
    byte[] getRating_last_image();

    void setRating_last_image(byte[] rating_last_image);

    /**
     * 获取 [最新图像评级]脏标记
     */
    boolean getRating_last_imageDirtyFlag();

    /**
     * 采购说明
     */
    String getDescription_purchase();

    void setDescription_purchase(String description_purchase);

    /**
     * 获取 [采购说明]脏标记
     */
    boolean getDescription_purchaseDirtyFlag();

    /**
     * 网站
     */
    Integer getWebsite_id();

    void setWebsite_id(Integer website_id);

    /**
     * 获取 [网站]脏标记
     */
    boolean getWebsite_idDirtyFlag();

    /**
     * 报销
     */
    String getCan_be_expensed();

    void setCan_be_expensed(String can_be_expensed);

    /**
     * 获取 [报销]脏标记
     */
    boolean getCan_be_expensedDirtyFlag();

    /**
     * 销售订单行
     */
    String getSale_line_warn();

    void setSale_line_warn(String sale_line_warn);

    /**
     * 获取 [销售订单行]脏标记
     */
    boolean getSale_line_warnDirtyFlag();

    /**
     * 尺寸 X
     */
    Integer getWebsite_size_x();

    void setWebsite_size_x(Integer website_size_x);

    /**
     * 获取 [尺寸 X]脏标记
     */
    boolean getWebsite_size_xDirtyFlag();

    /**
     * 自动采购
     */
    String getService_to_purchase();

    void setService_to_purchase(String service_to_purchase);

    /**
     * 获取 [自动采购]脏标记
     */
    boolean getService_to_purchaseDirtyFlag();

    /**
     * 网站序列
     */
    Integer getWebsite_sequence();

    void setWebsite_sequence(Integer website_sequence);

    /**
     * 获取 [网站序列]脏标记
     */
    boolean getWebsite_sequenceDirtyFlag();

    /**
     * 库存位置
     */
    Integer getProperty_stock_inventory();

    void setProperty_stock_inventory(Integer property_stock_inventory);

    /**
     * 获取 [库存位置]脏标记
     */
    boolean getProperty_stock_inventoryDirtyFlag();

    /**
     * 地点
     */
    Integer getLocation_id();

    void setLocation_id(Integer location_id);

    /**
     * 获取 [地点]脏标记
     */
    boolean getLocation_idDirtyFlag();

    /**
     * 库存计价
     */
    String getProperty_valuation();

    void setProperty_valuation(String property_valuation);

    /**
     * 获取 [库存计价]脏标记
     */
    boolean getProperty_valuationDirtyFlag();

    /**
     * 已发布
     */
    String getIs_published();

    void setIs_published(String is_published);

    /**
     * 获取 [已发布]脏标记
     */
    boolean getIs_publishedDirtyFlag();

    /**
     * 重开收据规则
     */
    String getExpense_policy();

    void setExpense_policy(String expense_policy);

    /**
     * 获取 [重开收据规则]脏标记
     */
    boolean getExpense_policyDirtyFlag();

    /**
     * 测量的重量单位
     */
    Integer getWeight_uom_id();

    void setWeight_uom_id(Integer weight_uom_id);

    /**
     * 获取 [测量的重量单位]脏标记
     */
    boolean getWeight_uom_idDirtyFlag();

    /**
     * 颜色索引
     */
    Integer getColor();

    void setColor(Integer color);

    /**
     * 获取 [颜色索引]脏标记
     */
    boolean getColorDirtyFlag();

    /**
     * 生产位置
     */
    Integer getProperty_stock_production();

    void setProperty_stock_production(Integer property_stock_production);

    /**
     * 获取 [生产位置]脏标记
     */
    boolean getProperty_stock_productionDirtyFlag();

    /**
     * 在当前网站显示
     */
    String getWebsite_published();

    void setWebsite_published(String website_published);

    /**
     * 获取 [在当前网站显示]脏标记
     */
    boolean getWebsite_publishedDirtyFlag();

    /**
     * 网站meta关键词
     */
    String getWebsite_meta_keywords();

    void setWebsite_meta_keywords(String website_meta_keywords);

    /**
     * 获取 [网站meta关键词]脏标记
     */
    boolean getWebsite_meta_keywordsDirtyFlag();

    /**
     * 出库单说明
     */
    String getDescription_pickingout();

    void setDescription_pickingout(String description_pickingout);

    /**
     * 获取 [出库单说明]脏标记
     */
    boolean getDescription_pickingoutDirtyFlag();

    /**
     * 价格表
     */
    Integer getPricelist_id();

    void setPricelist_id(Integer pricelist_id);

    /**
     * 获取 [价格表]脏标记
     */
    boolean getPricelist_idDirtyFlag();

    /**
     * 评级数
     */
    Integer getRating_count();

    void setRating_count(Integer rating_count);

    /**
     * 获取 [评级数]脏标记
     */
    boolean getRating_countDirtyFlag();

    /**
     * 网站元说明
     */
    String getWebsite_meta_description();

    void setWebsite_meta_description(String website_meta_description);

    /**
     * 获取 [网站元说明]脏标记
     */
    boolean getWebsite_meta_descriptionDirtyFlag();

    /**
     * 计价
     */
    String getValuation();

    void setValuation(String valuation);

    /**
     * 获取 [计价]脏标记
     */
    boolean getValuationDirtyFlag();

    /**
     * 开票策略
     */
    String getInvoice_policy();

    void setInvoice_policy(String invoice_policy);

    /**
     * 获取 [开票策略]脏标记
     */
    boolean getInvoice_policyDirtyFlag();

    /**
     * 采购订单明细的消息
     */
    String getPurchase_line_warn_msg();

    void setPurchase_line_warn_msg(String purchase_line_warn_msg);

    /**
     * 获取 [采购订单明细的消息]脏标记
     */
    boolean getPurchase_line_warn_msgDirtyFlag();

    /**
     * 最后更新人
     */
    String getWrite_uid_text();

    void setWrite_uid_text(String write_uid_text);

    /**
     * 获取 [最后更新人]脏标记
     */
    boolean getWrite_uid_textDirtyFlag();

    /**
     * 收入科目
     */
    Integer getProperty_account_income_id();

    void setProperty_account_income_id(Integer property_account_income_id);

    /**
     * 获取 [收入科目]脏标记
     */
    boolean getProperty_account_income_idDirtyFlag();

    /**
     * 成本方法
     */
    String getProperty_cost_method();

    void setProperty_cost_method(String property_cost_method);

    /**
     * 获取 [成本方法]脏标记
     */
    boolean getProperty_cost_methodDirtyFlag();

    /**
     * 产品种类
     */
    Integer getCateg_id();

    void setCateg_id(Integer categ_id);

    /**
     * 获取 [产品种类]脏标记
     */
    boolean getCateg_idDirtyFlag();

    /**
     * Can be Part
     */
    String getIsParts();

    void setIsParts(String isParts);

    /**
     * 获取 [Can be Part]脏标记
     */
    boolean getIsPartsDirtyFlag();

    /**
     * 计量单位
     */
    Integer getUom_id();

    void setUom_id(Integer uom_id);

    /**
     * 获取 [计量单位]脏标记
     */
    boolean getUom_idDirtyFlag();

    /**
     * 产品
     */
    Integer getProduct_variant_id();

    void setProduct_variant_id(Integer product_variant_id);

    /**
     * 获取 [产品]脏标记
     */
    boolean getProduct_variant_idDirtyFlag();

    /**
     * 产品类型
     */
    String getType();

    void setType(String type);

    /**
     * 获取 [产品类型]脏标记
     */
    boolean getTypeDirtyFlag();

    /**
     * 控制策略
     */
    String getPurchase_method();

    void setPurchase_method(String purchase_method);

    /**
     * 获取 [控制策略]脏标记
     */
    boolean getPurchase_methodDirtyFlag();

    /**
     * 负责人
     */
    Integer getResponsible_id();

    void setResponsible_id(Integer responsible_id);

    /**
     * 获取 [负责人]脏标记
     */
    boolean getResponsible_idDirtyFlag();

    /**
     * 跟踪服务
     */
    String getService_type();

    void setService_type(String service_type);

    /**
     * 获取 [跟踪服务]脏标记
     */
    boolean getService_typeDirtyFlag();

    /**
     * 单位名称
     */
    String getUom_name();

    void setUom_name(String uom_name);

    /**
     * 获取 [单位名称]脏标记
     */
    boolean getUom_nameDirtyFlag();

    /**
     * 可用阈值
     */
    Double getAvailable_threshold();

    void setAvailable_threshold(Double available_threshold);

    /**
     * 获取 [可用阈值]脏标记
     */
    boolean getAvailable_thresholdDirtyFlag();

    /**
     * 采购订单行
     */
    String getPurchase_line_warn();

    void setPurchase_line_warn(String purchase_line_warn);

    /**
     * 获取 [采购订单行]脏标记
     */
    boolean getPurchase_line_warnDirtyFlag();

    /**
     * # 产品变体
     */
    Integer getProduct_variant_count();

    void setProduct_variant_count(Integer product_variant_count);

    /**
     * 获取 [# 产品变体]脏标记
     */
    boolean getProduct_variant_countDirtyFlag();

    /**
     * POS类别
     */
    Integer getPos_categ_id();

    void setPos_categ_id(Integer pos_categ_id);

    /**
     * 获取 [POS类别]脏标记
     */
    boolean getPos_categ_idDirtyFlag();

    /**
     * 自定义消息
     */
    String getCustom_message();

    void setCustom_message(String custom_message);

    /**
     * 获取 [自定义消息]脏标记
     */
    boolean getCustom_messageDirtyFlag();

    /**
     * 费用科目
     */
    Integer getProperty_account_expense_id();

    void setProperty_account_expense_id(Integer property_account_expense_id);

    /**
     * 获取 [费用科目]脏标记
     */
    boolean getProperty_account_expense_idDirtyFlag();

    /**
     * 客户前置时间
     */
    Double getSale_delay();

    void setSale_delay(Double sale_delay);

    /**
     * 获取 [客户前置时间]脏标记
     */
    boolean getSale_delayDirtyFlag();

    /**
     * 采购计量单位
     */
    Integer getUom_po_id();

    void setUom_po_id(Integer uom_po_id);

    /**
     * 获取 [采购计量单位]脏标记
     */
    boolean getUom_po_idDirtyFlag();

    /**
     * POS可用
     */
    String getAvailable_in_pos();

    void setAvailable_in_pos(String available_in_pos);

    /**
     * 获取 [POS可用]脏标记
     */
    boolean getAvailable_in_posDirtyFlag();

    /**
     * 创建人
     */
    Integer getCreate_uid();

    void setCreate_uid(Integer create_uid);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uidDirtyFlag();

    /**
     * 最后更新人
     */
    Integer getWrite_uid();

    void setWrite_uid(Integer write_uid);

    /**
     * 获取 [最后更新人]脏标记
     */
    boolean getWrite_uidDirtyFlag();

    /**
     * 产品模板
     */
    Integer getProduct_tmpl_id();

    void setProduct_tmpl_id(Integer product_tmpl_id);

    /**
     * 获取 [产品模板]脏标记
     */
    boolean getProduct_tmpl_idDirtyFlag();

}
