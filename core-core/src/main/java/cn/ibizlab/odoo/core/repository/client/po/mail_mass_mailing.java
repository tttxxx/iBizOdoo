package cn.ibizlab.odoo.core.repository.client.po;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigDecimal;

/**
 * [mail_mass_mailing] 对象
 */
public interface mail_mass_mailing {

    public String getActive();

    public void setActive(String active);

    public String getAttachment_ids();

    public void setAttachment_ids(String attachment_ids);

    public String getBody_html();

    public void setBody_html(String body_html);

    public Integer getBounced();

    public void setBounced(Integer bounced);

    public Integer getBounced_ratio();

    public void setBounced_ratio(Integer bounced_ratio);

    public Integer getCampaign_id();

    public void setCampaign_id(Integer campaign_id);

    public String getCampaign_id_text();

    public void setCampaign_id_text(String campaign_id_text);

    public Integer getClicked();

    public void setClicked(Integer clicked);

    public Integer getClicks_ratio();

    public void setClicks_ratio(Integer clicks_ratio);

    public Integer getColor();

    public void setColor(Integer color);

    public Integer getContact_ab_pc();

    public void setContact_ab_pc(Integer contact_ab_pc);

    public String getContact_list_ids();

    public void setContact_list_ids(String contact_list_ids);

    public Timestamp getCreate_date();

    public void setCreate_date(Timestamp create_date);

    public Integer getCreate_uid();

    public void setCreate_uid(Integer create_uid);

    public String getCreate_uid_text();

    public void setCreate_uid_text(String create_uid_text);

    public String getCrm_lead_activated();

    public void setCrm_lead_activated(String crm_lead_activated);

    public Integer getCrm_lead_count();

    public void setCrm_lead_count(Integer crm_lead_count);

    public Integer getCrm_opportunities_count();

    public void setCrm_opportunities_count(Integer crm_opportunities_count);

    public Integer getDelivered();

    public void setDelivered(Integer delivered);

    public String getDisplay_name();

    public void setDisplay_name(String display_name);

    public String getEmail_from();

    public void setEmail_from(String email_from);

    public Integer getExpected();

    public void setExpected(Integer expected);

    public Integer getFailed();

    public void setFailed(Integer failed);

    public Integer getId();

    public void setId(Integer id);

    public Integer getIgnored();

    public void setIgnored(Integer ignored);

    public String getKeep_archives();

    public void setKeep_archives(String keep_archives);

    public String getMailing_domain();

    public void setMailing_domain(String mailing_domain);

    public String getMailing_model_name();

    public void setMailing_model_name(String mailing_model_name);

    public String getMailing_model_real();

    public void setMailing_model_real(String mailing_model_real);

    public Integer getMass_mailing_campaign_id();

    public void setMass_mailing_campaign_id(Integer mass_mailing_campaign_id);

    public String getMass_mailing_campaign_id_text();

    public void setMass_mailing_campaign_id_text(String mass_mailing_campaign_id_text);

    public Integer getMedium_id();

    public void setMedium_id(Integer medium_id);

    public String getMedium_id_text();

    public void setMedium_id_text(String medium_id_text);

    public String getName();

    public void setName(String name);

    public Timestamp getNext_departure();

    public void setNext_departure(Timestamp next_departure);

    public Integer getOpened();

    public void setOpened(Integer opened);

    public Integer getOpened_ratio();

    public void setOpened_ratio(Integer opened_ratio);

    public Integer getReceived_ratio();

    public void setReceived_ratio(Integer received_ratio);

    public Integer getReplied();

    public void setReplied(Integer replied);

    public Integer getReplied_ratio();

    public void setReplied_ratio(Integer replied_ratio);

    public String getReply_to();

    public void setReply_to(String reply_to);

    public String getReply_to_mode();

    public void setReply_to_mode(String reply_to_mode);

    public Integer getSale_invoiced_amount();

    public void setSale_invoiced_amount(Integer sale_invoiced_amount);

    public Integer getSale_quotation_count();

    public void setSale_quotation_count(Integer sale_quotation_count);

    public Integer getScheduled();

    public void setScheduled(Integer scheduled);

    public Timestamp getSchedule_date();

    public void setSchedule_date(Timestamp schedule_date);

    public Integer getSent();

    public void setSent(Integer sent);

    public Timestamp getSent_date();

    public void setSent_date(Timestamp sent_date);

    public Integer getSource_id();

    public void setSource_id(Integer source_id);

    public String getSource_id_text();

    public void setSource_id_text(String source_id_text);

    public String getState();

    public void setState(String state);

    public String getStatistics_ids();

    public void setStatistics_ids(String statistics_ids);

    public Integer getTotal();

    public void setTotal(Integer total);

    public Integer getUser_id();

    public void setUser_id(Integer user_id);

    public String getUser_id_text();

    public void setUser_id_text(String user_id_text);

    public Timestamp getWrite_date();

    public void setWrite_date(Timestamp write_date);

    public Integer getWrite_uid();

    public void setWrite_uid(Integer write_uid);

    public String getWrite_uid_text();

    public void setWrite_uid_text(String write_uid_text);

    public Timestamp get__last_update();

    public void set__last_update(Timestamp __last_update);

}
