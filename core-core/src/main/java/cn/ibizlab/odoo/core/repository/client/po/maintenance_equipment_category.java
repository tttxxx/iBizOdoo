package cn.ibizlab.odoo.core.repository.client.po;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigDecimal;

/**
 * [maintenance_equipment_category] 对象
 */
public interface maintenance_equipment_category {

    public String getAlias_contact();

    public void setAlias_contact(String alias_contact);

    public String getAlias_defaults();

    public void setAlias_defaults(String alias_defaults);

    public String getAlias_domain();

    public void setAlias_domain(String alias_domain);

    public Integer getAlias_force_thread_id();

    public void setAlias_force_thread_id(Integer alias_force_thread_id);

    public Integer getAlias_id();

    public void setAlias_id(Integer alias_id);

    public String getAlias_name();

    public void setAlias_name(String alias_name);

    public Integer getAlias_parent_thread_id();

    public void setAlias_parent_thread_id(Integer alias_parent_thread_id);

    public Integer getAlias_user_id();

    public void setAlias_user_id(Integer alias_user_id);

    public String getAlias_user_id_text();

    public void setAlias_user_id_text(String alias_user_id_text);

    public Integer getColor();

    public void setColor(Integer color);

    public Integer getCompany_id();

    public void setCompany_id(Integer company_id);

    public String getCompany_id_text();

    public void setCompany_id_text(String company_id_text);

    public Timestamp getCreate_date();

    public void setCreate_date(Timestamp create_date);

    public Integer getCreate_uid();

    public void setCreate_uid(Integer create_uid);

    public String getCreate_uid_text();

    public void setCreate_uid_text(String create_uid_text);

    public String getDisplay_name();

    public void setDisplay_name(String display_name);

    public Integer getEquipment_count();

    public void setEquipment_count(Integer equipment_count);

    public String getEquipment_ids();

    public void setEquipment_ids(String equipment_ids);

    public String getFold();

    public void setFold(String fold);

    public Integer getId();

    public void setId(Integer id);

    public Integer getMaintenance_count();

    public void setMaintenance_count(Integer maintenance_count);

    public String getMaintenance_ids();

    public void setMaintenance_ids(String maintenance_ids);

    public Integer getMessage_attachment_count();

    public void setMessage_attachment_count(Integer message_attachment_count);

    public String getMessage_channel_ids();

    public void setMessage_channel_ids(String message_channel_ids);

    public String getMessage_follower_ids();

    public void setMessage_follower_ids(String message_follower_ids);

    public String getMessage_has_error();

    public void setMessage_has_error(String message_has_error);

    public Integer getMessage_has_error_counter();

    public void setMessage_has_error_counter(Integer message_has_error_counter);

    public String getMessage_ids();

    public void setMessage_ids(String message_ids);

    public String getMessage_is_follower();

    public void setMessage_is_follower(String message_is_follower);

    public String getMessage_needaction();

    public void setMessage_needaction(String message_needaction);

    public Integer getMessage_needaction_counter();

    public void setMessage_needaction_counter(Integer message_needaction_counter);

    public String getMessage_partner_ids();

    public void setMessage_partner_ids(String message_partner_ids);

    public String getMessage_unread();

    public void setMessage_unread(String message_unread);

    public Integer getMessage_unread_counter();

    public void setMessage_unread_counter(Integer message_unread_counter);

    public String getName();

    public void setName(String name);

    public String getNote();

    public void setNote(String note);

    public Integer getTechnician_user_id();

    public void setTechnician_user_id(Integer technician_user_id);

    public String getTechnician_user_id_text();

    public void setTechnician_user_id_text(String technician_user_id_text);

    public String getWebsite_message_ids();

    public void setWebsite_message_ids(String website_message_ids);

    public Timestamp getWrite_date();

    public void setWrite_date(Timestamp write_date);

    public Integer getWrite_uid();

    public void setWrite_uid(Integer write_uid);

    public String getWrite_uid_text();

    public void setWrite_uid_text(String write_uid_text);

    public Timestamp get__last_update();

    public void set__last_update(Timestamp __last_update);

}
