package cn.ibizlab.odoo.core.odoo_crm.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_crm.domain.Crm_lead2opportunity_partner;
import cn.ibizlab.odoo.core.odoo_crm.filter.Crm_lead2opportunity_partnerSearchContext;
import cn.ibizlab.odoo.core.odoo_crm.service.ICrm_lead2opportunity_partnerService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;



import cn.ibizlab.odoo.core.odoo_crm.client.crm_lead2opportunity_partnerOdooClient;
import cn.ibizlab.odoo.core.odoo_crm.clientmodel.crm_lead2opportunity_partnerClientModel;

/**
 * 实体[转化线索为商机（单个）] 服务对象接口实现
 */
@Slf4j
@Service
public class Crm_lead2opportunity_partnerServiceImpl implements ICrm_lead2opportunity_partnerService {

    @Autowired
    crm_lead2opportunity_partnerOdooClient crm_lead2opportunity_partnerOdooClient;


    @Override
    public boolean create(Crm_lead2opportunity_partner et) {
        crm_lead2opportunity_partnerClientModel clientModel = convert2Model(et,null);
		crm_lead2opportunity_partnerOdooClient.create(clientModel);
        Crm_lead2opportunity_partner rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Crm_lead2opportunity_partner> list){
    }

    @Override
    public boolean remove(Integer id) {
        crm_lead2opportunity_partnerClientModel clientModel = new crm_lead2opportunity_partnerClientModel();
        clientModel.setId(id);
		crm_lead2opportunity_partnerOdooClient.remove(clientModel);
        return true; 
    }

    public void removeBatch(Collection<Integer> idList){
    }

    @Override
    public Crm_lead2opportunity_partner get(Integer id) {
        crm_lead2opportunity_partnerClientModel clientModel = new crm_lead2opportunity_partnerClientModel();
        clientModel.setId(id);
		crm_lead2opportunity_partnerOdooClient.get(clientModel);
        Crm_lead2opportunity_partner et = convert2Domain(clientModel,null);
        if(et==null){
            et=new Crm_lead2opportunity_partner();
            et.setId(id);
        }
        else{
        }

        return  et;
    }

    @Override
    public boolean update(Crm_lead2opportunity_partner et) {
        crm_lead2opportunity_partnerClientModel clientModel = convert2Model(et,null);
		crm_lead2opportunity_partnerOdooClient.update(clientModel);
        Crm_lead2opportunity_partner rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }
    
    public void updateBatch(List<Crm_lead2opportunity_partner> list){
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Crm_lead2opportunity_partner> searchDefault(Crm_lead2opportunity_partnerSearchContext context) {
        List<Crm_lead2opportunity_partner> list = new ArrayList<Crm_lead2opportunity_partner>();
        Page<crm_lead2opportunity_partnerClientModel> clientModelList = crm_lead2opportunity_partnerOdooClient.search(context);
        for(crm_lead2opportunity_partnerClientModel clientModel : clientModelList){
            list.add( this.convert2Domain(clientModel,null));
        }
        return new PageImpl<Crm_lead2opportunity_partner>(list, context.getPageable(), clientModelList.getTotalElements());
    }

    public crm_lead2opportunity_partnerClientModel convert2Model(Crm_lead2opportunity_partner domain , crm_lead2opportunity_partnerClientModel model) {
        if(domain == null)
            return model ;
        if(model==null){
            model = new crm_lead2opportunity_partnerClientModel();

        }
        if((Boolean) domain.getExtensionparams().get("dirtyflagenable")){
            if((Boolean) domain.getExtensionparams().get("create_datedirtyflag"))
                model.setCreate_date(domain.getCreateDate());
            if((Boolean) domain.getExtensionparams().get("write_datedirtyflag"))
                model.setWrite_date(domain.getWriteDate());
            if((Boolean) domain.getExtensionparams().get("iddirtyflag"))
                model.setId(domain.getId());
            if((Boolean) domain.getExtensionparams().get("__last_updatedirtyflag"))
                model.set__last_update(domain.getLastUpdate());
            if((Boolean) domain.getExtensionparams().get("opportunity_idsdirtyflag"))
                model.setOpportunity_ids(domain.getOpportunityIds());
            if((Boolean) domain.getExtensionparams().get("actiondirtyflag"))
                model.setAction(domain.getAction());
            if((Boolean) domain.getExtensionparams().get("display_namedirtyflag"))
                model.setDisplay_name(domain.getDisplayName());
            if((Boolean) domain.getExtensionparams().get("namedirtyflag"))
                model.setName(domain.getName());
            if((Boolean) domain.getExtensionparams().get("write_uid_textdirtyflag"))
                model.setWrite_uid_text(domain.getWriteUidText());
            if((Boolean) domain.getExtensionparams().get("create_uid_textdirtyflag"))
                model.setCreate_uid_text(domain.getCreateUidText());
            if((Boolean) domain.getExtensionparams().get("user_id_textdirtyflag"))
                model.setUser_id_text(domain.getUserIdText());
            if((Boolean) domain.getExtensionparams().get("team_id_textdirtyflag"))
                model.setTeam_id_text(domain.getTeamIdText());
            if((Boolean) domain.getExtensionparams().get("partner_id_textdirtyflag"))
                model.setPartner_id_text(domain.getPartnerIdText());
            if((Boolean) domain.getExtensionparams().get("write_uiddirtyflag"))
                model.setWrite_uid(domain.getWriteUid());
            if((Boolean) domain.getExtensionparams().get("partner_iddirtyflag"))
                model.setPartner_id(domain.getPartnerId());
            if((Boolean) domain.getExtensionparams().get("team_iddirtyflag"))
                model.setTeam_id(domain.getTeamId());
            if((Boolean) domain.getExtensionparams().get("create_uiddirtyflag"))
                model.setCreate_uid(domain.getCreateUid());
            if((Boolean) domain.getExtensionparams().get("user_iddirtyflag"))
                model.setUser_id(domain.getUserId());
            domain.getFocusNull().clear();
        }
        return model ;
    }

    public Crm_lead2opportunity_partner convert2Domain( crm_lead2opportunity_partnerClientModel model ,Crm_lead2opportunity_partner domain) {
        if(model == null)
            return domain ;
        if(domain == null){
            domain = new Crm_lead2opportunity_partner();
        }

        if(model.getCreate_dateDirtyFlag())
            domain.setCreateDate(model.getCreate_date());
        if(model.getWrite_dateDirtyFlag())
            domain.setWriteDate(model.getWrite_date());
        if(model.getIdDirtyFlag())
            domain.setId(model.getId());
        if(model.get__last_updateDirtyFlag())
            domain.setLastUpdate(model.get__last_update());
        if(model.getOpportunity_idsDirtyFlag())
            domain.setOpportunityIds(model.getOpportunity_ids());
        if(model.getActionDirtyFlag())
            domain.setAction(model.getAction());
        if(model.getDisplay_nameDirtyFlag())
            domain.setDisplayName(model.getDisplay_name());
        if(model.getNameDirtyFlag())
            domain.setName(model.getName());
        if(model.getWrite_uid_textDirtyFlag())
            domain.setWriteUidText(model.getWrite_uid_text());
        if(model.getCreate_uid_textDirtyFlag())
            domain.setCreateUidText(model.getCreate_uid_text());
        if(model.getUser_id_textDirtyFlag())
            domain.setUserIdText(model.getUser_id_text());
        if(model.getTeam_id_textDirtyFlag())
            domain.setTeamIdText(model.getTeam_id_text());
        if(model.getPartner_id_textDirtyFlag())
            domain.setPartnerIdText(model.getPartner_id_text());
        if(model.getWrite_uidDirtyFlag())
            domain.setWriteUid(model.getWrite_uid());
        if(model.getPartner_idDirtyFlag())
            domain.setPartnerId(model.getPartner_id());
        if(model.getTeam_idDirtyFlag())
            domain.setTeamId(model.getTeam_id());
        if(model.getCreate_uidDirtyFlag())
            domain.setCreateUid(model.getCreate_uid());
        if(model.getUser_idDirtyFlag())
            domain.setUserId(model.getUser_id());
        return domain ;
    }

}

    



