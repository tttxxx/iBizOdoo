package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Res_lang;
import cn.ibizlab.odoo.core.odoo_base.filter.Res_langSearchContext;

/**
 * 实体 [语言] 存储对象
 */
public interface Res_langRepository extends Repository<Res_lang> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Res_lang> searchDefault(Res_langSearchContext context);

    Res_lang convert2PO(cn.ibizlab.odoo.core.odoo_base.domain.Res_lang domain , Res_lang po) ;

    cn.ibizlab.odoo.core.odoo_base.domain.Res_lang convert2Domain( Res_lang po ,cn.ibizlab.odoo.core.odoo_base.domain.Res_lang domain) ;

}
