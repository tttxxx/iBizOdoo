package cn.ibizlab.odoo.core.odoo_repair.clientmodel;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.math.BigInteger;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.util.helper.OdooClientHelper;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang.ArrayUtils;
import org.springframework.util.StringUtils;

/**
 * 接口实体[repair_fee] 对象
 */
public class repair_feeClientModel implements Serializable{

    /**
     * 创建时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp create_date;

    @JsonIgnore
    public boolean create_dateDirtyFlag;
    
    /**
     * 创建者
     */
    public Integer create_uid;

    @JsonIgnore
    public boolean create_uidDirtyFlag;
    
    /**
     * 创建者
     */
    public String create_uid_text;

    @JsonIgnore
    public boolean create_uid_textDirtyFlag;
    
    /**
     * 显示名称
     */
    public String display_name;

    @JsonIgnore
    public boolean display_nameDirtyFlag;
    
    /**
     * ID
     */
    public Integer id;

    @JsonIgnore
    public boolean idDirtyFlag;
    
    /**
     * 已开票
     */
    public String invoiced;

    @JsonIgnore
    public boolean invoicedDirtyFlag;
    
    /**
     * 发票明细
     */
    public Integer invoice_line_id;

    @JsonIgnore
    public boolean invoice_line_idDirtyFlag;
    
    /**
     * 发票明细
     */
    public String invoice_line_id_text;

    @JsonIgnore
    public boolean invoice_line_id_textDirtyFlag;
    
    /**
     * 描述
     */
    public String name;

    @JsonIgnore
    public boolean nameDirtyFlag;
    
    /**
     * 小计
     */
    public Double price_subtotal;

    @JsonIgnore
    public boolean price_subtotalDirtyFlag;
    
    /**
     * 单价
     */
    public Double price_unit;

    @JsonIgnore
    public boolean price_unitDirtyFlag;
    
    /**
     * 产品
     */
    public Integer product_id;

    @JsonIgnore
    public boolean product_idDirtyFlag;
    
    /**
     * 产品
     */
    public String product_id_text;

    @JsonIgnore
    public boolean product_id_textDirtyFlag;
    
    /**
     * 产品量度单位
     */
    public Integer product_uom;

    @JsonIgnore
    public boolean product_uomDirtyFlag;
    
    /**
     * 数量
     */
    public Double product_uom_qty;

    @JsonIgnore
    public boolean product_uom_qtyDirtyFlag;
    
    /**
     * 产品量度单位
     */
    public String product_uom_text;

    @JsonIgnore
    public boolean product_uom_textDirtyFlag;
    
    /**
     * 维修单编＃
     */
    public Integer repair_id;

    @JsonIgnore
    public boolean repair_idDirtyFlag;
    
    /**
     * 维修单编＃
     */
    public String repair_id_text;

    @JsonIgnore
    public boolean repair_id_textDirtyFlag;
    
    /**
     * 税
     */
    public String tax_id;

    @JsonIgnore
    public boolean tax_idDirtyFlag;
    
    /**
     * 最后更新时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp write_date;

    @JsonIgnore
    public boolean write_dateDirtyFlag;
    
    /**
     * 最后更新人
     */
    public Integer write_uid;

    @JsonIgnore
    public boolean write_uidDirtyFlag;
    
    /**
     * 最后更新人
     */
    public String write_uid_text;

    @JsonIgnore
    public boolean write_uid_textDirtyFlag;
    
    /**
     * 最后修改时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp __last_update;

    @JsonIgnore
    public boolean __last_updateDirtyFlag;
    
    /**
     * 获取 [创建时间]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return this.create_date ;
    }

    /**
     * 设置 [创建时间]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

     /**
     * 获取 [创建时间]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return this.create_dateDirtyFlag ;
    }   

    /**
     * 获取 [创建者]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return this.create_uid ;
    }

    /**
     * 设置 [创建者]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

     /**
     * 获取 [创建者]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return this.create_uidDirtyFlag ;
    }   

    /**
     * 获取 [创建者]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return this.create_uid_text ;
    }

    /**
     * 设置 [创建者]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

     /**
     * 获取 [创建者]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return this.create_uid_textDirtyFlag ;
    }   

    /**
     * 获取 [显示名称]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return this.display_name ;
    }

    /**
     * 设置 [显示名称]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

     /**
     * 获取 [显示名称]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return this.display_nameDirtyFlag ;
    }   

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return this.id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

     /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return this.idDirtyFlag ;
    }   

    /**
     * 获取 [已开票]
     */
    @JsonProperty("invoiced")
    public String getInvoiced(){
        return this.invoiced ;
    }

    /**
     * 设置 [已开票]
     */
    @JsonProperty("invoiced")
    public void setInvoiced(String  invoiced){
        this.invoiced = invoiced ;
        this.invoicedDirtyFlag = true ;
    }

     /**
     * 获取 [已开票]脏标记
     */
    @JsonIgnore
    public boolean getInvoicedDirtyFlag(){
        return this.invoicedDirtyFlag ;
    }   

    /**
     * 获取 [发票明细]
     */
    @JsonProperty("invoice_line_id")
    public Integer getInvoice_line_id(){
        return this.invoice_line_id ;
    }

    /**
     * 设置 [发票明细]
     */
    @JsonProperty("invoice_line_id")
    public void setInvoice_line_id(Integer  invoice_line_id){
        this.invoice_line_id = invoice_line_id ;
        this.invoice_line_idDirtyFlag = true ;
    }

     /**
     * 获取 [发票明细]脏标记
     */
    @JsonIgnore
    public boolean getInvoice_line_idDirtyFlag(){
        return this.invoice_line_idDirtyFlag ;
    }   

    /**
     * 获取 [发票明细]
     */
    @JsonProperty("invoice_line_id_text")
    public String getInvoice_line_id_text(){
        return this.invoice_line_id_text ;
    }

    /**
     * 设置 [发票明细]
     */
    @JsonProperty("invoice_line_id_text")
    public void setInvoice_line_id_text(String  invoice_line_id_text){
        this.invoice_line_id_text = invoice_line_id_text ;
        this.invoice_line_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [发票明细]脏标记
     */
    @JsonIgnore
    public boolean getInvoice_line_id_textDirtyFlag(){
        return this.invoice_line_id_textDirtyFlag ;
    }   

    /**
     * 获取 [描述]
     */
    @JsonProperty("name")
    public String getName(){
        return this.name ;
    }

    /**
     * 设置 [描述]
     */
    @JsonProperty("name")
    public void setName(String  name){
        this.name = name ;
        this.nameDirtyFlag = true ;
    }

     /**
     * 获取 [描述]脏标记
     */
    @JsonIgnore
    public boolean getNameDirtyFlag(){
        return this.nameDirtyFlag ;
    }   

    /**
     * 获取 [小计]
     */
    @JsonProperty("price_subtotal")
    public Double getPrice_subtotal(){
        return this.price_subtotal ;
    }

    /**
     * 设置 [小计]
     */
    @JsonProperty("price_subtotal")
    public void setPrice_subtotal(Double  price_subtotal){
        this.price_subtotal = price_subtotal ;
        this.price_subtotalDirtyFlag = true ;
    }

     /**
     * 获取 [小计]脏标记
     */
    @JsonIgnore
    public boolean getPrice_subtotalDirtyFlag(){
        return this.price_subtotalDirtyFlag ;
    }   

    /**
     * 获取 [单价]
     */
    @JsonProperty("price_unit")
    public Double getPrice_unit(){
        return this.price_unit ;
    }

    /**
     * 设置 [单价]
     */
    @JsonProperty("price_unit")
    public void setPrice_unit(Double  price_unit){
        this.price_unit = price_unit ;
        this.price_unitDirtyFlag = true ;
    }

     /**
     * 获取 [单价]脏标记
     */
    @JsonIgnore
    public boolean getPrice_unitDirtyFlag(){
        return this.price_unitDirtyFlag ;
    }   

    /**
     * 获取 [产品]
     */
    @JsonProperty("product_id")
    public Integer getProduct_id(){
        return this.product_id ;
    }

    /**
     * 设置 [产品]
     */
    @JsonProperty("product_id")
    public void setProduct_id(Integer  product_id){
        this.product_id = product_id ;
        this.product_idDirtyFlag = true ;
    }

     /**
     * 获取 [产品]脏标记
     */
    @JsonIgnore
    public boolean getProduct_idDirtyFlag(){
        return this.product_idDirtyFlag ;
    }   

    /**
     * 获取 [产品]
     */
    @JsonProperty("product_id_text")
    public String getProduct_id_text(){
        return this.product_id_text ;
    }

    /**
     * 设置 [产品]
     */
    @JsonProperty("product_id_text")
    public void setProduct_id_text(String  product_id_text){
        this.product_id_text = product_id_text ;
        this.product_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [产品]脏标记
     */
    @JsonIgnore
    public boolean getProduct_id_textDirtyFlag(){
        return this.product_id_textDirtyFlag ;
    }   

    /**
     * 获取 [产品量度单位]
     */
    @JsonProperty("product_uom")
    public Integer getProduct_uom(){
        return this.product_uom ;
    }

    /**
     * 设置 [产品量度单位]
     */
    @JsonProperty("product_uom")
    public void setProduct_uom(Integer  product_uom){
        this.product_uom = product_uom ;
        this.product_uomDirtyFlag = true ;
    }

     /**
     * 获取 [产品量度单位]脏标记
     */
    @JsonIgnore
    public boolean getProduct_uomDirtyFlag(){
        return this.product_uomDirtyFlag ;
    }   

    /**
     * 获取 [数量]
     */
    @JsonProperty("product_uom_qty")
    public Double getProduct_uom_qty(){
        return this.product_uom_qty ;
    }

    /**
     * 设置 [数量]
     */
    @JsonProperty("product_uom_qty")
    public void setProduct_uom_qty(Double  product_uom_qty){
        this.product_uom_qty = product_uom_qty ;
        this.product_uom_qtyDirtyFlag = true ;
    }

     /**
     * 获取 [数量]脏标记
     */
    @JsonIgnore
    public boolean getProduct_uom_qtyDirtyFlag(){
        return this.product_uom_qtyDirtyFlag ;
    }   

    /**
     * 获取 [产品量度单位]
     */
    @JsonProperty("product_uom_text")
    public String getProduct_uom_text(){
        return this.product_uom_text ;
    }

    /**
     * 设置 [产品量度单位]
     */
    @JsonProperty("product_uom_text")
    public void setProduct_uom_text(String  product_uom_text){
        this.product_uom_text = product_uom_text ;
        this.product_uom_textDirtyFlag = true ;
    }

     /**
     * 获取 [产品量度单位]脏标记
     */
    @JsonIgnore
    public boolean getProduct_uom_textDirtyFlag(){
        return this.product_uom_textDirtyFlag ;
    }   

    /**
     * 获取 [维修单编＃]
     */
    @JsonProperty("repair_id")
    public Integer getRepair_id(){
        return this.repair_id ;
    }

    /**
     * 设置 [维修单编＃]
     */
    @JsonProperty("repair_id")
    public void setRepair_id(Integer  repair_id){
        this.repair_id = repair_id ;
        this.repair_idDirtyFlag = true ;
    }

     /**
     * 获取 [维修单编＃]脏标记
     */
    @JsonIgnore
    public boolean getRepair_idDirtyFlag(){
        return this.repair_idDirtyFlag ;
    }   

    /**
     * 获取 [维修单编＃]
     */
    @JsonProperty("repair_id_text")
    public String getRepair_id_text(){
        return this.repair_id_text ;
    }

    /**
     * 设置 [维修单编＃]
     */
    @JsonProperty("repair_id_text")
    public void setRepair_id_text(String  repair_id_text){
        this.repair_id_text = repair_id_text ;
        this.repair_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [维修单编＃]脏标记
     */
    @JsonIgnore
    public boolean getRepair_id_textDirtyFlag(){
        return this.repair_id_textDirtyFlag ;
    }   

    /**
     * 获取 [税]
     */
    @JsonProperty("tax_id")
    public String getTax_id(){
        return this.tax_id ;
    }

    /**
     * 设置 [税]
     */
    @JsonProperty("tax_id")
    public void setTax_id(String  tax_id){
        this.tax_id = tax_id ;
        this.tax_idDirtyFlag = true ;
    }

     /**
     * 获取 [税]脏标记
     */
    @JsonIgnore
    public boolean getTax_idDirtyFlag(){
        return this.tax_idDirtyFlag ;
    }   

    /**
     * 获取 [最后更新时间]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return this.write_date ;
    }

    /**
     * 设置 [最后更新时间]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新时间]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return this.write_dateDirtyFlag ;
    }   

    /**
     * 获取 [最后更新人]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return this.write_uid ;
    }

    /**
     * 设置 [最后更新人]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新人]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return this.write_uidDirtyFlag ;
    }   

    /**
     * 获取 [最后更新人]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return this.write_uid_text ;
    }

    /**
     * 设置 [最后更新人]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新人]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return this.write_uid_textDirtyFlag ;
    }   

    /**
     * 获取 [最后修改时间]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return this.__last_update ;
    }

    /**
     * 设置 [最后修改时间]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

     /**
     * 获取 [最后修改时间]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return this.__last_updateDirtyFlag ;
    }   



    public void fromMap(Map<String, Object> map) throws Exception {
		if(!(map.get("create_date") instanceof Boolean)&& map.get("create_date")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse((String)map.get("create_date"));
   			this.setCreate_date(new Timestamp(parse.getTime()));
		}
		if(!(map.get("create_uid") instanceof Boolean)&& map.get("create_uid")!=null){
			Object[] objs = (Object[])map.get("create_uid");
			if(objs.length > 0){
				this.setCreate_uid((Integer)objs[0]);
			}
		}
		if(!(map.get("create_uid") instanceof Boolean)&& map.get("create_uid")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("create_uid");
			if(objs.length > 1){
				this.setCreate_uid_text((String)objs[1]);
			}
		}
		if(!(map.get("display_name") instanceof Boolean)&& map.get("display_name")!=null){
			this.setDisplay_name((String)map.get("display_name"));
		}
		if(!(map.get("id") instanceof Boolean)&& map.get("id")!=null){
			this.setId((Integer)map.get("id"));
		}
		if(map.get("invoiced") instanceof Boolean){
			this.setInvoiced(((Boolean)map.get("invoiced"))? "true" : "false");
		}
		if(!(map.get("invoice_line_id") instanceof Boolean)&& map.get("invoice_line_id")!=null){
			Object[] objs = (Object[])map.get("invoice_line_id");
			if(objs.length > 0){
				this.setInvoice_line_id((Integer)objs[0]);
			}
		}
		if(!(map.get("invoice_line_id") instanceof Boolean)&& map.get("invoice_line_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("invoice_line_id");
			if(objs.length > 1){
				this.setInvoice_line_id_text((String)objs[1]);
			}
		}
		if(!(map.get("name") instanceof Boolean)&& map.get("name")!=null){
			this.setName((String)map.get("name"));
		}
		if(!(map.get("price_subtotal") instanceof Boolean)&& map.get("price_subtotal")!=null){
			this.setPrice_subtotal((Double)map.get("price_subtotal"));
		}
		if(!(map.get("price_unit") instanceof Boolean)&& map.get("price_unit")!=null){
			this.setPrice_unit((Double)map.get("price_unit"));
		}
		if(!(map.get("product_id") instanceof Boolean)&& map.get("product_id")!=null){
			Object[] objs = (Object[])map.get("product_id");
			if(objs.length > 0){
				this.setProduct_id((Integer)objs[0]);
			}
		}
		if(!(map.get("product_id") instanceof Boolean)&& map.get("product_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("product_id");
			if(objs.length > 1){
				this.setProduct_id_text((String)objs[1]);
			}
		}
		if(!(map.get("product_uom") instanceof Boolean)&& map.get("product_uom")!=null){
			Object[] objs = (Object[])map.get("product_uom");
			if(objs.length > 0){
				this.setProduct_uom((Integer)objs[0]);
			}
		}
		if(!(map.get("product_uom_qty") instanceof Boolean)&& map.get("product_uom_qty")!=null){
			this.setProduct_uom_qty((Double)map.get("product_uom_qty"));
		}
		if(!(map.get("product_uom") instanceof Boolean)&& map.get("product_uom")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("product_uom");
			if(objs.length > 1){
				this.setProduct_uom_text((String)objs[1]);
			}
		}
		if(!(map.get("repair_id") instanceof Boolean)&& map.get("repair_id")!=null){
			Object[] objs = (Object[])map.get("repair_id");
			if(objs.length > 0){
				this.setRepair_id((Integer)objs[0]);
			}
		}
		if(!(map.get("repair_id") instanceof Boolean)&& map.get("repair_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("repair_id");
			if(objs.length > 1){
				this.setRepair_id_text((String)objs[1]);
			}
		}
		if(!(map.get("tax_id") instanceof Boolean)&& map.get("tax_id")!=null){
			Object[] objs = (Object[])map.get("tax_id");
			if(objs.length > 0){
				Integer[] tax_id = Arrays.copyOfRange(objs,0,objs.length,Integer[].class);
				this.setTax_id(Arrays.toString(tax_id).replace(" ",""));
			}
		}
		if(!(map.get("write_date") instanceof Boolean)&& map.get("write_date")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse((String)map.get("write_date"));
   			this.setWrite_date(new Timestamp(parse.getTime()));
		}
		if(!(map.get("write_uid") instanceof Boolean)&& map.get("write_uid")!=null){
			Object[] objs = (Object[])map.get("write_uid");
			if(objs.length > 0){
				this.setWrite_uid((Integer)objs[0]);
			}
		}
		if(!(map.get("write_uid") instanceof Boolean)&& map.get("write_uid")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("write_uid");
			if(objs.length > 1){
				this.setWrite_uid_text((String)objs[1]);
			}
		}
		if(!(map.get("__last_update") instanceof Boolean)&& map.get("__last_update")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse((String)map.get("__last_update"));
   			this.set__last_update(new Timestamp(parse.getTime()));
		}
	}

	public Map<String, Object> toMap() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		if(this.getCreate_date()!=null&&this.getCreate_dateDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String datetimeStr = sdf.format(this.getCreate_date());
			map.put("create_date",datetimeStr);
		}else if(this.getCreate_dateDirtyFlag()){
			map.put("create_date",false);
		}
		if(this.getCreate_uid()!=null&&this.getCreate_uidDirtyFlag()){
			map.put("create_uid",this.getCreate_uid());
		}else if(this.getCreate_uidDirtyFlag()){
			map.put("create_uid",false);
		}
		if(this.getCreate_uid_text()!=null&&this.getCreate_uid_textDirtyFlag()){
			//忽略文本外键create_uid_text
		}else if(this.getCreate_uid_textDirtyFlag()){
			map.put("create_uid",false);
		}
		if(this.getDisplay_name()!=null&&this.getDisplay_nameDirtyFlag()){
			map.put("display_name",this.getDisplay_name());
		}else if(this.getDisplay_nameDirtyFlag()){
			map.put("display_name",false);
		}
		if(this.getId()!=null&&this.getIdDirtyFlag()){
			map.put("id",this.getId());
		}else if(this.getIdDirtyFlag()){
			map.put("id",false);
		}
		if(this.getInvoiced()!=null&&this.getInvoicedDirtyFlag()){
			map.put("invoiced",Boolean.parseBoolean(this.getInvoiced()));		
		}		if(this.getInvoice_line_id()!=null&&this.getInvoice_line_idDirtyFlag()){
			map.put("invoice_line_id",this.getInvoice_line_id());
		}else if(this.getInvoice_line_idDirtyFlag()){
			map.put("invoice_line_id",false);
		}
		if(this.getInvoice_line_id_text()!=null&&this.getInvoice_line_id_textDirtyFlag()){
			//忽略文本外键invoice_line_id_text
		}else if(this.getInvoice_line_id_textDirtyFlag()){
			map.put("invoice_line_id",false);
		}
		if(this.getName()!=null&&this.getNameDirtyFlag()){
			map.put("name",this.getName());
		}else if(this.getNameDirtyFlag()){
			map.put("name",false);
		}
		if(this.getPrice_subtotal()!=null&&this.getPrice_subtotalDirtyFlag()){
			map.put("price_subtotal",this.getPrice_subtotal());
		}else if(this.getPrice_subtotalDirtyFlag()){
			map.put("price_subtotal",false);
		}
		if(this.getPrice_unit()!=null&&this.getPrice_unitDirtyFlag()){
			map.put("price_unit",this.getPrice_unit());
		}else if(this.getPrice_unitDirtyFlag()){
			map.put("price_unit",false);
		}
		if(this.getProduct_id()!=null&&this.getProduct_idDirtyFlag()){
			map.put("product_id",this.getProduct_id());
		}else if(this.getProduct_idDirtyFlag()){
			map.put("product_id",false);
		}
		if(this.getProduct_id_text()!=null&&this.getProduct_id_textDirtyFlag()){
			//忽略文本外键product_id_text
		}else if(this.getProduct_id_textDirtyFlag()){
			map.put("product_id",false);
		}
		if(this.getProduct_uom()!=null&&this.getProduct_uomDirtyFlag()){
			map.put("product_uom",this.getProduct_uom());
		}else if(this.getProduct_uomDirtyFlag()){
			map.put("product_uom",false);
		}
		if(this.getProduct_uom_qty()!=null&&this.getProduct_uom_qtyDirtyFlag()){
			map.put("product_uom_qty",this.getProduct_uom_qty());
		}else if(this.getProduct_uom_qtyDirtyFlag()){
			map.put("product_uom_qty",false);
		}
		if(this.getProduct_uom_text()!=null&&this.getProduct_uom_textDirtyFlag()){
			//忽略文本外键product_uom_text
		}else if(this.getProduct_uom_textDirtyFlag()){
			map.put("product_uom",false);
		}
		if(this.getRepair_id()!=null&&this.getRepair_idDirtyFlag()){
			map.put("repair_id",this.getRepair_id());
		}else if(this.getRepair_idDirtyFlag()){
			map.put("repair_id",false);
		}
		if(this.getRepair_id_text()!=null&&this.getRepair_id_textDirtyFlag()){
			//忽略文本外键repair_id_text
		}else if(this.getRepair_id_textDirtyFlag()){
			map.put("repair_id",false);
		}
		if(this.getTax_id()!=null&&this.getTax_idDirtyFlag()){
			map.put("tax_id",this.getTax_id());
		}else if(this.getTax_idDirtyFlag()){
			map.put("tax_id",false);
		}
		if(this.getWrite_date()!=null&&this.getWrite_dateDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String datetimeStr = sdf.format(this.getWrite_date());
			map.put("write_date",datetimeStr);
		}else if(this.getWrite_dateDirtyFlag()){
			map.put("write_date",false);
		}
		if(this.getWrite_uid()!=null&&this.getWrite_uidDirtyFlag()){
			map.put("write_uid",this.getWrite_uid());
		}else if(this.getWrite_uidDirtyFlag()){
			map.put("write_uid",false);
		}
		if(this.getWrite_uid_text()!=null&&this.getWrite_uid_textDirtyFlag()){
			//忽略文本外键write_uid_text
		}else if(this.getWrite_uid_textDirtyFlag()){
			map.put("write_uid",false);
		}
		if(this.get__last_update()!=null&&this.get__last_updateDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String datetimeStr = sdf.format(this.get__last_update());
			map.put("__last_update",datetimeStr);
		}else if(this.get__last_updateDirtyFlag()){
			map.put("__last_update",false);
		}
		return map;
	}

}
