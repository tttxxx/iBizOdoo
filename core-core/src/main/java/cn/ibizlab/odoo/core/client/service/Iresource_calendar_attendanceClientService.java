package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Iresource_calendar_attendance;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[resource_calendar_attendance] 服务对象接口
 */
public interface Iresource_calendar_attendanceClientService{

    public Iresource_calendar_attendance createModel() ;

    public void removeBatch(List<Iresource_calendar_attendance> resource_calendar_attendances);

    public void update(Iresource_calendar_attendance resource_calendar_attendance);

    public void get(Iresource_calendar_attendance resource_calendar_attendance);

    public void remove(Iresource_calendar_attendance resource_calendar_attendance);

    public void updateBatch(List<Iresource_calendar_attendance> resource_calendar_attendances);

    public void create(Iresource_calendar_attendance resource_calendar_attendance);

    public Page<Iresource_calendar_attendance> search(SearchContext context);

    public void createBatch(List<Iresource_calendar_attendance> resource_calendar_attendances);

    public Page<Iresource_calendar_attendance> select(SearchContext context);

}
