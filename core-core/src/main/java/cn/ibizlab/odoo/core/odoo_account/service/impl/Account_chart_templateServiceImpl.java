package cn.ibizlab.odoo.core.odoo_account.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_account.domain.Account_chart_template;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_chart_templateSearchContext;
import cn.ibizlab.odoo.core.odoo_account.service.IAccount_chart_templateService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;



import cn.ibizlab.odoo.core.odoo_account.client.account_chart_templateOdooClient;
import cn.ibizlab.odoo.core.odoo_account.clientmodel.account_chart_templateClientModel;

/**
 * 实体[科目表模版] 服务对象接口实现
 */
@Slf4j
@Service
public class Account_chart_templateServiceImpl implements IAccount_chart_templateService {

    @Autowired
    account_chart_templateOdooClient account_chart_templateOdooClient;


    @Override
    public Account_chart_template get(Integer id) {
        account_chart_templateClientModel clientModel = new account_chart_templateClientModel();
        clientModel.setId(id);
		account_chart_templateOdooClient.get(clientModel);
        Account_chart_template et = convert2Domain(clientModel,null);
        if(et==null){
            et=new Account_chart_template();
            et.setId(id);
        }
        else{
        }

        return  et;
    }

    @Override
    public boolean remove(Integer id) {
        account_chart_templateClientModel clientModel = new account_chart_templateClientModel();
        clientModel.setId(id);
		account_chart_templateOdooClient.remove(clientModel);
        return true; 
    }

    public void removeBatch(Collection<Integer> idList){
    }

    @Override
    public boolean update(Account_chart_template et) {
        account_chart_templateClientModel clientModel = convert2Model(et,null);
		account_chart_templateOdooClient.update(clientModel);
        Account_chart_template rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }
    
    public void updateBatch(List<Account_chart_template> list){
    }

    @Override
    public boolean create(Account_chart_template et) {
        account_chart_templateClientModel clientModel = convert2Model(et,null);
		account_chart_templateOdooClient.create(clientModel);
        Account_chart_template rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Account_chart_template> list){
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Account_chart_template> searchDefault(Account_chart_templateSearchContext context) {
        List<Account_chart_template> list = new ArrayList<Account_chart_template>();
        Page<account_chart_templateClientModel> clientModelList = account_chart_templateOdooClient.search(context);
        for(account_chart_templateClientModel clientModel : clientModelList){
            list.add( this.convert2Domain(clientModel,null));
        }
        return new PageImpl<Account_chart_template>(list, context.getPageable(), clientModelList.getTotalElements());
    }

    public account_chart_templateClientModel convert2Model(Account_chart_template domain , account_chart_templateClientModel model) {
        if(domain == null)
            return model ;
        if(model==null){
            model = new account_chart_templateClientModel();

        }
        if((Boolean) domain.getExtensionparams().get("dirtyflagenable")){
            if((Boolean) domain.getExtensionparams().get("code_digitsdirtyflag"))
                model.setCode_digits(domain.getCodeDigits());
            if((Boolean) domain.getExtensionparams().get("transfer_account_code_prefixdirtyflag"))
                model.setTransfer_account_code_prefix(domain.getTransferAccountCodePrefix());
            if((Boolean) domain.getExtensionparams().get("bank_account_code_prefixdirtyflag"))
                model.setBank_account_code_prefix(domain.getBankAccountCodePrefix());
            if((Boolean) domain.getExtensionparams().get("spoken_languagesdirtyflag"))
                model.setSpoken_languages(domain.getSpokenLanguages());
            if((Boolean) domain.getExtensionparams().get("tax_template_idsdirtyflag"))
                model.setTax_template_ids(domain.getTaxTemplateIds());
            if((Boolean) domain.getExtensionparams().get("visibledirtyflag"))
                model.setVisible(domain.getVisible());
            if((Boolean) domain.getExtensionparams().get("account_idsdirtyflag"))
                model.setAccount_ids(domain.getAccountIds());
            if((Boolean) domain.getExtensionparams().get("complete_tax_setdirtyflag"))
                model.setComplete_tax_set(domain.getCompleteTaxSet());
            if((Boolean) domain.getExtensionparams().get("iddirtyflag"))
                model.setId(domain.getId());
            if((Boolean) domain.getExtensionparams().get("cash_account_code_prefixdirtyflag"))
                model.setCash_account_code_prefix(domain.getCashAccountCodePrefix());
            if((Boolean) domain.getExtensionparams().get("display_namedirtyflag"))
                model.setDisplay_name(domain.getDisplayName());
            if((Boolean) domain.getExtensionparams().get("create_datedirtyflag"))
                model.setCreate_date(domain.getCreateDate());
            if((Boolean) domain.getExtensionparams().get("use_anglo_saxondirtyflag"))
                model.setUse_anglo_saxon(domain.getUseAngloSaxon());
            if((Boolean) domain.getExtensionparams().get("__last_updatedirtyflag"))
                model.set__last_update(domain.getLastUpdate());
            if((Boolean) domain.getExtensionparams().get("write_datedirtyflag"))
                model.setWrite_date(domain.getWriteDate());
            if((Boolean) domain.getExtensionparams().get("namedirtyflag"))
                model.setName(domain.getName());
            if((Boolean) domain.getExtensionparams().get("expense_currency_exchange_account_id_textdirtyflag"))
                model.setExpense_currency_exchange_account_id_text(domain.getExpenseCurrencyExchangeAccountIdText());
            if((Boolean) domain.getExtensionparams().get("property_account_receivable_id_textdirtyflag"))
                model.setProperty_account_receivable_id_text(domain.getPropertyAccountReceivableIdText());
            if((Boolean) domain.getExtensionparams().get("currency_id_textdirtyflag"))
                model.setCurrency_id_text(domain.getCurrencyIdText());
            if((Boolean) domain.getExtensionparams().get("property_account_expense_id_textdirtyflag"))
                model.setProperty_account_expense_id_text(domain.getPropertyAccountExpenseIdText());
            if((Boolean) domain.getExtensionparams().get("create_uid_textdirtyflag"))
                model.setCreate_uid_text(domain.getCreateUidText());
            if((Boolean) domain.getExtensionparams().get("parent_id_textdirtyflag"))
                model.setParent_id_text(domain.getParentIdText());
            if((Boolean) domain.getExtensionparams().get("property_stock_account_input_categ_id_textdirtyflag"))
                model.setProperty_stock_account_input_categ_id_text(domain.getPropertyStockAccountInputCategIdText());
            if((Boolean) domain.getExtensionparams().get("property_account_income_id_textdirtyflag"))
                model.setProperty_account_income_id_text(domain.getPropertyAccountIncomeIdText());
            if((Boolean) domain.getExtensionparams().get("property_account_expense_categ_id_textdirtyflag"))
                model.setProperty_account_expense_categ_id_text(domain.getPropertyAccountExpenseCategIdText());
            if((Boolean) domain.getExtensionparams().get("write_uid_textdirtyflag"))
                model.setWrite_uid_text(domain.getWriteUidText());
            if((Boolean) domain.getExtensionparams().get("income_currency_exchange_account_id_textdirtyflag"))
                model.setIncome_currency_exchange_account_id_text(domain.getIncomeCurrencyExchangeAccountIdText());
            if((Boolean) domain.getExtensionparams().get("property_account_payable_id_textdirtyflag"))
                model.setProperty_account_payable_id_text(domain.getPropertyAccountPayableIdText());
            if((Boolean) domain.getExtensionparams().get("property_account_income_categ_id_textdirtyflag"))
                model.setProperty_account_income_categ_id_text(domain.getPropertyAccountIncomeCategIdText());
            if((Boolean) domain.getExtensionparams().get("property_stock_valuation_account_id_textdirtyflag"))
                model.setProperty_stock_valuation_account_id_text(domain.getPropertyStockValuationAccountIdText());
            if((Boolean) domain.getExtensionparams().get("property_stock_account_output_categ_id_textdirtyflag"))
                model.setProperty_stock_account_output_categ_id_text(domain.getPropertyStockAccountOutputCategIdText());
            if((Boolean) domain.getExtensionparams().get("property_stock_valuation_account_iddirtyflag"))
                model.setProperty_stock_valuation_account_id(domain.getPropertyStockValuationAccountId());
            if((Boolean) domain.getExtensionparams().get("expense_currency_exchange_account_iddirtyflag"))
                model.setExpense_currency_exchange_account_id(domain.getExpenseCurrencyExchangeAccountId());
            if((Boolean) domain.getExtensionparams().get("property_account_payable_iddirtyflag"))
                model.setProperty_account_payable_id(domain.getPropertyAccountPayableId());
            if((Boolean) domain.getExtensionparams().get("property_account_income_categ_iddirtyflag"))
                model.setProperty_account_income_categ_id(domain.getPropertyAccountIncomeCategId());
            if((Boolean) domain.getExtensionparams().get("property_stock_account_output_categ_iddirtyflag"))
                model.setProperty_stock_account_output_categ_id(domain.getPropertyStockAccountOutputCategId());
            if((Boolean) domain.getExtensionparams().get("property_account_expense_iddirtyflag"))
                model.setProperty_account_expense_id(domain.getPropertyAccountExpenseId());
            if((Boolean) domain.getExtensionparams().get("currency_iddirtyflag"))
                model.setCurrency_id(domain.getCurrencyId());
            if((Boolean) domain.getExtensionparams().get("parent_iddirtyflag"))
                model.setParent_id(domain.getParentId());
            if((Boolean) domain.getExtensionparams().get("property_account_receivable_iddirtyflag"))
                model.setProperty_account_receivable_id(domain.getPropertyAccountReceivableId());
            if((Boolean) domain.getExtensionparams().get("write_uiddirtyflag"))
                model.setWrite_uid(domain.getWriteUid());
            if((Boolean) domain.getExtensionparams().get("property_account_expense_categ_iddirtyflag"))
                model.setProperty_account_expense_categ_id(domain.getPropertyAccountExpenseCategId());
            if((Boolean) domain.getExtensionparams().get("income_currency_exchange_account_iddirtyflag"))
                model.setIncome_currency_exchange_account_id(domain.getIncomeCurrencyExchangeAccountId());
            if((Boolean) domain.getExtensionparams().get("create_uiddirtyflag"))
                model.setCreate_uid(domain.getCreateUid());
            if((Boolean) domain.getExtensionparams().get("property_stock_account_input_categ_iddirtyflag"))
                model.setProperty_stock_account_input_categ_id(domain.getPropertyStockAccountInputCategId());
            if((Boolean) domain.getExtensionparams().get("property_account_income_iddirtyflag"))
                model.setProperty_account_income_id(domain.getPropertyAccountIncomeId());
            domain.getFocusNull().clear();
        }
        return model ;
    }

    public Account_chart_template convert2Domain( account_chart_templateClientModel model ,Account_chart_template domain) {
        if(model == null)
            return domain ;
        if(domain == null){
            domain = new Account_chart_template();
        }

        if(model.getCode_digitsDirtyFlag())
            domain.setCodeDigits(model.getCode_digits());
        if(model.getTransfer_account_code_prefixDirtyFlag())
            domain.setTransferAccountCodePrefix(model.getTransfer_account_code_prefix());
        if(model.getBank_account_code_prefixDirtyFlag())
            domain.setBankAccountCodePrefix(model.getBank_account_code_prefix());
        if(model.getSpoken_languagesDirtyFlag())
            domain.setSpokenLanguages(model.getSpoken_languages());
        if(model.getTax_template_idsDirtyFlag())
            domain.setTaxTemplateIds(model.getTax_template_ids());
        if(model.getVisibleDirtyFlag())
            domain.setVisible(model.getVisible());
        if(model.getAccount_idsDirtyFlag())
            domain.setAccountIds(model.getAccount_ids());
        if(model.getComplete_tax_setDirtyFlag())
            domain.setCompleteTaxSet(model.getComplete_tax_set());
        if(model.getIdDirtyFlag())
            domain.setId(model.getId());
        if(model.getCash_account_code_prefixDirtyFlag())
            domain.setCashAccountCodePrefix(model.getCash_account_code_prefix());
        if(model.getDisplay_nameDirtyFlag())
            domain.setDisplayName(model.getDisplay_name());
        if(model.getCreate_dateDirtyFlag())
            domain.setCreateDate(model.getCreate_date());
        if(model.getUse_anglo_saxonDirtyFlag())
            domain.setUseAngloSaxon(model.getUse_anglo_saxon());
        if(model.get__last_updateDirtyFlag())
            domain.setLastUpdate(model.get__last_update());
        if(model.getWrite_dateDirtyFlag())
            domain.setWriteDate(model.getWrite_date());
        if(model.getNameDirtyFlag())
            domain.setName(model.getName());
        if(model.getExpense_currency_exchange_account_id_textDirtyFlag())
            domain.setExpenseCurrencyExchangeAccountIdText(model.getExpense_currency_exchange_account_id_text());
        if(model.getProperty_account_receivable_id_textDirtyFlag())
            domain.setPropertyAccountReceivableIdText(model.getProperty_account_receivable_id_text());
        if(model.getCurrency_id_textDirtyFlag())
            domain.setCurrencyIdText(model.getCurrency_id_text());
        if(model.getProperty_account_expense_id_textDirtyFlag())
            domain.setPropertyAccountExpenseIdText(model.getProperty_account_expense_id_text());
        if(model.getCreate_uid_textDirtyFlag())
            domain.setCreateUidText(model.getCreate_uid_text());
        if(model.getParent_id_textDirtyFlag())
            domain.setParentIdText(model.getParent_id_text());
        if(model.getProperty_stock_account_input_categ_id_textDirtyFlag())
            domain.setPropertyStockAccountInputCategIdText(model.getProperty_stock_account_input_categ_id_text());
        if(model.getProperty_account_income_id_textDirtyFlag())
            domain.setPropertyAccountIncomeIdText(model.getProperty_account_income_id_text());
        if(model.getProperty_account_expense_categ_id_textDirtyFlag())
            domain.setPropertyAccountExpenseCategIdText(model.getProperty_account_expense_categ_id_text());
        if(model.getWrite_uid_textDirtyFlag())
            domain.setWriteUidText(model.getWrite_uid_text());
        if(model.getIncome_currency_exchange_account_id_textDirtyFlag())
            domain.setIncomeCurrencyExchangeAccountIdText(model.getIncome_currency_exchange_account_id_text());
        if(model.getProperty_account_payable_id_textDirtyFlag())
            domain.setPropertyAccountPayableIdText(model.getProperty_account_payable_id_text());
        if(model.getProperty_account_income_categ_id_textDirtyFlag())
            domain.setPropertyAccountIncomeCategIdText(model.getProperty_account_income_categ_id_text());
        if(model.getProperty_stock_valuation_account_id_textDirtyFlag())
            domain.setPropertyStockValuationAccountIdText(model.getProperty_stock_valuation_account_id_text());
        if(model.getProperty_stock_account_output_categ_id_textDirtyFlag())
            domain.setPropertyStockAccountOutputCategIdText(model.getProperty_stock_account_output_categ_id_text());
        if(model.getProperty_stock_valuation_account_idDirtyFlag())
            domain.setPropertyStockValuationAccountId(model.getProperty_stock_valuation_account_id());
        if(model.getExpense_currency_exchange_account_idDirtyFlag())
            domain.setExpenseCurrencyExchangeAccountId(model.getExpense_currency_exchange_account_id());
        if(model.getProperty_account_payable_idDirtyFlag())
            domain.setPropertyAccountPayableId(model.getProperty_account_payable_id());
        if(model.getProperty_account_income_categ_idDirtyFlag())
            domain.setPropertyAccountIncomeCategId(model.getProperty_account_income_categ_id());
        if(model.getProperty_stock_account_output_categ_idDirtyFlag())
            domain.setPropertyStockAccountOutputCategId(model.getProperty_stock_account_output_categ_id());
        if(model.getProperty_account_expense_idDirtyFlag())
            domain.setPropertyAccountExpenseId(model.getProperty_account_expense_id());
        if(model.getCurrency_idDirtyFlag())
            domain.setCurrencyId(model.getCurrency_id());
        if(model.getParent_idDirtyFlag())
            domain.setParentId(model.getParent_id());
        if(model.getProperty_account_receivable_idDirtyFlag())
            domain.setPropertyAccountReceivableId(model.getProperty_account_receivable_id());
        if(model.getWrite_uidDirtyFlag())
            domain.setWriteUid(model.getWrite_uid());
        if(model.getProperty_account_expense_categ_idDirtyFlag())
            domain.setPropertyAccountExpenseCategId(model.getProperty_account_expense_categ_id());
        if(model.getIncome_currency_exchange_account_idDirtyFlag())
            domain.setIncomeCurrencyExchangeAccountId(model.getIncome_currency_exchange_account_id());
        if(model.getCreate_uidDirtyFlag())
            domain.setCreateUid(model.getCreate_uid());
        if(model.getProperty_stock_account_input_categ_idDirtyFlag())
            domain.setPropertyStockAccountInputCategId(model.getProperty_stock_account_input_categ_id());
        if(model.getProperty_account_income_idDirtyFlag())
            domain.setPropertyAccountIncomeId(model.getProperty_account_income_id());
        return domain ;
    }

}

    



