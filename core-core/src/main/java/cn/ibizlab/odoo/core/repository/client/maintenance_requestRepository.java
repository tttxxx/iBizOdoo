package cn.ibizlab.odoo.core.repository.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import cn.ibizlab.odoo.core.repository.client.po.maintenance_request;

/**
 * 实体[maintenance_request] 服务对象接口
 */
public interface maintenance_requestRepository{


    public maintenance_request createPO() ;
        public List<maintenance_request> search();

        public void updateBatch(maintenance_request maintenance_request);

        public void create(maintenance_request maintenance_request);

        public void remove(String id);

        public void createBatch(maintenance_request maintenance_request);

        public void get(String id);

        public void update(maintenance_request maintenance_request);

        public void removeBatch(String id);


}
