package cn.ibizlab.odoo.core.repository.po ;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.math.BigInteger;

import cn.ibizlab.odoo.core.odoo_base.filter.Res_companySearchContext;

/**
 * 实体 [公司] 存储模型
 */
public interface Res_company{

    /**
     * 公司口号
     */
    String getReport_header();

    void setReport_header(String report_header);

    /**
     * 获取 [公司口号]脏标记
     */
    boolean getReport_headerDirtyFlag();

    /**
     * 国家/地区
     */
    Integer getCountry_id();

    void setCountry_id(Integer country_id);

    /**
     * 获取 [国家/地区]脏标记
     */
    boolean getCountry_idDirtyFlag();

    /**
     * 正进行销售面板的状态
     */
    String getSale_quotation_onboarding_state();

    void setSale_quotation_onboarding_state(String sale_quotation_onboarding_state);

    /**
     * 获取 [正进行销售面板的状态]脏标记
     */
    boolean getSale_quotation_onboarding_stateDirtyFlag();

    /**
     * 默认报价有效期（日）
     */
    Integer getQuotation_validity_days();

    void setQuotation_validity_days(Integer quotation_validity_days);

    /**
     * 获取 [默认报价有效期（日）]脏标记
     */
    boolean getQuotation_validity_daysDirtyFlag();

    /**
     * 有待被确认的发票步骤的状态
     */
    String getAccount_onboarding_invoice_layout_state();

    void setAccount_onboarding_invoice_layout_state(String account_onboarding_invoice_layout_state);

    /**
     * 获取 [有待被确认的发票步骤的状态]脏标记
     */
    boolean getAccount_onboarding_invoice_layout_stateDirtyFlag();

    /**
     * 科目号码
     */
    String getAccount_no();

    void setAccount_no(String account_no);

    /**
     * 获取 [科目号码]脏标记
     */
    boolean getAccount_noDirtyFlag();

    /**
     * 银行科目的前缀
     */
    String getBank_account_code_prefix();

    void setBank_account_code_prefix(String bank_account_code_prefix);

    /**
     * 获取 [银行科目的前缀]脏标记
     */
    boolean getBank_account_code_prefixDirtyFlag();

    /**
     * 银行日记账
     */
    String getBank_journal_ids();

    void setBank_journal_ids(String bank_journal_ids);

    /**
     * 获取 [银行日记账]脏标记
     */
    boolean getBank_journal_idsDirtyFlag();

    /**
     * 邮政编码
     */
    String getZip();

    void setZip(String zip);

    /**
     * 获取 [邮政编码]脏标记
     */
    boolean getZipDirtyFlag();

    /**
     * 非顾问的锁定日期
     */
    Timestamp getPeriod_lock_date();

    void setPeriod_lock_date(Timestamp period_lock_date);

    /**
     * 获取 [非顾问的锁定日期]脏标记
     */
    boolean getPeriod_lock_dateDirtyFlag();

    /**
     * 状态
     */
    Integer getState_id();

    void setState_id(Integer state_id);

    /**
     * 获取 [状态]脏标记
     */
    boolean getState_idDirtyFlag();

    /**
     * 使用现金收付制
     */
    String getTax_exigibility();

    void setTax_exigibility(String tax_exigibility);

    /**
     * 获取 [使用现金收付制]脏标记
     */
    boolean getTax_exigibilityDirtyFlag();

    /**
     * 工作时间
     */
    String getResource_calendar_ids();

    void setResource_calendar_ids(String resource_calendar_ids);

    /**
     * 获取 [工作时间]脏标记
     */
    boolean getResource_calendar_idsDirtyFlag();

    /**
     * 银行账户
     */
    String getBank_ids();

    void setBank_ids(String bank_ids);

    /**
     * 获取 [银行账户]脏标记
     */
    boolean getBank_idsDirtyFlag();

    /**
     * 处于会计面板的状态
     */
    String getAccount_dashboard_onboarding_state();

    void setAccount_dashboard_onboarding_state(String account_dashboard_onboarding_state);

    /**
     * 获取 [处于会计面板的状态]脏标记
     */
    boolean getAccount_dashboard_onboarding_stateDirtyFlag();

    /**
     * 连接在一起的库存移动的日期变化传播的最小差值。
     */
    Integer getPropagation_minimum_delta();

    void setPropagation_minimum_delta(Integer propagation_minimum_delta);

    /**
     * 获取 [连接在一起的库存移动的日期变化传播的最小差值。]脏标记
     */
    boolean getPropagation_minimum_deltaDirtyFlag();

    /**
     * 创建时间
     */
    Timestamp getCreate_date();

    void setCreate_date(Timestamp create_date);

    /**
     * 获取 [创建时间]脏标记
     */
    boolean getCreate_dateDirtyFlag();

    /**
     * 颜色
     */
    String getSnailmail_color();

    void setSnailmail_color(String snailmail_color);

    /**
     * 获取 [颜色]脏标记
     */
    boolean getSnailmail_colorDirtyFlag();

    /**
     * 逾期追款消息
     */
    String getOverdue_msg();

    void setOverdue_msg(String overdue_msg);

    /**
     * 获取 [逾期追款消息]脏标记
     */
    boolean getOverdue_msgDirtyFlag();

    /**
     * 城市
     */
    String getCity();

    void setCity(String city);

    /**
     * 获取 [城市]脏标记
     */
    boolean getCityDirtyFlag();

    /**
     * 科目状态
     */
    String getAccount_setup_coa_state();

    void setAccount_setup_coa_state(String account_setup_coa_state);

    /**
     * 获取 [科目状态]脏标记
     */
    boolean getAccount_setup_coa_stateDirtyFlag();

    /**
     * 默认信息类型
     */
    String getInvoice_reference_type();

    void setInvoice_reference_type(String invoice_reference_type);

    /**
     * 获取 [默认信息类型]脏标记
     */
    boolean getInvoice_reference_typeDirtyFlag();

    /**
     * 预设邮件
     */
    String getCatchall();

    void setCatchall(String catchall);

    /**
     * 获取 [预设邮件]脏标记
     */
    boolean getCatchallDirtyFlag();

    /**
     * 显示名称
     */
    String getDisplay_name();

    void setDisplay_name(String display_name);

    /**
     * 获取 [显示名称]脏标记
     */
    boolean getDisplay_nameDirtyFlag();

    /**
     * 使用anglo-saxon会计
     */
    String getAnglo_saxon_accounting();

    void setAnglo_saxon_accounting(String anglo_saxon_accounting);

    /**
     * 获取 [使用anglo-saxon会计]脏标记
     */
    boolean getAnglo_saxon_accountingDirtyFlag();

    /**
     * 双面
     */
    String getSnailmail_duplex();

    void setSnailmail_duplex(String snailmail_duplex);

    /**
     * 获取 [双面]脏标记
     */
    boolean getSnailmail_duplexDirtyFlag();

    /**
     * GitHub账户
     */
    String getSocial_github();

    void setSocial_github(String social_github);

    /**
     * 获取 [GitHub账户]脏标记
     */
    boolean getSocial_githubDirtyFlag();

    /**
     * 有待被确认的银行数据步骤的状态
     */
    String getAccount_setup_bank_data_state();

    void setAccount_setup_bank_data_state(String account_setup_bank_data_state);

    /**
     * 获取 [有待被确认的银行数据步骤的状态]脏标记
     */
    boolean getAccount_setup_bank_data_stateDirtyFlag();

    /**
     * ID
     */
    Integer getId();

    void setId(Integer id);

    /**
     * 获取 [ID]脏标记
     */
    boolean getIdDirtyFlag();

    /**
     * 街道 2
     */
    String getStreet2();

    void setStreet2(String street2);

    /**
     * 获取 [街道 2]脏标记
     */
    boolean getStreet2DirtyFlag();

    /**
     * 预计会计科目表
     */
    String getExpects_chart_of_accounts();

    void setExpects_chart_of_accounts(String expects_chart_of_accounts);

    /**
     * 获取 [预计会计科目表]脏标记
     */
    boolean getExpects_chart_of_accountsDirtyFlag();

    /**
     * 转账帐户的前缀
     */
    String getTransfer_account_code_prefix();

    void setTransfer_account_code_prefix(String transfer_account_code_prefix);

    /**
     * 获取 [转账帐户的前缀]脏标记
     */
    boolean getTransfer_account_code_prefixDirtyFlag();

    /**
     * 会计年度的最后一天
     */
    Integer getFiscalyear_last_day();

    void setFiscalyear_last_day(Integer fiscalyear_last_day);

    /**
     * 获取 [会计年度的最后一天]脏标记
     */
    boolean getFiscalyear_last_dayDirtyFlag();

    /**
     * 接受的用户
     */
    String getUser_ids();

    void setUser_ids(String user_ids);

    /**
     * 获取 [接受的用户]脏标记
     */
    boolean getUser_idsDirtyFlag();

    /**
     * 银行核销阈值
     */
    Timestamp getAccount_bank_reconciliation_start();

    void setAccount_bank_reconciliation_start(Timestamp account_bank_reconciliation_start);

    /**
     * 获取 [银行核销阈值]脏标记
     */
    boolean getAccount_bank_reconciliation_startDirtyFlag();

    /**
     * 在线支付
     */
    String getPortal_confirmation_pay();

    void setPortal_confirmation_pay(String portal_confirmation_pay);

    /**
     * 获取 [在线支付]脏标记
     */
    boolean getPortal_confirmation_payDirtyFlag();

    /**
     * 显示SEPA QR码
     */
    String getQr_code();

    void setQr_code(String qr_code);

    /**
     * 获取 [显示SEPA QR码]脏标记
     */
    boolean getQr_codeDirtyFlag();

    /**
     * 街道
     */
    String getStreet();

    void setStreet(String street);

    /**
     * 获取 [街道]脏标记
     */
    boolean getStreetDirtyFlag();

    /**
     * 处于会计发票面板的状态
     */
    String getAccount_invoice_onboarding_state();

    void setAccount_invoice_onboarding_state(String account_invoice_onboarding_state);

    /**
     * 获取 [处于会计发票面板的状态]脏标记
     */
    boolean getAccount_invoice_onboarding_stateDirtyFlag();

    /**
     * 下级公司
     */
    String getChild_ids();

    void setChild_ids(String child_ids);

    /**
     * 获取 [下级公司]脏标记
     */
    boolean getChild_idsDirtyFlag();

    /**
     * 序号
     */
    Integer getSequence();

    void setSequence(Integer sequence);

    /**
     * 获取 [序号]脏标记
     */
    boolean getSequenceDirtyFlag();

    /**
     * 命名规则
     */
    Integer getNomenclature_id();

    void setNomenclature_id(Integer nomenclature_id);

    /**
     * 获取 [命名规则]脏标记
     */
    boolean getNomenclature_idDirtyFlag();

    /**
     * Google+账户
     */
    String getSocial_googleplus();

    void setSocial_googleplus(String social_googleplus);

    /**
     * 获取 [Google+账户]脏标记
     */
    boolean getSocial_googleplusDirtyFlag();

    /**
     * 入职支付收单机构的状态
     */
    String getPayment_acquirer_onboarding_state();

    void setPayment_acquirer_onboarding_state(String payment_acquirer_onboarding_state);

    /**
     * 获取 [入职支付收单机构的状态]脏标记
     */
    boolean getPayment_acquirer_onboarding_stateDirtyFlag();

    /**
     * 报表页脚
     */
    String getReport_footer();

    void setReport_footer(String report_footer);

    /**
     * 获取 [报表页脚]脏标记
     */
    boolean getReport_footerDirtyFlag();

    /**
     * 选择付款方式
     */
    String getPayment_onboarding_payment_method();

    void setPayment_onboarding_payment_method(String payment_onboarding_payment_method);

    /**
     * 获取 [选择付款方式]脏标记
     */
    boolean getPayment_onboarding_payment_methodDirtyFlag();

    /**
     * 最后更新时间
     */
    Timestamp getWrite_date();

    void setWrite_date(Timestamp write_date);

    /**
     * 获取 [最后更新时间]脏标记
     */
    boolean getWrite_dateDirtyFlag();

    /**
     * 批准等级
     */
    String getPo_double_validation();

    void setPo_double_validation(String po_double_validation);

    /**
     * 获取 [批准等级]脏标记
     */
    boolean getPo_double_validationDirtyFlag();

    /**
     * 采购提前时间
     */
    Double getPo_lead();

    void setPo_lead(Double po_lead);

    /**
     * 获取 [采购提前时间]脏标记
     */
    boolean getPo_leadDirtyFlag();

    /**
     * 有待被确认的样品报价单步骤的状态
     */
    String getSale_onboarding_sample_quotation_state();

    void setSale_onboarding_sample_quotation_state(String sale_onboarding_sample_quotation_state);

    /**
     * 获取 [有待被确认的样品报价单步骤的状态]脏标记
     */
    boolean getSale_onboarding_sample_quotation_stateDirtyFlag();

    /**
     * 有待被确认的订单步骤的状态
     */
    String getSale_onboarding_order_confirmation_state();

    void setSale_onboarding_order_confirmation_state(String sale_onboarding_order_confirmation_state);

    /**
     * 获取 [有待被确认的订单步骤的状态]脏标记
     */
    boolean getSale_onboarding_order_confirmation_stateDirtyFlag();

    /**
     * 文档模板
     */
    Integer getExternal_report_layout_id();

    void setExternal_report_layout_id(Integer external_report_layout_id);

    /**
     * 获取 [文档模板]脏标记
     */
    boolean getExternal_report_layout_idDirtyFlag();

    /**
     * 请选择付款方式
     */
    String getSale_onboarding_payment_method();

    void setSale_onboarding_payment_method(String sale_onboarding_payment_method);

    /**
     * 获取 [请选择付款方式]脏标记
     */
    boolean getSale_onboarding_payment_methodDirtyFlag();

    /**
     * 有待被确认的样品报价单步骤的状态
     */
    String getAccount_onboarding_sample_invoice_state();

    void setAccount_onboarding_sample_invoice_state(String account_onboarding_sample_invoice_state);

    /**
     * 获取 [有待被确认的样品报价单步骤的状态]脏标记
     */
    boolean getAccount_onboarding_sample_invoice_stateDirtyFlag();

    /**
     * 公司状态
     */
    String getBase_onboarding_company_state();

    void setBase_onboarding_company_state(String base_onboarding_company_state);

    /**
     * 获取 [公司状态]脏标记
     */
    boolean getBase_onboarding_company_stateDirtyFlag();

    /**
     * 领英账号
     */
    String getSocial_linkedin();

    void setSocial_linkedin(String social_linkedin);

    /**
     * 获取 [领英账号]脏标记
     */
    boolean getSocial_linkedinDirtyFlag();

    /**
     * 制造提前期(日)
     */
    Double getManufacturing_lead();

    void setManufacturing_lead(Double manufacturing_lead);

    /**
     * 获取 [制造提前期(日)]脏标记
     */
    boolean getManufacturing_leadDirtyFlag();

    /**
     * 默认条款和条件
     */
    String getSale_note();

    void setSale_note(String sale_note);

    /**
     * 获取 [默认条款和条件]脏标记
     */
    boolean getSale_noteDirtyFlag();

    /**
     * 再次验证金额
     */
    Double getPo_double_validation_amount();

    void setPo_double_validation_amount(Double po_double_validation_amount);

    /**
     * 获取 [再次验证金额]脏标记
     */
    boolean getPo_double_validation_amountDirtyFlag();

    /**
     * 销售订单修改
     */
    String getPo_lock();

    void setPo_lock(String po_lock);

    /**
     * 获取 [销售订单修改]脏标记
     */
    boolean getPo_lockDirtyFlag();

    /**
     * Twitter账号
     */
    String getSocial_twitter();

    void setSocial_twitter(String social_twitter);

    /**
     * 获取 [Twitter账号]脏标记
     */
    boolean getSocial_twitterDirtyFlag();

    /**
     * Instagram 账号
     */
    String getSocial_instagram();

    void setSocial_instagram(String social_instagram);

    /**
     * 获取 [Instagram 账号]脏标记
     */
    boolean getSocial_instagramDirtyFlag();

    /**
     * 有待被确认的会计年度步骤的状态
     */
    String getAccount_setup_fy_data_state();

    void setAccount_setup_fy_data_state(String account_setup_fy_data_state);

    /**
     * 获取 [有待被确认的会计年度步骤的状态]脏标记
     */
    boolean getAccount_setup_fy_data_stateDirtyFlag();

    /**
     * 税率计算的舍入方法
     */
    String getTax_calculation_rounding_method();

    void setTax_calculation_rounding_method(String tax_calculation_rounding_method);

    /**
     * 获取 [税率计算的舍入方法]脏标记
     */
    boolean getTax_calculation_rounding_methodDirtyFlag();

    /**
     * 现金科目的前缀
     */
    String getCash_account_code_prefix();

    void setCash_account_code_prefix(String cash_account_code_prefix);

    /**
     * 获取 [现金科目的前缀]脏标记
     */
    boolean getCash_account_code_prefixDirtyFlag();

    /**
     * 有待被确认的报价单步骤的状态
     */
    String getAccount_onboarding_sale_tax_state();

    void setAccount_onboarding_sale_tax_state(String account_onboarding_sale_tax_state);

    /**
     * 获取 [有待被确认的报价单步骤的状态]脏标记
     */
    boolean getAccount_onboarding_sale_tax_stateDirtyFlag();

    /**
     * 销售安全天数
     */
    Double getSecurity_lead();

    void setSecurity_lead(Double security_lead);

    /**
     * 获取 [销售安全天数]脏标记
     */
    boolean getSecurity_leadDirtyFlag();

    /**
     * 招聘网站主题一步完成
     */
    String getWebsite_theme_onboarding_done();

    void setWebsite_theme_onboarding_done(String website_theme_onboarding_done);

    /**
     * 获取 [招聘网站主题一步完成]脏标记
     */
    boolean getWebsite_theme_onboarding_doneDirtyFlag();

    /**
     * 通过默认值打印
     */
    String getInvoice_is_print();

    void setInvoice_is_print(String invoice_is_print);

    /**
     * 获取 [通过默认值打印]脏标记
     */
    boolean getInvoice_is_printDirtyFlag();

    /**
     * 公司注册
     */
    String getCompany_registry();

    void setCompany_registry(String company_registry);

    /**
     * 获取 [公司注册]脏标记
     */
    boolean getCompany_registryDirtyFlag();

    /**
     * 最后修改日
     */
    Timestamp get__last_update();

    void set__last_update(Timestamp __last_update);

    /**
     * 获取 [最后修改日]脏标记
     */
    boolean get__last_updateDirtyFlag();

    /**
     * 网页徽标
     */
    byte[] getLogo_web();

    void setLogo_web(byte[] logo_web);

    /**
     * 获取 [网页徽标]脏标记
     */
    boolean getLogo_webDirtyFlag();

    /**
     * 锁定日期
     */
    Timestamp getFiscalyear_lock_date();

    void setFiscalyear_lock_date(Timestamp fiscalyear_lock_date);

    /**
     * 获取 [锁定日期]脏标记
     */
    boolean getFiscalyear_lock_dateDirtyFlag();

    /**
     * 默认以信件发送
     */
    String getInvoice_is_snailmail();

    void setInvoice_is_snailmail(String invoice_is_snailmail);

    /**
     * 获取 [默认以信件发送]脏标记
     */
    boolean getInvoice_is_snailmailDirtyFlag();

    /**
     * 网站销售状态入职付款收单机构步骤
     */
    String getWebsite_sale_onboarding_payment_acquirer_state();

    void setWebsite_sale_onboarding_payment_acquirer_state(String website_sale_onboarding_payment_acquirer_state);

    /**
     * 获取 [网站销售状态入职付款收单机构步骤]脏标记
     */
    boolean getWebsite_sale_onboarding_payment_acquirer_stateDirtyFlag();

    /**
     * 脸书账号
     */
    String getSocial_facebook();

    void setSocial_facebook(String social_facebook);

    /**
     * 获取 [脸书账号]脏标记
     */
    boolean getSocial_facebookDirtyFlag();

    /**
     * 在线签名
     */
    String getPortal_confirmation_sign();

    void setPortal_confirmation_sign(String portal_confirmation_sign);

    /**
     * 获取 [在线签名]脏标记
     */
    boolean getPortal_confirmation_signDirtyFlag();

    /**
     * 纸张格式
     */
    Integer getPaperformat_id();

    void setPaperformat_id(Integer paperformat_id);

    /**
     * 获取 [纸张格式]脏标记
     */
    boolean getPaperformat_idDirtyFlag();

    /**
     * 会计年度的最后一个月
     */
    String getFiscalyear_last_month();

    void setFiscalyear_last_month(String fiscalyear_last_month);

    /**
     * 获取 [会计年度的最后一个月]脏标记
     */
    boolean getFiscalyear_last_monthDirtyFlag();

    /**
     * 默认邮件
     */
    String getInvoice_is_email();

    void setInvoice_is_email(String invoice_is_email);

    /**
     * 获取 [默认邮件]脏标记
     */
    boolean getInvoice_is_emailDirtyFlag();

    /**
     * Youtube账号
     */
    String getSocial_youtube();

    void setSocial_youtube(String social_youtube);

    /**
     * 获取 [Youtube账号]脏标记
     */
    boolean getSocial_youtubeDirtyFlag();

    /**
     * 汇率损失科目
     */
    Integer getExpense_currency_exchange_account_id();

    void setExpense_currency_exchange_account_id(Integer expense_currency_exchange_account_id);

    /**
     * 获取 [汇率损失科目]脏标记
     */
    boolean getExpense_currency_exchange_account_idDirtyFlag();

    /**
     * 公司数据库ID
     */
    Integer getPartner_gid();

    void setPartner_gid(Integer partner_gid);

    /**
     * 获取 [公司数据库ID]脏标记
     */
    boolean getPartner_gidDirtyFlag();

    /**
     * 电话
     */
    String getPhone();

    void setPhone(String phone);

    /**
     * 获取 [电话]脏标记
     */
    boolean getPhoneDirtyFlag();

    /**
     * 公司 Logo
     */
    byte[] getLogo();

    void setLogo(byte[] logo);

    /**
     * 获取 [公司 Logo]脏标记
     */
    boolean getLogoDirtyFlag();

    /**
     * 库存计价的入库科目
     */
    String getProperty_stock_account_input_categ_id_text();

    void setProperty_stock_account_input_categ_id_text(String property_stock_account_input_categ_id_text);

    /**
     * 获取 [库存计价的入库科目]脏标记
     */
    boolean getProperty_stock_account_input_categ_id_textDirtyFlag();

    /**
     * 默认进项税
     */
    String getAccount_purchase_tax_id_text();

    void setAccount_purchase_tax_id_text(String account_purchase_tax_id_text);

    /**
     * 获取 [默认进项税]脏标记
     */
    boolean getAccount_purchase_tax_id_textDirtyFlag();

    /**
     * 公司名称
     */
    String getName();

    void setName(String name);

    /**
     * 获取 [公司名称]脏标记
     */
    boolean getNameDirtyFlag();

    /**
     * 默认国际贸易术语
     */
    String getIncoterm_id_text();

    void setIncoterm_id_text(String incoterm_id_text);

    /**
     * 获取 [默认国际贸易术语]脏标记
     */
    boolean getIncoterm_id_textDirtyFlag();

    /**
     * 期初日记账
     */
    Integer getAccount_opening_journal_id();

    void setAccount_opening_journal_id(Integer account_opening_journal_id);

    /**
     * 获取 [期初日记账]脏标记
     */
    boolean getAccount_opening_journal_idDirtyFlag();

    /**
     * 银行间转账科目
     */
    String getTransfer_account_id_text();

    void setTransfer_account_id_text(String transfer_account_id_text);

    /**
     * 获取 [银行间转账科目]脏标记
     */
    boolean getTransfer_account_id_textDirtyFlag();

    /**
     * 汇率增益科目
     */
    Integer getIncome_currency_exchange_account_id();

    void setIncome_currency_exchange_account_id(Integer income_currency_exchange_account_id);

    /**
     * 获取 [汇率增益科目]脏标记
     */
    boolean getIncome_currency_exchange_account_idDirtyFlag();

    /**
     * 币种
     */
    String getCurrency_id_text();

    void setCurrency_id_text(String currency_id_text);

    /**
     * 获取 [币种]脏标记
     */
    boolean getCurrency_id_textDirtyFlag();

    /**
     * 期初日期
     */
    Timestamp getAccount_opening_date();

    void setAccount_opening_date(Timestamp account_opening_date);

    /**
     * 获取 [期初日期]脏标记
     */
    boolean getAccount_opening_dateDirtyFlag();

    /**
     * 最后更新者
     */
    String getWrite_uid_text();

    void setWrite_uid_text(String write_uid_text);

    /**
     * 获取 [最后更新者]脏标记
     */
    boolean getWrite_uid_textDirtyFlag();

    /**
     * 表模板
     */
    String getChart_template_id_text();

    void setChart_template_id_text(String chart_template_id_text);

    /**
     * 获取 [表模板]脏标记
     */
    boolean getChart_template_id_textDirtyFlag();

    /**
     * 默认销售税
     */
    String getAccount_sale_tax_id_text();

    void setAccount_sale_tax_id_text(String account_sale_tax_id_text);

    /**
     * 获取 [默认销售税]脏标记
     */
    boolean getAccount_sale_tax_id_textDirtyFlag();

    /**
     * 期初日记账分录
     */
    String getAccount_opening_move_id_text();

    void setAccount_opening_move_id_text(String account_opening_move_id_text);

    /**
     * 获取 [期初日记账分录]脏标记
     */
    boolean getAccount_opening_move_id_textDirtyFlag();

    /**
     * EMail
     */
    String getEmail();

    void setEmail(String email);

    /**
     * 获取 [EMail]脏标记
     */
    boolean getEmailDirtyFlag();

    /**
     * 创建人
     */
    String getCreate_uid_text();

    void setCreate_uid_text(String create_uid_text);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uid_textDirtyFlag();

    /**
     * 库存计价的出货科目
     */
    String getProperty_stock_account_output_categ_id_text();

    void setProperty_stock_account_output_categ_id_text(String property_stock_account_output_categ_id_text);

    /**
     * 获取 [库存计价的出货科目]脏标记
     */
    boolean getProperty_stock_account_output_categ_id_textDirtyFlag();

    /**
     * 库存计价的科目模板
     */
    String getProperty_stock_valuation_account_id_text();

    void setProperty_stock_valuation_account_id_text(String property_stock_valuation_account_id_text);

    /**
     * 获取 [库存计价的科目模板]脏标记
     */
    boolean getProperty_stock_valuation_account_id_textDirtyFlag();

    /**
     * 上级公司
     */
    String getParent_id_text();

    void setParent_id_text(String parent_id_text);

    /**
     * 获取 [上级公司]脏标记
     */
    boolean getParent_id_textDirtyFlag();

    /**
     * 现金收付制日记账
     */
    String getTax_cash_basis_journal_id_text();

    void setTax_cash_basis_journal_id_text(String tax_cash_basis_journal_id_text);

    /**
     * 获取 [现金收付制日记账]脏标记
     */
    boolean getTax_cash_basis_journal_id_textDirtyFlag();

    /**
     * 内部中转位置
     */
    String getInternal_transit_location_id_text();

    void setInternal_transit_location_id_text(String internal_transit_location_id_text);

    /**
     * 获取 [内部中转位置]脏标记
     */
    boolean getInternal_transit_location_id_textDirtyFlag();

    /**
     * 网站
     */
    String getWebsite();

    void setWebsite(String website);

    /**
     * 获取 [网站]脏标记
     */
    boolean getWebsiteDirtyFlag();

    /**
     * 税号
     */
    String getVat();

    void setVat(String vat);

    /**
     * 获取 [税号]脏标记
     */
    boolean getVatDirtyFlag();

    /**
     * 默认工作时间
     */
    String getResource_calendar_id_text();

    void setResource_calendar_id_text(String resource_calendar_id_text);

    /**
     * 获取 [默认工作时间]脏标记
     */
    boolean getResource_calendar_id_textDirtyFlag();

    /**
     * 汇兑损益
     */
    String getCurrency_exchange_journal_id_text();

    void setCurrency_exchange_journal_id_text(String currency_exchange_journal_id_text);

    /**
     * 获取 [汇兑损益]脏标记
     */
    boolean getCurrency_exchange_journal_id_textDirtyFlag();

    /**
     * 创建人
     */
    Integer getCreate_uid();

    void setCreate_uid(Integer create_uid);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uidDirtyFlag();

    /**
     * 上级公司
     */
    Integer getParent_id();

    void setParent_id(Integer parent_id);

    /**
     * 获取 [上级公司]脏标记
     */
    boolean getParent_idDirtyFlag();

    /**
     * 币种
     */
    Integer getCurrency_id();

    void setCurrency_id(Integer currency_id);

    /**
     * 获取 [币种]脏标记
     */
    boolean getCurrency_idDirtyFlag();

    /**
     * 最后更新者
     */
    Integer getWrite_uid();

    void setWrite_uid(Integer write_uid);

    /**
     * 获取 [最后更新者]脏标记
     */
    boolean getWrite_uidDirtyFlag();

    /**
     * 库存计价的出货科目
     */
    Integer getProperty_stock_account_output_categ_id();

    void setProperty_stock_account_output_categ_id(Integer property_stock_account_output_categ_id);

    /**
     * 获取 [库存计价的出货科目]脏标记
     */
    boolean getProperty_stock_account_output_categ_idDirtyFlag();

    /**
     * 库存计价的科目模板
     */
    Integer getProperty_stock_valuation_account_id();

    void setProperty_stock_valuation_account_id(Integer property_stock_valuation_account_id);

    /**
     * 获取 [库存计价的科目模板]脏标记
     */
    boolean getProperty_stock_valuation_account_idDirtyFlag();

    /**
     * 期初日记账分录
     */
    Integer getAccount_opening_move_id();

    void setAccount_opening_move_id(Integer account_opening_move_id);

    /**
     * 获取 [期初日记账分录]脏标记
     */
    boolean getAccount_opening_move_idDirtyFlag();

    /**
     * 内部中转位置
     */
    Integer getInternal_transit_location_id();

    void setInternal_transit_location_id(Integer internal_transit_location_id);

    /**
     * 获取 [内部中转位置]脏标记
     */
    boolean getInternal_transit_location_idDirtyFlag();

    /**
     * 默认进项税
     */
    Integer getAccount_purchase_tax_id();

    void setAccount_purchase_tax_id(Integer account_purchase_tax_id);

    /**
     * 获取 [默认进项税]脏标记
     */
    boolean getAccount_purchase_tax_idDirtyFlag();

    /**
     * 表模板
     */
    Integer getChart_template_id();

    void setChart_template_id(Integer chart_template_id);

    /**
     * 获取 [表模板]脏标记
     */
    boolean getChart_template_idDirtyFlag();

    /**
     * 默认销售税
     */
    Integer getAccount_sale_tax_id();

    void setAccount_sale_tax_id(Integer account_sale_tax_id);

    /**
     * 获取 [默认销售税]脏标记
     */
    boolean getAccount_sale_tax_idDirtyFlag();

    /**
     * 现金收付制日记账
     */
    Integer getTax_cash_basis_journal_id();

    void setTax_cash_basis_journal_id(Integer tax_cash_basis_journal_id);

    /**
     * 获取 [现金收付制日记账]脏标记
     */
    boolean getTax_cash_basis_journal_idDirtyFlag();

    /**
     * 库存计价的入库科目
     */
    Integer getProperty_stock_account_input_categ_id();

    void setProperty_stock_account_input_categ_id(Integer property_stock_account_input_categ_id);

    /**
     * 获取 [库存计价的入库科目]脏标记
     */
    boolean getProperty_stock_account_input_categ_idDirtyFlag();

    /**
     * 业务伙伴
     */
    Integer getPartner_id();

    void setPartner_id(Integer partner_id);

    /**
     * 获取 [业务伙伴]脏标记
     */
    boolean getPartner_idDirtyFlag();

    /**
     * 默认国际贸易术语
     */
    Integer getIncoterm_id();

    void setIncoterm_id(Integer incoterm_id);

    /**
     * 获取 [默认国际贸易术语]脏标记
     */
    boolean getIncoterm_idDirtyFlag();

    /**
     * 默认工作时间
     */
    Integer getResource_calendar_id();

    void setResource_calendar_id(Integer resource_calendar_id);

    /**
     * 获取 [默认工作时间]脏标记
     */
    boolean getResource_calendar_idDirtyFlag();

    /**
     * 银行间转账科目
     */
    Integer getTransfer_account_id();

    void setTransfer_account_id(Integer transfer_account_id);

    /**
     * 获取 [银行间转账科目]脏标记
     */
    boolean getTransfer_account_idDirtyFlag();

    /**
     * 汇兑损益
     */
    Integer getCurrency_exchange_journal_id();

    void setCurrency_exchange_journal_id(Integer currency_exchange_journal_id);

    /**
     * 获取 [汇兑损益]脏标记
     */
    boolean getCurrency_exchange_journal_idDirtyFlag();

}
