package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Base_partner_merge_automatic_wizard;
import cn.ibizlab.odoo.core.odoo_base.filter.Base_partner_merge_automatic_wizardSearchContext;

/**
 * 实体 [合并业务伙伴向导] 存储对象
 */
public interface Base_partner_merge_automatic_wizardRepository extends Repository<Base_partner_merge_automatic_wizard> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Base_partner_merge_automatic_wizard> searchDefault(Base_partner_merge_automatic_wizardSearchContext context);

    Base_partner_merge_automatic_wizard convert2PO(cn.ibizlab.odoo.core.odoo_base.domain.Base_partner_merge_automatic_wizard domain , Base_partner_merge_automatic_wizard po) ;

    cn.ibizlab.odoo.core.odoo_base.domain.Base_partner_merge_automatic_wizard convert2Domain( Base_partner_merge_automatic_wizard po ,cn.ibizlab.odoo.core.odoo_base.domain.Base_partner_merge_automatic_wizard domain) ;

}
