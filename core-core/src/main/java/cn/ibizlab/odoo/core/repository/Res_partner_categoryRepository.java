package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Res_partner_category;
import cn.ibizlab.odoo.core.odoo_base.filter.Res_partner_categorySearchContext;

/**
 * 实体 [业务伙伴标签] 存储对象
 */
public interface Res_partner_categoryRepository extends Repository<Res_partner_category> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Res_partner_category> searchDefault(Res_partner_categorySearchContext context);

    Res_partner_category convert2PO(cn.ibizlab.odoo.core.odoo_base.domain.Res_partner_category domain , Res_partner_category po) ;

    cn.ibizlab.odoo.core.odoo_base.domain.Res_partner_category convert2Domain( Res_partner_category po ,cn.ibizlab.odoo.core.odoo_base.domain.Res_partner_category domain) ;

}
