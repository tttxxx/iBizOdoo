package cn.ibizlab.odoo.core.repository.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import cn.ibizlab.odoo.core.repository.client.po.hr_attendance;

/**
 * 实体[hr_attendance] 服务对象接口
 */
public interface hr_attendanceRepository{


    public hr_attendance createPO() ;
        public List<hr_attendance> search();

        public void update(hr_attendance hr_attendance);

        public void createBatch(hr_attendance hr_attendance);

        public void create(hr_attendance hr_attendance);

        public void remove(String id);

        public void updateBatch(hr_attendance hr_attendance);

        public void removeBatch(String id);

        public void get(String id);


}
