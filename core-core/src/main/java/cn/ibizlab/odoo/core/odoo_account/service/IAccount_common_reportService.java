package cn.ibizlab.odoo.core.odoo_account.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_account.domain.Account_common_report;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_common_reportSearchContext;


/**
 * 实体[Account_common_report] 服务对象接口
 */
public interface IAccount_common_reportService{

    Account_common_report get(Integer key) ;
    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    boolean create(Account_common_report et) ;
    void createBatch(List<Account_common_report> list) ;
    boolean update(Account_common_report et) ;
    void updateBatch(List<Account_common_report> list) ;
    Page<Account_common_report> searchDefault(Account_common_reportSearchContext context) ;

}



