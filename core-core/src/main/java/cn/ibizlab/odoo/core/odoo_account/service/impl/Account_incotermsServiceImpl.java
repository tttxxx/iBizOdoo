package cn.ibizlab.odoo.core.odoo_account.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_account.domain.Account_incoterms;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_incotermsSearchContext;
import cn.ibizlab.odoo.core.odoo_account.service.IAccount_incotermsService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;



import cn.ibizlab.odoo.core.odoo_account.client.account_incotermsOdooClient;
import cn.ibizlab.odoo.core.odoo_account.clientmodel.account_incotermsClientModel;

/**
 * 实体[贸易条款] 服务对象接口实现
 */
@Slf4j
@Service
public class Account_incotermsServiceImpl implements IAccount_incotermsService {

    @Autowired
    account_incotermsOdooClient account_incotermsOdooClient;


    @Override
    public Account_incoterms get(Integer id) {
        account_incotermsClientModel clientModel = new account_incotermsClientModel();
        clientModel.setId(id);
		account_incotermsOdooClient.get(clientModel);
        Account_incoterms et = convert2Domain(clientModel,null);
        if(et==null){
            et=new Account_incoterms();
            et.setId(id);
        }
        else{
        }

        return  et;
    }

    @Override
    public boolean create(Account_incoterms et) {
        account_incotermsClientModel clientModel = convert2Model(et,null);
		account_incotermsOdooClient.create(clientModel);
        Account_incoterms rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Account_incoterms> list){
    }

    @Override
    public boolean remove(Integer id) {
        account_incotermsClientModel clientModel = new account_incotermsClientModel();
        clientModel.setId(id);
		account_incotermsOdooClient.remove(clientModel);
        return true; 
    }

    public void removeBatch(Collection<Integer> idList){
    }

    @Override
    public boolean update(Account_incoterms et) {
        account_incotermsClientModel clientModel = convert2Model(et,null);
		account_incotermsOdooClient.update(clientModel);
        Account_incoterms rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }
    
    public void updateBatch(List<Account_incoterms> list){
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Account_incoterms> searchDefault(Account_incotermsSearchContext context) {
        List<Account_incoterms> list = new ArrayList<Account_incoterms>();
        Page<account_incotermsClientModel> clientModelList = account_incotermsOdooClient.search(context);
        for(account_incotermsClientModel clientModel : clientModelList){
            list.add( this.convert2Domain(clientModel,null));
        }
        return new PageImpl<Account_incoterms>(list, context.getPageable(), clientModelList.getTotalElements());
    }

    public account_incotermsClientModel convert2Model(Account_incoterms domain , account_incotermsClientModel model) {
        if(domain == null)
            return model ;
        if(model==null){
            model = new account_incotermsClientModel();

        }
        if((Boolean) domain.getExtensionparams().get("dirtyflagenable")){
            if((Boolean) domain.getExtensionparams().get("write_datedirtyflag"))
                model.setWrite_date(domain.getWriteDate());
            if((Boolean) domain.getExtensionparams().get("iddirtyflag"))
                model.setId(domain.getId());
            if((Boolean) domain.getExtensionparams().get("namedirtyflag"))
                model.setName(domain.getName());
            if((Boolean) domain.getExtensionparams().get("__last_updatedirtyflag"))
                model.set__last_update(domain.getLastUpdate());
            if((Boolean) domain.getExtensionparams().get("activedirtyflag"))
                model.setActive(domain.getActive());
            if((Boolean) domain.getExtensionparams().get("create_datedirtyflag"))
                model.setCreate_date(domain.getCreateDate());
            if((Boolean) domain.getExtensionparams().get("codedirtyflag"))
                model.setCode(domain.getCode());
            if((Boolean) domain.getExtensionparams().get("display_namedirtyflag"))
                model.setDisplay_name(domain.getDisplayName());
            if((Boolean) domain.getExtensionparams().get("write_uid_textdirtyflag"))
                model.setWrite_uid_text(domain.getWriteUidText());
            if((Boolean) domain.getExtensionparams().get("create_uid_textdirtyflag"))
                model.setCreate_uid_text(domain.getCreateUidText());
            if((Boolean) domain.getExtensionparams().get("create_uiddirtyflag"))
                model.setCreate_uid(domain.getCreateUid());
            if((Boolean) domain.getExtensionparams().get("write_uiddirtyflag"))
                model.setWrite_uid(domain.getWriteUid());
            domain.getFocusNull().clear();
        }
        return model ;
    }

    public Account_incoterms convert2Domain( account_incotermsClientModel model ,Account_incoterms domain) {
        if(model == null)
            return domain ;
        if(domain == null){
            domain = new Account_incoterms();
        }

        if(model.getWrite_dateDirtyFlag())
            domain.setWriteDate(model.getWrite_date());
        if(model.getIdDirtyFlag())
            domain.setId(model.getId());
        if(model.getNameDirtyFlag())
            domain.setName(model.getName());
        if(model.get__last_updateDirtyFlag())
            domain.setLastUpdate(model.get__last_update());
        if(model.getActiveDirtyFlag())
            domain.setActive(model.getActive());
        if(model.getCreate_dateDirtyFlag())
            domain.setCreateDate(model.getCreate_date());
        if(model.getCodeDirtyFlag())
            domain.setCode(model.getCode());
        if(model.getDisplay_nameDirtyFlag())
            domain.setDisplayName(model.getDisplay_name());
        if(model.getWrite_uid_textDirtyFlag())
            domain.setWriteUidText(model.getWrite_uid_text());
        if(model.getCreate_uid_textDirtyFlag())
            domain.setCreateUidText(model.getCreate_uid_text());
        if(model.getCreate_uidDirtyFlag())
            domain.setCreateUid(model.getCreate_uid());
        if(model.getWrite_uidDirtyFlag())
            domain.setWriteUid(model.getWrite_uid());
        return domain ;
    }

}

    



