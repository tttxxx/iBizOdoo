package cn.ibizlab.odoo.core.repository.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import cn.ibizlab.odoo.core.repository.client.po.mro_pm_rule;

/**
 * 实体[mro_pm_rule] 服务对象接口
 */
public interface mro_pm_ruleRepository{


    public mro_pm_rule createPO() ;
        public void create(mro_pm_rule mro_pm_rule);

        public List<mro_pm_rule> search();

        public void get(String id);

        public void removeBatch(String id);

        public void createBatch(mro_pm_rule mro_pm_rule);

        public void remove(String id);

        public void update(mro_pm_rule mro_pm_rule);

        public void updateBatch(mro_pm_rule mro_pm_rule);


}
