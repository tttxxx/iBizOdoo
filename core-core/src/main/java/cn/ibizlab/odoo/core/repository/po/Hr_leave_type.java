package cn.ibizlab.odoo.core.repository.po ;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.math.BigInteger;

import cn.ibizlab.odoo.core.odoo_hr.filter.Hr_leave_typeSearchContext;

/**
 * 实体 [休假类型] 存储模型
 */
public interface Hr_leave_type{

    /**
     * 有效
     */
    String getValid();

    void setValid(String valid);

    /**
     * 获取 [有效]脏标记
     */
    boolean getValidDirtyFlag();

    /**
     * 最后更新时间
     */
    Timestamp getWrite_date();

    void setWrite_date(Timestamp write_date);

    /**
     * 获取 [最后更新时间]脏标记
     */
    boolean getWrite_dateDirtyFlag();

    /**
     * 虚拟剩余休假
     */
    Double getVirtual_remaining_leaves();

    void setVirtual_remaining_leaves(Double virtual_remaining_leaves);

    /**
     * 获取 [虚拟剩余休假]脏标记
     */
    boolean getVirtual_remaining_leavesDirtyFlag();

    /**
     * 休假早已使用
     */
    Double getLeaves_taken();

    void setLeaves_taken(Double leaves_taken);

    /**
     * 获取 [休假早已使用]脏标记
     */
    boolean getLeaves_takenDirtyFlag();

    /**
     * 分配天数
     */
    Double getGroup_days_allocation();

    void setGroup_days_allocation(Double group_days_allocation);

    /**
     * 获取 [分配天数]脏标记
     */
    boolean getGroup_days_allocationDirtyFlag();

    /**
     * 应用双重验证
     */
    String getDouble_validation();

    void setDouble_validation(String double_validation);

    /**
     * 获取 [应用双重验证]脏标记
     */
    boolean getDouble_validationDirtyFlag();

    /**
     * 模式
     */
    String getAllocation_type();

    void setAllocation_type(String allocation_type);

    /**
     * 获取 [模式]脏标记
     */
    boolean getAllocation_typeDirtyFlag();

    /**
     * 显示名称
     */
    String getDisplay_name();

    void setDisplay_name(String display_name);

    /**
     * 获取 [显示名称]脏标记
     */
    boolean getDisplay_nameDirtyFlag();

    /**
     * 序号
     */
    Integer getSequence();

    void setSequence(Integer sequence);

    /**
     * 获取 [序号]脏标记
     */
    boolean getSequenceDirtyFlag();

    /**
     * 没付款
     */
    String getUnpaid();

    void setUnpaid(String unpaid);

    /**
     * 获取 [没付款]脏标记
     */
    boolean getUnpaidDirtyFlag();

    /**
     * ID
     */
    Integer getId();

    void setId(Integer id);

    /**
     * 获取 [ID]脏标记
     */
    boolean getIdDirtyFlag();

    /**
     * 最大允许
     */
    Double getMax_leaves();

    void setMax_leaves(Double max_leaves);

    /**
     * 获取 [最大允许]脏标记
     */
    boolean getMax_leavesDirtyFlag();

    /**
     * 结束日期
     */
    Timestamp getValidity_stop();

    void setValidity_stop(Timestamp validity_stop);

    /**
     * 获取 [结束日期]脏标记
     */
    boolean getValidity_stopDirtyFlag();

    /**
     * 验证人
     */
    String getValidation_type();

    void setValidation_type(String validation_type);

    /**
     * 获取 [验证人]脏标记
     */
    boolean getValidation_typeDirtyFlag();

    /**
     * 请假
     */
    String getTime_type();

    void setTime_type(String time_type);

    /**
     * 获取 [请假]脏标记
     */
    boolean getTime_typeDirtyFlag();

    /**
     * 最后修改日
     */
    Timestamp get__last_update();

    void set__last_update(Timestamp __last_update);

    /**
     * 获取 [最后修改日]脏标记
     */
    boolean get__last_updateDirtyFlag();

    /**
     * 休假
     */
    String getRequest_unit();

    void setRequest_unit(String request_unit);

    /**
     * 获取 [休假]脏标记
     */
    boolean getRequest_unitDirtyFlag();

    /**
     * 集团假期
     */
    Double getGroup_days_leave();

    void setGroup_days_leave(Double group_days_leave);

    /**
     * 获取 [集团假期]脏标记
     */
    boolean getGroup_days_leaveDirtyFlag();

    /**
     * 报表中的颜色
     */
    String getColor_name();

    void setColor_name(String color_name);

    /**
     * 获取 [报表中的颜色]脏标记
     */
    boolean getColor_nameDirtyFlag();

    /**
     * 剩余休假
     */
    Double getRemaining_leaves();

    void setRemaining_leaves(Double remaining_leaves);

    /**
     * 获取 [剩余休假]脏标记
     */
    boolean getRemaining_leavesDirtyFlag();

    /**
     * 休假类型
     */
    String getName();

    void setName(String name);

    /**
     * 获取 [休假类型]脏标记
     */
    boolean getNameDirtyFlag();

    /**
     * 开始日期
     */
    Timestamp getValidity_start();

    void setValidity_start(Timestamp validity_start);

    /**
     * 获取 [开始日期]脏标记
     */
    boolean getValidity_startDirtyFlag();

    /**
     * 创建时间
     */
    Timestamp getCreate_date();

    void setCreate_date(Timestamp create_date);

    /**
     * 获取 [创建时间]脏标记
     */
    boolean getCreate_dateDirtyFlag();

    /**
     * 有效
     */
    String getActive();

    void setActive(String active);

    /**
     * 获取 [有效]脏标记
     */
    boolean getActiveDirtyFlag();

    /**
     * 会议类型
     */
    String getCateg_id_text();

    void setCateg_id_text(String categ_id_text);

    /**
     * 获取 [会议类型]脏标记
     */
    boolean getCateg_id_textDirtyFlag();

    /**
     * 最后更新人
     */
    String getWrite_uid_text();

    void setWrite_uid_text(String write_uid_text);

    /**
     * 获取 [最后更新人]脏标记
     */
    boolean getWrite_uid_textDirtyFlag();

    /**
     * 创建人
     */
    String getCreate_uid_text();

    void setCreate_uid_text(String create_uid_text);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uid_textDirtyFlag();

    /**
     * 公司
     */
    String getCompany_id_text();

    void setCompany_id_text(String company_id_text);

    /**
     * 获取 [公司]脏标记
     */
    boolean getCompany_id_textDirtyFlag();

    /**
     * 会议类型
     */
    Integer getCateg_id();

    void setCateg_id(Integer categ_id);

    /**
     * 获取 [会议类型]脏标记
     */
    boolean getCateg_idDirtyFlag();

    /**
     * 公司
     */
    Integer getCompany_id();

    void setCompany_id(Integer company_id);

    /**
     * 获取 [公司]脏标记
     */
    boolean getCompany_idDirtyFlag();

    /**
     * 创建人
     */
    Integer getCreate_uid();

    void setCreate_uid(Integer create_uid);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uidDirtyFlag();

    /**
     * 最后更新人
     */
    Integer getWrite_uid();

    void setWrite_uid(Integer write_uid);

    /**
     * 获取 [最后更新人]脏标记
     */
    boolean getWrite_uidDirtyFlag();

}
