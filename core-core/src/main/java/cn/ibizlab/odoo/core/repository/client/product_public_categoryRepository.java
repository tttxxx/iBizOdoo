package cn.ibizlab.odoo.core.repository.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import cn.ibizlab.odoo.core.repository.client.po.product_public_category;

/**
 * 实体[product_public_category] 服务对象接口
 */
public interface product_public_categoryRepository{


    public product_public_category createPO() ;
        public void get(String id);

        public void create(product_public_category product_public_category);

        public void removeBatch(String id);

        public void createBatch(product_public_category product_public_category);

        public void update(product_public_category product_public_category);

        public void updateBatch(product_public_category product_public_category);

        public List<product_public_category> search();

        public void remove(String id);


}
