package cn.ibizlab.odoo.core.repository.po ;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.math.BigInteger;

import cn.ibizlab.odoo.core.odoo_mail.filter.Mail_aliasSearchContext;

/**
 * 实体 [EMail别名] 存储模型
 */
public interface Mail_alias{

    /**
     * ID
     */
    Integer getId();

    void setId(Integer id);

    /**
     * 获取 [ID]脏标记
     */
    boolean getIdDirtyFlag();

    /**
     * 创建时间
     */
    Timestamp getCreate_date();

    void setCreate_date(Timestamp create_date);

    /**
     * 获取 [创建时间]脏标记
     */
    boolean getCreate_dateDirtyFlag();

    /**
     * 上级记录ID
     */
    Integer getAlias_parent_thread_id();

    void setAlias_parent_thread_id(Integer alias_parent_thread_id);

    /**
     * 获取 [上级记录ID]脏标记
     */
    boolean getAlias_parent_thread_idDirtyFlag();

    /**
     * 网域别名
     */
    String getAlias_domain();

    void setAlias_domain(String alias_domain);

    /**
     * 获取 [网域别名]脏标记
     */
    boolean getAlias_domainDirtyFlag();

    /**
     * 模型别名
     */
    Integer getAlias_model_id();

    void setAlias_model_id(Integer alias_model_id);

    /**
     * 获取 [模型别名]脏标记
     */
    boolean getAlias_model_idDirtyFlag();

    /**
     * 默认值
     */
    String getAlias_defaults();

    void setAlias_defaults(String alias_defaults);

    /**
     * 获取 [默认值]脏标记
     */
    boolean getAlias_defaultsDirtyFlag();

    /**
     * 显示名称
     */
    String getDisplay_name();

    void setDisplay_name(String display_name);

    /**
     * 获取 [显示名称]脏标记
     */
    boolean getDisplay_nameDirtyFlag();

    /**
     * 别名
     */
    String getAlias_name();

    void setAlias_name(String alias_name);

    /**
     * 获取 [别名]脏标记
     */
    boolean getAlias_nameDirtyFlag();

    /**
     * 记录线索ID
     */
    Integer getAlias_force_thread_id();

    void setAlias_force_thread_id(Integer alias_force_thread_id);

    /**
     * 获取 [记录线索ID]脏标记
     */
    boolean getAlias_force_thread_idDirtyFlag();

    /**
     * 最后修改日
     */
    Timestamp get__last_update();

    void set__last_update(Timestamp __last_update);

    /**
     * 获取 [最后修改日]脏标记
     */
    boolean get__last_updateDirtyFlag();

    /**
     * 上级模型
     */
    Integer getAlias_parent_model_id();

    void setAlias_parent_model_id(Integer alias_parent_model_id);

    /**
     * 获取 [上级模型]脏标记
     */
    boolean getAlias_parent_model_idDirtyFlag();

    /**
     * 安全联系人别名
     */
    String getAlias_contact();

    void setAlias_contact(String alias_contact);

    /**
     * 获取 [安全联系人别名]脏标记
     */
    boolean getAlias_contactDirtyFlag();

    /**
     * 最后更新时间
     */
    Timestamp getWrite_date();

    void setWrite_date(Timestamp write_date);

    /**
     * 获取 [最后更新时间]脏标记
     */
    boolean getWrite_dateDirtyFlag();

    /**
     * 创建人
     */
    String getCreate_uid_text();

    void setCreate_uid_text(String create_uid_text);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uid_textDirtyFlag();

    /**
     * 最后更新者
     */
    String getWrite_uid_text();

    void setWrite_uid_text(String write_uid_text);

    /**
     * 获取 [最后更新者]脏标记
     */
    boolean getWrite_uid_textDirtyFlag();

    /**
     * 所有者
     */
    String getAlias_user_id_text();

    void setAlias_user_id_text(String alias_user_id_text);

    /**
     * 获取 [所有者]脏标记
     */
    boolean getAlias_user_id_textDirtyFlag();

    /**
     * 所有者
     */
    Integer getAlias_user_id();

    void setAlias_user_id(Integer alias_user_id);

    /**
     * 获取 [所有者]脏标记
     */
    boolean getAlias_user_idDirtyFlag();

    /**
     * 最后更新者
     */
    Integer getWrite_uid();

    void setWrite_uid(Integer write_uid);

    /**
     * 获取 [最后更新者]脏标记
     */
    boolean getWrite_uidDirtyFlag();

    /**
     * 创建人
     */
    Integer getCreate_uid();

    void setCreate_uid(Integer create_uid);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uidDirtyFlag();

}
