package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Mail_activity_type;
import cn.ibizlab.odoo.core.odoo_mail.filter.Mail_activity_typeSearchContext;

/**
 * 实体 [活动类型] 存储对象
 */
public interface Mail_activity_typeRepository extends Repository<Mail_activity_type> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Mail_activity_type> searchDefault(Mail_activity_typeSearchContext context);

    Mail_activity_type convert2PO(cn.ibizlab.odoo.core.odoo_mail.domain.Mail_activity_type domain , Mail_activity_type po) ;

    cn.ibizlab.odoo.core.odoo_mail.domain.Mail_activity_type convert2Domain( Mail_activity_type po ,cn.ibizlab.odoo.core.odoo_mail.domain.Mail_activity_type domain) ;

}
