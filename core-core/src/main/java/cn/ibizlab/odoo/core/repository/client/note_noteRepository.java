package cn.ibizlab.odoo.core.repository.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import cn.ibizlab.odoo.core.repository.client.po.note_note;

/**
 * 实体[note_note] 服务对象接口
 */
public interface note_noteRepository{


    public note_note createPO() ;
        public void updateBatch(note_note note_note);

        public void update(note_note note_note);

        public void get(String id);

        public void createBatch(note_note note_note);

        public void remove(String id);

        public List<note_note> search();

        public void removeBatch(String id);

        public void create(note_note note_note);


}
