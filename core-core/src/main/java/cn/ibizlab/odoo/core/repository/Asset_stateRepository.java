package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Asset_state;
import cn.ibizlab.odoo.core.odoo_asset.filter.Asset_stateSearchContext;

/**
 * 实体 [State of Asset] 存储对象
 */
public interface Asset_stateRepository extends Repository<Asset_state> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Asset_state> searchDefault(Asset_stateSearchContext context);

    Asset_state convert2PO(cn.ibizlab.odoo.core.odoo_asset.domain.Asset_state domain , Asset_state po) ;

    cn.ibizlab.odoo.core.odoo_asset.domain.Asset_state convert2Domain( Asset_state po ,cn.ibizlab.odoo.core.odoo_asset.domain.Asset_state domain) ;

}
