package cn.ibizlab.odoo.core.repository.client.po;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigDecimal;

/**
 * [mrp_bom_line] 对象
 */
public interface mrp_bom_line {

    public String getAttribute_value_ids();

    public void setAttribute_value_ids(String attribute_value_ids);

    public Integer getBom_id();

    public void setBom_id(Integer bom_id);

    public Integer getChild_bom_id();

    public void setChild_bom_id(Integer child_bom_id);

    public String getChild_line_ids();

    public void setChild_line_ids(String child_line_ids);

    public Timestamp getCreate_date();

    public void setCreate_date(Timestamp create_date);

    public Integer getCreate_uid();

    public void setCreate_uid(Integer create_uid);

    public String getCreate_uid_text();

    public void setCreate_uid_text(String create_uid_text);

    public String getDisplay_name();

    public void setDisplay_name(String display_name);

    public String getHas_attachments();

    public void setHas_attachments(String has_attachments);

    public Integer getId();

    public void setId(Integer id);

    public Integer getOperation_id();

    public void setOperation_id(Integer operation_id);

    public String getOperation_id_text();

    public void setOperation_id_text(String operation_id_text);

    public Integer getParent_product_tmpl_id();

    public void setParent_product_tmpl_id(Integer parent_product_tmpl_id);

    public String getParent_product_tmpl_id_text();

    public void setParent_product_tmpl_id_text(String parent_product_tmpl_id_text);

    public Integer getProduct_id();

    public void setProduct_id(Integer product_id);

    public String getProduct_id_text();

    public void setProduct_id_text(String product_id_text);

    public Double getProduct_qty();

    public void setProduct_qty(Double product_qty);

    public Integer getProduct_tmpl_id();

    public void setProduct_tmpl_id(Integer product_tmpl_id);

    public String getProduct_tmpl_id_text();

    public void setProduct_tmpl_id_text(String product_tmpl_id_text);

    public Integer getProduct_uom_id();

    public void setProduct_uom_id(Integer product_uom_id);

    public String getProduct_uom_id_text();

    public void setProduct_uom_id_text(String product_uom_id_text);

    public Integer getRouting_id();

    public void setRouting_id(Integer routing_id);

    public String getRouting_id_text();

    public void setRouting_id_text(String routing_id_text);

    public Integer getSequence();

    public void setSequence(Integer sequence);

    public String getValid_product_attribute_value_ids();

    public void setValid_product_attribute_value_ids(String valid_product_attribute_value_ids);

    public Timestamp getWrite_date();

    public void setWrite_date(Timestamp write_date);

    public Integer getWrite_uid();

    public void setWrite_uid(Integer write_uid);

    public String getWrite_uid_text();

    public void setWrite_uid_text(String write_uid_text);

    public Timestamp get__last_update();

    public void set__last_update(Timestamp __last_update);

}
