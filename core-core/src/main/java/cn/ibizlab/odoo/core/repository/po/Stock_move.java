package cn.ibizlab.odoo.core.repository.po ;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.math.BigInteger;

import cn.ibizlab.odoo.core.odoo_stock.filter.Stock_moveSearchContext;

/**
 * 实体 [库存移动] 存储模型
 */
public interface Stock_move{

    /**
     * 全部退回移动
     */
    String getReturned_move_ids();

    void setReturned_move_ids(String returned_move_ids);

    /**
     * 获取 [全部退回移动]脏标记
     */
    boolean getReturned_move_idsDirtyFlag();

    /**
     * 在分拣确认后是否添加了移动
     */
    String getAdditional();

    void setAdditional(String additional);

    /**
     * 获取 [在分拣确认后是否添加了移动]脏标记
     */
    boolean getAdditionalDirtyFlag();

    /**
     * 单价
     */
    Double getPrice_unit();

    void setPrice_unit(Double price_unit);

    /**
     * 获取 [单价]脏标记
     */
    boolean getPrice_unitDirtyFlag();

    /**
     * 补货组
     */
    Integer getGroup_id();

    void setGroup_id(Integer group_id);

    /**
     * 获取 [补货组]脏标记
     */
    boolean getGroup_idDirtyFlag();

    /**
     * 报废
     */
    String getScrap_ids();

    void setScrap_ids(String scrap_ids);

    /**
     * 获取 [报废]脏标记
     */
    boolean getScrap_idsDirtyFlag();

    /**
     * 会计凭证
     */
    String getAccount_move_ids();

    void setAccount_move_ids(String account_move_ids);

    /**
     * 获取 [会计凭证]脏标记
     */
    boolean getAccount_move_idsDirtyFlag();

    /**
     * 目的地移动
     */
    String getMove_dest_ids();

    void setMove_dest_ids(String move_dest_ids);

    /**
     * 获取 [目的地移动]脏标记
     */
    boolean getMove_dest_idsDirtyFlag();

    /**
     * ID
     */
    Integer getId();

    void setId(Integer id);

    /**
     * 获取 [ID]脏标记
     */
    boolean getIdDirtyFlag();

    /**
     * 退款 (更新 SO/PO)
     */
    String getTo_refund();

    void setTo_refund(String to_refund);

    /**
     * 获取 [退款 (更新 SO/PO)]脏标记
     */
    boolean getTo_refundDirtyFlag();

    /**
     * 单位因子
     */
    Double getUnit_factor();

    void setUnit_factor(Double unit_factor);

    /**
     * 获取 [单位因子]脏标记
     */
    boolean getUnit_factorDirtyFlag();

    /**
     * 最后修改日
     */
    Timestamp get__last_update();

    void set__last_update(Timestamp __last_update);

    /**
     * 获取 [最后修改日]脏标记
     */
    boolean get__last_updateDirtyFlag();

    /**
     * 预测数量
     */
    Double getAvailability();

    void setAvailability(Double availability);

    /**
     * 获取 [预测数量]脏标记
     */
    boolean getAvailabilityDirtyFlag();

    /**
     * 剩余数量
     */
    Double getRemaining_qty();

    void setRemaining_qty(Double remaining_qty);

    /**
     * 获取 [剩余数量]脏标记
     */
    boolean getRemaining_qtyDirtyFlag();

    /**
     * 传播取消以及拆分
     */
    String getPropagate();

    void setPropagate(String propagate);

    /**
     * 获取 [传播取消以及拆分]脏标记
     */
    boolean getPropagateDirtyFlag();

    /**
     * 编号
     */
    String getReference();

    void setReference(String reference);

    /**
     * 获取 [编号]脏标记
     */
    boolean getReferenceDirtyFlag();

    /**
     * 批次
     */
    String getActive_move_line_ids();

    void setActive_move_line_ids(String active_move_line_ids);

    /**
     * 获取 [批次]脏标记
     */
    boolean getActive_move_line_idsDirtyFlag();

    /**
     * 移动行无建议
     */
    String getMove_line_nosuggest_ids();

    void setMove_line_nosuggest_ids(String move_line_nosuggest_ids);

    /**
     * 获取 [移动行无建议]脏标记
     */
    boolean getMove_line_nosuggest_idsDirtyFlag();

    /**
     * 详情可见
     */
    String getShow_details_visible();

    void setShow_details_visible(String show_details_visible);

    /**
     * 获取 [详情可见]脏标记
     */
    boolean getShow_details_visibleDirtyFlag();

    /**
     * 实际数量
     */
    Double getProduct_qty();

    void setProduct_qty(Double product_qty);

    /**
     * 获取 [实际数量]脏标记
     */
    boolean getProduct_qtyDirtyFlag();

    /**
     * 最后更新时间
     */
    Timestamp getWrite_date();

    void setWrite_date(Timestamp write_date);

    /**
     * 获取 [最后更新时间]脏标记
     */
    boolean getWrite_dateDirtyFlag();

    /**
     * 源文档
     */
    String getOrigin();

    void setOrigin(String origin);

    /**
     * 获取 [源文档]脏标记
     */
    boolean getOriginDirtyFlag();

    /**
     * 初始需求
     */
    Double getProduct_uom_qty();

    void setProduct_uom_qty(Double product_uom_qty);

    /**
     * 获取 [初始需求]脏标记
     */
    boolean getProduct_uom_qtyDirtyFlag();

    /**
     * 追踪
     */
    String getNeeds_lots();

    void setNeeds_lots(String needs_lots);

    /**
     * 获取 [追踪]脏标记
     */
    boolean getNeeds_lotsDirtyFlag();

    /**
     * 优先级
     */
    String getPriority();

    void setPriority(String priority);

    /**
     * 获取 [优先级]脏标记
     */
    boolean getPriorityDirtyFlag();

    /**
     * 日期
     */
    Timestamp getDate();

    void setDate(Timestamp date);

    /**
     * 获取 [日期]脏标记
     */
    boolean getDateDirtyFlag();

    /**
     * 剩余价值
     */
    Double getRemaining_value();

    void setRemaining_value(Double remaining_value);

    /**
     * 获取 [剩余价值]脏标记
     */
    boolean getRemaining_valueDirtyFlag();

    /**
     * 订单完成批次
     */
    String getOrder_finished_lot_ids();

    void setOrder_finished_lot_ids(String order_finished_lot_ids);

    /**
     * 获取 [订单完成批次]脏标记
     */
    boolean getOrder_finished_lot_idsDirtyFlag();

    /**
     * 序号
     */
    Integer getSequence();

    void setSequence(Integer sequence);

    /**
     * 获取 [序号]脏标记
     */
    boolean getSequenceDirtyFlag();

    /**
     * 创建日期
     */
    Timestamp getCreate_date();

    void setCreate_date(Timestamp create_date);

    /**
     * 获取 [创建日期]脏标记
     */
    boolean getCreate_dateDirtyFlag();

    /**
     * 原始移动
     */
    String getMove_orig_ids();

    void setMove_orig_ids(String move_orig_ids);

    /**
     * 获取 [原始移动]脏标记
     */
    boolean getMove_orig_idsDirtyFlag();

    /**
     * 凭证明细
     */
    String getMove_line_ids();

    void setMove_line_ids(String move_line_ids);

    /**
     * 获取 [凭证明细]脏标记
     */
    boolean getMove_line_idsDirtyFlag();

    /**
     * 备注
     */
    String getNote();

    void setNote(String note);

    /**
     * 获取 [备注]脏标记
     */
    boolean getNoteDirtyFlag();

    /**
     * 已预留数量
     */
    Double getReserved_availability();

    void setReserved_availability(Double reserved_availability);

    /**
     * 获取 [已预留数量]脏标记
     */
    boolean getReserved_availabilityDirtyFlag();

    /**
     * 初始需求是否可以编辑
     */
    String getIs_initial_demand_editable();

    void setIs_initial_demand_editable(String is_initial_demand_editable);

    /**
     * 获取 [初始需求是否可以编辑]脏标记
     */
    boolean getIs_initial_demand_editableDirtyFlag();

    /**
     * 完成数量
     */
    Double getQuantity_done();

    void setQuantity_done(Double quantity_done);

    /**
     * 获取 [完成数量]脏标记
     */
    boolean getQuantity_doneDirtyFlag();

    /**
     * 是锁定
     */
    String getIs_locked();

    void setIs_locked(String is_locked);

    /**
     * 获取 [是锁定]脏标记
     */
    boolean getIs_lockedDirtyFlag();

    /**
     * 可用量
     */
    String getString_availability_info();

    void setString_availability_info(String string_availability_info);

    /**
     * 获取 [可用量]脏标记
     */
    boolean getString_availability_infoDirtyFlag();

    /**
     * 显示名称
     */
    String getDisplay_name();

    void setDisplay_name(String display_name);

    /**
     * 获取 [显示名称]脏标记
     */
    boolean getDisplay_nameDirtyFlag();

    /**
     * 从供应商
     */
    String getShow_reserved_availability();

    void setShow_reserved_availability(String show_reserved_availability);

    /**
     * 获取 [从供应商]脏标记
     */
    boolean getShow_reserved_availabilityDirtyFlag();

    /**
     * 目的路线
     */
    String getRoute_ids();

    void setRoute_ids(String route_ids);

    /**
     * 获取 [目的路线]脏标记
     */
    boolean getRoute_idsDirtyFlag();

    /**
     * 状态
     */
    String getState();

    void setState(String state);

    /**
     * 获取 [状态]脏标记
     */
    boolean getStateDirtyFlag();

    /**
     * 说明
     */
    String getName();

    void setName(String name);

    /**
     * 获取 [说明]脏标记
     */
    boolean getNameDirtyFlag();

    /**
     * 值
     */
    Double getValue();

    void setValue(Double value);

    /**
     * 获取 [值]脏标记
     */
    boolean getValueDirtyFlag();

    /**
     * 完工批次已存在
     */
    String getFinished_lots_exist();

    void setFinished_lots_exist(String finished_lots_exist);

    /**
     * 获取 [完工批次已存在]脏标记
     */
    boolean getFinished_lots_existDirtyFlag();

    /**
     * 完成
     */
    String getIs_done();

    void setIs_done(String is_done);

    /**
     * 获取 [完成]脏标记
     */
    boolean getIs_doneDirtyFlag();

    /**
     * 完成数量是否可以编辑
     */
    String getIs_quantity_done_editable();

    void setIs_quantity_done_editable(String is_quantity_done_editable);

    /**
     * 获取 [完成数量是否可以编辑]脏标记
     */
    boolean getIs_quantity_done_editableDirtyFlag();

    /**
     * 有移动行
     */
    String getHas_move_lines();

    void setHas_move_lines(String has_move_lines);

    /**
     * 获取 [有移动行]脏标记
     */
    boolean getHas_move_linesDirtyFlag();

    /**
     * 预计日期
     */
    Timestamp getDate_expected();

    void setDate_expected(Timestamp date_expected);

    /**
     * 获取 [预计日期]脏标记
     */
    boolean getDate_expectedDirtyFlag();

    /**
     * 供应方法
     */
    String getProcure_method();

    void setProcure_method(String procure_method);

    /**
     * 获取 [供应方法]脏标记
     */
    boolean getProcure_methodDirtyFlag();

    /**
     * 产品类型
     */
    String getProduct_type();

    void setProduct_type(String product_type);

    /**
     * 获取 [产品类型]脏标记
     */
    boolean getProduct_typeDirtyFlag();

    /**
     * 拆卸顺序
     */
    String getUnbuild_id_text();

    void setUnbuild_id_text(String unbuild_id_text);

    /**
     * 获取 [拆卸顺序]脏标记
     */
    boolean getUnbuild_id_textDirtyFlag();

    /**
     * 已报废
     */
    String getScrapped();

    void setScrapped(String scrapped);

    /**
     * 获取 [已报废]脏标记
     */
    boolean getScrappedDirtyFlag();

    /**
     * 公司
     */
    String getCompany_id_text();

    void setCompany_id_text(String company_id_text);

    /**
     * 获取 [公司]脏标记
     */
    boolean getCompany_id_textDirtyFlag();

    /**
     * 成品的生产订单
     */
    String getProduction_id_text();

    void setProduction_id_text(String production_id_text);

    /**
     * 获取 [成品的生产订单]脏标记
     */
    boolean getProduction_id_textDirtyFlag();

    /**
     * 仓库
     */
    String getWarehouse_id_text();

    void setWarehouse_id_text(String warehouse_id_text);

    /**
     * 获取 [仓库]脏标记
     */
    boolean getWarehouse_id_textDirtyFlag();

    /**
     * 显示详细作业
     */
    String getShow_operations();

    void setShow_operations(String show_operations);

    /**
     * 获取 [显示详细作业]脏标记
     */
    boolean getShow_operationsDirtyFlag();

    /**
     * 创建生产订单
     */
    String getCreated_production_id_text();

    void setCreated_production_id_text(String created_production_id_text);

    /**
     * 获取 [创建生产订单]脏标记
     */
    boolean getCreated_production_id_textDirtyFlag();

    /**
     * 创建采购订单行
     */
    String getCreated_purchase_line_id_text();

    void setCreated_purchase_line_id_text(String created_purchase_line_id_text);

    /**
     * 获取 [创建采购订单行]脏标记
     */
    boolean getCreated_purchase_line_id_textDirtyFlag();

    /**
     * 目的位置
     */
    String getLocation_dest_id_text();

    void setLocation_dest_id_text(String location_dest_id_text);

    /**
     * 获取 [目的位置]脏标记
     */
    boolean getLocation_dest_id_textDirtyFlag();

    /**
     * 使用追踪的产品
     */
    String getHas_tracking();

    void setHas_tracking(String has_tracking);

    /**
     * 获取 [使用追踪的产品]脏标记
     */
    boolean getHas_trackingDirtyFlag();

    /**
     * 作业类型
     */
    String getPicking_type_id_text();

    void setPicking_type_id_text(String picking_type_id_text);

    /**
     * 获取 [作业类型]脏标记
     */
    boolean getPicking_type_id_textDirtyFlag();

    /**
     * 销售明细行
     */
    String getSale_line_id_text();

    void setSale_line_id_text(String sale_line_id_text);

    /**
     * 获取 [销售明细行]脏标记
     */
    boolean getSale_line_id_textDirtyFlag();

    /**
     * 目的地地址
     */
    String getPartner_id_text();

    void setPartner_id_text(String partner_id_text);

    /**
     * 获取 [目的地地址]脏标记
     */
    boolean getPartner_id_textDirtyFlag();

    /**
     * 调拨目的地地址
     */
    Integer getPicking_partner_id();

    void setPicking_partner_id(Integer picking_partner_id);

    /**
     * 获取 [调拨目的地地址]脏标记
     */
    boolean getPicking_partner_idDirtyFlag();

    /**
     * 原材料的生产订单
     */
    String getRaw_material_production_id_text();

    void setRaw_material_production_id_text(String raw_material_production_id_text);

    /**
     * 获取 [原材料的生产订单]脏标记
     */
    boolean getRaw_material_production_id_textDirtyFlag();

    /**
     * 原始退回移动
     */
    String getOrigin_returned_move_id_text();

    void setOrigin_returned_move_id_text(String origin_returned_move_id_text);

    /**
     * 获取 [原始退回移动]脏标记
     */
    boolean getOrigin_returned_move_id_textDirtyFlag();

    /**
     * 维修
     */
    String getRepair_id_text();

    void setRepair_id_text(String repair_id_text);

    /**
     * 获取 [维修]脏标记
     */
    boolean getRepair_id_textDirtyFlag();

    /**
     * 欠单
     */
    Integer getBackorder_id();

    void setBackorder_id(Integer backorder_id);

    /**
     * 获取 [欠单]脏标记
     */
    boolean getBackorder_idDirtyFlag();

    /**
     * 采购订单行
     */
    String getPurchase_line_id_text();

    void setPurchase_line_id_text(String purchase_line_id_text);

    /**
     * 获取 [采购订单行]脏标记
     */
    boolean getPurchase_line_id_textDirtyFlag();

    /**
     * 调拨参照
     */
    String getPicking_id_text();

    void setPicking_id_text(String picking_id_text);

    /**
     * 获取 [调拨参照]脏标记
     */
    boolean getPicking_id_textDirtyFlag();

    /**
     * 产品
     */
    String getProduct_id_text();

    void setProduct_id_text(String product_id_text);

    /**
     * 获取 [产品]脏标记
     */
    boolean getProduct_id_textDirtyFlag();

    /**
     * 源位置
     */
    String getLocation_id_text();

    void setLocation_id_text(String location_id_text);

    /**
     * 获取 [源位置]脏标记
     */
    boolean getLocation_id_textDirtyFlag();

    /**
     * 创建人
     */
    String getCreate_uid_text();

    void setCreate_uid_text(String create_uid_text);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uid_textDirtyFlag();

    /**
     * 单位
     */
    String getProduct_uom_text();

    void setProduct_uom_text(String product_uom_text);

    /**
     * 获取 [单位]脏标记
     */
    boolean getProduct_uom_textDirtyFlag();

    /**
     * 待加工的作业
     */
    String getOperation_id_text();

    void setOperation_id_text(String operation_id_text);

    /**
     * 获取 [待加工的作业]脏标记
     */
    boolean getOperation_id_textDirtyFlag();

    /**
     * 待消耗的工单
     */
    String getWorkorder_id_text();

    void setWorkorder_id_text(String workorder_id_text);

    /**
     * 获取 [待消耗的工单]脏标记
     */
    boolean getWorkorder_id_textDirtyFlag();

    /**
     * 拆卸单
     */
    String getConsume_unbuild_id_text();

    void setConsume_unbuild_id_text(String consume_unbuild_id_text);

    /**
     * 获取 [拆卸单]脏标记
     */
    boolean getConsume_unbuild_id_textDirtyFlag();

    /**
     * 首选包装
     */
    String getProduct_packaging_text();

    void setProduct_packaging_text(String product_packaging_text);

    /**
     * 获取 [首选包装]脏标记
     */
    boolean getProduct_packaging_textDirtyFlag();

    /**
     * 所有者
     */
    String getRestrict_partner_id_text();

    void setRestrict_partner_id_text(String restrict_partner_id_text);

    /**
     * 获取 [所有者]脏标记
     */
    boolean getRestrict_partner_id_textDirtyFlag();

    /**
     * 库存
     */
    String getInventory_id_text();

    void setInventory_id_text(String inventory_id_text);

    /**
     * 获取 [库存]脏标记
     */
    boolean getInventory_id_textDirtyFlag();

    /**
     * 产品模板
     */
    Integer getProduct_tmpl_id();

    void setProduct_tmpl_id(Integer product_tmpl_id);

    /**
     * 获取 [产品模板]脏标记
     */
    boolean getProduct_tmpl_idDirtyFlag();

    /**
     * 库存规则
     */
    String getRule_id_text();

    void setRule_id_text(String rule_id_text);

    /**
     * 获取 [库存规则]脏标记
     */
    boolean getRule_id_textDirtyFlag();

    /**
     * 最后更新者
     */
    String getWrite_uid_text();

    void setWrite_uid_text(String write_uid_text);

    /**
     * 获取 [最后更新者]脏标记
     */
    boolean getWrite_uid_textDirtyFlag();

    /**
     * 作业的类型
     */
    String getPicking_code();

    void setPicking_code(String picking_code);

    /**
     * 获取 [作业的类型]脏标记
     */
    boolean getPicking_codeDirtyFlag();

    /**
     * 移动整个包裹
     */
    String getPicking_type_entire_packs();

    void setPicking_type_entire_packs(String picking_type_entire_packs);

    /**
     * 获取 [移动整个包裹]脏标记
     */
    boolean getPicking_type_entire_packsDirtyFlag();

    /**
     * 创建生产订单
     */
    Integer getCreated_production_id();

    void setCreated_production_id(Integer created_production_id);

    /**
     * 获取 [创建生产订单]脏标记
     */
    boolean getCreated_production_idDirtyFlag();

    /**
     * 拆卸单
     */
    Integer getConsume_unbuild_id();

    void setConsume_unbuild_id(Integer consume_unbuild_id);

    /**
     * 获取 [拆卸单]脏标记
     */
    boolean getConsume_unbuild_idDirtyFlag();

    /**
     * 销售明细行
     */
    Integer getSale_line_id();

    void setSale_line_id(Integer sale_line_id);

    /**
     * 获取 [销售明细行]脏标记
     */
    boolean getSale_line_idDirtyFlag();

    /**
     * 原始退回移动
     */
    Integer getOrigin_returned_move_id();

    void setOrigin_returned_move_id(Integer origin_returned_move_id);

    /**
     * 获取 [原始退回移动]脏标记
     */
    boolean getOrigin_returned_move_idDirtyFlag();

    /**
     * 作业类型
     */
    Integer getPicking_type_id();

    void setPicking_type_id(Integer picking_type_id);

    /**
     * 获取 [作业类型]脏标记
     */
    boolean getPicking_type_idDirtyFlag();

    /**
     * 待加工的作业
     */
    Integer getOperation_id();

    void setOperation_id(Integer operation_id);

    /**
     * 获取 [待加工的作业]脏标记
     */
    boolean getOperation_idDirtyFlag();

    /**
     * BOM行
     */
    Integer getBom_line_id();

    void setBom_line_id(Integer bom_line_id);

    /**
     * 获取 [BOM行]脏标记
     */
    boolean getBom_line_idDirtyFlag();

    /**
     * 维修
     */
    Integer getRepair_id();

    void setRepair_id(Integer repair_id);

    /**
     * 获取 [维修]脏标记
     */
    boolean getRepair_idDirtyFlag();

    /**
     * 产品
     */
    Integer getProduct_id();

    void setProduct_id(Integer product_id);

    /**
     * 获取 [产品]脏标记
     */
    boolean getProduct_idDirtyFlag();

    /**
     * 拆卸顺序
     */
    Integer getUnbuild_id();

    void setUnbuild_id(Integer unbuild_id);

    /**
     * 获取 [拆卸顺序]脏标记
     */
    boolean getUnbuild_idDirtyFlag();

    /**
     * 成品的生产订单
     */
    Integer getProduction_id();

    void setProduction_id(Integer production_id);

    /**
     * 获取 [成品的生产订单]脏标记
     */
    boolean getProduction_idDirtyFlag();

    /**
     * 创建采购订单行
     */
    Integer getCreated_purchase_line_id();

    void setCreated_purchase_line_id(Integer created_purchase_line_id);

    /**
     * 获取 [创建采购订单行]脏标记
     */
    boolean getCreated_purchase_line_idDirtyFlag();

    /**
     * 待消耗的工单
     */
    Integer getWorkorder_id();

    void setWorkorder_id(Integer workorder_id);

    /**
     * 获取 [待消耗的工单]脏标记
     */
    boolean getWorkorder_idDirtyFlag();

    /**
     * 单位
     */
    Integer getProduct_uom();

    void setProduct_uom(Integer product_uom);

    /**
     * 获取 [单位]脏标记
     */
    boolean getProduct_uomDirtyFlag();

    /**
     * 调拨参照
     */
    Integer getPicking_id();

    void setPicking_id(Integer picking_id);

    /**
     * 获取 [调拨参照]脏标记
     */
    boolean getPicking_idDirtyFlag();

    /**
     * 目的地地址
     */
    Integer getPartner_id();

    void setPartner_id(Integer partner_id);

    /**
     * 获取 [目的地地址]脏标记
     */
    boolean getPartner_idDirtyFlag();

    /**
     * 创建人
     */
    Integer getCreate_uid();

    void setCreate_uid(Integer create_uid);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uidDirtyFlag();

    /**
     * 包裹层级
     */
    Integer getPackage_level_id();

    void setPackage_level_id(Integer package_level_id);

    /**
     * 获取 [包裹层级]脏标记
     */
    boolean getPackage_level_idDirtyFlag();

    /**
     * 首选包装
     */
    Integer getProduct_packaging();

    void setProduct_packaging(Integer product_packaging);

    /**
     * 获取 [首选包装]脏标记
     */
    boolean getProduct_packagingDirtyFlag();

    /**
     * 所有者
     */
    Integer getRestrict_partner_id();

    void setRestrict_partner_id(Integer restrict_partner_id);

    /**
     * 获取 [所有者]脏标记
     */
    boolean getRestrict_partner_idDirtyFlag();

    /**
     * 库存规则
     */
    Integer getRule_id();

    void setRule_id(Integer rule_id);

    /**
     * 获取 [库存规则]脏标记
     */
    boolean getRule_idDirtyFlag();

    /**
     * 仓库
     */
    Integer getWarehouse_id();

    void setWarehouse_id(Integer warehouse_id);

    /**
     * 获取 [仓库]脏标记
     */
    boolean getWarehouse_idDirtyFlag();

    /**
     * 目的位置
     */
    Integer getLocation_dest_id();

    void setLocation_dest_id(Integer location_dest_id);

    /**
     * 获取 [目的位置]脏标记
     */
    boolean getLocation_dest_idDirtyFlag();

    /**
     * 原材料的生产订单
     */
    Integer getRaw_material_production_id();

    void setRaw_material_production_id(Integer raw_material_production_id);

    /**
     * 获取 [原材料的生产订单]脏标记
     */
    boolean getRaw_material_production_idDirtyFlag();

    /**
     * 库存
     */
    Integer getInventory_id();

    void setInventory_id(Integer inventory_id);

    /**
     * 获取 [库存]脏标记
     */
    boolean getInventory_idDirtyFlag();

    /**
     * 采购订单行
     */
    Integer getPurchase_line_id();

    void setPurchase_line_id(Integer purchase_line_id);

    /**
     * 获取 [采购订单行]脏标记
     */
    boolean getPurchase_line_idDirtyFlag();

    /**
     * 源位置
     */
    Integer getLocation_id();

    void setLocation_id(Integer location_id);

    /**
     * 获取 [源位置]脏标记
     */
    boolean getLocation_idDirtyFlag();

    /**
     * 公司
     */
    Integer getCompany_id();

    void setCompany_id(Integer company_id);

    /**
     * 获取 [公司]脏标记
     */
    boolean getCompany_idDirtyFlag();

    /**
     * 最后更新者
     */
    Integer getWrite_uid();

    void setWrite_uid(Integer write_uid);

    /**
     * 获取 [最后更新者]脏标记
     */
    boolean getWrite_uidDirtyFlag();

}
