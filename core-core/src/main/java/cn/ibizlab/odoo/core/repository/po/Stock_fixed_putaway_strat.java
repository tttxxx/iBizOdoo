package cn.ibizlab.odoo.core.repository.po ;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.math.BigInteger;

import cn.ibizlab.odoo.core.odoo_stock.filter.Stock_fixed_putaway_stratSearchContext;

/**
 * 实体 [位置固定上架策略] 存储模型
 */
public interface Stock_fixed_putaway_strat{

    /**
     * 最后更新时间
     */
    Timestamp getWrite_date();

    void setWrite_date(Timestamp write_date);

    /**
     * 获取 [最后更新时间]脏标记
     */
    boolean getWrite_dateDirtyFlag();

    /**
     * ID
     */
    Integer getId();

    void setId(Integer id);

    /**
     * 获取 [ID]脏标记
     */
    boolean getIdDirtyFlag();

    /**
     * 创建时间
     */
    Timestamp getCreate_date();

    void setCreate_date(Timestamp create_date);

    /**
     * 获取 [创建时间]脏标记
     */
    boolean getCreate_dateDirtyFlag();

    /**
     * 优先级
     */
    Integer getSequence();

    void setSequence(Integer sequence);

    /**
     * 获取 [优先级]脏标记
     */
    boolean getSequenceDirtyFlag();

    /**
     * 显示名称
     */
    String getDisplay_name();

    void setDisplay_name(String display_name);

    /**
     * 获取 [显示名称]脏标记
     */
    boolean getDisplay_nameDirtyFlag();

    /**
     * 最后修改日
     */
    Timestamp get__last_update();

    void set__last_update(Timestamp __last_update);

    /**
     * 获取 [最后修改日]脏标记
     */
    boolean get__last_updateDirtyFlag();

    /**
     * 产品分类
     */
    String getCategory_id_text();

    void setCategory_id_text(String category_id_text);

    /**
     * 获取 [产品分类]脏标记
     */
    boolean getCategory_id_textDirtyFlag();

    /**
     * 位置
     */
    String getFixed_location_id_text();

    void setFixed_location_id_text(String fixed_location_id_text);

    /**
     * 获取 [位置]脏标记
     */
    boolean getFixed_location_id_textDirtyFlag();

    /**
     * 最后更新者
     */
    String getWrite_uid_text();

    void setWrite_uid_text(String write_uid_text);

    /**
     * 获取 [最后更新者]脏标记
     */
    boolean getWrite_uid_textDirtyFlag();

    /**
     * 产品
     */
    String getProduct_id_text();

    void setProduct_id_text(String product_id_text);

    /**
     * 获取 [产品]脏标记
     */
    boolean getProduct_id_textDirtyFlag();

    /**
     * 上架方法
     */
    String getPutaway_id_text();

    void setPutaway_id_text(String putaway_id_text);

    /**
     * 获取 [上架方法]脏标记
     */
    boolean getPutaway_id_textDirtyFlag();

    /**
     * 创建人
     */
    String getCreate_uid_text();

    void setCreate_uid_text(String create_uid_text);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uid_textDirtyFlag();

    /**
     * 上架方法
     */
    Integer getPutaway_id();

    void setPutaway_id(Integer putaway_id);

    /**
     * 获取 [上架方法]脏标记
     */
    boolean getPutaway_idDirtyFlag();

    /**
     * 产品分类
     */
    Integer getCategory_id();

    void setCategory_id(Integer category_id);

    /**
     * 获取 [产品分类]脏标记
     */
    boolean getCategory_idDirtyFlag();

    /**
     * 最后更新者
     */
    Integer getWrite_uid();

    void setWrite_uid(Integer write_uid);

    /**
     * 获取 [最后更新者]脏标记
     */
    boolean getWrite_uidDirtyFlag();

    /**
     * 产品
     */
    Integer getProduct_id();

    void setProduct_id(Integer product_id);

    /**
     * 获取 [产品]脏标记
     */
    boolean getProduct_idDirtyFlag();

    /**
     * 创建人
     */
    Integer getCreate_uid();

    void setCreate_uid(Integer create_uid);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uidDirtyFlag();

    /**
     * 位置
     */
    Integer getFixed_location_id();

    void setFixed_location_id(Integer fixed_location_id);

    /**
     * 获取 [位置]脏标记
     */
    boolean getFixed_location_idDirtyFlag();

}
