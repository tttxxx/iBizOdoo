package cn.ibizlab.odoo.core.odoo_base.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_base.domain.Base_module_uninstall;
import cn.ibizlab.odoo.core.odoo_base.filter.Base_module_uninstallSearchContext;
import cn.ibizlab.odoo.core.odoo_base.service.IBase_module_uninstallService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;



import cn.ibizlab.odoo.core.odoo_base.client.base_module_uninstallOdooClient;
import cn.ibizlab.odoo.core.odoo_base.clientmodel.base_module_uninstallClientModel;

/**
 * 实体[模块卸载] 服务对象接口实现
 */
@Slf4j
@Service
public class Base_module_uninstallServiceImpl implements IBase_module_uninstallService {

    @Autowired
    base_module_uninstallOdooClient base_module_uninstallOdooClient;


    @Override
    public boolean update(Base_module_uninstall et) {
        base_module_uninstallClientModel clientModel = convert2Model(et,null);
		base_module_uninstallOdooClient.update(clientModel);
        Base_module_uninstall rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }
    
    public void updateBatch(List<Base_module_uninstall> list){
    }

    @Override
    public Base_module_uninstall get(Integer id) {
        base_module_uninstallClientModel clientModel = new base_module_uninstallClientModel();
        clientModel.setId(id);
		base_module_uninstallOdooClient.get(clientModel);
        Base_module_uninstall et = convert2Domain(clientModel,null);
        if(et==null){
            et=new Base_module_uninstall();
            et.setId(id);
        }
        else{
        }

        return  et;
    }

    @Override
    public boolean remove(Integer id) {
        base_module_uninstallClientModel clientModel = new base_module_uninstallClientModel();
        clientModel.setId(id);
		base_module_uninstallOdooClient.remove(clientModel);
        return true; 
    }

    public void removeBatch(Collection<Integer> idList){
    }

    @Override
    public boolean create(Base_module_uninstall et) {
        base_module_uninstallClientModel clientModel = convert2Model(et,null);
		base_module_uninstallOdooClient.create(clientModel);
        Base_module_uninstall rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Base_module_uninstall> list){
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Base_module_uninstall> searchDefault(Base_module_uninstallSearchContext context) {
        List<Base_module_uninstall> list = new ArrayList<Base_module_uninstall>();
        Page<base_module_uninstallClientModel> clientModelList = base_module_uninstallOdooClient.search(context);
        for(base_module_uninstallClientModel clientModel : clientModelList){
            list.add( this.convert2Domain(clientModel,null));
        }
        return new PageImpl<Base_module_uninstall>(list, context.getPageable(), clientModelList.getTotalElements());
    }

    public base_module_uninstallClientModel convert2Model(Base_module_uninstall domain , base_module_uninstallClientModel model) {
        if(domain == null)
            return model ;
        if(model==null){
            model = new base_module_uninstallClientModel();

        }
        if((Boolean) domain.getExtensionparams().get("dirtyflagenable")){
            if((Boolean) domain.getExtensionparams().get("module_idsdirtyflag"))
                model.setModule_ids(domain.getModuleIds());
            if((Boolean) domain.getExtensionparams().get("iddirtyflag"))
                model.setId(domain.getId());
            if((Boolean) domain.getExtensionparams().get("model_idsdirtyflag"))
                model.setModel_ids(domain.getModelIds());
            if((Boolean) domain.getExtensionparams().get("show_alldirtyflag"))
                model.setShow_all(domain.getShowAll());
            if((Boolean) domain.getExtensionparams().get("__last_updatedirtyflag"))
                model.set__last_update(domain.getLastUpdate());
            if((Boolean) domain.getExtensionparams().get("write_datedirtyflag"))
                model.setWrite_date(domain.getWriteDate());
            if((Boolean) domain.getExtensionparams().get("create_datedirtyflag"))
                model.setCreate_date(domain.getCreateDate());
            if((Boolean) domain.getExtensionparams().get("module_iddirtyflag"))
                model.setModule_id(domain.getModuleId());
            if((Boolean) domain.getExtensionparams().get("display_namedirtyflag"))
                model.setDisplay_name(domain.getDisplayName());
            if((Boolean) domain.getExtensionparams().get("create_uid_textdirtyflag"))
                model.setCreate_uid_text(domain.getCreateUidText());
            if((Boolean) domain.getExtensionparams().get("write_uid_textdirtyflag"))
                model.setWrite_uid_text(domain.getWriteUidText());
            if((Boolean) domain.getExtensionparams().get("create_uiddirtyflag"))
                model.setCreate_uid(domain.getCreateUid());
            if((Boolean) domain.getExtensionparams().get("write_uiddirtyflag"))
                model.setWrite_uid(domain.getWriteUid());
            domain.getFocusNull().clear();
        }
        return model ;
    }

    public Base_module_uninstall convert2Domain( base_module_uninstallClientModel model ,Base_module_uninstall domain) {
        if(model == null)
            return domain ;
        if(domain == null){
            domain = new Base_module_uninstall();
        }

        if(model.getModule_idsDirtyFlag())
            domain.setModuleIds(model.getModule_ids());
        if(model.getIdDirtyFlag())
            domain.setId(model.getId());
        if(model.getModel_idsDirtyFlag())
            domain.setModelIds(model.getModel_ids());
        if(model.getShow_allDirtyFlag())
            domain.setShowAll(model.getShow_all());
        if(model.get__last_updateDirtyFlag())
            domain.setLastUpdate(model.get__last_update());
        if(model.getWrite_dateDirtyFlag())
            domain.setWriteDate(model.getWrite_date());
        if(model.getCreate_dateDirtyFlag())
            domain.setCreateDate(model.getCreate_date());
        if(model.getModule_idDirtyFlag())
            domain.setModuleId(model.getModule_id());
        if(model.getDisplay_nameDirtyFlag())
            domain.setDisplayName(model.getDisplay_name());
        if(model.getCreate_uid_textDirtyFlag())
            domain.setCreateUidText(model.getCreate_uid_text());
        if(model.getWrite_uid_textDirtyFlag())
            domain.setWriteUidText(model.getWrite_uid_text());
        if(model.getCreate_uidDirtyFlag())
            domain.setCreateUid(model.getCreate_uid());
        if(model.getWrite_uidDirtyFlag())
            domain.setWriteUid(model.getWrite_uid());
        return domain ;
    }

}

    



