package cn.ibizlab.odoo.core.odoo_project.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_project.domain.Project_task_type;
import cn.ibizlab.odoo.core.odoo_project.filter.Project_task_typeSearchContext;
import cn.ibizlab.odoo.core.odoo_project.service.IProject_task_typeService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;



import cn.ibizlab.odoo.core.odoo_project.client.project_task_typeOdooClient;
import cn.ibizlab.odoo.core.odoo_project.clientmodel.project_task_typeClientModel;

/**
 * 实体[任务阶段] 服务对象接口实现
 */
@Slf4j
@Service
public class Project_task_typeServiceImpl implements IProject_task_typeService {

    @Autowired
    project_task_typeOdooClient project_task_typeOdooClient;


    @Override
    public Project_task_type get(Integer id) {
        project_task_typeClientModel clientModel = new project_task_typeClientModel();
        clientModel.setId(id);
		project_task_typeOdooClient.get(clientModel);
        Project_task_type et = convert2Domain(clientModel,null);
        if(et==null){
            et=new Project_task_type();
            et.setId(id);
        }
        else{
        }

        return  et;
    }

    @Override
    public boolean create(Project_task_type et) {
        project_task_typeClientModel clientModel = convert2Model(et,null);
		project_task_typeOdooClient.create(clientModel);
        Project_task_type rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Project_task_type> list){
    }

    @Override
    public boolean remove(Integer id) {
        project_task_typeClientModel clientModel = new project_task_typeClientModel();
        clientModel.setId(id);
		project_task_typeOdooClient.remove(clientModel);
        return true; 
    }

    public void removeBatch(Collection<Integer> idList){
    }

    @Override
    public boolean update(Project_task_type et) {
        project_task_typeClientModel clientModel = convert2Model(et,null);
		project_task_typeOdooClient.update(clientModel);
        Project_task_type rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }
    
    public void updateBatch(List<Project_task_type> list){
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Project_task_type> searchDefault(Project_task_typeSearchContext context) {
        List<Project_task_type> list = new ArrayList<Project_task_type>();
        Page<project_task_typeClientModel> clientModelList = project_task_typeOdooClient.search(context);
        for(project_task_typeClientModel clientModel : clientModelList){
            list.add( this.convert2Domain(clientModel,null));
        }
        return new PageImpl<Project_task_type>(list, context.getPageable(), clientModelList.getTotalElements());
    }

    public project_task_typeClientModel convert2Model(Project_task_type domain , project_task_typeClientModel model) {
        if(domain == null)
            return model ;
        if(model==null){
            model = new project_task_typeClientModel();

        }
        if((Boolean) domain.getExtensionparams().get("dirtyflagenable")){
            if((Boolean) domain.getExtensionparams().get("namedirtyflag"))
                model.setName(domain.getName());
            if((Boolean) domain.getExtensionparams().get("auto_validation_kanban_statedirtyflag"))
                model.setAuto_validation_kanban_state(domain.getAutoValidationKanbanState());
            if((Boolean) domain.getExtensionparams().get("write_datedirtyflag"))
                model.setWrite_date(domain.getWriteDate());
            if((Boolean) domain.getExtensionparams().get("display_namedirtyflag"))
                model.setDisplay_name(domain.getDisplayName());
            if((Boolean) domain.getExtensionparams().get("iddirtyflag"))
                model.setId(domain.getId());
            if((Boolean) domain.getExtensionparams().get("project_idsdirtyflag"))
                model.setProject_ids(domain.getProjectIds());
            if((Boolean) domain.getExtensionparams().get("sequencedirtyflag"))
                model.setSequence(domain.getSequence());
            if((Boolean) domain.getExtensionparams().get("descriptiondirtyflag"))
                model.setDescription(domain.getDescription());
            if((Boolean) domain.getExtensionparams().get("legend_normaldirtyflag"))
                model.setLegend_normal(domain.getLegendNormal());
            if((Boolean) domain.getExtensionparams().get("folddirtyflag"))
                model.setFold(domain.getFold());
            if((Boolean) domain.getExtensionparams().get("create_datedirtyflag"))
                model.setCreate_date(domain.getCreateDate());
            if((Boolean) domain.getExtensionparams().get("legend_blockeddirtyflag"))
                model.setLegend_blocked(domain.getLegendBlocked());
            if((Boolean) domain.getExtensionparams().get("legend_prioritydirtyflag"))
                model.setLegend_priority(domain.getLegendPriority());
            if((Boolean) domain.getExtensionparams().get("__last_updatedirtyflag"))
                model.set__last_update(domain.getLastUpdate());
            if((Boolean) domain.getExtensionparams().get("legend_donedirtyflag"))
                model.setLegend_done(domain.getLegendDone());
            if((Boolean) domain.getExtensionparams().get("create_uid_textdirtyflag"))
                model.setCreate_uid_text(domain.getCreateUidText());
            if((Boolean) domain.getExtensionparams().get("rating_template_id_textdirtyflag"))
                model.setRating_template_id_text(domain.getRatingTemplateIdText());
            if((Boolean) domain.getExtensionparams().get("write_uid_textdirtyflag"))
                model.setWrite_uid_text(domain.getWriteUidText());
            if((Boolean) domain.getExtensionparams().get("mail_template_id_textdirtyflag"))
                model.setMail_template_id_text(domain.getMailTemplateIdText());
            if((Boolean) domain.getExtensionparams().get("mail_template_iddirtyflag"))
                model.setMail_template_id(domain.getMailTemplateId());
            if((Boolean) domain.getExtensionparams().get("create_uiddirtyflag"))
                model.setCreate_uid(domain.getCreateUid());
            if((Boolean) domain.getExtensionparams().get("rating_template_iddirtyflag"))
                model.setRating_template_id(domain.getRatingTemplateId());
            if((Boolean) domain.getExtensionparams().get("write_uiddirtyflag"))
                model.setWrite_uid(domain.getWriteUid());
            domain.getFocusNull().clear();
        }
        return model ;
    }

    public Project_task_type convert2Domain( project_task_typeClientModel model ,Project_task_type domain) {
        if(model == null)
            return domain ;
        if(domain == null){
            domain = new Project_task_type();
        }

        if(model.getNameDirtyFlag())
            domain.setName(model.getName());
        if(model.getAuto_validation_kanban_stateDirtyFlag())
            domain.setAutoValidationKanbanState(model.getAuto_validation_kanban_state());
        if(model.getWrite_dateDirtyFlag())
            domain.setWriteDate(model.getWrite_date());
        if(model.getDisplay_nameDirtyFlag())
            domain.setDisplayName(model.getDisplay_name());
        if(model.getIdDirtyFlag())
            domain.setId(model.getId());
        if(model.getProject_idsDirtyFlag())
            domain.setProjectIds(model.getProject_ids());
        if(model.getSequenceDirtyFlag())
            domain.setSequence(model.getSequence());
        if(model.getDescriptionDirtyFlag())
            domain.setDescription(model.getDescription());
        if(model.getLegend_normalDirtyFlag())
            domain.setLegendNormal(model.getLegend_normal());
        if(model.getFoldDirtyFlag())
            domain.setFold(model.getFold());
        if(model.getCreate_dateDirtyFlag())
            domain.setCreateDate(model.getCreate_date());
        if(model.getLegend_blockedDirtyFlag())
            domain.setLegendBlocked(model.getLegend_blocked());
        if(model.getLegend_priorityDirtyFlag())
            domain.setLegendPriority(model.getLegend_priority());
        if(model.get__last_updateDirtyFlag())
            domain.setLastUpdate(model.get__last_update());
        if(model.getLegend_doneDirtyFlag())
            domain.setLegendDone(model.getLegend_done());
        if(model.getCreate_uid_textDirtyFlag())
            domain.setCreateUidText(model.getCreate_uid_text());
        if(model.getRating_template_id_textDirtyFlag())
            domain.setRatingTemplateIdText(model.getRating_template_id_text());
        if(model.getWrite_uid_textDirtyFlag())
            domain.setWriteUidText(model.getWrite_uid_text());
        if(model.getMail_template_id_textDirtyFlag())
            domain.setMailTemplateIdText(model.getMail_template_id_text());
        if(model.getMail_template_idDirtyFlag())
            domain.setMailTemplateId(model.getMail_template_id());
        if(model.getCreate_uidDirtyFlag())
            domain.setCreateUid(model.getCreate_uid());
        if(model.getRating_template_idDirtyFlag())
            domain.setRatingTemplateId(model.getRating_template_id());
        if(model.getWrite_uidDirtyFlag())
            domain.setWriteUid(model.getWrite_uid());
        return domain ;
    }

}

    



