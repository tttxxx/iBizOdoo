package cn.ibizlab.odoo.core.repository.client.po;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigDecimal;

/**
 * [rating_mixin] 对象
 */
public interface rating_mixin {

    public String getDisplay_name();

    public void setDisplay_name(String display_name);

    public Integer getId();

    public void setId(Integer id);

    public Integer getRating_count();

    public void setRating_count(Integer rating_count);

    public String getRating_ids();

    public void setRating_ids(String rating_ids);

    public String getRating_last_feedback();

    public void setRating_last_feedback(String rating_last_feedback);

    public byte[] getRating_last_image();

    public void setRating_last_image(byte[] rating_last_image);

    public Double getRating_last_value();

    public void setRating_last_value(Double rating_last_value);

    public Timestamp get__last_update();

    public void set__last_update(Timestamp __last_update);

}
