package cn.ibizlab.odoo.core.odoo_stock.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_stock.domain.Stock_location_route;
import cn.ibizlab.odoo.core.odoo_stock.filter.Stock_location_routeSearchContext;
import cn.ibizlab.odoo.core.odoo_stock.service.IStock_location_routeService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;



import cn.ibizlab.odoo.core.odoo_stock.client.stock_location_routeOdooClient;
import cn.ibizlab.odoo.core.odoo_stock.clientmodel.stock_location_routeClientModel;

/**
 * 实体[库存路线] 服务对象接口实现
 */
@Slf4j
@Service
public class Stock_location_routeServiceImpl implements IStock_location_routeService {

    @Autowired
    stock_location_routeOdooClient stock_location_routeOdooClient;


    @Override
    public boolean create(Stock_location_route et) {
        stock_location_routeClientModel clientModel = convert2Model(et,null);
		stock_location_routeOdooClient.create(clientModel);
        Stock_location_route rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Stock_location_route> list){
    }

    @Override
    public boolean remove(Integer id) {
        stock_location_routeClientModel clientModel = new stock_location_routeClientModel();
        clientModel.setId(id);
		stock_location_routeOdooClient.remove(clientModel);
        return true; 
    }

    public void removeBatch(Collection<Integer> idList){
    }

    @Override
    public Stock_location_route get(Integer id) {
        stock_location_routeClientModel clientModel = new stock_location_routeClientModel();
        clientModel.setId(id);
		stock_location_routeOdooClient.get(clientModel);
        Stock_location_route et = convert2Domain(clientModel,null);
        if(et==null){
            et=new Stock_location_route();
            et.setId(id);
        }
        else{
        }

        return  et;
    }

    @Override
    public boolean update(Stock_location_route et) {
        stock_location_routeClientModel clientModel = convert2Model(et,null);
		stock_location_routeOdooClient.update(clientModel);
        Stock_location_route rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }
    
    public void updateBatch(List<Stock_location_route> list){
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Stock_location_route> searchDefault(Stock_location_routeSearchContext context) {
        List<Stock_location_route> list = new ArrayList<Stock_location_route>();
        Page<stock_location_routeClientModel> clientModelList = stock_location_routeOdooClient.search(context);
        for(stock_location_routeClientModel clientModel : clientModelList){
            list.add( this.convert2Domain(clientModel,null));
        }
        return new PageImpl<Stock_location_route>(list, context.getPageable(), clientModelList.getTotalElements());
    }

    public stock_location_routeClientModel convert2Model(Stock_location_route domain , stock_location_routeClientModel model) {
        if(domain == null)
            return model ;
        if(model==null){
            model = new stock_location_routeClientModel();

        }
        if((Boolean) domain.getExtensionparams().get("dirtyflagenable")){
            if((Boolean) domain.getExtensionparams().get("sale_selectabledirtyflag"))
                model.setSale_selectable(domain.getSaleSelectable());
            if((Boolean) domain.getExtensionparams().get("write_datedirtyflag"))
                model.setWrite_date(domain.getWriteDate());
            if((Boolean) domain.getExtensionparams().get("product_idsdirtyflag"))
                model.setProduct_ids(domain.getProductIds());
            if((Boolean) domain.getExtensionparams().get("iddirtyflag"))
                model.setId(domain.getId());
            if((Boolean) domain.getExtensionparams().get("create_datedirtyflag"))
                model.setCreate_date(domain.getCreateDate());
            if((Boolean) domain.getExtensionparams().get("display_namedirtyflag"))
                model.setDisplay_name(domain.getDisplayName());
            if((Boolean) domain.getExtensionparams().get("namedirtyflag"))
                model.setName(domain.getName());
            if((Boolean) domain.getExtensionparams().get("categ_idsdirtyflag"))
                model.setCateg_ids(domain.getCategIds());
            if((Boolean) domain.getExtensionparams().get("warehouse_selectabledirtyflag"))
                model.setWarehouse_selectable(domain.getWarehouseSelectable());
            if((Boolean) domain.getExtensionparams().get("sequencedirtyflag"))
                model.setSequence(domain.getSequence());
            if((Boolean) domain.getExtensionparams().get("rule_idsdirtyflag"))
                model.setRule_ids(domain.getRuleIds());
            if((Boolean) domain.getExtensionparams().get("product_selectabledirtyflag"))
                model.setProduct_selectable(domain.getProductSelectable());
            if((Boolean) domain.getExtensionparams().get("activedirtyflag"))
                model.setActive(domain.getActive());
            if((Boolean) domain.getExtensionparams().get("product_categ_selectabledirtyflag"))
                model.setProduct_categ_selectable(domain.getProductCategSelectable());
            if((Boolean) domain.getExtensionparams().get("__last_updatedirtyflag"))
                model.set__last_update(domain.getLastUpdate());
            if((Boolean) domain.getExtensionparams().get("warehouse_idsdirtyflag"))
                model.setWarehouse_ids(domain.getWarehouseIds());
            if((Boolean) domain.getExtensionparams().get("create_uid_textdirtyflag"))
                model.setCreate_uid_text(domain.getCreateUidText());
            if((Boolean) domain.getExtensionparams().get("supplied_wh_id_textdirtyflag"))
                model.setSupplied_wh_id_text(domain.getSuppliedWhIdText());
            if((Boolean) domain.getExtensionparams().get("write_uid_textdirtyflag"))
                model.setWrite_uid_text(domain.getWriteUidText());
            if((Boolean) domain.getExtensionparams().get("supplier_wh_id_textdirtyflag"))
                model.setSupplier_wh_id_text(domain.getSupplierWhIdText());
            if((Boolean) domain.getExtensionparams().get("company_id_textdirtyflag"))
                model.setCompany_id_text(domain.getCompanyIdText());
            if((Boolean) domain.getExtensionparams().get("company_iddirtyflag"))
                model.setCompany_id(domain.getCompanyId());
            if((Boolean) domain.getExtensionparams().get("create_uiddirtyflag"))
                model.setCreate_uid(domain.getCreateUid());
            if((Boolean) domain.getExtensionparams().get("write_uiddirtyflag"))
                model.setWrite_uid(domain.getWriteUid());
            if((Boolean) domain.getExtensionparams().get("supplier_wh_iddirtyflag"))
                model.setSupplier_wh_id(domain.getSupplierWhId());
            if((Boolean) domain.getExtensionparams().get("supplied_wh_iddirtyflag"))
                model.setSupplied_wh_id(domain.getSuppliedWhId());
            domain.getFocusNull().clear();
        }
        return model ;
    }

    public Stock_location_route convert2Domain( stock_location_routeClientModel model ,Stock_location_route domain) {
        if(model == null)
            return domain ;
        if(domain == null){
            domain = new Stock_location_route();
        }

        if(model.getSale_selectableDirtyFlag())
            domain.setSaleSelectable(model.getSale_selectable());
        if(model.getWrite_dateDirtyFlag())
            domain.setWriteDate(model.getWrite_date());
        if(model.getProduct_idsDirtyFlag())
            domain.setProductIds(model.getProduct_ids());
        if(model.getIdDirtyFlag())
            domain.setId(model.getId());
        if(model.getCreate_dateDirtyFlag())
            domain.setCreateDate(model.getCreate_date());
        if(model.getDisplay_nameDirtyFlag())
            domain.setDisplayName(model.getDisplay_name());
        if(model.getNameDirtyFlag())
            domain.setName(model.getName());
        if(model.getCateg_idsDirtyFlag())
            domain.setCategIds(model.getCateg_ids());
        if(model.getWarehouse_selectableDirtyFlag())
            domain.setWarehouseSelectable(model.getWarehouse_selectable());
        if(model.getSequenceDirtyFlag())
            domain.setSequence(model.getSequence());
        if(model.getRule_idsDirtyFlag())
            domain.setRuleIds(model.getRule_ids());
        if(model.getProduct_selectableDirtyFlag())
            domain.setProductSelectable(model.getProduct_selectable());
        if(model.getActiveDirtyFlag())
            domain.setActive(model.getActive());
        if(model.getProduct_categ_selectableDirtyFlag())
            domain.setProductCategSelectable(model.getProduct_categ_selectable());
        if(model.get__last_updateDirtyFlag())
            domain.setLastUpdate(model.get__last_update());
        if(model.getWarehouse_idsDirtyFlag())
            domain.setWarehouseIds(model.getWarehouse_ids());
        if(model.getCreate_uid_textDirtyFlag())
            domain.setCreateUidText(model.getCreate_uid_text());
        if(model.getSupplied_wh_id_textDirtyFlag())
            domain.setSuppliedWhIdText(model.getSupplied_wh_id_text());
        if(model.getWrite_uid_textDirtyFlag())
            domain.setWriteUidText(model.getWrite_uid_text());
        if(model.getSupplier_wh_id_textDirtyFlag())
            domain.setSupplierWhIdText(model.getSupplier_wh_id_text());
        if(model.getCompany_id_textDirtyFlag())
            domain.setCompanyIdText(model.getCompany_id_text());
        if(model.getCompany_idDirtyFlag())
            domain.setCompanyId(model.getCompany_id());
        if(model.getCreate_uidDirtyFlag())
            domain.setCreateUid(model.getCreate_uid());
        if(model.getWrite_uidDirtyFlag())
            domain.setWriteUid(model.getWrite_uid());
        if(model.getSupplier_wh_idDirtyFlag())
            domain.setSupplierWhId(model.getSupplier_wh_id());
        if(model.getSupplied_wh_idDirtyFlag())
            domain.setSuppliedWhId(model.getSupplied_wh_id());
        return domain ;
    }

}

    



