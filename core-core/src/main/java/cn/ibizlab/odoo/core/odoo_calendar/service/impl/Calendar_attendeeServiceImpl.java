package cn.ibizlab.odoo.core.odoo_calendar.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_calendar.domain.Calendar_attendee;
import cn.ibizlab.odoo.core.odoo_calendar.filter.Calendar_attendeeSearchContext;
import cn.ibizlab.odoo.core.odoo_calendar.service.ICalendar_attendeeService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;



import cn.ibizlab.odoo.core.odoo_calendar.client.calendar_attendeeOdooClient;
import cn.ibizlab.odoo.core.odoo_calendar.clientmodel.calendar_attendeeClientModel;

/**
 * 实体[日历出席者信息] 服务对象接口实现
 */
@Slf4j
@Service
public class Calendar_attendeeServiceImpl implements ICalendar_attendeeService {

    @Autowired
    calendar_attendeeOdooClient calendar_attendeeOdooClient;


    @Override
    public boolean update(Calendar_attendee et) {
        calendar_attendeeClientModel clientModel = convert2Model(et,null);
		calendar_attendeeOdooClient.update(clientModel);
        Calendar_attendee rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }
    
    public void updateBatch(List<Calendar_attendee> list){
    }

    @Override
    public boolean remove(Integer id) {
        calendar_attendeeClientModel clientModel = new calendar_attendeeClientModel();
        clientModel.setId(id);
		calendar_attendeeOdooClient.remove(clientModel);
        return true; 
    }

    public void removeBatch(Collection<Integer> idList){
    }

    @Override
    public boolean create(Calendar_attendee et) {
        calendar_attendeeClientModel clientModel = convert2Model(et,null);
		calendar_attendeeOdooClient.create(clientModel);
        Calendar_attendee rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Calendar_attendee> list){
    }

    @Override
    public Calendar_attendee get(Integer id) {
        calendar_attendeeClientModel clientModel = new calendar_attendeeClientModel();
        clientModel.setId(id);
		calendar_attendeeOdooClient.get(clientModel);
        Calendar_attendee et = convert2Domain(clientModel,null);
        if(et==null){
            et=new Calendar_attendee();
            et.setId(id);
        }
        else{
        }

        return  et;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Calendar_attendee> searchDefault(Calendar_attendeeSearchContext context) {
        List<Calendar_attendee> list = new ArrayList<Calendar_attendee>();
        Page<calendar_attendeeClientModel> clientModelList = calendar_attendeeOdooClient.search(context);
        for(calendar_attendeeClientModel clientModel : clientModelList){
            list.add( this.convert2Domain(clientModel,null));
        }
        return new PageImpl<Calendar_attendee>(list, context.getPageable(), clientModelList.getTotalElements());
    }

    public calendar_attendeeClientModel convert2Model(Calendar_attendee domain , calendar_attendeeClientModel model) {
        if(domain == null)
            return model ;
        if(model==null){
            model = new calendar_attendeeClientModel();

        }
        if((Boolean) domain.getExtensionparams().get("dirtyflagenable")){
            if((Boolean) domain.getExtensionparams().get("display_namedirtyflag"))
                model.setDisplay_name(domain.getDisplayName());
            if((Boolean) domain.getExtensionparams().get("create_datedirtyflag"))
                model.setCreate_date(domain.getCreateDate());
            if((Boolean) domain.getExtensionparams().get("__last_updatedirtyflag"))
                model.set__last_update(domain.getLastUpdate());
            if((Boolean) domain.getExtensionparams().get("write_datedirtyflag"))
                model.setWrite_date(domain.getWriteDate());
            if((Boolean) domain.getExtensionparams().get("access_tokendirtyflag"))
                model.setAccess_token(domain.getAccessToken());
            if((Boolean) domain.getExtensionparams().get("iddirtyflag"))
                model.setId(domain.getId());
            if((Boolean) domain.getExtensionparams().get("statedirtyflag"))
                model.setState(domain.getState());
            if((Boolean) domain.getExtensionparams().get("common_namedirtyflag"))
                model.setCommon_name(domain.getCommonName());
            if((Boolean) domain.getExtensionparams().get("availabilitydirtyflag"))
                model.setAvailability(domain.getAvailability());
            if((Boolean) domain.getExtensionparams().get("emaildirtyflag"))
                model.setEmail(domain.getEmail());
            if((Boolean) domain.getExtensionparams().get("create_uid_textdirtyflag"))
                model.setCreate_uid_text(domain.getCreateUidText());
            if((Boolean) domain.getExtensionparams().get("partner_id_textdirtyflag"))
                model.setPartner_id_text(domain.getPartnerIdText());
            if((Boolean) domain.getExtensionparams().get("event_id_textdirtyflag"))
                model.setEvent_id_text(domain.getEventIdText());
            if((Boolean) domain.getExtensionparams().get("write_uid_textdirtyflag"))
                model.setWrite_uid_text(domain.getWriteUidText());
            if((Boolean) domain.getExtensionparams().get("create_uiddirtyflag"))
                model.setCreate_uid(domain.getCreateUid());
            if((Boolean) domain.getExtensionparams().get("write_uiddirtyflag"))
                model.setWrite_uid(domain.getWriteUid());
            if((Boolean) domain.getExtensionparams().get("event_iddirtyflag"))
                model.setEvent_id(domain.getEventId());
            if((Boolean) domain.getExtensionparams().get("partner_iddirtyflag"))
                model.setPartner_id(domain.getPartnerId());
            domain.getFocusNull().clear();
        }
        return model ;
    }

    public Calendar_attendee convert2Domain( calendar_attendeeClientModel model ,Calendar_attendee domain) {
        if(model == null)
            return domain ;
        if(domain == null){
            domain = new Calendar_attendee();
        }

        if(model.getDisplay_nameDirtyFlag())
            domain.setDisplayName(model.getDisplay_name());
        if(model.getCreate_dateDirtyFlag())
            domain.setCreateDate(model.getCreate_date());
        if(model.get__last_updateDirtyFlag())
            domain.setLastUpdate(model.get__last_update());
        if(model.getWrite_dateDirtyFlag())
            domain.setWriteDate(model.getWrite_date());
        if(model.getAccess_tokenDirtyFlag())
            domain.setAccessToken(model.getAccess_token());
        if(model.getIdDirtyFlag())
            domain.setId(model.getId());
        if(model.getStateDirtyFlag())
            domain.setState(model.getState());
        if(model.getCommon_nameDirtyFlag())
            domain.setCommonName(model.getCommon_name());
        if(model.getAvailabilityDirtyFlag())
            domain.setAvailability(model.getAvailability());
        if(model.getEmailDirtyFlag())
            domain.setEmail(model.getEmail());
        if(model.getCreate_uid_textDirtyFlag())
            domain.setCreateUidText(model.getCreate_uid_text());
        if(model.getPartner_id_textDirtyFlag())
            domain.setPartnerIdText(model.getPartner_id_text());
        if(model.getEvent_id_textDirtyFlag())
            domain.setEventIdText(model.getEvent_id_text());
        if(model.getWrite_uid_textDirtyFlag())
            domain.setWriteUidText(model.getWrite_uid_text());
        if(model.getCreate_uidDirtyFlag())
            domain.setCreateUid(model.getCreate_uid());
        if(model.getWrite_uidDirtyFlag())
            domain.setWriteUid(model.getWrite_uid());
        if(model.getEvent_idDirtyFlag())
            domain.setEventId(model.getEvent_id());
        if(model.getPartner_idDirtyFlag())
            domain.setPartnerId(model.getPartner_id());
        return domain ;
    }

}

    



