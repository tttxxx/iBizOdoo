package cn.ibizlab.odoo.core.client.model;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigDecimal;
import java.math.BigInteger;

/**
 * Client 实体 [event_mail] 对象
 */
public interface Ievent_mail {

    /**
     * 获取 [创建时间]
     */
    public void setCreate_date(Timestamp create_date);
    
    /**
     * 设置 [创建时间]
     */
    public Timestamp getCreate_date();

    /**
     * 获取 [创建时间]脏标记
     */
    public boolean getCreate_dateDirtyFlag();
    /**
     * 获取 [创建人]
     */
    public void setCreate_uid(Integer create_uid);
    
    /**
     * 设置 [创建人]
     */
    public Integer getCreate_uid();

    /**
     * 获取 [创建人]脏标记
     */
    public boolean getCreate_uidDirtyFlag();
    /**
     * 获取 [创建人]
     */
    public void setCreate_uid_text(String create_uid_text);
    
    /**
     * 设置 [创建人]
     */
    public String getCreate_uid_text();

    /**
     * 获取 [创建人]脏标记
     */
    public boolean getCreate_uid_textDirtyFlag();
    /**
     * 获取 [显示名称]
     */
    public void setDisplay_name(String display_name);
    
    /**
     * 设置 [显示名称]
     */
    public String getDisplay_name();

    /**
     * 获取 [显示名称]脏标记
     */
    public boolean getDisplay_nameDirtyFlag();
    /**
     * 获取 [已汇]
     */
    public void setDone(String done);
    
    /**
     * 设置 [已汇]
     */
    public String getDone();

    /**
     * 获取 [已汇]脏标记
     */
    public boolean getDoneDirtyFlag();
    /**
     * 获取 [活动]
     */
    public void setEvent_id(Integer event_id);
    
    /**
     * 设置 [活动]
     */
    public Integer getEvent_id();

    /**
     * 获取 [活动]脏标记
     */
    public boolean getEvent_idDirtyFlag();
    /**
     * 获取 [活动]
     */
    public void setEvent_id_text(String event_id_text);
    
    /**
     * 设置 [活动]
     */
    public String getEvent_id_text();

    /**
     * 获取 [活动]脏标记
     */
    public boolean getEvent_id_textDirtyFlag();
    /**
     * 获取 [ID]
     */
    public void setId(Integer id);
    
    /**
     * 设置 [ID]
     */
    public Integer getId();

    /**
     * 获取 [ID]脏标记
     */
    public boolean getIdDirtyFlag();
    /**
     * 获取 [间隔]
     */
    public void setInterval_nbr(Integer interval_nbr);
    
    /**
     * 设置 [间隔]
     */
    public Integer getInterval_nbr();

    /**
     * 获取 [间隔]脏标记
     */
    public boolean getInterval_nbrDirtyFlag();
    /**
     * 获取 [触发器]
     */
    public void setInterval_type(String interval_type);
    
    /**
     * 设置 [触发器]
     */
    public String getInterval_type();

    /**
     * 获取 [触发器]脏标记
     */
    public boolean getInterval_typeDirtyFlag();
    /**
     * 获取 [单位]
     */
    public void setInterval_unit(String interval_unit);
    
    /**
     * 设置 [单位]
     */
    public String getInterval_unit();

    /**
     * 获取 [单位]脏标记
     */
    public boolean getInterval_unitDirtyFlag();
    /**
     * 获取 [邮箱注册]
     */
    public void setMail_registration_ids(String mail_registration_ids);
    
    /**
     * 设置 [邮箱注册]
     */
    public String getMail_registration_ids();

    /**
     * 获取 [邮箱注册]脏标记
     */
    public boolean getMail_registration_idsDirtyFlag();
    /**
     * 获取 [在事件上发送EMail]
     */
    public void setMail_sent(String mail_sent);
    
    /**
     * 设置 [在事件上发送EMail]
     */
    public String getMail_sent();

    /**
     * 获取 [在事件上发送EMail]脏标记
     */
    public boolean getMail_sentDirtyFlag();
    /**
     * 获取 [计划发出的邮件]
     */
    public void setScheduled_date(Timestamp scheduled_date);
    
    /**
     * 设置 [计划发出的邮件]
     */
    public Timestamp getScheduled_date();

    /**
     * 获取 [计划发出的邮件]脏标记
     */
    public boolean getScheduled_dateDirtyFlag();
    /**
     * 获取 [现实顺序]
     */
    public void setSequence(Integer sequence);
    
    /**
     * 设置 [现实顺序]
     */
    public Integer getSequence();

    /**
     * 获取 [现实顺序]脏标记
     */
    public boolean getSequenceDirtyFlag();
    /**
     * 获取 [EMail模板]
     */
    public void setTemplate_id(Integer template_id);
    
    /**
     * 设置 [EMail模板]
     */
    public Integer getTemplate_id();

    /**
     * 获取 [EMail模板]脏标记
     */
    public boolean getTemplate_idDirtyFlag();
    /**
     * 获取 [EMail模板]
     */
    public void setTemplate_id_text(String template_id_text);
    
    /**
     * 设置 [EMail模板]
     */
    public String getTemplate_id_text();

    /**
     * 获取 [EMail模板]脏标记
     */
    public boolean getTemplate_id_textDirtyFlag();
    /**
     * 获取 [最后更新时间]
     */
    public void setWrite_date(Timestamp write_date);
    
    /**
     * 设置 [最后更新时间]
     */
    public Timestamp getWrite_date();

    /**
     * 获取 [最后更新时间]脏标记
     */
    public boolean getWrite_dateDirtyFlag();
    /**
     * 获取 [最后更新人]
     */
    public void setWrite_uid(Integer write_uid);
    
    /**
     * 设置 [最后更新人]
     */
    public Integer getWrite_uid();

    /**
     * 获取 [最后更新人]脏标记
     */
    public boolean getWrite_uidDirtyFlag();
    /**
     * 获取 [最后更新人]
     */
    public void setWrite_uid_text(String write_uid_text);
    
    /**
     * 设置 [最后更新人]
     */
    public String getWrite_uid_text();

    /**
     * 获取 [最后更新人]脏标记
     */
    public boolean getWrite_uid_textDirtyFlag();
    /**
     * 获取 [最后修改日]
     */
    public void set__last_update(Timestamp __last_update);
    
    /**
     * 设置 [最后修改日]
     */
    public Timestamp get__last_update();

    /**
     * 获取 [最后修改日]脏标记
     */
    public boolean get__last_updateDirtyFlag();


    /**
     *  转换为Map
     */
    public Map<String, Object> toMap() throws Exception ;

    /**
     *  从Map构建
     */
   public void fromMap(Map<String, Object> map) throws Exception;
}
