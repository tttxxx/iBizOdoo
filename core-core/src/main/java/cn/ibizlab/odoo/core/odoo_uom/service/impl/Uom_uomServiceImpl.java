package cn.ibizlab.odoo.core.odoo_uom.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_uom.domain.Uom_uom;
import cn.ibizlab.odoo.core.odoo_uom.filter.Uom_uomSearchContext;
import cn.ibizlab.odoo.core.odoo_uom.service.IUom_uomService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;



import cn.ibizlab.odoo.core.odoo_uom.client.uom_uomOdooClient;
import cn.ibizlab.odoo.core.odoo_uom.clientmodel.uom_uomClientModel;

/**
 * 实体[产品计量单位] 服务对象接口实现
 */
@Slf4j
@Service
public class Uom_uomServiceImpl implements IUom_uomService {

    @Autowired
    uom_uomOdooClient uom_uomOdooClient;


    @Override
    public boolean remove(Integer id) {
        uom_uomClientModel clientModel = new uom_uomClientModel();
        clientModel.setId(id);
		uom_uomOdooClient.remove(clientModel);
        return true; 
    }

    public void removeBatch(Collection<Integer> idList){
    }

    @Override
    public boolean update(Uom_uom et) {
        uom_uomClientModel clientModel = convert2Model(et,null);
		uom_uomOdooClient.update(clientModel);
        Uom_uom rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }
    
    public void updateBatch(List<Uom_uom> list){
    }

    @Override
    public Uom_uom get(Integer id) {
        uom_uomClientModel clientModel = new uom_uomClientModel();
        clientModel.setId(id);
		uom_uomOdooClient.get(clientModel);
        Uom_uom et = convert2Domain(clientModel,null);
        if(et==null){
            et=new Uom_uom();
            et.setId(id);
        }
        else{
        }

        return  et;
    }

    @Override
    public boolean create(Uom_uom et) {
        uom_uomClientModel clientModel = convert2Model(et,null);
		uom_uomOdooClient.create(clientModel);
        Uom_uom rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Uom_uom> list){
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Uom_uom> searchDefault(Uom_uomSearchContext context) {
        List<Uom_uom> list = new ArrayList<Uom_uom>();
        Page<uom_uomClientModel> clientModelList = uom_uomOdooClient.search(context);
        for(uom_uomClientModel clientModel : clientModelList){
            list.add( this.convert2Domain(clientModel,null));
        }
        return new PageImpl<Uom_uom>(list, context.getPageable(), clientModelList.getTotalElements());
    }

    public uom_uomClientModel convert2Model(Uom_uom domain , uom_uomClientModel model) {
        if(domain == null)
            return model ;
        if(model==null){
            model = new uom_uomClientModel();

        }
        if((Boolean) domain.getExtensionparams().get("dirtyflagenable")){
            if((Boolean) domain.getExtensionparams().get("namedirtyflag"))
                model.setName(domain.getName());
            if((Boolean) domain.getExtensionparams().get("write_datedirtyflag"))
                model.setWrite_date(domain.getWriteDate());
            if((Boolean) domain.getExtensionparams().get("factor_invdirtyflag"))
                model.setFactor_inv(domain.getFactorInv());
            if((Boolean) domain.getExtensionparams().get("activedirtyflag"))
                model.setActive(domain.getActive());
            if((Boolean) domain.getExtensionparams().get("__last_updatedirtyflag"))
                model.set__last_update(domain.getLastUpdate());
            if((Boolean) domain.getExtensionparams().get("roundingdirtyflag"))
                model.setRounding(domain.getRounding());
            if((Boolean) domain.getExtensionparams().get("display_namedirtyflag"))
                model.setDisplay_name(domain.getDisplayName());
            if((Boolean) domain.getExtensionparams().get("factordirtyflag"))
                model.setFactor(domain.getFactor());
            if((Boolean) domain.getExtensionparams().get("iddirtyflag"))
                model.setId(domain.getId());
            if((Boolean) domain.getExtensionparams().get("create_datedirtyflag"))
                model.setCreate_date(domain.getCreateDate());
            if((Boolean) domain.getExtensionparams().get("uom_typedirtyflag"))
                model.setUom_type(domain.getUomType());
            if((Boolean) domain.getExtensionparams().get("write_uid_textdirtyflag"))
                model.setWrite_uid_text(domain.getWriteUidText());
            if((Boolean) domain.getExtensionparams().get("category_id_textdirtyflag"))
                model.setCategory_id_text(domain.getCategoryIdText());
            if((Boolean) domain.getExtensionparams().get("is_pos_groupabledirtyflag"))
                model.setIs_pos_groupable(domain.getIsPosGroupable());
            if((Boolean) domain.getExtensionparams().get("create_uid_textdirtyflag"))
                model.setCreate_uid_text(domain.getCreateUidText());
            if((Boolean) domain.getExtensionparams().get("measure_typedirtyflag"))
                model.setMeasure_type(domain.getMeasureType());
            if((Boolean) domain.getExtensionparams().get("create_uiddirtyflag"))
                model.setCreate_uid(domain.getCreateUid());
            if((Boolean) domain.getExtensionparams().get("category_iddirtyflag"))
                model.setCategory_id(domain.getCategoryId());
            if((Boolean) domain.getExtensionparams().get("write_uiddirtyflag"))
                model.setWrite_uid(domain.getWriteUid());
            domain.getFocusNull().clear();
        }
        return model ;
    }

    public Uom_uom convert2Domain( uom_uomClientModel model ,Uom_uom domain) {
        if(model == null)
            return domain ;
        if(domain == null){
            domain = new Uom_uom();
        }

        if(model.getNameDirtyFlag())
            domain.setName(model.getName());
        if(model.getWrite_dateDirtyFlag())
            domain.setWriteDate(model.getWrite_date());
        if(model.getFactor_invDirtyFlag())
            domain.setFactorInv(model.getFactor_inv());
        if(model.getActiveDirtyFlag())
            domain.setActive(model.getActive());
        if(model.get__last_updateDirtyFlag())
            domain.setLastUpdate(model.get__last_update());
        if(model.getRoundingDirtyFlag())
            domain.setRounding(model.getRounding());
        if(model.getDisplay_nameDirtyFlag())
            domain.setDisplayName(model.getDisplay_name());
        if(model.getFactorDirtyFlag())
            domain.setFactor(model.getFactor());
        if(model.getIdDirtyFlag())
            domain.setId(model.getId());
        if(model.getCreate_dateDirtyFlag())
            domain.setCreateDate(model.getCreate_date());
        if(model.getUom_typeDirtyFlag())
            domain.setUomType(model.getUom_type());
        if(model.getWrite_uid_textDirtyFlag())
            domain.setWriteUidText(model.getWrite_uid_text());
        if(model.getCategory_id_textDirtyFlag())
            domain.setCategoryIdText(model.getCategory_id_text());
        if(model.getIs_pos_groupableDirtyFlag())
            domain.setIsPosGroupable(model.getIs_pos_groupable());
        if(model.getCreate_uid_textDirtyFlag())
            domain.setCreateUidText(model.getCreate_uid_text());
        if(model.getMeasure_typeDirtyFlag())
            domain.setMeasureType(model.getMeasure_type());
        if(model.getCreate_uidDirtyFlag())
            domain.setCreateUid(model.getCreate_uid());
        if(model.getCategory_idDirtyFlag())
            domain.setCategoryId(model.getCategory_id());
        if(model.getWrite_uidDirtyFlag())
            domain.setWriteUid(model.getWrite_uid());
        return domain ;
    }

}

    



