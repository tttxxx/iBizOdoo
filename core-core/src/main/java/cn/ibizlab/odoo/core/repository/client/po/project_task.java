package cn.ibizlab.odoo.core.repository.client.po;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigDecimal;

/**
 * [project_task] 对象
 */
public interface project_task {

    public String getAccess_token();

    public void setAccess_token(String access_token);

    public String getAccess_url();

    public void setAccess_url(String access_url);

    public String getAccess_warning();

    public void setAccess_warning(String access_warning);

    public String getActive();

    public void setActive(String active);

    public Timestamp getActivity_date_deadline();

    public void setActivity_date_deadline(Timestamp activity_date_deadline);

    public String getActivity_ids();

    public void setActivity_ids(String activity_ids);

    public String getActivity_state();

    public void setActivity_state(String activity_state);

    public String getActivity_summary();

    public void setActivity_summary(String activity_summary);

    public Integer getActivity_type_id();

    public void setActivity_type_id(Integer activity_type_id);

    public String getActivity_type_id_text();

    public void setActivity_type_id_text(String activity_type_id_text);

    public Integer getActivity_user_id();

    public void setActivity_user_id(Integer activity_user_id);

    public String getActivity_user_id_text();

    public void setActivity_user_id_text(String activity_user_id_text);

    public String getAttachment_ids();

    public void setAttachment_ids(String attachment_ids);

    public String getChild_ids();

    public void setChild_ids(String child_ids);

    public Integer getColor();

    public void setColor(Integer color);

    public Integer getCompany_id();

    public void setCompany_id(Integer company_id);

    public String getCompany_id_text();

    public void setCompany_id_text(String company_id_text);

    public Timestamp getCreate_date();

    public void setCreate_date(Timestamp create_date);

    public Integer getCreate_uid();

    public void setCreate_uid(Integer create_uid);

    public String getCreate_uid_text();

    public void setCreate_uid_text(String create_uid_text);

    public Timestamp getDate_assign();

    public void setDate_assign(Timestamp date_assign);

    public Timestamp getDate_deadline();

    public void setDate_deadline(Timestamp date_deadline);

    public Timestamp getDate_end();

    public void setDate_end(Timestamp date_end);

    public Timestamp getDate_last_stage_update();

    public void setDate_last_stage_update(Timestamp date_last_stage_update);

    public Timestamp getDate_start();

    public void setDate_start(Timestamp date_start);

    public String getDescription();

    public void setDescription(String description);

    public String getDisplay_name();

    public void setDisplay_name(String display_name);

    public String getEmail_cc();

    public void setEmail_cc(String email_cc);

    public String getEmail_from();

    public void setEmail_from(String email_from);

    public Integer getId();

    public void setId(Integer id);

    public String getKanban_state();

    public void setKanban_state(String kanban_state);

    public String getKanban_state_label();

    public void setKanban_state_label(String kanban_state_label);

    public String getLegend_blocked();

    public void setLegend_blocked(String legend_blocked);

    public String getLegend_done();

    public void setLegend_done(String legend_done);

    public String getLegend_normal();

    public void setLegend_normal(String legend_normal);

    public Integer getManager_id();

    public void setManager_id(Integer manager_id);

    public String getManager_id_text();

    public void setManager_id_text(String manager_id_text);

    public Integer getMessage_attachment_count();

    public void setMessage_attachment_count(Integer message_attachment_count);

    public String getMessage_channel_ids();

    public void setMessage_channel_ids(String message_channel_ids);

    public String getMessage_follower_ids();

    public void setMessage_follower_ids(String message_follower_ids);

    public String getMessage_has_error();

    public void setMessage_has_error(String message_has_error);

    public Integer getMessage_has_error_counter();

    public void setMessage_has_error_counter(Integer message_has_error_counter);

    public String getMessage_ids();

    public void setMessage_ids(String message_ids);

    public String getMessage_is_follower();

    public void setMessage_is_follower(String message_is_follower);

    public String getMessage_needaction();

    public void setMessage_needaction(String message_needaction);

    public Integer getMessage_needaction_counter();

    public void setMessage_needaction_counter(Integer message_needaction_counter);

    public String getMessage_partner_ids();

    public void setMessage_partner_ids(String message_partner_ids);

    public String getMessage_unread();

    public void setMessage_unread(String message_unread);

    public Integer getMessage_unread_counter();

    public void setMessage_unread_counter(Integer message_unread_counter);

    public String getName();

    public void setName(String name);

    public String getNotes();

    public void setNotes(String notes);

    public Integer getParent_id();

    public void setParent_id(Integer parent_id);

    public String getParent_id_text();

    public void setParent_id_text(String parent_id_text);

    public Integer getPartner_id();

    public void setPartner_id(Integer partner_id);

    public String getPartner_id_text();

    public void setPartner_id_text(String partner_id_text);

    public Double getPlanned_hours();

    public void setPlanned_hours(Double planned_hours);

    public String getPriority();

    public void setPriority(String priority);

    public Integer getProject_id();

    public void setProject_id(Integer project_id);

    public String getProject_id_text();

    public void setProject_id_text(String project_id_text);

    public Integer getRating_count();

    public void setRating_count(Integer rating_count);

    public String getRating_ids();

    public void setRating_ids(String rating_ids);

    public String getRating_last_feedback();

    public void setRating_last_feedback(String rating_last_feedback);

    public byte[] getRating_last_image();

    public void setRating_last_image(byte[] rating_last_image);

    public Double getRating_last_value();

    public void setRating_last_value(Double rating_last_value);

    public Integer getSequence();

    public void setSequence(Integer sequence);

    public Integer getStage_id();

    public void setStage_id(Integer stage_id);

    public String getStage_id_text();

    public void setStage_id_text(String stage_id_text);

    public Integer getSubtask_count();

    public void setSubtask_count(Integer subtask_count);

    public Double getSubtask_planned_hours();

    public void setSubtask_planned_hours(Double subtask_planned_hours);

    public Integer getSubtask_project_id();

    public void setSubtask_project_id(Integer subtask_project_id);

    public String getSubtask_project_id_text();

    public void setSubtask_project_id_text(String subtask_project_id_text);

    public String getTag_ids();

    public void setTag_ids(String tag_ids);

    public String getUser_email();

    public void setUser_email(String user_email);

    public Integer getUser_id();

    public void setUser_id(Integer user_id);

    public String getUser_id_text();

    public void setUser_id_text(String user_id_text);

    public String getWebsite_message_ids();

    public void setWebsite_message_ids(String website_message_ids);

    public Double getWorking_days_close();

    public void setWorking_days_close(Double working_days_close);

    public Double getWorking_days_open();

    public void setWorking_days_open(Double working_days_open);

    public Double getWorking_hours_close();

    public void setWorking_hours_close(Double working_hours_close);

    public Double getWorking_hours_open();

    public void setWorking_hours_open(Double working_hours_open);

    public Timestamp getWrite_date();

    public void setWrite_date(Timestamp write_date);

    public Integer getWrite_uid();

    public void setWrite_uid(Integer write_uid);

    public String getWrite_uid_text();

    public void setWrite_uid_text(String write_uid_text);

    public Timestamp get__last_update();

    public void set__last_update(Timestamp __last_update);

}
