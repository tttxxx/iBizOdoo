package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Mrp_workcenter;
import cn.ibizlab.odoo.core.odoo_mrp.filter.Mrp_workcenterSearchContext;

/**
 * 实体 [工作中心] 存储对象
 */
public interface Mrp_workcenterRepository extends Repository<Mrp_workcenter> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Mrp_workcenter> searchDefault(Mrp_workcenterSearchContext context);

    Mrp_workcenter convert2PO(cn.ibizlab.odoo.core.odoo_mrp.domain.Mrp_workcenter domain , Mrp_workcenter po) ;

    cn.ibizlab.odoo.core.odoo_mrp.domain.Mrp_workcenter convert2Domain( Mrp_workcenter po ,cn.ibizlab.odoo.core.odoo_mrp.domain.Mrp_workcenter domain) ;

}
