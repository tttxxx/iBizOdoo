package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Imail_blacklist;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[mail_blacklist] 服务对象接口
 */
public interface Imail_blacklistClientService{

    public Imail_blacklist createModel() ;

    public void createBatch(List<Imail_blacklist> mail_blacklists);

    public void get(Imail_blacklist mail_blacklist);

    public void removeBatch(List<Imail_blacklist> mail_blacklists);

    public void remove(Imail_blacklist mail_blacklist);

    public void update(Imail_blacklist mail_blacklist);

    public void create(Imail_blacklist mail_blacklist);

    public void updateBatch(List<Imail_blacklist> mail_blacklists);

    public Page<Imail_blacklist> search(SearchContext context);

    public Page<Imail_blacklist> select(SearchContext context);

}
