package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Crm_partner_binding;
import cn.ibizlab.odoo.core.odoo_crm.filter.Crm_partner_bindingSearchContext;

/**
 * 实体 [在CRM向导中处理业务伙伴的绑定或生成。] 存储对象
 */
public interface Crm_partner_bindingRepository extends Repository<Crm_partner_binding> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Crm_partner_binding> searchDefault(Crm_partner_bindingSearchContext context);

    Crm_partner_binding convert2PO(cn.ibizlab.odoo.core.odoo_crm.domain.Crm_partner_binding domain , Crm_partner_binding po) ;

    cn.ibizlab.odoo.core.odoo_crm.domain.Crm_partner_binding convert2Domain( Crm_partner_binding po ,cn.ibizlab.odoo.core.odoo_crm.domain.Crm_partner_binding domain) ;

}
