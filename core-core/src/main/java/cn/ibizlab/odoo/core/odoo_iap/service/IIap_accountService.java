package cn.ibizlab.odoo.core.odoo_iap.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_iap.domain.Iap_account;
import cn.ibizlab.odoo.core.odoo_iap.filter.Iap_accountSearchContext;


/**
 * 实体[Iap_account] 服务对象接口
 */
public interface IIap_accountService{

    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    Iap_account get(Integer key) ;
    boolean update(Iap_account et) ;
    void updateBatch(List<Iap_account> list) ;
    boolean create(Iap_account et) ;
    void createBatch(List<Iap_account> list) ;
    Page<Iap_account> searchDefault(Iap_accountSearchContext context) ;

}



