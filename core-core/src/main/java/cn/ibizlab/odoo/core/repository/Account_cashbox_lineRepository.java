package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Account_cashbox_line;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_cashbox_lineSearchContext;

/**
 * 实体 [钱箱明细] 存储对象
 */
public interface Account_cashbox_lineRepository extends Repository<Account_cashbox_line> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Account_cashbox_line> searchDefault(Account_cashbox_lineSearchContext context);

    Account_cashbox_line convert2PO(cn.ibizlab.odoo.core.odoo_account.domain.Account_cashbox_line domain , Account_cashbox_line po) ;

    cn.ibizlab.odoo.core.odoo_account.domain.Account_cashbox_line convert2Domain( Account_cashbox_line po ,cn.ibizlab.odoo.core.odoo_account.domain.Account_cashbox_line domain) ;

}
