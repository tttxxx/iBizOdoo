package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Stock_quant;
import cn.ibizlab.odoo.core.odoo_stock.filter.Stock_quantSearchContext;

/**
 * 实体 [即时库存] 存储对象
 */
public interface Stock_quantRepository extends Repository<Stock_quant> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Stock_quant> searchDefault(Stock_quantSearchContext context);

    Stock_quant convert2PO(cn.ibizlab.odoo.core.odoo_stock.domain.Stock_quant domain , Stock_quant po) ;

    cn.ibizlab.odoo.core.odoo_stock.domain.Stock_quant convert2Domain( Stock_quant po ,cn.ibizlab.odoo.core.odoo_stock.domain.Stock_quant domain) ;

}
