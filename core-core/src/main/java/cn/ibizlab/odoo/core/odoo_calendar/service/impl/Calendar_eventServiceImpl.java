package cn.ibizlab.odoo.core.odoo_calendar.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_calendar.domain.Calendar_event;
import cn.ibizlab.odoo.core.odoo_calendar.filter.Calendar_eventSearchContext;
import cn.ibizlab.odoo.core.odoo_calendar.service.ICalendar_eventService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;



import cn.ibizlab.odoo.core.odoo_calendar.client.calendar_eventOdooClient;
import cn.ibizlab.odoo.core.odoo_calendar.clientmodel.calendar_eventClientModel;

/**
 * 实体[活动] 服务对象接口实现
 */
@Slf4j
@Service
public class Calendar_eventServiceImpl implements ICalendar_eventService {

    @Autowired
    calendar_eventOdooClient calendar_eventOdooClient;


    @Override
    public boolean update(Calendar_event et) {
        calendar_eventClientModel clientModel = convert2Model(et,null);
		calendar_eventOdooClient.update(clientModel);
        Calendar_event rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }
    
    public void updateBatch(List<Calendar_event> list){
    }

    @Override
    public Calendar_event get(Integer id) {
        calendar_eventClientModel clientModel = new calendar_eventClientModel();
        clientModel.setId(id);
		calendar_eventOdooClient.get(clientModel);
        Calendar_event et = convert2Domain(clientModel,null);
        if(et==null){
            et=new Calendar_event();
            et.setId(id);
        }
        else{
        }

        return  et;
    }

    @Override
    public boolean remove(Integer id) {
        calendar_eventClientModel clientModel = new calendar_eventClientModel();
        clientModel.setId(id);
		calendar_eventOdooClient.remove(clientModel);
        return true; 
    }

    public void removeBatch(Collection<Integer> idList){
    }

    @Override
    public boolean create(Calendar_event et) {
        calendar_eventClientModel clientModel = convert2Model(et,null);
		calendar_eventOdooClient.create(clientModel);
        Calendar_event rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Calendar_event> list){
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Calendar_event> searchDefault(Calendar_eventSearchContext context) {
        List<Calendar_event> list = new ArrayList<Calendar_event>();
        Page<calendar_eventClientModel> clientModelList = calendar_eventOdooClient.search(context);
        for(calendar_eventClientModel clientModel : clientModelList){
            list.add( this.convert2Domain(clientModel,null));
        }
        return new PageImpl<Calendar_event>(list, context.getPageable(), clientModelList.getTotalElements());
    }

    public calendar_eventClientModel convert2Model(Calendar_event domain , calendar_eventClientModel model) {
        if(domain == null)
            return model ;
        if(model==null){
            model = new calendar_eventClientModel();

        }
        if((Boolean) domain.getExtensionparams().get("dirtyflagenable")){
            if((Boolean) domain.getExtensionparams().get("locationdirtyflag"))
                model.setLocation(domain.getLocation());
            if((Boolean) domain.getExtensionparams().get("message_main_attachment_iddirtyflag"))
                model.setMessage_main_attachment_id(domain.getMessageMainAttachmentId());
            if((Boolean) domain.getExtensionparams().get("descriptiondirtyflag"))
                model.setDescription(domain.getDescription());
            if((Boolean) domain.getExtensionparams().get("recurrent_id_datedirtyflag"))
                model.setRecurrent_id_date(domain.getRecurrentIdDate());
            if((Boolean) domain.getExtensionparams().get("sadirtyflag"))
                model.setSa(domain.getSa());
            if((Boolean) domain.getExtensionparams().get("display_namedirtyflag"))
                model.setDisplay_name(domain.getDisplayName());
            if((Boolean) domain.getExtensionparams().get("recurrencydirtyflag"))
                model.setRecurrency(domain.getRecurrency());
            if((Boolean) domain.getExtensionparams().get("countdirtyflag"))
                model.setCount(domain.getCount());
            if((Boolean) domain.getExtensionparams().get("frdirtyflag"))
                model.setFr(domain.getFr());
            if((Boolean) domain.getExtensionparams().get("rruledirtyflag"))
                model.setRrule(domain.getRrule());
            if((Boolean) domain.getExtensionparams().get("month_bydirtyflag"))
                model.setMonth_by(domain.getMonthBy());
            if((Boolean) domain.getExtensionparams().get("week_listdirtyflag"))
                model.setWeek_list(domain.getWeekList());
            if((Boolean) domain.getExtensionparams().get("message_unreaddirtyflag"))
                model.setMessage_unread(domain.getMessageUnread());
            if((Boolean) domain.getExtensionparams().get("stopdirtyflag"))
                model.setStop(domain.getStop());
            if((Boolean) domain.getExtensionparams().get("res_iddirtyflag"))
                model.setRes_id(domain.getResId());
            if((Boolean) domain.getExtensionparams().get("message_has_error_counterdirtyflag"))
                model.setMessage_has_error_counter(domain.getMessageHasErrorCounter());
            if((Boolean) domain.getExtensionparams().get("recurrent_iddirtyflag"))
                model.setRecurrent_id(domain.getRecurrentId());
            if((Boolean) domain.getExtensionparams().get("message_has_errordirtyflag"))
                model.setMessage_has_error(domain.getMessageHasError());
            if((Boolean) domain.getExtensionparams().get("end_typedirtyflag"))
                model.setEnd_type(domain.getEndType());
            if((Boolean) domain.getExtensionparams().get("attendee_idsdirtyflag"))
                model.setAttendee_ids(domain.getAttendeeIds());
            if((Boolean) domain.getExtensionparams().get("attendee_statusdirtyflag"))
                model.setAttendee_status(domain.getAttendeeStatus());
            if((Boolean) domain.getExtensionparams().get("activedirtyflag"))
                model.setActive(domain.getActive());
            if((Boolean) domain.getExtensionparams().get("rrule_typedirtyflag"))
                model.setRrule_type(domain.getRruleType());
            if((Boolean) domain.getExtensionparams().get("intervaldirtyflag"))
                model.setInterval(domain.getInterval());
            if((Boolean) domain.getExtensionparams().get("privacydirtyflag"))
                model.setPrivacy(domain.getPrivacy());
            if((Boolean) domain.getExtensionparams().get("durationdirtyflag"))
                model.setDuration(domain.getDuration());
            if((Boolean) domain.getExtensionparams().get("start_datetimedirtyflag"))
                model.setStart_datetime(domain.getStartDatetime());
            if((Boolean) domain.getExtensionparams().get("is_attendeedirtyflag"))
                model.setIs_attendee(domain.getIsAttendee());
            if((Boolean) domain.getExtensionparams().get("start_datedirtyflag"))
                model.setStart_date(domain.getStartDate());
            if((Boolean) domain.getExtensionparams().get("modirtyflag"))
                model.setMo(domain.getMo());
            if((Boolean) domain.getExtensionparams().get("statedirtyflag"))
                model.setState(domain.getState());
            if((Boolean) domain.getExtensionparams().get("__last_updatedirtyflag"))
                model.set__last_update(domain.getLastUpdate());
            if((Boolean) domain.getExtensionparams().get("wedirtyflag"))
                model.setWe(domain.getWe());
            if((Boolean) domain.getExtensionparams().get("message_partner_idsdirtyflag"))
                model.setMessage_partner_ids(domain.getMessagePartnerIds());
            if((Boolean) domain.getExtensionparams().get("display_timedirtyflag"))
                model.setDisplay_time(domain.getDisplayTime());
            if((Boolean) domain.getExtensionparams().get("display_startdirtyflag"))
                model.setDisplay_start(domain.getDisplayStart());
            if((Boolean) domain.getExtensionparams().get("stop_datedirtyflag"))
                model.setStop_date(domain.getStopDate());
            if((Boolean) domain.getExtensionparams().get("message_needactiondirtyflag"))
                model.setMessage_needaction(domain.getMessageNeedaction());
            if((Boolean) domain.getExtensionparams().get("message_unread_counterdirtyflag"))
                model.setMessage_unread_counter(domain.getMessageUnreadCounter());
            if((Boolean) domain.getExtensionparams().get("final_datedirtyflag"))
                model.setFinal_date(domain.getFinalDate());
            if((Boolean) domain.getExtensionparams().get("res_model_iddirtyflag"))
                model.setRes_model_id(domain.getResModelId());
            if((Boolean) domain.getExtensionparams().get("is_highlighteddirtyflag"))
                model.setIs_highlighted(domain.getIsHighlighted());
            if((Boolean) domain.getExtensionparams().get("iddirtyflag"))
                model.setId(domain.getId());
            if((Boolean) domain.getExtensionparams().get("message_channel_idsdirtyflag"))
                model.setMessage_channel_ids(domain.getMessageChannelIds());
            if((Boolean) domain.getExtensionparams().get("write_datedirtyflag"))
                model.setWrite_date(domain.getWriteDate());
            if((Boolean) domain.getExtensionparams().get("stop_datetimedirtyflag"))
                model.setStop_datetime(domain.getStopDatetime());
            if((Boolean) domain.getExtensionparams().get("activity_idsdirtyflag"))
                model.setActivity_ids(domain.getActivityIds());
            if((Boolean) domain.getExtensionparams().get("res_modeldirtyflag"))
                model.setRes_model(domain.getResModel());
            if((Boolean) domain.getExtensionparams().get("tudirtyflag"))
                model.setTu(domain.getTu());
            if((Boolean) domain.getExtensionparams().get("create_datedirtyflag"))
                model.setCreate_date(domain.getCreateDate());
            if((Boolean) domain.getExtensionparams().get("show_asdirtyflag"))
                model.setShow_as(domain.getShowAs());
            if((Boolean) domain.getExtensionparams().get("categ_idsdirtyflag"))
                model.setCateg_ids(domain.getCategIds());
            if((Boolean) domain.getExtensionparams().get("alarm_idsdirtyflag"))
                model.setAlarm_ids(domain.getAlarmIds());
            if((Boolean) domain.getExtensionparams().get("thdirtyflag"))
                model.setTh(domain.getTh());
            if((Boolean) domain.getExtensionparams().get("message_needaction_counterdirtyflag"))
                model.setMessage_needaction_counter(domain.getMessageNeedactionCounter());
            if((Boolean) domain.getExtensionparams().get("bydaydirtyflag"))
                model.setByday(domain.getByday());
            if((Boolean) domain.getExtensionparams().get("daydirtyflag"))
                model.setDay(domain.getDay());
            if((Boolean) domain.getExtensionparams().get("startdirtyflag"))
                model.setStart(domain.getStart());
            if((Boolean) domain.getExtensionparams().get("message_attachment_countdirtyflag"))
                model.setMessage_attachment_count(domain.getMessageAttachmentCount());
            if((Boolean) domain.getExtensionparams().get("partner_idsdirtyflag"))
                model.setPartner_ids(domain.getPartnerIds());
            if((Boolean) domain.getExtensionparams().get("alldaydirtyflag"))
                model.setAllday(domain.getAllday());
            if((Boolean) domain.getExtensionparams().get("message_idsdirtyflag"))
                model.setMessage_ids(domain.getMessageIds());
            if((Boolean) domain.getExtensionparams().get("website_message_idsdirtyflag"))
                model.setWebsite_message_ids(domain.getWebsiteMessageIds());
            if((Boolean) domain.getExtensionparams().get("message_follower_idsdirtyflag"))
                model.setMessage_follower_ids(domain.getMessageFollowerIds());
            if((Boolean) domain.getExtensionparams().get("sudirtyflag"))
                model.setSu(domain.getSu());
            if((Boolean) domain.getExtensionparams().get("message_is_followerdirtyflag"))
                model.setMessage_is_follower(domain.getMessageIsFollower());
            if((Boolean) domain.getExtensionparams().get("namedirtyflag"))
                model.setName(domain.getName());
            if((Boolean) domain.getExtensionparams().get("opportunity_id_textdirtyflag"))
                model.setOpportunity_id_text(domain.getOpportunityIdText());
            if((Boolean) domain.getExtensionparams().get("create_uid_textdirtyflag"))
                model.setCreate_uid_text(domain.getCreateUidText());
            if((Boolean) domain.getExtensionparams().get("write_uid_textdirtyflag"))
                model.setWrite_uid_text(domain.getWriteUidText());
            if((Boolean) domain.getExtensionparams().get("user_id_textdirtyflag"))
                model.setUser_id_text(domain.getUserIdText());
            if((Boolean) domain.getExtensionparams().get("partner_iddirtyflag"))
                model.setPartner_id(domain.getPartnerId());
            if((Boolean) domain.getExtensionparams().get("applicant_id_textdirtyflag"))
                model.setApplicant_id_text(domain.getApplicantIdText());
            if((Boolean) domain.getExtensionparams().get("create_uiddirtyflag"))
                model.setCreate_uid(domain.getCreateUid());
            if((Boolean) domain.getExtensionparams().get("applicant_iddirtyflag"))
                model.setApplicant_id(domain.getApplicantId());
            if((Boolean) domain.getExtensionparams().get("write_uiddirtyflag"))
                model.setWrite_uid(domain.getWriteUid());
            if((Boolean) domain.getExtensionparams().get("user_iddirtyflag"))
                model.setUser_id(domain.getUserId());
            if((Boolean) domain.getExtensionparams().get("opportunity_iddirtyflag"))
                model.setOpportunity_id(domain.getOpportunityId());
            domain.getFocusNull().clear();
        }
        return model ;
    }

    public Calendar_event convert2Domain( calendar_eventClientModel model ,Calendar_event domain) {
        if(model == null)
            return domain ;
        if(domain == null){
            domain = new Calendar_event();
        }

        if(model.getLocationDirtyFlag())
            domain.setLocation(model.getLocation());
        if(model.getMessage_main_attachment_idDirtyFlag())
            domain.setMessageMainAttachmentId(model.getMessage_main_attachment_id());
        if(model.getDescriptionDirtyFlag())
            domain.setDescription(model.getDescription());
        if(model.getRecurrent_id_dateDirtyFlag())
            domain.setRecurrentIdDate(model.getRecurrent_id_date());
        if(model.getSaDirtyFlag())
            domain.setSa(model.getSa());
        if(model.getDisplay_nameDirtyFlag())
            domain.setDisplayName(model.getDisplay_name());
        if(model.getRecurrencyDirtyFlag())
            domain.setRecurrency(model.getRecurrency());
        if(model.getCountDirtyFlag())
            domain.setCount(model.getCount());
        if(model.getFrDirtyFlag())
            domain.setFr(model.getFr());
        if(model.getRruleDirtyFlag())
            domain.setRrule(model.getRrule());
        if(model.getMonth_byDirtyFlag())
            domain.setMonthBy(model.getMonth_by());
        if(model.getWeek_listDirtyFlag())
            domain.setWeekList(model.getWeek_list());
        if(model.getMessage_unreadDirtyFlag())
            domain.setMessageUnread(model.getMessage_unread());
        if(model.getStopDirtyFlag())
            domain.setStop(model.getStop());
        if(model.getRes_idDirtyFlag())
            domain.setResId(model.getRes_id());
        if(model.getMessage_has_error_counterDirtyFlag())
            domain.setMessageHasErrorCounter(model.getMessage_has_error_counter());
        if(model.getRecurrent_idDirtyFlag())
            domain.setRecurrentId(model.getRecurrent_id());
        if(model.getMessage_has_errorDirtyFlag())
            domain.setMessageHasError(model.getMessage_has_error());
        if(model.getEnd_typeDirtyFlag())
            domain.setEndType(model.getEnd_type());
        if(model.getAttendee_idsDirtyFlag())
            domain.setAttendeeIds(model.getAttendee_ids());
        if(model.getAttendee_statusDirtyFlag())
            domain.setAttendeeStatus(model.getAttendee_status());
        if(model.getActiveDirtyFlag())
            domain.setActive(model.getActive());
        if(model.getRrule_typeDirtyFlag())
            domain.setRruleType(model.getRrule_type());
        if(model.getIntervalDirtyFlag())
            domain.setInterval(model.getInterval());
        if(model.getPrivacyDirtyFlag())
            domain.setPrivacy(model.getPrivacy());
        if(model.getDurationDirtyFlag())
            domain.setDuration(model.getDuration());
        if(model.getStart_datetimeDirtyFlag())
            domain.setStartDatetime(model.getStart_datetime());
        if(model.getIs_attendeeDirtyFlag())
            domain.setIsAttendee(model.getIs_attendee());
        if(model.getStart_dateDirtyFlag())
            domain.setStartDate(model.getStart_date());
        if(model.getMoDirtyFlag())
            domain.setMo(model.getMo());
        if(model.getStateDirtyFlag())
            domain.setState(model.getState());
        if(model.get__last_updateDirtyFlag())
            domain.setLastUpdate(model.get__last_update());
        if(model.getWeDirtyFlag())
            domain.setWe(model.getWe());
        if(model.getMessage_partner_idsDirtyFlag())
            domain.setMessagePartnerIds(model.getMessage_partner_ids());
        if(model.getDisplay_timeDirtyFlag())
            domain.setDisplayTime(model.getDisplay_time());
        if(model.getDisplay_startDirtyFlag())
            domain.setDisplayStart(model.getDisplay_start());
        if(model.getStop_dateDirtyFlag())
            domain.setStopDate(model.getStop_date());
        if(model.getMessage_needactionDirtyFlag())
            domain.setMessageNeedaction(model.getMessage_needaction());
        if(model.getMessage_unread_counterDirtyFlag())
            domain.setMessageUnreadCounter(model.getMessage_unread_counter());
        if(model.getFinal_dateDirtyFlag())
            domain.setFinalDate(model.getFinal_date());
        if(model.getRes_model_idDirtyFlag())
            domain.setResModelId(model.getRes_model_id());
        if(model.getIs_highlightedDirtyFlag())
            domain.setIsHighlighted(model.getIs_highlighted());
        if(model.getIdDirtyFlag())
            domain.setId(model.getId());
        if(model.getMessage_channel_idsDirtyFlag())
            domain.setMessageChannelIds(model.getMessage_channel_ids());
        if(model.getWrite_dateDirtyFlag())
            domain.setWriteDate(model.getWrite_date());
        if(model.getStop_datetimeDirtyFlag())
            domain.setStopDatetime(model.getStop_datetime());
        if(model.getActivity_idsDirtyFlag())
            domain.setActivityIds(model.getActivity_ids());
        if(model.getRes_modelDirtyFlag())
            domain.setResModel(model.getRes_model());
        if(model.getTuDirtyFlag())
            domain.setTu(model.getTu());
        if(model.getCreate_dateDirtyFlag())
            domain.setCreateDate(model.getCreate_date());
        if(model.getShow_asDirtyFlag())
            domain.setShowAs(model.getShow_as());
        if(model.getCateg_idsDirtyFlag())
            domain.setCategIds(model.getCateg_ids());
        if(model.getAlarm_idsDirtyFlag())
            domain.setAlarmIds(model.getAlarm_ids());
        if(model.getThDirtyFlag())
            domain.setTh(model.getTh());
        if(model.getMessage_needaction_counterDirtyFlag())
            domain.setMessageNeedactionCounter(model.getMessage_needaction_counter());
        if(model.getBydayDirtyFlag())
            domain.setByday(model.getByday());
        if(model.getDayDirtyFlag())
            domain.setDay(model.getDay());
        if(model.getStartDirtyFlag())
            domain.setStart(model.getStart());
        if(model.getMessage_attachment_countDirtyFlag())
            domain.setMessageAttachmentCount(model.getMessage_attachment_count());
        if(model.getPartner_idsDirtyFlag())
            domain.setPartnerIds(model.getPartner_ids());
        if(model.getAlldayDirtyFlag())
            domain.setAllday(model.getAllday());
        if(model.getMessage_idsDirtyFlag())
            domain.setMessageIds(model.getMessage_ids());
        if(model.getWebsite_message_idsDirtyFlag())
            domain.setWebsiteMessageIds(model.getWebsite_message_ids());
        if(model.getMessage_follower_idsDirtyFlag())
            domain.setMessageFollowerIds(model.getMessage_follower_ids());
        if(model.getSuDirtyFlag())
            domain.setSu(model.getSu());
        if(model.getMessage_is_followerDirtyFlag())
            domain.setMessageIsFollower(model.getMessage_is_follower());
        if(model.getNameDirtyFlag())
            domain.setName(model.getName());
        if(model.getOpportunity_id_textDirtyFlag())
            domain.setOpportunityIdText(model.getOpportunity_id_text());
        if(model.getCreate_uid_textDirtyFlag())
            domain.setCreateUidText(model.getCreate_uid_text());
        if(model.getWrite_uid_textDirtyFlag())
            domain.setWriteUidText(model.getWrite_uid_text());
        if(model.getUser_id_textDirtyFlag())
            domain.setUserIdText(model.getUser_id_text());
        if(model.getPartner_idDirtyFlag())
            domain.setPartnerId(model.getPartner_id());
        if(model.getApplicant_id_textDirtyFlag())
            domain.setApplicantIdText(model.getApplicant_id_text());
        if(model.getCreate_uidDirtyFlag())
            domain.setCreateUid(model.getCreate_uid());
        if(model.getApplicant_idDirtyFlag())
            domain.setApplicantId(model.getApplicant_id());
        if(model.getWrite_uidDirtyFlag())
            domain.setWriteUid(model.getWrite_uid());
        if(model.getUser_idDirtyFlag())
            domain.setUserId(model.getUser_id());
        if(model.getOpportunity_idDirtyFlag())
            domain.setOpportunityId(model.getOpportunity_id());
        return domain ;
    }

}

    



