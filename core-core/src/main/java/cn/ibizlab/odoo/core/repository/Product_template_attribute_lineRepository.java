package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Product_template_attribute_line;
import cn.ibizlab.odoo.core.odoo_product.filter.Product_template_attribute_lineSearchContext;

/**
 * 实体 [产品模板属性明细行] 存储对象
 */
public interface Product_template_attribute_lineRepository extends Repository<Product_template_attribute_line> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Product_template_attribute_line> searchDefault(Product_template_attribute_lineSearchContext context);

    Product_template_attribute_line convert2PO(cn.ibizlab.odoo.core.odoo_product.domain.Product_template_attribute_line domain , Product_template_attribute_line po) ;

    cn.ibizlab.odoo.core.odoo_product.domain.Product_template_attribute_line convert2Domain( Product_template_attribute_line po ,cn.ibizlab.odoo.core.odoo_product.domain.Product_template_attribute_line domain) ;

}
