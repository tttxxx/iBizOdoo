package cn.ibizlab.odoo.core.repository.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import cn.ibizlab.odoo.core.repository.client.po.gamification_badge;

/**
 * 实体[gamification_badge] 服务对象接口
 */
public interface gamification_badgeRepository{


    public gamification_badge createPO() ;
        public void remove(String id);

        public void get(String id);

        public void removeBatch(String id);

        public List<gamification_badge> search();

        public void createBatch(gamification_badge gamification_badge);

        public void updateBatch(gamification_badge gamification_badge);

        public void update(gamification_badge gamification_badge);

        public void create(gamification_badge gamification_badge);


}
