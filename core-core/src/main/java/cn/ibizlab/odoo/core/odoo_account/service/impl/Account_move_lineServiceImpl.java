package cn.ibizlab.odoo.core.odoo_account.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_account.domain.Account_move_line;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_move_lineSearchContext;
import cn.ibizlab.odoo.core.odoo_account.service.IAccount_move_lineService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;



import cn.ibizlab.odoo.core.odoo_account.client.account_move_lineOdooClient;
import cn.ibizlab.odoo.core.odoo_account.clientmodel.account_move_lineClientModel;

/**
 * 实体[日记账项目] 服务对象接口实现
 */
@Slf4j
@Service
public class Account_move_lineServiceImpl implements IAccount_move_lineService {

    @Autowired
    account_move_lineOdooClient account_move_lineOdooClient;


    @Override
    public boolean update(Account_move_line et) {
        account_move_lineClientModel clientModel = convert2Model(et,null);
		account_move_lineOdooClient.update(clientModel);
        Account_move_line rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }
    
    public void updateBatch(List<Account_move_line> list){
    }

    @Override
    public boolean create(Account_move_line et) {
        account_move_lineClientModel clientModel = convert2Model(et,null);
		account_move_lineOdooClient.create(clientModel);
        Account_move_line rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Account_move_line> list){
    }

    @Override
    public Account_move_line get(Integer id) {
        account_move_lineClientModel clientModel = new account_move_lineClientModel();
        clientModel.setId(id);
		account_move_lineOdooClient.get(clientModel);
        Account_move_line et = convert2Domain(clientModel,null);
        if(et==null){
            et=new Account_move_line();
            et.setId(id);
        }
        else{
        }

        return  et;
    }

    @Override
    public boolean remove(Integer id) {
        account_move_lineClientModel clientModel = new account_move_lineClientModel();
        clientModel.setId(id);
		account_move_lineOdooClient.remove(clientModel);
        return true; 
    }

    public void removeBatch(Collection<Integer> idList){
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Account_move_line> searchDefault(Account_move_lineSearchContext context) {
        List<Account_move_line> list = new ArrayList<Account_move_line>();
        Page<account_move_lineClientModel> clientModelList = account_move_lineOdooClient.search(context);
        for(account_move_lineClientModel clientModel : clientModelList){
            list.add( this.convert2Domain(clientModel,null));
        }
        return new PageImpl<Account_move_line>(list, context.getPageable(), clientModelList.getTotalElements());
    }

    public account_move_lineClientModel convert2Model(Account_move_line domain , account_move_lineClientModel model) {
        if(domain == null)
            return model ;
        if(model==null){
            model = new account_move_lineClientModel();

        }
        if((Boolean) domain.getExtensionparams().get("dirtyflagenable")){
            if((Boolean) domain.getExtensionparams().get("analytic_tag_idsdirtyflag"))
                model.setAnalytic_tag_ids(domain.getAnalyticTagIds());
            if((Boolean) domain.getExtensionparams().get("write_datedirtyflag"))
                model.setWrite_date(domain.getWriteDate());
            if((Boolean) domain.getExtensionparams().get("namedirtyflag"))
                model.setName(domain.getName());
            if((Boolean) domain.getExtensionparams().get("balancedirtyflag"))
                model.setBalance(domain.getBalance());
            if((Boolean) domain.getExtensionparams().get("tax_idsdirtyflag"))
                model.setTax_ids(domain.getTaxIds());
            if((Boolean) domain.getExtensionparams().get("amount_currencydirtyflag"))
                model.setAmount_currency(domain.getAmountCurrency());
            if((Boolean) domain.getExtensionparams().get("tax_line_grouping_keydirtyflag"))
                model.setTax_line_grouping_key(domain.getTaxLineGroupingKey());
            if((Boolean) domain.getExtensionparams().get("iddirtyflag"))
                model.setId(domain.getId());
            if((Boolean) domain.getExtensionparams().get("reconcileddirtyflag"))
                model.setReconciled(domain.getReconciled());
            if((Boolean) domain.getExtensionparams().get("display_namedirtyflag"))
                model.setDisplay_name(domain.getDisplayName());
            if((Boolean) domain.getExtensionparams().get("balance_cash_basisdirtyflag"))
                model.setBalance_cash_basis(domain.getBalanceCashBasis());
            if((Boolean) domain.getExtensionparams().get("credit_cash_basisdirtyflag"))
                model.setCredit_cash_basis(domain.getCreditCashBasis());
            if((Boolean) domain.getExtensionparams().get("quantitydirtyflag"))
                model.setQuantity(domain.getQuantity());
            if((Boolean) domain.getExtensionparams().get("amount_residualdirtyflag"))
                model.setAmount_residual(domain.getAmountResidual());
            if((Boolean) domain.getExtensionparams().get("recompute_tax_linedirtyflag"))
                model.setRecompute_tax_line(domain.getRecomputeTaxLine());
            if((Boolean) domain.getExtensionparams().get("tax_exigibledirtyflag"))
                model.setTax_exigible(domain.getTaxExigible());
            if((Boolean) domain.getExtensionparams().get("amount_residual_currencydirtyflag"))
                model.setAmount_residual_currency(domain.getAmountResidualCurrency());
            if((Boolean) domain.getExtensionparams().get("tax_base_amountdirtyflag"))
                model.setTax_base_amount(domain.getTaxBaseAmount());
            if((Boolean) domain.getExtensionparams().get("parent_statedirtyflag"))
                model.setParent_state(domain.getParentState());
            if((Boolean) domain.getExtensionparams().get("blockeddirtyflag"))
                model.setBlocked(domain.getBlocked());
            if((Boolean) domain.getExtensionparams().get("create_datedirtyflag"))
                model.setCreate_date(domain.getCreateDate());
            if((Boolean) domain.getExtensionparams().get("matched_credit_idsdirtyflag"))
                model.setMatched_credit_ids(domain.getMatchedCreditIds());
            if((Boolean) domain.getExtensionparams().get("analytic_line_idsdirtyflag"))
                model.setAnalytic_line_ids(domain.getAnalyticLineIds());
            if((Boolean) domain.getExtensionparams().get("date_maturitydirtyflag"))
                model.setDate_maturity(domain.getDateMaturity());
            if((Boolean) domain.getExtensionparams().get("debitdirtyflag"))
                model.setDebit(domain.getDebit());
            if((Boolean) domain.getExtensionparams().get("__last_updatedirtyflag"))
                model.set__last_update(domain.getLastUpdate());
            if((Boolean) domain.getExtensionparams().get("counterpartdirtyflag"))
                model.setCounterpart(domain.getCounterpart());
            if((Boolean) domain.getExtensionparams().get("matched_debit_idsdirtyflag"))
                model.setMatched_debit_ids(domain.getMatchedDebitIds());
            if((Boolean) domain.getExtensionparams().get("debit_cash_basisdirtyflag"))
                model.setDebit_cash_basis(domain.getDebitCashBasis());
            if((Boolean) domain.getExtensionparams().get("creditdirtyflag"))
                model.setCredit(domain.getCredit());
            if((Boolean) domain.getExtensionparams().get("narrationdirtyflag"))
                model.setNarration(domain.getNarration());
            if((Boolean) domain.getExtensionparams().get("product_id_textdirtyflag"))
                model.setProduct_id_text(domain.getProductIdText());
            if((Boolean) domain.getExtensionparams().get("user_type_id_textdirtyflag"))
                model.setUser_type_id_text(domain.getUserTypeIdText());
            if((Boolean) domain.getExtensionparams().get("payment_id_textdirtyflag"))
                model.setPayment_id_text(domain.getPaymentIdText());
            if((Boolean) domain.getExtensionparams().get("tax_line_id_textdirtyflag"))
                model.setTax_line_id_text(domain.getTaxLineIdText());
            if((Boolean) domain.getExtensionparams().get("full_reconcile_id_textdirtyflag"))
                model.setFull_reconcile_id_text(domain.getFullReconcileIdText());
            if((Boolean) domain.getExtensionparams().get("expense_id_textdirtyflag"))
                model.setExpense_id_text(domain.getExpenseIdText());
            if((Boolean) domain.getExtensionparams().get("move_id_textdirtyflag"))
                model.setMove_id_text(domain.getMoveIdText());
            if((Boolean) domain.getExtensionparams().get("journal_id_textdirtyflag"))
                model.setJournal_id_text(domain.getJournalIdText());
            if((Boolean) domain.getExtensionparams().get("company_currency_id_textdirtyflag"))
                model.setCompany_currency_id_text(domain.getCompanyCurrencyIdText());
            if((Boolean) domain.getExtensionparams().get("invoice_id_textdirtyflag"))
                model.setInvoice_id_text(domain.getInvoiceIdText());
            if((Boolean) domain.getExtensionparams().get("analytic_account_id_textdirtyflag"))
                model.setAnalytic_account_id_text(domain.getAnalyticAccountIdText());
            if((Boolean) domain.getExtensionparams().get("create_uid_textdirtyflag"))
                model.setCreate_uid_text(domain.getCreateUidText());
            if((Boolean) domain.getExtensionparams().get("currency_id_textdirtyflag"))
                model.setCurrency_id_text(domain.getCurrencyIdText());
            if((Boolean) domain.getExtensionparams().get("refdirtyflag"))
                model.setRef(domain.getRef());
            if((Boolean) domain.getExtensionparams().get("company_id_textdirtyflag"))
                model.setCompany_id_text(domain.getCompanyIdText());
            if((Boolean) domain.getExtensionparams().get("statement_id_textdirtyflag"))
                model.setStatement_id_text(domain.getStatementIdText());
            if((Boolean) domain.getExtensionparams().get("statement_line_id_textdirtyflag"))
                model.setStatement_line_id_text(domain.getStatementLineIdText());
            if((Boolean) domain.getExtensionparams().get("partner_id_textdirtyflag"))
                model.setPartner_id_text(domain.getPartnerIdText());
            if((Boolean) domain.getExtensionparams().get("account_id_textdirtyflag"))
                model.setAccount_id_text(domain.getAccountIdText());
            if((Boolean) domain.getExtensionparams().get("datedirtyflag"))
                model.setDate(domain.getDate());
            if((Boolean) domain.getExtensionparams().get("product_uom_id_textdirtyflag"))
                model.setProduct_uom_id_text(domain.getProductUomIdText());
            if((Boolean) domain.getExtensionparams().get("write_uid_textdirtyflag"))
                model.setWrite_uid_text(domain.getWriteUidText());
            if((Boolean) domain.getExtensionparams().get("move_iddirtyflag"))
                model.setMove_id(domain.getMoveId());
            if((Boolean) domain.getExtensionparams().get("currency_iddirtyflag"))
                model.setCurrency_id(domain.getCurrencyId());
            if((Boolean) domain.getExtensionparams().get("company_iddirtyflag"))
                model.setCompany_id(domain.getCompanyId());
            if((Boolean) domain.getExtensionparams().get("payment_iddirtyflag"))
                model.setPayment_id(domain.getPaymentId());
            if((Boolean) domain.getExtensionparams().get("company_currency_iddirtyflag"))
                model.setCompany_currency_id(domain.getCompanyCurrencyId());
            if((Boolean) domain.getExtensionparams().get("journal_iddirtyflag"))
                model.setJournal_id(domain.getJournalId());
            if((Boolean) domain.getExtensionparams().get("analytic_account_iddirtyflag"))
                model.setAnalytic_account_id(domain.getAnalyticAccountId());
            if((Boolean) domain.getExtensionparams().get("invoice_iddirtyflag"))
                model.setInvoice_id(domain.getInvoiceId());
            if((Boolean) domain.getExtensionparams().get("tax_line_iddirtyflag"))
                model.setTax_line_id(domain.getTaxLineId());
            if((Boolean) domain.getExtensionparams().get("full_reconcile_iddirtyflag"))
                model.setFull_reconcile_id(domain.getFullReconcileId());
            if((Boolean) domain.getExtensionparams().get("statement_line_iddirtyflag"))
                model.setStatement_line_id(domain.getStatementLineId());
            if((Boolean) domain.getExtensionparams().get("create_uiddirtyflag"))
                model.setCreate_uid(domain.getCreateUid());
            if((Boolean) domain.getExtensionparams().get("product_iddirtyflag"))
                model.setProduct_id(domain.getProductId());
            if((Boolean) domain.getExtensionparams().get("product_uom_iddirtyflag"))
                model.setProduct_uom_id(domain.getProductUomId());
            if((Boolean) domain.getExtensionparams().get("partner_iddirtyflag"))
                model.setPartner_id(domain.getPartnerId());
            if((Boolean) domain.getExtensionparams().get("account_iddirtyflag"))
                model.setAccount_id(domain.getAccountId());
            if((Boolean) domain.getExtensionparams().get("statement_iddirtyflag"))
                model.setStatement_id(domain.getStatementId());
            if((Boolean) domain.getExtensionparams().get("write_uiddirtyflag"))
                model.setWrite_uid(domain.getWriteUid());
            if((Boolean) domain.getExtensionparams().get("user_type_iddirtyflag"))
                model.setUser_type_id(domain.getUserTypeId());
            if((Boolean) domain.getExtensionparams().get("expense_iddirtyflag"))
                model.setExpense_id(domain.getExpenseId());
            domain.getFocusNull().clear();
        }
        return model ;
    }

    public Account_move_line convert2Domain( account_move_lineClientModel model ,Account_move_line domain) {
        if(model == null)
            return domain ;
        if(domain == null){
            domain = new Account_move_line();
        }

        if(model.getAnalytic_tag_idsDirtyFlag())
            domain.setAnalyticTagIds(model.getAnalytic_tag_ids());
        if(model.getWrite_dateDirtyFlag())
            domain.setWriteDate(model.getWrite_date());
        if(model.getNameDirtyFlag())
            domain.setName(model.getName());
        if(model.getBalanceDirtyFlag())
            domain.setBalance(model.getBalance());
        if(model.getTax_idsDirtyFlag())
            domain.setTaxIds(model.getTax_ids());
        if(model.getAmount_currencyDirtyFlag())
            domain.setAmountCurrency(model.getAmount_currency());
        if(model.getTax_line_grouping_keyDirtyFlag())
            domain.setTaxLineGroupingKey(model.getTax_line_grouping_key());
        if(model.getIdDirtyFlag())
            domain.setId(model.getId());
        if(model.getReconciledDirtyFlag())
            domain.setReconciled(model.getReconciled());
        if(model.getDisplay_nameDirtyFlag())
            domain.setDisplayName(model.getDisplay_name());
        if(model.getBalance_cash_basisDirtyFlag())
            domain.setBalanceCashBasis(model.getBalance_cash_basis());
        if(model.getCredit_cash_basisDirtyFlag())
            domain.setCreditCashBasis(model.getCredit_cash_basis());
        if(model.getQuantityDirtyFlag())
            domain.setQuantity(model.getQuantity());
        if(model.getAmount_residualDirtyFlag())
            domain.setAmountResidual(model.getAmount_residual());
        if(model.getRecompute_tax_lineDirtyFlag())
            domain.setRecomputeTaxLine(model.getRecompute_tax_line());
        if(model.getTax_exigibleDirtyFlag())
            domain.setTaxExigible(model.getTax_exigible());
        if(model.getAmount_residual_currencyDirtyFlag())
            domain.setAmountResidualCurrency(model.getAmount_residual_currency());
        if(model.getTax_base_amountDirtyFlag())
            domain.setTaxBaseAmount(model.getTax_base_amount());
        if(model.getParent_stateDirtyFlag())
            domain.setParentState(model.getParent_state());
        if(model.getBlockedDirtyFlag())
            domain.setBlocked(model.getBlocked());
        if(model.getCreate_dateDirtyFlag())
            domain.setCreateDate(model.getCreate_date());
        if(model.getMatched_credit_idsDirtyFlag())
            domain.setMatchedCreditIds(model.getMatched_credit_ids());
        if(model.getAnalytic_line_idsDirtyFlag())
            domain.setAnalyticLineIds(model.getAnalytic_line_ids());
        if(model.getDate_maturityDirtyFlag())
            domain.setDateMaturity(model.getDate_maturity());
        if(model.getDebitDirtyFlag())
            domain.setDebit(model.getDebit());
        if(model.get__last_updateDirtyFlag())
            domain.setLastUpdate(model.get__last_update());
        if(model.getCounterpartDirtyFlag())
            domain.setCounterpart(model.getCounterpart());
        if(model.getMatched_debit_idsDirtyFlag())
            domain.setMatchedDebitIds(model.getMatched_debit_ids());
        if(model.getDebit_cash_basisDirtyFlag())
            domain.setDebitCashBasis(model.getDebit_cash_basis());
        if(model.getCreditDirtyFlag())
            domain.setCredit(model.getCredit());
        if(model.getNarrationDirtyFlag())
            domain.setNarration(model.getNarration());
        if(model.getProduct_id_textDirtyFlag())
            domain.setProductIdText(model.getProduct_id_text());
        if(model.getUser_type_id_textDirtyFlag())
            domain.setUserTypeIdText(model.getUser_type_id_text());
        if(model.getPayment_id_textDirtyFlag())
            domain.setPaymentIdText(model.getPayment_id_text());
        if(model.getTax_line_id_textDirtyFlag())
            domain.setTaxLineIdText(model.getTax_line_id_text());
        if(model.getFull_reconcile_id_textDirtyFlag())
            domain.setFullReconcileIdText(model.getFull_reconcile_id_text());
        if(model.getExpense_id_textDirtyFlag())
            domain.setExpenseIdText(model.getExpense_id_text());
        if(model.getMove_id_textDirtyFlag())
            domain.setMoveIdText(model.getMove_id_text());
        if(model.getJournal_id_textDirtyFlag())
            domain.setJournalIdText(model.getJournal_id_text());
        if(model.getCompany_currency_id_textDirtyFlag())
            domain.setCompanyCurrencyIdText(model.getCompany_currency_id_text());
        if(model.getInvoice_id_textDirtyFlag())
            domain.setInvoiceIdText(model.getInvoice_id_text());
        if(model.getAnalytic_account_id_textDirtyFlag())
            domain.setAnalyticAccountIdText(model.getAnalytic_account_id_text());
        if(model.getCreate_uid_textDirtyFlag())
            domain.setCreateUidText(model.getCreate_uid_text());
        if(model.getCurrency_id_textDirtyFlag())
            domain.setCurrencyIdText(model.getCurrency_id_text());
        if(model.getRefDirtyFlag())
            domain.setRef(model.getRef());
        if(model.getCompany_id_textDirtyFlag())
            domain.setCompanyIdText(model.getCompany_id_text());
        if(model.getStatement_id_textDirtyFlag())
            domain.setStatementIdText(model.getStatement_id_text());
        if(model.getStatement_line_id_textDirtyFlag())
            domain.setStatementLineIdText(model.getStatement_line_id_text());
        if(model.getPartner_id_textDirtyFlag())
            domain.setPartnerIdText(model.getPartner_id_text());
        if(model.getAccount_id_textDirtyFlag())
            domain.setAccountIdText(model.getAccount_id_text());
        if(model.getDateDirtyFlag())
            domain.setDate(model.getDate());
        if(model.getProduct_uom_id_textDirtyFlag())
            domain.setProductUomIdText(model.getProduct_uom_id_text());
        if(model.getWrite_uid_textDirtyFlag())
            domain.setWriteUidText(model.getWrite_uid_text());
        if(model.getMove_idDirtyFlag())
            domain.setMoveId(model.getMove_id());
        if(model.getCurrency_idDirtyFlag())
            domain.setCurrencyId(model.getCurrency_id());
        if(model.getCompany_idDirtyFlag())
            domain.setCompanyId(model.getCompany_id());
        if(model.getPayment_idDirtyFlag())
            domain.setPaymentId(model.getPayment_id());
        if(model.getCompany_currency_idDirtyFlag())
            domain.setCompanyCurrencyId(model.getCompany_currency_id());
        if(model.getJournal_idDirtyFlag())
            domain.setJournalId(model.getJournal_id());
        if(model.getAnalytic_account_idDirtyFlag())
            domain.setAnalyticAccountId(model.getAnalytic_account_id());
        if(model.getInvoice_idDirtyFlag())
            domain.setInvoiceId(model.getInvoice_id());
        if(model.getTax_line_idDirtyFlag())
            domain.setTaxLineId(model.getTax_line_id());
        if(model.getFull_reconcile_idDirtyFlag())
            domain.setFullReconcileId(model.getFull_reconcile_id());
        if(model.getStatement_line_idDirtyFlag())
            domain.setStatementLineId(model.getStatement_line_id());
        if(model.getCreate_uidDirtyFlag())
            domain.setCreateUid(model.getCreate_uid());
        if(model.getProduct_idDirtyFlag())
            domain.setProductId(model.getProduct_id());
        if(model.getProduct_uom_idDirtyFlag())
            domain.setProductUomId(model.getProduct_uom_id());
        if(model.getPartner_idDirtyFlag())
            domain.setPartnerId(model.getPartner_id());
        if(model.getAccount_idDirtyFlag())
            domain.setAccountId(model.getAccount_id());
        if(model.getStatement_idDirtyFlag())
            domain.setStatementId(model.getStatement_id());
        if(model.getWrite_uidDirtyFlag())
            domain.setWriteUid(model.getWrite_uid());
        if(model.getUser_type_idDirtyFlag())
            domain.setUserTypeId(model.getUser_type_id());
        if(model.getExpense_idDirtyFlag())
            domain.setExpenseId(model.getExpense_id());
        return domain ;
    }

}

    



