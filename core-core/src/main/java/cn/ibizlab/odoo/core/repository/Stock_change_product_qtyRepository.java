package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Stock_change_product_qty;
import cn.ibizlab.odoo.core.odoo_stock.filter.Stock_change_product_qtySearchContext;

/**
 * 实体 [更改产品数量] 存储对象
 */
public interface Stock_change_product_qtyRepository extends Repository<Stock_change_product_qty> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Stock_change_product_qty> searchDefault(Stock_change_product_qtySearchContext context);

    Stock_change_product_qty convert2PO(cn.ibizlab.odoo.core.odoo_stock.domain.Stock_change_product_qty domain , Stock_change_product_qty po) ;

    cn.ibizlab.odoo.core.odoo_stock.domain.Stock_change_product_qty convert2Domain( Stock_change_product_qty po ,cn.ibizlab.odoo.core.odoo_stock.domain.Stock_change_product_qty domain) ;

}
