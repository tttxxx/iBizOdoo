package cn.ibizlab.odoo.core.odoo_stock.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_stock.domain.Stock_location;
import cn.ibizlab.odoo.core.odoo_stock.filter.Stock_locationSearchContext;


/**
 * 实体[Stock_location] 服务对象接口
 */
public interface IStock_locationService{

    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    boolean create(Stock_location et) ;
    void createBatch(List<Stock_location> list) ;
    Stock_location get(Integer key) ;
    boolean update(Stock_location et) ;
    void updateBatch(List<Stock_location> list) ;
    Page<Stock_location> searchDefault(Stock_locationSearchContext context) ;

}



