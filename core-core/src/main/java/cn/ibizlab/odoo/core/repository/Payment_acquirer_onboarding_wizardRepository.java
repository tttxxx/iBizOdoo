package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Payment_acquirer_onboarding_wizard;
import cn.ibizlab.odoo.core.odoo_payment.filter.Payment_acquirer_onboarding_wizardSearchContext;

/**
 * 实体 [付款获得onboarding向导] 存储对象
 */
public interface Payment_acquirer_onboarding_wizardRepository extends Repository<Payment_acquirer_onboarding_wizard> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Payment_acquirer_onboarding_wizard> searchDefault(Payment_acquirer_onboarding_wizardSearchContext context);

    Payment_acquirer_onboarding_wizard convert2PO(cn.ibizlab.odoo.core.odoo_payment.domain.Payment_acquirer_onboarding_wizard domain , Payment_acquirer_onboarding_wizard po) ;

    cn.ibizlab.odoo.core.odoo_payment.domain.Payment_acquirer_onboarding_wizard convert2Domain( Payment_acquirer_onboarding_wizard po ,cn.ibizlab.odoo.core.odoo_payment.domain.Payment_acquirer_onboarding_wizard domain) ;

}
