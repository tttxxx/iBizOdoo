package cn.ibizlab.odoo.core.repository.po ;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.math.BigInteger;

import cn.ibizlab.odoo.core.odoo_asset.filter.Asset_assetSearchContext;

/**
 * 实体 [Asset] 存储模型
 */
public interface Asset_asset{

    /**
     * Map
     */
    String getPosition();

    void setPosition(String position);

    /**
     * 获取 [Map]脏标记
     */
    boolean getPositionDirtyFlag();

    /**
     * Meter
     */
    String getMeter_ids();

    void setMeter_ids(String meter_ids);

    /**
     * 获取 [Meter]脏标记
     */
    boolean getMeter_idsDirtyFlag();

    /**
     * 附件数量
     */
    Integer getMessage_attachment_count();

    void setMessage_attachment_count(Integer message_attachment_count);

    /**
     * 获取 [附件数量]脏标记
     */
    boolean getMessage_attachment_countDirtyFlag();

    /**
     * 开始日期
     */
    Timestamp getStart_date();

    void setStart_date(Timestamp start_date);

    /**
     * 获取 [开始日期]脏标记
     */
    boolean getStart_dateDirtyFlag();

    /**
     * Maintenance Date
     */
    Timestamp getMaintenance_date();

    void setMaintenance_date(Timestamp maintenance_date);

    /**
     * 获取 [Maintenance Date]脏标记
     */
    boolean getMaintenance_dateDirtyFlag();

    /**
     * 图像
     */
    byte[] getImage();

    void setImage(byte[] image);

    /**
     * 获取 [图像]脏标记
     */
    boolean getImageDirtyFlag();

    /**
     * Asset Location
     */
    Integer getProperty_stock_asset();

    void setProperty_stock_asset(Integer property_stock_asset);

    /**
     * 获取 [Asset Location]脏标记
     */
    boolean getProperty_stock_assetDirtyFlag();

    /**
     * 关注者(渠道)
     */
    String getMessage_channel_ids();

    void setMessage_channel_ids(String message_channel_ids);

    /**
     * 获取 [关注者(渠道)]脏标记
     */
    boolean getMessage_channel_idsDirtyFlag();

    /**
     * 模型
     */
    String getModel();

    void setModel(String model);

    /**
     * 获取 [模型]脏标记
     */
    boolean getModelDirtyFlag();

    /**
     * Asset Number
     */
    String getAsset_number();

    void setAsset_number(String asset_number);

    /**
     * 获取 [Asset Number]脏标记
     */
    boolean getAsset_numberDirtyFlag();

    /**
     * 未读消息
     */
    String getMessage_unread();

    void setMessage_unread(String message_unread);

    /**
     * 获取 [未读消息]脏标记
     */
    boolean getMessage_unreadDirtyFlag();

    /**
     * 附件
     */
    Integer getMessage_main_attachment_id();

    void setMessage_main_attachment_id(Integer message_main_attachment_id);

    /**
     * 获取 [附件]脏标记
     */
    boolean getMessage_main_attachment_idDirtyFlag();

    /**
     * Serial no.
     */
    String getSerial();

    void setSerial(String serial);

    /**
     * 获取 [Serial no.]脏标记
     */
    boolean getSerialDirtyFlag();

    /**
     * 错误个数
     */
    Integer getMessage_has_error_counter();

    void setMessage_has_error_counter(Integer message_has_error_counter);

    /**
     * 获取 [错误个数]脏标记
     */
    boolean getMessage_has_error_counterDirtyFlag();

    /**
     * 消息递送错误
     */
    String getMessage_has_error();

    void setMessage_has_error(String message_has_error);

    /**
     * 获取 [消息递送错误]脏标记
     */
    boolean getMessage_has_errorDirtyFlag();

    /**
     * 关注者(业务伙伴)
     */
    String getMessage_partner_ids();

    void setMessage_partner_ids(String message_partner_ids);

    /**
     * 获取 [关注者(业务伙伴)]脏标记
     */
    boolean getMessage_partner_idsDirtyFlag();

    /**
     * 有效
     */
    String getActive();

    void setActive(String active);

    /**
     * 获取 [有效]脏标记
     */
    boolean getActiveDirtyFlag();

    /**
     * 关注者
     */
    String getMessage_follower_ids();

    void setMessage_follower_ids(String message_follower_ids);

    /**
     * 获取 [关注者]脏标记
     */
    boolean getMessage_follower_idsDirtyFlag();

    /**
     * 最后更新时间
     */
    Timestamp getWrite_date();

    void setWrite_date(Timestamp write_date);

    /**
     * 获取 [最后更新时间]脏标记
     */
    boolean getWrite_dateDirtyFlag();

    /**
     * 操作次数
     */
    Integer getMessage_needaction_counter();

    void setMessage_needaction_counter(Integer message_needaction_counter);

    /**
     * 获取 [操作次数]脏标记
     */
    boolean getMessage_needaction_counterDirtyFlag();

    /**
     * Purchase Date
     */
    Timestamp getPurchase_date();

    void setPurchase_date(Timestamp purchase_date);

    /**
     * 获取 [Purchase Date]脏标记
     */
    boolean getPurchase_dateDirtyFlag();

    /**
     * 中等尺寸图像
     */
    byte[] getImage_medium();

    void setImage_medium(byte[] image_medium);

    /**
     * 获取 [中等尺寸图像]脏标记
     */
    boolean getImage_mediumDirtyFlag();

    /**
     * 显示名称
     */
    String getDisplay_name();

    void setDisplay_name(String display_name);

    /**
     * 获取 [显示名称]脏标记
     */
    boolean getDisplay_nameDirtyFlag();

    /**
     * ID
     */
    Integer getId();

    void setId(Integer id);

    /**
     * 获取 [ID]脏标记
     */
    boolean getIdDirtyFlag();

    /**
     * 标签
     */
    String getCategory_ids();

    void setCategory_ids(String category_ids);

    /**
     * 获取 [标签]脏标记
     */
    boolean getCategory_idsDirtyFlag();

    /**
     * Warranty Start
     */
    Timestamp getWarranty_start_date();

    void setWarranty_start_date(Timestamp warranty_start_date);

    /**
     * 获取 [Warranty Start]脏标记
     */
    boolean getWarranty_start_dateDirtyFlag();

    /**
     * 未读消息计数器
     */
    Integer getMessage_unread_counter();

    void setMessage_unread_counter(Integer message_unread_counter);

    /**
     * 获取 [未读消息计数器]脏标记
     */
    boolean getMessage_unread_counterDirtyFlag();

    /**
     * 创建时间
     */
    Timestamp getCreate_date();

    void setCreate_date(Timestamp create_date);

    /**
     * 获取 [创建时间]脏标记
     */
    boolean getCreate_dateDirtyFlag();

    /**
     * Warranty End
     */
    Timestamp getWarranty_end_date();

    void setWarranty_end_date(Timestamp warranty_end_date);

    /**
     * 获取 [Warranty End]脏标记
     */
    boolean getWarranty_end_dateDirtyFlag();

    /**
     * 网站消息
     */
    String getWebsite_message_ids();

    void setWebsite_message_ids(String website_message_ids);

    /**
     * 获取 [网站消息]脏标记
     */
    boolean getWebsite_message_idsDirtyFlag();

    /**
     * 小尺寸图像
     */
    byte[] getImage_small();

    void setImage_small(byte[] image_small);

    /**
     * 获取 [小尺寸图像]脏标记
     */
    boolean getImage_smallDirtyFlag();

    /**
     * Asset Name
     */
    String getName();

    void setName(String name);

    /**
     * 获取 [Asset Name]脏标记
     */
    boolean getNameDirtyFlag();

    /**
     * Criticality
     */
    String getCriticality();

    void setCriticality(String criticality);

    /**
     * 获取 [Criticality]脏标记
     */
    boolean getCriticalityDirtyFlag();

    /**
     * 消息
     */
    String getMessage_ids();

    void setMessage_ids(String message_ids);

    /**
     * 获取 [消息]脏标记
     */
    boolean getMessage_idsDirtyFlag();

    /**
     * 前置操作
     */
    String getMessage_needaction();

    void setMessage_needaction(String message_needaction);

    /**
     * 获取 [前置操作]脏标记
     */
    boolean getMessage_needactionDirtyFlag();

    /**
     * 最后修改日
     */
    Timestamp get__last_update();

    void set__last_update(Timestamp __last_update);

    /**
     * 获取 [最后修改日]脏标记
     */
    boolean get__last_updateDirtyFlag();

    /**
     * # Maintenance
     */
    Integer getMro_count();

    void setMro_count(Integer mro_count);

    /**
     * 获取 [# Maintenance]脏标记
     */
    boolean getMro_countDirtyFlag();

    /**
     * 关注者
     */
    String getMessage_is_follower();

    void setMessage_is_follower(String message_is_follower);

    /**
     * 获取 [关注者]脏标记
     */
    boolean getMessage_is_followerDirtyFlag();

    /**
     * 省/ 州
     */
    String getWarehouse_state_id_text();

    void setWarehouse_state_id_text(String warehouse_state_id_text);

    /**
     * 获取 [省/ 州]脏标记
     */
    boolean getWarehouse_state_id_textDirtyFlag();

    /**
     * 分派给
     */
    String getUser_id_text();

    void setUser_id_text(String user_id_text);

    /**
     * 获取 [分派给]脏标记
     */
    boolean getUser_id_textDirtyFlag();

    /**
     * 创建人
     */
    String getCreate_uid_text();

    void setCreate_uid_text(String create_uid_text);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uid_textDirtyFlag();

    /**
     * 颜色
     */
    String getMaintenance_state_color();

    void setMaintenance_state_color(String maintenance_state_color);

    /**
     * 获取 [颜色]脏标记
     */
    boolean getMaintenance_state_colorDirtyFlag();

    /**
     * 省/ 州
     */
    String getAccounting_state_id_text();

    void setAccounting_state_id_text(String accounting_state_id_text);

    /**
     * 获取 [省/ 州]脏标记
     */
    boolean getAccounting_state_id_textDirtyFlag();

    /**
     * 省/ 州
     */
    String getManufacture_state_id_text();

    void setManufacture_state_id_text(String manufacture_state_id_text);

    /**
     * 获取 [省/ 州]脏标记
     */
    boolean getManufacture_state_id_textDirtyFlag();

    /**
     * 省/ 州
     */
    String getFinance_state_id_text();

    void setFinance_state_id_text(String finance_state_id_text);

    /**
     * 获取 [省/ 州]脏标记
     */
    boolean getFinance_state_id_textDirtyFlag();

    /**
     * 省/ 州
     */
    String getMaintenance_state_id_text();

    void setMaintenance_state_id_text(String maintenance_state_id_text);

    /**
     * 获取 [省/ 州]脏标记
     */
    boolean getMaintenance_state_id_textDirtyFlag();

    /**
     * Manufacturer
     */
    String getManufacturer_id_text();

    void setManufacturer_id_text(String manufacturer_id_text);

    /**
     * 获取 [Manufacturer]脏标记
     */
    boolean getManufacturer_id_textDirtyFlag();

    /**
     * 最后更新者
     */
    String getWrite_uid_text();

    void setWrite_uid_text(String write_uid_text);

    /**
     * 获取 [最后更新者]脏标记
     */
    boolean getWrite_uid_textDirtyFlag();

    /**
     * 供应商
     */
    String getVendor_id_text();

    void setVendor_id_text(String vendor_id_text);

    /**
     * 获取 [供应商]脏标记
     */
    boolean getVendor_id_textDirtyFlag();

    /**
     * 省/ 州
     */
    Integer getManufacture_state_id();

    void setManufacture_state_id(Integer manufacture_state_id);

    /**
     * 获取 [省/ 州]脏标记
     */
    boolean getManufacture_state_idDirtyFlag();

    /**
     * 省/ 州
     */
    Integer getAccounting_state_id();

    void setAccounting_state_id(Integer accounting_state_id);

    /**
     * 获取 [省/ 州]脏标记
     */
    boolean getAccounting_state_idDirtyFlag();

    /**
     * 创建人
     */
    Integer getCreate_uid();

    void setCreate_uid(Integer create_uid);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uidDirtyFlag();

    /**
     * 供应商
     */
    Integer getVendor_id();

    void setVendor_id(Integer vendor_id);

    /**
     * 获取 [供应商]脏标记
     */
    boolean getVendor_idDirtyFlag();

    /**
     * 省/ 州
     */
    Integer getWarehouse_state_id();

    void setWarehouse_state_id(Integer warehouse_state_id);

    /**
     * 获取 [省/ 州]脏标记
     */
    boolean getWarehouse_state_idDirtyFlag();

    /**
     * 最后更新者
     */
    Integer getWrite_uid();

    void setWrite_uid(Integer write_uid);

    /**
     * 获取 [最后更新者]脏标记
     */
    boolean getWrite_uidDirtyFlag();

    /**
     * 分派给
     */
    Integer getUser_id();

    void setUser_id(Integer user_id);

    /**
     * 获取 [分派给]脏标记
     */
    boolean getUser_idDirtyFlag();

    /**
     * 省/ 州
     */
    Integer getFinance_state_id();

    void setFinance_state_id(Integer finance_state_id);

    /**
     * 获取 [省/ 州]脏标记
     */
    boolean getFinance_state_idDirtyFlag();

    /**
     * 省/ 州
     */
    Integer getMaintenance_state_id();

    void setMaintenance_state_id(Integer maintenance_state_id);

    /**
     * 获取 [省/ 州]脏标记
     */
    boolean getMaintenance_state_idDirtyFlag();

    /**
     * Manufacturer
     */
    Integer getManufacturer_id();

    void setManufacturer_id(Integer manufacturer_id);

    /**
     * 获取 [Manufacturer]脏标记
     */
    boolean getManufacturer_idDirtyFlag();

}
