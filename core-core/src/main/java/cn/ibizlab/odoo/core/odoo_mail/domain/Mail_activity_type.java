package cn.ibizlab.odoo.core.odoo_mail.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.odoo.util.domain.EntityBase;
import cn.ibizlab.odoo.util.annotation.DEField;
import cn.ibizlab.odoo.util.enums.DEPredefinedFieldType;
import cn.ibizlab.odoo.util.enums.DEFieldDefaultValueType;
import java.io.Serializable;
import lombok.Data;
import org.springframework.data.annotation.Transient;

import cn.ibizlab.odoo.util.domain.EntityClient;

/**
 * ServiceApi [活动类型] 对象
 */
@Data
public class Mail_activity_type extends EntityClient implements Serializable {

    /**
     * 模型已更改
     */
    @JSONField(name = "res_model_change")
    @JsonProperty("res_model_change")
    private String resModelChange;

    /**
     * 摘要
     */
    @JSONField(name = "summary")
    @JsonProperty("summary")
    private String summary;

    /**
     * 自动安排下一个活动
     */
    @DEField(name = "force_next")
    @JSONField(name = "force_next")
    @JsonProperty("force_next")
    private String forceNext;

    /**
     * 序号
     */
    @JSONField(name = "sequence")
    @JsonProperty("sequence")
    private Integer sequence;

    /**
     * ID
     */
    @DEField(isKeyField=true)
    @JSONField(name = "id")
    @JsonProperty("id")
    private Integer id;

    /**
     * 类别
     */
    @JSONField(name = "category")
    @JsonProperty("category")
    private String category;

    /**
     * 图标
     */
    @JSONField(name = "icon")
    @JsonProperty("icon")
    private String icon;

    /**
     * 之后
     */
    @DEField(name = "delay_count")
    @JSONField(name = "delay_count")
    @JsonProperty("delay_count")
    private Integer delayCount;

    /**
     * 预先活动
     */
    @JSONField(name = "previous_type_ids")
    @JsonProperty("previous_type_ids")
    private String previousTypeIds;

    /**
     * 显示名称
     */
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    private String displayName;

    /**
     * 有效
     */
    @JSONField(name = "active")
    @JsonProperty("active")
    private String active;

    /**
     * 延迟类型
     */
    @DEField(name = "delay_from")
    @JSONField(name = "delay_from")
    @JsonProperty("delay_from")
    private String delayFrom;

    /**
     * 最后更新时间
     */
    @DEField(name = "write_date" , preType = DEPredefinedFieldType.UPDATEDATE)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;

    /**
     * 邮件模板
     */
    @JSONField(name = "mail_template_ids")
    @JsonProperty("mail_template_ids")
    private String mailTemplateIds;

    /**
     * 名称
     */
    @JSONField(name = "name")
    @JsonProperty("name")
    private String name;

    /**
     * 创建时间
     */
    @DEField(name = "create_date" , preType = DEPredefinedFieldType.CREATEDATE)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;

    /**
     * 模型
     */
    @DEField(name = "res_model_id")
    @JSONField(name = "res_model_id")
    @JsonProperty("res_model_id")
    private Integer resModelId;

    /**
     * 最后修改日
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;

    /**
     * 排版类型
     */
    @DEField(name = "decoration_type")
    @JSONField(name = "decoration_type")
    @JsonProperty("decoration_type")
    private String decorationType;

    /**
     * 推荐的下一活动
     */
    @JSONField(name = "next_type_ids")
    @JsonProperty("next_type_ids")
    private String nextTypeIds;

    /**
     * 延迟单位
     */
    @DEField(name = "delay_unit")
    @JSONField(name = "delay_unit")
    @JsonProperty("delay_unit")
    private String delayUnit;

    /**
     * 初始模型
     */
    @JSONField(name = "initial_res_model_id")
    @JsonProperty("initial_res_model_id")
    private Integer initialResModelId;

    /**
     * 设置默认下一个活动
     */
    @JSONField(name = "default_next_type_id_text")
    @JsonProperty("default_next_type_id_text")
    private String defaultNextTypeIdText;

    /**
     * 最后更新者
     */
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    private String writeUidText;

    /**
     * 创建人
     */
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    private String createUidText;

    /**
     * 创建人
     */
    @DEField(name = "create_uid" , preType = DEPredefinedFieldType.CREATEMAN)
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    private Integer createUid;

    /**
     * 最后更新者
     */
    @DEField(name = "write_uid" , preType = DEPredefinedFieldType.UPDATEMAN)
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    private Integer writeUid;

    /**
     * 设置默认下一个活动
     */
    @DEField(name = "default_next_type_id")
    @JSONField(name = "default_next_type_id")
    @JsonProperty("default_next_type_id")
    private Integer defaultNextTypeId;


    /**
     * 
     */
    @JSONField(name = "odoodefaultnexttype")
    @JsonProperty("odoodefaultnexttype")
    private cn.ibizlab.odoo.core.odoo_mail.domain.Mail_activity_type odooDefaultNextType;

    /**
     * 
     */
    @JSONField(name = "odoocreate")
    @JsonProperty("odoocreate")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_users odooCreate;

    /**
     * 
     */
    @JSONField(name = "odoowrite")
    @JsonProperty("odoowrite")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_users odooWrite;




    /**
     * 设置 [摘要]
     */
    public void setSummary(String summary){
        this.summary = summary ;
        this.modify("summary",summary);
    }
    /**
     * 设置 [自动安排下一个活动]
     */
    public void setForceNext(String forceNext){
        this.forceNext = forceNext ;
        this.modify("force_next",forceNext);
    }
    /**
     * 设置 [序号]
     */
    public void setSequence(Integer sequence){
        this.sequence = sequence ;
        this.modify("sequence",sequence);
    }
    /**
     * 设置 [类别]
     */
    public void setCategory(String category){
        this.category = category ;
        this.modify("category",category);
    }
    /**
     * 设置 [图标]
     */
    public void setIcon(String icon){
        this.icon = icon ;
        this.modify("icon",icon);
    }
    /**
     * 设置 [之后]
     */
    public void setDelayCount(Integer delayCount){
        this.delayCount = delayCount ;
        this.modify("delay_count",delayCount);
    }
    /**
     * 设置 [有效]
     */
    public void setActive(String active){
        this.active = active ;
        this.modify("active",active);
    }
    /**
     * 设置 [延迟类型]
     */
    public void setDelayFrom(String delayFrom){
        this.delayFrom = delayFrom ;
        this.modify("delay_from",delayFrom);
    }
    /**
     * 设置 [名称]
     */
    public void setName(String name){
        this.name = name ;
        this.modify("name",name);
    }
    /**
     * 设置 [模型]
     */
    public void setResModelId(Integer resModelId){
        this.resModelId = resModelId ;
        this.modify("res_model_id",resModelId);
    }
    /**
     * 设置 [排版类型]
     */
    public void setDecorationType(String decorationType){
        this.decorationType = decorationType ;
        this.modify("decoration_type",decorationType);
    }
    /**
     * 设置 [延迟单位]
     */
    public void setDelayUnit(String delayUnit){
        this.delayUnit = delayUnit ;
        this.modify("delay_unit",delayUnit);
    }
    /**
     * 设置 [设置默认下一个活动]
     */
    public void setDefaultNextTypeId(Integer defaultNextTypeId){
        this.defaultNextTypeId = defaultNextTypeId ;
        this.modify("default_next_type_id",defaultNextTypeId);
    }

}


