package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Ihr_department;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[hr_department] 服务对象接口
 */
public interface Ihr_departmentClientService{

    public Ihr_department createModel() ;

    public void remove(Ihr_department hr_department);

    public Page<Ihr_department> search(SearchContext context);

    public void get(Ihr_department hr_department);

    public void update(Ihr_department hr_department);

    public void removeBatch(List<Ihr_department> hr_departments);

    public void createBatch(List<Ihr_department> hr_departments);

    public void create(Ihr_department hr_department);

    public void updateBatch(List<Ihr_department> hr_departments);

    public Page<Ihr_department> select(SearchContext context);

}
