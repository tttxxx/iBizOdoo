package cn.ibizlab.odoo.core.repository.po ;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.math.BigInteger;

import cn.ibizlab.odoo.core.odoo_maintenance.filter.Maintenance_requestSearchContext;

/**
 * 实体 [保养请求] 存储模型
 */
public interface Maintenance_request{

    /**
     * 需要采取行动
     */
    String getMessage_needaction();

    void setMessage_needaction(String message_needaction);

    /**
     * 获取 [需要采取行动]脏标记
     */
    boolean getMessage_needactionDirtyFlag();

    /**
     * 主题
     */
    String getName();

    void setName(String name);

    /**
     * 获取 [主题]脏标记
     */
    boolean getNameDirtyFlag();

    /**
     * 下一活动截止日期
     */
    Timestamp getActivity_date_deadline();

    void setActivity_date_deadline(Timestamp activity_date_deadline);

    /**
     * 获取 [下一活动截止日期]脏标记
     */
    boolean getActivity_date_deadlineDirtyFlag();

    /**
     * 创建时间
     */
    Timestamp getCreate_date();

    void setCreate_date(Timestamp create_date);

    /**
     * 获取 [创建时间]脏标记
     */
    boolean getCreate_dateDirtyFlag();

    /**
     * 最后更新时间
     */
    Timestamp getWrite_date();

    void setWrite_date(Timestamp write_date);

    /**
     * 获取 [最后更新时间]脏标记
     */
    boolean getWrite_dateDirtyFlag();

    /**
     * ID
     */
    Integer getId();

    void setId(Integer id);

    /**
     * 获取 [ID]脏标记
     */
    boolean getIdDirtyFlag();

    /**
     * 下一活动摘要
     */
    String getActivity_summary();

    void setActivity_summary(String activity_summary);

    /**
     * 获取 [下一活动摘要]脏标记
     */
    boolean getActivity_summaryDirtyFlag();

    /**
     * 持续时间
     */
    Double getDuration();

    void setDuration(Double duration);

    /**
     * 获取 [持续时间]脏标记
     */
    boolean getDurationDirtyFlag();

    /**
     * 行动数量
     */
    Integer getMessage_needaction_counter();

    void setMessage_needaction_counter(Integer message_needaction_counter);

    /**
     * 获取 [行动数量]脏标记
     */
    boolean getMessage_needaction_counterDirtyFlag();

    /**
     * 附件
     */
    Integer getMessage_main_attachment_id();

    void setMessage_main_attachment_id(Integer message_main_attachment_id);

    /**
     * 获取 [附件]脏标记
     */
    boolean getMessage_main_attachment_idDirtyFlag();

    /**
     * 显示名称
     */
    String getDisplay_name();

    void setDisplay_name(String display_name);

    /**
     * 获取 [显示名称]脏标记
     */
    boolean getDisplay_nameDirtyFlag();

    /**
     * 计划日期
     */
    Timestamp getSchedule_date();

    void setSchedule_date(Timestamp schedule_date);

    /**
     * 获取 [计划日期]脏标记
     */
    boolean getSchedule_dateDirtyFlag();

    /**
     * 看板状态
     */
    String getKanban_state();

    void setKanban_state(String kanban_state);

    /**
     * 获取 [看板状态]脏标记
     */
    boolean getKanban_stateDirtyFlag();

    /**
     * 关注者(业务伙伴)
     */
    String getMessage_partner_ids();

    void setMessage_partner_ids(String message_partner_ids);

    /**
     * 获取 [关注者(业务伙伴)]脏标记
     */
    boolean getMessage_partner_idsDirtyFlag();

    /**
     * 是关注者
     */
    String getMessage_is_follower();

    void setMessage_is_follower(String message_is_follower);

    /**
     * 获取 [是关注者]脏标记
     */
    boolean getMessage_is_followerDirtyFlag();

    /**
     * 保养类型
     */
    String getMaintenance_type();

    void setMaintenance_type(String maintenance_type);

    /**
     * 获取 [保养类型]脏标记
     */
    boolean getMaintenance_typeDirtyFlag();

    /**
     * 网站消息
     */
    String getWebsite_message_ids();

    void setWebsite_message_ids(String website_message_ids);

    /**
     * 获取 [网站消息]脏标记
     */
    boolean getWebsite_message_idsDirtyFlag();

    /**
     * 未读消息
     */
    String getMessage_unread();

    void setMessage_unread(String message_unread);

    /**
     * 获取 [未读消息]脏标记
     */
    boolean getMessage_unreadDirtyFlag();

    /**
     * 消息递送错误
     */
    String getMessage_has_error();

    void setMessage_has_error(String message_has_error);

    /**
     * 获取 [消息递送错误]脏标记
     */
    boolean getMessage_has_errorDirtyFlag();

    /**
     * 关闭日期
     */
    Timestamp getClose_date();

    void setClose_date(Timestamp close_date);

    /**
     * 获取 [关闭日期]脏标记
     */
    boolean getClose_dateDirtyFlag();

    /**
     * 活动
     */
    String getActivity_ids();

    void setActivity_ids(String activity_ids);

    /**
     * 获取 [活动]脏标记
     */
    boolean getActivity_idsDirtyFlag();

    /**
     * 未读消息计数器
     */
    Integer getMessage_unread_counter();

    void setMessage_unread_counter(Integer message_unread_counter);

    /**
     * 获取 [未读消息计数器]脏标记
     */
    boolean getMessage_unread_counterDirtyFlag();

    /**
     * 责任用户
     */
    Integer getActivity_user_id();

    void setActivity_user_id(Integer activity_user_id);

    /**
     * 获取 [责任用户]脏标记
     */
    boolean getActivity_user_idDirtyFlag();

    /**
     * 活动状态
     */
    String getActivity_state();

    void setActivity_state(String activity_state);

    /**
     * 获取 [活动状态]脏标记
     */
    boolean getActivity_stateDirtyFlag();

    /**
     * 下一活动类型
     */
    Integer getActivity_type_id();

    void setActivity_type_id(Integer activity_type_id);

    /**
     * 获取 [下一活动类型]脏标记
     */
    boolean getActivity_type_idDirtyFlag();

    /**
     * 关注者
     */
    String getMessage_follower_ids();

    void setMessage_follower_ids(String message_follower_ids);

    /**
     * 获取 [关注者]脏标记
     */
    boolean getMessage_follower_idsDirtyFlag();

    /**
     * 请求日期
     */
    Timestamp getRequest_date();

    void setRequest_date(Timestamp request_date);

    /**
     * 获取 [请求日期]脏标记
     */
    boolean getRequest_dateDirtyFlag();

    /**
     * 归档
     */
    String getArchive();

    void setArchive(String archive);

    /**
     * 获取 [归档]脏标记
     */
    boolean getArchiveDirtyFlag();

    /**
     * 消息
     */
    String getMessage_ids();

    void setMessage_ids(String message_ids);

    /**
     * 获取 [消息]脏标记
     */
    boolean getMessage_idsDirtyFlag();

    /**
     * 错误数
     */
    Integer getMessage_has_error_counter();

    void setMessage_has_error_counter(Integer message_has_error_counter);

    /**
     * 获取 [错误数]脏标记
     */
    boolean getMessage_has_error_counterDirtyFlag();

    /**
     * 最后修改日
     */
    Timestamp get__last_update();

    void set__last_update(Timestamp __last_update);

    /**
     * 获取 [最后修改日]脏标记
     */
    boolean get__last_updateDirtyFlag();

    /**
     * 颜色索引
     */
    Integer getColor();

    void setColor(Integer color);

    /**
     * 获取 [颜色索引]脏标记
     */
    boolean getColorDirtyFlag();

    /**
     * 关注者(渠道)
     */
    String getMessage_channel_ids();

    void setMessage_channel_ids(String message_channel_ids);

    /**
     * 获取 [关注者(渠道)]脏标记
     */
    boolean getMessage_channel_idsDirtyFlag();

    /**
     * 优先级
     */
    String getPriority();

    void setPriority(String priority);

    /**
     * 获取 [优先级]脏标记
     */
    boolean getPriorityDirtyFlag();

    /**
     * 说明
     */
    String getDescription();

    void setDescription(String description);

    /**
     * 获取 [说明]脏标记
     */
    boolean getDescriptionDirtyFlag();

    /**
     * 附件数量
     */
    Integer getMessage_attachment_count();

    void setMessage_attachment_count(Integer message_attachment_count);

    /**
     * 获取 [附件数量]脏标记
     */
    boolean getMessage_attachment_countDirtyFlag();

    /**
     * 创建人
     */
    String getOwner_user_id_text();

    void setOwner_user_id_text(String owner_user_id_text);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getOwner_user_id_textDirtyFlag();

    /**
     * 员工
     */
    String getEmployee_id_text();

    void setEmployee_id_text(String employee_id_text);

    /**
     * 获取 [员工]脏标记
     */
    boolean getEmployee_id_textDirtyFlag();

    /**
     * 最后更新人
     */
    String getWrite_uid_text();

    void setWrite_uid_text(String write_uid_text);

    /**
     * 获取 [最后更新人]脏标记
     */
    boolean getWrite_uid_textDirtyFlag();

    /**
     * 团队
     */
    String getMaintenance_team_id_text();

    void setMaintenance_team_id_text(String maintenance_team_id_text);

    /**
     * 获取 [团队]脏标记
     */
    boolean getMaintenance_team_id_textDirtyFlag();

    /**
     * 公司
     */
    String getCompany_id_text();

    void setCompany_id_text(String company_id_text);

    /**
     * 获取 [公司]脏标记
     */
    boolean getCompany_id_textDirtyFlag();

    /**
     * 设备
     */
    String getEquipment_id_text();

    void setEquipment_id_text(String equipment_id_text);

    /**
     * 获取 [设备]脏标记
     */
    boolean getEquipment_id_textDirtyFlag();

    /**
     * 阶段
     */
    String getStage_id_text();

    void setStage_id_text(String stage_id_text);

    /**
     * 获取 [阶段]脏标记
     */
    boolean getStage_id_textDirtyFlag();

    /**
     * 创建人
     */
    String getCreate_uid_text();

    void setCreate_uid_text(String create_uid_text);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uid_textDirtyFlag();

    /**
     * 类别
     */
    String getCategory_id_text();

    void setCategory_id_text(String category_id_text);

    /**
     * 获取 [类别]脏标记
     */
    boolean getCategory_id_textDirtyFlag();

    /**
     * 技术员
     */
    String getUser_id_text();

    void setUser_id_text(String user_id_text);

    /**
     * 获取 [技术员]脏标记
     */
    boolean getUser_id_textDirtyFlag();

    /**
     * 部门
     */
    String getDepartment_id_text();

    void setDepartment_id_text(String department_id_text);

    /**
     * 获取 [部门]脏标记
     */
    boolean getDepartment_id_textDirtyFlag();

    /**
     * 员工
     */
    Integer getEmployee_id();

    void setEmployee_id(Integer employee_id);

    /**
     * 获取 [员工]脏标记
     */
    boolean getEmployee_idDirtyFlag();

    /**
     * 公司
     */
    Integer getCompany_id();

    void setCompany_id(Integer company_id);

    /**
     * 获取 [公司]脏标记
     */
    boolean getCompany_idDirtyFlag();

    /**
     * 设备
     */
    Integer getEquipment_id();

    void setEquipment_id(Integer equipment_id);

    /**
     * 获取 [设备]脏标记
     */
    boolean getEquipment_idDirtyFlag();

    /**
     * 阶段
     */
    Integer getStage_id();

    void setStage_id(Integer stage_id);

    /**
     * 获取 [阶段]脏标记
     */
    boolean getStage_idDirtyFlag();

    /**
     * 创建人
     */
    Integer getOwner_user_id();

    void setOwner_user_id(Integer owner_user_id);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getOwner_user_idDirtyFlag();

    /**
     * 类别
     */
    Integer getCategory_id();

    void setCategory_id(Integer category_id);

    /**
     * 获取 [类别]脏标记
     */
    boolean getCategory_idDirtyFlag();

    /**
     * 部门
     */
    Integer getDepartment_id();

    void setDepartment_id(Integer department_id);

    /**
     * 获取 [部门]脏标记
     */
    boolean getDepartment_idDirtyFlag();

    /**
     * 技术员
     */
    Integer getUser_id();

    void setUser_id(Integer user_id);

    /**
     * 获取 [技术员]脏标记
     */
    boolean getUser_idDirtyFlag();

    /**
     * 最后更新人
     */
    Integer getWrite_uid();

    void setWrite_uid(Integer write_uid);

    /**
     * 获取 [最后更新人]脏标记
     */
    boolean getWrite_uidDirtyFlag();

    /**
     * 团队
     */
    Integer getMaintenance_team_id();

    void setMaintenance_team_id(Integer maintenance_team_id);

    /**
     * 获取 [团队]脏标记
     */
    boolean getMaintenance_team_idDirtyFlag();

    /**
     * 创建人
     */
    Integer getCreate_uid();

    void setCreate_uid(Integer create_uid);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uidDirtyFlag();

}
