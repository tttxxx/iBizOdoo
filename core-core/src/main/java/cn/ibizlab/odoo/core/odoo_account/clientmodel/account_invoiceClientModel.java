package cn.ibizlab.odoo.core.odoo_account.clientmodel;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.math.BigInteger;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.util.helper.OdooClientHelper;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang.ArrayUtils;
import org.springframework.util.StringUtils;

/**
 * 接口实体[account_invoice] 对象
 */
public class account_invoiceClientModel implements Serializable{

    /**
     * 安全令牌
     */
    public String access_token;

    @JsonIgnore
    public boolean access_tokenDirtyFlag;
    
    /**
     * 门户访问网址
     */
    public String access_url;

    @JsonIgnore
    public boolean access_urlDirtyFlag;
    
    /**
     * 访问警告
     */
    public String access_warning;

    @JsonIgnore
    public boolean access_warningDirtyFlag;
    
    /**
     * 科目
     */
    public Integer account_id;

    @JsonIgnore
    public boolean account_idDirtyFlag;
    
    /**
     * 科目
     */
    public String account_id_text;

    @JsonIgnore
    public boolean account_id_textDirtyFlag;
    
    /**
     * 下一活动截止日期
     */
    public Timestamp activity_date_deadline;

    @JsonIgnore
    public boolean activity_date_deadlineDirtyFlag;
    
    /**
     * 活动
     */
    public String activity_ids;

    @JsonIgnore
    public boolean activity_idsDirtyFlag;
    
    /**
     * 活动状态
     */
    public String activity_state;

    @JsonIgnore
    public boolean activity_stateDirtyFlag;
    
    /**
     * 下一活动摘要
     */
    public String activity_summary;

    @JsonIgnore
    public boolean activity_summaryDirtyFlag;
    
    /**
     * 下一活动类型
     */
    public Integer activity_type_id;

    @JsonIgnore
    public boolean activity_type_idDirtyFlag;
    
    /**
     * 责任用户
     */
    public Integer activity_user_id;

    @JsonIgnore
    public boolean activity_user_idDirtyFlag;
    
    /**
     * 按组分配税额
     */
    public byte[] amount_by_group;

    @JsonIgnore
    public boolean amount_by_groupDirtyFlag;
    
    /**
     * 税率
     */
    public Double amount_tax;

    @JsonIgnore
    public boolean amount_taxDirtyFlag;
    
    /**
     * Tax in Invoice Currency
     */
    public Double amount_tax_signed;

    @JsonIgnore
    public boolean amount_tax_signedDirtyFlag;
    
    /**
     * 总计
     */
    public Double amount_total;

    @JsonIgnore
    public boolean amount_totalDirtyFlag;
    
    /**
     * 公司货币的合计
     */
    public Double amount_total_company_signed;

    @JsonIgnore
    public boolean amount_total_company_signedDirtyFlag;
    
    /**
     * 以发票币种总计
     */
    public Double amount_total_signed;

    @JsonIgnore
    public boolean amount_total_signedDirtyFlag;
    
    /**
     * 未税金额
     */
    public Double amount_untaxed;

    @JsonIgnore
    public boolean amount_untaxedDirtyFlag;
    
    /**
     * Untaxed Amount in Invoice Currency
     */
    public Double amount_untaxed_invoice_signed;

    @JsonIgnore
    public boolean amount_untaxed_invoice_signedDirtyFlag;
    
    /**
     * 按公司本位币计的不含税金额
     */
    public Double amount_untaxed_signed;

    @JsonIgnore
    public boolean amount_untaxed_signedDirtyFlag;
    
    /**
     * 已授权的交易
     */
    public String authorized_transaction_ids;

    @JsonIgnore
    public boolean authorized_transaction_idsDirtyFlag;
    
    /**
     * 营销
     */
    public Integer campaign_id;

    @JsonIgnore
    public boolean campaign_idDirtyFlag;
    
    /**
     * 营销
     */
    public String campaign_id_text;

    @JsonIgnore
    public boolean campaign_id_textDirtyFlag;
    
    /**
     * 现金舍入方式
     */
    public Integer cash_rounding_id;

    @JsonIgnore
    public boolean cash_rounding_idDirtyFlag;
    
    /**
     * 现金舍入方式
     */
    public String cash_rounding_id_text;

    @JsonIgnore
    public boolean cash_rounding_id_textDirtyFlag;
    
    /**
     * 额外的信息
     */
    public String comment;

    @JsonIgnore
    public boolean commentDirtyFlag;
    
    /**
     * 商业实体
     */
    public Integer commercial_partner_id;

    @JsonIgnore
    public boolean commercial_partner_idDirtyFlag;
    
    /**
     * 商业实体
     */
    public String commercial_partner_id_text;

    @JsonIgnore
    public boolean commercial_partner_id_textDirtyFlag;
    
    /**
     * 公司货币
     */
    public Integer company_currency_id;

    @JsonIgnore
    public boolean company_currency_idDirtyFlag;
    
    /**
     * 公司
     */
    public Integer company_id;

    @JsonIgnore
    public boolean company_idDirtyFlag;
    
    /**
     * 公司
     */
    public String company_id_text;

    @JsonIgnore
    public boolean company_id_textDirtyFlag;
    
    /**
     * 创建时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp create_date;

    @JsonIgnore
    public boolean create_dateDirtyFlag;
    
    /**
     * 创建人
     */
    public Integer create_uid;

    @JsonIgnore
    public boolean create_uidDirtyFlag;
    
    /**
     * 创建人
     */
    public String create_uid_text;

    @JsonIgnore
    public boolean create_uid_textDirtyFlag;
    
    /**
     * 币种
     */
    public Integer currency_id;

    @JsonIgnore
    public boolean currency_idDirtyFlag;
    
    /**
     * 币种
     */
    public String currency_id_text;

    @JsonIgnore
    public boolean currency_id_textDirtyFlag;
    
    /**
     * 会计日期
     */
    public Timestamp date;

    @JsonIgnore
    public boolean dateDirtyFlag;
    
    /**
     * 到期日期
     */
    public Timestamp date_due;

    @JsonIgnore
    public boolean date_dueDirtyFlag;
    
    /**
     * 开票日期
     */
    public Timestamp date_invoice;

    @JsonIgnore
    public boolean date_invoiceDirtyFlag;
    
    /**
     * 显示名称
     */
    public String display_name;

    @JsonIgnore
    public boolean display_nameDirtyFlag;
    
    /**
     * 税科目调整
     */
    public Integer fiscal_position_id;

    @JsonIgnore
    public boolean fiscal_position_idDirtyFlag;
    
    /**
     * 税科目调整
     */
    public String fiscal_position_id_text;

    @JsonIgnore
    public boolean fiscal_position_id_textDirtyFlag;
    
    /**
     * 有未清项
     */
    public String has_outstanding;

    @JsonIgnore
    public boolean has_outstandingDirtyFlag;
    
    /**
     * ID
     */
    public Integer id;

    @JsonIgnore
    public boolean idDirtyFlag;
    
    /**
     * 贸易条款
     */
    public Integer incoterms_id;

    @JsonIgnore
    public boolean incoterms_idDirtyFlag;
    
    /**
     * 贸易条款
     */
    public String incoterms_id_text;

    @JsonIgnore
    public boolean incoterms_id_textDirtyFlag;
    
    /**
     * 国际贸易术语
     */
    public Integer incoterm_id;

    @JsonIgnore
    public boolean incoterm_idDirtyFlag;
    
    /**
     * 国际贸易术语
     */
    public String incoterm_id_text;

    @JsonIgnore
    public boolean incoterm_id_textDirtyFlag;
    
    /**
     * 发票标示
     */
    public String invoice_icon;

    @JsonIgnore
    public boolean invoice_iconDirtyFlag;
    
    /**
     * 发票行
     */
    public String invoice_line_ids;

    @JsonIgnore
    public boolean invoice_line_idsDirtyFlag;
    
    /**
     * 日记账
     */
    public Integer journal_id;

    @JsonIgnore
    public boolean journal_idDirtyFlag;
    
    /**
     * 日记账
     */
    public String journal_id_text;

    @JsonIgnore
    public boolean journal_id_textDirtyFlag;
    
    /**
     * 媒体
     */
    public Integer medium_id;

    @JsonIgnore
    public boolean medium_idDirtyFlag;
    
    /**
     * 媒体
     */
    public String medium_id_text;

    @JsonIgnore
    public boolean medium_id_textDirtyFlag;
    
    /**
     * 附件数量
     */
    public Integer message_attachment_count;

    @JsonIgnore
    public boolean message_attachment_countDirtyFlag;
    
    /**
     * 关注者(渠道)
     */
    public String message_channel_ids;

    @JsonIgnore
    public boolean message_channel_idsDirtyFlag;
    
    /**
     * 关注者
     */
    public String message_follower_ids;

    @JsonIgnore
    public boolean message_follower_idsDirtyFlag;
    
    /**
     * 消息递送错误
     */
    public String message_has_error;

    @JsonIgnore
    public boolean message_has_errorDirtyFlag;
    
    /**
     * 需要一个动作消息的编码
     */
    public Integer message_has_error_counter;

    @JsonIgnore
    public boolean message_has_error_counterDirtyFlag;
    
    /**
     * 消息
     */
    public String message_ids;

    @JsonIgnore
    public boolean message_idsDirtyFlag;
    
    /**
     * 是关注者
     */
    public String message_is_follower;

    @JsonIgnore
    public boolean message_is_followerDirtyFlag;
    
    /**
     * 附件
     */
    public Integer message_main_attachment_id;

    @JsonIgnore
    public boolean message_main_attachment_idDirtyFlag;
    
    /**
     * 需要采取行动
     */
    public String message_needaction;

    @JsonIgnore
    public boolean message_needactionDirtyFlag;
    
    /**
     * 操作编号
     */
    public Integer message_needaction_counter;

    @JsonIgnore
    public boolean message_needaction_counterDirtyFlag;
    
    /**
     * 关注者(业务伙伴)
     */
    public String message_partner_ids;

    @JsonIgnore
    public boolean message_partner_idsDirtyFlag;
    
    /**
     * 未读消息
     */
    public String message_unread;

    @JsonIgnore
    public boolean message_unreadDirtyFlag;
    
    /**
     * 未读消息计数器
     */
    public Integer message_unread_counter;

    @JsonIgnore
    public boolean message_unread_counterDirtyFlag;
    
    /**
     * 日记账分录
     */
    public Integer move_id;

    @JsonIgnore
    public boolean move_idDirtyFlag;
    
    /**
     * 日记账分录名称
     */
    public String move_name;

    @JsonIgnore
    public boolean move_nameDirtyFlag;
    
    /**
     * 参考/说明
     */
    public String name;

    @JsonIgnore
    public boolean nameDirtyFlag;
    
    /**
     * 号码
     */
    public String number;

    @JsonIgnore
    public boolean numberDirtyFlag;
    
    /**
     * 源文档
     */
    public String origin;

    @JsonIgnore
    public boolean originDirtyFlag;
    
    /**
     * 未到期贷项
     */
    public String outstanding_credits_debits_widget;

    @JsonIgnore
    public boolean outstanding_credits_debits_widgetDirtyFlag;
    
    /**
     * 银行账户
     */
    public Integer partner_bank_id;

    @JsonIgnore
    public boolean partner_bank_idDirtyFlag;
    
    /**
     * 业务伙伴
     */
    public Integer partner_id;

    @JsonIgnore
    public boolean partner_idDirtyFlag;
    
    /**
     * 业务伙伴
     */
    public String partner_id_text;

    @JsonIgnore
    public boolean partner_id_textDirtyFlag;
    
    /**
     * 送货地址
     */
    public Integer partner_shipping_id;

    @JsonIgnore
    public boolean partner_shipping_idDirtyFlag;
    
    /**
     * 送货地址
     */
    public String partner_shipping_id_text;

    @JsonIgnore
    public boolean partner_shipping_id_textDirtyFlag;
    
    /**
     * 支付挂件
     */
    public String payments_widget;

    @JsonIgnore
    public boolean payments_widgetDirtyFlag;
    
    /**
     * 付款
     */
    public String payment_ids;

    @JsonIgnore
    public boolean payment_idsDirtyFlag;
    
    /**
     * 付款凭证明细
     */
    public String payment_move_line_ids;

    @JsonIgnore
    public boolean payment_move_line_idsDirtyFlag;
    
    /**
     * 付款条款
     */
    public Integer payment_term_id;

    @JsonIgnore
    public boolean payment_term_idDirtyFlag;
    
    /**
     * 付款条款
     */
    public String payment_term_id_text;

    @JsonIgnore
    public boolean payment_term_id_textDirtyFlag;
    
    /**
     * 添加采购订单
     */
    public Integer purchase_id;

    @JsonIgnore
    public boolean purchase_idDirtyFlag;
    
    /**
     * 添加采购订单
     */
    public String purchase_id_text;

    @JsonIgnore
    public boolean purchase_id_textDirtyFlag;
    
    /**
     * 已付／已核销
     */
    public String reconciled;

    @JsonIgnore
    public boolean reconciledDirtyFlag;
    
    /**
     * 付款参考:
     */
    public String reference;

    @JsonIgnore
    public boolean referenceDirtyFlag;
    
    /**
     * 此发票为信用票的发票
     */
    public Integer refund_invoice_id;

    @JsonIgnore
    public boolean refund_invoice_idDirtyFlag;
    
    /**
     * 退款发票
     */
    public String refund_invoice_ids;

    @JsonIgnore
    public boolean refund_invoice_idsDirtyFlag;
    
    /**
     * 此发票为信用票的发票
     */
    public String refund_invoice_id_text;

    @JsonIgnore
    public boolean refund_invoice_id_textDirtyFlag;
    
    /**
     * 到期金额
     */
    public Double residual;

    @JsonIgnore
    public boolean residualDirtyFlag;
    
    /**
     * 公司使用币种的逾期金额
     */
    public Double residual_company_signed;

    @JsonIgnore
    public boolean residual_company_signedDirtyFlag;
    
    /**
     * 发票使用币种的逾期金额
     */
    public Double residual_signed;

    @JsonIgnore
    public boolean residual_signedDirtyFlag;
    
    /**
     * 已汇
     */
    public String sent;

    @JsonIgnore
    public boolean sentDirtyFlag;
    
    /**
     * 下一号码
     */
    public String sequence_number_next;

    @JsonIgnore
    public boolean sequence_number_nextDirtyFlag;
    
    /**
     * 下一个号码前缀
     */
    public String sequence_number_next_prefix;

    @JsonIgnore
    public boolean sequence_number_next_prefixDirtyFlag;
    
    /**
     * 源邮箱
     */
    public String source_email;

    @JsonIgnore
    public boolean source_emailDirtyFlag;
    
    /**
     * 来源
     */
    public Integer source_id;

    @JsonIgnore
    public boolean source_idDirtyFlag;
    
    /**
     * 来源
     */
    public String source_id_text;

    @JsonIgnore
    public boolean source_id_textDirtyFlag;
    
    /**
     * 状态
     */
    public String state;

    @JsonIgnore
    public boolean stateDirtyFlag;
    
    /**
     * 税率明细行
     */
    public String tax_line_ids;

    @JsonIgnore
    public boolean tax_line_idsDirtyFlag;
    
    /**
     * 销售团队
     */
    public Integer team_id;

    @JsonIgnore
    public boolean team_idDirtyFlag;
    
    /**
     * 销售团队
     */
    public String team_id_text;

    @JsonIgnore
    public boolean team_id_textDirtyFlag;
    
    /**
     * 交易
     */
    public String transaction_ids;

    @JsonIgnore
    public boolean transaction_idsDirtyFlag;
    
    /**
     * 类型
     */
    public String type;

    @JsonIgnore
    public boolean typeDirtyFlag;
    
    /**
     * 销售员
     */
    public Integer user_id;

    @JsonIgnore
    public boolean user_idDirtyFlag;
    
    /**
     * 销售员
     */
    public String user_id_text;

    @JsonIgnore
    public boolean user_id_textDirtyFlag;
    
    /**
     * 供应商账单
     */
    public Integer vendor_bill_id;

    @JsonIgnore
    public boolean vendor_bill_idDirtyFlag;
    
    /**
     * 供应商账单
     */
    public String vendor_bill_id_text;

    @JsonIgnore
    public boolean vendor_bill_id_textDirtyFlag;
    
    /**
     * 自动完成
     */
    public Integer vendor_bill_purchase_id;

    @JsonIgnore
    public boolean vendor_bill_purchase_idDirtyFlag;
    
    /**
     * 自动完成
     */
    public String vendor_bill_purchase_id_text;

    @JsonIgnore
    public boolean vendor_bill_purchase_id_textDirtyFlag;
    
    /**
     * 供应商名称
     */
    public String vendor_display_name;

    @JsonIgnore
    public boolean vendor_display_nameDirtyFlag;
    
    /**
     * 网站
     */
    public Integer website_id;

    @JsonIgnore
    public boolean website_idDirtyFlag;
    
    /**
     * 网站信息
     */
    public String website_message_ids;

    @JsonIgnore
    public boolean website_message_idsDirtyFlag;
    
    /**
     * 最后更新时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp write_date;

    @JsonIgnore
    public boolean write_dateDirtyFlag;
    
    /**
     * 最后更新人
     */
    public Integer write_uid;

    @JsonIgnore
    public boolean write_uidDirtyFlag;
    
    /**
     * 最后更新人
     */
    public String write_uid_text;

    @JsonIgnore
    public boolean write_uid_textDirtyFlag;
    
    /**
     * 最后修改日
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp __last_update;

    @JsonIgnore
    public boolean __last_updateDirtyFlag;
    
    /**
     * 获取 [安全令牌]
     */
    @JsonProperty("access_token")
    public String getAccess_token(){
        return this.access_token ;
    }

    /**
     * 设置 [安全令牌]
     */
    @JsonProperty("access_token")
    public void setAccess_token(String  access_token){
        this.access_token = access_token ;
        this.access_tokenDirtyFlag = true ;
    }

     /**
     * 获取 [安全令牌]脏标记
     */
    @JsonIgnore
    public boolean getAccess_tokenDirtyFlag(){
        return this.access_tokenDirtyFlag ;
    }   

    /**
     * 获取 [门户访问网址]
     */
    @JsonProperty("access_url")
    public String getAccess_url(){
        return this.access_url ;
    }

    /**
     * 设置 [门户访问网址]
     */
    @JsonProperty("access_url")
    public void setAccess_url(String  access_url){
        this.access_url = access_url ;
        this.access_urlDirtyFlag = true ;
    }

     /**
     * 获取 [门户访问网址]脏标记
     */
    @JsonIgnore
    public boolean getAccess_urlDirtyFlag(){
        return this.access_urlDirtyFlag ;
    }   

    /**
     * 获取 [访问警告]
     */
    @JsonProperty("access_warning")
    public String getAccess_warning(){
        return this.access_warning ;
    }

    /**
     * 设置 [访问警告]
     */
    @JsonProperty("access_warning")
    public void setAccess_warning(String  access_warning){
        this.access_warning = access_warning ;
        this.access_warningDirtyFlag = true ;
    }

     /**
     * 获取 [访问警告]脏标记
     */
    @JsonIgnore
    public boolean getAccess_warningDirtyFlag(){
        return this.access_warningDirtyFlag ;
    }   

    /**
     * 获取 [科目]
     */
    @JsonProperty("account_id")
    public Integer getAccount_id(){
        return this.account_id ;
    }

    /**
     * 设置 [科目]
     */
    @JsonProperty("account_id")
    public void setAccount_id(Integer  account_id){
        this.account_id = account_id ;
        this.account_idDirtyFlag = true ;
    }

     /**
     * 获取 [科目]脏标记
     */
    @JsonIgnore
    public boolean getAccount_idDirtyFlag(){
        return this.account_idDirtyFlag ;
    }   

    /**
     * 获取 [科目]
     */
    @JsonProperty("account_id_text")
    public String getAccount_id_text(){
        return this.account_id_text ;
    }

    /**
     * 设置 [科目]
     */
    @JsonProperty("account_id_text")
    public void setAccount_id_text(String  account_id_text){
        this.account_id_text = account_id_text ;
        this.account_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [科目]脏标记
     */
    @JsonIgnore
    public boolean getAccount_id_textDirtyFlag(){
        return this.account_id_textDirtyFlag ;
    }   

    /**
     * 获取 [下一活动截止日期]
     */
    @JsonProperty("activity_date_deadline")
    public Timestamp getActivity_date_deadline(){
        return this.activity_date_deadline ;
    }

    /**
     * 设置 [下一活动截止日期]
     */
    @JsonProperty("activity_date_deadline")
    public void setActivity_date_deadline(Timestamp  activity_date_deadline){
        this.activity_date_deadline = activity_date_deadline ;
        this.activity_date_deadlineDirtyFlag = true ;
    }

     /**
     * 获取 [下一活动截止日期]脏标记
     */
    @JsonIgnore
    public boolean getActivity_date_deadlineDirtyFlag(){
        return this.activity_date_deadlineDirtyFlag ;
    }   

    /**
     * 获取 [活动]
     */
    @JsonProperty("activity_ids")
    public String getActivity_ids(){
        return this.activity_ids ;
    }

    /**
     * 设置 [活动]
     */
    @JsonProperty("activity_ids")
    public void setActivity_ids(String  activity_ids){
        this.activity_ids = activity_ids ;
        this.activity_idsDirtyFlag = true ;
    }

     /**
     * 获取 [活动]脏标记
     */
    @JsonIgnore
    public boolean getActivity_idsDirtyFlag(){
        return this.activity_idsDirtyFlag ;
    }   

    /**
     * 获取 [活动状态]
     */
    @JsonProperty("activity_state")
    public String getActivity_state(){
        return this.activity_state ;
    }

    /**
     * 设置 [活动状态]
     */
    @JsonProperty("activity_state")
    public void setActivity_state(String  activity_state){
        this.activity_state = activity_state ;
        this.activity_stateDirtyFlag = true ;
    }

     /**
     * 获取 [活动状态]脏标记
     */
    @JsonIgnore
    public boolean getActivity_stateDirtyFlag(){
        return this.activity_stateDirtyFlag ;
    }   

    /**
     * 获取 [下一活动摘要]
     */
    @JsonProperty("activity_summary")
    public String getActivity_summary(){
        return this.activity_summary ;
    }

    /**
     * 设置 [下一活动摘要]
     */
    @JsonProperty("activity_summary")
    public void setActivity_summary(String  activity_summary){
        this.activity_summary = activity_summary ;
        this.activity_summaryDirtyFlag = true ;
    }

     /**
     * 获取 [下一活动摘要]脏标记
     */
    @JsonIgnore
    public boolean getActivity_summaryDirtyFlag(){
        return this.activity_summaryDirtyFlag ;
    }   

    /**
     * 获取 [下一活动类型]
     */
    @JsonProperty("activity_type_id")
    public Integer getActivity_type_id(){
        return this.activity_type_id ;
    }

    /**
     * 设置 [下一活动类型]
     */
    @JsonProperty("activity_type_id")
    public void setActivity_type_id(Integer  activity_type_id){
        this.activity_type_id = activity_type_id ;
        this.activity_type_idDirtyFlag = true ;
    }

     /**
     * 获取 [下一活动类型]脏标记
     */
    @JsonIgnore
    public boolean getActivity_type_idDirtyFlag(){
        return this.activity_type_idDirtyFlag ;
    }   

    /**
     * 获取 [责任用户]
     */
    @JsonProperty("activity_user_id")
    public Integer getActivity_user_id(){
        return this.activity_user_id ;
    }

    /**
     * 设置 [责任用户]
     */
    @JsonProperty("activity_user_id")
    public void setActivity_user_id(Integer  activity_user_id){
        this.activity_user_id = activity_user_id ;
        this.activity_user_idDirtyFlag = true ;
    }

     /**
     * 获取 [责任用户]脏标记
     */
    @JsonIgnore
    public boolean getActivity_user_idDirtyFlag(){
        return this.activity_user_idDirtyFlag ;
    }   

    /**
     * 获取 [按组分配税额]
     */
    @JsonProperty("amount_by_group")
    public byte[] getAmount_by_group(){
        return this.amount_by_group ;
    }

    /**
     * 设置 [按组分配税额]
     */
    @JsonProperty("amount_by_group")
    public void setAmount_by_group(byte[]  amount_by_group){
        this.amount_by_group = amount_by_group ;
        this.amount_by_groupDirtyFlag = true ;
    }

     /**
     * 获取 [按组分配税额]脏标记
     */
    @JsonIgnore
    public boolean getAmount_by_groupDirtyFlag(){
        return this.amount_by_groupDirtyFlag ;
    }   

    /**
     * 获取 [税率]
     */
    @JsonProperty("amount_tax")
    public Double getAmount_tax(){
        return this.amount_tax ;
    }

    /**
     * 设置 [税率]
     */
    @JsonProperty("amount_tax")
    public void setAmount_tax(Double  amount_tax){
        this.amount_tax = amount_tax ;
        this.amount_taxDirtyFlag = true ;
    }

     /**
     * 获取 [税率]脏标记
     */
    @JsonIgnore
    public boolean getAmount_taxDirtyFlag(){
        return this.amount_taxDirtyFlag ;
    }   

    /**
     * 获取 [Tax in Invoice Currency]
     */
    @JsonProperty("amount_tax_signed")
    public Double getAmount_tax_signed(){
        return this.amount_tax_signed ;
    }

    /**
     * 设置 [Tax in Invoice Currency]
     */
    @JsonProperty("amount_tax_signed")
    public void setAmount_tax_signed(Double  amount_tax_signed){
        this.amount_tax_signed = amount_tax_signed ;
        this.amount_tax_signedDirtyFlag = true ;
    }

     /**
     * 获取 [Tax in Invoice Currency]脏标记
     */
    @JsonIgnore
    public boolean getAmount_tax_signedDirtyFlag(){
        return this.amount_tax_signedDirtyFlag ;
    }   

    /**
     * 获取 [总计]
     */
    @JsonProperty("amount_total")
    public Double getAmount_total(){
        return this.amount_total ;
    }

    /**
     * 设置 [总计]
     */
    @JsonProperty("amount_total")
    public void setAmount_total(Double  amount_total){
        this.amount_total = amount_total ;
        this.amount_totalDirtyFlag = true ;
    }

     /**
     * 获取 [总计]脏标记
     */
    @JsonIgnore
    public boolean getAmount_totalDirtyFlag(){
        return this.amount_totalDirtyFlag ;
    }   

    /**
     * 获取 [公司货币的合计]
     */
    @JsonProperty("amount_total_company_signed")
    public Double getAmount_total_company_signed(){
        return this.amount_total_company_signed ;
    }

    /**
     * 设置 [公司货币的合计]
     */
    @JsonProperty("amount_total_company_signed")
    public void setAmount_total_company_signed(Double  amount_total_company_signed){
        this.amount_total_company_signed = amount_total_company_signed ;
        this.amount_total_company_signedDirtyFlag = true ;
    }

     /**
     * 获取 [公司货币的合计]脏标记
     */
    @JsonIgnore
    public boolean getAmount_total_company_signedDirtyFlag(){
        return this.amount_total_company_signedDirtyFlag ;
    }   

    /**
     * 获取 [以发票币种总计]
     */
    @JsonProperty("amount_total_signed")
    public Double getAmount_total_signed(){
        return this.amount_total_signed ;
    }

    /**
     * 设置 [以发票币种总计]
     */
    @JsonProperty("amount_total_signed")
    public void setAmount_total_signed(Double  amount_total_signed){
        this.amount_total_signed = amount_total_signed ;
        this.amount_total_signedDirtyFlag = true ;
    }

     /**
     * 获取 [以发票币种总计]脏标记
     */
    @JsonIgnore
    public boolean getAmount_total_signedDirtyFlag(){
        return this.amount_total_signedDirtyFlag ;
    }   

    /**
     * 获取 [未税金额]
     */
    @JsonProperty("amount_untaxed")
    public Double getAmount_untaxed(){
        return this.amount_untaxed ;
    }

    /**
     * 设置 [未税金额]
     */
    @JsonProperty("amount_untaxed")
    public void setAmount_untaxed(Double  amount_untaxed){
        this.amount_untaxed = amount_untaxed ;
        this.amount_untaxedDirtyFlag = true ;
    }

     /**
     * 获取 [未税金额]脏标记
     */
    @JsonIgnore
    public boolean getAmount_untaxedDirtyFlag(){
        return this.amount_untaxedDirtyFlag ;
    }   

    /**
     * 获取 [Untaxed Amount in Invoice Currency]
     */
    @JsonProperty("amount_untaxed_invoice_signed")
    public Double getAmount_untaxed_invoice_signed(){
        return this.amount_untaxed_invoice_signed ;
    }

    /**
     * 设置 [Untaxed Amount in Invoice Currency]
     */
    @JsonProperty("amount_untaxed_invoice_signed")
    public void setAmount_untaxed_invoice_signed(Double  amount_untaxed_invoice_signed){
        this.amount_untaxed_invoice_signed = amount_untaxed_invoice_signed ;
        this.amount_untaxed_invoice_signedDirtyFlag = true ;
    }

     /**
     * 获取 [Untaxed Amount in Invoice Currency]脏标记
     */
    @JsonIgnore
    public boolean getAmount_untaxed_invoice_signedDirtyFlag(){
        return this.amount_untaxed_invoice_signedDirtyFlag ;
    }   

    /**
     * 获取 [按公司本位币计的不含税金额]
     */
    @JsonProperty("amount_untaxed_signed")
    public Double getAmount_untaxed_signed(){
        return this.amount_untaxed_signed ;
    }

    /**
     * 设置 [按公司本位币计的不含税金额]
     */
    @JsonProperty("amount_untaxed_signed")
    public void setAmount_untaxed_signed(Double  amount_untaxed_signed){
        this.amount_untaxed_signed = amount_untaxed_signed ;
        this.amount_untaxed_signedDirtyFlag = true ;
    }

     /**
     * 获取 [按公司本位币计的不含税金额]脏标记
     */
    @JsonIgnore
    public boolean getAmount_untaxed_signedDirtyFlag(){
        return this.amount_untaxed_signedDirtyFlag ;
    }   

    /**
     * 获取 [已授权的交易]
     */
    @JsonProperty("authorized_transaction_ids")
    public String getAuthorized_transaction_ids(){
        return this.authorized_transaction_ids ;
    }

    /**
     * 设置 [已授权的交易]
     */
    @JsonProperty("authorized_transaction_ids")
    public void setAuthorized_transaction_ids(String  authorized_transaction_ids){
        this.authorized_transaction_ids = authorized_transaction_ids ;
        this.authorized_transaction_idsDirtyFlag = true ;
    }

     /**
     * 获取 [已授权的交易]脏标记
     */
    @JsonIgnore
    public boolean getAuthorized_transaction_idsDirtyFlag(){
        return this.authorized_transaction_idsDirtyFlag ;
    }   

    /**
     * 获取 [营销]
     */
    @JsonProperty("campaign_id")
    public Integer getCampaign_id(){
        return this.campaign_id ;
    }

    /**
     * 设置 [营销]
     */
    @JsonProperty("campaign_id")
    public void setCampaign_id(Integer  campaign_id){
        this.campaign_id = campaign_id ;
        this.campaign_idDirtyFlag = true ;
    }

     /**
     * 获取 [营销]脏标记
     */
    @JsonIgnore
    public boolean getCampaign_idDirtyFlag(){
        return this.campaign_idDirtyFlag ;
    }   

    /**
     * 获取 [营销]
     */
    @JsonProperty("campaign_id_text")
    public String getCampaign_id_text(){
        return this.campaign_id_text ;
    }

    /**
     * 设置 [营销]
     */
    @JsonProperty("campaign_id_text")
    public void setCampaign_id_text(String  campaign_id_text){
        this.campaign_id_text = campaign_id_text ;
        this.campaign_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [营销]脏标记
     */
    @JsonIgnore
    public boolean getCampaign_id_textDirtyFlag(){
        return this.campaign_id_textDirtyFlag ;
    }   

    /**
     * 获取 [现金舍入方式]
     */
    @JsonProperty("cash_rounding_id")
    public Integer getCash_rounding_id(){
        return this.cash_rounding_id ;
    }

    /**
     * 设置 [现金舍入方式]
     */
    @JsonProperty("cash_rounding_id")
    public void setCash_rounding_id(Integer  cash_rounding_id){
        this.cash_rounding_id = cash_rounding_id ;
        this.cash_rounding_idDirtyFlag = true ;
    }

     /**
     * 获取 [现金舍入方式]脏标记
     */
    @JsonIgnore
    public boolean getCash_rounding_idDirtyFlag(){
        return this.cash_rounding_idDirtyFlag ;
    }   

    /**
     * 获取 [现金舍入方式]
     */
    @JsonProperty("cash_rounding_id_text")
    public String getCash_rounding_id_text(){
        return this.cash_rounding_id_text ;
    }

    /**
     * 设置 [现金舍入方式]
     */
    @JsonProperty("cash_rounding_id_text")
    public void setCash_rounding_id_text(String  cash_rounding_id_text){
        this.cash_rounding_id_text = cash_rounding_id_text ;
        this.cash_rounding_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [现金舍入方式]脏标记
     */
    @JsonIgnore
    public boolean getCash_rounding_id_textDirtyFlag(){
        return this.cash_rounding_id_textDirtyFlag ;
    }   

    /**
     * 获取 [额外的信息]
     */
    @JsonProperty("comment")
    public String getComment(){
        return this.comment ;
    }

    /**
     * 设置 [额外的信息]
     */
    @JsonProperty("comment")
    public void setComment(String  comment){
        this.comment = comment ;
        this.commentDirtyFlag = true ;
    }

     /**
     * 获取 [额外的信息]脏标记
     */
    @JsonIgnore
    public boolean getCommentDirtyFlag(){
        return this.commentDirtyFlag ;
    }   

    /**
     * 获取 [商业实体]
     */
    @JsonProperty("commercial_partner_id")
    public Integer getCommercial_partner_id(){
        return this.commercial_partner_id ;
    }

    /**
     * 设置 [商业实体]
     */
    @JsonProperty("commercial_partner_id")
    public void setCommercial_partner_id(Integer  commercial_partner_id){
        this.commercial_partner_id = commercial_partner_id ;
        this.commercial_partner_idDirtyFlag = true ;
    }

     /**
     * 获取 [商业实体]脏标记
     */
    @JsonIgnore
    public boolean getCommercial_partner_idDirtyFlag(){
        return this.commercial_partner_idDirtyFlag ;
    }   

    /**
     * 获取 [商业实体]
     */
    @JsonProperty("commercial_partner_id_text")
    public String getCommercial_partner_id_text(){
        return this.commercial_partner_id_text ;
    }

    /**
     * 设置 [商业实体]
     */
    @JsonProperty("commercial_partner_id_text")
    public void setCommercial_partner_id_text(String  commercial_partner_id_text){
        this.commercial_partner_id_text = commercial_partner_id_text ;
        this.commercial_partner_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [商业实体]脏标记
     */
    @JsonIgnore
    public boolean getCommercial_partner_id_textDirtyFlag(){
        return this.commercial_partner_id_textDirtyFlag ;
    }   

    /**
     * 获取 [公司货币]
     */
    @JsonProperty("company_currency_id")
    public Integer getCompany_currency_id(){
        return this.company_currency_id ;
    }

    /**
     * 设置 [公司货币]
     */
    @JsonProperty("company_currency_id")
    public void setCompany_currency_id(Integer  company_currency_id){
        this.company_currency_id = company_currency_id ;
        this.company_currency_idDirtyFlag = true ;
    }

     /**
     * 获取 [公司货币]脏标记
     */
    @JsonIgnore
    public boolean getCompany_currency_idDirtyFlag(){
        return this.company_currency_idDirtyFlag ;
    }   

    /**
     * 获取 [公司]
     */
    @JsonProperty("company_id")
    public Integer getCompany_id(){
        return this.company_id ;
    }

    /**
     * 设置 [公司]
     */
    @JsonProperty("company_id")
    public void setCompany_id(Integer  company_id){
        this.company_id = company_id ;
        this.company_idDirtyFlag = true ;
    }

     /**
     * 获取 [公司]脏标记
     */
    @JsonIgnore
    public boolean getCompany_idDirtyFlag(){
        return this.company_idDirtyFlag ;
    }   

    /**
     * 获取 [公司]
     */
    @JsonProperty("company_id_text")
    public String getCompany_id_text(){
        return this.company_id_text ;
    }

    /**
     * 设置 [公司]
     */
    @JsonProperty("company_id_text")
    public void setCompany_id_text(String  company_id_text){
        this.company_id_text = company_id_text ;
        this.company_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [公司]脏标记
     */
    @JsonIgnore
    public boolean getCompany_id_textDirtyFlag(){
        return this.company_id_textDirtyFlag ;
    }   

    /**
     * 获取 [创建时间]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return this.create_date ;
    }

    /**
     * 设置 [创建时间]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

     /**
     * 获取 [创建时间]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return this.create_dateDirtyFlag ;
    }   

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return this.create_uid ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

     /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return this.create_uidDirtyFlag ;
    }   

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return this.create_uid_text ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

     /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return this.create_uid_textDirtyFlag ;
    }   

    /**
     * 获取 [币种]
     */
    @JsonProperty("currency_id")
    public Integer getCurrency_id(){
        return this.currency_id ;
    }

    /**
     * 设置 [币种]
     */
    @JsonProperty("currency_id")
    public void setCurrency_id(Integer  currency_id){
        this.currency_id = currency_id ;
        this.currency_idDirtyFlag = true ;
    }

     /**
     * 获取 [币种]脏标记
     */
    @JsonIgnore
    public boolean getCurrency_idDirtyFlag(){
        return this.currency_idDirtyFlag ;
    }   

    /**
     * 获取 [币种]
     */
    @JsonProperty("currency_id_text")
    public String getCurrency_id_text(){
        return this.currency_id_text ;
    }

    /**
     * 设置 [币种]
     */
    @JsonProperty("currency_id_text")
    public void setCurrency_id_text(String  currency_id_text){
        this.currency_id_text = currency_id_text ;
        this.currency_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [币种]脏标记
     */
    @JsonIgnore
    public boolean getCurrency_id_textDirtyFlag(){
        return this.currency_id_textDirtyFlag ;
    }   

    /**
     * 获取 [会计日期]
     */
    @JsonProperty("date")
    public Timestamp getDate(){
        return this.date ;
    }

    /**
     * 设置 [会计日期]
     */
    @JsonProperty("date")
    public void setDate(Timestamp  date){
        this.date = date ;
        this.dateDirtyFlag = true ;
    }

     /**
     * 获取 [会计日期]脏标记
     */
    @JsonIgnore
    public boolean getDateDirtyFlag(){
        return this.dateDirtyFlag ;
    }   

    /**
     * 获取 [到期日期]
     */
    @JsonProperty("date_due")
    public Timestamp getDate_due(){
        return this.date_due ;
    }

    /**
     * 设置 [到期日期]
     */
    @JsonProperty("date_due")
    public void setDate_due(Timestamp  date_due){
        this.date_due = date_due ;
        this.date_dueDirtyFlag = true ;
    }

     /**
     * 获取 [到期日期]脏标记
     */
    @JsonIgnore
    public boolean getDate_dueDirtyFlag(){
        return this.date_dueDirtyFlag ;
    }   

    /**
     * 获取 [开票日期]
     */
    @JsonProperty("date_invoice")
    public Timestamp getDate_invoice(){
        return this.date_invoice ;
    }

    /**
     * 设置 [开票日期]
     */
    @JsonProperty("date_invoice")
    public void setDate_invoice(Timestamp  date_invoice){
        this.date_invoice = date_invoice ;
        this.date_invoiceDirtyFlag = true ;
    }

     /**
     * 获取 [开票日期]脏标记
     */
    @JsonIgnore
    public boolean getDate_invoiceDirtyFlag(){
        return this.date_invoiceDirtyFlag ;
    }   

    /**
     * 获取 [显示名称]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return this.display_name ;
    }

    /**
     * 设置 [显示名称]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

     /**
     * 获取 [显示名称]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return this.display_nameDirtyFlag ;
    }   

    /**
     * 获取 [税科目调整]
     */
    @JsonProperty("fiscal_position_id")
    public Integer getFiscal_position_id(){
        return this.fiscal_position_id ;
    }

    /**
     * 设置 [税科目调整]
     */
    @JsonProperty("fiscal_position_id")
    public void setFiscal_position_id(Integer  fiscal_position_id){
        this.fiscal_position_id = fiscal_position_id ;
        this.fiscal_position_idDirtyFlag = true ;
    }

     /**
     * 获取 [税科目调整]脏标记
     */
    @JsonIgnore
    public boolean getFiscal_position_idDirtyFlag(){
        return this.fiscal_position_idDirtyFlag ;
    }   

    /**
     * 获取 [税科目调整]
     */
    @JsonProperty("fiscal_position_id_text")
    public String getFiscal_position_id_text(){
        return this.fiscal_position_id_text ;
    }

    /**
     * 设置 [税科目调整]
     */
    @JsonProperty("fiscal_position_id_text")
    public void setFiscal_position_id_text(String  fiscal_position_id_text){
        this.fiscal_position_id_text = fiscal_position_id_text ;
        this.fiscal_position_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [税科目调整]脏标记
     */
    @JsonIgnore
    public boolean getFiscal_position_id_textDirtyFlag(){
        return this.fiscal_position_id_textDirtyFlag ;
    }   

    /**
     * 获取 [有未清项]
     */
    @JsonProperty("has_outstanding")
    public String getHas_outstanding(){
        return this.has_outstanding ;
    }

    /**
     * 设置 [有未清项]
     */
    @JsonProperty("has_outstanding")
    public void setHas_outstanding(String  has_outstanding){
        this.has_outstanding = has_outstanding ;
        this.has_outstandingDirtyFlag = true ;
    }

     /**
     * 获取 [有未清项]脏标记
     */
    @JsonIgnore
    public boolean getHas_outstandingDirtyFlag(){
        return this.has_outstandingDirtyFlag ;
    }   

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return this.id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

     /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return this.idDirtyFlag ;
    }   

    /**
     * 获取 [贸易条款]
     */
    @JsonProperty("incoterms_id")
    public Integer getIncoterms_id(){
        return this.incoterms_id ;
    }

    /**
     * 设置 [贸易条款]
     */
    @JsonProperty("incoterms_id")
    public void setIncoterms_id(Integer  incoterms_id){
        this.incoterms_id = incoterms_id ;
        this.incoterms_idDirtyFlag = true ;
    }

     /**
     * 获取 [贸易条款]脏标记
     */
    @JsonIgnore
    public boolean getIncoterms_idDirtyFlag(){
        return this.incoterms_idDirtyFlag ;
    }   

    /**
     * 获取 [贸易条款]
     */
    @JsonProperty("incoterms_id_text")
    public String getIncoterms_id_text(){
        return this.incoterms_id_text ;
    }

    /**
     * 设置 [贸易条款]
     */
    @JsonProperty("incoterms_id_text")
    public void setIncoterms_id_text(String  incoterms_id_text){
        this.incoterms_id_text = incoterms_id_text ;
        this.incoterms_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [贸易条款]脏标记
     */
    @JsonIgnore
    public boolean getIncoterms_id_textDirtyFlag(){
        return this.incoterms_id_textDirtyFlag ;
    }   

    /**
     * 获取 [国际贸易术语]
     */
    @JsonProperty("incoterm_id")
    public Integer getIncoterm_id(){
        return this.incoterm_id ;
    }

    /**
     * 设置 [国际贸易术语]
     */
    @JsonProperty("incoterm_id")
    public void setIncoterm_id(Integer  incoterm_id){
        this.incoterm_id = incoterm_id ;
        this.incoterm_idDirtyFlag = true ;
    }

     /**
     * 获取 [国际贸易术语]脏标记
     */
    @JsonIgnore
    public boolean getIncoterm_idDirtyFlag(){
        return this.incoterm_idDirtyFlag ;
    }   

    /**
     * 获取 [国际贸易术语]
     */
    @JsonProperty("incoterm_id_text")
    public String getIncoterm_id_text(){
        return this.incoterm_id_text ;
    }

    /**
     * 设置 [国际贸易术语]
     */
    @JsonProperty("incoterm_id_text")
    public void setIncoterm_id_text(String  incoterm_id_text){
        this.incoterm_id_text = incoterm_id_text ;
        this.incoterm_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [国际贸易术语]脏标记
     */
    @JsonIgnore
    public boolean getIncoterm_id_textDirtyFlag(){
        return this.incoterm_id_textDirtyFlag ;
    }   

    /**
     * 获取 [发票标示]
     */
    @JsonProperty("invoice_icon")
    public String getInvoice_icon(){
        return this.invoice_icon ;
    }

    /**
     * 设置 [发票标示]
     */
    @JsonProperty("invoice_icon")
    public void setInvoice_icon(String  invoice_icon){
        this.invoice_icon = invoice_icon ;
        this.invoice_iconDirtyFlag = true ;
    }

     /**
     * 获取 [发票标示]脏标记
     */
    @JsonIgnore
    public boolean getInvoice_iconDirtyFlag(){
        return this.invoice_iconDirtyFlag ;
    }   

    /**
     * 获取 [发票行]
     */
    @JsonProperty("invoice_line_ids")
    public String getInvoice_line_ids(){
        return this.invoice_line_ids ;
    }

    /**
     * 设置 [发票行]
     */
    @JsonProperty("invoice_line_ids")
    public void setInvoice_line_ids(String  invoice_line_ids){
        this.invoice_line_ids = invoice_line_ids ;
        this.invoice_line_idsDirtyFlag = true ;
    }

     /**
     * 获取 [发票行]脏标记
     */
    @JsonIgnore
    public boolean getInvoice_line_idsDirtyFlag(){
        return this.invoice_line_idsDirtyFlag ;
    }   

    /**
     * 获取 [日记账]
     */
    @JsonProperty("journal_id")
    public Integer getJournal_id(){
        return this.journal_id ;
    }

    /**
     * 设置 [日记账]
     */
    @JsonProperty("journal_id")
    public void setJournal_id(Integer  journal_id){
        this.journal_id = journal_id ;
        this.journal_idDirtyFlag = true ;
    }

     /**
     * 获取 [日记账]脏标记
     */
    @JsonIgnore
    public boolean getJournal_idDirtyFlag(){
        return this.journal_idDirtyFlag ;
    }   

    /**
     * 获取 [日记账]
     */
    @JsonProperty("journal_id_text")
    public String getJournal_id_text(){
        return this.journal_id_text ;
    }

    /**
     * 设置 [日记账]
     */
    @JsonProperty("journal_id_text")
    public void setJournal_id_text(String  journal_id_text){
        this.journal_id_text = journal_id_text ;
        this.journal_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [日记账]脏标记
     */
    @JsonIgnore
    public boolean getJournal_id_textDirtyFlag(){
        return this.journal_id_textDirtyFlag ;
    }   

    /**
     * 获取 [媒体]
     */
    @JsonProperty("medium_id")
    public Integer getMedium_id(){
        return this.medium_id ;
    }

    /**
     * 设置 [媒体]
     */
    @JsonProperty("medium_id")
    public void setMedium_id(Integer  medium_id){
        this.medium_id = medium_id ;
        this.medium_idDirtyFlag = true ;
    }

     /**
     * 获取 [媒体]脏标记
     */
    @JsonIgnore
    public boolean getMedium_idDirtyFlag(){
        return this.medium_idDirtyFlag ;
    }   

    /**
     * 获取 [媒体]
     */
    @JsonProperty("medium_id_text")
    public String getMedium_id_text(){
        return this.medium_id_text ;
    }

    /**
     * 设置 [媒体]
     */
    @JsonProperty("medium_id_text")
    public void setMedium_id_text(String  medium_id_text){
        this.medium_id_text = medium_id_text ;
        this.medium_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [媒体]脏标记
     */
    @JsonIgnore
    public boolean getMedium_id_textDirtyFlag(){
        return this.medium_id_textDirtyFlag ;
    }   

    /**
     * 获取 [附件数量]
     */
    @JsonProperty("message_attachment_count")
    public Integer getMessage_attachment_count(){
        return this.message_attachment_count ;
    }

    /**
     * 设置 [附件数量]
     */
    @JsonProperty("message_attachment_count")
    public void setMessage_attachment_count(Integer  message_attachment_count){
        this.message_attachment_count = message_attachment_count ;
        this.message_attachment_countDirtyFlag = true ;
    }

     /**
     * 获取 [附件数量]脏标记
     */
    @JsonIgnore
    public boolean getMessage_attachment_countDirtyFlag(){
        return this.message_attachment_countDirtyFlag ;
    }   

    /**
     * 获取 [关注者(渠道)]
     */
    @JsonProperty("message_channel_ids")
    public String getMessage_channel_ids(){
        return this.message_channel_ids ;
    }

    /**
     * 设置 [关注者(渠道)]
     */
    @JsonProperty("message_channel_ids")
    public void setMessage_channel_ids(String  message_channel_ids){
        this.message_channel_ids = message_channel_ids ;
        this.message_channel_idsDirtyFlag = true ;
    }

     /**
     * 获取 [关注者(渠道)]脏标记
     */
    @JsonIgnore
    public boolean getMessage_channel_idsDirtyFlag(){
        return this.message_channel_idsDirtyFlag ;
    }   

    /**
     * 获取 [关注者]
     */
    @JsonProperty("message_follower_ids")
    public String getMessage_follower_ids(){
        return this.message_follower_ids ;
    }

    /**
     * 设置 [关注者]
     */
    @JsonProperty("message_follower_ids")
    public void setMessage_follower_ids(String  message_follower_ids){
        this.message_follower_ids = message_follower_ids ;
        this.message_follower_idsDirtyFlag = true ;
    }

     /**
     * 获取 [关注者]脏标记
     */
    @JsonIgnore
    public boolean getMessage_follower_idsDirtyFlag(){
        return this.message_follower_idsDirtyFlag ;
    }   

    /**
     * 获取 [消息递送错误]
     */
    @JsonProperty("message_has_error")
    public String getMessage_has_error(){
        return this.message_has_error ;
    }

    /**
     * 设置 [消息递送错误]
     */
    @JsonProperty("message_has_error")
    public void setMessage_has_error(String  message_has_error){
        this.message_has_error = message_has_error ;
        this.message_has_errorDirtyFlag = true ;
    }

     /**
     * 获取 [消息递送错误]脏标记
     */
    @JsonIgnore
    public boolean getMessage_has_errorDirtyFlag(){
        return this.message_has_errorDirtyFlag ;
    }   

    /**
     * 获取 [需要一个动作消息的编码]
     */
    @JsonProperty("message_has_error_counter")
    public Integer getMessage_has_error_counter(){
        return this.message_has_error_counter ;
    }

    /**
     * 设置 [需要一个动作消息的编码]
     */
    @JsonProperty("message_has_error_counter")
    public void setMessage_has_error_counter(Integer  message_has_error_counter){
        this.message_has_error_counter = message_has_error_counter ;
        this.message_has_error_counterDirtyFlag = true ;
    }

     /**
     * 获取 [需要一个动作消息的编码]脏标记
     */
    @JsonIgnore
    public boolean getMessage_has_error_counterDirtyFlag(){
        return this.message_has_error_counterDirtyFlag ;
    }   

    /**
     * 获取 [消息]
     */
    @JsonProperty("message_ids")
    public String getMessage_ids(){
        return this.message_ids ;
    }

    /**
     * 设置 [消息]
     */
    @JsonProperty("message_ids")
    public void setMessage_ids(String  message_ids){
        this.message_ids = message_ids ;
        this.message_idsDirtyFlag = true ;
    }

     /**
     * 获取 [消息]脏标记
     */
    @JsonIgnore
    public boolean getMessage_idsDirtyFlag(){
        return this.message_idsDirtyFlag ;
    }   

    /**
     * 获取 [是关注者]
     */
    @JsonProperty("message_is_follower")
    public String getMessage_is_follower(){
        return this.message_is_follower ;
    }

    /**
     * 设置 [是关注者]
     */
    @JsonProperty("message_is_follower")
    public void setMessage_is_follower(String  message_is_follower){
        this.message_is_follower = message_is_follower ;
        this.message_is_followerDirtyFlag = true ;
    }

     /**
     * 获取 [是关注者]脏标记
     */
    @JsonIgnore
    public boolean getMessage_is_followerDirtyFlag(){
        return this.message_is_followerDirtyFlag ;
    }   

    /**
     * 获取 [附件]
     */
    @JsonProperty("message_main_attachment_id")
    public Integer getMessage_main_attachment_id(){
        return this.message_main_attachment_id ;
    }

    /**
     * 设置 [附件]
     */
    @JsonProperty("message_main_attachment_id")
    public void setMessage_main_attachment_id(Integer  message_main_attachment_id){
        this.message_main_attachment_id = message_main_attachment_id ;
        this.message_main_attachment_idDirtyFlag = true ;
    }

     /**
     * 获取 [附件]脏标记
     */
    @JsonIgnore
    public boolean getMessage_main_attachment_idDirtyFlag(){
        return this.message_main_attachment_idDirtyFlag ;
    }   

    /**
     * 获取 [需要采取行动]
     */
    @JsonProperty("message_needaction")
    public String getMessage_needaction(){
        return this.message_needaction ;
    }

    /**
     * 设置 [需要采取行动]
     */
    @JsonProperty("message_needaction")
    public void setMessage_needaction(String  message_needaction){
        this.message_needaction = message_needaction ;
        this.message_needactionDirtyFlag = true ;
    }

     /**
     * 获取 [需要采取行动]脏标记
     */
    @JsonIgnore
    public boolean getMessage_needactionDirtyFlag(){
        return this.message_needactionDirtyFlag ;
    }   

    /**
     * 获取 [操作编号]
     */
    @JsonProperty("message_needaction_counter")
    public Integer getMessage_needaction_counter(){
        return this.message_needaction_counter ;
    }

    /**
     * 设置 [操作编号]
     */
    @JsonProperty("message_needaction_counter")
    public void setMessage_needaction_counter(Integer  message_needaction_counter){
        this.message_needaction_counter = message_needaction_counter ;
        this.message_needaction_counterDirtyFlag = true ;
    }

     /**
     * 获取 [操作编号]脏标记
     */
    @JsonIgnore
    public boolean getMessage_needaction_counterDirtyFlag(){
        return this.message_needaction_counterDirtyFlag ;
    }   

    /**
     * 获取 [关注者(业务伙伴)]
     */
    @JsonProperty("message_partner_ids")
    public String getMessage_partner_ids(){
        return this.message_partner_ids ;
    }

    /**
     * 设置 [关注者(业务伙伴)]
     */
    @JsonProperty("message_partner_ids")
    public void setMessage_partner_ids(String  message_partner_ids){
        this.message_partner_ids = message_partner_ids ;
        this.message_partner_idsDirtyFlag = true ;
    }

     /**
     * 获取 [关注者(业务伙伴)]脏标记
     */
    @JsonIgnore
    public boolean getMessage_partner_idsDirtyFlag(){
        return this.message_partner_idsDirtyFlag ;
    }   

    /**
     * 获取 [未读消息]
     */
    @JsonProperty("message_unread")
    public String getMessage_unread(){
        return this.message_unread ;
    }

    /**
     * 设置 [未读消息]
     */
    @JsonProperty("message_unread")
    public void setMessage_unread(String  message_unread){
        this.message_unread = message_unread ;
        this.message_unreadDirtyFlag = true ;
    }

     /**
     * 获取 [未读消息]脏标记
     */
    @JsonIgnore
    public boolean getMessage_unreadDirtyFlag(){
        return this.message_unreadDirtyFlag ;
    }   

    /**
     * 获取 [未读消息计数器]
     */
    @JsonProperty("message_unread_counter")
    public Integer getMessage_unread_counter(){
        return this.message_unread_counter ;
    }

    /**
     * 设置 [未读消息计数器]
     */
    @JsonProperty("message_unread_counter")
    public void setMessage_unread_counter(Integer  message_unread_counter){
        this.message_unread_counter = message_unread_counter ;
        this.message_unread_counterDirtyFlag = true ;
    }

     /**
     * 获取 [未读消息计数器]脏标记
     */
    @JsonIgnore
    public boolean getMessage_unread_counterDirtyFlag(){
        return this.message_unread_counterDirtyFlag ;
    }   

    /**
     * 获取 [日记账分录]
     */
    @JsonProperty("move_id")
    public Integer getMove_id(){
        return this.move_id ;
    }

    /**
     * 设置 [日记账分录]
     */
    @JsonProperty("move_id")
    public void setMove_id(Integer  move_id){
        this.move_id = move_id ;
        this.move_idDirtyFlag = true ;
    }

     /**
     * 获取 [日记账分录]脏标记
     */
    @JsonIgnore
    public boolean getMove_idDirtyFlag(){
        return this.move_idDirtyFlag ;
    }   

    /**
     * 获取 [日记账分录名称]
     */
    @JsonProperty("move_name")
    public String getMove_name(){
        return this.move_name ;
    }

    /**
     * 设置 [日记账分录名称]
     */
    @JsonProperty("move_name")
    public void setMove_name(String  move_name){
        this.move_name = move_name ;
        this.move_nameDirtyFlag = true ;
    }

     /**
     * 获取 [日记账分录名称]脏标记
     */
    @JsonIgnore
    public boolean getMove_nameDirtyFlag(){
        return this.move_nameDirtyFlag ;
    }   

    /**
     * 获取 [参考/说明]
     */
    @JsonProperty("name")
    public String getName(){
        return this.name ;
    }

    /**
     * 设置 [参考/说明]
     */
    @JsonProperty("name")
    public void setName(String  name){
        this.name = name ;
        this.nameDirtyFlag = true ;
    }

     /**
     * 获取 [参考/说明]脏标记
     */
    @JsonIgnore
    public boolean getNameDirtyFlag(){
        return this.nameDirtyFlag ;
    }   

    /**
     * 获取 [号码]
     */
    @JsonProperty("number")
    public String getNumber(){
        return this.number ;
    }

    /**
     * 设置 [号码]
     */
    @JsonProperty("number")
    public void setNumber(String  number){
        this.number = number ;
        this.numberDirtyFlag = true ;
    }

     /**
     * 获取 [号码]脏标记
     */
    @JsonIgnore
    public boolean getNumberDirtyFlag(){
        return this.numberDirtyFlag ;
    }   

    /**
     * 获取 [源文档]
     */
    @JsonProperty("origin")
    public String getOrigin(){
        return this.origin ;
    }

    /**
     * 设置 [源文档]
     */
    @JsonProperty("origin")
    public void setOrigin(String  origin){
        this.origin = origin ;
        this.originDirtyFlag = true ;
    }

     /**
     * 获取 [源文档]脏标记
     */
    @JsonIgnore
    public boolean getOriginDirtyFlag(){
        return this.originDirtyFlag ;
    }   

    /**
     * 获取 [未到期贷项]
     */
    @JsonProperty("outstanding_credits_debits_widget")
    public String getOutstanding_credits_debits_widget(){
        return this.outstanding_credits_debits_widget ;
    }

    /**
     * 设置 [未到期贷项]
     */
    @JsonProperty("outstanding_credits_debits_widget")
    public void setOutstanding_credits_debits_widget(String  outstanding_credits_debits_widget){
        this.outstanding_credits_debits_widget = outstanding_credits_debits_widget ;
        this.outstanding_credits_debits_widgetDirtyFlag = true ;
    }

     /**
     * 获取 [未到期贷项]脏标记
     */
    @JsonIgnore
    public boolean getOutstanding_credits_debits_widgetDirtyFlag(){
        return this.outstanding_credits_debits_widgetDirtyFlag ;
    }   

    /**
     * 获取 [银行账户]
     */
    @JsonProperty("partner_bank_id")
    public Integer getPartner_bank_id(){
        return this.partner_bank_id ;
    }

    /**
     * 设置 [银行账户]
     */
    @JsonProperty("partner_bank_id")
    public void setPartner_bank_id(Integer  partner_bank_id){
        this.partner_bank_id = partner_bank_id ;
        this.partner_bank_idDirtyFlag = true ;
    }

     /**
     * 获取 [银行账户]脏标记
     */
    @JsonIgnore
    public boolean getPartner_bank_idDirtyFlag(){
        return this.partner_bank_idDirtyFlag ;
    }   

    /**
     * 获取 [业务伙伴]
     */
    @JsonProperty("partner_id")
    public Integer getPartner_id(){
        return this.partner_id ;
    }

    /**
     * 设置 [业务伙伴]
     */
    @JsonProperty("partner_id")
    public void setPartner_id(Integer  partner_id){
        this.partner_id = partner_id ;
        this.partner_idDirtyFlag = true ;
    }

     /**
     * 获取 [业务伙伴]脏标记
     */
    @JsonIgnore
    public boolean getPartner_idDirtyFlag(){
        return this.partner_idDirtyFlag ;
    }   

    /**
     * 获取 [业务伙伴]
     */
    @JsonProperty("partner_id_text")
    public String getPartner_id_text(){
        return this.partner_id_text ;
    }

    /**
     * 设置 [业务伙伴]
     */
    @JsonProperty("partner_id_text")
    public void setPartner_id_text(String  partner_id_text){
        this.partner_id_text = partner_id_text ;
        this.partner_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [业务伙伴]脏标记
     */
    @JsonIgnore
    public boolean getPartner_id_textDirtyFlag(){
        return this.partner_id_textDirtyFlag ;
    }   

    /**
     * 获取 [送货地址]
     */
    @JsonProperty("partner_shipping_id")
    public Integer getPartner_shipping_id(){
        return this.partner_shipping_id ;
    }

    /**
     * 设置 [送货地址]
     */
    @JsonProperty("partner_shipping_id")
    public void setPartner_shipping_id(Integer  partner_shipping_id){
        this.partner_shipping_id = partner_shipping_id ;
        this.partner_shipping_idDirtyFlag = true ;
    }

     /**
     * 获取 [送货地址]脏标记
     */
    @JsonIgnore
    public boolean getPartner_shipping_idDirtyFlag(){
        return this.partner_shipping_idDirtyFlag ;
    }   

    /**
     * 获取 [送货地址]
     */
    @JsonProperty("partner_shipping_id_text")
    public String getPartner_shipping_id_text(){
        return this.partner_shipping_id_text ;
    }

    /**
     * 设置 [送货地址]
     */
    @JsonProperty("partner_shipping_id_text")
    public void setPartner_shipping_id_text(String  partner_shipping_id_text){
        this.partner_shipping_id_text = partner_shipping_id_text ;
        this.partner_shipping_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [送货地址]脏标记
     */
    @JsonIgnore
    public boolean getPartner_shipping_id_textDirtyFlag(){
        return this.partner_shipping_id_textDirtyFlag ;
    }   

    /**
     * 获取 [支付挂件]
     */
    @JsonProperty("payments_widget")
    public String getPayments_widget(){
        return this.payments_widget ;
    }

    /**
     * 设置 [支付挂件]
     */
    @JsonProperty("payments_widget")
    public void setPayments_widget(String  payments_widget){
        this.payments_widget = payments_widget ;
        this.payments_widgetDirtyFlag = true ;
    }

     /**
     * 获取 [支付挂件]脏标记
     */
    @JsonIgnore
    public boolean getPayments_widgetDirtyFlag(){
        return this.payments_widgetDirtyFlag ;
    }   

    /**
     * 获取 [付款]
     */
    @JsonProperty("payment_ids")
    public String getPayment_ids(){
        return this.payment_ids ;
    }

    /**
     * 设置 [付款]
     */
    @JsonProperty("payment_ids")
    public void setPayment_ids(String  payment_ids){
        this.payment_ids = payment_ids ;
        this.payment_idsDirtyFlag = true ;
    }

     /**
     * 获取 [付款]脏标记
     */
    @JsonIgnore
    public boolean getPayment_idsDirtyFlag(){
        return this.payment_idsDirtyFlag ;
    }   

    /**
     * 获取 [付款凭证明细]
     */
    @JsonProperty("payment_move_line_ids")
    public String getPayment_move_line_ids(){
        return this.payment_move_line_ids ;
    }

    /**
     * 设置 [付款凭证明细]
     */
    @JsonProperty("payment_move_line_ids")
    public void setPayment_move_line_ids(String  payment_move_line_ids){
        this.payment_move_line_ids = payment_move_line_ids ;
        this.payment_move_line_idsDirtyFlag = true ;
    }

     /**
     * 获取 [付款凭证明细]脏标记
     */
    @JsonIgnore
    public boolean getPayment_move_line_idsDirtyFlag(){
        return this.payment_move_line_idsDirtyFlag ;
    }   

    /**
     * 获取 [付款条款]
     */
    @JsonProperty("payment_term_id")
    public Integer getPayment_term_id(){
        return this.payment_term_id ;
    }

    /**
     * 设置 [付款条款]
     */
    @JsonProperty("payment_term_id")
    public void setPayment_term_id(Integer  payment_term_id){
        this.payment_term_id = payment_term_id ;
        this.payment_term_idDirtyFlag = true ;
    }

     /**
     * 获取 [付款条款]脏标记
     */
    @JsonIgnore
    public boolean getPayment_term_idDirtyFlag(){
        return this.payment_term_idDirtyFlag ;
    }   

    /**
     * 获取 [付款条款]
     */
    @JsonProperty("payment_term_id_text")
    public String getPayment_term_id_text(){
        return this.payment_term_id_text ;
    }

    /**
     * 设置 [付款条款]
     */
    @JsonProperty("payment_term_id_text")
    public void setPayment_term_id_text(String  payment_term_id_text){
        this.payment_term_id_text = payment_term_id_text ;
        this.payment_term_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [付款条款]脏标记
     */
    @JsonIgnore
    public boolean getPayment_term_id_textDirtyFlag(){
        return this.payment_term_id_textDirtyFlag ;
    }   

    /**
     * 获取 [添加采购订单]
     */
    @JsonProperty("purchase_id")
    public Integer getPurchase_id(){
        return this.purchase_id ;
    }

    /**
     * 设置 [添加采购订单]
     */
    @JsonProperty("purchase_id")
    public void setPurchase_id(Integer  purchase_id){
        this.purchase_id = purchase_id ;
        this.purchase_idDirtyFlag = true ;
    }

     /**
     * 获取 [添加采购订单]脏标记
     */
    @JsonIgnore
    public boolean getPurchase_idDirtyFlag(){
        return this.purchase_idDirtyFlag ;
    }   

    /**
     * 获取 [添加采购订单]
     */
    @JsonProperty("purchase_id_text")
    public String getPurchase_id_text(){
        return this.purchase_id_text ;
    }

    /**
     * 设置 [添加采购订单]
     */
    @JsonProperty("purchase_id_text")
    public void setPurchase_id_text(String  purchase_id_text){
        this.purchase_id_text = purchase_id_text ;
        this.purchase_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [添加采购订单]脏标记
     */
    @JsonIgnore
    public boolean getPurchase_id_textDirtyFlag(){
        return this.purchase_id_textDirtyFlag ;
    }   

    /**
     * 获取 [已付／已核销]
     */
    @JsonProperty("reconciled")
    public String getReconciled(){
        return this.reconciled ;
    }

    /**
     * 设置 [已付／已核销]
     */
    @JsonProperty("reconciled")
    public void setReconciled(String  reconciled){
        this.reconciled = reconciled ;
        this.reconciledDirtyFlag = true ;
    }

     /**
     * 获取 [已付／已核销]脏标记
     */
    @JsonIgnore
    public boolean getReconciledDirtyFlag(){
        return this.reconciledDirtyFlag ;
    }   

    /**
     * 获取 [付款参考:]
     */
    @JsonProperty("reference")
    public String getReference(){
        return this.reference ;
    }

    /**
     * 设置 [付款参考:]
     */
    @JsonProperty("reference")
    public void setReference(String  reference){
        this.reference = reference ;
        this.referenceDirtyFlag = true ;
    }

     /**
     * 获取 [付款参考:]脏标记
     */
    @JsonIgnore
    public boolean getReferenceDirtyFlag(){
        return this.referenceDirtyFlag ;
    }   

    /**
     * 获取 [此发票为信用票的发票]
     */
    @JsonProperty("refund_invoice_id")
    public Integer getRefund_invoice_id(){
        return this.refund_invoice_id ;
    }

    /**
     * 设置 [此发票为信用票的发票]
     */
    @JsonProperty("refund_invoice_id")
    public void setRefund_invoice_id(Integer  refund_invoice_id){
        this.refund_invoice_id = refund_invoice_id ;
        this.refund_invoice_idDirtyFlag = true ;
    }

     /**
     * 获取 [此发票为信用票的发票]脏标记
     */
    @JsonIgnore
    public boolean getRefund_invoice_idDirtyFlag(){
        return this.refund_invoice_idDirtyFlag ;
    }   

    /**
     * 获取 [退款发票]
     */
    @JsonProperty("refund_invoice_ids")
    public String getRefund_invoice_ids(){
        return this.refund_invoice_ids ;
    }

    /**
     * 设置 [退款发票]
     */
    @JsonProperty("refund_invoice_ids")
    public void setRefund_invoice_ids(String  refund_invoice_ids){
        this.refund_invoice_ids = refund_invoice_ids ;
        this.refund_invoice_idsDirtyFlag = true ;
    }

     /**
     * 获取 [退款发票]脏标记
     */
    @JsonIgnore
    public boolean getRefund_invoice_idsDirtyFlag(){
        return this.refund_invoice_idsDirtyFlag ;
    }   

    /**
     * 获取 [此发票为信用票的发票]
     */
    @JsonProperty("refund_invoice_id_text")
    public String getRefund_invoice_id_text(){
        return this.refund_invoice_id_text ;
    }

    /**
     * 设置 [此发票为信用票的发票]
     */
    @JsonProperty("refund_invoice_id_text")
    public void setRefund_invoice_id_text(String  refund_invoice_id_text){
        this.refund_invoice_id_text = refund_invoice_id_text ;
        this.refund_invoice_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [此发票为信用票的发票]脏标记
     */
    @JsonIgnore
    public boolean getRefund_invoice_id_textDirtyFlag(){
        return this.refund_invoice_id_textDirtyFlag ;
    }   

    /**
     * 获取 [到期金额]
     */
    @JsonProperty("residual")
    public Double getResidual(){
        return this.residual ;
    }

    /**
     * 设置 [到期金额]
     */
    @JsonProperty("residual")
    public void setResidual(Double  residual){
        this.residual = residual ;
        this.residualDirtyFlag = true ;
    }

     /**
     * 获取 [到期金额]脏标记
     */
    @JsonIgnore
    public boolean getResidualDirtyFlag(){
        return this.residualDirtyFlag ;
    }   

    /**
     * 获取 [公司使用币种的逾期金额]
     */
    @JsonProperty("residual_company_signed")
    public Double getResidual_company_signed(){
        return this.residual_company_signed ;
    }

    /**
     * 设置 [公司使用币种的逾期金额]
     */
    @JsonProperty("residual_company_signed")
    public void setResidual_company_signed(Double  residual_company_signed){
        this.residual_company_signed = residual_company_signed ;
        this.residual_company_signedDirtyFlag = true ;
    }

     /**
     * 获取 [公司使用币种的逾期金额]脏标记
     */
    @JsonIgnore
    public boolean getResidual_company_signedDirtyFlag(){
        return this.residual_company_signedDirtyFlag ;
    }   

    /**
     * 获取 [发票使用币种的逾期金额]
     */
    @JsonProperty("residual_signed")
    public Double getResidual_signed(){
        return this.residual_signed ;
    }

    /**
     * 设置 [发票使用币种的逾期金额]
     */
    @JsonProperty("residual_signed")
    public void setResidual_signed(Double  residual_signed){
        this.residual_signed = residual_signed ;
        this.residual_signedDirtyFlag = true ;
    }

     /**
     * 获取 [发票使用币种的逾期金额]脏标记
     */
    @JsonIgnore
    public boolean getResidual_signedDirtyFlag(){
        return this.residual_signedDirtyFlag ;
    }   

    /**
     * 获取 [已汇]
     */
    @JsonProperty("sent")
    public String getSent(){
        return this.sent ;
    }

    /**
     * 设置 [已汇]
     */
    @JsonProperty("sent")
    public void setSent(String  sent){
        this.sent = sent ;
        this.sentDirtyFlag = true ;
    }

     /**
     * 获取 [已汇]脏标记
     */
    @JsonIgnore
    public boolean getSentDirtyFlag(){
        return this.sentDirtyFlag ;
    }   

    /**
     * 获取 [下一号码]
     */
    @JsonProperty("sequence_number_next")
    public String getSequence_number_next(){
        return this.sequence_number_next ;
    }

    /**
     * 设置 [下一号码]
     */
    @JsonProperty("sequence_number_next")
    public void setSequence_number_next(String  sequence_number_next){
        this.sequence_number_next = sequence_number_next ;
        this.sequence_number_nextDirtyFlag = true ;
    }

     /**
     * 获取 [下一号码]脏标记
     */
    @JsonIgnore
    public boolean getSequence_number_nextDirtyFlag(){
        return this.sequence_number_nextDirtyFlag ;
    }   

    /**
     * 获取 [下一个号码前缀]
     */
    @JsonProperty("sequence_number_next_prefix")
    public String getSequence_number_next_prefix(){
        return this.sequence_number_next_prefix ;
    }

    /**
     * 设置 [下一个号码前缀]
     */
    @JsonProperty("sequence_number_next_prefix")
    public void setSequence_number_next_prefix(String  sequence_number_next_prefix){
        this.sequence_number_next_prefix = sequence_number_next_prefix ;
        this.sequence_number_next_prefixDirtyFlag = true ;
    }

     /**
     * 获取 [下一个号码前缀]脏标记
     */
    @JsonIgnore
    public boolean getSequence_number_next_prefixDirtyFlag(){
        return this.sequence_number_next_prefixDirtyFlag ;
    }   

    /**
     * 获取 [源邮箱]
     */
    @JsonProperty("source_email")
    public String getSource_email(){
        return this.source_email ;
    }

    /**
     * 设置 [源邮箱]
     */
    @JsonProperty("source_email")
    public void setSource_email(String  source_email){
        this.source_email = source_email ;
        this.source_emailDirtyFlag = true ;
    }

     /**
     * 获取 [源邮箱]脏标记
     */
    @JsonIgnore
    public boolean getSource_emailDirtyFlag(){
        return this.source_emailDirtyFlag ;
    }   

    /**
     * 获取 [来源]
     */
    @JsonProperty("source_id")
    public Integer getSource_id(){
        return this.source_id ;
    }

    /**
     * 设置 [来源]
     */
    @JsonProperty("source_id")
    public void setSource_id(Integer  source_id){
        this.source_id = source_id ;
        this.source_idDirtyFlag = true ;
    }

     /**
     * 获取 [来源]脏标记
     */
    @JsonIgnore
    public boolean getSource_idDirtyFlag(){
        return this.source_idDirtyFlag ;
    }   

    /**
     * 获取 [来源]
     */
    @JsonProperty("source_id_text")
    public String getSource_id_text(){
        return this.source_id_text ;
    }

    /**
     * 设置 [来源]
     */
    @JsonProperty("source_id_text")
    public void setSource_id_text(String  source_id_text){
        this.source_id_text = source_id_text ;
        this.source_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [来源]脏标记
     */
    @JsonIgnore
    public boolean getSource_id_textDirtyFlag(){
        return this.source_id_textDirtyFlag ;
    }   

    /**
     * 获取 [状态]
     */
    @JsonProperty("state")
    public String getState(){
        return this.state ;
    }

    /**
     * 设置 [状态]
     */
    @JsonProperty("state")
    public void setState(String  state){
        this.state = state ;
        this.stateDirtyFlag = true ;
    }

     /**
     * 获取 [状态]脏标记
     */
    @JsonIgnore
    public boolean getStateDirtyFlag(){
        return this.stateDirtyFlag ;
    }   

    /**
     * 获取 [税率明细行]
     */
    @JsonProperty("tax_line_ids")
    public String getTax_line_ids(){
        return this.tax_line_ids ;
    }

    /**
     * 设置 [税率明细行]
     */
    @JsonProperty("tax_line_ids")
    public void setTax_line_ids(String  tax_line_ids){
        this.tax_line_ids = tax_line_ids ;
        this.tax_line_idsDirtyFlag = true ;
    }

     /**
     * 获取 [税率明细行]脏标记
     */
    @JsonIgnore
    public boolean getTax_line_idsDirtyFlag(){
        return this.tax_line_idsDirtyFlag ;
    }   

    /**
     * 获取 [销售团队]
     */
    @JsonProperty("team_id")
    public Integer getTeam_id(){
        return this.team_id ;
    }

    /**
     * 设置 [销售团队]
     */
    @JsonProperty("team_id")
    public void setTeam_id(Integer  team_id){
        this.team_id = team_id ;
        this.team_idDirtyFlag = true ;
    }

     /**
     * 获取 [销售团队]脏标记
     */
    @JsonIgnore
    public boolean getTeam_idDirtyFlag(){
        return this.team_idDirtyFlag ;
    }   

    /**
     * 获取 [销售团队]
     */
    @JsonProperty("team_id_text")
    public String getTeam_id_text(){
        return this.team_id_text ;
    }

    /**
     * 设置 [销售团队]
     */
    @JsonProperty("team_id_text")
    public void setTeam_id_text(String  team_id_text){
        this.team_id_text = team_id_text ;
        this.team_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [销售团队]脏标记
     */
    @JsonIgnore
    public boolean getTeam_id_textDirtyFlag(){
        return this.team_id_textDirtyFlag ;
    }   

    /**
     * 获取 [交易]
     */
    @JsonProperty("transaction_ids")
    public String getTransaction_ids(){
        return this.transaction_ids ;
    }

    /**
     * 设置 [交易]
     */
    @JsonProperty("transaction_ids")
    public void setTransaction_ids(String  transaction_ids){
        this.transaction_ids = transaction_ids ;
        this.transaction_idsDirtyFlag = true ;
    }

     /**
     * 获取 [交易]脏标记
     */
    @JsonIgnore
    public boolean getTransaction_idsDirtyFlag(){
        return this.transaction_idsDirtyFlag ;
    }   

    /**
     * 获取 [类型]
     */
    @JsonProperty("type")
    public String getType(){
        return this.type ;
    }

    /**
     * 设置 [类型]
     */
    @JsonProperty("type")
    public void setType(String  type){
        this.type = type ;
        this.typeDirtyFlag = true ;
    }

     /**
     * 获取 [类型]脏标记
     */
    @JsonIgnore
    public boolean getTypeDirtyFlag(){
        return this.typeDirtyFlag ;
    }   

    /**
     * 获取 [销售员]
     */
    @JsonProperty("user_id")
    public Integer getUser_id(){
        return this.user_id ;
    }

    /**
     * 设置 [销售员]
     */
    @JsonProperty("user_id")
    public void setUser_id(Integer  user_id){
        this.user_id = user_id ;
        this.user_idDirtyFlag = true ;
    }

     /**
     * 获取 [销售员]脏标记
     */
    @JsonIgnore
    public boolean getUser_idDirtyFlag(){
        return this.user_idDirtyFlag ;
    }   

    /**
     * 获取 [销售员]
     */
    @JsonProperty("user_id_text")
    public String getUser_id_text(){
        return this.user_id_text ;
    }

    /**
     * 设置 [销售员]
     */
    @JsonProperty("user_id_text")
    public void setUser_id_text(String  user_id_text){
        this.user_id_text = user_id_text ;
        this.user_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [销售员]脏标记
     */
    @JsonIgnore
    public boolean getUser_id_textDirtyFlag(){
        return this.user_id_textDirtyFlag ;
    }   

    /**
     * 获取 [供应商账单]
     */
    @JsonProperty("vendor_bill_id")
    public Integer getVendor_bill_id(){
        return this.vendor_bill_id ;
    }

    /**
     * 设置 [供应商账单]
     */
    @JsonProperty("vendor_bill_id")
    public void setVendor_bill_id(Integer  vendor_bill_id){
        this.vendor_bill_id = vendor_bill_id ;
        this.vendor_bill_idDirtyFlag = true ;
    }

     /**
     * 获取 [供应商账单]脏标记
     */
    @JsonIgnore
    public boolean getVendor_bill_idDirtyFlag(){
        return this.vendor_bill_idDirtyFlag ;
    }   

    /**
     * 获取 [供应商账单]
     */
    @JsonProperty("vendor_bill_id_text")
    public String getVendor_bill_id_text(){
        return this.vendor_bill_id_text ;
    }

    /**
     * 设置 [供应商账单]
     */
    @JsonProperty("vendor_bill_id_text")
    public void setVendor_bill_id_text(String  vendor_bill_id_text){
        this.vendor_bill_id_text = vendor_bill_id_text ;
        this.vendor_bill_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [供应商账单]脏标记
     */
    @JsonIgnore
    public boolean getVendor_bill_id_textDirtyFlag(){
        return this.vendor_bill_id_textDirtyFlag ;
    }   

    /**
     * 获取 [自动完成]
     */
    @JsonProperty("vendor_bill_purchase_id")
    public Integer getVendor_bill_purchase_id(){
        return this.vendor_bill_purchase_id ;
    }

    /**
     * 设置 [自动完成]
     */
    @JsonProperty("vendor_bill_purchase_id")
    public void setVendor_bill_purchase_id(Integer  vendor_bill_purchase_id){
        this.vendor_bill_purchase_id = vendor_bill_purchase_id ;
        this.vendor_bill_purchase_idDirtyFlag = true ;
    }

     /**
     * 获取 [自动完成]脏标记
     */
    @JsonIgnore
    public boolean getVendor_bill_purchase_idDirtyFlag(){
        return this.vendor_bill_purchase_idDirtyFlag ;
    }   

    /**
     * 获取 [自动完成]
     */
    @JsonProperty("vendor_bill_purchase_id_text")
    public String getVendor_bill_purchase_id_text(){
        return this.vendor_bill_purchase_id_text ;
    }

    /**
     * 设置 [自动完成]
     */
    @JsonProperty("vendor_bill_purchase_id_text")
    public void setVendor_bill_purchase_id_text(String  vendor_bill_purchase_id_text){
        this.vendor_bill_purchase_id_text = vendor_bill_purchase_id_text ;
        this.vendor_bill_purchase_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [自动完成]脏标记
     */
    @JsonIgnore
    public boolean getVendor_bill_purchase_id_textDirtyFlag(){
        return this.vendor_bill_purchase_id_textDirtyFlag ;
    }   

    /**
     * 获取 [供应商名称]
     */
    @JsonProperty("vendor_display_name")
    public String getVendor_display_name(){
        return this.vendor_display_name ;
    }

    /**
     * 设置 [供应商名称]
     */
    @JsonProperty("vendor_display_name")
    public void setVendor_display_name(String  vendor_display_name){
        this.vendor_display_name = vendor_display_name ;
        this.vendor_display_nameDirtyFlag = true ;
    }

     /**
     * 获取 [供应商名称]脏标记
     */
    @JsonIgnore
    public boolean getVendor_display_nameDirtyFlag(){
        return this.vendor_display_nameDirtyFlag ;
    }   

    /**
     * 获取 [网站]
     */
    @JsonProperty("website_id")
    public Integer getWebsite_id(){
        return this.website_id ;
    }

    /**
     * 设置 [网站]
     */
    @JsonProperty("website_id")
    public void setWebsite_id(Integer  website_id){
        this.website_id = website_id ;
        this.website_idDirtyFlag = true ;
    }

     /**
     * 获取 [网站]脏标记
     */
    @JsonIgnore
    public boolean getWebsite_idDirtyFlag(){
        return this.website_idDirtyFlag ;
    }   

    /**
     * 获取 [网站信息]
     */
    @JsonProperty("website_message_ids")
    public String getWebsite_message_ids(){
        return this.website_message_ids ;
    }

    /**
     * 设置 [网站信息]
     */
    @JsonProperty("website_message_ids")
    public void setWebsite_message_ids(String  website_message_ids){
        this.website_message_ids = website_message_ids ;
        this.website_message_idsDirtyFlag = true ;
    }

     /**
     * 获取 [网站信息]脏标记
     */
    @JsonIgnore
    public boolean getWebsite_message_idsDirtyFlag(){
        return this.website_message_idsDirtyFlag ;
    }   

    /**
     * 获取 [最后更新时间]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return this.write_date ;
    }

    /**
     * 设置 [最后更新时间]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新时间]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return this.write_dateDirtyFlag ;
    }   

    /**
     * 获取 [最后更新人]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return this.write_uid ;
    }

    /**
     * 设置 [最后更新人]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新人]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return this.write_uidDirtyFlag ;
    }   

    /**
     * 获取 [最后更新人]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return this.write_uid_text ;
    }

    /**
     * 设置 [最后更新人]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新人]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return this.write_uid_textDirtyFlag ;
    }   

    /**
     * 获取 [最后修改日]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return this.__last_update ;
    }

    /**
     * 设置 [最后修改日]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

     /**
     * 获取 [最后修改日]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return this.__last_updateDirtyFlag ;
    }   



    public void fromMap(Map<String, Object> map) throws Exception {
		if(!(map.get("access_token") instanceof Boolean)&& map.get("access_token")!=null){
			this.setAccess_token((String)map.get("access_token"));
		}
		if(!(map.get("access_url") instanceof Boolean)&& map.get("access_url")!=null){
			this.setAccess_url((String)map.get("access_url"));
		}
		if(!(map.get("access_warning") instanceof Boolean)&& map.get("access_warning")!=null){
			this.setAccess_warning((String)map.get("access_warning"));
		}
		if(!(map.get("account_id") instanceof Boolean)&& map.get("account_id")!=null){
			Object[] objs = (Object[])map.get("account_id");
			if(objs.length > 0){
				this.setAccount_id((Integer)objs[0]);
			}
		}
		if(!(map.get("account_id") instanceof Boolean)&& map.get("account_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("account_id");
			if(objs.length > 1){
				this.setAccount_id_text((String)objs[1]);
			}
		}
		if(!(map.get("activity_date_deadline") instanceof Boolean)&& map.get("activity_date_deadline")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd").parse((String)map.get("activity_date_deadline"));
   			this.setActivity_date_deadline(new Timestamp(parse.getTime()));
		}
		if(!(map.get("activity_ids") instanceof Boolean)&& map.get("activity_ids")!=null){
			Object[] objs = (Object[])map.get("activity_ids");
			if(objs.length > 0){
				Integer[] activity_ids = Arrays.copyOfRange(objs,0,objs.length,Integer[].class);
				this.setActivity_ids(Arrays.toString(activity_ids).replace(" ",""));
			}
		}
		if(!(map.get("activity_state") instanceof Boolean)&& map.get("activity_state")!=null){
			this.setActivity_state((String)map.get("activity_state"));
		}
		if(!(map.get("activity_summary") instanceof Boolean)&& map.get("activity_summary")!=null){
			this.setActivity_summary((String)map.get("activity_summary"));
		}
		if(!(map.get("activity_type_id") instanceof Boolean)&& map.get("activity_type_id")!=null){
			Object[] objs = (Object[])map.get("activity_type_id");
			if(objs.length > 0){
				this.setActivity_type_id((Integer)objs[0]);
			}
		}
		if(!(map.get("activity_user_id") instanceof Boolean)&& map.get("activity_user_id")!=null){
			Object[] objs = (Object[])map.get("activity_user_id");
			if(objs.length > 0){
				this.setActivity_user_id((Integer)objs[0]);
			}
		}
		if(!(map.get("amount_by_group") instanceof Boolean)&& map.get("amount_by_group")!=null){
			//暂时忽略
			//this.setAmount_by_group(((String)map.get("amount_by_group")).getBytes("UTF-8"));
		}
		if(!(map.get("amount_tax") instanceof Boolean)&& map.get("amount_tax")!=null){
			this.setAmount_tax((Double)map.get("amount_tax"));
		}
		if(!(map.get("amount_tax_signed") instanceof Boolean)&& map.get("amount_tax_signed")!=null){
			this.setAmount_tax_signed((Double)map.get("amount_tax_signed"));
		}
		if(!(map.get("amount_total") instanceof Boolean)&& map.get("amount_total")!=null){
			this.setAmount_total((Double)map.get("amount_total"));
		}
		if(!(map.get("amount_total_company_signed") instanceof Boolean)&& map.get("amount_total_company_signed")!=null){
			this.setAmount_total_company_signed((Double)map.get("amount_total_company_signed"));
		}
		if(!(map.get("amount_total_signed") instanceof Boolean)&& map.get("amount_total_signed")!=null){
			this.setAmount_total_signed((Double)map.get("amount_total_signed"));
		}
		if(!(map.get("amount_untaxed") instanceof Boolean)&& map.get("amount_untaxed")!=null){
			this.setAmount_untaxed((Double)map.get("amount_untaxed"));
		}
		if(!(map.get("amount_untaxed_invoice_signed") instanceof Boolean)&& map.get("amount_untaxed_invoice_signed")!=null){
			this.setAmount_untaxed_invoice_signed((Double)map.get("amount_untaxed_invoice_signed"));
		}
		if(!(map.get("amount_untaxed_signed") instanceof Boolean)&& map.get("amount_untaxed_signed")!=null){
			this.setAmount_untaxed_signed((Double)map.get("amount_untaxed_signed"));
		}
		if(!(map.get("authorized_transaction_ids") instanceof Boolean)&& map.get("authorized_transaction_ids")!=null){
			Object[] objs = (Object[])map.get("authorized_transaction_ids");
			if(objs.length > 0){
				Integer[] authorized_transaction_ids = Arrays.copyOfRange(objs,0,objs.length,Integer[].class);
				this.setAuthorized_transaction_ids(Arrays.toString(authorized_transaction_ids).replace(" ",""));
			}
		}
		if(!(map.get("campaign_id") instanceof Boolean)&& map.get("campaign_id")!=null){
			Object[] objs = (Object[])map.get("campaign_id");
			if(objs.length > 0){
				this.setCampaign_id((Integer)objs[0]);
			}
		}
		if(!(map.get("campaign_id") instanceof Boolean)&& map.get("campaign_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("campaign_id");
			if(objs.length > 1){
				this.setCampaign_id_text((String)objs[1]);
			}
		}
		if(!(map.get("cash_rounding_id") instanceof Boolean)&& map.get("cash_rounding_id")!=null){
			Object[] objs = (Object[])map.get("cash_rounding_id");
			if(objs.length > 0){
				this.setCash_rounding_id((Integer)objs[0]);
			}
		}
		if(!(map.get("cash_rounding_id") instanceof Boolean)&& map.get("cash_rounding_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("cash_rounding_id");
			if(objs.length > 1){
				this.setCash_rounding_id_text((String)objs[1]);
			}
		}
		if(!(map.get("comment") instanceof Boolean)&& map.get("comment")!=null){
			this.setComment((String)map.get("comment"));
		}
		if(!(map.get("commercial_partner_id") instanceof Boolean)&& map.get("commercial_partner_id")!=null){
			Object[] objs = (Object[])map.get("commercial_partner_id");
			if(objs.length > 0){
				this.setCommercial_partner_id((Integer)objs[0]);
			}
		}
		if(!(map.get("commercial_partner_id") instanceof Boolean)&& map.get("commercial_partner_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("commercial_partner_id");
			if(objs.length > 1){
				this.setCommercial_partner_id_text((String)objs[1]);
			}
		}
		if(!(map.get("company_currency_id") instanceof Boolean)&& map.get("company_currency_id")!=null){
			Object[] objs = (Object[])map.get("company_currency_id");
			if(objs.length > 0){
				this.setCompany_currency_id((Integer)objs[0]);
			}
		}
		if(!(map.get("company_id") instanceof Boolean)&& map.get("company_id")!=null){
			Object[] objs = (Object[])map.get("company_id");
			if(objs.length > 0){
				this.setCompany_id((Integer)objs[0]);
			}
		}
		if(!(map.get("company_id") instanceof Boolean)&& map.get("company_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("company_id");
			if(objs.length > 1){
				this.setCompany_id_text((String)objs[1]);
			}
		}
		if(!(map.get("create_date") instanceof Boolean)&& map.get("create_date")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse((String)map.get("create_date"));
   			this.setCreate_date(new Timestamp(parse.getTime()));
		}
		if(!(map.get("create_uid") instanceof Boolean)&& map.get("create_uid")!=null){
			Object[] objs = (Object[])map.get("create_uid");
			if(objs.length > 0){
				this.setCreate_uid((Integer)objs[0]);
			}
		}
		if(!(map.get("create_uid") instanceof Boolean)&& map.get("create_uid")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("create_uid");
			if(objs.length > 1){
				this.setCreate_uid_text((String)objs[1]);
			}
		}
		if(!(map.get("currency_id") instanceof Boolean)&& map.get("currency_id")!=null){
			Object[] objs = (Object[])map.get("currency_id");
			if(objs.length > 0){
				this.setCurrency_id((Integer)objs[0]);
			}
		}
		if(!(map.get("currency_id") instanceof Boolean)&& map.get("currency_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("currency_id");
			if(objs.length > 1){
				this.setCurrency_id_text((String)objs[1]);
			}
		}
		if(!(map.get("date") instanceof Boolean)&& map.get("date")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd").parse((String)map.get("date"));
   			this.setDate(new Timestamp(parse.getTime()));
		}
		if(!(map.get("date_due") instanceof Boolean)&& map.get("date_due")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd").parse((String)map.get("date_due"));
   			this.setDate_due(new Timestamp(parse.getTime()));
		}
		if(!(map.get("date_invoice") instanceof Boolean)&& map.get("date_invoice")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd").parse((String)map.get("date_invoice"));
   			this.setDate_invoice(new Timestamp(parse.getTime()));
		}
		if(!(map.get("display_name") instanceof Boolean)&& map.get("display_name")!=null){
			this.setDisplay_name((String)map.get("display_name"));
		}
		if(!(map.get("fiscal_position_id") instanceof Boolean)&& map.get("fiscal_position_id")!=null){
			Object[] objs = (Object[])map.get("fiscal_position_id");
			if(objs.length > 0){
				this.setFiscal_position_id((Integer)objs[0]);
			}
		}
		if(!(map.get("fiscal_position_id") instanceof Boolean)&& map.get("fiscal_position_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("fiscal_position_id");
			if(objs.length > 1){
				this.setFiscal_position_id_text((String)objs[1]);
			}
		}
		if(map.get("has_outstanding") instanceof Boolean){
			this.setHas_outstanding(((Boolean)map.get("has_outstanding"))? "true" : "false");
		}
		if(!(map.get("id") instanceof Boolean)&& map.get("id")!=null){
			this.setId((Integer)map.get("id"));
		}
		if(!(map.get("incoterms_id") instanceof Boolean)&& map.get("incoterms_id")!=null){
			Object[] objs = (Object[])map.get("incoterms_id");
			if(objs.length > 0){
				this.setIncoterms_id((Integer)objs[0]);
			}
		}
		if(!(map.get("incoterms_id") instanceof Boolean)&& map.get("incoterms_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("incoterms_id");
			if(objs.length > 1){
				this.setIncoterms_id_text((String)objs[1]);
			}
		}
		if(!(map.get("incoterm_id") instanceof Boolean)&& map.get("incoterm_id")!=null){
			Object[] objs = (Object[])map.get("incoterm_id");
			if(objs.length > 0){
				this.setIncoterm_id((Integer)objs[0]);
			}
		}
		if(!(map.get("incoterm_id") instanceof Boolean)&& map.get("incoterm_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("incoterm_id");
			if(objs.length > 1){
				this.setIncoterm_id_text((String)objs[1]);
			}
		}
		if(!(map.get("invoice_icon") instanceof Boolean)&& map.get("invoice_icon")!=null){
			this.setInvoice_icon((String)map.get("invoice_icon"));
		}
		if(!(map.get("invoice_line_ids") instanceof Boolean)&& map.get("invoice_line_ids")!=null){
			Object[] objs = (Object[])map.get("invoice_line_ids");
			if(objs.length > 0){
				Integer[] invoice_line_ids = Arrays.copyOfRange(objs,0,objs.length,Integer[].class);
				this.setInvoice_line_ids(Arrays.toString(invoice_line_ids).replace(" ",""));
			}
		}
		if(!(map.get("journal_id") instanceof Boolean)&& map.get("journal_id")!=null){
			Object[] objs = (Object[])map.get("journal_id");
			if(objs.length > 0){
				this.setJournal_id((Integer)objs[0]);
			}
		}
		if(!(map.get("journal_id") instanceof Boolean)&& map.get("journal_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("journal_id");
			if(objs.length > 1){
				this.setJournal_id_text((String)objs[1]);
			}
		}
		if(!(map.get("medium_id") instanceof Boolean)&& map.get("medium_id")!=null){
			Object[] objs = (Object[])map.get("medium_id");
			if(objs.length > 0){
				this.setMedium_id((Integer)objs[0]);
			}
		}
		if(!(map.get("medium_id") instanceof Boolean)&& map.get("medium_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("medium_id");
			if(objs.length > 1){
				this.setMedium_id_text((String)objs[1]);
			}
		}
		if(!(map.get("message_attachment_count") instanceof Boolean)&& map.get("message_attachment_count")!=null){
			this.setMessage_attachment_count((Integer)map.get("message_attachment_count"));
		}
		if(!(map.get("message_channel_ids") instanceof Boolean)&& map.get("message_channel_ids")!=null){
			Object[] objs = (Object[])map.get("message_channel_ids");
			if(objs.length > 0){
				Integer[] message_channel_ids = Arrays.copyOfRange(objs,0,objs.length,Integer[].class);
				this.setMessage_channel_ids(Arrays.toString(message_channel_ids).replace(" ",""));
			}
		}
		if(!(map.get("message_follower_ids") instanceof Boolean)&& map.get("message_follower_ids")!=null){
			Object[] objs = (Object[])map.get("message_follower_ids");
			if(objs.length > 0){
				Integer[] message_follower_ids = Arrays.copyOfRange(objs,0,objs.length,Integer[].class);
				this.setMessage_follower_ids(Arrays.toString(message_follower_ids).replace(" ",""));
			}
		}
		if(map.get("message_has_error") instanceof Boolean){
			this.setMessage_has_error(((Boolean)map.get("message_has_error"))? "true" : "false");
		}
		if(!(map.get("message_has_error_counter") instanceof Boolean)&& map.get("message_has_error_counter")!=null){
			this.setMessage_has_error_counter((Integer)map.get("message_has_error_counter"));
		}
		if(!(map.get("message_ids") instanceof Boolean)&& map.get("message_ids")!=null){
			Object[] objs = (Object[])map.get("message_ids");
			if(objs.length > 0){
				Integer[] message_ids = Arrays.copyOfRange(objs,0,objs.length,Integer[].class);
				this.setMessage_ids(Arrays.toString(message_ids).replace(" ",""));
			}
		}
		if(map.get("message_is_follower") instanceof Boolean){
			this.setMessage_is_follower(((Boolean)map.get("message_is_follower"))? "true" : "false");
		}
		if(!(map.get("message_main_attachment_id") instanceof Boolean)&& map.get("message_main_attachment_id")!=null){
			Object[] objs = (Object[])map.get("message_main_attachment_id");
			if(objs.length > 0){
				this.setMessage_main_attachment_id((Integer)objs[0]);
			}
		}
		if(map.get("message_needaction") instanceof Boolean){
			this.setMessage_needaction(((Boolean)map.get("message_needaction"))? "true" : "false");
		}
		if(!(map.get("message_needaction_counter") instanceof Boolean)&& map.get("message_needaction_counter")!=null){
			this.setMessage_needaction_counter((Integer)map.get("message_needaction_counter"));
		}
		if(!(map.get("message_partner_ids") instanceof Boolean)&& map.get("message_partner_ids")!=null){
			Object[] objs = (Object[])map.get("message_partner_ids");
			if(objs.length > 0){
				Integer[] message_partner_ids = Arrays.copyOfRange(objs,0,objs.length,Integer[].class);
				this.setMessage_partner_ids(Arrays.toString(message_partner_ids).replace(" ",""));
			}
		}
		if(map.get("message_unread") instanceof Boolean){
			this.setMessage_unread(((Boolean)map.get("message_unread"))? "true" : "false");
		}
		if(!(map.get("message_unread_counter") instanceof Boolean)&& map.get("message_unread_counter")!=null){
			this.setMessage_unread_counter((Integer)map.get("message_unread_counter"));
		}
		if(!(map.get("move_id") instanceof Boolean)&& map.get("move_id")!=null){
			Object[] objs = (Object[])map.get("move_id");
			if(objs.length > 0){
				this.setMove_id((Integer)objs[0]);
			}
		}
		if(!(map.get("move_name") instanceof Boolean)&& map.get("move_name")!=null){
			this.setMove_name((String)map.get("move_name"));
		}
		if(!(map.get("name") instanceof Boolean)&& map.get("name")!=null){
			this.setName((String)map.get("name"));
		}
		if(!(map.get("number") instanceof Boolean)&& map.get("number")!=null){
			this.setNumber((String)map.get("number"));
		}
		if(!(map.get("origin") instanceof Boolean)&& map.get("origin")!=null){
			this.setOrigin((String)map.get("origin"));
		}
		if(!(map.get("outstanding_credits_debits_widget") instanceof Boolean)&& map.get("outstanding_credits_debits_widget")!=null){
			this.setOutstanding_credits_debits_widget((String)map.get("outstanding_credits_debits_widget"));
		}
		if(!(map.get("partner_bank_id") instanceof Boolean)&& map.get("partner_bank_id")!=null){
			Object[] objs = (Object[])map.get("partner_bank_id");
			if(objs.length > 0){
				this.setPartner_bank_id((Integer)objs[0]);
			}
		}
		if(!(map.get("partner_id") instanceof Boolean)&& map.get("partner_id")!=null){
			Object[] objs = (Object[])map.get("partner_id");
			if(objs.length > 0){
				this.setPartner_id((Integer)objs[0]);
			}
		}
		if(!(map.get("partner_id") instanceof Boolean)&& map.get("partner_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("partner_id");
			if(objs.length > 1){
				this.setPartner_id_text((String)objs[1]);
			}
		}
		if(!(map.get("partner_shipping_id") instanceof Boolean)&& map.get("partner_shipping_id")!=null){
			Object[] objs = (Object[])map.get("partner_shipping_id");
			if(objs.length > 0){
				this.setPartner_shipping_id((Integer)objs[0]);
			}
		}
		if(!(map.get("partner_shipping_id") instanceof Boolean)&& map.get("partner_shipping_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("partner_shipping_id");
			if(objs.length > 1){
				this.setPartner_shipping_id_text((String)objs[1]);
			}
		}
		if(!(map.get("payments_widget") instanceof Boolean)&& map.get("payments_widget")!=null){
			this.setPayments_widget((String)map.get("payments_widget"));
		}
		if(!(map.get("payment_ids") instanceof Boolean)&& map.get("payment_ids")!=null){
			Object[] objs = (Object[])map.get("payment_ids");
			if(objs.length > 0){
				Integer[] payment_ids = Arrays.copyOfRange(objs,0,objs.length,Integer[].class);
				this.setPayment_ids(Arrays.toString(payment_ids).replace(" ",""));
			}
		}
		if(!(map.get("payment_move_line_ids") instanceof Boolean)&& map.get("payment_move_line_ids")!=null){
			Object[] objs = (Object[])map.get("payment_move_line_ids");
			if(objs.length > 0){
				Integer[] payment_move_line_ids = Arrays.copyOfRange(objs,0,objs.length,Integer[].class);
				this.setPayment_move_line_ids(Arrays.toString(payment_move_line_ids).replace(" ",""));
			}
		}
		if(!(map.get("payment_term_id") instanceof Boolean)&& map.get("payment_term_id")!=null){
			Object[] objs = (Object[])map.get("payment_term_id");
			if(objs.length > 0){
				this.setPayment_term_id((Integer)objs[0]);
			}
		}
		if(!(map.get("payment_term_id") instanceof Boolean)&& map.get("payment_term_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("payment_term_id");
			if(objs.length > 1){
				this.setPayment_term_id_text((String)objs[1]);
			}
		}
		if(!(map.get("purchase_id") instanceof Boolean)&& map.get("purchase_id")!=null){
			Object[] objs = (Object[])map.get("purchase_id");
			if(objs.length > 0){
				this.setPurchase_id((Integer)objs[0]);
			}
		}
		if(!(map.get("purchase_id") instanceof Boolean)&& map.get("purchase_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("purchase_id");
			if(objs.length > 1){
				this.setPurchase_id_text((String)objs[1]);
			}
		}
		if(map.get("reconciled") instanceof Boolean){
			this.setReconciled(((Boolean)map.get("reconciled"))? "true" : "false");
		}
		if(!(map.get("reference") instanceof Boolean)&& map.get("reference")!=null){
			this.setReference((String)map.get("reference"));
		}
		if(!(map.get("refund_invoice_id") instanceof Boolean)&& map.get("refund_invoice_id")!=null){
			Object[] objs = (Object[])map.get("refund_invoice_id");
			if(objs.length > 0){
				this.setRefund_invoice_id((Integer)objs[0]);
			}
		}
		if(!(map.get("refund_invoice_ids") instanceof Boolean)&& map.get("refund_invoice_ids")!=null){
			Object[] objs = (Object[])map.get("refund_invoice_ids");
			if(objs.length > 0){
				Integer[] refund_invoice_ids = Arrays.copyOfRange(objs,0,objs.length,Integer[].class);
				this.setRefund_invoice_ids(Arrays.toString(refund_invoice_ids).replace(" ",""));
			}
		}
		if(!(map.get("refund_invoice_id") instanceof Boolean)&& map.get("refund_invoice_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("refund_invoice_id");
			if(objs.length > 1){
				this.setRefund_invoice_id_text((String)objs[1]);
			}
		}
		if(!(map.get("residual") instanceof Boolean)&& map.get("residual")!=null){
			this.setResidual((Double)map.get("residual"));
		}
		if(!(map.get("residual_company_signed") instanceof Boolean)&& map.get("residual_company_signed")!=null){
			this.setResidual_company_signed((Double)map.get("residual_company_signed"));
		}
		if(!(map.get("residual_signed") instanceof Boolean)&& map.get("residual_signed")!=null){
			this.setResidual_signed((Double)map.get("residual_signed"));
		}
		if(map.get("sent") instanceof Boolean){
			this.setSent(((Boolean)map.get("sent"))? "true" : "false");
		}
		if(!(map.get("sequence_number_next") instanceof Boolean)&& map.get("sequence_number_next")!=null){
			this.setSequence_number_next((String)map.get("sequence_number_next"));
		}
		if(!(map.get("sequence_number_next_prefix") instanceof Boolean)&& map.get("sequence_number_next_prefix")!=null){
			this.setSequence_number_next_prefix((String)map.get("sequence_number_next_prefix"));
		}
		if(!(map.get("source_email") instanceof Boolean)&& map.get("source_email")!=null){
			this.setSource_email((String)map.get("source_email"));
		}
		if(!(map.get("source_id") instanceof Boolean)&& map.get("source_id")!=null){
			Object[] objs = (Object[])map.get("source_id");
			if(objs.length > 0){
				this.setSource_id((Integer)objs[0]);
			}
		}
		if(!(map.get("source_id") instanceof Boolean)&& map.get("source_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("source_id");
			if(objs.length > 1){
				this.setSource_id_text((String)objs[1]);
			}
		}
		if(!(map.get("state") instanceof Boolean)&& map.get("state")!=null){
			this.setState((String)map.get("state"));
		}
		if(!(map.get("tax_line_ids") instanceof Boolean)&& map.get("tax_line_ids")!=null){
			Object[] objs = (Object[])map.get("tax_line_ids");
			if(objs.length > 0){
				Integer[] tax_line_ids = Arrays.copyOfRange(objs,0,objs.length,Integer[].class);
				this.setTax_line_ids(Arrays.toString(tax_line_ids).replace(" ",""));
			}
		}
		if(!(map.get("team_id") instanceof Boolean)&& map.get("team_id")!=null){
			Object[] objs = (Object[])map.get("team_id");
			if(objs.length > 0){
				this.setTeam_id((Integer)objs[0]);
			}
		}
		if(!(map.get("team_id") instanceof Boolean)&& map.get("team_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("team_id");
			if(objs.length > 1){
				this.setTeam_id_text((String)objs[1]);
			}
		}
		if(!(map.get("transaction_ids") instanceof Boolean)&& map.get("transaction_ids")!=null){
			Object[] objs = (Object[])map.get("transaction_ids");
			if(objs.length > 0){
				Integer[] transaction_ids = Arrays.copyOfRange(objs,0,objs.length,Integer[].class);
				this.setTransaction_ids(Arrays.toString(transaction_ids).replace(" ",""));
			}
		}
		if(!(map.get("type") instanceof Boolean)&& map.get("type")!=null){
			this.setType((String)map.get("type"));
		}
		if(!(map.get("user_id") instanceof Boolean)&& map.get("user_id")!=null){
			Object[] objs = (Object[])map.get("user_id");
			if(objs.length > 0){
				this.setUser_id((Integer)objs[0]);
			}
		}
		if(!(map.get("user_id") instanceof Boolean)&& map.get("user_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("user_id");
			if(objs.length > 1){
				this.setUser_id_text((String)objs[1]);
			}
		}
		if(!(map.get("vendor_bill_id") instanceof Boolean)&& map.get("vendor_bill_id")!=null){
			Object[] objs = (Object[])map.get("vendor_bill_id");
			if(objs.length > 0){
				this.setVendor_bill_id((Integer)objs[0]);
			}
		}
		if(!(map.get("vendor_bill_id") instanceof Boolean)&& map.get("vendor_bill_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("vendor_bill_id");
			if(objs.length > 1){
				this.setVendor_bill_id_text((String)objs[1]);
			}
		}
		if(!(map.get("vendor_bill_purchase_id") instanceof Boolean)&& map.get("vendor_bill_purchase_id")!=null){
			Object[] objs = (Object[])map.get("vendor_bill_purchase_id");
			if(objs.length > 0){
				this.setVendor_bill_purchase_id((Integer)objs[0]);
			}
		}
		if(!(map.get("vendor_bill_purchase_id") instanceof Boolean)&& map.get("vendor_bill_purchase_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("vendor_bill_purchase_id");
			if(objs.length > 1){
				this.setVendor_bill_purchase_id_text((String)objs[1]);
			}
		}
		if(!(map.get("vendor_display_name") instanceof Boolean)&& map.get("vendor_display_name")!=null){
			this.setVendor_display_name((String)map.get("vendor_display_name"));
		}
		if(!(map.get("website_id") instanceof Boolean)&& map.get("website_id")!=null){
			Object[] objs = (Object[])map.get("website_id");
			if(objs.length > 0){
				this.setWebsite_id((Integer)objs[0]);
			}
		}
		if(!(map.get("website_message_ids") instanceof Boolean)&& map.get("website_message_ids")!=null){
			Object[] objs = (Object[])map.get("website_message_ids");
			if(objs.length > 0){
				Integer[] website_message_ids = Arrays.copyOfRange(objs,0,objs.length,Integer[].class);
				this.setWebsite_message_ids(Arrays.toString(website_message_ids).replace(" ",""));
			}
		}
		if(!(map.get("write_date") instanceof Boolean)&& map.get("write_date")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse((String)map.get("write_date"));
   			this.setWrite_date(new Timestamp(parse.getTime()));
		}
		if(!(map.get("write_uid") instanceof Boolean)&& map.get("write_uid")!=null){
			Object[] objs = (Object[])map.get("write_uid");
			if(objs.length > 0){
				this.setWrite_uid((Integer)objs[0]);
			}
		}
		if(!(map.get("write_uid") instanceof Boolean)&& map.get("write_uid")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("write_uid");
			if(objs.length > 1){
				this.setWrite_uid_text((String)objs[1]);
			}
		}
		if(!(map.get("__last_update") instanceof Boolean)&& map.get("__last_update")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse((String)map.get("__last_update"));
   			this.set__last_update(new Timestamp(parse.getTime()));
		}
	}

	public Map<String, Object> toMap() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		if(this.getAccess_token()!=null&&this.getAccess_tokenDirtyFlag()){
			map.put("access_token",this.getAccess_token());
		}else if(this.getAccess_tokenDirtyFlag()){
			map.put("access_token",false);
		}
		if(this.getAccess_url()!=null&&this.getAccess_urlDirtyFlag()){
			map.put("access_url",this.getAccess_url());
		}else if(this.getAccess_urlDirtyFlag()){
			map.put("access_url",false);
		}
		if(this.getAccess_warning()!=null&&this.getAccess_warningDirtyFlag()){
			map.put("access_warning",this.getAccess_warning());
		}else if(this.getAccess_warningDirtyFlag()){
			map.put("access_warning",false);
		}
		if(this.getAccount_id()!=null&&this.getAccount_idDirtyFlag()){
			map.put("account_id",this.getAccount_id());
		}else if(this.getAccount_idDirtyFlag()){
			map.put("account_id",false);
		}
		if(this.getAccount_id_text()!=null&&this.getAccount_id_textDirtyFlag()){
			//忽略文本外键account_id_text
		}else if(this.getAccount_id_textDirtyFlag()){
			map.put("account_id",false);
		}
		if(this.getActivity_date_deadline()!=null&&this.getActivity_date_deadlineDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			String datetimeStr = sdf.format(this.getActivity_date_deadline());
			map.put("activity_date_deadline",datetimeStr);
		}else if(this.getActivity_date_deadlineDirtyFlag()){
			map.put("activity_date_deadline",false);
		}
		if(this.getActivity_ids()!=null&&this.getActivity_idsDirtyFlag()){
			map.put("activity_ids",this.getActivity_ids());
		}else if(this.getActivity_idsDirtyFlag()){
			map.put("activity_ids",false);
		}
		if(this.getActivity_state()!=null&&this.getActivity_stateDirtyFlag()){
			map.put("activity_state",this.getActivity_state());
		}else if(this.getActivity_stateDirtyFlag()){
			map.put("activity_state",false);
		}
		if(this.getActivity_summary()!=null&&this.getActivity_summaryDirtyFlag()){
			map.put("activity_summary",this.getActivity_summary());
		}else if(this.getActivity_summaryDirtyFlag()){
			map.put("activity_summary",false);
		}
		if(this.getActivity_type_id()!=null&&this.getActivity_type_idDirtyFlag()){
			map.put("activity_type_id",this.getActivity_type_id());
		}else if(this.getActivity_type_idDirtyFlag()){
			map.put("activity_type_id",false);
		}
		if(this.getActivity_user_id()!=null&&this.getActivity_user_idDirtyFlag()){
			map.put("activity_user_id",this.getActivity_user_id());
		}else if(this.getActivity_user_idDirtyFlag()){
			map.put("activity_user_id",false);
		}
		if(this.getAmount_by_group()!=null&&this.getAmount_by_groupDirtyFlag()){
			//暂不支持binary类型amount_by_group
		}else if(this.getAmount_by_groupDirtyFlag()){
			map.put("amount_by_group",false);
		}
		if(this.getAmount_tax()!=null&&this.getAmount_taxDirtyFlag()){
			map.put("amount_tax",this.getAmount_tax());
		}else if(this.getAmount_taxDirtyFlag()){
			map.put("amount_tax",false);
		}
		if(this.getAmount_tax_signed()!=null&&this.getAmount_tax_signedDirtyFlag()){
			map.put("amount_tax_signed",this.getAmount_tax_signed());
		}else if(this.getAmount_tax_signedDirtyFlag()){
			map.put("amount_tax_signed",false);
		}
		if(this.getAmount_total()!=null&&this.getAmount_totalDirtyFlag()){
			map.put("amount_total",this.getAmount_total());
		}else if(this.getAmount_totalDirtyFlag()){
			map.put("amount_total",false);
		}
		if(this.getAmount_total_company_signed()!=null&&this.getAmount_total_company_signedDirtyFlag()){
			map.put("amount_total_company_signed",this.getAmount_total_company_signed());
		}else if(this.getAmount_total_company_signedDirtyFlag()){
			map.put("amount_total_company_signed",false);
		}
		if(this.getAmount_total_signed()!=null&&this.getAmount_total_signedDirtyFlag()){
			map.put("amount_total_signed",this.getAmount_total_signed());
		}else if(this.getAmount_total_signedDirtyFlag()){
			map.put("amount_total_signed",false);
		}
		if(this.getAmount_untaxed()!=null&&this.getAmount_untaxedDirtyFlag()){
			map.put("amount_untaxed",this.getAmount_untaxed());
		}else if(this.getAmount_untaxedDirtyFlag()){
			map.put("amount_untaxed",false);
		}
		if(this.getAmount_untaxed_invoice_signed()!=null&&this.getAmount_untaxed_invoice_signedDirtyFlag()){
			map.put("amount_untaxed_invoice_signed",this.getAmount_untaxed_invoice_signed());
		}else if(this.getAmount_untaxed_invoice_signedDirtyFlag()){
			map.put("amount_untaxed_invoice_signed",false);
		}
		if(this.getAmount_untaxed_signed()!=null&&this.getAmount_untaxed_signedDirtyFlag()){
			map.put("amount_untaxed_signed",this.getAmount_untaxed_signed());
		}else if(this.getAmount_untaxed_signedDirtyFlag()){
			map.put("amount_untaxed_signed",false);
		}
		if(this.getAuthorized_transaction_ids()!=null&&this.getAuthorized_transaction_idsDirtyFlag()){
			map.put("authorized_transaction_ids",this.getAuthorized_transaction_ids());
		}else if(this.getAuthorized_transaction_idsDirtyFlag()){
			map.put("authorized_transaction_ids",false);
		}
		if(this.getCampaign_id()!=null&&this.getCampaign_idDirtyFlag()){
			map.put("campaign_id",this.getCampaign_id());
		}else if(this.getCampaign_idDirtyFlag()){
			map.put("campaign_id",false);
		}
		if(this.getCampaign_id_text()!=null&&this.getCampaign_id_textDirtyFlag()){
			//忽略文本外键campaign_id_text
		}else if(this.getCampaign_id_textDirtyFlag()){
			map.put("campaign_id",false);
		}
		if(this.getCash_rounding_id()!=null&&this.getCash_rounding_idDirtyFlag()){
			map.put("cash_rounding_id",this.getCash_rounding_id());
		}else if(this.getCash_rounding_idDirtyFlag()){
			map.put("cash_rounding_id",false);
		}
		if(this.getCash_rounding_id_text()!=null&&this.getCash_rounding_id_textDirtyFlag()){
			//忽略文本外键cash_rounding_id_text
		}else if(this.getCash_rounding_id_textDirtyFlag()){
			map.put("cash_rounding_id",false);
		}
		if(this.getComment()!=null&&this.getCommentDirtyFlag()){
			map.put("comment",this.getComment());
		}else if(this.getCommentDirtyFlag()){
			map.put("comment",false);
		}
		if(this.getCommercial_partner_id()!=null&&this.getCommercial_partner_idDirtyFlag()){
			map.put("commercial_partner_id",this.getCommercial_partner_id());
		}else if(this.getCommercial_partner_idDirtyFlag()){
			map.put("commercial_partner_id",false);
		}
		if(this.getCommercial_partner_id_text()!=null&&this.getCommercial_partner_id_textDirtyFlag()){
			//忽略文本外键commercial_partner_id_text
		}else if(this.getCommercial_partner_id_textDirtyFlag()){
			map.put("commercial_partner_id",false);
		}
		if(this.getCompany_currency_id()!=null&&this.getCompany_currency_idDirtyFlag()){
			map.put("company_currency_id",this.getCompany_currency_id());
		}else if(this.getCompany_currency_idDirtyFlag()){
			map.put("company_currency_id",false);
		}
		if(this.getCompany_id()!=null&&this.getCompany_idDirtyFlag()){
			map.put("company_id",this.getCompany_id());
		}else if(this.getCompany_idDirtyFlag()){
			map.put("company_id",false);
		}
		if(this.getCompany_id_text()!=null&&this.getCompany_id_textDirtyFlag()){
			//忽略文本外键company_id_text
		}else if(this.getCompany_id_textDirtyFlag()){
			map.put("company_id",false);
		}
		if(this.getCreate_date()!=null&&this.getCreate_dateDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String datetimeStr = sdf.format(this.getCreate_date());
			map.put("create_date",datetimeStr);
		}else if(this.getCreate_dateDirtyFlag()){
			map.put("create_date",false);
		}
		if(this.getCreate_uid()!=null&&this.getCreate_uidDirtyFlag()){
			map.put("create_uid",this.getCreate_uid());
		}else if(this.getCreate_uidDirtyFlag()){
			map.put("create_uid",false);
		}
		if(this.getCreate_uid_text()!=null&&this.getCreate_uid_textDirtyFlag()){
			//忽略文本外键create_uid_text
		}else if(this.getCreate_uid_textDirtyFlag()){
			map.put("create_uid",false);
		}
		if(this.getCurrency_id()!=null&&this.getCurrency_idDirtyFlag()){
			map.put("currency_id",this.getCurrency_id());
		}else if(this.getCurrency_idDirtyFlag()){
			map.put("currency_id",false);
		}
		if(this.getCurrency_id_text()!=null&&this.getCurrency_id_textDirtyFlag()){
			//忽略文本外键currency_id_text
		}else if(this.getCurrency_id_textDirtyFlag()){
			map.put("currency_id",false);
		}
		if(this.getDate()!=null&&this.getDateDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			String datetimeStr = sdf.format(this.getDate());
			map.put("date",datetimeStr);
		}else if(this.getDateDirtyFlag()){
			map.put("date",false);
		}
		if(this.getDate_due()!=null&&this.getDate_dueDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			String datetimeStr = sdf.format(this.getDate_due());
			map.put("date_due",datetimeStr);
		}else if(this.getDate_dueDirtyFlag()){
			map.put("date_due",false);
		}
		if(this.getDate_invoice()!=null&&this.getDate_invoiceDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			String datetimeStr = sdf.format(this.getDate_invoice());
			map.put("date_invoice",datetimeStr);
		}else if(this.getDate_invoiceDirtyFlag()){
			map.put("date_invoice",false);
		}
		if(this.getDisplay_name()!=null&&this.getDisplay_nameDirtyFlag()){
			map.put("display_name",this.getDisplay_name());
		}else if(this.getDisplay_nameDirtyFlag()){
			map.put("display_name",false);
		}
		if(this.getFiscal_position_id()!=null&&this.getFiscal_position_idDirtyFlag()){
			map.put("fiscal_position_id",this.getFiscal_position_id());
		}else if(this.getFiscal_position_idDirtyFlag()){
			map.put("fiscal_position_id",false);
		}
		if(this.getFiscal_position_id_text()!=null&&this.getFiscal_position_id_textDirtyFlag()){
			//忽略文本外键fiscal_position_id_text
		}else if(this.getFiscal_position_id_textDirtyFlag()){
			map.put("fiscal_position_id",false);
		}
		if(this.getHas_outstanding()!=null&&this.getHas_outstandingDirtyFlag()){
			map.put("has_outstanding",Boolean.parseBoolean(this.getHas_outstanding()));		
		}		if(this.getId()!=null&&this.getIdDirtyFlag()){
			map.put("id",this.getId());
		}else if(this.getIdDirtyFlag()){
			map.put("id",false);
		}
		if(this.getIncoterms_id()!=null&&this.getIncoterms_idDirtyFlag()){
			map.put("incoterms_id",this.getIncoterms_id());
		}else if(this.getIncoterms_idDirtyFlag()){
			map.put("incoterms_id",false);
		}
		if(this.getIncoterms_id_text()!=null&&this.getIncoterms_id_textDirtyFlag()){
			//忽略文本外键incoterms_id_text
		}else if(this.getIncoterms_id_textDirtyFlag()){
			map.put("incoterms_id",false);
		}
		if(this.getIncoterm_id()!=null&&this.getIncoterm_idDirtyFlag()){
			map.put("incoterm_id",this.getIncoterm_id());
		}else if(this.getIncoterm_idDirtyFlag()){
			map.put("incoterm_id",false);
		}
		if(this.getIncoterm_id_text()!=null&&this.getIncoterm_id_textDirtyFlag()){
			//忽略文本外键incoterm_id_text
		}else if(this.getIncoterm_id_textDirtyFlag()){
			map.put("incoterm_id",false);
		}
		if(this.getInvoice_icon()!=null&&this.getInvoice_iconDirtyFlag()){
			map.put("invoice_icon",this.getInvoice_icon());
		}else if(this.getInvoice_iconDirtyFlag()){
			map.put("invoice_icon",false);
		}
		if(this.getInvoice_line_ids()!=null&&this.getInvoice_line_idsDirtyFlag()){
			map.put("invoice_line_ids",this.getInvoice_line_ids());
		}else if(this.getInvoice_line_idsDirtyFlag()){
			map.put("invoice_line_ids",false);
		}
		if(this.getJournal_id()!=null&&this.getJournal_idDirtyFlag()){
			map.put("journal_id",this.getJournal_id());
		}else if(this.getJournal_idDirtyFlag()){
			map.put("journal_id",false);
		}
		if(this.getJournal_id_text()!=null&&this.getJournal_id_textDirtyFlag()){
			//忽略文本外键journal_id_text
		}else if(this.getJournal_id_textDirtyFlag()){
			map.put("journal_id",false);
		}
		if(this.getMedium_id()!=null&&this.getMedium_idDirtyFlag()){
			map.put("medium_id",this.getMedium_id());
		}else if(this.getMedium_idDirtyFlag()){
			map.put("medium_id",false);
		}
		if(this.getMedium_id_text()!=null&&this.getMedium_id_textDirtyFlag()){
			//忽略文本外键medium_id_text
		}else if(this.getMedium_id_textDirtyFlag()){
			map.put("medium_id",false);
		}
		if(this.getMessage_attachment_count()!=null&&this.getMessage_attachment_countDirtyFlag()){
			map.put("message_attachment_count",this.getMessage_attachment_count());
		}else if(this.getMessage_attachment_countDirtyFlag()){
			map.put("message_attachment_count",false);
		}
		if(this.getMessage_channel_ids()!=null&&this.getMessage_channel_idsDirtyFlag()){
			map.put("message_channel_ids",this.getMessage_channel_ids());
		}else if(this.getMessage_channel_idsDirtyFlag()){
			map.put("message_channel_ids",false);
		}
		if(this.getMessage_follower_ids()!=null&&this.getMessage_follower_idsDirtyFlag()){
			map.put("message_follower_ids",this.getMessage_follower_ids());
		}else if(this.getMessage_follower_idsDirtyFlag()){
			map.put("message_follower_ids",false);
		}
		if(this.getMessage_has_error()!=null&&this.getMessage_has_errorDirtyFlag()){
			map.put("message_has_error",Boolean.parseBoolean(this.getMessage_has_error()));		
		}		if(this.getMessage_has_error_counter()!=null&&this.getMessage_has_error_counterDirtyFlag()){
			map.put("message_has_error_counter",this.getMessage_has_error_counter());
		}else if(this.getMessage_has_error_counterDirtyFlag()){
			map.put("message_has_error_counter",false);
		}
		if(this.getMessage_ids()!=null&&this.getMessage_idsDirtyFlag()){
			map.put("message_ids",this.getMessage_ids());
		}else if(this.getMessage_idsDirtyFlag()){
			map.put("message_ids",false);
		}
		if(this.getMessage_is_follower()!=null&&this.getMessage_is_followerDirtyFlag()){
			map.put("message_is_follower",Boolean.parseBoolean(this.getMessage_is_follower()));		
		}		if(this.getMessage_main_attachment_id()!=null&&this.getMessage_main_attachment_idDirtyFlag()){
			map.put("message_main_attachment_id",this.getMessage_main_attachment_id());
		}else if(this.getMessage_main_attachment_idDirtyFlag()){
			map.put("message_main_attachment_id",false);
		}
		if(this.getMessage_needaction()!=null&&this.getMessage_needactionDirtyFlag()){
			map.put("message_needaction",Boolean.parseBoolean(this.getMessage_needaction()));		
		}		if(this.getMessage_needaction_counter()!=null&&this.getMessage_needaction_counterDirtyFlag()){
			map.put("message_needaction_counter",this.getMessage_needaction_counter());
		}else if(this.getMessage_needaction_counterDirtyFlag()){
			map.put("message_needaction_counter",false);
		}
		if(this.getMessage_partner_ids()!=null&&this.getMessage_partner_idsDirtyFlag()){
			map.put("message_partner_ids",this.getMessage_partner_ids());
		}else if(this.getMessage_partner_idsDirtyFlag()){
			map.put("message_partner_ids",false);
		}
		if(this.getMessage_unread()!=null&&this.getMessage_unreadDirtyFlag()){
			map.put("message_unread",Boolean.parseBoolean(this.getMessage_unread()));		
		}		if(this.getMessage_unread_counter()!=null&&this.getMessage_unread_counterDirtyFlag()){
			map.put("message_unread_counter",this.getMessage_unread_counter());
		}else if(this.getMessage_unread_counterDirtyFlag()){
			map.put("message_unread_counter",false);
		}
		if(this.getMove_id()!=null&&this.getMove_idDirtyFlag()){
			map.put("move_id",this.getMove_id());
		}else if(this.getMove_idDirtyFlag()){
			map.put("move_id",false);
		}
		if(this.getMove_name()!=null&&this.getMove_nameDirtyFlag()){
			map.put("move_name",this.getMove_name());
		}else if(this.getMove_nameDirtyFlag()){
			map.put("move_name",false);
		}
		if(this.getName()!=null&&this.getNameDirtyFlag()){
			map.put("name",this.getName());
		}else if(this.getNameDirtyFlag()){
			map.put("name",false);
		}
		if(this.getNumber()!=null&&this.getNumberDirtyFlag()){
			map.put("number",this.getNumber());
		}else if(this.getNumberDirtyFlag()){
			map.put("number",false);
		}
		if(this.getOrigin()!=null&&this.getOriginDirtyFlag()){
			map.put("origin",this.getOrigin());
		}else if(this.getOriginDirtyFlag()){
			map.put("origin",false);
		}
		if(this.getOutstanding_credits_debits_widget()!=null&&this.getOutstanding_credits_debits_widgetDirtyFlag()){
			map.put("outstanding_credits_debits_widget",this.getOutstanding_credits_debits_widget());
		}else if(this.getOutstanding_credits_debits_widgetDirtyFlag()){
			map.put("outstanding_credits_debits_widget",false);
		}
		if(this.getPartner_bank_id()!=null&&this.getPartner_bank_idDirtyFlag()){
			map.put("partner_bank_id",this.getPartner_bank_id());
		}else if(this.getPartner_bank_idDirtyFlag()){
			map.put("partner_bank_id",false);
		}
		if(this.getPartner_id()!=null&&this.getPartner_idDirtyFlag()){
			map.put("partner_id",this.getPartner_id());
		}else if(this.getPartner_idDirtyFlag()){
			map.put("partner_id",false);
		}
		if(this.getPartner_id_text()!=null&&this.getPartner_id_textDirtyFlag()){
			//忽略文本外键partner_id_text
		}else if(this.getPartner_id_textDirtyFlag()){
			map.put("partner_id",false);
		}
		if(this.getPartner_shipping_id()!=null&&this.getPartner_shipping_idDirtyFlag()){
			map.put("partner_shipping_id",this.getPartner_shipping_id());
		}else if(this.getPartner_shipping_idDirtyFlag()){
			map.put("partner_shipping_id",false);
		}
		if(this.getPartner_shipping_id_text()!=null&&this.getPartner_shipping_id_textDirtyFlag()){
			//忽略文本外键partner_shipping_id_text
		}else if(this.getPartner_shipping_id_textDirtyFlag()){
			map.put("partner_shipping_id",false);
		}
		if(this.getPayments_widget()!=null&&this.getPayments_widgetDirtyFlag()){
			map.put("payments_widget",this.getPayments_widget());
		}else if(this.getPayments_widgetDirtyFlag()){
			map.put("payments_widget",false);
		}
		if(this.getPayment_ids()!=null&&this.getPayment_idsDirtyFlag()){
			map.put("payment_ids",this.getPayment_ids());
		}else if(this.getPayment_idsDirtyFlag()){
			map.put("payment_ids",false);
		}
		if(this.getPayment_move_line_ids()!=null&&this.getPayment_move_line_idsDirtyFlag()){
			map.put("payment_move_line_ids",this.getPayment_move_line_ids());
		}else if(this.getPayment_move_line_idsDirtyFlag()){
			map.put("payment_move_line_ids",false);
		}
		if(this.getPayment_term_id()!=null&&this.getPayment_term_idDirtyFlag()){
			map.put("payment_term_id",this.getPayment_term_id());
		}else if(this.getPayment_term_idDirtyFlag()){
			map.put("payment_term_id",false);
		}
		if(this.getPayment_term_id_text()!=null&&this.getPayment_term_id_textDirtyFlag()){
			//忽略文本外键payment_term_id_text
		}else if(this.getPayment_term_id_textDirtyFlag()){
			map.put("payment_term_id",false);
		}
		if(this.getPurchase_id()!=null&&this.getPurchase_idDirtyFlag()){
			map.put("purchase_id",this.getPurchase_id());
		}else if(this.getPurchase_idDirtyFlag()){
			map.put("purchase_id",false);
		}
		if(this.getPurchase_id_text()!=null&&this.getPurchase_id_textDirtyFlag()){
			//忽略文本外键purchase_id_text
		}else if(this.getPurchase_id_textDirtyFlag()){
			map.put("purchase_id",false);
		}
		if(this.getReconciled()!=null&&this.getReconciledDirtyFlag()){
			map.put("reconciled",Boolean.parseBoolean(this.getReconciled()));		
		}		if(this.getReference()!=null&&this.getReferenceDirtyFlag()){
			map.put("reference",this.getReference());
		}else if(this.getReferenceDirtyFlag()){
			map.put("reference",false);
		}
		if(this.getRefund_invoice_id()!=null&&this.getRefund_invoice_idDirtyFlag()){
			map.put("refund_invoice_id",this.getRefund_invoice_id());
		}else if(this.getRefund_invoice_idDirtyFlag()){
			map.put("refund_invoice_id",false);
		}
		if(this.getRefund_invoice_ids()!=null&&this.getRefund_invoice_idsDirtyFlag()){
			map.put("refund_invoice_ids",this.getRefund_invoice_ids());
		}else if(this.getRefund_invoice_idsDirtyFlag()){
			map.put("refund_invoice_ids",false);
		}
		if(this.getRefund_invoice_id_text()!=null&&this.getRefund_invoice_id_textDirtyFlag()){
			//忽略文本外键refund_invoice_id_text
		}else if(this.getRefund_invoice_id_textDirtyFlag()){
			map.put("refund_invoice_id",false);
		}
		if(this.getResidual()!=null&&this.getResidualDirtyFlag()){
			map.put("residual",this.getResidual());
		}else if(this.getResidualDirtyFlag()){
			map.put("residual",false);
		}
		if(this.getResidual_company_signed()!=null&&this.getResidual_company_signedDirtyFlag()){
			map.put("residual_company_signed",this.getResidual_company_signed());
		}else if(this.getResidual_company_signedDirtyFlag()){
			map.put("residual_company_signed",false);
		}
		if(this.getResidual_signed()!=null&&this.getResidual_signedDirtyFlag()){
			map.put("residual_signed",this.getResidual_signed());
		}else if(this.getResidual_signedDirtyFlag()){
			map.put("residual_signed",false);
		}
		if(this.getSent()!=null&&this.getSentDirtyFlag()){
			map.put("sent",Boolean.parseBoolean(this.getSent()));		
		}		if(this.getSequence_number_next()!=null&&this.getSequence_number_nextDirtyFlag()){
			map.put("sequence_number_next",this.getSequence_number_next());
		}else if(this.getSequence_number_nextDirtyFlag()){
			map.put("sequence_number_next",false);
		}
		if(this.getSequence_number_next_prefix()!=null&&this.getSequence_number_next_prefixDirtyFlag()){
			map.put("sequence_number_next_prefix",this.getSequence_number_next_prefix());
		}else if(this.getSequence_number_next_prefixDirtyFlag()){
			map.put("sequence_number_next_prefix",false);
		}
		if(this.getSource_email()!=null&&this.getSource_emailDirtyFlag()){
			map.put("source_email",this.getSource_email());
		}else if(this.getSource_emailDirtyFlag()){
			map.put("source_email",false);
		}
		if(this.getSource_id()!=null&&this.getSource_idDirtyFlag()){
			map.put("source_id",this.getSource_id());
		}else if(this.getSource_idDirtyFlag()){
			map.put("source_id",false);
		}
		if(this.getSource_id_text()!=null&&this.getSource_id_textDirtyFlag()){
			//忽略文本外键source_id_text
		}else if(this.getSource_id_textDirtyFlag()){
			map.put("source_id",false);
		}
		if(this.getState()!=null&&this.getStateDirtyFlag()){
			map.put("state",this.getState());
		}else if(this.getStateDirtyFlag()){
			map.put("state",false);
		}
		if(this.getTax_line_ids()!=null&&this.getTax_line_idsDirtyFlag()){
			map.put("tax_line_ids",this.getTax_line_ids());
		}else if(this.getTax_line_idsDirtyFlag()){
			map.put("tax_line_ids",false);
		}
		if(this.getTeam_id()!=null&&this.getTeam_idDirtyFlag()){
			map.put("team_id",this.getTeam_id());
		}else if(this.getTeam_idDirtyFlag()){
			map.put("team_id",false);
		}
		if(this.getTeam_id_text()!=null&&this.getTeam_id_textDirtyFlag()){
			//忽略文本外键team_id_text
		}else if(this.getTeam_id_textDirtyFlag()){
			map.put("team_id",false);
		}
		if(this.getTransaction_ids()!=null&&this.getTransaction_idsDirtyFlag()){
			map.put("transaction_ids",this.getTransaction_ids());
		}else if(this.getTransaction_idsDirtyFlag()){
			map.put("transaction_ids",false);
		}
		if(this.getType()!=null&&this.getTypeDirtyFlag()){
			map.put("type",this.getType());
		}else if(this.getTypeDirtyFlag()){
			map.put("type",false);
		}
		if(this.getUser_id()!=null&&this.getUser_idDirtyFlag()){
			map.put("user_id",this.getUser_id());
		}else if(this.getUser_idDirtyFlag()){
			map.put("user_id",false);
		}
		if(this.getUser_id_text()!=null&&this.getUser_id_textDirtyFlag()){
			//忽略文本外键user_id_text
		}else if(this.getUser_id_textDirtyFlag()){
			map.put("user_id",false);
		}
		if(this.getVendor_bill_id()!=null&&this.getVendor_bill_idDirtyFlag()){
			map.put("vendor_bill_id",this.getVendor_bill_id());
		}else if(this.getVendor_bill_idDirtyFlag()){
			map.put("vendor_bill_id",false);
		}
		if(this.getVendor_bill_id_text()!=null&&this.getVendor_bill_id_textDirtyFlag()){
			//忽略文本外键vendor_bill_id_text
		}else if(this.getVendor_bill_id_textDirtyFlag()){
			map.put("vendor_bill_id",false);
		}
		if(this.getVendor_bill_purchase_id()!=null&&this.getVendor_bill_purchase_idDirtyFlag()){
			map.put("vendor_bill_purchase_id",this.getVendor_bill_purchase_id());
		}else if(this.getVendor_bill_purchase_idDirtyFlag()){
			map.put("vendor_bill_purchase_id",false);
		}
		if(this.getVendor_bill_purchase_id_text()!=null&&this.getVendor_bill_purchase_id_textDirtyFlag()){
			//忽略文本外键vendor_bill_purchase_id_text
		}else if(this.getVendor_bill_purchase_id_textDirtyFlag()){
			map.put("vendor_bill_purchase_id",false);
		}
		if(this.getVendor_display_name()!=null&&this.getVendor_display_nameDirtyFlag()){
			map.put("vendor_display_name",this.getVendor_display_name());
		}else if(this.getVendor_display_nameDirtyFlag()){
			map.put("vendor_display_name",false);
		}
		if(this.getWebsite_id()!=null&&this.getWebsite_idDirtyFlag()){
			map.put("website_id",this.getWebsite_id());
		}else if(this.getWebsite_idDirtyFlag()){
			map.put("website_id",false);
		}
		if(this.getWebsite_message_ids()!=null&&this.getWebsite_message_idsDirtyFlag()){
			map.put("website_message_ids",this.getWebsite_message_ids());
		}else if(this.getWebsite_message_idsDirtyFlag()){
			map.put("website_message_ids",false);
		}
		if(this.getWrite_date()!=null&&this.getWrite_dateDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String datetimeStr = sdf.format(this.getWrite_date());
			map.put("write_date",datetimeStr);
		}else if(this.getWrite_dateDirtyFlag()){
			map.put("write_date",false);
		}
		if(this.getWrite_uid()!=null&&this.getWrite_uidDirtyFlag()){
			map.put("write_uid",this.getWrite_uid());
		}else if(this.getWrite_uidDirtyFlag()){
			map.put("write_uid",false);
		}
		if(this.getWrite_uid_text()!=null&&this.getWrite_uid_textDirtyFlag()){
			//忽略文本外键write_uid_text
		}else if(this.getWrite_uid_textDirtyFlag()){
			map.put("write_uid",false);
		}
		if(this.get__last_update()!=null&&this.get__last_updateDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String datetimeStr = sdf.format(this.get__last_update());
			map.put("__last_update",datetimeStr);
		}else if(this.get__last_updateDirtyFlag()){
			map.put("__last_update",false);
		}
		return map;
	}

}
