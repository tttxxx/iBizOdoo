package cn.ibizlab.odoo.core.odoo_base.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_base.domain.Res_bank;
import cn.ibizlab.odoo.core.odoo_base.filter.Res_bankSearchContext;
import cn.ibizlab.odoo.core.odoo_base.service.IRes_bankService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;



import cn.ibizlab.odoo.core.odoo_base.client.res_bankOdooClient;
import cn.ibizlab.odoo.core.odoo_base.clientmodel.res_bankClientModel;

/**
 * 实体[银行] 服务对象接口实现
 */
@Slf4j
@Service
public class Res_bankServiceImpl implements IRes_bankService {

    @Autowired
    res_bankOdooClient res_bankOdooClient;


    @Override
    public boolean create(Res_bank et) {
        res_bankClientModel clientModel = convert2Model(et,null);
		res_bankOdooClient.create(clientModel);
        Res_bank rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Res_bank> list){
    }

    @Override
    public Res_bank get(Integer id) {
        res_bankClientModel clientModel = new res_bankClientModel();
        clientModel.setId(id);
		res_bankOdooClient.get(clientModel);
        Res_bank et = convert2Domain(clientModel,null);
        if(et==null){
            et=new Res_bank();
            et.setId(id);
        }
        else{
        }

        return  et;
    }

    @Override
    public boolean remove(Integer id) {
        res_bankClientModel clientModel = new res_bankClientModel();
        clientModel.setId(id);
		res_bankOdooClient.remove(clientModel);
        return true; 
    }

    public void removeBatch(Collection<Integer> idList){
    }

    @Override
    public boolean update(Res_bank et) {
        res_bankClientModel clientModel = convert2Model(et,null);
		res_bankOdooClient.update(clientModel);
        Res_bank rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }
    
    public void updateBatch(List<Res_bank> list){
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Res_bank> searchDefault(Res_bankSearchContext context) {
        List<Res_bank> list = new ArrayList<Res_bank>();
        Page<res_bankClientModel> clientModelList = res_bankOdooClient.search(context);
        for(res_bankClientModel clientModel : clientModelList){
            list.add( this.convert2Domain(clientModel,null));
        }
        return new PageImpl<Res_bank>(list, context.getPageable(), clientModelList.getTotalElements());
    }

    public res_bankClientModel convert2Model(Res_bank domain , res_bankClientModel model) {
        if(domain == null)
            return model ;
        if(model==null){
            model = new res_bankClientModel();

        }
        if((Boolean) domain.getExtensionparams().get("dirtyflagenable")){
            if((Boolean) domain.getExtensionparams().get("street2dirtyflag"))
                model.setStreet2(domain.getStreet2());
            if((Boolean) domain.getExtensionparams().get("phonedirtyflag"))
                model.setPhone(domain.getPhone());
            if((Boolean) domain.getExtensionparams().get("citydirtyflag"))
                model.setCity(domain.getCity());
            if((Boolean) domain.getExtensionparams().get("streetdirtyflag"))
                model.setStreet(domain.getStreet());
            if((Boolean) domain.getExtensionparams().get("namedirtyflag"))
                model.setName(domain.getName());
            if((Boolean) domain.getExtensionparams().get("emaildirtyflag"))
                model.setEmail(domain.getEmail());
            if((Boolean) domain.getExtensionparams().get("activedirtyflag"))
                model.setActive(domain.getActive());
            if((Boolean) domain.getExtensionparams().get("display_namedirtyflag"))
                model.setDisplay_name(domain.getDisplayName());
            if((Boolean) domain.getExtensionparams().get("__last_updatedirtyflag"))
                model.set__last_update(domain.getLastUpdate());
            if((Boolean) domain.getExtensionparams().get("iddirtyflag"))
                model.setId(domain.getId());
            if((Boolean) domain.getExtensionparams().get("bicdirtyflag"))
                model.setBic(domain.getBic());
            if((Boolean) domain.getExtensionparams().get("create_datedirtyflag"))
                model.setCreate_date(domain.getCreateDate());
            if((Boolean) domain.getExtensionparams().get("zipdirtyflag"))
                model.setZip(domain.getZip());
            if((Boolean) domain.getExtensionparams().get("write_datedirtyflag"))
                model.setWrite_date(domain.getWriteDate());
            if((Boolean) domain.getExtensionparams().get("write_uid_textdirtyflag"))
                model.setWrite_uid_text(domain.getWriteUidText());
            if((Boolean) domain.getExtensionparams().get("create_uid_textdirtyflag"))
                model.setCreate_uid_text(domain.getCreateUidText());
            if((Boolean) domain.getExtensionparams().get("country_textdirtyflag"))
                model.setCountry_text(domain.getCountryText());
            if((Boolean) domain.getExtensionparams().get("state_textdirtyflag"))
                model.setState_text(domain.getStateText());
            if((Boolean) domain.getExtensionparams().get("create_uiddirtyflag"))
                model.setCreate_uid(domain.getCreateUid());
            if((Boolean) domain.getExtensionparams().get("countrydirtyflag"))
                model.setCountry(domain.getCountry());
            if((Boolean) domain.getExtensionparams().get("statedirtyflag"))
                model.setState(domain.getState());
            if((Boolean) domain.getExtensionparams().get("write_uiddirtyflag"))
                model.setWrite_uid(domain.getWriteUid());
            domain.getFocusNull().clear();
        }
        return model ;
    }

    public Res_bank convert2Domain( res_bankClientModel model ,Res_bank domain) {
        if(model == null)
            return domain ;
        if(domain == null){
            domain = new Res_bank();
        }

        if(model.getStreet2DirtyFlag())
            domain.setStreet2(model.getStreet2());
        if(model.getPhoneDirtyFlag())
            domain.setPhone(model.getPhone());
        if(model.getCityDirtyFlag())
            domain.setCity(model.getCity());
        if(model.getStreetDirtyFlag())
            domain.setStreet(model.getStreet());
        if(model.getNameDirtyFlag())
            domain.setName(model.getName());
        if(model.getEmailDirtyFlag())
            domain.setEmail(model.getEmail());
        if(model.getActiveDirtyFlag())
            domain.setActive(model.getActive());
        if(model.getDisplay_nameDirtyFlag())
            domain.setDisplayName(model.getDisplay_name());
        if(model.get__last_updateDirtyFlag())
            domain.setLastUpdate(model.get__last_update());
        if(model.getIdDirtyFlag())
            domain.setId(model.getId());
        if(model.getBicDirtyFlag())
            domain.setBic(model.getBic());
        if(model.getCreate_dateDirtyFlag())
            domain.setCreateDate(model.getCreate_date());
        if(model.getZipDirtyFlag())
            domain.setZip(model.getZip());
        if(model.getWrite_dateDirtyFlag())
            domain.setWriteDate(model.getWrite_date());
        if(model.getWrite_uid_textDirtyFlag())
            domain.setWriteUidText(model.getWrite_uid_text());
        if(model.getCreate_uid_textDirtyFlag())
            domain.setCreateUidText(model.getCreate_uid_text());
        if(model.getCountry_textDirtyFlag())
            domain.setCountryText(model.getCountry_text());
        if(model.getState_textDirtyFlag())
            domain.setStateText(model.getState_text());
        if(model.getCreate_uidDirtyFlag())
            domain.setCreateUid(model.getCreate_uid());
        if(model.getCountryDirtyFlag())
            domain.setCountry(model.getCountry());
        if(model.getStateDirtyFlag())
            domain.setState(model.getState());
        if(model.getWrite_uidDirtyFlag())
            domain.setWriteUid(model.getWrite_uid());
        return domain ;
    }

}

    



