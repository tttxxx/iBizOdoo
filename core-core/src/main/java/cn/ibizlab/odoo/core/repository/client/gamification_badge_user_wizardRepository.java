package cn.ibizlab.odoo.core.repository.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import cn.ibizlab.odoo.core.repository.client.po.gamification_badge_user_wizard;

/**
 * 实体[gamification_badge_user_wizard] 服务对象接口
 */
public interface gamification_badge_user_wizardRepository{


    public gamification_badge_user_wizard createPO() ;
        public void updateBatch(gamification_badge_user_wizard gamification_badge_user_wizard);

        public void removeBatch(String id);

        public void get(String id);

        public void remove(String id);

        public void create(gamification_badge_user_wizard gamification_badge_user_wizard);

        public List<gamification_badge_user_wizard> search();

        public void update(gamification_badge_user_wizard gamification_badge_user_wizard);

        public void createBatch(gamification_badge_user_wizard gamification_badge_user_wizard);


}
