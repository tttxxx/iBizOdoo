package cn.ibizlab.odoo.core.repository.client.po;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigDecimal;

/**
 * [mro_pm_rule] 对象
 */
public interface mro_pm_rule {

    public String getActive();

    public void setActive(String active);

    public Integer getCategory_id();

    public void setCategory_id(Integer category_id);

    public String getCategory_id_text();

    public void setCategory_id_text(String category_id_text);

    public Timestamp getCreate_date();

    public void setCreate_date(Timestamp create_date);

    public Integer getCreate_uid();

    public void setCreate_uid(Integer create_uid);

    public String getCreate_uid_text();

    public void setCreate_uid_text(String create_uid_text);

    public String getDisplay_name();

    public void setDisplay_name(String display_name);

    public Double getHorizon();

    public void setHorizon(Double horizon);

    public Integer getId();

    public void setId(Integer id);

    public String getName();

    public void setName(String name);

    public Integer getParameter_id();

    public void setParameter_id(Integer parameter_id);

    public String getParameter_id_text();

    public void setParameter_id_text(String parameter_id_text);

    public Integer getParameter_uom();

    public void setParameter_uom(Integer parameter_uom);

    public String getParameter_uom_text();

    public void setParameter_uom_text(String parameter_uom_text);

    public String getPm_rules_line_ids();

    public void setPm_rules_line_ids(String pm_rules_line_ids);

    public Timestamp getWrite_date();

    public void setWrite_date(Timestamp write_date);

    public Integer getWrite_uid();

    public void setWrite_uid(Integer write_uid);

    public String getWrite_uid_text();

    public void setWrite_uid_text(String write_uid_text);

    public Timestamp get__last_update();

    public void set__last_update(Timestamp __last_update);

}
