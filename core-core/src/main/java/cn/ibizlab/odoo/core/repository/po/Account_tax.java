package cn.ibizlab.odoo.core.repository.po ;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.math.BigInteger;

import cn.ibizlab.odoo.core.odoo_account.filter.Account_taxSearchContext;

/**
 * 实体 [税率] 存储模型
 */
public interface Account_tax{

    /**
     * ID
     */
    Integer getId();

    void setId(Integer id);

    /**
     * 获取 [ID]脏标记
     */
    boolean getIdDirtyFlag();

    /**
     * 创建时间
     */
    Timestamp getCreate_date();

    void setCreate_date(Timestamp create_date);

    /**
     * 获取 [创建时间]脏标记
     */
    boolean getCreate_dateDirtyFlag();

    /**
     * 显示名称
     */
    String getDisplay_name();

    void setDisplay_name(String display_name);

    /**
     * 获取 [显示名称]脏标记
     */
    boolean getDisplay_nameDirtyFlag();

    /**
     * 税范围
     */
    String getType_tax_use();

    void setType_tax_use(String type_tax_use);

    /**
     * 获取 [税范围]脏标记
     */
    boolean getType_tax_useDirtyFlag();

    /**
     * 有效
     */
    String getActive();

    void setActive(String active);

    /**
     * 获取 [有效]脏标记
     */
    boolean getActiveDirtyFlag();

    /**
     * 子级税
     */
    String getChildren_tax_ids();

    void setChildren_tax_ids(String children_tax_ids);

    /**
     * 获取 [子级税]脏标记
     */
    boolean getChildren_tax_idsDirtyFlag();

    /**
     * 包含在分析成本
     */
    String getAnalytic();

    void setAnalytic(String analytic);

    /**
     * 获取 [包含在分析成本]脏标记
     */
    boolean getAnalyticDirtyFlag();

    /**
     * 包含在价格中
     */
    String getPrice_include();

    void setPrice_include(String price_include);

    /**
     * 获取 [包含在价格中]脏标记
     */
    boolean getPrice_includeDirtyFlag();

    /**
     * 发票上的标签
     */
    String getDescription();

    void setDescription(String description);

    /**
     * 获取 [发票上的标签]脏标记
     */
    boolean getDescriptionDirtyFlag();

    /**
     * 最后更新时间
     */
    Timestamp getWrite_date();

    void setWrite_date(Timestamp write_date);

    /**
     * 获取 [最后更新时间]脏标记
     */
    boolean getWrite_dateDirtyFlag();

    /**
     * 应有税金
     */
    String getTax_exigibility();

    void setTax_exigibility(String tax_exigibility);

    /**
     * 获取 [应有税金]脏标记
     */
    boolean getTax_exigibilityDirtyFlag();

    /**
     * 税率名称
     */
    String getName();

    void setName(String name);

    /**
     * 获取 [税率名称]脏标记
     */
    boolean getNameDirtyFlag();

    /**
     * 标签
     */
    String getTag_ids();

    void setTag_ids(String tag_ids);

    /**
     * 获取 [标签]脏标记
     */
    boolean getTag_idsDirtyFlag();

    /**
     * 税率计算
     */
    String getAmount_type();

    void setAmount_type(String amount_type);

    /**
     * 获取 [税率计算]脏标记
     */
    boolean getAmount_typeDirtyFlag();

    /**
     * 最后修改日
     */
    Timestamp get__last_update();

    void set__last_update(Timestamp __last_update);

    /**
     * 获取 [最后修改日]脏标记
     */
    boolean get__last_updateDirtyFlag();

    /**
     * 金额
     */
    Double getAmount();

    void setAmount(Double amount);

    /**
     * 获取 [金额]脏标记
     */
    boolean getAmountDirtyFlag();

    /**
     * 序号
     */
    Integer getSequence();

    void setSequence(Integer sequence);

    /**
     * 获取 [序号]脏标记
     */
    boolean getSequenceDirtyFlag();

    /**
     * 影响后续税收的基础
     */
    String getInclude_base_amount();

    void setInclude_base_amount(String include_base_amount);

    /**
     * 获取 [影响后续税收的基础]脏标记
     */
    boolean getInclude_base_amountDirtyFlag();

    /**
     * 基本税应收科目
     */
    String getCash_basis_base_account_id_text();

    void setCash_basis_base_account_id_text(String cash_basis_base_account_id_text);

    /**
     * 获取 [基本税应收科目]脏标记
     */
    boolean getCash_basis_base_account_id_textDirtyFlag();

    /**
     * 税应收科目
     */
    String getCash_basis_account_id_text();

    void setCash_basis_account_id_text(String cash_basis_account_id_text);

    /**
     * 获取 [税应收科目]脏标记
     */
    boolean getCash_basis_account_id_textDirtyFlag();

    /**
     * 隐藏现金收付制选项
     */
    String getHide_tax_exigibility();

    void setHide_tax_exigibility(String hide_tax_exigibility);

    /**
     * 获取 [隐藏现金收付制选项]脏标记
     */
    boolean getHide_tax_exigibilityDirtyFlag();

    /**
     * 税组
     */
    String getTax_group_id_text();

    void setTax_group_id_text(String tax_group_id_text);

    /**
     * 获取 [税组]脏标记
     */
    boolean getTax_group_id_textDirtyFlag();

    /**
     * 公司
     */
    String getCompany_id_text();

    void setCompany_id_text(String company_id_text);

    /**
     * 获取 [公司]脏标记
     */
    boolean getCompany_id_textDirtyFlag();

    /**
     * 退款单的税率科目
     */
    String getRefund_account_id_text();

    void setRefund_account_id_text(String refund_account_id_text);

    /**
     * 获取 [退款单的税率科目]脏标记
     */
    boolean getRefund_account_id_textDirtyFlag();

    /**
     * 税率科目
     */
    String getAccount_id_text();

    void setAccount_id_text(String account_id_text);

    /**
     * 获取 [税率科目]脏标记
     */
    boolean getAccount_id_textDirtyFlag();

    /**
     * 最后更新人
     */
    String getWrite_uid_text();

    void setWrite_uid_text(String write_uid_text);

    /**
     * 获取 [最后更新人]脏标记
     */
    boolean getWrite_uid_textDirtyFlag();

    /**
     * 创建人
     */
    String getCreate_uid_text();

    void setCreate_uid_text(String create_uid_text);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uid_textDirtyFlag();

    /**
     * 退款单的税率科目
     */
    Integer getRefund_account_id();

    void setRefund_account_id(Integer refund_account_id);

    /**
     * 获取 [退款单的税率科目]脏标记
     */
    boolean getRefund_account_idDirtyFlag();

    /**
     * 税应收科目
     */
    Integer getCash_basis_account_id();

    void setCash_basis_account_id(Integer cash_basis_account_id);

    /**
     * 获取 [税应收科目]脏标记
     */
    boolean getCash_basis_account_idDirtyFlag();

    /**
     * 创建人
     */
    Integer getCreate_uid();

    void setCreate_uid(Integer create_uid);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uidDirtyFlag();

    /**
     * 最后更新人
     */
    Integer getWrite_uid();

    void setWrite_uid(Integer write_uid);

    /**
     * 获取 [最后更新人]脏标记
     */
    boolean getWrite_uidDirtyFlag();

    /**
     * 税率科目
     */
    Integer getAccount_id();

    void setAccount_id(Integer account_id);

    /**
     * 获取 [税率科目]脏标记
     */
    boolean getAccount_idDirtyFlag();

    /**
     * 基本税应收科目
     */
    Integer getCash_basis_base_account_id();

    void setCash_basis_base_account_id(Integer cash_basis_base_account_id);

    /**
     * 获取 [基本税应收科目]脏标记
     */
    boolean getCash_basis_base_account_idDirtyFlag();

    /**
     * 税组
     */
    Integer getTax_group_id();

    void setTax_group_id(Integer tax_group_id);

    /**
     * 获取 [税组]脏标记
     */
    boolean getTax_group_idDirtyFlag();

    /**
     * 公司
     */
    Integer getCompany_id();

    void setCompany_id(Integer company_id);

    /**
     * 获取 [公司]脏标记
     */
    boolean getCompany_idDirtyFlag();

}
