package cn.ibizlab.odoo.core.repository.po ;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.math.BigInteger;

import cn.ibizlab.odoo.core.odoo_purchase.filter.Purchase_orderSearchContext;

/**
 * 实体 [采购订单] 存储模型
 */
public interface Purchase_order{

    /**
     * 是否要运送
     */
    String getIs_shipped();

    void setIs_shipped(String is_shipped);

    /**
     * 获取 [是否要运送]脏标记
     */
    boolean getIs_shippedDirtyFlag();

    /**
     * 最后更新时间
     */
    Timestamp getWrite_date();

    void setWrite_date(Timestamp write_date);

    /**
     * 获取 [最后更新时间]脏标记
     */
    boolean getWrite_dateDirtyFlag();

    /**
     * 安全令牌
     */
    String getAccess_token();

    void setAccess_token(String access_token);

    /**
     * 获取 [安全令牌]脏标记
     */
    boolean getAccess_tokenDirtyFlag();

    /**
     * 责任用户
     */
    Integer getActivity_user_id();

    void setActivity_user_id(Integer activity_user_id);

    /**
     * 获取 [责任用户]脏标记
     */
    boolean getActivity_user_idDirtyFlag();

    /**
     * 未读消息
     */
    String getMessage_unread();

    void setMessage_unread(String message_unread);

    /**
     * 获取 [未读消息]脏标记
     */
    boolean getMessage_unreadDirtyFlag();

    /**
     * 关注者(渠道)
     */
    String getMessage_channel_ids();

    void setMessage_channel_ids(String message_channel_ids);

    /**
     * 获取 [关注者(渠道)]脏标记
     */
    boolean getMessage_channel_idsDirtyFlag();

    /**
     * 订单行
     */
    String getOrder_line();

    void setOrder_line(String order_line);

    /**
     * 获取 [订单行]脏标记
     */
    boolean getOrder_lineDirtyFlag();

    /**
     * 下一活动截止日期
     */
    Timestamp getActivity_date_deadline();

    void setActivity_date_deadline(Timestamp activity_date_deadline);

    /**
     * 获取 [下一活动截止日期]脏标记
     */
    boolean getActivity_date_deadlineDirtyFlag();

    /**
     * 条款和条件
     */
    String getNotes();

    void setNotes(String notes);

    /**
     * 获取 [条款和条件]脏标记
     */
    boolean getNotesDirtyFlag();

    /**
     * 总计
     */
    Double getAmount_total();

    void setAmount_total(Double amount_total);

    /**
     * 获取 [总计]脏标记
     */
    boolean getAmount_totalDirtyFlag();

    /**
     * 下一活动摘要
     */
    String getActivity_summary();

    void setActivity_summary(String activity_summary);

    /**
     * 获取 [下一活动摘要]脏标记
     */
    boolean getActivity_summaryDirtyFlag();

    /**
     * 税率
     */
    Double getAmount_tax();

    void setAmount_tax(Double amount_tax);

    /**
     * 获取 [税率]脏标记
     */
    boolean getAmount_taxDirtyFlag();

    /**
     * 门户访问网址
     */
    String getAccess_url();

    void setAccess_url(String access_url);

    /**
     * 获取 [门户访问网址]脏标记
     */
    boolean getAccess_urlDirtyFlag();

    /**
     * 账单数量
     */
    Integer getInvoice_count();

    void setInvoice_count(Integer invoice_count);

    /**
     * 获取 [账单数量]脏标记
     */
    boolean getInvoice_countDirtyFlag();

    /**
     * 未税金额
     */
    Double getAmount_untaxed();

    void setAmount_untaxed(Double amount_untaxed);

    /**
     * 获取 [未税金额]脏标记
     */
    boolean getAmount_untaxedDirtyFlag();

    /**
     * 接收
     */
    String getPicking_ids();

    void setPicking_ids(String picking_ids);

    /**
     * 获取 [接收]脏标记
     */
    boolean getPicking_idsDirtyFlag();

    /**
     * 创建时间
     */
    Timestamp getCreate_date();

    void setCreate_date(Timestamp create_date);

    /**
     * 获取 [创建时间]脏标记
     */
    boolean getCreate_dateDirtyFlag();

    /**
     * 关注者(业务伙伴)
     */
    String getMessage_partner_ids();

    void setMessage_partner_ids(String message_partner_ids);

    /**
     * 获取 [关注者(业务伙伴)]脏标记
     */
    boolean getMessage_partner_idsDirtyFlag();

    /**
     * 访问警告
     */
    String getAccess_warning();

    void setAccess_warning(String access_warning);

    /**
     * 获取 [访问警告]脏标记
     */
    boolean getAccess_warningDirtyFlag();

    /**
     * 最后修改日
     */
    Timestamp get__last_update();

    void set__last_update(Timestamp __last_update);

    /**
     * 获取 [最后修改日]脏标记
     */
    boolean get__last_updateDirtyFlag();

    /**
     * 状态
     */
    String getState();

    void setState(String state);

    /**
     * 获取 [状态]脏标记
     */
    boolean getStateDirtyFlag();

    /**
     * 动作编号
     */
    Integer getMessage_needaction_counter();

    void setMessage_needaction_counter(Integer message_needaction_counter);

    /**
     * 获取 [动作编号]脏标记
     */
    boolean getMessage_needaction_counterDirtyFlag();

    /**
     * 下一活动类型
     */
    Integer getActivity_type_id();

    void setActivity_type_id(Integer activity_type_id);

    /**
     * 获取 [下一活动类型]脏标记
     */
    boolean getActivity_type_idDirtyFlag();

    /**
     * 拣货数
     */
    Integer getPicking_count();

    void setPicking_count(Integer picking_count);

    /**
     * 获取 [拣货数]脏标记
     */
    boolean getPicking_countDirtyFlag();

    /**
     * 消息
     */
    String getMessage_ids();

    void setMessage_ids(String message_ids);

    /**
     * 获取 [消息]脏标记
     */
    boolean getMessage_idsDirtyFlag();

    /**
     * 未读消息计数器
     */
    Integer getMessage_unread_counter();

    void setMessage_unread_counter(Integer message_unread_counter);

    /**
     * 获取 [未读消息计数器]脏标记
     */
    boolean getMessage_unread_counterDirtyFlag();

    /**
     * 消息传输错误
     */
    String getMessage_has_error();

    void setMessage_has_error(String message_has_error);

    /**
     * 获取 [消息传输错误]脏标记
     */
    boolean getMessage_has_errorDirtyFlag();

    /**
     * 账单
     */
    String getInvoice_ids();

    void setInvoice_ids(String invoice_ids);

    /**
     * 获取 [账单]脏标记
     */
    boolean getInvoice_idsDirtyFlag();

    /**
     * ID
     */
    Integer getId();

    void setId(Integer id);

    /**
     * 获取 [ID]脏标记
     */
    boolean getIdDirtyFlag();

    /**
     * 产品
     */
    Integer getProduct_id();

    void setProduct_id(Integer product_id);

    /**
     * 获取 [产品]脏标记
     */
    boolean getProduct_idDirtyFlag();

    /**
     * 网站信息
     */
    String getWebsite_message_ids();

    void setWebsite_message_ids(String website_message_ids);

    /**
     * 获取 [网站信息]脏标记
     */
    boolean getWebsite_message_idsDirtyFlag();

    /**
     * 错误数
     */
    Integer getMessage_has_error_counter();

    void setMessage_has_error_counter(Integer message_has_error_counter);

    /**
     * 获取 [错误数]脏标记
     */
    boolean getMessage_has_error_counterDirtyFlag();

    /**
     * 是关注者
     */
    String getMessage_is_follower();

    void setMessage_is_follower(String message_is_follower);

    /**
     * 获取 [是关注者]脏标记
     */
    boolean getMessage_is_followerDirtyFlag();

    /**
     * 单据日期
     */
    Timestamp getDate_order();

    void setDate_order(Timestamp date_order);

    /**
     * 获取 [单据日期]脏标记
     */
    boolean getDate_orderDirtyFlag();

    /**
     * 附件数量
     */
    Integer getMessage_attachment_count();

    void setMessage_attachment_count(Integer message_attachment_count);

    /**
     * 获取 [附件数量]脏标记
     */
    boolean getMessage_attachment_countDirtyFlag();

    /**
     * 前置操作
     */
    String getMessage_needaction();

    void setMessage_needaction(String message_needaction);

    /**
     * 获取 [前置操作]脏标记
     */
    boolean getMessage_needactionDirtyFlag();

    /**
     * 附件
     */
    Integer getMessage_main_attachment_id();

    void setMessage_main_attachment_id(Integer message_main_attachment_id);

    /**
     * 获取 [附件]脏标记
     */
    boolean getMessage_main_attachment_idDirtyFlag();

    /**
     * 目标位置类型
     */
    String getDefault_location_dest_id_usage();

    void setDefault_location_dest_id_usage(String default_location_dest_id_usage);

    /**
     * 获取 [目标位置类型]脏标记
     */
    boolean getDefault_location_dest_id_usageDirtyFlag();

    /**
     * 订单关联
     */
    String getName();

    void setName(String name);

    /**
     * 获取 [订单关联]脏标记
     */
    boolean getNameDirtyFlag();

    /**
     * 补货组
     */
    Integer getGroup_id();

    void setGroup_id(Integer group_id);

    /**
     * 获取 [补货组]脏标记
     */
    boolean getGroup_idDirtyFlag();

    /**
     * 显示名称
     */
    String getDisplay_name();

    void setDisplay_name(String display_name);

    /**
     * 获取 [显示名称]脏标记
     */
    boolean getDisplay_nameDirtyFlag();

    /**
     * 账单状态
     */
    String getInvoice_status();

    void setInvoice_status(String invoice_status);

    /**
     * 获取 [账单状态]脏标记
     */
    boolean getInvoice_statusDirtyFlag();

    /**
     * 活动状态
     */
    String getActivity_state();

    void setActivity_state(String activity_state);

    /**
     * 获取 [活动状态]脏标记
     */
    boolean getActivity_stateDirtyFlag();

    /**
     * 审批日期
     */
    Timestamp getDate_approve();

    void setDate_approve(Timestamp date_approve);

    /**
     * 获取 [审批日期]脏标记
     */
    boolean getDate_approveDirtyFlag();

    /**
     * 活动
     */
    String getActivity_ids();

    void setActivity_ids(String activity_ids);

    /**
     * 获取 [活动]脏标记
     */
    boolean getActivity_idsDirtyFlag();

    /**
     * 供应商参考
     */
    String getPartner_ref();

    void setPartner_ref(String partner_ref);

    /**
     * 获取 [供应商参考]脏标记
     */
    boolean getPartner_refDirtyFlag();

    /**
     * 关注者
     */
    String getMessage_follower_ids();

    void setMessage_follower_ids(String message_follower_ids);

    /**
     * 获取 [关注者]脏标记
     */
    boolean getMessage_follower_idsDirtyFlag();

    /**
     * 计划日期
     */
    Timestamp getDate_planned();

    void setDate_planned(Timestamp date_planned);

    /**
     * 获取 [计划日期]脏标记
     */
    boolean getDate_plannedDirtyFlag();

    /**
     * 源文档
     */
    String getOrigin();

    void setOrigin(String origin);

    /**
     * 获取 [源文档]脏标记
     */
    boolean getOriginDirtyFlag();

    /**
     * 采购员
     */
    String getUser_id_text();

    void setUser_id_text(String user_id_text);

    /**
     * 获取 [采购员]脏标记
     */
    boolean getUser_id_textDirtyFlag();

    /**
     * 税科目调整
     */
    String getFiscal_position_id_text();

    void setFiscal_position_id_text(String fiscal_position_id_text);

    /**
     * 获取 [税科目调整]脏标记
     */
    boolean getFiscal_position_id_textDirtyFlag();

    /**
     * 币种
     */
    String getCurrency_id_text();

    void setCurrency_id_text(String currency_id_text);

    /**
     * 获取 [币种]脏标记
     */
    boolean getCurrency_id_textDirtyFlag();

    /**
     * 公司
     */
    String getCompany_id_text();

    void setCompany_id_text(String company_id_text);

    /**
     * 获取 [公司]脏标记
     */
    boolean getCompany_id_textDirtyFlag();

    /**
     * 代发货地址
     */
    String getDest_address_id_text();

    void setDest_address_id_text(String dest_address_id_text);

    /**
     * 获取 [代发货地址]脏标记
     */
    boolean getDest_address_id_textDirtyFlag();

    /**
     * 最后更新人
     */
    String getWrite_uid_text();

    void setWrite_uid_text(String write_uid_text);

    /**
     * 获取 [最后更新人]脏标记
     */
    boolean getWrite_uid_textDirtyFlag();

    /**
     * 付款条款
     */
    String getPayment_term_id_text();

    void setPayment_term_id_text(String payment_term_id_text);

    /**
     * 获取 [付款条款]脏标记
     */
    boolean getPayment_term_id_textDirtyFlag();

    /**
     * 创建人
     */
    String getCreate_uid_text();

    void setCreate_uid_text(String create_uid_text);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uid_textDirtyFlag();

    /**
     * 供应商
     */
    String getPartner_id_text();

    void setPartner_id_text(String partner_id_text);

    /**
     * 获取 [供应商]脏标记
     */
    boolean getPartner_id_textDirtyFlag();

    /**
     * 交货到
     */
    String getPicking_type_id_text();

    void setPicking_type_id_text(String picking_type_id_text);

    /**
     * 获取 [交货到]脏标记
     */
    boolean getPicking_type_id_textDirtyFlag();

    /**
     * 国际贸易术语
     */
    String getIncoterm_id_text();

    void setIncoterm_id_text(String incoterm_id_text);

    /**
     * 获取 [国际贸易术语]脏标记
     */
    boolean getIncoterm_id_textDirtyFlag();

    /**
     * 创建人
     */
    Integer getCreate_uid();

    void setCreate_uid(Integer create_uid);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uidDirtyFlag();

    /**
     * 供应商
     */
    Integer getPartner_id();

    void setPartner_id(Integer partner_id);

    /**
     * 获取 [供应商]脏标记
     */
    boolean getPartner_idDirtyFlag();

    /**
     * 代发货地址
     */
    Integer getDest_address_id();

    void setDest_address_id(Integer dest_address_id);

    /**
     * 获取 [代发货地址]脏标记
     */
    boolean getDest_address_idDirtyFlag();

    /**
     * 公司
     */
    Integer getCompany_id();

    void setCompany_id(Integer company_id);

    /**
     * 获取 [公司]脏标记
     */
    boolean getCompany_idDirtyFlag();

    /**
     * 币种
     */
    Integer getCurrency_id();

    void setCurrency_id(Integer currency_id);

    /**
     * 获取 [币种]脏标记
     */
    boolean getCurrency_idDirtyFlag();

    /**
     * 付款条款
     */
    Integer getPayment_term_id();

    void setPayment_term_id(Integer payment_term_id);

    /**
     * 获取 [付款条款]脏标记
     */
    boolean getPayment_term_idDirtyFlag();

    /**
     * 采购员
     */
    Integer getUser_id();

    void setUser_id(Integer user_id);

    /**
     * 获取 [采购员]脏标记
     */
    boolean getUser_idDirtyFlag();

    /**
     * 最后更新人
     */
    Integer getWrite_uid();

    void setWrite_uid(Integer write_uid);

    /**
     * 获取 [最后更新人]脏标记
     */
    boolean getWrite_uidDirtyFlag();

    /**
     * 国际贸易术语
     */
    Integer getIncoterm_id();

    void setIncoterm_id(Integer incoterm_id);

    /**
     * 获取 [国际贸易术语]脏标记
     */
    boolean getIncoterm_idDirtyFlag();

    /**
     * 税科目调整
     */
    Integer getFiscal_position_id();

    void setFiscal_position_id(Integer fiscal_position_id);

    /**
     * 获取 [税科目调整]脏标记
     */
    boolean getFiscal_position_idDirtyFlag();

    /**
     * 交货到
     */
    Integer getPicking_type_id();

    void setPicking_type_id(Integer picking_type_id);

    /**
     * 获取 [交货到]脏标记
     */
    boolean getPicking_type_idDirtyFlag();

}
