package cn.ibizlab.odoo.core.odoo_bus.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_bus.domain.Bus_bus;
import cn.ibizlab.odoo.core.odoo_bus.filter.Bus_busSearchContext;


/**
 * 实体[Bus_bus] 服务对象接口
 */
public interface IBus_busService{

    Bus_bus get(Integer key) ;
    boolean update(Bus_bus et) ;
    void updateBatch(List<Bus_bus> list) ;
    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    boolean create(Bus_bus et) ;
    void createBatch(List<Bus_bus> list) ;
    Page<Bus_bus> searchDefault(Bus_busSearchContext context) ;

}



