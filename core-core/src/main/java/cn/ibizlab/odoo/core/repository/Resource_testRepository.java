package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Resource_test;
import cn.ibizlab.odoo.core.odoo_resource.filter.Resource_testSearchContext;

/**
 * 实体 [测试资源模型] 存储对象
 */
public interface Resource_testRepository extends Repository<Resource_test> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Resource_test> searchDefault(Resource_testSearchContext context);

    Resource_test convert2PO(cn.ibizlab.odoo.core.odoo_resource.domain.Resource_test domain , Resource_test po) ;

    cn.ibizlab.odoo.core.odoo_resource.domain.Resource_test convert2Domain( Resource_test po ,cn.ibizlab.odoo.core.odoo_resource.domain.Resource_test domain) ;

}
