package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Mail_mass_mailing_test;
import cn.ibizlab.odoo.core.odoo_mail.filter.Mail_mass_mailing_testSearchContext;

/**
 * 实体 [示例邮件向导] 存储对象
 */
public interface Mail_mass_mailing_testRepository extends Repository<Mail_mass_mailing_test> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Mail_mass_mailing_test> searchDefault(Mail_mass_mailing_testSearchContext context);

    Mail_mass_mailing_test convert2PO(cn.ibizlab.odoo.core.odoo_mail.domain.Mail_mass_mailing_test domain , Mail_mass_mailing_test po) ;

    cn.ibizlab.odoo.core.odoo_mail.domain.Mail_mass_mailing_test convert2Domain( Mail_mass_mailing_test po ,cn.ibizlab.odoo.core.odoo_mail.domain.Mail_mass_mailing_test domain) ;

}
