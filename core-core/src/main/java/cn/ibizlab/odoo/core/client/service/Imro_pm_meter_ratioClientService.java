package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Imro_pm_meter_ratio;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[mro_pm_meter_ratio] 服务对象接口
 */
public interface Imro_pm_meter_ratioClientService{

    public Imro_pm_meter_ratio createModel() ;

    public Page<Imro_pm_meter_ratio> search(SearchContext context);

    public void get(Imro_pm_meter_ratio mro_pm_meter_ratio);

    public void create(Imro_pm_meter_ratio mro_pm_meter_ratio);

    public void update(Imro_pm_meter_ratio mro_pm_meter_ratio);

    public void removeBatch(List<Imro_pm_meter_ratio> mro_pm_meter_ratios);

    public void remove(Imro_pm_meter_ratio mro_pm_meter_ratio);

    public void updateBatch(List<Imro_pm_meter_ratio> mro_pm_meter_ratios);

    public void createBatch(List<Imro_pm_meter_ratio> mro_pm_meter_ratios);

    public Page<Imro_pm_meter_ratio> select(SearchContext context);

}
