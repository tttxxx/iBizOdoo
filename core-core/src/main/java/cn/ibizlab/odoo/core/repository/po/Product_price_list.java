package cn.ibizlab.odoo.core.repository.po ;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.math.BigInteger;

import cn.ibizlab.odoo.core.odoo_product.filter.Product_price_listSearchContext;

/**
 * 实体 [基于价格列表版本的单位产品价格] 存储模型
 */
public interface Product_price_list{

    /**
     * 显示名称
     */
    String getDisplay_name();

    void setDisplay_name(String display_name);

    /**
     * 获取 [显示名称]脏标记
     */
    boolean getDisplay_nameDirtyFlag();

    /**
     * 数量-5
     */
    Integer getQty5();

    void setQty5(Integer qty5);

    /**
     * 获取 [数量-5]脏标记
     */
    boolean getQty5DirtyFlag();

    /**
     * 最后修改日
     */
    Timestamp get__last_update();

    void set__last_update(Timestamp __last_update);

    /**
     * 获取 [最后修改日]脏标记
     */
    boolean get__last_updateDirtyFlag();

    /**
     * 数量-3
     */
    Integer getQty3();

    void setQty3(Integer qty3);

    /**
     * 获取 [数量-3]脏标记
     */
    boolean getQty3DirtyFlag();

    /**
     * 数量-4
     */
    Integer getQty4();

    void setQty4(Integer qty4);

    /**
     * 获取 [数量-4]脏标记
     */
    boolean getQty4DirtyFlag();

    /**
     * 创建时间
     */
    Timestamp getCreate_date();

    void setCreate_date(Timestamp create_date);

    /**
     * 获取 [创建时间]脏标记
     */
    boolean getCreate_dateDirtyFlag();

    /**
     * 数量-1
     */
    Integer getQty1();

    void setQty1(Integer qty1);

    /**
     * 获取 [数量-1]脏标记
     */
    boolean getQty1DirtyFlag();

    /**
     * 数量-2
     */
    Integer getQty2();

    void setQty2(Integer qty2);

    /**
     * 获取 [数量-2]脏标记
     */
    boolean getQty2DirtyFlag();

    /**
     * 最后更新时间
     */
    Timestamp getWrite_date();

    void setWrite_date(Timestamp write_date);

    /**
     * 获取 [最后更新时间]脏标记
     */
    boolean getWrite_dateDirtyFlag();

    /**
     * ID
     */
    Integer getId();

    void setId(Integer id);

    /**
     * 获取 [ID]脏标记
     */
    boolean getIdDirtyFlag();

    /**
     * 创建人
     */
    String getCreate_uid_text();

    void setCreate_uid_text(String create_uid_text);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uid_textDirtyFlag();

    /**
     * 最后更新人
     */
    String getWrite_uid_text();

    void setWrite_uid_text(String write_uid_text);

    /**
     * 获取 [最后更新人]脏标记
     */
    boolean getWrite_uid_textDirtyFlag();

    /**
     * 价格表
     */
    String getPrice_list_text();

    void setPrice_list_text(String price_list_text);

    /**
     * 获取 [价格表]脏标记
     */
    boolean getPrice_list_textDirtyFlag();

    /**
     * 创建人
     */
    Integer getCreate_uid();

    void setCreate_uid(Integer create_uid);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uidDirtyFlag();

    /**
     * 价格表
     */
    Integer getPrice_list();

    void setPrice_list(Integer price_list);

    /**
     * 获取 [价格表]脏标记
     */
    boolean getPrice_listDirtyFlag();

    /**
     * 最后更新人
     */
    Integer getWrite_uid();

    void setWrite_uid(Integer write_uid);

    /**
     * 获取 [最后更新人]脏标记
     */
    boolean getWrite_uidDirtyFlag();

}
