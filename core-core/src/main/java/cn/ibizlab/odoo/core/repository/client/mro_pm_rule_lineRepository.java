package cn.ibizlab.odoo.core.repository.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import cn.ibizlab.odoo.core.repository.client.po.mro_pm_rule_line;

/**
 * 实体[mro_pm_rule_line] 服务对象接口
 */
public interface mro_pm_rule_lineRepository{


    public mro_pm_rule_line createPO() ;
        public List<mro_pm_rule_line> search();

        public void remove(String id);

        public void updateBatch(mro_pm_rule_line mro_pm_rule_line);

        public void removeBatch(String id);

        public void create(mro_pm_rule_line mro_pm_rule_line);

        public void createBatch(mro_pm_rule_line mro_pm_rule_line);

        public void update(mro_pm_rule_line mro_pm_rule_line);

        public void get(String id);


}
