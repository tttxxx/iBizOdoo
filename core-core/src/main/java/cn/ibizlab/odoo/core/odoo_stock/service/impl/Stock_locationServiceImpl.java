package cn.ibizlab.odoo.core.odoo_stock.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_stock.domain.Stock_location;
import cn.ibizlab.odoo.core.odoo_stock.filter.Stock_locationSearchContext;
import cn.ibizlab.odoo.core.odoo_stock.service.IStock_locationService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;



import cn.ibizlab.odoo.core.odoo_stock.client.stock_locationOdooClient;
import cn.ibizlab.odoo.core.odoo_stock.clientmodel.stock_locationClientModel;

/**
 * 实体[库存位置] 服务对象接口实现
 */
@Slf4j
@Service
public class Stock_locationServiceImpl implements IStock_locationService {

    @Autowired
    stock_locationOdooClient stock_locationOdooClient;


    @Override
    public boolean remove(Integer id) {
        stock_locationClientModel clientModel = new stock_locationClientModel();
        clientModel.setId(id);
		stock_locationOdooClient.remove(clientModel);
        return true; 
    }

    public void removeBatch(Collection<Integer> idList){
    }

    @Override
    public boolean create(Stock_location et) {
        stock_locationClientModel clientModel = convert2Model(et,null);
		stock_locationOdooClient.create(clientModel);
        Stock_location rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Stock_location> list){
    }

    @Override
    public Stock_location get(Integer id) {
        stock_locationClientModel clientModel = new stock_locationClientModel();
        clientModel.setId(id);
		stock_locationOdooClient.get(clientModel);
        Stock_location et = convert2Domain(clientModel,null);
        if(et==null){
            et=new Stock_location();
            et.setId(id);
        }
        else{
        }

        return  et;
    }

    @Override
    public boolean update(Stock_location et) {
        stock_locationClientModel clientModel = convert2Model(et,null);
		stock_locationOdooClient.update(clientModel);
        Stock_location rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }
    
    public void updateBatch(List<Stock_location> list){
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Stock_location> searchDefault(Stock_locationSearchContext context) {
        List<Stock_location> list = new ArrayList<Stock_location>();
        Page<stock_locationClientModel> clientModelList = stock_locationOdooClient.search(context);
        for(stock_locationClientModel clientModel : clientModelList){
            list.add( this.convert2Domain(clientModel,null));
        }
        return new PageImpl<Stock_location>(list, context.getPageable(), clientModelList.getTotalElements());
    }

    public stock_locationClientModel convert2Model(Stock_location domain , stock_locationClientModel model) {
        if(domain == null)
            return model ;
        if(model==null){
            model = new stock_locationClientModel();

        }
        if((Boolean) domain.getExtensionparams().get("dirtyflagenable")){
            if((Boolean) domain.getExtensionparams().get("posxdirtyflag"))
                model.setPosx(domain.getPosx());
            if((Boolean) domain.getExtensionparams().get("posydirtyflag"))
                model.setPosy(domain.getPosy());
            if((Boolean) domain.getExtensionparams().get("activedirtyflag"))
                model.setActive(domain.getActive());
            if((Boolean) domain.getExtensionparams().get("create_datedirtyflag"))
                model.setCreate_date(domain.getCreateDate());
            if((Boolean) domain.getExtensionparams().get("barcodedirtyflag"))
                model.setBarcode(domain.getBarcode());
            if((Boolean) domain.getExtensionparams().get("write_datedirtyflag"))
                model.setWrite_date(domain.getWriteDate());
            if((Boolean) domain.getExtensionparams().get("display_namedirtyflag"))
                model.setDisplay_name(domain.getDisplayName());
            if((Boolean) domain.getExtensionparams().get("poszdirtyflag"))
                model.setPosz(domain.getPosz());
            if((Boolean) domain.getExtensionparams().get("complete_namedirtyflag"))
                model.setComplete_name(domain.getCompleteName());
            if((Boolean) domain.getExtensionparams().get("scrap_locationdirtyflag"))
                model.setScrap_location(domain.getScrapLocation());
            if((Boolean) domain.getExtensionparams().get("return_locationdirtyflag"))
                model.setReturn_location(domain.getReturnLocation());
            if((Boolean) domain.getExtensionparams().get("namedirtyflag"))
                model.setName(domain.getName());
            if((Boolean) domain.getExtensionparams().get("__last_updatedirtyflag"))
                model.set__last_update(domain.getLastUpdate());
            if((Boolean) domain.getExtensionparams().get("parent_pathdirtyflag"))
                model.setParent_path(domain.getParentPath());
            if((Boolean) domain.getExtensionparams().get("quant_idsdirtyflag"))
                model.setQuant_ids(domain.getQuantIds());
            if((Boolean) domain.getExtensionparams().get("usagedirtyflag"))
                model.setUsage(domain.getUsage());
            if((Boolean) domain.getExtensionparams().get("commentdirtyflag"))
                model.setComment(domain.getComment());
            if((Boolean) domain.getExtensionparams().get("iddirtyflag"))
                model.setId(domain.getId());
            if((Boolean) domain.getExtensionparams().get("child_idsdirtyflag"))
                model.setChild_ids(domain.getChildIds());
            if((Boolean) domain.getExtensionparams().get("location_id_textdirtyflag"))
                model.setLocation_id_text(domain.getLocationIdText());
            if((Boolean) domain.getExtensionparams().get("company_id_textdirtyflag"))
                model.setCompany_id_text(domain.getCompanyIdText());
            if((Boolean) domain.getExtensionparams().get("create_uid_textdirtyflag"))
                model.setCreate_uid_text(domain.getCreateUidText());
            if((Boolean) domain.getExtensionparams().get("valuation_out_account_id_textdirtyflag"))
                model.setValuation_out_account_id_text(domain.getValuationOutAccountIdText());
            if((Boolean) domain.getExtensionparams().get("partner_id_textdirtyflag"))
                model.setPartner_id_text(domain.getPartnerIdText());
            if((Boolean) domain.getExtensionparams().get("removal_strategy_id_textdirtyflag"))
                model.setRemoval_strategy_id_text(domain.getRemovalStrategyIdText());
            if((Boolean) domain.getExtensionparams().get("putaway_strategy_id_textdirtyflag"))
                model.setPutaway_strategy_id_text(domain.getPutawayStrategyIdText());
            if((Boolean) domain.getExtensionparams().get("write_uid_textdirtyflag"))
                model.setWrite_uid_text(domain.getWriteUidText());
            if((Boolean) domain.getExtensionparams().get("valuation_in_account_id_textdirtyflag"))
                model.setValuation_in_account_id_text(domain.getValuationInAccountIdText());
            if((Boolean) domain.getExtensionparams().get("write_uiddirtyflag"))
                model.setWrite_uid(domain.getWriteUid());
            if((Boolean) domain.getExtensionparams().get("create_uiddirtyflag"))
                model.setCreate_uid(domain.getCreateUid());
            if((Boolean) domain.getExtensionparams().get("valuation_in_account_iddirtyflag"))
                model.setValuation_in_account_id(domain.getValuationInAccountId());
            if((Boolean) domain.getExtensionparams().get("company_iddirtyflag"))
                model.setCompany_id(domain.getCompanyId());
            if((Boolean) domain.getExtensionparams().get("location_iddirtyflag"))
                model.setLocation_id(domain.getLocationId());
            if((Boolean) domain.getExtensionparams().get("valuation_out_account_iddirtyflag"))
                model.setValuation_out_account_id(domain.getValuationOutAccountId());
            if((Boolean) domain.getExtensionparams().get("partner_iddirtyflag"))
                model.setPartner_id(domain.getPartnerId());
            if((Boolean) domain.getExtensionparams().get("putaway_strategy_iddirtyflag"))
                model.setPutaway_strategy_id(domain.getPutawayStrategyId());
            if((Boolean) domain.getExtensionparams().get("removal_strategy_iddirtyflag"))
                model.setRemoval_strategy_id(domain.getRemovalStrategyId());
            domain.getFocusNull().clear();
        }
        return model ;
    }

    public Stock_location convert2Domain( stock_locationClientModel model ,Stock_location domain) {
        if(model == null)
            return domain ;
        if(domain == null){
            domain = new Stock_location();
        }

        if(model.getPosxDirtyFlag())
            domain.setPosx(model.getPosx());
        if(model.getPosyDirtyFlag())
            domain.setPosy(model.getPosy());
        if(model.getActiveDirtyFlag())
            domain.setActive(model.getActive());
        if(model.getCreate_dateDirtyFlag())
            domain.setCreateDate(model.getCreate_date());
        if(model.getBarcodeDirtyFlag())
            domain.setBarcode(model.getBarcode());
        if(model.getWrite_dateDirtyFlag())
            domain.setWriteDate(model.getWrite_date());
        if(model.getDisplay_nameDirtyFlag())
            domain.setDisplayName(model.getDisplay_name());
        if(model.getPoszDirtyFlag())
            domain.setPosz(model.getPosz());
        if(model.getComplete_nameDirtyFlag())
            domain.setCompleteName(model.getComplete_name());
        if(model.getScrap_locationDirtyFlag())
            domain.setScrapLocation(model.getScrap_location());
        if(model.getReturn_locationDirtyFlag())
            domain.setReturnLocation(model.getReturn_location());
        if(model.getNameDirtyFlag())
            domain.setName(model.getName());
        if(model.get__last_updateDirtyFlag())
            domain.setLastUpdate(model.get__last_update());
        if(model.getParent_pathDirtyFlag())
            domain.setParentPath(model.getParent_path());
        if(model.getQuant_idsDirtyFlag())
            domain.setQuantIds(model.getQuant_ids());
        if(model.getUsageDirtyFlag())
            domain.setUsage(model.getUsage());
        if(model.getCommentDirtyFlag())
            domain.setComment(model.getComment());
        if(model.getIdDirtyFlag())
            domain.setId(model.getId());
        if(model.getChild_idsDirtyFlag())
            domain.setChildIds(model.getChild_ids());
        if(model.getLocation_id_textDirtyFlag())
            domain.setLocationIdText(model.getLocation_id_text());
        if(model.getCompany_id_textDirtyFlag())
            domain.setCompanyIdText(model.getCompany_id_text());
        if(model.getCreate_uid_textDirtyFlag())
            domain.setCreateUidText(model.getCreate_uid_text());
        if(model.getValuation_out_account_id_textDirtyFlag())
            domain.setValuationOutAccountIdText(model.getValuation_out_account_id_text());
        if(model.getPartner_id_textDirtyFlag())
            domain.setPartnerIdText(model.getPartner_id_text());
        if(model.getRemoval_strategy_id_textDirtyFlag())
            domain.setRemovalStrategyIdText(model.getRemoval_strategy_id_text());
        if(model.getPutaway_strategy_id_textDirtyFlag())
            domain.setPutawayStrategyIdText(model.getPutaway_strategy_id_text());
        if(model.getWrite_uid_textDirtyFlag())
            domain.setWriteUidText(model.getWrite_uid_text());
        if(model.getValuation_in_account_id_textDirtyFlag())
            domain.setValuationInAccountIdText(model.getValuation_in_account_id_text());
        if(model.getWrite_uidDirtyFlag())
            domain.setWriteUid(model.getWrite_uid());
        if(model.getCreate_uidDirtyFlag())
            domain.setCreateUid(model.getCreate_uid());
        if(model.getValuation_in_account_idDirtyFlag())
            domain.setValuationInAccountId(model.getValuation_in_account_id());
        if(model.getCompany_idDirtyFlag())
            domain.setCompanyId(model.getCompany_id());
        if(model.getLocation_idDirtyFlag())
            domain.setLocationId(model.getLocation_id());
        if(model.getValuation_out_account_idDirtyFlag())
            domain.setValuationOutAccountId(model.getValuation_out_account_id());
        if(model.getPartner_idDirtyFlag())
            domain.setPartnerId(model.getPartner_id());
        if(model.getPutaway_strategy_idDirtyFlag())
            domain.setPutawayStrategyId(model.getPutaway_strategy_id());
        if(model.getRemoval_strategy_idDirtyFlag())
            domain.setRemovalStrategyId(model.getRemoval_strategy_id());
        return domain ;
    }

}

    



