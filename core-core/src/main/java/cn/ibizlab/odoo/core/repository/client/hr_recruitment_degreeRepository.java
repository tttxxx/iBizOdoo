package cn.ibizlab.odoo.core.repository.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import cn.ibizlab.odoo.core.repository.client.po.hr_recruitment_degree;

/**
 * 实体[hr_recruitment_degree] 服务对象接口
 */
public interface hr_recruitment_degreeRepository{


    public hr_recruitment_degree createPO() ;
        public List<hr_recruitment_degree> search();

        public void create(hr_recruitment_degree hr_recruitment_degree);

        public void updateBatch(hr_recruitment_degree hr_recruitment_degree);

        public void removeBatch(String id);

        public void remove(String id);

        public void update(hr_recruitment_degree hr_recruitment_degree);

        public void get(String id);

        public void createBatch(hr_recruitment_degree hr_recruitment_degree);


}
