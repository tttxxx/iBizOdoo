package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Ilunch_order;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[lunch_order] 服务对象接口
 */
public interface Ilunch_orderClientService{

    public Ilunch_order createModel() ;

    public void removeBatch(List<Ilunch_order> lunch_orders);

    public void createBatch(List<Ilunch_order> lunch_orders);

    public void remove(Ilunch_order lunch_order);

    public void create(Ilunch_order lunch_order);

    public void updateBatch(List<Ilunch_order> lunch_orders);

    public void get(Ilunch_order lunch_order);

    public Page<Ilunch_order> search(SearchContext context);

    public void update(Ilunch_order lunch_order);

    public Page<Ilunch_order> select(SearchContext context);

}
