package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Mail_compose_message;
import cn.ibizlab.odoo.core.odoo_mail.filter.Mail_compose_messageSearchContext;

/**
 * 实体 [邮件撰写向导] 存储对象
 */
public interface Mail_compose_messageRepository extends Repository<Mail_compose_message> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Mail_compose_message> searchDefault(Mail_compose_messageSearchContext context);

    Mail_compose_message convert2PO(cn.ibizlab.odoo.core.odoo_mail.domain.Mail_compose_message domain , Mail_compose_message po) ;

    cn.ibizlab.odoo.core.odoo_mail.domain.Mail_compose_message convert2Domain( Mail_compose_message po ,cn.ibizlab.odoo.core.odoo_mail.domain.Mail_compose_message domain) ;

}
