package cn.ibizlab.odoo.core.odoo_stock.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_stock.domain.Stock_move_line;
import cn.ibizlab.odoo.core.odoo_stock.filter.Stock_move_lineSearchContext;
import cn.ibizlab.odoo.core.odoo_stock.service.IStock_move_lineService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;



import cn.ibizlab.odoo.core.odoo_stock.client.stock_move_lineOdooClient;
import cn.ibizlab.odoo.core.odoo_stock.clientmodel.stock_move_lineClientModel;

/**
 * 实体[产品移动(移库明细)] 服务对象接口实现
 */
@Slf4j
@Service
public class Stock_move_lineServiceImpl implements IStock_move_lineService {

    @Autowired
    stock_move_lineOdooClient stock_move_lineOdooClient;


    @Override
    public Stock_move_line get(Integer id) {
        stock_move_lineClientModel clientModel = new stock_move_lineClientModel();
        clientModel.setId(id);
		stock_move_lineOdooClient.get(clientModel);
        Stock_move_line et = convert2Domain(clientModel,null);
        if(et==null){
            et=new Stock_move_line();
            et.setId(id);
        }
        else{
        }

        return  et;
    }

    @Override
    public boolean create(Stock_move_line et) {
        stock_move_lineClientModel clientModel = convert2Model(et,null);
		stock_move_lineOdooClient.create(clientModel);
        Stock_move_line rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Stock_move_line> list){
    }

    @Override
    public boolean update(Stock_move_line et) {
        stock_move_lineClientModel clientModel = convert2Model(et,null);
		stock_move_lineOdooClient.update(clientModel);
        Stock_move_line rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }
    
    public void updateBatch(List<Stock_move_line> list){
    }

    @Override
    public boolean remove(Integer id) {
        stock_move_lineClientModel clientModel = new stock_move_lineClientModel();
        clientModel.setId(id);
		stock_move_lineOdooClient.remove(clientModel);
        return true; 
    }

    public void removeBatch(Collection<Integer> idList){
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Stock_move_line> searchDefault(Stock_move_lineSearchContext context) {
        List<Stock_move_line> list = new ArrayList<Stock_move_line>();
        Page<stock_move_lineClientModel> clientModelList = stock_move_lineOdooClient.search(context);
        for(stock_move_lineClientModel clientModel : clientModelList){
            list.add( this.convert2Domain(clientModel,null));
        }
        return new PageImpl<Stock_move_line>(list, context.getPageable(), clientModelList.getTotalElements());
    }

    public stock_move_lineClientModel convert2Model(Stock_move_line domain , stock_move_lineClientModel model) {
        if(domain == null)
            return model ;
        if(model==null){
            model = new stock_move_lineClientModel();

        }
        if((Boolean) domain.getExtensionparams().get("dirtyflagenable")){
            if((Boolean) domain.getExtensionparams().get("qty_donedirtyflag"))
                model.setQty_done(domain.getQtyDone());
            if((Boolean) domain.getExtensionparams().get("picking_type_use_existing_lotsdirtyflag"))
                model.setPicking_type_use_existing_lots(domain.getPickingTypeUseExistingLots());
            if((Boolean) domain.getExtensionparams().get("consume_line_idsdirtyflag"))
                model.setConsume_line_ids(domain.getConsumeLineIds());
            if((Boolean) domain.getExtensionparams().get("lots_visibledirtyflag"))
                model.setLots_visible(domain.getLotsVisible());
            if((Boolean) domain.getExtensionparams().get("product_uom_qtydirtyflag"))
                model.setProduct_uom_qty(domain.getProductUomQty());
            if((Boolean) domain.getExtensionparams().get("__last_updatedirtyflag"))
                model.set__last_update(domain.getLastUpdate());
            if((Boolean) domain.getExtensionparams().get("done_wodirtyflag"))
                model.setDone_wo(domain.getDoneWo());
            if((Boolean) domain.getExtensionparams().get("picking_type_use_create_lotsdirtyflag"))
                model.setPicking_type_use_create_lots(domain.getPickingTypeUseCreateLots());
            if((Boolean) domain.getExtensionparams().get("produce_line_idsdirtyflag"))
                model.setProduce_line_ids(domain.getProduceLineIds());
            if((Boolean) domain.getExtensionparams().get("display_namedirtyflag"))
                model.setDisplay_name(domain.getDisplayName());
            if((Boolean) domain.getExtensionparams().get("datedirtyflag"))
                model.setDate(domain.getDate());
            if((Boolean) domain.getExtensionparams().get("picking_type_entire_packsdirtyflag"))
                model.setPicking_type_entire_packs(domain.getPickingTypeEntirePacks());
            if((Boolean) domain.getExtensionparams().get("iddirtyflag"))
                model.setId(domain.getId());
            if((Boolean) domain.getExtensionparams().get("write_datedirtyflag"))
                model.setWrite_date(domain.getWriteDate());
            if((Boolean) domain.getExtensionparams().get("create_datedirtyflag"))
                model.setCreate_date(domain.getCreateDate());
            if((Boolean) domain.getExtensionparams().get("lot_namedirtyflag"))
                model.setLot_name(domain.getLotName());
            if((Boolean) domain.getExtensionparams().get("product_qtydirtyflag"))
                model.setProduct_qty(domain.getProductQty());
            if((Boolean) domain.getExtensionparams().get("lot_produced_qtydirtyflag"))
                model.setLot_produced_qty(domain.getLotProducedQty());
            if((Boolean) domain.getExtensionparams().get("package_id_textdirtyflag"))
                model.setPackage_id_text(domain.getPackageIdText());
            if((Boolean) domain.getExtensionparams().get("lot_produced_id_textdirtyflag"))
                model.setLot_produced_id_text(domain.getLotProducedIdText());
            if((Boolean) domain.getExtensionparams().get("product_id_textdirtyflag"))
                model.setProduct_id_text(domain.getProductIdText());
            if((Boolean) domain.getExtensionparams().get("workorder_id_textdirtyflag"))
                model.setWorkorder_id_text(domain.getWorkorderIdText());
            if((Boolean) domain.getExtensionparams().get("product_uom_id_textdirtyflag"))
                model.setProduct_uom_id_text(domain.getProductUomIdText());
            if((Boolean) domain.getExtensionparams().get("trackingdirtyflag"))
                model.setTracking(domain.getTracking());
            if((Boolean) domain.getExtensionparams().get("create_uid_textdirtyflag"))
                model.setCreate_uid_text(domain.getCreateUidText());
            if((Boolean) domain.getExtensionparams().get("move_id_textdirtyflag"))
                model.setMove_id_text(domain.getMoveIdText());
            if((Boolean) domain.getExtensionparams().get("location_dest_id_textdirtyflag"))
                model.setLocation_dest_id_text(domain.getLocationDestIdText());
            if((Boolean) domain.getExtensionparams().get("is_lockeddirtyflag"))
                model.setIs_locked(domain.getIsLocked());
            if((Boolean) domain.getExtensionparams().get("is_initial_demand_editabledirtyflag"))
                model.setIs_initial_demand_editable(domain.getIsInitialDemandEditable());
            if((Boolean) domain.getExtensionparams().get("production_id_textdirtyflag"))
                model.setProduction_id_text(domain.getProductionIdText());
            if((Boolean) domain.getExtensionparams().get("statedirtyflag"))
                model.setState(domain.getState());
            if((Boolean) domain.getExtensionparams().get("lot_id_textdirtyflag"))
                model.setLot_id_text(domain.getLotIdText());
            if((Boolean) domain.getExtensionparams().get("write_uid_textdirtyflag"))
                model.setWrite_uid_text(domain.getWriteUidText());
            if((Boolean) domain.getExtensionparams().get("referencedirtyflag"))
                model.setReference(domain.getReference());
            if((Boolean) domain.getExtensionparams().get("result_package_id_textdirtyflag"))
                model.setResult_package_id_text(domain.getResultPackageIdText());
            if((Boolean) domain.getExtensionparams().get("done_movedirtyflag"))
                model.setDone_move(domain.getDoneMove());
            if((Boolean) domain.getExtensionparams().get("picking_id_textdirtyflag"))
                model.setPicking_id_text(domain.getPickingIdText());
            if((Boolean) domain.getExtensionparams().get("owner_id_textdirtyflag"))
                model.setOwner_id_text(domain.getOwnerIdText());
            if((Boolean) domain.getExtensionparams().get("location_id_textdirtyflag"))
                model.setLocation_id_text(domain.getLocationIdText());
            if((Boolean) domain.getExtensionparams().get("product_uom_iddirtyflag"))
                model.setProduct_uom_id(domain.getProductUomId());
            if((Boolean) domain.getExtensionparams().get("move_iddirtyflag"))
                model.setMove_id(domain.getMoveId());
            if((Boolean) domain.getExtensionparams().get("result_package_iddirtyflag"))
                model.setResult_package_id(domain.getResultPackageId());
            if((Boolean) domain.getExtensionparams().get("write_uiddirtyflag"))
                model.setWrite_uid(domain.getWriteUid());
            if((Boolean) domain.getExtensionparams().get("lot_produced_iddirtyflag"))
                model.setLot_produced_id(domain.getLotProducedId());
            if((Boolean) domain.getExtensionparams().get("product_iddirtyflag"))
                model.setProduct_id(domain.getProductId());
            if((Boolean) domain.getExtensionparams().get("create_uiddirtyflag"))
                model.setCreate_uid(domain.getCreateUid());
            if((Boolean) domain.getExtensionparams().get("lot_iddirtyflag"))
                model.setLot_id(domain.getLotId());
            if((Boolean) domain.getExtensionparams().get("picking_iddirtyflag"))
                model.setPicking_id(domain.getPickingId());
            if((Boolean) domain.getExtensionparams().get("workorder_iddirtyflag"))
                model.setWorkorder_id(domain.getWorkorderId());
            if((Boolean) domain.getExtensionparams().get("location_iddirtyflag"))
                model.setLocation_id(domain.getLocationId());
            if((Boolean) domain.getExtensionparams().get("package_iddirtyflag"))
                model.setPackage_id(domain.getPackageId());
            if((Boolean) domain.getExtensionparams().get("owner_iddirtyflag"))
                model.setOwner_id(domain.getOwnerId());
            if((Boolean) domain.getExtensionparams().get("package_level_iddirtyflag"))
                model.setPackage_level_id(domain.getPackageLevelId());
            if((Boolean) domain.getExtensionparams().get("location_dest_iddirtyflag"))
                model.setLocation_dest_id(domain.getLocationDestId());
            if((Boolean) domain.getExtensionparams().get("production_iddirtyflag"))
                model.setProduction_id(domain.getProductionId());
            domain.getFocusNull().clear();
        }
        return model ;
    }

    public Stock_move_line convert2Domain( stock_move_lineClientModel model ,Stock_move_line domain) {
        if(model == null)
            return domain ;
        if(domain == null){
            domain = new Stock_move_line();
        }

        if(model.getQty_doneDirtyFlag())
            domain.setQtyDone(model.getQty_done());
        if(model.getPicking_type_use_existing_lotsDirtyFlag())
            domain.setPickingTypeUseExistingLots(model.getPicking_type_use_existing_lots());
        if(model.getConsume_line_idsDirtyFlag())
            domain.setConsumeLineIds(model.getConsume_line_ids());
        if(model.getLots_visibleDirtyFlag())
            domain.setLotsVisible(model.getLots_visible());
        if(model.getProduct_uom_qtyDirtyFlag())
            domain.setProductUomQty(model.getProduct_uom_qty());
        if(model.get__last_updateDirtyFlag())
            domain.setLastUpdate(model.get__last_update());
        if(model.getDone_woDirtyFlag())
            domain.setDoneWo(model.getDone_wo());
        if(model.getPicking_type_use_create_lotsDirtyFlag())
            domain.setPickingTypeUseCreateLots(model.getPicking_type_use_create_lots());
        if(model.getProduce_line_idsDirtyFlag())
            domain.setProduceLineIds(model.getProduce_line_ids());
        if(model.getDisplay_nameDirtyFlag())
            domain.setDisplayName(model.getDisplay_name());
        if(model.getDateDirtyFlag())
            domain.setDate(model.getDate());
        if(model.getPicking_type_entire_packsDirtyFlag())
            domain.setPickingTypeEntirePacks(model.getPicking_type_entire_packs());
        if(model.getIdDirtyFlag())
            domain.setId(model.getId());
        if(model.getWrite_dateDirtyFlag())
            domain.setWriteDate(model.getWrite_date());
        if(model.getCreate_dateDirtyFlag())
            domain.setCreateDate(model.getCreate_date());
        if(model.getLot_nameDirtyFlag())
            domain.setLotName(model.getLot_name());
        if(model.getProduct_qtyDirtyFlag())
            domain.setProductQty(model.getProduct_qty());
        if(model.getLot_produced_qtyDirtyFlag())
            domain.setLotProducedQty(model.getLot_produced_qty());
        if(model.getPackage_id_textDirtyFlag())
            domain.setPackageIdText(model.getPackage_id_text());
        if(model.getLot_produced_id_textDirtyFlag())
            domain.setLotProducedIdText(model.getLot_produced_id_text());
        if(model.getProduct_id_textDirtyFlag())
            domain.setProductIdText(model.getProduct_id_text());
        if(model.getWorkorder_id_textDirtyFlag())
            domain.setWorkorderIdText(model.getWorkorder_id_text());
        if(model.getProduct_uom_id_textDirtyFlag())
            domain.setProductUomIdText(model.getProduct_uom_id_text());
        if(model.getTrackingDirtyFlag())
            domain.setTracking(model.getTracking());
        if(model.getCreate_uid_textDirtyFlag())
            domain.setCreateUidText(model.getCreate_uid_text());
        if(model.getMove_id_textDirtyFlag())
            domain.setMoveIdText(model.getMove_id_text());
        if(model.getLocation_dest_id_textDirtyFlag())
            domain.setLocationDestIdText(model.getLocation_dest_id_text());
        if(model.getIs_lockedDirtyFlag())
            domain.setIsLocked(model.getIs_locked());
        if(model.getIs_initial_demand_editableDirtyFlag())
            domain.setIsInitialDemandEditable(model.getIs_initial_demand_editable());
        if(model.getProduction_id_textDirtyFlag())
            domain.setProductionIdText(model.getProduction_id_text());
        if(model.getStateDirtyFlag())
            domain.setState(model.getState());
        if(model.getLot_id_textDirtyFlag())
            domain.setLotIdText(model.getLot_id_text());
        if(model.getWrite_uid_textDirtyFlag())
            domain.setWriteUidText(model.getWrite_uid_text());
        if(model.getReferenceDirtyFlag())
            domain.setReference(model.getReference());
        if(model.getResult_package_id_textDirtyFlag())
            domain.setResultPackageIdText(model.getResult_package_id_text());
        if(model.getDone_moveDirtyFlag())
            domain.setDoneMove(model.getDone_move());
        if(model.getPicking_id_textDirtyFlag())
            domain.setPickingIdText(model.getPicking_id_text());
        if(model.getOwner_id_textDirtyFlag())
            domain.setOwnerIdText(model.getOwner_id_text());
        if(model.getLocation_id_textDirtyFlag())
            domain.setLocationIdText(model.getLocation_id_text());
        if(model.getProduct_uom_idDirtyFlag())
            domain.setProductUomId(model.getProduct_uom_id());
        if(model.getMove_idDirtyFlag())
            domain.setMoveId(model.getMove_id());
        if(model.getResult_package_idDirtyFlag())
            domain.setResultPackageId(model.getResult_package_id());
        if(model.getWrite_uidDirtyFlag())
            domain.setWriteUid(model.getWrite_uid());
        if(model.getLot_produced_idDirtyFlag())
            domain.setLotProducedId(model.getLot_produced_id());
        if(model.getProduct_idDirtyFlag())
            domain.setProductId(model.getProduct_id());
        if(model.getCreate_uidDirtyFlag())
            domain.setCreateUid(model.getCreate_uid());
        if(model.getLot_idDirtyFlag())
            domain.setLotId(model.getLot_id());
        if(model.getPicking_idDirtyFlag())
            domain.setPickingId(model.getPicking_id());
        if(model.getWorkorder_idDirtyFlag())
            domain.setWorkorderId(model.getWorkorder_id());
        if(model.getLocation_idDirtyFlag())
            domain.setLocationId(model.getLocation_id());
        if(model.getPackage_idDirtyFlag())
            domain.setPackageId(model.getPackage_id());
        if(model.getOwner_idDirtyFlag())
            domain.setOwnerId(model.getOwner_id());
        if(model.getPackage_level_idDirtyFlag())
            domain.setPackageLevelId(model.getPackage_level_id());
        if(model.getLocation_dest_idDirtyFlag())
            domain.setLocationDestId(model.getLocation_dest_id());
        if(model.getProduction_idDirtyFlag())
            domain.setProductionId(model.getProduction_id());
        return domain ;
    }

}

    



