package cn.ibizlab.odoo.core.odoo_product.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_product.domain.Product_product;
import cn.ibizlab.odoo.core.odoo_product.filter.Product_productSearchContext;
import cn.ibizlab.odoo.core.odoo_product.service.IProduct_productService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;



import cn.ibizlab.odoo.core.odoo_product.client.product_productOdooClient;
import cn.ibizlab.odoo.core.odoo_product.clientmodel.product_productClientModel;

/**
 * 实体[产品] 服务对象接口实现
 */
@Slf4j
@Service
public class Product_productServiceImpl implements IProduct_productService {

    @Autowired
    product_productOdooClient product_productOdooClient;


    @Override
    public Product_product get(Integer id) {
        product_productClientModel clientModel = new product_productClientModel();
        clientModel.setId(id);
		product_productOdooClient.get(clientModel);
        Product_product et = convert2Domain(clientModel,null);
        if(et==null){
            et=new Product_product();
            et.setId(id);
        }
        else{
        }

        return  et;
    }

    @Override
    public boolean create(Product_product et) {
        product_productClientModel clientModel = convert2Model(et,null);
		product_productOdooClient.create(clientModel);
        Product_product rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Product_product> list){
    }

    @Override
    public boolean update(Product_product et) {
        product_productClientModel clientModel = convert2Model(et,null);
		product_productOdooClient.update(clientModel);
        Product_product rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }
    
    public void updateBatch(List<Product_product> list){
    }

    @Override
    public boolean remove(Integer id) {
        product_productClientModel clientModel = new product_productClientModel();
        clientModel.setId(id);
		product_productOdooClient.remove(clientModel);
        return true; 
    }

    public void removeBatch(Collection<Integer> idList){
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Product_product> searchDefault(Product_productSearchContext context) {
        List<Product_product> list = new ArrayList<Product_product>();
        Page<product_productClientModel> clientModelList = product_productOdooClient.search(context);
        for(product_productClientModel clientModel : clientModelList){
            list.add( this.convert2Domain(clientModel,null));
        }
        return new PageImpl<Product_product>(list, context.getPageable(), clientModelList.getTotalElements());
    }

    public product_productClientModel convert2Model(Product_product domain , product_productClientModel model) {
        if(domain == null)
            return model ;
        if(model==null){
            model = new product_productClientModel();

        }
        if((Boolean) domain.getExtensionparams().get("dirtyflagenable")){
            if((Boolean) domain.getExtensionparams().get("variant_seller_idsdirtyflag"))
                model.setVariant_seller_ids(domain.getVariantSellerIds());
            if((Boolean) domain.getExtensionparams().get("product_template_attribute_value_idsdirtyflag"))
                model.setProduct_template_attribute_value_ids(domain.getProductTemplateAttributeValueIds());
            if((Boolean) domain.getExtensionparams().get("product_variant_idsdirtyflag"))
                model.setProduct_variant_ids(domain.getProductVariantIds());
            if((Boolean) domain.getExtensionparams().get("image_smalldirtyflag"))
                model.setImage_small(domain.getImageSmall());
            if((Boolean) domain.getExtensionparams().get("message_unreaddirtyflag"))
                model.setMessage_unread(domain.getMessageUnread());
            if((Boolean) domain.getExtensionparams().get("volumedirtyflag"))
                model.setVolume(domain.getVolume());
            if((Boolean) domain.getExtensionparams().get("lst_pricedirtyflag"))
                model.setLst_price(domain.getLstPrice());
            if((Boolean) domain.getExtensionparams().get("valid_product_attribute_idsdirtyflag"))
                model.setValid_product_attribute_ids(domain.getValidProductAttributeIds());
            if((Boolean) domain.getExtensionparams().get("stock_fifo_manual_move_idsdirtyflag"))
                model.setStock_fifo_manual_move_ids(domain.getStockFifoManualMoveIds());
            if((Boolean) domain.getExtensionparams().get("stock_quant_idsdirtyflag"))
                model.setStock_quant_ids(domain.getStockQuantIds());
            if((Boolean) domain.getExtensionparams().get("supplier_taxes_iddirtyflag"))
                model.setSupplier_taxes_id(domain.getSupplierTaxesId());
            if((Boolean) domain.getExtensionparams().get("pricelist_item_idsdirtyflag"))
                model.setPricelist_item_ids(domain.getPricelistItemIds());
            if((Boolean) domain.getExtensionparams().get("__last_updatedirtyflag"))
                model.set__last_update(domain.getLastUpdate());
            if((Boolean) domain.getExtensionparams().get("accessory_product_idsdirtyflag"))
                model.setAccessory_product_ids(domain.getAccessoryProductIds());
            if((Boolean) domain.getExtensionparams().get("seller_idsdirtyflag"))
                model.setSeller_ids(domain.getSellerIds());
            if((Boolean) domain.getExtensionparams().get("valid_product_attribute_value_wnva_idsdirtyflag"))
                model.setValid_product_attribute_value_wnva_ids(domain.getValidProductAttributeValueWnvaIds());
            if((Boolean) domain.getExtensionparams().get("partner_refdirtyflag"))
                model.setPartner_ref(domain.getPartnerRef());
            if((Boolean) domain.getExtensionparams().get("product_image_idsdirtyflag"))
                model.setProduct_image_ids(domain.getProductImageIds());
            if((Boolean) domain.getExtensionparams().get("mrp_product_qtydirtyflag"))
                model.setMrp_product_qty(domain.getMrpProductQty());
            if((Boolean) domain.getExtensionparams().get("write_datedirtyflag"))
                model.setWrite_date(domain.getWriteDate());
            if((Boolean) domain.getExtensionparams().get("valid_product_template_attribute_line_idsdirtyflag"))
                model.setValid_product_template_attribute_line_ids(domain.getValidProductTemplateAttributeLineIds());
            if((Boolean) domain.getExtensionparams().get("activity_type_iddirtyflag"))
                model.setActivity_type_id(domain.getActivityTypeId());
            if((Boolean) domain.getExtensionparams().get("public_categ_idsdirtyflag"))
                model.setPublic_categ_ids(domain.getPublicCategIds());
            if((Boolean) domain.getExtensionparams().get("event_ticket_idsdirtyflag"))
                model.setEvent_ticket_ids(domain.getEventTicketIds());
            if((Boolean) domain.getExtensionparams().get("pricedirtyflag"))
                model.setPrice(domain.getPrice());
            if((Boolean) domain.getExtensionparams().get("attribute_line_idsdirtyflag"))
                model.setAttribute_line_ids(domain.getAttributeLineIds());
            if((Boolean) domain.getExtensionparams().get("virtual_availabledirtyflag"))
                model.setVirtual_available(domain.getVirtualAvailable());
            if((Boolean) domain.getExtensionparams().get("nbr_reordering_rulesdirtyflag"))
                model.setNbr_reordering_rules(domain.getNbrReorderingRules());
            if((Boolean) domain.getExtensionparams().get("activedirtyflag"))
                model.setActive(domain.getActive());
            if((Boolean) domain.getExtensionparams().get("message_is_followerdirtyflag"))
                model.setMessage_is_follower(domain.getMessageIsFollower());
            if((Boolean) domain.getExtensionparams().get("message_unread_counterdirtyflag"))
                model.setMessage_unread_counter(domain.getMessageUnreadCounter());
            if((Boolean) domain.getExtensionparams().get("iddirtyflag"))
                model.setId(domain.getId());
            if((Boolean) domain.getExtensionparams().get("website_price_differencedirtyflag"))
                model.setWebsite_price_difference(domain.getWebsitePriceDifference());
            if((Boolean) domain.getExtensionparams().get("message_follower_idsdirtyflag"))
                model.setMessage_follower_ids(domain.getMessageFollowerIds());
            if((Boolean) domain.getExtensionparams().get("cart_qtydirtyflag"))
                model.setCart_qty(domain.getCartQty());
            if((Boolean) domain.getExtensionparams().get("website_public_pricedirtyflag"))
                model.setWebsite_public_price(domain.getWebsitePublicPrice());
            if((Boolean) domain.getExtensionparams().get("rating_idsdirtyflag"))
                model.setRating_ids(domain.getRatingIds());
            if((Boolean) domain.getExtensionparams().get("bom_line_idsdirtyflag"))
                model.setBom_line_ids(domain.getBomLineIds());
            if((Boolean) domain.getExtensionparams().get("website_pricedirtyflag"))
                model.setWebsite_price(domain.getWebsitePrice());
            if((Boolean) domain.getExtensionparams().get("outgoing_qtydirtyflag"))
                model.setOutgoing_qty(domain.getOutgoingQty());
            if((Boolean) domain.getExtensionparams().get("sales_countdirtyflag"))
                model.setSales_count(domain.getSalesCount());
            if((Boolean) domain.getExtensionparams().get("valid_product_attribute_wnva_idsdirtyflag"))
                model.setValid_product_attribute_wnva_ids(domain.getValidProductAttributeWnvaIds());
            if((Boolean) domain.getExtensionparams().get("image_mediumdirtyflag"))
                model.setImage_medium(domain.getImageMedium());
            if((Boolean) domain.getExtensionparams().get("valid_existing_variant_idsdirtyflag"))
                model.setValid_existing_variant_ids(domain.getValidExistingVariantIds());
            if((Boolean) domain.getExtensionparams().get("stock_value_currency_iddirtyflag"))
                model.setStock_value_currency_id(domain.getStockValueCurrencyId());
            if((Boolean) domain.getExtensionparams().get("stock_valuedirtyflag"))
                model.setStock_value(domain.getStockValue());
            if((Boolean) domain.getExtensionparams().get("website_style_idsdirtyflag"))
                model.setWebsite_style_ids(domain.getWebsiteStyleIds());
            if((Boolean) domain.getExtensionparams().get("message_channel_idsdirtyflag"))
                model.setMessage_channel_ids(domain.getMessageChannelIds());
            if((Boolean) domain.getExtensionparams().get("weightdirtyflag"))
                model.setWeight(domain.getWeight());
            if((Boolean) domain.getExtensionparams().get("bom_idsdirtyflag"))
                model.setBom_ids(domain.getBomIds());
            if((Boolean) domain.getExtensionparams().get("message_has_error_counterdirtyflag"))
                model.setMessage_has_error_counter(domain.getMessageHasErrorCounter());
            if((Boolean) domain.getExtensionparams().get("activity_statedirtyflag"))
                model.setActivity_state(domain.getActivityState());
            if((Boolean) domain.getExtensionparams().get("valid_product_template_attribute_line_wnva_idsdirtyflag"))
                model.setValid_product_template_attribute_line_wnva_ids(domain.getValidProductTemplateAttributeLineWnvaIds());
            if((Boolean) domain.getExtensionparams().get("message_main_attachment_iddirtyflag"))
                model.setMessage_main_attachment_id(domain.getMessageMainAttachmentId());
            if((Boolean) domain.getExtensionparams().get("message_partner_idsdirtyflag"))
                model.setMessage_partner_ids(domain.getMessagePartnerIds());
            if((Boolean) domain.getExtensionparams().get("create_datedirtyflag"))
                model.setCreate_date(domain.getCreateDate());
            if((Boolean) domain.getExtensionparams().get("valid_product_attribute_value_idsdirtyflag"))
                model.setValid_product_attribute_value_ids(domain.getValidProductAttributeValueIds());
            if((Boolean) domain.getExtensionparams().get("qty_availabledirtyflag"))
                model.setQty_available(domain.getQtyAvailable());
            if((Boolean) domain.getExtensionparams().get("message_attachment_countdirtyflag"))
                model.setMessage_attachment_count(domain.getMessageAttachmentCount());
            if((Boolean) domain.getExtensionparams().get("image_variantdirtyflag"))
                model.setImage_variant(domain.getImageVariant());
            if((Boolean) domain.getExtensionparams().get("stock_move_idsdirtyflag"))
                model.setStock_move_ids(domain.getStockMoveIds());
            if((Boolean) domain.getExtensionparams().get("message_needactiondirtyflag"))
                model.setMessage_needaction(domain.getMessageNeedaction());
            if((Boolean) domain.getExtensionparams().get("website_message_idsdirtyflag"))
                model.setWebsite_message_ids(domain.getWebsiteMessageIds());
            if((Boolean) domain.getExtensionparams().get("stock_fifo_real_time_aml_idsdirtyflag"))
                model.setStock_fifo_real_time_aml_ids(domain.getStockFifoRealTimeAmlIds());
            if((Boolean) domain.getExtensionparams().get("activity_date_deadlinedirtyflag"))
                model.setActivity_date_deadline(domain.getActivityDateDeadline());
            if((Boolean) domain.getExtensionparams().get("codedirtyflag"))
                model.setCode(domain.getCode());
            if((Boolean) domain.getExtensionparams().get("reordering_min_qtydirtyflag"))
                model.setReordering_min_qty(domain.getReorderingMinQty());
            if((Boolean) domain.getExtensionparams().get("imagedirtyflag"))
                model.setImage(domain.getImage());
            if((Boolean) domain.getExtensionparams().get("route_idsdirtyflag"))
                model.setRoute_ids(domain.getRouteIds());
            if((Boolean) domain.getExtensionparams().get("taxes_iddirtyflag"))
                model.setTaxes_id(domain.getTaxesId());
            if((Boolean) domain.getExtensionparams().get("bom_countdirtyflag"))
                model.setBom_count(domain.getBomCount());
            if((Boolean) domain.getExtensionparams().get("message_needaction_counterdirtyflag"))
                model.setMessage_needaction_counter(domain.getMessageNeedactionCounter());
            if((Boolean) domain.getExtensionparams().get("packaging_idsdirtyflag"))
                model.setPackaging_ids(domain.getPackagingIds());
            if((Boolean) domain.getExtensionparams().get("valid_archived_variant_idsdirtyflag"))
                model.setValid_archived_variant_ids(domain.getValidArchivedVariantIds());
            if((Boolean) domain.getExtensionparams().get("activity_user_iddirtyflag"))
                model.setActivity_user_id(domain.getActivityUserId());
            if((Boolean) domain.getExtensionparams().get("item_idsdirtyflag"))
                model.setItem_ids(domain.getItemIds());
            if((Boolean) domain.getExtensionparams().get("purchased_product_qtydirtyflag"))
                model.setPurchased_product_qty(domain.getPurchasedProductQty());
            if((Boolean) domain.getExtensionparams().get("reordering_max_qtydirtyflag"))
                model.setReordering_max_qty(domain.getReorderingMaxQty());
            if((Boolean) domain.getExtensionparams().get("orderpoint_idsdirtyflag"))
                model.setOrderpoint_ids(domain.getOrderpointIds());
            if((Boolean) domain.getExtensionparams().get("optional_product_idsdirtyflag"))
                model.setOptional_product_ids(domain.getOptionalProductIds());
            if((Boolean) domain.getExtensionparams().get("is_product_variantdirtyflag"))
                model.setIs_product_variant(domain.getIsProductVariant());
            if((Boolean) domain.getExtensionparams().get("used_in_bom_countdirtyflag"))
                model.setUsed_in_bom_count(domain.getUsedInBomCount());
            if((Boolean) domain.getExtensionparams().get("qty_at_datedirtyflag"))
                model.setQty_at_date(domain.getQtyAtDate());
            if((Boolean) domain.getExtensionparams().get("message_has_errordirtyflag"))
                model.setMessage_has_error(domain.getMessageHasError());
            if((Boolean) domain.getExtensionparams().get("activity_idsdirtyflag"))
                model.setActivity_ids(domain.getActivityIds());
            if((Boolean) domain.getExtensionparams().get("message_idsdirtyflag"))
                model.setMessage_ids(domain.getMessageIds());
            if((Boolean) domain.getExtensionparams().get("barcodedirtyflag"))
                model.setBarcode(domain.getBarcode());
            if((Boolean) domain.getExtensionparams().get("display_namedirtyflag"))
                model.setDisplay_name(domain.getDisplayName());
            if((Boolean) domain.getExtensionparams().get("standard_pricedirtyflag"))
                model.setStandard_price(domain.getStandardPrice());
            if((Boolean) domain.getExtensionparams().get("attribute_value_idsdirtyflag"))
                model.setAttribute_value_ids(domain.getAttributeValueIds());
            if((Boolean) domain.getExtensionparams().get("price_extradirtyflag"))
                model.setPrice_extra(domain.getPriceExtra());
            if((Boolean) domain.getExtensionparams().get("variant_bom_idsdirtyflag"))
                model.setVariant_bom_ids(domain.getVariantBomIds());
            if((Boolean) domain.getExtensionparams().get("alternative_product_idsdirtyflag"))
                model.setAlternative_product_ids(domain.getAlternativeProductIds());
            if((Boolean) domain.getExtensionparams().get("default_codedirtyflag"))
                model.setDefault_code(domain.getDefaultCode());
            if((Boolean) domain.getExtensionparams().get("route_from_categ_idsdirtyflag"))
                model.setRoute_from_categ_ids(domain.getRouteFromCategIds());
            if((Boolean) domain.getExtensionparams().get("activity_summarydirtyflag"))
                model.setActivity_summary(domain.getActivitySummary());
            if((Boolean) domain.getExtensionparams().get("incoming_qtydirtyflag"))
                model.setIncoming_qty(domain.getIncomingQty());
            if((Boolean) domain.getExtensionparams().get("currency_iddirtyflag"))
                model.setCurrency_id(domain.getCurrencyId());
            if((Boolean) domain.getExtensionparams().get("trackingdirtyflag"))
                model.setTracking(domain.getTracking());
            if((Boolean) domain.getExtensionparams().get("description_pickingdirtyflag"))
                model.setDescription_picking(domain.getDescriptionPicking());
            if((Boolean) domain.getExtensionparams().get("property_stock_account_outputdirtyflag"))
                model.setProperty_stock_account_output(domain.getPropertyStockAccountOutput());
            if((Boolean) domain.getExtensionparams().get("sale_okdirtyflag"))
                model.setSale_ok(domain.getSaleOk());
            if((Boolean) domain.getExtensionparams().get("website_descriptiondirtyflag"))
                model.setWebsite_description(domain.getWebsiteDescription());
            if((Boolean) domain.getExtensionparams().get("website_meta_og_imgdirtyflag"))
                model.setWebsite_meta_og_img(domain.getWebsiteMetaOgImg());
            if((Boolean) domain.getExtensionparams().get("company_iddirtyflag"))
                model.setCompany_id(domain.getCompanyId());
            if((Boolean) domain.getExtensionparams().get("to_weightdirtyflag"))
                model.setTo_weight(domain.getToWeight());
            if((Boolean) domain.getExtensionparams().get("descriptiondirtyflag"))
                model.setDescription(domain.getDescription());
            if((Boolean) domain.getExtensionparams().get("description_pickingindirtyflag"))
                model.setDescription_pickingin(domain.getDescriptionPickingin());
            if((Boolean) domain.getExtensionparams().get("list_pricedirtyflag"))
                model.setList_price(domain.getListPrice());
            if((Boolean) domain.getExtensionparams().get("hide_expense_policydirtyflag"))
                model.setHide_expense_policy(domain.getHideExpensePolicy());
            if((Boolean) domain.getExtensionparams().get("description_saledirtyflag"))
                model.setDescription_sale(domain.getDescriptionSale());
            if((Boolean) domain.getExtensionparams().get("cost_methoddirtyflag"))
                model.setCost_method(domain.getCostMethod());
            if((Boolean) domain.getExtensionparams().get("sequencedirtyflag"))
                model.setSequence(domain.getSequence());
            if((Boolean) domain.getExtensionparams().get("sale_line_warn_msgdirtyflag"))
                model.setSale_line_warn_msg(domain.getSaleLineWarnMsg());
            if((Boolean) domain.getExtensionparams().get("warehouse_iddirtyflag"))
                model.setWarehouse_id(domain.getWarehouseId());
            if((Boolean) domain.getExtensionparams().get("rentaldirtyflag"))
                model.setRental(domain.getRental());
            if((Boolean) domain.getExtensionparams().get("property_account_creditor_price_differencedirtyflag"))
                model.setProperty_account_creditor_price_difference(domain.getPropertyAccountCreditorPriceDifference());
            if((Boolean) domain.getExtensionparams().get("weight_uom_namedirtyflag"))
                model.setWeight_uom_name(domain.getWeightUomName());
            if((Boolean) domain.getExtensionparams().get("cost_currency_iddirtyflag"))
                model.setCost_currency_id(domain.getCostCurrencyId());
            if((Boolean) domain.getExtensionparams().get("property_stock_account_inputdirtyflag"))
                model.setProperty_stock_account_input(domain.getPropertyStockAccountInput());
            if((Boolean) domain.getExtensionparams().get("namedirtyflag"))
                model.setName(domain.getName());
            if((Boolean) domain.getExtensionparams().get("produce_delaydirtyflag"))
                model.setProduce_delay(domain.getProduceDelay());
            if((Boolean) domain.getExtensionparams().get("is_seo_optimizeddirtyflag"))
                model.setIs_seo_optimized(domain.getIsSeoOptimized());
            if((Boolean) domain.getExtensionparams().get("website_urldirtyflag"))
                model.setWebsite_url(domain.getWebsiteUrl());
            if((Boolean) domain.getExtensionparams().get("rating_last_feedbackdirtyflag"))
                model.setRating_last_feedback(domain.getRatingLastFeedback());
            if((Boolean) domain.getExtensionparams().get("website_size_ydirtyflag"))
                model.setWebsite_size_y(domain.getWebsiteSizeY());
            if((Boolean) domain.getExtensionparams().get("event_okdirtyflag"))
                model.setEvent_ok(domain.getEventOk());
            if((Boolean) domain.getExtensionparams().get("inventory_availabilitydirtyflag"))
                model.setInventory_availability(domain.getInventoryAvailability());
            if((Boolean) domain.getExtensionparams().get("purchase_okdirtyflag"))
                model.setPurchase_ok(domain.getPurchaseOk());
            if((Boolean) domain.getExtensionparams().get("create_uid_textdirtyflag"))
                model.setCreate_uid_text(domain.getCreateUidText());
            if((Boolean) domain.getExtensionparams().get("rating_last_valuedirtyflag"))
                model.setRating_last_value(domain.getRatingLastValue());
            if((Boolean) domain.getExtensionparams().get("website_meta_titledirtyflag"))
                model.setWebsite_meta_title(domain.getWebsiteMetaTitle());
            if((Boolean) domain.getExtensionparams().get("rating_last_imagedirtyflag"))
                model.setRating_last_image(domain.getRatingLastImage());
            if((Boolean) domain.getExtensionparams().get("description_purchasedirtyflag"))
                model.setDescription_purchase(domain.getDescriptionPurchase());
            if((Boolean) domain.getExtensionparams().get("website_iddirtyflag"))
                model.setWebsite_id(domain.getWebsiteId());
            if((Boolean) domain.getExtensionparams().get("can_be_expenseddirtyflag"))
                model.setCan_be_expensed(domain.getCanBeExpensed());
            if((Boolean) domain.getExtensionparams().get("sale_line_warndirtyflag"))
                model.setSale_line_warn(domain.getSaleLineWarn());
            if((Boolean) domain.getExtensionparams().get("website_size_xdirtyflag"))
                model.setWebsite_size_x(domain.getWebsiteSizeX());
            if((Boolean) domain.getExtensionparams().get("service_to_purchasedirtyflag"))
                model.setService_to_purchase(domain.getServiceToPurchase());
            if((Boolean) domain.getExtensionparams().get("website_sequencedirtyflag"))
                model.setWebsite_sequence(domain.getWebsiteSequence());
            if((Boolean) domain.getExtensionparams().get("property_stock_inventorydirtyflag"))
                model.setProperty_stock_inventory(domain.getPropertyStockInventory());
            if((Boolean) domain.getExtensionparams().get("location_iddirtyflag"))
                model.setLocation_id(domain.getLocationId());
            if((Boolean) domain.getExtensionparams().get("property_valuationdirtyflag"))
                model.setProperty_valuation(domain.getPropertyValuation());
            if((Boolean) domain.getExtensionparams().get("is_publisheddirtyflag"))
                model.setIs_published(domain.getIsPublished());
            if((Boolean) domain.getExtensionparams().get("expense_policydirtyflag"))
                model.setExpense_policy(domain.getExpensePolicy());
            if((Boolean) domain.getExtensionparams().get("weight_uom_iddirtyflag"))
                model.setWeight_uom_id(domain.getWeightUomId());
            if((Boolean) domain.getExtensionparams().get("colordirtyflag"))
                model.setColor(domain.getColor());
            if((Boolean) domain.getExtensionparams().get("property_stock_productiondirtyflag"))
                model.setProperty_stock_production(domain.getPropertyStockProduction());
            if((Boolean) domain.getExtensionparams().get("website_publisheddirtyflag"))
                model.setWebsite_published(domain.getWebsitePublished());
            if((Boolean) domain.getExtensionparams().get("website_meta_keywordsdirtyflag"))
                model.setWebsite_meta_keywords(domain.getWebsiteMetaKeywords());
            if((Boolean) domain.getExtensionparams().get("description_pickingoutdirtyflag"))
                model.setDescription_pickingout(domain.getDescriptionPickingout());
            if((Boolean) domain.getExtensionparams().get("pricelist_iddirtyflag"))
                model.setPricelist_id(domain.getPricelistId());
            if((Boolean) domain.getExtensionparams().get("rating_countdirtyflag"))
                model.setRating_count(domain.getRatingCount());
            if((Boolean) domain.getExtensionparams().get("website_meta_descriptiondirtyflag"))
                model.setWebsite_meta_description(domain.getWebsiteMetaDescription());
            if((Boolean) domain.getExtensionparams().get("valuationdirtyflag"))
                model.setValuation(domain.getValuation());
            if((Boolean) domain.getExtensionparams().get("invoice_policydirtyflag"))
                model.setInvoice_policy(domain.getInvoicePolicy());
            if((Boolean) domain.getExtensionparams().get("purchase_line_warn_msgdirtyflag"))
                model.setPurchase_line_warn_msg(domain.getPurchaseLineWarnMsg());
            if((Boolean) domain.getExtensionparams().get("write_uid_textdirtyflag"))
                model.setWrite_uid_text(domain.getWriteUidText());
            if((Boolean) domain.getExtensionparams().get("property_account_income_iddirtyflag"))
                model.setProperty_account_income_id(domain.getPropertyAccountIncomeId());
            if((Boolean) domain.getExtensionparams().get("property_cost_methoddirtyflag"))
                model.setProperty_cost_method(domain.getPropertyCostMethod());
            if((Boolean) domain.getExtensionparams().get("categ_iddirtyflag"))
                model.setCateg_id(domain.getCategId());
            if((Boolean) domain.getExtensionparams().get("ispartsdirtyflag"))
                model.setIsParts(domain.getIsparts());
            if((Boolean) domain.getExtensionparams().get("uom_iddirtyflag"))
                model.setUom_id(domain.getUomId());
            if((Boolean) domain.getExtensionparams().get("product_variant_iddirtyflag"))
                model.setProduct_variant_id(domain.getProductVariantId());
            if((Boolean) domain.getExtensionparams().get("typedirtyflag"))
                model.setType(domain.getType());
            if((Boolean) domain.getExtensionparams().get("purchase_methoddirtyflag"))
                model.setPurchase_method(domain.getPurchaseMethod());
            if((Boolean) domain.getExtensionparams().get("responsible_iddirtyflag"))
                model.setResponsible_id(domain.getResponsibleId());
            if((Boolean) domain.getExtensionparams().get("service_typedirtyflag"))
                model.setService_type(domain.getServiceType());
            if((Boolean) domain.getExtensionparams().get("uom_namedirtyflag"))
                model.setUom_name(domain.getUomName());
            if((Boolean) domain.getExtensionparams().get("available_thresholddirtyflag"))
                model.setAvailable_threshold(domain.getAvailableThreshold());
            if((Boolean) domain.getExtensionparams().get("purchase_line_warndirtyflag"))
                model.setPurchase_line_warn(domain.getPurchaseLineWarn());
            if((Boolean) domain.getExtensionparams().get("product_variant_countdirtyflag"))
                model.setProduct_variant_count(domain.getProductVariantCount());
            if((Boolean) domain.getExtensionparams().get("pos_categ_iddirtyflag"))
                model.setPos_categ_id(domain.getPosCategId());
            if((Boolean) domain.getExtensionparams().get("custom_messagedirtyflag"))
                model.setCustom_message(domain.getCustomMessage());
            if((Boolean) domain.getExtensionparams().get("property_account_expense_iddirtyflag"))
                model.setProperty_account_expense_id(domain.getPropertyAccountExpenseId());
            if((Boolean) domain.getExtensionparams().get("sale_delaydirtyflag"))
                model.setSale_delay(domain.getSaleDelay());
            if((Boolean) domain.getExtensionparams().get("uom_po_iddirtyflag"))
                model.setUom_po_id(domain.getUomPoId());
            if((Boolean) domain.getExtensionparams().get("available_in_posdirtyflag"))
                model.setAvailable_in_pos(domain.getAvailableInPos());
            if((Boolean) domain.getExtensionparams().get("create_uiddirtyflag"))
                model.setCreate_uid(domain.getCreateUid());
            if((Boolean) domain.getExtensionparams().get("write_uiddirtyflag"))
                model.setWrite_uid(domain.getWriteUid());
            if((Boolean) domain.getExtensionparams().get("product_tmpl_iddirtyflag"))
                model.setProduct_tmpl_id(domain.getProductTmplId());
            domain.getFocusNull().clear();
        }
        return model ;
    }

    public Product_product convert2Domain( product_productClientModel model ,Product_product domain) {
        if(model == null)
            return domain ;
        if(domain == null){
            domain = new Product_product();
        }

        if(model.getVariant_seller_idsDirtyFlag())
            domain.setVariantSellerIds(model.getVariant_seller_ids());
        if(model.getProduct_template_attribute_value_idsDirtyFlag())
            domain.setProductTemplateAttributeValueIds(model.getProduct_template_attribute_value_ids());
        if(model.getProduct_variant_idsDirtyFlag())
            domain.setProductVariantIds(model.getProduct_variant_ids());
        if(model.getImage_smallDirtyFlag())
            domain.setImageSmall(model.getImage_small());
        if(model.getMessage_unreadDirtyFlag())
            domain.setMessageUnread(model.getMessage_unread());
        if(model.getVolumeDirtyFlag())
            domain.setVolume(model.getVolume());
        if(model.getLst_priceDirtyFlag())
            domain.setLstPrice(model.getLst_price());
        if(model.getValid_product_attribute_idsDirtyFlag())
            domain.setValidProductAttributeIds(model.getValid_product_attribute_ids());
        if(model.getStock_fifo_manual_move_idsDirtyFlag())
            domain.setStockFifoManualMoveIds(model.getStock_fifo_manual_move_ids());
        if(model.getStock_quant_idsDirtyFlag())
            domain.setStockQuantIds(model.getStock_quant_ids());
        if(model.getSupplier_taxes_idDirtyFlag())
            domain.setSupplierTaxesId(model.getSupplier_taxes_id());
        if(model.getPricelist_item_idsDirtyFlag())
            domain.setPricelistItemIds(model.getPricelist_item_ids());
        if(model.get__last_updateDirtyFlag())
            domain.setLastUpdate(model.get__last_update());
        if(model.getAccessory_product_idsDirtyFlag())
            domain.setAccessoryProductIds(model.getAccessory_product_ids());
        if(model.getSeller_idsDirtyFlag())
            domain.setSellerIds(model.getSeller_ids());
        if(model.getValid_product_attribute_value_wnva_idsDirtyFlag())
            domain.setValidProductAttributeValueWnvaIds(model.getValid_product_attribute_value_wnva_ids());
        if(model.getPartner_refDirtyFlag())
            domain.setPartnerRef(model.getPartner_ref());
        if(model.getProduct_image_idsDirtyFlag())
            domain.setProductImageIds(model.getProduct_image_ids());
        if(model.getMrp_product_qtyDirtyFlag())
            domain.setMrpProductQty(model.getMrp_product_qty());
        if(model.getWrite_dateDirtyFlag())
            domain.setWriteDate(model.getWrite_date());
        if(model.getValid_product_template_attribute_line_idsDirtyFlag())
            domain.setValidProductTemplateAttributeLineIds(model.getValid_product_template_attribute_line_ids());
        if(model.getActivity_type_idDirtyFlag())
            domain.setActivityTypeId(model.getActivity_type_id());
        if(model.getPublic_categ_idsDirtyFlag())
            domain.setPublicCategIds(model.getPublic_categ_ids());
        if(model.getEvent_ticket_idsDirtyFlag())
            domain.setEventTicketIds(model.getEvent_ticket_ids());
        if(model.getPriceDirtyFlag())
            domain.setPrice(model.getPrice());
        if(model.getAttribute_line_idsDirtyFlag())
            domain.setAttributeLineIds(model.getAttribute_line_ids());
        if(model.getVirtual_availableDirtyFlag())
            domain.setVirtualAvailable(model.getVirtual_available());
        if(model.getNbr_reordering_rulesDirtyFlag())
            domain.setNbrReorderingRules(model.getNbr_reordering_rules());
        if(model.getActiveDirtyFlag())
            domain.setActive(model.getActive());
        if(model.getMessage_is_followerDirtyFlag())
            domain.setMessageIsFollower(model.getMessage_is_follower());
        if(model.getMessage_unread_counterDirtyFlag())
            domain.setMessageUnreadCounter(model.getMessage_unread_counter());
        if(model.getIdDirtyFlag())
            domain.setId(model.getId());
        if(model.getWebsite_price_differenceDirtyFlag())
            domain.setWebsitePriceDifference(model.getWebsite_price_difference());
        if(model.getMessage_follower_idsDirtyFlag())
            domain.setMessageFollowerIds(model.getMessage_follower_ids());
        if(model.getCart_qtyDirtyFlag())
            domain.setCartQty(model.getCart_qty());
        if(model.getWebsite_public_priceDirtyFlag())
            domain.setWebsitePublicPrice(model.getWebsite_public_price());
        if(model.getRating_idsDirtyFlag())
            domain.setRatingIds(model.getRating_ids());
        if(model.getBom_line_idsDirtyFlag())
            domain.setBomLineIds(model.getBom_line_ids());
        if(model.getWebsite_priceDirtyFlag())
            domain.setWebsitePrice(model.getWebsite_price());
        if(model.getOutgoing_qtyDirtyFlag())
            domain.setOutgoingQty(model.getOutgoing_qty());
        if(model.getSales_countDirtyFlag())
            domain.setSalesCount(model.getSales_count());
        if(model.getValid_product_attribute_wnva_idsDirtyFlag())
            domain.setValidProductAttributeWnvaIds(model.getValid_product_attribute_wnva_ids());
        if(model.getImage_mediumDirtyFlag())
            domain.setImageMedium(model.getImage_medium());
        if(model.getValid_existing_variant_idsDirtyFlag())
            domain.setValidExistingVariantIds(model.getValid_existing_variant_ids());
        if(model.getStock_value_currency_idDirtyFlag())
            domain.setStockValueCurrencyId(model.getStock_value_currency_id());
        if(model.getStock_valueDirtyFlag())
            domain.setStockValue(model.getStock_value());
        if(model.getWebsite_style_idsDirtyFlag())
            domain.setWebsiteStyleIds(model.getWebsite_style_ids());
        if(model.getMessage_channel_idsDirtyFlag())
            domain.setMessageChannelIds(model.getMessage_channel_ids());
        if(model.getWeightDirtyFlag())
            domain.setWeight(model.getWeight());
        if(model.getBom_idsDirtyFlag())
            domain.setBomIds(model.getBom_ids());
        if(model.getMessage_has_error_counterDirtyFlag())
            domain.setMessageHasErrorCounter(model.getMessage_has_error_counter());
        if(model.getActivity_stateDirtyFlag())
            domain.setActivityState(model.getActivity_state());
        if(model.getValid_product_template_attribute_line_wnva_idsDirtyFlag())
            domain.setValidProductTemplateAttributeLineWnvaIds(model.getValid_product_template_attribute_line_wnva_ids());
        if(model.getMessage_main_attachment_idDirtyFlag())
            domain.setMessageMainAttachmentId(model.getMessage_main_attachment_id());
        if(model.getMessage_partner_idsDirtyFlag())
            domain.setMessagePartnerIds(model.getMessage_partner_ids());
        if(model.getCreate_dateDirtyFlag())
            domain.setCreateDate(model.getCreate_date());
        if(model.getValid_product_attribute_value_idsDirtyFlag())
            domain.setValidProductAttributeValueIds(model.getValid_product_attribute_value_ids());
        if(model.getQty_availableDirtyFlag())
            domain.setQtyAvailable(model.getQty_available());
        if(model.getMessage_attachment_countDirtyFlag())
            domain.setMessageAttachmentCount(model.getMessage_attachment_count());
        if(model.getImage_variantDirtyFlag())
            domain.setImageVariant(model.getImage_variant());
        if(model.getStock_move_idsDirtyFlag())
            domain.setStockMoveIds(model.getStock_move_ids());
        if(model.getMessage_needactionDirtyFlag())
            domain.setMessageNeedaction(model.getMessage_needaction());
        if(model.getWebsite_message_idsDirtyFlag())
            domain.setWebsiteMessageIds(model.getWebsite_message_ids());
        if(model.getStock_fifo_real_time_aml_idsDirtyFlag())
            domain.setStockFifoRealTimeAmlIds(model.getStock_fifo_real_time_aml_ids());
        if(model.getActivity_date_deadlineDirtyFlag())
            domain.setActivityDateDeadline(model.getActivity_date_deadline());
        if(model.getCodeDirtyFlag())
            domain.setCode(model.getCode());
        if(model.getReordering_min_qtyDirtyFlag())
            domain.setReorderingMinQty(model.getReordering_min_qty());
        if(model.getImageDirtyFlag())
            domain.setImage(model.getImage());
        if(model.getRoute_idsDirtyFlag())
            domain.setRouteIds(model.getRoute_ids());
        if(model.getTaxes_idDirtyFlag())
            domain.setTaxesId(model.getTaxes_id());
        if(model.getBom_countDirtyFlag())
            domain.setBomCount(model.getBom_count());
        if(model.getMessage_needaction_counterDirtyFlag())
            domain.setMessageNeedactionCounter(model.getMessage_needaction_counter());
        if(model.getPackaging_idsDirtyFlag())
            domain.setPackagingIds(model.getPackaging_ids());
        if(model.getValid_archived_variant_idsDirtyFlag())
            domain.setValidArchivedVariantIds(model.getValid_archived_variant_ids());
        if(model.getActivity_user_idDirtyFlag())
            domain.setActivityUserId(model.getActivity_user_id());
        if(model.getItem_idsDirtyFlag())
            domain.setItemIds(model.getItem_ids());
        if(model.getPurchased_product_qtyDirtyFlag())
            domain.setPurchasedProductQty(model.getPurchased_product_qty());
        if(model.getReordering_max_qtyDirtyFlag())
            domain.setReorderingMaxQty(model.getReordering_max_qty());
        if(model.getOrderpoint_idsDirtyFlag())
            domain.setOrderpointIds(model.getOrderpoint_ids());
        if(model.getOptional_product_idsDirtyFlag())
            domain.setOptionalProductIds(model.getOptional_product_ids());
        if(model.getIs_product_variantDirtyFlag())
            domain.setIsProductVariant(model.getIs_product_variant());
        if(model.getUsed_in_bom_countDirtyFlag())
            domain.setUsedInBomCount(model.getUsed_in_bom_count());
        if(model.getQty_at_dateDirtyFlag())
            domain.setQtyAtDate(model.getQty_at_date());
        if(model.getMessage_has_errorDirtyFlag())
            domain.setMessageHasError(model.getMessage_has_error());
        if(model.getActivity_idsDirtyFlag())
            domain.setActivityIds(model.getActivity_ids());
        if(model.getMessage_idsDirtyFlag())
            domain.setMessageIds(model.getMessage_ids());
        if(model.getBarcodeDirtyFlag())
            domain.setBarcode(model.getBarcode());
        if(model.getDisplay_nameDirtyFlag())
            domain.setDisplayName(model.getDisplay_name());
        if(model.getStandard_priceDirtyFlag())
            domain.setStandardPrice(model.getStandard_price());
        if(model.getAttribute_value_idsDirtyFlag())
            domain.setAttributeValueIds(model.getAttribute_value_ids());
        if(model.getPrice_extraDirtyFlag())
            domain.setPriceExtra(model.getPrice_extra());
        if(model.getVariant_bom_idsDirtyFlag())
            domain.setVariantBomIds(model.getVariant_bom_ids());
        if(model.getAlternative_product_idsDirtyFlag())
            domain.setAlternativeProductIds(model.getAlternative_product_ids());
        if(model.getDefault_codeDirtyFlag())
            domain.setDefaultCode(model.getDefault_code());
        if(model.getRoute_from_categ_idsDirtyFlag())
            domain.setRouteFromCategIds(model.getRoute_from_categ_ids());
        if(model.getActivity_summaryDirtyFlag())
            domain.setActivitySummary(model.getActivity_summary());
        if(model.getIncoming_qtyDirtyFlag())
            domain.setIncomingQty(model.getIncoming_qty());
        if(model.getCurrency_idDirtyFlag())
            domain.setCurrencyId(model.getCurrency_id());
        if(model.getTrackingDirtyFlag())
            domain.setTracking(model.getTracking());
        if(model.getDescription_pickingDirtyFlag())
            domain.setDescriptionPicking(model.getDescription_picking());
        if(model.getProperty_stock_account_outputDirtyFlag())
            domain.setPropertyStockAccountOutput(model.getProperty_stock_account_output());
        if(model.getSale_okDirtyFlag())
            domain.setSaleOk(model.getSale_ok());
        if(model.getWebsite_descriptionDirtyFlag())
            domain.setWebsiteDescription(model.getWebsite_description());
        if(model.getWebsite_meta_og_imgDirtyFlag())
            domain.setWebsiteMetaOgImg(model.getWebsite_meta_og_img());
        if(model.getCompany_idDirtyFlag())
            domain.setCompanyId(model.getCompany_id());
        if(model.getTo_weightDirtyFlag())
            domain.setToWeight(model.getTo_weight());
        if(model.getDescriptionDirtyFlag())
            domain.setDescription(model.getDescription());
        if(model.getDescription_pickinginDirtyFlag())
            domain.setDescriptionPickingin(model.getDescription_pickingin());
        if(model.getList_priceDirtyFlag())
            domain.setListPrice(model.getList_price());
        if(model.getHide_expense_policyDirtyFlag())
            domain.setHideExpensePolicy(model.getHide_expense_policy());
        if(model.getDescription_saleDirtyFlag())
            domain.setDescriptionSale(model.getDescription_sale());
        if(model.getCost_methodDirtyFlag())
            domain.setCostMethod(model.getCost_method());
        if(model.getSequenceDirtyFlag())
            domain.setSequence(model.getSequence());
        if(model.getSale_line_warn_msgDirtyFlag())
            domain.setSaleLineWarnMsg(model.getSale_line_warn_msg());
        if(model.getWarehouse_idDirtyFlag())
            domain.setWarehouseId(model.getWarehouse_id());
        if(model.getRentalDirtyFlag())
            domain.setRental(model.getRental());
        if(model.getProperty_account_creditor_price_differenceDirtyFlag())
            domain.setPropertyAccountCreditorPriceDifference(model.getProperty_account_creditor_price_difference());
        if(model.getWeight_uom_nameDirtyFlag())
            domain.setWeightUomName(model.getWeight_uom_name());
        if(model.getCost_currency_idDirtyFlag())
            domain.setCostCurrencyId(model.getCost_currency_id());
        if(model.getProperty_stock_account_inputDirtyFlag())
            domain.setPropertyStockAccountInput(model.getProperty_stock_account_input());
        if(model.getNameDirtyFlag())
            domain.setName(model.getName());
        if(model.getProduce_delayDirtyFlag())
            domain.setProduceDelay(model.getProduce_delay());
        if(model.getIs_seo_optimizedDirtyFlag())
            domain.setIsSeoOptimized(model.getIs_seo_optimized());
        if(model.getWebsite_urlDirtyFlag())
            domain.setWebsiteUrl(model.getWebsite_url());
        if(model.getRating_last_feedbackDirtyFlag())
            domain.setRatingLastFeedback(model.getRating_last_feedback());
        if(model.getWebsite_size_yDirtyFlag())
            domain.setWebsiteSizeY(model.getWebsite_size_y());
        if(model.getEvent_okDirtyFlag())
            domain.setEventOk(model.getEvent_ok());
        if(model.getInventory_availabilityDirtyFlag())
            domain.setInventoryAvailability(model.getInventory_availability());
        if(model.getPurchase_okDirtyFlag())
            domain.setPurchaseOk(model.getPurchase_ok());
        if(model.getCreate_uid_textDirtyFlag())
            domain.setCreateUidText(model.getCreate_uid_text());
        if(model.getRating_last_valueDirtyFlag())
            domain.setRatingLastValue(model.getRating_last_value());
        if(model.getWebsite_meta_titleDirtyFlag())
            domain.setWebsiteMetaTitle(model.getWebsite_meta_title());
        if(model.getRating_last_imageDirtyFlag())
            domain.setRatingLastImage(model.getRating_last_image());
        if(model.getDescription_purchaseDirtyFlag())
            domain.setDescriptionPurchase(model.getDescription_purchase());
        if(model.getWebsite_idDirtyFlag())
            domain.setWebsiteId(model.getWebsite_id());
        if(model.getCan_be_expensedDirtyFlag())
            domain.setCanBeExpensed(model.getCan_be_expensed());
        if(model.getSale_line_warnDirtyFlag())
            domain.setSaleLineWarn(model.getSale_line_warn());
        if(model.getWebsite_size_xDirtyFlag())
            domain.setWebsiteSizeX(model.getWebsite_size_x());
        if(model.getService_to_purchaseDirtyFlag())
            domain.setServiceToPurchase(model.getService_to_purchase());
        if(model.getWebsite_sequenceDirtyFlag())
            domain.setWebsiteSequence(model.getWebsite_sequence());
        if(model.getProperty_stock_inventoryDirtyFlag())
            domain.setPropertyStockInventory(model.getProperty_stock_inventory());
        if(model.getLocation_idDirtyFlag())
            domain.setLocationId(model.getLocation_id());
        if(model.getProperty_valuationDirtyFlag())
            domain.setPropertyValuation(model.getProperty_valuation());
        if(model.getIs_publishedDirtyFlag())
            domain.setIsPublished(model.getIs_published());
        if(model.getExpense_policyDirtyFlag())
            domain.setExpensePolicy(model.getExpense_policy());
        if(model.getWeight_uom_idDirtyFlag())
            domain.setWeightUomId(model.getWeight_uom_id());
        if(model.getColorDirtyFlag())
            domain.setColor(model.getColor());
        if(model.getProperty_stock_productionDirtyFlag())
            domain.setPropertyStockProduction(model.getProperty_stock_production());
        if(model.getWebsite_publishedDirtyFlag())
            domain.setWebsitePublished(model.getWebsite_published());
        if(model.getWebsite_meta_keywordsDirtyFlag())
            domain.setWebsiteMetaKeywords(model.getWebsite_meta_keywords());
        if(model.getDescription_pickingoutDirtyFlag())
            domain.setDescriptionPickingout(model.getDescription_pickingout());
        if(model.getPricelist_idDirtyFlag())
            domain.setPricelistId(model.getPricelist_id());
        if(model.getRating_countDirtyFlag())
            domain.setRatingCount(model.getRating_count());
        if(model.getWebsite_meta_descriptionDirtyFlag())
            domain.setWebsiteMetaDescription(model.getWebsite_meta_description());
        if(model.getValuationDirtyFlag())
            domain.setValuation(model.getValuation());
        if(model.getInvoice_policyDirtyFlag())
            domain.setInvoicePolicy(model.getInvoice_policy());
        if(model.getPurchase_line_warn_msgDirtyFlag())
            domain.setPurchaseLineWarnMsg(model.getPurchase_line_warn_msg());
        if(model.getWrite_uid_textDirtyFlag())
            domain.setWriteUidText(model.getWrite_uid_text());
        if(model.getProperty_account_income_idDirtyFlag())
            domain.setPropertyAccountIncomeId(model.getProperty_account_income_id());
        if(model.getProperty_cost_methodDirtyFlag())
            domain.setPropertyCostMethod(model.getProperty_cost_method());
        if(model.getCateg_idDirtyFlag())
            domain.setCategId(model.getCateg_id());
        if(model.getIsPartsDirtyFlag())
            domain.setIsparts(model.getIsParts());
        if(model.getUom_idDirtyFlag())
            domain.setUomId(model.getUom_id());
        if(model.getProduct_variant_idDirtyFlag())
            domain.setProductVariantId(model.getProduct_variant_id());
        if(model.getTypeDirtyFlag())
            domain.setType(model.getType());
        if(model.getPurchase_methodDirtyFlag())
            domain.setPurchaseMethod(model.getPurchase_method());
        if(model.getResponsible_idDirtyFlag())
            domain.setResponsibleId(model.getResponsible_id());
        if(model.getService_typeDirtyFlag())
            domain.setServiceType(model.getService_type());
        if(model.getUom_nameDirtyFlag())
            domain.setUomName(model.getUom_name());
        if(model.getAvailable_thresholdDirtyFlag())
            domain.setAvailableThreshold(model.getAvailable_threshold());
        if(model.getPurchase_line_warnDirtyFlag())
            domain.setPurchaseLineWarn(model.getPurchase_line_warn());
        if(model.getProduct_variant_countDirtyFlag())
            domain.setProductVariantCount(model.getProduct_variant_count());
        if(model.getPos_categ_idDirtyFlag())
            domain.setPosCategId(model.getPos_categ_id());
        if(model.getCustom_messageDirtyFlag())
            domain.setCustomMessage(model.getCustom_message());
        if(model.getProperty_account_expense_idDirtyFlag())
            domain.setPropertyAccountExpenseId(model.getProperty_account_expense_id());
        if(model.getSale_delayDirtyFlag())
            domain.setSaleDelay(model.getSale_delay());
        if(model.getUom_po_idDirtyFlag())
            domain.setUomPoId(model.getUom_po_id());
        if(model.getAvailable_in_posDirtyFlag())
            domain.setAvailableInPos(model.getAvailable_in_pos());
        if(model.getCreate_uidDirtyFlag())
            domain.setCreateUid(model.getCreate_uid());
        if(model.getWrite_uidDirtyFlag())
            domain.setWriteUid(model.getWrite_uid());
        if(model.getProduct_tmpl_idDirtyFlag())
            domain.setProductTmplId(model.getProduct_tmpl_id());
        return domain ;
    }

}

    



