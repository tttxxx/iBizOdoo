package cn.ibizlab.odoo.core.odoo_payment.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.odoo.util.domain.EntityBase;
import cn.ibizlab.odoo.util.annotation.DEField;
import cn.ibizlab.odoo.util.enums.DEPredefinedFieldType;
import cn.ibizlab.odoo.util.enums.DEFieldDefaultValueType;
import java.io.Serializable;
import lombok.Data;
import org.springframework.data.annotation.Transient;

import cn.ibizlab.odoo.util.domain.EntityClient;

/**
 * ServiceApi [付款交易] 对象
 */
@Data
public class Payment_transaction extends EntityClient implements Serializable {

    /**
     * ID
     */
    @DEField(isKeyField=true)
    @JSONField(name = "id")
    @JsonProperty("id")
    private Integer id;

    /**
     * 金额
     */
    @JSONField(name = "amount")
    @JsonProperty("amount")
    private Double amount;

    /**
     * 电话
     */
    @DEField(name = "partner_phone")
    @JSONField(name = "partner_phone")
    @JsonProperty("partner_phone")
    private String partnerPhone;

    /**
     * EMail
     */
    @DEField(name = "partner_email")
    @JSONField(name = "partner_email")
    @JsonProperty("partner_email")
    private String partnerEmail;

    /**
     * 回调方法
     */
    @DEField(name = "callback_method")
    @JSONField(name = "callback_method")
    @JsonProperty("callback_method")
    private String callbackMethod;

    /**
     * 付款后返回网址
     */
    @DEField(name = "return_url")
    @JSONField(name = "return_url")
    @JsonProperty("return_url")
    private String returnUrl;

    /**
     * # 销售订单
     */
    @JSONField(name = "sale_order_ids_nbr")
    @JsonProperty("sale_order_ids_nbr")
    private Integer saleOrderIdsNbr;

    /**
     * 收单方参考
     */
    @DEField(name = "acquirer_reference")
    @JSONField(name = "acquirer_reference")
    @JsonProperty("acquirer_reference")
    private String acquirerReference;

    /**
     * 语言
     */
    @DEField(name = "partner_lang")
    @JSONField(name = "partner_lang")
    @JsonProperty("partner_lang")
    private String partnerLang;

    /**
     * 邮政编码
     */
    @DEField(name = "partner_zip")
    @JSONField(name = "partner_zip")
    @JsonProperty("partner_zip")
    private String partnerZip;

    /**
     * 显示名称
     */
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    private String displayName;

    /**
     * 状态
     */
    @JSONField(name = "state")
    @JsonProperty("state")
    private String state;

    /**
     * 验证日期
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("date")
    private Timestamp date;

    /**
     * 回调文档模型
     */
    @DEField(name = "callback_model_id")
    @JSONField(name = "callback_model_id")
    @JsonProperty("callback_model_id")
    private Integer callbackModelId;

    /**
     * 最后修改日
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;

    /**
     * 发票
     */
    @JSONField(name = "invoice_ids")
    @JsonProperty("invoice_ids")
    private String invoiceIds;

    /**
     * 销售订单
     */
    @JSONField(name = "sale_order_ids")
    @JsonProperty("sale_order_ids")
    private String saleOrderIds;

    /**
     * 费用
     */
    @JSONField(name = "fees")
    @JsonProperty("fees")
    private Double fees;

    /**
     * 消息
     */
    @DEField(name = "state_message")
    @JSONField(name = "state_message")
    @JsonProperty("state_message")
    private String stateMessage;

    /**
     * 回调文档 ID
     */
    @DEField(name = "callback_res_id")
    @JSONField(name = "callback_res_id")
    @JsonProperty("callback_res_id")
    private Integer callbackResId;

    /**
     * 城市
     */
    @DEField(name = "partner_city")
    @JSONField(name = "partner_city")
    @JsonProperty("partner_city")
    private String partnerCity;

    /**
     * 最后更新时间
     */
    @DEField(name = "write_date" , preType = DEPredefinedFieldType.UPDATEDATE)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;

    /**
     * 创建时间
     */
    @DEField(name = "create_date" , preType = DEPredefinedFieldType.CREATEDATE)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;

    /**
     * # 发票
     */
    @JSONField(name = "invoice_ids_nbr")
    @JsonProperty("invoice_ids_nbr")
    private Integer invoiceIdsNbr;

    /**
     * 类型
     */
    @JSONField(name = "type")
    @JsonProperty("type")
    private String type;

    /**
     * 3D Secure HTML
     */
    @DEField(name = "html_3ds")
    @JSONField(name = "html_3ds")
    @JsonProperty("html_3ds")
    private String html3ds;

    /**
     * 合作伙伴名称
     */
    @DEField(name = "partner_name")
    @JSONField(name = "partner_name")
    @JsonProperty("partner_name")
    private String partnerName;

    /**
     * 参考
     */
    @JSONField(name = "reference")
    @JsonProperty("reference")
    private String reference;

    /**
     * 地址
     */
    @DEField(name = "partner_address")
    @JSONField(name = "partner_address")
    @JsonProperty("partner_address")
    private String partnerAddress;

    /**
     * 付款是否已过账处理
     */
    @DEField(name = "is_processed")
    @JSONField(name = "is_processed")
    @JsonProperty("is_processed")
    private String isProcessed;

    /**
     * 回调哈希函数
     */
    @DEField(name = "callback_hash")
    @JSONField(name = "callback_hash")
    @JsonProperty("callback_hash")
    private String callbackHash;

    /**
     * 币种
     */
    @JSONField(name = "currency_id_text")
    @JsonProperty("currency_id_text")
    private String currencyIdText;

    /**
     * 最后更新人
     */
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    private String writeUidText;

    /**
     * 创建人
     */
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    private String createUidText;

    /**
     * 付款
     */
    @JSONField(name = "payment_id_text")
    @JsonProperty("payment_id_text")
    private String paymentIdText;

    /**
     * 客户
     */
    @JSONField(name = "partner_id_text")
    @JsonProperty("partner_id_text")
    private String partnerIdText;

    /**
     * 服务商
     */
    @JSONField(name = "provider")
    @JsonProperty("provider")
    private String provider;

    /**
     * 收单方
     */
    @JSONField(name = "acquirer_id_text")
    @JsonProperty("acquirer_id_text")
    private String acquirerIdText;

    /**
     * 国家
     */
    @JSONField(name = "partner_country_id_text")
    @JsonProperty("partner_country_id_text")
    private String partnerCountryIdText;

    /**
     * 付款令牌
     */
    @JSONField(name = "payment_token_id_text")
    @JsonProperty("payment_token_id_text")
    private String paymentTokenIdText;

    /**
     * 币种
     */
    @DEField(name = "currency_id")
    @JSONField(name = "currency_id")
    @JsonProperty("currency_id")
    private Integer currencyId;

    /**
     * 最后更新人
     */
    @DEField(name = "write_uid" , preType = DEPredefinedFieldType.UPDATEMAN)
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    private Integer writeUid;

    /**
     * 国家
     */
    @DEField(name = "partner_country_id")
    @JSONField(name = "partner_country_id")
    @JsonProperty("partner_country_id")
    private Integer partnerCountryId;

    /**
     * 付款令牌
     */
    @DEField(name = "payment_token_id")
    @JSONField(name = "payment_token_id")
    @JsonProperty("payment_token_id")
    private Integer paymentTokenId;

    /**
     * 客户
     */
    @DEField(name = "partner_id")
    @JSONField(name = "partner_id")
    @JsonProperty("partner_id")
    private Integer partnerId;

    /**
     * 创建人
     */
    @DEField(name = "create_uid" , preType = DEPredefinedFieldType.CREATEMAN)
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    private Integer createUid;

    /**
     * 付款
     */
    @DEField(name = "payment_id")
    @JSONField(name = "payment_id")
    @JsonProperty("payment_id")
    private Integer paymentId;

    /**
     * 收单方
     */
    @DEField(name = "acquirer_id")
    @JSONField(name = "acquirer_id")
    @JsonProperty("acquirer_id")
    private Integer acquirerId;


    /**
     * 
     */
    @JSONField(name = "odoopayment")
    @JsonProperty("odoopayment")
    private cn.ibizlab.odoo.core.odoo_account.domain.Account_payment odooPayment;

    /**
     * 
     */
    @JSONField(name = "odooacquirer")
    @JsonProperty("odooacquirer")
    private cn.ibizlab.odoo.core.odoo_payment.domain.Payment_acquirer odooAcquirer;

    /**
     * 
     */
    @JSONField(name = "odoopaymenttoken")
    @JsonProperty("odoopaymenttoken")
    private cn.ibizlab.odoo.core.odoo_payment.domain.Payment_token odooPaymentToken;

    /**
     * 
     */
    @JSONField(name = "odoopartnercountry")
    @JsonProperty("odoopartnercountry")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_country odooPartnerCountry;

    /**
     * 
     */
    @JSONField(name = "odoocurrency")
    @JsonProperty("odoocurrency")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_currency odooCurrency;

    /**
     * 
     */
    @JSONField(name = "odoopartner")
    @JsonProperty("odoopartner")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_partner odooPartner;

    /**
     * 
     */
    @JSONField(name = "odoocreate")
    @JsonProperty("odoocreate")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_users odooCreate;

    /**
     * 
     */
    @JSONField(name = "odoowrite")
    @JsonProperty("odoowrite")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_users odooWrite;




    /**
     * 设置 [金额]
     */
    public void setAmount(Double amount){
        this.amount = amount ;
        this.modify("amount",amount);
    }
    /**
     * 设置 [电话]
     */
    public void setPartnerPhone(String partnerPhone){
        this.partnerPhone = partnerPhone ;
        this.modify("partner_phone",partnerPhone);
    }
    /**
     * 设置 [EMail]
     */
    public void setPartnerEmail(String partnerEmail){
        this.partnerEmail = partnerEmail ;
        this.modify("partner_email",partnerEmail);
    }
    /**
     * 设置 [回调方法]
     */
    public void setCallbackMethod(String callbackMethod){
        this.callbackMethod = callbackMethod ;
        this.modify("callback_method",callbackMethod);
    }
    /**
     * 设置 [付款后返回网址]
     */
    public void setReturnUrl(String returnUrl){
        this.returnUrl = returnUrl ;
        this.modify("return_url",returnUrl);
    }
    /**
     * 设置 [收单方参考]
     */
    public void setAcquirerReference(String acquirerReference){
        this.acquirerReference = acquirerReference ;
        this.modify("acquirer_reference",acquirerReference);
    }
    /**
     * 设置 [语言]
     */
    public void setPartnerLang(String partnerLang){
        this.partnerLang = partnerLang ;
        this.modify("partner_lang",partnerLang);
    }
    /**
     * 设置 [邮政编码]
     */
    public void setPartnerZip(String partnerZip){
        this.partnerZip = partnerZip ;
        this.modify("partner_zip",partnerZip);
    }
    /**
     * 设置 [状态]
     */
    public void setState(String state){
        this.state = state ;
        this.modify("state",state);
    }
    /**
     * 设置 [验证日期]
     */
    public void setDate(Timestamp date){
        this.date = date ;
        this.modify("date",date);
    }
    /**
     * 设置 [回调文档模型]
     */
    public void setCallbackModelId(Integer callbackModelId){
        this.callbackModelId = callbackModelId ;
        this.modify("callback_model_id",callbackModelId);
    }
    /**
     * 设置 [费用]
     */
    public void setFees(Double fees){
        this.fees = fees ;
        this.modify("fees",fees);
    }
    /**
     * 设置 [消息]
     */
    public void setStateMessage(String stateMessage){
        this.stateMessage = stateMessage ;
        this.modify("state_message",stateMessage);
    }
    /**
     * 设置 [回调文档 ID]
     */
    public void setCallbackResId(Integer callbackResId){
        this.callbackResId = callbackResId ;
        this.modify("callback_res_id",callbackResId);
    }
    /**
     * 设置 [城市]
     */
    public void setPartnerCity(String partnerCity){
        this.partnerCity = partnerCity ;
        this.modify("partner_city",partnerCity);
    }
    /**
     * 设置 [类型]
     */
    public void setType(String type){
        this.type = type ;
        this.modify("type",type);
    }
    /**
     * 设置 [3D Secure HTML]
     */
    public void setHtml3ds(String html3ds){
        this.html3ds = html3ds ;
        this.modify("html_3ds",html3ds);
    }
    /**
     * 设置 [合作伙伴名称]
     */
    public void setPartnerName(String partnerName){
        this.partnerName = partnerName ;
        this.modify("partner_name",partnerName);
    }
    /**
     * 设置 [参考]
     */
    public void setReference(String reference){
        this.reference = reference ;
        this.modify("reference",reference);
    }
    /**
     * 设置 [地址]
     */
    public void setPartnerAddress(String partnerAddress){
        this.partnerAddress = partnerAddress ;
        this.modify("partner_address",partnerAddress);
    }
    /**
     * 设置 [付款是否已过账处理]
     */
    public void setIsProcessed(String isProcessed){
        this.isProcessed = isProcessed ;
        this.modify("is_processed",isProcessed);
    }
    /**
     * 设置 [回调哈希函数]
     */
    public void setCallbackHash(String callbackHash){
        this.callbackHash = callbackHash ;
        this.modify("callback_hash",callbackHash);
    }
    /**
     * 设置 [币种]
     */
    public void setCurrencyId(Integer currencyId){
        this.currencyId = currencyId ;
        this.modify("currency_id",currencyId);
    }
    /**
     * 设置 [国家]
     */
    public void setPartnerCountryId(Integer partnerCountryId){
        this.partnerCountryId = partnerCountryId ;
        this.modify("partner_country_id",partnerCountryId);
    }
    /**
     * 设置 [付款令牌]
     */
    public void setPaymentTokenId(Integer paymentTokenId){
        this.paymentTokenId = paymentTokenId ;
        this.modify("payment_token_id",paymentTokenId);
    }
    /**
     * 设置 [客户]
     */
    public void setPartnerId(Integer partnerId){
        this.partnerId = partnerId ;
        this.modify("partner_id",partnerId);
    }
    /**
     * 设置 [付款]
     */
    public void setPaymentId(Integer paymentId){
        this.paymentId = paymentId ;
        this.modify("payment_id",paymentId);
    }
    /**
     * 设置 [收单方]
     */
    public void setAcquirerId(Integer acquirerId){
        this.acquirerId = acquirerId ;
        this.modify("acquirer_id",acquirerId);
    }

}


