package cn.ibizlab.odoo.core.odoo_web_tour.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_web_tour.domain.Web_tour_tour;
import cn.ibizlab.odoo.core.odoo_web_tour.filter.Web_tour_tourSearchContext;
import cn.ibizlab.odoo.core.odoo_web_tour.service.IWeb_tour_tourService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;



import cn.ibizlab.odoo.core.odoo_web_tour.client.web_tour_tourOdooClient;
import cn.ibizlab.odoo.core.odoo_web_tour.clientmodel.web_tour_tourClientModel;

/**
 * 实体[向导] 服务对象接口实现
 */
@Slf4j
@Service
public class Web_tour_tourServiceImpl implements IWeb_tour_tourService {

    @Autowired
    web_tour_tourOdooClient web_tour_tourOdooClient;


    @Override
    public boolean update(Web_tour_tour et) {
        web_tour_tourClientModel clientModel = convert2Model(et,null);
		web_tour_tourOdooClient.update(clientModel);
        Web_tour_tour rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }
    
    public void updateBatch(List<Web_tour_tour> list){
    }

    @Override
    public boolean remove(Integer id) {
        web_tour_tourClientModel clientModel = new web_tour_tourClientModel();
        clientModel.setId(id);
		web_tour_tourOdooClient.remove(clientModel);
        return true; 
    }

    public void removeBatch(Collection<Integer> idList){
    }

    @Override
    public Web_tour_tour get(Integer id) {
        web_tour_tourClientModel clientModel = new web_tour_tourClientModel();
        clientModel.setId(id);
		web_tour_tourOdooClient.get(clientModel);
        Web_tour_tour et = convert2Domain(clientModel,null);
        if(et==null){
            et=new Web_tour_tour();
            et.setId(id);
        }
        else{
        }

        return  et;
    }

    @Override
    public boolean create(Web_tour_tour et) {
        web_tour_tourClientModel clientModel = convert2Model(et,null);
		web_tour_tourOdooClient.create(clientModel);
        Web_tour_tour rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Web_tour_tour> list){
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Web_tour_tour> searchDefault(Web_tour_tourSearchContext context) {
        List<Web_tour_tour> list = new ArrayList<Web_tour_tour>();
        Page<web_tour_tourClientModel> clientModelList = web_tour_tourOdooClient.search(context);
        for(web_tour_tourClientModel clientModel : clientModelList){
            list.add( this.convert2Domain(clientModel,null));
        }
        return new PageImpl<Web_tour_tour>(list, context.getPageable(), clientModelList.getTotalElements());
    }

    public web_tour_tourClientModel convert2Model(Web_tour_tour domain , web_tour_tourClientModel model) {
        if(domain == null)
            return model ;
        if(model==null){
            model = new web_tour_tourClientModel();

        }
        if((Boolean) domain.getExtensionparams().get("dirtyflagenable")){
            if((Boolean) domain.getExtensionparams().get("display_namedirtyflag"))
                model.setDisplay_name(domain.getDisplayName());
            if((Boolean) domain.getExtensionparams().get("namedirtyflag"))
                model.setName(domain.getName());
            if((Boolean) domain.getExtensionparams().get("iddirtyflag"))
                model.setId(domain.getId());
            if((Boolean) domain.getExtensionparams().get("__last_updatedirtyflag"))
                model.set__last_update(domain.getLastUpdate());
            if((Boolean) domain.getExtensionparams().get("user_id_textdirtyflag"))
                model.setUser_id_text(domain.getUserIdText());
            if((Boolean) domain.getExtensionparams().get("user_iddirtyflag"))
                model.setUser_id(domain.getUserId());
            domain.getFocusNull().clear();
        }
        return model ;
    }

    public Web_tour_tour convert2Domain( web_tour_tourClientModel model ,Web_tour_tour domain) {
        if(model == null)
            return domain ;
        if(domain == null){
            domain = new Web_tour_tour();
        }

        if(model.getDisplay_nameDirtyFlag())
            domain.setDisplayName(model.getDisplay_name());
        if(model.getNameDirtyFlag())
            domain.setName(model.getName());
        if(model.getIdDirtyFlag())
            domain.setId(model.getId());
        if(model.get__last_updateDirtyFlag())
            domain.setLastUpdate(model.get__last_update());
        if(model.getUser_id_textDirtyFlag())
            domain.setUserIdText(model.getUser_id_text());
        if(model.getUser_idDirtyFlag())
            domain.setUserId(model.getUser_id());
        return domain ;
    }

}

    



