package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Stock_inventory_line;
import cn.ibizlab.odoo.core.odoo_stock.filter.Stock_inventory_lineSearchContext;

/**
 * 实体 [库存明细] 存储对象
 */
public interface Stock_inventory_lineRepository extends Repository<Stock_inventory_line> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Stock_inventory_line> searchDefault(Stock_inventory_lineSearchContext context);

    Stock_inventory_line convert2PO(cn.ibizlab.odoo.core.odoo_stock.domain.Stock_inventory_line domain , Stock_inventory_line po) ;

    cn.ibizlab.odoo.core.odoo_stock.domain.Stock_inventory_line convert2Domain( Stock_inventory_line po ,cn.ibizlab.odoo.core.odoo_stock.domain.Stock_inventory_line domain) ;

}
