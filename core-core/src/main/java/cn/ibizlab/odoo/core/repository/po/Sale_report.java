package cn.ibizlab.odoo.core.repository.po ;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.math.BigInteger;

import cn.ibizlab.odoo.core.odoo_sale.filter.Sale_reportSearchContext;

/**
 * 实体 [销售分析报告] 存储模型
 */
public interface Sale_report{

    /**
     * 不含税总计
     */
    Double getPrice_subtotal();

    void setPrice_subtotal(Double price_subtotal);

    /**
     * 获取 [不含税总计]脏标记
     */
    boolean getPrice_subtotalDirtyFlag();

    /**
     * ID
     */
    Integer getId();

    void setId(Integer id);

    /**
     * 获取 [ID]脏标记
     */
    boolean getIdDirtyFlag();

    /**
     * 折扣 %
     */
    Double getDiscount();

    void setDiscount(Double discount);

    /**
     * 获取 [折扣 %]脏标记
     */
    boolean getDiscountDirtyFlag();

    /**
     * 折扣金额
     */
    Double getDiscount_amount();

    void setDiscount_amount(Double discount_amount);

    /**
     * 获取 [折扣金额]脏标记
     */
    boolean getDiscount_amountDirtyFlag();

    /**
     * 体积
     */
    Double getVolume();

    void setVolume(Double volume);

    /**
     * 获取 [体积]脏标记
     */
    boolean getVolumeDirtyFlag();

    /**
     * 总计
     */
    Double getPrice_total();

    void setPrice_total(Double price_total);

    /**
     * 获取 [总计]脏标记
     */
    boolean getPrice_totalDirtyFlag();

    /**
     * 确认日期
     */
    Timestamp getConfirmation_date();

    void setConfirmation_date(Timestamp confirmation_date);

    /**
     * 获取 [确认日期]脏标记
     */
    boolean getConfirmation_dateDirtyFlag();

    /**
     * 单据日期
     */
    Timestamp getDate();

    void setDate(Timestamp date);

    /**
     * 获取 [单据日期]脏标记
     */
    boolean getDateDirtyFlag();

    /**
     * 最后修改日
     */
    Timestamp get__last_update();

    void set__last_update(Timestamp __last_update);

    /**
     * 获取 [最后修改日]脏标记
     */
    boolean get__last_updateDirtyFlag();

    /**
     * 订单关联
     */
    String getName();

    void setName(String name);

    /**
     * 获取 [订单关联]脏标记
     */
    boolean getNameDirtyFlag();

    /**
     * 显示名称
     */
    String getDisplay_name();

    void setDisplay_name(String display_name);

    /**
     * 获取 [显示名称]脏标记
     */
    boolean getDisplay_nameDirtyFlag();

    /**
     * 状态
     */
    String getState();

    void setState(String state);

    /**
     * 获取 [状态]脏标记
     */
    boolean getStateDirtyFlag();

    /**
     * 待开票数量
     */
    Double getQty_to_invoice();

    void setQty_to_invoice(Double qty_to_invoice);

    /**
     * 获取 [待开票数量]脏标记
     */
    boolean getQty_to_invoiceDirtyFlag();

    /**
     * # 明细行
     */
    Integer getNbr();

    void setNbr(Integer nbr);

    /**
     * 获取 [# 明细行]脏标记
     */
    boolean getNbrDirtyFlag();

    /**
     * 毛重
     */
    Double getWeight();

    void setWeight(Double weight);

    /**
     * 获取 [毛重]脏标记
     */
    boolean getWeightDirtyFlag();

    /**
     * 订购数量
     */
    Double getProduct_uom_qty();

    void setProduct_uom_qty(Double product_uom_qty);

    /**
     * 获取 [订购数量]脏标记
     */
    boolean getProduct_uom_qtyDirtyFlag();

    /**
     * 不含税待开票金额
     */
    Double getUntaxed_amount_to_invoice();

    void setUntaxed_amount_to_invoice(Double untaxed_amount_to_invoice);

    /**
     * 获取 [不含税待开票金额]脏标记
     */
    boolean getUntaxed_amount_to_invoiceDirtyFlag();

    /**
     * 不含税已开票金额
     */
    Double getUntaxed_amount_invoiced();

    void setUntaxed_amount_invoiced(Double untaxed_amount_invoiced);

    /**
     * 获取 [不含税已开票金额]脏标记
     */
    boolean getUntaxed_amount_invoicedDirtyFlag();

    /**
     * 网站
     */
    Integer getWebsite_id();

    void setWebsite_id(Integer website_id);

    /**
     * 获取 [网站]脏标记
     */
    boolean getWebsite_idDirtyFlag();

    /**
     * 已送货数量
     */
    Double getQty_delivered();

    void setQty_delivered(Double qty_delivered);

    /**
     * 获取 [已送货数量]脏标记
     */
    boolean getQty_deliveredDirtyFlag();

    /**
     * 已开票数量
     */
    Double getQty_invoiced();

    void setQty_invoiced(Double qty_invoiced);

    /**
     * 获取 [已开票数量]脏标记
     */
    boolean getQty_invoicedDirtyFlag();

    /**
     * 产品
     */
    String getProduct_tmpl_id_text();

    void setProduct_tmpl_id_text(String product_tmpl_id_text);

    /**
     * 获取 [产品]脏标记
     */
    boolean getProduct_tmpl_id_textDirtyFlag();

    /**
     * 仓库
     */
    String getWarehouse_id_text();

    void setWarehouse_id_text(String warehouse_id_text);

    /**
     * 获取 [仓库]脏标记
     */
    boolean getWarehouse_id_textDirtyFlag();

    /**
     * 分析账户
     */
    String getAnalytic_account_id_text();

    void setAnalytic_account_id_text(String analytic_account_id_text);

    /**
     * 获取 [分析账户]脏标记
     */
    boolean getAnalytic_account_id_textDirtyFlag();

    /**
     * 产品变体
     */
    String getProduct_id_text();

    void setProduct_id_text(String product_id_text);

    /**
     * 获取 [产品变体]脏标记
     */
    boolean getProduct_id_textDirtyFlag();

    /**
     * 客户国家
     */
    String getCountry_id_text();

    void setCountry_id_text(String country_id_text);

    /**
     * 获取 [客户国家]脏标记
     */
    boolean getCountry_id_textDirtyFlag();

    /**
     * 产品种类
     */
    String getCateg_id_text();

    void setCateg_id_text(String categ_id_text);

    /**
     * 获取 [产品种类]脏标记
     */
    boolean getCateg_id_textDirtyFlag();

    /**
     * 来源
     */
    String getSource_id_text();

    void setSource_id_text(String source_id_text);

    /**
     * 获取 [来源]脏标记
     */
    boolean getSource_id_textDirtyFlag();

    /**
     * 营销
     */
    String getCampaign_id_text();

    void setCampaign_id_text(String campaign_id_text);

    /**
     * 获取 [营销]脏标记
     */
    boolean getCampaign_id_textDirtyFlag();

    /**
     * 订单 #
     */
    String getOrder_id_text();

    void setOrder_id_text(String order_id_text);

    /**
     * 获取 [订单 #]脏标记
     */
    boolean getOrder_id_textDirtyFlag();

    /**
     * 销售员
     */
    String getUser_id_text();

    void setUser_id_text(String user_id_text);

    /**
     * 获取 [销售员]脏标记
     */
    boolean getUser_id_textDirtyFlag();

    /**
     * 客户实体
     */
    String getCommercial_partner_id_text();

    void setCommercial_partner_id_text(String commercial_partner_id_text);

    /**
     * 获取 [客户实体]脏标记
     */
    boolean getCommercial_partner_id_textDirtyFlag();

    /**
     * 媒体
     */
    String getMedium_id_text();

    void setMedium_id_text(String medium_id_text);

    /**
     * 获取 [媒体]脏标记
     */
    boolean getMedium_id_textDirtyFlag();

    /**
     * 计量单位
     */
    String getProduct_uom_text();

    void setProduct_uom_text(String product_uom_text);

    /**
     * 获取 [计量单位]脏标记
     */
    boolean getProduct_uom_textDirtyFlag();

    /**
     * 公司
     */
    String getCompany_id_text();

    void setCompany_id_text(String company_id_text);

    /**
     * 获取 [公司]脏标记
     */
    boolean getCompany_id_textDirtyFlag();

    /**
     * 销售团队
     */
    String getTeam_id_text();

    void setTeam_id_text(String team_id_text);

    /**
     * 获取 [销售团队]脏标记
     */
    boolean getTeam_id_textDirtyFlag();

    /**
     * 价格表
     */
    String getPricelist_id_text();

    void setPricelist_id_text(String pricelist_id_text);

    /**
     * 获取 [价格表]脏标记
     */
    boolean getPricelist_id_textDirtyFlag();

    /**
     * 客户
     */
    String getPartner_id_text();

    void setPartner_id_text(String partner_id_text);

    /**
     * 获取 [客户]脏标记
     */
    boolean getPartner_id_textDirtyFlag();

    /**
     * 销售团队
     */
    Integer getTeam_id();

    void setTeam_id(Integer team_id);

    /**
     * 获取 [销售团队]脏标记
     */
    boolean getTeam_idDirtyFlag();

    /**
     * 客户实体
     */
    Integer getCommercial_partner_id();

    void setCommercial_partner_id(Integer commercial_partner_id);

    /**
     * 获取 [客户实体]脏标记
     */
    boolean getCommercial_partner_idDirtyFlag();

    /**
     * 销售员
     */
    Integer getUser_id();

    void setUser_id(Integer user_id);

    /**
     * 获取 [销售员]脏标记
     */
    boolean getUser_idDirtyFlag();

    /**
     * 产品
     */
    Integer getProduct_tmpl_id();

    void setProduct_tmpl_id(Integer product_tmpl_id);

    /**
     * 获取 [产品]脏标记
     */
    boolean getProduct_tmpl_idDirtyFlag();

    /**
     * 分析账户
     */
    Integer getAnalytic_account_id();

    void setAnalytic_account_id(Integer analytic_account_id);

    /**
     * 获取 [分析账户]脏标记
     */
    boolean getAnalytic_account_idDirtyFlag();

    /**
     * 公司
     */
    Integer getCompany_id();

    void setCompany_id(Integer company_id);

    /**
     * 获取 [公司]脏标记
     */
    boolean getCompany_idDirtyFlag();

    /**
     * 媒体
     */
    Integer getMedium_id();

    void setMedium_id(Integer medium_id);

    /**
     * 获取 [媒体]脏标记
     */
    boolean getMedium_idDirtyFlag();

    /**
     * 订单 #
     */
    Integer getOrder_id();

    void setOrder_id(Integer order_id);

    /**
     * 获取 [订单 #]脏标记
     */
    boolean getOrder_idDirtyFlag();

    /**
     * 客户
     */
    Integer getPartner_id();

    void setPartner_id(Integer partner_id);

    /**
     * 获取 [客户]脏标记
     */
    boolean getPartner_idDirtyFlag();

    /**
     * 来源
     */
    Integer getSource_id();

    void setSource_id(Integer source_id);

    /**
     * 获取 [来源]脏标记
     */
    boolean getSource_idDirtyFlag();

    /**
     * 营销
     */
    Integer getCampaign_id();

    void setCampaign_id(Integer campaign_id);

    /**
     * 获取 [营销]脏标记
     */
    boolean getCampaign_idDirtyFlag();

    /**
     * 产品种类
     */
    Integer getCateg_id();

    void setCateg_id(Integer categ_id);

    /**
     * 获取 [产品种类]脏标记
     */
    boolean getCateg_idDirtyFlag();

    /**
     * 仓库
     */
    Integer getWarehouse_id();

    void setWarehouse_id(Integer warehouse_id);

    /**
     * 获取 [仓库]脏标记
     */
    boolean getWarehouse_idDirtyFlag();

    /**
     * 产品变体
     */
    Integer getProduct_id();

    void setProduct_id(Integer product_id);

    /**
     * 获取 [产品变体]脏标记
     */
    boolean getProduct_idDirtyFlag();

    /**
     * 价格表
     */
    Integer getPricelist_id();

    void setPricelist_id(Integer pricelist_id);

    /**
     * 获取 [价格表]脏标记
     */
    boolean getPricelist_idDirtyFlag();

    /**
     * 计量单位
     */
    Integer getProduct_uom();

    void setProduct_uom(Integer product_uom);

    /**
     * 获取 [计量单位]脏标记
     */
    boolean getProduct_uomDirtyFlag();

    /**
     * 客户国家
     */
    Integer getCountry_id();

    void setCountry_id(Integer country_id);

    /**
     * 获取 [客户国家]脏标记
     */
    boolean getCountry_idDirtyFlag();

}
