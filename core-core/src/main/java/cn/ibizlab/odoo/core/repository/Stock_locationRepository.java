package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Stock_location;
import cn.ibizlab.odoo.core.odoo_stock.filter.Stock_locationSearchContext;

/**
 * 实体 [库存位置] 存储对象
 */
public interface Stock_locationRepository extends Repository<Stock_location> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Stock_location> searchDefault(Stock_locationSearchContext context);

    Stock_location convert2PO(cn.ibizlab.odoo.core.odoo_stock.domain.Stock_location domain , Stock_location po) ;

    cn.ibizlab.odoo.core.odoo_stock.domain.Stock_location convert2Domain( Stock_location po ,cn.ibizlab.odoo.core.odoo_stock.domain.Stock_location domain) ;

}
