package cn.ibizlab.odoo.core.repository.po ;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.math.BigInteger;

import cn.ibizlab.odoo.core.odoo_base.filter.Base_language_exportSearchContext;

/**
 * 实体 [语言输出] 存储模型
 */
public interface Base_language_export{

    /**
     * 创建时间
     */
    Timestamp getCreate_date();

    void setCreate_date(Timestamp create_date);

    /**
     * 获取 [创建时间]脏标记
     */
    boolean getCreate_dateDirtyFlag();

    /**
     * 语言
     */
    String getLang();

    void setLang(String lang);

    /**
     * 获取 [语言]脏标记
     */
    boolean getLangDirtyFlag();

    /**
     * 最后更新时间
     */
    Timestamp getWrite_date();

    void setWrite_date(Timestamp write_date);

    /**
     * 获取 [最后更新时间]脏标记
     */
    boolean getWrite_dateDirtyFlag();

    /**
     * 文件格式
     */
    String getFormat();

    void setFormat(String format);

    /**
     * 获取 [文件格式]脏标记
     */
    boolean getFormatDirtyFlag();

    /**
     * 显示名称
     */
    String getDisplay_name();

    void setDisplay_name(String display_name);

    /**
     * 获取 [显示名称]脏标记
     */
    boolean getDisplay_nameDirtyFlag();

    /**
     * 文件
     */
    byte[] getData();

    void setData(byte[] data);

    /**
     * 获取 [文件]脏标记
     */
    boolean getDataDirtyFlag();

    /**
     * 要导出的应用
     */
    String getModules();

    void setModules(String modules);

    /**
     * 获取 [要导出的应用]脏标记
     */
    boolean getModulesDirtyFlag();

    /**
     * ID
     */
    Integer getId();

    void setId(Integer id);

    /**
     * 获取 [ID]脏标记
     */
    boolean getIdDirtyFlag();

    /**
     * 最后修改日
     */
    Timestamp get__last_update();

    void set__last_update(Timestamp __last_update);

    /**
     * 获取 [最后修改日]脏标记
     */
    boolean get__last_updateDirtyFlag();

    /**
     * 文件名称
     */
    String getName();

    void setName(String name);

    /**
     * 获取 [文件名称]脏标记
     */
    boolean getNameDirtyFlag();

    /**
     * 省/ 州
     */
    String getState();

    void setState(String state);

    /**
     * 获取 [省/ 州]脏标记
     */
    boolean getStateDirtyFlag();

    /**
     * 最后更新者
     */
    String getWrite_uid_text();

    void setWrite_uid_text(String write_uid_text);

    /**
     * 获取 [最后更新者]脏标记
     */
    boolean getWrite_uid_textDirtyFlag();

    /**
     * 创建人
     */
    String getCreate_uid_text();

    void setCreate_uid_text(String create_uid_text);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uid_textDirtyFlag();

    /**
     * 最后更新者
     */
    Integer getWrite_uid();

    void setWrite_uid(Integer write_uid);

    /**
     * 获取 [最后更新者]脏标记
     */
    boolean getWrite_uidDirtyFlag();

    /**
     * 创建人
     */
    Integer getCreate_uid();

    void setCreate_uid(Integer create_uid);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uidDirtyFlag();

}
