package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Iaccount_move_line;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[account_move_line] 服务对象接口
 */
public interface Iaccount_move_lineClientService{

    public Iaccount_move_line createModel() ;

    public Page<Iaccount_move_line> search(SearchContext context);

    public void update(Iaccount_move_line account_move_line);

    public void createBatch(List<Iaccount_move_line> account_move_lines);

    public void create(Iaccount_move_line account_move_line);

    public void remove(Iaccount_move_line account_move_line);

    public void get(Iaccount_move_line account_move_line);

    public void updateBatch(List<Iaccount_move_line> account_move_lines);

    public void removeBatch(List<Iaccount_move_line> account_move_lines);

    public Page<Iaccount_move_line> select(SearchContext context);

}
