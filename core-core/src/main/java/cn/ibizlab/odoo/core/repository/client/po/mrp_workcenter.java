package cn.ibizlab.odoo.core.repository.client.po;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigDecimal;

/**
 * [mrp_workcenter] 对象
 */
public interface mrp_workcenter {

    public String getActive();

    public void setActive(String active);

    public Double getBlocked_time();

    public void setBlocked_time(Double blocked_time);

    public Double getCapacity();

    public void setCapacity(Double capacity);

    public String getCode();

    public void setCode(String code);

    public Integer getColor();

    public void setColor(Integer color);

    public Integer getCompany_id();

    public void setCompany_id(Integer company_id);

    public String getCompany_id_text();

    public void setCompany_id_text(String company_id_text);

    public Double getCosts_hour();

    public void setCosts_hour(Double costs_hour);

    public Timestamp getCreate_date();

    public void setCreate_date(Timestamp create_date);

    public Integer getCreate_uid();

    public void setCreate_uid(Integer create_uid);

    public String getCreate_uid_text();

    public void setCreate_uid_text(String create_uid_text);

    public String getDisplay_name();

    public void setDisplay_name(String display_name);

    public Integer getId();

    public void setId(Integer id);

    public String getName();

    public void setName(String name);

    public String getNote();

    public void setNote(String note);

    public Double getOee();

    public void setOee(Double oee);

    public Double getOee_target();

    public void setOee_target(Double oee_target);

    public String getOrder_ids();

    public void setOrder_ids(String order_ids);

    public Integer getPerformance();

    public void setPerformance(Integer performance);

    public Double getProductive_time();

    public void setProductive_time(Double productive_time);

    public Integer getResource_calendar_id();

    public void setResource_calendar_id(Integer resource_calendar_id);

    public String getResource_calendar_id_text();

    public void setResource_calendar_id_text(String resource_calendar_id_text);

    public Integer getResource_id();

    public void setResource_id(Integer resource_id);

    public String getResource_id_text();

    public void setResource_id_text(String resource_id_text);

    public String getRouting_line_ids();

    public void setRouting_line_ids(String routing_line_ids);

    public Integer getSequence();

    public void setSequence(Integer sequence);

    public Double getTime_efficiency();

    public void setTime_efficiency(Double time_efficiency);

    public String getTime_ids();

    public void setTime_ids(String time_ids);

    public Double getTime_start();

    public void setTime_start(Double time_start);

    public Double getTime_stop();

    public void setTime_stop(Double time_stop);

    public String getTz();

    public void setTz(String tz);

    public Double getWorkcenter_load();

    public void setWorkcenter_load(Double workcenter_load);

    public String getWorking_state();

    public void setWorking_state(String working_state);

    public Integer getWorkorder_count();

    public void setWorkorder_count(Integer workorder_count);

    public Integer getWorkorder_late_count();

    public void setWorkorder_late_count(Integer workorder_late_count);

    public Integer getWorkorder_pending_count();

    public void setWorkorder_pending_count(Integer workorder_pending_count);

    public Integer getWorkorder_progress_count();

    public void setWorkorder_progress_count(Integer workorder_progress_count);

    public Integer getWorkorder_ready_count();

    public void setWorkorder_ready_count(Integer workorder_ready_count);

    public Timestamp getWrite_date();

    public void setWrite_date(Timestamp write_date);

    public Integer getWrite_uid();

    public void setWrite_uid(Integer write_uid);

    public String getWrite_uid_text();

    public void setWrite_uid_text(String write_uid_text);

    public Timestamp get__last_update();

    public void set__last_update(Timestamp __last_update);

}
