package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Account_chart_template;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_chart_templateSearchContext;

/**
 * 实体 [科目表模版] 存储对象
 */
public interface Account_chart_templateRepository extends Repository<Account_chart_template> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Account_chart_template> searchDefault(Account_chart_templateSearchContext context);

    Account_chart_template convert2PO(cn.ibizlab.odoo.core.odoo_account.domain.Account_chart_template domain , Account_chart_template po) ;

    cn.ibizlab.odoo.core.odoo_account.domain.Account_chart_template convert2Domain( Account_chart_template po ,cn.ibizlab.odoo.core.odoo_account.domain.Account_chart_template domain) ;

}
