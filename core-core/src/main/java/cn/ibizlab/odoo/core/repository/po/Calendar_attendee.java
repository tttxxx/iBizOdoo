package cn.ibizlab.odoo.core.repository.po ;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.math.BigInteger;

import cn.ibizlab.odoo.core.odoo_calendar.filter.Calendar_attendeeSearchContext;

/**
 * 实体 [日历出席者信息] 存储模型
 */
public interface Calendar_attendee{

    /**
     * 显示名称
     */
    String getDisplay_name();

    void setDisplay_name(String display_name);

    /**
     * 获取 [显示名称]脏标记
     */
    boolean getDisplay_nameDirtyFlag();

    /**
     * 创建时间
     */
    Timestamp getCreate_date();

    void setCreate_date(Timestamp create_date);

    /**
     * 获取 [创建时间]脏标记
     */
    boolean getCreate_dateDirtyFlag();

    /**
     * 最后修改日
     */
    Timestamp get__last_update();

    void set__last_update(Timestamp __last_update);

    /**
     * 获取 [最后修改日]脏标记
     */
    boolean get__last_updateDirtyFlag();

    /**
     * 最后更新时间
     */
    Timestamp getWrite_date();

    void setWrite_date(Timestamp write_date);

    /**
     * 获取 [最后更新时间]脏标记
     */
    boolean getWrite_dateDirtyFlag();

    /**
     * 邀请令牌
     */
    String getAccess_token();

    void setAccess_token(String access_token);

    /**
     * 获取 [邀请令牌]脏标记
     */
    boolean getAccess_tokenDirtyFlag();

    /**
     * ID
     */
    Integer getId();

    void setId(Integer id);

    /**
     * 获取 [ID]脏标记
     */
    boolean getIdDirtyFlag();

    /**
     * 状态
     */
    String getState();

    void setState(String state);

    /**
     * 获取 [状态]脏标记
     */
    boolean getStateDirtyFlag();

    /**
     * 通用名称
     */
    String getCommon_name();

    void setCommon_name(String common_name);

    /**
     * 获取 [通用名称]脏标记
     */
    boolean getCommon_nameDirtyFlag();

    /**
     * 空闲/忙碌
     */
    String getAvailability();

    void setAvailability(String availability);

    /**
     * 获取 [空闲/忙碌]脏标记
     */
    boolean getAvailabilityDirtyFlag();

    /**
     * EMail
     */
    String getEmail();

    void setEmail(String email);

    /**
     * 获取 [EMail]脏标记
     */
    boolean getEmailDirtyFlag();

    /**
     * 创建人
     */
    String getCreate_uid_text();

    void setCreate_uid_text(String create_uid_text);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uid_textDirtyFlag();

    /**
     * 联系
     */
    String getPartner_id_text();

    void setPartner_id_text(String partner_id_text);

    /**
     * 获取 [联系]脏标记
     */
    boolean getPartner_id_textDirtyFlag();

    /**
     * 会议已连接
     */
    String getEvent_id_text();

    void setEvent_id_text(String event_id_text);

    /**
     * 获取 [会议已连接]脏标记
     */
    boolean getEvent_id_textDirtyFlag();

    /**
     * 最后更新人
     */
    String getWrite_uid_text();

    void setWrite_uid_text(String write_uid_text);

    /**
     * 获取 [最后更新人]脏标记
     */
    boolean getWrite_uid_textDirtyFlag();

    /**
     * 创建人
     */
    Integer getCreate_uid();

    void setCreate_uid(Integer create_uid);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uidDirtyFlag();

    /**
     * 最后更新人
     */
    Integer getWrite_uid();

    void setWrite_uid(Integer write_uid);

    /**
     * 获取 [最后更新人]脏标记
     */
    boolean getWrite_uidDirtyFlag();

    /**
     * 会议已连接
     */
    Integer getEvent_id();

    void setEvent_id(Integer event_id);

    /**
     * 获取 [会议已连接]脏标记
     */
    boolean getEvent_idDirtyFlag();

    /**
     * 联系
     */
    Integer getPartner_id();

    void setPartner_id(Integer partner_id);

    /**
     * 获取 [联系]脏标记
     */
    boolean getPartner_idDirtyFlag();

}
