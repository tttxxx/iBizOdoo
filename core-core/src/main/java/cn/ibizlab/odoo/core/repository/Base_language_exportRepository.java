package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Base_language_export;
import cn.ibizlab.odoo.core.odoo_base.filter.Base_language_exportSearchContext;

/**
 * 实体 [语言输出] 存储对象
 */
public interface Base_language_exportRepository extends Repository<Base_language_export> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Base_language_export> searchDefault(Base_language_exportSearchContext context);

    Base_language_export convert2PO(cn.ibizlab.odoo.core.odoo_base.domain.Base_language_export domain , Base_language_export po) ;

    cn.ibizlab.odoo.core.odoo_base.domain.Base_language_export convert2Domain( Base_language_export po ,cn.ibizlab.odoo.core.odoo_base.domain.Base_language_export domain) ;

}
