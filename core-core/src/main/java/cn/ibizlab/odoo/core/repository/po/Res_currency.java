package cn.ibizlab.odoo.core.repository.po ;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.math.BigInteger;

import cn.ibizlab.odoo.core.odoo_base.filter.Res_currencySearchContext;

/**
 * 实体 [币种] 存储模型
 */
public interface Res_currency{

    /**
     * 创建时间
     */
    Timestamp getCreate_date();

    void setCreate_date(Timestamp create_date);

    /**
     * 获取 [创建时间]脏标记
     */
    boolean getCreate_dateDirtyFlag();

    /**
     * 小数点位置
     */
    Integer getDecimal_places();

    void setDecimal_places(Integer decimal_places);

    /**
     * 获取 [小数点位置]脏标记
     */
    boolean getDecimal_placesDirtyFlag();

    /**
     * 币种
     */
    String getName();

    void setName(String name);

    /**
     * 获取 [币种]脏标记
     */
    boolean getNameDirtyFlag();

    /**
     * 货币单位
     */
    String getCurrency_unit_label();

    void setCurrency_unit_label(String currency_unit_label);

    /**
     * 获取 [货币单位]脏标记
     */
    boolean getCurrency_unit_labelDirtyFlag();

    /**
     * 舍入系数
     */
    Double getRounding();

    void setRounding(Double rounding);

    /**
     * 获取 [舍入系数]脏标记
     */
    boolean getRoundingDirtyFlag();

    /**
     * 最后修改日
     */
    Timestamp get__last_update();

    void set__last_update(Timestamp __last_update);

    /**
     * 获取 [最后修改日]脏标记
     */
    boolean get__last_updateDirtyFlag();

    /**
     * ID
     */
    Integer getId();

    void setId(Integer id);

    /**
     * 获取 [ID]脏标记
     */
    boolean getIdDirtyFlag();

    /**
     * 最后更新时间
     */
    Timestamp getWrite_date();

    void setWrite_date(Timestamp write_date);

    /**
     * 获取 [最后更新时间]脏标记
     */
    boolean getWrite_dateDirtyFlag();

    /**
     * 日期
     */
    Timestamp getDate();

    void setDate(Timestamp date);

    /**
     * 获取 [日期]脏标记
     */
    boolean getDateDirtyFlag();

    /**
     * 符号位置
     */
    String getPosition();

    void setPosition(String position);

    /**
     * 获取 [符号位置]脏标记
     */
    boolean getPositionDirtyFlag();

    /**
     * 显示名称
     */
    String getDisplay_name();

    void setDisplay_name(String display_name);

    /**
     * 获取 [显示名称]脏标记
     */
    boolean getDisplay_nameDirtyFlag();

    /**
     * 有效
     */
    String getActive();

    void setActive(String active);

    /**
     * 获取 [有效]脏标记
     */
    boolean getActiveDirtyFlag();

    /**
     * 当前汇率
     */
    Double getRate();

    void setRate(Double rate);

    /**
     * 获取 [当前汇率]脏标记
     */
    boolean getRateDirtyFlag();

    /**
     * 比率
     */
    String getRate_ids();

    void setRate_ids(String rate_ids);

    /**
     * 获取 [比率]脏标记
     */
    boolean getRate_idsDirtyFlag();

    /**
     * 货币符号
     */
    String getSymbol();

    void setSymbol(String symbol);

    /**
     * 获取 [货币符号]脏标记
     */
    boolean getSymbolDirtyFlag();

    /**
     * 货币子单位
     */
    String getCurrency_subunit_label();

    void setCurrency_subunit_label(String currency_subunit_label);

    /**
     * 获取 [货币子单位]脏标记
     */
    boolean getCurrency_subunit_labelDirtyFlag();

    /**
     * 最后更新者
     */
    String getWrite_uid_text();

    void setWrite_uid_text(String write_uid_text);

    /**
     * 获取 [最后更新者]脏标记
     */
    boolean getWrite_uid_textDirtyFlag();

    /**
     * 创建人
     */
    String getCreate_uid_text();

    void setCreate_uid_text(String create_uid_text);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uid_textDirtyFlag();

    /**
     * 最后更新者
     */
    Integer getWrite_uid();

    void setWrite_uid(Integer write_uid);

    /**
     * 获取 [最后更新者]脏标记
     */
    boolean getWrite_uidDirtyFlag();

    /**
     * 创建人
     */
    Integer getCreate_uid();

    void setCreate_uid(Integer create_uid);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uidDirtyFlag();

}
