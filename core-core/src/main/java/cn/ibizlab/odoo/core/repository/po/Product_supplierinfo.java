package cn.ibizlab.odoo.core.repository.po ;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.math.BigInteger;

import cn.ibizlab.odoo.core.odoo_product.filter.Product_supplierinfoSearchContext;

/**
 * 实体 [供应商价格表] 存储模型
 */
public interface Product_supplierinfo{

    /**
     * 供应商产品代码
     */
    String getProduct_code();

    void setProduct_code(String product_code);

    /**
     * 获取 [供应商产品代码]脏标记
     */
    boolean getProduct_codeDirtyFlag();

    /**
     * 结束日期
     */
    Timestamp getDate_end();

    void setDate_end(Timestamp date_end);

    /**
     * 获取 [结束日期]脏标记
     */
    boolean getDate_endDirtyFlag();

    /**
     * 开始日期
     */
    Timestamp getDate_start();

    void setDate_start(Timestamp date_start);

    /**
     * 获取 [开始日期]脏标记
     */
    boolean getDate_startDirtyFlag();

    /**
     * 最少数量
     */
    Double getMin_qty();

    void setMin_qty(Double min_qty);

    /**
     * 获取 [最少数量]脏标记
     */
    boolean getMin_qtyDirtyFlag();

    /**
     * 最后更新时间
     */
    Timestamp getWrite_date();

    void setWrite_date(Timestamp write_date);

    /**
     * 获取 [最后更新时间]脏标记
     */
    boolean getWrite_dateDirtyFlag();

    /**
     * ID
     */
    Integer getId();

    void setId(Integer id);

    /**
     * 获取 [ID]脏标记
     */
    boolean getIdDirtyFlag();

    /**
     * 供应商产品名称
     */
    String getProduct_name();

    void setProduct_name(String product_name);

    /**
     * 获取 [供应商产品名称]脏标记
     */
    boolean getProduct_nameDirtyFlag();

    /**
     * 序号
     */
    Integer getSequence();

    void setSequence(Integer sequence);

    /**
     * 获取 [序号]脏标记
     */
    boolean getSequenceDirtyFlag();

    /**
     * 交货提前时间
     */
    Integer getDelay();

    void setDelay(Integer delay);

    /**
     * 获取 [交货提前时间]脏标记
     */
    boolean getDelayDirtyFlag();

    /**
     * 显示名称
     */
    String getDisplay_name();

    void setDisplay_name(String display_name);

    /**
     * 获取 [显示名称]脏标记
     */
    boolean getDisplay_nameDirtyFlag();

    /**
     * 最后修改日
     */
    Timestamp get__last_update();

    void set__last_update(Timestamp __last_update);

    /**
     * 获取 [最后修改日]脏标记
     */
    boolean get__last_updateDirtyFlag();

    /**
     * 创建时间
     */
    Timestamp getCreate_date();

    void setCreate_date(Timestamp create_date);

    /**
     * 获取 [创建时间]脏标记
     */
    boolean getCreate_dateDirtyFlag();

    /**
     * 价格
     */
    Double getPrice();

    void setPrice(Double price);

    /**
     * 获取 [价格]脏标记
     */
    boolean getPriceDirtyFlag();

    /**
     * 币种
     */
    String getCurrency_id_text();

    void setCurrency_id_text(String currency_id_text);

    /**
     * 获取 [币种]脏标记
     */
    boolean getCurrency_id_textDirtyFlag();

    /**
     * 产品模板
     */
    String getProduct_tmpl_id_text();

    void setProduct_tmpl_id_text(String product_tmpl_id_text);

    /**
     * 获取 [产品模板]脏标记
     */
    boolean getProduct_tmpl_id_textDirtyFlag();

    /**
     * 公司
     */
    String getCompany_id_text();

    void setCompany_id_text(String company_id_text);

    /**
     * 获取 [公司]脏标记
     */
    boolean getCompany_id_textDirtyFlag();

    /**
     * 计量单位
     */
    Integer getProduct_uom();

    void setProduct_uom(Integer product_uom);

    /**
     * 获取 [计量单位]脏标记
     */
    boolean getProduct_uomDirtyFlag();

    /**
     * 变体计数
     */
    Integer getProduct_variant_count();

    void setProduct_variant_count(Integer product_variant_count);

    /**
     * 获取 [变体计数]脏标记
     */
    boolean getProduct_variant_countDirtyFlag();

    /**
     * 创建人
     */
    String getCreate_uid_text();

    void setCreate_uid_text(String create_uid_text);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uid_textDirtyFlag();

    /**
     * 产品变体
     */
    String getProduct_id_text();

    void setProduct_id_text(String product_id_text);

    /**
     * 获取 [产品变体]脏标记
     */
    boolean getProduct_id_textDirtyFlag();

    /**
     * 最后更新人
     */
    String getWrite_uid_text();

    void setWrite_uid_text(String write_uid_text);

    /**
     * 获取 [最后更新人]脏标记
     */
    boolean getWrite_uid_textDirtyFlag();

    /**
     * 供应商
     */
    String getName_text();

    void setName_text(String name_text);

    /**
     * 获取 [供应商]脏标记
     */
    boolean getName_textDirtyFlag();

    /**
     * 产品模板
     */
    Integer getProduct_tmpl_id();

    void setProduct_tmpl_id(Integer product_tmpl_id);

    /**
     * 获取 [产品模板]脏标记
     */
    boolean getProduct_tmpl_idDirtyFlag();

    /**
     * 公司
     */
    Integer getCompany_id();

    void setCompany_id(Integer company_id);

    /**
     * 获取 [公司]脏标记
     */
    boolean getCompany_idDirtyFlag();

    /**
     * 产品变体
     */
    Integer getProduct_id();

    void setProduct_id(Integer product_id);

    /**
     * 获取 [产品变体]脏标记
     */
    boolean getProduct_idDirtyFlag();

    /**
     * 创建人
     */
    Integer getCreate_uid();

    void setCreate_uid(Integer create_uid);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uidDirtyFlag();

    /**
     * 币种
     */
    Integer getCurrency_id();

    void setCurrency_id(Integer currency_id);

    /**
     * 获取 [币种]脏标记
     */
    boolean getCurrency_idDirtyFlag();

    /**
     * 供应商
     */
    Integer getName();

    void setName(Integer name);

    /**
     * 获取 [供应商]脏标记
     */
    boolean getNameDirtyFlag();

    /**
     * 最后更新人
     */
    Integer getWrite_uid();

    void setWrite_uid(Integer write_uid);

    /**
     * 获取 [最后更新人]脏标记
     */
    boolean getWrite_uidDirtyFlag();

}
