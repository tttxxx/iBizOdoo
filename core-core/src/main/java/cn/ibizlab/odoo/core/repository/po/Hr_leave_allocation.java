package cn.ibizlab.odoo.core.repository.po ;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.math.BigInteger;

import cn.ibizlab.odoo.core.odoo_hr.filter.Hr_leave_allocationSearchContext;

/**
 * 实体 [休假分配] 存储模型
 */
public interface Hr_leave_allocation{

    /**
     * 需要采取行动
     */
    String getMessage_needaction();

    void setMessage_needaction(String message_needaction);

    /**
     * 获取 [需要采取行动]脏标记
     */
    boolean getMessage_needactionDirtyFlag();

    /**
     * 说明
     */
    String getName();

    void setName(String name);

    /**
     * 获取 [说明]脏标记
     */
    boolean getNameDirtyFlag();

    /**
     * 结束日期
     */
    Timestamp getDate_to();

    void setDate_to(Timestamp date_to);

    /**
     * 获取 [结束日期]脏标记
     */
    boolean getDate_toDirtyFlag();

    /**
     * 附件
     */
    Integer getMessage_main_attachment_id();

    void setMessage_main_attachment_id(Integer message_main_attachment_id);

    /**
     * 获取 [附件]脏标记
     */
    boolean getMessage_main_attachment_idDirtyFlag();

    /**
     * 每个间隔的单位数
     */
    Double getNumber_per_interval();

    void setNumber_per_interval(Double number_per_interval);

    /**
     * 获取 [每个间隔的单位数]脏标记
     */
    boolean getNumber_per_intervalDirtyFlag();

    /**
     * 持续时间（天）
     */
    Double getNumber_of_days_display();

    void setNumber_of_days_display(Double number_of_days_display);

    /**
     * 获取 [持续时间（天）]脏标记
     */
    boolean getNumber_of_days_displayDirtyFlag();

    /**
     * 关注者
     */
    String getMessage_follower_ids();

    void setMessage_follower_ids(String message_follower_ids);

    /**
     * 获取 [关注者]脏标记
     */
    boolean getMessage_follower_idsDirtyFlag();

    /**
     * 最后修改日
     */
    Timestamp get__last_update();

    void set__last_update(Timestamp __last_update);

    /**
     * 获取 [最后修改日]脏标记
     */
    boolean get__last_updateDirtyFlag();

    /**
     * 分配模式
     */
    String getHoliday_type();

    void setHoliday_type(String holiday_type);

    /**
     * 获取 [分配模式]脏标记
     */
    boolean getHoliday_typeDirtyFlag();

    /**
     * 两个间隔之间的单位数
     */
    Integer getInterval_number();

    void setInterval_number(Integer interval_number);

    /**
     * 获取 [两个间隔之间的单位数]脏标记
     */
    boolean getInterval_numberDirtyFlag();

    /**
     * 附件数量
     */
    Integer getMessage_attachment_count();

    void setMessage_attachment_count(Integer message_attachment_count);

    /**
     * 获取 [附件数量]脏标记
     */
    boolean getMessage_attachment_countDirtyFlag();

    /**
     * 时长(小时)
     */
    Double getNumber_of_hours_display();

    void setNumber_of_hours_display(Double number_of_hours_display);

    /**
     * 获取 [时长(小时)]脏标记
     */
    boolean getNumber_of_hours_displayDirtyFlag();

    /**
     * 权责发生制
     */
    String getAccrual();

    void setAccrual(String accrual);

    /**
     * 获取 [权责发生制]脏标记
     */
    boolean getAccrualDirtyFlag();

    /**
     * 未读消息
     */
    String getMessage_unread();

    void setMessage_unread(String message_unread);

    /**
     * 获取 [未读消息]脏标记
     */
    boolean getMessage_unreadDirtyFlag();

    /**
     * 责任用户
     */
    Integer getActivity_user_id();

    void setActivity_user_id(Integer activity_user_id);

    /**
     * 获取 [责任用户]脏标记
     */
    boolean getActivity_user_idDirtyFlag();

    /**
     * 链接申请
     */
    String getLinked_request_ids();

    void setLinked_request_ids(String linked_request_ids);

    /**
     * 获取 [链接申请]脏标记
     */
    boolean getLinked_request_idsDirtyFlag();

    /**
     * 创建时间
     */
    Timestamp getCreate_date();

    void setCreate_date(Timestamp create_date);

    /**
     * 获取 [创建时间]脏标记
     */
    boolean getCreate_dateDirtyFlag();

    /**
     * 活动
     */
    String getActivity_ids();

    void setActivity_ids(String activity_ids);

    /**
     * 获取 [活动]脏标记
     */
    boolean getActivity_idsDirtyFlag();

    /**
     * 信息
     */
    String getMessage_ids();

    void setMessage_ids(String message_ids);

    /**
     * 获取 [信息]脏标记
     */
    boolean getMessage_idsDirtyFlag();

    /**
     * 下一活动类型
     */
    Integer getActivity_type_id();

    void setActivity_type_id(Integer activity_type_id);

    /**
     * 获取 [下一活动类型]脏标记
     */
    boolean getActivity_type_idDirtyFlag();

    /**
     * 下一次应计分配的日期
     */
    Timestamp getNextcall();

    void setNextcall(Timestamp nextcall);

    /**
     * 获取 [下一次应计分配的日期]脏标记
     */
    boolean getNextcallDirtyFlag();

    /**
     * 关注者(渠道)
     */
    String getMessage_channel_ids();

    void setMessage_channel_ids(String message_channel_ids);

    /**
     * 获取 [关注者(渠道)]脏标记
     */
    boolean getMessage_channel_idsDirtyFlag();

    /**
     * 行动数量
     */
    Integer getMessage_needaction_counter();

    void setMessage_needaction_counter(Integer message_needaction_counter);

    /**
     * 获取 [行动数量]脏标记
     */
    boolean getMessage_needaction_counterDirtyFlag();

    /**
     * 下一活动摘要
     */
    String getActivity_summary();

    void setActivity_summary(String activity_summary);

    /**
     * 获取 [下一活动摘要]脏标记
     */
    boolean getActivity_summaryDirtyFlag();

    /**
     * 天数
     */
    Double getNumber_of_days();

    void setNumber_of_days(Double number_of_days);

    /**
     * 获取 [天数]脏标记
     */
    boolean getNumber_of_daysDirtyFlag();

    /**
     * 理由
     */
    String getNotes();

    void setNotes(String notes);

    /**
     * 获取 [理由]脏标记
     */
    boolean getNotesDirtyFlag();

    /**
     * 活动状态
     */
    String getActivity_state();

    void setActivity_state(String activity_state);

    /**
     * 获取 [活动状态]脏标记
     */
    boolean getActivity_stateDirtyFlag();

    /**
     * 是关注者
     */
    String getMessage_is_follower();

    void setMessage_is_follower(String message_is_follower);

    /**
     * 获取 [是关注者]脏标记
     */
    boolean getMessage_is_followerDirtyFlag();

    /**
     * 关注者(业务伙伴)
     */
    String getMessage_partner_ids();

    void setMessage_partner_ids(String message_partner_ids);

    /**
     * 获取 [关注者(业务伙伴)]脏标记
     */
    boolean getMessage_partner_idsDirtyFlag();

    /**
     * 状态
     */
    String getState();

    void setState(String state);

    /**
     * 获取 [状态]脏标记
     */
    boolean getStateDirtyFlag();

    /**
     * 两个区间之间的时间单位
     */
    String getInterval_unit();

    void setInterval_unit(String interval_unit);

    /**
     * 获取 [两个区间之间的时间单位]脏标记
     */
    boolean getInterval_unitDirtyFlag();

    /**
     * 分配 （天/小时）
     */
    String getDuration_display();

    void setDuration_display(String duration_display);

    /**
     * 获取 [分配 （天/小时）]脏标记
     */
    boolean getDuration_displayDirtyFlag();

    /**
     * 能批准
     */
    String getCan_approve();

    void setCan_approve(String can_approve);

    /**
     * 获取 [能批准]脏标记
     */
    boolean getCan_approveDirtyFlag();

    /**
     * ID
     */
    Integer getId();

    void setId(Integer id);

    /**
     * 获取 [ID]脏标记
     */
    boolean getIdDirtyFlag();

    /**
     * 能重置
     */
    String getCan_reset();

    void setCan_reset(String can_reset);

    /**
     * 获取 [能重置]脏标记
     */
    boolean getCan_resetDirtyFlag();

    /**
     * 错误数
     */
    Integer getMessage_has_error_counter();

    void setMessage_has_error_counter(Integer message_has_error_counter);

    /**
     * 获取 [错误数]脏标记
     */
    boolean getMessage_has_error_counterDirtyFlag();

    /**
     * 网站消息
     */
    String getWebsite_message_ids();

    void setWebsite_message_ids(String website_message_ids);

    /**
     * 获取 [网站消息]脏标记
     */
    boolean getWebsite_message_idsDirtyFlag();

    /**
     * 在每个区间添加的时间单位
     */
    String getUnit_per_interval();

    void setUnit_per_interval(String unit_per_interval);

    /**
     * 获取 [在每个区间添加的时间单位]脏标记
     */
    boolean getUnit_per_intervalDirtyFlag();

    /**
     * 余额限额
     */
    Integer getAccrual_limit();

    void setAccrual_limit(Integer accrual_limit);

    /**
     * 获取 [余额限额]脏标记
     */
    boolean getAccrual_limitDirtyFlag();

    /**
     * 最后更新时间
     */
    Timestamp getWrite_date();

    void setWrite_date(Timestamp write_date);

    /**
     * 获取 [最后更新时间]脏标记
     */
    boolean getWrite_dateDirtyFlag();

    /**
     * 未读消息计数器
     */
    Integer getMessage_unread_counter();

    void setMessage_unread_counter(Integer message_unread_counter);

    /**
     * 获取 [未读消息计数器]脏标记
     */
    boolean getMessage_unread_counterDirtyFlag();

    /**
     * 显示名称
     */
    String getDisplay_name();

    void setDisplay_name(String display_name);

    /**
     * 获取 [显示名称]脏标记
     */
    boolean getDisplay_nameDirtyFlag();

    /**
     * 开始日期
     */
    Timestamp getDate_from();

    void setDate_from(Timestamp date_from);

    /**
     * 获取 [开始日期]脏标记
     */
    boolean getDate_fromDirtyFlag();

    /**
     * 消息递送错误
     */
    String getMessage_has_error();

    void setMessage_has_error(String message_has_error);

    /**
     * 获取 [消息递送错误]脏标记
     */
    boolean getMessage_has_errorDirtyFlag();

    /**
     * 下一活动截止日期
     */
    Timestamp getActivity_date_deadline();

    void setActivity_date_deadline(Timestamp activity_date_deadline);

    /**
     * 获取 [下一活动截止日期]脏标记
     */
    boolean getActivity_date_deadlineDirtyFlag();

    /**
     * 部门
     */
    String getDepartment_id_text();

    void setDepartment_id_text(String department_id_text);

    /**
     * 获取 [部门]脏标记
     */
    boolean getDepartment_id_textDirtyFlag();

    /**
     * 员工
     */
    String getEmployee_id_text();

    void setEmployee_id_text(String employee_id_text);

    /**
     * 获取 [员工]脏标记
     */
    boolean getEmployee_id_textDirtyFlag();

    /**
     * 验证人
     */
    String getValidation_type();

    void setValidation_type(String validation_type);

    /**
     * 获取 [验证人]脏标记
     */
    boolean getValidation_typeDirtyFlag();

    /**
     * 创建人
     */
    String getCreate_uid_text();

    void setCreate_uid_text(String create_uid_text);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uid_textDirtyFlag();

    /**
     * 公司
     */
    String getMode_company_id_text();

    void setMode_company_id_text(String mode_company_id_text);

    /**
     * 获取 [公司]脏标记
     */
    boolean getMode_company_id_textDirtyFlag();

    /**
     * 首次审批
     */
    String getFirst_approver_id_text();

    void setFirst_approver_id_text(String first_approver_id_text);

    /**
     * 获取 [首次审批]脏标记
     */
    boolean getFirst_approver_id_textDirtyFlag();

    /**
     * 休假类型
     */
    String getHoliday_status_id_text();

    void setHoliday_status_id_text(String holiday_status_id_text);

    /**
     * 获取 [休假类型]脏标记
     */
    boolean getHoliday_status_id_textDirtyFlag();

    /**
     * 最后更新人
     */
    String getWrite_uid_text();

    void setWrite_uid_text(String write_uid_text);

    /**
     * 获取 [最后更新人]脏标记
     */
    boolean getWrite_uid_textDirtyFlag();

    /**
     * 休假
     */
    String getType_request_unit();

    void setType_request_unit(String type_request_unit);

    /**
     * 获取 [休假]脏标记
     */
    boolean getType_request_unitDirtyFlag();

    /**
     * 上级
     */
    String getParent_id_text();

    void setParent_id_text(String parent_id_text);

    /**
     * 获取 [上级]脏标记
     */
    boolean getParent_id_textDirtyFlag();

    /**
     * 第二次审批
     */
    String getSecond_approver_id_text();

    void setSecond_approver_id_text(String second_approver_id_text);

    /**
     * 获取 [第二次审批]脏标记
     */
    boolean getSecond_approver_id_textDirtyFlag();

    /**
     * 员工标签
     */
    String getCategory_id_text();

    void setCategory_id_text(String category_id_text);

    /**
     * 获取 [员工标签]脏标记
     */
    boolean getCategory_id_textDirtyFlag();

    /**
     * 休假类型
     */
    Integer getHoliday_status_id();

    void setHoliday_status_id(Integer holiday_status_id);

    /**
     * 获取 [休假类型]脏标记
     */
    boolean getHoliday_status_idDirtyFlag();

    /**
     * 部门
     */
    Integer getDepartment_id();

    void setDepartment_id(Integer department_id);

    /**
     * 获取 [部门]脏标记
     */
    boolean getDepartment_idDirtyFlag();

    /**
     * 上级
     */
    Integer getParent_id();

    void setParent_id(Integer parent_id);

    /**
     * 获取 [上级]脏标记
     */
    boolean getParent_idDirtyFlag();

    /**
     * 创建人
     */
    Integer getCreate_uid();

    void setCreate_uid(Integer create_uid);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uidDirtyFlag();

    /**
     * 第二次审批
     */
    Integer getSecond_approver_id();

    void setSecond_approver_id(Integer second_approver_id);

    /**
     * 获取 [第二次审批]脏标记
     */
    boolean getSecond_approver_idDirtyFlag();

    /**
     * 公司
     */
    Integer getMode_company_id();

    void setMode_company_id(Integer mode_company_id);

    /**
     * 获取 [公司]脏标记
     */
    boolean getMode_company_idDirtyFlag();

    /**
     * 最后更新人
     */
    Integer getWrite_uid();

    void setWrite_uid(Integer write_uid);

    /**
     * 获取 [最后更新人]脏标记
     */
    boolean getWrite_uidDirtyFlag();

    /**
     * 首次审批
     */
    Integer getFirst_approver_id();

    void setFirst_approver_id(Integer first_approver_id);

    /**
     * 获取 [首次审批]脏标记
     */
    boolean getFirst_approver_idDirtyFlag();

    /**
     * 员工标签
     */
    Integer getCategory_id();

    void setCategory_id(Integer category_id);

    /**
     * 获取 [员工标签]脏标记
     */
    boolean getCategory_idDirtyFlag();

    /**
     * 员工
     */
    Integer getEmployee_id();

    void setEmployee_id(Integer employee_id);

    /**
     * 获取 [员工]脏标记
     */
    boolean getEmployee_idDirtyFlag();

}
