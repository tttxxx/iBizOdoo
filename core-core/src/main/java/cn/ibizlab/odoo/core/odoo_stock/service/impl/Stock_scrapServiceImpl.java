package cn.ibizlab.odoo.core.odoo_stock.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_stock.domain.Stock_scrap;
import cn.ibizlab.odoo.core.odoo_stock.filter.Stock_scrapSearchContext;
import cn.ibizlab.odoo.core.odoo_stock.service.IStock_scrapService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;



import cn.ibizlab.odoo.core.odoo_stock.client.stock_scrapOdooClient;
import cn.ibizlab.odoo.core.odoo_stock.clientmodel.stock_scrapClientModel;

/**
 * 实体[报废] 服务对象接口实现
 */
@Slf4j
@Service
public class Stock_scrapServiceImpl implements IStock_scrapService {

    @Autowired
    stock_scrapOdooClient stock_scrapOdooClient;


    @Override
    public boolean remove(Integer id) {
        stock_scrapClientModel clientModel = new stock_scrapClientModel();
        clientModel.setId(id);
		stock_scrapOdooClient.remove(clientModel);
        return true; 
    }

    public void removeBatch(Collection<Integer> idList){
    }

    @Override
    public boolean create(Stock_scrap et) {
        stock_scrapClientModel clientModel = convert2Model(et,null);
		stock_scrapOdooClient.create(clientModel);
        Stock_scrap rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Stock_scrap> list){
    }

    @Override
    public boolean update(Stock_scrap et) {
        stock_scrapClientModel clientModel = convert2Model(et,null);
		stock_scrapOdooClient.update(clientModel);
        Stock_scrap rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }
    
    public void updateBatch(List<Stock_scrap> list){
    }

    @Override
    public Stock_scrap get(Integer id) {
        stock_scrapClientModel clientModel = new stock_scrapClientModel();
        clientModel.setId(id);
		stock_scrapOdooClient.get(clientModel);
        Stock_scrap et = convert2Domain(clientModel,null);
        if(et==null){
            et=new Stock_scrap();
            et.setId(id);
        }
        else{
        }

        return  et;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Stock_scrap> searchDefault(Stock_scrapSearchContext context) {
        List<Stock_scrap> list = new ArrayList<Stock_scrap>();
        Page<stock_scrapClientModel> clientModelList = stock_scrapOdooClient.search(context);
        for(stock_scrapClientModel clientModel : clientModelList){
            list.add( this.convert2Domain(clientModel,null));
        }
        return new PageImpl<Stock_scrap>(list, context.getPageable(), clientModelList.getTotalElements());
    }

    public stock_scrapClientModel convert2Model(Stock_scrap domain , stock_scrapClientModel model) {
        if(domain == null)
            return model ;
        if(model==null){
            model = new stock_scrapClientModel();

        }
        if((Boolean) domain.getExtensionparams().get("dirtyflagenable")){
            if((Boolean) domain.getExtensionparams().get("write_datedirtyflag"))
                model.setWrite_date(domain.getWriteDate());
            if((Boolean) domain.getExtensionparams().get("date_expecteddirtyflag"))
                model.setDate_expected(domain.getDateExpected());
            if((Boolean) domain.getExtensionparams().get("scrap_qtydirtyflag"))
                model.setScrap_qty(domain.getScrapQty());
            if((Boolean) domain.getExtensionparams().get("__last_updatedirtyflag"))
                model.set__last_update(domain.getLastUpdate());
            if((Boolean) domain.getExtensionparams().get("origindirtyflag"))
                model.setOrigin(domain.getOrigin());
            if((Boolean) domain.getExtensionparams().get("statedirtyflag"))
                model.setState(domain.getState());
            if((Boolean) domain.getExtensionparams().get("iddirtyflag"))
                model.setId(domain.getId());
            if((Boolean) domain.getExtensionparams().get("create_datedirtyflag"))
                model.setCreate_date(domain.getCreateDate());
            if((Boolean) domain.getExtensionparams().get("display_namedirtyflag"))
                model.setDisplay_name(domain.getDisplayName());
            if((Boolean) domain.getExtensionparams().get("namedirtyflag"))
                model.setName(domain.getName());
            if((Boolean) domain.getExtensionparams().get("workorder_id_textdirtyflag"))
                model.setWorkorder_id_text(domain.getWorkorderIdText());
            if((Boolean) domain.getExtensionparams().get("scrap_location_id_textdirtyflag"))
                model.setScrap_location_id_text(domain.getScrapLocationIdText());
            if((Boolean) domain.getExtensionparams().get("lot_id_textdirtyflag"))
                model.setLot_id_text(domain.getLotIdText());
            if((Boolean) domain.getExtensionparams().get("write_uid_textdirtyflag"))
                model.setWrite_uid_text(domain.getWriteUidText());
            if((Boolean) domain.getExtensionparams().get("move_id_textdirtyflag"))
                model.setMove_id_text(domain.getMoveIdText());
            if((Boolean) domain.getExtensionparams().get("picking_id_textdirtyflag"))
                model.setPicking_id_text(domain.getPickingIdText());
            if((Boolean) domain.getExtensionparams().get("production_id_textdirtyflag"))
                model.setProduction_id_text(domain.getProductionIdText());
            if((Boolean) domain.getExtensionparams().get("location_id_textdirtyflag"))
                model.setLocation_id_text(domain.getLocationIdText());
            if((Boolean) domain.getExtensionparams().get("product_uom_id_textdirtyflag"))
                model.setProduct_uom_id_text(domain.getProductUomIdText());
            if((Boolean) domain.getExtensionparams().get("trackingdirtyflag"))
                model.setTracking(domain.getTracking());
            if((Boolean) domain.getExtensionparams().get("product_id_textdirtyflag"))
                model.setProduct_id_text(domain.getProductIdText());
            if((Boolean) domain.getExtensionparams().get("package_id_textdirtyflag"))
                model.setPackage_id_text(domain.getPackageIdText());
            if((Boolean) domain.getExtensionparams().get("create_uid_textdirtyflag"))
                model.setCreate_uid_text(domain.getCreateUidText());
            if((Boolean) domain.getExtensionparams().get("owner_id_textdirtyflag"))
                model.setOwner_id_text(domain.getOwnerIdText());
            if((Boolean) domain.getExtensionparams().get("create_uiddirtyflag"))
                model.setCreate_uid(domain.getCreateUid());
            if((Boolean) domain.getExtensionparams().get("lot_iddirtyflag"))
                model.setLot_id(domain.getLotId());
            if((Boolean) domain.getExtensionparams().get("production_iddirtyflag"))
                model.setProduction_id(domain.getProductionId());
            if((Boolean) domain.getExtensionparams().get("picking_iddirtyflag"))
                model.setPicking_id(domain.getPickingId());
            if((Boolean) domain.getExtensionparams().get("scrap_location_iddirtyflag"))
                model.setScrap_location_id(domain.getScrapLocationId());
            if((Boolean) domain.getExtensionparams().get("move_iddirtyflag"))
                model.setMove_id(domain.getMoveId());
            if((Boolean) domain.getExtensionparams().get("location_iddirtyflag"))
                model.setLocation_id(domain.getLocationId());
            if((Boolean) domain.getExtensionparams().get("write_uiddirtyflag"))
                model.setWrite_uid(domain.getWriteUid());
            if((Boolean) domain.getExtensionparams().get("workorder_iddirtyflag"))
                model.setWorkorder_id(domain.getWorkorderId());
            if((Boolean) domain.getExtensionparams().get("product_uom_iddirtyflag"))
                model.setProduct_uom_id(domain.getProductUomId());
            if((Boolean) domain.getExtensionparams().get("package_iddirtyflag"))
                model.setPackage_id(domain.getPackageId());
            if((Boolean) domain.getExtensionparams().get("product_iddirtyflag"))
                model.setProduct_id(domain.getProductId());
            if((Boolean) domain.getExtensionparams().get("owner_iddirtyflag"))
                model.setOwner_id(domain.getOwnerId());
            domain.getFocusNull().clear();
        }
        return model ;
    }

    public Stock_scrap convert2Domain( stock_scrapClientModel model ,Stock_scrap domain) {
        if(model == null)
            return domain ;
        if(domain == null){
            domain = new Stock_scrap();
        }

        if(model.getWrite_dateDirtyFlag())
            domain.setWriteDate(model.getWrite_date());
        if(model.getDate_expectedDirtyFlag())
            domain.setDateExpected(model.getDate_expected());
        if(model.getScrap_qtyDirtyFlag())
            domain.setScrapQty(model.getScrap_qty());
        if(model.get__last_updateDirtyFlag())
            domain.setLastUpdate(model.get__last_update());
        if(model.getOriginDirtyFlag())
            domain.setOrigin(model.getOrigin());
        if(model.getStateDirtyFlag())
            domain.setState(model.getState());
        if(model.getIdDirtyFlag())
            domain.setId(model.getId());
        if(model.getCreate_dateDirtyFlag())
            domain.setCreateDate(model.getCreate_date());
        if(model.getDisplay_nameDirtyFlag())
            domain.setDisplayName(model.getDisplay_name());
        if(model.getNameDirtyFlag())
            domain.setName(model.getName());
        if(model.getWorkorder_id_textDirtyFlag())
            domain.setWorkorderIdText(model.getWorkorder_id_text());
        if(model.getScrap_location_id_textDirtyFlag())
            domain.setScrapLocationIdText(model.getScrap_location_id_text());
        if(model.getLot_id_textDirtyFlag())
            domain.setLotIdText(model.getLot_id_text());
        if(model.getWrite_uid_textDirtyFlag())
            domain.setWriteUidText(model.getWrite_uid_text());
        if(model.getMove_id_textDirtyFlag())
            domain.setMoveIdText(model.getMove_id_text());
        if(model.getPicking_id_textDirtyFlag())
            domain.setPickingIdText(model.getPicking_id_text());
        if(model.getProduction_id_textDirtyFlag())
            domain.setProductionIdText(model.getProduction_id_text());
        if(model.getLocation_id_textDirtyFlag())
            domain.setLocationIdText(model.getLocation_id_text());
        if(model.getProduct_uom_id_textDirtyFlag())
            domain.setProductUomIdText(model.getProduct_uom_id_text());
        if(model.getTrackingDirtyFlag())
            domain.setTracking(model.getTracking());
        if(model.getProduct_id_textDirtyFlag())
            domain.setProductIdText(model.getProduct_id_text());
        if(model.getPackage_id_textDirtyFlag())
            domain.setPackageIdText(model.getPackage_id_text());
        if(model.getCreate_uid_textDirtyFlag())
            domain.setCreateUidText(model.getCreate_uid_text());
        if(model.getOwner_id_textDirtyFlag())
            domain.setOwnerIdText(model.getOwner_id_text());
        if(model.getCreate_uidDirtyFlag())
            domain.setCreateUid(model.getCreate_uid());
        if(model.getLot_idDirtyFlag())
            domain.setLotId(model.getLot_id());
        if(model.getProduction_idDirtyFlag())
            domain.setProductionId(model.getProduction_id());
        if(model.getPicking_idDirtyFlag())
            domain.setPickingId(model.getPicking_id());
        if(model.getScrap_location_idDirtyFlag())
            domain.setScrapLocationId(model.getScrap_location_id());
        if(model.getMove_idDirtyFlag())
            domain.setMoveId(model.getMove_id());
        if(model.getLocation_idDirtyFlag())
            domain.setLocationId(model.getLocation_id());
        if(model.getWrite_uidDirtyFlag())
            domain.setWriteUid(model.getWrite_uid());
        if(model.getWorkorder_idDirtyFlag())
            domain.setWorkorderId(model.getWorkorder_id());
        if(model.getProduct_uom_idDirtyFlag())
            domain.setProductUomId(model.getProduct_uom_id());
        if(model.getPackage_idDirtyFlag())
            domain.setPackageId(model.getPackage_id());
        if(model.getProduct_idDirtyFlag())
            domain.setProductId(model.getProduct_id());
        if(model.getOwner_idDirtyFlag())
            domain.setOwnerId(model.getOwner_id());
        return domain ;
    }

}

    



