package cn.ibizlab.odoo.core.odoo_product.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_product.domain.Product_pricelist;
import cn.ibizlab.odoo.core.odoo_product.filter.Product_pricelistSearchContext;
import cn.ibizlab.odoo.core.odoo_product.service.IProduct_pricelistService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;



import cn.ibizlab.odoo.core.odoo_product.client.product_pricelistOdooClient;
import cn.ibizlab.odoo.core.odoo_product.clientmodel.product_pricelistClientModel;

/**
 * 实体[价格表] 服务对象接口实现
 */
@Slf4j
@Service
public class Product_pricelistServiceImpl implements IProduct_pricelistService {

    @Autowired
    product_pricelistOdooClient product_pricelistOdooClient;


    @Override
    public Product_pricelist get(Integer id) {
        product_pricelistClientModel clientModel = new product_pricelistClientModel();
        clientModel.setId(id);
		product_pricelistOdooClient.get(clientModel);
        Product_pricelist et = convert2Domain(clientModel,null);
        if(et==null){
            et=new Product_pricelist();
            et.setId(id);
        }
        else{
        }

        return  et;
    }

    @Override
    public boolean update(Product_pricelist et) {
        product_pricelistClientModel clientModel = convert2Model(et,null);
		product_pricelistOdooClient.update(clientModel);
        Product_pricelist rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }
    
    public void updateBatch(List<Product_pricelist> list){
    }

    @Override
    public boolean remove(Integer id) {
        product_pricelistClientModel clientModel = new product_pricelistClientModel();
        clientModel.setId(id);
		product_pricelistOdooClient.remove(clientModel);
        return true; 
    }

    public void removeBatch(Collection<Integer> idList){
    }

    @Override
    public boolean create(Product_pricelist et) {
        product_pricelistClientModel clientModel = convert2Model(et,null);
		product_pricelistOdooClient.create(clientModel);
        Product_pricelist rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Product_pricelist> list){
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Product_pricelist> searchDefault(Product_pricelistSearchContext context) {
        List<Product_pricelist> list = new ArrayList<Product_pricelist>();
        Page<product_pricelistClientModel> clientModelList = product_pricelistOdooClient.search(context);
        for(product_pricelistClientModel clientModel : clientModelList){
            list.add( this.convert2Domain(clientModel,null));
        }
        return new PageImpl<Product_pricelist>(list, context.getPageable(), clientModelList.getTotalElements());
    }

    public product_pricelistClientModel convert2Model(Product_pricelist domain , product_pricelistClientModel model) {
        if(domain == null)
            return model ;
        if(model==null){
            model = new product_pricelistClientModel();

        }
        if((Boolean) domain.getExtensionparams().get("dirtyflagenable")){
            if((Boolean) domain.getExtensionparams().get("namedirtyflag"))
                model.setName(domain.getName());
            if((Boolean) domain.getExtensionparams().get("display_namedirtyflag"))
                model.setDisplay_name(domain.getDisplayName());
            if((Boolean) domain.getExtensionparams().get("sequencedirtyflag"))
                model.setSequence(domain.getSequence());
            if((Boolean) domain.getExtensionparams().get("selectabledirtyflag"))
                model.setSelectable(domain.getSelectable());
            if((Boolean) domain.getExtensionparams().get("discount_policydirtyflag"))
                model.setDiscount_policy(domain.getDiscountPolicy());
            if((Boolean) domain.getExtensionparams().get("country_group_idsdirtyflag"))
                model.setCountry_group_ids(domain.getCountryGroupIds());
            if((Boolean) domain.getExtensionparams().get("codedirtyflag"))
                model.setCode(domain.getCode());
            if((Boolean) domain.getExtensionparams().get("website_iddirtyflag"))
                model.setWebsite_id(domain.getWebsiteId());
            if((Boolean) domain.getExtensionparams().get("activedirtyflag"))
                model.setActive(domain.getActive());
            if((Boolean) domain.getExtensionparams().get("write_datedirtyflag"))
                model.setWrite_date(domain.getWriteDate());
            if((Boolean) domain.getExtensionparams().get("create_datedirtyflag"))
                model.setCreate_date(domain.getCreateDate());
            if((Boolean) domain.getExtensionparams().get("item_idsdirtyflag"))
                model.setItem_ids(domain.getItemIds());
            if((Boolean) domain.getExtensionparams().get("iddirtyflag"))
                model.setId(domain.getId());
            if((Boolean) domain.getExtensionparams().get("__last_updatedirtyflag"))
                model.set__last_update(domain.getLastUpdate());
            if((Boolean) domain.getExtensionparams().get("currency_id_textdirtyflag"))
                model.setCurrency_id_text(domain.getCurrencyIdText());
            if((Boolean) domain.getExtensionparams().get("write_uid_textdirtyflag"))
                model.setWrite_uid_text(domain.getWriteUidText());
            if((Boolean) domain.getExtensionparams().get("create_uid_textdirtyflag"))
                model.setCreate_uid_text(domain.getCreateUidText());
            if((Boolean) domain.getExtensionparams().get("company_id_textdirtyflag"))
                model.setCompany_id_text(domain.getCompanyIdText());
            if((Boolean) domain.getExtensionparams().get("create_uiddirtyflag"))
                model.setCreate_uid(domain.getCreateUid());
            if((Boolean) domain.getExtensionparams().get("write_uiddirtyflag"))
                model.setWrite_uid(domain.getWriteUid());
            if((Boolean) domain.getExtensionparams().get("currency_iddirtyflag"))
                model.setCurrency_id(domain.getCurrencyId());
            if((Boolean) domain.getExtensionparams().get("company_iddirtyflag"))
                model.setCompany_id(domain.getCompanyId());
            domain.getFocusNull().clear();
        }
        return model ;
    }

    public Product_pricelist convert2Domain( product_pricelistClientModel model ,Product_pricelist domain) {
        if(model == null)
            return domain ;
        if(domain == null){
            domain = new Product_pricelist();
        }

        if(model.getNameDirtyFlag())
            domain.setName(model.getName());
        if(model.getDisplay_nameDirtyFlag())
            domain.setDisplayName(model.getDisplay_name());
        if(model.getSequenceDirtyFlag())
            domain.setSequence(model.getSequence());
        if(model.getSelectableDirtyFlag())
            domain.setSelectable(model.getSelectable());
        if(model.getDiscount_policyDirtyFlag())
            domain.setDiscountPolicy(model.getDiscount_policy());
        if(model.getCountry_group_idsDirtyFlag())
            domain.setCountryGroupIds(model.getCountry_group_ids());
        if(model.getCodeDirtyFlag())
            domain.setCode(model.getCode());
        if(model.getWebsite_idDirtyFlag())
            domain.setWebsiteId(model.getWebsite_id());
        if(model.getActiveDirtyFlag())
            domain.setActive(model.getActive());
        if(model.getWrite_dateDirtyFlag())
            domain.setWriteDate(model.getWrite_date());
        if(model.getCreate_dateDirtyFlag())
            domain.setCreateDate(model.getCreate_date());
        if(model.getItem_idsDirtyFlag())
            domain.setItemIds(model.getItem_ids());
        if(model.getIdDirtyFlag())
            domain.setId(model.getId());
        if(model.get__last_updateDirtyFlag())
            domain.setLastUpdate(model.get__last_update());
        if(model.getCurrency_id_textDirtyFlag())
            domain.setCurrencyIdText(model.getCurrency_id_text());
        if(model.getWrite_uid_textDirtyFlag())
            domain.setWriteUidText(model.getWrite_uid_text());
        if(model.getCreate_uid_textDirtyFlag())
            domain.setCreateUidText(model.getCreate_uid_text());
        if(model.getCompany_id_textDirtyFlag())
            domain.setCompanyIdText(model.getCompany_id_text());
        if(model.getCreate_uidDirtyFlag())
            domain.setCreateUid(model.getCreate_uid());
        if(model.getWrite_uidDirtyFlag())
            domain.setWriteUid(model.getWrite_uid());
        if(model.getCurrency_idDirtyFlag())
            domain.setCurrencyId(model.getCurrency_id());
        if(model.getCompany_idDirtyFlag())
            domain.setCompanyId(model.getCompany_id());
        return domain ;
    }

}

    



