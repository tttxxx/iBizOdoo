package cn.ibizlab.odoo.core.odoo_sale.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_sale.domain.Sale_order_line;
import cn.ibizlab.odoo.core.odoo_sale.filter.Sale_order_lineSearchContext;
import cn.ibizlab.odoo.core.odoo_sale.service.ISale_order_lineService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;



import cn.ibizlab.odoo.core.odoo_sale.client.sale_order_lineOdooClient;
import cn.ibizlab.odoo.core.odoo_sale.clientmodel.sale_order_lineClientModel;

/**
 * 实体[销售订单行] 服务对象接口实现
 */
@Slf4j
@Service
public class Sale_order_lineServiceImpl implements ISale_order_lineService {

    @Autowired
    sale_order_lineOdooClient sale_order_lineOdooClient;


    @Override
    public Sale_order_line get(Integer id) {
        sale_order_lineClientModel clientModel = new sale_order_lineClientModel();
        clientModel.setId(id);
		sale_order_lineOdooClient.get(clientModel);
        Sale_order_line et = convert2Domain(clientModel,null);
        if(et==null){
            et=new Sale_order_line();
            et.setId(id);
        }
        else{
        }

        return  et;
    }

    @Override
    public boolean remove(Integer id) {
        sale_order_lineClientModel clientModel = new sale_order_lineClientModel();
        clientModel.setId(id);
		sale_order_lineOdooClient.remove(clientModel);
        return true; 
    }

    public void removeBatch(Collection<Integer> idList){
    }

    @Override
    public boolean update(Sale_order_line et) {
        sale_order_lineClientModel clientModel = convert2Model(et,null);
		sale_order_lineOdooClient.update(clientModel);
        Sale_order_line rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }
    
    public void updateBatch(List<Sale_order_line> list){
    }

    @Override
    public boolean create(Sale_order_line et) {
        sale_order_lineClientModel clientModel = convert2Model(et,null);
		sale_order_lineOdooClient.create(clientModel);
        Sale_order_line rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Sale_order_line> list){
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Sale_order_line> searchDefault(Sale_order_lineSearchContext context) {
        List<Sale_order_line> list = new ArrayList<Sale_order_line>();
        Page<sale_order_lineClientModel> clientModelList = sale_order_lineOdooClient.search(context);
        for(sale_order_lineClientModel clientModel : clientModelList){
            list.add( this.convert2Domain(clientModel,null));
        }
        return new PageImpl<Sale_order_line>(list, context.getPageable(), clientModelList.getTotalElements());
    }

    public sale_order_lineClientModel convert2Model(Sale_order_line domain , sale_order_lineClientModel model) {
        if(domain == null)
            return model ;
        if(model==null){
            model = new sale_order_lineClientModel();

        }
        if((Boolean) domain.getExtensionparams().get("dirtyflagenable")){
            if((Boolean) domain.getExtensionparams().get("is_expensedirtyflag"))
                model.setIs_expense(domain.getIsExpense());
            if((Boolean) domain.getExtensionparams().get("write_datedirtyflag"))
                model.setWrite_date(domain.getWriteDate());
            if((Boolean) domain.getExtensionparams().get("qty_to_invoicedirtyflag"))
                model.setQty_to_invoice(domain.getQtyToInvoice());
            if((Boolean) domain.getExtensionparams().get("analytic_tag_idsdirtyflag"))
                model.setAnalytic_tag_ids(domain.getAnalyticTagIds());
            if((Boolean) domain.getExtensionparams().get("warning_stockdirtyflag"))
                model.setWarning_stock(domain.getWarningStock());
            if((Boolean) domain.getExtensionparams().get("iddirtyflag"))
                model.setId(domain.getId());
            if((Boolean) domain.getExtensionparams().get("display_namedirtyflag"))
                model.setDisplay_name(domain.getDisplayName());
            if((Boolean) domain.getExtensionparams().get("price_reduce_taxexcldirtyflag"))
                model.setPrice_reduce_taxexcl(domain.getPriceReduceTaxexcl());
            if((Boolean) domain.getExtensionparams().get("product_custom_attribute_value_idsdirtyflag"))
                model.setProduct_custom_attribute_value_ids(domain.getProductCustomAttributeValueIds());
            if((Boolean) domain.getExtensionparams().get("qty_delivered_manualdirtyflag"))
                model.setQty_delivered_manual(domain.getQtyDeliveredManual());
            if((Boolean) domain.getExtensionparams().get("price_taxdirtyflag"))
                model.setPrice_tax(domain.getPriceTax());
            if((Boolean) domain.getExtensionparams().get("qty_delivered_methoddirtyflag"))
                model.setQty_delivered_method(domain.getQtyDeliveredMethod());
            if((Boolean) domain.getExtensionparams().get("invoice_statusdirtyflag"))
                model.setInvoice_status(domain.getInvoiceStatus());
            if((Boolean) domain.getExtensionparams().get("display_typedirtyflag"))
                model.setDisplay_type(domain.getDisplayType());
            if((Boolean) domain.getExtensionparams().get("move_idsdirtyflag"))
                model.setMove_ids(domain.getMoveIds());
            if((Boolean) domain.getExtensionparams().get("untaxed_amount_invoiceddirtyflag"))
                model.setUntaxed_amount_invoiced(domain.getUntaxedAmountInvoiced());
            if((Boolean) domain.getExtensionparams().get("product_no_variant_attribute_value_idsdirtyflag"))
                model.setProduct_no_variant_attribute_value_ids(domain.getProductNoVariantAttributeValueIds());
            if((Boolean) domain.getExtensionparams().get("__last_updatedirtyflag"))
                model.set__last_update(domain.getLastUpdate());
            if((Boolean) domain.getExtensionparams().get("is_downpaymentdirtyflag"))
                model.setIs_downpayment(domain.getIsDownpayment());
            if((Boolean) domain.getExtensionparams().get("analytic_line_idsdirtyflag"))
                model.setAnalytic_line_ids(domain.getAnalyticLineIds());
            if((Boolean) domain.getExtensionparams().get("price_reduce_taxincdirtyflag"))
                model.setPrice_reduce_taxinc(domain.getPriceReduceTaxinc());
            if((Boolean) domain.getExtensionparams().get("qty_delivereddirtyflag"))
                model.setQty_delivered(domain.getQtyDelivered());
            if((Boolean) domain.getExtensionparams().get("discountdirtyflag"))
                model.setDiscount(domain.getDiscount());
            if((Boolean) domain.getExtensionparams().get("product_uom_qtydirtyflag"))
                model.setProduct_uom_qty(domain.getProductUomQty());
            if((Boolean) domain.getExtensionparams().get("price_reducedirtyflag"))
                model.setPrice_reduce(domain.getPriceReduce());
            if((Boolean) domain.getExtensionparams().get("customer_leaddirtyflag"))
                model.setCustomer_lead(domain.getCustomerLead());
            if((Boolean) domain.getExtensionparams().get("product_updatabledirtyflag"))
                model.setProduct_updatable(domain.getProductUpdatable());
            if((Boolean) domain.getExtensionparams().get("qty_invoiceddirtyflag"))
                model.setQty_invoiced(domain.getQtyInvoiced());
            if((Boolean) domain.getExtensionparams().get("untaxed_amount_to_invoicedirtyflag"))
                model.setUntaxed_amount_to_invoice(domain.getUntaxedAmountToInvoice());
            if((Boolean) domain.getExtensionparams().get("namedirtyflag"))
                model.setName(domain.getName());
            if((Boolean) domain.getExtensionparams().get("price_unitdirtyflag"))
                model.setPrice_unit(domain.getPriceUnit());
            if((Boolean) domain.getExtensionparams().get("create_datedirtyflag"))
                model.setCreate_date(domain.getCreateDate());
            if((Boolean) domain.getExtensionparams().get("purchase_line_countdirtyflag"))
                model.setPurchase_line_count(domain.getPurchaseLineCount());
            if((Boolean) domain.getExtensionparams().get("name_shortdirtyflag"))
                model.setName_short(domain.getNameShort());
            if((Boolean) domain.getExtensionparams().get("option_line_idsdirtyflag"))
                model.setOption_line_ids(domain.getOptionLineIds());
            if((Boolean) domain.getExtensionparams().get("tax_iddirtyflag"))
                model.setTax_id(domain.getTaxId());
            if((Boolean) domain.getExtensionparams().get("sale_order_option_idsdirtyflag"))
                model.setSale_order_option_ids(domain.getSaleOrderOptionIds());
            if((Boolean) domain.getExtensionparams().get("price_subtotaldirtyflag"))
                model.setPrice_subtotal(domain.getPriceSubtotal());
            if((Boolean) domain.getExtensionparams().get("sequencedirtyflag"))
                model.setSequence(domain.getSequence());
            if((Boolean) domain.getExtensionparams().get("price_totaldirtyflag"))
                model.setPrice_total(domain.getPriceTotal());
            if((Boolean) domain.getExtensionparams().get("invoice_linesdirtyflag"))
                model.setInvoice_lines(domain.getInvoiceLines());
            if((Boolean) domain.getExtensionparams().get("purchase_line_idsdirtyflag"))
                model.setPurchase_line_ids(domain.getPurchaseLineIds());
            if((Boolean) domain.getExtensionparams().get("event_okdirtyflag"))
                model.setEvent_ok(domain.getEventOk());
            if((Boolean) domain.getExtensionparams().get("event_ticket_id_textdirtyflag"))
                model.setEvent_ticket_id_text(domain.getEventTicketIdText());
            if((Boolean) domain.getExtensionparams().get("create_uid_textdirtyflag"))
                model.setCreate_uid_text(domain.getCreateUidText());
            if((Boolean) domain.getExtensionparams().get("linked_line_id_textdirtyflag"))
                model.setLinked_line_id_text(domain.getLinkedLineIdText());
            if((Boolean) domain.getExtensionparams().get("order_partner_id_textdirtyflag"))
                model.setOrder_partner_id_text(domain.getOrderPartnerIdText());
            if((Boolean) domain.getExtensionparams().get("product_imagedirtyflag"))
                model.setProduct_image(domain.getProductImage());
            if((Boolean) domain.getExtensionparams().get("currency_id_textdirtyflag"))
                model.setCurrency_id_text(domain.getCurrencyIdText());
            if((Boolean) domain.getExtensionparams().get("product_id_textdirtyflag"))
                model.setProduct_id_text(domain.getProductIdText());
            if((Boolean) domain.getExtensionparams().get("product_packaging_textdirtyflag"))
                model.setProduct_packaging_text(domain.getProductPackagingText());
            if((Boolean) domain.getExtensionparams().get("order_id_textdirtyflag"))
                model.setOrder_id_text(domain.getOrderIdText());
            if((Boolean) domain.getExtensionparams().get("event_id_textdirtyflag"))
                model.setEvent_id_text(domain.getEventIdText());
            if((Boolean) domain.getExtensionparams().get("salesman_id_textdirtyflag"))
                model.setSalesman_id_text(domain.getSalesmanIdText());
            if((Boolean) domain.getExtensionparams().get("product_uom_textdirtyflag"))
                model.setProduct_uom_text(domain.getProductUomText());
            if((Boolean) domain.getExtensionparams().get("write_uid_textdirtyflag"))
                model.setWrite_uid_text(domain.getWriteUidText());
            if((Boolean) domain.getExtensionparams().get("company_id_textdirtyflag"))
                model.setCompany_id_text(domain.getCompanyIdText());
            if((Boolean) domain.getExtensionparams().get("statedirtyflag"))
                model.setState(domain.getState());
            if((Boolean) domain.getExtensionparams().get("route_id_textdirtyflag"))
                model.setRoute_id_text(domain.getRouteIdText());
            if((Boolean) domain.getExtensionparams().get("salesman_iddirtyflag"))
                model.setSalesman_id(domain.getSalesmanId());
            if((Boolean) domain.getExtensionparams().get("currency_iddirtyflag"))
                model.setCurrency_id(domain.getCurrencyId());
            if((Boolean) domain.getExtensionparams().get("order_iddirtyflag"))
                model.setOrder_id(domain.getOrderId());
            if((Boolean) domain.getExtensionparams().get("event_iddirtyflag"))
                model.setEvent_id(domain.getEventId());
            if((Boolean) domain.getExtensionparams().get("linked_line_iddirtyflag"))
                model.setLinked_line_id(domain.getLinkedLineId());
            if((Boolean) domain.getExtensionparams().get("write_uiddirtyflag"))
                model.setWrite_uid(domain.getWriteUid());
            if((Boolean) domain.getExtensionparams().get("create_uiddirtyflag"))
                model.setCreate_uid(domain.getCreateUid());
            if((Boolean) domain.getExtensionparams().get("route_iddirtyflag"))
                model.setRoute_id(domain.getRouteId());
            if((Boolean) domain.getExtensionparams().get("order_partner_iddirtyflag"))
                model.setOrder_partner_id(domain.getOrderPartnerId());
            if((Boolean) domain.getExtensionparams().get("product_uomdirtyflag"))
                model.setProduct_uom(domain.getProductUom());
            if((Boolean) domain.getExtensionparams().get("product_iddirtyflag"))
                model.setProduct_id(domain.getProductId());
            if((Boolean) domain.getExtensionparams().get("company_iddirtyflag"))
                model.setCompany_id(domain.getCompanyId());
            if((Boolean) domain.getExtensionparams().get("product_packagingdirtyflag"))
                model.setProduct_packaging(domain.getProductPackaging());
            if((Boolean) domain.getExtensionparams().get("event_ticket_iddirtyflag"))
                model.setEvent_ticket_id(domain.getEventTicketId());
            domain.getFocusNull().clear();
        }
        return model ;
    }

    public Sale_order_line convert2Domain( sale_order_lineClientModel model ,Sale_order_line domain) {
        if(model == null)
            return domain ;
        if(domain == null){
            domain = new Sale_order_line();
        }

        if(model.getIs_expenseDirtyFlag())
            domain.setIsExpense(model.getIs_expense());
        if(model.getWrite_dateDirtyFlag())
            domain.setWriteDate(model.getWrite_date());
        if(model.getQty_to_invoiceDirtyFlag())
            domain.setQtyToInvoice(model.getQty_to_invoice());
        if(model.getAnalytic_tag_idsDirtyFlag())
            domain.setAnalyticTagIds(model.getAnalytic_tag_ids());
        if(model.getWarning_stockDirtyFlag())
            domain.setWarningStock(model.getWarning_stock());
        if(model.getIdDirtyFlag())
            domain.setId(model.getId());
        if(model.getDisplay_nameDirtyFlag())
            domain.setDisplayName(model.getDisplay_name());
        if(model.getPrice_reduce_taxexclDirtyFlag())
            domain.setPriceReduceTaxexcl(model.getPrice_reduce_taxexcl());
        if(model.getProduct_custom_attribute_value_idsDirtyFlag())
            domain.setProductCustomAttributeValueIds(model.getProduct_custom_attribute_value_ids());
        if(model.getQty_delivered_manualDirtyFlag())
            domain.setQtyDeliveredManual(model.getQty_delivered_manual());
        if(model.getPrice_taxDirtyFlag())
            domain.setPriceTax(model.getPrice_tax());
        if(model.getQty_delivered_methodDirtyFlag())
            domain.setQtyDeliveredMethod(model.getQty_delivered_method());
        if(model.getInvoice_statusDirtyFlag())
            domain.setInvoiceStatus(model.getInvoice_status());
        if(model.getDisplay_typeDirtyFlag())
            domain.setDisplayType(model.getDisplay_type());
        if(model.getMove_idsDirtyFlag())
            domain.setMoveIds(model.getMove_ids());
        if(model.getUntaxed_amount_invoicedDirtyFlag())
            domain.setUntaxedAmountInvoiced(model.getUntaxed_amount_invoiced());
        if(model.getProduct_no_variant_attribute_value_idsDirtyFlag())
            domain.setProductNoVariantAttributeValueIds(model.getProduct_no_variant_attribute_value_ids());
        if(model.get__last_updateDirtyFlag())
            domain.setLastUpdate(model.get__last_update());
        if(model.getIs_downpaymentDirtyFlag())
            domain.setIsDownpayment(model.getIs_downpayment());
        if(model.getAnalytic_line_idsDirtyFlag())
            domain.setAnalyticLineIds(model.getAnalytic_line_ids());
        if(model.getPrice_reduce_taxincDirtyFlag())
            domain.setPriceReduceTaxinc(model.getPrice_reduce_taxinc());
        if(model.getQty_deliveredDirtyFlag())
            domain.setQtyDelivered(model.getQty_delivered());
        if(model.getDiscountDirtyFlag())
            domain.setDiscount(model.getDiscount());
        if(model.getProduct_uom_qtyDirtyFlag())
            domain.setProductUomQty(model.getProduct_uom_qty());
        if(model.getPrice_reduceDirtyFlag())
            domain.setPriceReduce(model.getPrice_reduce());
        if(model.getCustomer_leadDirtyFlag())
            domain.setCustomerLead(model.getCustomer_lead());
        if(model.getProduct_updatableDirtyFlag())
            domain.setProductUpdatable(model.getProduct_updatable());
        if(model.getQty_invoicedDirtyFlag())
            domain.setQtyInvoiced(model.getQty_invoiced());
        if(model.getUntaxed_amount_to_invoiceDirtyFlag())
            domain.setUntaxedAmountToInvoice(model.getUntaxed_amount_to_invoice());
        if(model.getNameDirtyFlag())
            domain.setName(model.getName());
        if(model.getPrice_unitDirtyFlag())
            domain.setPriceUnit(model.getPrice_unit());
        if(model.getCreate_dateDirtyFlag())
            domain.setCreateDate(model.getCreate_date());
        if(model.getPurchase_line_countDirtyFlag())
            domain.setPurchaseLineCount(model.getPurchase_line_count());
        if(model.getName_shortDirtyFlag())
            domain.setNameShort(model.getName_short());
        if(model.getOption_line_idsDirtyFlag())
            domain.setOptionLineIds(model.getOption_line_ids());
        if(model.getTax_idDirtyFlag())
            domain.setTaxId(model.getTax_id());
        if(model.getSale_order_option_idsDirtyFlag())
            domain.setSaleOrderOptionIds(model.getSale_order_option_ids());
        if(model.getPrice_subtotalDirtyFlag())
            domain.setPriceSubtotal(model.getPrice_subtotal());
        if(model.getSequenceDirtyFlag())
            domain.setSequence(model.getSequence());
        if(model.getPrice_totalDirtyFlag())
            domain.setPriceTotal(model.getPrice_total());
        if(model.getInvoice_linesDirtyFlag())
            domain.setInvoiceLines(model.getInvoice_lines());
        if(model.getPurchase_line_idsDirtyFlag())
            domain.setPurchaseLineIds(model.getPurchase_line_ids());
        if(model.getEvent_okDirtyFlag())
            domain.setEventOk(model.getEvent_ok());
        if(model.getEvent_ticket_id_textDirtyFlag())
            domain.setEventTicketIdText(model.getEvent_ticket_id_text());
        if(model.getCreate_uid_textDirtyFlag())
            domain.setCreateUidText(model.getCreate_uid_text());
        if(model.getLinked_line_id_textDirtyFlag())
            domain.setLinkedLineIdText(model.getLinked_line_id_text());
        if(model.getOrder_partner_id_textDirtyFlag())
            domain.setOrderPartnerIdText(model.getOrder_partner_id_text());
        if(model.getProduct_imageDirtyFlag())
            domain.setProductImage(model.getProduct_image());
        if(model.getCurrency_id_textDirtyFlag())
            domain.setCurrencyIdText(model.getCurrency_id_text());
        if(model.getProduct_id_textDirtyFlag())
            domain.setProductIdText(model.getProduct_id_text());
        if(model.getProduct_packaging_textDirtyFlag())
            domain.setProductPackagingText(model.getProduct_packaging_text());
        if(model.getOrder_id_textDirtyFlag())
            domain.setOrderIdText(model.getOrder_id_text());
        if(model.getEvent_id_textDirtyFlag())
            domain.setEventIdText(model.getEvent_id_text());
        if(model.getSalesman_id_textDirtyFlag())
            domain.setSalesmanIdText(model.getSalesman_id_text());
        if(model.getProduct_uom_textDirtyFlag())
            domain.setProductUomText(model.getProduct_uom_text());
        if(model.getWrite_uid_textDirtyFlag())
            domain.setWriteUidText(model.getWrite_uid_text());
        if(model.getCompany_id_textDirtyFlag())
            domain.setCompanyIdText(model.getCompany_id_text());
        if(model.getStateDirtyFlag())
            domain.setState(model.getState());
        if(model.getRoute_id_textDirtyFlag())
            domain.setRouteIdText(model.getRoute_id_text());
        if(model.getSalesman_idDirtyFlag())
            domain.setSalesmanId(model.getSalesman_id());
        if(model.getCurrency_idDirtyFlag())
            domain.setCurrencyId(model.getCurrency_id());
        if(model.getOrder_idDirtyFlag())
            domain.setOrderId(model.getOrder_id());
        if(model.getEvent_idDirtyFlag())
            domain.setEventId(model.getEvent_id());
        if(model.getLinked_line_idDirtyFlag())
            domain.setLinkedLineId(model.getLinked_line_id());
        if(model.getWrite_uidDirtyFlag())
            domain.setWriteUid(model.getWrite_uid());
        if(model.getCreate_uidDirtyFlag())
            domain.setCreateUid(model.getCreate_uid());
        if(model.getRoute_idDirtyFlag())
            domain.setRouteId(model.getRoute_id());
        if(model.getOrder_partner_idDirtyFlag())
            domain.setOrderPartnerId(model.getOrder_partner_id());
        if(model.getProduct_uomDirtyFlag())
            domain.setProductUom(model.getProduct_uom());
        if(model.getProduct_idDirtyFlag())
            domain.setProductId(model.getProduct_id());
        if(model.getCompany_idDirtyFlag())
            domain.setCompanyId(model.getCompany_id());
        if(model.getProduct_packagingDirtyFlag())
            domain.setProductPackaging(model.getProduct_packaging());
        if(model.getEvent_ticket_idDirtyFlag())
            domain.setEventTicketId(model.getEvent_ticket_id());
        return domain ;
    }

}

    



