package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Sms_api;
import cn.ibizlab.odoo.core.odoo_sms.filter.Sms_apiSearchContext;

/**
 * 实体 [短信API] 存储对象
 */
public interface Sms_apiRepository extends Repository<Sms_api> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Sms_api> searchDefault(Sms_apiSearchContext context);

    Sms_api convert2PO(cn.ibizlab.odoo.core.odoo_sms.domain.Sms_api domain , Sms_api po) ;

    cn.ibizlab.odoo.core.odoo_sms.domain.Sms_api convert2Domain( Sms_api po ,cn.ibizlab.odoo.core.odoo_sms.domain.Sms_api domain) ;

}
