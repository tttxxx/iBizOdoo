package cn.ibizlab.odoo.core.odoo_im_livechat.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_im_livechat.domain.Im_livechat_channel;
import cn.ibizlab.odoo.core.odoo_im_livechat.filter.Im_livechat_channelSearchContext;
import cn.ibizlab.odoo.core.odoo_im_livechat.service.IIm_livechat_channelService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;



import cn.ibizlab.odoo.core.odoo_im_livechat.client.im_livechat_channelOdooClient;
import cn.ibizlab.odoo.core.odoo_im_livechat.clientmodel.im_livechat_channelClientModel;

/**
 * 实体[即时聊天] 服务对象接口实现
 */
@Slf4j
@Service
public class Im_livechat_channelServiceImpl implements IIm_livechat_channelService {

    @Autowired
    im_livechat_channelOdooClient im_livechat_channelOdooClient;


    @Override
    public Im_livechat_channel get(Integer id) {
        im_livechat_channelClientModel clientModel = new im_livechat_channelClientModel();
        clientModel.setId(id);
		im_livechat_channelOdooClient.get(clientModel);
        Im_livechat_channel et = convert2Domain(clientModel,null);
        if(et==null){
            et=new Im_livechat_channel();
            et.setId(id);
        }
        else{
        }

        return  et;
    }

    @Override
    public boolean update(Im_livechat_channel et) {
        im_livechat_channelClientModel clientModel = convert2Model(et,null);
		im_livechat_channelOdooClient.update(clientModel);
        Im_livechat_channel rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }
    
    public void updateBatch(List<Im_livechat_channel> list){
    }

    @Override
    public boolean remove(Integer id) {
        im_livechat_channelClientModel clientModel = new im_livechat_channelClientModel();
        clientModel.setId(id);
		im_livechat_channelOdooClient.remove(clientModel);
        return true; 
    }

    public void removeBatch(Collection<Integer> idList){
    }

    @Override
    public boolean create(Im_livechat_channel et) {
        im_livechat_channelClientModel clientModel = convert2Model(et,null);
		im_livechat_channelOdooClient.create(clientModel);
        Im_livechat_channel rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Im_livechat_channel> list){
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Im_livechat_channel> searchDefault(Im_livechat_channelSearchContext context) {
        List<Im_livechat_channel> list = new ArrayList<Im_livechat_channel>();
        Page<im_livechat_channelClientModel> clientModelList = im_livechat_channelOdooClient.search(context);
        for(im_livechat_channelClientModel clientModel : clientModelList){
            list.add( this.convert2Domain(clientModel,null));
        }
        return new PageImpl<Im_livechat_channel>(list, context.getPageable(), clientModelList.getTotalElements());
    }

    public im_livechat_channelClientModel convert2Model(Im_livechat_channel domain , im_livechat_channelClientModel model) {
        if(domain == null)
            return model ;
        if(model==null){
            model = new im_livechat_channelClientModel();

        }
        if((Boolean) domain.getExtensionparams().get("dirtyflagenable")){
            if((Boolean) domain.getExtensionparams().get("user_idsdirtyflag"))
                model.setUser_ids(domain.getUserIds());
            if((Boolean) domain.getExtensionparams().get("website_urldirtyflag"))
                model.setWebsite_url(domain.getWebsiteUrl());
            if((Boolean) domain.getExtensionparams().get("rule_idsdirtyflag"))
                model.setRule_ids(domain.getRuleIds());
            if((Boolean) domain.getExtensionparams().get("imagedirtyflag"))
                model.setImage(domain.getImage());
            if((Boolean) domain.getExtensionparams().get("web_pagedirtyflag"))
                model.setWeb_page(domain.getWebPage());
            if((Boolean) domain.getExtensionparams().get("is_publisheddirtyflag"))
                model.setIs_published(domain.getIsPublished());
            if((Boolean) domain.getExtensionparams().get("script_externaldirtyflag"))
                model.setScript_external(domain.getScriptExternal());
            if((Boolean) domain.getExtensionparams().get("nbr_channeldirtyflag"))
                model.setNbr_channel(domain.getNbrChannel());
            if((Boolean) domain.getExtensionparams().get("button_textdirtyflag"))
                model.setButton_text(domain.getButtonText());
            if((Boolean) domain.getExtensionparams().get("website_descriptiondirtyflag"))
                model.setWebsite_description(domain.getWebsiteDescription());
            if((Boolean) domain.getExtensionparams().get("channel_idsdirtyflag"))
                model.setChannel_ids(domain.getChannelIds());
            if((Boolean) domain.getExtensionparams().get("namedirtyflag"))
                model.setName(domain.getName());
            if((Boolean) domain.getExtensionparams().get("website_publisheddirtyflag"))
                model.setWebsite_published(domain.getWebsitePublished());
            if((Boolean) domain.getExtensionparams().get("__last_updatedirtyflag"))
                model.set__last_update(domain.getLastUpdate());
            if((Boolean) domain.getExtensionparams().get("create_datedirtyflag"))
                model.setCreate_date(domain.getCreateDate());
            if((Boolean) domain.getExtensionparams().get("display_namedirtyflag"))
                model.setDisplay_name(domain.getDisplayName());
            if((Boolean) domain.getExtensionparams().get("iddirtyflag"))
                model.setId(domain.getId());
            if((Boolean) domain.getExtensionparams().get("image_mediumdirtyflag"))
                model.setImage_medium(domain.getImageMedium());
            if((Boolean) domain.getExtensionparams().get("rating_percentage_satisfactiondirtyflag"))
                model.setRating_percentage_satisfaction(domain.getRatingPercentageSatisfaction());
            if((Boolean) domain.getExtensionparams().get("input_placeholderdirtyflag"))
                model.setInput_placeholder(domain.getInputPlaceholder());
            if((Boolean) domain.getExtensionparams().get("image_smalldirtyflag"))
                model.setImage_small(domain.getImageSmall());
            if((Boolean) domain.getExtensionparams().get("are_you_insidedirtyflag"))
                model.setAre_you_inside(domain.getAreYouInside());
            if((Boolean) domain.getExtensionparams().get("default_messagedirtyflag"))
                model.setDefault_message(domain.getDefaultMessage());
            if((Boolean) domain.getExtensionparams().get("write_datedirtyflag"))
                model.setWrite_date(domain.getWriteDate());
            if((Boolean) domain.getExtensionparams().get("write_uid_textdirtyflag"))
                model.setWrite_uid_text(domain.getWriteUidText());
            if((Boolean) domain.getExtensionparams().get("create_uid_textdirtyflag"))
                model.setCreate_uid_text(domain.getCreateUidText());
            if((Boolean) domain.getExtensionparams().get("write_uiddirtyflag"))
                model.setWrite_uid(domain.getWriteUid());
            if((Boolean) domain.getExtensionparams().get("create_uiddirtyflag"))
                model.setCreate_uid(domain.getCreateUid());
            domain.getFocusNull().clear();
        }
        return model ;
    }

    public Im_livechat_channel convert2Domain( im_livechat_channelClientModel model ,Im_livechat_channel domain) {
        if(model == null)
            return domain ;
        if(domain == null){
            domain = new Im_livechat_channel();
        }

        if(model.getUser_idsDirtyFlag())
            domain.setUserIds(model.getUser_ids());
        if(model.getWebsite_urlDirtyFlag())
            domain.setWebsiteUrl(model.getWebsite_url());
        if(model.getRule_idsDirtyFlag())
            domain.setRuleIds(model.getRule_ids());
        if(model.getImageDirtyFlag())
            domain.setImage(model.getImage());
        if(model.getWeb_pageDirtyFlag())
            domain.setWebPage(model.getWeb_page());
        if(model.getIs_publishedDirtyFlag())
            domain.setIsPublished(model.getIs_published());
        if(model.getScript_externalDirtyFlag())
            domain.setScriptExternal(model.getScript_external());
        if(model.getNbr_channelDirtyFlag())
            domain.setNbrChannel(model.getNbr_channel());
        if(model.getButton_textDirtyFlag())
            domain.setButtonText(model.getButton_text());
        if(model.getWebsite_descriptionDirtyFlag())
            domain.setWebsiteDescription(model.getWebsite_description());
        if(model.getChannel_idsDirtyFlag())
            domain.setChannelIds(model.getChannel_ids());
        if(model.getNameDirtyFlag())
            domain.setName(model.getName());
        if(model.getWebsite_publishedDirtyFlag())
            domain.setWebsitePublished(model.getWebsite_published());
        if(model.get__last_updateDirtyFlag())
            domain.setLastUpdate(model.get__last_update());
        if(model.getCreate_dateDirtyFlag())
            domain.setCreateDate(model.getCreate_date());
        if(model.getDisplay_nameDirtyFlag())
            domain.setDisplayName(model.getDisplay_name());
        if(model.getIdDirtyFlag())
            domain.setId(model.getId());
        if(model.getImage_mediumDirtyFlag())
            domain.setImageMedium(model.getImage_medium());
        if(model.getRating_percentage_satisfactionDirtyFlag())
            domain.setRatingPercentageSatisfaction(model.getRating_percentage_satisfaction());
        if(model.getInput_placeholderDirtyFlag())
            domain.setInputPlaceholder(model.getInput_placeholder());
        if(model.getImage_smallDirtyFlag())
            domain.setImageSmall(model.getImage_small());
        if(model.getAre_you_insideDirtyFlag())
            domain.setAreYouInside(model.getAre_you_inside());
        if(model.getDefault_messageDirtyFlag())
            domain.setDefaultMessage(model.getDefault_message());
        if(model.getWrite_dateDirtyFlag())
            domain.setWriteDate(model.getWrite_date());
        if(model.getWrite_uid_textDirtyFlag())
            domain.setWriteUidText(model.getWrite_uid_text());
        if(model.getCreate_uid_textDirtyFlag())
            domain.setCreateUidText(model.getCreate_uid_text());
        if(model.getWrite_uidDirtyFlag())
            domain.setWriteUid(model.getWrite_uid());
        if(model.getCreate_uidDirtyFlag())
            domain.setCreateUid(model.getCreate_uid());
        return domain ;
    }

}

    



