package cn.ibizlab.odoo.core.odoo_account.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_account.domain.Account_cashbox_line;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_cashbox_lineSearchContext;
import cn.ibizlab.odoo.core.odoo_account.service.IAccount_cashbox_lineService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;



import cn.ibizlab.odoo.core.odoo_account.client.account_cashbox_lineOdooClient;
import cn.ibizlab.odoo.core.odoo_account.clientmodel.account_cashbox_lineClientModel;

/**
 * 实体[钱箱明细] 服务对象接口实现
 */
@Slf4j
@Service
public class Account_cashbox_lineServiceImpl implements IAccount_cashbox_lineService {

    @Autowired
    account_cashbox_lineOdooClient account_cashbox_lineOdooClient;


    @Override
    public boolean create(Account_cashbox_line et) {
        account_cashbox_lineClientModel clientModel = convert2Model(et,null);
		account_cashbox_lineOdooClient.create(clientModel);
        Account_cashbox_line rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Account_cashbox_line> list){
    }

    @Override
    public boolean update(Account_cashbox_line et) {
        account_cashbox_lineClientModel clientModel = convert2Model(et,null);
		account_cashbox_lineOdooClient.update(clientModel);
        Account_cashbox_line rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }
    
    public void updateBatch(List<Account_cashbox_line> list){
    }

    @Override
    public boolean remove(Integer id) {
        account_cashbox_lineClientModel clientModel = new account_cashbox_lineClientModel();
        clientModel.setId(id);
		account_cashbox_lineOdooClient.remove(clientModel);
        return true; 
    }

    public void removeBatch(Collection<Integer> idList){
    }

    @Override
    public Account_cashbox_line get(Integer id) {
        account_cashbox_lineClientModel clientModel = new account_cashbox_lineClientModel();
        clientModel.setId(id);
		account_cashbox_lineOdooClient.get(clientModel);
        Account_cashbox_line et = convert2Domain(clientModel,null);
        if(et==null){
            et=new Account_cashbox_line();
            et.setId(id);
        }
        else{
        }

        return  et;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Account_cashbox_line> searchDefault(Account_cashbox_lineSearchContext context) {
        List<Account_cashbox_line> list = new ArrayList<Account_cashbox_line>();
        Page<account_cashbox_lineClientModel> clientModelList = account_cashbox_lineOdooClient.search(context);
        for(account_cashbox_lineClientModel clientModel : clientModelList){
            list.add( this.convert2Domain(clientModel,null));
        }
        return new PageImpl<Account_cashbox_line>(list, context.getPageable(), clientModelList.getTotalElements());
    }

    public account_cashbox_lineClientModel convert2Model(Account_cashbox_line domain , account_cashbox_lineClientModel model) {
        if(domain == null)
            return model ;
        if(model==null){
            model = new account_cashbox_lineClientModel();

        }
        if((Boolean) domain.getExtensionparams().get("dirtyflagenable")){
            if((Boolean) domain.getExtensionparams().get("default_pos_iddirtyflag"))
                model.setDefault_pos_id(domain.getDefaultPosId());
            if((Boolean) domain.getExtensionparams().get("subtotaldirtyflag"))
                model.setSubtotal(domain.getSubtotal());
            if((Boolean) domain.getExtensionparams().get("coin_valuedirtyflag"))
                model.setCoin_value(domain.getCoinValue());
            if((Boolean) domain.getExtensionparams().get("iddirtyflag"))
                model.setId(domain.getId());
            if((Boolean) domain.getExtensionparams().get("write_datedirtyflag"))
                model.setWrite_date(domain.getWriteDate());
            if((Boolean) domain.getExtensionparams().get("create_datedirtyflag"))
                model.setCreate_date(domain.getCreateDate());
            if((Boolean) domain.getExtensionparams().get("__last_updatedirtyflag"))
                model.set__last_update(domain.getLastUpdate());
            if((Boolean) domain.getExtensionparams().get("display_namedirtyflag"))
                model.setDisplay_name(domain.getDisplayName());
            if((Boolean) domain.getExtensionparams().get("numberdirtyflag"))
                model.setNumber(domain.getNumber());
            if((Boolean) domain.getExtensionparams().get("create_uid_textdirtyflag"))
                model.setCreate_uid_text(domain.getCreateUidText());
            if((Boolean) domain.getExtensionparams().get("write_uid_textdirtyflag"))
                model.setWrite_uid_text(domain.getWriteUidText());
            if((Boolean) domain.getExtensionparams().get("cashbox_iddirtyflag"))
                model.setCashbox_id(domain.getCashboxId());
            if((Boolean) domain.getExtensionparams().get("create_uiddirtyflag"))
                model.setCreate_uid(domain.getCreateUid());
            if((Boolean) domain.getExtensionparams().get("write_uiddirtyflag"))
                model.setWrite_uid(domain.getWriteUid());
            domain.getFocusNull().clear();
        }
        return model ;
    }

    public Account_cashbox_line convert2Domain( account_cashbox_lineClientModel model ,Account_cashbox_line domain) {
        if(model == null)
            return domain ;
        if(domain == null){
            domain = new Account_cashbox_line();
        }

        if(model.getDefault_pos_idDirtyFlag())
            domain.setDefaultPosId(model.getDefault_pos_id());
        if(model.getSubtotalDirtyFlag())
            domain.setSubtotal(model.getSubtotal());
        if(model.getCoin_valueDirtyFlag())
            domain.setCoinValue(model.getCoin_value());
        if(model.getIdDirtyFlag())
            domain.setId(model.getId());
        if(model.getWrite_dateDirtyFlag())
            domain.setWriteDate(model.getWrite_date());
        if(model.getCreate_dateDirtyFlag())
            domain.setCreateDate(model.getCreate_date());
        if(model.get__last_updateDirtyFlag())
            domain.setLastUpdate(model.get__last_update());
        if(model.getDisplay_nameDirtyFlag())
            domain.setDisplayName(model.getDisplay_name());
        if(model.getNumberDirtyFlag())
            domain.setNumber(model.getNumber());
        if(model.getCreate_uid_textDirtyFlag())
            domain.setCreateUidText(model.getCreate_uid_text());
        if(model.getWrite_uid_textDirtyFlag())
            domain.setWriteUidText(model.getWrite_uid_text());
        if(model.getCashbox_idDirtyFlag())
            domain.setCashboxId(model.getCashbox_id());
        if(model.getCreate_uidDirtyFlag())
            domain.setCreateUid(model.getCreate_uid());
        if(model.getWrite_uidDirtyFlag())
            domain.setWriteUid(model.getWrite_uid());
        return domain ;
    }

}

    



