package cn.ibizlab.odoo.core.repository.po ;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.math.BigInteger;

import cn.ibizlab.odoo.core.odoo_crm.filter.Crm_stageSearchContext;

/**
 * 实体 [CRM 阶段] 存储模型
 */
public interface Crm_stage{

    /**
     * 概率(%)
     */
    Double getProbability();

    void setProbability(Double probability);

    /**
     * 获取 [概率(%)]脏标记
     */
    boolean getProbabilityDirtyFlag();

    /**
     * 显示名称
     */
    String getDisplay_name();

    void setDisplay_name(String display_name);

    /**
     * 获取 [显示名称]脏标记
     */
    boolean getDisplay_nameDirtyFlag();

    /**
     * 要求
     */
    String getRequirements();

    void setRequirements(String requirements);

    /**
     * 获取 [要求]脏标记
     */
    boolean getRequirementsDirtyFlag();

    /**
     * 在漏斗中折叠
     */
    String getFold();

    void setFold(String fold);

    /**
     * 获取 [在漏斗中折叠]脏标记
     */
    boolean getFoldDirtyFlag();

    /**
     * 序号
     */
    Integer getSequence();

    void setSequence(Integer sequence);

    /**
     * 获取 [序号]脏标记
     */
    boolean getSequenceDirtyFlag();

    /**
     * 自动修改概率
     */
    String getOn_change();

    void setOn_change(String on_change);

    /**
     * 获取 [自动修改概率]脏标记
     */
    boolean getOn_changeDirtyFlag();

    /**
     * 最后更新时间
     */
    Timestamp getWrite_date();

    void setWrite_date(Timestamp write_date);

    /**
     * 获取 [最后更新时间]脏标记
     */
    boolean getWrite_dateDirtyFlag();

    /**
     * 优先级管理解释
     */
    String getLegend_priority();

    void setLegend_priority(String legend_priority);

    /**
     * 获取 [优先级管理解释]脏标记
     */
    boolean getLegend_priorityDirtyFlag();

    /**
     * 阶段名称
     */
    String getName();

    void setName(String name);

    /**
     * 获取 [阶段名称]脏标记
     */
    boolean getNameDirtyFlag();

    /**
     * ID
     */
    Integer getId();

    void setId(Integer id);

    /**
     * 获取 [ID]脏标记
     */
    boolean getIdDirtyFlag();

    /**
     * 创建时间
     */
    Timestamp getCreate_date();

    void setCreate_date(Timestamp create_date);

    /**
     * 获取 [创建时间]脏标记
     */
    boolean getCreate_dateDirtyFlag();

    /**
     * team_count
     */
    Integer getTeam_count();

    void setTeam_count(Integer team_count);

    /**
     * 获取 [team_count]脏标记
     */
    boolean getTeam_countDirtyFlag();

    /**
     * 最后修改日
     */
    Timestamp get__last_update();

    void set__last_update(Timestamp __last_update);

    /**
     * 获取 [最后修改日]脏标记
     */
    boolean get__last_updateDirtyFlag();

    /**
     * 创建人
     */
    String getCreate_uid_text();

    void setCreate_uid_text(String create_uid_text);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uid_textDirtyFlag();

    /**
     * 最后更新
     */
    String getWrite_uid_text();

    void setWrite_uid_text(String write_uid_text);

    /**
     * 获取 [最后更新]脏标记
     */
    boolean getWrite_uid_textDirtyFlag();

    /**
     * 销售团队
     */
    String getTeam_id_text();

    void setTeam_id_text(String team_id_text);

    /**
     * 获取 [销售团队]脏标记
     */
    boolean getTeam_id_textDirtyFlag();

    /**
     * 创建人
     */
    Integer getCreate_uid();

    void setCreate_uid(Integer create_uid);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uidDirtyFlag();

    /**
     * 销售团队
     */
    Integer getTeam_id();

    void setTeam_id(Integer team_id);

    /**
     * 获取 [销售团队]脏标记
     */
    boolean getTeam_idDirtyFlag();

    /**
     * 最后更新
     */
    Integer getWrite_uid();

    void setWrite_uid(Integer write_uid);

    /**
     * 获取 [最后更新]脏标记
     */
    boolean getWrite_uidDirtyFlag();

}
