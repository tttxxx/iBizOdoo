package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Event_event_ticket;
import cn.ibizlab.odoo.core.odoo_event.filter.Event_event_ticketSearchContext;

/**
 * 实体 [活动入场券] 存储对象
 */
public interface Event_event_ticketRepository extends Repository<Event_event_ticket> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Event_event_ticket> searchDefault(Event_event_ticketSearchContext context);

    Event_event_ticket convert2PO(cn.ibizlab.odoo.core.odoo_event.domain.Event_event_ticket domain , Event_event_ticket po) ;

    cn.ibizlab.odoo.core.odoo_event.domain.Event_event_ticket convert2Domain( Event_event_ticket po ,cn.ibizlab.odoo.core.odoo_event.domain.Event_event_ticket domain) ;

}
