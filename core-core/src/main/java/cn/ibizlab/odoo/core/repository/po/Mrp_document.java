package cn.ibizlab.odoo.core.repository.po ;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.math.BigInteger;

import cn.ibizlab.odoo.core.odoo_mrp.filter.Mrp_documentSearchContext;

/**
 * 实体 [生产文档] 存储模型
 */
public interface Mrp_document{

    /**
     * 创建时间
     */
    Timestamp getCreate_date();

    void setCreate_date(Timestamp create_date);

    /**
     * 获取 [创建时间]脏标记
     */
    boolean getCreate_dateDirtyFlag();

    /**
     * 有效
     */
    String getActive();

    void setActive(String active);

    /**
     * 获取 [有效]脏标记
     */
    boolean getActiveDirtyFlag();

    /**
     * 资源字段
     */
    String getRes_field();

    void setRes_field(String res_field);

    /**
     * 获取 [资源字段]脏标记
     */
    boolean getRes_fieldDirtyFlag();

    /**
     * 资源名称
     */
    String getRes_name();

    void setRes_name(String res_name);

    /**
     * 获取 [资源名称]脏标记
     */
    boolean getRes_nameDirtyFlag();

    /**
     * RES型号名称
     */
    String getRes_model_name();

    void setRes_model_name(String res_model_name);

    /**
     * 获取 [RES型号名称]脏标记
     */
    boolean getRes_model_nameDirtyFlag();

    /**
     * 文件名
     */
    String getDatas_fname();

    void setDatas_fname(String datas_fname);

    /**
     * 获取 [文件名]脏标记
     */
    boolean getDatas_fnameDirtyFlag();

    /**
     * 主题模板
     */
    Integer getTheme_template_id();

    void setTheme_template_id(Integer theme_template_id);

    /**
     * 获取 [主题模板]脏标记
     */
    boolean getTheme_template_idDirtyFlag();

    /**
     * MIME 类型
     */
    String getMimetype();

    void setMimetype(String mimetype);

    /**
     * 获取 [MIME 类型]脏标记
     */
    boolean getMimetypeDirtyFlag();

    /**
     * 资源ID
     */
    Integer getRes_id();

    void setRes_id(Integer res_id);

    /**
     * 获取 [资源ID]脏标记
     */
    boolean getRes_idDirtyFlag();

    /**
     * 存储的文件名
     */
    String getStore_fname();

    void setStore_fname(String store_fname);

    /**
     * 获取 [存储的文件名]脏标记
     */
    boolean getStore_fnameDirtyFlag();

    /**
     * 最后更新时间
     */
    Timestamp getWrite_date();

    void setWrite_date(Timestamp write_date);

    /**
     * 获取 [最后更新时间]脏标记
     */
    boolean getWrite_dateDirtyFlag();

    /**
     * 附件网址
     */
    String getLocal_url();

    void setLocal_url(String local_url);

    /**
     * 获取 [附件网址]脏标记
     */
    boolean getLocal_urlDirtyFlag();

    /**
     * 键
     */
    String getKey();

    void setKey(String key);

    /**
     * 获取 [键]脏标记
     */
    boolean getKeyDirtyFlag();

    /**
     * 是公开文档
     */
    String getIbizpublic();

    void setIbizpublic(String ibizpublic);

    /**
     * 获取 [是公开文档]脏标记
     */
    boolean getIbizpublicDirtyFlag();

    /**
     * 资源模型
     */
    String getRes_model();

    void setRes_model(String res_model);

    /**
     * 获取 [资源模型]脏标记
     */
    boolean getRes_modelDirtyFlag();

    /**
     * 最后修改日
     */
    Timestamp get__last_update();

    void set__last_update(Timestamp __last_update);

    /**
     * 获取 [最后修改日]脏标记
     */
    boolean get__last_updateDirtyFlag();

    /**
     * 略所图
     */
    byte[] getThumbnail();

    void setThumbnail(byte[] thumbnail);

    /**
     * 获取 [略所图]脏标记
     */
    boolean getThumbnailDirtyFlag();

    /**
     * Url网址
     */
    String getUrl();

    void setUrl(String url);

    /**
     * 获取 [Url网址]脏标记
     */
    boolean getUrlDirtyFlag();

    /**
     * 文件大小
     */
    Integer getFile_size();

    void setFile_size(Integer file_size);

    /**
     * 获取 [文件大小]脏标记
     */
    boolean getFile_sizeDirtyFlag();

    /**
     * 访问令牌
     */
    String getAccess_token();

    void setAccess_token(String access_token);

    /**
     * 获取 [访问令牌]脏标记
     */
    boolean getAccess_tokenDirtyFlag();

    /**
     * 公司
     */
    Integer getCompany_id();

    void setCompany_id(Integer company_id);

    /**
     * 获取 [公司]脏标记
     */
    boolean getCompany_idDirtyFlag();

    /**
     * 相关的附件
     */
    Integer getIr_attachment_id();

    void setIr_attachment_id(Integer ir_attachment_id);

    /**
     * 获取 [相关的附件]脏标记
     */
    boolean getIr_attachment_idDirtyFlag();

    /**
     * ID
     */
    Integer getId();

    void setId(Integer id);

    /**
     * 获取 [ID]脏标记
     */
    boolean getIdDirtyFlag();

    /**
     * 类型
     */
    String getType();

    void setType(String type);

    /**
     * 获取 [类型]脏标记
     */
    boolean getTypeDirtyFlag();

    /**
     * 校验和/SHA1
     */
    String getChecksum();

    void setChecksum(String checksum);

    /**
     * 获取 [校验和/SHA1]脏标记
     */
    boolean getChecksumDirtyFlag();

    /**
     * 数据库数据
     */
    byte[] getDb_datas();

    void setDb_datas(byte[] db_datas);

    /**
     * 获取 [数据库数据]脏标记
     */
    boolean getDb_datasDirtyFlag();

    /**
     * 索引的内容
     */
    String getIndex_content();

    void setIndex_content(String index_content);

    /**
     * 获取 [索引的内容]脏标记
     */
    boolean getIndex_contentDirtyFlag();

    /**
     * 说明
     */
    String getDescription();

    void setDescription(String description);

    /**
     * 获取 [说明]脏标记
     */
    boolean getDescriptionDirtyFlag();

    /**
     * 名称
     */
    String getName();

    void setName(String name);

    /**
     * 获取 [名称]脏标记
     */
    boolean getNameDirtyFlag();

    /**
     * 显示名称
     */
    String getDisplay_name();

    void setDisplay_name(String display_name);

    /**
     * 获取 [显示名称]脏标记
     */
    boolean getDisplay_nameDirtyFlag();

    /**
     * 网站网址
     */
    String getWebsite_url();

    void setWebsite_url(String website_url);

    /**
     * 获取 [网站网址]脏标记
     */
    boolean getWebsite_urlDirtyFlag();

    /**
     * 网站
     */
    Integer getWebsite_id();

    void setWebsite_id(Integer website_id);

    /**
     * 获取 [网站]脏标记
     */
    boolean getWebsite_idDirtyFlag();

    /**
     * 文件内容
     */
    byte[] getDatas();

    void setDatas(byte[] datas);

    /**
     * 获取 [文件内容]脏标记
     */
    boolean getDatasDirtyFlag();

    /**
     * 优先级
     */
    String getPriority();

    void setPriority(String priority);

    /**
     * 获取 [优先级]脏标记
     */
    boolean getPriorityDirtyFlag();

    /**
     * 最后更新者
     */
    String getWrite_uid_text();

    void setWrite_uid_text(String write_uid_text);

    /**
     * 获取 [最后更新者]脏标记
     */
    boolean getWrite_uid_textDirtyFlag();

    /**
     * 创建人
     */
    String getCreate_uid_text();

    void setCreate_uid_text(String create_uid_text);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uid_textDirtyFlag();

    /**
     * 创建人
     */
    Integer getCreate_uid();

    void setCreate_uid(Integer create_uid);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uidDirtyFlag();

    /**
     * 最后更新者
     */
    Integer getWrite_uid();

    void setWrite_uid(Integer write_uid);

    /**
     * 获取 [最后更新者]脏标记
     */
    boolean getWrite_uidDirtyFlag();

}
