package cn.ibizlab.odoo.core.repository.po ;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.math.BigInteger;

import cn.ibizlab.odoo.core.odoo_im_livechat.filter.Im_livechat_report_channelSearchContext;

/**
 * 实体 [实时聊天支持频道报告] 存储模型
 */
public interface Im_livechat_report_channel{

    /**
     * 显示名称
     */
    String getDisplay_name();

    void setDisplay_name(String display_name);

    /**
     * 获取 [显示名称]脏标记
     */
    boolean getDisplay_nameDirtyFlag();

    /**
     * 最后修改日
     */
    Timestamp get__last_update();

    void set__last_update(Timestamp __last_update);

    /**
     * 获取 [最后修改日]脏标记
     */
    boolean get__last_updateDirtyFlag();

    /**
     * 平均时间
     */
    Double getDuration();

    void setDuration(Double duration);

    /**
     * 获取 [平均时间]脏标记
     */
    boolean getDurationDirtyFlag();

    /**
     * 会话的开始日期
     */
    Timestamp getStart_date();

    void setStart_date(Timestamp start_date);

    /**
     * 获取 [会话的开始日期]脏标记
     */
    boolean getStart_dateDirtyFlag();

    /**
     * 会话开始的小时数
     */
    String getStart_date_hour();

    void setStart_date_hour(String start_date_hour);

    /**
     * 获取 [会话开始的小时数]脏标记
     */
    boolean getStart_date_hourDirtyFlag();

    /**
     * ID
     */
    Integer getId();

    void setId(Integer id);

    /**
     * 获取 [ID]脏标记
     */
    boolean getIdDirtyFlag();

    /**
     * 渠道名称
     */
    String getChannel_name();

    void setChannel_name(String channel_name);

    /**
     * 获取 [渠道名称]脏标记
     */
    boolean getChannel_nameDirtyFlag();

    /**
     * 每个消息
     */
    Integer getNbr_message();

    void setNbr_message(Integer nbr_message);

    /**
     * 获取 [每个消息]脏标记
     */
    boolean getNbr_messageDirtyFlag();

    /**
     * UUID
     */
    String getUuid();

    void setUuid(String uuid);

    /**
     * 获取 [UUID]脏标记
     */
    boolean getUuidDirtyFlag();

    /**
     * 代号
     */
    String getTechnical_name();

    void setTechnical_name(String technical_name);

    /**
     * 获取 [代号]脏标记
     */
    boolean getTechnical_nameDirtyFlag();

    /**
     * 讲解人
     */
    Integer getNbr_speaker();

    void setNbr_speaker(Integer nbr_speaker);

    /**
     * 获取 [讲解人]脏标记
     */
    boolean getNbr_speakerDirtyFlag();

    /**
     * 渠道
     */
    String getLivechat_channel_id_text();

    void setLivechat_channel_id_text(String livechat_channel_id_text);

    /**
     * 获取 [渠道]脏标记
     */
    boolean getLivechat_channel_id_textDirtyFlag();

    /**
     * 运算符
     */
    String getPartner_id_text();

    void setPartner_id_text(String partner_id_text);

    /**
     * 获取 [运算符]脏标记
     */
    boolean getPartner_id_textDirtyFlag();

    /**
     * 对话
     */
    String getChannel_id_text();

    void setChannel_id_text(String channel_id_text);

    /**
     * 获取 [对话]脏标记
     */
    boolean getChannel_id_textDirtyFlag();

    /**
     * 对话
     */
    Integer getChannel_id();

    void setChannel_id(Integer channel_id);

    /**
     * 获取 [对话]脏标记
     */
    boolean getChannel_idDirtyFlag();

    /**
     * 渠道
     */
    Integer getLivechat_channel_id();

    void setLivechat_channel_id(Integer livechat_channel_id);

    /**
     * 获取 [渠道]脏标记
     */
    boolean getLivechat_channel_idDirtyFlag();

    /**
     * 运算符
     */
    Integer getPartner_id();

    void setPartner_id(Integer partner_id);

    /**
     * 获取 [运算符]脏标记
     */
    boolean getPartner_idDirtyFlag();

}
