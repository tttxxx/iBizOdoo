package cn.ibizlab.odoo.core.repository.client.po;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigDecimal;

/**
 * [product_attribute_custom_value] 对象
 */
public interface product_attribute_custom_value {

    public Integer getAttribute_value_id();

    public void setAttribute_value_id(Integer attribute_value_id);

    public String getAttribute_value_id_text();

    public void setAttribute_value_id_text(String attribute_value_id_text);

    public Timestamp getCreate_date();

    public void setCreate_date(Timestamp create_date);

    public Integer getCreate_uid();

    public void setCreate_uid(Integer create_uid);

    public String getCreate_uid_text();

    public void setCreate_uid_text(String create_uid_text);

    public String getCustom_value();

    public void setCustom_value(String custom_value);

    public String getDisplay_name();

    public void setDisplay_name(String display_name);

    public Integer getId();

    public void setId(Integer id);

    public Integer getSale_order_line_id();

    public void setSale_order_line_id(Integer sale_order_line_id);

    public String getSale_order_line_id_text();

    public void setSale_order_line_id_text(String sale_order_line_id_text);

    public Timestamp getWrite_date();

    public void setWrite_date(Timestamp write_date);

    public Integer getWrite_uid();

    public void setWrite_uid(Integer write_uid);

    public String getWrite_uid_text();

    public void setWrite_uid_text(String write_uid_text);

    public Timestamp get__last_update();

    public void set__last_update(Timestamp __last_update);

}
