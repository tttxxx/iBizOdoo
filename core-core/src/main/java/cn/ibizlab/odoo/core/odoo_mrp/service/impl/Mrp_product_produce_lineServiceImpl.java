package cn.ibizlab.odoo.core.odoo_mrp.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_mrp.domain.Mrp_product_produce_line;
import cn.ibizlab.odoo.core.odoo_mrp.filter.Mrp_product_produce_lineSearchContext;
import cn.ibizlab.odoo.core.odoo_mrp.service.IMrp_product_produce_lineService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;



import cn.ibizlab.odoo.core.odoo_mrp.client.mrp_product_produce_lineOdooClient;
import cn.ibizlab.odoo.core.odoo_mrp.clientmodel.mrp_product_produce_lineClientModel;

/**
 * 实体[记录生产明细] 服务对象接口实现
 */
@Slf4j
@Service
public class Mrp_product_produce_lineServiceImpl implements IMrp_product_produce_lineService {

    @Autowired
    mrp_product_produce_lineOdooClient mrp_product_produce_lineOdooClient;


    @Override
    public boolean remove(Integer id) {
        mrp_product_produce_lineClientModel clientModel = new mrp_product_produce_lineClientModel();
        clientModel.setId(id);
		mrp_product_produce_lineOdooClient.remove(clientModel);
        return true; 
    }

    public void removeBatch(Collection<Integer> idList){
    }

    @Override
    public Mrp_product_produce_line get(Integer id) {
        mrp_product_produce_lineClientModel clientModel = new mrp_product_produce_lineClientModel();
        clientModel.setId(id);
		mrp_product_produce_lineOdooClient.get(clientModel);
        Mrp_product_produce_line et = convert2Domain(clientModel,null);
        if(et==null){
            et=new Mrp_product_produce_line();
            et.setId(id);
        }
        else{
        }

        return  et;
    }

    @Override
    public boolean update(Mrp_product_produce_line et) {
        mrp_product_produce_lineClientModel clientModel = convert2Model(et,null);
		mrp_product_produce_lineOdooClient.update(clientModel);
        Mrp_product_produce_line rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }
    
    public void updateBatch(List<Mrp_product_produce_line> list){
    }

    @Override
    public boolean create(Mrp_product_produce_line et) {
        mrp_product_produce_lineClientModel clientModel = convert2Model(et,null);
		mrp_product_produce_lineOdooClient.create(clientModel);
        Mrp_product_produce_line rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Mrp_product_produce_line> list){
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Mrp_product_produce_line> searchDefault(Mrp_product_produce_lineSearchContext context) {
        List<Mrp_product_produce_line> list = new ArrayList<Mrp_product_produce_line>();
        Page<mrp_product_produce_lineClientModel> clientModelList = mrp_product_produce_lineOdooClient.search(context);
        for(mrp_product_produce_lineClientModel clientModel : clientModelList){
            list.add( this.convert2Domain(clientModel,null));
        }
        return new PageImpl<Mrp_product_produce_line>(list, context.getPageable(), clientModelList.getTotalElements());
    }

    public mrp_product_produce_lineClientModel convert2Model(Mrp_product_produce_line domain , mrp_product_produce_lineClientModel model) {
        if(domain == null)
            return model ;
        if(model==null){
            model = new mrp_product_produce_lineClientModel();

        }
        if((Boolean) domain.getExtensionparams().get("dirtyflagenable")){
            if((Boolean) domain.getExtensionparams().get("create_datedirtyflag"))
                model.setCreate_date(domain.getCreateDate());
            if((Boolean) domain.getExtensionparams().get("iddirtyflag"))
                model.setId(domain.getId());
            if((Boolean) domain.getExtensionparams().get("qty_to_consumedirtyflag"))
                model.setQty_to_consume(domain.getQtyToConsume());
            if((Boolean) domain.getExtensionparams().get("write_datedirtyflag"))
                model.setWrite_date(domain.getWriteDate());
            if((Boolean) domain.getExtensionparams().get("display_namedirtyflag"))
                model.setDisplay_name(domain.getDisplayName());
            if((Boolean) domain.getExtensionparams().get("qty_donedirtyflag"))
                model.setQty_done(domain.getQtyDone());
            if((Boolean) domain.getExtensionparams().get("qty_reserveddirtyflag"))
                model.setQty_reserved(domain.getQtyReserved());
            if((Boolean) domain.getExtensionparams().get("__last_updatedirtyflag"))
                model.set__last_update(domain.getLastUpdate());
            if((Boolean) domain.getExtensionparams().get("product_id_textdirtyflag"))
                model.setProduct_id_text(domain.getProductIdText());
            if((Boolean) domain.getExtensionparams().get("write_uid_textdirtyflag"))
                model.setWrite_uid_text(domain.getWriteUidText());
            if((Boolean) domain.getExtensionparams().get("lot_id_textdirtyflag"))
                model.setLot_id_text(domain.getLotIdText());
            if((Boolean) domain.getExtensionparams().get("create_uid_textdirtyflag"))
                model.setCreate_uid_text(domain.getCreateUidText());
            if((Boolean) domain.getExtensionparams().get("product_uom_id_textdirtyflag"))
                model.setProduct_uom_id_text(domain.getProductUomIdText());
            if((Boolean) domain.getExtensionparams().get("product_trackingdirtyflag"))
                model.setProduct_tracking(domain.getProductTracking());
            if((Boolean) domain.getExtensionparams().get("move_id_textdirtyflag"))
                model.setMove_id_text(domain.getMoveIdText());
            if((Boolean) domain.getExtensionparams().get("move_iddirtyflag"))
                model.setMove_id(domain.getMoveId());
            if((Boolean) domain.getExtensionparams().get("product_iddirtyflag"))
                model.setProduct_id(domain.getProductId());
            if((Boolean) domain.getExtensionparams().get("lot_iddirtyflag"))
                model.setLot_id(domain.getLotId());
            if((Boolean) domain.getExtensionparams().get("product_produce_iddirtyflag"))
                model.setProduct_produce_id(domain.getProductProduceId());
            if((Boolean) domain.getExtensionparams().get("create_uiddirtyflag"))
                model.setCreate_uid(domain.getCreateUid());
            if((Boolean) domain.getExtensionparams().get("product_uom_iddirtyflag"))
                model.setProduct_uom_id(domain.getProductUomId());
            if((Boolean) domain.getExtensionparams().get("write_uiddirtyflag"))
                model.setWrite_uid(domain.getWriteUid());
            domain.getFocusNull().clear();
        }
        return model ;
    }

    public Mrp_product_produce_line convert2Domain( mrp_product_produce_lineClientModel model ,Mrp_product_produce_line domain) {
        if(model == null)
            return domain ;
        if(domain == null){
            domain = new Mrp_product_produce_line();
        }

        if(model.getCreate_dateDirtyFlag())
            domain.setCreateDate(model.getCreate_date());
        if(model.getIdDirtyFlag())
            domain.setId(model.getId());
        if(model.getQty_to_consumeDirtyFlag())
            domain.setQtyToConsume(model.getQty_to_consume());
        if(model.getWrite_dateDirtyFlag())
            domain.setWriteDate(model.getWrite_date());
        if(model.getDisplay_nameDirtyFlag())
            domain.setDisplayName(model.getDisplay_name());
        if(model.getQty_doneDirtyFlag())
            domain.setQtyDone(model.getQty_done());
        if(model.getQty_reservedDirtyFlag())
            domain.setQtyReserved(model.getQty_reserved());
        if(model.get__last_updateDirtyFlag())
            domain.setLastUpdate(model.get__last_update());
        if(model.getProduct_id_textDirtyFlag())
            domain.setProductIdText(model.getProduct_id_text());
        if(model.getWrite_uid_textDirtyFlag())
            domain.setWriteUidText(model.getWrite_uid_text());
        if(model.getLot_id_textDirtyFlag())
            domain.setLotIdText(model.getLot_id_text());
        if(model.getCreate_uid_textDirtyFlag())
            domain.setCreateUidText(model.getCreate_uid_text());
        if(model.getProduct_uom_id_textDirtyFlag())
            domain.setProductUomIdText(model.getProduct_uom_id_text());
        if(model.getProduct_trackingDirtyFlag())
            domain.setProductTracking(model.getProduct_tracking());
        if(model.getMove_id_textDirtyFlag())
            domain.setMoveIdText(model.getMove_id_text());
        if(model.getMove_idDirtyFlag())
            domain.setMoveId(model.getMove_id());
        if(model.getProduct_idDirtyFlag())
            domain.setProductId(model.getProduct_id());
        if(model.getLot_idDirtyFlag())
            domain.setLotId(model.getLot_id());
        if(model.getProduct_produce_idDirtyFlag())
            domain.setProductProduceId(model.getProduct_produce_id());
        if(model.getCreate_uidDirtyFlag())
            domain.setCreateUid(model.getCreate_uid());
        if(model.getProduct_uom_idDirtyFlag())
            domain.setProductUomId(model.getProduct_uom_id());
        if(model.getWrite_uidDirtyFlag())
            domain.setWriteUid(model.getWrite_uid());
        return domain ;
    }

}

    



