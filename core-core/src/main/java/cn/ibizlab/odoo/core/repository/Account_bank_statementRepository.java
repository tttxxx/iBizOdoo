package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Account_bank_statement;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_bank_statementSearchContext;

/**
 * 实体 [银行对账单] 存储对象
 */
public interface Account_bank_statementRepository extends Repository<Account_bank_statement> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Account_bank_statement> searchDefault(Account_bank_statementSearchContext context);

    Account_bank_statement convert2PO(cn.ibizlab.odoo.core.odoo_account.domain.Account_bank_statement domain , Account_bank_statement po) ;

    cn.ibizlab.odoo.core.odoo_account.domain.Account_bank_statement convert2Domain( Account_bank_statement po ,cn.ibizlab.odoo.core.odoo_account.domain.Account_bank_statement domain) ;

}
