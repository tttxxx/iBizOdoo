package cn.ibizlab.odoo.core.odoo_account.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_account.domain.Account_invoice_tax;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_invoice_taxSearchContext;
import cn.ibizlab.odoo.core.odoo_account.service.IAccount_invoice_taxService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;



import cn.ibizlab.odoo.core.odoo_account.client.account_invoice_taxOdooClient;
import cn.ibizlab.odoo.core.odoo_account.clientmodel.account_invoice_taxClientModel;

/**
 * 实体[发票税率] 服务对象接口实现
 */
@Slf4j
@Service
public class Account_invoice_taxServiceImpl implements IAccount_invoice_taxService {

    @Autowired
    account_invoice_taxOdooClient account_invoice_taxOdooClient;


    @Override
    public boolean update(Account_invoice_tax et) {
        account_invoice_taxClientModel clientModel = convert2Model(et,null);
		account_invoice_taxOdooClient.update(clientModel);
        Account_invoice_tax rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }
    
    public void updateBatch(List<Account_invoice_tax> list){
    }

    @Override
    public Account_invoice_tax get(Integer id) {
        account_invoice_taxClientModel clientModel = new account_invoice_taxClientModel();
        clientModel.setId(id);
		account_invoice_taxOdooClient.get(clientModel);
        Account_invoice_tax et = convert2Domain(clientModel,null);
        if(et==null){
            et=new Account_invoice_tax();
            et.setId(id);
        }
        else{
        }

        return  et;
    }

    @Override
    public boolean create(Account_invoice_tax et) {
        account_invoice_taxClientModel clientModel = convert2Model(et,null);
		account_invoice_taxOdooClient.create(clientModel);
        Account_invoice_tax rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Account_invoice_tax> list){
    }

    @Override
    public boolean remove(Integer id) {
        account_invoice_taxClientModel clientModel = new account_invoice_taxClientModel();
        clientModel.setId(id);
		account_invoice_taxOdooClient.remove(clientModel);
        return true; 
    }

    public void removeBatch(Collection<Integer> idList){
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Account_invoice_tax> searchDefault(Account_invoice_taxSearchContext context) {
        List<Account_invoice_tax> list = new ArrayList<Account_invoice_tax>();
        Page<account_invoice_taxClientModel> clientModelList = account_invoice_taxOdooClient.search(context);
        for(account_invoice_taxClientModel clientModel : clientModelList){
            list.add( this.convert2Domain(clientModel,null));
        }
        return new PageImpl<Account_invoice_tax>(list, context.getPageable(), clientModelList.getTotalElements());
    }

    public account_invoice_taxClientModel convert2Model(Account_invoice_tax domain , account_invoice_taxClientModel model) {
        if(domain == null)
            return model ;
        if(model==null){
            model = new account_invoice_taxClientModel();

        }
        if((Boolean) domain.getExtensionparams().get("dirtyflagenable")){
            if((Boolean) domain.getExtensionparams().get("manualdirtyflag"))
                model.setManual(domain.getManual());
            if((Boolean) domain.getExtensionparams().get("amount_totaldirtyflag"))
                model.setAmount_total(domain.getAmountTotal());
            if((Boolean) domain.getExtensionparams().get("create_datedirtyflag"))
                model.setCreate_date(domain.getCreateDate());
            if((Boolean) domain.getExtensionparams().get("basedirtyflag"))
                model.setBase(domain.getBase());
            if((Boolean) domain.getExtensionparams().get("__last_updatedirtyflag"))
                model.set__last_update(domain.getLastUpdate());
            if((Boolean) domain.getExtensionparams().get("iddirtyflag"))
                model.setId(domain.getId());
            if((Boolean) domain.getExtensionparams().get("display_namedirtyflag"))
                model.setDisplay_name(domain.getDisplayName());
            if((Boolean) domain.getExtensionparams().get("amountdirtyflag"))
                model.setAmount(domain.getAmount());
            if((Boolean) domain.getExtensionparams().get("namedirtyflag"))
                model.setName(domain.getName());
            if((Boolean) domain.getExtensionparams().get("sequencedirtyflag"))
                model.setSequence(domain.getSequence());
            if((Boolean) domain.getExtensionparams().get("write_datedirtyflag"))
                model.setWrite_date(domain.getWriteDate());
            if((Boolean) domain.getExtensionparams().get("analytic_tag_idsdirtyflag"))
                model.setAnalytic_tag_ids(domain.getAnalyticTagIds());
            if((Boolean) domain.getExtensionparams().get("amount_roundingdirtyflag"))
                model.setAmount_rounding(domain.getAmountRounding());
            if((Boolean) domain.getExtensionparams().get("tax_id_textdirtyflag"))
                model.setTax_id_text(domain.getTaxIdText());
            if((Boolean) domain.getExtensionparams().get("create_uid_textdirtyflag"))
                model.setCreate_uid_text(domain.getCreateUidText());
            if((Boolean) domain.getExtensionparams().get("invoice_id_textdirtyflag"))
                model.setInvoice_id_text(domain.getInvoiceIdText());
            if((Boolean) domain.getExtensionparams().get("currency_id_textdirtyflag"))
                model.setCurrency_id_text(domain.getCurrencyIdText());
            if((Boolean) domain.getExtensionparams().get("write_uid_textdirtyflag"))
                model.setWrite_uid_text(domain.getWriteUidText());
            if((Boolean) domain.getExtensionparams().get("company_id_textdirtyflag"))
                model.setCompany_id_text(domain.getCompanyIdText());
            if((Boolean) domain.getExtensionparams().get("account_analytic_id_textdirtyflag"))
                model.setAccount_analytic_id_text(domain.getAccountAnalyticIdText());
            if((Boolean) domain.getExtensionparams().get("account_id_textdirtyflag"))
                model.setAccount_id_text(domain.getAccountIdText());
            if((Boolean) domain.getExtensionparams().get("create_uiddirtyflag"))
                model.setCreate_uid(domain.getCreateUid());
            if((Boolean) domain.getExtensionparams().get("account_iddirtyflag"))
                model.setAccount_id(domain.getAccountId());
            if((Boolean) domain.getExtensionparams().get("invoice_iddirtyflag"))
                model.setInvoice_id(domain.getInvoiceId());
            if((Boolean) domain.getExtensionparams().get("currency_iddirtyflag"))
                model.setCurrency_id(domain.getCurrencyId());
            if((Boolean) domain.getExtensionparams().get("account_analytic_iddirtyflag"))
                model.setAccount_analytic_id(domain.getAccountAnalyticId());
            if((Boolean) domain.getExtensionparams().get("write_uiddirtyflag"))
                model.setWrite_uid(domain.getWriteUid());
            if((Boolean) domain.getExtensionparams().get("company_iddirtyflag"))
                model.setCompany_id(domain.getCompanyId());
            if((Boolean) domain.getExtensionparams().get("tax_iddirtyflag"))
                model.setTax_id(domain.getTaxId());
            domain.getFocusNull().clear();
        }
        return model ;
    }

    public Account_invoice_tax convert2Domain( account_invoice_taxClientModel model ,Account_invoice_tax domain) {
        if(model == null)
            return domain ;
        if(domain == null){
            domain = new Account_invoice_tax();
        }

        if(model.getManualDirtyFlag())
            domain.setManual(model.getManual());
        if(model.getAmount_totalDirtyFlag())
            domain.setAmountTotal(model.getAmount_total());
        if(model.getCreate_dateDirtyFlag())
            domain.setCreateDate(model.getCreate_date());
        if(model.getBaseDirtyFlag())
            domain.setBase(model.getBase());
        if(model.get__last_updateDirtyFlag())
            domain.setLastUpdate(model.get__last_update());
        if(model.getIdDirtyFlag())
            domain.setId(model.getId());
        if(model.getDisplay_nameDirtyFlag())
            domain.setDisplayName(model.getDisplay_name());
        if(model.getAmountDirtyFlag())
            domain.setAmount(model.getAmount());
        if(model.getNameDirtyFlag())
            domain.setName(model.getName());
        if(model.getSequenceDirtyFlag())
            domain.setSequence(model.getSequence());
        if(model.getWrite_dateDirtyFlag())
            domain.setWriteDate(model.getWrite_date());
        if(model.getAnalytic_tag_idsDirtyFlag())
            domain.setAnalyticTagIds(model.getAnalytic_tag_ids());
        if(model.getAmount_roundingDirtyFlag())
            domain.setAmountRounding(model.getAmount_rounding());
        if(model.getTax_id_textDirtyFlag())
            domain.setTaxIdText(model.getTax_id_text());
        if(model.getCreate_uid_textDirtyFlag())
            domain.setCreateUidText(model.getCreate_uid_text());
        if(model.getInvoice_id_textDirtyFlag())
            domain.setInvoiceIdText(model.getInvoice_id_text());
        if(model.getCurrency_id_textDirtyFlag())
            domain.setCurrencyIdText(model.getCurrency_id_text());
        if(model.getWrite_uid_textDirtyFlag())
            domain.setWriteUidText(model.getWrite_uid_text());
        if(model.getCompany_id_textDirtyFlag())
            domain.setCompanyIdText(model.getCompany_id_text());
        if(model.getAccount_analytic_id_textDirtyFlag())
            domain.setAccountAnalyticIdText(model.getAccount_analytic_id_text());
        if(model.getAccount_id_textDirtyFlag())
            domain.setAccountIdText(model.getAccount_id_text());
        if(model.getCreate_uidDirtyFlag())
            domain.setCreateUid(model.getCreate_uid());
        if(model.getAccount_idDirtyFlag())
            domain.setAccountId(model.getAccount_id());
        if(model.getInvoice_idDirtyFlag())
            domain.setInvoiceId(model.getInvoice_id());
        if(model.getCurrency_idDirtyFlag())
            domain.setCurrencyId(model.getCurrency_id());
        if(model.getAccount_analytic_idDirtyFlag())
            domain.setAccountAnalyticId(model.getAccount_analytic_id());
        if(model.getWrite_uidDirtyFlag())
            domain.setWriteUid(model.getWrite_uid());
        if(model.getCompany_idDirtyFlag())
            domain.setCompanyId(model.getCompany_id());
        if(model.getTax_idDirtyFlag())
            domain.setTaxId(model.getTax_id());
        return domain ;
    }

}

    



