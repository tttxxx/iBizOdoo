package cn.ibizlab.odoo.core.odoo_account.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_account.domain.Account_account_template;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_account_templateSearchContext;
import cn.ibizlab.odoo.core.odoo_account.service.IAccount_account_templateService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;



import cn.ibizlab.odoo.core.odoo_account.client.account_account_templateOdooClient;
import cn.ibizlab.odoo.core.odoo_account.clientmodel.account_account_templateClientModel;

/**
 * 实体[科目模板] 服务对象接口实现
 */
@Slf4j
@Service
public class Account_account_templateServiceImpl implements IAccount_account_templateService {

    @Autowired
    account_account_templateOdooClient account_account_templateOdooClient;


    @Override
    public Account_account_template get(Integer id) {
        account_account_templateClientModel clientModel = new account_account_templateClientModel();
        clientModel.setId(id);
		account_account_templateOdooClient.get(clientModel);
        Account_account_template et = convert2Domain(clientModel,null);
        if(et==null){
            et=new Account_account_template();
            et.setId(id);
        }
        else{
        }

        return  et;
    }

    @Override
    public boolean create(Account_account_template et) {
        account_account_templateClientModel clientModel = convert2Model(et,null);
		account_account_templateOdooClient.create(clientModel);
        Account_account_template rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Account_account_template> list){
    }

    @Override
    public boolean remove(Integer id) {
        account_account_templateClientModel clientModel = new account_account_templateClientModel();
        clientModel.setId(id);
		account_account_templateOdooClient.remove(clientModel);
        return true; 
    }

    public void removeBatch(Collection<Integer> idList){
    }

    @Override
    public boolean update(Account_account_template et) {
        account_account_templateClientModel clientModel = convert2Model(et,null);
		account_account_templateOdooClient.update(clientModel);
        Account_account_template rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }
    
    public void updateBatch(List<Account_account_template> list){
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Account_account_template> searchDefault(Account_account_templateSearchContext context) {
        List<Account_account_template> list = new ArrayList<Account_account_template>();
        Page<account_account_templateClientModel> clientModelList = account_account_templateOdooClient.search(context);
        for(account_account_templateClientModel clientModel : clientModelList){
            list.add( this.convert2Domain(clientModel,null));
        }
        return new PageImpl<Account_account_template>(list, context.getPageable(), clientModelList.getTotalElements());
    }

    public account_account_templateClientModel convert2Model(Account_account_template domain , account_account_templateClientModel model) {
        if(domain == null)
            return model ;
        if(model==null){
            model = new account_account_templateClientModel();

        }
        if((Boolean) domain.getExtensionparams().get("dirtyflagenable")){
            if((Boolean) domain.getExtensionparams().get("codedirtyflag"))
                model.setCode(domain.getCode());
            if((Boolean) domain.getExtensionparams().get("tag_idsdirtyflag"))
                model.setTag_ids(domain.getTagIds());
            if((Boolean) domain.getExtensionparams().get("nocreatedirtyflag"))
                model.setNocreate(domain.getNocreate());
            if((Boolean) domain.getExtensionparams().get("tax_idsdirtyflag"))
                model.setTax_ids(domain.getTaxIds());
            if((Boolean) domain.getExtensionparams().get("create_datedirtyflag"))
                model.setCreate_date(domain.getCreateDate());
            if((Boolean) domain.getExtensionparams().get("display_namedirtyflag"))
                model.setDisplay_name(domain.getDisplayName());
            if((Boolean) domain.getExtensionparams().get("__last_updatedirtyflag"))
                model.set__last_update(domain.getLastUpdate());
            if((Boolean) domain.getExtensionparams().get("namedirtyflag"))
                model.setName(domain.getName());
            if((Boolean) domain.getExtensionparams().get("iddirtyflag"))
                model.setId(domain.getId());
            if((Boolean) domain.getExtensionparams().get("notedirtyflag"))
                model.setNote(domain.getNote());
            if((Boolean) domain.getExtensionparams().get("write_datedirtyflag"))
                model.setWrite_date(domain.getWriteDate());
            if((Boolean) domain.getExtensionparams().get("reconciledirtyflag"))
                model.setReconcile(domain.getReconcile());
            if((Boolean) domain.getExtensionparams().get("chart_template_id_textdirtyflag"))
                model.setChart_template_id_text(domain.getChartTemplateIdText());
            if((Boolean) domain.getExtensionparams().get("currency_id_textdirtyflag"))
                model.setCurrency_id_text(domain.getCurrencyIdText());
            if((Boolean) domain.getExtensionparams().get("user_type_id_textdirtyflag"))
                model.setUser_type_id_text(domain.getUserTypeIdText());
            if((Boolean) domain.getExtensionparams().get("create_uid_textdirtyflag"))
                model.setCreate_uid_text(domain.getCreateUidText());
            if((Boolean) domain.getExtensionparams().get("write_uid_textdirtyflag"))
                model.setWrite_uid_text(domain.getWriteUidText());
            if((Boolean) domain.getExtensionparams().get("group_id_textdirtyflag"))
                model.setGroup_id_text(domain.getGroupIdText());
            if((Boolean) domain.getExtensionparams().get("write_uiddirtyflag"))
                model.setWrite_uid(domain.getWriteUid());
            if((Boolean) domain.getExtensionparams().get("create_uiddirtyflag"))
                model.setCreate_uid(domain.getCreateUid());
            if((Boolean) domain.getExtensionparams().get("chart_template_iddirtyflag"))
                model.setChart_template_id(domain.getChartTemplateId());
            if((Boolean) domain.getExtensionparams().get("currency_iddirtyflag"))
                model.setCurrency_id(domain.getCurrencyId());
            if((Boolean) domain.getExtensionparams().get("user_type_iddirtyflag"))
                model.setUser_type_id(domain.getUserTypeId());
            if((Boolean) domain.getExtensionparams().get("group_iddirtyflag"))
                model.setGroup_id(domain.getGroupId());
            domain.getFocusNull().clear();
        }
        return model ;
    }

    public Account_account_template convert2Domain( account_account_templateClientModel model ,Account_account_template domain) {
        if(model == null)
            return domain ;
        if(domain == null){
            domain = new Account_account_template();
        }

        if(model.getCodeDirtyFlag())
            domain.setCode(model.getCode());
        if(model.getTag_idsDirtyFlag())
            domain.setTagIds(model.getTag_ids());
        if(model.getNocreateDirtyFlag())
            domain.setNocreate(model.getNocreate());
        if(model.getTax_idsDirtyFlag())
            domain.setTaxIds(model.getTax_ids());
        if(model.getCreate_dateDirtyFlag())
            domain.setCreateDate(model.getCreate_date());
        if(model.getDisplay_nameDirtyFlag())
            domain.setDisplayName(model.getDisplay_name());
        if(model.get__last_updateDirtyFlag())
            domain.setLastUpdate(model.get__last_update());
        if(model.getNameDirtyFlag())
            domain.setName(model.getName());
        if(model.getIdDirtyFlag())
            domain.setId(model.getId());
        if(model.getNoteDirtyFlag())
            domain.setNote(model.getNote());
        if(model.getWrite_dateDirtyFlag())
            domain.setWriteDate(model.getWrite_date());
        if(model.getReconcileDirtyFlag())
            domain.setReconcile(model.getReconcile());
        if(model.getChart_template_id_textDirtyFlag())
            domain.setChartTemplateIdText(model.getChart_template_id_text());
        if(model.getCurrency_id_textDirtyFlag())
            domain.setCurrencyIdText(model.getCurrency_id_text());
        if(model.getUser_type_id_textDirtyFlag())
            domain.setUserTypeIdText(model.getUser_type_id_text());
        if(model.getCreate_uid_textDirtyFlag())
            domain.setCreateUidText(model.getCreate_uid_text());
        if(model.getWrite_uid_textDirtyFlag())
            domain.setWriteUidText(model.getWrite_uid_text());
        if(model.getGroup_id_textDirtyFlag())
            domain.setGroupIdText(model.getGroup_id_text());
        if(model.getWrite_uidDirtyFlag())
            domain.setWriteUid(model.getWrite_uid());
        if(model.getCreate_uidDirtyFlag())
            domain.setCreateUid(model.getCreate_uid());
        if(model.getChart_template_idDirtyFlag())
            domain.setChartTemplateId(model.getChart_template_id());
        if(model.getCurrency_idDirtyFlag())
            domain.setCurrencyId(model.getCurrency_id());
        if(model.getUser_type_idDirtyFlag())
            domain.setUserTypeId(model.getUser_type_id());
        if(model.getGroup_idDirtyFlag())
            domain.setGroupId(model.getGroup_id());
        return domain ;
    }

}

    



