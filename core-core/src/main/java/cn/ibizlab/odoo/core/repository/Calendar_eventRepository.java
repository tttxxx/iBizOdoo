package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Calendar_event;
import cn.ibizlab.odoo.core.odoo_calendar.filter.Calendar_eventSearchContext;

/**
 * 实体 [活动] 存储对象
 */
public interface Calendar_eventRepository extends Repository<Calendar_event> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Calendar_event> searchDefault(Calendar_eventSearchContext context);

    Calendar_event convert2PO(cn.ibizlab.odoo.core.odoo_calendar.domain.Calendar_event domain , Calendar_event po) ;

    cn.ibizlab.odoo.core.odoo_calendar.domain.Calendar_event convert2Domain( Calendar_event po ,cn.ibizlab.odoo.core.odoo_calendar.domain.Calendar_event domain) ;

}
