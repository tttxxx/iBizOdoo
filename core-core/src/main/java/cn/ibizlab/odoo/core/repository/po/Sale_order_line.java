package cn.ibizlab.odoo.core.repository.po ;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.math.BigInteger;

import cn.ibizlab.odoo.core.odoo_sale.filter.Sale_order_lineSearchContext;

/**
 * 实体 [销售订单行] 存储模型
 */
public interface Sale_order_line{

    /**
     * 可报销
     */
    String getIs_expense();

    void setIs_expense(String is_expense);

    /**
     * 获取 [可报销]脏标记
     */
    boolean getIs_expenseDirtyFlag();

    /**
     * 最后更新时间
     */
    Timestamp getWrite_date();

    void setWrite_date(Timestamp write_date);

    /**
     * 获取 [最后更新时间]脏标记
     */
    boolean getWrite_dateDirtyFlag();

    /**
     * 发票数量
     */
    Double getQty_to_invoice();

    void setQty_to_invoice(Double qty_to_invoice);

    /**
     * 获取 [发票数量]脏标记
     */
    boolean getQty_to_invoiceDirtyFlag();

    /**
     * 分析标签
     */
    String getAnalytic_tag_ids();

    void setAnalytic_tag_ids(String analytic_tag_ids);

    /**
     * 获取 [分析标签]脏标记
     */
    boolean getAnalytic_tag_idsDirtyFlag();

    /**
     * 警告
     */
    String getWarning_stock();

    void setWarning_stock(String warning_stock);

    /**
     * 获取 [警告]脏标记
     */
    boolean getWarning_stockDirtyFlag();

    /**
     * ID
     */
    Integer getId();

    void setId(Integer id);

    /**
     * 获取 [ID]脏标记
     */
    boolean getIdDirtyFlag();

    /**
     * 显示名称
     */
    String getDisplay_name();

    void setDisplay_name(String display_name);

    /**
     * 获取 [显示名称]脏标记
     */
    boolean getDisplay_nameDirtyFlag();

    /**
     * 不含税价
     */
    Double getPrice_reduce_taxexcl();

    void setPrice_reduce_taxexcl(Double price_reduce_taxexcl);

    /**
     * 获取 [不含税价]脏标记
     */
    boolean getPrice_reduce_taxexclDirtyFlag();

    /**
     * 用户输入自定义产品属性值
     */
    String getProduct_custom_attribute_value_ids();

    void setProduct_custom_attribute_value_ids(String product_custom_attribute_value_ids);

    /**
     * 获取 [用户输入自定义产品属性值]脏标记
     */
    boolean getProduct_custom_attribute_value_idsDirtyFlag();

    /**
     * 手动发货
     */
    Double getQty_delivered_manual();

    void setQty_delivered_manual(Double qty_delivered_manual);

    /**
     * 获取 [手动发货]脏标记
     */
    boolean getQty_delivered_manualDirtyFlag();

    /**
     * 税额总计
     */
    Double getPrice_tax();

    void setPrice_tax(Double price_tax);

    /**
     * 获取 [税额总计]脏标记
     */
    boolean getPrice_taxDirtyFlag();

    /**
     * 更新数量的方法
     */
    String getQty_delivered_method();

    void setQty_delivered_method(String qty_delivered_method);

    /**
     * 获取 [更新数量的方法]脏标记
     */
    boolean getQty_delivered_methodDirtyFlag();

    /**
     * 发票状态
     */
    String getInvoice_status();

    void setInvoice_status(String invoice_status);

    /**
     * 获取 [发票状态]脏标记
     */
    boolean getInvoice_statusDirtyFlag();

    /**
     * 显示类型
     */
    String getDisplay_type();

    void setDisplay_type(String display_type);

    /**
     * 获取 [显示类型]脏标记
     */
    boolean getDisplay_typeDirtyFlag();

    /**
     * 库存移动
     */
    String getMove_ids();

    void setMove_ids(String move_ids);

    /**
     * 获取 [库存移动]脏标记
     */
    boolean getMove_idsDirtyFlag();

    /**
     * 未含税的发票金额
     */
    Double getUntaxed_amount_invoiced();

    void setUntaxed_amount_invoiced(Double untaxed_amount_invoiced);

    /**
     * 获取 [未含税的发票金额]脏标记
     */
    boolean getUntaxed_amount_invoicedDirtyFlag();

    /**
     * 没有创建变量的产品属性值
     */
    String getProduct_no_variant_attribute_value_ids();

    void setProduct_no_variant_attribute_value_ids(String product_no_variant_attribute_value_ids);

    /**
     * 获取 [没有创建变量的产品属性值]脏标记
     */
    boolean getProduct_no_variant_attribute_value_idsDirtyFlag();

    /**
     * 最后修改日
     */
    Timestamp get__last_update();

    void set__last_update(Timestamp __last_update);

    /**
     * 获取 [最后修改日]脏标记
     */
    boolean get__last_updateDirtyFlag();

    /**
     * 是首付款吗？
     */
    String getIs_downpayment();

    void setIs_downpayment(String is_downpayment);

    /**
     * 获取 [是首付款吗？]脏标记
     */
    boolean getIs_downpaymentDirtyFlag();

    /**
     * 分析明细行
     */
    String getAnalytic_line_ids();

    void setAnalytic_line_ids(String analytic_line_ids);

    /**
     * 获取 [分析明细行]脏标记
     */
    boolean getAnalytic_line_idsDirtyFlag();

    /**
     * 减税后价格
     */
    Double getPrice_reduce_taxinc();

    void setPrice_reduce_taxinc(Double price_reduce_taxinc);

    /**
     * 获取 [减税后价格]脏标记
     */
    boolean getPrice_reduce_taxincDirtyFlag();

    /**
     * 已交货数量
     */
    Double getQty_delivered();

    void setQty_delivered(Double qty_delivered);

    /**
     * 获取 [已交货数量]脏标记
     */
    boolean getQty_deliveredDirtyFlag();

    /**
     * 折扣(%)
     */
    Double getDiscount();

    void setDiscount(Double discount);

    /**
     * 获取 [折扣(%)]脏标记
     */
    boolean getDiscountDirtyFlag();

    /**
     * 订购数量
     */
    Double getProduct_uom_qty();

    void setProduct_uom_qty(Double product_uom_qty);

    /**
     * 获取 [订购数量]脏标记
     */
    boolean getProduct_uom_qtyDirtyFlag();

    /**
     * 降价
     */
    Double getPrice_reduce();

    void setPrice_reduce(Double price_reduce);

    /**
     * 获取 [降价]脏标记
     */
    boolean getPrice_reduceDirtyFlag();

    /**
     * 交货提前时间
     */
    Double getCustomer_lead();

    void setCustomer_lead(Double customer_lead);

    /**
     * 获取 [交货提前时间]脏标记
     */
    boolean getCustomer_leadDirtyFlag();

    /**
     * 允许编辑
     */
    String getProduct_updatable();

    void setProduct_updatable(String product_updatable);

    /**
     * 获取 [允许编辑]脏标记
     */
    boolean getProduct_updatableDirtyFlag();

    /**
     * 已开发票数量
     */
    Double getQty_invoiced();

    void setQty_invoiced(Double qty_invoiced);

    /**
     * 获取 [已开发票数量]脏标记
     */
    boolean getQty_invoicedDirtyFlag();

    /**
     * 不含税待开票金额
     */
    Double getUntaxed_amount_to_invoice();

    void setUntaxed_amount_to_invoice(Double untaxed_amount_to_invoice);

    /**
     * 获取 [不含税待开票金额]脏标记
     */
    boolean getUntaxed_amount_to_invoiceDirtyFlag();

    /**
     * 说明
     */
    String getName();

    void setName(String name);

    /**
     * 获取 [说明]脏标记
     */
    boolean getNameDirtyFlag();

    /**
     * 单价
     */
    Double getPrice_unit();

    void setPrice_unit(Double price_unit);

    /**
     * 获取 [单价]脏标记
     */
    boolean getPrice_unitDirtyFlag();

    /**
     * 创建时间
     */
    Timestamp getCreate_date();

    void setCreate_date(Timestamp create_date);

    /**
     * 获取 [创建时间]脏标记
     */
    boolean getCreate_dateDirtyFlag();

    /**
     * 生成采购订单号
     */
    Integer getPurchase_line_count();

    void setPurchase_line_count(Integer purchase_line_count);

    /**
     * 获取 [生成采购订单号]脏标记
     */
    boolean getPurchase_line_countDirtyFlag();

    /**
     * 姓名简称
     */
    String getName_short();

    void setName_short(String name_short);

    /**
     * 获取 [姓名简称]脏标记
     */
    boolean getName_shortDirtyFlag();

    /**
     * 链接选项
     */
    String getOption_line_ids();

    void setOption_line_ids(String option_line_ids);

    /**
     * 获取 [链接选项]脏标记
     */
    boolean getOption_line_idsDirtyFlag();

    /**
     * 税率
     */
    String getTax_id();

    void setTax_id(String tax_id);

    /**
     * 获取 [税率]脏标记
     */
    boolean getTax_idDirtyFlag();

    /**
     * 可选产品行
     */
    String getSale_order_option_ids();

    void setSale_order_option_ids(String sale_order_option_ids);

    /**
     * 获取 [可选产品行]脏标记
     */
    boolean getSale_order_option_idsDirtyFlag();

    /**
     * 小计
     */
    Double getPrice_subtotal();

    void setPrice_subtotal(Double price_subtotal);

    /**
     * 获取 [小计]脏标记
     */
    boolean getPrice_subtotalDirtyFlag();

    /**
     * 序号
     */
    Integer getSequence();

    void setSequence(Integer sequence);

    /**
     * 获取 [序号]脏标记
     */
    boolean getSequenceDirtyFlag();

    /**
     * 总计
     */
    Double getPrice_total();

    void setPrice_total(Double price_total);

    /**
     * 获取 [总计]脏标记
     */
    boolean getPrice_totalDirtyFlag();

    /**
     * 发票行
     */
    String getInvoice_lines();

    void setInvoice_lines(String invoice_lines);

    /**
     * 获取 [发票行]脏标记
     */
    boolean getInvoice_linesDirtyFlag();

    /**
     * 生成采购订单明细行
     */
    String getPurchase_line_ids();

    void setPurchase_line_ids(String purchase_line_ids);

    /**
     * 获取 [生成采购订单明细行]脏标记
     */
    boolean getPurchase_line_idsDirtyFlag();

    /**
     * 是一张活动票吗？
     */
    String getEvent_ok();

    void setEvent_ok(String event_ok);

    /**
     * 获取 [是一张活动票吗？]脏标记
     */
    boolean getEvent_okDirtyFlag();

    /**
     * 活动入场券
     */
    String getEvent_ticket_id_text();

    void setEvent_ticket_id_text(String event_ticket_id_text);

    /**
     * 获取 [活动入场券]脏标记
     */
    boolean getEvent_ticket_id_textDirtyFlag();

    /**
     * 创建人
     */
    String getCreate_uid_text();

    void setCreate_uid_text(String create_uid_text);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uid_textDirtyFlag();

    /**
     * 链接的订单明细
     */
    String getLinked_line_id_text();

    void setLinked_line_id_text(String linked_line_id_text);

    /**
     * 获取 [链接的订单明细]脏标记
     */
    boolean getLinked_line_id_textDirtyFlag();

    /**
     * 客户
     */
    String getOrder_partner_id_text();

    void setOrder_partner_id_text(String order_partner_id_text);

    /**
     * 获取 [客户]脏标记
     */
    boolean getOrder_partner_id_textDirtyFlag();

    /**
     * 产品图片
     */
    byte[] getProduct_image();

    void setProduct_image(byte[] product_image);

    /**
     * 获取 [产品图片]脏标记
     */
    boolean getProduct_imageDirtyFlag();

    /**
     * 币种
     */
    String getCurrency_id_text();

    void setCurrency_id_text(String currency_id_text);

    /**
     * 获取 [币种]脏标记
     */
    boolean getCurrency_id_textDirtyFlag();

    /**
     * 产品
     */
    String getProduct_id_text();

    void setProduct_id_text(String product_id_text);

    /**
     * 获取 [产品]脏标记
     */
    boolean getProduct_id_textDirtyFlag();

    /**
     * 包裹
     */
    String getProduct_packaging_text();

    void setProduct_packaging_text(String product_packaging_text);

    /**
     * 获取 [包裹]脏标记
     */
    boolean getProduct_packaging_textDirtyFlag();

    /**
     * 订单关联
     */
    String getOrder_id_text();

    void setOrder_id_text(String order_id_text);

    /**
     * 获取 [订单关联]脏标记
     */
    boolean getOrder_id_textDirtyFlag();

    /**
     * 活动
     */
    String getEvent_id_text();

    void setEvent_id_text(String event_id_text);

    /**
     * 获取 [活动]脏标记
     */
    boolean getEvent_id_textDirtyFlag();

    /**
     * 销售员
     */
    String getSalesman_id_text();

    void setSalesman_id_text(String salesman_id_text);

    /**
     * 获取 [销售员]脏标记
     */
    boolean getSalesman_id_textDirtyFlag();

    /**
     * 计量单位
     */
    String getProduct_uom_text();

    void setProduct_uom_text(String product_uom_text);

    /**
     * 获取 [计量单位]脏标记
     */
    boolean getProduct_uom_textDirtyFlag();

    /**
     * 最后更新人
     */
    String getWrite_uid_text();

    void setWrite_uid_text(String write_uid_text);

    /**
     * 获取 [最后更新人]脏标记
     */
    boolean getWrite_uid_textDirtyFlag();

    /**
     * 公司
     */
    String getCompany_id_text();

    void setCompany_id_text(String company_id_text);

    /**
     * 获取 [公司]脏标记
     */
    boolean getCompany_id_textDirtyFlag();

    /**
     * 订单状态
     */
    String getState();

    void setState(String state);

    /**
     * 获取 [订单状态]脏标记
     */
    boolean getStateDirtyFlag();

    /**
     * 路线
     */
    String getRoute_id_text();

    void setRoute_id_text(String route_id_text);

    /**
     * 获取 [路线]脏标记
     */
    boolean getRoute_id_textDirtyFlag();

    /**
     * 销售员
     */
    Integer getSalesman_id();

    void setSalesman_id(Integer salesman_id);

    /**
     * 获取 [销售员]脏标记
     */
    boolean getSalesman_idDirtyFlag();

    /**
     * 币种
     */
    Integer getCurrency_id();

    void setCurrency_id(Integer currency_id);

    /**
     * 获取 [币种]脏标记
     */
    boolean getCurrency_idDirtyFlag();

    /**
     * 订单关联
     */
    Integer getOrder_id();

    void setOrder_id(Integer order_id);

    /**
     * 获取 [订单关联]脏标记
     */
    boolean getOrder_idDirtyFlag();

    /**
     * 活动
     */
    Integer getEvent_id();

    void setEvent_id(Integer event_id);

    /**
     * 获取 [活动]脏标记
     */
    boolean getEvent_idDirtyFlag();

    /**
     * 链接的订单明细
     */
    Integer getLinked_line_id();

    void setLinked_line_id(Integer linked_line_id);

    /**
     * 获取 [链接的订单明细]脏标记
     */
    boolean getLinked_line_idDirtyFlag();

    /**
     * 最后更新人
     */
    Integer getWrite_uid();

    void setWrite_uid(Integer write_uid);

    /**
     * 获取 [最后更新人]脏标记
     */
    boolean getWrite_uidDirtyFlag();

    /**
     * 创建人
     */
    Integer getCreate_uid();

    void setCreate_uid(Integer create_uid);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uidDirtyFlag();

    /**
     * 路线
     */
    Integer getRoute_id();

    void setRoute_id(Integer route_id);

    /**
     * 获取 [路线]脏标记
     */
    boolean getRoute_idDirtyFlag();

    /**
     * 客户
     */
    Integer getOrder_partner_id();

    void setOrder_partner_id(Integer order_partner_id);

    /**
     * 获取 [客户]脏标记
     */
    boolean getOrder_partner_idDirtyFlag();

    /**
     * 计量单位
     */
    Integer getProduct_uom();

    void setProduct_uom(Integer product_uom);

    /**
     * 获取 [计量单位]脏标记
     */
    boolean getProduct_uomDirtyFlag();

    /**
     * 产品
     */
    Integer getProduct_id();

    void setProduct_id(Integer product_id);

    /**
     * 获取 [产品]脏标记
     */
    boolean getProduct_idDirtyFlag();

    /**
     * 公司
     */
    Integer getCompany_id();

    void setCompany_id(Integer company_id);

    /**
     * 获取 [公司]脏标记
     */
    boolean getCompany_idDirtyFlag();

    /**
     * 包裹
     */
    Integer getProduct_packaging();

    void setProduct_packaging(Integer product_packaging);

    /**
     * 获取 [包裹]脏标记
     */
    boolean getProduct_packagingDirtyFlag();

    /**
     * 活动入场券
     */
    Integer getEvent_ticket_id();

    void setEvent_ticket_id(Integer event_ticket_id);

    /**
     * 获取 [活动入场券]脏标记
     */
    boolean getEvent_ticket_idDirtyFlag();

}
