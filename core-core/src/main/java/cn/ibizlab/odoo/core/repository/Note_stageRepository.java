package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Note_stage;
import cn.ibizlab.odoo.core.odoo_note.filter.Note_stageSearchContext;

/**
 * 实体 [便签阶段] 存储对象
 */
public interface Note_stageRepository extends Repository<Note_stage> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Note_stage> searchDefault(Note_stageSearchContext context);

    Note_stage convert2PO(cn.ibizlab.odoo.core.odoo_note.domain.Note_stage domain , Note_stage po) ;

    cn.ibizlab.odoo.core.odoo_note.domain.Note_stage convert2Domain( Note_stage po ,cn.ibizlab.odoo.core.odoo_note.domain.Note_stage domain) ;

}
