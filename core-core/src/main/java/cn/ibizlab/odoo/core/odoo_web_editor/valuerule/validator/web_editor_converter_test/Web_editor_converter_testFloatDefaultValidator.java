package cn.ibizlab.odoo.core.odoo_web_editor.valuerule.validator.web_editor_converter_test;

import lombok.extern.slf4j.Slf4j;
import cn.ibizlab.odoo.util.helper.SpringContextHolder;
import cn.ibizlab.odoo.util.valuerule.SysValueRule;
import cn.ibizlab.odoo.util.valuerule.StringLengthValueRule;
import cn.ibizlab.odoo.util.SearchFieldFilter;
import cn.ibizlab.odoo.util.enums.SearchFieldType;
import cn.ibizlab.odoo.core.odoo_web_editor.valuerule.anno.web_editor_converter_test.Web_editor_converter_testFloatDefault;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.sql.Timestamp;
import java.util.Arrays;
import java.util.List;

/**
 * 值规则注解解析类
 * 实体：Web_editor_converter_test
 * 属性：Float
 * 值规则：Default
 */
@Slf4j
@Component
public class Web_editor_converter_testFloatDefaultValidator implements ConstraintValidator<Web_editor_converter_testFloatDefault, Double> {
     String message;
    /**
     * 用来完成将注解中的内容初始化
    */
    @Override
    public void initialize(Web_editor_converter_testFloatDefault constraintAnnotation) {
        this.message = constraintAnnotation.message();
    }

    @Override
    public boolean isValid(Double value, ConstraintValidatorContext context) {
        boolean isValid = true;


        if(!isValid){
            context.disableDefaultConstraintViolation();
            context.buildConstraintViolationWithTemplate(message)
                    .addConstraintViolation();
        }
        return isValid;
    }
}

