package cn.ibizlab.odoo.core.repository.client.po;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigDecimal;

/**
 * [hr_expense] 对象
 */
public interface hr_expense {

    public Integer getAccount_id();

    public void setAccount_id(Integer account_id);

    public String getAccount_id_text();

    public void setAccount_id_text(String account_id_text);

    public Timestamp getActivity_date_deadline();

    public void setActivity_date_deadline(Timestamp activity_date_deadline);

    public String getActivity_ids();

    public void setActivity_ids(String activity_ids);

    public String getActivity_state();

    public void setActivity_state(String activity_state);

    public String getActivity_summary();

    public void setActivity_summary(String activity_summary);

    public Integer getActivity_type_id();

    public void setActivity_type_id(Integer activity_type_id);

    public String getActivity_type_id_text();

    public void setActivity_type_id_text(String activity_type_id_text);

    public Integer getActivity_user_id();

    public void setActivity_user_id(Integer activity_user_id);

    public String getActivity_user_id_text();

    public void setActivity_user_id_text(String activity_user_id_text);

    public Integer getAnalytic_account_id();

    public void setAnalytic_account_id(Integer analytic_account_id);

    public String getAnalytic_account_id_text();

    public void setAnalytic_account_id_text(String analytic_account_id_text);

    public String getAnalytic_tag_ids();

    public void setAnalytic_tag_ids(String analytic_tag_ids);

    public Integer getAttachment_number();

    public void setAttachment_number(Integer attachment_number);

    public Integer getCompany_currency_id();

    public void setCompany_currency_id(Integer company_currency_id);

    public String getCompany_currency_id_text();

    public void setCompany_currency_id_text(String company_currency_id_text);

    public Integer getCompany_id();

    public void setCompany_id(Integer company_id);

    public String getCompany_id_text();

    public void setCompany_id_text(String company_id_text);

    public Timestamp getCreate_date();

    public void setCreate_date(Timestamp create_date);

    public Integer getCreate_uid();

    public void setCreate_uid(Integer create_uid);

    public String getCreate_uid_text();

    public void setCreate_uid_text(String create_uid_text);

    public Integer getCurrency_id();

    public void setCurrency_id(Integer currency_id);

    public String getCurrency_id_text();

    public void setCurrency_id_text(String currency_id_text);

    public Timestamp getDate();

    public void setDate(Timestamp date);

    public String getDescription();

    public void setDescription(String description);

    public String getDisplay_name();

    public void setDisplay_name(String display_name);

    public Integer getEmployee_id();

    public void setEmployee_id(Integer employee_id);

    public String getEmployee_id_text();

    public void setEmployee_id_text(String employee_id_text);

    public Integer getId();

    public void setId(Integer id);

    public String getIs_refused();

    public void setIs_refused(String is_refused);

    public Integer getMessage_attachment_count();

    public void setMessage_attachment_count(Integer message_attachment_count);

    public String getMessage_channel_ids();

    public void setMessage_channel_ids(String message_channel_ids);

    public String getMessage_follower_ids();

    public void setMessage_follower_ids(String message_follower_ids);

    public String getMessage_has_error();

    public void setMessage_has_error(String message_has_error);

    public Integer getMessage_has_error_counter();

    public void setMessage_has_error_counter(Integer message_has_error_counter);

    public String getMessage_ids();

    public void setMessage_ids(String message_ids);

    public String getMessage_is_follower();

    public void setMessage_is_follower(String message_is_follower);

    public String getMessage_needaction();

    public void setMessage_needaction(String message_needaction);

    public Integer getMessage_needaction_counter();

    public void setMessage_needaction_counter(Integer message_needaction_counter);

    public String getMessage_partner_ids();

    public void setMessage_partner_ids(String message_partner_ids);

    public String getMessage_unread();

    public void setMessage_unread(String message_unread);

    public Integer getMessage_unread_counter();

    public void setMessage_unread_counter(Integer message_unread_counter);

    public String getName();

    public void setName(String name);

    public String getPayment_mode();

    public void setPayment_mode(String payment_mode);

    public Integer getProduct_id();

    public void setProduct_id(Integer product_id);

    public String getProduct_id_text();

    public void setProduct_id_text(String product_id_text);

    public Integer getProduct_uom_id();

    public void setProduct_uom_id(Integer product_uom_id);

    public String getProduct_uom_id_text();

    public void setProduct_uom_id_text(String product_uom_id_text);

    public Double getQuantity();

    public void setQuantity(Double quantity);

    public String getReference();

    public void setReference(String reference);

    public Integer getSale_order_id();

    public void setSale_order_id(Integer sale_order_id);

    public String getSale_order_id_text();

    public void setSale_order_id_text(String sale_order_id_text);

    public Integer getSheet_id();

    public void setSheet_id(Integer sheet_id);

    public String getSheet_id_text();

    public void setSheet_id_text(String sheet_id_text);

    public String getState();

    public void setState(String state);

    public String getTax_ids();

    public void setTax_ids(String tax_ids);

    public Double getTotal_amount();

    public void setTotal_amount(Double total_amount);

    public Double getTotal_amount_company();

    public void setTotal_amount_company(Double total_amount_company);

    public Double getUnit_amount();

    public void setUnit_amount(Double unit_amount);

    public Double getUntaxed_amount();

    public void setUntaxed_amount(Double untaxed_amount);

    public String getWebsite_message_ids();

    public void setWebsite_message_ids(String website_message_ids);

    public Timestamp getWrite_date();

    public void setWrite_date(Timestamp write_date);

    public Integer getWrite_uid();

    public void setWrite_uid(Integer write_uid);

    public String getWrite_uid_text();

    public void setWrite_uid_text(String write_uid_text);

    public Timestamp get__last_update();

    public void set__last_update(Timestamp __last_update);

}
