package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Stock_quantity_history;
import cn.ibizlab.odoo.core.odoo_stock.filter.Stock_quantity_historySearchContext;

/**
 * 实体 [库存数量历史] 存储对象
 */
public interface Stock_quantity_historyRepository extends Repository<Stock_quantity_history> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Stock_quantity_history> searchDefault(Stock_quantity_historySearchContext context);

    Stock_quantity_history convert2PO(cn.ibizlab.odoo.core.odoo_stock.domain.Stock_quantity_history domain , Stock_quantity_history po) ;

    cn.ibizlab.odoo.core.odoo_stock.domain.Stock_quantity_history convert2Domain( Stock_quantity_history po ,cn.ibizlab.odoo.core.odoo_stock.domain.Stock_quantity_history domain) ;

}
