package cn.ibizlab.odoo.core.repository.po ;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.math.BigInteger;

import cn.ibizlab.odoo.core.odoo_product.filter.Product_pricelist_itemSearchContext;

/**
 * 实体 [价格表明细] 存储模型
 */
public interface Product_pricelist_item{

    /**
     * 显示名称
     */
    String getDisplay_name();

    void setDisplay_name(String display_name);

    /**
     * 获取 [显示名称]脏标记
     */
    boolean getDisplay_nameDirtyFlag();

    /**
     * 固定价格
     */
    Double getFixed_price();

    void setFixed_price(Double fixed_price);

    /**
     * 获取 [固定价格]脏标记
     */
    boolean getFixed_priceDirtyFlag();

    /**
     * 名称
     */
    String getName();

    void setName(String name);

    /**
     * 获取 [名称]脏标记
     */
    boolean getNameDirtyFlag();

    /**
     * 价格
     */
    String getPrice();

    void setPrice(String price);

    /**
     * 获取 [价格]脏标记
     */
    boolean getPriceDirtyFlag();

    /**
     * 最后修改日
     */
    Timestamp get__last_update();

    void set__last_update(Timestamp __last_update);

    /**
     * 获取 [最后修改日]脏标记
     */
    boolean get__last_updateDirtyFlag();

    /**
     * 基于
     */
    String getBase();

    void setBase(String base);

    /**
     * 获取 [基于]脏标记
     */
    boolean getBaseDirtyFlag();

    /**
     * 结束日期
     */
    Timestamp getDate_end();

    void setDate_end(Timestamp date_end);

    /**
     * 获取 [结束日期]脏标记
     */
    boolean getDate_endDirtyFlag();

    /**
     * 百分比价格
     */
    Double getPercent_price();

    void setPercent_price(Double percent_price);

    /**
     * 获取 [百分比价格]脏标记
     */
    boolean getPercent_priceDirtyFlag();

    /**
     * 价格舍入
     */
    Double getPrice_round();

    void setPrice_round(Double price_round);

    /**
     * 获取 [价格舍入]脏标记
     */
    boolean getPrice_roundDirtyFlag();

    /**
     * 最后更新时间
     */
    Timestamp getWrite_date();

    void setWrite_date(Timestamp write_date);

    /**
     * 获取 [最后更新时间]脏标记
     */
    boolean getWrite_dateDirtyFlag();

    /**
     * ID
     */
    Integer getId();

    void setId(Integer id);

    /**
     * 获取 [ID]脏标记
     */
    boolean getIdDirtyFlag();

    /**
     * 创建时间
     */
    Timestamp getCreate_date();

    void setCreate_date(Timestamp create_date);

    /**
     * 获取 [创建时间]脏标记
     */
    boolean getCreate_dateDirtyFlag();

    /**
     * 计算价格
     */
    String getCompute_price();

    void setCompute_price(String compute_price);

    /**
     * 获取 [计算价格]脏标记
     */
    boolean getCompute_priceDirtyFlag();

    /**
     * 最小价格毛利
     */
    Double getPrice_min_margin();

    void setPrice_min_margin(Double price_min_margin);

    /**
     * 获取 [最小价格毛利]脏标记
     */
    boolean getPrice_min_marginDirtyFlag();

    /**
     * 开始日期
     */
    Timestamp getDate_start();

    void setDate_start(Timestamp date_start);

    /**
     * 获取 [开始日期]脏标记
     */
    boolean getDate_startDirtyFlag();

    /**
     * 最大价格毛利
     */
    Double getPrice_max_margin();

    void setPrice_max_margin(Double price_max_margin);

    /**
     * 获取 [最大价格毛利]脏标记
     */
    boolean getPrice_max_marginDirtyFlag();

    /**
     * 应用于
     */
    String getApplied_on();

    void setApplied_on(String applied_on);

    /**
     * 获取 [应用于]脏标记
     */
    boolean getApplied_onDirtyFlag();

    /**
     * 最小数量
     */
    Integer getMin_quantity();

    void setMin_quantity(Integer min_quantity);

    /**
     * 获取 [最小数量]脏标记
     */
    boolean getMin_quantityDirtyFlag();

    /**
     * 价格附加费用
     */
    Double getPrice_surcharge();

    void setPrice_surcharge(Double price_surcharge);

    /**
     * 获取 [价格附加费用]脏标记
     */
    boolean getPrice_surchargeDirtyFlag();

    /**
     * 价格折扣
     */
    Double getPrice_discount();

    void setPrice_discount(Double price_discount);

    /**
     * 获取 [价格折扣]脏标记
     */
    boolean getPrice_discountDirtyFlag();

    /**
     * 价格表
     */
    String getPricelist_id_text();

    void setPricelist_id_text(String pricelist_id_text);

    /**
     * 获取 [价格表]脏标记
     */
    boolean getPricelist_id_textDirtyFlag();

    /**
     * 币种
     */
    String getCurrency_id_text();

    void setCurrency_id_text(String currency_id_text);

    /**
     * 获取 [币种]脏标记
     */
    boolean getCurrency_id_textDirtyFlag();

    /**
     * 产品种类
     */
    String getCateg_id_text();

    void setCateg_id_text(String categ_id_text);

    /**
     * 获取 [产品种类]脏标记
     */
    boolean getCateg_id_textDirtyFlag();

    /**
     * 其他价格表
     */
    String getBase_pricelist_id_text();

    void setBase_pricelist_id_text(String base_pricelist_id_text);

    /**
     * 获取 [其他价格表]脏标记
     */
    boolean getBase_pricelist_id_textDirtyFlag();

    /**
     * 最后更新人
     */
    String getWrite_uid_text();

    void setWrite_uid_text(String write_uid_text);

    /**
     * 获取 [最后更新人]脏标记
     */
    boolean getWrite_uid_textDirtyFlag();

    /**
     * 产品模板
     */
    String getProduct_tmpl_id_text();

    void setProduct_tmpl_id_text(String product_tmpl_id_text);

    /**
     * 获取 [产品模板]脏标记
     */
    boolean getProduct_tmpl_id_textDirtyFlag();

    /**
     * 创建人
     */
    String getCreate_uid_text();

    void setCreate_uid_text(String create_uid_text);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uid_textDirtyFlag();

    /**
     * 公司
     */
    String getCompany_id_text();

    void setCompany_id_text(String company_id_text);

    /**
     * 获取 [公司]脏标记
     */
    boolean getCompany_id_textDirtyFlag();

    /**
     * 产品
     */
    String getProduct_id_text();

    void setProduct_id_text(String product_id_text);

    /**
     * 获取 [产品]脏标记
     */
    boolean getProduct_id_textDirtyFlag();

    /**
     * 产品
     */
    Integer getProduct_id();

    void setProduct_id(Integer product_id);

    /**
     * 获取 [产品]脏标记
     */
    boolean getProduct_idDirtyFlag();

    /**
     * 价格表
     */
    Integer getPricelist_id();

    void setPricelist_id(Integer pricelist_id);

    /**
     * 获取 [价格表]脏标记
     */
    boolean getPricelist_idDirtyFlag();

    /**
     * 最后更新人
     */
    Integer getWrite_uid();

    void setWrite_uid(Integer write_uid);

    /**
     * 获取 [最后更新人]脏标记
     */
    boolean getWrite_uidDirtyFlag();

    /**
     * 公司
     */
    Integer getCompany_id();

    void setCompany_id(Integer company_id);

    /**
     * 获取 [公司]脏标记
     */
    boolean getCompany_idDirtyFlag();

    /**
     * 其他价格表
     */
    Integer getBase_pricelist_id();

    void setBase_pricelist_id(Integer base_pricelist_id);

    /**
     * 获取 [其他价格表]脏标记
     */
    boolean getBase_pricelist_idDirtyFlag();

    /**
     * 币种
     */
    Integer getCurrency_id();

    void setCurrency_id(Integer currency_id);

    /**
     * 获取 [币种]脏标记
     */
    boolean getCurrency_idDirtyFlag();

    /**
     * 产品种类
     */
    Integer getCateg_id();

    void setCateg_id(Integer categ_id);

    /**
     * 获取 [产品种类]脏标记
     */
    boolean getCateg_idDirtyFlag();

    /**
     * 创建人
     */
    Integer getCreate_uid();

    void setCreate_uid(Integer create_uid);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uidDirtyFlag();

    /**
     * 产品模板
     */
    Integer getProduct_tmpl_id();

    void setProduct_tmpl_id(Integer product_tmpl_id);

    /**
     * 获取 [产品模板]脏标记
     */
    boolean getProduct_tmpl_idDirtyFlag();

}
