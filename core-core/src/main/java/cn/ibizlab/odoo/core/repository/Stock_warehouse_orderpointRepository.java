package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Stock_warehouse_orderpoint;
import cn.ibizlab.odoo.core.odoo_stock.filter.Stock_warehouse_orderpointSearchContext;

/**
 * 实体 [最小库存规则] 存储对象
 */
public interface Stock_warehouse_orderpointRepository extends Repository<Stock_warehouse_orderpoint> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Stock_warehouse_orderpoint> searchDefault(Stock_warehouse_orderpointSearchContext context);

    Stock_warehouse_orderpoint convert2PO(cn.ibizlab.odoo.core.odoo_stock.domain.Stock_warehouse_orderpoint domain , Stock_warehouse_orderpoint po) ;

    cn.ibizlab.odoo.core.odoo_stock.domain.Stock_warehouse_orderpoint convert2Domain( Stock_warehouse_orderpoint po ,cn.ibizlab.odoo.core.odoo_stock.domain.Stock_warehouse_orderpoint domain) ;

}
