package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Istock_immediate_transfer;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[stock_immediate_transfer] 服务对象接口
 */
public interface Istock_immediate_transferClientService{

    public Istock_immediate_transfer createModel() ;

    public void createBatch(List<Istock_immediate_transfer> stock_immediate_transfers);

    public void create(Istock_immediate_transfer stock_immediate_transfer);

    public void update(Istock_immediate_transfer stock_immediate_transfer);

    public void get(Istock_immediate_transfer stock_immediate_transfer);

    public void removeBatch(List<Istock_immediate_transfer> stock_immediate_transfers);

    public Page<Istock_immediate_transfer> search(SearchContext context);

    public void remove(Istock_immediate_transfer stock_immediate_transfer);

    public void updateBatch(List<Istock_immediate_transfer> stock_immediate_transfers);

    public Page<Istock_immediate_transfer> select(SearchContext context);

}
