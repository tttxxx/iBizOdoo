package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Mail_mass_mailing_campaign;
import cn.ibizlab.odoo.core.odoo_mail.filter.Mail_mass_mailing_campaignSearchContext;

/**
 * 实体 [群发邮件营销] 存储对象
 */
public interface Mail_mass_mailing_campaignRepository extends Repository<Mail_mass_mailing_campaign> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Mail_mass_mailing_campaign> searchDefault(Mail_mass_mailing_campaignSearchContext context);

    Mail_mass_mailing_campaign convert2PO(cn.ibizlab.odoo.core.odoo_mail.domain.Mail_mass_mailing_campaign domain , Mail_mass_mailing_campaign po) ;

    cn.ibizlab.odoo.core.odoo_mail.domain.Mail_mass_mailing_campaign convert2Domain( Mail_mass_mailing_campaign po ,cn.ibizlab.odoo.core.odoo_mail.domain.Mail_mass_mailing_campaign domain) ;

}
