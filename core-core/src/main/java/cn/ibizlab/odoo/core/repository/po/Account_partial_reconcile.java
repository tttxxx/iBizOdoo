package cn.ibizlab.odoo.core.repository.po ;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.math.BigInteger;

import cn.ibizlab.odoo.core.odoo_account.filter.Account_partial_reconcileSearchContext;

/**
 * 实体 [部分核销] 存储模型
 */
public interface Account_partial_reconcile{

    /**
     * 外币金额
     */
    Double getAmount_currency();

    void setAmount_currency(Double amount_currency);

    /**
     * 获取 [外币金额]脏标记
     */
    boolean getAmount_currencyDirtyFlag();

    /**
     * 金额
     */
    Double getAmount();

    void setAmount(Double amount);

    /**
     * 获取 [金额]脏标记
     */
    boolean getAmountDirtyFlag();

    /**
     * 最后更新时间
     */
    Timestamp getWrite_date();

    void setWrite_date(Timestamp write_date);

    /**
     * 获取 [最后更新时间]脏标记
     */
    boolean getWrite_dateDirtyFlag();

    /**
     * ID
     */
    Integer getId();

    void setId(Integer id);

    /**
     * 获取 [ID]脏标记
     */
    boolean getIdDirtyFlag();

    /**
     * 最后修改日
     */
    Timestamp get__last_update();

    void set__last_update(Timestamp __last_update);

    /**
     * 获取 [最后修改日]脏标记
     */
    boolean get__last_updateDirtyFlag();

    /**
     * 显示名称
     */
    String getDisplay_name();

    void setDisplay_name(String display_name);

    /**
     * 获取 [显示名称]脏标记
     */
    boolean getDisplay_nameDirtyFlag();

    /**
     * 创建时间
     */
    Timestamp getCreate_date();

    void setCreate_date(Timestamp create_date);

    /**
     * 获取 [创建时间]脏标记
     */
    boolean getCreate_dateDirtyFlag();

    /**
     * 匹配明细的最大时间
     */
    Timestamp getMax_date();

    void setMax_date(Timestamp max_date);

    /**
     * 获取 [匹配明细的最大时间]脏标记
     */
    boolean getMax_dateDirtyFlag();

    /**
     * 公司
     */
    String getCompany_id_text();

    void setCompany_id_text(String company_id_text);

    /**
     * 获取 [公司]脏标记
     */
    boolean getCompany_id_textDirtyFlag();

    /**
     * 贷方凭证
     */
    String getCredit_move_id_text();

    void setCredit_move_id_text(String credit_move_id_text);

    /**
     * 获取 [贷方凭证]脏标记
     */
    boolean getCredit_move_id_textDirtyFlag();

    /**
     * 公司货币
     */
    Integer getCompany_currency_id();

    void setCompany_currency_id(Integer company_currency_id);

    /**
     * 获取 [公司货币]脏标记
     */
    boolean getCompany_currency_idDirtyFlag();

    /**
     * 创建人
     */
    String getCreate_uid_text();

    void setCreate_uid_text(String create_uid_text);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uid_textDirtyFlag();

    /**
     * 借方移动
     */
    String getDebit_move_id_text();

    void setDebit_move_id_text(String debit_move_id_text);

    /**
     * 获取 [借方移动]脏标记
     */
    boolean getDebit_move_id_textDirtyFlag();

    /**
     * 币种
     */
    String getCurrency_id_text();

    void setCurrency_id_text(String currency_id_text);

    /**
     * 获取 [币种]脏标记
     */
    boolean getCurrency_id_textDirtyFlag();

    /**
     * 最后更新人
     */
    String getWrite_uid_text();

    void setWrite_uid_text(String write_uid_text);

    /**
     * 获取 [最后更新人]脏标记
     */
    boolean getWrite_uid_textDirtyFlag();

    /**
     * 完全核销
     */
    String getFull_reconcile_id_text();

    void setFull_reconcile_id_text(String full_reconcile_id_text);

    /**
     * 获取 [完全核销]脏标记
     */
    boolean getFull_reconcile_id_textDirtyFlag();

    /**
     * 公司
     */
    Integer getCompany_id();

    void setCompany_id(Integer company_id);

    /**
     * 获取 [公司]脏标记
     */
    boolean getCompany_idDirtyFlag();

    /**
     * 币种
     */
    Integer getCurrency_id();

    void setCurrency_id(Integer currency_id);

    /**
     * 获取 [币种]脏标记
     */
    boolean getCurrency_idDirtyFlag();

    /**
     * 完全核销
     */
    Integer getFull_reconcile_id();

    void setFull_reconcile_id(Integer full_reconcile_id);

    /**
     * 获取 [完全核销]脏标记
     */
    boolean getFull_reconcile_idDirtyFlag();

    /**
     * 创建人
     */
    Integer getCreate_uid();

    void setCreate_uid(Integer create_uid);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uidDirtyFlag();

    /**
     * 借方移动
     */
    Integer getDebit_move_id();

    void setDebit_move_id(Integer debit_move_id);

    /**
     * 获取 [借方移动]脏标记
     */
    boolean getDebit_move_idDirtyFlag();

    /**
     * 贷方凭证
     */
    Integer getCredit_move_id();

    void setCredit_move_id(Integer credit_move_id);

    /**
     * 获取 [贷方凭证]脏标记
     */
    boolean getCredit_move_idDirtyFlag();

    /**
     * 最后更新人
     */
    Integer getWrite_uid();

    void setWrite_uid(Integer write_uid);

    /**
     * 获取 [最后更新人]脏标记
     */
    boolean getWrite_uidDirtyFlag();

}
