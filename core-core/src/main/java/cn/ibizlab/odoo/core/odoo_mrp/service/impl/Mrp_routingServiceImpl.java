package cn.ibizlab.odoo.core.odoo_mrp.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_mrp.domain.Mrp_routing;
import cn.ibizlab.odoo.core.odoo_mrp.filter.Mrp_routingSearchContext;
import cn.ibizlab.odoo.core.odoo_mrp.service.IMrp_routingService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;



import cn.ibizlab.odoo.core.odoo_mrp.client.mrp_routingOdooClient;
import cn.ibizlab.odoo.core.odoo_mrp.clientmodel.mrp_routingClientModel;

/**
 * 实体[工艺] 服务对象接口实现
 */
@Slf4j
@Service
public class Mrp_routingServiceImpl implements IMrp_routingService {

    @Autowired
    mrp_routingOdooClient mrp_routingOdooClient;


    @Override
    public boolean remove(Integer id) {
        mrp_routingClientModel clientModel = new mrp_routingClientModel();
        clientModel.setId(id);
		mrp_routingOdooClient.remove(clientModel);
        return true; 
    }

    public void removeBatch(Collection<Integer> idList){
    }

    @Override
    public Mrp_routing get(Integer id) {
        mrp_routingClientModel clientModel = new mrp_routingClientModel();
        clientModel.setId(id);
		mrp_routingOdooClient.get(clientModel);
        Mrp_routing et = convert2Domain(clientModel,null);
        if(et==null){
            et=new Mrp_routing();
            et.setId(id);
        }
        else{
        }

        return  et;
    }

    @Override
    public boolean create(Mrp_routing et) {
        mrp_routingClientModel clientModel = convert2Model(et,null);
		mrp_routingOdooClient.create(clientModel);
        Mrp_routing rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Mrp_routing> list){
    }

    @Override
    public boolean update(Mrp_routing et) {
        mrp_routingClientModel clientModel = convert2Model(et,null);
		mrp_routingOdooClient.update(clientModel);
        Mrp_routing rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }
    
    public void updateBatch(List<Mrp_routing> list){
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Mrp_routing> searchDefault(Mrp_routingSearchContext context) {
        List<Mrp_routing> list = new ArrayList<Mrp_routing>();
        Page<mrp_routingClientModel> clientModelList = mrp_routingOdooClient.search(context);
        for(mrp_routingClientModel clientModel : clientModelList){
            list.add( this.convert2Domain(clientModel,null));
        }
        return new PageImpl<Mrp_routing>(list, context.getPageable(), clientModelList.getTotalElements());
    }

    public mrp_routingClientModel convert2Model(Mrp_routing domain , mrp_routingClientModel model) {
        if(domain == null)
            return model ;
        if(model==null){
            model = new mrp_routingClientModel();

        }
        if((Boolean) domain.getExtensionparams().get("dirtyflagenable")){
            if((Boolean) domain.getExtensionparams().get("activedirtyflag"))
                model.setActive(domain.getActive());
            if((Boolean) domain.getExtensionparams().get("notedirtyflag"))
                model.setNote(domain.getNote());
            if((Boolean) domain.getExtensionparams().get("iddirtyflag"))
                model.setId(domain.getId());
            if((Boolean) domain.getExtensionparams().get("create_datedirtyflag"))
                model.setCreate_date(domain.getCreateDate());
            if((Boolean) domain.getExtensionparams().get("codedirtyflag"))
                model.setCode(domain.getCode());
            if((Boolean) domain.getExtensionparams().get("display_namedirtyflag"))
                model.setDisplay_name(domain.getDisplayName());
            if((Boolean) domain.getExtensionparams().get("namedirtyflag"))
                model.setName(domain.getName());
            if((Boolean) domain.getExtensionparams().get("__last_updatedirtyflag"))
                model.set__last_update(domain.getLastUpdate());
            if((Boolean) domain.getExtensionparams().get("write_datedirtyflag"))
                model.setWrite_date(domain.getWriteDate());
            if((Boolean) domain.getExtensionparams().get("operation_idsdirtyflag"))
                model.setOperation_ids(domain.getOperationIds());
            if((Boolean) domain.getExtensionparams().get("create_uid_textdirtyflag"))
                model.setCreate_uid_text(domain.getCreateUidText());
            if((Boolean) domain.getExtensionparams().get("write_uid_textdirtyflag"))
                model.setWrite_uid_text(domain.getWriteUidText());
            if((Boolean) domain.getExtensionparams().get("location_id_textdirtyflag"))
                model.setLocation_id_text(domain.getLocationIdText());
            if((Boolean) domain.getExtensionparams().get("company_id_textdirtyflag"))
                model.setCompany_id_text(domain.getCompanyIdText());
            if((Boolean) domain.getExtensionparams().get("company_iddirtyflag"))
                model.setCompany_id(domain.getCompanyId());
            if((Boolean) domain.getExtensionparams().get("write_uiddirtyflag"))
                model.setWrite_uid(domain.getWriteUid());
            if((Boolean) domain.getExtensionparams().get("location_iddirtyflag"))
                model.setLocation_id(domain.getLocationId());
            if((Boolean) domain.getExtensionparams().get("create_uiddirtyflag"))
                model.setCreate_uid(domain.getCreateUid());
            domain.getFocusNull().clear();
        }
        return model ;
    }

    public Mrp_routing convert2Domain( mrp_routingClientModel model ,Mrp_routing domain) {
        if(model == null)
            return domain ;
        if(domain == null){
            domain = new Mrp_routing();
        }

        if(model.getActiveDirtyFlag())
            domain.setActive(model.getActive());
        if(model.getNoteDirtyFlag())
            domain.setNote(model.getNote());
        if(model.getIdDirtyFlag())
            domain.setId(model.getId());
        if(model.getCreate_dateDirtyFlag())
            domain.setCreateDate(model.getCreate_date());
        if(model.getCodeDirtyFlag())
            domain.setCode(model.getCode());
        if(model.getDisplay_nameDirtyFlag())
            domain.setDisplayName(model.getDisplay_name());
        if(model.getNameDirtyFlag())
            domain.setName(model.getName());
        if(model.get__last_updateDirtyFlag())
            domain.setLastUpdate(model.get__last_update());
        if(model.getWrite_dateDirtyFlag())
            domain.setWriteDate(model.getWrite_date());
        if(model.getOperation_idsDirtyFlag())
            domain.setOperationIds(model.getOperation_ids());
        if(model.getCreate_uid_textDirtyFlag())
            domain.setCreateUidText(model.getCreate_uid_text());
        if(model.getWrite_uid_textDirtyFlag())
            domain.setWriteUidText(model.getWrite_uid_text());
        if(model.getLocation_id_textDirtyFlag())
            domain.setLocationIdText(model.getLocation_id_text());
        if(model.getCompany_id_textDirtyFlag())
            domain.setCompanyIdText(model.getCompany_id_text());
        if(model.getCompany_idDirtyFlag())
            domain.setCompanyId(model.getCompany_id());
        if(model.getWrite_uidDirtyFlag())
            domain.setWriteUid(model.getWrite_uid());
        if(model.getLocation_idDirtyFlag())
            domain.setLocationId(model.getLocation_id());
        if(model.getCreate_uidDirtyFlag())
            domain.setCreateUid(model.getCreate_uid());
        return domain ;
    }

}

    



