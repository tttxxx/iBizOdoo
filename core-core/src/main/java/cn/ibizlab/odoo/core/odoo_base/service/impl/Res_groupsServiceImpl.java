package cn.ibizlab.odoo.core.odoo_base.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_base.domain.Res_groups;
import cn.ibizlab.odoo.core.odoo_base.filter.Res_groupsSearchContext;
import cn.ibizlab.odoo.core.odoo_base.service.IRes_groupsService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;



import cn.ibizlab.odoo.core.odoo_base.client.res_groupsOdooClient;
import cn.ibizlab.odoo.core.odoo_base.clientmodel.res_groupsClientModel;

/**
 * 实体[访问群] 服务对象接口实现
 */
@Slf4j
@Service
public class Res_groupsServiceImpl implements IRes_groupsService {

    @Autowired
    res_groupsOdooClient res_groupsOdooClient;


    @Override
    public Res_groups get(Integer id) {
        res_groupsClientModel clientModel = new res_groupsClientModel();
        clientModel.setId(id);
		res_groupsOdooClient.get(clientModel);
        Res_groups et = convert2Domain(clientModel,null);
        if(et==null){
            et=new Res_groups();
            et.setId(id);
        }
        else{
        }

        return  et;
    }

    @Override
    public boolean update(Res_groups et) {
        res_groupsClientModel clientModel = convert2Model(et,null);
		res_groupsOdooClient.update(clientModel);
        Res_groups rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }
    
    public void updateBatch(List<Res_groups> list){
    }

    @Override
    public boolean remove(Integer id) {
        res_groupsClientModel clientModel = new res_groupsClientModel();
        clientModel.setId(id);
		res_groupsOdooClient.remove(clientModel);
        return true; 
    }

    public void removeBatch(Collection<Integer> idList){
    }

    @Override
    public boolean create(Res_groups et) {
        res_groupsClientModel clientModel = convert2Model(et,null);
		res_groupsOdooClient.create(clientModel);
        Res_groups rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Res_groups> list){
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Res_groups> searchDefault(Res_groupsSearchContext context) {
        List<Res_groups> list = new ArrayList<Res_groups>();
        Page<res_groupsClientModel> clientModelList = res_groupsOdooClient.search(context);
        for(res_groupsClientModel clientModel : clientModelList){
            list.add( this.convert2Domain(clientModel,null));
        }
        return new PageImpl<Res_groups>(list, context.getPageable(), clientModelList.getTotalElements());
    }

    public res_groupsClientModel convert2Model(Res_groups domain , res_groupsClientModel model) {
        if(domain == null)
            return model ;
        if(model==null){
            model = new res_groupsClientModel();

        }
        if((Boolean) domain.getExtensionparams().get("dirtyflagenable")){
            if((Boolean) domain.getExtensionparams().get("usersdirtyflag"))
                model.setUsers(domain.getUsers());
            if((Boolean) domain.getExtensionparams().get("namedirtyflag"))
                model.setName(domain.getName());
            if((Boolean) domain.getExtensionparams().get("implied_idsdirtyflag"))
                model.setImplied_ids(domain.getImpliedIds());
            if((Boolean) domain.getExtensionparams().get("trans_implied_idsdirtyflag"))
                model.setTrans_implied_ids(domain.getTransImpliedIds());
            if((Boolean) domain.getExtensionparams().get("display_namedirtyflag"))
                model.setDisplay_name(domain.getDisplayName());
            if((Boolean) domain.getExtensionparams().get("view_accessdirtyflag"))
                model.setView_access(domain.getViewAccess());
            if((Boolean) domain.getExtensionparams().get("rule_groupsdirtyflag"))
                model.setRule_groups(domain.getRuleGroups());
            if((Boolean) domain.getExtensionparams().get("model_accessdirtyflag"))
                model.setModel_access(domain.getModelAccess());
            if((Boolean) domain.getExtensionparams().get("category_iddirtyflag"))
                model.setCategory_id(domain.getCategoryId());
            if((Boolean) domain.getExtensionparams().get("iddirtyflag"))
                model.setId(domain.getId());
            if((Boolean) domain.getExtensionparams().get("menu_accessdirtyflag"))
                model.setMenu_access(domain.getMenuAccess());
            if((Boolean) domain.getExtensionparams().get("__last_updatedirtyflag"))
                model.set__last_update(domain.getLastUpdate());
            if((Boolean) domain.getExtensionparams().get("create_datedirtyflag"))
                model.setCreate_date(domain.getCreateDate());
            if((Boolean) domain.getExtensionparams().get("commentdirtyflag"))
                model.setComment(domain.getComment());
            if((Boolean) domain.getExtensionparams().get("write_datedirtyflag"))
                model.setWrite_date(domain.getWriteDate());
            if((Boolean) domain.getExtensionparams().get("sharedirtyflag"))
                model.setShare(domain.getShare());
            if((Boolean) domain.getExtensionparams().get("colordirtyflag"))
                model.setColor(domain.getColor());
            if((Boolean) domain.getExtensionparams().get("full_namedirtyflag"))
                model.setFull_name(domain.getFullName());
            if((Boolean) domain.getExtensionparams().get("create_uid_textdirtyflag"))
                model.setCreate_uid_text(domain.getCreateUidText());
            if((Boolean) domain.getExtensionparams().get("write_uid_textdirtyflag"))
                model.setWrite_uid_text(domain.getWriteUidText());
            if((Boolean) domain.getExtensionparams().get("create_uiddirtyflag"))
                model.setCreate_uid(domain.getCreateUid());
            if((Boolean) domain.getExtensionparams().get("write_uiddirtyflag"))
                model.setWrite_uid(domain.getWriteUid());
            domain.getFocusNull().clear();
        }
        return model ;
    }

    public Res_groups convert2Domain( res_groupsClientModel model ,Res_groups domain) {
        if(model == null)
            return domain ;
        if(domain == null){
            domain = new Res_groups();
        }

        if(model.getUsersDirtyFlag())
            domain.setUsers(model.getUsers());
        if(model.getNameDirtyFlag())
            domain.setName(model.getName());
        if(model.getImplied_idsDirtyFlag())
            domain.setImpliedIds(model.getImplied_ids());
        if(model.getTrans_implied_idsDirtyFlag())
            domain.setTransImpliedIds(model.getTrans_implied_ids());
        if(model.getDisplay_nameDirtyFlag())
            domain.setDisplayName(model.getDisplay_name());
        if(model.getView_accessDirtyFlag())
            domain.setViewAccess(model.getView_access());
        if(model.getRule_groupsDirtyFlag())
            domain.setRuleGroups(model.getRule_groups());
        if(model.getModel_accessDirtyFlag())
            domain.setModelAccess(model.getModel_access());
        if(model.getCategory_idDirtyFlag())
            domain.setCategoryId(model.getCategory_id());
        if(model.getIdDirtyFlag())
            domain.setId(model.getId());
        if(model.getMenu_accessDirtyFlag())
            domain.setMenuAccess(model.getMenu_access());
        if(model.get__last_updateDirtyFlag())
            domain.setLastUpdate(model.get__last_update());
        if(model.getCreate_dateDirtyFlag())
            domain.setCreateDate(model.getCreate_date());
        if(model.getCommentDirtyFlag())
            domain.setComment(model.getComment());
        if(model.getWrite_dateDirtyFlag())
            domain.setWriteDate(model.getWrite_date());
        if(model.getShareDirtyFlag())
            domain.setShare(model.getShare());
        if(model.getColorDirtyFlag())
            domain.setColor(model.getColor());
        if(model.getFull_nameDirtyFlag())
            domain.setFullName(model.getFull_name());
        if(model.getCreate_uid_textDirtyFlag())
            domain.setCreateUidText(model.getCreate_uid_text());
        if(model.getWrite_uid_textDirtyFlag())
            domain.setWriteUidText(model.getWrite_uid_text());
        if(model.getCreate_uidDirtyFlag())
            domain.setCreateUid(model.getCreate_uid());
        if(model.getWrite_uidDirtyFlag())
            domain.setWriteUid(model.getWrite_uid());
        return domain ;
    }

}

    



