package cn.ibizlab.odoo.core.repository.client.po;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigDecimal;

/**
 * [lunch_alert] 对象
 */
public interface lunch_alert {

    public String getActive();

    public void setActive(String active);

    public String getAlert_type();

    public void setAlert_type(String alert_type);

    public Timestamp getCreate_date();

    public void setCreate_date(Timestamp create_date);

    public Integer getCreate_uid();

    public void setCreate_uid(Integer create_uid);

    public String getCreate_uid_text();

    public void setCreate_uid_text(String create_uid_text);

    public String getDisplay();

    public void setDisplay(String display);

    public String getDisplay_name();

    public void setDisplay_name(String display_name);

    public Double getEnd_hour();

    public void setEnd_hour(Double end_hour);

    public String getFriday();

    public void setFriday(String friday);

    public Integer getId();

    public void setId(Integer id);

    public String getMessage();

    public void setMessage(String message);

    public String getMonday();

    public void setMonday(String monday);

    public Integer getPartner_id();

    public void setPartner_id(Integer partner_id);

    public String getPartner_id_text();

    public void setPartner_id_text(String partner_id_text);

    public String getSaturday();

    public void setSaturday(String saturday);

    public Timestamp getSpecific_day();

    public void setSpecific_day(Timestamp specific_day);

    public Double getStart_hour();

    public void setStart_hour(Double start_hour);

    public String getSunday();

    public void setSunday(String sunday);

    public String getThursday();

    public void setThursday(String thursday);

    public String getTuesday();

    public void setTuesday(String tuesday);

    public String getWednesday();

    public void setWednesday(String wednesday);

    public Timestamp getWrite_date();

    public void setWrite_date(Timestamp write_date);

    public Integer getWrite_uid();

    public void setWrite_uid(Integer write_uid);

    public String getWrite_uid_text();

    public void setWrite_uid_text(String write_uid_text);

    public Timestamp get__last_update();

    public void set__last_update(Timestamp __last_update);

}
