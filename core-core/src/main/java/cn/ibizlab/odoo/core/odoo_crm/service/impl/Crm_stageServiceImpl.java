package cn.ibizlab.odoo.core.odoo_crm.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_crm.domain.Crm_stage;
import cn.ibizlab.odoo.core.odoo_crm.filter.Crm_stageSearchContext;
import cn.ibizlab.odoo.core.odoo_crm.service.ICrm_stageService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;



import cn.ibizlab.odoo.core.odoo_crm.client.crm_stageOdooClient;
import cn.ibizlab.odoo.core.odoo_crm.clientmodel.crm_stageClientModel;

/**
 * 实体[CRM 阶段] 服务对象接口实现
 */
@Slf4j
@Service
public class Crm_stageServiceImpl implements ICrm_stageService {

    @Autowired
    crm_stageOdooClient crm_stageOdooClient;


    @Override
    public boolean create(Crm_stage et) {
        crm_stageClientModel clientModel = convert2Model(et,null);
		crm_stageOdooClient.create(clientModel);
        Crm_stage rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Crm_stage> list){
    }

    @Override
    public boolean remove(Integer id) {
        crm_stageClientModel clientModel = new crm_stageClientModel();
        clientModel.setId(id);
		crm_stageOdooClient.remove(clientModel);
        return true; 
    }

    public void removeBatch(Collection<Integer> idList){
    }

    @Override
    public boolean update(Crm_stage et) {
        crm_stageClientModel clientModel = convert2Model(et,null);
		crm_stageOdooClient.update(clientModel);
        Crm_stage rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }
    
    public void updateBatch(List<Crm_stage> list){
    }

    @Override
    public Crm_stage get(Integer id) {
        crm_stageClientModel clientModel = new crm_stageClientModel();
        clientModel.setId(id);
		crm_stageOdooClient.get(clientModel);
        Crm_stage et = convert2Domain(clientModel,null);
        if(et==null){
            et=new Crm_stage();
            et.setId(id);
        }
        else{
        }

        return  et;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Crm_stage> searchDefault(Crm_stageSearchContext context) {
        List<Crm_stage> list = new ArrayList<Crm_stage>();
        Page<crm_stageClientModel> clientModelList = crm_stageOdooClient.search(context);
        for(crm_stageClientModel clientModel : clientModelList){
            list.add( this.convert2Domain(clientModel,null));
        }
        return new PageImpl<Crm_stage>(list, context.getPageable(), clientModelList.getTotalElements());
    }

    public crm_stageClientModel convert2Model(Crm_stage domain , crm_stageClientModel model) {
        if(domain == null)
            return model ;
        if(model==null){
            model = new crm_stageClientModel();

        }
        if((Boolean) domain.getExtensionparams().get("dirtyflagenable")){
            if((Boolean) domain.getExtensionparams().get("probabilitydirtyflag"))
                model.setProbability(domain.getProbability());
            if((Boolean) domain.getExtensionparams().get("display_namedirtyflag"))
                model.setDisplay_name(domain.getDisplayName());
            if((Boolean) domain.getExtensionparams().get("requirementsdirtyflag"))
                model.setRequirements(domain.getRequirements());
            if((Boolean) domain.getExtensionparams().get("folddirtyflag"))
                model.setFold(domain.getFold());
            if((Boolean) domain.getExtensionparams().get("sequencedirtyflag"))
                model.setSequence(domain.getSequence());
            if((Boolean) domain.getExtensionparams().get("on_changedirtyflag"))
                model.setOn_change(domain.getOnChange());
            if((Boolean) domain.getExtensionparams().get("write_datedirtyflag"))
                model.setWrite_date(domain.getWriteDate());
            if((Boolean) domain.getExtensionparams().get("legend_prioritydirtyflag"))
                model.setLegend_priority(domain.getLegendPriority());
            if((Boolean) domain.getExtensionparams().get("namedirtyflag"))
                model.setName(domain.getName());
            if((Boolean) domain.getExtensionparams().get("iddirtyflag"))
                model.setId(domain.getId());
            if((Boolean) domain.getExtensionparams().get("create_datedirtyflag"))
                model.setCreate_date(domain.getCreateDate());
            if((Boolean) domain.getExtensionparams().get("team_countdirtyflag"))
                model.setTeam_count(domain.getTeamCount());
            if((Boolean) domain.getExtensionparams().get("__last_updatedirtyflag"))
                model.set__last_update(domain.getLastUpdate());
            if((Boolean) domain.getExtensionparams().get("create_uid_textdirtyflag"))
                model.setCreate_uid_text(domain.getCreateUidText());
            if((Boolean) domain.getExtensionparams().get("write_uid_textdirtyflag"))
                model.setWrite_uid_text(domain.getWriteUidText());
            if((Boolean) domain.getExtensionparams().get("team_id_textdirtyflag"))
                model.setTeam_id_text(domain.getTeamIdText());
            if((Boolean) domain.getExtensionparams().get("create_uiddirtyflag"))
                model.setCreate_uid(domain.getCreateUid());
            if((Boolean) domain.getExtensionparams().get("team_iddirtyflag"))
                model.setTeam_id(domain.getTeamId());
            if((Boolean) domain.getExtensionparams().get("write_uiddirtyflag"))
                model.setWrite_uid(domain.getWriteUid());
            domain.getFocusNull().clear();
        }
        return model ;
    }

    public Crm_stage convert2Domain( crm_stageClientModel model ,Crm_stage domain) {
        if(model == null)
            return domain ;
        if(domain == null){
            domain = new Crm_stage();
        }

        if(model.getProbabilityDirtyFlag())
            domain.setProbability(model.getProbability());
        if(model.getDisplay_nameDirtyFlag())
            domain.setDisplayName(model.getDisplay_name());
        if(model.getRequirementsDirtyFlag())
            domain.setRequirements(model.getRequirements());
        if(model.getFoldDirtyFlag())
            domain.setFold(model.getFold());
        if(model.getSequenceDirtyFlag())
            domain.setSequence(model.getSequence());
        if(model.getOn_changeDirtyFlag())
            domain.setOnChange(model.getOn_change());
        if(model.getWrite_dateDirtyFlag())
            domain.setWriteDate(model.getWrite_date());
        if(model.getLegend_priorityDirtyFlag())
            domain.setLegendPriority(model.getLegend_priority());
        if(model.getNameDirtyFlag())
            domain.setName(model.getName());
        if(model.getIdDirtyFlag())
            domain.setId(model.getId());
        if(model.getCreate_dateDirtyFlag())
            domain.setCreateDate(model.getCreate_date());
        if(model.getTeam_countDirtyFlag())
            domain.setTeamCount(model.getTeam_count());
        if(model.get__last_updateDirtyFlag())
            domain.setLastUpdate(model.get__last_update());
        if(model.getCreate_uid_textDirtyFlag())
            domain.setCreateUidText(model.getCreate_uid_text());
        if(model.getWrite_uid_textDirtyFlag())
            domain.setWriteUidText(model.getWrite_uid_text());
        if(model.getTeam_id_textDirtyFlag())
            domain.setTeamIdText(model.getTeam_id_text());
        if(model.getCreate_uidDirtyFlag())
            domain.setCreateUid(model.getCreate_uid());
        if(model.getTeam_idDirtyFlag())
            domain.setTeamId(model.getTeam_id());
        if(model.getWrite_uidDirtyFlag())
            domain.setWriteUid(model.getWrite_uid());
        return domain ;
    }

}

    



