package cn.ibizlab.odoo.core.repository.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import cn.ibizlab.odoo.core.repository.client.po.repair_fee;

/**
 * 实体[repair_fee] 服务对象接口
 */
public interface repair_feeRepository{


    public repair_fee createPO() ;
        public void get(String id);

        public void create(repair_fee repair_fee);

        public void updateBatch(repair_fee repair_fee);

        public void createBatch(repair_fee repair_fee);

        public void update(repair_fee repair_fee);

        public void removeBatch(String id);

        public void remove(String id);

        public List<repair_fee> search();


}
