package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Account_payment_term;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_payment_termSearchContext;

/**
 * 实体 [付款条款] 存储对象
 */
public interface Account_payment_termRepository extends Repository<Account_payment_term> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Account_payment_term> searchDefault(Account_payment_termSearchContext context);

    Account_payment_term convert2PO(cn.ibizlab.odoo.core.odoo_account.domain.Account_payment_term domain , Account_payment_term po) ;

    cn.ibizlab.odoo.core.odoo_account.domain.Account_payment_term convert2Domain( Account_payment_term po ,cn.ibizlab.odoo.core.odoo_account.domain.Account_payment_term domain) ;

}
