package cn.ibizlab.odoo.core.client.model;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigDecimal;
import java.math.BigInteger;

/**
 * Client 实体 [im_livechat_channel] 对象
 */
public interface Iim_livechat_channel {

    /**
     * 获取 [您是否在频道中？]
     */
    public void setAre_you_inside(String are_you_inside);
    
    /**
     * 设置 [您是否在频道中？]
     */
    public String getAre_you_inside();

    /**
     * 获取 [您是否在频道中？]脏标记
     */
    public boolean getAre_you_insideDirtyFlag();
    /**
     * 获取 [按钮的文本]
     */
    public void setButton_text(String button_text);
    
    /**
     * 设置 [按钮的文本]
     */
    public String getButton_text();

    /**
     * 获取 [按钮的文本]脏标记
     */
    public boolean getButton_textDirtyFlag();
    /**
     * 获取 [会话]
     */
    public void setChannel_ids(String channel_ids);
    
    /**
     * 设置 [会话]
     */
    public String getChannel_ids();

    /**
     * 获取 [会话]脏标记
     */
    public boolean getChannel_idsDirtyFlag();
    /**
     * 获取 [创建时间]
     */
    public void setCreate_date(Timestamp create_date);
    
    /**
     * 设置 [创建时间]
     */
    public Timestamp getCreate_date();

    /**
     * 获取 [创建时间]脏标记
     */
    public boolean getCreate_dateDirtyFlag();
    /**
     * 获取 [创建人]
     */
    public void setCreate_uid(Integer create_uid);
    
    /**
     * 设置 [创建人]
     */
    public Integer getCreate_uid();

    /**
     * 获取 [创建人]脏标记
     */
    public boolean getCreate_uidDirtyFlag();
    /**
     * 获取 [创建人]
     */
    public void setCreate_uid_text(String create_uid_text);
    
    /**
     * 设置 [创建人]
     */
    public String getCreate_uid_text();

    /**
     * 获取 [创建人]脏标记
     */
    public boolean getCreate_uid_textDirtyFlag();
    /**
     * 获取 [欢迎信息]
     */
    public void setDefault_message(String default_message);
    
    /**
     * 设置 [欢迎信息]
     */
    public String getDefault_message();

    /**
     * 获取 [欢迎信息]脏标记
     */
    public boolean getDefault_messageDirtyFlag();
    /**
     * 获取 [显示名称]
     */
    public void setDisplay_name(String display_name);
    
    /**
     * 设置 [显示名称]
     */
    public String getDisplay_name();

    /**
     * 获取 [显示名称]脏标记
     */
    public boolean getDisplay_nameDirtyFlag();
    /**
     * 获取 [ID]
     */
    public void setId(Integer id);
    
    /**
     * 设置 [ID]
     */
    public Integer getId();

    /**
     * 获取 [ID]脏标记
     */
    public boolean getIdDirtyFlag();
    /**
     * 获取 [图像]
     */
    public void setImage(byte[] image);
    
    /**
     * 设置 [图像]
     */
    public byte[] getImage();

    /**
     * 获取 [图像]脏标记
     */
    public boolean getImageDirtyFlag();
    /**
     * 获取 [普通]
     */
    public void setImage_medium(byte[] image_medium);
    
    /**
     * 设置 [普通]
     */
    public byte[] getImage_medium();

    /**
     * 获取 [普通]脏标记
     */
    public boolean getImage_mediumDirtyFlag();
    /**
     * 获取 [缩略图]
     */
    public void setImage_small(byte[] image_small);
    
    /**
     * 设置 [缩略图]
     */
    public byte[] getImage_small();

    /**
     * 获取 [缩略图]脏标记
     */
    public boolean getImage_smallDirtyFlag();
    /**
     * 获取 [聊天输入为空时显示]
     */
    public void setInput_placeholder(String input_placeholder);
    
    /**
     * 设置 [聊天输入为空时显示]
     */
    public String getInput_placeholder();

    /**
     * 获取 [聊天输入为空时显示]脏标记
     */
    public boolean getInput_placeholderDirtyFlag();
    /**
     * 获取 [已发布]
     */
    public void setIs_published(String is_published);
    
    /**
     * 设置 [已发布]
     */
    public String getIs_published();

    /**
     * 获取 [已发布]脏标记
     */
    public boolean getIs_publishedDirtyFlag();
    /**
     * 获取 [名称]
     */
    public void setName(String name);
    
    /**
     * 设置 [名称]
     */
    public String getName();

    /**
     * 获取 [名称]脏标记
     */
    public boolean getNameDirtyFlag();
    /**
     * 获取 [对话数]
     */
    public void setNbr_channel(Integer nbr_channel);
    
    /**
     * 设置 [对话数]
     */
    public Integer getNbr_channel();

    /**
     * 获取 [对话数]脏标记
     */
    public boolean getNbr_channelDirtyFlag();
    /**
     * 获取 [% 高兴]
     */
    public void setRating_percentage_satisfaction(Integer rating_percentage_satisfaction);
    
    /**
     * 设置 [% 高兴]
     */
    public Integer getRating_percentage_satisfaction();

    /**
     * 获取 [% 高兴]脏标记
     */
    public boolean getRating_percentage_satisfactionDirtyFlag();
    /**
     * 获取 [规则]
     */
    public void setRule_ids(String rule_ids);
    
    /**
     * 设置 [规则]
     */
    public String getRule_ids();

    /**
     * 获取 [规则]脏标记
     */
    public boolean getRule_idsDirtyFlag();
    /**
     * 获取 [脚本（外部）]
     */
    public void setScript_external(String script_external);
    
    /**
     * 设置 [脚本（外部）]
     */
    public String getScript_external();

    /**
     * 获取 [脚本（外部）]脏标记
     */
    public boolean getScript_externalDirtyFlag();
    /**
     * 获取 [操作员]
     */
    public void setUser_ids(String user_ids);
    
    /**
     * 设置 [操作员]
     */
    public String getUser_ids();

    /**
     * 获取 [操作员]脏标记
     */
    public boolean getUser_idsDirtyFlag();
    /**
     * 获取 [网站说明]
     */
    public void setWebsite_description(String website_description);
    
    /**
     * 设置 [网站说明]
     */
    public String getWebsite_description();

    /**
     * 获取 [网站说明]脏标记
     */
    public boolean getWebsite_descriptionDirtyFlag();
    /**
     * 获取 [在当前网站显示]
     */
    public void setWebsite_published(String website_published);
    
    /**
     * 设置 [在当前网站显示]
     */
    public String getWebsite_published();

    /**
     * 获取 [在当前网站显示]脏标记
     */
    public boolean getWebsite_publishedDirtyFlag();
    /**
     * 获取 [网站网址]
     */
    public void setWebsite_url(String website_url);
    
    /**
     * 设置 [网站网址]
     */
    public String getWebsite_url();

    /**
     * 获取 [网站网址]脏标记
     */
    public boolean getWebsite_urlDirtyFlag();
    /**
     * 获取 [Web页]
     */
    public void setWeb_page(String web_page);
    
    /**
     * 设置 [Web页]
     */
    public String getWeb_page();

    /**
     * 获取 [Web页]脏标记
     */
    public boolean getWeb_pageDirtyFlag();
    /**
     * 获取 [最后更新时间]
     */
    public void setWrite_date(Timestamp write_date);
    
    /**
     * 设置 [最后更新时间]
     */
    public Timestamp getWrite_date();

    /**
     * 获取 [最后更新时间]脏标记
     */
    public boolean getWrite_dateDirtyFlag();
    /**
     * 获取 [最后更新人]
     */
    public void setWrite_uid(Integer write_uid);
    
    /**
     * 设置 [最后更新人]
     */
    public Integer getWrite_uid();

    /**
     * 获取 [最后更新人]脏标记
     */
    public boolean getWrite_uidDirtyFlag();
    /**
     * 获取 [最后更新人]
     */
    public void setWrite_uid_text(String write_uid_text);
    
    /**
     * 设置 [最后更新人]
     */
    public String getWrite_uid_text();

    /**
     * 获取 [最后更新人]脏标记
     */
    public boolean getWrite_uid_textDirtyFlag();
    /**
     * 获取 [最后修改日]
     */
    public void set__last_update(Timestamp __last_update);
    
    /**
     * 设置 [最后修改日]
     */
    public Timestamp get__last_update();

    /**
     * 获取 [最后修改日]脏标记
     */
    public boolean get__last_updateDirtyFlag();


    /**
     *  转换为Map
     */
    public Map<String, Object> toMap() throws Exception ;

    /**
     *  从Map构建
     */
   public void fromMap(Map<String, Object> map) throws Exception;
}
