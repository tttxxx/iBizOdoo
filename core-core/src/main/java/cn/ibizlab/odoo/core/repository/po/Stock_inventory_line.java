package cn.ibizlab.odoo.core.repository.po ;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.math.BigInteger;

import cn.ibizlab.odoo.core.odoo_stock.filter.Stock_inventory_lineSearchContext;

/**
 * 实体 [库存明细] 存储模型
 */
public interface Stock_inventory_line{

    /**
     * ID
     */
    Integer getId();

    void setId(Integer id);

    /**
     * 获取 [ID]脏标记
     */
    boolean getIdDirtyFlag();

    /**
     * 显示名称
     */
    String getDisplay_name();

    void setDisplay_name(String display_name);

    /**
     * 获取 [显示名称]脏标记
     */
    boolean getDisplay_nameDirtyFlag();

    /**
     * 理论数量
     */
    Double getTheoretical_qty();

    void setTheoretical_qty(Double theoretical_qty);

    /**
     * 获取 [理论数量]脏标记
     */
    boolean getTheoretical_qtyDirtyFlag();

    /**
     * 最后更新时间
     */
    Timestamp getWrite_date();

    void setWrite_date(Timestamp write_date);

    /**
     * 获取 [最后更新时间]脏标记
     */
    boolean getWrite_dateDirtyFlag();

    /**
     * 创建时间
     */
    Timestamp getCreate_date();

    void setCreate_date(Timestamp create_date);

    /**
     * 获取 [创建时间]脏标记
     */
    boolean getCreate_dateDirtyFlag();

    /**
     * 最后修改日
     */
    Timestamp get__last_update();

    void set__last_update(Timestamp __last_update);

    /**
     * 获取 [最后修改日]脏标记
     */
    boolean get__last_updateDirtyFlag();

    /**
     * 已检查的数量
     */
    Double getProduct_qty();

    void setProduct_qty(Double product_qty);

    /**
     * 获取 [已检查的数量]脏标记
     */
    boolean getProduct_qtyDirtyFlag();

    /**
     * 包裹
     */
    String getPackage_id_text();

    void setPackage_id_text(String package_id_text);

    /**
     * 获取 [包裹]脏标记
     */
    boolean getPackage_id_textDirtyFlag();

    /**
     * 度量单位分类
     */
    Integer getProduct_uom_category_id();

    void setProduct_uom_category_id(Integer product_uom_category_id);

    /**
     * 获取 [度量单位分类]脏标记
     */
    boolean getProduct_uom_category_idDirtyFlag();

    /**
     * 库存
     */
    String getInventory_id_text();

    void setInventory_id_text(String inventory_id_text);

    /**
     * 获取 [库存]脏标记
     */
    boolean getInventory_id_textDirtyFlag();

    /**
     * 库存位置
     */
    Integer getInventory_location_id();

    void setInventory_location_id(Integer inventory_location_id);

    /**
     * 获取 [库存位置]脏标记
     */
    boolean getInventory_location_idDirtyFlag();

    /**
     * 所有者
     */
    String getPartner_id_text();

    void setPartner_id_text(String partner_id_text);

    /**
     * 获取 [所有者]脏标记
     */
    boolean getPartner_id_textDirtyFlag();

    /**
     * 状态
     */
    String getState();

    void setState(String state);

    /**
     * 获取 [状态]脏标记
     */
    boolean getStateDirtyFlag();

    /**
     * 位置
     */
    String getLocation_id_text();

    void setLocation_id_text(String location_id_text);

    /**
     * 获取 [位置]脏标记
     */
    boolean getLocation_id_textDirtyFlag();

    /**
     * 产品
     */
    String getProduct_id_text();

    void setProduct_id_text(String product_id_text);

    /**
     * 获取 [产品]脏标记
     */
    boolean getProduct_id_textDirtyFlag();

    /**
     * 创建人
     */
    String getCreate_uid_text();

    void setCreate_uid_text(String create_uid_text);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uid_textDirtyFlag();

    /**
     * 最后更新者
     */
    String getWrite_uid_text();

    void setWrite_uid_text(String write_uid_text);

    /**
     * 获取 [最后更新者]脏标记
     */
    boolean getWrite_uid_textDirtyFlag();

    /**
     * 公司
     */
    String getCompany_id_text();

    void setCompany_id_text(String company_id_text);

    /**
     * 获取 [公司]脏标记
     */
    boolean getCompany_id_textDirtyFlag();

    /**
     * 产品量度单位
     */
    String getProduct_uom_id_text();

    void setProduct_uom_id_text(String product_uom_id_text);

    /**
     * 获取 [产品量度单位]脏标记
     */
    boolean getProduct_uom_id_textDirtyFlag();

    /**
     * 批次/序列号码
     */
    String getProd_lot_id_text();

    void setProd_lot_id_text(String prod_lot_id_text);

    /**
     * 获取 [批次/序列号码]脏标记
     */
    boolean getProd_lot_id_textDirtyFlag();

    /**
     * 追踪
     */
    String getProduct_tracking();

    void setProduct_tracking(String product_tracking);

    /**
     * 获取 [追踪]脏标记
     */
    boolean getProduct_trackingDirtyFlag();

    /**
     * 位置
     */
    Integer getLocation_id();

    void setLocation_id(Integer location_id);

    /**
     * 获取 [位置]脏标记
     */
    boolean getLocation_idDirtyFlag();

    /**
     * 产品
     */
    Integer getProduct_id();

    void setProduct_id(Integer product_id);

    /**
     * 获取 [产品]脏标记
     */
    boolean getProduct_idDirtyFlag();

    /**
     * 批次/序列号码
     */
    Integer getProd_lot_id();

    void setProd_lot_id(Integer prod_lot_id);

    /**
     * 获取 [批次/序列号码]脏标记
     */
    boolean getProd_lot_idDirtyFlag();

    /**
     * 创建人
     */
    Integer getCreate_uid();

    void setCreate_uid(Integer create_uid);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uidDirtyFlag();

    /**
     * 公司
     */
    Integer getCompany_id();

    void setCompany_id(Integer company_id);

    /**
     * 获取 [公司]脏标记
     */
    boolean getCompany_idDirtyFlag();

    /**
     * 包裹
     */
    Integer getPackage_id();

    void setPackage_id(Integer package_id);

    /**
     * 获取 [包裹]脏标记
     */
    boolean getPackage_idDirtyFlag();

    /**
     * 最后更新者
     */
    Integer getWrite_uid();

    void setWrite_uid(Integer write_uid);

    /**
     * 获取 [最后更新者]脏标记
     */
    boolean getWrite_uidDirtyFlag();

    /**
     * 所有者
     */
    Integer getPartner_id();

    void setPartner_id(Integer partner_id);

    /**
     * 获取 [所有者]脏标记
     */
    boolean getPartner_idDirtyFlag();

    /**
     * 库存
     */
    Integer getInventory_id();

    void setInventory_id(Integer inventory_id);

    /**
     * 获取 [库存]脏标记
     */
    boolean getInventory_idDirtyFlag();

    /**
     * 产品量度单位
     */
    Integer getProduct_uom_id();

    void setProduct_uom_id(Integer product_uom_id);

    /**
     * 获取 [产品量度单位]脏标记
     */
    boolean getProduct_uom_idDirtyFlag();

}
