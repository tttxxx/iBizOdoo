package cn.ibizlab.odoo.core.repository.client.po;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigDecimal;

/**
 * [im_livechat_channel_rule] 对象
 */
public interface im_livechat_channel_rule {

    public String getAction();

    public void setAction(String action);

    public Integer getAuto_popup_timer();

    public void setAuto_popup_timer(Integer auto_popup_timer);

    public Integer getChannel_id();

    public void setChannel_id(Integer channel_id);

    public String getChannel_id_text();

    public void setChannel_id_text(String channel_id_text);

    public String getCountry_ids();

    public void setCountry_ids(String country_ids);

    public Timestamp getCreate_date();

    public void setCreate_date(Timestamp create_date);

    public Integer getCreate_uid();

    public void setCreate_uid(Integer create_uid);

    public String getCreate_uid_text();

    public void setCreate_uid_text(String create_uid_text);

    public String getDisplay_name();

    public void setDisplay_name(String display_name);

    public Integer getId();

    public void setId(Integer id);

    public String getRegex_url();

    public void setRegex_url(String regex_url);

    public Integer getSequence();

    public void setSequence(Integer sequence);

    public Timestamp getWrite_date();

    public void setWrite_date(Timestamp write_date);

    public Integer getWrite_uid();

    public void setWrite_uid(Integer write_uid);

    public String getWrite_uid_text();

    public void setWrite_uid_text(String write_uid_text);

    public Timestamp get__last_update();

    public void set__last_update(Timestamp __last_update);

}
