package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Mail_message_subtype;
import cn.ibizlab.odoo.core.odoo_mail.filter.Mail_message_subtypeSearchContext;

/**
 * 实体 [消息子类型] 存储对象
 */
public interface Mail_message_subtypeRepository extends Repository<Mail_message_subtype> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Mail_message_subtype> searchDefault(Mail_message_subtypeSearchContext context);

    Mail_message_subtype convert2PO(cn.ibizlab.odoo.core.odoo_mail.domain.Mail_message_subtype domain , Mail_message_subtype po) ;

    cn.ibizlab.odoo.core.odoo_mail.domain.Mail_message_subtype convert2Domain( Mail_message_subtype po ,cn.ibizlab.odoo.core.odoo_mail.domain.Mail_message_subtype domain) ;

}
