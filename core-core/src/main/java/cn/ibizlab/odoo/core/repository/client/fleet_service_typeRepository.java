package cn.ibizlab.odoo.core.repository.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import cn.ibizlab.odoo.core.repository.client.po.fleet_service_type;

/**
 * 实体[fleet_service_type] 服务对象接口
 */
public interface fleet_service_typeRepository{


    public fleet_service_type createPO() ;
        public void updateBatch(fleet_service_type fleet_service_type);

        public void update(fleet_service_type fleet_service_type);

        public void removeBatch(String id);

        public void get(String id);

        public void createBatch(fleet_service_type fleet_service_type);

        public void create(fleet_service_type fleet_service_type);

        public void remove(String id);

        public List<fleet_service_type> search();


}
