package cn.ibizlab.odoo.core.repository.po ;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.math.BigInteger;

import cn.ibizlab.odoo.core.odoo_mrp.filter.Mrp_workorderSearchContext;

/**
 * 实体 [工单] 存储模型
 */
public interface Mrp_workorder{

    /**
     * 是关注者
     */
    String getMessage_is_follower();

    void setMessage_is_follower(String message_is_follower);

    /**
     * 获取 [是关注者]脏标记
     */
    boolean getMessage_is_followerDirtyFlag();

    /**
     * 未读消息
     */
    String getMessage_unread();

    void setMessage_unread(String message_unread);

    /**
     * 获取 [未读消息]脏标记
     */
    boolean getMessage_unreadDirtyFlag();

    /**
     * 消息递送错误
     */
    String getMessage_has_error();

    void setMessage_has_error(String message_has_error);

    /**
     * 获取 [消息递送错误]脏标记
     */
    boolean getMessage_has_errorDirtyFlag();

    /**
     * 时间
     */
    String getTime_ids();

    void setTime_ids(String time_ids);

    /**
     * 获取 [时间]脏标记
     */
    boolean getTime_idsDirtyFlag();

    /**
     * 消息
     */
    String getMessage_ids();

    void setMessage_ids(String message_ids);

    /**
     * 获取 [消息]脏标记
     */
    boolean getMessage_idsDirtyFlag();

    /**
     * 在此工单工作的用户
     */
    String getWorking_user_ids();

    void setWorking_user_ids(String working_user_ids);

    /**
     * 获取 [在此工单工作的用户]脏标记
     */
    boolean getWorking_user_idsDirtyFlag();

    /**
     * 显示名称
     */
    String getDisplay_name();

    void setDisplay_name(String display_name);

    /**
     * 获取 [显示名称]脏标记
     */
    boolean getDisplay_nameDirtyFlag();

    /**
     * 数量
     */
    Double getQty_produced();

    void setQty_produced(Double qty_produced);

    /**
     * 获取 [数量]脏标记
     */
    boolean getQty_producedDirtyFlag();

    /**
     * 实际时长
     */
    Double getDuration();

    void setDuration(Double duration);

    /**
     * 获取 [实际时长]脏标记
     */
    boolean getDurationDirtyFlag();

    /**
     * 关注者(业务伙伴)
     */
    String getMessage_partner_ids();

    void setMessage_partner_ids(String message_partner_ids);

    /**
     * 获取 [关注者(业务伙伴)]脏标记
     */
    boolean getMessage_partner_idsDirtyFlag();

    /**
     * 状态
     */
    String getState();

    void setState(String state);

    /**
     * 获取 [状态]脏标记
     */
    boolean getStateDirtyFlag();

    /**
     * 报废转移
     */
    Integer getScrap_count();

    void setScrap_count(Integer scrap_count);

    /**
     * 获取 [报废转移]脏标记
     */
    boolean getScrap_countDirtyFlag();

    /**
     * 安排的开始日期
     */
    Timestamp getDate_planned_start();

    void setDate_planned_start(Timestamp date_planned_start);

    /**
     * 获取 [安排的开始日期]脏标记
     */
    boolean getDate_planned_startDirtyFlag();

    /**
     * 需要采取行动
     */
    String getMessage_needaction();

    void setMessage_needaction(String message_needaction);

    /**
     * 获取 [需要采取行动]脏标记
     */
    boolean getMessage_needactionDirtyFlag();

    /**
     * 当前用户正在工作吗？
     */
    String getIs_user_working();

    void setIs_user_working(String is_user_working);

    /**
     * 获取 [当前用户正在工作吗？]脏标记
     */
    boolean getIs_user_workingDirtyFlag();

    /**
     * 移动
     */
    String getMove_raw_ids();

    void setMove_raw_ids(String move_raw_ids);

    /**
     * 获取 [移动]脏标记
     */
    boolean getMove_raw_idsDirtyFlag();

    /**
     * 上一个在此工单工作的用户
     */
    String getLast_working_user_id();

    void setLast_working_user_id(String last_working_user_id);

    /**
     * 获取 [上一个在此工单工作的用户]脏标记
     */
    boolean getLast_working_user_idDirtyFlag();

    /**
     * 行动数量
     */
    Integer getMessage_needaction_counter();

    void setMessage_needaction_counter(Integer message_needaction_counter);

    /**
     * 获取 [行动数量]脏标记
     */
    boolean getMessage_needaction_counterDirtyFlag();

    /**
     * 待追踪的产品
     */
    String getMove_line_ids();

    void setMove_line_ids(String move_line_ids);

    /**
     * 获取 [待追踪的产品]脏标记
     */
    boolean getMove_line_idsDirtyFlag();

    /**
     * 工单
     */
    String getName();

    void setName(String name);

    /**
     * 获取 [工单]脏标记
     */
    boolean getNameDirtyFlag();

    /**
     * Is the first WO to produce
     */
    String getIs_first_wo();

    void setIs_first_wo(String is_first_wo);

    /**
     * 获取 [Is the first WO to produce]脏标记
     */
    boolean getIs_first_woDirtyFlag();

    /**
     * 已生产
     */
    String getIs_produced();

    void setIs_produced(String is_produced);

    /**
     * 获取 [已生产]脏标记
     */
    boolean getIs_producedDirtyFlag();

    /**
     * 实际开始日期
     */
    Timestamp getDate_start();

    void setDate_start(Timestamp date_start);

    /**
     * 获取 [实际开始日期]脏标记
     */
    boolean getDate_startDirtyFlag();

    /**
     * 网站信息
     */
    String getWebsite_message_ids();

    void setWebsite_message_ids(String website_message_ids);

    /**
     * 获取 [网站信息]脏标记
     */
    boolean getWebsite_message_idsDirtyFlag();

    /**
     * 最后修改日
     */
    Timestamp get__last_update();

    void set__last_update(Timestamp __last_update);

    /**
     * 获取 [最后修改日]脏标记
     */
    boolean get__last_updateDirtyFlag();

    /**
     * 关注者
     */
    String getMessage_follower_ids();

    void setMessage_follower_ids(String message_follower_ids);

    /**
     * 获取 [关注者]脏标记
     */
    boolean getMessage_follower_idsDirtyFlag();

    /**
     * 颜色
     */
    Integer getColor();

    void setColor(Integer color);

    /**
     * 获取 [颜色]脏标记
     */
    boolean getColorDirtyFlag();

    /**
     * 附件数量
     */
    Integer getMessage_attachment_count();

    void setMessage_attachment_count(Integer message_attachment_count);

    /**
     * 获取 [附件数量]脏标记
     */
    boolean getMessage_attachment_countDirtyFlag();

    /**
     * 最后更新时间
     */
    Timestamp getWrite_date();

    void setWrite_date(Timestamp write_date);

    /**
     * 获取 [最后更新时间]脏标记
     */
    boolean getWrite_dateDirtyFlag();

    /**
     * 关注者(渠道)
     */
    String getMessage_channel_ids();

    void setMessage_channel_ids(String message_channel_ids);

    /**
     * 获取 [关注者(渠道)]脏标记
     */
    boolean getMessage_channel_idsDirtyFlag();

    /**
     * 每单位时长
     */
    Double getDuration_unit();

    void setDuration_unit(Double duration_unit);

    /**
     * 获取 [每单位时长]脏标记
     */
    boolean getDuration_unitDirtyFlag();

    /**
     * 附件
     */
    Integer getMessage_main_attachment_id();

    void setMessage_main_attachment_id(Integer message_main_attachment_id);

    /**
     * 获取 [附件]脏标记
     */
    boolean getMessage_main_attachment_idDirtyFlag();

    /**
     * 容量
     */
    Double getCapacity();

    void setCapacity(Double capacity);

    /**
     * 获取 [容量]脏标记
     */
    boolean getCapacityDirtyFlag();

    /**
     * 操作凭证行
     */
    String getActive_move_line_ids();

    void setActive_move_line_ids(String active_move_line_ids);

    /**
     * 获取 [操作凭证行]脏标记
     */
    boolean getActive_move_line_idsDirtyFlag();

    /**
     * 安排的完工日期
     */
    Timestamp getDate_planned_finished();

    void setDate_planned_finished(Timestamp date_planned_finished);

    /**
     * 获取 [安排的完工日期]脏标记
     */
    boolean getDate_planned_finishedDirtyFlag();

    /**
     * 实际结束日期
     */
    Timestamp getDate_finished();

    void setDate_finished(Timestamp date_finished);

    /**
     * 获取 [实际结束日期]脏标记
     */
    boolean getDate_finishedDirtyFlag();

    /**
     * 将被生产的数量
     */
    Double getQty_remaining();

    void setQty_remaining(Double qty_remaining);

    /**
     * 获取 [将被生产的数量]脏标记
     */
    boolean getQty_remainingDirtyFlag();

    /**
     * 报废
     */
    String getScrap_ids();

    void setScrap_ids(String scrap_ids);

    /**
     * 获取 [报废]脏标记
     */
    boolean getScrap_idsDirtyFlag();

    /**
     * 时长偏差(%)
     */
    Integer getDuration_percent();

    void setDuration_percent(Integer duration_percent);

    /**
     * 获取 [时长偏差(%)]脏标记
     */
    boolean getDuration_percentDirtyFlag();

    /**
     * ID
     */
    Integer getId();

    void setId(Integer id);

    /**
     * 获取 [ID]脏标记
     */
    boolean getIdDirtyFlag();

    /**
     * 创建时间
     */
    Timestamp getCreate_date();

    void setCreate_date(Timestamp create_date);

    /**
     * 获取 [创建时间]脏标记
     */
    boolean getCreate_dateDirtyFlag();

    /**
     * 预计时长
     */
    Double getDuration_expected();

    void setDuration_expected(Double duration_expected);

    /**
     * 获取 [预计时长]脏标记
     */
    boolean getDuration_expectedDirtyFlag();

    /**
     * 错误数
     */
    Integer getMessage_has_error_counter();

    void setMessage_has_error_counter(Integer message_has_error_counter);

    /**
     * 获取 [错误数]脏标记
     */
    boolean getMessage_has_error_counterDirtyFlag();

    /**
     * 未读消息计数器
     */
    Integer getMessage_unread_counter();

    void setMessage_unread_counter(Integer message_unread_counter);

    /**
     * 获取 [未读消息计数器]脏标记
     */
    boolean getMessage_unread_counterDirtyFlag();

    /**
     * 当前的已生产数量
     */
    Double getQty_producing();

    void setQty_producing(Double qty_producing);

    /**
     * 获取 [当前的已生产数量]脏标记
     */
    boolean getQty_producingDirtyFlag();

    /**
     * 生产日期
     */
    Timestamp getProduction_date();

    void setProduction_date(Timestamp production_date);

    /**
     * 获取 [生产日期]脏标记
     */
    boolean getProduction_dateDirtyFlag();

    /**
     * 工作记录表
     */
    byte[] getWorksheet();

    void setWorksheet(byte[] worksheet);

    /**
     * 获取 [工作记录表]脏标记
     */
    boolean getWorksheetDirtyFlag();

    /**
     * 工作中心
     */
    String getWorkcenter_id_text();

    void setWorkcenter_id_text(String workcenter_id_text);

    /**
     * 获取 [工作中心]脏标记
     */
    boolean getWorkcenter_id_textDirtyFlag();

    /**
     * 批次/序列号码
     */
    String getFinal_lot_id_text();

    void setFinal_lot_id_text(String final_lot_id_text);

    /**
     * 获取 [批次/序列号码]脏标记
     */
    boolean getFinal_lot_id_textDirtyFlag();

    /**
     * 工作中心状态
     */
    String getWorking_state();

    void setWorking_state(String working_state);

    /**
     * 获取 [工作中心状态]脏标记
     */
    boolean getWorking_stateDirtyFlag();

    /**
     * 原始生产数量
     */
    Double getQty_production();

    void setQty_production(Double qty_production);

    /**
     * 获取 [原始生产数量]脏标记
     */
    boolean getQty_productionDirtyFlag();

    /**
     * 计量单位
     */
    Integer getProduct_uom_id();

    void setProduct_uom_id(Integer product_uom_id);

    /**
     * 获取 [计量单位]脏标记
     */
    boolean getProduct_uom_idDirtyFlag();

    /**
     * 创建人
     */
    String getCreate_uid_text();

    void setCreate_uid_text(String create_uid_text);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uid_textDirtyFlag();

    /**
     * 材料可用性
     */
    String getProduction_availability();

    void setProduction_availability(String production_availability);

    /**
     * 获取 [材料可用性]脏标记
     */
    boolean getProduction_availabilityDirtyFlag();

    /**
     * 状态
     */
    String getProduction_state();

    void setProduction_state(String production_state);

    /**
     * 获取 [状态]脏标记
     */
    boolean getProduction_stateDirtyFlag();

    /**
     * 下一工单
     */
    String getNext_work_order_id_text();

    void setNext_work_order_id_text(String next_work_order_id_text);

    /**
     * 获取 [下一工单]脏标记
     */
    boolean getNext_work_order_id_textDirtyFlag();

    /**
     * 操作
     */
    String getOperation_id_text();

    void setOperation_id_text(String operation_id_text);

    /**
     * 获取 [操作]脏标记
     */
    boolean getOperation_id_textDirtyFlag();

    /**
     * 最后更新者
     */
    String getWrite_uid_text();

    void setWrite_uid_text(String write_uid_text);

    /**
     * 获取 [最后更新者]脏标记
     */
    boolean getWrite_uid_textDirtyFlag();

    /**
     * 制造订单
     */
    String getProduction_id_text();

    void setProduction_id_text(String production_id_text);

    /**
     * 获取 [制造订单]脏标记
     */
    boolean getProduction_id_textDirtyFlag();

    /**
     * 产品
     */
    String getProduct_id_text();

    void setProduct_id_text(String product_id_text);

    /**
     * 获取 [产品]脏标记
     */
    boolean getProduct_id_textDirtyFlag();

    /**
     * 追踪
     */
    String getProduct_tracking();

    void setProduct_tracking(String product_tracking);

    /**
     * 获取 [追踪]脏标记
     */
    boolean getProduct_trackingDirtyFlag();

    /**
     * 批次/序列号码
     */
    Integer getFinal_lot_id();

    void setFinal_lot_id(Integer final_lot_id);

    /**
     * 获取 [批次/序列号码]脏标记
     */
    boolean getFinal_lot_idDirtyFlag();

    /**
     * 工作中心
     */
    Integer getWorkcenter_id();

    void setWorkcenter_id(Integer workcenter_id);

    /**
     * 获取 [工作中心]脏标记
     */
    boolean getWorkcenter_idDirtyFlag();

    /**
     * 制造订单
     */
    Integer getProduction_id();

    void setProduction_id(Integer production_id);

    /**
     * 获取 [制造订单]脏标记
     */
    boolean getProduction_idDirtyFlag();

    /**
     * 最后更新者
     */
    Integer getWrite_uid();

    void setWrite_uid(Integer write_uid);

    /**
     * 获取 [最后更新者]脏标记
     */
    boolean getWrite_uidDirtyFlag();

    /**
     * 操作
     */
    Integer getOperation_id();

    void setOperation_id(Integer operation_id);

    /**
     * 获取 [操作]脏标记
     */
    boolean getOperation_idDirtyFlag();

    /**
     * 下一工单
     */
    Integer getNext_work_order_id();

    void setNext_work_order_id(Integer next_work_order_id);

    /**
     * 获取 [下一工单]脏标记
     */
    boolean getNext_work_order_idDirtyFlag();

    /**
     * 产品
     */
    Integer getProduct_id();

    void setProduct_id(Integer product_id);

    /**
     * 获取 [产品]脏标记
     */
    boolean getProduct_idDirtyFlag();

    /**
     * 创建人
     */
    Integer getCreate_uid();

    void setCreate_uid(Integer create_uid);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uidDirtyFlag();

}
