package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Crm_lost_reason;
import cn.ibizlab.odoo.core.odoo_crm.filter.Crm_lost_reasonSearchContext;

/**
 * 实体 [丢单原因] 存储对象
 */
public interface Crm_lost_reasonRepository extends Repository<Crm_lost_reason> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Crm_lost_reason> searchDefault(Crm_lost_reasonSearchContext context);

    Crm_lost_reason convert2PO(cn.ibizlab.odoo.core.odoo_crm.domain.Crm_lost_reason domain , Crm_lost_reason po) ;

    cn.ibizlab.odoo.core.odoo_crm.domain.Crm_lost_reason convert2Domain( Crm_lost_reason po ,cn.ibizlab.odoo.core.odoo_crm.domain.Crm_lost_reason domain) ;

}
