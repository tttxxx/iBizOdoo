package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Iaccount_reconciliation_widget;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[account_reconciliation_widget] 服务对象接口
 */
public interface Iaccount_reconciliation_widgetClientService{

    public Iaccount_reconciliation_widget createModel() ;

    public void removeBatch(List<Iaccount_reconciliation_widget> account_reconciliation_widgets);

    public void update(Iaccount_reconciliation_widget account_reconciliation_widget);

    public Page<Iaccount_reconciliation_widget> search(SearchContext context);

    public void createBatch(List<Iaccount_reconciliation_widget> account_reconciliation_widgets);

    public void get(Iaccount_reconciliation_widget account_reconciliation_widget);

    public void create(Iaccount_reconciliation_widget account_reconciliation_widget);

    public void remove(Iaccount_reconciliation_widget account_reconciliation_widget);

    public void updateBatch(List<Iaccount_reconciliation_widget> account_reconciliation_widgets);

    public Page<Iaccount_reconciliation_widget> select(SearchContext context);

}
