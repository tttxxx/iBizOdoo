package cn.ibizlab.odoo.core.odoo_product.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_product.domain.Product_image;
import cn.ibizlab.odoo.core.odoo_product.filter.Product_imageSearchContext;


/**
 * 实体[Product_image] 服务对象接口
 */
public interface IProduct_imageService{

    boolean update(Product_image et) ;
    void updateBatch(List<Product_image> list) ;
    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    Product_image get(Integer key) ;
    boolean create(Product_image et) ;
    void createBatch(List<Product_image> list) ;
    Page<Product_image> searchDefault(Product_imageSearchContext context) ;

}



