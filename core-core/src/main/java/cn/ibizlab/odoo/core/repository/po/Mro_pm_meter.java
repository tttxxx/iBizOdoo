package cn.ibizlab.odoo.core.repository.po ;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.math.BigInteger;

import cn.ibizlab.odoo.core.odoo_mro.filter.Mro_pm_meterSearchContext;

/**
 * 实体 [Asset Meters] 存储模型
 */
public interface Mro_pm_meter{

    /**
     * ID
     */
    Integer getId();

    void setId(Integer id);

    /**
     * 获取 [ID]脏标记
     */
    boolean getIdDirtyFlag();

    /**
     * Min Utilization (per day)
     */
    Double getMin_utilization();

    void setMin_utilization(Double min_utilization);

    /**
     * 获取 [Min Utilization (per day)]脏标记
     */
    boolean getMin_utilizationDirtyFlag();

    /**
     * 日期
     */
    Timestamp getDate();

    void setDate(Timestamp date);

    /**
     * 获取 [日期]脏标记
     */
    boolean getDateDirtyFlag();

    /**
     * Reading Type
     */
    String getReading_type();

    void setReading_type(String reading_type);

    /**
     * 获取 [Reading Type]脏标记
     */
    boolean getReading_typeDirtyFlag();

    /**
     * 值
     */
    Double getValue();

    void setValue(Double value);

    /**
     * 获取 [值]脏标记
     */
    boolean getValueDirtyFlag();

    /**
     * Meters
     */
    String getMeter_line_ids();

    void setMeter_line_ids(String meter_line_ids);

    /**
     * 获取 [Meters]脏标记
     */
    boolean getMeter_line_idsDirtyFlag();

    /**
     * 显示名称
     */
    String getDisplay_name();

    void setDisplay_name(String display_name);

    /**
     * 获取 [显示名称]脏标记
     */
    boolean getDisplay_nameDirtyFlag();

    /**
     * 最后更新时间
     */
    Timestamp getWrite_date();

    void setWrite_date(Timestamp write_date);

    /**
     * 获取 [最后更新时间]脏标记
     */
    boolean getWrite_dateDirtyFlag();

    /**
     * View Line
     */
    String getView_line_ids();

    void setView_line_ids(String view_line_ids);

    /**
     * 获取 [View Line]脏标记
     */
    boolean getView_line_idsDirtyFlag();

    /**
     * 状态
     */
    String getState();

    void setState(String state);

    /**
     * 获取 [状态]脏标记
     */
    boolean getStateDirtyFlag();

    /**
     * 创建时间
     */
    Timestamp getCreate_date();

    void setCreate_date(Timestamp create_date);

    /**
     * 获取 [创建时间]脏标记
     */
    boolean getCreate_dateDirtyFlag();

    /**
     * 最后修改日
     */
    Timestamp get__last_update();

    void set__last_update(Timestamp __last_update);

    /**
     * 获取 [最后修改日]脏标记
     */
    boolean get__last_updateDirtyFlag();

    /**
     * Utilization (per day)
     */
    Double getUtilization();

    void setUtilization(Double utilization);

    /**
     * 获取 [Utilization (per day)]脏标记
     */
    boolean getUtilizationDirtyFlag();

    /**
     * New value
     */
    Double getNew_value();

    void setNew_value(Double new_value);

    /**
     * 获取 [New value]脏标记
     */
    boolean getNew_valueDirtyFlag();

    /**
     * Averaging time (days)
     */
    Double getAv_time();

    void setAv_time(Double av_time);

    /**
     * 获取 [Averaging time (days)]脏标记
     */
    boolean getAv_timeDirtyFlag();

    /**
     * Total Value
     */
    Double getTotal_value();

    void setTotal_value(Double total_value);

    /**
     * 获取 [Total Value]脏标记
     */
    boolean getTotal_valueDirtyFlag();

    /**
     * Meter
     */
    String getName_text();

    void setName_text(String name_text);

    /**
     * 获取 [Meter]脏标记
     */
    boolean getName_textDirtyFlag();

    /**
     * 创建人
     */
    String getCreate_uid_text();

    void setCreate_uid_text(String create_uid_text);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uid_textDirtyFlag();

    /**
     * 单位
     */
    Integer getMeter_uom();

    void setMeter_uom(Integer meter_uom);

    /**
     * 获取 [单位]脏标记
     */
    boolean getMeter_uomDirtyFlag();

    /**
     * 最后更新者
     */
    String getWrite_uid_text();

    void setWrite_uid_text(String write_uid_text);

    /**
     * 获取 [最后更新者]脏标记
     */
    boolean getWrite_uid_textDirtyFlag();

    /**
     * Asset
     */
    String getAsset_id_text();

    void setAsset_id_text(String asset_id_text);

    /**
     * 获取 [Asset]脏标记
     */
    boolean getAsset_id_textDirtyFlag();

    /**
     * Ratio to Source
     */
    String getParent_ratio_id_text();

    void setParent_ratio_id_text(String parent_ratio_id_text);

    /**
     * 获取 [Ratio to Source]脏标记
     */
    boolean getParent_ratio_id_textDirtyFlag();

    /**
     * Source Meter
     */
    Integer getParent_meter_id();

    void setParent_meter_id(Integer parent_meter_id);

    /**
     * 获取 [Source Meter]脏标记
     */
    boolean getParent_meter_idDirtyFlag();

    /**
     * Ratio to Source
     */
    Integer getParent_ratio_id();

    void setParent_ratio_id(Integer parent_ratio_id);

    /**
     * 获取 [Ratio to Source]脏标记
     */
    boolean getParent_ratio_idDirtyFlag();

    /**
     * Meter
     */
    Integer getName();

    void setName(Integer name);

    /**
     * 获取 [Meter]脏标记
     */
    boolean getNameDirtyFlag();

    /**
     * 最后更新者
     */
    Integer getWrite_uid();

    void setWrite_uid(Integer write_uid);

    /**
     * 获取 [最后更新者]脏标记
     */
    boolean getWrite_uidDirtyFlag();

    /**
     * 创建人
     */
    Integer getCreate_uid();

    void setCreate_uid(Integer create_uid);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uidDirtyFlag();

    /**
     * Asset
     */
    Integer getAsset_id();

    void setAsset_id(Integer asset_id);

    /**
     * 获取 [Asset]脏标记
     */
    boolean getAsset_idDirtyFlag();

}
