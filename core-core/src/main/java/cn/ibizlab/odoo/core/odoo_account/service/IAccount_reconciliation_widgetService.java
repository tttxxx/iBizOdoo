package cn.ibizlab.odoo.core.odoo_account.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_account.domain.Account_reconciliation_widget;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_reconciliation_widgetSearchContext;


/**
 * 实体[Account_reconciliation_widget] 服务对象接口
 */
public interface IAccount_reconciliation_widgetService{

    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    boolean create(Account_reconciliation_widget et) ;
    void createBatch(List<Account_reconciliation_widget> list) ;
    boolean update(Account_reconciliation_widget et) ;
    void updateBatch(List<Account_reconciliation_widget> list) ;
    Account_reconciliation_widget get(Integer key) ;
    Page<Account_reconciliation_widget> searchDefault(Account_reconciliation_widgetSearchContext context) ;

}



