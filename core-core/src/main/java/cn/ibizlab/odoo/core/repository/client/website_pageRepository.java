package cn.ibizlab.odoo.core.repository.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import cn.ibizlab.odoo.core.repository.client.po.website_page;

/**
 * 实体[website_page] 服务对象接口
 */
public interface website_pageRepository{


    public website_page createPO() ;
        public void updateBatch(website_page website_page);

        public void update(website_page website_page);

        public void get(String id);

        public void create(website_page website_page);

        public void createBatch(website_page website_page);

        public void removeBatch(String id);

        public void remove(String id);

        public List<website_page> search();


}
