package cn.ibizlab.odoo.core.odoo_base.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_base.domain.Res_company;
import cn.ibizlab.odoo.core.odoo_base.filter.Res_companySearchContext;


/**
 * 实体[Res_company] 服务对象接口
 */
public interface IRes_companyService{

    Res_company get(Integer key) ;
    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    boolean create(Res_company et) ;
    void createBatch(List<Res_company> list) ;
    boolean update(Res_company et) ;
    void updateBatch(List<Res_company> list) ;
    Page<Res_company> searchDefault(Res_companySearchContext context) ;

}



