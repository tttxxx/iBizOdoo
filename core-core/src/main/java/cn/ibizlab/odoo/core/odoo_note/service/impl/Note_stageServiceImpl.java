package cn.ibizlab.odoo.core.odoo_note.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_note.domain.Note_stage;
import cn.ibizlab.odoo.core.odoo_note.filter.Note_stageSearchContext;
import cn.ibizlab.odoo.core.odoo_note.service.INote_stageService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;



import cn.ibizlab.odoo.core.odoo_note.client.note_stageOdooClient;
import cn.ibizlab.odoo.core.odoo_note.clientmodel.note_stageClientModel;

/**
 * 实体[便签阶段] 服务对象接口实现
 */
@Slf4j
@Service
public class Note_stageServiceImpl implements INote_stageService {

    @Autowired
    note_stageOdooClient note_stageOdooClient;


    @Override
    public boolean remove(Integer id) {
        note_stageClientModel clientModel = new note_stageClientModel();
        clientModel.setId(id);
		note_stageOdooClient.remove(clientModel);
        return true; 
    }

    public void removeBatch(Collection<Integer> idList){
    }

    @Override
    public boolean create(Note_stage et) {
        note_stageClientModel clientModel = convert2Model(et,null);
		note_stageOdooClient.create(clientModel);
        Note_stage rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Note_stage> list){
    }

    @Override
    public boolean update(Note_stage et) {
        note_stageClientModel clientModel = convert2Model(et,null);
		note_stageOdooClient.update(clientModel);
        Note_stage rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }
    
    public void updateBatch(List<Note_stage> list){
    }

    @Override
    public Note_stage get(Integer id) {
        note_stageClientModel clientModel = new note_stageClientModel();
        clientModel.setId(id);
		note_stageOdooClient.get(clientModel);
        Note_stage et = convert2Domain(clientModel,null);
        if(et==null){
            et=new Note_stage();
            et.setId(id);
        }
        else{
        }

        return  et;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Note_stage> searchDefault(Note_stageSearchContext context) {
        List<Note_stage> list = new ArrayList<Note_stage>();
        Page<note_stageClientModel> clientModelList = note_stageOdooClient.search(context);
        for(note_stageClientModel clientModel : clientModelList){
            list.add( this.convert2Domain(clientModel,null));
        }
        return new PageImpl<Note_stage>(list, context.getPageable(), clientModelList.getTotalElements());
    }

    public note_stageClientModel convert2Model(Note_stage domain , note_stageClientModel model) {
        if(domain == null)
            return model ;
        if(model==null){
            model = new note_stageClientModel();

        }
        if((Boolean) domain.getExtensionparams().get("dirtyflagenable")){
            if((Boolean) domain.getExtensionparams().get("namedirtyflag"))
                model.setName(domain.getName());
            if((Boolean) domain.getExtensionparams().get("display_namedirtyflag"))
                model.setDisplay_name(domain.getDisplayName());
            if((Boolean) domain.getExtensionparams().get("__last_updatedirtyflag"))
                model.set__last_update(domain.getLastUpdate());
            if((Boolean) domain.getExtensionparams().get("create_datedirtyflag"))
                model.setCreate_date(domain.getCreateDate());
            if((Boolean) domain.getExtensionparams().get("write_datedirtyflag"))
                model.setWrite_date(domain.getWriteDate());
            if((Boolean) domain.getExtensionparams().get("sequencedirtyflag"))
                model.setSequence(domain.getSequence());
            if((Boolean) domain.getExtensionparams().get("folddirtyflag"))
                model.setFold(domain.getFold());
            if((Boolean) domain.getExtensionparams().get("iddirtyflag"))
                model.setId(domain.getId());
            if((Boolean) domain.getExtensionparams().get("write_uid_textdirtyflag"))
                model.setWrite_uid_text(domain.getWriteUidText());
            if((Boolean) domain.getExtensionparams().get("create_uid_textdirtyflag"))
                model.setCreate_uid_text(domain.getCreateUidText());
            if((Boolean) domain.getExtensionparams().get("user_id_textdirtyflag"))
                model.setUser_id_text(domain.getUserIdText());
            if((Boolean) domain.getExtensionparams().get("user_iddirtyflag"))
                model.setUser_id(domain.getUserId());
            if((Boolean) domain.getExtensionparams().get("create_uiddirtyflag"))
                model.setCreate_uid(domain.getCreateUid());
            if((Boolean) domain.getExtensionparams().get("write_uiddirtyflag"))
                model.setWrite_uid(domain.getWriteUid());
            domain.getFocusNull().clear();
        }
        return model ;
    }

    public Note_stage convert2Domain( note_stageClientModel model ,Note_stage domain) {
        if(model == null)
            return domain ;
        if(domain == null){
            domain = new Note_stage();
        }

        if(model.getNameDirtyFlag())
            domain.setName(model.getName());
        if(model.getDisplay_nameDirtyFlag())
            domain.setDisplayName(model.getDisplay_name());
        if(model.get__last_updateDirtyFlag())
            domain.setLastUpdate(model.get__last_update());
        if(model.getCreate_dateDirtyFlag())
            domain.setCreateDate(model.getCreate_date());
        if(model.getWrite_dateDirtyFlag())
            domain.setWriteDate(model.getWrite_date());
        if(model.getSequenceDirtyFlag())
            domain.setSequence(model.getSequence());
        if(model.getFoldDirtyFlag())
            domain.setFold(model.getFold());
        if(model.getIdDirtyFlag())
            domain.setId(model.getId());
        if(model.getWrite_uid_textDirtyFlag())
            domain.setWriteUidText(model.getWrite_uid_text());
        if(model.getCreate_uid_textDirtyFlag())
            domain.setCreateUidText(model.getCreate_uid_text());
        if(model.getUser_id_textDirtyFlag())
            domain.setUserIdText(model.getUser_id_text());
        if(model.getUser_idDirtyFlag())
            domain.setUserId(model.getUser_id());
        if(model.getCreate_uidDirtyFlag())
            domain.setCreateUid(model.getCreate_uid());
        if(model.getWrite_uidDirtyFlag())
            domain.setWriteUid(model.getWrite_uid());
        return domain ;
    }

}

    



