package cn.ibizlab.odoo.core.odoo_event.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_event.domain.Event_mail_registration;
import cn.ibizlab.odoo.core.odoo_event.filter.Event_mail_registrationSearchContext;
import cn.ibizlab.odoo.core.odoo_event.service.IEvent_mail_registrationService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;



import cn.ibizlab.odoo.core.odoo_event.client.event_mail_registrationOdooClient;
import cn.ibizlab.odoo.core.odoo_event.clientmodel.event_mail_registrationClientModel;

/**
 * 实体[登记邮件调度] 服务对象接口实现
 */
@Slf4j
@Service
public class Event_mail_registrationServiceImpl implements IEvent_mail_registrationService {

    @Autowired
    event_mail_registrationOdooClient event_mail_registrationOdooClient;


    @Override
    public boolean remove(Integer id) {
        event_mail_registrationClientModel clientModel = new event_mail_registrationClientModel();
        clientModel.setId(id);
		event_mail_registrationOdooClient.remove(clientModel);
        return true; 
    }

    public void removeBatch(Collection<Integer> idList){
    }

    @Override
    public boolean update(Event_mail_registration et) {
        event_mail_registrationClientModel clientModel = convert2Model(et,null);
		event_mail_registrationOdooClient.update(clientModel);
        Event_mail_registration rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }
    
    public void updateBatch(List<Event_mail_registration> list){
    }

    @Override
    public boolean create(Event_mail_registration et) {
        event_mail_registrationClientModel clientModel = convert2Model(et,null);
		event_mail_registrationOdooClient.create(clientModel);
        Event_mail_registration rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Event_mail_registration> list){
    }

    @Override
    public Event_mail_registration get(Integer id) {
        event_mail_registrationClientModel clientModel = new event_mail_registrationClientModel();
        clientModel.setId(id);
		event_mail_registrationOdooClient.get(clientModel);
        Event_mail_registration et = convert2Domain(clientModel,null);
        if(et==null){
            et=new Event_mail_registration();
            et.setId(id);
        }
        else{
        }

        return  et;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Event_mail_registration> searchDefault(Event_mail_registrationSearchContext context) {
        List<Event_mail_registration> list = new ArrayList<Event_mail_registration>();
        Page<event_mail_registrationClientModel> clientModelList = event_mail_registrationOdooClient.search(context);
        for(event_mail_registrationClientModel clientModel : clientModelList){
            list.add( this.convert2Domain(clientModel,null));
        }
        return new PageImpl<Event_mail_registration>(list, context.getPageable(), clientModelList.getTotalElements());
    }

    public event_mail_registrationClientModel convert2Model(Event_mail_registration domain , event_mail_registrationClientModel model) {
        if(domain == null)
            return model ;
        if(model==null){
            model = new event_mail_registrationClientModel();

        }
        if((Boolean) domain.getExtensionparams().get("dirtyflagenable")){
            if((Boolean) domain.getExtensionparams().get("display_namedirtyflag"))
                model.setDisplay_name(domain.getDisplayName());
            if((Boolean) domain.getExtensionparams().get("__last_updatedirtyflag"))
                model.set__last_update(domain.getLastUpdate());
            if((Boolean) domain.getExtensionparams().get("mail_sentdirtyflag"))
                model.setMail_sent(domain.getMailSent());
            if((Boolean) domain.getExtensionparams().get("scheduled_datedirtyflag"))
                model.setScheduled_date(domain.getScheduledDate());
            if((Boolean) domain.getExtensionparams().get("create_datedirtyflag"))
                model.setCreate_date(domain.getCreateDate());
            if((Boolean) domain.getExtensionparams().get("iddirtyflag"))
                model.setId(domain.getId());
            if((Boolean) domain.getExtensionparams().get("write_datedirtyflag"))
                model.setWrite_date(domain.getWriteDate());
            if((Boolean) domain.getExtensionparams().get("write_uid_textdirtyflag"))
                model.setWrite_uid_text(domain.getWriteUidText());
            if((Boolean) domain.getExtensionparams().get("create_uid_textdirtyflag"))
                model.setCreate_uid_text(domain.getCreateUidText());
            if((Boolean) domain.getExtensionparams().get("registration_id_textdirtyflag"))
                model.setRegistration_id_text(domain.getRegistrationIdText());
            if((Boolean) domain.getExtensionparams().get("write_uiddirtyflag"))
                model.setWrite_uid(domain.getWriteUid());
            if((Boolean) domain.getExtensionparams().get("registration_iddirtyflag"))
                model.setRegistration_id(domain.getRegistrationId());
            if((Boolean) domain.getExtensionparams().get("create_uiddirtyflag"))
                model.setCreate_uid(domain.getCreateUid());
            if((Boolean) domain.getExtensionparams().get("scheduler_iddirtyflag"))
                model.setScheduler_id(domain.getSchedulerId());
            domain.getFocusNull().clear();
        }
        return model ;
    }

    public Event_mail_registration convert2Domain( event_mail_registrationClientModel model ,Event_mail_registration domain) {
        if(model == null)
            return domain ;
        if(domain == null){
            domain = new Event_mail_registration();
        }

        if(model.getDisplay_nameDirtyFlag())
            domain.setDisplayName(model.getDisplay_name());
        if(model.get__last_updateDirtyFlag())
            domain.setLastUpdate(model.get__last_update());
        if(model.getMail_sentDirtyFlag())
            domain.setMailSent(model.getMail_sent());
        if(model.getScheduled_dateDirtyFlag())
            domain.setScheduledDate(model.getScheduled_date());
        if(model.getCreate_dateDirtyFlag())
            domain.setCreateDate(model.getCreate_date());
        if(model.getIdDirtyFlag())
            domain.setId(model.getId());
        if(model.getWrite_dateDirtyFlag())
            domain.setWriteDate(model.getWrite_date());
        if(model.getWrite_uid_textDirtyFlag())
            domain.setWriteUidText(model.getWrite_uid_text());
        if(model.getCreate_uid_textDirtyFlag())
            domain.setCreateUidText(model.getCreate_uid_text());
        if(model.getRegistration_id_textDirtyFlag())
            domain.setRegistrationIdText(model.getRegistration_id_text());
        if(model.getWrite_uidDirtyFlag())
            domain.setWriteUid(model.getWrite_uid());
        if(model.getRegistration_idDirtyFlag())
            domain.setRegistrationId(model.getRegistration_id());
        if(model.getCreate_uidDirtyFlag())
            domain.setCreateUid(model.getCreate_uid());
        if(model.getScheduler_idDirtyFlag())
            domain.setSchedulerId(model.getScheduler_id());
        return domain ;
    }

}

    



