package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Uom_category;
import cn.ibizlab.odoo.core.odoo_uom.filter.Uom_categorySearchContext;

/**
 * 实体 [产品计量单位 类别] 存储对象
 */
public interface Uom_categoryRepository extends Repository<Uom_category> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Uom_category> searchDefault(Uom_categorySearchContext context);

    Uom_category convert2PO(cn.ibizlab.odoo.core.odoo_uom.domain.Uom_category domain , Uom_category po) ;

    cn.ibizlab.odoo.core.odoo_uom.domain.Uom_category convert2Domain( Uom_category po ,cn.ibizlab.odoo.core.odoo_uom.domain.Uom_category domain) ;

}
