package cn.ibizlab.odoo.core.repository.po ;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.math.BigInteger;

import cn.ibizlab.odoo.core.odoo_mail.filter.Mail_mass_mailing_campaignSearchContext;

/**
 * 实体 [群发邮件营销] 存储模型
 */
public interface Mail_mass_mailing_campaign{

    /**
     * 总计
     */
    Integer getTotal();

    void setTotal(Integer total);

    /**
     * 获取 [总计]脏标记
     */
    boolean getTotalDirtyFlag();

    /**
     * 最后修改日
     */
    Timestamp get__last_update();

    void set__last_update(Timestamp __last_update);

    /**
     * 获取 [最后修改日]脏标记
     */
    boolean get__last_updateDirtyFlag();

    /**
     * 颜色索引
     */
    Integer getColor();

    void setColor(Integer color);

    /**
     * 获取 [颜色索引]脏标记
     */
    boolean getColorDirtyFlag();

    /**
     * 群发邮件
     */
    String getMass_mailing_ids();

    void setMass_mailing_ids(String mass_mailing_ids);

    /**
     * 获取 [群发邮件]脏标记
     */
    boolean getMass_mailing_idsDirtyFlag();

    /**
     * 打开比例
     */
    Integer getOpened_ratio();

    void setOpened_ratio(Integer opened_ratio);

    /**
     * 获取 [打开比例]脏标记
     */
    boolean getOpened_ratioDirtyFlag();

    /**
     * 失败的
     */
    Integer getFailed();

    void setFailed(Integer failed);

    /**
     * 获取 [失败的]脏标记
     */
    boolean getFailedDirtyFlag();

    /**
     * 安排
     */
    Integer getScheduled();

    void setScheduled(Integer scheduled);

    /**
     * 获取 [安排]脏标记
     */
    boolean getScheduledDirtyFlag();

    /**
     * 被退回
     */
    Integer getBounced();

    void setBounced(Integer bounced);

    /**
     * 获取 [被退回]脏标记
     */
    boolean getBouncedDirtyFlag();

    /**
     * 点击数
     */
    Integer getClicks_ratio();

    void setClicks_ratio(Integer clicks_ratio);

    /**
     * 获取 [点击数]脏标记
     */
    boolean getClicks_ratioDirtyFlag();

    /**
     * 发送邮件
     */
    Integer getSent();

    void setSent(Integer sent);

    /**
     * 获取 [发送邮件]脏标记
     */
    boolean getSentDirtyFlag();

    /**
     * 已接收比例
     */
    Integer getReceived_ratio();

    void setReceived_ratio(Integer received_ratio);

    /**
     * 获取 [已接收比例]脏标记
     */
    boolean getReceived_ratioDirtyFlag();

    /**
     * 忽略
     */
    Integer getIgnored();

    void setIgnored(Integer ignored);

    /**
     * 获取 [忽略]脏标记
     */
    boolean getIgnoredDirtyFlag();

    /**
     * 支持 A/B 测试
     */
    String getUnique_ab_testing();

    void setUnique_ab_testing(String unique_ab_testing);

    /**
     * 获取 [支持 A/B 测试]脏标记
     */
    boolean getUnique_ab_testingDirtyFlag();

    /**
     * 已回复
     */
    Integer getReplied();

    void setReplied(Integer replied);

    /**
     * 获取 [已回复]脏标记
     */
    boolean getRepliedDirtyFlag();

    /**
     * 邮件
     */
    Integer getTotal_mailings();

    void setTotal_mailings(Integer total_mailings);

    /**
     * 获取 [邮件]脏标记
     */
    boolean getTotal_mailingsDirtyFlag();

    /**
     * 显示名称
     */
    String getDisplay_name();

    void setDisplay_name(String display_name);

    /**
     * 获取 [显示名称]脏标记
     */
    boolean getDisplay_nameDirtyFlag();

    /**
     * 被退回的比率
     */
    Integer getBounced_ratio();

    void setBounced_ratio(Integer bounced_ratio);

    /**
     * 获取 [被退回的比率]脏标记
     */
    boolean getBounced_ratioDirtyFlag();

    /**
     * ID
     */
    Integer getId();

    void setId(Integer id);

    /**
     * 获取 [ID]脏标记
     */
    boolean getIdDirtyFlag();

    /**
     * 标签
     */
    String getTag_ids();

    void setTag_ids(String tag_ids);

    /**
     * 获取 [标签]脏标记
     */
    boolean getTag_idsDirtyFlag();

    /**
     * 已开启
     */
    Integer getOpened();

    void setOpened(Integer opened);

    /**
     * 获取 [已开启]脏标记
     */
    boolean getOpenedDirtyFlag();

    /**
     * 回复比例
     */
    Integer getReplied_ratio();

    void setReplied_ratio(Integer replied_ratio);

    /**
     * 获取 [回复比例]脏标记
     */
    boolean getReplied_ratioDirtyFlag();

    /**
     * 最后更新时间
     */
    Timestamp getWrite_date();

    void setWrite_date(Timestamp write_date);

    /**
     * 获取 [最后更新时间]脏标记
     */
    boolean getWrite_dateDirtyFlag();

    /**
     * 已送货
     */
    Integer getDelivered();

    void setDelivered(Integer delivered);

    /**
     * 获取 [已送货]脏标记
     */
    boolean getDeliveredDirtyFlag();

    /**
     * 创建时间
     */
    Timestamp getCreate_date();

    void setCreate_date(Timestamp create_date);

    /**
     * 获取 [创建时间]脏标记
     */
    boolean getCreate_dateDirtyFlag();

    /**
     * 媒体
     */
    String getMedium_id_text();

    void setMedium_id_text(String medium_id_text);

    /**
     * 获取 [媒体]脏标记
     */
    boolean getMedium_id_textDirtyFlag();

    /**
     * 阶段
     */
    String getStage_id_text();

    void setStage_id_text(String stage_id_text);

    /**
     * 获取 [阶段]脏标记
     */
    boolean getStage_id_textDirtyFlag();

    /**
     * 负责人
     */
    String getUser_id_text();

    void setUser_id_text(String user_id_text);

    /**
     * 获取 [负责人]脏标记
     */
    boolean getUser_id_textDirtyFlag();

    /**
     * 最后更新者
     */
    String getWrite_uid_text();

    void setWrite_uid_text(String write_uid_text);

    /**
     * 获取 [最后更新者]脏标记
     */
    boolean getWrite_uid_textDirtyFlag();

    /**
     * 创建人
     */
    String getCreate_uid_text();

    void setCreate_uid_text(String create_uid_text);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uid_textDirtyFlag();

    /**
     * 来源
     */
    String getSource_id_text();

    void setSource_id_text(String source_id_text);

    /**
     * 获取 [来源]脏标记
     */
    boolean getSource_id_textDirtyFlag();

    /**
     * 营销名称
     */
    String getName();

    void setName(String name);

    /**
     * 获取 [营销名称]脏标记
     */
    boolean getNameDirtyFlag();

    /**
     * 负责人
     */
    Integer getUser_id();

    void setUser_id(Integer user_id);

    /**
     * 获取 [负责人]脏标记
     */
    boolean getUser_idDirtyFlag();

    /**
     * 创建人
     */
    Integer getCreate_uid();

    void setCreate_uid(Integer create_uid);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uidDirtyFlag();

    /**
     * 运动_ ID
     */
    Integer getCampaign_id();

    void setCampaign_id(Integer campaign_id);

    /**
     * 获取 [运动_ ID]脏标记
     */
    boolean getCampaign_idDirtyFlag();

    /**
     * 最后更新者
     */
    Integer getWrite_uid();

    void setWrite_uid(Integer write_uid);

    /**
     * 获取 [最后更新者]脏标记
     */
    boolean getWrite_uidDirtyFlag();

    /**
     * 阶段
     */
    Integer getStage_id();

    void setStage_id(Integer stage_id);

    /**
     * 获取 [阶段]脏标记
     */
    boolean getStage_idDirtyFlag();

    /**
     * 来源
     */
    Integer getSource_id();

    void setSource_id(Integer source_id);

    /**
     * 获取 [来源]脏标记
     */
    boolean getSource_idDirtyFlag();

    /**
     * 媒体
     */
    Integer getMedium_id();

    void setMedium_id(Integer medium_id);

    /**
     * 获取 [媒体]脏标记
     */
    boolean getMedium_idDirtyFlag();

}
