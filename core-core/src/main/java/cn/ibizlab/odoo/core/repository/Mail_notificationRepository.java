package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Mail_notification;
import cn.ibizlab.odoo.core.odoo_mail.filter.Mail_notificationSearchContext;

/**
 * 实体 [消息通知] 存储对象
 */
public interface Mail_notificationRepository extends Repository<Mail_notification> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Mail_notification> searchDefault(Mail_notificationSearchContext context);

    Mail_notification convert2PO(cn.ibizlab.odoo.core.odoo_mail.domain.Mail_notification domain , Mail_notification po) ;

    cn.ibizlab.odoo.core.odoo_mail.domain.Mail_notification convert2Domain( Mail_notification po ,cn.ibizlab.odoo.core.odoo_mail.domain.Mail_notification domain) ;

}
