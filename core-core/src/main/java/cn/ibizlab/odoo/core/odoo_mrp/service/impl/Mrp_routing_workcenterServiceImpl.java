package cn.ibizlab.odoo.core.odoo_mrp.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_mrp.domain.Mrp_routing_workcenter;
import cn.ibizlab.odoo.core.odoo_mrp.filter.Mrp_routing_workcenterSearchContext;
import cn.ibizlab.odoo.core.odoo_mrp.service.IMrp_routing_workcenterService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;



import cn.ibizlab.odoo.core.odoo_mrp.client.mrp_routing_workcenterOdooClient;
import cn.ibizlab.odoo.core.odoo_mrp.clientmodel.mrp_routing_workcenterClientModel;

/**
 * 实体[工作中心使用情况] 服务对象接口实现
 */
@Slf4j
@Service
public class Mrp_routing_workcenterServiceImpl implements IMrp_routing_workcenterService {

    @Autowired
    mrp_routing_workcenterOdooClient mrp_routing_workcenterOdooClient;


    @Override
    public boolean remove(Integer id) {
        mrp_routing_workcenterClientModel clientModel = new mrp_routing_workcenterClientModel();
        clientModel.setId(id);
		mrp_routing_workcenterOdooClient.remove(clientModel);
        return true; 
    }

    public void removeBatch(Collection<Integer> idList){
    }

    @Override
    public boolean create(Mrp_routing_workcenter et) {
        mrp_routing_workcenterClientModel clientModel = convert2Model(et,null);
		mrp_routing_workcenterOdooClient.create(clientModel);
        Mrp_routing_workcenter rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Mrp_routing_workcenter> list){
    }

    @Override
    public boolean update(Mrp_routing_workcenter et) {
        mrp_routing_workcenterClientModel clientModel = convert2Model(et,null);
		mrp_routing_workcenterOdooClient.update(clientModel);
        Mrp_routing_workcenter rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }
    
    public void updateBatch(List<Mrp_routing_workcenter> list){
    }

    @Override
    public Mrp_routing_workcenter get(Integer id) {
        mrp_routing_workcenterClientModel clientModel = new mrp_routing_workcenterClientModel();
        clientModel.setId(id);
		mrp_routing_workcenterOdooClient.get(clientModel);
        Mrp_routing_workcenter et = convert2Domain(clientModel,null);
        if(et==null){
            et=new Mrp_routing_workcenter();
            et.setId(id);
        }
        else{
        }

        return  et;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Mrp_routing_workcenter> searchDefault(Mrp_routing_workcenterSearchContext context) {
        List<Mrp_routing_workcenter> list = new ArrayList<Mrp_routing_workcenter>();
        Page<mrp_routing_workcenterClientModel> clientModelList = mrp_routing_workcenterOdooClient.search(context);
        for(mrp_routing_workcenterClientModel clientModel : clientModelList){
            list.add( this.convert2Domain(clientModel,null));
        }
        return new PageImpl<Mrp_routing_workcenter>(list, context.getPageable(), clientModelList.getTotalElements());
    }

    public mrp_routing_workcenterClientModel convert2Model(Mrp_routing_workcenter domain , mrp_routing_workcenterClientModel model) {
        if(domain == null)
            return model ;
        if(model==null){
            model = new mrp_routing_workcenterClientModel();

        }
        if((Boolean) domain.getExtensionparams().get("dirtyflagenable")){
            if((Boolean) domain.getExtensionparams().get("time_mode_batchdirtyflag"))
                model.setTime_mode_batch(domain.getTimeModeBatch());
            if((Boolean) domain.getExtensionparams().get("batch_sizedirtyflag"))
                model.setBatch_size(domain.getBatchSize());
            if((Boolean) domain.getExtensionparams().get("workorder_idsdirtyflag"))
                model.setWorkorder_ids(domain.getWorkorderIds());
            if((Boolean) domain.getExtensionparams().get("workorder_countdirtyflag"))
                model.setWorkorder_count(domain.getWorkorderCount());
            if((Boolean) domain.getExtensionparams().get("sequencedirtyflag"))
                model.setSequence(domain.getSequence());
            if((Boolean) domain.getExtensionparams().get("write_datedirtyflag"))
                model.setWrite_date(domain.getWriteDate());
            if((Boolean) domain.getExtensionparams().get("batchdirtyflag"))
                model.setBatch(domain.getBatch());
            if((Boolean) domain.getExtensionparams().get("time_cycle_manualdirtyflag"))
                model.setTime_cycle_manual(domain.getTimeCycleManual());
            if((Boolean) domain.getExtensionparams().get("iddirtyflag"))
                model.setId(domain.getId());
            if((Boolean) domain.getExtensionparams().get("notedirtyflag"))
                model.setNote(domain.getNote());
            if((Boolean) domain.getExtensionparams().get("time_modedirtyflag"))
                model.setTime_mode(domain.getTimeMode());
            if((Boolean) domain.getExtensionparams().get("display_namedirtyflag"))
                model.setDisplay_name(domain.getDisplayName());
            if((Boolean) domain.getExtensionparams().get("__last_updatedirtyflag"))
                model.set__last_update(domain.getLastUpdate());
            if((Boolean) domain.getExtensionparams().get("namedirtyflag"))
                model.setName(domain.getName());
            if((Boolean) domain.getExtensionparams().get("time_cycledirtyflag"))
                model.setTime_cycle(domain.getTimeCycle());
            if((Boolean) domain.getExtensionparams().get("worksheetdirtyflag"))
                model.setWorksheet(domain.getWorksheet());
            if((Boolean) domain.getExtensionparams().get("create_datedirtyflag"))
                model.setCreate_date(domain.getCreateDate());
            if((Boolean) domain.getExtensionparams().get("company_id_textdirtyflag"))
                model.setCompany_id_text(domain.getCompanyIdText());
            if((Boolean) domain.getExtensionparams().get("workcenter_id_textdirtyflag"))
                model.setWorkcenter_id_text(domain.getWorkcenterIdText());
            if((Boolean) domain.getExtensionparams().get("routing_id_textdirtyflag"))
                model.setRouting_id_text(domain.getRoutingIdText());
            if((Boolean) domain.getExtensionparams().get("write_uid_textdirtyflag"))
                model.setWrite_uid_text(domain.getWriteUidText());
            if((Boolean) domain.getExtensionparams().get("create_uid_textdirtyflag"))
                model.setCreate_uid_text(domain.getCreateUidText());
            if((Boolean) domain.getExtensionparams().get("routing_iddirtyflag"))
                model.setRouting_id(domain.getRoutingId());
            if((Boolean) domain.getExtensionparams().get("write_uiddirtyflag"))
                model.setWrite_uid(domain.getWriteUid());
            if((Boolean) domain.getExtensionparams().get("company_iddirtyflag"))
                model.setCompany_id(domain.getCompanyId());
            if((Boolean) domain.getExtensionparams().get("workcenter_iddirtyflag"))
                model.setWorkcenter_id(domain.getWorkcenterId());
            if((Boolean) domain.getExtensionparams().get("create_uiddirtyflag"))
                model.setCreate_uid(domain.getCreateUid());
            domain.getFocusNull().clear();
        }
        return model ;
    }

    public Mrp_routing_workcenter convert2Domain( mrp_routing_workcenterClientModel model ,Mrp_routing_workcenter domain) {
        if(model == null)
            return domain ;
        if(domain == null){
            domain = new Mrp_routing_workcenter();
        }

        if(model.getTime_mode_batchDirtyFlag())
            domain.setTimeModeBatch(model.getTime_mode_batch());
        if(model.getBatch_sizeDirtyFlag())
            domain.setBatchSize(model.getBatch_size());
        if(model.getWorkorder_idsDirtyFlag())
            domain.setWorkorderIds(model.getWorkorder_ids());
        if(model.getWorkorder_countDirtyFlag())
            domain.setWorkorderCount(model.getWorkorder_count());
        if(model.getSequenceDirtyFlag())
            domain.setSequence(model.getSequence());
        if(model.getWrite_dateDirtyFlag())
            domain.setWriteDate(model.getWrite_date());
        if(model.getBatchDirtyFlag())
            domain.setBatch(model.getBatch());
        if(model.getTime_cycle_manualDirtyFlag())
            domain.setTimeCycleManual(model.getTime_cycle_manual());
        if(model.getIdDirtyFlag())
            domain.setId(model.getId());
        if(model.getNoteDirtyFlag())
            domain.setNote(model.getNote());
        if(model.getTime_modeDirtyFlag())
            domain.setTimeMode(model.getTime_mode());
        if(model.getDisplay_nameDirtyFlag())
            domain.setDisplayName(model.getDisplay_name());
        if(model.get__last_updateDirtyFlag())
            domain.setLastUpdate(model.get__last_update());
        if(model.getNameDirtyFlag())
            domain.setName(model.getName());
        if(model.getTime_cycleDirtyFlag())
            domain.setTimeCycle(model.getTime_cycle());
        if(model.getWorksheetDirtyFlag())
            domain.setWorksheet(model.getWorksheet());
        if(model.getCreate_dateDirtyFlag())
            domain.setCreateDate(model.getCreate_date());
        if(model.getCompany_id_textDirtyFlag())
            domain.setCompanyIdText(model.getCompany_id_text());
        if(model.getWorkcenter_id_textDirtyFlag())
            domain.setWorkcenterIdText(model.getWorkcenter_id_text());
        if(model.getRouting_id_textDirtyFlag())
            domain.setRoutingIdText(model.getRouting_id_text());
        if(model.getWrite_uid_textDirtyFlag())
            domain.setWriteUidText(model.getWrite_uid_text());
        if(model.getCreate_uid_textDirtyFlag())
            domain.setCreateUidText(model.getCreate_uid_text());
        if(model.getRouting_idDirtyFlag())
            domain.setRoutingId(model.getRouting_id());
        if(model.getWrite_uidDirtyFlag())
            domain.setWriteUid(model.getWrite_uid());
        if(model.getCompany_idDirtyFlag())
            domain.setCompanyId(model.getCompany_id());
        if(model.getWorkcenter_idDirtyFlag())
            domain.setWorkcenterId(model.getWorkcenter_id());
        if(model.getCreate_uidDirtyFlag())
            domain.setCreateUid(model.getCreate_uid());
        return domain ;
    }

}

    



