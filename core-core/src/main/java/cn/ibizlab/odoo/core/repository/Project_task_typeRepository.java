package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Project_task_type;
import cn.ibizlab.odoo.core.odoo_project.filter.Project_task_typeSearchContext;

/**
 * 实体 [任务阶段] 存储对象
 */
public interface Project_task_typeRepository extends Repository<Project_task_type> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Project_task_type> searchDefault(Project_task_typeSearchContext context);

    Project_task_type convert2PO(cn.ibizlab.odoo.core.odoo_project.domain.Project_task_type domain , Project_task_type po) ;

    cn.ibizlab.odoo.core.odoo_project.domain.Project_task_type convert2Domain( Project_task_type po ,cn.ibizlab.odoo.core.odoo_project.domain.Project_task_type domain) ;

}
