package cn.ibizlab.odoo.core.odoo_calendar.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_calendar.domain.Calendar_alarm_manager;
import cn.ibizlab.odoo.core.odoo_calendar.filter.Calendar_alarm_managerSearchContext;
import cn.ibizlab.odoo.core.odoo_calendar.service.ICalendar_alarm_managerService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;



import cn.ibizlab.odoo.core.odoo_calendar.client.calendar_alarm_managerOdooClient;
import cn.ibizlab.odoo.core.odoo_calendar.clientmodel.calendar_alarm_managerClientModel;

/**
 * 实体[活动提醒管理] 服务对象接口实现
 */
@Slf4j
@Service
public class Calendar_alarm_managerServiceImpl implements ICalendar_alarm_managerService {

    @Autowired
    calendar_alarm_managerOdooClient calendar_alarm_managerOdooClient;


    @Override
    public Calendar_alarm_manager get(Integer id) {
        calendar_alarm_managerClientModel clientModel = new calendar_alarm_managerClientModel();
        clientModel.setId(id);
		calendar_alarm_managerOdooClient.get(clientModel);
        Calendar_alarm_manager et = convert2Domain(clientModel,null);
        if(et==null){
            et=new Calendar_alarm_manager();
            et.setId(id);
        }
        else{
        }

        return  et;
    }

    @Override
    public boolean update(Calendar_alarm_manager et) {
        calendar_alarm_managerClientModel clientModel = convert2Model(et,null);
		calendar_alarm_managerOdooClient.update(clientModel);
        Calendar_alarm_manager rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }
    
    public void updateBatch(List<Calendar_alarm_manager> list){
    }

    @Override
    public boolean remove(Integer id) {
        calendar_alarm_managerClientModel clientModel = new calendar_alarm_managerClientModel();
        clientModel.setId(id);
		calendar_alarm_managerOdooClient.remove(clientModel);
        return true; 
    }

    public void removeBatch(Collection<Integer> idList){
    }

    @Override
    public boolean create(Calendar_alarm_manager et) {
        calendar_alarm_managerClientModel clientModel = convert2Model(et,null);
		calendar_alarm_managerOdooClient.create(clientModel);
        Calendar_alarm_manager rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Calendar_alarm_manager> list){
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Calendar_alarm_manager> searchDefault(Calendar_alarm_managerSearchContext context) {
        List<Calendar_alarm_manager> list = new ArrayList<Calendar_alarm_manager>();
        Page<calendar_alarm_managerClientModel> clientModelList = calendar_alarm_managerOdooClient.search(context);
        for(calendar_alarm_managerClientModel clientModel : clientModelList){
            list.add( this.convert2Domain(clientModel,null));
        }
        return new PageImpl<Calendar_alarm_manager>(list, context.getPageable(), clientModelList.getTotalElements());
    }

    public calendar_alarm_managerClientModel convert2Model(Calendar_alarm_manager domain , calendar_alarm_managerClientModel model) {
        if(domain == null)
            return model ;
        if(model==null){
            model = new calendar_alarm_managerClientModel();

        }
        if((Boolean) domain.getExtensionparams().get("dirtyflagenable")){
            if((Boolean) domain.getExtensionparams().get("__last_updatedirtyflag"))
                model.set__last_update(domain.getLastUpdate());
            if((Boolean) domain.getExtensionparams().get("display_namedirtyflag"))
                model.setDisplay_name(domain.getDisplayName());
            if((Boolean) domain.getExtensionparams().get("iddirtyflag"))
                model.setId(domain.getId());
            domain.getFocusNull().clear();
        }
        return model ;
    }

    public Calendar_alarm_manager convert2Domain( calendar_alarm_managerClientModel model ,Calendar_alarm_manager domain) {
        if(model == null)
            return domain ;
        if(domain == null){
            domain = new Calendar_alarm_manager();
        }

        if(model.get__last_updateDirtyFlag())
            domain.setLastUpdate(model.get__last_update());
        if(model.getDisplay_nameDirtyFlag())
            domain.setDisplayName(model.getDisplay_name());
        if(model.getIdDirtyFlag())
            domain.setId(model.getId());
        return domain ;
    }

}

    



