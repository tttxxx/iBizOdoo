package cn.ibizlab.odoo.core.repository.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import cn.ibizlab.odoo.core.repository.client.po.mro_pm_replan;

/**
 * 实体[mro_pm_replan] 服务对象接口
 */
public interface mro_pm_replanRepository{


    public mro_pm_replan createPO() ;
        public void remove(String id);

        public void update(mro_pm_replan mro_pm_replan);

        public void get(String id);

        public void updateBatch(mro_pm_replan mro_pm_replan);

        public List<mro_pm_replan> search();

        public void createBatch(mro_pm_replan mro_pm_replan);

        public void removeBatch(String id);

        public void create(mro_pm_replan mro_pm_replan);


}
