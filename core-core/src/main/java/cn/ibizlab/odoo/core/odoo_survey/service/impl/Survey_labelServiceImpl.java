package cn.ibizlab.odoo.core.odoo_survey.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_survey.domain.Survey_label;
import cn.ibizlab.odoo.core.odoo_survey.filter.Survey_labelSearchContext;
import cn.ibizlab.odoo.core.odoo_survey.service.ISurvey_labelService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;



import cn.ibizlab.odoo.core.odoo_survey.client.survey_labelOdooClient;
import cn.ibizlab.odoo.core.odoo_survey.clientmodel.survey_labelClientModel;

/**
 * 实体[调查标签] 服务对象接口实现
 */
@Slf4j
@Service
public class Survey_labelServiceImpl implements ISurvey_labelService {

    @Autowired
    survey_labelOdooClient survey_labelOdooClient;


    @Override
    public boolean create(Survey_label et) {
        survey_labelClientModel clientModel = convert2Model(et,null);
		survey_labelOdooClient.create(clientModel);
        Survey_label rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Survey_label> list){
    }

    @Override
    public boolean update(Survey_label et) {
        survey_labelClientModel clientModel = convert2Model(et,null);
		survey_labelOdooClient.update(clientModel);
        Survey_label rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }
    
    public void updateBatch(List<Survey_label> list){
    }

    @Override
    public Survey_label get(Integer id) {
        survey_labelClientModel clientModel = new survey_labelClientModel();
        clientModel.setId(id);
		survey_labelOdooClient.get(clientModel);
        Survey_label et = convert2Domain(clientModel,null);
        if(et==null){
            et=new Survey_label();
            et.setId(id);
        }
        else{
        }

        return  et;
    }

    @Override
    public boolean remove(Integer id) {
        survey_labelClientModel clientModel = new survey_labelClientModel();
        clientModel.setId(id);
		survey_labelOdooClient.remove(clientModel);
        return true; 
    }

    public void removeBatch(Collection<Integer> idList){
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Survey_label> searchDefault(Survey_labelSearchContext context) {
        List<Survey_label> list = new ArrayList<Survey_label>();
        Page<survey_labelClientModel> clientModelList = survey_labelOdooClient.search(context);
        for(survey_labelClientModel clientModel : clientModelList){
            list.add( this.convert2Domain(clientModel,null));
        }
        return new PageImpl<Survey_label>(list, context.getPageable(), clientModelList.getTotalElements());
    }

    public survey_labelClientModel convert2Model(Survey_label domain , survey_labelClientModel model) {
        if(domain == null)
            return model ;
        if(model==null){
            model = new survey_labelClientModel();

        }
        if((Boolean) domain.getExtensionparams().get("dirtyflagenable")){
            if((Boolean) domain.getExtensionparams().get("display_namedirtyflag"))
                model.setDisplay_name(domain.getDisplayName());
            if((Boolean) domain.getExtensionparams().get("__last_updatedirtyflag"))
                model.set__last_update(domain.getLastUpdate());
            if((Boolean) domain.getExtensionparams().get("write_datedirtyflag"))
                model.setWrite_date(domain.getWriteDate());
            if((Boolean) domain.getExtensionparams().get("sequencedirtyflag"))
                model.setSequence(domain.getSequence());
            if((Boolean) domain.getExtensionparams().get("iddirtyflag"))
                model.setId(domain.getId());
            if((Boolean) domain.getExtensionparams().get("quizz_markdirtyflag"))
                model.setQuizz_mark(domain.getQuizzMark());
            if((Boolean) domain.getExtensionparams().get("valuedirtyflag"))
                model.setValue(domain.getValue());
            if((Boolean) domain.getExtensionparams().get("create_datedirtyflag"))
                model.setCreate_date(domain.getCreateDate());
            if((Boolean) domain.getExtensionparams().get("write_uid_textdirtyflag"))
                model.setWrite_uid_text(domain.getWriteUidText());
            if((Boolean) domain.getExtensionparams().get("create_uid_textdirtyflag"))
                model.setCreate_uid_text(domain.getCreateUidText());
            if((Boolean) domain.getExtensionparams().get("create_uiddirtyflag"))
                model.setCreate_uid(domain.getCreateUid());
            if((Boolean) domain.getExtensionparams().get("question_id_2dirtyflag"))
                model.setQuestion_id_2(domain.getQuestionId2());
            if((Boolean) domain.getExtensionparams().get("question_iddirtyflag"))
                model.setQuestion_id(domain.getQuestionId());
            if((Boolean) domain.getExtensionparams().get("write_uiddirtyflag"))
                model.setWrite_uid(domain.getWriteUid());
            domain.getFocusNull().clear();
        }
        return model ;
    }

    public Survey_label convert2Domain( survey_labelClientModel model ,Survey_label domain) {
        if(model == null)
            return domain ;
        if(domain == null){
            domain = new Survey_label();
        }

        if(model.getDisplay_nameDirtyFlag())
            domain.setDisplayName(model.getDisplay_name());
        if(model.get__last_updateDirtyFlag())
            domain.setLastUpdate(model.get__last_update());
        if(model.getWrite_dateDirtyFlag())
            domain.setWriteDate(model.getWrite_date());
        if(model.getSequenceDirtyFlag())
            domain.setSequence(model.getSequence());
        if(model.getIdDirtyFlag())
            domain.setId(model.getId());
        if(model.getQuizz_markDirtyFlag())
            domain.setQuizzMark(model.getQuizz_mark());
        if(model.getValueDirtyFlag())
            domain.setValue(model.getValue());
        if(model.getCreate_dateDirtyFlag())
            domain.setCreateDate(model.getCreate_date());
        if(model.getWrite_uid_textDirtyFlag())
            domain.setWriteUidText(model.getWrite_uid_text());
        if(model.getCreate_uid_textDirtyFlag())
            domain.setCreateUidText(model.getCreate_uid_text());
        if(model.getCreate_uidDirtyFlag())
            domain.setCreateUid(model.getCreate_uid());
        if(model.getQuestion_id_2DirtyFlag())
            domain.setQuestionId2(model.getQuestion_id_2());
        if(model.getQuestion_idDirtyFlag())
            domain.setQuestionId(model.getQuestion_id());
        if(model.getWrite_uidDirtyFlag())
            domain.setWriteUid(model.getWrite_uid());
        return domain ;
    }

}

    



