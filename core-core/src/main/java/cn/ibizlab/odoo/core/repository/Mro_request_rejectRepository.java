package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Mro_request_reject;
import cn.ibizlab.odoo.core.odoo_mro.filter.Mro_request_rejectSearchContext;

/**
 * 实体 [Reject Request] 存储对象
 */
public interface Mro_request_rejectRepository extends Repository<Mro_request_reject> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Mro_request_reject> searchDefault(Mro_request_rejectSearchContext context);

    Mro_request_reject convert2PO(cn.ibizlab.odoo.core.odoo_mro.domain.Mro_request_reject domain , Mro_request_reject po) ;

    cn.ibizlab.odoo.core.odoo_mro.domain.Mro_request_reject convert2Domain( Mro_request_reject po ,cn.ibizlab.odoo.core.odoo_mro.domain.Mro_request_reject domain) ;

}
