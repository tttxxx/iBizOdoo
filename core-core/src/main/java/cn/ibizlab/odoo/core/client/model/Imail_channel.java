package cn.ibizlab.odoo.core.client.model;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigDecimal;
import java.math.BigInteger;

/**
 * Client 实体 [mail_channel] 对象
 */
public interface Imail_channel {

    /**
     * 获取 [安全联系人别名]
     */
    public void setAlias_contact(String alias_contact);
    
    /**
     * 设置 [安全联系人别名]
     */
    public String getAlias_contact();

    /**
     * 获取 [安全联系人别名]脏标记
     */
    public boolean getAlias_contactDirtyFlag();
    /**
     * 获取 [默认值]
     */
    public void setAlias_defaults(String alias_defaults);
    
    /**
     * 设置 [默认值]
     */
    public String getAlias_defaults();

    /**
     * 获取 [默认值]脏标记
     */
    public boolean getAlias_defaultsDirtyFlag();
    /**
     * 获取 [网域别名]
     */
    public void setAlias_domain(String alias_domain);
    
    /**
     * 设置 [网域别名]
     */
    public String getAlias_domain();

    /**
     * 获取 [网域别名]脏标记
     */
    public boolean getAlias_domainDirtyFlag();
    /**
     * 获取 [记录线索ID]
     */
    public void setAlias_force_thread_id(Integer alias_force_thread_id);
    
    /**
     * 设置 [记录线索ID]
     */
    public Integer getAlias_force_thread_id();

    /**
     * 获取 [记录线索ID]脏标记
     */
    public boolean getAlias_force_thread_idDirtyFlag();
    /**
     * 获取 [别名]
     */
    public void setAlias_id(Integer alias_id);
    
    /**
     * 设置 [别名]
     */
    public Integer getAlias_id();

    /**
     * 获取 [别名]脏标记
     */
    public boolean getAlias_idDirtyFlag();
    /**
     * 获取 [模型别名]
     */
    public void setAlias_model_id(Integer alias_model_id);
    
    /**
     * 设置 [模型别名]
     */
    public Integer getAlias_model_id();

    /**
     * 获取 [模型别名]脏标记
     */
    public boolean getAlias_model_idDirtyFlag();
    /**
     * 获取 [别名]
     */
    public void setAlias_name(String alias_name);
    
    /**
     * 设置 [别名]
     */
    public String getAlias_name();

    /**
     * 获取 [别名]脏标记
     */
    public boolean getAlias_nameDirtyFlag();
    /**
     * 获取 [上级模型]
     */
    public void setAlias_parent_model_id(Integer alias_parent_model_id);
    
    /**
     * 设置 [上级模型]
     */
    public Integer getAlias_parent_model_id();

    /**
     * 获取 [上级模型]脏标记
     */
    public boolean getAlias_parent_model_idDirtyFlag();
    /**
     * 获取 [上级记录ID]
     */
    public void setAlias_parent_thread_id(Integer alias_parent_thread_id);
    
    /**
     * 设置 [上级记录ID]
     */
    public Integer getAlias_parent_thread_id();

    /**
     * 获取 [上级记录ID]脏标记
     */
    public boolean getAlias_parent_thread_idDirtyFlag();
    /**
     * 获取 [所有者]
     */
    public void setAlias_user_id(Integer alias_user_id);
    
    /**
     * 设置 [所有者]
     */
    public Integer getAlias_user_id();

    /**
     * 获取 [所有者]脏标记
     */
    public boolean getAlias_user_idDirtyFlag();
    /**
     * 获取 [匿名用户姓名]
     */
    public void setAnonymous_name(String anonymous_name);
    
    /**
     * 设置 [匿名用户姓名]
     */
    public String getAnonymous_name();

    /**
     * 获取 [匿名用户姓名]脏标记
     */
    public boolean getAnonymous_nameDirtyFlag();
    /**
     * 获取 [最近一次查阅]
     */
    public void setChannel_last_seen_partner_ids(String channel_last_seen_partner_ids);
    
    /**
     * 设置 [最近一次查阅]
     */
    public String getChannel_last_seen_partner_ids();

    /**
     * 获取 [最近一次查阅]脏标记
     */
    public boolean getChannel_last_seen_partner_idsDirtyFlag();
    /**
     * 获取 [渠道消息]
     */
    public void setChannel_message_ids(String channel_message_ids);
    
    /**
     * 设置 [渠道消息]
     */
    public String getChannel_message_ids();

    /**
     * 获取 [渠道消息]脏标记
     */
    public boolean getChannel_message_idsDirtyFlag();
    /**
     * 获取 [监听器]
     */
    public void setChannel_partner_ids(String channel_partner_ids);
    
    /**
     * 设置 [监听器]
     */
    public String getChannel_partner_ids();

    /**
     * 获取 [监听器]脏标记
     */
    public boolean getChannel_partner_idsDirtyFlag();
    /**
     * 获取 [渠道类型]
     */
    public void setChannel_type(String channel_type);
    
    /**
     * 设置 [渠道类型]
     */
    public String getChannel_type();

    /**
     * 获取 [渠道类型]脏标记
     */
    public boolean getChannel_typeDirtyFlag();
    /**
     * 获取 [创建时间]
     */
    public void setCreate_date(Timestamp create_date);
    
    /**
     * 设置 [创建时间]
     */
    public Timestamp getCreate_date();

    /**
     * 获取 [创建时间]脏标记
     */
    public boolean getCreate_dateDirtyFlag();
    /**
     * 获取 [创建人]
     */
    public void setCreate_uid(Integer create_uid);
    
    /**
     * 设置 [创建人]
     */
    public Integer getCreate_uid();

    /**
     * 获取 [创建人]脏标记
     */
    public boolean getCreate_uidDirtyFlag();
    /**
     * 获取 [创建人]
     */
    public void setCreate_uid_text(String create_uid_text);
    
    /**
     * 设置 [创建人]
     */
    public String getCreate_uid_text();

    /**
     * 获取 [创建人]脏标记
     */
    public boolean getCreate_uid_textDirtyFlag();
    /**
     * 获取 [说明]
     */
    public void setDescription(String description);
    
    /**
     * 设置 [说明]
     */
    public String getDescription();

    /**
     * 获取 [说明]脏标记
     */
    public boolean getDescriptionDirtyFlag();
    /**
     * 获取 [显示名称]
     */
    public void setDisplay_name(String display_name);
    
    /**
     * 设置 [显示名称]
     */
    public String getDisplay_name();

    /**
     * 获取 [显示名称]脏标记
     */
    public boolean getDisplay_nameDirtyFlag();
    /**
     * 获取 [以邮件形式发送]
     */
    public void setEmail_send(String email_send);
    
    /**
     * 设置 [以邮件形式发送]
     */
    public String getEmail_send();

    /**
     * 获取 [以邮件形式发送]脏标记
     */
    public boolean getEmail_sendDirtyFlag();
    /**
     * 获取 [自动订阅]
     */
    public void setGroup_ids(String group_ids);
    
    /**
     * 设置 [自动订阅]
     */
    public String getGroup_ids();

    /**
     * 获取 [自动订阅]脏标记
     */
    public boolean getGroup_idsDirtyFlag();
    /**
     * 获取 [经授权的群组]
     */
    public void setGroup_public_id(Integer group_public_id);
    
    /**
     * 设置 [经授权的群组]
     */
    public Integer getGroup_public_id();

    /**
     * 获取 [经授权的群组]脏标记
     */
    public boolean getGroup_public_idDirtyFlag();
    /**
     * 获取 [经授权的群组]
     */
    public void setGroup_public_id_text(String group_public_id_text);
    
    /**
     * 设置 [经授权的群组]
     */
    public String getGroup_public_id_text();

    /**
     * 获取 [经授权的群组]脏标记
     */
    public boolean getGroup_public_id_textDirtyFlag();
    /**
     * 获取 [隐私]
     */
    public void setIbizpublic(String ibizpublic);
    
    /**
     * 设置 [隐私]
     */
    public String getIbizpublic();

    /**
     * 获取 [隐私]脏标记
     */
    public boolean getIbizpublicDirtyFlag();
    /**
     * 获取 [ID]
     */
    public void setId(Integer id);
    
    /**
     * 设置 [ID]
     */
    public Integer getId();

    /**
     * 获取 [ID]脏标记
     */
    public boolean getIdDirtyFlag();
    /**
     * 获取 [照片]
     */
    public void setImage(byte[] image);
    
    /**
     * 设置 [照片]
     */
    public byte[] getImage();

    /**
     * 获取 [照片]脏标记
     */
    public boolean getImageDirtyFlag();
    /**
     * 获取 [中等尺寸照片]
     */
    public void setImage_medium(byte[] image_medium);
    
    /**
     * 设置 [中等尺寸照片]
     */
    public byte[] getImage_medium();

    /**
     * 获取 [中等尺寸照片]脏标记
     */
    public boolean getImage_mediumDirtyFlag();
    /**
     * 获取 [小尺寸照片]
     */
    public void setImage_small(byte[] image_small);
    
    /**
     * 设置 [小尺寸照片]
     */
    public byte[] getImage_small();

    /**
     * 获取 [小尺寸照片]脏标记
     */
    public boolean getImage_smallDirtyFlag();
    /**
     * 获取 [是聊天]
     */
    public void setIs_chat(String is_chat);
    
    /**
     * 设置 [是聊天]
     */
    public String getIs_chat();

    /**
     * 获取 [是聊天]脏标记
     */
    public boolean getIs_chatDirtyFlag();
    /**
     * 获取 [成员]
     */
    public void setIs_member(String is_member);
    
    /**
     * 设置 [成员]
     */
    public String getIs_member();

    /**
     * 获取 [成员]脏标记
     */
    public boolean getIs_memberDirtyFlag();
    /**
     * 获取 [管理员]
     */
    public void setIs_moderator(String is_moderator);
    
    /**
     * 设置 [管理员]
     */
    public String getIs_moderator();

    /**
     * 获取 [管理员]脏标记
     */
    public boolean getIs_moderatorDirtyFlag();
    /**
     * 获取 [已订阅]
     */
    public void setIs_subscribed(String is_subscribed);
    
    /**
     * 设置 [已订阅]
     */
    public String getIs_subscribed();

    /**
     * 获取 [已订阅]脏标记
     */
    public boolean getIs_subscribedDirtyFlag();
    /**
     * 获取 [渠道]
     */
    public void setLivechat_channel_id(Integer livechat_channel_id);
    
    /**
     * 设置 [渠道]
     */
    public Integer getLivechat_channel_id();

    /**
     * 获取 [渠道]脏标记
     */
    public boolean getLivechat_channel_idDirtyFlag();
    /**
     * 获取 [渠道]
     */
    public void setLivechat_channel_id_text(String livechat_channel_id_text);
    
    /**
     * 设置 [渠道]
     */
    public String getLivechat_channel_id_text();

    /**
     * 获取 [渠道]脏标记
     */
    public boolean getLivechat_channel_id_textDirtyFlag();
    /**
     * 获取 [附件数量]
     */
    public void setMessage_attachment_count(Integer message_attachment_count);
    
    /**
     * 设置 [附件数量]
     */
    public Integer getMessage_attachment_count();

    /**
     * 获取 [附件数量]脏标记
     */
    public boolean getMessage_attachment_countDirtyFlag();
    /**
     * 获取 [关注者(渠道)]
     */
    public void setMessage_channel_ids(String message_channel_ids);
    
    /**
     * 设置 [关注者(渠道)]
     */
    public String getMessage_channel_ids();

    /**
     * 获取 [关注者(渠道)]脏标记
     */
    public boolean getMessage_channel_idsDirtyFlag();
    /**
     * 获取 [关注者]
     */
    public void setMessage_follower_ids(String message_follower_ids);
    
    /**
     * 设置 [关注者]
     */
    public String getMessage_follower_ids();

    /**
     * 获取 [关注者]脏标记
     */
    public boolean getMessage_follower_idsDirtyFlag();
    /**
     * 获取 [消息递送错误]
     */
    public void setMessage_has_error(String message_has_error);
    
    /**
     * 设置 [消息递送错误]
     */
    public String getMessage_has_error();

    /**
     * 获取 [消息递送错误]脏标记
     */
    public boolean getMessage_has_errorDirtyFlag();
    /**
     * 获取 [错误个数]
     */
    public void setMessage_has_error_counter(Integer message_has_error_counter);
    
    /**
     * 设置 [错误个数]
     */
    public Integer getMessage_has_error_counter();

    /**
     * 获取 [错误个数]脏标记
     */
    public boolean getMessage_has_error_counterDirtyFlag();
    /**
     * 获取 [消息]
     */
    public void setMessage_ids(String message_ids);
    
    /**
     * 设置 [消息]
     */
    public String getMessage_ids();

    /**
     * 获取 [消息]脏标记
     */
    public boolean getMessage_idsDirtyFlag();
    /**
     * 获取 [关注者]
     */
    public void setMessage_is_follower(String message_is_follower);
    
    /**
     * 设置 [关注者]
     */
    public String getMessage_is_follower();

    /**
     * 获取 [关注者]脏标记
     */
    public boolean getMessage_is_followerDirtyFlag();
    /**
     * 获取 [附件]
     */
    public void setMessage_main_attachment_id(Integer message_main_attachment_id);
    
    /**
     * 设置 [附件]
     */
    public Integer getMessage_main_attachment_id();

    /**
     * 获取 [附件]脏标记
     */
    public boolean getMessage_main_attachment_idDirtyFlag();
    /**
     * 获取 [前置操作]
     */
    public void setMessage_needaction(String message_needaction);
    
    /**
     * 设置 [前置操作]
     */
    public String getMessage_needaction();

    /**
     * 获取 [前置操作]脏标记
     */
    public boolean getMessage_needactionDirtyFlag();
    /**
     * 获取 [操作次数]
     */
    public void setMessage_needaction_counter(Integer message_needaction_counter);
    
    /**
     * 设置 [操作次数]
     */
    public Integer getMessage_needaction_counter();

    /**
     * 获取 [操作次数]脏标记
     */
    public boolean getMessage_needaction_counterDirtyFlag();
    /**
     * 获取 [关注者(业务伙伴)]
     */
    public void setMessage_partner_ids(String message_partner_ids);
    
    /**
     * 设置 [关注者(业务伙伴)]
     */
    public String getMessage_partner_ids();

    /**
     * 获取 [关注者(业务伙伴)]脏标记
     */
    public boolean getMessage_partner_idsDirtyFlag();
    /**
     * 获取 [未读消息]
     */
    public void setMessage_unread(String message_unread);
    
    /**
     * 设置 [未读消息]
     */
    public String getMessage_unread();

    /**
     * 获取 [未读消息]脏标记
     */
    public boolean getMessage_unreadDirtyFlag();
    /**
     * 获取 [未读消息计数器]
     */
    public void setMessage_unread_counter(Integer message_unread_counter);
    
    /**
     * 设置 [未读消息计数器]
     */
    public Integer getMessage_unread_counter();

    /**
     * 获取 [未读消息计数器]脏标记
     */
    public boolean getMessage_unread_counterDirtyFlag();
    /**
     * 获取 [管理频道]
     */
    public void setModeration(String moderation);
    
    /**
     * 设置 [管理频道]
     */
    public String getModeration();

    /**
     * 获取 [管理频道]脏标记
     */
    public boolean getModerationDirtyFlag();
    /**
     * 获取 [管理EMail账户]
     */
    public void setModeration_count(Integer moderation_count);
    
    /**
     * 设置 [管理EMail账户]
     */
    public Integer getModeration_count();

    /**
     * 获取 [管理EMail账户]脏标记
     */
    public boolean getModeration_countDirtyFlag();
    /**
     * 获取 [向新用户发送订阅指南]
     */
    public void setModeration_guidelines(String moderation_guidelines);
    
    /**
     * 设置 [向新用户发送订阅指南]
     */
    public String getModeration_guidelines();

    /**
     * 获取 [向新用户发送订阅指南]脏标记
     */
    public boolean getModeration_guidelinesDirtyFlag();
    /**
     * 获取 [方针]
     */
    public void setModeration_guidelines_msg(String moderation_guidelines_msg);
    
    /**
     * 设置 [方针]
     */
    public String getModeration_guidelines_msg();

    /**
     * 获取 [方针]脏标记
     */
    public boolean getModeration_guidelines_msgDirtyFlag();
    /**
     * 获取 [管理EMail]
     */
    public void setModeration_ids(String moderation_ids);
    
    /**
     * 设置 [管理EMail]
     */
    public String getModeration_ids();

    /**
     * 获取 [管理EMail]脏标记
     */
    public boolean getModeration_idsDirtyFlag();
    /**
     * 获取 [自动通知]
     */
    public void setModeration_notify(String moderation_notify);
    
    /**
     * 设置 [自动通知]
     */
    public String getModeration_notify();

    /**
     * 获取 [自动通知]脏标记
     */
    public boolean getModeration_notifyDirtyFlag();
    /**
     * 获取 [通知消息]
     */
    public void setModeration_notify_msg(String moderation_notify_msg);
    
    /**
     * 设置 [通知消息]
     */
    public String getModeration_notify_msg();

    /**
     * 获取 [通知消息]脏标记
     */
    public boolean getModeration_notify_msgDirtyFlag();
    /**
     * 获取 [管理员]
     */
    public void setModerator_ids(String moderator_ids);
    
    /**
     * 设置 [管理员]
     */
    public String getModerator_ids();

    /**
     * 获取 [管理员]脏标记
     */
    public boolean getModerator_idsDirtyFlag();
    /**
     * 获取 [名称]
     */
    public void setName(String name);
    
    /**
     * 设置 [名称]
     */
    public String getName();

    /**
     * 获取 [名称]脏标记
     */
    public boolean getNameDirtyFlag();
    /**
     * 获取 [评级数]
     */
    public void setRating_count(Integer rating_count);
    
    /**
     * 设置 [评级数]
     */
    public Integer getRating_count();

    /**
     * 获取 [评级数]脏标记
     */
    public boolean getRating_countDirtyFlag();
    /**
     * 获取 [评级]
     */
    public void setRating_ids(String rating_ids);
    
    /**
     * 设置 [评级]
     */
    public String getRating_ids();

    /**
     * 获取 [评级]脏标记
     */
    public boolean getRating_idsDirtyFlag();
    /**
     * 获取 [最新反馈评级]
     */
    public void setRating_last_feedback(String rating_last_feedback);
    
    /**
     * 设置 [最新反馈评级]
     */
    public String getRating_last_feedback();

    /**
     * 获取 [最新反馈评级]脏标记
     */
    public boolean getRating_last_feedbackDirtyFlag();
    /**
     * 获取 [最新图像评级]
     */
    public void setRating_last_image(byte[] rating_last_image);
    
    /**
     * 设置 [最新图像评级]
     */
    public byte[] getRating_last_image();

    /**
     * 获取 [最新图像评级]脏标记
     */
    public boolean getRating_last_imageDirtyFlag();
    /**
     * 获取 [最新值评级]
     */
    public void setRating_last_value(Double rating_last_value);
    
    /**
     * 设置 [最新值评级]
     */
    public Double getRating_last_value();

    /**
     * 获取 [最新值评级]脏标记
     */
    public boolean getRating_last_valueDirtyFlag();
    /**
     * 获取 [人力资源部门]
     */
    public void setSubscription_department_ids(String subscription_department_ids);
    
    /**
     * 设置 [人力资源部门]
     */
    public String getSubscription_department_ids();

    /**
     * 获取 [人力资源部门]脏标记
     */
    public boolean getSubscription_department_idsDirtyFlag();
    /**
     * 获取 [UUID]
     */
    public void setUuid(String uuid);
    
    /**
     * 设置 [UUID]
     */
    public String getUuid();

    /**
     * 获取 [UUID]脏标记
     */
    public boolean getUuidDirtyFlag();
    /**
     * 获取 [网站消息]
     */
    public void setWebsite_message_ids(String website_message_ids);
    
    /**
     * 设置 [网站消息]
     */
    public String getWebsite_message_ids();

    /**
     * 获取 [网站消息]脏标记
     */
    public boolean getWebsite_message_idsDirtyFlag();
    /**
     * 获取 [最后更新时间]
     */
    public void setWrite_date(Timestamp write_date);
    
    /**
     * 设置 [最后更新时间]
     */
    public Timestamp getWrite_date();

    /**
     * 获取 [最后更新时间]脏标记
     */
    public boolean getWrite_dateDirtyFlag();
    /**
     * 获取 [最后更新者]
     */
    public void setWrite_uid(Integer write_uid);
    
    /**
     * 设置 [最后更新者]
     */
    public Integer getWrite_uid();

    /**
     * 获取 [最后更新者]脏标记
     */
    public boolean getWrite_uidDirtyFlag();
    /**
     * 获取 [最后更新者]
     */
    public void setWrite_uid_text(String write_uid_text);
    
    /**
     * 设置 [最后更新者]
     */
    public String getWrite_uid_text();

    /**
     * 获取 [最后更新者]脏标记
     */
    public boolean getWrite_uid_textDirtyFlag();
    /**
     * 获取 [最后修改日]
     */
    public void set__last_update(Timestamp __last_update);
    
    /**
     * 设置 [最后修改日]
     */
    public Timestamp get__last_update();

    /**
     * 获取 [最后修改日]脏标记
     */
    public boolean get__last_updateDirtyFlag();


    /**
     *  转换为Map
     */
    public Map<String, Object> toMap() throws Exception ;

    /**
     *  从Map构建
     */
   public void fromMap(Map<String, Object> map) throws Exception;
}
