package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Mro_pm_meter_ratio;
import cn.ibizlab.odoo.core.odoo_mro.filter.Mro_pm_meter_ratioSearchContext;

/**
 * 实体 [Rules for Meter to Meter Ratio] 存储对象
 */
public interface Mro_pm_meter_ratioRepository extends Repository<Mro_pm_meter_ratio> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Mro_pm_meter_ratio> searchDefault(Mro_pm_meter_ratioSearchContext context);

    Mro_pm_meter_ratio convert2PO(cn.ibizlab.odoo.core.odoo_mro.domain.Mro_pm_meter_ratio domain , Mro_pm_meter_ratio po) ;

    cn.ibizlab.odoo.core.odoo_mro.domain.Mro_pm_meter_ratio convert2Domain( Mro_pm_meter_ratio po ,cn.ibizlab.odoo.core.odoo_mro.domain.Mro_pm_meter_ratio domain) ;

}
