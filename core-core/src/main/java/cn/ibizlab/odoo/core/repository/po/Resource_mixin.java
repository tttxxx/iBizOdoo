package cn.ibizlab.odoo.core.repository.po ;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.math.BigInteger;

import cn.ibizlab.odoo.core.odoo_resource.filter.Resource_mixinSearchContext;

/**
 * 实体 [资源装饰] 存储模型
 */
public interface Resource_mixin{

    /**
     * 最后修改日
     */
    Timestamp get__last_update();

    void set__last_update(Timestamp __last_update);

    /**
     * 获取 [最后修改日]脏标记
     */
    boolean get__last_updateDirtyFlag();

    /**
     * 显示名称
     */
    String getDisplay_name();

    void setDisplay_name(String display_name);

    /**
     * 获取 [显示名称]脏标记
     */
    boolean getDisplay_nameDirtyFlag();

    /**
     * ID
     */
    Integer getId();

    void setId(Integer id);

    /**
     * 获取 [ID]脏标记
     */
    boolean getIdDirtyFlag();

    /**
     * 时区
     */
    String getTz();

    void setTz(String tz);

    /**
     * 获取 [时区]脏标记
     */
    boolean getTzDirtyFlag();

    /**
     * 资源
     */
    String getResource_id_text();

    void setResource_id_text(String resource_id_text);

    /**
     * 获取 [资源]脏标记
     */
    boolean getResource_id_textDirtyFlag();

    /**
     * 工作时间
     */
    String getResource_calendar_id_text();

    void setResource_calendar_id_text(String resource_calendar_id_text);

    /**
     * 获取 [工作时间]脏标记
     */
    boolean getResource_calendar_id_textDirtyFlag();

    /**
     * 公司
     */
    String getCompany_id_text();

    void setCompany_id_text(String company_id_text);

    /**
     * 获取 [公司]脏标记
     */
    boolean getCompany_id_textDirtyFlag();

    /**
     * 工作时间
     */
    Integer getResource_calendar_id();

    void setResource_calendar_id(Integer resource_calendar_id);

    /**
     * 获取 [工作时间]脏标记
     */
    boolean getResource_calendar_idDirtyFlag();

    /**
     * 公司
     */
    Integer getCompany_id();

    void setCompany_id(Integer company_id);

    /**
     * 获取 [公司]脏标记
     */
    boolean getCompany_idDirtyFlag();

    /**
     * 资源
     */
    Integer getResource_id();

    void setResource_id(Integer resource_id);

    /**
     * 获取 [资源]脏标记
     */
    boolean getResource_idDirtyFlag();

}
