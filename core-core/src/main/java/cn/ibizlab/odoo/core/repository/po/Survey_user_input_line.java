package cn.ibizlab.odoo.core.repository.po ;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.math.BigInteger;

import cn.ibizlab.odoo.core.odoo_survey.filter.Survey_user_input_lineSearchContext;

/**
 * 实体 [调查用户输入明细] 存储模型
 */
public interface Survey_user_input_line{

    /**
     * 回复日期
     */
    Timestamp getValue_date();

    void setValue_date(Timestamp value_date);

    /**
     * 获取 [回复日期]脏标记
     */
    boolean getValue_dateDirtyFlag();

    /**
     * 自由文本答案
     */
    String getValue_free_text();

    void setValue_free_text(String value_free_text);

    /**
     * 获取 [自由文本答案]脏标记
     */
    boolean getValue_free_textDirtyFlag();

    /**
     * ID
     */
    Integer getId();

    void setId(Integer id);

    /**
     * 获取 [ID]脏标记
     */
    boolean getIdDirtyFlag();

    /**
     * 创建日期
     */
    Timestamp getDate_create();

    void setDate_create(Timestamp date_create);

    /**
     * 获取 [创建日期]脏标记
     */
    boolean getDate_createDirtyFlag();

    /**
     * 忽略
     */
    String getSkipped();

    void setSkipped(String skipped);

    /**
     * 获取 [忽略]脏标记
     */
    boolean getSkippedDirtyFlag();

    /**
     * 最后修改日
     */
    Timestamp get__last_update();

    void set__last_update(Timestamp __last_update);

    /**
     * 获取 [最后修改日]脏标记
     */
    boolean get__last_updateDirtyFlag();

    /**
     * 这个选项分配的分数
     */
    Double getQuizz_mark();

    void setQuizz_mark(Double quizz_mark);

    /**
     * 获取 [这个选项分配的分数]脏标记
     */
    boolean getQuizz_markDirtyFlag();

    /**
     * 创建时间
     */
    Timestamp getCreate_date();

    void setCreate_date(Timestamp create_date);

    /**
     * 获取 [创建时间]脏标记
     */
    boolean getCreate_dateDirtyFlag();

    /**
     * 显示名称
     */
    String getDisplay_name();

    void setDisplay_name(String display_name);

    /**
     * 获取 [显示名称]脏标记
     */
    boolean getDisplay_nameDirtyFlag();

    /**
     * 回复类型
     */
    String getAnswer_type();

    void setAnswer_type(String answer_type);

    /**
     * 获取 [回复类型]脏标记
     */
    boolean getAnswer_typeDirtyFlag();

    /**
     * 文本答案
     */
    String getValue_text();

    void setValue_text(String value_text);

    /**
     * 获取 [文本答案]脏标记
     */
    boolean getValue_textDirtyFlag();

    /**
     * 数字答案
     */
    Double getValue_number();

    void setValue_number(Double value_number);

    /**
     * 获取 [数字答案]脏标记
     */
    boolean getValue_numberDirtyFlag();

    /**
     * 最后更新时间
     */
    Timestamp getWrite_date();

    void setWrite_date(Timestamp write_date);

    /**
     * 获取 [最后更新时间]脏标记
     */
    boolean getWrite_dateDirtyFlag();

    /**
     * 最后更新者
     */
    String getWrite_uid_text();

    void setWrite_uid_text(String write_uid_text);

    /**
     * 获取 [最后更新者]脏标记
     */
    boolean getWrite_uid_textDirtyFlag();

    /**
     * 创建人
     */
    String getCreate_uid_text();

    void setCreate_uid_text(String create_uid_text);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uid_textDirtyFlag();

    /**
     * 页
     */
    Integer getPage_id();

    void setPage_id(Integer page_id);

    /**
     * 获取 [页]脏标记
     */
    boolean getPage_idDirtyFlag();

    /**
     * 用户输入
     */
    Integer getUser_input_id();

    void setUser_input_id(Integer user_input_id);

    /**
     * 获取 [用户输入]脏标记
     */
    boolean getUser_input_idDirtyFlag();

    /**
     * 建议答案
     */
    Integer getValue_suggested();

    void setValue_suggested(Integer value_suggested);

    /**
     * 获取 [建议答案]脏标记
     */
    boolean getValue_suggestedDirtyFlag();

    /**
     * 创建人
     */
    Integer getCreate_uid();

    void setCreate_uid(Integer create_uid);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uidDirtyFlag();

    /**
     * 答案行
     */
    Integer getValue_suggested_row();

    void setValue_suggested_row(Integer value_suggested_row);

    /**
     * 获取 [答案行]脏标记
     */
    boolean getValue_suggested_rowDirtyFlag();

    /**
     * 最后更新者
     */
    Integer getWrite_uid();

    void setWrite_uid(Integer write_uid);

    /**
     * 获取 [最后更新者]脏标记
     */
    boolean getWrite_uidDirtyFlag();

    /**
     * 疑问
     */
    Integer getQuestion_id();

    void setQuestion_id(Integer question_id);

    /**
     * 获取 [疑问]脏标记
     */
    boolean getQuestion_idDirtyFlag();

    /**
     * 问卷
     */
    Integer getSurvey_id();

    void setSurvey_id(Integer survey_id);

    /**
     * 获取 [问卷]脏标记
     */
    boolean getSurvey_idDirtyFlag();

}
