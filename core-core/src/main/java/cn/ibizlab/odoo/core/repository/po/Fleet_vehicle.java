package cn.ibizlab.odoo.core.repository.po ;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.math.BigInteger;

import cn.ibizlab.odoo.core.odoo_fleet.filter.Fleet_vehicleSearchContext;

/**
 * 实体 [车辆] 存储模型
 */
public interface Fleet_vehicle{

    /**
     * 燃油类型
     */
    String getFuel_type();

    void setFuel_type(String fuel_type);

    /**
     * 获取 [燃油类型]脏标记
     */
    boolean getFuel_typeDirtyFlag();

    /**
     * 标签
     */
    String getTag_ids();

    void setTag_ids(String tag_ids);

    /**
     * 获取 [标签]脏标记
     */
    boolean getTag_idsDirtyFlag();

    /**
     * 活动
     */
    String getActivity_ids();

    void setActivity_ids(String activity_ids);

    /**
     * 获取 [活动]脏标记
     */
    boolean getActivity_idsDirtyFlag();

    /**
     * 未读消息
     */
    String getMessage_unread();

    void setMessage_unread(String message_unread);

    /**
     * 获取 [未读消息]脏标记
     */
    boolean getMessage_unreadDirtyFlag();

    /**
     * 网站消息
     */
    String getWebsite_message_ids();

    void setWebsite_message_ids(String website_message_ids);

    /**
     * 获取 [网站消息]脏标记
     */
    boolean getWebsite_message_idsDirtyFlag();

    /**
     * 动力
     */
    Integer getPower();

    void setPower(Integer power);

    /**
     * 获取 [动力]脏标记
     */
    boolean getPowerDirtyFlag();

    /**
     * 需要激活
     */
    String getMessage_needaction();

    void setMessage_needaction(String message_needaction);

    /**
     * 获取 [需要激活]脏标记
     */
    boolean getMessage_needactionDirtyFlag();

    /**
     * 残余价值
     */
    Double getResidual_value();

    void setResidual_value(Double residual_value);

    /**
     * 获取 [残余价值]脏标记
     */
    boolean getResidual_valueDirtyFlag();

    /**
     * 最新里程表
     */
    Double getOdometer();

    void setOdometer(Double odometer);

    /**
     * 获取 [最新里程表]脏标记
     */
    boolean getOdometerDirtyFlag();

    /**
     * 关注者(渠道)
     */
    String getMessage_channel_ids();

    void setMessage_channel_ids(String message_channel_ids);

    /**
     * 获取 [关注者(渠道)]脏标记
     */
    boolean getMessage_channel_idsDirtyFlag();

    /**
     * 加油记录统计
     */
    Integer getFuel_logs_count();

    void setFuel_logs_count(Integer fuel_logs_count);

    /**
     * 获取 [加油记录统计]脏标记
     */
    boolean getFuel_logs_countDirtyFlag();

    /**
     * 关注者
     */
    String getMessage_follower_ids();

    void setMessage_follower_ids(String message_follower_ids);

    /**
     * 获取 [关注者]脏标记
     */
    boolean getMessage_follower_idsDirtyFlag();

    /**
     * 型号年份
     */
    String getModel_year();

    void setModel_year(String model_year);

    /**
     * 获取 [型号年份]脏标记
     */
    boolean getModel_yearDirtyFlag();

    /**
     * 目录值（包括增值税）
     */
    Double getCar_value();

    void setCar_value(Double car_value);

    /**
     * 获取 [目录值（包括增值税）]脏标记
     */
    boolean getCar_valueDirtyFlag();

    /**
     * 车辆牌照
     */
    String getLicense_plate();

    void setLicense_plate(String license_plate);

    /**
     * 获取 [车辆牌照]脏标记
     */
    boolean getLicense_plateDirtyFlag();

    /**
     * 有逾期合同
     */
    String getContract_renewal_overdue();

    void setContract_renewal_overdue(String contract_renewal_overdue);

    /**
     * 获取 [有逾期合同]脏标记
     */
    boolean getContract_renewal_overdueDirtyFlag();

    /**
     * 未读消息计数器
     */
    Integer getMessage_unread_counter();

    void setMessage_unread_counter(Integer message_unread_counter);

    /**
     * 获取 [未读消息计数器]脏标记
     */
    boolean getMessage_unread_counterDirtyFlag();

    /**
     * 注册日期
     */
    Timestamp getAcquisition_date();

    void setAcquisition_date(Timestamp acquisition_date);

    /**
     * 获取 [注册日期]脏标记
     */
    boolean getAcquisition_dateDirtyFlag();

    /**
     * 下一活动截止日期
     */
    Timestamp getActivity_date_deadline();

    void setActivity_date_deadline(Timestamp activity_date_deadline);

    /**
     * 获取 [下一活动截止日期]脏标记
     */
    boolean getActivity_date_deadlineDirtyFlag();

    /**
     * 最后更新时间
     */
    Timestamp getWrite_date();

    void setWrite_date(Timestamp write_date);

    /**
     * 获取 [最后更新时间]脏标记
     */
    boolean getWrite_dateDirtyFlag();

    /**
     * 责任用户
     */
    Integer getActivity_user_id();

    void setActivity_user_id(Integer activity_user_id);

    /**
     * 获取 [责任用户]脏标记
     */
    boolean getActivity_user_idDirtyFlag();

    /**
     * 下一活动类型
     */
    Integer getActivity_type_id();

    void setActivity_type_id(Integer activity_type_id);

    /**
     * 获取 [下一活动类型]脏标记
     */
    boolean getActivity_type_idDirtyFlag();

    /**
     * 车船税
     */
    Double getHorsepower_tax();

    void setHorsepower_tax(Double horsepower_tax);

    /**
     * 获取 [车船税]脏标记
     */
    boolean getHorsepower_taxDirtyFlag();

    /**
     * 地点
     */
    String getLocation();

    void setLocation(String location);

    /**
     * 获取 [地点]脏标记
     */
    boolean getLocationDirtyFlag();

    /**
     * 错误数
     */
    Integer getMessage_has_error_counter();

    void setMessage_has_error_counter(Integer message_has_error_counter);

    /**
     * 获取 [错误数]脏标记
     */
    boolean getMessage_has_error_counterDirtyFlag();

    /**
     * 消息
     */
    String getMessage_ids();

    void setMessage_ids(String message_ids);

    /**
     * 获取 [消息]脏标记
     */
    boolean getMessage_idsDirtyFlag();

    /**
     * 关注者
     */
    String getMessage_is_follower();

    void setMessage_is_follower(String message_is_follower);

    /**
     * 获取 [关注者]脏标记
     */
    boolean getMessage_is_followerDirtyFlag();

    /**
     * 有效
     */
    String getActive();

    void setActive(String active);

    /**
     * 获取 [有效]脏标记
     */
    boolean getActiveDirtyFlag();

    /**
     * 马力
     */
    Integer getHorsepower();

    void setHorsepower(Integer horsepower);

    /**
     * 获取 [马力]脏标记
     */
    boolean getHorsepowerDirtyFlag();

    /**
     * 车架号
     */
    String getVin_sn();

    void setVin_sn(String vin_sn);

    /**
     * 获取 [车架号]脏标记
     */
    boolean getVin_snDirtyFlag();

    /**
     * 显示名称
     */
    String getDisplay_name();

    void setDisplay_name(String display_name);

    /**
     * 获取 [显示名称]脏标记
     */
    boolean getDisplay_nameDirtyFlag();

    /**
     * 消息递送错误
     */
    String getMessage_has_error();

    void setMessage_has_error(String message_has_error);

    /**
     * 获取 [消息递送错误]脏标记
     */
    boolean getMessage_has_errorDirtyFlag();

    /**
     * 里程表
     */
    Integer getOdometer_count();

    void setOdometer_count(Integer odometer_count);

    /**
     * 获取 [里程表]脏标记
     */
    boolean getOdometer_countDirtyFlag();

    /**
     * 关注者(业务伙伴)
     */
    String getMessage_partner_ids();

    void setMessage_partner_ids(String message_partner_ids);

    /**
     * 获取 [关注者(业务伙伴)]脏标记
     */
    boolean getMessage_partner_idsDirtyFlag();

    /**
     * 行动数量
     */
    Integer getMessage_needaction_counter();

    void setMessage_needaction_counter(Integer message_needaction_counter);

    /**
     * 获取 [行动数量]脏标记
     */
    boolean getMessage_needaction_counterDirtyFlag();

    /**
     * 需马上续签合同的名称
     */
    String getContract_renewal_name();

    void setContract_renewal_name(String contract_renewal_name);

    /**
     * 获取 [需马上续签合同的名称]脏标记
     */
    boolean getContract_renewal_nameDirtyFlag();

    /**
     * 创建时间
     */
    Timestamp getCreate_date();

    void setCreate_date(Timestamp create_date);

    /**
     * 获取 [创建时间]脏标记
     */
    boolean getCreate_dateDirtyFlag();

    /**
     * 下一活动摘要
     */
    String getActivity_summary();

    void setActivity_summary(String activity_summary);

    /**
     * 获取 [下一活动摘要]脏标记
     */
    boolean getActivity_summaryDirtyFlag();

    /**
     * 座位数
     */
    Integer getSeats();

    void setSeats(Integer seats);

    /**
     * 获取 [座位数]脏标记
     */
    boolean getSeatsDirtyFlag();

    /**
     * 燃油记录
     */
    String getLog_fuel();

    void setLog_fuel(String log_fuel);

    /**
     * 获取 [燃油记录]脏标记
     */
    boolean getLog_fuelDirtyFlag();

    /**
     * 费用
     */
    Integer getCost_count();

    void setCost_count(Integer cost_count);

    /**
     * 获取 [费用]脏标记
     */
    boolean getCost_countDirtyFlag();

    /**
     * 指派记录
     */
    String getLog_drivers();

    void setLog_drivers(String log_drivers);

    /**
     * 获取 [指派记录]脏标记
     */
    boolean getLog_driversDirtyFlag();

    /**
     * 最后修改日
     */
    Timestamp get__last_update();

    void set__last_update(Timestamp __last_update);

    /**
     * 获取 [最后修改日]脏标记
     */
    boolean get__last_updateDirtyFlag();

    /**
     * 有合同待续签
     */
    String getContract_renewal_due_soon();

    void setContract_renewal_due_soon(String contract_renewal_due_soon);

    /**
     * 获取 [有合同待续签]脏标记
     */
    boolean getContract_renewal_due_soonDirtyFlag();

    /**
     * 里程表单位
     */
    String getOdometer_unit();

    void setOdometer_unit(String odometer_unit);

    /**
     * 获取 [里程表单位]脏标记
     */
    boolean getOdometer_unitDirtyFlag();

    /**
     * 二氧化碳排放量
     */
    Double getCo2();

    void setCo2(Double co2);

    /**
     * 获取 [二氧化碳排放量]脏标记
     */
    boolean getCo2DirtyFlag();

    /**
     * 附件
     */
    Integer getMessage_main_attachment_id();

    void setMessage_main_attachment_id(Integer message_main_attachment_id);

    /**
     * 获取 [附件]脏标记
     */
    boolean getMessage_main_attachment_idDirtyFlag();

    /**
     * 合同
     */
    String getLog_contracts();

    void setLog_contracts(String log_contracts);

    /**
     * 获取 [合同]脏标记
     */
    boolean getLog_contractsDirtyFlag();

    /**
     * 首次合同日期
     */
    Timestamp getFirst_contract_date();

    void setFirst_contract_date(Timestamp first_contract_date);

    /**
     * 获取 [首次合同日期]脏标记
     */
    boolean getFirst_contract_dateDirtyFlag();

    /**
     * 活动状态
     */
    String getActivity_state();

    void setActivity_state(String activity_state);

    /**
     * 获取 [活动状态]脏标记
     */
    boolean getActivity_stateDirtyFlag();

    /**
     * 颜色
     */
    String getColor();

    void setColor(String color);

    /**
     * 获取 [颜色]脏标记
     */
    boolean getColorDirtyFlag();

    /**
     * 车门数量
     */
    Integer getDoors();

    void setDoors(Integer doors);

    /**
     * 获取 [车门数量]脏标记
     */
    boolean getDoorsDirtyFlag();

    /**
     * ID
     */
    Integer getId();

    void setId(Integer id);

    /**
     * 获取 [ID]脏标记
     */
    boolean getIdDirtyFlag();

    /**
     * 附件数量
     */
    Integer getMessage_attachment_count();

    void setMessage_attachment_count(Integer message_attachment_count);

    /**
     * 获取 [附件数量]脏标记
     */
    boolean getMessage_attachment_countDirtyFlag();

    /**
     * 名称
     */
    String getName();

    void setName(String name);

    /**
     * 获取 [名称]脏标记
     */
    boolean getNameDirtyFlag();

    /**
     * 服务记录
     */
    String getLog_services();

    void setLog_services(String log_services);

    /**
     * 获取 [服务记录]脏标记
     */
    boolean getLog_servicesDirtyFlag();

    /**
     * 合同统计
     */
    Integer getContract_count();

    void setContract_count(Integer contract_count);

    /**
     * 获取 [合同统计]脏标记
     */
    boolean getContract_countDirtyFlag();

    /**
     * 变速器
     */
    String getTransmission();

    void setTransmission(String transmission);

    /**
     * 获取 [变速器]脏标记
     */
    boolean getTransmissionDirtyFlag();

    /**
     * 服务
     */
    Integer getService_count();

    void setService_count(Integer service_count);

    /**
     * 获取 [服务]脏标记
     */
    boolean getService_countDirtyFlag();

    /**
     * 截止或者逾期减一的合同总计
     */
    String getContract_renewal_total();

    void setContract_renewal_total(String contract_renewal_total);

    /**
     * 获取 [截止或者逾期减一的合同总计]脏标记
     */
    boolean getContract_renewal_totalDirtyFlag();

    /**
     * 公司
     */
    String getCompany_id_text();

    void setCompany_id_text(String company_id_text);

    /**
     * 获取 [公司]脏标记
     */
    boolean getCompany_id_textDirtyFlag();

    /**
     * 最后更新人
     */
    String getWrite_uid_text();

    void setWrite_uid_text(String write_uid_text);

    /**
     * 获取 [最后更新人]脏标记
     */
    boolean getWrite_uid_textDirtyFlag();

    /**
     * 驾驶员
     */
    String getDriver_id_text();

    void setDriver_id_text(String driver_id_text);

    /**
     * 获取 [驾驶员]脏标记
     */
    boolean getDriver_id_textDirtyFlag();

    /**
     * 创建人
     */
    String getCreate_uid_text();

    void setCreate_uid_text(String create_uid_text);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uid_textDirtyFlag();

    /**
     * 车辆标志图片(普通大小)
     */
    byte[] getImage_medium();

    void setImage_medium(byte[] image_medium);

    /**
     * 获取 [车辆标志图片(普通大小)]脏标记
     */
    boolean getImage_mediumDirtyFlag();

    /**
     * 徽标
     */
    byte[] getImage();

    void setImage(byte[] image);

    /**
     * 获取 [徽标]脏标记
     */
    boolean getImageDirtyFlag();

    /**
     * 型号
     */
    String getModel_id_text();

    void setModel_id_text(String model_id_text);

    /**
     * 获取 [型号]脏标记
     */
    boolean getModel_id_textDirtyFlag();

    /**
     * 车辆标志图片(小图片)
     */
    byte[] getImage_small();

    void setImage_small(byte[] image_small);

    /**
     * 获取 [车辆标志图片(小图片)]脏标记
     */
    boolean getImage_smallDirtyFlag();

    /**
     * 品牌
     */
    String getBrand_id_text();

    void setBrand_id_text(String brand_id_text);

    /**
     * 获取 [品牌]脏标记
     */
    boolean getBrand_id_textDirtyFlag();

    /**
     * 状态
     */
    String getState_id_text();

    void setState_id_text(String state_id_text);

    /**
     * 获取 [状态]脏标记
     */
    boolean getState_id_textDirtyFlag();

    /**
     * 创建人
     */
    Integer getCreate_uid();

    void setCreate_uid(Integer create_uid);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uidDirtyFlag();

    /**
     * 型号
     */
    Integer getModel_id();

    void setModel_id(Integer model_id);

    /**
     * 获取 [型号]脏标记
     */
    boolean getModel_idDirtyFlag();

    /**
     * 品牌
     */
    Integer getBrand_id();

    void setBrand_id(Integer brand_id);

    /**
     * 获取 [品牌]脏标记
     */
    boolean getBrand_idDirtyFlag();

    /**
     * 公司
     */
    Integer getCompany_id();

    void setCompany_id(Integer company_id);

    /**
     * 获取 [公司]脏标记
     */
    boolean getCompany_idDirtyFlag();

    /**
     * 状态
     */
    Integer getState_id();

    void setState_id(Integer state_id);

    /**
     * 获取 [状态]脏标记
     */
    boolean getState_idDirtyFlag();

    /**
     * 驾驶员
     */
    Integer getDriver_id();

    void setDriver_id(Integer driver_id);

    /**
     * 获取 [驾驶员]脏标记
     */
    boolean getDriver_idDirtyFlag();

    /**
     * 最后更新人
     */
    Integer getWrite_uid();

    void setWrite_uid(Integer write_uid);

    /**
     * 获取 [最后更新人]脏标记
     */
    boolean getWrite_uidDirtyFlag();

}
