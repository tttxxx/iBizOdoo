package cn.ibizlab.odoo.core.odoo_account.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_account.domain.Account_reconciliation_widget;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_reconciliation_widgetSearchContext;
import cn.ibizlab.odoo.core.odoo_account.service.IAccount_reconciliation_widgetService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;



import cn.ibizlab.odoo.core.odoo_account.client.account_reconciliation_widgetOdooClient;
import cn.ibizlab.odoo.core.odoo_account.clientmodel.account_reconciliation_widgetClientModel;

/**
 * 实体[会计核销挂账] 服务对象接口实现
 */
@Slf4j
@Service
public class Account_reconciliation_widgetServiceImpl implements IAccount_reconciliation_widgetService {

    @Autowired
    account_reconciliation_widgetOdooClient account_reconciliation_widgetOdooClient;


    @Override
    public boolean remove(Integer id) {
        account_reconciliation_widgetClientModel clientModel = new account_reconciliation_widgetClientModel();
        clientModel.setId(id);
		account_reconciliation_widgetOdooClient.remove(clientModel);
        return true; 
    }

    public void removeBatch(Collection<Integer> idList){
    }

    @Override
    public boolean create(Account_reconciliation_widget et) {
        account_reconciliation_widgetClientModel clientModel = convert2Model(et,null);
		account_reconciliation_widgetOdooClient.create(clientModel);
        Account_reconciliation_widget rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Account_reconciliation_widget> list){
    }

    @Override
    public boolean update(Account_reconciliation_widget et) {
        account_reconciliation_widgetClientModel clientModel = convert2Model(et,null);
		account_reconciliation_widgetOdooClient.update(clientModel);
        Account_reconciliation_widget rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }
    
    public void updateBatch(List<Account_reconciliation_widget> list){
    }

    @Override
    public Account_reconciliation_widget get(Integer id) {
        account_reconciliation_widgetClientModel clientModel = new account_reconciliation_widgetClientModel();
        clientModel.setId(id);
		account_reconciliation_widgetOdooClient.get(clientModel);
        Account_reconciliation_widget et = convert2Domain(clientModel,null);
        if(et==null){
            et=new Account_reconciliation_widget();
            et.setId(id);
        }
        else{
        }

        return  et;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Account_reconciliation_widget> searchDefault(Account_reconciliation_widgetSearchContext context) {
        List<Account_reconciliation_widget> list = new ArrayList<Account_reconciliation_widget>();
        Page<account_reconciliation_widgetClientModel> clientModelList = account_reconciliation_widgetOdooClient.search(context);
        for(account_reconciliation_widgetClientModel clientModel : clientModelList){
            list.add( this.convert2Domain(clientModel,null));
        }
        return new PageImpl<Account_reconciliation_widget>(list, context.getPageable(), clientModelList.getTotalElements());
    }

    public account_reconciliation_widgetClientModel convert2Model(Account_reconciliation_widget domain , account_reconciliation_widgetClientModel model) {
        if(domain == null)
            return model ;
        if(model==null){
            model = new account_reconciliation_widgetClientModel();

        }
        if((Boolean) domain.getExtensionparams().get("dirtyflagenable")){
            if((Boolean) domain.getExtensionparams().get("display_namedirtyflag"))
                model.setDisplay_name(domain.getDisplayName());
            if((Boolean) domain.getExtensionparams().get("__last_updatedirtyflag"))
                model.set__last_update(domain.getLastUpdate());
            if((Boolean) domain.getExtensionparams().get("iddirtyflag"))
                model.setId(domain.getId());
            domain.getFocusNull().clear();
        }
        return model ;
    }

    public Account_reconciliation_widget convert2Domain( account_reconciliation_widgetClientModel model ,Account_reconciliation_widget domain) {
        if(model == null)
            return domain ;
        if(domain == null){
            domain = new Account_reconciliation_widget();
        }

        if(model.getDisplay_nameDirtyFlag())
            domain.setDisplayName(model.getDisplay_name());
        if(model.get__last_updateDirtyFlag())
            domain.setLastUpdate(model.get__last_update());
        if(model.getIdDirtyFlag())
            domain.setId(model.getId());
        return domain ;
    }

}

    



