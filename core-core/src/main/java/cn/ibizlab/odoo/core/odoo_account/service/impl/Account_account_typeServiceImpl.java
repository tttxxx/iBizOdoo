package cn.ibizlab.odoo.core.odoo_account.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_account.domain.Account_account_type;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_account_typeSearchContext;
import cn.ibizlab.odoo.core.odoo_account.service.IAccount_account_typeService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;



import cn.ibizlab.odoo.core.odoo_account.client.account_account_typeOdooClient;
import cn.ibizlab.odoo.core.odoo_account.clientmodel.account_account_typeClientModel;

/**
 * 实体[科目类型] 服务对象接口实现
 */
@Slf4j
@Service
public class Account_account_typeServiceImpl implements IAccount_account_typeService {

    @Autowired
    account_account_typeOdooClient account_account_typeOdooClient;


    @Override
    public Account_account_type get(Integer id) {
        account_account_typeClientModel clientModel = new account_account_typeClientModel();
        clientModel.setId(id);
		account_account_typeOdooClient.get(clientModel);
        Account_account_type et = convert2Domain(clientModel,null);
        if(et==null){
            et=new Account_account_type();
            et.setId(id);
        }
        else{
        }

        return  et;
    }

    @Override
    public boolean update(Account_account_type et) {
        account_account_typeClientModel clientModel = convert2Model(et,null);
		account_account_typeOdooClient.update(clientModel);
        Account_account_type rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }
    
    public void updateBatch(List<Account_account_type> list){
    }

    @Override
    public boolean create(Account_account_type et) {
        account_account_typeClientModel clientModel = convert2Model(et,null);
		account_account_typeOdooClient.create(clientModel);
        Account_account_type rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Account_account_type> list){
    }

    @Override
    public boolean remove(Integer id) {
        account_account_typeClientModel clientModel = new account_account_typeClientModel();
        clientModel.setId(id);
		account_account_typeOdooClient.remove(clientModel);
        return true; 
    }

    public void removeBatch(Collection<Integer> idList){
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Account_account_type> searchDefault(Account_account_typeSearchContext context) {
        List<Account_account_type> list = new ArrayList<Account_account_type>();
        Page<account_account_typeClientModel> clientModelList = account_account_typeOdooClient.search(context);
        for(account_account_typeClientModel clientModel : clientModelList){
            list.add( this.convert2Domain(clientModel,null));
        }
        return new PageImpl<Account_account_type>(list, context.getPageable(), clientModelList.getTotalElements());
    }

    public account_account_typeClientModel convert2Model(Account_account_type domain , account_account_typeClientModel model) {
        if(domain == null)
            return model ;
        if(model==null){
            model = new account_account_typeClientModel();

        }
        if((Boolean) domain.getExtensionparams().get("dirtyflagenable")){
            if((Boolean) domain.getExtensionparams().get("display_namedirtyflag"))
                model.setDisplay_name(domain.getDisplayName());
            if((Boolean) domain.getExtensionparams().get("typedirtyflag"))
                model.setType(domain.getType());
            if((Boolean) domain.getExtensionparams().get("__last_updatedirtyflag"))
                model.set__last_update(domain.getLastUpdate());
            if((Boolean) domain.getExtensionparams().get("iddirtyflag"))
                model.setId(domain.getId());
            if((Boolean) domain.getExtensionparams().get("write_datedirtyflag"))
                model.setWrite_date(domain.getWriteDate());
            if((Boolean) domain.getExtensionparams().get("create_datedirtyflag"))
                model.setCreate_date(domain.getCreateDate());
            if((Boolean) domain.getExtensionparams().get("notedirtyflag"))
                model.setNote(domain.getNote());
            if((Boolean) domain.getExtensionparams().get("include_initial_balancedirtyflag"))
                model.setInclude_initial_balance(domain.getIncludeInitialBalance());
            if((Boolean) domain.getExtensionparams().get("namedirtyflag"))
                model.setName(domain.getName());
            if((Boolean) domain.getExtensionparams().get("internal_groupdirtyflag"))
                model.setInternal_group(domain.getInternalGroup());
            if((Boolean) domain.getExtensionparams().get("write_uid_textdirtyflag"))
                model.setWrite_uid_text(domain.getWriteUidText());
            if((Boolean) domain.getExtensionparams().get("create_uid_textdirtyflag"))
                model.setCreate_uid_text(domain.getCreateUidText());
            if((Boolean) domain.getExtensionparams().get("create_uiddirtyflag"))
                model.setCreate_uid(domain.getCreateUid());
            if((Boolean) domain.getExtensionparams().get("write_uiddirtyflag"))
                model.setWrite_uid(domain.getWriteUid());
            domain.getFocusNull().clear();
        }
        return model ;
    }

    public Account_account_type convert2Domain( account_account_typeClientModel model ,Account_account_type domain) {
        if(model == null)
            return domain ;
        if(domain == null){
            domain = new Account_account_type();
        }

        if(model.getDisplay_nameDirtyFlag())
            domain.setDisplayName(model.getDisplay_name());
        if(model.getTypeDirtyFlag())
            domain.setType(model.getType());
        if(model.get__last_updateDirtyFlag())
            domain.setLastUpdate(model.get__last_update());
        if(model.getIdDirtyFlag())
            domain.setId(model.getId());
        if(model.getWrite_dateDirtyFlag())
            domain.setWriteDate(model.getWrite_date());
        if(model.getCreate_dateDirtyFlag())
            domain.setCreateDate(model.getCreate_date());
        if(model.getNoteDirtyFlag())
            domain.setNote(model.getNote());
        if(model.getInclude_initial_balanceDirtyFlag())
            domain.setIncludeInitialBalance(model.getInclude_initial_balance());
        if(model.getNameDirtyFlag())
            domain.setName(model.getName());
        if(model.getInternal_groupDirtyFlag())
            domain.setInternalGroup(model.getInternal_group());
        if(model.getWrite_uid_textDirtyFlag())
            domain.setWriteUidText(model.getWrite_uid_text());
        if(model.getCreate_uid_textDirtyFlag())
            domain.setCreateUidText(model.getCreate_uid_text());
        if(model.getCreate_uidDirtyFlag())
            domain.setCreateUid(model.getCreate_uid());
        if(model.getWrite_uidDirtyFlag())
            domain.setWriteUid(model.getWrite_uid());
        return domain ;
    }

}

    



