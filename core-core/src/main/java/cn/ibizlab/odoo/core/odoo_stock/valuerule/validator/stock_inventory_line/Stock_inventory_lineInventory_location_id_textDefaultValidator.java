package cn.ibizlab.odoo.core.odoo_stock.valuerule.validator.stock_inventory_line;

import lombok.extern.slf4j.Slf4j;
import cn.ibizlab.odoo.util.helper.SpringContextHolder;
import cn.ibizlab.odoo.util.valuerule.SysValueRule;
import cn.ibizlab.odoo.util.valuerule.StringLengthValueRule;
import cn.ibizlab.odoo.util.SearchFieldFilter;
import cn.ibizlab.odoo.util.enums.SearchFieldType;
import cn.ibizlab.odoo.core.odoo_stock.valuerule.anno.stock_inventory_line.Stock_inventory_lineInventory_location_id_textDefault;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import org.springframework.util.CollectionUtils;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.sql.Timestamp;
import java.util.Arrays;
import java.util.List;

/**
 * 实体值规则注解解析类
 * 实体：Stock_inventory_line
 * 属性：Inventory_location_id_text
 * 值规则：Default
 * 值规则信息：内容长度必须小于等于[200]
 */
@Slf4j
@Component("Stock_inventory_lineInventory_location_id_textDefaultValidator")
public class Stock_inventory_lineInventory_location_id_textDefaultValidator implements ConstraintValidator<Stock_inventory_lineInventory_location_id_textDefault, String>,Validator {
    private static final String MESSAGE = "内容长度必须小于等于[200]";

    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {
        boolean isValid = doValidate(value);
        if(!isValid) {
            context.disableDefaultConstraintViolation();
            context.buildConstraintViolationWithTemplate(MESSAGE)
                    .addConstraintViolation();
        }
        return doValidate(value);
    }

    @Override
    public boolean supports(Class<?> aClass) {
        return true;
    }

    @Override
    public void validate(Object o, Errors errors) {
        if(supports(o.getClass())){
            if (!doValidate((String) o)){
                errors.reject(MESSAGE);
            }
        }
    }

    public boolean doValidate(String value) {
        boolean isValid = true;

        {   //组条件：默认组
            //组合条件操作：AND
            boolean groupValid = true;
            {   //字符长度（STRINGLENGTH）:默认字符串长度
                Integer minlength = null;
                Integer maxlength = 200;

                boolean isInRange = StringLengthValueRule.isValid(value, minlength, maxlength, false, true);
                groupValid = groupValid && isInRange;
            }

            isValid = isValid && groupValid;
        }

        return isValid;
    }
}

