package cn.ibizlab.odoo.core.odoo_repair.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_repair.domain.Repair_line;
import cn.ibizlab.odoo.core.odoo_repair.filter.Repair_lineSearchContext;
import cn.ibizlab.odoo.core.odoo_repair.service.IRepair_lineService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;



import cn.ibizlab.odoo.core.odoo_repair.client.repair_lineOdooClient;
import cn.ibizlab.odoo.core.odoo_repair.clientmodel.repair_lineClientModel;

/**
 * 实体[修理明细行(零件)] 服务对象接口实现
 */
@Slf4j
@Service
public class Repair_lineServiceImpl implements IRepair_lineService {

    @Autowired
    repair_lineOdooClient repair_lineOdooClient;


    @Override
    public Repair_line get(Integer id) {
        repair_lineClientModel clientModel = new repair_lineClientModel();
        clientModel.setId(id);
		repair_lineOdooClient.get(clientModel);
        Repair_line et = convert2Domain(clientModel,null);
        if(et==null){
            et=new Repair_line();
            et.setId(id);
        }
        else{
        }

        return  et;
    }

    @Override
    public boolean update(Repair_line et) {
        repair_lineClientModel clientModel = convert2Model(et,null);
		repair_lineOdooClient.update(clientModel);
        Repair_line rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }
    
    public void updateBatch(List<Repair_line> list){
    }

    @Override
    public boolean remove(Integer id) {
        repair_lineClientModel clientModel = new repair_lineClientModel();
        clientModel.setId(id);
		repair_lineOdooClient.remove(clientModel);
        return true; 
    }

    public void removeBatch(Collection<Integer> idList){
    }

    @Override
    public boolean create(Repair_line et) {
        repair_lineClientModel clientModel = convert2Model(et,null);
		repair_lineOdooClient.create(clientModel);
        Repair_line rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Repair_line> list){
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Repair_line> searchDefault(Repair_lineSearchContext context) {
        List<Repair_line> list = new ArrayList<Repair_line>();
        Page<repair_lineClientModel> clientModelList = repair_lineOdooClient.search(context);
        for(repair_lineClientModel clientModel : clientModelList){
            list.add( this.convert2Domain(clientModel,null));
        }
        return new PageImpl<Repair_line>(list, context.getPageable(), clientModelList.getTotalElements());
    }

    public repair_lineClientModel convert2Model(Repair_line domain , repair_lineClientModel model) {
        if(domain == null)
            return model ;
        if(model==null){
            model = new repair_lineClientModel();

        }
        if((Boolean) domain.getExtensionparams().get("dirtyflagenable")){
            if((Boolean) domain.getExtensionparams().get("display_namedirtyflag"))
                model.setDisplay_name(domain.getDisplayName());
            if((Boolean) domain.getExtensionparams().get("write_datedirtyflag"))
                model.setWrite_date(domain.getWriteDate());
            if((Boolean) domain.getExtensionparams().get("namedirtyflag"))
                model.setName(domain.getName());
            if((Boolean) domain.getExtensionparams().get("product_uom_qtydirtyflag"))
                model.setProduct_uom_qty(domain.getProductUomQty());
            if((Boolean) domain.getExtensionparams().get("invoiceddirtyflag"))
                model.setInvoiced(domain.getInvoiced());
            if((Boolean) domain.getExtensionparams().get("statedirtyflag"))
                model.setState(domain.getState());
            if((Boolean) domain.getExtensionparams().get("typedirtyflag"))
                model.setType(domain.getType());
            if((Boolean) domain.getExtensionparams().get("price_unitdirtyflag"))
                model.setPrice_unit(domain.getPriceUnit());
            if((Boolean) domain.getExtensionparams().get("__last_updatedirtyflag"))
                model.set__last_update(domain.getLastUpdate());
            if((Boolean) domain.getExtensionparams().get("create_datedirtyflag"))
                model.setCreate_date(domain.getCreateDate());
            if((Boolean) domain.getExtensionparams().get("iddirtyflag"))
                model.setId(domain.getId());
            if((Boolean) domain.getExtensionparams().get("tax_iddirtyflag"))
                model.setTax_id(domain.getTaxId());
            if((Boolean) domain.getExtensionparams().get("price_subtotaldirtyflag"))
                model.setPrice_subtotal(domain.getPriceSubtotal());
            if((Boolean) domain.getExtensionparams().get("location_dest_id_textdirtyflag"))
                model.setLocation_dest_id_text(domain.getLocationDestIdText());
            if((Boolean) domain.getExtensionparams().get("write_uid_textdirtyflag"))
                model.setWrite_uid_text(domain.getWriteUidText());
            if((Boolean) domain.getExtensionparams().get("move_id_textdirtyflag"))
                model.setMove_id_text(domain.getMoveIdText());
            if((Boolean) domain.getExtensionparams().get("location_id_textdirtyflag"))
                model.setLocation_id_text(domain.getLocationIdText());
            if((Boolean) domain.getExtensionparams().get("lot_id_textdirtyflag"))
                model.setLot_id_text(domain.getLotIdText());
            if((Boolean) domain.getExtensionparams().get("product_uom_textdirtyflag"))
                model.setProduct_uom_text(domain.getProductUomText());
            if((Boolean) domain.getExtensionparams().get("invoice_line_id_textdirtyflag"))
                model.setInvoice_line_id_text(domain.getInvoiceLineIdText());
            if((Boolean) domain.getExtensionparams().get("create_uid_textdirtyflag"))
                model.setCreate_uid_text(domain.getCreateUidText());
            if((Boolean) domain.getExtensionparams().get("product_id_textdirtyflag"))
                model.setProduct_id_text(domain.getProductIdText());
            if((Boolean) domain.getExtensionparams().get("repair_id_textdirtyflag"))
                model.setRepair_id_text(domain.getRepairIdText());
            if((Boolean) domain.getExtensionparams().get("lot_iddirtyflag"))
                model.setLot_id(domain.getLotId());
            if((Boolean) domain.getExtensionparams().get("repair_iddirtyflag"))
                model.setRepair_id(domain.getRepairId());
            if((Boolean) domain.getExtensionparams().get("move_iddirtyflag"))
                model.setMove_id(domain.getMoveId());
            if((Boolean) domain.getExtensionparams().get("invoice_line_iddirtyflag"))
                model.setInvoice_line_id(domain.getInvoiceLineId());
            if((Boolean) domain.getExtensionparams().get("product_iddirtyflag"))
                model.setProduct_id(domain.getProductId());
            if((Boolean) domain.getExtensionparams().get("location_iddirtyflag"))
                model.setLocation_id(domain.getLocationId());
            if((Boolean) domain.getExtensionparams().get("create_uiddirtyflag"))
                model.setCreate_uid(domain.getCreateUid());
            if((Boolean) domain.getExtensionparams().get("location_dest_iddirtyflag"))
                model.setLocation_dest_id(domain.getLocationDestId());
            if((Boolean) domain.getExtensionparams().get("write_uiddirtyflag"))
                model.setWrite_uid(domain.getWriteUid());
            if((Boolean) domain.getExtensionparams().get("product_uomdirtyflag"))
                model.setProduct_uom(domain.getProductUom());
            domain.getFocusNull().clear();
        }
        return model ;
    }

    public Repair_line convert2Domain( repair_lineClientModel model ,Repair_line domain) {
        if(model == null)
            return domain ;
        if(domain == null){
            domain = new Repair_line();
        }

        if(model.getDisplay_nameDirtyFlag())
            domain.setDisplayName(model.getDisplay_name());
        if(model.getWrite_dateDirtyFlag())
            domain.setWriteDate(model.getWrite_date());
        if(model.getNameDirtyFlag())
            domain.setName(model.getName());
        if(model.getProduct_uom_qtyDirtyFlag())
            domain.setProductUomQty(model.getProduct_uom_qty());
        if(model.getInvoicedDirtyFlag())
            domain.setInvoiced(model.getInvoiced());
        if(model.getStateDirtyFlag())
            domain.setState(model.getState());
        if(model.getTypeDirtyFlag())
            domain.setType(model.getType());
        if(model.getPrice_unitDirtyFlag())
            domain.setPriceUnit(model.getPrice_unit());
        if(model.get__last_updateDirtyFlag())
            domain.setLastUpdate(model.get__last_update());
        if(model.getCreate_dateDirtyFlag())
            domain.setCreateDate(model.getCreate_date());
        if(model.getIdDirtyFlag())
            domain.setId(model.getId());
        if(model.getTax_idDirtyFlag())
            domain.setTaxId(model.getTax_id());
        if(model.getPrice_subtotalDirtyFlag())
            domain.setPriceSubtotal(model.getPrice_subtotal());
        if(model.getLocation_dest_id_textDirtyFlag())
            domain.setLocationDestIdText(model.getLocation_dest_id_text());
        if(model.getWrite_uid_textDirtyFlag())
            domain.setWriteUidText(model.getWrite_uid_text());
        if(model.getMove_id_textDirtyFlag())
            domain.setMoveIdText(model.getMove_id_text());
        if(model.getLocation_id_textDirtyFlag())
            domain.setLocationIdText(model.getLocation_id_text());
        if(model.getLot_id_textDirtyFlag())
            domain.setLotIdText(model.getLot_id_text());
        if(model.getProduct_uom_textDirtyFlag())
            domain.setProductUomText(model.getProduct_uom_text());
        if(model.getInvoice_line_id_textDirtyFlag())
            domain.setInvoiceLineIdText(model.getInvoice_line_id_text());
        if(model.getCreate_uid_textDirtyFlag())
            domain.setCreateUidText(model.getCreate_uid_text());
        if(model.getProduct_id_textDirtyFlag())
            domain.setProductIdText(model.getProduct_id_text());
        if(model.getRepair_id_textDirtyFlag())
            domain.setRepairIdText(model.getRepair_id_text());
        if(model.getLot_idDirtyFlag())
            domain.setLotId(model.getLot_id());
        if(model.getRepair_idDirtyFlag())
            domain.setRepairId(model.getRepair_id());
        if(model.getMove_idDirtyFlag())
            domain.setMoveId(model.getMove_id());
        if(model.getInvoice_line_idDirtyFlag())
            domain.setInvoiceLineId(model.getInvoice_line_id());
        if(model.getProduct_idDirtyFlag())
            domain.setProductId(model.getProduct_id());
        if(model.getLocation_idDirtyFlag())
            domain.setLocationId(model.getLocation_id());
        if(model.getCreate_uidDirtyFlag())
            domain.setCreateUid(model.getCreate_uid());
        if(model.getLocation_dest_idDirtyFlag())
            domain.setLocationDestId(model.getLocation_dest_id());
        if(model.getWrite_uidDirtyFlag())
            domain.setWriteUid(model.getWrite_uid());
        if(model.getProduct_uomDirtyFlag())
            domain.setProductUom(model.getProduct_uom());
        return domain ;
    }

}

    



