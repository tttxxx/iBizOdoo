package cn.ibizlab.odoo.core.client.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.client.model.Imrp_product_produce_line;
import cn.ibizlab.odoo.util.SearchContext;

/**
 * 实体[mrp_product_produce_line] 服务对象接口
 */
public interface Imrp_product_produce_lineClientService{

    public Imrp_product_produce_line createModel() ;

    public void createBatch(List<Imrp_product_produce_line> mrp_product_produce_lines);

    public void removeBatch(List<Imrp_product_produce_line> mrp_product_produce_lines);

    public void update(Imrp_product_produce_line mrp_product_produce_line);

    public Page<Imrp_product_produce_line> search(SearchContext context);

    public void remove(Imrp_product_produce_line mrp_product_produce_line);

    public void get(Imrp_product_produce_line mrp_product_produce_line);

    public void updateBatch(List<Imrp_product_produce_line> mrp_product_produce_lines);

    public void create(Imrp_product_produce_line mrp_product_produce_line);

    public Page<Imrp_product_produce_line> select(SearchContext context);

}
