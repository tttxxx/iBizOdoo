package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Account_print_journal;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_print_journalSearchContext;

/**
 * 实体 [会计打印日记账] 存储对象
 */
public interface Account_print_journalRepository extends Repository<Account_print_journal> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Account_print_journal> searchDefault(Account_print_journalSearchContext context);

    Account_print_journal convert2PO(cn.ibizlab.odoo.core.odoo_account.domain.Account_print_journal domain , Account_print_journal po) ;

    cn.ibizlab.odoo.core.odoo_account.domain.Account_print_journal convert2Domain( Account_print_journal po ,cn.ibizlab.odoo.core.odoo_account.domain.Account_print_journal domain) ;

}
