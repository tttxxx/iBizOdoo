package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Crm_lead2opportunity_partner;
import cn.ibizlab.odoo.core.odoo_crm.filter.Crm_lead2opportunity_partnerSearchContext;

/**
 * 实体 [转化线索为商机（单个）] 存储对象
 */
public interface Crm_lead2opportunity_partnerRepository extends Repository<Crm_lead2opportunity_partner> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Crm_lead2opportunity_partner> searchDefault(Crm_lead2opportunity_partnerSearchContext context);

    Crm_lead2opportunity_partner convert2PO(cn.ibizlab.odoo.core.odoo_crm.domain.Crm_lead2opportunity_partner domain , Crm_lead2opportunity_partner po) ;

    cn.ibizlab.odoo.core.odoo_crm.domain.Crm_lead2opportunity_partner convert2Domain( Crm_lead2opportunity_partner po ,cn.ibizlab.odoo.core.odoo_crm.domain.Crm_lead2opportunity_partner domain) ;

}
