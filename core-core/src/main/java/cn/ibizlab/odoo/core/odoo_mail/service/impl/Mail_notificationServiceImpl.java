package cn.ibizlab.odoo.core.odoo_mail.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_mail.domain.Mail_notification;
import cn.ibizlab.odoo.core.odoo_mail.filter.Mail_notificationSearchContext;
import cn.ibizlab.odoo.core.odoo_mail.service.IMail_notificationService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;



import cn.ibizlab.odoo.core.odoo_mail.client.mail_notificationOdooClient;
import cn.ibizlab.odoo.core.odoo_mail.clientmodel.mail_notificationClientModel;

/**
 * 实体[消息通知] 服务对象接口实现
 */
@Slf4j
@Service
public class Mail_notificationServiceImpl implements IMail_notificationService {

    @Autowired
    mail_notificationOdooClient mail_notificationOdooClient;


    @Override
    public boolean update(Mail_notification et) {
        mail_notificationClientModel clientModel = convert2Model(et,null);
		mail_notificationOdooClient.update(clientModel);
        Mail_notification rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }
    
    public void updateBatch(List<Mail_notification> list){
    }

    @Override
    public boolean create(Mail_notification et) {
        mail_notificationClientModel clientModel = convert2Model(et,null);
		mail_notificationOdooClient.create(clientModel);
        Mail_notification rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Mail_notification> list){
    }

    @Override
    public Mail_notification get(Integer id) {
        mail_notificationClientModel clientModel = new mail_notificationClientModel();
        clientModel.setId(id);
		mail_notificationOdooClient.get(clientModel);
        Mail_notification et = convert2Domain(clientModel,null);
        if(et==null){
            et=new Mail_notification();
            et.setId(id);
        }
        else{
        }

        return  et;
    }

    @Override
    public boolean remove(Integer id) {
        mail_notificationClientModel clientModel = new mail_notificationClientModel();
        clientModel.setId(id);
		mail_notificationOdooClient.remove(clientModel);
        return true; 
    }

    public void removeBatch(Collection<Integer> idList){
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Mail_notification> searchDefault(Mail_notificationSearchContext context) {
        List<Mail_notification> list = new ArrayList<Mail_notification>();
        Page<mail_notificationClientModel> clientModelList = mail_notificationOdooClient.search(context);
        for(mail_notificationClientModel clientModel : clientModelList){
            list.add( this.convert2Domain(clientModel,null));
        }
        return new PageImpl<Mail_notification>(list, context.getPageable(), clientModelList.getTotalElements());
    }

    public mail_notificationClientModel convert2Model(Mail_notification domain , mail_notificationClientModel model) {
        if(domain == null)
            return model ;
        if(model==null){
            model = new mail_notificationClientModel();

        }
        if((Boolean) domain.getExtensionparams().get("dirtyflagenable")){
            if((Boolean) domain.getExtensionparams().get("__last_updatedirtyflag"))
                model.set__last_update(domain.getLastUpdate());
            if((Boolean) domain.getExtensionparams().get("iddirtyflag"))
                model.setId(domain.getId());
            if((Boolean) domain.getExtensionparams().get("failure_reasondirtyflag"))
                model.setFailure_reason(domain.getFailureReason());
            if((Boolean) domain.getExtensionparams().get("is_emaildirtyflag"))
                model.setIs_email(domain.getIsEmail());
            if((Boolean) domain.getExtensionparams().get("is_readdirtyflag"))
                model.setIs_read(domain.getIsRead());
            if((Boolean) domain.getExtensionparams().get("email_statusdirtyflag"))
                model.setEmail_status(domain.getEmailStatus());
            if((Boolean) domain.getExtensionparams().get("display_namedirtyflag"))
                model.setDisplay_name(domain.getDisplayName());
            if((Boolean) domain.getExtensionparams().get("failure_typedirtyflag"))
                model.setFailure_type(domain.getFailureType());
            if((Boolean) domain.getExtensionparams().get("res_partner_id_textdirtyflag"))
                model.setRes_partner_id_text(domain.getResPartnerIdText());
            if((Boolean) domain.getExtensionparams().get("mail_iddirtyflag"))
                model.setMail_id(domain.getMailId());
            if((Boolean) domain.getExtensionparams().get("mail_message_iddirtyflag"))
                model.setMail_message_id(domain.getMailMessageId());
            if((Boolean) domain.getExtensionparams().get("res_partner_iddirtyflag"))
                model.setRes_partner_id(domain.getResPartnerId());
            domain.getFocusNull().clear();
        }
        return model ;
    }

    public Mail_notification convert2Domain( mail_notificationClientModel model ,Mail_notification domain) {
        if(model == null)
            return domain ;
        if(domain == null){
            domain = new Mail_notification();
        }

        if(model.get__last_updateDirtyFlag())
            domain.setLastUpdate(model.get__last_update());
        if(model.getIdDirtyFlag())
            domain.setId(model.getId());
        if(model.getFailure_reasonDirtyFlag())
            domain.setFailureReason(model.getFailure_reason());
        if(model.getIs_emailDirtyFlag())
            domain.setIsEmail(model.getIs_email());
        if(model.getIs_readDirtyFlag())
            domain.setIsRead(model.getIs_read());
        if(model.getEmail_statusDirtyFlag())
            domain.setEmailStatus(model.getEmail_status());
        if(model.getDisplay_nameDirtyFlag())
            domain.setDisplayName(model.getDisplay_name());
        if(model.getFailure_typeDirtyFlag())
            domain.setFailureType(model.getFailure_type());
        if(model.getRes_partner_id_textDirtyFlag())
            domain.setResPartnerIdText(model.getRes_partner_id_text());
        if(model.getMail_idDirtyFlag())
            domain.setMailId(model.getMail_id());
        if(model.getMail_message_idDirtyFlag())
            domain.setMailMessageId(model.getMail_message_id());
        if(model.getRes_partner_idDirtyFlag())
            domain.setResPartnerId(model.getRes_partner_id());
        return domain ;
    }

}

    



