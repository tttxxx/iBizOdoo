package cn.ibizlab.odoo.core.repository.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import cn.ibizlab.odoo.core.repository.client.po.mail_mass_mailing_campaign;

/**
 * 实体[mail_mass_mailing_campaign] 服务对象接口
 */
public interface mail_mass_mailing_campaignRepository{


    public mail_mass_mailing_campaign createPO() ;
        public void update(mail_mass_mailing_campaign mail_mass_mailing_campaign);

        public void remove(String id);

        public void createBatch(mail_mass_mailing_campaign mail_mass_mailing_campaign);

        public void updateBatch(mail_mass_mailing_campaign mail_mass_mailing_campaign);

        public void get(String id);

        public List<mail_mass_mailing_campaign> search();

        public void create(mail_mass_mailing_campaign mail_mass_mailing_campaign);

        public void removeBatch(String id);


}
