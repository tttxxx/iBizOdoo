package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Fleet_vehicle_log_contract;
import cn.ibizlab.odoo.core.odoo_fleet.filter.Fleet_vehicle_log_contractSearchContext;

/**
 * 实体 [车辆合同信息] 存储对象
 */
public interface Fleet_vehicle_log_contractRepository extends Repository<Fleet_vehicle_log_contract> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Fleet_vehicle_log_contract> searchDefault(Fleet_vehicle_log_contractSearchContext context);

    Fleet_vehicle_log_contract convert2PO(cn.ibizlab.odoo.core.odoo_fleet.domain.Fleet_vehicle_log_contract domain , Fleet_vehicle_log_contract po) ;

    cn.ibizlab.odoo.core.odoo_fleet.domain.Fleet_vehicle_log_contract convert2Domain( Fleet_vehicle_log_contract po ,cn.ibizlab.odoo.core.odoo_fleet.domain.Fleet_vehicle_log_contract domain) ;

}
