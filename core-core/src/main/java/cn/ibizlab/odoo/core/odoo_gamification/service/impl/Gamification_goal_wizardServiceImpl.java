package cn.ibizlab.odoo.core.odoo_gamification.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_gamification.domain.Gamification_goal_wizard;
import cn.ibizlab.odoo.core.odoo_gamification.filter.Gamification_goal_wizardSearchContext;
import cn.ibizlab.odoo.core.odoo_gamification.service.IGamification_goal_wizardService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;



import cn.ibizlab.odoo.core.odoo_gamification.client.gamification_goal_wizardOdooClient;
import cn.ibizlab.odoo.core.odoo_gamification.clientmodel.gamification_goal_wizardClientModel;

/**
 * 实体[游戏化目标向导] 服务对象接口实现
 */
@Slf4j
@Service
public class Gamification_goal_wizardServiceImpl implements IGamification_goal_wizardService {

    @Autowired
    gamification_goal_wizardOdooClient gamification_goal_wizardOdooClient;


    @Override
    public boolean remove(Integer id) {
        gamification_goal_wizardClientModel clientModel = new gamification_goal_wizardClientModel();
        clientModel.setId(id);
		gamification_goal_wizardOdooClient.remove(clientModel);
        return true; 
    }

    public void removeBatch(Collection<Integer> idList){
    }

    @Override
    public boolean update(Gamification_goal_wizard et) {
        gamification_goal_wizardClientModel clientModel = convert2Model(et,null);
		gamification_goal_wizardOdooClient.update(clientModel);
        Gamification_goal_wizard rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }
    
    public void updateBatch(List<Gamification_goal_wizard> list){
    }

    @Override
    public boolean create(Gamification_goal_wizard et) {
        gamification_goal_wizardClientModel clientModel = convert2Model(et,null);
		gamification_goal_wizardOdooClient.create(clientModel);
        Gamification_goal_wizard rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Gamification_goal_wizard> list){
    }

    @Override
    public Gamification_goal_wizard get(Integer id) {
        gamification_goal_wizardClientModel clientModel = new gamification_goal_wizardClientModel();
        clientModel.setId(id);
		gamification_goal_wizardOdooClient.get(clientModel);
        Gamification_goal_wizard et = convert2Domain(clientModel,null);
        if(et==null){
            et=new Gamification_goal_wizard();
            et.setId(id);
        }
        else{
        }

        return  et;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Gamification_goal_wizard> searchDefault(Gamification_goal_wizardSearchContext context) {
        List<Gamification_goal_wizard> list = new ArrayList<Gamification_goal_wizard>();
        Page<gamification_goal_wizardClientModel> clientModelList = gamification_goal_wizardOdooClient.search(context);
        for(gamification_goal_wizardClientModel clientModel : clientModelList){
            list.add( this.convert2Domain(clientModel,null));
        }
        return new PageImpl<Gamification_goal_wizard>(list, context.getPageable(), clientModelList.getTotalElements());
    }

    public gamification_goal_wizardClientModel convert2Model(Gamification_goal_wizard domain , gamification_goal_wizardClientModel model) {
        if(domain == null)
            return model ;
        if(model==null){
            model = new gamification_goal_wizardClientModel();

        }
        if((Boolean) domain.getExtensionparams().get("dirtyflagenable")){
            if((Boolean) domain.getExtensionparams().get("iddirtyflag"))
                model.setId(domain.getId());
            if((Boolean) domain.getExtensionparams().get("create_datedirtyflag"))
                model.setCreate_date(domain.getCreateDate());
            if((Boolean) domain.getExtensionparams().get("currentdirtyflag"))
                model.setCurrent(domain.getCurrent());
            if((Boolean) domain.getExtensionparams().get("write_datedirtyflag"))
                model.setWrite_date(domain.getWriteDate());
            if((Boolean) domain.getExtensionparams().get("display_namedirtyflag"))
                model.setDisplay_name(domain.getDisplayName());
            if((Boolean) domain.getExtensionparams().get("__last_updatedirtyflag"))
                model.set__last_update(domain.getLastUpdate());
            if((Boolean) domain.getExtensionparams().get("write_uid_textdirtyflag"))
                model.setWrite_uid_text(domain.getWriteUidText());
            if((Boolean) domain.getExtensionparams().get("create_uid_textdirtyflag"))
                model.setCreate_uid_text(domain.getCreateUidText());
            if((Boolean) domain.getExtensionparams().get("create_uiddirtyflag"))
                model.setCreate_uid(domain.getCreateUid());
            if((Boolean) domain.getExtensionparams().get("goal_iddirtyflag"))
                model.setGoal_id(domain.getGoalId());
            if((Boolean) domain.getExtensionparams().get("write_uiddirtyflag"))
                model.setWrite_uid(domain.getWriteUid());
            domain.getFocusNull().clear();
        }
        return model ;
    }

    public Gamification_goal_wizard convert2Domain( gamification_goal_wizardClientModel model ,Gamification_goal_wizard domain) {
        if(model == null)
            return domain ;
        if(domain == null){
            domain = new Gamification_goal_wizard();
        }

        if(model.getIdDirtyFlag())
            domain.setId(model.getId());
        if(model.getCreate_dateDirtyFlag())
            domain.setCreateDate(model.getCreate_date());
        if(model.getCurrentDirtyFlag())
            domain.setCurrent(model.getCurrent());
        if(model.getWrite_dateDirtyFlag())
            domain.setWriteDate(model.getWrite_date());
        if(model.getDisplay_nameDirtyFlag())
            domain.setDisplayName(model.getDisplay_name());
        if(model.get__last_updateDirtyFlag())
            domain.setLastUpdate(model.get__last_update());
        if(model.getWrite_uid_textDirtyFlag())
            domain.setWriteUidText(model.getWrite_uid_text());
        if(model.getCreate_uid_textDirtyFlag())
            domain.setCreateUidText(model.getCreate_uid_text());
        if(model.getCreate_uidDirtyFlag())
            domain.setCreateUid(model.getCreate_uid());
        if(model.getGoal_idDirtyFlag())
            domain.setGoalId(model.getGoal_id());
        if(model.getWrite_uidDirtyFlag())
            domain.setWriteUid(model.getWrite_uid());
        return domain ;
    }

}

    



