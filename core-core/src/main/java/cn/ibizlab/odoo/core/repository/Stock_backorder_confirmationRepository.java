package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Stock_backorder_confirmation;
import cn.ibizlab.odoo.core.odoo_stock.filter.Stock_backorder_confirmationSearchContext;

/**
 * 实体 [欠单确认] 存储对象
 */
public interface Stock_backorder_confirmationRepository extends Repository<Stock_backorder_confirmation> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Stock_backorder_confirmation> searchDefault(Stock_backorder_confirmationSearchContext context);

    Stock_backorder_confirmation convert2PO(cn.ibizlab.odoo.core.odoo_stock.domain.Stock_backorder_confirmation domain , Stock_backorder_confirmation po) ;

    cn.ibizlab.odoo.core.odoo_stock.domain.Stock_backorder_confirmation convert2Domain( Stock_backorder_confirmation po ,cn.ibizlab.odoo.core.odoo_stock.domain.Stock_backorder_confirmation domain) ;

}
