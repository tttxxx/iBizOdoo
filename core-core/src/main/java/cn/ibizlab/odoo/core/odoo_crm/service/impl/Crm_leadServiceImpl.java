package cn.ibizlab.odoo.core.odoo_crm.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_crm.domain.Crm_lead;
import cn.ibizlab.odoo.core.odoo_crm.filter.Crm_leadSearchContext;
import cn.ibizlab.odoo.core.odoo_crm.service.ICrm_leadService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;



import cn.ibizlab.odoo.core.odoo_crm.client.crm_leadOdooClient;
import cn.ibizlab.odoo.core.odoo_crm.clientmodel.crm_leadClientModel;

/**
 * 实体[线索/商机] 服务对象接口实现
 */
@Slf4j
@Service
public class Crm_leadServiceImpl implements ICrm_leadService {

    @Autowired
    crm_leadOdooClient crm_leadOdooClient;


    @Override
    public boolean remove(Integer id) {
        crm_leadClientModel clientModel = new crm_leadClientModel();
        clientModel.setId(id);
		crm_leadOdooClient.remove(clientModel);
        return true; 
    }

    public void removeBatch(Collection<Integer> idList){
    }

    @Override
    public Crm_lead get(Integer id) {
        crm_leadClientModel clientModel = new crm_leadClientModel();
        clientModel.setId(id);
		crm_leadOdooClient.get(clientModel);
        Crm_lead et = convert2Domain(clientModel,null);
        if(et==null){
            et=new Crm_lead();
            et.setId(id);
        }
        else{
        }

        return  et;
    }

    @Override
    public boolean create(Crm_lead et) {
        crm_leadClientModel clientModel = convert2Model(et,null);
		crm_leadOdooClient.create(clientModel);
        Crm_lead rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Crm_lead> list){
    }

    @Override
    public boolean update(Crm_lead et) {
        crm_leadClientModel clientModel = convert2Model(et,null);
		crm_leadOdooClient.update(clientModel);
        Crm_lead rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }
    
    public void updateBatch(List<Crm_lead> list){
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Crm_lead> searchDefault(Crm_leadSearchContext context) {
        List<Crm_lead> list = new ArrayList<Crm_lead>();
        Page<crm_leadClientModel> clientModelList = crm_leadOdooClient.search(context);
        for(crm_leadClientModel clientModel : clientModelList){
            list.add( this.convert2Domain(clientModel,null));
        }
        return new PageImpl<Crm_lead>(list, context.getPageable(), clientModelList.getTotalElements());
    }

    public crm_leadClientModel convert2Model(Crm_lead domain , crm_leadClientModel model) {
        if(domain == null)
            return model ;
        if(model==null){
            model = new crm_leadClientModel();

        }
        if((Boolean) domain.getExtensionparams().get("dirtyflagenable")){
            if((Boolean) domain.getExtensionparams().get("activity_user_iddirtyflag"))
                model.setActivity_user_id(domain.getActivityUserId());
            if((Boolean) domain.getExtensionparams().get("meeting_countdirtyflag"))
                model.setMeeting_count(domain.getMeetingCount());
            if((Boolean) domain.getExtensionparams().get("ibizfunctiondirtyflag"))
                model.setIbizfunction(domain.getIbizfunction());
            if((Boolean) domain.getExtensionparams().get("message_bouncedirtyflag"))
                model.setMessage_bounce(domain.getMessageBounce());
            if((Boolean) domain.getExtensionparams().get("citydirtyflag"))
                model.setCity(domain.getCity());
            if((Boolean) domain.getExtensionparams().get("day_closedirtyflag"))
                model.setDay_close(domain.getDayClose());
            if((Boolean) domain.getExtensionparams().get("activity_idsdirtyflag"))
                model.setActivity_ids(domain.getActivityIds());
            if((Boolean) domain.getExtensionparams().get("message_has_errordirtyflag"))
                model.setMessage_has_error(domain.getMessageHasError());
            if((Boolean) domain.getExtensionparams().get("message_unread_counterdirtyflag"))
                model.setMessage_unread_counter(domain.getMessageUnreadCounter());
            if((Boolean) domain.getExtensionparams().get("expected_revenuedirtyflag"))
                model.setExpected_revenue(domain.getExpectedRevenue());
            if((Boolean) domain.getExtensionparams().get("date_closeddirtyflag"))
                model.setDate_closed(domain.getDateClosed());
            if((Boolean) domain.getExtensionparams().get("create_datedirtyflag"))
                model.setCreate_date(domain.getCreateDate());
            if((Boolean) domain.getExtensionparams().get("email_ccdirtyflag"))
                model.setEmail_cc(domain.getEmailCc());
            if((Boolean) domain.getExtensionparams().get("contact_namedirtyflag"))
                model.setContact_name(domain.getContactName());
            if((Boolean) domain.getExtensionparams().get("date_last_stage_updatedirtyflag"))
                model.setDate_last_stage_update(domain.getDateLastStageUpdate());
            if((Boolean) domain.getExtensionparams().get("planned_revenuedirtyflag"))
                model.setPlanned_revenue(domain.getPlannedRevenue());
            if((Boolean) domain.getExtensionparams().get("website_message_idsdirtyflag"))
                model.setWebsite_message_ids(domain.getWebsiteMessageIds());
            if((Boolean) domain.getExtensionparams().get("activity_type_iddirtyflag"))
                model.setActivity_type_id(domain.getActivityTypeId());
            if((Boolean) domain.getExtensionparams().get("date_deadlinedirtyflag"))
                model.setDate_deadline(domain.getDateDeadline());
            if((Boolean) domain.getExtensionparams().get("zipdirtyflag"))
                model.setZip(domain.getZip());
            if((Boolean) domain.getExtensionparams().get("namedirtyflag"))
                model.setName(domain.getName());
            if((Boolean) domain.getExtensionparams().get("iddirtyflag"))
                model.setId(domain.getId());
            if((Boolean) domain.getExtensionparams().get("descriptiondirtyflag"))
                model.setDescription(domain.getDescription());
            if((Boolean) domain.getExtensionparams().get("mobiledirtyflag"))
                model.setMobile(domain.getMobile());
            if((Boolean) domain.getExtensionparams().get("message_idsdirtyflag"))
                model.setMessage_ids(domain.getMessageIds());
            if((Boolean) domain.getExtensionparams().get("message_partner_idsdirtyflag"))
                model.setMessage_partner_ids(domain.getMessagePartnerIds());
            if((Boolean) domain.getExtensionparams().get("activity_statedirtyflag"))
                model.setActivity_state(domain.getActivityState());
            if((Boolean) domain.getExtensionparams().get("activity_date_deadlinedirtyflag"))
                model.setActivity_date_deadline(domain.getActivityDateDeadline());
            if((Boolean) domain.getExtensionparams().get("typedirtyflag"))
                model.setType(domain.getType());
            if((Boolean) domain.getExtensionparams().get("websitedirtyflag"))
                model.setWebsite(domain.getWebsite());
            if((Boolean) domain.getExtensionparams().get("message_attachment_countdirtyflag"))
                model.setMessage_attachment_count(domain.getMessageAttachmentCount());
            if((Boolean) domain.getExtensionparams().get("email_fromdirtyflag"))
                model.setEmail_from(domain.getEmailFrom());
            if((Boolean) domain.getExtensionparams().get("date_conversiondirtyflag"))
                model.setDate_conversion(domain.getDateConversion());
            if((Boolean) domain.getExtensionparams().get("partner_namedirtyflag"))
                model.setPartner_name(domain.getPartnerName());
            if((Boolean) domain.getExtensionparams().get("sale_amount_totaldirtyflag"))
                model.setSale_amount_total(domain.getSaleAmountTotal());
            if((Boolean) domain.getExtensionparams().get("message_unreaddirtyflag"))
                model.setMessage_unread(domain.getMessageUnread());
            if((Boolean) domain.getExtensionparams().get("message_needactiondirtyflag"))
                model.setMessage_needaction(domain.getMessageNeedaction());
            if((Boolean) domain.getExtensionparams().get("__last_updatedirtyflag"))
                model.set__last_update(domain.getLastUpdate());
            if((Boolean) domain.getExtensionparams().get("kanban_statedirtyflag"))
                model.setKanban_state(domain.getKanbanState());
            if((Boolean) domain.getExtensionparams().get("message_main_attachment_iddirtyflag"))
                model.setMessage_main_attachment_id(domain.getMessageMainAttachmentId());
            if((Boolean) domain.getExtensionparams().get("message_channel_idsdirtyflag"))
                model.setMessage_channel_ids(domain.getMessageChannelIds());
            if((Boolean) domain.getExtensionparams().get("referreddirtyflag"))
                model.setReferred(domain.getReferred());
            if((Boolean) domain.getExtensionparams().get("probabilitydirtyflag"))
                model.setProbability(domain.getProbability());
            if((Boolean) domain.getExtensionparams().get("date_action_lastdirtyflag"))
                model.setDate_action_last(domain.getDateActionLast());
            if((Boolean) domain.getExtensionparams().get("message_has_error_counterdirtyflag"))
                model.setMessage_has_error_counter(domain.getMessageHasErrorCounter());
            if((Boolean) domain.getExtensionparams().get("date_opendirtyflag"))
                model.setDate_open(domain.getDateOpen());
            if((Boolean) domain.getExtensionparams().get("phonedirtyflag"))
                model.setPhone(domain.getPhone());
            if((Boolean) domain.getExtensionparams().get("message_is_followerdirtyflag"))
                model.setMessage_is_follower(domain.getMessageIsFollower());
            if((Boolean) domain.getExtensionparams().get("day_opendirtyflag"))
                model.setDay_open(domain.getDayOpen());
            if((Boolean) domain.getExtensionparams().get("activity_summarydirtyflag"))
                model.setActivity_summary(domain.getActivitySummary());
            if((Boolean) domain.getExtensionparams().get("sale_numberdirtyflag"))
                model.setSale_number(domain.getSaleNumber());
            if((Boolean) domain.getExtensionparams().get("street2dirtyflag"))
                model.setStreet2(domain.getStreet2());
            if((Boolean) domain.getExtensionparams().get("order_idsdirtyflag"))
                model.setOrder_ids(domain.getOrderIds());
            if((Boolean) domain.getExtensionparams().get("message_follower_idsdirtyflag"))
                model.setMessage_follower_ids(domain.getMessageFollowerIds());
            if((Boolean) domain.getExtensionparams().get("streetdirtyflag"))
                model.setStreet(domain.getStreet());
            if((Boolean) domain.getExtensionparams().get("colordirtyflag"))
                model.setColor(domain.getColor());
            if((Boolean) domain.getExtensionparams().get("write_datedirtyflag"))
                model.setWrite_date(domain.getWriteDate());
            if((Boolean) domain.getExtensionparams().get("activedirtyflag"))
                model.setActive(domain.getActive());
            if((Boolean) domain.getExtensionparams().get("display_namedirtyflag"))
                model.setDisplay_name(domain.getDisplayName());
            if((Boolean) domain.getExtensionparams().get("tag_idsdirtyflag"))
                model.setTag_ids(domain.getTagIds());
            if((Boolean) domain.getExtensionparams().get("is_blacklisteddirtyflag"))
                model.setIs_blacklisted(domain.getIsBlacklisted());
            if((Boolean) domain.getExtensionparams().get("message_needaction_counterdirtyflag"))
                model.setMessage_needaction_counter(domain.getMessageNeedactionCounter());
            if((Boolean) domain.getExtensionparams().get("prioritydirtyflag"))
                model.setPriority(domain.getPriority());
            if((Boolean) domain.getExtensionparams().get("user_id_textdirtyflag"))
                model.setUser_id_text(domain.getUserIdText());
            if((Boolean) domain.getExtensionparams().get("source_id_textdirtyflag"))
                model.setSource_id_text(domain.getSourceIdText());
            if((Boolean) domain.getExtensionparams().get("partner_address_namedirtyflag"))
                model.setPartner_address_name(domain.getPartnerAddressName());
            if((Boolean) domain.getExtensionparams().get("medium_id_textdirtyflag"))
                model.setMedium_id_text(domain.getMediumIdText());
            if((Boolean) domain.getExtensionparams().get("company_currencydirtyflag"))
                model.setCompany_currency(domain.getCompanyCurrency());
            if((Boolean) domain.getExtensionparams().get("team_id_textdirtyflag"))
                model.setTeam_id_text(domain.getTeamIdText());
            if((Boolean) domain.getExtensionparams().get("partner_address_mobiledirtyflag"))
                model.setPartner_address_mobile(domain.getPartnerAddressMobile());
            if((Boolean) domain.getExtensionparams().get("write_uid_textdirtyflag"))
                model.setWrite_uid_text(domain.getWriteUidText());
            if((Boolean) domain.getExtensionparams().get("create_uid_textdirtyflag"))
                model.setCreate_uid_text(domain.getCreateUidText());
            if((Boolean) domain.getExtensionparams().get("user_emaildirtyflag"))
                model.setUser_email(domain.getUserEmail());
            if((Boolean) domain.getExtensionparams().get("user_logindirtyflag"))
                model.setUser_login(domain.getUserLogin());
            if((Boolean) domain.getExtensionparams().get("partner_address_phonedirtyflag"))
                model.setPartner_address_phone(domain.getPartnerAddressPhone());
            if((Boolean) domain.getExtensionparams().get("state_id_textdirtyflag"))
                model.setState_id_text(domain.getStateIdText());
            if((Boolean) domain.getExtensionparams().get("campaign_id_textdirtyflag"))
                model.setCampaign_id_text(domain.getCampaignIdText());
            if((Boolean) domain.getExtensionparams().get("partner_is_blacklisteddirtyflag"))
                model.setPartner_is_blacklisted(domain.getPartnerIsBlacklisted());
            if((Boolean) domain.getExtensionparams().get("partner_address_emaildirtyflag"))
                model.setPartner_address_email(domain.getPartnerAddressEmail());
            if((Boolean) domain.getExtensionparams().get("stage_id_textdirtyflag"))
                model.setStage_id_text(domain.getStageIdText());
            if((Boolean) domain.getExtensionparams().get("company_id_textdirtyflag"))
                model.setCompany_id_text(domain.getCompanyIdText());
            if((Boolean) domain.getExtensionparams().get("country_id_textdirtyflag"))
                model.setCountry_id_text(domain.getCountryIdText());
            if((Boolean) domain.getExtensionparams().get("title_textdirtyflag"))
                model.setTitle_text(domain.getTitleText());
            if((Boolean) domain.getExtensionparams().get("lost_reason_textdirtyflag"))
                model.setLost_reason_text(domain.getLostReasonText());
            if((Boolean) domain.getExtensionparams().get("lost_reasondirtyflag"))
                model.setLost_reason(domain.getLostReason());
            if((Boolean) domain.getExtensionparams().get("create_uiddirtyflag"))
                model.setCreate_uid(domain.getCreateUid());
            if((Boolean) domain.getExtensionparams().get("write_uiddirtyflag"))
                model.setWrite_uid(domain.getWriteUid());
            if((Boolean) domain.getExtensionparams().get("company_iddirtyflag"))
                model.setCompany_id(domain.getCompanyId());
            if((Boolean) domain.getExtensionparams().get("user_iddirtyflag"))
                model.setUser_id(domain.getUserId());
            if((Boolean) domain.getExtensionparams().get("state_iddirtyflag"))
                model.setState_id(domain.getStateId());
            if((Boolean) domain.getExtensionparams().get("medium_iddirtyflag"))
                model.setMedium_id(domain.getMediumId());
            if((Boolean) domain.getExtensionparams().get("stage_iddirtyflag"))
                model.setStage_id(domain.getStageId());
            if((Boolean) domain.getExtensionparams().get("source_iddirtyflag"))
                model.setSource_id(domain.getSourceId());
            if((Boolean) domain.getExtensionparams().get("country_iddirtyflag"))
                model.setCountry_id(domain.getCountryId());
            if((Boolean) domain.getExtensionparams().get("campaign_iddirtyflag"))
                model.setCampaign_id(domain.getCampaignId());
            if((Boolean) domain.getExtensionparams().get("partner_iddirtyflag"))
                model.setPartner_id(domain.getPartnerId());
            if((Boolean) domain.getExtensionparams().get("team_iddirtyflag"))
                model.setTeam_id(domain.getTeamId());
            if((Boolean) domain.getExtensionparams().get("titledirtyflag"))
                model.setTitle(domain.getTitle());
            domain.getFocusNull().clear();
        }
        return model ;
    }

    public Crm_lead convert2Domain( crm_leadClientModel model ,Crm_lead domain) {
        if(model == null)
            return domain ;
        if(domain == null){
            domain = new Crm_lead();
        }

        if(model.getActivity_user_idDirtyFlag())
            domain.setActivityUserId(model.getActivity_user_id());
        if(model.getMeeting_countDirtyFlag())
            domain.setMeetingCount(model.getMeeting_count());
        if(model.getIbizfunctionDirtyFlag())
            domain.setIbizfunction(model.getIbizfunction());
        if(model.getMessage_bounceDirtyFlag())
            domain.setMessageBounce(model.getMessage_bounce());
        if(model.getCityDirtyFlag())
            domain.setCity(model.getCity());
        if(model.getDay_closeDirtyFlag())
            domain.setDayClose(model.getDay_close());
        if(model.getActivity_idsDirtyFlag())
            domain.setActivityIds(model.getActivity_ids());
        if(model.getMessage_has_errorDirtyFlag())
            domain.setMessageHasError(model.getMessage_has_error());
        if(model.getMessage_unread_counterDirtyFlag())
            domain.setMessageUnreadCounter(model.getMessage_unread_counter());
        if(model.getExpected_revenueDirtyFlag())
            domain.setExpectedRevenue(model.getExpected_revenue());
        if(model.getDate_closedDirtyFlag())
            domain.setDateClosed(model.getDate_closed());
        if(model.getCreate_dateDirtyFlag())
            domain.setCreateDate(model.getCreate_date());
        if(model.getEmail_ccDirtyFlag())
            domain.setEmailCc(model.getEmail_cc());
        if(model.getContact_nameDirtyFlag())
            domain.setContactName(model.getContact_name());
        if(model.getDate_last_stage_updateDirtyFlag())
            domain.setDateLastStageUpdate(model.getDate_last_stage_update());
        if(model.getPlanned_revenueDirtyFlag())
            domain.setPlannedRevenue(model.getPlanned_revenue());
        if(model.getWebsite_message_idsDirtyFlag())
            domain.setWebsiteMessageIds(model.getWebsite_message_ids());
        if(model.getActivity_type_idDirtyFlag())
            domain.setActivityTypeId(model.getActivity_type_id());
        if(model.getDate_deadlineDirtyFlag())
            domain.setDateDeadline(model.getDate_deadline());
        if(model.getZipDirtyFlag())
            domain.setZip(model.getZip());
        if(model.getNameDirtyFlag())
            domain.setName(model.getName());
        if(model.getIdDirtyFlag())
            domain.setId(model.getId());
        if(model.getDescriptionDirtyFlag())
            domain.setDescription(model.getDescription());
        if(model.getMobileDirtyFlag())
            domain.setMobile(model.getMobile());
        if(model.getMessage_idsDirtyFlag())
            domain.setMessageIds(model.getMessage_ids());
        if(model.getMessage_partner_idsDirtyFlag())
            domain.setMessagePartnerIds(model.getMessage_partner_ids());
        if(model.getActivity_stateDirtyFlag())
            domain.setActivityState(model.getActivity_state());
        if(model.getActivity_date_deadlineDirtyFlag())
            domain.setActivityDateDeadline(model.getActivity_date_deadline());
        if(model.getTypeDirtyFlag())
            domain.setType(model.getType());
        if(model.getWebsiteDirtyFlag())
            domain.setWebsite(model.getWebsite());
        if(model.getMessage_attachment_countDirtyFlag())
            domain.setMessageAttachmentCount(model.getMessage_attachment_count());
        if(model.getEmail_fromDirtyFlag())
            domain.setEmailFrom(model.getEmail_from());
        if(model.getDate_conversionDirtyFlag())
            domain.setDateConversion(model.getDate_conversion());
        if(model.getPartner_nameDirtyFlag())
            domain.setPartnerName(model.getPartner_name());
        if(model.getSale_amount_totalDirtyFlag())
            domain.setSaleAmountTotal(model.getSale_amount_total());
        if(model.getMessage_unreadDirtyFlag())
            domain.setMessageUnread(model.getMessage_unread());
        if(model.getMessage_needactionDirtyFlag())
            domain.setMessageNeedaction(model.getMessage_needaction());
        if(model.get__last_updateDirtyFlag())
            domain.setLastUpdate(model.get__last_update());
        if(model.getKanban_stateDirtyFlag())
            domain.setKanbanState(model.getKanban_state());
        if(model.getMessage_main_attachment_idDirtyFlag())
            domain.setMessageMainAttachmentId(model.getMessage_main_attachment_id());
        if(model.getMessage_channel_idsDirtyFlag())
            domain.setMessageChannelIds(model.getMessage_channel_ids());
        if(model.getReferredDirtyFlag())
            domain.setReferred(model.getReferred());
        if(model.getProbabilityDirtyFlag())
            domain.setProbability(model.getProbability());
        if(model.getDate_action_lastDirtyFlag())
            domain.setDateActionLast(model.getDate_action_last());
        if(model.getMessage_has_error_counterDirtyFlag())
            domain.setMessageHasErrorCounter(model.getMessage_has_error_counter());
        if(model.getDate_openDirtyFlag())
            domain.setDateOpen(model.getDate_open());
        if(model.getPhoneDirtyFlag())
            domain.setPhone(model.getPhone());
        if(model.getMessage_is_followerDirtyFlag())
            domain.setMessageIsFollower(model.getMessage_is_follower());
        if(model.getDay_openDirtyFlag())
            domain.setDayOpen(model.getDay_open());
        if(model.getActivity_summaryDirtyFlag())
            domain.setActivitySummary(model.getActivity_summary());
        if(model.getSale_numberDirtyFlag())
            domain.setSaleNumber(model.getSale_number());
        if(model.getStreet2DirtyFlag())
            domain.setStreet2(model.getStreet2());
        if(model.getOrder_idsDirtyFlag())
            domain.setOrderIds(model.getOrder_ids());
        if(model.getMessage_follower_idsDirtyFlag())
            domain.setMessageFollowerIds(model.getMessage_follower_ids());
        if(model.getStreetDirtyFlag())
            domain.setStreet(model.getStreet());
        if(model.getColorDirtyFlag())
            domain.setColor(model.getColor());
        if(model.getWrite_dateDirtyFlag())
            domain.setWriteDate(model.getWrite_date());
        if(model.getActiveDirtyFlag())
            domain.setActive(model.getActive());
        if(model.getDisplay_nameDirtyFlag())
            domain.setDisplayName(model.getDisplay_name());
        if(model.getTag_idsDirtyFlag())
            domain.setTagIds(model.getTag_ids());
        if(model.getIs_blacklistedDirtyFlag())
            domain.setIsBlacklisted(model.getIs_blacklisted());
        if(model.getMessage_needaction_counterDirtyFlag())
            domain.setMessageNeedactionCounter(model.getMessage_needaction_counter());
        if(model.getPriorityDirtyFlag())
            domain.setPriority(model.getPriority());
        if(model.getUser_id_textDirtyFlag())
            domain.setUserIdText(model.getUser_id_text());
        if(model.getSource_id_textDirtyFlag())
            domain.setSourceIdText(model.getSource_id_text());
        if(model.getPartner_address_nameDirtyFlag())
            domain.setPartnerAddressName(model.getPartner_address_name());
        if(model.getMedium_id_textDirtyFlag())
            domain.setMediumIdText(model.getMedium_id_text());
        if(model.getCompany_currencyDirtyFlag())
            domain.setCompanyCurrency(model.getCompany_currency());
        if(model.getTeam_id_textDirtyFlag())
            domain.setTeamIdText(model.getTeam_id_text());
        if(model.getPartner_address_mobileDirtyFlag())
            domain.setPartnerAddressMobile(model.getPartner_address_mobile());
        if(model.getWrite_uid_textDirtyFlag())
            domain.setWriteUidText(model.getWrite_uid_text());
        if(model.getCreate_uid_textDirtyFlag())
            domain.setCreateUidText(model.getCreate_uid_text());
        if(model.getUser_emailDirtyFlag())
            domain.setUserEmail(model.getUser_email());
        if(model.getUser_loginDirtyFlag())
            domain.setUserLogin(model.getUser_login());
        if(model.getPartner_address_phoneDirtyFlag())
            domain.setPartnerAddressPhone(model.getPartner_address_phone());
        if(model.getState_id_textDirtyFlag())
            domain.setStateIdText(model.getState_id_text());
        if(model.getCampaign_id_textDirtyFlag())
            domain.setCampaignIdText(model.getCampaign_id_text());
        if(model.getPartner_is_blacklistedDirtyFlag())
            domain.setPartnerIsBlacklisted(model.getPartner_is_blacklisted());
        if(model.getPartner_address_emailDirtyFlag())
            domain.setPartnerAddressEmail(model.getPartner_address_email());
        if(model.getStage_id_textDirtyFlag())
            domain.setStageIdText(model.getStage_id_text());
        if(model.getCompany_id_textDirtyFlag())
            domain.setCompanyIdText(model.getCompany_id_text());
        if(model.getCountry_id_textDirtyFlag())
            domain.setCountryIdText(model.getCountry_id_text());
        if(model.getTitle_textDirtyFlag())
            domain.setTitleText(model.getTitle_text());
        if(model.getLost_reason_textDirtyFlag())
            domain.setLostReasonText(model.getLost_reason_text());
        if(model.getLost_reasonDirtyFlag())
            domain.setLostReason(model.getLost_reason());
        if(model.getCreate_uidDirtyFlag())
            domain.setCreateUid(model.getCreate_uid());
        if(model.getWrite_uidDirtyFlag())
            domain.setWriteUid(model.getWrite_uid());
        if(model.getCompany_idDirtyFlag())
            domain.setCompanyId(model.getCompany_id());
        if(model.getUser_idDirtyFlag())
            domain.setUserId(model.getUser_id());
        if(model.getState_idDirtyFlag())
            domain.setStateId(model.getState_id());
        if(model.getMedium_idDirtyFlag())
            domain.setMediumId(model.getMedium_id());
        if(model.getStage_idDirtyFlag())
            domain.setStageId(model.getStage_id());
        if(model.getSource_idDirtyFlag())
            domain.setSourceId(model.getSource_id());
        if(model.getCountry_idDirtyFlag())
            domain.setCountryId(model.getCountry_id());
        if(model.getCampaign_idDirtyFlag())
            domain.setCampaignId(model.getCampaign_id());
        if(model.getPartner_idDirtyFlag())
            domain.setPartnerId(model.getPartner_id());
        if(model.getTeam_idDirtyFlag())
            domain.setTeamId(model.getTeam_id());
        if(model.getTitleDirtyFlag())
            domain.setTitle(model.getTitle());
        return domain ;
    }

}

    



