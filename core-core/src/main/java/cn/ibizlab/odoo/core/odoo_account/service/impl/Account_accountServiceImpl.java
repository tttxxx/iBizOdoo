package cn.ibizlab.odoo.core.odoo_account.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_account.domain.Account_account;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_accountSearchContext;
import cn.ibizlab.odoo.core.odoo_account.service.IAccount_accountService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;



import cn.ibizlab.odoo.core.odoo_account.client.account_accountOdooClient;
import cn.ibizlab.odoo.core.odoo_account.clientmodel.account_accountClientModel;

/**
 * 实体[科目] 服务对象接口实现
 */
@Slf4j
@Service
public class Account_accountServiceImpl implements IAccount_accountService {

    @Autowired
    account_accountOdooClient account_accountOdooClient;


    @Override
    public Account_account get(Integer id) {
        account_accountClientModel clientModel = new account_accountClientModel();
        clientModel.setId(id);
		account_accountOdooClient.get(clientModel);
        Account_account et = convert2Domain(clientModel,null);
        if(et==null){
            et=new Account_account();
            et.setId(id);
        }
        else{
        }

        return  et;
    }

    @Override
    public boolean update(Account_account et) {
        account_accountClientModel clientModel = convert2Model(et,null);
		account_accountOdooClient.update(clientModel);
        Account_account rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }
    
    public void updateBatch(List<Account_account> list){
    }

    @Override
    public boolean remove(Integer id) {
        account_accountClientModel clientModel = new account_accountClientModel();
        clientModel.setId(id);
		account_accountOdooClient.remove(clientModel);
        return true; 
    }

    public void removeBatch(Collection<Integer> idList){
    }

    @Override
    public boolean create(Account_account et) {
        account_accountClientModel clientModel = convert2Model(et,null);
		account_accountOdooClient.create(clientModel);
        Account_account rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Account_account> list){
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Account_account> searchDefault(Account_accountSearchContext context) {
        List<Account_account> list = new ArrayList<Account_account>();
        Page<account_accountClientModel> clientModelList = account_accountOdooClient.search(context);
        for(account_accountClientModel clientModel : clientModelList){
            list.add( this.convert2Domain(clientModel,null));
        }
        return new PageImpl<Account_account>(list, context.getPageable(), clientModelList.getTotalElements());
    }

    public account_accountClientModel convert2Model(Account_account domain , account_accountClientModel model) {
        if(domain == null)
            return model ;
        if(model==null){
            model = new account_accountClientModel();

        }
        if((Boolean) domain.getExtensionparams().get("dirtyflagenable")){
            if((Boolean) domain.getExtensionparams().get("iddirtyflag"))
                model.setId(domain.getId());
            if((Boolean) domain.getExtensionparams().get("opening_debitdirtyflag"))
                model.setOpening_debit(domain.getOpeningDebit());
            if((Boolean) domain.getExtensionparams().get("notedirtyflag"))
                model.setNote(domain.getNote());
            if((Boolean) domain.getExtensionparams().get("opening_creditdirtyflag"))
                model.setOpening_credit(domain.getOpeningCredit());
            if((Boolean) domain.getExtensionparams().get("create_datedirtyflag"))
                model.setCreate_date(domain.getCreateDate());
            if((Boolean) domain.getExtensionparams().get("codedirtyflag"))
                model.setCode(domain.getCode());
            if((Boolean) domain.getExtensionparams().get("tax_idsdirtyflag"))
                model.setTax_ids(domain.getTaxIds());
            if((Boolean) domain.getExtensionparams().get("write_datedirtyflag"))
                model.setWrite_date(domain.getWriteDate());
            if((Boolean) domain.getExtensionparams().get("deprecateddirtyflag"))
                model.setDeprecated(domain.getDeprecated());
            if((Boolean) domain.getExtensionparams().get("display_namedirtyflag"))
                model.setDisplay_name(domain.getDisplayName());
            if((Boolean) domain.getExtensionparams().get("last_time_entries_checkeddirtyflag"))
                model.setLast_time_entries_checked(domain.getLastTimeEntriesChecked());
            if((Boolean) domain.getExtensionparams().get("reconciledirtyflag"))
                model.setReconcile(domain.getReconcile());
            if((Boolean) domain.getExtensionparams().get("tag_idsdirtyflag"))
                model.setTag_ids(domain.getTagIds());
            if((Boolean) domain.getExtensionparams().get("namedirtyflag"))
                model.setName(domain.getName());
            if((Boolean) domain.getExtensionparams().get("__last_updatedirtyflag"))
                model.set__last_update(domain.getLastUpdate());
            if((Boolean) domain.getExtensionparams().get("internal_typedirtyflag"))
                model.setInternal_type(domain.getInternalType());
            if((Boolean) domain.getExtensionparams().get("create_uid_textdirtyflag"))
                model.setCreate_uid_text(domain.getCreateUidText());
            if((Boolean) domain.getExtensionparams().get("currency_id_textdirtyflag"))
                model.setCurrency_id_text(domain.getCurrencyIdText());
            if((Boolean) domain.getExtensionparams().get("internal_groupdirtyflag"))
                model.setInternal_group(domain.getInternalGroup());
            if((Boolean) domain.getExtensionparams().get("group_id_textdirtyflag"))
                model.setGroup_id_text(domain.getGroupIdText());
            if((Boolean) domain.getExtensionparams().get("write_uid_textdirtyflag"))
                model.setWrite_uid_text(domain.getWriteUidText());
            if((Boolean) domain.getExtensionparams().get("company_id_textdirtyflag"))
                model.setCompany_id_text(domain.getCompanyIdText());
            if((Boolean) domain.getExtensionparams().get("user_type_id_textdirtyflag"))
                model.setUser_type_id_text(domain.getUserTypeIdText());
            if((Boolean) domain.getExtensionparams().get("company_iddirtyflag"))
                model.setCompany_id(domain.getCompanyId());
            if((Boolean) domain.getExtensionparams().get("group_iddirtyflag"))
                model.setGroup_id(domain.getGroupId());
            if((Boolean) domain.getExtensionparams().get("user_type_iddirtyflag"))
                model.setUser_type_id(domain.getUserTypeId());
            if((Boolean) domain.getExtensionparams().get("currency_iddirtyflag"))
                model.setCurrency_id(domain.getCurrencyId());
            if((Boolean) domain.getExtensionparams().get("write_uiddirtyflag"))
                model.setWrite_uid(domain.getWriteUid());
            if((Boolean) domain.getExtensionparams().get("create_uiddirtyflag"))
                model.setCreate_uid(domain.getCreateUid());
            domain.getFocusNull().clear();
        }
        return model ;
    }

    public Account_account convert2Domain( account_accountClientModel model ,Account_account domain) {
        if(model == null)
            return domain ;
        if(domain == null){
            domain = new Account_account();
        }

        if(model.getIdDirtyFlag())
            domain.setId(model.getId());
        if(model.getOpening_debitDirtyFlag())
            domain.setOpeningDebit(model.getOpening_debit());
        if(model.getNoteDirtyFlag())
            domain.setNote(model.getNote());
        if(model.getOpening_creditDirtyFlag())
            domain.setOpeningCredit(model.getOpening_credit());
        if(model.getCreate_dateDirtyFlag())
            domain.setCreateDate(model.getCreate_date());
        if(model.getCodeDirtyFlag())
            domain.setCode(model.getCode());
        if(model.getTax_idsDirtyFlag())
            domain.setTaxIds(model.getTax_ids());
        if(model.getWrite_dateDirtyFlag())
            domain.setWriteDate(model.getWrite_date());
        if(model.getDeprecatedDirtyFlag())
            domain.setDeprecated(model.getDeprecated());
        if(model.getDisplay_nameDirtyFlag())
            domain.setDisplayName(model.getDisplay_name());
        if(model.getLast_time_entries_checkedDirtyFlag())
            domain.setLastTimeEntriesChecked(model.getLast_time_entries_checked());
        if(model.getReconcileDirtyFlag())
            domain.setReconcile(model.getReconcile());
        if(model.getTag_idsDirtyFlag())
            domain.setTagIds(model.getTag_ids());
        if(model.getNameDirtyFlag())
            domain.setName(model.getName());
        if(model.get__last_updateDirtyFlag())
            domain.setLastUpdate(model.get__last_update());
        if(model.getInternal_typeDirtyFlag())
            domain.setInternalType(model.getInternal_type());
        if(model.getCreate_uid_textDirtyFlag())
            domain.setCreateUidText(model.getCreate_uid_text());
        if(model.getCurrency_id_textDirtyFlag())
            domain.setCurrencyIdText(model.getCurrency_id_text());
        if(model.getInternal_groupDirtyFlag())
            domain.setInternalGroup(model.getInternal_group());
        if(model.getGroup_id_textDirtyFlag())
            domain.setGroupIdText(model.getGroup_id_text());
        if(model.getWrite_uid_textDirtyFlag())
            domain.setWriteUidText(model.getWrite_uid_text());
        if(model.getCompany_id_textDirtyFlag())
            domain.setCompanyIdText(model.getCompany_id_text());
        if(model.getUser_type_id_textDirtyFlag())
            domain.setUserTypeIdText(model.getUser_type_id_text());
        if(model.getCompany_idDirtyFlag())
            domain.setCompanyId(model.getCompany_id());
        if(model.getGroup_idDirtyFlag())
            domain.setGroupId(model.getGroup_id());
        if(model.getUser_type_idDirtyFlag())
            domain.setUserTypeId(model.getUser_type_id());
        if(model.getCurrency_idDirtyFlag())
            domain.setCurrencyId(model.getCurrency_id());
        if(model.getWrite_uidDirtyFlag())
            domain.setWriteUid(model.getWrite_uid());
        if(model.getCreate_uidDirtyFlag())
            domain.setCreateUid(model.getCreate_uid());
        return domain ;
    }

}

    



