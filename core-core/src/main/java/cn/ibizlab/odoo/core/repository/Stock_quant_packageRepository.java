package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Stock_quant_package;
import cn.ibizlab.odoo.core.odoo_stock.filter.Stock_quant_packageSearchContext;

/**
 * 实体 [包裹] 存储对象
 */
public interface Stock_quant_packageRepository extends Repository<Stock_quant_package> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Stock_quant_package> searchDefault(Stock_quant_packageSearchContext context);

    Stock_quant_package convert2PO(cn.ibizlab.odoo.core.odoo_stock.domain.Stock_quant_package domain , Stock_quant_package po) ;

    cn.ibizlab.odoo.core.odoo_stock.domain.Stock_quant_package convert2Domain( Stock_quant_package po ,cn.ibizlab.odoo.core.odoo_stock.domain.Stock_quant_package domain) ;

}
