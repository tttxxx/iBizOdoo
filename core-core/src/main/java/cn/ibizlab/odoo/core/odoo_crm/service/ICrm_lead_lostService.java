package cn.ibizlab.odoo.core.odoo_crm.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_crm.domain.Crm_lead_lost;
import cn.ibizlab.odoo.core.odoo_crm.filter.Crm_lead_lostSearchContext;


/**
 * 实体[Crm_lead_lost] 服务对象接口
 */
public interface ICrm_lead_lostService{

    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    boolean update(Crm_lead_lost et) ;
    void updateBatch(List<Crm_lead_lost> list) ;
    Crm_lead_lost get(Integer key) ;
    boolean create(Crm_lead_lost et) ;
    void createBatch(List<Crm_lead_lost> list) ;
    Page<Crm_lead_lost> searchDefault(Crm_lead_lostSearchContext context) ;

}



