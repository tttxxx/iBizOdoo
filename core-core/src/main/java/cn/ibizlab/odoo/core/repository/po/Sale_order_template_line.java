package cn.ibizlab.odoo.core.repository.po ;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.math.BigInteger;

import cn.ibizlab.odoo.core.odoo_sale.filter.Sale_order_template_lineSearchContext;

/**
 * 实体 [报价单模板行] 存储模型
 */
public interface Sale_order_template_line{

    /**
     * 创建时间
     */
    Timestamp getCreate_date();

    void setCreate_date(Timestamp create_date);

    /**
     * 获取 [创建时间]脏标记
     */
    boolean getCreate_dateDirtyFlag();

    /**
     * ID
     */
    Integer getId();

    void setId(Integer id);

    /**
     * 获取 [ID]脏标记
     */
    boolean getIdDirtyFlag();

    /**
     * 序号
     */
    Integer getSequence();

    void setSequence(Integer sequence);

    /**
     * 获取 [序号]脏标记
     */
    boolean getSequenceDirtyFlag();

    /**
     * 数量
     */
    Double getProduct_uom_qty();

    void setProduct_uom_qty(Double product_uom_qty);

    /**
     * 获取 [数量]脏标记
     */
    boolean getProduct_uom_qtyDirtyFlag();

    /**
     * 说明
     */
    String getName();

    void setName(String name);

    /**
     * 获取 [说明]脏标记
     */
    boolean getNameDirtyFlag();

    /**
     * 显示名称
     */
    String getDisplay_name();

    void setDisplay_name(String display_name);

    /**
     * 获取 [显示名称]脏标记
     */
    boolean getDisplay_nameDirtyFlag();

    /**
     * 最后更新时间
     */
    Timestamp getWrite_date();

    void setWrite_date(Timestamp write_date);

    /**
     * 获取 [最后更新时间]脏标记
     */
    boolean getWrite_dateDirtyFlag();

    /**
     * 折扣(%)
     */
    Double getDiscount();

    void setDiscount(Double discount);

    /**
     * 获取 [折扣(%)]脏标记
     */
    boolean getDiscountDirtyFlag();

    /**
     * 最后修改日
     */
    Timestamp get__last_update();

    void set__last_update(Timestamp __last_update);

    /**
     * 获取 [最后修改日]脏标记
     */
    boolean get__last_updateDirtyFlag();

    /**
     * 单价
     */
    Double getPrice_unit();

    void setPrice_unit(Double price_unit);

    /**
     * 获取 [单价]脏标记
     */
    boolean getPrice_unitDirtyFlag();

    /**
     * 显示类型
     */
    String getDisplay_type();

    void setDisplay_type(String display_type);

    /**
     * 获取 [显示类型]脏标记
     */
    boolean getDisplay_typeDirtyFlag();

    /**
     * 创建人
     */
    String getCreate_uid_text();

    void setCreate_uid_text(String create_uid_text);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uid_textDirtyFlag();

    /**
     * 产品
     */
    String getProduct_id_text();

    void setProduct_id_text(String product_id_text);

    /**
     * 获取 [产品]脏标记
     */
    boolean getProduct_id_textDirtyFlag();

    /**
     * 最后更新人
     */
    String getWrite_uid_text();

    void setWrite_uid_text(String write_uid_text);

    /**
     * 获取 [最后更新人]脏标记
     */
    boolean getWrite_uid_textDirtyFlag();

    /**
     * 计量单位
     */
    String getProduct_uom_id_text();

    void setProduct_uom_id_text(String product_uom_id_text);

    /**
     * 获取 [计量单位]脏标记
     */
    boolean getProduct_uom_id_textDirtyFlag();

    /**
     * 报价单模板参考
     */
    String getSale_order_template_id_text();

    void setSale_order_template_id_text(String sale_order_template_id_text);

    /**
     * 获取 [报价单模板参考]脏标记
     */
    boolean getSale_order_template_id_textDirtyFlag();

    /**
     * 计量单位
     */
    Integer getProduct_uom_id();

    void setProduct_uom_id(Integer product_uom_id);

    /**
     * 获取 [计量单位]脏标记
     */
    boolean getProduct_uom_idDirtyFlag();

    /**
     * 创建人
     */
    Integer getCreate_uid();

    void setCreate_uid(Integer create_uid);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uidDirtyFlag();

    /**
     * 产品
     */
    Integer getProduct_id();

    void setProduct_id(Integer product_id);

    /**
     * 获取 [产品]脏标记
     */
    boolean getProduct_idDirtyFlag();

    /**
     * 报价单模板参考
     */
    Integer getSale_order_template_id();

    void setSale_order_template_id(Integer sale_order_template_id);

    /**
     * 获取 [报价单模板参考]脏标记
     */
    boolean getSale_order_template_idDirtyFlag();

    /**
     * 最后更新人
     */
    Integer getWrite_uid();

    void setWrite_uid(Integer write_uid);

    /**
     * 获取 [最后更新人]脏标记
     */
    boolean getWrite_uidDirtyFlag();

}
