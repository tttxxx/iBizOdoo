package cn.ibizlab.odoo.core.repository.po ;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.math.BigInteger;

import cn.ibizlab.odoo.core.odoo_fleet.filter.Fleet_vehicle_log_contractSearchContext;

/**
 * 实体 [车辆合同信息] 存储模型
 */
public interface Fleet_vehicle_log_contract{

    /**
     * 下一活动摘要
     */
    String getActivity_summary();

    void setActivity_summary(String activity_summary);

    /**
     * 获取 [下一活动摘要]脏标记
     */
    boolean getActivity_summaryDirtyFlag();

    /**
     * 合同参考
     */
    String getIns_ref();

    void setIns_ref(String ins_ref);

    /**
     * 获取 [合同参考]脏标记
     */
    boolean getIns_refDirtyFlag();

    /**
     * 有效
     */
    String getActive();

    void setActive(String active);

    /**
     * 获取 [有效]脏标记
     */
    boolean getActiveDirtyFlag();

    /**
     * 指标性成本总计
     */
    Double getSum_cost();

    void setSum_cost(Double sum_cost);

    /**
     * 获取 [指标性成本总计]脏标记
     */
    boolean getSum_costDirtyFlag();

    /**
     * 条款和条件
     */
    String getNotes();

    void setNotes(String notes);

    /**
     * 获取 [条款和条件]脏标记
     */
    boolean getNotesDirtyFlag();

    /**
     * 显示名称
     */
    String getDisplay_name();

    void setDisplay_name(String display_name);

    /**
     * 获取 [显示名称]脏标记
     */
    boolean getDisplay_nameDirtyFlag();

    /**
     * 行动数量
     */
    Integer getMessage_needaction_counter();

    void setMessage_needaction_counter(Integer message_needaction_counter);

    /**
     * 获取 [行动数量]脏标记
     */
    boolean getMessage_needaction_counterDirtyFlag();

    /**
     * 合同到期日期
     */
    Timestamp getExpiration_date();

    void setExpiration_date(Timestamp expiration_date);

    /**
     * 获取 [合同到期日期]脏标记
     */
    boolean getExpiration_dateDirtyFlag();

    /**
     * 关注者(业务伙伴)
     */
    String getMessage_partner_ids();

    void setMessage_partner_ids(String message_partner_ids);

    /**
     * 获取 [关注者(业务伙伴)]脏标记
     */
    boolean getMessage_partner_idsDirtyFlag();

    /**
     * 消息递送错误
     */
    String getMessage_has_error();

    void setMessage_has_error(String message_has_error);

    /**
     * 获取 [消息递送错误]脏标记
     */
    boolean getMessage_has_errorDirtyFlag();

    /**
     * 下一活动类型
     */
    Integer getActivity_type_id();

    void setActivity_type_id(Integer activity_type_id);

    /**
     * 获取 [下一活动类型]脏标记
     */
    boolean getActivity_type_idDirtyFlag();

    /**
     * 未读消息计数器
     */
    Integer getMessage_unread_counter();

    void setMessage_unread_counter(Integer message_unread_counter);

    /**
     * 获取 [未读消息计数器]脏标记
     */
    boolean getMessage_unread_counterDirtyFlag();

    /**
     * 责任用户
     */
    Integer getActivity_user_id();

    void setActivity_user_id(Integer activity_user_id);

    /**
     * 获取 [责任用户]脏标记
     */
    boolean getActivity_user_idDirtyFlag();

    /**
     * 警告日期
     */
    Integer getDays_left();

    void setDays_left(Integer days_left);

    /**
     * 获取 [警告日期]脏标记
     */
    boolean getDays_leftDirtyFlag();

    /**
     * 关注者(渠道)
     */
    String getMessage_channel_ids();

    void setMessage_channel_ids(String message_channel_ids);

    /**
     * 获取 [关注者(渠道)]脏标记
     */
    boolean getMessage_channel_idsDirtyFlag();

    /**
     * 关注者
     */
    String getMessage_follower_ids();

    void setMessage_follower_ids(String message_follower_ids);

    /**
     * 获取 [关注者]脏标记
     */
    boolean getMessage_follower_idsDirtyFlag();

    /**
     * 创建时的里程表
     */
    Double getOdometer();

    void setOdometer(Double odometer);

    /**
     * 获取 [创建时的里程表]脏标记
     */
    boolean getOdometerDirtyFlag();

    /**
     * 状态
     */
    String getState();

    void setState(String state);

    /**
     * 获取 [状态]脏标记
     */
    boolean getStateDirtyFlag();

    /**
     * 包括服务
     */
    String getCost_ids();

    void setCost_ids(String cost_ids);

    /**
     * 获取 [包括服务]脏标记
     */
    boolean getCost_idsDirtyFlag();

    /**
     * 未读消息
     */
    String getMessage_unread();

    void setMessage_unread(String message_unread);

    /**
     * 获取 [未读消息]脏标记
     */
    boolean getMessage_unreadDirtyFlag();

    /**
     * 经常成本数量
     */
    Double getCost_generated();

    void setCost_generated(Double cost_generated);

    /**
     * 获取 [经常成本数量]脏标记
     */
    boolean getCost_generatedDirtyFlag();

    /**
     * 最后修改日
     */
    Timestamp get__last_update();

    void set__last_update(Timestamp __last_update);

    /**
     * 获取 [最后修改日]脏标记
     */
    boolean get__last_updateDirtyFlag();

    /**
     * 网站消息
     */
    String getWebsite_message_ids();

    void setWebsite_message_ids(String website_message_ids);

    /**
     * 获取 [网站消息]脏标记
     */
    boolean getWebsite_message_idsDirtyFlag();

    /**
     * 创建时间
     */
    Timestamp getCreate_date();

    void setCreate_date(Timestamp create_date);

    /**
     * 获取 [创建时间]脏标记
     */
    boolean getCreate_dateDirtyFlag();

    /**
     * 消息
     */
    String getMessage_ids();

    void setMessage_ids(String message_ids);

    /**
     * 获取 [消息]脏标记
     */
    boolean getMessage_idsDirtyFlag();

    /**
     * 关注者
     */
    String getMessage_is_follower();

    void setMessage_is_follower(String message_is_follower);

    /**
     * 获取 [关注者]脏标记
     */
    boolean getMessage_is_followerDirtyFlag();

    /**
     * ID
     */
    Integer getId();

    void setId(Integer id);

    /**
     * 获取 [ID]脏标记
     */
    boolean getIdDirtyFlag();

    /**
     * 最后更新时间
     */
    Timestamp getWrite_date();

    void setWrite_date(Timestamp write_date);

    /**
     * 获取 [最后更新时间]脏标记
     */
    boolean getWrite_dateDirtyFlag();

    /**
     * 需要激活
     */
    String getMessage_needaction();

    void setMessage_needaction(String message_needaction);

    /**
     * 获取 [需要激活]脏标记
     */
    boolean getMessage_needactionDirtyFlag();

    /**
     * 附件
     */
    Integer getMessage_main_attachment_id();

    void setMessage_main_attachment_id(Integer message_main_attachment_id);

    /**
     * 获取 [附件]脏标记
     */
    boolean getMessage_main_attachment_idDirtyFlag();

    /**
     * 错误数
     */
    Integer getMessage_has_error_counter();

    void setMessage_has_error_counter(Integer message_has_error_counter);

    /**
     * 获取 [错误数]脏标记
     */
    boolean getMessage_has_error_counterDirtyFlag();

    /**
     * 已发生费用
     */
    String getGenerated_cost_ids();

    void setGenerated_cost_ids(String generated_cost_ids);

    /**
     * 获取 [已发生费用]脏标记
     */
    boolean getGenerated_cost_idsDirtyFlag();

    /**
     * 名称
     */
    String getName();

    void setName(String name);

    /**
     * 获取 [名称]脏标记
     */
    boolean getNameDirtyFlag();

    /**
     * 经常成本频率
     */
    String getCost_frequency();

    void setCost_frequency(String cost_frequency);

    /**
     * 获取 [经常成本频率]脏标记
     */
    boolean getCost_frequencyDirtyFlag();

    /**
     * 合同开始日期
     */
    Timestamp getStart_date();

    void setStart_date(Timestamp start_date);

    /**
     * 获取 [合同开始日期]脏标记
     */
    boolean getStart_dateDirtyFlag();

    /**
     * 活动状态
     */
    String getActivity_state();

    void setActivity_state(String activity_state);

    /**
     * 获取 [活动状态]脏标记
     */
    boolean getActivity_stateDirtyFlag();

    /**
     * 附件数量
     */
    Integer getMessage_attachment_count();

    void setMessage_attachment_count(Integer message_attachment_count);

    /**
     * 获取 [附件数量]脏标记
     */
    boolean getMessage_attachment_countDirtyFlag();

    /**
     * 下一活动截止日期
     */
    Timestamp getActivity_date_deadline();

    void setActivity_date_deadline(Timestamp activity_date_deadline);

    /**
     * 获取 [下一活动截止日期]脏标记
     */
    boolean getActivity_date_deadlineDirtyFlag();

    /**
     * 活动
     */
    String getActivity_ids();

    void setActivity_ids(String activity_ids);

    /**
     * 获取 [活动]脏标记
     */
    boolean getActivity_idsDirtyFlag();

    /**
     * 车辆
     */
    Integer getVehicle_id();

    void setVehicle_id(Integer vehicle_id);

    /**
     * 获取 [车辆]脏标记
     */
    boolean getVehicle_idDirtyFlag();

    /**
     * 自动生成
     */
    String getAuto_generated();

    void setAuto_generated(String auto_generated);

    /**
     * 获取 [自动生成]脏标记
     */
    boolean getAuto_generatedDirtyFlag();

    /**
     * 创建人
     */
    String getCreate_uid_text();

    void setCreate_uid_text(String create_uid_text);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uid_textDirtyFlag();

    /**
     * 里程表
     */
    Integer getOdometer_id();

    void setOdometer_id(Integer odometer_id);

    /**
     * 获取 [里程表]脏标记
     */
    boolean getOdometer_idDirtyFlag();

    /**
     * 费用所属类别
     */
    String getCost_type();

    void setCost_type(String cost_type);

    /**
     * 获取 [费用所属类别]脏标记
     */
    boolean getCost_typeDirtyFlag();

    /**
     * 上级
     */
    Integer getParent_id();

    void setParent_id(Integer parent_id);

    /**
     * 获取 [上级]脏标记
     */
    boolean getParent_idDirtyFlag();

    /**
     * 驾驶员
     */
    String getPurchaser_id_text();

    void setPurchaser_id_text(String purchaser_id_text);

    /**
     * 获取 [驾驶员]脏标记
     */
    boolean getPurchaser_id_textDirtyFlag();

    /**
     * 类型
     */
    Integer getCost_subtype_id();

    void setCost_subtype_id(Integer cost_subtype_id);

    /**
     * 获取 [类型]脏标记
     */
    boolean getCost_subtype_idDirtyFlag();

    /**
     * 总额
     */
    Double getCost_amount();

    void setCost_amount(Double cost_amount);

    /**
     * 获取 [总额]脏标记
     */
    boolean getCost_amountDirtyFlag();

    /**
     * 总价
     */
    Double getAmount();

    void setAmount(Double amount);

    /**
     * 获取 [总价]脏标记
     */
    boolean getAmountDirtyFlag();

    /**
     * 供应商
     */
    String getInsurer_id_text();

    void setInsurer_id_text(String insurer_id_text);

    /**
     * 获取 [供应商]脏标记
     */
    boolean getInsurer_id_textDirtyFlag();

    /**
     * 成本
     */
    String getCost_id_text();

    void setCost_id_text(String cost_id_text);

    /**
     * 获取 [成本]脏标记
     */
    boolean getCost_id_textDirtyFlag();

    /**
     * 成本说明
     */
    String getDescription();

    void setDescription(String description);

    /**
     * 获取 [成本说明]脏标记
     */
    boolean getDescriptionDirtyFlag();

    /**
     * 负责
     */
    String getUser_id_text();

    void setUser_id_text(String user_id_text);

    /**
     * 获取 [负责]脏标记
     */
    boolean getUser_id_textDirtyFlag();

    /**
     * 日期
     */
    Timestamp getDate();

    void setDate(Timestamp date);

    /**
     * 获取 [日期]脏标记
     */
    boolean getDateDirtyFlag();

    /**
     * 单位
     */
    String getOdometer_unit();

    void setOdometer_unit(String odometer_unit);

    /**
     * 获取 [单位]脏标记
     */
    boolean getOdometer_unitDirtyFlag();

    /**
     * 合同
     */
    Integer getContract_id();

    void setContract_id(Integer contract_id);

    /**
     * 获取 [合同]脏标记
     */
    boolean getContract_idDirtyFlag();

    /**
     * 最后更新人
     */
    String getWrite_uid_text();

    void setWrite_uid_text(String write_uid_text);

    /**
     * 获取 [最后更新人]脏标记
     */
    boolean getWrite_uid_textDirtyFlag();

    /**
     * 最后更新人
     */
    Integer getWrite_uid();

    void setWrite_uid(Integer write_uid);

    /**
     * 获取 [最后更新人]脏标记
     */
    boolean getWrite_uidDirtyFlag();

    /**
     * 成本
     */
    Integer getCost_id();

    void setCost_id(Integer cost_id);

    /**
     * 获取 [成本]脏标记
     */
    boolean getCost_idDirtyFlag();

    /**
     * 供应商
     */
    Integer getInsurer_id();

    void setInsurer_id(Integer insurer_id);

    /**
     * 获取 [供应商]脏标记
     */
    boolean getInsurer_idDirtyFlag();

    /**
     * 负责
     */
    Integer getUser_id();

    void setUser_id(Integer user_id);

    /**
     * 获取 [负责]脏标记
     */
    boolean getUser_idDirtyFlag();

    /**
     * 创建人
     */
    Integer getCreate_uid();

    void setCreate_uid(Integer create_uid);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uidDirtyFlag();

    /**
     * 驾驶员
     */
    Integer getPurchaser_id();

    void setPurchaser_id(Integer purchaser_id);

    /**
     * 获取 [驾驶员]脏标记
     */
    boolean getPurchaser_idDirtyFlag();

}
