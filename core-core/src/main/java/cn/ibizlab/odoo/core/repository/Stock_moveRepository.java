package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Stock_move;
import cn.ibizlab.odoo.core.odoo_stock.filter.Stock_moveSearchContext;

/**
 * 实体 [库存移动] 存储对象
 */
public interface Stock_moveRepository extends Repository<Stock_move> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Stock_move> searchDefault(Stock_moveSearchContext context);

    Stock_move convert2PO(cn.ibizlab.odoo.core.odoo_stock.domain.Stock_move domain , Stock_move po) ;

    cn.ibizlab.odoo.core.odoo_stock.domain.Stock_move convert2Domain( Stock_move po ,cn.ibizlab.odoo.core.odoo_stock.domain.Stock_move domain) ;

}
