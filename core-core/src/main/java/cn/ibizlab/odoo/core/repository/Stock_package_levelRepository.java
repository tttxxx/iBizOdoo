package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Stock_package_level;
import cn.ibizlab.odoo.core.odoo_stock.filter.Stock_package_levelSearchContext;

/**
 * 实体 [库存包装层级] 存储对象
 */
public interface Stock_package_levelRepository extends Repository<Stock_package_level> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Stock_package_level> searchDefault(Stock_package_levelSearchContext context);

    Stock_package_level convert2PO(cn.ibizlab.odoo.core.odoo_stock.domain.Stock_package_level domain , Stock_package_level po) ;

    cn.ibizlab.odoo.core.odoo_stock.domain.Stock_package_level convert2Domain( Stock_package_level po ,cn.ibizlab.odoo.core.odoo_stock.domain.Stock_package_level domain) ;

}
