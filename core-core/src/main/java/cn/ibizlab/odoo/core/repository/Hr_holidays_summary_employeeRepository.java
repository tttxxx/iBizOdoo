package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Hr_holidays_summary_employee;
import cn.ibizlab.odoo.core.odoo_hr.filter.Hr_holidays_summary_employeeSearchContext;

/**
 * 实体 [按员工的休假摘要报告] 存储对象
 */
public interface Hr_holidays_summary_employeeRepository extends Repository<Hr_holidays_summary_employee> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Hr_holidays_summary_employee> searchDefault(Hr_holidays_summary_employeeSearchContext context);

    Hr_holidays_summary_employee convert2PO(cn.ibizlab.odoo.core.odoo_hr.domain.Hr_holidays_summary_employee domain , Hr_holidays_summary_employee po) ;

    cn.ibizlab.odoo.core.odoo_hr.domain.Hr_holidays_summary_employee convert2Domain( Hr_holidays_summary_employee po ,cn.ibizlab.odoo.core.odoo_hr.domain.Hr_holidays_summary_employee domain) ;

}
