package cn.ibizlab.odoo.core.odoo_mro.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_mro.domain.Mro_pm_meter_interval;
import cn.ibizlab.odoo.core.odoo_mro.filter.Mro_pm_meter_intervalSearchContext;
import cn.ibizlab.odoo.core.odoo_mro.service.IMro_pm_meter_intervalService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;



import cn.ibizlab.odoo.core.odoo_mro.client.mro_pm_meter_intervalOdooClient;
import cn.ibizlab.odoo.core.odoo_mro.clientmodel.mro_pm_meter_intervalClientModel;

/**
 * 实体[Meter interval] 服务对象接口实现
 */
@Slf4j
@Service
public class Mro_pm_meter_intervalServiceImpl implements IMro_pm_meter_intervalService {

    @Autowired
    mro_pm_meter_intervalOdooClient mro_pm_meter_intervalOdooClient;


    @Override
    public boolean create(Mro_pm_meter_interval et) {
        mro_pm_meter_intervalClientModel clientModel = convert2Model(et,null);
		mro_pm_meter_intervalOdooClient.create(clientModel);
        Mro_pm_meter_interval rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Mro_pm_meter_interval> list){
    }

    @Override
    public Mro_pm_meter_interval get(Integer id) {
        mro_pm_meter_intervalClientModel clientModel = new mro_pm_meter_intervalClientModel();
        clientModel.setId(id);
		mro_pm_meter_intervalOdooClient.get(clientModel);
        Mro_pm_meter_interval et = convert2Domain(clientModel,null);
        if(et==null){
            et=new Mro_pm_meter_interval();
            et.setId(id);
        }
        else{
        }

        return  et;
    }

    @Override
    public boolean remove(Integer id) {
        mro_pm_meter_intervalClientModel clientModel = new mro_pm_meter_intervalClientModel();
        clientModel.setId(id);
		mro_pm_meter_intervalOdooClient.remove(clientModel);
        return true; 
    }

    public void removeBatch(Collection<Integer> idList){
    }

    @Override
    public boolean update(Mro_pm_meter_interval et) {
        mro_pm_meter_intervalClientModel clientModel = convert2Model(et,null);
		mro_pm_meter_intervalOdooClient.update(clientModel);
        Mro_pm_meter_interval rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }
    
    public void updateBatch(List<Mro_pm_meter_interval> list){
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Mro_pm_meter_interval> searchDefault(Mro_pm_meter_intervalSearchContext context) {
        List<Mro_pm_meter_interval> list = new ArrayList<Mro_pm_meter_interval>();
        Page<mro_pm_meter_intervalClientModel> clientModelList = mro_pm_meter_intervalOdooClient.search(context);
        for(mro_pm_meter_intervalClientModel clientModel : clientModelList){
            list.add( this.convert2Domain(clientModel,null));
        }
        return new PageImpl<Mro_pm_meter_interval>(list, context.getPageable(), clientModelList.getTotalElements());
    }

    public mro_pm_meter_intervalClientModel convert2Model(Mro_pm_meter_interval domain , mro_pm_meter_intervalClientModel model) {
        if(domain == null)
            return model ;
        if(model==null){
            model = new mro_pm_meter_intervalClientModel();

        }
        if((Boolean) domain.getExtensionparams().get("dirtyflagenable")){
            if((Boolean) domain.getExtensionparams().get("display_namedirtyflag"))
                model.setDisplay_name(domain.getDisplayName());
            if((Boolean) domain.getExtensionparams().get("iddirtyflag"))
                model.setId(domain.getId());
            if((Boolean) domain.getExtensionparams().get("namedirtyflag"))
                model.setName(domain.getName());
            if((Boolean) domain.getExtensionparams().get("__last_updatedirtyflag"))
                model.set__last_update(domain.getLastUpdate());
            if((Boolean) domain.getExtensionparams().get("interval_mindirtyflag"))
                model.setInterval_min(domain.getIntervalMin());
            if((Boolean) domain.getExtensionparams().get("write_datedirtyflag"))
                model.setWrite_date(domain.getWriteDate());
            if((Boolean) domain.getExtensionparams().get("create_datedirtyflag"))
                model.setCreate_date(domain.getCreateDate());
            if((Boolean) domain.getExtensionparams().get("interval_maxdirtyflag"))
                model.setInterval_max(domain.getIntervalMax());
            if((Boolean) domain.getExtensionparams().get("create_uid_textdirtyflag"))
                model.setCreate_uid_text(domain.getCreateUidText());
            if((Boolean) domain.getExtensionparams().get("write_uid_textdirtyflag"))
                model.setWrite_uid_text(domain.getWriteUidText());
            if((Boolean) domain.getExtensionparams().get("write_uiddirtyflag"))
                model.setWrite_uid(domain.getWriteUid());
            if((Boolean) domain.getExtensionparams().get("create_uiddirtyflag"))
                model.setCreate_uid(domain.getCreateUid());
            domain.getFocusNull().clear();
        }
        return model ;
    }

    public Mro_pm_meter_interval convert2Domain( mro_pm_meter_intervalClientModel model ,Mro_pm_meter_interval domain) {
        if(model == null)
            return domain ;
        if(domain == null){
            domain = new Mro_pm_meter_interval();
        }

        if(model.getDisplay_nameDirtyFlag())
            domain.setDisplayName(model.getDisplay_name());
        if(model.getIdDirtyFlag())
            domain.setId(model.getId());
        if(model.getNameDirtyFlag())
            domain.setName(model.getName());
        if(model.get__last_updateDirtyFlag())
            domain.setLastUpdate(model.get__last_update());
        if(model.getInterval_minDirtyFlag())
            domain.setIntervalMin(model.getInterval_min());
        if(model.getWrite_dateDirtyFlag())
            domain.setWriteDate(model.getWrite_date());
        if(model.getCreate_dateDirtyFlag())
            domain.setCreateDate(model.getCreate_date());
        if(model.getInterval_maxDirtyFlag())
            domain.setIntervalMax(model.getInterval_max());
        if(model.getCreate_uid_textDirtyFlag())
            domain.setCreateUidText(model.getCreate_uid_text());
        if(model.getWrite_uid_textDirtyFlag())
            domain.setWriteUidText(model.getWrite_uid_text());
        if(model.getWrite_uidDirtyFlag())
            domain.setWriteUid(model.getWrite_uid());
        if(model.getCreate_uidDirtyFlag())
            domain.setCreateUid(model.getCreate_uid());
        return domain ;
    }

}

    



