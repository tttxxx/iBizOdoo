package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Base_automation_lead_test;
import cn.ibizlab.odoo.core.odoo_base.filter.Base_automation_lead_testSearchContext;

/**
 * 实体 [自动化规则测试] 存储对象
 */
public interface Base_automation_lead_testRepository extends Repository<Base_automation_lead_test> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Base_automation_lead_test> searchDefault(Base_automation_lead_testSearchContext context);

    Base_automation_lead_test convert2PO(cn.ibizlab.odoo.core.odoo_base.domain.Base_automation_lead_test domain , Base_automation_lead_test po) ;

    cn.ibizlab.odoo.core.odoo_base.domain.Base_automation_lead_test convert2Domain( Base_automation_lead_test po ,cn.ibizlab.odoo.core.odoo_base.domain.Base_automation_lead_test domain) ;

}
