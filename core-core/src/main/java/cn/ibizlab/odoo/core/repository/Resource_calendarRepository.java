package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Resource_calendar;
import cn.ibizlab.odoo.core.odoo_resource.filter.Resource_calendarSearchContext;

/**
 * 实体 [资源工作时间] 存储对象
 */
public interface Resource_calendarRepository extends Repository<Resource_calendar> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Resource_calendar> searchDefault(Resource_calendarSearchContext context);

    Resource_calendar convert2PO(cn.ibizlab.odoo.core.odoo_resource.domain.Resource_calendar domain , Resource_calendar po) ;

    cn.ibizlab.odoo.core.odoo_resource.domain.Resource_calendar convert2Domain( Resource_calendar po ,cn.ibizlab.odoo.core.odoo_resource.domain.Resource_calendar domain) ;

}
