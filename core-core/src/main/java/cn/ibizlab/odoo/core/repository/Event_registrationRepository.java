package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Event_registration;
import cn.ibizlab.odoo.core.odoo_event.filter.Event_registrationSearchContext;

/**
 * 实体 [事件记录] 存储对象
 */
public interface Event_registrationRepository extends Repository<Event_registration> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Event_registration> searchDefault(Event_registrationSearchContext context);

    Event_registration convert2PO(cn.ibizlab.odoo.core.odoo_event.domain.Event_registration domain , Event_registration po) ;

    cn.ibizlab.odoo.core.odoo_event.domain.Event_registration convert2Domain( Event_registration po ,cn.ibizlab.odoo.core.odoo_event.domain.Event_registration domain) ;

}
