package cn.ibizlab.odoo.core.client.model;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigDecimal;
import java.math.BigInteger;

/**
 * Client 实体 [mro_pm_rule] 对象
 */
public interface Imro_pm_rule {

    /**
     * 获取 [有效]
     */
    public void setActive(String active);
    
    /**
     * 设置 [有效]
     */
    public String getActive();

    /**
     * 获取 [有效]脏标记
     */
    public boolean getActiveDirtyFlag();
    /**
     * 获取 [Asset Category]
     */
    public void setCategory_id(Integer category_id);
    
    /**
     * 设置 [Asset Category]
     */
    public Integer getCategory_id();

    /**
     * 获取 [Asset Category]脏标记
     */
    public boolean getCategory_idDirtyFlag();
    /**
     * 获取 [Asset Category]
     */
    public void setCategory_id_text(String category_id_text);
    
    /**
     * 设置 [Asset Category]
     */
    public String getCategory_id_text();

    /**
     * 获取 [Asset Category]脏标记
     */
    public boolean getCategory_id_textDirtyFlag();
    /**
     * 获取 [创建时间]
     */
    public void setCreate_date(Timestamp create_date);
    
    /**
     * 设置 [创建时间]
     */
    public Timestamp getCreate_date();

    /**
     * 获取 [创建时间]脏标记
     */
    public boolean getCreate_dateDirtyFlag();
    /**
     * 获取 [创建人]
     */
    public void setCreate_uid(Integer create_uid);
    
    /**
     * 设置 [创建人]
     */
    public Integer getCreate_uid();

    /**
     * 获取 [创建人]脏标记
     */
    public boolean getCreate_uidDirtyFlag();
    /**
     * 获取 [创建人]
     */
    public void setCreate_uid_text(String create_uid_text);
    
    /**
     * 设置 [创建人]
     */
    public String getCreate_uid_text();

    /**
     * 获取 [创建人]脏标记
     */
    public boolean getCreate_uid_textDirtyFlag();
    /**
     * 获取 [显示名称]
     */
    public void setDisplay_name(String display_name);
    
    /**
     * 设置 [显示名称]
     */
    public String getDisplay_name();

    /**
     * 获取 [显示名称]脏标记
     */
    public boolean getDisplay_nameDirtyFlag();
    /**
     * 获取 [Planning horizon (months)]
     */
    public void setHorizon(Double horizon);
    
    /**
     * 设置 [Planning horizon (months)]
     */
    public Double getHorizon();

    /**
     * 获取 [Planning horizon (months)]脏标记
     */
    public boolean getHorizonDirtyFlag();
    /**
     * 获取 [ID]
     */
    public void setId(Integer id);
    
    /**
     * 设置 [ID]
     */
    public Integer getId();

    /**
     * 获取 [ID]脏标记
     */
    public boolean getIdDirtyFlag();
    /**
     * 获取 [名称]
     */
    public void setName(String name);
    
    /**
     * 设置 [名称]
     */
    public String getName();

    /**
     * 获取 [名称]脏标记
     */
    public boolean getNameDirtyFlag();
    /**
     * 获取 [Parameter]
     */
    public void setParameter_id(Integer parameter_id);
    
    /**
     * 设置 [Parameter]
     */
    public Integer getParameter_id();

    /**
     * 获取 [Parameter]脏标记
     */
    public boolean getParameter_idDirtyFlag();
    /**
     * 获取 [Parameter]
     */
    public void setParameter_id_text(String parameter_id_text);
    
    /**
     * 设置 [Parameter]
     */
    public String getParameter_id_text();

    /**
     * 获取 [Parameter]脏标记
     */
    public boolean getParameter_id_textDirtyFlag();
    /**
     * 获取 [单位]
     */
    public void setParameter_uom(Integer parameter_uom);
    
    /**
     * 设置 [单位]
     */
    public Integer getParameter_uom();

    /**
     * 获取 [单位]脏标记
     */
    public boolean getParameter_uomDirtyFlag();
    /**
     * 获取 [任务]
     */
    public void setPm_rules_line_ids(String pm_rules_line_ids);
    
    /**
     * 设置 [任务]
     */
    public String getPm_rules_line_ids();

    /**
     * 获取 [任务]脏标记
     */
    public boolean getPm_rules_line_idsDirtyFlag();
    /**
     * 获取 [最后更新时间]
     */
    public void setWrite_date(Timestamp write_date);
    
    /**
     * 设置 [最后更新时间]
     */
    public Timestamp getWrite_date();

    /**
     * 获取 [最后更新时间]脏标记
     */
    public boolean getWrite_dateDirtyFlag();
    /**
     * 获取 [最后更新者]
     */
    public void setWrite_uid(Integer write_uid);
    
    /**
     * 设置 [最后更新者]
     */
    public Integer getWrite_uid();

    /**
     * 获取 [最后更新者]脏标记
     */
    public boolean getWrite_uidDirtyFlag();
    /**
     * 获取 [最后更新者]
     */
    public void setWrite_uid_text(String write_uid_text);
    
    /**
     * 设置 [最后更新者]
     */
    public String getWrite_uid_text();

    /**
     * 获取 [最后更新者]脏标记
     */
    public boolean getWrite_uid_textDirtyFlag();
    /**
     * 获取 [最后修改日]
     */
    public void set__last_update(Timestamp __last_update);
    
    /**
     * 设置 [最后修改日]
     */
    public Timestamp get__last_update();

    /**
     * 获取 [最后修改日]脏标记
     */
    public boolean get__last_updateDirtyFlag();


    /**
     *  转换为Map
     */
    public Map<String, Object> toMap() throws Exception ;

    /**
     *  从Map构建
     */
   public void fromMap(Map<String, Object> map) throws Exception;
}
