package cn.ibizlab.odoo.core.odoo_lunch.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_lunch.domain.Lunch_product;
import cn.ibizlab.odoo.core.odoo_lunch.filter.Lunch_productSearchContext;


/**
 * 实体[Lunch_product] 服务对象接口
 */
public interface ILunch_productService{

    Lunch_product get(Integer key) ;
    boolean update(Lunch_product et) ;
    void updateBatch(List<Lunch_product> list) ;
    boolean create(Lunch_product et) ;
    void createBatch(List<Lunch_product> list) ;
    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    Page<Lunch_product> searchDefault(Lunch_productSearchContext context) ;

}



