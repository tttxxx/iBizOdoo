package cn.ibizlab.odoo.core.repository.client.po;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigDecimal;

/**
 * [hr_holidays_summary_employee] 对象
 */
public interface hr_holidays_summary_employee {

    public Timestamp getCreate_date();

    public void setCreate_date(Timestamp create_date);

    public Integer getCreate_uid();

    public void setCreate_uid(Integer create_uid);

    public String getCreate_uid_text();

    public void setCreate_uid_text(String create_uid_text);

    public Timestamp getDate_from();

    public void setDate_from(Timestamp date_from);

    public String getDisplay_name();

    public void setDisplay_name(String display_name);

    public String getEmp();

    public void setEmp(String emp);

    public String getHoliday_type();

    public void setHoliday_type(String holiday_type);

    public Integer getId();

    public void setId(Integer id);

    public Timestamp getWrite_date();

    public void setWrite_date(Timestamp write_date);

    public Integer getWrite_uid();

    public void setWrite_uid(Integer write_uid);

    public String getWrite_uid_text();

    public void setWrite_uid_text(String write_uid_text);

    public Timestamp get__last_update();

    public void set__last_update(Timestamp __last_update);

}
