package cn.ibizlab.odoo.core.odoo_mro.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;

import cn.ibizlab.odoo.core.odoo_mro.domain.Mro_pm_meter_ratio;
import cn.ibizlab.odoo.core.odoo_mro.filter.Mro_pm_meter_ratioSearchContext;


/**
 * 实体[Mro_pm_meter_ratio] 服务对象接口
 */
public interface IMro_pm_meter_ratioService{

    Mro_pm_meter_ratio get(Integer key) ;
    boolean update(Mro_pm_meter_ratio et) ;
    void updateBatch(List<Mro_pm_meter_ratio> list) ;
    boolean remove(Integer key) ;
    void removeBatch(Collection<Integer> idList) ;
    boolean create(Mro_pm_meter_ratio et) ;
    void createBatch(List<Mro_pm_meter_ratio> list) ;
    Page<Mro_pm_meter_ratio> searchDefault(Mro_pm_meter_ratioSearchContext context) ;

}



