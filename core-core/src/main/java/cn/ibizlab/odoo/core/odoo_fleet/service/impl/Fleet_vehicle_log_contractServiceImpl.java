package cn.ibizlab.odoo.core.odoo_fleet.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_fleet.domain.Fleet_vehicle_log_contract;
import cn.ibizlab.odoo.core.odoo_fleet.filter.Fleet_vehicle_log_contractSearchContext;
import cn.ibizlab.odoo.core.odoo_fleet.service.IFleet_vehicle_log_contractService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;



import cn.ibizlab.odoo.core.odoo_fleet.client.fleet_vehicle_log_contractOdooClient;
import cn.ibizlab.odoo.core.odoo_fleet.clientmodel.fleet_vehicle_log_contractClientModel;

/**
 * 实体[车辆合同信息] 服务对象接口实现
 */
@Slf4j
@Service
public class Fleet_vehicle_log_contractServiceImpl implements IFleet_vehicle_log_contractService {

    @Autowired
    fleet_vehicle_log_contractOdooClient fleet_vehicle_log_contractOdooClient;


    @Override
    public Fleet_vehicle_log_contract get(Integer id) {
        fleet_vehicle_log_contractClientModel clientModel = new fleet_vehicle_log_contractClientModel();
        clientModel.setId(id);
		fleet_vehicle_log_contractOdooClient.get(clientModel);
        Fleet_vehicle_log_contract et = convert2Domain(clientModel,null);
        if(et==null){
            et=new Fleet_vehicle_log_contract();
            et.setId(id);
        }
        else{
        }

        return  et;
    }

    @Override
    public boolean update(Fleet_vehicle_log_contract et) {
        fleet_vehicle_log_contractClientModel clientModel = convert2Model(et,null);
		fleet_vehicle_log_contractOdooClient.update(clientModel);
        Fleet_vehicle_log_contract rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }
    
    public void updateBatch(List<Fleet_vehicle_log_contract> list){
    }

    @Override
    public boolean remove(Integer id) {
        fleet_vehicle_log_contractClientModel clientModel = new fleet_vehicle_log_contractClientModel();
        clientModel.setId(id);
		fleet_vehicle_log_contractOdooClient.remove(clientModel);
        return true; 
    }

    public void removeBatch(Collection<Integer> idList){
    }

    @Override
    public boolean create(Fleet_vehicle_log_contract et) {
        fleet_vehicle_log_contractClientModel clientModel = convert2Model(et,null);
		fleet_vehicle_log_contractOdooClient.create(clientModel);
        Fleet_vehicle_log_contract rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Fleet_vehicle_log_contract> list){
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Fleet_vehicle_log_contract> searchDefault(Fleet_vehicle_log_contractSearchContext context) {
        List<Fleet_vehicle_log_contract> list = new ArrayList<Fleet_vehicle_log_contract>();
        Page<fleet_vehicle_log_contractClientModel> clientModelList = fleet_vehicle_log_contractOdooClient.search(context);
        for(fleet_vehicle_log_contractClientModel clientModel : clientModelList){
            list.add( this.convert2Domain(clientModel,null));
        }
        return new PageImpl<Fleet_vehicle_log_contract>(list, context.getPageable(), clientModelList.getTotalElements());
    }

    public fleet_vehicle_log_contractClientModel convert2Model(Fleet_vehicle_log_contract domain , fleet_vehicle_log_contractClientModel model) {
        if(domain == null)
            return model ;
        if(model==null){
            model = new fleet_vehicle_log_contractClientModel();

        }
        if((Boolean) domain.getExtensionparams().get("dirtyflagenable")){
            if((Boolean) domain.getExtensionparams().get("activity_summarydirtyflag"))
                model.setActivity_summary(domain.getActivitySummary());
            if((Boolean) domain.getExtensionparams().get("ins_refdirtyflag"))
                model.setIns_ref(domain.getInsRef());
            if((Boolean) domain.getExtensionparams().get("activedirtyflag"))
                model.setActive(domain.getActive());
            if((Boolean) domain.getExtensionparams().get("sum_costdirtyflag"))
                model.setSum_cost(domain.getSumCost());
            if((Boolean) domain.getExtensionparams().get("notesdirtyflag"))
                model.setNotes(domain.getNotes());
            if((Boolean) domain.getExtensionparams().get("display_namedirtyflag"))
                model.setDisplay_name(domain.getDisplayName());
            if((Boolean) domain.getExtensionparams().get("message_needaction_counterdirtyflag"))
                model.setMessage_needaction_counter(domain.getMessageNeedactionCounter());
            if((Boolean) domain.getExtensionparams().get("expiration_datedirtyflag"))
                model.setExpiration_date(domain.getExpirationDate());
            if((Boolean) domain.getExtensionparams().get("message_partner_idsdirtyflag"))
                model.setMessage_partner_ids(domain.getMessagePartnerIds());
            if((Boolean) domain.getExtensionparams().get("message_has_errordirtyflag"))
                model.setMessage_has_error(domain.getMessageHasError());
            if((Boolean) domain.getExtensionparams().get("activity_type_iddirtyflag"))
                model.setActivity_type_id(domain.getActivityTypeId());
            if((Boolean) domain.getExtensionparams().get("message_unread_counterdirtyflag"))
                model.setMessage_unread_counter(domain.getMessageUnreadCounter());
            if((Boolean) domain.getExtensionparams().get("activity_user_iddirtyflag"))
                model.setActivity_user_id(domain.getActivityUserId());
            if((Boolean) domain.getExtensionparams().get("days_leftdirtyflag"))
                model.setDays_left(domain.getDaysLeft());
            if((Boolean) domain.getExtensionparams().get("message_channel_idsdirtyflag"))
                model.setMessage_channel_ids(domain.getMessageChannelIds());
            if((Boolean) domain.getExtensionparams().get("message_follower_idsdirtyflag"))
                model.setMessage_follower_ids(domain.getMessageFollowerIds());
            if((Boolean) domain.getExtensionparams().get("odometerdirtyflag"))
                model.setOdometer(domain.getOdometer());
            if((Boolean) domain.getExtensionparams().get("statedirtyflag"))
                model.setState(domain.getState());
            if((Boolean) domain.getExtensionparams().get("cost_idsdirtyflag"))
                model.setCost_ids(domain.getCostIds());
            if((Boolean) domain.getExtensionparams().get("message_unreaddirtyflag"))
                model.setMessage_unread(domain.getMessageUnread());
            if((Boolean) domain.getExtensionparams().get("cost_generateddirtyflag"))
                model.setCost_generated(domain.getCostGenerated());
            if((Boolean) domain.getExtensionparams().get("__last_updatedirtyflag"))
                model.set__last_update(domain.getLastUpdate());
            if((Boolean) domain.getExtensionparams().get("website_message_idsdirtyflag"))
                model.setWebsite_message_ids(domain.getWebsiteMessageIds());
            if((Boolean) domain.getExtensionparams().get("create_datedirtyflag"))
                model.setCreate_date(domain.getCreateDate());
            if((Boolean) domain.getExtensionparams().get("message_idsdirtyflag"))
                model.setMessage_ids(domain.getMessageIds());
            if((Boolean) domain.getExtensionparams().get("message_is_followerdirtyflag"))
                model.setMessage_is_follower(domain.getMessageIsFollower());
            if((Boolean) domain.getExtensionparams().get("iddirtyflag"))
                model.setId(domain.getId());
            if((Boolean) domain.getExtensionparams().get("write_datedirtyflag"))
                model.setWrite_date(domain.getWriteDate());
            if((Boolean) domain.getExtensionparams().get("message_needactiondirtyflag"))
                model.setMessage_needaction(domain.getMessageNeedaction());
            if((Boolean) domain.getExtensionparams().get("message_main_attachment_iddirtyflag"))
                model.setMessage_main_attachment_id(domain.getMessageMainAttachmentId());
            if((Boolean) domain.getExtensionparams().get("message_has_error_counterdirtyflag"))
                model.setMessage_has_error_counter(domain.getMessageHasErrorCounter());
            if((Boolean) domain.getExtensionparams().get("generated_cost_idsdirtyflag"))
                model.setGenerated_cost_ids(domain.getGeneratedCostIds());
            if((Boolean) domain.getExtensionparams().get("namedirtyflag"))
                model.setName(domain.getName());
            if((Boolean) domain.getExtensionparams().get("cost_frequencydirtyflag"))
                model.setCost_frequency(domain.getCostFrequency());
            if((Boolean) domain.getExtensionparams().get("start_datedirtyflag"))
                model.setStart_date(domain.getStartDate());
            if((Boolean) domain.getExtensionparams().get("activity_statedirtyflag"))
                model.setActivity_state(domain.getActivityState());
            if((Boolean) domain.getExtensionparams().get("message_attachment_countdirtyflag"))
                model.setMessage_attachment_count(domain.getMessageAttachmentCount());
            if((Boolean) domain.getExtensionparams().get("activity_date_deadlinedirtyflag"))
                model.setActivity_date_deadline(domain.getActivityDateDeadline());
            if((Boolean) domain.getExtensionparams().get("activity_idsdirtyflag"))
                model.setActivity_ids(domain.getActivityIds());
            if((Boolean) domain.getExtensionparams().get("vehicle_iddirtyflag"))
                model.setVehicle_id(domain.getVehicleId());
            if((Boolean) domain.getExtensionparams().get("auto_generateddirtyflag"))
                model.setAuto_generated(domain.getAutoGenerated());
            if((Boolean) domain.getExtensionparams().get("create_uid_textdirtyflag"))
                model.setCreate_uid_text(domain.getCreateUidText());
            if((Boolean) domain.getExtensionparams().get("odometer_iddirtyflag"))
                model.setOdometer_id(domain.getOdometerId());
            if((Boolean) domain.getExtensionparams().get("cost_typedirtyflag"))
                model.setCost_type(domain.getCostType());
            if((Boolean) domain.getExtensionparams().get("parent_iddirtyflag"))
                model.setParent_id(domain.getParentId());
            if((Boolean) domain.getExtensionparams().get("purchaser_id_textdirtyflag"))
                model.setPurchaser_id_text(domain.getPurchaserIdText());
            if((Boolean) domain.getExtensionparams().get("cost_subtype_iddirtyflag"))
                model.setCost_subtype_id(domain.getCostSubtypeId());
            if((Boolean) domain.getExtensionparams().get("cost_amountdirtyflag"))
                model.setCost_amount(domain.getCostAmount());
            if((Boolean) domain.getExtensionparams().get("amountdirtyflag"))
                model.setAmount(domain.getAmount());
            if((Boolean) domain.getExtensionparams().get("insurer_id_textdirtyflag"))
                model.setInsurer_id_text(domain.getInsurerIdText());
            if((Boolean) domain.getExtensionparams().get("cost_id_textdirtyflag"))
                model.setCost_id_text(domain.getCostIdText());
            if((Boolean) domain.getExtensionparams().get("descriptiondirtyflag"))
                model.setDescription(domain.getDescription());
            if((Boolean) domain.getExtensionparams().get("user_id_textdirtyflag"))
                model.setUser_id_text(domain.getUserIdText());
            if((Boolean) domain.getExtensionparams().get("datedirtyflag"))
                model.setDate(domain.getDate());
            if((Boolean) domain.getExtensionparams().get("odometer_unitdirtyflag"))
                model.setOdometer_unit(domain.getOdometerUnit());
            if((Boolean) domain.getExtensionparams().get("contract_iddirtyflag"))
                model.setContract_id(domain.getContractId());
            if((Boolean) domain.getExtensionparams().get("write_uid_textdirtyflag"))
                model.setWrite_uid_text(domain.getWriteUidText());
            if((Boolean) domain.getExtensionparams().get("write_uiddirtyflag"))
                model.setWrite_uid(domain.getWriteUid());
            if((Boolean) domain.getExtensionparams().get("cost_iddirtyflag"))
                model.setCost_id(domain.getCostId());
            if((Boolean) domain.getExtensionparams().get("insurer_iddirtyflag"))
                model.setInsurer_id(domain.getInsurerId());
            if((Boolean) domain.getExtensionparams().get("user_iddirtyflag"))
                model.setUser_id(domain.getUserId());
            if((Boolean) domain.getExtensionparams().get("create_uiddirtyflag"))
                model.setCreate_uid(domain.getCreateUid());
            if((Boolean) domain.getExtensionparams().get("purchaser_iddirtyflag"))
                model.setPurchaser_id(domain.getPurchaserId());
            domain.getFocusNull().clear();
        }
        return model ;
    }

    public Fleet_vehicle_log_contract convert2Domain( fleet_vehicle_log_contractClientModel model ,Fleet_vehicle_log_contract domain) {
        if(model == null)
            return domain ;
        if(domain == null){
            domain = new Fleet_vehicle_log_contract();
        }

        if(model.getActivity_summaryDirtyFlag())
            domain.setActivitySummary(model.getActivity_summary());
        if(model.getIns_refDirtyFlag())
            domain.setInsRef(model.getIns_ref());
        if(model.getActiveDirtyFlag())
            domain.setActive(model.getActive());
        if(model.getSum_costDirtyFlag())
            domain.setSumCost(model.getSum_cost());
        if(model.getNotesDirtyFlag())
            domain.setNotes(model.getNotes());
        if(model.getDisplay_nameDirtyFlag())
            domain.setDisplayName(model.getDisplay_name());
        if(model.getMessage_needaction_counterDirtyFlag())
            domain.setMessageNeedactionCounter(model.getMessage_needaction_counter());
        if(model.getExpiration_dateDirtyFlag())
            domain.setExpirationDate(model.getExpiration_date());
        if(model.getMessage_partner_idsDirtyFlag())
            domain.setMessagePartnerIds(model.getMessage_partner_ids());
        if(model.getMessage_has_errorDirtyFlag())
            domain.setMessageHasError(model.getMessage_has_error());
        if(model.getActivity_type_idDirtyFlag())
            domain.setActivityTypeId(model.getActivity_type_id());
        if(model.getMessage_unread_counterDirtyFlag())
            domain.setMessageUnreadCounter(model.getMessage_unread_counter());
        if(model.getActivity_user_idDirtyFlag())
            domain.setActivityUserId(model.getActivity_user_id());
        if(model.getDays_leftDirtyFlag())
            domain.setDaysLeft(model.getDays_left());
        if(model.getMessage_channel_idsDirtyFlag())
            domain.setMessageChannelIds(model.getMessage_channel_ids());
        if(model.getMessage_follower_idsDirtyFlag())
            domain.setMessageFollowerIds(model.getMessage_follower_ids());
        if(model.getOdometerDirtyFlag())
            domain.setOdometer(model.getOdometer());
        if(model.getStateDirtyFlag())
            domain.setState(model.getState());
        if(model.getCost_idsDirtyFlag())
            domain.setCostIds(model.getCost_ids());
        if(model.getMessage_unreadDirtyFlag())
            domain.setMessageUnread(model.getMessage_unread());
        if(model.getCost_generatedDirtyFlag())
            domain.setCostGenerated(model.getCost_generated());
        if(model.get__last_updateDirtyFlag())
            domain.setLastUpdate(model.get__last_update());
        if(model.getWebsite_message_idsDirtyFlag())
            domain.setWebsiteMessageIds(model.getWebsite_message_ids());
        if(model.getCreate_dateDirtyFlag())
            domain.setCreateDate(model.getCreate_date());
        if(model.getMessage_idsDirtyFlag())
            domain.setMessageIds(model.getMessage_ids());
        if(model.getMessage_is_followerDirtyFlag())
            domain.setMessageIsFollower(model.getMessage_is_follower());
        if(model.getIdDirtyFlag())
            domain.setId(model.getId());
        if(model.getWrite_dateDirtyFlag())
            domain.setWriteDate(model.getWrite_date());
        if(model.getMessage_needactionDirtyFlag())
            domain.setMessageNeedaction(model.getMessage_needaction());
        if(model.getMessage_main_attachment_idDirtyFlag())
            domain.setMessageMainAttachmentId(model.getMessage_main_attachment_id());
        if(model.getMessage_has_error_counterDirtyFlag())
            domain.setMessageHasErrorCounter(model.getMessage_has_error_counter());
        if(model.getGenerated_cost_idsDirtyFlag())
            domain.setGeneratedCostIds(model.getGenerated_cost_ids());
        if(model.getNameDirtyFlag())
            domain.setName(model.getName());
        if(model.getCost_frequencyDirtyFlag())
            domain.setCostFrequency(model.getCost_frequency());
        if(model.getStart_dateDirtyFlag())
            domain.setStartDate(model.getStart_date());
        if(model.getActivity_stateDirtyFlag())
            domain.setActivityState(model.getActivity_state());
        if(model.getMessage_attachment_countDirtyFlag())
            domain.setMessageAttachmentCount(model.getMessage_attachment_count());
        if(model.getActivity_date_deadlineDirtyFlag())
            domain.setActivityDateDeadline(model.getActivity_date_deadline());
        if(model.getActivity_idsDirtyFlag())
            domain.setActivityIds(model.getActivity_ids());
        if(model.getVehicle_idDirtyFlag())
            domain.setVehicleId(model.getVehicle_id());
        if(model.getAuto_generatedDirtyFlag())
            domain.setAutoGenerated(model.getAuto_generated());
        if(model.getCreate_uid_textDirtyFlag())
            domain.setCreateUidText(model.getCreate_uid_text());
        if(model.getOdometer_idDirtyFlag())
            domain.setOdometerId(model.getOdometer_id());
        if(model.getCost_typeDirtyFlag())
            domain.setCostType(model.getCost_type());
        if(model.getParent_idDirtyFlag())
            domain.setParentId(model.getParent_id());
        if(model.getPurchaser_id_textDirtyFlag())
            domain.setPurchaserIdText(model.getPurchaser_id_text());
        if(model.getCost_subtype_idDirtyFlag())
            domain.setCostSubtypeId(model.getCost_subtype_id());
        if(model.getCost_amountDirtyFlag())
            domain.setCostAmount(model.getCost_amount());
        if(model.getAmountDirtyFlag())
            domain.setAmount(model.getAmount());
        if(model.getInsurer_id_textDirtyFlag())
            domain.setInsurerIdText(model.getInsurer_id_text());
        if(model.getCost_id_textDirtyFlag())
            domain.setCostIdText(model.getCost_id_text());
        if(model.getDescriptionDirtyFlag())
            domain.setDescription(model.getDescription());
        if(model.getUser_id_textDirtyFlag())
            domain.setUserIdText(model.getUser_id_text());
        if(model.getDateDirtyFlag())
            domain.setDate(model.getDate());
        if(model.getOdometer_unitDirtyFlag())
            domain.setOdometerUnit(model.getOdometer_unit());
        if(model.getContract_idDirtyFlag())
            domain.setContractId(model.getContract_id());
        if(model.getWrite_uid_textDirtyFlag())
            domain.setWriteUidText(model.getWrite_uid_text());
        if(model.getWrite_uidDirtyFlag())
            domain.setWriteUid(model.getWrite_uid());
        if(model.getCost_idDirtyFlag())
            domain.setCostId(model.getCost_id());
        if(model.getInsurer_idDirtyFlag())
            domain.setInsurerId(model.getInsurer_id());
        if(model.getUser_idDirtyFlag())
            domain.setUserId(model.getUser_id());
        if(model.getCreate_uidDirtyFlag())
            domain.setCreateUid(model.getCreate_uid());
        if(model.getPurchaser_idDirtyFlag())
            domain.setPurchaserId(model.getPurchaser_id());
        return domain ;
    }

}

    



