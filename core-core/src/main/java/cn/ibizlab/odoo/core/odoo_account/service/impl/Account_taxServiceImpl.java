package cn.ibizlab.odoo.core.odoo_account.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_account.domain.Account_tax;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_taxSearchContext;
import cn.ibizlab.odoo.core.odoo_account.service.IAccount_taxService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;



import cn.ibizlab.odoo.core.odoo_account.client.account_taxOdooClient;
import cn.ibizlab.odoo.core.odoo_account.clientmodel.account_taxClientModel;

/**
 * 实体[税率] 服务对象接口实现
 */
@Slf4j
@Service
public class Account_taxServiceImpl implements IAccount_taxService {

    @Autowired
    account_taxOdooClient account_taxOdooClient;


    @Override
    public boolean remove(Integer id) {
        account_taxClientModel clientModel = new account_taxClientModel();
        clientModel.setId(id);
		account_taxOdooClient.remove(clientModel);
        return true; 
    }

    public void removeBatch(Collection<Integer> idList){
    }

    @Override
    public boolean update(Account_tax et) {
        account_taxClientModel clientModel = convert2Model(et,null);
		account_taxOdooClient.update(clientModel);
        Account_tax rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }
    
    public void updateBatch(List<Account_tax> list){
    }

    @Override
    public boolean create(Account_tax et) {
        account_taxClientModel clientModel = convert2Model(et,null);
		account_taxOdooClient.create(clientModel);
        Account_tax rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Account_tax> list){
    }

    @Override
    public Account_tax get(Integer id) {
        account_taxClientModel clientModel = new account_taxClientModel();
        clientModel.setId(id);
		account_taxOdooClient.get(clientModel);
        Account_tax et = convert2Domain(clientModel,null);
        if(et==null){
            et=new Account_tax();
            et.setId(id);
        }
        else{
        }

        return  et;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Account_tax> searchDefault(Account_taxSearchContext context) {
        List<Account_tax> list = new ArrayList<Account_tax>();
        Page<account_taxClientModel> clientModelList = account_taxOdooClient.search(context);
        for(account_taxClientModel clientModel : clientModelList){
            list.add( this.convert2Domain(clientModel,null));
        }
        return new PageImpl<Account_tax>(list, context.getPageable(), clientModelList.getTotalElements());
    }

    public account_taxClientModel convert2Model(Account_tax domain , account_taxClientModel model) {
        if(domain == null)
            return model ;
        if(model==null){
            model = new account_taxClientModel();

        }
        if((Boolean) domain.getExtensionparams().get("dirtyflagenable")){
            if((Boolean) domain.getExtensionparams().get("iddirtyflag"))
                model.setId(domain.getId());
            if((Boolean) domain.getExtensionparams().get("create_datedirtyflag"))
                model.setCreate_date(domain.getCreateDate());
            if((Boolean) domain.getExtensionparams().get("display_namedirtyflag"))
                model.setDisplay_name(domain.getDisplayName());
            if((Boolean) domain.getExtensionparams().get("type_tax_usedirtyflag"))
                model.setType_tax_use(domain.getTypeTaxUse());
            if((Boolean) domain.getExtensionparams().get("activedirtyflag"))
                model.setActive(domain.getActive());
            if((Boolean) domain.getExtensionparams().get("children_tax_idsdirtyflag"))
                model.setChildren_tax_ids(domain.getChildrenTaxIds());
            if((Boolean) domain.getExtensionparams().get("analyticdirtyflag"))
                model.setAnalytic(domain.getAnalytic());
            if((Boolean) domain.getExtensionparams().get("price_includedirtyflag"))
                model.setPrice_include(domain.getPriceInclude());
            if((Boolean) domain.getExtensionparams().get("descriptiondirtyflag"))
                model.setDescription(domain.getDescription());
            if((Boolean) domain.getExtensionparams().get("write_datedirtyflag"))
                model.setWrite_date(domain.getWriteDate());
            if((Boolean) domain.getExtensionparams().get("tax_exigibilitydirtyflag"))
                model.setTax_exigibility(domain.getTaxExigibility());
            if((Boolean) domain.getExtensionparams().get("namedirtyflag"))
                model.setName(domain.getName());
            if((Boolean) domain.getExtensionparams().get("tag_idsdirtyflag"))
                model.setTag_ids(domain.getTagIds());
            if((Boolean) domain.getExtensionparams().get("amount_typedirtyflag"))
                model.setAmount_type(domain.getAmountType());
            if((Boolean) domain.getExtensionparams().get("__last_updatedirtyflag"))
                model.set__last_update(domain.getLastUpdate());
            if((Boolean) domain.getExtensionparams().get("amountdirtyflag"))
                model.setAmount(domain.getAmount());
            if((Boolean) domain.getExtensionparams().get("sequencedirtyflag"))
                model.setSequence(domain.getSequence());
            if((Boolean) domain.getExtensionparams().get("include_base_amountdirtyflag"))
                model.setInclude_base_amount(domain.getIncludeBaseAmount());
            if((Boolean) domain.getExtensionparams().get("cash_basis_base_account_id_textdirtyflag"))
                model.setCash_basis_base_account_id_text(domain.getCashBasisBaseAccountIdText());
            if((Boolean) domain.getExtensionparams().get("cash_basis_account_id_textdirtyflag"))
                model.setCash_basis_account_id_text(domain.getCashBasisAccountIdText());
            if((Boolean) domain.getExtensionparams().get("hide_tax_exigibilitydirtyflag"))
                model.setHide_tax_exigibility(domain.getHideTaxExigibility());
            if((Boolean) domain.getExtensionparams().get("tax_group_id_textdirtyflag"))
                model.setTax_group_id_text(domain.getTaxGroupIdText());
            if((Boolean) domain.getExtensionparams().get("company_id_textdirtyflag"))
                model.setCompany_id_text(domain.getCompanyIdText());
            if((Boolean) domain.getExtensionparams().get("refund_account_id_textdirtyflag"))
                model.setRefund_account_id_text(domain.getRefundAccountIdText());
            if((Boolean) domain.getExtensionparams().get("account_id_textdirtyflag"))
                model.setAccount_id_text(domain.getAccountIdText());
            if((Boolean) domain.getExtensionparams().get("write_uid_textdirtyflag"))
                model.setWrite_uid_text(domain.getWriteUidText());
            if((Boolean) domain.getExtensionparams().get("create_uid_textdirtyflag"))
                model.setCreate_uid_text(domain.getCreateUidText());
            if((Boolean) domain.getExtensionparams().get("refund_account_iddirtyflag"))
                model.setRefund_account_id(domain.getRefundAccountId());
            if((Boolean) domain.getExtensionparams().get("cash_basis_account_iddirtyflag"))
                model.setCash_basis_account_id(domain.getCashBasisAccountId());
            if((Boolean) domain.getExtensionparams().get("create_uiddirtyflag"))
                model.setCreate_uid(domain.getCreateUid());
            if((Boolean) domain.getExtensionparams().get("write_uiddirtyflag"))
                model.setWrite_uid(domain.getWriteUid());
            if((Boolean) domain.getExtensionparams().get("account_iddirtyflag"))
                model.setAccount_id(domain.getAccountId());
            if((Boolean) domain.getExtensionparams().get("cash_basis_base_account_iddirtyflag"))
                model.setCash_basis_base_account_id(domain.getCashBasisBaseAccountId());
            if((Boolean) domain.getExtensionparams().get("tax_group_iddirtyflag"))
                model.setTax_group_id(domain.getTaxGroupId());
            if((Boolean) domain.getExtensionparams().get("company_iddirtyflag"))
                model.setCompany_id(domain.getCompanyId());
            domain.getFocusNull().clear();
        }
        return model ;
    }

    public Account_tax convert2Domain( account_taxClientModel model ,Account_tax domain) {
        if(model == null)
            return domain ;
        if(domain == null){
            domain = new Account_tax();
        }

        if(model.getIdDirtyFlag())
            domain.setId(model.getId());
        if(model.getCreate_dateDirtyFlag())
            domain.setCreateDate(model.getCreate_date());
        if(model.getDisplay_nameDirtyFlag())
            domain.setDisplayName(model.getDisplay_name());
        if(model.getType_tax_useDirtyFlag())
            domain.setTypeTaxUse(model.getType_tax_use());
        if(model.getActiveDirtyFlag())
            domain.setActive(model.getActive());
        if(model.getChildren_tax_idsDirtyFlag())
            domain.setChildrenTaxIds(model.getChildren_tax_ids());
        if(model.getAnalyticDirtyFlag())
            domain.setAnalytic(model.getAnalytic());
        if(model.getPrice_includeDirtyFlag())
            domain.setPriceInclude(model.getPrice_include());
        if(model.getDescriptionDirtyFlag())
            domain.setDescription(model.getDescription());
        if(model.getWrite_dateDirtyFlag())
            domain.setWriteDate(model.getWrite_date());
        if(model.getTax_exigibilityDirtyFlag())
            domain.setTaxExigibility(model.getTax_exigibility());
        if(model.getNameDirtyFlag())
            domain.setName(model.getName());
        if(model.getTag_idsDirtyFlag())
            domain.setTagIds(model.getTag_ids());
        if(model.getAmount_typeDirtyFlag())
            domain.setAmountType(model.getAmount_type());
        if(model.get__last_updateDirtyFlag())
            domain.setLastUpdate(model.get__last_update());
        if(model.getAmountDirtyFlag())
            domain.setAmount(model.getAmount());
        if(model.getSequenceDirtyFlag())
            domain.setSequence(model.getSequence());
        if(model.getInclude_base_amountDirtyFlag())
            domain.setIncludeBaseAmount(model.getInclude_base_amount());
        if(model.getCash_basis_base_account_id_textDirtyFlag())
            domain.setCashBasisBaseAccountIdText(model.getCash_basis_base_account_id_text());
        if(model.getCash_basis_account_id_textDirtyFlag())
            domain.setCashBasisAccountIdText(model.getCash_basis_account_id_text());
        if(model.getHide_tax_exigibilityDirtyFlag())
            domain.setHideTaxExigibility(model.getHide_tax_exigibility());
        if(model.getTax_group_id_textDirtyFlag())
            domain.setTaxGroupIdText(model.getTax_group_id_text());
        if(model.getCompany_id_textDirtyFlag())
            domain.setCompanyIdText(model.getCompany_id_text());
        if(model.getRefund_account_id_textDirtyFlag())
            domain.setRefundAccountIdText(model.getRefund_account_id_text());
        if(model.getAccount_id_textDirtyFlag())
            domain.setAccountIdText(model.getAccount_id_text());
        if(model.getWrite_uid_textDirtyFlag())
            domain.setWriteUidText(model.getWrite_uid_text());
        if(model.getCreate_uid_textDirtyFlag())
            domain.setCreateUidText(model.getCreate_uid_text());
        if(model.getRefund_account_idDirtyFlag())
            domain.setRefundAccountId(model.getRefund_account_id());
        if(model.getCash_basis_account_idDirtyFlag())
            domain.setCashBasisAccountId(model.getCash_basis_account_id());
        if(model.getCreate_uidDirtyFlag())
            domain.setCreateUid(model.getCreate_uid());
        if(model.getWrite_uidDirtyFlag())
            domain.setWriteUid(model.getWrite_uid());
        if(model.getAccount_idDirtyFlag())
            domain.setAccountId(model.getAccount_id());
        if(model.getCash_basis_base_account_idDirtyFlag())
            domain.setCashBasisBaseAccountId(model.getCash_basis_base_account_id());
        if(model.getTax_group_idDirtyFlag())
            domain.setTaxGroupId(model.getTax_group_id());
        if(model.getCompany_idDirtyFlag())
            domain.setCompanyId(model.getCompany_id());
        return domain ;
    }

}

    



