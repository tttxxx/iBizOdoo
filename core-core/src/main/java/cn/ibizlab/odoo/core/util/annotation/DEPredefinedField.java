package cn.ibizlab.odoo.core.util.annotation;

import cn.ibizlab.odoo.core.util.enums.DEPredefinedFieldType;
import cn.ibizlab.odoo.core.util.enums.DEPredefinedFieldFillMode;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target({ ElementType.FIELD})
public @interface DEPredefinedField
{
	/**
	 * 填充模式
	 * @return
	 */
	DEPredefinedFieldFillMode fill() default DEPredefinedFieldFillMode.INSERT_UPDATE;

	/**
	 * 预置属性类型
	 * @return
	 */
	DEPredefinedFieldType preType() default DEPredefinedFieldType.DEFAULT;
}

