package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Base_partner_merge_line;
import cn.ibizlab.odoo.core.odoo_base.filter.Base_partner_merge_lineSearchContext;

/**
 * 实体 [合并合作伙伴明细行] 存储对象
 */
public interface Base_partner_merge_lineRepository extends Repository<Base_partner_merge_line> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Base_partner_merge_line> searchDefault(Base_partner_merge_lineSearchContext context);

    Base_partner_merge_line convert2PO(cn.ibizlab.odoo.core.odoo_base.domain.Base_partner_merge_line domain , Base_partner_merge_line po) ;

    cn.ibizlab.odoo.core.odoo_base.domain.Base_partner_merge_line convert2Domain( Base_partner_merge_line po ,cn.ibizlab.odoo.core.odoo_base.domain.Base_partner_merge_line domain) ;

}
