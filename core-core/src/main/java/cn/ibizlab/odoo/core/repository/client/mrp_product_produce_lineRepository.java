package cn.ibizlab.odoo.core.repository.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import cn.ibizlab.odoo.core.repository.client.po.mrp_product_produce_line;

/**
 * 实体[mrp_product_produce_line] 服务对象接口
 */
public interface mrp_product_produce_lineRepository{


    public mrp_product_produce_line createPO() ;
        public void createBatch(mrp_product_produce_line mrp_product_produce_line);

        public void removeBatch(String id);

        public void update(mrp_product_produce_line mrp_product_produce_line);

        public List<mrp_product_produce_line> search();

        public void remove(String id);

        public void get(String id);

        public void updateBatch(mrp_product_produce_line mrp_product_produce_line);

        public void create(mrp_product_produce_line mrp_product_produce_line);


}
