package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Purchase_order;
import cn.ibizlab.odoo.core.odoo_purchase.filter.Purchase_orderSearchContext;

/**
 * 实体 [采购订单] 存储对象
 */
public interface Purchase_orderRepository extends Repository<Purchase_order> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Purchase_order> searchDefault(Purchase_orderSearchContext context);

    Purchase_order convert2PO(cn.ibizlab.odoo.core.odoo_purchase.domain.Purchase_order domain , Purchase_order po) ;

    cn.ibizlab.odoo.core.odoo_purchase.domain.Purchase_order convert2Domain( Purchase_order po ,cn.ibizlab.odoo.core.odoo_purchase.domain.Purchase_order domain) ;

}
