package cn.ibizlab.odoo.core.odoo_mro.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_mro.domain.Mro_task;
import cn.ibizlab.odoo.core.odoo_mro.filter.Mro_taskSearchContext;
import cn.ibizlab.odoo.core.odoo_mro.service.IMro_taskService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;



import cn.ibizlab.odoo.core.odoo_mro.client.mro_taskOdooClient;
import cn.ibizlab.odoo.core.odoo_mro.clientmodel.mro_taskClientModel;

/**
 * 实体[Maintenance Task] 服务对象接口实现
 */
@Slf4j
@Service
public class Mro_taskServiceImpl implements IMro_taskService {

    @Autowired
    mro_taskOdooClient mro_taskOdooClient;


    @Override
    public boolean update(Mro_task et) {
        mro_taskClientModel clientModel = convert2Model(et,null);
		mro_taskOdooClient.update(clientModel);
        Mro_task rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }
    
    public void updateBatch(List<Mro_task> list){
    }

    @Override
    public Mro_task get(Integer id) {
        mro_taskClientModel clientModel = new mro_taskClientModel();
        clientModel.setId(id);
		mro_taskOdooClient.get(clientModel);
        Mro_task et = convert2Domain(clientModel,null);
        if(et==null){
            et=new Mro_task();
            et.setId(id);
        }
        else{
        }

        return  et;
    }

    @Override
    public boolean remove(Integer id) {
        mro_taskClientModel clientModel = new mro_taskClientModel();
        clientModel.setId(id);
		mro_taskOdooClient.remove(clientModel);
        return true; 
    }

    public void removeBatch(Collection<Integer> idList){
    }

    @Override
    public boolean create(Mro_task et) {
        mro_taskClientModel clientModel = convert2Model(et,null);
		mro_taskOdooClient.create(clientModel);
        Mro_task rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Mro_task> list){
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Mro_task> searchDefault(Mro_taskSearchContext context) {
        List<Mro_task> list = new ArrayList<Mro_task>();
        Page<mro_taskClientModel> clientModelList = mro_taskOdooClient.search(context);
        for(mro_taskClientModel clientModel : clientModelList){
            list.add( this.convert2Domain(clientModel,null));
        }
        return new PageImpl<Mro_task>(list, context.getPageable(), clientModelList.getTotalElements());
    }

    public mro_taskClientModel convert2Model(Mro_task domain , mro_taskClientModel model) {
        if(domain == null)
            return model ;
        if(model==null){
            model = new mro_taskClientModel();

        }
        if((Boolean) domain.getExtensionparams().get("dirtyflagenable")){
            if((Boolean) domain.getExtensionparams().get("parts_linesdirtyflag"))
                model.setParts_lines(domain.getPartsLines());
            if((Boolean) domain.getExtensionparams().get("namedirtyflag"))
                model.setName(domain.getName());
            if((Boolean) domain.getExtensionparams().get("iddirtyflag"))
                model.setId(domain.getId());
            if((Boolean) domain.getExtensionparams().get("display_namedirtyflag"))
                model.setDisplay_name(domain.getDisplayName());
            if((Boolean) domain.getExtensionparams().get("activedirtyflag"))
                model.setActive(domain.getActive());
            if((Boolean) domain.getExtensionparams().get("tools_descriptiondirtyflag"))
                model.setTools_description(domain.getToolsDescription());
            if((Boolean) domain.getExtensionparams().get("labor_descriptiondirtyflag"))
                model.setLabor_description(domain.getLaborDescription());
            if((Boolean) domain.getExtensionparams().get("write_datedirtyflag"))
                model.setWrite_date(domain.getWriteDate());
            if((Boolean) domain.getExtensionparams().get("maintenance_typedirtyflag"))
                model.setMaintenance_type(domain.getMaintenanceType());
            if((Boolean) domain.getExtensionparams().get("operations_descriptiondirtyflag"))
                model.setOperations_description(domain.getOperationsDescription());
            if((Boolean) domain.getExtensionparams().get("documentation_descriptiondirtyflag"))
                model.setDocumentation_description(domain.getDocumentationDescription());
            if((Boolean) domain.getExtensionparams().get("create_datedirtyflag"))
                model.setCreate_date(domain.getCreateDate());
            if((Boolean) domain.getExtensionparams().get("__last_updatedirtyflag"))
                model.set__last_update(domain.getLastUpdate());
            if((Boolean) domain.getExtensionparams().get("write_uid_textdirtyflag"))
                model.setWrite_uid_text(domain.getWriteUidText());
            if((Boolean) domain.getExtensionparams().get("category_id_textdirtyflag"))
                model.setCategory_id_text(domain.getCategoryIdText());
            if((Boolean) domain.getExtensionparams().get("create_uid_textdirtyflag"))
                model.setCreate_uid_text(domain.getCreateUidText());
            if((Boolean) domain.getExtensionparams().get("category_iddirtyflag"))
                model.setCategory_id(domain.getCategoryId());
            if((Boolean) domain.getExtensionparams().get("create_uiddirtyflag"))
                model.setCreate_uid(domain.getCreateUid());
            if((Boolean) domain.getExtensionparams().get("write_uiddirtyflag"))
                model.setWrite_uid(domain.getWriteUid());
            domain.getFocusNull().clear();
        }
        return model ;
    }

    public Mro_task convert2Domain( mro_taskClientModel model ,Mro_task domain) {
        if(model == null)
            return domain ;
        if(domain == null){
            domain = new Mro_task();
        }

        if(model.getParts_linesDirtyFlag())
            domain.setPartsLines(model.getParts_lines());
        if(model.getNameDirtyFlag())
            domain.setName(model.getName());
        if(model.getIdDirtyFlag())
            domain.setId(model.getId());
        if(model.getDisplay_nameDirtyFlag())
            domain.setDisplayName(model.getDisplay_name());
        if(model.getActiveDirtyFlag())
            domain.setActive(model.getActive());
        if(model.getTools_descriptionDirtyFlag())
            domain.setToolsDescription(model.getTools_description());
        if(model.getLabor_descriptionDirtyFlag())
            domain.setLaborDescription(model.getLabor_description());
        if(model.getWrite_dateDirtyFlag())
            domain.setWriteDate(model.getWrite_date());
        if(model.getMaintenance_typeDirtyFlag())
            domain.setMaintenanceType(model.getMaintenance_type());
        if(model.getOperations_descriptionDirtyFlag())
            domain.setOperationsDescription(model.getOperations_description());
        if(model.getDocumentation_descriptionDirtyFlag())
            domain.setDocumentationDescription(model.getDocumentation_description());
        if(model.getCreate_dateDirtyFlag())
            domain.setCreateDate(model.getCreate_date());
        if(model.get__last_updateDirtyFlag())
            domain.setLastUpdate(model.get__last_update());
        if(model.getWrite_uid_textDirtyFlag())
            domain.setWriteUidText(model.getWrite_uid_text());
        if(model.getCategory_id_textDirtyFlag())
            domain.setCategoryIdText(model.getCategory_id_text());
        if(model.getCreate_uid_textDirtyFlag())
            domain.setCreateUidText(model.getCreate_uid_text());
        if(model.getCategory_idDirtyFlag())
            domain.setCategoryId(model.getCategory_id());
        if(model.getCreate_uidDirtyFlag())
            domain.setCreateUid(model.getCreate_uid());
        if(model.getWrite_uidDirtyFlag())
            domain.setWriteUid(model.getWrite_uid());
        return domain ;
    }

}

    



