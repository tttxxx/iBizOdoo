package cn.ibizlab.odoo.core.odoo_crm.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_crm.domain.Crm_partner_binding;
import cn.ibizlab.odoo.core.odoo_crm.filter.Crm_partner_bindingSearchContext;
import cn.ibizlab.odoo.core.odoo_crm.service.ICrm_partner_bindingService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;



import cn.ibizlab.odoo.core.odoo_crm.client.crm_partner_bindingOdooClient;
import cn.ibizlab.odoo.core.odoo_crm.clientmodel.crm_partner_bindingClientModel;

/**
 * 实体[在CRM向导中处理业务伙伴的绑定或生成。] 服务对象接口实现
 */
@Slf4j
@Service
public class Crm_partner_bindingServiceImpl implements ICrm_partner_bindingService {

    @Autowired
    crm_partner_bindingOdooClient crm_partner_bindingOdooClient;


    @Override
    public Crm_partner_binding get(Integer id) {
        crm_partner_bindingClientModel clientModel = new crm_partner_bindingClientModel();
        clientModel.setId(id);
		crm_partner_bindingOdooClient.get(clientModel);
        Crm_partner_binding et = convert2Domain(clientModel,null);
        if(et==null){
            et=new Crm_partner_binding();
            et.setId(id);
        }
        else{
        }

        return  et;
    }

    @Override
    public boolean update(Crm_partner_binding et) {
        crm_partner_bindingClientModel clientModel = convert2Model(et,null);
		crm_partner_bindingOdooClient.update(clientModel);
        Crm_partner_binding rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }
    
    public void updateBatch(List<Crm_partner_binding> list){
    }

    @Override
    public boolean create(Crm_partner_binding et) {
        crm_partner_bindingClientModel clientModel = convert2Model(et,null);
		crm_partner_bindingOdooClient.create(clientModel);
        Crm_partner_binding rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Crm_partner_binding> list){
    }

    @Override
    public boolean remove(Integer id) {
        crm_partner_bindingClientModel clientModel = new crm_partner_bindingClientModel();
        clientModel.setId(id);
		crm_partner_bindingOdooClient.remove(clientModel);
        return true; 
    }

    public void removeBatch(Collection<Integer> idList){
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Crm_partner_binding> searchDefault(Crm_partner_bindingSearchContext context) {
        List<Crm_partner_binding> list = new ArrayList<Crm_partner_binding>();
        Page<crm_partner_bindingClientModel> clientModelList = crm_partner_bindingOdooClient.search(context);
        for(crm_partner_bindingClientModel clientModel : clientModelList){
            list.add( this.convert2Domain(clientModel,null));
        }
        return new PageImpl<Crm_partner_binding>(list, context.getPageable(), clientModelList.getTotalElements());
    }

    public crm_partner_bindingClientModel convert2Model(Crm_partner_binding domain , crm_partner_bindingClientModel model) {
        if(domain == null)
            return model ;
        if(model==null){
            model = new crm_partner_bindingClientModel();

        }
        if((Boolean) domain.getExtensionparams().get("dirtyflagenable")){
            if((Boolean) domain.getExtensionparams().get("write_datedirtyflag"))
                model.setWrite_date(domain.getWriteDate());
            if((Boolean) domain.getExtensionparams().get("display_namedirtyflag"))
                model.setDisplay_name(domain.getDisplayName());
            if((Boolean) domain.getExtensionparams().get("actiondirtyflag"))
                model.setAction(domain.getAction());
            if((Boolean) domain.getExtensionparams().get("__last_updatedirtyflag"))
                model.set__last_update(domain.getLastUpdate());
            if((Boolean) domain.getExtensionparams().get("create_datedirtyflag"))
                model.setCreate_date(domain.getCreateDate());
            if((Boolean) domain.getExtensionparams().get("iddirtyflag"))
                model.setId(domain.getId());
            if((Boolean) domain.getExtensionparams().get("create_uid_textdirtyflag"))
                model.setCreate_uid_text(domain.getCreateUidText());
            if((Boolean) domain.getExtensionparams().get("write_uid_textdirtyflag"))
                model.setWrite_uid_text(domain.getWriteUidText());
            if((Boolean) domain.getExtensionparams().get("partner_id_textdirtyflag"))
                model.setPartner_id_text(domain.getPartnerIdText());
            if((Boolean) domain.getExtensionparams().get("create_uiddirtyflag"))
                model.setCreate_uid(domain.getCreateUid());
            if((Boolean) domain.getExtensionparams().get("partner_iddirtyflag"))
                model.setPartner_id(domain.getPartnerId());
            if((Boolean) domain.getExtensionparams().get("write_uiddirtyflag"))
                model.setWrite_uid(domain.getWriteUid());
            domain.getFocusNull().clear();
        }
        return model ;
    }

    public Crm_partner_binding convert2Domain( crm_partner_bindingClientModel model ,Crm_partner_binding domain) {
        if(model == null)
            return domain ;
        if(domain == null){
            domain = new Crm_partner_binding();
        }

        if(model.getWrite_dateDirtyFlag())
            domain.setWriteDate(model.getWrite_date());
        if(model.getDisplay_nameDirtyFlag())
            domain.setDisplayName(model.getDisplay_name());
        if(model.getActionDirtyFlag())
            domain.setAction(model.getAction());
        if(model.get__last_updateDirtyFlag())
            domain.setLastUpdate(model.get__last_update());
        if(model.getCreate_dateDirtyFlag())
            domain.setCreateDate(model.getCreate_date());
        if(model.getIdDirtyFlag())
            domain.setId(model.getId());
        if(model.getCreate_uid_textDirtyFlag())
            domain.setCreateUidText(model.getCreate_uid_text());
        if(model.getWrite_uid_textDirtyFlag())
            domain.setWriteUidText(model.getWrite_uid_text());
        if(model.getPartner_id_textDirtyFlag())
            domain.setPartnerIdText(model.getPartner_id_text());
        if(model.getCreate_uidDirtyFlag())
            domain.setCreateUid(model.getCreate_uid());
        if(model.getPartner_idDirtyFlag())
            domain.setPartnerId(model.getPartner_id());
        if(model.getWrite_uidDirtyFlag())
            domain.setWriteUid(model.getWrite_uid());
        return domain ;
    }

}

    



