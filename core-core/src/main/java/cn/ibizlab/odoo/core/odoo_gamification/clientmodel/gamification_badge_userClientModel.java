package cn.ibizlab.odoo.core.odoo_gamification.clientmodel;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.math.BigInteger;
import java.io.Serializable;
import java.math.BigDecimal;
import cn.ibizlab.odoo.util.helper.OdooClientHelper;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang.ArrayUtils;
import org.springframework.util.StringUtils;

/**
 * 接口实体[gamification_badge_user] 对象
 */
public class gamification_badge_userClientModel implements Serializable{

    /**
     * 徽章
     */
    public Integer badge_id;

    @JsonIgnore
    public boolean badge_idDirtyFlag;
    
    /**
     * 徽章名称
     */
    public String badge_name;

    @JsonIgnore
    public boolean badge_nameDirtyFlag;
    
    /**
     * 挑战源于
     */
    public Integer challenge_id;

    @JsonIgnore
    public boolean challenge_idDirtyFlag;
    
    /**
     * 挑战源于
     */
    public String challenge_id_text;

    @JsonIgnore
    public boolean challenge_id_textDirtyFlag;
    
    /**
     * 备注
     */
    public String comment;

    @JsonIgnore
    public boolean commentDirtyFlag;
    
    /**
     * 创建时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp create_date;

    @JsonIgnore
    public boolean create_dateDirtyFlag;
    
    /**
     * 创建人
     */
    public Integer create_uid;

    @JsonIgnore
    public boolean create_uidDirtyFlag;
    
    /**
     * 创建人
     */
    public String create_uid_text;

    @JsonIgnore
    public boolean create_uid_textDirtyFlag;
    
    /**
     * 显示名称
     */
    public String display_name;

    @JsonIgnore
    public boolean display_nameDirtyFlag;
    
    /**
     * 员工
     */
    public Integer employee_id;

    @JsonIgnore
    public boolean employee_idDirtyFlag;
    
    /**
     * 员工
     */
    public String employee_id_text;

    @JsonIgnore
    public boolean employee_id_textDirtyFlag;
    
    /**
     * ID
     */
    public Integer id;

    @JsonIgnore
    public boolean idDirtyFlag;
    
    /**
     * 论坛徽章等级
     */
    public String level;

    @JsonIgnore
    public boolean levelDirtyFlag;
    
    /**
     * 发送者
     */
    public Integer sender_id;

    @JsonIgnore
    public boolean sender_idDirtyFlag;
    
    /**
     * 发送者
     */
    public String sender_id_text;

    @JsonIgnore
    public boolean sender_id_textDirtyFlag;
    
    /**
     * 用户
     */
    public Integer user_id;

    @JsonIgnore
    public boolean user_idDirtyFlag;
    
    /**
     * 用户
     */
    public String user_id_text;

    @JsonIgnore
    public boolean user_id_textDirtyFlag;
    
    /**
     * 最后更新时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp write_date;

    @JsonIgnore
    public boolean write_dateDirtyFlag;
    
    /**
     * 最后更新人
     */
    public Integer write_uid;

    @JsonIgnore
    public boolean write_uidDirtyFlag;
    
    /**
     * 最后更新人
     */
    public String write_uid_text;

    @JsonIgnore
    public boolean write_uid_textDirtyFlag;
    
    /**
     * 最后修改日
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    public Timestamp __last_update;

    @JsonIgnore
    public boolean __last_updateDirtyFlag;
    
    /**
     * 获取 [徽章]
     */
    @JsonProperty("badge_id")
    public Integer getBadge_id(){
        return this.badge_id ;
    }

    /**
     * 设置 [徽章]
     */
    @JsonProperty("badge_id")
    public void setBadge_id(Integer  badge_id){
        this.badge_id = badge_id ;
        this.badge_idDirtyFlag = true ;
    }

     /**
     * 获取 [徽章]脏标记
     */
    @JsonIgnore
    public boolean getBadge_idDirtyFlag(){
        return this.badge_idDirtyFlag ;
    }   

    /**
     * 获取 [徽章名称]
     */
    @JsonProperty("badge_name")
    public String getBadge_name(){
        return this.badge_name ;
    }

    /**
     * 设置 [徽章名称]
     */
    @JsonProperty("badge_name")
    public void setBadge_name(String  badge_name){
        this.badge_name = badge_name ;
        this.badge_nameDirtyFlag = true ;
    }

     /**
     * 获取 [徽章名称]脏标记
     */
    @JsonIgnore
    public boolean getBadge_nameDirtyFlag(){
        return this.badge_nameDirtyFlag ;
    }   

    /**
     * 获取 [挑战源于]
     */
    @JsonProperty("challenge_id")
    public Integer getChallenge_id(){
        return this.challenge_id ;
    }

    /**
     * 设置 [挑战源于]
     */
    @JsonProperty("challenge_id")
    public void setChallenge_id(Integer  challenge_id){
        this.challenge_id = challenge_id ;
        this.challenge_idDirtyFlag = true ;
    }

     /**
     * 获取 [挑战源于]脏标记
     */
    @JsonIgnore
    public boolean getChallenge_idDirtyFlag(){
        return this.challenge_idDirtyFlag ;
    }   

    /**
     * 获取 [挑战源于]
     */
    @JsonProperty("challenge_id_text")
    public String getChallenge_id_text(){
        return this.challenge_id_text ;
    }

    /**
     * 设置 [挑战源于]
     */
    @JsonProperty("challenge_id_text")
    public void setChallenge_id_text(String  challenge_id_text){
        this.challenge_id_text = challenge_id_text ;
        this.challenge_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [挑战源于]脏标记
     */
    @JsonIgnore
    public boolean getChallenge_id_textDirtyFlag(){
        return this.challenge_id_textDirtyFlag ;
    }   

    /**
     * 获取 [备注]
     */
    @JsonProperty("comment")
    public String getComment(){
        return this.comment ;
    }

    /**
     * 设置 [备注]
     */
    @JsonProperty("comment")
    public void setComment(String  comment){
        this.comment = comment ;
        this.commentDirtyFlag = true ;
    }

     /**
     * 获取 [备注]脏标记
     */
    @JsonIgnore
    public boolean getCommentDirtyFlag(){
        return this.commentDirtyFlag ;
    }   

    /**
     * 获取 [创建时间]
     */
    @JsonProperty("create_date")
    public Timestamp getCreate_date(){
        return this.create_date ;
    }

    /**
     * 设置 [创建时间]
     */
    @JsonProperty("create_date")
    public void setCreate_date(Timestamp  create_date){
        this.create_date = create_date ;
        this.create_dateDirtyFlag = true ;
    }

     /**
     * 获取 [创建时间]脏标记
     */
    @JsonIgnore
    public boolean getCreate_dateDirtyFlag(){
        return this.create_dateDirtyFlag ;
    }   

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid")
    public Integer getCreate_uid(){
        return this.create_uid ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid")
    public void setCreate_uid(Integer  create_uid){
        this.create_uid = create_uid ;
        this.create_uidDirtyFlag = true ;
    }

     /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uidDirtyFlag(){
        return this.create_uidDirtyFlag ;
    }   

    /**
     * 获取 [创建人]
     */
    @JsonProperty("create_uid_text")
    public String getCreate_uid_text(){
        return this.create_uid_text ;
    }

    /**
     * 设置 [创建人]
     */
    @JsonProperty("create_uid_text")
    public void setCreate_uid_text(String  create_uid_text){
        this.create_uid_text = create_uid_text ;
        this.create_uid_textDirtyFlag = true ;
    }

     /**
     * 获取 [创建人]脏标记
     */
    @JsonIgnore
    public boolean getCreate_uid_textDirtyFlag(){
        return this.create_uid_textDirtyFlag ;
    }   

    /**
     * 获取 [显示名称]
     */
    @JsonProperty("display_name")
    public String getDisplay_name(){
        return this.display_name ;
    }

    /**
     * 设置 [显示名称]
     */
    @JsonProperty("display_name")
    public void setDisplay_name(String  display_name){
        this.display_name = display_name ;
        this.display_nameDirtyFlag = true ;
    }

     /**
     * 获取 [显示名称]脏标记
     */
    @JsonIgnore
    public boolean getDisplay_nameDirtyFlag(){
        return this.display_nameDirtyFlag ;
    }   

    /**
     * 获取 [员工]
     */
    @JsonProperty("employee_id")
    public Integer getEmployee_id(){
        return this.employee_id ;
    }

    /**
     * 设置 [员工]
     */
    @JsonProperty("employee_id")
    public void setEmployee_id(Integer  employee_id){
        this.employee_id = employee_id ;
        this.employee_idDirtyFlag = true ;
    }

     /**
     * 获取 [员工]脏标记
     */
    @JsonIgnore
    public boolean getEmployee_idDirtyFlag(){
        return this.employee_idDirtyFlag ;
    }   

    /**
     * 获取 [员工]
     */
    @JsonProperty("employee_id_text")
    public String getEmployee_id_text(){
        return this.employee_id_text ;
    }

    /**
     * 设置 [员工]
     */
    @JsonProperty("employee_id_text")
    public void setEmployee_id_text(String  employee_id_text){
        this.employee_id_text = employee_id_text ;
        this.employee_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [员工]脏标记
     */
    @JsonIgnore
    public boolean getEmployee_id_textDirtyFlag(){
        return this.employee_id_textDirtyFlag ;
    }   

    /**
     * 获取 [ID]
     */
    @JsonProperty("id")
    public Integer getId(){
        return this.id ;
    }

    /**
     * 设置 [ID]
     */
    @JsonProperty("id")
    public void setId(Integer  id){
        this.id = id ;
        this.idDirtyFlag = true ;
    }

     /**
     * 获取 [ID]脏标记
     */
    @JsonIgnore
    public boolean getIdDirtyFlag(){
        return this.idDirtyFlag ;
    }   

    /**
     * 获取 [论坛徽章等级]
     */
    @JsonProperty("level")
    public String getLevel(){
        return this.level ;
    }

    /**
     * 设置 [论坛徽章等级]
     */
    @JsonProperty("level")
    public void setLevel(String  level){
        this.level = level ;
        this.levelDirtyFlag = true ;
    }

     /**
     * 获取 [论坛徽章等级]脏标记
     */
    @JsonIgnore
    public boolean getLevelDirtyFlag(){
        return this.levelDirtyFlag ;
    }   

    /**
     * 获取 [发送者]
     */
    @JsonProperty("sender_id")
    public Integer getSender_id(){
        return this.sender_id ;
    }

    /**
     * 设置 [发送者]
     */
    @JsonProperty("sender_id")
    public void setSender_id(Integer  sender_id){
        this.sender_id = sender_id ;
        this.sender_idDirtyFlag = true ;
    }

     /**
     * 获取 [发送者]脏标记
     */
    @JsonIgnore
    public boolean getSender_idDirtyFlag(){
        return this.sender_idDirtyFlag ;
    }   

    /**
     * 获取 [发送者]
     */
    @JsonProperty("sender_id_text")
    public String getSender_id_text(){
        return this.sender_id_text ;
    }

    /**
     * 设置 [发送者]
     */
    @JsonProperty("sender_id_text")
    public void setSender_id_text(String  sender_id_text){
        this.sender_id_text = sender_id_text ;
        this.sender_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [发送者]脏标记
     */
    @JsonIgnore
    public boolean getSender_id_textDirtyFlag(){
        return this.sender_id_textDirtyFlag ;
    }   

    /**
     * 获取 [用户]
     */
    @JsonProperty("user_id")
    public Integer getUser_id(){
        return this.user_id ;
    }

    /**
     * 设置 [用户]
     */
    @JsonProperty("user_id")
    public void setUser_id(Integer  user_id){
        this.user_id = user_id ;
        this.user_idDirtyFlag = true ;
    }

     /**
     * 获取 [用户]脏标记
     */
    @JsonIgnore
    public boolean getUser_idDirtyFlag(){
        return this.user_idDirtyFlag ;
    }   

    /**
     * 获取 [用户]
     */
    @JsonProperty("user_id_text")
    public String getUser_id_text(){
        return this.user_id_text ;
    }

    /**
     * 设置 [用户]
     */
    @JsonProperty("user_id_text")
    public void setUser_id_text(String  user_id_text){
        this.user_id_text = user_id_text ;
        this.user_id_textDirtyFlag = true ;
    }

     /**
     * 获取 [用户]脏标记
     */
    @JsonIgnore
    public boolean getUser_id_textDirtyFlag(){
        return this.user_id_textDirtyFlag ;
    }   

    /**
     * 获取 [最后更新时间]
     */
    @JsonProperty("write_date")
    public Timestamp getWrite_date(){
        return this.write_date ;
    }

    /**
     * 设置 [最后更新时间]
     */
    @JsonProperty("write_date")
    public void setWrite_date(Timestamp  write_date){
        this.write_date = write_date ;
        this.write_dateDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新时间]脏标记
     */
    @JsonIgnore
    public boolean getWrite_dateDirtyFlag(){
        return this.write_dateDirtyFlag ;
    }   

    /**
     * 获取 [最后更新人]
     */
    @JsonProperty("write_uid")
    public Integer getWrite_uid(){
        return this.write_uid ;
    }

    /**
     * 设置 [最后更新人]
     */
    @JsonProperty("write_uid")
    public void setWrite_uid(Integer  write_uid){
        this.write_uid = write_uid ;
        this.write_uidDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新人]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uidDirtyFlag(){
        return this.write_uidDirtyFlag ;
    }   

    /**
     * 获取 [最后更新人]
     */
    @JsonProperty("write_uid_text")
    public String getWrite_uid_text(){
        return this.write_uid_text ;
    }

    /**
     * 设置 [最后更新人]
     */
    @JsonProperty("write_uid_text")
    public void setWrite_uid_text(String  write_uid_text){
        this.write_uid_text = write_uid_text ;
        this.write_uid_textDirtyFlag = true ;
    }

     /**
     * 获取 [最后更新人]脏标记
     */
    @JsonIgnore
    public boolean getWrite_uid_textDirtyFlag(){
        return this.write_uid_textDirtyFlag ;
    }   

    /**
     * 获取 [最后修改日]
     */
    @JsonProperty("__last_update")
    public Timestamp get__last_update(){
        return this.__last_update ;
    }

    /**
     * 设置 [最后修改日]
     */
    @JsonProperty("__last_update")
    public void set__last_update(Timestamp  __last_update){
        this.__last_update = __last_update ;
        this.__last_updateDirtyFlag = true ;
    }

     /**
     * 获取 [最后修改日]脏标记
     */
    @JsonIgnore
    public boolean get__last_updateDirtyFlag(){
        return this.__last_updateDirtyFlag ;
    }   



    public void fromMap(Map<String, Object> map) throws Exception {
		if(!(map.get("badge_id") instanceof Boolean)&& map.get("badge_id")!=null){
			Object[] objs = (Object[])map.get("badge_id");
			if(objs.length > 0){
				this.setBadge_id((Integer)objs[0]);
			}
		}
		if(!(map.get("badge_name") instanceof Boolean)&& map.get("badge_name")!=null){
			this.setBadge_name((String)map.get("badge_name"));
		}
		if(!(map.get("challenge_id") instanceof Boolean)&& map.get("challenge_id")!=null){
			Object[] objs = (Object[])map.get("challenge_id");
			if(objs.length > 0){
				this.setChallenge_id((Integer)objs[0]);
			}
		}
		if(!(map.get("challenge_id") instanceof Boolean)&& map.get("challenge_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("challenge_id");
			if(objs.length > 1){
				this.setChallenge_id_text((String)objs[1]);
			}
		}
		if(!(map.get("comment") instanceof Boolean)&& map.get("comment")!=null){
			this.setComment((String)map.get("comment"));
		}
		if(!(map.get("create_date") instanceof Boolean)&& map.get("create_date")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse((String)map.get("create_date"));
   			this.setCreate_date(new Timestamp(parse.getTime()));
		}
		if(!(map.get("create_uid") instanceof Boolean)&& map.get("create_uid")!=null){
			Object[] objs = (Object[])map.get("create_uid");
			if(objs.length > 0){
				this.setCreate_uid((Integer)objs[0]);
			}
		}
		if(!(map.get("create_uid") instanceof Boolean)&& map.get("create_uid")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("create_uid");
			if(objs.length > 1){
				this.setCreate_uid_text((String)objs[1]);
			}
		}
		if(!(map.get("display_name") instanceof Boolean)&& map.get("display_name")!=null){
			this.setDisplay_name((String)map.get("display_name"));
		}
		if(!(map.get("employee_id") instanceof Boolean)&& map.get("employee_id")!=null){
			Object[] objs = (Object[])map.get("employee_id");
			if(objs.length > 0){
				this.setEmployee_id((Integer)objs[0]);
			}
		}
		if(!(map.get("employee_id") instanceof Boolean)&& map.get("employee_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("employee_id");
			if(objs.length > 1){
				this.setEmployee_id_text((String)objs[1]);
			}
		}
		if(!(map.get("id") instanceof Boolean)&& map.get("id")!=null){
			this.setId((Integer)map.get("id"));
		}
		if(!(map.get("level") instanceof Boolean)&& map.get("level")!=null){
			this.setLevel((String)map.get("level"));
		}
		if(!(map.get("sender_id") instanceof Boolean)&& map.get("sender_id")!=null){
			Object[] objs = (Object[])map.get("sender_id");
			if(objs.length > 0){
				this.setSender_id((Integer)objs[0]);
			}
		}
		if(!(map.get("sender_id") instanceof Boolean)&& map.get("sender_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("sender_id");
			if(objs.length > 1){
				this.setSender_id_text((String)objs[1]);
			}
		}
		if(!(map.get("user_id") instanceof Boolean)&& map.get("user_id")!=null){
			Object[] objs = (Object[])map.get("user_id");
			if(objs.length > 0){
				this.setUser_id((Integer)objs[0]);
			}
		}
		if(!(map.get("user_id") instanceof Boolean)&& map.get("user_id")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("user_id");
			if(objs.length > 1){
				this.setUser_id_text((String)objs[1]);
			}
		}
		if(!(map.get("write_date") instanceof Boolean)&& map.get("write_date")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse((String)map.get("write_date"));
   			this.setWrite_date(new Timestamp(parse.getTime()));
		}
		if(!(map.get("write_uid") instanceof Boolean)&& map.get("write_uid")!=null){
			Object[] objs = (Object[])map.get("write_uid");
			if(objs.length > 0){
				this.setWrite_uid((Integer)objs[0]);
			}
		}
		if(!(map.get("write_uid") instanceof Boolean)&& map.get("write_uid")!=null){
			//外键文本转化
			Object[] objs = (Object[])map.get("write_uid");
			if(objs.length > 1){
				this.setWrite_uid_text((String)objs[1]);
			}
		}
		if(!(map.get("__last_update") instanceof Boolean)&& map.get("__last_update")!=null){
			Date parse = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse((String)map.get("__last_update"));
   			this.set__last_update(new Timestamp(parse.getTime()));
		}
	}

	public Map<String, Object> toMap() throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		if(this.getBadge_id()!=null&&this.getBadge_idDirtyFlag()){
			map.put("badge_id",this.getBadge_id());
		}else if(this.getBadge_idDirtyFlag()){
			map.put("badge_id",false);
		}
		if(this.getBadge_name()!=null&&this.getBadge_nameDirtyFlag()){
			map.put("badge_name",this.getBadge_name());
		}else if(this.getBadge_nameDirtyFlag()){
			map.put("badge_name",false);
		}
		if(this.getChallenge_id()!=null&&this.getChallenge_idDirtyFlag()){
			map.put("challenge_id",this.getChallenge_id());
		}else if(this.getChallenge_idDirtyFlag()){
			map.put("challenge_id",false);
		}
		if(this.getChallenge_id_text()!=null&&this.getChallenge_id_textDirtyFlag()){
			//忽略文本外键challenge_id_text
		}else if(this.getChallenge_id_textDirtyFlag()){
			map.put("challenge_id",false);
		}
		if(this.getComment()!=null&&this.getCommentDirtyFlag()){
			map.put("comment",this.getComment());
		}else if(this.getCommentDirtyFlag()){
			map.put("comment",false);
		}
		if(this.getCreate_date()!=null&&this.getCreate_dateDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String datetimeStr = sdf.format(this.getCreate_date());
			map.put("create_date",datetimeStr);
		}else if(this.getCreate_dateDirtyFlag()){
			map.put("create_date",false);
		}
		if(this.getCreate_uid()!=null&&this.getCreate_uidDirtyFlag()){
			map.put("create_uid",this.getCreate_uid());
		}else if(this.getCreate_uidDirtyFlag()){
			map.put("create_uid",false);
		}
		if(this.getCreate_uid_text()!=null&&this.getCreate_uid_textDirtyFlag()){
			//忽略文本外键create_uid_text
		}else if(this.getCreate_uid_textDirtyFlag()){
			map.put("create_uid",false);
		}
		if(this.getDisplay_name()!=null&&this.getDisplay_nameDirtyFlag()){
			map.put("display_name",this.getDisplay_name());
		}else if(this.getDisplay_nameDirtyFlag()){
			map.put("display_name",false);
		}
		if(this.getEmployee_id()!=null&&this.getEmployee_idDirtyFlag()){
			map.put("employee_id",this.getEmployee_id());
		}else if(this.getEmployee_idDirtyFlag()){
			map.put("employee_id",false);
		}
		if(this.getEmployee_id_text()!=null&&this.getEmployee_id_textDirtyFlag()){
			//忽略文本外键employee_id_text
		}else if(this.getEmployee_id_textDirtyFlag()){
			map.put("employee_id",false);
		}
		if(this.getId()!=null&&this.getIdDirtyFlag()){
			map.put("id",this.getId());
		}else if(this.getIdDirtyFlag()){
			map.put("id",false);
		}
		if(this.getLevel()!=null&&this.getLevelDirtyFlag()){
			map.put("level",this.getLevel());
		}else if(this.getLevelDirtyFlag()){
			map.put("level",false);
		}
		if(this.getSender_id()!=null&&this.getSender_idDirtyFlag()){
			map.put("sender_id",this.getSender_id());
		}else if(this.getSender_idDirtyFlag()){
			map.put("sender_id",false);
		}
		if(this.getSender_id_text()!=null&&this.getSender_id_textDirtyFlag()){
			//忽略文本外键sender_id_text
		}else if(this.getSender_id_textDirtyFlag()){
			map.put("sender_id",false);
		}
		if(this.getUser_id()!=null&&this.getUser_idDirtyFlag()){
			map.put("user_id",this.getUser_id());
		}else if(this.getUser_idDirtyFlag()){
			map.put("user_id",false);
		}
		if(this.getUser_id_text()!=null&&this.getUser_id_textDirtyFlag()){
			//忽略文本外键user_id_text
		}else if(this.getUser_id_textDirtyFlag()){
			map.put("user_id",false);
		}
		if(this.getWrite_date()!=null&&this.getWrite_dateDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String datetimeStr = sdf.format(this.getWrite_date());
			map.put("write_date",datetimeStr);
		}else if(this.getWrite_dateDirtyFlag()){
			map.put("write_date",false);
		}
		if(this.getWrite_uid()!=null&&this.getWrite_uidDirtyFlag()){
			map.put("write_uid",this.getWrite_uid());
		}else if(this.getWrite_uidDirtyFlag()){
			map.put("write_uid",false);
		}
		if(this.getWrite_uid_text()!=null&&this.getWrite_uid_textDirtyFlag()){
			//忽略文本外键write_uid_text
		}else if(this.getWrite_uid_textDirtyFlag()){
			map.put("write_uid",false);
		}
		if(this.get__last_update()!=null&&this.get__last_updateDirtyFlag()){
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String datetimeStr = sdf.format(this.get__last_update());
			map.put("__last_update",datetimeStr);
		}else if(this.get__last_updateDirtyFlag()){
			map.put("__last_update",false);
		}
		return map;
	}

}
