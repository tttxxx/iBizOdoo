package cn.ibizlab.odoo.core.odoo_sale.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_sale.domain.Sale_order_template_line;
import cn.ibizlab.odoo.core.odoo_sale.filter.Sale_order_template_lineSearchContext;
import cn.ibizlab.odoo.core.odoo_sale.service.ISale_order_template_lineService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;



import cn.ibizlab.odoo.core.odoo_sale.client.sale_order_template_lineOdooClient;
import cn.ibizlab.odoo.core.odoo_sale.clientmodel.sale_order_template_lineClientModel;

/**
 * 实体[报价单模板行] 服务对象接口实现
 */
@Slf4j
@Service
public class Sale_order_template_lineServiceImpl implements ISale_order_template_lineService {

    @Autowired
    sale_order_template_lineOdooClient sale_order_template_lineOdooClient;


    @Override
    public boolean remove(Integer id) {
        sale_order_template_lineClientModel clientModel = new sale_order_template_lineClientModel();
        clientModel.setId(id);
		sale_order_template_lineOdooClient.remove(clientModel);
        return true; 
    }

    public void removeBatch(Collection<Integer> idList){
    }

    @Override
    public boolean create(Sale_order_template_line et) {
        sale_order_template_lineClientModel clientModel = convert2Model(et,null);
		sale_order_template_lineOdooClient.create(clientModel);
        Sale_order_template_line rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Sale_order_template_line> list){
    }

    @Override
    public Sale_order_template_line get(Integer id) {
        sale_order_template_lineClientModel clientModel = new sale_order_template_lineClientModel();
        clientModel.setId(id);
		sale_order_template_lineOdooClient.get(clientModel);
        Sale_order_template_line et = convert2Domain(clientModel,null);
        if(et==null){
            et=new Sale_order_template_line();
            et.setId(id);
        }
        else{
        }

        return  et;
    }

    @Override
    public boolean update(Sale_order_template_line et) {
        sale_order_template_lineClientModel clientModel = convert2Model(et,null);
		sale_order_template_lineOdooClient.update(clientModel);
        Sale_order_template_line rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }
    
    public void updateBatch(List<Sale_order_template_line> list){
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Sale_order_template_line> searchDefault(Sale_order_template_lineSearchContext context) {
        List<Sale_order_template_line> list = new ArrayList<Sale_order_template_line>();
        Page<sale_order_template_lineClientModel> clientModelList = sale_order_template_lineOdooClient.search(context);
        for(sale_order_template_lineClientModel clientModel : clientModelList){
            list.add( this.convert2Domain(clientModel,null));
        }
        return new PageImpl<Sale_order_template_line>(list, context.getPageable(), clientModelList.getTotalElements());
    }

    public sale_order_template_lineClientModel convert2Model(Sale_order_template_line domain , sale_order_template_lineClientModel model) {
        if(domain == null)
            return model ;
        if(model==null){
            model = new sale_order_template_lineClientModel();

        }
        if((Boolean) domain.getExtensionparams().get("dirtyflagenable")){
            if((Boolean) domain.getExtensionparams().get("create_datedirtyflag"))
                model.setCreate_date(domain.getCreateDate());
            if((Boolean) domain.getExtensionparams().get("iddirtyflag"))
                model.setId(domain.getId());
            if((Boolean) domain.getExtensionparams().get("sequencedirtyflag"))
                model.setSequence(domain.getSequence());
            if((Boolean) domain.getExtensionparams().get("product_uom_qtydirtyflag"))
                model.setProduct_uom_qty(domain.getProductUomQty());
            if((Boolean) domain.getExtensionparams().get("namedirtyflag"))
                model.setName(domain.getName());
            if((Boolean) domain.getExtensionparams().get("display_namedirtyflag"))
                model.setDisplay_name(domain.getDisplayName());
            if((Boolean) domain.getExtensionparams().get("write_datedirtyflag"))
                model.setWrite_date(domain.getWriteDate());
            if((Boolean) domain.getExtensionparams().get("discountdirtyflag"))
                model.setDiscount(domain.getDiscount());
            if((Boolean) domain.getExtensionparams().get("__last_updatedirtyflag"))
                model.set__last_update(domain.getLastUpdate());
            if((Boolean) domain.getExtensionparams().get("price_unitdirtyflag"))
                model.setPrice_unit(domain.getPriceUnit());
            if((Boolean) domain.getExtensionparams().get("display_typedirtyflag"))
                model.setDisplay_type(domain.getDisplayType());
            if((Boolean) domain.getExtensionparams().get("create_uid_textdirtyflag"))
                model.setCreate_uid_text(domain.getCreateUidText());
            if((Boolean) domain.getExtensionparams().get("product_id_textdirtyflag"))
                model.setProduct_id_text(domain.getProductIdText());
            if((Boolean) domain.getExtensionparams().get("write_uid_textdirtyflag"))
                model.setWrite_uid_text(domain.getWriteUidText());
            if((Boolean) domain.getExtensionparams().get("product_uom_id_textdirtyflag"))
                model.setProduct_uom_id_text(domain.getProductUomIdText());
            if((Boolean) domain.getExtensionparams().get("sale_order_template_id_textdirtyflag"))
                model.setSale_order_template_id_text(domain.getSaleOrderTemplateIdText());
            if((Boolean) domain.getExtensionparams().get("product_uom_iddirtyflag"))
                model.setProduct_uom_id(domain.getProductUomId());
            if((Boolean) domain.getExtensionparams().get("create_uiddirtyflag"))
                model.setCreate_uid(domain.getCreateUid());
            if((Boolean) domain.getExtensionparams().get("product_iddirtyflag"))
                model.setProduct_id(domain.getProductId());
            if((Boolean) domain.getExtensionparams().get("sale_order_template_iddirtyflag"))
                model.setSale_order_template_id(domain.getSaleOrderTemplateId());
            if((Boolean) domain.getExtensionparams().get("write_uiddirtyflag"))
                model.setWrite_uid(domain.getWriteUid());
            domain.getFocusNull().clear();
        }
        return model ;
    }

    public Sale_order_template_line convert2Domain( sale_order_template_lineClientModel model ,Sale_order_template_line domain) {
        if(model == null)
            return domain ;
        if(domain == null){
            domain = new Sale_order_template_line();
        }

        if(model.getCreate_dateDirtyFlag())
            domain.setCreateDate(model.getCreate_date());
        if(model.getIdDirtyFlag())
            domain.setId(model.getId());
        if(model.getSequenceDirtyFlag())
            domain.setSequence(model.getSequence());
        if(model.getProduct_uom_qtyDirtyFlag())
            domain.setProductUomQty(model.getProduct_uom_qty());
        if(model.getNameDirtyFlag())
            domain.setName(model.getName());
        if(model.getDisplay_nameDirtyFlag())
            domain.setDisplayName(model.getDisplay_name());
        if(model.getWrite_dateDirtyFlag())
            domain.setWriteDate(model.getWrite_date());
        if(model.getDiscountDirtyFlag())
            domain.setDiscount(model.getDiscount());
        if(model.get__last_updateDirtyFlag())
            domain.setLastUpdate(model.get__last_update());
        if(model.getPrice_unitDirtyFlag())
            domain.setPriceUnit(model.getPrice_unit());
        if(model.getDisplay_typeDirtyFlag())
            domain.setDisplayType(model.getDisplay_type());
        if(model.getCreate_uid_textDirtyFlag())
            domain.setCreateUidText(model.getCreate_uid_text());
        if(model.getProduct_id_textDirtyFlag())
            domain.setProductIdText(model.getProduct_id_text());
        if(model.getWrite_uid_textDirtyFlag())
            domain.setWriteUidText(model.getWrite_uid_text());
        if(model.getProduct_uom_id_textDirtyFlag())
            domain.setProductUomIdText(model.getProduct_uom_id_text());
        if(model.getSale_order_template_id_textDirtyFlag())
            domain.setSaleOrderTemplateIdText(model.getSale_order_template_id_text());
        if(model.getProduct_uom_idDirtyFlag())
            domain.setProductUomId(model.getProduct_uom_id());
        if(model.getCreate_uidDirtyFlag())
            domain.setCreateUid(model.getCreate_uid());
        if(model.getProduct_idDirtyFlag())
            domain.setProductId(model.getProduct_id());
        if(model.getSale_order_template_idDirtyFlag())
            domain.setSaleOrderTemplateId(model.getSale_order_template_id());
        if(model.getWrite_uidDirtyFlag())
            domain.setWriteUid(model.getWrite_uid());
        return domain ;
    }

}

    



