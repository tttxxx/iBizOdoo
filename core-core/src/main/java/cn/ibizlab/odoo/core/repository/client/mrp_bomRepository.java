package cn.ibizlab.odoo.core.repository.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import cn.ibizlab.odoo.core.repository.client.po.mrp_bom;

/**
 * 实体[mrp_bom] 服务对象接口
 */
public interface mrp_bomRepository{


    public mrp_bom createPO() ;
        public void create(mrp_bom mrp_bom);

        public void remove(String id);

        public void update(mrp_bom mrp_bom);

        public void get(String id);

        public void createBatch(mrp_bom mrp_bom);

        public void updateBatch(mrp_bom mrp_bom);

        public void removeBatch(String id);

        public List<mrp_bom> search();


}
