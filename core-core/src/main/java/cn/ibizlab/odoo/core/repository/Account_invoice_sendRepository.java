package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Account_invoice_send;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_invoice_sendSearchContext;

/**
 * 实体 [发送会计发票] 存储对象
 */
public interface Account_invoice_sendRepository extends Repository<Account_invoice_send> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Account_invoice_send> searchDefault(Account_invoice_sendSearchContext context);

    Account_invoice_send convert2PO(cn.ibizlab.odoo.core.odoo_account.domain.Account_invoice_send domain , Account_invoice_send po) ;

    cn.ibizlab.odoo.core.odoo_account.domain.Account_invoice_send convert2Domain( Account_invoice_send po ,cn.ibizlab.odoo.core.odoo_account.domain.Account_invoice_send domain) ;

}
