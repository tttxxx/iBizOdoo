package cn.ibizlab.odoo.core.repository.po ;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.math.BigInteger;

import cn.ibizlab.odoo.core.odoo_payment.filter.Payment_transactionSearchContext;

/**
 * 实体 [付款交易] 存储模型
 */
public interface Payment_transaction{

    /**
     * ID
     */
    Integer getId();

    void setId(Integer id);

    /**
     * 获取 [ID]脏标记
     */
    boolean getIdDirtyFlag();

    /**
     * 金额
     */
    Double getAmount();

    void setAmount(Double amount);

    /**
     * 获取 [金额]脏标记
     */
    boolean getAmountDirtyFlag();

    /**
     * 电话
     */
    String getPartner_phone();

    void setPartner_phone(String partner_phone);

    /**
     * 获取 [电话]脏标记
     */
    boolean getPartner_phoneDirtyFlag();

    /**
     * EMail
     */
    String getPartner_email();

    void setPartner_email(String partner_email);

    /**
     * 获取 [EMail]脏标记
     */
    boolean getPartner_emailDirtyFlag();

    /**
     * 回调方法
     */
    String getCallback_method();

    void setCallback_method(String callback_method);

    /**
     * 获取 [回调方法]脏标记
     */
    boolean getCallback_methodDirtyFlag();

    /**
     * 付款后返回网址
     */
    String getReturn_url();

    void setReturn_url(String return_url);

    /**
     * 获取 [付款后返回网址]脏标记
     */
    boolean getReturn_urlDirtyFlag();

    /**
     * # 销售订单
     */
    Integer getSale_order_ids_nbr();

    void setSale_order_ids_nbr(Integer sale_order_ids_nbr);

    /**
     * 获取 [# 销售订单]脏标记
     */
    boolean getSale_order_ids_nbrDirtyFlag();

    /**
     * 收单方参考
     */
    String getAcquirer_reference();

    void setAcquirer_reference(String acquirer_reference);

    /**
     * 获取 [收单方参考]脏标记
     */
    boolean getAcquirer_referenceDirtyFlag();

    /**
     * 语言
     */
    String getPartner_lang();

    void setPartner_lang(String partner_lang);

    /**
     * 获取 [语言]脏标记
     */
    boolean getPartner_langDirtyFlag();

    /**
     * 邮政编码
     */
    String getPartner_zip();

    void setPartner_zip(String partner_zip);

    /**
     * 获取 [邮政编码]脏标记
     */
    boolean getPartner_zipDirtyFlag();

    /**
     * 显示名称
     */
    String getDisplay_name();

    void setDisplay_name(String display_name);

    /**
     * 获取 [显示名称]脏标记
     */
    boolean getDisplay_nameDirtyFlag();

    /**
     * 状态
     */
    String getState();

    void setState(String state);

    /**
     * 获取 [状态]脏标记
     */
    boolean getStateDirtyFlag();

    /**
     * 验证日期
     */
    Timestamp getDate();

    void setDate(Timestamp date);

    /**
     * 获取 [验证日期]脏标记
     */
    boolean getDateDirtyFlag();

    /**
     * 回调文档模型
     */
    Integer getCallback_model_id();

    void setCallback_model_id(Integer callback_model_id);

    /**
     * 获取 [回调文档模型]脏标记
     */
    boolean getCallback_model_idDirtyFlag();

    /**
     * 最后修改日
     */
    Timestamp get__last_update();

    void set__last_update(Timestamp __last_update);

    /**
     * 获取 [最后修改日]脏标记
     */
    boolean get__last_updateDirtyFlag();

    /**
     * 发票
     */
    String getInvoice_ids();

    void setInvoice_ids(String invoice_ids);

    /**
     * 获取 [发票]脏标记
     */
    boolean getInvoice_idsDirtyFlag();

    /**
     * 销售订单
     */
    String getSale_order_ids();

    void setSale_order_ids(String sale_order_ids);

    /**
     * 获取 [销售订单]脏标记
     */
    boolean getSale_order_idsDirtyFlag();

    /**
     * 费用
     */
    Double getFees();

    void setFees(Double fees);

    /**
     * 获取 [费用]脏标记
     */
    boolean getFeesDirtyFlag();

    /**
     * 消息
     */
    String getState_message();

    void setState_message(String state_message);

    /**
     * 获取 [消息]脏标记
     */
    boolean getState_messageDirtyFlag();

    /**
     * 回调文档 ID
     */
    Integer getCallback_res_id();

    void setCallback_res_id(Integer callback_res_id);

    /**
     * 获取 [回调文档 ID]脏标记
     */
    boolean getCallback_res_idDirtyFlag();

    /**
     * 城市
     */
    String getPartner_city();

    void setPartner_city(String partner_city);

    /**
     * 获取 [城市]脏标记
     */
    boolean getPartner_cityDirtyFlag();

    /**
     * 最后更新时间
     */
    Timestamp getWrite_date();

    void setWrite_date(Timestamp write_date);

    /**
     * 获取 [最后更新时间]脏标记
     */
    boolean getWrite_dateDirtyFlag();

    /**
     * 创建时间
     */
    Timestamp getCreate_date();

    void setCreate_date(Timestamp create_date);

    /**
     * 获取 [创建时间]脏标记
     */
    boolean getCreate_dateDirtyFlag();

    /**
     * # 发票
     */
    Integer getInvoice_ids_nbr();

    void setInvoice_ids_nbr(Integer invoice_ids_nbr);

    /**
     * 获取 [# 发票]脏标记
     */
    boolean getInvoice_ids_nbrDirtyFlag();

    /**
     * 类型
     */
    String getType();

    void setType(String type);

    /**
     * 获取 [类型]脏标记
     */
    boolean getTypeDirtyFlag();

    /**
     * 3D Secure HTML
     */
    String getHtml_3ds();

    void setHtml_3ds(String html_3ds);

    /**
     * 获取 [3D Secure HTML]脏标记
     */
    boolean getHtml_3dsDirtyFlag();

    /**
     * 合作伙伴名称
     */
    String getPartner_name();

    void setPartner_name(String partner_name);

    /**
     * 获取 [合作伙伴名称]脏标记
     */
    boolean getPartner_nameDirtyFlag();

    /**
     * 参考
     */
    String getReference();

    void setReference(String reference);

    /**
     * 获取 [参考]脏标记
     */
    boolean getReferenceDirtyFlag();

    /**
     * 地址
     */
    String getPartner_address();

    void setPartner_address(String partner_address);

    /**
     * 获取 [地址]脏标记
     */
    boolean getPartner_addressDirtyFlag();

    /**
     * 付款是否已过账处理
     */
    String getIs_processed();

    void setIs_processed(String is_processed);

    /**
     * 获取 [付款是否已过账处理]脏标记
     */
    boolean getIs_processedDirtyFlag();

    /**
     * 回调哈希函数
     */
    String getCallback_hash();

    void setCallback_hash(String callback_hash);

    /**
     * 获取 [回调哈希函数]脏标记
     */
    boolean getCallback_hashDirtyFlag();

    /**
     * 币种
     */
    String getCurrency_id_text();

    void setCurrency_id_text(String currency_id_text);

    /**
     * 获取 [币种]脏标记
     */
    boolean getCurrency_id_textDirtyFlag();

    /**
     * 最后更新人
     */
    String getWrite_uid_text();

    void setWrite_uid_text(String write_uid_text);

    /**
     * 获取 [最后更新人]脏标记
     */
    boolean getWrite_uid_textDirtyFlag();

    /**
     * 创建人
     */
    String getCreate_uid_text();

    void setCreate_uid_text(String create_uid_text);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uid_textDirtyFlag();

    /**
     * 付款
     */
    String getPayment_id_text();

    void setPayment_id_text(String payment_id_text);

    /**
     * 获取 [付款]脏标记
     */
    boolean getPayment_id_textDirtyFlag();

    /**
     * 客户
     */
    String getPartner_id_text();

    void setPartner_id_text(String partner_id_text);

    /**
     * 获取 [客户]脏标记
     */
    boolean getPartner_id_textDirtyFlag();

    /**
     * 服务商
     */
    String getProvider();

    void setProvider(String provider);

    /**
     * 获取 [服务商]脏标记
     */
    boolean getProviderDirtyFlag();

    /**
     * 收单方
     */
    String getAcquirer_id_text();

    void setAcquirer_id_text(String acquirer_id_text);

    /**
     * 获取 [收单方]脏标记
     */
    boolean getAcquirer_id_textDirtyFlag();

    /**
     * 国家
     */
    String getPartner_country_id_text();

    void setPartner_country_id_text(String partner_country_id_text);

    /**
     * 获取 [国家]脏标记
     */
    boolean getPartner_country_id_textDirtyFlag();

    /**
     * 付款令牌
     */
    String getPayment_token_id_text();

    void setPayment_token_id_text(String payment_token_id_text);

    /**
     * 获取 [付款令牌]脏标记
     */
    boolean getPayment_token_id_textDirtyFlag();

    /**
     * 币种
     */
    Integer getCurrency_id();

    void setCurrency_id(Integer currency_id);

    /**
     * 获取 [币种]脏标记
     */
    boolean getCurrency_idDirtyFlag();

    /**
     * 最后更新人
     */
    Integer getWrite_uid();

    void setWrite_uid(Integer write_uid);

    /**
     * 获取 [最后更新人]脏标记
     */
    boolean getWrite_uidDirtyFlag();

    /**
     * 国家
     */
    Integer getPartner_country_id();

    void setPartner_country_id(Integer partner_country_id);

    /**
     * 获取 [国家]脏标记
     */
    boolean getPartner_country_idDirtyFlag();

    /**
     * 付款令牌
     */
    Integer getPayment_token_id();

    void setPayment_token_id(Integer payment_token_id);

    /**
     * 获取 [付款令牌]脏标记
     */
    boolean getPayment_token_idDirtyFlag();

    /**
     * 客户
     */
    Integer getPartner_id();

    void setPartner_id(Integer partner_id);

    /**
     * 获取 [客户]脏标记
     */
    boolean getPartner_idDirtyFlag();

    /**
     * 创建人
     */
    Integer getCreate_uid();

    void setCreate_uid(Integer create_uid);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uidDirtyFlag();

    /**
     * 付款
     */
    Integer getPayment_id();

    void setPayment_id(Integer payment_id);

    /**
     * 获取 [付款]脏标记
     */
    boolean getPayment_idDirtyFlag();

    /**
     * 收单方
     */
    Integer getAcquirer_id();

    void setAcquirer_id(Integer acquirer_id);

    /**
     * 获取 [收单方]脏标记
     */
    boolean getAcquirer_idDirtyFlag();

}
