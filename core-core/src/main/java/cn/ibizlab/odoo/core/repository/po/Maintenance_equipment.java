package cn.ibizlab.odoo.core.repository.po ;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.math.BigInteger;

import cn.ibizlab.odoo.core.odoo_maintenance.filter.Maintenance_equipmentSearchContext;

/**
 * 实体 [保养设备] 存储模型
 */
public interface Maintenance_equipment{

    /**
     * 下一活动摘要
     */
    String getActivity_summary();

    void setActivity_summary(String activity_summary);

    /**
     * 获取 [下一活动摘要]脏标记
     */
    boolean getActivity_summaryDirtyFlag();

    /**
     * 最后修改日
     */
    Timestamp get__last_update();

    void set__last_update(Timestamp __last_update);

    /**
     * 获取 [最后修改日]脏标记
     */
    boolean get__last_updateDirtyFlag();

    /**
     * 活动
     */
    String getActivity_ids();

    void setActivity_ids(String activity_ids);

    /**
     * 获取 [活动]脏标记
     */
    boolean getActivity_idsDirtyFlag();

    /**
     * 行动数量
     */
    Integer getMessage_needaction_counter();

    void setMessage_needaction_counter(Integer message_needaction_counter);

    /**
     * 获取 [行动数量]脏标记
     */
    boolean getMessage_needaction_counterDirtyFlag();

    /**
     * 未读消息
     */
    String getMessage_unread();

    void setMessage_unread(String message_unread);

    /**
     * 获取 [未读消息]脏标记
     */
    boolean getMessage_unreadDirtyFlag();

    /**
     * ID
     */
    Integer getId();

    void setId(Integer id);

    /**
     * 获取 [ID]脏标记
     */
    boolean getIdDirtyFlag();

    /**
     * 用于
     */
    String getEquipment_assign_to();

    void setEquipment_assign_to(String equipment_assign_to);

    /**
     * 获取 [用于]脏标记
     */
    boolean getEquipment_assign_toDirtyFlag();

    /**
     * 地点
     */
    String getLocation();

    void setLocation(String location);

    /**
     * 获取 [地点]脏标记
     */
    boolean getLocationDirtyFlag();

    /**
     * 保修截止日期
     */
    Timestamp getWarranty_date();

    void setWarranty_date(Timestamp warranty_date);

    /**
     * 获取 [保修截止日期]脏标记
     */
    boolean getWarranty_dateDirtyFlag();

    /**
     * 网站消息
     */
    String getWebsite_message_ids();

    void setWebsite_message_ids(String website_message_ids);

    /**
     * 获取 [网站消息]脏标记
     */
    boolean getWebsite_message_idsDirtyFlag();

    /**
     * 设备名称
     */
    String getName();

    void setName(String name);

    /**
     * 获取 [设备名称]脏标记
     */
    boolean getNameDirtyFlag();

    /**
     * 保养时长
     */
    Double getMaintenance_duration();

    void setMaintenance_duration(Double maintenance_duration);

    /**
     * 获取 [保养时长]脏标记
     */
    boolean getMaintenance_durationDirtyFlag();

    /**
     * 显示名称
     */
    String getDisplay_name();

    void setDisplay_name(String display_name);

    /**
     * 获取 [显示名称]脏标记
     */
    boolean getDisplay_nameDirtyFlag();

    /**
     * 型号
     */
    String getModel();

    void setModel(String model);

    /**
     * 获取 [型号]脏标记
     */
    boolean getModelDirtyFlag();

    /**
     * 当前维护
     */
    Integer getMaintenance_open_count();

    void setMaintenance_open_count(Integer maintenance_open_count);

    /**
     * 获取 [当前维护]脏标记
     */
    boolean getMaintenance_open_countDirtyFlag();

    /**
     * 是关注者
     */
    String getMessage_is_follower();

    void setMessage_is_follower(String message_is_follower);

    /**
     * 获取 [是关注者]脏标记
     */
    boolean getMessage_is_followerDirtyFlag();

    /**
     * 活动状态
     */
    String getActivity_state();

    void setActivity_state(String activity_state);

    /**
     * 获取 [活动状态]脏标记
     */
    boolean getActivity_stateDirtyFlag();

    /**
     * 下一活动截止日期
     */
    Timestamp getActivity_date_deadline();

    void setActivity_date_deadline(Timestamp activity_date_deadline);

    /**
     * 获取 [下一活动截止日期]脏标记
     */
    boolean getActivity_date_deadlineDirtyFlag();

    /**
     * 维修统计
     */
    Integer getMaintenance_count();

    void setMaintenance_count(Integer maintenance_count);

    /**
     * 获取 [维修统计]脏标记
     */
    boolean getMaintenance_countDirtyFlag();

    /**
     * 最后更新时间
     */
    Timestamp getWrite_date();

    void setWrite_date(Timestamp write_date);

    /**
     * 获取 [最后更新时间]脏标记
     */
    boolean getWrite_dateDirtyFlag();

    /**
     * 未读消息计数器
     */
    Integer getMessage_unread_counter();

    void setMessage_unread_counter(Integer message_unread_counter);

    /**
     * 获取 [未读消息计数器]脏标记
     */
    boolean getMessage_unread_counterDirtyFlag();

    /**
     * 笔记
     */
    String getNote();

    void setNote(String note);

    /**
     * 获取 [笔记]脏标记
     */
    boolean getNoteDirtyFlag();

    /**
     * 供应商参考
     */
    String getPartner_ref();

    void setPartner_ref(String partner_ref);

    /**
     * 获取 [供应商参考]脏标记
     */
    boolean getPartner_refDirtyFlag();

    /**
     * 保养
     */
    String getMaintenance_ids();

    void setMaintenance_ids(String maintenance_ids);

    /**
     * 获取 [保养]脏标记
     */
    boolean getMaintenance_idsDirtyFlag();

    /**
     * 错误数
     */
    Integer getMessage_has_error_counter();

    void setMessage_has_error_counter(Integer message_has_error_counter);

    /**
     * 获取 [错误数]脏标记
     */
    boolean getMessage_has_error_counterDirtyFlag();

    /**
     * 责任用户
     */
    Integer getActivity_user_id();

    void setActivity_user_id(Integer activity_user_id);

    /**
     * 获取 [责任用户]脏标记
     */
    boolean getActivity_user_idDirtyFlag();

    /**
     * 附件
     */
    Integer getMessage_main_attachment_id();

    void setMessage_main_attachment_id(Integer message_main_attachment_id);

    /**
     * 获取 [附件]脏标记
     */
    boolean getMessage_main_attachment_idDirtyFlag();

    /**
     * 序列号
     */
    String getSerial_no();

    void setSerial_no(String serial_no);

    /**
     * 获取 [序列号]脏标记
     */
    boolean getSerial_noDirtyFlag();

    /**
     * 附件数量
     */
    Integer getMessage_attachment_count();

    void setMessage_attachment_count(Integer message_attachment_count);

    /**
     * 获取 [附件数量]脏标记
     */
    boolean getMessage_attachment_countDirtyFlag();

    /**
     * 成本
     */
    Double getCost();

    void setCost(Double cost);

    /**
     * 获取 [成本]脏标记
     */
    boolean getCostDirtyFlag();

    /**
     * 下次预防维护日期
     */
    Timestamp getNext_action_date();

    void setNext_action_date(Timestamp next_action_date);

    /**
     * 获取 [下次预防维护日期]脏标记
     */
    boolean getNext_action_dateDirtyFlag();

    /**
     * 分配日期
     */
    Timestamp getAssign_date();

    void setAssign_date(Timestamp assign_date);

    /**
     * 获取 [分配日期]脏标记
     */
    boolean getAssign_dateDirtyFlag();

    /**
     * 实际日期
     */
    Timestamp getEffective_date();

    void setEffective_date(Timestamp effective_date);

    /**
     * 获取 [实际日期]脏标记
     */
    boolean getEffective_dateDirtyFlag();

    /**
     * 需要采取行动
     */
    String getMessage_needaction();

    void setMessage_needaction(String message_needaction);

    /**
     * 获取 [需要采取行动]脏标记
     */
    boolean getMessage_needactionDirtyFlag();

    /**
     * 关注者(渠道)
     */
    String getMessage_channel_ids();

    void setMessage_channel_ids(String message_channel_ids);

    /**
     * 获取 [关注者(渠道)]脏标记
     */
    boolean getMessage_channel_idsDirtyFlag();

    /**
     * 有效
     */
    String getActive();

    void setActive(String active);

    /**
     * 获取 [有效]脏标记
     */
    boolean getActiveDirtyFlag();

    /**
     * 消息
     */
    String getMessage_ids();

    void setMessage_ids(String message_ids);

    /**
     * 获取 [消息]脏标记
     */
    boolean getMessage_idsDirtyFlag();

    /**
     * 报废日期
     */
    Timestamp getScrap_date();

    void setScrap_date(Timestamp scrap_date);

    /**
     * 获取 [报废日期]脏标记
     */
    boolean getScrap_dateDirtyFlag();

    /**
     * 创建时间
     */
    Timestamp getCreate_date();

    void setCreate_date(Timestamp create_date);

    /**
     * 获取 [创建时间]脏标记
     */
    boolean getCreate_dateDirtyFlag();

    /**
     * 关注者
     */
    String getMessage_follower_ids();

    void setMessage_follower_ids(String message_follower_ids);

    /**
     * 获取 [关注者]脏标记
     */
    boolean getMessage_follower_idsDirtyFlag();

    /**
     * 预防维护间隔天数
     */
    Integer getPeriod();

    void setPeriod(Integer period);

    /**
     * 获取 [预防维护间隔天数]脏标记
     */
    boolean getPeriodDirtyFlag();

    /**
     * 消息递送错误
     */
    String getMessage_has_error();

    void setMessage_has_error(String message_has_error);

    /**
     * 获取 [消息递送错误]脏标记
     */
    boolean getMessage_has_errorDirtyFlag();

    /**
     * 下一活动类型
     */
    Integer getActivity_type_id();

    void setActivity_type_id(Integer activity_type_id);

    /**
     * 获取 [下一活动类型]脏标记
     */
    boolean getActivity_type_idDirtyFlag();

    /**
     * 颜色索引
     */
    Integer getColor();

    void setColor(Integer color);

    /**
     * 获取 [颜色索引]脏标记
     */
    boolean getColorDirtyFlag();

    /**
     * 关注者(业务伙伴)
     */
    String getMessage_partner_ids();

    void setMessage_partner_ids(String message_partner_ids);

    /**
     * 获取 [关注者(业务伙伴)]脏标记
     */
    boolean getMessage_partner_idsDirtyFlag();

    /**
     * 分配到部门
     */
    String getDepartment_id_text();

    void setDepartment_id_text(String department_id_text);

    /**
     * 获取 [分配到部门]脏标记
     */
    boolean getDepartment_id_textDirtyFlag();

    /**
     * 分配到员工
     */
    String getEmployee_id_text();

    void setEmployee_id_text(String employee_id_text);

    /**
     * 获取 [分配到员工]脏标记
     */
    boolean getEmployee_id_textDirtyFlag();

    /**
     * 技术员
     */
    String getTechnician_user_id_text();

    void setTechnician_user_id_text(String technician_user_id_text);

    /**
     * 获取 [技术员]脏标记
     */
    boolean getTechnician_user_id_textDirtyFlag();

    /**
     * 公司
     */
    String getCompany_id_text();

    void setCompany_id_text(String company_id_text);

    /**
     * 获取 [公司]脏标记
     */
    boolean getCompany_id_textDirtyFlag();

    /**
     * 设备类别
     */
    String getCategory_id_text();

    void setCategory_id_text(String category_id_text);

    /**
     * 获取 [设备类别]脏标记
     */
    boolean getCategory_id_textDirtyFlag();

    /**
     * 创建人
     */
    String getCreate_uid_text();

    void setCreate_uid_text(String create_uid_text);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uid_textDirtyFlag();

    /**
     * 供应商
     */
    String getPartner_id_text();

    void setPartner_id_text(String partner_id_text);

    /**
     * 获取 [供应商]脏标记
     */
    boolean getPartner_id_textDirtyFlag();

    /**
     * 最后更新人
     */
    String getWrite_uid_text();

    void setWrite_uid_text(String write_uid_text);

    /**
     * 获取 [最后更新人]脏标记
     */
    boolean getWrite_uid_textDirtyFlag();

    /**
     * 所有者
     */
    String getOwner_user_id_text();

    void setOwner_user_id_text(String owner_user_id_text);

    /**
     * 获取 [所有者]脏标记
     */
    boolean getOwner_user_id_textDirtyFlag();

    /**
     * 保养团队
     */
    String getMaintenance_team_id_text();

    void setMaintenance_team_id_text(String maintenance_team_id_text);

    /**
     * 获取 [保养团队]脏标记
     */
    boolean getMaintenance_team_id_textDirtyFlag();

    /**
     * 创建人
     */
    Integer getCreate_uid();

    void setCreate_uid(Integer create_uid);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uidDirtyFlag();

    /**
     * 技术员
     */
    Integer getTechnician_user_id();

    void setTechnician_user_id(Integer technician_user_id);

    /**
     * 获取 [技术员]脏标记
     */
    boolean getTechnician_user_idDirtyFlag();

    /**
     * 供应商
     */
    Integer getPartner_id();

    void setPartner_id(Integer partner_id);

    /**
     * 获取 [供应商]脏标记
     */
    boolean getPartner_idDirtyFlag();

    /**
     * 设备类别
     */
    Integer getCategory_id();

    void setCategory_id(Integer category_id);

    /**
     * 获取 [设备类别]脏标记
     */
    boolean getCategory_idDirtyFlag();

    /**
     * 分配到员工
     */
    Integer getEmployee_id();

    void setEmployee_id(Integer employee_id);

    /**
     * 获取 [分配到员工]脏标记
     */
    boolean getEmployee_idDirtyFlag();

    /**
     * 保养团队
     */
    Integer getMaintenance_team_id();

    void setMaintenance_team_id(Integer maintenance_team_id);

    /**
     * 获取 [保养团队]脏标记
     */
    boolean getMaintenance_team_idDirtyFlag();

    /**
     * 公司
     */
    Integer getCompany_id();

    void setCompany_id(Integer company_id);

    /**
     * 获取 [公司]脏标记
     */
    boolean getCompany_idDirtyFlag();

    /**
     * 所有者
     */
    Integer getOwner_user_id();

    void setOwner_user_id(Integer owner_user_id);

    /**
     * 获取 [所有者]脏标记
     */
    boolean getOwner_user_idDirtyFlag();

    /**
     * 分配到部门
     */
    Integer getDepartment_id();

    void setDepartment_id(Integer department_id);

    /**
     * 获取 [分配到部门]脏标记
     */
    boolean getDepartment_idDirtyFlag();

    /**
     * 最后更新人
     */
    Integer getWrite_uid();

    void setWrite_uid(Integer write_uid);

    /**
     * 获取 [最后更新人]脏标记
     */
    boolean getWrite_uidDirtyFlag();

}
