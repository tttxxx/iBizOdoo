package cn.ibizlab.odoo.core.repository.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import cn.ibizlab.odoo.core.repository.client.po.project_project;

/**
 * 实体[project_project] 服务对象接口
 */
public interface project_projectRepository{


    public project_project createPO() ;
        public void updateBatch(project_project project_project);

        public List<project_project> search();

        public void createBatch(project_project project_project);

        public void removeBatch(String id);

        public void remove(String id);

        public void create(project_project project_project);

        public void get(String id);

        public void update(project_project project_project);


}
