package cn.ibizlab.odoo.core.odoo_mail.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_mail.domain.Mail_mass_mailing_test;
import cn.ibizlab.odoo.core.odoo_mail.filter.Mail_mass_mailing_testSearchContext;
import cn.ibizlab.odoo.core.odoo_mail.service.IMail_mass_mailing_testService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;



import cn.ibizlab.odoo.core.odoo_mail.client.mail_mass_mailing_testOdooClient;
import cn.ibizlab.odoo.core.odoo_mail.clientmodel.mail_mass_mailing_testClientModel;

/**
 * 实体[示例邮件向导] 服务对象接口实现
 */
@Slf4j
@Service
public class Mail_mass_mailing_testServiceImpl implements IMail_mass_mailing_testService {

    @Autowired
    mail_mass_mailing_testOdooClient mail_mass_mailing_testOdooClient;


    @Override
    public boolean update(Mail_mass_mailing_test et) {
        mail_mass_mailing_testClientModel clientModel = convert2Model(et,null);
		mail_mass_mailing_testOdooClient.update(clientModel);
        Mail_mass_mailing_test rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }
    
    public void updateBatch(List<Mail_mass_mailing_test> list){
    }

    @Override
    public boolean create(Mail_mass_mailing_test et) {
        mail_mass_mailing_testClientModel clientModel = convert2Model(et,null);
		mail_mass_mailing_testOdooClient.create(clientModel);
        Mail_mass_mailing_test rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Mail_mass_mailing_test> list){
    }

    @Override
    public Mail_mass_mailing_test get(Integer id) {
        mail_mass_mailing_testClientModel clientModel = new mail_mass_mailing_testClientModel();
        clientModel.setId(id);
		mail_mass_mailing_testOdooClient.get(clientModel);
        Mail_mass_mailing_test et = convert2Domain(clientModel,null);
        if(et==null){
            et=new Mail_mass_mailing_test();
            et.setId(id);
        }
        else{
        }

        return  et;
    }

    @Override
    public boolean remove(Integer id) {
        mail_mass_mailing_testClientModel clientModel = new mail_mass_mailing_testClientModel();
        clientModel.setId(id);
		mail_mass_mailing_testOdooClient.remove(clientModel);
        return true; 
    }

    public void removeBatch(Collection<Integer> idList){
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Mail_mass_mailing_test> searchDefault(Mail_mass_mailing_testSearchContext context) {
        List<Mail_mass_mailing_test> list = new ArrayList<Mail_mass_mailing_test>();
        Page<mail_mass_mailing_testClientModel> clientModelList = mail_mass_mailing_testOdooClient.search(context);
        for(mail_mass_mailing_testClientModel clientModel : clientModelList){
            list.add( this.convert2Domain(clientModel,null));
        }
        return new PageImpl<Mail_mass_mailing_test>(list, context.getPageable(), clientModelList.getTotalElements());
    }

    public mail_mass_mailing_testClientModel convert2Model(Mail_mass_mailing_test domain , mail_mass_mailing_testClientModel model) {
        if(domain == null)
            return model ;
        if(model==null){
            model = new mail_mass_mailing_testClientModel();

        }
        if((Boolean) domain.getExtensionparams().get("dirtyflagenable")){
            if((Boolean) domain.getExtensionparams().get("iddirtyflag"))
                model.setId(domain.getId());
            if((Boolean) domain.getExtensionparams().get("write_datedirtyflag"))
                model.setWrite_date(domain.getWriteDate());
            if((Boolean) domain.getExtensionparams().get("email_todirtyflag"))
                model.setEmail_to(domain.getEmailTo());
            if((Boolean) domain.getExtensionparams().get("display_namedirtyflag"))
                model.setDisplay_name(domain.getDisplayName());
            if((Boolean) domain.getExtensionparams().get("__last_updatedirtyflag"))
                model.set__last_update(domain.getLastUpdate());
            if((Boolean) domain.getExtensionparams().get("create_datedirtyflag"))
                model.setCreate_date(domain.getCreateDate());
            if((Boolean) domain.getExtensionparams().get("mass_mailing_id_textdirtyflag"))
                model.setMass_mailing_id_text(domain.getMassMailingIdText());
            if((Boolean) domain.getExtensionparams().get("write_uid_textdirtyflag"))
                model.setWrite_uid_text(domain.getWriteUidText());
            if((Boolean) domain.getExtensionparams().get("create_uid_textdirtyflag"))
                model.setCreate_uid_text(domain.getCreateUidText());
            if((Boolean) domain.getExtensionparams().get("create_uiddirtyflag"))
                model.setCreate_uid(domain.getCreateUid());
            if((Boolean) domain.getExtensionparams().get("mass_mailing_iddirtyflag"))
                model.setMass_mailing_id(domain.getMassMailingId());
            if((Boolean) domain.getExtensionparams().get("write_uiddirtyflag"))
                model.setWrite_uid(domain.getWriteUid());
            domain.getFocusNull().clear();
        }
        return model ;
    }

    public Mail_mass_mailing_test convert2Domain( mail_mass_mailing_testClientModel model ,Mail_mass_mailing_test domain) {
        if(model == null)
            return domain ;
        if(domain == null){
            domain = new Mail_mass_mailing_test();
        }

        if(model.getIdDirtyFlag())
            domain.setId(model.getId());
        if(model.getWrite_dateDirtyFlag())
            domain.setWriteDate(model.getWrite_date());
        if(model.getEmail_toDirtyFlag())
            domain.setEmailTo(model.getEmail_to());
        if(model.getDisplay_nameDirtyFlag())
            domain.setDisplayName(model.getDisplay_name());
        if(model.get__last_updateDirtyFlag())
            domain.setLastUpdate(model.get__last_update());
        if(model.getCreate_dateDirtyFlag())
            domain.setCreateDate(model.getCreate_date());
        if(model.getMass_mailing_id_textDirtyFlag())
            domain.setMassMailingIdText(model.getMass_mailing_id_text());
        if(model.getWrite_uid_textDirtyFlag())
            domain.setWriteUidText(model.getWrite_uid_text());
        if(model.getCreate_uid_textDirtyFlag())
            domain.setCreateUidText(model.getCreate_uid_text());
        if(model.getCreate_uidDirtyFlag())
            domain.setCreateUid(model.getCreate_uid());
        if(model.getMass_mailing_idDirtyFlag())
            domain.setMassMailingId(model.getMass_mailing_id());
        if(model.getWrite_uidDirtyFlag())
            domain.setWriteUid(model.getWrite_uid());
        return domain ;
    }

}

    



