package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Res_users_log;
import cn.ibizlab.odoo.core.odoo_base.filter.Res_users_logSearchContext;

/**
 * 实体 [用户日志] 存储对象
 */
public interface Res_users_logRepository extends Repository<Res_users_log> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Res_users_log> searchDefault(Res_users_logSearchContext context);

    Res_users_log convert2PO(cn.ibizlab.odoo.core.odoo_base.domain.Res_users_log domain , Res_users_log po) ;

    cn.ibizlab.odoo.core.odoo_base.domain.Res_users_log convert2Domain( Res_users_log po ,cn.ibizlab.odoo.core.odoo_base.domain.Res_users_log domain) ;

}
