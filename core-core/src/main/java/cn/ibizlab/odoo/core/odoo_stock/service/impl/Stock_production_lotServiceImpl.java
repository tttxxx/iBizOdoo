package cn.ibizlab.odoo.core.odoo_stock.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_stock.domain.Stock_production_lot;
import cn.ibizlab.odoo.core.odoo_stock.filter.Stock_production_lotSearchContext;
import cn.ibizlab.odoo.core.odoo_stock.service.IStock_production_lotService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;



import cn.ibizlab.odoo.core.odoo_stock.client.stock_production_lotOdooClient;
import cn.ibizlab.odoo.core.odoo_stock.clientmodel.stock_production_lotClientModel;

/**
 * 实体[批次/序列号] 服务对象接口实现
 */
@Slf4j
@Service
public class Stock_production_lotServiceImpl implements IStock_production_lotService {

    @Autowired
    stock_production_lotOdooClient stock_production_lotOdooClient;


    @Override
    public boolean update(Stock_production_lot et) {
        stock_production_lotClientModel clientModel = convert2Model(et,null);
		stock_production_lotOdooClient.update(clientModel);
        Stock_production_lot rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }
    
    public void updateBatch(List<Stock_production_lot> list){
    }

    @Override
    public boolean remove(Integer id) {
        stock_production_lotClientModel clientModel = new stock_production_lotClientModel();
        clientModel.setId(id);
		stock_production_lotOdooClient.remove(clientModel);
        return true; 
    }

    public void removeBatch(Collection<Integer> idList){
    }

    @Override
    public Stock_production_lot get(Integer id) {
        stock_production_lotClientModel clientModel = new stock_production_lotClientModel();
        clientModel.setId(id);
		stock_production_lotOdooClient.get(clientModel);
        Stock_production_lot et = convert2Domain(clientModel,null);
        if(et==null){
            et=new Stock_production_lot();
            et.setId(id);
        }
        else{
        }

        return  et;
    }

    @Override
    public boolean create(Stock_production_lot et) {
        stock_production_lotClientModel clientModel = convert2Model(et,null);
		stock_production_lotOdooClient.create(clientModel);
        Stock_production_lot rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Stock_production_lot> list){
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Stock_production_lot> searchDefault(Stock_production_lotSearchContext context) {
        List<Stock_production_lot> list = new ArrayList<Stock_production_lot>();
        Page<stock_production_lotClientModel> clientModelList = stock_production_lotOdooClient.search(context);
        for(stock_production_lotClientModel clientModel : clientModelList){
            list.add( this.convert2Domain(clientModel,null));
        }
        return new PageImpl<Stock_production_lot>(list, context.getPageable(), clientModelList.getTotalElements());
    }

    public stock_production_lotClientModel convert2Model(Stock_production_lot domain , stock_production_lotClientModel model) {
        if(domain == null)
            return model ;
        if(model==null){
            model = new stock_production_lotClientModel();

        }
        if((Boolean) domain.getExtensionparams().get("dirtyflagenable")){
            if((Boolean) domain.getExtensionparams().get("activity_type_iddirtyflag"))
                model.setActivity_type_id(domain.getActivityTypeId());
            if((Boolean) domain.getExtensionparams().get("message_needactiondirtyflag"))
                model.setMessage_needaction(domain.getMessageNeedaction());
            if((Boolean) domain.getExtensionparams().get("message_is_followerdirtyflag"))
                model.setMessage_is_follower(domain.getMessageIsFollower());
            if((Boolean) domain.getExtensionparams().get("iddirtyflag"))
                model.setId(domain.getId());
            if((Boolean) domain.getExtensionparams().get("purchase_order_countdirtyflag"))
                model.setPurchase_order_count(domain.getPurchaseOrderCount());
            if((Boolean) domain.getExtensionparams().get("message_needaction_counterdirtyflag"))
                model.setMessage_needaction_counter(domain.getMessageNeedactionCounter());
            if((Boolean) domain.getExtensionparams().get("activity_statedirtyflag"))
                model.setActivity_state(domain.getActivityState());
            if((Boolean) domain.getExtensionparams().get("message_unread_counterdirtyflag"))
                model.setMessage_unread_counter(domain.getMessageUnreadCounter());
            if((Boolean) domain.getExtensionparams().get("activity_user_iddirtyflag"))
                model.setActivity_user_id(domain.getActivityUserId());
            if((Boolean) domain.getExtensionparams().get("message_idsdirtyflag"))
                model.setMessage_ids(domain.getMessageIds());
            if((Boolean) domain.getExtensionparams().get("activity_date_deadlinedirtyflag"))
                model.setActivity_date_deadline(domain.getActivityDateDeadline());
            if((Boolean) domain.getExtensionparams().get("product_qtydirtyflag"))
                model.setProduct_qty(domain.getProductQty());
            if((Boolean) domain.getExtensionparams().get("display_namedirtyflag"))
                model.setDisplay_name(domain.getDisplayName());
            if((Boolean) domain.getExtensionparams().get("sale_order_countdirtyflag"))
                model.setSale_order_count(domain.getSaleOrderCount());
            if((Boolean) domain.getExtensionparams().get("namedirtyflag"))
                model.setName(domain.getName());
            if((Boolean) domain.getExtensionparams().get("quant_idsdirtyflag"))
                model.setQuant_ids(domain.getQuantIds());
            if((Boolean) domain.getExtensionparams().get("activity_summarydirtyflag"))
                model.setActivity_summary(domain.getActivitySummary());
            if((Boolean) domain.getExtensionparams().get("activity_idsdirtyflag"))
                model.setActivity_ids(domain.getActivityIds());
            if((Boolean) domain.getExtensionparams().get("message_attachment_countdirtyflag"))
                model.setMessage_attachment_count(domain.getMessageAttachmentCount());
            if((Boolean) domain.getExtensionparams().get("message_has_errordirtyflag"))
                model.setMessage_has_error(domain.getMessageHasError());
            if((Boolean) domain.getExtensionparams().get("message_unreaddirtyflag"))
                model.setMessage_unread(domain.getMessageUnread());
            if((Boolean) domain.getExtensionparams().get("write_datedirtyflag"))
                model.setWrite_date(domain.getWriteDate());
            if((Boolean) domain.getExtensionparams().get("__last_updatedirtyflag"))
                model.set__last_update(domain.getLastUpdate());
            if((Boolean) domain.getExtensionparams().get("message_partner_idsdirtyflag"))
                model.setMessage_partner_ids(domain.getMessagePartnerIds());
            if((Boolean) domain.getExtensionparams().get("message_follower_idsdirtyflag"))
                model.setMessage_follower_ids(domain.getMessageFollowerIds());
            if((Boolean) domain.getExtensionparams().get("refdirtyflag"))
                model.setRef(domain.getRef());
            if((Boolean) domain.getExtensionparams().get("sale_order_idsdirtyflag"))
                model.setSale_order_ids(domain.getSaleOrderIds());
            if((Boolean) domain.getExtensionparams().get("website_message_idsdirtyflag"))
                model.setWebsite_message_ids(domain.getWebsiteMessageIds());
            if((Boolean) domain.getExtensionparams().get("message_channel_idsdirtyflag"))
                model.setMessage_channel_ids(domain.getMessageChannelIds());
            if((Boolean) domain.getExtensionparams().get("message_has_error_counterdirtyflag"))
                model.setMessage_has_error_counter(domain.getMessageHasErrorCounter());
            if((Boolean) domain.getExtensionparams().get("message_main_attachment_iddirtyflag"))
                model.setMessage_main_attachment_id(domain.getMessageMainAttachmentId());
            if((Boolean) domain.getExtensionparams().get("create_datedirtyflag"))
                model.setCreate_date(domain.getCreateDate());
            if((Boolean) domain.getExtensionparams().get("purchase_order_idsdirtyflag"))
                model.setPurchase_order_ids(domain.getPurchaseOrderIds());
            if((Boolean) domain.getExtensionparams().get("product_id_textdirtyflag"))
                model.setProduct_id_text(domain.getProductIdText());
            if((Boolean) domain.getExtensionparams().get("product_uom_id_textdirtyflag"))
                model.setProduct_uom_id_text(domain.getProductUomIdText());
            if((Boolean) domain.getExtensionparams().get("create_uid_textdirtyflag"))
                model.setCreate_uid_text(domain.getCreateUidText());
            if((Boolean) domain.getExtensionparams().get("use_next_on_work_order_id_textdirtyflag"))
                model.setUse_next_on_work_order_id_text(domain.getUseNextOnWorkOrderIdText());
            if((Boolean) domain.getExtensionparams().get("write_uid_textdirtyflag"))
                model.setWrite_uid_text(domain.getWriteUidText());
            if((Boolean) domain.getExtensionparams().get("product_iddirtyflag"))
                model.setProduct_id(domain.getProductId());
            if((Boolean) domain.getExtensionparams().get("use_next_on_work_order_iddirtyflag"))
                model.setUse_next_on_work_order_id(domain.getUseNextOnWorkOrderId());
            if((Boolean) domain.getExtensionparams().get("product_uom_iddirtyflag"))
                model.setProduct_uom_id(domain.getProductUomId());
            if((Boolean) domain.getExtensionparams().get("create_uiddirtyflag"))
                model.setCreate_uid(domain.getCreateUid());
            if((Boolean) domain.getExtensionparams().get("write_uiddirtyflag"))
                model.setWrite_uid(domain.getWriteUid());
            domain.getFocusNull().clear();
        }
        return model ;
    }

    public Stock_production_lot convert2Domain( stock_production_lotClientModel model ,Stock_production_lot domain) {
        if(model == null)
            return domain ;
        if(domain == null){
            domain = new Stock_production_lot();
        }

        if(model.getActivity_type_idDirtyFlag())
            domain.setActivityTypeId(model.getActivity_type_id());
        if(model.getMessage_needactionDirtyFlag())
            domain.setMessageNeedaction(model.getMessage_needaction());
        if(model.getMessage_is_followerDirtyFlag())
            domain.setMessageIsFollower(model.getMessage_is_follower());
        if(model.getIdDirtyFlag())
            domain.setId(model.getId());
        if(model.getPurchase_order_countDirtyFlag())
            domain.setPurchaseOrderCount(model.getPurchase_order_count());
        if(model.getMessage_needaction_counterDirtyFlag())
            domain.setMessageNeedactionCounter(model.getMessage_needaction_counter());
        if(model.getActivity_stateDirtyFlag())
            domain.setActivityState(model.getActivity_state());
        if(model.getMessage_unread_counterDirtyFlag())
            domain.setMessageUnreadCounter(model.getMessage_unread_counter());
        if(model.getActivity_user_idDirtyFlag())
            domain.setActivityUserId(model.getActivity_user_id());
        if(model.getMessage_idsDirtyFlag())
            domain.setMessageIds(model.getMessage_ids());
        if(model.getActivity_date_deadlineDirtyFlag())
            domain.setActivityDateDeadline(model.getActivity_date_deadline());
        if(model.getProduct_qtyDirtyFlag())
            domain.setProductQty(model.getProduct_qty());
        if(model.getDisplay_nameDirtyFlag())
            domain.setDisplayName(model.getDisplay_name());
        if(model.getSale_order_countDirtyFlag())
            domain.setSaleOrderCount(model.getSale_order_count());
        if(model.getNameDirtyFlag())
            domain.setName(model.getName());
        if(model.getQuant_idsDirtyFlag())
            domain.setQuantIds(model.getQuant_ids());
        if(model.getActivity_summaryDirtyFlag())
            domain.setActivitySummary(model.getActivity_summary());
        if(model.getActivity_idsDirtyFlag())
            domain.setActivityIds(model.getActivity_ids());
        if(model.getMessage_attachment_countDirtyFlag())
            domain.setMessageAttachmentCount(model.getMessage_attachment_count());
        if(model.getMessage_has_errorDirtyFlag())
            domain.setMessageHasError(model.getMessage_has_error());
        if(model.getMessage_unreadDirtyFlag())
            domain.setMessageUnread(model.getMessage_unread());
        if(model.getWrite_dateDirtyFlag())
            domain.setWriteDate(model.getWrite_date());
        if(model.get__last_updateDirtyFlag())
            domain.setLastUpdate(model.get__last_update());
        if(model.getMessage_partner_idsDirtyFlag())
            domain.setMessagePartnerIds(model.getMessage_partner_ids());
        if(model.getMessage_follower_idsDirtyFlag())
            domain.setMessageFollowerIds(model.getMessage_follower_ids());
        if(model.getRefDirtyFlag())
            domain.setRef(model.getRef());
        if(model.getSale_order_idsDirtyFlag())
            domain.setSaleOrderIds(model.getSale_order_ids());
        if(model.getWebsite_message_idsDirtyFlag())
            domain.setWebsiteMessageIds(model.getWebsite_message_ids());
        if(model.getMessage_channel_idsDirtyFlag())
            domain.setMessageChannelIds(model.getMessage_channel_ids());
        if(model.getMessage_has_error_counterDirtyFlag())
            domain.setMessageHasErrorCounter(model.getMessage_has_error_counter());
        if(model.getMessage_main_attachment_idDirtyFlag())
            domain.setMessageMainAttachmentId(model.getMessage_main_attachment_id());
        if(model.getCreate_dateDirtyFlag())
            domain.setCreateDate(model.getCreate_date());
        if(model.getPurchase_order_idsDirtyFlag())
            domain.setPurchaseOrderIds(model.getPurchase_order_ids());
        if(model.getProduct_id_textDirtyFlag())
            domain.setProductIdText(model.getProduct_id_text());
        if(model.getProduct_uom_id_textDirtyFlag())
            domain.setProductUomIdText(model.getProduct_uom_id_text());
        if(model.getCreate_uid_textDirtyFlag())
            domain.setCreateUidText(model.getCreate_uid_text());
        if(model.getUse_next_on_work_order_id_textDirtyFlag())
            domain.setUseNextOnWorkOrderIdText(model.getUse_next_on_work_order_id_text());
        if(model.getWrite_uid_textDirtyFlag())
            domain.setWriteUidText(model.getWrite_uid_text());
        if(model.getProduct_idDirtyFlag())
            domain.setProductId(model.getProduct_id());
        if(model.getUse_next_on_work_order_idDirtyFlag())
            domain.setUseNextOnWorkOrderId(model.getUse_next_on_work_order_id());
        if(model.getProduct_uom_idDirtyFlag())
            domain.setProductUomId(model.getProduct_uom_id());
        if(model.getCreate_uidDirtyFlag())
            domain.setCreateUid(model.getCreate_uid());
        if(model.getWrite_uidDirtyFlag())
            domain.setWriteUid(model.getWrite_uid());
        return domain ;
    }

}

    



