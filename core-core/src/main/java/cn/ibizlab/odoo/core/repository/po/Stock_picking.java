package cn.ibizlab.odoo.core.repository.po ;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.math.BigInteger;

import cn.ibizlab.odoo.core.odoo_stock.filter.Stock_pickingSearchContext;

/**
 * 实体 [调拨] 存储模型
 */
public interface Stock_picking{

    /**
     * 包裹层级
     */
    String getPackage_level_ids();

    void setPackage_level_ids(String package_level_ids);

    /**
     * 获取 [包裹层级]脏标记
     */
    boolean getPackage_level_idsDirtyFlag();

    /**
     * 备注
     */
    String getNote();

    void setNote(String note);

    /**
     * 获取 [备注]脏标记
     */
    boolean getNoteDirtyFlag();

    /**
     * 网站
     */
    Integer getWebsite_id();

    void setWebsite_id(Integer website_id);

    /**
     * 获取 [网站]脏标记
     */
    boolean getWebsite_idDirtyFlag();

    /**
     * 下一活动类型
     */
    Integer getActivity_type_id();

    void setActivity_type_id(Integer activity_type_id);

    /**
     * 获取 [下一活动类型]脏标记
     */
    boolean getActivity_type_idDirtyFlag();

    /**
     * 已打印
     */
    String getPrinted();

    void setPrinted(String printed);

    /**
     * 获取 [已打印]脏标记
     */
    boolean getPrintedDirtyFlag();

    /**
     * 行动数量
     */
    Integer getMessage_needaction_counter();

    void setMessage_needaction_counter(Integer message_needaction_counter);

    /**
     * 获取 [行动数量]脏标记
     */
    boolean getMessage_needaction_counterDirtyFlag();

    /**
     * 产品
     */
    Integer getProduct_id();

    void setProduct_id(Integer product_id);

    /**
     * 获取 [产品]脏标记
     */
    boolean getProduct_idDirtyFlag();

    /**
     * 关注者(业务伙伴)
     */
    String getMessage_partner_ids();

    void setMessage_partner_ids(String message_partner_ids);

    /**
     * 获取 [关注者(业务伙伴)]脏标记
     */
    boolean getMessage_partner_idsDirtyFlag();

    /**
     * 未读消息计数器
     */
    Integer getMessage_unread_counter();

    void setMessage_unread_counter(Integer message_unread_counter);

    /**
     * 获取 [未读消息计数器]脏标记
     */
    boolean getMessage_unread_counterDirtyFlag();

    /**
     * 送货策略
     */
    String getMove_type();

    void setMove_type(String move_type);

    /**
     * 获取 [送货策略]脏标记
     */
    boolean getMove_typeDirtyFlag();

    /**
     * 欠单
     */
    String getBackorder_ids();

    void setBackorder_ids(String backorder_ids);

    /**
     * 获取 [欠单]脏标记
     */
    boolean getBackorder_idsDirtyFlag();

    /**
     * 需要采取行动
     */
    String getMessage_needaction();

    void setMessage_needaction(String message_needaction);

    /**
     * 获取 [需要采取行动]脏标记
     */
    boolean getMessage_needactionDirtyFlag();

    /**
     * 消息
     */
    String getMessage_ids();

    void setMessage_ids(String message_ids);

    /**
     * 获取 [消息]脏标记
     */
    boolean getMessage_idsDirtyFlag();

    /**
     * 有包裹
     */
    String getHas_packages();

    void setHas_packages(String has_packages);

    /**
     * 获取 [有包裹]脏标记
     */
    boolean getHas_packagesDirtyFlag();

    /**
     * 源文档
     */
    String getOrigin();

    void setOrigin(String origin);

    /**
     * 获取 [源文档]脏标记
     */
    boolean getOriginDirtyFlag();

    /**
     * 是锁定
     */
    String getIs_locked();

    void setIs_locked(String is_locked);

    /**
     * 获取 [是锁定]脏标记
     */
    boolean getIs_lockedDirtyFlag();

    /**
     * 显示作业
     */
    String getShow_operations();

    void setShow_operations(String show_operations);

    /**
     * 获取 [显示作业]脏标记
     */
    boolean getShow_operationsDirtyFlag();

    /**
     * 补货组
     */
    Integer getGroup_id();

    void setGroup_id(Integer group_id);

    /**
     * 获取 [补货组]脏标记
     */
    boolean getGroup_idDirtyFlag();

    /**
     * 错误数
     */
    Integer getMessage_has_error_counter();

    void setMessage_has_error_counter(Integer message_has_error_counter);

    /**
     * 获取 [错误数]脏标记
     */
    boolean getMessage_has_error_counterDirtyFlag();

    /**
     * 无包裹作业
     */
    String getMove_line_ids_without_package();

    void setMove_line_ids_without_package(String move_line_ids_without_package);

    /**
     * 获取 [无包裹作业]脏标记
     */
    boolean getMove_line_ids_without_packageDirtyFlag();

    /**
     * 下一活动截止日期
     */
    Timestamp getActivity_date_deadline();

    void setActivity_date_deadline(Timestamp activity_date_deadline);

    /**
     * 获取 [下一活动截止日期]脏标记
     */
    boolean getActivity_date_deadlineDirtyFlag();

    /**
     * 责任用户
     */
    Integer getActivity_user_id();

    void setActivity_user_id(Integer activity_user_id);

    /**
     * 获取 [责任用户]脏标记
     */
    boolean getActivity_user_idDirtyFlag();

    /**
     * 活动状态
     */
    String getActivity_state();

    void setActivity_state(String activity_state);

    /**
     * 获取 [活动状态]脏标记
     */
    boolean getActivity_stateDirtyFlag();

    /**
     * 活动
     */
    String getActivity_ids();

    void setActivity_ids(String activity_ids);

    /**
     * 获取 [活动]脏标记
     */
    boolean getActivity_idsDirtyFlag();

    /**
     * ID
     */
    Integer getId();

    void setId(Integer id);

    /**
     * 获取 [ID]脏标记
     */
    boolean getIdDirtyFlag();

    /**
     * 下一活动摘要
     */
    String getActivity_summary();

    void setActivity_summary(String activity_summary);

    /**
     * 获取 [下一活动摘要]脏标记
     */
    boolean getActivity_summaryDirtyFlag();

    /**
     * 消息递送错误
     */
    String getMessage_has_error();

    void setMessage_has_error(String message_has_error);

    /**
     * 获取 [消息递送错误]脏标记
     */
    boolean getMessage_has_errorDirtyFlag();

    /**
     * 作业
     */
    String getMove_line_ids();

    void setMove_line_ids(String move_line_ids);

    /**
     * 获取 [作业]脏标记
     */
    boolean getMove_line_idsDirtyFlag();

    /**
     * 包裹层级ids 详情
     */
    String getPackage_level_ids_details();

    void setPackage_level_ids_details(String package_level_ids_details);

    /**
     * 获取 [包裹层级ids 详情]脏标记
     */
    boolean getPackage_level_ids_detailsDirtyFlag();

    /**
     * 未读消息
     */
    String getMessage_unread();

    void setMessage_unread(String message_unread);

    /**
     * 获取 [未读消息]脏标记
     */
    boolean getMessage_unreadDirtyFlag();

    /**
     * 创建日期
     */
    Timestamp getDate();

    void setDate(Timestamp date);

    /**
     * 获取 [创建日期]脏标记
     */
    boolean getDateDirtyFlag();

    /**
     * 调拨日期
     */
    Timestamp getDate_done();

    void setDate_done(Timestamp date_done);

    /**
     * 获取 [调拨日期]脏标记
     */
    boolean getDate_doneDirtyFlag();

    /**
     * 有跟踪
     */
    String getHas_tracking();

    void setHas_tracking(String has_tracking);

    /**
     * 获取 [有跟踪]脏标记
     */
    boolean getHas_trackingDirtyFlag();

    /**
     * 附件
     */
    Integer getMessage_main_attachment_id();

    void setMessage_main_attachment_id(Integer message_main_attachment_id);

    /**
     * 获取 [附件]脏标记
     */
    boolean getMessage_main_attachment_idDirtyFlag();

    /**
     * 状态
     */
    String getState();

    void setState(String state);

    /**
     * 获取 [状态]脏标记
     */
    boolean getStateDirtyFlag();

    /**
     * 立即调拨
     */
    String getImmediate_transfer();

    void setImmediate_transfer(String immediate_transfer);

    /**
     * 获取 [立即调拨]脏标记
     */
    boolean getImmediate_transferDirtyFlag();

    /**
     * 有报废移动
     */
    String getHas_scrap_move();

    void setHas_scrap_move(String has_scrap_move);

    /**
     * 获取 [有报废移动]脏标记
     */
    boolean getHas_scrap_moveDirtyFlag();

    /**
     * 库存移动不在包裹里
     */
    String getMove_ids_without_package();

    void setMove_ids_without_package(String move_ids_without_package);

    /**
     * 获取 [库存移动不在包裹里]脏标记
     */
    boolean getMove_ids_without_packageDirtyFlag();

    /**
     * 创建时间
     */
    Timestamp getCreate_date();

    void setCreate_date(Timestamp create_date);

    /**
     * 获取 [创建时间]脏标记
     */
    boolean getCreate_dateDirtyFlag();

    /**
     * 显示验证
     */
    String getShow_validate();

    void setShow_validate(String show_validate);

    /**
     * 获取 [显示验证]脏标记
     */
    boolean getShow_validateDirtyFlag();

    /**
     * 编号
     */
    String getName();

    void setName(String name);

    /**
     * 获取 [编号]脏标记
     */
    boolean getNameDirtyFlag();

    /**
     * 关注者(渠道)
     */
    String getMessage_channel_ids();

    void setMessage_channel_ids(String message_channel_ids);

    /**
     * 获取 [关注者(渠道)]脏标记
     */
    boolean getMessage_channel_idsDirtyFlag();

    /**
     * 显示名称
     */
    String getDisplay_name();

    void setDisplay_name(String display_name);

    /**
     * 获取 [显示名称]脏标记
     */
    boolean getDisplay_nameDirtyFlag();

    /**
     * 优先级
     */
    String getPriority();

    void setPriority(String priority);

    /**
     * 获取 [优先级]脏标记
     */
    boolean getPriorityDirtyFlag();

    /**
     * 库存移动
     */
    String getMove_lines();

    void setMove_lines(String move_lines);

    /**
     * 获取 [库存移动]脏标记
     */
    boolean getMove_linesDirtyFlag();

    /**
     * 采购订单
     */
    Integer getPurchase_id();

    void setPurchase_id(Integer purchase_id);

    /**
     * 获取 [采购订单]脏标记
     */
    boolean getPurchase_idDirtyFlag();

    /**
     * 最后更新时间
     */
    Timestamp getWrite_date();

    void setWrite_date(Timestamp write_date);

    /**
     * 获取 [最后更新时间]脏标记
     */
    boolean getWrite_dateDirtyFlag();

    /**
     * 网站信息
     */
    String getWebsite_message_ids();

    void setWebsite_message_ids(String website_message_ids);

    /**
     * 获取 [网站信息]脏标记
     */
    boolean getWebsite_message_idsDirtyFlag();

    /**
     * 显示标记为代办
     */
    String getShow_mark_as_todo();

    void setShow_mark_as_todo(String show_mark_as_todo);

    /**
     * 获取 [显示标记为代办]脏标记
     */
    boolean getShow_mark_as_todoDirtyFlag();

    /**
     * 关注者
     */
    String getMessage_follower_ids();

    void setMessage_follower_ids(String message_follower_ids);

    /**
     * 获取 [关注者]脏标记
     */
    boolean getMessage_follower_idsDirtyFlag();

    /**
     * 有包裹作业
     */
    String getMove_line_exist();

    void setMove_line_exist(String move_line_exist);

    /**
     * 获取 [有包裹作业]脏标记
     */
    boolean getMove_line_existDirtyFlag();

    /**
     * 附件数量
     */
    Integer getMessage_attachment_count();

    void setMessage_attachment_count(Integer message_attachment_count);

    /**
     * 获取 [附件数量]脏标记
     */
    boolean getMessage_attachment_countDirtyFlag();

    /**
     * 最后修改日
     */
    Timestamp get__last_update();

    void set__last_update(Timestamp __last_update);

    /**
     * 获取 [最后修改日]脏标记
     */
    boolean get__last_updateDirtyFlag();

    /**
     * 预定交货日期
     */
    Timestamp getScheduled_date();

    void setScheduled_date(Timestamp scheduled_date);

    /**
     * 获取 [预定交货日期]脏标记
     */
    boolean getScheduled_dateDirtyFlag();

    /**
     * 是关注者
     */
    String getMessage_is_follower();

    void setMessage_is_follower(String message_is_follower);

    /**
     * 获取 [是关注者]脏标记
     */
    boolean getMessage_is_followerDirtyFlag();

    /**
     * 显示检查可用
     */
    String getShow_check_availability();

    void setShow_check_availability(String show_check_availability);

    /**
     * 获取 [显示检查可用]脏标记
     */
    boolean getShow_check_availabilityDirtyFlag();

    /**
     * 显示批次文本
     */
    String getShow_lots_text();

    void setShow_lots_text(String show_lots_text);

    /**
     * 获取 [显示批次文本]脏标记
     */
    boolean getShow_lots_textDirtyFlag();

    /**
     * 最后更新者
     */
    String getWrite_uid_text();

    void setWrite_uid_text(String write_uid_text);

    /**
     * 获取 [最后更新者]脏标记
     */
    boolean getWrite_uid_textDirtyFlag();

    /**
     * 欠单
     */
    String getBackorder_id_text();

    void setBackorder_id_text(String backorder_id_text);

    /**
     * 获取 [欠单]脏标记
     */
    boolean getBackorder_id_textDirtyFlag();

    /**
     * 作业类型
     */
    String getPicking_type_id_text();

    void setPicking_type_id_text(String picking_type_id_text);

    /**
     * 获取 [作业类型]脏标记
     */
    boolean getPicking_type_id_textDirtyFlag();

    /**
     * 业务伙伴
     */
    String getPartner_id_text();

    void setPartner_id_text(String partner_id_text);

    /**
     * 获取 [业务伙伴]脏标记
     */
    boolean getPartner_id_textDirtyFlag();

    /**
     * 创建人
     */
    String getCreate_uid_text();

    void setCreate_uid_text(String create_uid_text);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uid_textDirtyFlag();

    /**
     * 源位置
     */
    String getLocation_id_text();

    void setLocation_id_text(String location_id_text);

    /**
     * 获取 [源位置]脏标记
     */
    boolean getLocation_id_textDirtyFlag();

    /**
     * 目的位置
     */
    String getLocation_dest_id_text();

    void setLocation_dest_id_text(String location_dest_id_text);

    /**
     * 获取 [目的位置]脏标记
     */
    boolean getLocation_dest_id_textDirtyFlag();

    /**
     * 作业的类型
     */
    String getPicking_type_code();

    void setPicking_type_code(String picking_type_code);

    /**
     * 获取 [作业的类型]脏标记
     */
    boolean getPicking_type_codeDirtyFlag();

    /**
     * 移动整个包裹
     */
    String getPicking_type_entire_packs();

    void setPicking_type_entire_packs(String picking_type_entire_packs);

    /**
     * 获取 [移动整个包裹]脏标记
     */
    boolean getPicking_type_entire_packsDirtyFlag();

    /**
     * 所有者
     */
    String getOwner_id_text();

    void setOwner_id_text(String owner_id_text);

    /**
     * 获取 [所有者]脏标记
     */
    boolean getOwner_id_textDirtyFlag();

    /**
     * 销售订单
     */
    String getSale_id_text();

    void setSale_id_text(String sale_id_text);

    /**
     * 获取 [销售订单]脏标记
     */
    boolean getSale_id_textDirtyFlag();

    /**
     * 公司
     */
    String getCompany_id_text();

    void setCompany_id_text(String company_id_text);

    /**
     * 获取 [公司]脏标记
     */
    boolean getCompany_id_textDirtyFlag();

    /**
     * 作业类型
     */
    Integer getPicking_type_id();

    void setPicking_type_id(Integer picking_type_id);

    /**
     * 获取 [作业类型]脏标记
     */
    boolean getPicking_type_idDirtyFlag();

    /**
     * 销售订单
     */
    Integer getSale_id();

    void setSale_id(Integer sale_id);

    /**
     * 获取 [销售订单]脏标记
     */
    boolean getSale_idDirtyFlag();

    /**
     * 目的位置
     */
    Integer getLocation_dest_id();

    void setLocation_dest_id(Integer location_dest_id);

    /**
     * 获取 [目的位置]脏标记
     */
    boolean getLocation_dest_idDirtyFlag();

    /**
     * 创建人
     */
    Integer getCreate_uid();

    void setCreate_uid(Integer create_uid);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uidDirtyFlag();

    /**
     * 业务伙伴
     */
    Integer getPartner_id();

    void setPartner_id(Integer partner_id);

    /**
     * 获取 [业务伙伴]脏标记
     */
    boolean getPartner_idDirtyFlag();

    /**
     * 所有者
     */
    Integer getOwner_id();

    void setOwner_id(Integer owner_id);

    /**
     * 获取 [所有者]脏标记
     */
    boolean getOwner_idDirtyFlag();

    /**
     * 最后更新者
     */
    Integer getWrite_uid();

    void setWrite_uid(Integer write_uid);

    /**
     * 获取 [最后更新者]脏标记
     */
    boolean getWrite_uidDirtyFlag();

    /**
     * 公司
     */
    Integer getCompany_id();

    void setCompany_id(Integer company_id);

    /**
     * 获取 [公司]脏标记
     */
    boolean getCompany_idDirtyFlag();

    /**
     * 源位置
     */
    Integer getLocation_id();

    void setLocation_id(Integer location_id);

    /**
     * 获取 [源位置]脏标记
     */
    boolean getLocation_idDirtyFlag();

    /**
     * 欠单
     */
    Integer getBackorder_id();

    void setBackorder_id(Integer backorder_id);

    /**
     * 获取 [欠单]脏标记
     */
    boolean getBackorder_idDirtyFlag();

}
