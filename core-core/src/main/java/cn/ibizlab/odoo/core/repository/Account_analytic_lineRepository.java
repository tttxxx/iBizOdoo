package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Account_analytic_line;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_analytic_lineSearchContext;

/**
 * 实体 [分析行] 存储对象
 */
public interface Account_analytic_lineRepository extends Repository<Account_analytic_line> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Account_analytic_line> searchDefault(Account_analytic_lineSearchContext context);

    Account_analytic_line convert2PO(cn.ibizlab.odoo.core.odoo_account.domain.Account_analytic_line domain , Account_analytic_line po) ;

    cn.ibizlab.odoo.core.odoo_account.domain.Account_analytic_line convert2Domain( Account_analytic_line po ,cn.ibizlab.odoo.core.odoo_account.domain.Account_analytic_line domain) ;

}
