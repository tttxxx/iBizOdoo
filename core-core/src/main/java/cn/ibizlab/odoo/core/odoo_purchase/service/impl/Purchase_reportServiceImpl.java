package cn.ibizlab.odoo.core.odoo_purchase.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_purchase.domain.Purchase_report;
import cn.ibizlab.odoo.core.odoo_purchase.filter.Purchase_reportSearchContext;
import cn.ibizlab.odoo.core.odoo_purchase.service.IPurchase_reportService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;



import cn.ibizlab.odoo.core.odoo_purchase.client.purchase_reportOdooClient;
import cn.ibizlab.odoo.core.odoo_purchase.clientmodel.purchase_reportClientModel;

/**
 * 实体[采购报表] 服务对象接口实现
 */
@Slf4j
@Service
public class Purchase_reportServiceImpl implements IPurchase_reportService {

    @Autowired
    purchase_reportOdooClient purchase_reportOdooClient;


    @Override
    public boolean update(Purchase_report et) {
        purchase_reportClientModel clientModel = convert2Model(et,null);
		purchase_reportOdooClient.update(clientModel);
        Purchase_report rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }
    
    public void updateBatch(List<Purchase_report> list){
    }

    @Override
    public boolean create(Purchase_report et) {
        purchase_reportClientModel clientModel = convert2Model(et,null);
		purchase_reportOdooClient.create(clientModel);
        Purchase_report rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Purchase_report> list){
    }

    @Override
    public boolean remove(Integer id) {
        purchase_reportClientModel clientModel = new purchase_reportClientModel();
        clientModel.setId(id);
		purchase_reportOdooClient.remove(clientModel);
        return true; 
    }

    public void removeBatch(Collection<Integer> idList){
    }

    @Override
    public Purchase_report get(Integer id) {
        purchase_reportClientModel clientModel = new purchase_reportClientModel();
        clientModel.setId(id);
		purchase_reportOdooClient.get(clientModel);
        Purchase_report et = convert2Domain(clientModel,null);
        if(et==null){
            et=new Purchase_report();
            et.setId(id);
        }
        else{
        }

        return  et;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Purchase_report> searchDefault(Purchase_reportSearchContext context) {
        List<Purchase_report> list = new ArrayList<Purchase_report>();
        Page<purchase_reportClientModel> clientModelList = purchase_reportOdooClient.search(context);
        for(purchase_reportClientModel clientModel : clientModelList){
            list.add( this.convert2Domain(clientModel,null));
        }
        return new PageImpl<Purchase_report>(list, context.getPageable(), clientModelList.getTotalElements());
    }

    public purchase_reportClientModel convert2Model(Purchase_report domain , purchase_reportClientModel model) {
        if(domain == null)
            return model ;
        if(model==null){
            model = new purchase_reportClientModel();

        }
        if((Boolean) domain.getExtensionparams().get("dirtyflagenable")){
            if((Boolean) domain.getExtensionparams().get("delay_passdirtyflag"))
                model.setDelay_pass(domain.getDelayPass());
            if((Boolean) domain.getExtensionparams().get("date_approvedirtyflag"))
                model.setDate_approve(domain.getDateApprove());
            if((Boolean) domain.getExtensionparams().get("volumedirtyflag"))
                model.setVolume(domain.getVolume());
            if((Boolean) domain.getExtensionparams().get("nbr_linesdirtyflag"))
                model.setNbr_lines(domain.getNbrLines());
            if((Boolean) domain.getExtensionparams().get("negociationdirtyflag"))
                model.setNegociation(domain.getNegociation());
            if((Boolean) domain.getExtensionparams().get("statedirtyflag"))
                model.setState(domain.getState());
            if((Boolean) domain.getExtensionparams().get("display_namedirtyflag"))
                model.setDisplay_name(domain.getDisplayName());
            if((Boolean) domain.getExtensionparams().get("price_totaldirtyflag"))
                model.setPrice_total(domain.getPriceTotal());
            if((Boolean) domain.getExtensionparams().get("weightdirtyflag"))
                model.setWeight(domain.getWeight());
            if((Boolean) domain.getExtensionparams().get("date_orderdirtyflag"))
                model.setDate_order(domain.getDateOrder());
            if((Boolean) domain.getExtensionparams().get("price_averagedirtyflag"))
                model.setPrice_average(domain.getPriceAverage());
            if((Boolean) domain.getExtensionparams().get("delaydirtyflag"))
                model.setDelay(domain.getDelay());
            if((Boolean) domain.getExtensionparams().get("unit_quantitydirtyflag"))
                model.setUnit_quantity(domain.getUnitQuantity());
            if((Boolean) domain.getExtensionparams().get("iddirtyflag"))
                model.setId(domain.getId());
            if((Boolean) domain.getExtensionparams().get("__last_updatedirtyflag"))
                model.set__last_update(domain.getLastUpdate());
            if((Boolean) domain.getExtensionparams().get("price_standarddirtyflag"))
                model.setPrice_standard(domain.getPriceStandard());
            if((Boolean) domain.getExtensionparams().get("category_id_textdirtyflag"))
                model.setCategory_id_text(domain.getCategoryIdText());
            if((Boolean) domain.getExtensionparams().get("company_id_textdirtyflag"))
                model.setCompany_id_text(domain.getCompanyIdText());
            if((Boolean) domain.getExtensionparams().get("partner_id_textdirtyflag"))
                model.setPartner_id_text(domain.getPartnerIdText());
            if((Boolean) domain.getExtensionparams().get("currency_id_textdirtyflag"))
                model.setCurrency_id_text(domain.getCurrencyIdText());
            if((Boolean) domain.getExtensionparams().get("product_tmpl_id_textdirtyflag"))
                model.setProduct_tmpl_id_text(domain.getProductTmplIdText());
            if((Boolean) domain.getExtensionparams().get("country_id_textdirtyflag"))
                model.setCountry_id_text(domain.getCountryIdText());
            if((Boolean) domain.getExtensionparams().get("picking_type_id_textdirtyflag"))
                model.setPicking_type_id_text(domain.getPickingTypeIdText());
            if((Boolean) domain.getExtensionparams().get("fiscal_position_id_textdirtyflag"))
                model.setFiscal_position_id_text(domain.getFiscalPositionIdText());
            if((Boolean) domain.getExtensionparams().get("account_analytic_id_textdirtyflag"))
                model.setAccount_analytic_id_text(domain.getAccountAnalyticIdText());
            if((Boolean) domain.getExtensionparams().get("user_id_textdirtyflag"))
                model.setUser_id_text(domain.getUserIdText());
            if((Boolean) domain.getExtensionparams().get("commercial_partner_id_textdirtyflag"))
                model.setCommercial_partner_id_text(domain.getCommercialPartnerIdText());
            if((Boolean) domain.getExtensionparams().get("product_uom_textdirtyflag"))
                model.setProduct_uom_text(domain.getProductUomText());
            if((Boolean) domain.getExtensionparams().get("product_id_textdirtyflag"))
                model.setProduct_id_text(domain.getProductIdText());
            if((Boolean) domain.getExtensionparams().get("category_iddirtyflag"))
                model.setCategory_id(domain.getCategoryId());
            if((Boolean) domain.getExtensionparams().get("company_iddirtyflag"))
                model.setCompany_id(domain.getCompanyId());
            if((Boolean) domain.getExtensionparams().get("currency_iddirtyflag"))
                model.setCurrency_id(domain.getCurrencyId());
            if((Boolean) domain.getExtensionparams().get("picking_type_iddirtyflag"))
                model.setPicking_type_id(domain.getPickingTypeId());
            if((Boolean) domain.getExtensionparams().get("user_iddirtyflag"))
                model.setUser_id(domain.getUserId());
            if((Boolean) domain.getExtensionparams().get("commercial_partner_iddirtyflag"))
                model.setCommercial_partner_id(domain.getCommercialPartnerId());
            if((Boolean) domain.getExtensionparams().get("product_tmpl_iddirtyflag"))
                model.setProduct_tmpl_id(domain.getProductTmplId());
            if((Boolean) domain.getExtensionparams().get("partner_iddirtyflag"))
                model.setPartner_id(domain.getPartnerId());
            if((Boolean) domain.getExtensionparams().get("product_iddirtyflag"))
                model.setProduct_id(domain.getProductId());
            if((Boolean) domain.getExtensionparams().get("product_uomdirtyflag"))
                model.setProduct_uom(domain.getProductUom());
            if((Boolean) domain.getExtensionparams().get("country_iddirtyflag"))
                model.setCountry_id(domain.getCountryId());
            if((Boolean) domain.getExtensionparams().get("fiscal_position_iddirtyflag"))
                model.setFiscal_position_id(domain.getFiscalPositionId());
            if((Boolean) domain.getExtensionparams().get("account_analytic_iddirtyflag"))
                model.setAccount_analytic_id(domain.getAccountAnalyticId());
            domain.getFocusNull().clear();
        }
        return model ;
    }

    public Purchase_report convert2Domain( purchase_reportClientModel model ,Purchase_report domain) {
        if(model == null)
            return domain ;
        if(domain == null){
            domain = new Purchase_report();
        }

        if(model.getDelay_passDirtyFlag())
            domain.setDelayPass(model.getDelay_pass());
        if(model.getDate_approveDirtyFlag())
            domain.setDateApprove(model.getDate_approve());
        if(model.getVolumeDirtyFlag())
            domain.setVolume(model.getVolume());
        if(model.getNbr_linesDirtyFlag())
            domain.setNbrLines(model.getNbr_lines());
        if(model.getNegociationDirtyFlag())
            domain.setNegociation(model.getNegociation());
        if(model.getStateDirtyFlag())
            domain.setState(model.getState());
        if(model.getDisplay_nameDirtyFlag())
            domain.setDisplayName(model.getDisplay_name());
        if(model.getPrice_totalDirtyFlag())
            domain.setPriceTotal(model.getPrice_total());
        if(model.getWeightDirtyFlag())
            domain.setWeight(model.getWeight());
        if(model.getDate_orderDirtyFlag())
            domain.setDateOrder(model.getDate_order());
        if(model.getPrice_averageDirtyFlag())
            domain.setPriceAverage(model.getPrice_average());
        if(model.getDelayDirtyFlag())
            domain.setDelay(model.getDelay());
        if(model.getUnit_quantityDirtyFlag())
            domain.setUnitQuantity(model.getUnit_quantity());
        if(model.getIdDirtyFlag())
            domain.setId(model.getId());
        if(model.get__last_updateDirtyFlag())
            domain.setLastUpdate(model.get__last_update());
        if(model.getPrice_standardDirtyFlag())
            domain.setPriceStandard(model.getPrice_standard());
        if(model.getCategory_id_textDirtyFlag())
            domain.setCategoryIdText(model.getCategory_id_text());
        if(model.getCompany_id_textDirtyFlag())
            domain.setCompanyIdText(model.getCompany_id_text());
        if(model.getPartner_id_textDirtyFlag())
            domain.setPartnerIdText(model.getPartner_id_text());
        if(model.getCurrency_id_textDirtyFlag())
            domain.setCurrencyIdText(model.getCurrency_id_text());
        if(model.getProduct_tmpl_id_textDirtyFlag())
            domain.setProductTmplIdText(model.getProduct_tmpl_id_text());
        if(model.getCountry_id_textDirtyFlag())
            domain.setCountryIdText(model.getCountry_id_text());
        if(model.getPicking_type_id_textDirtyFlag())
            domain.setPickingTypeIdText(model.getPicking_type_id_text());
        if(model.getFiscal_position_id_textDirtyFlag())
            domain.setFiscalPositionIdText(model.getFiscal_position_id_text());
        if(model.getAccount_analytic_id_textDirtyFlag())
            domain.setAccountAnalyticIdText(model.getAccount_analytic_id_text());
        if(model.getUser_id_textDirtyFlag())
            domain.setUserIdText(model.getUser_id_text());
        if(model.getCommercial_partner_id_textDirtyFlag())
            domain.setCommercialPartnerIdText(model.getCommercial_partner_id_text());
        if(model.getProduct_uom_textDirtyFlag())
            domain.setProductUomText(model.getProduct_uom_text());
        if(model.getProduct_id_textDirtyFlag())
            domain.setProductIdText(model.getProduct_id_text());
        if(model.getCategory_idDirtyFlag())
            domain.setCategoryId(model.getCategory_id());
        if(model.getCompany_idDirtyFlag())
            domain.setCompanyId(model.getCompany_id());
        if(model.getCurrency_idDirtyFlag())
            domain.setCurrencyId(model.getCurrency_id());
        if(model.getPicking_type_idDirtyFlag())
            domain.setPickingTypeId(model.getPicking_type_id());
        if(model.getUser_idDirtyFlag())
            domain.setUserId(model.getUser_id());
        if(model.getCommercial_partner_idDirtyFlag())
            domain.setCommercialPartnerId(model.getCommercial_partner_id());
        if(model.getProduct_tmpl_idDirtyFlag())
            domain.setProductTmplId(model.getProduct_tmpl_id());
        if(model.getPartner_idDirtyFlag())
            domain.setPartnerId(model.getPartner_id());
        if(model.getProduct_idDirtyFlag())
            domain.setProductId(model.getProduct_id());
        if(model.getProduct_uomDirtyFlag())
            domain.setProductUom(model.getProduct_uom());
        if(model.getCountry_idDirtyFlag())
            domain.setCountryId(model.getCountry_id());
        if(model.getFiscal_position_idDirtyFlag())
            domain.setFiscalPositionId(model.getFiscal_position_id());
        if(model.getAccount_analytic_idDirtyFlag())
            domain.setAccountAnalyticId(model.getAccount_analytic_id());
        return domain ;
    }

}

    



