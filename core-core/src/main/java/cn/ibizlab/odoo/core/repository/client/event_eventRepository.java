package cn.ibizlab.odoo.core.repository.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import cn.ibizlab.odoo.core.repository.client.po.event_event;

/**
 * 实体[event_event] 服务对象接口
 */
public interface event_eventRepository{


    public event_event createPO() ;
        public List<event_event> search();

        public void create(event_event event_event);

        public void get(String id);

        public void createBatch(event_event event_event);

        public void updateBatch(event_event event_event);

        public void remove(String id);

        public void update(event_event event_event);

        public void removeBatch(String id);


}
