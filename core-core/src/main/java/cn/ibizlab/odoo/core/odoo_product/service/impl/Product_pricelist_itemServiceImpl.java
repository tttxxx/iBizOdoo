package cn.ibizlab.odoo.core.odoo_product.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_product.domain.Product_pricelist_item;
import cn.ibizlab.odoo.core.odoo_product.filter.Product_pricelist_itemSearchContext;
import cn.ibizlab.odoo.core.odoo_product.service.IProduct_pricelist_itemService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;



import cn.ibizlab.odoo.core.odoo_product.client.product_pricelist_itemOdooClient;
import cn.ibizlab.odoo.core.odoo_product.clientmodel.product_pricelist_itemClientModel;

/**
 * 实体[价格表明细] 服务对象接口实现
 */
@Slf4j
@Service
public class Product_pricelist_itemServiceImpl implements IProduct_pricelist_itemService {

    @Autowired
    product_pricelist_itemOdooClient product_pricelist_itemOdooClient;


    @Override
    public Product_pricelist_item get(Integer id) {
        product_pricelist_itemClientModel clientModel = new product_pricelist_itemClientModel();
        clientModel.setId(id);
		product_pricelist_itemOdooClient.get(clientModel);
        Product_pricelist_item et = convert2Domain(clientModel,null);
        if(et==null){
            et=new Product_pricelist_item();
            et.setId(id);
        }
        else{
        }

        return  et;
    }

    @Override
    public boolean create(Product_pricelist_item et) {
        product_pricelist_itemClientModel clientModel = convert2Model(et,null);
		product_pricelist_itemOdooClient.create(clientModel);
        Product_pricelist_item rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Product_pricelist_item> list){
    }

    @Override
    public boolean remove(Integer id) {
        product_pricelist_itemClientModel clientModel = new product_pricelist_itemClientModel();
        clientModel.setId(id);
		product_pricelist_itemOdooClient.remove(clientModel);
        return true; 
    }

    public void removeBatch(Collection<Integer> idList){
    }

    @Override
    public boolean update(Product_pricelist_item et) {
        product_pricelist_itemClientModel clientModel = convert2Model(et,null);
		product_pricelist_itemOdooClient.update(clientModel);
        Product_pricelist_item rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }
    
    public void updateBatch(List<Product_pricelist_item> list){
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Product_pricelist_item> searchDefault(Product_pricelist_itemSearchContext context) {
        List<Product_pricelist_item> list = new ArrayList<Product_pricelist_item>();
        Page<product_pricelist_itemClientModel> clientModelList = product_pricelist_itemOdooClient.search(context);
        for(product_pricelist_itemClientModel clientModel : clientModelList){
            list.add( this.convert2Domain(clientModel,null));
        }
        return new PageImpl<Product_pricelist_item>(list, context.getPageable(), clientModelList.getTotalElements());
    }

    public product_pricelist_itemClientModel convert2Model(Product_pricelist_item domain , product_pricelist_itemClientModel model) {
        if(domain == null)
            return model ;
        if(model==null){
            model = new product_pricelist_itemClientModel();

        }
        if((Boolean) domain.getExtensionparams().get("dirtyflagenable")){
            if((Boolean) domain.getExtensionparams().get("display_namedirtyflag"))
                model.setDisplay_name(domain.getDisplayName());
            if((Boolean) domain.getExtensionparams().get("fixed_pricedirtyflag"))
                model.setFixed_price(domain.getFixedPrice());
            if((Boolean) domain.getExtensionparams().get("namedirtyflag"))
                model.setName(domain.getName());
            if((Boolean) domain.getExtensionparams().get("pricedirtyflag"))
                model.setPrice(domain.getPrice());
            if((Boolean) domain.getExtensionparams().get("__last_updatedirtyflag"))
                model.set__last_update(domain.getLastUpdate());
            if((Boolean) domain.getExtensionparams().get("basedirtyflag"))
                model.setBase(domain.getBase());
            if((Boolean) domain.getExtensionparams().get("date_enddirtyflag"))
                model.setDate_end(domain.getDateEnd());
            if((Boolean) domain.getExtensionparams().get("percent_pricedirtyflag"))
                model.setPercent_price(domain.getPercentPrice());
            if((Boolean) domain.getExtensionparams().get("price_rounddirtyflag"))
                model.setPrice_round(domain.getPriceRound());
            if((Boolean) domain.getExtensionparams().get("write_datedirtyflag"))
                model.setWrite_date(domain.getWriteDate());
            if((Boolean) domain.getExtensionparams().get("iddirtyflag"))
                model.setId(domain.getId());
            if((Boolean) domain.getExtensionparams().get("create_datedirtyflag"))
                model.setCreate_date(domain.getCreateDate());
            if((Boolean) domain.getExtensionparams().get("compute_pricedirtyflag"))
                model.setCompute_price(domain.getComputePrice());
            if((Boolean) domain.getExtensionparams().get("price_min_margindirtyflag"))
                model.setPrice_min_margin(domain.getPriceMinMargin());
            if((Boolean) domain.getExtensionparams().get("date_startdirtyflag"))
                model.setDate_start(domain.getDateStart());
            if((Boolean) domain.getExtensionparams().get("price_max_margindirtyflag"))
                model.setPrice_max_margin(domain.getPriceMaxMargin());
            if((Boolean) domain.getExtensionparams().get("applied_ondirtyflag"))
                model.setApplied_on(domain.getAppliedOn());
            if((Boolean) domain.getExtensionparams().get("min_quantitydirtyflag"))
                model.setMin_quantity(domain.getMinQuantity());
            if((Boolean) domain.getExtensionparams().get("price_surchargedirtyflag"))
                model.setPrice_surcharge(domain.getPriceSurcharge());
            if((Boolean) domain.getExtensionparams().get("price_discountdirtyflag"))
                model.setPrice_discount(domain.getPriceDiscount());
            if((Boolean) domain.getExtensionparams().get("pricelist_id_textdirtyflag"))
                model.setPricelist_id_text(domain.getPricelistIdText());
            if((Boolean) domain.getExtensionparams().get("currency_id_textdirtyflag"))
                model.setCurrency_id_text(domain.getCurrencyIdText());
            if((Boolean) domain.getExtensionparams().get("categ_id_textdirtyflag"))
                model.setCateg_id_text(domain.getCategIdText());
            if((Boolean) domain.getExtensionparams().get("base_pricelist_id_textdirtyflag"))
                model.setBase_pricelist_id_text(domain.getBasePricelistIdText());
            if((Boolean) domain.getExtensionparams().get("write_uid_textdirtyflag"))
                model.setWrite_uid_text(domain.getWriteUidText());
            if((Boolean) domain.getExtensionparams().get("product_tmpl_id_textdirtyflag"))
                model.setProduct_tmpl_id_text(domain.getProductTmplIdText());
            if((Boolean) domain.getExtensionparams().get("create_uid_textdirtyflag"))
                model.setCreate_uid_text(domain.getCreateUidText());
            if((Boolean) domain.getExtensionparams().get("company_id_textdirtyflag"))
                model.setCompany_id_text(domain.getCompanyIdText());
            if((Boolean) domain.getExtensionparams().get("product_id_textdirtyflag"))
                model.setProduct_id_text(domain.getProductIdText());
            if((Boolean) domain.getExtensionparams().get("product_iddirtyflag"))
                model.setProduct_id(domain.getProductId());
            if((Boolean) domain.getExtensionparams().get("pricelist_iddirtyflag"))
                model.setPricelist_id(domain.getPricelistId());
            if((Boolean) domain.getExtensionparams().get("write_uiddirtyflag"))
                model.setWrite_uid(domain.getWriteUid());
            if((Boolean) domain.getExtensionparams().get("company_iddirtyflag"))
                model.setCompany_id(domain.getCompanyId());
            if((Boolean) domain.getExtensionparams().get("base_pricelist_iddirtyflag"))
                model.setBase_pricelist_id(domain.getBasePricelistId());
            if((Boolean) domain.getExtensionparams().get("currency_iddirtyflag"))
                model.setCurrency_id(domain.getCurrencyId());
            if((Boolean) domain.getExtensionparams().get("categ_iddirtyflag"))
                model.setCateg_id(domain.getCategId());
            if((Boolean) domain.getExtensionparams().get("create_uiddirtyflag"))
                model.setCreate_uid(domain.getCreateUid());
            if((Boolean) domain.getExtensionparams().get("product_tmpl_iddirtyflag"))
                model.setProduct_tmpl_id(domain.getProductTmplId());
            domain.getFocusNull().clear();
        }
        return model ;
    }

    public Product_pricelist_item convert2Domain( product_pricelist_itemClientModel model ,Product_pricelist_item domain) {
        if(model == null)
            return domain ;
        if(domain == null){
            domain = new Product_pricelist_item();
        }

        if(model.getDisplay_nameDirtyFlag())
            domain.setDisplayName(model.getDisplay_name());
        if(model.getFixed_priceDirtyFlag())
            domain.setFixedPrice(model.getFixed_price());
        if(model.getNameDirtyFlag())
            domain.setName(model.getName());
        if(model.getPriceDirtyFlag())
            domain.setPrice(model.getPrice());
        if(model.get__last_updateDirtyFlag())
            domain.setLastUpdate(model.get__last_update());
        if(model.getBaseDirtyFlag())
            domain.setBase(model.getBase());
        if(model.getDate_endDirtyFlag())
            domain.setDateEnd(model.getDate_end());
        if(model.getPercent_priceDirtyFlag())
            domain.setPercentPrice(model.getPercent_price());
        if(model.getPrice_roundDirtyFlag())
            domain.setPriceRound(model.getPrice_round());
        if(model.getWrite_dateDirtyFlag())
            domain.setWriteDate(model.getWrite_date());
        if(model.getIdDirtyFlag())
            domain.setId(model.getId());
        if(model.getCreate_dateDirtyFlag())
            domain.setCreateDate(model.getCreate_date());
        if(model.getCompute_priceDirtyFlag())
            domain.setComputePrice(model.getCompute_price());
        if(model.getPrice_min_marginDirtyFlag())
            domain.setPriceMinMargin(model.getPrice_min_margin());
        if(model.getDate_startDirtyFlag())
            domain.setDateStart(model.getDate_start());
        if(model.getPrice_max_marginDirtyFlag())
            domain.setPriceMaxMargin(model.getPrice_max_margin());
        if(model.getApplied_onDirtyFlag())
            domain.setAppliedOn(model.getApplied_on());
        if(model.getMin_quantityDirtyFlag())
            domain.setMinQuantity(model.getMin_quantity());
        if(model.getPrice_surchargeDirtyFlag())
            domain.setPriceSurcharge(model.getPrice_surcharge());
        if(model.getPrice_discountDirtyFlag())
            domain.setPriceDiscount(model.getPrice_discount());
        if(model.getPricelist_id_textDirtyFlag())
            domain.setPricelistIdText(model.getPricelist_id_text());
        if(model.getCurrency_id_textDirtyFlag())
            domain.setCurrencyIdText(model.getCurrency_id_text());
        if(model.getCateg_id_textDirtyFlag())
            domain.setCategIdText(model.getCateg_id_text());
        if(model.getBase_pricelist_id_textDirtyFlag())
            domain.setBasePricelistIdText(model.getBase_pricelist_id_text());
        if(model.getWrite_uid_textDirtyFlag())
            domain.setWriteUidText(model.getWrite_uid_text());
        if(model.getProduct_tmpl_id_textDirtyFlag())
            domain.setProductTmplIdText(model.getProduct_tmpl_id_text());
        if(model.getCreate_uid_textDirtyFlag())
            domain.setCreateUidText(model.getCreate_uid_text());
        if(model.getCompany_id_textDirtyFlag())
            domain.setCompanyIdText(model.getCompany_id_text());
        if(model.getProduct_id_textDirtyFlag())
            domain.setProductIdText(model.getProduct_id_text());
        if(model.getProduct_idDirtyFlag())
            domain.setProductId(model.getProduct_id());
        if(model.getPricelist_idDirtyFlag())
            domain.setPricelistId(model.getPricelist_id());
        if(model.getWrite_uidDirtyFlag())
            domain.setWriteUid(model.getWrite_uid());
        if(model.getCompany_idDirtyFlag())
            domain.setCompanyId(model.getCompany_id());
        if(model.getBase_pricelist_idDirtyFlag())
            domain.setBasePricelistId(model.getBase_pricelist_id());
        if(model.getCurrency_idDirtyFlag())
            domain.setCurrencyId(model.getCurrency_id());
        if(model.getCateg_idDirtyFlag())
            domain.setCategId(model.getCateg_id());
        if(model.getCreate_uidDirtyFlag())
            domain.setCreateUid(model.getCreate_uid());
        if(model.getProduct_tmpl_idDirtyFlag())
            domain.setProductTmplId(model.getProduct_tmpl_id());
        return domain ;
    }

}

    



