package cn.ibizlab.odoo.core.repository.client.po;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigDecimal;

/**
 * [gamification_goal] 对象
 */
public interface gamification_goal {

    public Integer getChallenge_id();

    public void setChallenge_id(Integer challenge_id);

    public String getChallenge_id_text();

    public void setChallenge_id_text(String challenge_id_text);

    public String getClosed();

    public void setClosed(String closed);

    public Double getCompleteness();

    public void setCompleteness(Double completeness);

    public String getComputation_mode();

    public void setComputation_mode(String computation_mode);

    public Timestamp getCreate_date();

    public void setCreate_date(Timestamp create_date);

    public Integer getCreate_uid();

    public void setCreate_uid(Integer create_uid);

    public String getCreate_uid_text();

    public void setCreate_uid_text(String create_uid_text);

    public Double getCurrent();

    public void setCurrent(Double current);

    public String getDefinition_condition();

    public void setDefinition_condition(String definition_condition);

    public String getDefinition_description();

    public void setDefinition_description(String definition_description);

    public String getDefinition_display();

    public void setDefinition_display(String definition_display);

    public Integer getDefinition_id();

    public void setDefinition_id(Integer definition_id);

    public String getDefinition_id_text();

    public void setDefinition_id_text(String definition_id_text);

    public String getDefinition_suffix();

    public void setDefinition_suffix(String definition_suffix);

    public String getDisplay_name();

    public void setDisplay_name(String display_name);

    public Timestamp getEnd_date();

    public void setEnd_date(Timestamp end_date);

    public Integer getId();

    public void setId(Integer id);

    public Timestamp getLast_update();

    public void setLast_update(Timestamp last_update);

    public Integer getLine_id();

    public void setLine_id(Integer line_id);

    public String getLine_id_text();

    public void setLine_id_text(String line_id_text);

    public Integer getRemind_update_delay();

    public void setRemind_update_delay(Integer remind_update_delay);

    public Timestamp getStart_date();

    public void setStart_date(Timestamp start_date);

    public String getState();

    public void setState(String state);

    public Double getTarget_goal();

    public void setTarget_goal(Double target_goal);

    public String getTo_update();

    public void setTo_update(String to_update);

    public Integer getUser_id();

    public void setUser_id(Integer user_id);

    public String getUser_id_text();

    public void setUser_id_text(String user_id_text);

    public Timestamp getWrite_date();

    public void setWrite_date(Timestamp write_date);

    public Integer getWrite_uid();

    public void setWrite_uid(Integer write_uid);

    public String getWrite_uid_text();

    public void setWrite_uid_text(String write_uid_text);

    public Timestamp get__last_update();

    public void set__last_update(Timestamp __last_update);

}
