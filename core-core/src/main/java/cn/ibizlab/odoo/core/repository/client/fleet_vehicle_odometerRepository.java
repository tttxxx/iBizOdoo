package cn.ibizlab.odoo.core.repository.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import cn.ibizlab.odoo.core.repository.client.po.fleet_vehicle_odometer;

/**
 * 实体[fleet_vehicle_odometer] 服务对象接口
 */
public interface fleet_vehicle_odometerRepository{


    public fleet_vehicle_odometer createPO() ;
        public List<fleet_vehicle_odometer> search();

        public void removeBatch(String id);

        public void createBatch(fleet_vehicle_odometer fleet_vehicle_odometer);

        public void remove(String id);

        public void updateBatch(fleet_vehicle_odometer fleet_vehicle_odometer);

        public void create(fleet_vehicle_odometer fleet_vehicle_odometer);

        public void get(String id);

        public void update(fleet_vehicle_odometer fleet_vehicle_odometer);


}
