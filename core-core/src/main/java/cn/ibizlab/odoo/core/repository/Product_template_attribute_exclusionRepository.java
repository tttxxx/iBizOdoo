package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Product_template_attribute_exclusion;
import cn.ibizlab.odoo.core.odoo_product.filter.Product_template_attribute_exclusionSearchContext;

/**
 * 实体 [产品模板属性排除] 存储对象
 */
public interface Product_template_attribute_exclusionRepository extends Repository<Product_template_attribute_exclusion> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Product_template_attribute_exclusion> searchDefault(Product_template_attribute_exclusionSearchContext context);

    Product_template_attribute_exclusion convert2PO(cn.ibizlab.odoo.core.odoo_product.domain.Product_template_attribute_exclusion domain , Product_template_attribute_exclusion po) ;

    cn.ibizlab.odoo.core.odoo_product.domain.Product_template_attribute_exclusion convert2Domain( Product_template_attribute_exclusion po ,cn.ibizlab.odoo.core.odoo_product.domain.Product_template_attribute_exclusion domain) ;

}
