package cn.ibizlab.odoo.core.odoo_event.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.odoo.util.domain.EntityBase;
import cn.ibizlab.odoo.util.annotation.DEField;
import cn.ibizlab.odoo.util.enums.DEPredefinedFieldType;
import cn.ibizlab.odoo.util.enums.DEFieldDefaultValueType;
import java.io.Serializable;
import lombok.Data;
import org.springframework.data.annotation.Transient;

import cn.ibizlab.odoo.util.domain.EntityClient;

/**
 * ServiceApi [活动类别] 对象
 */
@Data
public class Event_type extends EntityClient implements Serializable {

    /**
     * 最大注册数
     */
    @DEField(name = "default_registration_max")
    @JSONField(name = "default_registration_max")
    @JsonProperty("default_registration_max")
    private Integer defaultRegistrationMax;

    /**
     * 邮件排程
     */
    @JSONField(name = "event_type_mail_ids")
    @JsonProperty("event_type_mail_ids")
    private String eventTypeMailIds;

    /**
     * Twitter主题标签
     */
    @DEField(name = "default_hashtag")
    @JSONField(name = "default_hashtag")
    @JsonProperty("default_hashtag")
    private String defaultHashtag;

    /**
     * 时区
     */
    @DEField(name = "default_timezone")
    @JSONField(name = "default_timezone")
    @JsonProperty("default_timezone")
    private String defaultTimezone;

    /**
     * 最小注册数
     */
    @DEField(name = "default_registration_min")
    @JSONField(name = "default_registration_min")
    @JsonProperty("default_registration_min")
    private Integer defaultRegistrationMin;

    /**
     * 入场券
     */
    @JSONField(name = "event_ticket_ids")
    @JsonProperty("event_ticket_ids")
    private String eventTicketIds;

    /**
     * 出票
     */
    @DEField(name = "use_ticketing")
    @JSONField(name = "use_ticketing")
    @JsonProperty("use_ticketing")
    private String useTicketing;

    /**
     * ID
     */
    @DEField(isKeyField=true)
    @JSONField(name = "id")
    @JsonProperty("id")
    private Integer id;

    /**
     * 在线活动
     */
    @DEField(name = "is_online")
    @JSONField(name = "is_online")
    @JsonProperty("is_online")
    private String isOnline;

    /**
     * 活动类别
     */
    @JSONField(name = "name")
    @JsonProperty("name")
    private String name;

    /**
     * 创建时间
     */
    @DEField(name = "create_date" , preType = DEPredefinedFieldType.CREATEDATE)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;

    /**
     * 最后更新时间
     */
    @DEField(name = "write_date" , preType = DEPredefinedFieldType.UPDATEDATE)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;

    /**
     * 自动确认注册
     */
    @DEField(name = "auto_confirm")
    @JSONField(name = "auto_confirm")
    @JsonProperty("auto_confirm")
    private String autoConfirm;

    /**
     * 最后修改日
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;

    /**
     * 在网站上显示专用菜单
     */
    @DEField(name = "website_menu")
    @JSONField(name = "website_menu")
    @JsonProperty("website_menu")
    private String websiteMenu;

    /**
     * 使用默认主题标签
     */
    @DEField(name = "use_hashtag")
    @JSONField(name = "use_hashtag")
    @JsonProperty("use_hashtag")
    private String useHashtag;

    /**
     * 显示名称
     */
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    private String displayName;

    /**
     * 有限名额
     */
    @DEField(name = "has_seats_limitation")
    @JSONField(name = "has_seats_limitation")
    @JsonProperty("has_seats_limitation")
    private String hasSeatsLimitation;

    /**
     * 自动发送EMail
     */
    @DEField(name = "use_mail_schedule")
    @JSONField(name = "use_mail_schedule")
    @JsonProperty("use_mail_schedule")
    private String useMailSchedule;

    /**
     * 使用默认时区
     */
    @DEField(name = "use_timezone")
    @JSONField(name = "use_timezone")
    @JsonProperty("use_timezone")
    private String useTimezone;

    /**
     * 创建人
     */
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    private String createUidText;

    /**
     * 最后更新人
     */
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    private String writeUidText;

    /**
     * 最后更新人
     */
    @DEField(name = "write_uid" , preType = DEPredefinedFieldType.UPDATEMAN)
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    private Integer writeUid;

    /**
     * 创建人
     */
    @DEField(name = "create_uid" , preType = DEPredefinedFieldType.CREATEMAN)
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    private Integer createUid;


    /**
     * 
     */
    @JSONField(name = "odoocreate")
    @JsonProperty("odoocreate")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_users odooCreate;

    /**
     * 
     */
    @JSONField(name = "odoowrite")
    @JsonProperty("odoowrite")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_users odooWrite;




    /**
     * 设置 [最大注册数]
     */
    public void setDefaultRegistrationMax(Integer defaultRegistrationMax){
        this.defaultRegistrationMax = defaultRegistrationMax ;
        this.modify("default_registration_max",defaultRegistrationMax);
    }
    /**
     * 设置 [Twitter主题标签]
     */
    public void setDefaultHashtag(String defaultHashtag){
        this.defaultHashtag = defaultHashtag ;
        this.modify("default_hashtag",defaultHashtag);
    }
    /**
     * 设置 [时区]
     */
    public void setDefaultTimezone(String defaultTimezone){
        this.defaultTimezone = defaultTimezone ;
        this.modify("default_timezone",defaultTimezone);
    }
    /**
     * 设置 [最小注册数]
     */
    public void setDefaultRegistrationMin(Integer defaultRegistrationMin){
        this.defaultRegistrationMin = defaultRegistrationMin ;
        this.modify("default_registration_min",defaultRegistrationMin);
    }
    /**
     * 设置 [出票]
     */
    public void setUseTicketing(String useTicketing){
        this.useTicketing = useTicketing ;
        this.modify("use_ticketing",useTicketing);
    }
    /**
     * 设置 [在线活动]
     */
    public void setIsOnline(String isOnline){
        this.isOnline = isOnline ;
        this.modify("is_online",isOnline);
    }
    /**
     * 设置 [活动类别]
     */
    public void setName(String name){
        this.name = name ;
        this.modify("name",name);
    }
    /**
     * 设置 [自动确认注册]
     */
    public void setAutoConfirm(String autoConfirm){
        this.autoConfirm = autoConfirm ;
        this.modify("auto_confirm",autoConfirm);
    }
    /**
     * 设置 [在网站上显示专用菜单]
     */
    public void setWebsiteMenu(String websiteMenu){
        this.websiteMenu = websiteMenu ;
        this.modify("website_menu",websiteMenu);
    }
    /**
     * 设置 [使用默认主题标签]
     */
    public void setUseHashtag(String useHashtag){
        this.useHashtag = useHashtag ;
        this.modify("use_hashtag",useHashtag);
    }
    /**
     * 设置 [有限名额]
     */
    public void setHasSeatsLimitation(String hasSeatsLimitation){
        this.hasSeatsLimitation = hasSeatsLimitation ;
        this.modify("has_seats_limitation",hasSeatsLimitation);
    }
    /**
     * 设置 [自动发送EMail]
     */
    public void setUseMailSchedule(String useMailSchedule){
        this.useMailSchedule = useMailSchedule ;
        this.modify("use_mail_schedule",useMailSchedule);
    }
    /**
     * 设置 [使用默认时区]
     */
    public void setUseTimezone(String useTimezone){
        this.useTimezone = useTimezone ;
        this.modify("use_timezone",useTimezone);
    }

}


