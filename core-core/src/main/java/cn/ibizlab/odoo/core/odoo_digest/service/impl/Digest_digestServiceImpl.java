package cn.ibizlab.odoo.core.odoo_digest.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_digest.domain.Digest_digest;
import cn.ibizlab.odoo.core.odoo_digest.filter.Digest_digestSearchContext;
import cn.ibizlab.odoo.core.odoo_digest.service.IDigest_digestService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;



import cn.ibizlab.odoo.core.odoo_digest.client.digest_digestOdooClient;
import cn.ibizlab.odoo.core.odoo_digest.clientmodel.digest_digestClientModel;

/**
 * 实体[摘要] 服务对象接口实现
 */
@Slf4j
@Service
public class Digest_digestServiceImpl implements IDigest_digestService {

    @Autowired
    digest_digestOdooClient digest_digestOdooClient;


    @Override
    public boolean create(Digest_digest et) {
        digest_digestClientModel clientModel = convert2Model(et,null);
		digest_digestOdooClient.create(clientModel);
        Digest_digest rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Digest_digest> list){
    }

    @Override
    public Digest_digest get(Integer id) {
        digest_digestClientModel clientModel = new digest_digestClientModel();
        clientModel.setId(id);
		digest_digestOdooClient.get(clientModel);
        Digest_digest et = convert2Domain(clientModel,null);
        if(et==null){
            et=new Digest_digest();
            et.setId(id);
        }
        else{
        }

        return  et;
    }

    @Override
    public boolean remove(Integer id) {
        digest_digestClientModel clientModel = new digest_digestClientModel();
        clientModel.setId(id);
		digest_digestOdooClient.remove(clientModel);
        return true; 
    }

    public void removeBatch(Collection<Integer> idList){
    }

    @Override
    public boolean update(Digest_digest et) {
        digest_digestClientModel clientModel = convert2Model(et,null);
		digest_digestOdooClient.update(clientModel);
        Digest_digest rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }
    
    public void updateBatch(List<Digest_digest> list){
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Digest_digest> searchDefault(Digest_digestSearchContext context) {
        List<Digest_digest> list = new ArrayList<Digest_digest>();
        Page<digest_digestClientModel> clientModelList = digest_digestOdooClient.search(context);
        for(digest_digestClientModel clientModel : clientModelList){
            list.add( this.convert2Domain(clientModel,null));
        }
        return new PageImpl<Digest_digest>(list, context.getPageable(), clientModelList.getTotalElements());
    }

    public digest_digestClientModel convert2Model(Digest_digest domain , digest_digestClientModel model) {
        if(domain == null)
            return model ;
        if(model==null){
            model = new digest_digestClientModel();

        }
        if((Boolean) domain.getExtensionparams().get("dirtyflagenable")){
            if((Boolean) domain.getExtensionparams().get("namedirtyflag"))
                model.setName(domain.getName());
            if((Boolean) domain.getExtensionparams().get("kpi_website_sale_total_valuedirtyflag"))
                model.setKpi_website_sale_total_value(domain.getKpiWebsiteSaleTotalValue());
            if((Boolean) domain.getExtensionparams().get("create_datedirtyflag"))
                model.setCreate_date(domain.getCreateDate());
            if((Boolean) domain.getExtensionparams().get("kpi_crm_lead_created_valuedirtyflag"))
                model.setKpi_crm_lead_created_value(domain.getKpiCrmLeadCreatedValue());
            if((Boolean) domain.getExtensionparams().get("kpi_crm_opportunities_wondirtyflag"))
                model.setKpi_crm_opportunities_won(domain.getKpiCrmOpportunitiesWon());
            if((Boolean) domain.getExtensionparams().get("available_fieldsdirtyflag"))
                model.setAvailable_fields(domain.getAvailableFields());
            if((Boolean) domain.getExtensionparams().get("statedirtyflag"))
                model.setState(domain.getState());
            if((Boolean) domain.getExtensionparams().get("user_idsdirtyflag"))
                model.setUser_ids(domain.getUserIds());
            if((Boolean) domain.getExtensionparams().get("kpi_crm_opportunities_won_valuedirtyflag"))
                model.setKpi_crm_opportunities_won_value(domain.getKpiCrmOpportunitiesWonValue());
            if((Boolean) domain.getExtensionparams().get("kpi_hr_recruitment_new_colleaguesdirtyflag"))
                model.setKpi_hr_recruitment_new_colleagues(domain.getKpiHrRecruitmentNewColleagues());
            if((Boolean) domain.getExtensionparams().get("__last_updatedirtyflag"))
                model.set__last_update(domain.getLastUpdate());
            if((Boolean) domain.getExtensionparams().get("kpi_pos_totaldirtyflag"))
                model.setKpi_pos_total(domain.getKpiPosTotal());
            if((Boolean) domain.getExtensionparams().get("write_datedirtyflag"))
                model.setWrite_date(domain.getWriteDate());
            if((Boolean) domain.getExtensionparams().get("kpi_pos_total_valuedirtyflag"))
                model.setKpi_pos_total_value(domain.getKpiPosTotalValue());
            if((Boolean) domain.getExtensionparams().get("kpi_website_sale_totaldirtyflag"))
                model.setKpi_website_sale_total(domain.getKpiWebsiteSaleTotal());
            if((Boolean) domain.getExtensionparams().get("kpi_all_sale_totaldirtyflag"))
                model.setKpi_all_sale_total(domain.getKpiAllSaleTotal());
            if((Boolean) domain.getExtensionparams().get("next_run_datedirtyflag"))
                model.setNext_run_date(domain.getNextRunDate());
            if((Boolean) domain.getExtensionparams().get("kpi_res_users_connecteddirtyflag"))
                model.setKpi_res_users_connected(domain.getKpiResUsersConnected());
            if((Boolean) domain.getExtensionparams().get("kpi_res_users_connected_valuedirtyflag"))
                model.setKpi_res_users_connected_value(domain.getKpiResUsersConnectedValue());
            if((Boolean) domain.getExtensionparams().get("periodicitydirtyflag"))
                model.setPeriodicity(domain.getPeriodicity());
            if((Boolean) domain.getExtensionparams().get("iddirtyflag"))
                model.setId(domain.getId());
            if((Boolean) domain.getExtensionparams().get("kpi_hr_recruitment_new_colleagues_valuedirtyflag"))
                model.setKpi_hr_recruitment_new_colleagues_value(domain.getKpiHrRecruitmentNewColleaguesValue());
            if((Boolean) domain.getExtensionparams().get("kpi_account_total_revenuedirtyflag"))
                model.setKpi_account_total_revenue(domain.getKpiAccountTotalRevenue());
            if((Boolean) domain.getExtensionparams().get("kpi_crm_lead_createddirtyflag"))
                model.setKpi_crm_lead_created(domain.getKpiCrmLeadCreated());
            if((Boolean) domain.getExtensionparams().get("kpi_project_task_openeddirtyflag"))
                model.setKpi_project_task_opened(domain.getKpiProjectTaskOpened());
            if((Boolean) domain.getExtensionparams().get("kpi_all_sale_total_valuedirtyflag"))
                model.setKpi_all_sale_total_value(domain.getKpiAllSaleTotalValue());
            if((Boolean) domain.getExtensionparams().get("is_subscribeddirtyflag"))
                model.setIs_subscribed(domain.getIsSubscribed());
            if((Boolean) domain.getExtensionparams().get("display_namedirtyflag"))
                model.setDisplay_name(domain.getDisplayName());
            if((Boolean) domain.getExtensionparams().get("kpi_project_task_opened_valuedirtyflag"))
                model.setKpi_project_task_opened_value(domain.getKpiProjectTaskOpenedValue());
            if((Boolean) domain.getExtensionparams().get("kpi_account_total_revenue_valuedirtyflag"))
                model.setKpi_account_total_revenue_value(domain.getKpiAccountTotalRevenueValue());
            if((Boolean) domain.getExtensionparams().get("kpi_mail_message_total_valuedirtyflag"))
                model.setKpi_mail_message_total_value(domain.getKpiMailMessageTotalValue());
            if((Boolean) domain.getExtensionparams().get("kpi_mail_message_totaldirtyflag"))
                model.setKpi_mail_message_total(domain.getKpiMailMessageTotal());
            if((Boolean) domain.getExtensionparams().get("write_uid_textdirtyflag"))
                model.setWrite_uid_text(domain.getWriteUidText());
            if((Boolean) domain.getExtensionparams().get("template_id_textdirtyflag"))
                model.setTemplate_id_text(domain.getTemplateIdText());
            if((Boolean) domain.getExtensionparams().get("company_id_textdirtyflag"))
                model.setCompany_id_text(domain.getCompanyIdText());
            if((Boolean) domain.getExtensionparams().get("currency_iddirtyflag"))
                model.setCurrency_id(domain.getCurrencyId());
            if((Boolean) domain.getExtensionparams().get("create_uid_textdirtyflag"))
                model.setCreate_uid_text(domain.getCreateUidText());
            if((Boolean) domain.getExtensionparams().get("create_uiddirtyflag"))
                model.setCreate_uid(domain.getCreateUid());
            if((Boolean) domain.getExtensionparams().get("write_uiddirtyflag"))
                model.setWrite_uid(domain.getWriteUid());
            if((Boolean) domain.getExtensionparams().get("company_iddirtyflag"))
                model.setCompany_id(domain.getCompanyId());
            if((Boolean) domain.getExtensionparams().get("template_iddirtyflag"))
                model.setTemplate_id(domain.getTemplateId());
            domain.getFocusNull().clear();
        }
        return model ;
    }

    public Digest_digest convert2Domain( digest_digestClientModel model ,Digest_digest domain) {
        if(model == null)
            return domain ;
        if(domain == null){
            domain = new Digest_digest();
        }

        if(model.getNameDirtyFlag())
            domain.setName(model.getName());
        if(model.getKpi_website_sale_total_valueDirtyFlag())
            domain.setKpiWebsiteSaleTotalValue(model.getKpi_website_sale_total_value());
        if(model.getCreate_dateDirtyFlag())
            domain.setCreateDate(model.getCreate_date());
        if(model.getKpi_crm_lead_created_valueDirtyFlag())
            domain.setKpiCrmLeadCreatedValue(model.getKpi_crm_lead_created_value());
        if(model.getKpi_crm_opportunities_wonDirtyFlag())
            domain.setKpiCrmOpportunitiesWon(model.getKpi_crm_opportunities_won());
        if(model.getAvailable_fieldsDirtyFlag())
            domain.setAvailableFields(model.getAvailable_fields());
        if(model.getStateDirtyFlag())
            domain.setState(model.getState());
        if(model.getUser_idsDirtyFlag())
            domain.setUserIds(model.getUser_ids());
        if(model.getKpi_crm_opportunities_won_valueDirtyFlag())
            domain.setKpiCrmOpportunitiesWonValue(model.getKpi_crm_opportunities_won_value());
        if(model.getKpi_hr_recruitment_new_colleaguesDirtyFlag())
            domain.setKpiHrRecruitmentNewColleagues(model.getKpi_hr_recruitment_new_colleagues());
        if(model.get__last_updateDirtyFlag())
            domain.setLastUpdate(model.get__last_update());
        if(model.getKpi_pos_totalDirtyFlag())
            domain.setKpiPosTotal(model.getKpi_pos_total());
        if(model.getWrite_dateDirtyFlag())
            domain.setWriteDate(model.getWrite_date());
        if(model.getKpi_pos_total_valueDirtyFlag())
            domain.setKpiPosTotalValue(model.getKpi_pos_total_value());
        if(model.getKpi_website_sale_totalDirtyFlag())
            domain.setKpiWebsiteSaleTotal(model.getKpi_website_sale_total());
        if(model.getKpi_all_sale_totalDirtyFlag())
            domain.setKpiAllSaleTotal(model.getKpi_all_sale_total());
        if(model.getNext_run_dateDirtyFlag())
            domain.setNextRunDate(model.getNext_run_date());
        if(model.getKpi_res_users_connectedDirtyFlag())
            domain.setKpiResUsersConnected(model.getKpi_res_users_connected());
        if(model.getKpi_res_users_connected_valueDirtyFlag())
            domain.setKpiResUsersConnectedValue(model.getKpi_res_users_connected_value());
        if(model.getPeriodicityDirtyFlag())
            domain.setPeriodicity(model.getPeriodicity());
        if(model.getIdDirtyFlag())
            domain.setId(model.getId());
        if(model.getKpi_hr_recruitment_new_colleagues_valueDirtyFlag())
            domain.setKpiHrRecruitmentNewColleaguesValue(model.getKpi_hr_recruitment_new_colleagues_value());
        if(model.getKpi_account_total_revenueDirtyFlag())
            domain.setKpiAccountTotalRevenue(model.getKpi_account_total_revenue());
        if(model.getKpi_crm_lead_createdDirtyFlag())
            domain.setKpiCrmLeadCreated(model.getKpi_crm_lead_created());
        if(model.getKpi_project_task_openedDirtyFlag())
            domain.setKpiProjectTaskOpened(model.getKpi_project_task_opened());
        if(model.getKpi_all_sale_total_valueDirtyFlag())
            domain.setKpiAllSaleTotalValue(model.getKpi_all_sale_total_value());
        if(model.getIs_subscribedDirtyFlag())
            domain.setIsSubscribed(model.getIs_subscribed());
        if(model.getDisplay_nameDirtyFlag())
            domain.setDisplayName(model.getDisplay_name());
        if(model.getKpi_project_task_opened_valueDirtyFlag())
            domain.setKpiProjectTaskOpenedValue(model.getKpi_project_task_opened_value());
        if(model.getKpi_account_total_revenue_valueDirtyFlag())
            domain.setKpiAccountTotalRevenueValue(model.getKpi_account_total_revenue_value());
        if(model.getKpi_mail_message_total_valueDirtyFlag())
            domain.setKpiMailMessageTotalValue(model.getKpi_mail_message_total_value());
        if(model.getKpi_mail_message_totalDirtyFlag())
            domain.setKpiMailMessageTotal(model.getKpi_mail_message_total());
        if(model.getWrite_uid_textDirtyFlag())
            domain.setWriteUidText(model.getWrite_uid_text());
        if(model.getTemplate_id_textDirtyFlag())
            domain.setTemplateIdText(model.getTemplate_id_text());
        if(model.getCompany_id_textDirtyFlag())
            domain.setCompanyIdText(model.getCompany_id_text());
        if(model.getCurrency_idDirtyFlag())
            domain.setCurrencyId(model.getCurrency_id());
        if(model.getCreate_uid_textDirtyFlag())
            domain.setCreateUidText(model.getCreate_uid_text());
        if(model.getCreate_uidDirtyFlag())
            domain.setCreateUid(model.getCreate_uid());
        if(model.getWrite_uidDirtyFlag())
            domain.setWriteUid(model.getWrite_uid());
        if(model.getCompany_idDirtyFlag())
            domain.setCompanyId(model.getCompany_id());
        if(model.getTemplate_idDirtyFlag())
            domain.setTemplateId(model.getTemplate_id());
        return domain ;
    }

}

    



