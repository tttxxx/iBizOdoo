package cn.ibizlab.odoo.core.odoo_base.valuerule.anno.res_company;

import cn.ibizlab.odoo.core.odoo_base.valuerule.validator.res_company.Res_companyAccount_bank_reconciliation_startDefaultValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

/**
 * 值规则注解
 * 实体：Res_company
 * 属性：Account_bank_reconciliation_start
 * 值规则：Default
 */
@Target({ElementType.METHOD, ElementType.FIELD, ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Constraint(validatedBy = {Res_companyAccount_bank_reconciliation_startDefaultValidator.class})
public @interface Res_companyAccount_bank_reconciliation_startDefault {
    //是否默认检查
    boolean flag() default true;

    //值规则名称
    String info() default "默认规则";

    //值规则信息，如果为空，使用值规则名称。
    String message() default "默认规则";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
