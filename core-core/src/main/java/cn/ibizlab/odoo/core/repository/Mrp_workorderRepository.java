package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Mrp_workorder;
import cn.ibizlab.odoo.core.odoo_mrp.filter.Mrp_workorderSearchContext;

/**
 * 实体 [工单] 存储对象
 */
public interface Mrp_workorderRepository extends Repository<Mrp_workorder> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Mrp_workorder> searchDefault(Mrp_workorderSearchContext context);

    Mrp_workorder convert2PO(cn.ibizlab.odoo.core.odoo_mrp.domain.Mrp_workorder domain , Mrp_workorder po) ;

    cn.ibizlab.odoo.core.odoo_mrp.domain.Mrp_workorder convert2Domain( Mrp_workorder po ,cn.ibizlab.odoo.core.odoo_mrp.domain.Mrp_workorder domain) ;

}
