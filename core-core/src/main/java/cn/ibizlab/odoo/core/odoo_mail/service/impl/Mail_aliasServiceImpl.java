package cn.ibizlab.odoo.core.odoo_mail.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_mail.domain.Mail_alias;
import cn.ibizlab.odoo.core.odoo_mail.filter.Mail_aliasSearchContext;
import cn.ibizlab.odoo.core.odoo_mail.service.IMail_aliasService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;



import cn.ibizlab.odoo.core.odoo_mail.client.mail_aliasOdooClient;
import cn.ibizlab.odoo.core.odoo_mail.clientmodel.mail_aliasClientModel;

/**
 * 实体[EMail别名] 服务对象接口实现
 */
@Slf4j
@Service
public class Mail_aliasServiceImpl implements IMail_aliasService {

    @Autowired
    mail_aliasOdooClient mail_aliasOdooClient;


    @Override
    public boolean create(Mail_alias et) {
        mail_aliasClientModel clientModel = convert2Model(et,null);
		mail_aliasOdooClient.create(clientModel);
        Mail_alias rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Mail_alias> list){
    }

    @Override
    public boolean remove(Integer id) {
        mail_aliasClientModel clientModel = new mail_aliasClientModel();
        clientModel.setId(id);
		mail_aliasOdooClient.remove(clientModel);
        return true; 
    }

    public void removeBatch(Collection<Integer> idList){
    }

    @Override
    public boolean update(Mail_alias et) {
        mail_aliasClientModel clientModel = convert2Model(et,null);
		mail_aliasOdooClient.update(clientModel);
        Mail_alias rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }
    
    public void updateBatch(List<Mail_alias> list){
    }

    @Override
    public Mail_alias get(Integer id) {
        mail_aliasClientModel clientModel = new mail_aliasClientModel();
        clientModel.setId(id);
		mail_aliasOdooClient.get(clientModel);
        Mail_alias et = convert2Domain(clientModel,null);
        if(et==null){
            et=new Mail_alias();
            et.setId(id);
        }
        else{
        }

        return  et;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Mail_alias> searchDefault(Mail_aliasSearchContext context) {
        List<Mail_alias> list = new ArrayList<Mail_alias>();
        Page<mail_aliasClientModel> clientModelList = mail_aliasOdooClient.search(context);
        for(mail_aliasClientModel clientModel : clientModelList){
            list.add( this.convert2Domain(clientModel,null));
        }
        return new PageImpl<Mail_alias>(list, context.getPageable(), clientModelList.getTotalElements());
    }

    public mail_aliasClientModel convert2Model(Mail_alias domain , mail_aliasClientModel model) {
        if(domain == null)
            return model ;
        if(model==null){
            model = new mail_aliasClientModel();

        }
        if((Boolean) domain.getExtensionparams().get("dirtyflagenable")){
            if((Boolean) domain.getExtensionparams().get("iddirtyflag"))
                model.setId(domain.getId());
            if((Boolean) domain.getExtensionparams().get("create_datedirtyflag"))
                model.setCreate_date(domain.getCreateDate());
            if((Boolean) domain.getExtensionparams().get("alias_parent_thread_iddirtyflag"))
                model.setAlias_parent_thread_id(domain.getAliasParentThreadId());
            if((Boolean) domain.getExtensionparams().get("alias_domaindirtyflag"))
                model.setAlias_domain(domain.getAliasDomain());
            if((Boolean) domain.getExtensionparams().get("alias_model_iddirtyflag"))
                model.setAlias_model_id(domain.getAliasModelId());
            if((Boolean) domain.getExtensionparams().get("alias_defaultsdirtyflag"))
                model.setAlias_defaults(domain.getAliasDefaults());
            if((Boolean) domain.getExtensionparams().get("display_namedirtyflag"))
                model.setDisplay_name(domain.getDisplayName());
            if((Boolean) domain.getExtensionparams().get("alias_namedirtyflag"))
                model.setAlias_name(domain.getAliasName());
            if((Boolean) domain.getExtensionparams().get("alias_force_thread_iddirtyflag"))
                model.setAlias_force_thread_id(domain.getAliasForceThreadId());
            if((Boolean) domain.getExtensionparams().get("__last_updatedirtyflag"))
                model.set__last_update(domain.getLastUpdate());
            if((Boolean) domain.getExtensionparams().get("alias_parent_model_iddirtyflag"))
                model.setAlias_parent_model_id(domain.getAliasParentModelId());
            if((Boolean) domain.getExtensionparams().get("alias_contactdirtyflag"))
                model.setAlias_contact(domain.getAliasContact());
            if((Boolean) domain.getExtensionparams().get("write_datedirtyflag"))
                model.setWrite_date(domain.getWriteDate());
            if((Boolean) domain.getExtensionparams().get("create_uid_textdirtyflag"))
                model.setCreate_uid_text(domain.getCreateUidText());
            if((Boolean) domain.getExtensionparams().get("write_uid_textdirtyflag"))
                model.setWrite_uid_text(domain.getWriteUidText());
            if((Boolean) domain.getExtensionparams().get("alias_user_id_textdirtyflag"))
                model.setAlias_user_id_text(domain.getAliasUserIdText());
            if((Boolean) domain.getExtensionparams().get("alias_user_iddirtyflag"))
                model.setAlias_user_id(domain.getAliasUserId());
            if((Boolean) domain.getExtensionparams().get("write_uiddirtyflag"))
                model.setWrite_uid(domain.getWriteUid());
            if((Boolean) domain.getExtensionparams().get("create_uiddirtyflag"))
                model.setCreate_uid(domain.getCreateUid());
            domain.getFocusNull().clear();
        }
        return model ;
    }

    public Mail_alias convert2Domain( mail_aliasClientModel model ,Mail_alias domain) {
        if(model == null)
            return domain ;
        if(domain == null){
            domain = new Mail_alias();
        }

        if(model.getIdDirtyFlag())
            domain.setId(model.getId());
        if(model.getCreate_dateDirtyFlag())
            domain.setCreateDate(model.getCreate_date());
        if(model.getAlias_parent_thread_idDirtyFlag())
            domain.setAliasParentThreadId(model.getAlias_parent_thread_id());
        if(model.getAlias_domainDirtyFlag())
            domain.setAliasDomain(model.getAlias_domain());
        if(model.getAlias_model_idDirtyFlag())
            domain.setAliasModelId(model.getAlias_model_id());
        if(model.getAlias_defaultsDirtyFlag())
            domain.setAliasDefaults(model.getAlias_defaults());
        if(model.getDisplay_nameDirtyFlag())
            domain.setDisplayName(model.getDisplay_name());
        if(model.getAlias_nameDirtyFlag())
            domain.setAliasName(model.getAlias_name());
        if(model.getAlias_force_thread_idDirtyFlag())
            domain.setAliasForceThreadId(model.getAlias_force_thread_id());
        if(model.get__last_updateDirtyFlag())
            domain.setLastUpdate(model.get__last_update());
        if(model.getAlias_parent_model_idDirtyFlag())
            domain.setAliasParentModelId(model.getAlias_parent_model_id());
        if(model.getAlias_contactDirtyFlag())
            domain.setAliasContact(model.getAlias_contact());
        if(model.getWrite_dateDirtyFlag())
            domain.setWriteDate(model.getWrite_date());
        if(model.getCreate_uid_textDirtyFlag())
            domain.setCreateUidText(model.getCreate_uid_text());
        if(model.getWrite_uid_textDirtyFlag())
            domain.setWriteUidText(model.getWrite_uid_text());
        if(model.getAlias_user_id_textDirtyFlag())
            domain.setAliasUserIdText(model.getAlias_user_id_text());
        if(model.getAlias_user_idDirtyFlag())
            domain.setAliasUserId(model.getAlias_user_id());
        if(model.getWrite_uidDirtyFlag())
            domain.setWriteUid(model.getWrite_uid());
        if(model.getCreate_uidDirtyFlag())
            domain.setCreateUid(model.getCreate_uid());
        return domain ;
    }

}

    



