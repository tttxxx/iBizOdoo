package cn.ibizlab.odoo.core.repository.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import cn.ibizlab.odoo.core.repository.client.po.product_image;

/**
 * 实体[product_image] 服务对象接口
 */
public interface product_imageRepository{


    public product_image createPO() ;
        public void create(product_image product_image);

        public void remove(String id);

        public void update(product_image product_image);

        public List<product_image> search();

        public void updateBatch(product_image product_image);

        public void removeBatch(String id);

        public void get(String id);

        public void createBatch(product_image product_image);


}
