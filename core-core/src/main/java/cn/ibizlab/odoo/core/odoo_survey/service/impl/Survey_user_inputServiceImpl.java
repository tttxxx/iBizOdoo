package cn.ibizlab.odoo.core.odoo_survey.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_survey.domain.Survey_user_input;
import cn.ibizlab.odoo.core.odoo_survey.filter.Survey_user_inputSearchContext;
import cn.ibizlab.odoo.core.odoo_survey.service.ISurvey_user_inputService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;



import cn.ibizlab.odoo.core.odoo_survey.client.survey_user_inputOdooClient;
import cn.ibizlab.odoo.core.odoo_survey.clientmodel.survey_user_inputClientModel;

/**
 * 实体[调查用户输入] 服务对象接口实现
 */
@Slf4j
@Service
public class Survey_user_inputServiceImpl implements ISurvey_user_inputService {

    @Autowired
    survey_user_inputOdooClient survey_user_inputOdooClient;


    @Override
    public boolean update(Survey_user_input et) {
        survey_user_inputClientModel clientModel = convert2Model(et,null);
		survey_user_inputOdooClient.update(clientModel);
        Survey_user_input rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }
    
    public void updateBatch(List<Survey_user_input> list){
    }

    @Override
    public boolean create(Survey_user_input et) {
        survey_user_inputClientModel clientModel = convert2Model(et,null);
		survey_user_inputOdooClient.create(clientModel);
        Survey_user_input rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Survey_user_input> list){
    }

    @Override
    public boolean remove(Integer id) {
        survey_user_inputClientModel clientModel = new survey_user_inputClientModel();
        clientModel.setId(id);
		survey_user_inputOdooClient.remove(clientModel);
        return true; 
    }

    public void removeBatch(Collection<Integer> idList){
    }

    @Override
    public Survey_user_input get(Integer id) {
        survey_user_inputClientModel clientModel = new survey_user_inputClientModel();
        clientModel.setId(id);
		survey_user_inputOdooClient.get(clientModel);
        Survey_user_input et = convert2Domain(clientModel,null);
        if(et==null){
            et=new Survey_user_input();
            et.setId(id);
        }
        else{
        }

        return  et;
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Survey_user_input> searchDefault(Survey_user_inputSearchContext context) {
        List<Survey_user_input> list = new ArrayList<Survey_user_input>();
        Page<survey_user_inputClientModel> clientModelList = survey_user_inputOdooClient.search(context);
        for(survey_user_inputClientModel clientModel : clientModelList){
            list.add( this.convert2Domain(clientModel,null));
        }
        return new PageImpl<Survey_user_input>(list, context.getPageable(), clientModelList.getTotalElements());
    }

    public survey_user_inputClientModel convert2Model(Survey_user_input domain , survey_user_inputClientModel model) {
        if(domain == null)
            return model ;
        if(model==null){
            model = new survey_user_inputClientModel();

        }
        if((Boolean) domain.getExtensionparams().get("dirtyflagenable")){
            if((Boolean) domain.getExtensionparams().get("user_input_line_idsdirtyflag"))
                model.setUser_input_line_ids(domain.getUserInputLineIds());
            if((Boolean) domain.getExtensionparams().get("statedirtyflag"))
                model.setState(domain.getState());
            if((Boolean) domain.getExtensionparams().get("iddirtyflag"))
                model.setId(domain.getId());
            if((Boolean) domain.getExtensionparams().get("quizz_scoredirtyflag"))
                model.setQuizz_score(domain.getQuizzScore());
            if((Boolean) domain.getExtensionparams().get("tokendirtyflag"))
                model.setToken(domain.getToken());
            if((Boolean) domain.getExtensionparams().get("write_datedirtyflag"))
                model.setWrite_date(domain.getWriteDate());
            if((Boolean) domain.getExtensionparams().get("create_datedirtyflag"))
                model.setCreate_date(domain.getCreateDate());
            if((Boolean) domain.getExtensionparams().get("display_namedirtyflag"))
                model.setDisplay_name(domain.getDisplayName());
            if((Boolean) domain.getExtensionparams().get("emaildirtyflag"))
                model.setEmail(domain.getEmail());
            if((Boolean) domain.getExtensionparams().get("__last_updatedirtyflag"))
                model.set__last_update(domain.getLastUpdate());
            if((Boolean) domain.getExtensionparams().get("test_entrydirtyflag"))
                model.setTest_entry(domain.getTestEntry());
            if((Boolean) domain.getExtensionparams().get("deadlinedirtyflag"))
                model.setDeadline(domain.getDeadline());
            if((Boolean) domain.getExtensionparams().get("typedirtyflag"))
                model.setType(domain.getType());
            if((Boolean) domain.getExtensionparams().get("date_createdirtyflag"))
                model.setDate_create(domain.getDateCreate());
            if((Boolean) domain.getExtensionparams().get("write_uid_textdirtyflag"))
                model.setWrite_uid_text(domain.getWriteUidText());
            if((Boolean) domain.getExtensionparams().get("create_uid_textdirtyflag"))
                model.setCreate_uid_text(domain.getCreateUidText());
            if((Boolean) domain.getExtensionparams().get("print_urldirtyflag"))
                model.setPrint_url(domain.getPrintUrl());
            if((Boolean) domain.getExtensionparams().get("result_urldirtyflag"))
                model.setResult_url(domain.getResultUrl());
            if((Boolean) domain.getExtensionparams().get("partner_id_textdirtyflag"))
                model.setPartner_id_text(domain.getPartnerIdText());
            if((Boolean) domain.getExtensionparams().get("partner_iddirtyflag"))
                model.setPartner_id(domain.getPartnerId());
            if((Boolean) domain.getExtensionparams().get("survey_iddirtyflag"))
                model.setSurvey_id(domain.getSurveyId());
            if((Boolean) domain.getExtensionparams().get("last_displayed_page_iddirtyflag"))
                model.setLast_displayed_page_id(domain.getLastDisplayedPageId());
            if((Boolean) domain.getExtensionparams().get("write_uiddirtyflag"))
                model.setWrite_uid(domain.getWriteUid());
            if((Boolean) domain.getExtensionparams().get("create_uiddirtyflag"))
                model.setCreate_uid(domain.getCreateUid());
            domain.getFocusNull().clear();
        }
        return model ;
    }

    public Survey_user_input convert2Domain( survey_user_inputClientModel model ,Survey_user_input domain) {
        if(model == null)
            return domain ;
        if(domain == null){
            domain = new Survey_user_input();
        }

        if(model.getUser_input_line_idsDirtyFlag())
            domain.setUserInputLineIds(model.getUser_input_line_ids());
        if(model.getStateDirtyFlag())
            domain.setState(model.getState());
        if(model.getIdDirtyFlag())
            domain.setId(model.getId());
        if(model.getQuizz_scoreDirtyFlag())
            domain.setQuizzScore(model.getQuizz_score());
        if(model.getTokenDirtyFlag())
            domain.setToken(model.getToken());
        if(model.getWrite_dateDirtyFlag())
            domain.setWriteDate(model.getWrite_date());
        if(model.getCreate_dateDirtyFlag())
            domain.setCreateDate(model.getCreate_date());
        if(model.getDisplay_nameDirtyFlag())
            domain.setDisplayName(model.getDisplay_name());
        if(model.getEmailDirtyFlag())
            domain.setEmail(model.getEmail());
        if(model.get__last_updateDirtyFlag())
            domain.setLastUpdate(model.get__last_update());
        if(model.getTest_entryDirtyFlag())
            domain.setTestEntry(model.getTest_entry());
        if(model.getDeadlineDirtyFlag())
            domain.setDeadline(model.getDeadline());
        if(model.getTypeDirtyFlag())
            domain.setType(model.getType());
        if(model.getDate_createDirtyFlag())
            domain.setDateCreate(model.getDate_create());
        if(model.getWrite_uid_textDirtyFlag())
            domain.setWriteUidText(model.getWrite_uid_text());
        if(model.getCreate_uid_textDirtyFlag())
            domain.setCreateUidText(model.getCreate_uid_text());
        if(model.getPrint_urlDirtyFlag())
            domain.setPrintUrl(model.getPrint_url());
        if(model.getResult_urlDirtyFlag())
            domain.setResultUrl(model.getResult_url());
        if(model.getPartner_id_textDirtyFlag())
            domain.setPartnerIdText(model.getPartner_id_text());
        if(model.getPartner_idDirtyFlag())
            domain.setPartnerId(model.getPartner_id());
        if(model.getSurvey_idDirtyFlag())
            domain.setSurveyId(model.getSurvey_id());
        if(model.getLast_displayed_page_idDirtyFlag())
            domain.setLastDisplayedPageId(model.getLast_displayed_page_id());
        if(model.getWrite_uidDirtyFlag())
            domain.setWriteUid(model.getWrite_uid());
        if(model.getCreate_uidDirtyFlag())
            domain.setCreateUid(model.getCreate_uid());
        return domain ;
    }

}

    



