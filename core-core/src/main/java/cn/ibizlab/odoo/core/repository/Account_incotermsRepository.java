package cn.ibizlab.odoo.core.repository ;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.ibizlab.odoo.core.repository.po.Account_incoterms;
import cn.ibizlab.odoo.core.odoo_account.filter.Account_incotermsSearchContext;

/**
 * 实体 [贸易条款] 存储对象
 */
public interface Account_incotermsRepository extends Repository<Account_incoterms> {

    /**
	 * 查询集合 [默认查询]
	 * @param context
	 * @return
	 */
    Page<Account_incoterms> searchDefault(Account_incotermsSearchContext context);

    Account_incoterms convert2PO(cn.ibizlab.odoo.core.odoo_account.domain.Account_incoterms domain , Account_incoterms po) ;

    cn.ibizlab.odoo.core.odoo_account.domain.Account_incoterms convert2Domain( Account_incoterms po ,cn.ibizlab.odoo.core.odoo_account.domain.Account_incoterms domain) ;

}
