package cn.ibizlab.odoo.core.repository.po ;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.math.BigInteger;

import cn.ibizlab.odoo.core.odoo_mail.filter.Mail_templateSearchContext;

/**
 * 实体 [EMail模板] 存储模型
 */
public interface Mail_template{

    /**
     * ID
     */
    Integer getId();

    void setId(Integer id);

    /**
     * 获取 [ID]脏标记
     */
    boolean getIdDirtyFlag();

    /**
     * 默认收件人
     */
    String getUse_default_to();

    void setUse_default_to(String use_default_to);

    /**
     * 获取 [默认收件人]脏标记
     */
    boolean getUse_default_toDirtyFlag();

    /**
     * 主题
     */
    String getSubject();

    void setSubject(String subject);

    /**
     * 获取 [主题]脏标记
     */
    boolean getSubjectDirtyFlag();

    /**
     * 附件
     */
    String getAttachment_ids();

    void setAttachment_ids(String attachment_ids);

    /**
     * 获取 [附件]脏标记
     */
    boolean getAttachment_idsDirtyFlag();

    /**
     * 最后更新时间
     */
    Timestamp getWrite_date();

    void setWrite_date(Timestamp write_date);

    /**
     * 获取 [最后更新时间]脏标记
     */
    boolean getWrite_dateDirtyFlag();

    /**
     * 回复 至
     */
    String getReply_to();

    void setReply_to(String reply_to);

    /**
     * 获取 [回复 至]脏标记
     */
    boolean getReply_toDirtyFlag();

    /**
     * 子模型
     */
    Integer getSub_object();

    void setSub_object(Integer sub_object);

    /**
     * 获取 [子模型]脏标记
     */
    boolean getSub_objectDirtyFlag();

    /**
     * 计划日期
     */
    String getScheduled_date();

    void setScheduled_date(String scheduled_date);

    /**
     * 获取 [计划日期]脏标记
     */
    boolean getScheduled_dateDirtyFlag();

    /**
     * 应用于
     */
    Integer getModel_id();

    void setModel_id(Integer model_id);

    /**
     * 获取 [应用于]脏标记
     */
    boolean getModel_idDirtyFlag();

    /**
     * 邮件发送服务器
     */
    Integer getMail_server_id();

    void setMail_server_id(Integer mail_server_id);

    /**
     * 获取 [邮件发送服务器]脏标记
     */
    boolean getMail_server_idDirtyFlag();

    /**
     * 可选的打印和附加报表
     */
    Integer getReport_template();

    void setReport_template(Integer report_template);

    /**
     * 获取 [可选的打印和附加报表]脏标记
     */
    boolean getReport_templateDirtyFlag();

    /**
     * 正文
     */
    String getBody_html();

    void setBody_html(String body_html);

    /**
     * 获取 [正文]脏标记
     */
    boolean getBody_htmlDirtyFlag();

    /**
     * 边栏操作
     */
    Integer getRef_ir_act_window();

    void setRef_ir_act_window(Integer ref_ir_act_window);

    /**
     * 获取 [边栏操作]脏标记
     */
    boolean getRef_ir_act_windowDirtyFlag();

    /**
     * 名称
     */
    String getName();

    void setName(String name);

    /**
     * 获取 [名称]脏标记
     */
    boolean getNameDirtyFlag();

    /**
     * 至（合作伙伴）
     */
    String getPartner_to();

    void setPartner_to(String partner_to);

    /**
     * 获取 [至（合作伙伴）]脏标记
     */
    boolean getPartner_toDirtyFlag();

    /**
     * 相关的文档模型
     */
    String getModel();

    void setModel(String model);

    /**
     * 获取 [相关的文档模型]脏标记
     */
    boolean getModelDirtyFlag();

    /**
     * 从
     */
    String getEmail_from();

    void setEmail_from(String email_from);

    /**
     * 获取 [从]脏标记
     */
    boolean getEmail_fromDirtyFlag();

    /**
     * 自动删除
     */
    String getAuto_delete();

    void setAuto_delete(String auto_delete);

    /**
     * 获取 [自动删除]脏标记
     */
    boolean getAuto_deleteDirtyFlag();

    /**
     * 抄送
     */
    String getEmail_cc();

    void setEmail_cc(String email_cc);

    /**
     * 获取 [抄送]脏标记
     */
    boolean getEmail_ccDirtyFlag();

    /**
     * 报告文件名
     */
    String getReport_name();

    void setReport_name(String report_name);

    /**
     * 获取 [报告文件名]脏标记
     */
    boolean getReport_nameDirtyFlag();

    /**
     * 创建时间
     */
    Timestamp getCreate_date();

    void setCreate_date(Timestamp create_date);

    /**
     * 获取 [创建时间]脏标记
     */
    boolean getCreate_dateDirtyFlag();

    /**
     * 最后修改日
     */
    Timestamp get__last_update();

    void set__last_update(Timestamp __last_update);

    /**
     * 获取 [最后修改日]脏标记
     */
    boolean get__last_updateDirtyFlag();

    /**
     * 字段
     */
    Integer getModel_object_field();

    void setModel_object_field(Integer model_object_field);

    /**
     * 获取 [字段]脏标记
     */
    boolean getModel_object_fieldDirtyFlag();

    /**
     * 至（EMail）
     */
    String getEmail_to();

    void setEmail_to(String email_to);

    /**
     * 获取 [至（EMail）]脏标记
     */
    boolean getEmail_toDirtyFlag();

    /**
     * 默认值
     */
    String getNull_value();

    void setNull_value(String null_value);

    /**
     * 获取 [默认值]脏标记
     */
    boolean getNull_valueDirtyFlag();

    /**
     * 占位符表达式
     */
    String getCopyvalue();

    void setCopyvalue(String copyvalue);

    /**
     * 获取 [占位符表达式]脏标记
     */
    boolean getCopyvalueDirtyFlag();

    /**
     * 添加签名
     */
    String getUser_signature();

    void setUser_signature(String user_signature);

    /**
     * 获取 [添加签名]脏标记
     */
    boolean getUser_signatureDirtyFlag();

    /**
     * 子字段
     */
    Integer getSub_model_object_field();

    void setSub_model_object_field(Integer sub_model_object_field);

    /**
     * 获取 [子字段]脏标记
     */
    boolean getSub_model_object_fieldDirtyFlag();

    /**
     * 显示名称
     */
    String getDisplay_name();

    void setDisplay_name(String display_name);

    /**
     * 获取 [显示名称]脏标记
     */
    boolean getDisplay_nameDirtyFlag();

    /**
     * 语言
     */
    String getLang();

    void setLang(String lang);

    /**
     * 获取 [语言]脏标记
     */
    boolean getLangDirtyFlag();

    /**
     * 创建人
     */
    String getCreate_uid_text();

    void setCreate_uid_text(String create_uid_text);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uid_textDirtyFlag();

    /**
     * 最后更新者
     */
    String getWrite_uid_text();

    void setWrite_uid_text(String write_uid_text);

    /**
     * 获取 [最后更新者]脏标记
     */
    boolean getWrite_uid_textDirtyFlag();

    /**
     * 创建人
     */
    Integer getCreate_uid();

    void setCreate_uid(Integer create_uid);

    /**
     * 获取 [创建人]脏标记
     */
    boolean getCreate_uidDirtyFlag();

    /**
     * 最后更新者
     */
    Integer getWrite_uid();

    void setWrite_uid(Integer write_uid);

    /**
     * 获取 [最后更新者]脏标记
     */
    boolean getWrite_uidDirtyFlag();

}
