package cn.ibizlab.odoo.core.odoo_event.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.odoo.core.odoo_event.domain.Event_event;
import cn.ibizlab.odoo.core.odoo_event.filter.Event_eventSearchContext;
import cn.ibizlab.odoo.core.odoo_event.service.IEvent_eventService;

import cn.ibizlab.odoo.util.helper.CachedBeanCopier;



import cn.ibizlab.odoo.core.odoo_event.client.event_eventOdooClient;
import cn.ibizlab.odoo.core.odoo_event.clientmodel.event_eventClientModel;

/**
 * 实体[活动] 服务对象接口实现
 */
@Slf4j
@Service
public class Event_eventServiceImpl implements IEvent_eventService {

    @Autowired
    event_eventOdooClient event_eventOdooClient;


    @Override
    public Event_event get(Integer id) {
        event_eventClientModel clientModel = new event_eventClientModel();
        clientModel.setId(id);
		event_eventOdooClient.get(clientModel);
        Event_event et = convert2Domain(clientModel,null);
        if(et==null){
            et=new Event_event();
            et.setId(id);
        }
        else{
        }

        return  et;
    }

    @Override
    public boolean create(Event_event et) {
        event_eventClientModel clientModel = convert2Model(et,null);
		event_eventOdooClient.create(clientModel);
        Event_event rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;
    }

    public void createBatch(List<Event_event> list){
    }

    @Override
    public boolean update(Event_event et) {
        event_eventClientModel clientModel = convert2Model(et,null);
		event_eventOdooClient.update(clientModel);
        Event_event rt = convert2Domain(clientModel,null);
        if(rt==null)
            return false;
        CachedBeanCopier.copy(rt,et);
        return true;

    }
    
    public void updateBatch(List<Event_event> list){
    }

    @Override
    public boolean remove(Integer id) {
        event_eventClientModel clientModel = new event_eventClientModel();
        clientModel.setId(id);
		event_eventOdooClient.remove(clientModel);
        return true; 
    }

    public void removeBatch(Collection<Integer> idList){
    }





    /**
     * 查询集合 默认查询
     */
    @Override
    public Page<Event_event> searchDefault(Event_eventSearchContext context) {
        List<Event_event> list = new ArrayList<Event_event>();
        Page<event_eventClientModel> clientModelList = event_eventOdooClient.search(context);
        for(event_eventClientModel clientModel : clientModelList){
            list.add( this.convert2Domain(clientModel,null));
        }
        return new PageImpl<Event_event>(list, context.getPageable(), clientModelList.getTotalElements());
    }

    public event_eventClientModel convert2Model(Event_event domain , event_eventClientModel model) {
        if(domain == null)
            return model ;
        if(model==null){
            model = new event_eventClientModel();

        }
        if((Boolean) domain.getExtensionparams().get("dirtyflagenable")){
            if((Boolean) domain.getExtensionparams().get("seats_useddirtyflag"))
                model.setSeats_used(domain.getSeatsUsed());
            if((Boolean) domain.getExtensionparams().get("is_seo_optimizeddirtyflag"))
                model.setIs_seo_optimized(domain.getIsSeoOptimized());
            if((Boolean) domain.getExtensionparams().get("iddirtyflag"))
                model.setId(domain.getId());
            if((Boolean) domain.getExtensionparams().get("badge_frontdirtyflag"))
                model.setBadge_front(domain.getBadgeFront());
            if((Boolean) domain.getExtensionparams().get("statedirtyflag"))
                model.setState(domain.getState());
            if((Boolean) domain.getExtensionparams().get("event_mail_idsdirtyflag"))
                model.setEvent_mail_ids(domain.getEventMailIds());
            if((Boolean) domain.getExtensionparams().get("create_datedirtyflag"))
                model.setCreate_date(domain.getCreateDate());
            if((Boolean) domain.getExtensionparams().get("seats_expecteddirtyflag"))
                model.setSeats_expected(domain.getSeatsExpected());
            if((Boolean) domain.getExtensionparams().get("message_unreaddirtyflag"))
                model.setMessage_unread(domain.getMessageUnread());
            if((Boolean) domain.getExtensionparams().get("message_needactiondirtyflag"))
                model.setMessage_needaction(domain.getMessageNeedaction());
            if((Boolean) domain.getExtensionparams().get("seats_availabilitydirtyflag"))
                model.setSeats_availability(domain.getSeatsAvailability());
            if((Boolean) domain.getExtensionparams().get("event_ticket_idsdirtyflag"))
                model.setEvent_ticket_ids(domain.getEventTicketIds());
            if((Boolean) domain.getExtensionparams().get("seats_unconfirmeddirtyflag"))
                model.setSeats_unconfirmed(domain.getSeatsUnconfirmed());
            if((Boolean) domain.getExtensionparams().get("is_participatingdirtyflag"))
                model.setIs_participating(domain.getIsParticipating());
            if((Boolean) domain.getExtensionparams().get("message_channel_idsdirtyflag"))
                model.setMessage_channel_ids(domain.getMessageChannelIds());
            if((Boolean) domain.getExtensionparams().get("twitter_hashtagdirtyflag"))
                model.setTwitter_hashtag(domain.getTwitterHashtag());
            if((Boolean) domain.getExtensionparams().get("seats_maxdirtyflag"))
                model.setSeats_max(domain.getSeatsMax());
            if((Boolean) domain.getExtensionparams().get("date_begindirtyflag"))
                model.setDate_begin(domain.getDateBegin());
            if((Boolean) domain.getExtensionparams().get("message_main_attachment_iddirtyflag"))
                model.setMessage_main_attachment_id(domain.getMessageMainAttachmentId());
            if((Boolean) domain.getExtensionparams().get("website_meta_og_imgdirtyflag"))
                model.setWebsite_meta_og_img(domain.getWebsiteMetaOgImg());
            if((Boolean) domain.getExtensionparams().get("website_publisheddirtyflag"))
                model.setWebsite_published(domain.getWebsitePublished());
            if((Boolean) domain.getExtensionparams().get("message_idsdirtyflag"))
                model.setMessage_ids(domain.getMessageIds());
            if((Boolean) domain.getExtensionparams().get("website_meta_titledirtyflag"))
                model.setWebsite_meta_title(domain.getWebsiteMetaTitle());
            if((Boolean) domain.getExtensionparams().get("message_has_error_counterdirtyflag"))
                model.setMessage_has_error_counter(domain.getMessageHasErrorCounter());
            if((Boolean) domain.getExtensionparams().get("registration_idsdirtyflag"))
                model.setRegistration_ids(domain.getRegistrationIds());
            if((Boolean) domain.getExtensionparams().get("descriptiondirtyflag"))
                model.setDescription(domain.getDescription());
            if((Boolean) domain.getExtensionparams().get("badge_innerrightdirtyflag"))
                model.setBadge_innerright(domain.getBadgeInnerright());
            if((Boolean) domain.getExtensionparams().get("message_is_followerdirtyflag"))
                model.setMessage_is_follower(domain.getMessageIsFollower());
            if((Boolean) domain.getExtensionparams().get("website_meta_descriptiondirtyflag"))
                model.setWebsite_meta_description(domain.getWebsiteMetaDescription());
            if((Boolean) domain.getExtensionparams().get("seats_mindirtyflag"))
                model.setSeats_min(domain.getSeatsMin());
            if((Boolean) domain.getExtensionparams().get("message_follower_idsdirtyflag"))
                model.setMessage_follower_ids(domain.getMessageFollowerIds());
            if((Boolean) domain.getExtensionparams().get("badge_backdirtyflag"))
                model.setBadge_back(domain.getBadgeBack());
            if((Boolean) domain.getExtensionparams().get("website_meta_keywordsdirtyflag"))
                model.setWebsite_meta_keywords(domain.getWebsiteMetaKeywords());
            if((Boolean) domain.getExtensionparams().get("message_unread_counterdirtyflag"))
                model.setMessage_unread_counter(domain.getMessageUnreadCounter());
            if((Boolean) domain.getExtensionparams().get("auto_confirmdirtyflag"))
                model.setAuto_confirm(domain.getAutoConfirm());
            if((Boolean) domain.getExtensionparams().get("date_enddirtyflag"))
                model.setDate_end(domain.getDateEnd());
            if((Boolean) domain.getExtensionparams().get("namedirtyflag"))
                model.setName(domain.getName());
            if((Boolean) domain.getExtensionparams().get("badge_innerleftdirtyflag"))
                model.setBadge_innerleft(domain.getBadgeInnerleft());
            if((Boolean) domain.getExtensionparams().get("seats_reserveddirtyflag"))
                model.setSeats_reserved(domain.getSeatsReserved());
            if((Boolean) domain.getExtensionparams().get("website_menudirtyflag"))
                model.setWebsite_menu(domain.getWebsiteMenu());
            if((Boolean) domain.getExtensionparams().get("message_attachment_countdirtyflag"))
                model.setMessage_attachment_count(domain.getMessageAttachmentCount());
            if((Boolean) domain.getExtensionparams().get("website_iddirtyflag"))
                model.setWebsite_id(domain.getWebsiteId());
            if((Boolean) domain.getExtensionparams().get("activedirtyflag"))
                model.setActive(domain.getActive());
            if((Boolean) domain.getExtensionparams().get("date_end_locateddirtyflag"))
                model.setDate_end_located(domain.getDateEndLocated());
            if((Boolean) domain.getExtensionparams().get("seats_availabledirtyflag"))
                model.setSeats_available(domain.getSeatsAvailable());
            if((Boolean) domain.getExtensionparams().get("is_publisheddirtyflag"))
                model.setIs_published(domain.getIsPublished());
            if((Boolean) domain.getExtensionparams().get("message_partner_idsdirtyflag"))
                model.setMessage_partner_ids(domain.getMessagePartnerIds());
            if((Boolean) domain.getExtensionparams().get("is_onlinedirtyflag"))
                model.setIs_online(domain.getIsOnline());
            if((Boolean) domain.getExtensionparams().get("date_tzdirtyflag"))
                model.setDate_tz(domain.getDateTz());
            if((Boolean) domain.getExtensionparams().get("display_namedirtyflag"))
                model.setDisplay_name(domain.getDisplayName());
            if((Boolean) domain.getExtensionparams().get("message_has_errordirtyflag"))
                model.setMessage_has_error(domain.getMessageHasError());
            if((Boolean) domain.getExtensionparams().get("__last_updatedirtyflag"))
                model.set__last_update(domain.getLastUpdate());
            if((Boolean) domain.getExtensionparams().get("message_needaction_counterdirtyflag"))
                model.setMessage_needaction_counter(domain.getMessageNeedactionCounter());
            if((Boolean) domain.getExtensionparams().get("write_datedirtyflag"))
                model.setWrite_date(domain.getWriteDate());
            if((Boolean) domain.getExtensionparams().get("website_message_idsdirtyflag"))
                model.setWebsite_message_ids(domain.getWebsiteMessageIds());
            if((Boolean) domain.getExtensionparams().get("date_begin_locateddirtyflag"))
                model.setDate_begin_located(domain.getDateBeginLocated());
            if((Boolean) domain.getExtensionparams().get("event_logodirtyflag"))
                model.setEvent_logo(domain.getEventLogo());
            if((Boolean) domain.getExtensionparams().get("website_urldirtyflag"))
                model.setWebsite_url(domain.getWebsiteUrl());
            if((Boolean) domain.getExtensionparams().get("colordirtyflag"))
                model.setColor(domain.getColor());
            if((Boolean) domain.getExtensionparams().get("write_uid_textdirtyflag"))
                model.setWrite_uid_text(domain.getWriteUidText());
            if((Boolean) domain.getExtensionparams().get("event_type_id_textdirtyflag"))
                model.setEvent_type_id_text(domain.getEventTypeIdText());
            if((Boolean) domain.getExtensionparams().get("create_uid_textdirtyflag"))
                model.setCreate_uid_text(domain.getCreateUidText());
            if((Boolean) domain.getExtensionparams().get("address_id_textdirtyflag"))
                model.setAddress_id_text(domain.getAddressIdText());
            if((Boolean) domain.getExtensionparams().get("country_id_textdirtyflag"))
                model.setCountry_id_text(domain.getCountryIdText());
            if((Boolean) domain.getExtensionparams().get("menu_id_textdirtyflag"))
                model.setMenu_id_text(domain.getMenuIdText());
            if((Boolean) domain.getExtensionparams().get("company_id_textdirtyflag"))
                model.setCompany_id_text(domain.getCompanyIdText());
            if((Boolean) domain.getExtensionparams().get("user_id_textdirtyflag"))
                model.setUser_id_text(domain.getUserIdText());
            if((Boolean) domain.getExtensionparams().get("organizer_id_textdirtyflag"))
                model.setOrganizer_id_text(domain.getOrganizerIdText());
            if((Boolean) domain.getExtensionparams().get("user_iddirtyflag"))
                model.setUser_id(domain.getUserId());
            if((Boolean) domain.getExtensionparams().get("country_iddirtyflag"))
                model.setCountry_id(domain.getCountryId());
            if((Boolean) domain.getExtensionparams().get("company_iddirtyflag"))
                model.setCompany_id(domain.getCompanyId());
            if((Boolean) domain.getExtensionparams().get("organizer_iddirtyflag"))
                model.setOrganizer_id(domain.getOrganizerId());
            if((Boolean) domain.getExtensionparams().get("menu_iddirtyflag"))
                model.setMenu_id(domain.getMenuId());
            if((Boolean) domain.getExtensionparams().get("create_uiddirtyflag"))
                model.setCreate_uid(domain.getCreateUid());
            if((Boolean) domain.getExtensionparams().get("address_iddirtyflag"))
                model.setAddress_id(domain.getAddressId());
            if((Boolean) domain.getExtensionparams().get("write_uiddirtyflag"))
                model.setWrite_uid(domain.getWriteUid());
            if((Boolean) domain.getExtensionparams().get("event_type_iddirtyflag"))
                model.setEvent_type_id(domain.getEventTypeId());
            domain.getFocusNull().clear();
        }
        return model ;
    }

    public Event_event convert2Domain( event_eventClientModel model ,Event_event domain) {
        if(model == null)
            return domain ;
        if(domain == null){
            domain = new Event_event();
        }

        if(model.getSeats_usedDirtyFlag())
            domain.setSeatsUsed(model.getSeats_used());
        if(model.getIs_seo_optimizedDirtyFlag())
            domain.setIsSeoOptimized(model.getIs_seo_optimized());
        if(model.getIdDirtyFlag())
            domain.setId(model.getId());
        if(model.getBadge_frontDirtyFlag())
            domain.setBadgeFront(model.getBadge_front());
        if(model.getStateDirtyFlag())
            domain.setState(model.getState());
        if(model.getEvent_mail_idsDirtyFlag())
            domain.setEventMailIds(model.getEvent_mail_ids());
        if(model.getCreate_dateDirtyFlag())
            domain.setCreateDate(model.getCreate_date());
        if(model.getSeats_expectedDirtyFlag())
            domain.setSeatsExpected(model.getSeats_expected());
        if(model.getMessage_unreadDirtyFlag())
            domain.setMessageUnread(model.getMessage_unread());
        if(model.getMessage_needactionDirtyFlag())
            domain.setMessageNeedaction(model.getMessage_needaction());
        if(model.getSeats_availabilityDirtyFlag())
            domain.setSeatsAvailability(model.getSeats_availability());
        if(model.getEvent_ticket_idsDirtyFlag())
            domain.setEventTicketIds(model.getEvent_ticket_ids());
        if(model.getSeats_unconfirmedDirtyFlag())
            domain.setSeatsUnconfirmed(model.getSeats_unconfirmed());
        if(model.getIs_participatingDirtyFlag())
            domain.setIsParticipating(model.getIs_participating());
        if(model.getMessage_channel_idsDirtyFlag())
            domain.setMessageChannelIds(model.getMessage_channel_ids());
        if(model.getTwitter_hashtagDirtyFlag())
            domain.setTwitterHashtag(model.getTwitter_hashtag());
        if(model.getSeats_maxDirtyFlag())
            domain.setSeatsMax(model.getSeats_max());
        if(model.getDate_beginDirtyFlag())
            domain.setDateBegin(model.getDate_begin());
        if(model.getMessage_main_attachment_idDirtyFlag())
            domain.setMessageMainAttachmentId(model.getMessage_main_attachment_id());
        if(model.getWebsite_meta_og_imgDirtyFlag())
            domain.setWebsiteMetaOgImg(model.getWebsite_meta_og_img());
        if(model.getWebsite_publishedDirtyFlag())
            domain.setWebsitePublished(model.getWebsite_published());
        if(model.getMessage_idsDirtyFlag())
            domain.setMessageIds(model.getMessage_ids());
        if(model.getWebsite_meta_titleDirtyFlag())
            domain.setWebsiteMetaTitle(model.getWebsite_meta_title());
        if(model.getMessage_has_error_counterDirtyFlag())
            domain.setMessageHasErrorCounter(model.getMessage_has_error_counter());
        if(model.getRegistration_idsDirtyFlag())
            domain.setRegistrationIds(model.getRegistration_ids());
        if(model.getDescriptionDirtyFlag())
            domain.setDescription(model.getDescription());
        if(model.getBadge_innerrightDirtyFlag())
            domain.setBadgeInnerright(model.getBadge_innerright());
        if(model.getMessage_is_followerDirtyFlag())
            domain.setMessageIsFollower(model.getMessage_is_follower());
        if(model.getWebsite_meta_descriptionDirtyFlag())
            domain.setWebsiteMetaDescription(model.getWebsite_meta_description());
        if(model.getSeats_minDirtyFlag())
            domain.setSeatsMin(model.getSeats_min());
        if(model.getMessage_follower_idsDirtyFlag())
            domain.setMessageFollowerIds(model.getMessage_follower_ids());
        if(model.getBadge_backDirtyFlag())
            domain.setBadgeBack(model.getBadge_back());
        if(model.getWebsite_meta_keywordsDirtyFlag())
            domain.setWebsiteMetaKeywords(model.getWebsite_meta_keywords());
        if(model.getMessage_unread_counterDirtyFlag())
            domain.setMessageUnreadCounter(model.getMessage_unread_counter());
        if(model.getAuto_confirmDirtyFlag())
            domain.setAutoConfirm(model.getAuto_confirm());
        if(model.getDate_endDirtyFlag())
            domain.setDateEnd(model.getDate_end());
        if(model.getNameDirtyFlag())
            domain.setName(model.getName());
        if(model.getBadge_innerleftDirtyFlag())
            domain.setBadgeInnerleft(model.getBadge_innerleft());
        if(model.getSeats_reservedDirtyFlag())
            domain.setSeatsReserved(model.getSeats_reserved());
        if(model.getWebsite_menuDirtyFlag())
            domain.setWebsiteMenu(model.getWebsite_menu());
        if(model.getMessage_attachment_countDirtyFlag())
            domain.setMessageAttachmentCount(model.getMessage_attachment_count());
        if(model.getWebsite_idDirtyFlag())
            domain.setWebsiteId(model.getWebsite_id());
        if(model.getActiveDirtyFlag())
            domain.setActive(model.getActive());
        if(model.getDate_end_locatedDirtyFlag())
            domain.setDateEndLocated(model.getDate_end_located());
        if(model.getSeats_availableDirtyFlag())
            domain.setSeatsAvailable(model.getSeats_available());
        if(model.getIs_publishedDirtyFlag())
            domain.setIsPublished(model.getIs_published());
        if(model.getMessage_partner_idsDirtyFlag())
            domain.setMessagePartnerIds(model.getMessage_partner_ids());
        if(model.getIs_onlineDirtyFlag())
            domain.setIsOnline(model.getIs_online());
        if(model.getDate_tzDirtyFlag())
            domain.setDateTz(model.getDate_tz());
        if(model.getDisplay_nameDirtyFlag())
            domain.setDisplayName(model.getDisplay_name());
        if(model.getMessage_has_errorDirtyFlag())
            domain.setMessageHasError(model.getMessage_has_error());
        if(model.get__last_updateDirtyFlag())
            domain.setLastUpdate(model.get__last_update());
        if(model.getMessage_needaction_counterDirtyFlag())
            domain.setMessageNeedactionCounter(model.getMessage_needaction_counter());
        if(model.getWrite_dateDirtyFlag())
            domain.setWriteDate(model.getWrite_date());
        if(model.getWebsite_message_idsDirtyFlag())
            domain.setWebsiteMessageIds(model.getWebsite_message_ids());
        if(model.getDate_begin_locatedDirtyFlag())
            domain.setDateBeginLocated(model.getDate_begin_located());
        if(model.getEvent_logoDirtyFlag())
            domain.setEventLogo(model.getEvent_logo());
        if(model.getWebsite_urlDirtyFlag())
            domain.setWebsiteUrl(model.getWebsite_url());
        if(model.getColorDirtyFlag())
            domain.setColor(model.getColor());
        if(model.getWrite_uid_textDirtyFlag())
            domain.setWriteUidText(model.getWrite_uid_text());
        if(model.getEvent_type_id_textDirtyFlag())
            domain.setEventTypeIdText(model.getEvent_type_id_text());
        if(model.getCreate_uid_textDirtyFlag())
            domain.setCreateUidText(model.getCreate_uid_text());
        if(model.getAddress_id_textDirtyFlag())
            domain.setAddressIdText(model.getAddress_id_text());
        if(model.getCountry_id_textDirtyFlag())
            domain.setCountryIdText(model.getCountry_id_text());
        if(model.getMenu_id_textDirtyFlag())
            domain.setMenuIdText(model.getMenu_id_text());
        if(model.getCompany_id_textDirtyFlag())
            domain.setCompanyIdText(model.getCompany_id_text());
        if(model.getUser_id_textDirtyFlag())
            domain.setUserIdText(model.getUser_id_text());
        if(model.getOrganizer_id_textDirtyFlag())
            domain.setOrganizerIdText(model.getOrganizer_id_text());
        if(model.getUser_idDirtyFlag())
            domain.setUserId(model.getUser_id());
        if(model.getCountry_idDirtyFlag())
            domain.setCountryId(model.getCountry_id());
        if(model.getCompany_idDirtyFlag())
            domain.setCompanyId(model.getCompany_id());
        if(model.getOrganizer_idDirtyFlag())
            domain.setOrganizerId(model.getOrganizer_id());
        if(model.getMenu_idDirtyFlag())
            domain.setMenuId(model.getMenu_id());
        if(model.getCreate_uidDirtyFlag())
            domain.setCreateUid(model.getCreate_uid());
        if(model.getAddress_idDirtyFlag())
            domain.setAddressId(model.getAddress_id());
        if(model.getWrite_uidDirtyFlag())
            domain.setWriteUid(model.getWrite_uid());
        if(model.getEvent_type_idDirtyFlag())
            domain.setEventTypeId(model.getEvent_type_id());
        return domain ;
    }

}

    



