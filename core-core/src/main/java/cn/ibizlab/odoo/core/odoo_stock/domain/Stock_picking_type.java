package cn.ibizlab.odoo.core.odoo_stock.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import java.math.BigDecimal;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.util.DigestUtils;
import cn.ibizlab.odoo.util.domain.EntityBase;
import cn.ibizlab.odoo.util.annotation.DEField;
import cn.ibizlab.odoo.util.enums.DEPredefinedFieldType;
import cn.ibizlab.odoo.util.enums.DEFieldDefaultValueType;
import java.io.Serializable;
import lombok.Data;
import org.springframework.data.annotation.Transient;

import cn.ibizlab.odoo.util.domain.EntityClient;

/**
 * ServiceApi [拣货类型] 对象
 */
@Data
public class Stock_picking_type extends EntityClient implements Serializable {

    /**
     * 显示详细作业
     */
    @DEField(name = "show_operations")
    @JSONField(name = "show_operations")
    @JsonProperty("show_operations")
    private String showOperations;

    /**
     * ID
     */
    @DEField(isKeyField=true)
    @JSONField(name = "id")
    @JsonProperty("id")
    private Integer id;

    /**
     * 参考序列
     */
    @DEField(name = "sequence_id")
    @JSONField(name = "sequence_id")
    @JsonProperty("sequence_id")
    private Integer sequenceId;

    /**
     * 创建新批次/序列号码
     */
    @DEField(name = "use_create_lots")
    @JSONField(name = "use_create_lots")
    @JsonProperty("use_create_lots")
    private String useCreateLots;

    /**
     * 创建时间
     */
    @DEField(name = "create_date" , preType = DEPredefinedFieldType.CREATEDATE)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "create_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("create_date")
    private Timestamp createDate;

    /**
     * 条码
     */
    @JSONField(name = "barcode")
    @JsonProperty("barcode")
    private String barcode;

    /**
     * 显示预留
     */
    @DEField(name = "show_reserved")
    @JSONField(name = "show_reserved")
    @JsonProperty("show_reserved")
    private String showReserved;

    /**
     * 欠单比率
     */
    @JSONField(name = "rate_picking_backorders")
    @JsonProperty("rate_picking_backorders")
    private Integer ratePickingBackorders;

    /**
     * 序号
     */
    @JSONField(name = "sequence")
    @JsonProperty("sequence")
    private Integer sequence;

    /**
     * 拣货欠单个数
     */
    @JSONField(name = "count_picking_backorders")
    @JsonProperty("count_picking_backorders")
    private Integer countPickingBackorders;

    /**
     * 生产单的数量
     */
    @JSONField(name = "count_mo_todo")
    @JsonProperty("count_mo_todo")
    private Integer countMoTodo;

    /**
     * 颜色
     */
    @JSONField(name = "color")
    @JsonProperty("color")
    private Integer color;

    /**
     * 移动整个包裹
     */
    @DEField(name = "show_entire_packs")
    @JSONField(name = "show_entire_packs")
    @JsonProperty("show_entire_packs")
    private String showEntirePacks;

    /**
     * 显示名称
     */
    @JSONField(name = "display_name")
    @JsonProperty("display_name")
    private String displayName;

    /**
     * 生产单等待的数量
     */
    @JSONField(name = "count_mo_waiting")
    @JsonProperty("count_mo_waiting")
    private Integer countMoWaiting;

    /**
     * 草稿拣货个数
     */
    @JSONField(name = "count_picking_draft")
    @JsonProperty("count_picking_draft")
    private Integer countPickingDraft;

    /**
     * 有效
     */
    @JSONField(name = "active")
    @JsonProperty("active")
    private String active;

    /**
     * 迟到拣货个数
     */
    @JSONField(name = "count_picking_late")
    @JsonProperty("count_picking_late")
    private Integer countPickingLate;

    /**
     * 拣货个数
     */
    @JSONField(name = "count_picking")
    @JsonProperty("count_picking")
    private Integer countPicking;

    /**
     * 拣货个数准备好
     */
    @JSONField(name = "count_picking_ready")
    @JsonProperty("count_picking_ready")
    private Integer countPickingReady;

    /**
     * 最近10笔完成的拣货
     */
    @JSONField(name = "last_done_picking")
    @JsonProperty("last_done_picking")
    private String lastDonePicking;

    /**
     * 最后修改日
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "__last_update" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("__last_update")
    private Timestamp LastUpdate;

    /**
     * 等待拣货个数
     */
    @JSONField(name = "count_picking_waiting")
    @JsonProperty("count_picking_waiting")
    private Integer countPickingWaiting;

    /**
     * 作业的类型
     */
    @JSONField(name = "code")
    @JsonProperty("code")
    private String code;

    /**
     * 最后更新时间
     */
    @DEField(name = "write_date" , preType = DEPredefinedFieldType.UPDATEDATE)
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "write_date" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("write_date")
    private Timestamp writeDate;

    /**
     * 延迟比率
     */
    @JSONField(name = "rate_picking_late")
    @JsonProperty("rate_picking_late")
    private Integer ratePickingLate;

    /**
     * 作业类型
     */
    @JSONField(name = "name")
    @JsonProperty("name")
    private String name;

    /**
     * 生产单数量延迟
     */
    @JSONField(name = "count_mo_late")
    @JsonProperty("count_mo_late")
    private Integer countMoLate;

    /**
     * 使用已有批次/序列号码
     */
    @DEField(name = "use_existing_lots")
    @JSONField(name = "use_existing_lots")
    @JsonProperty("use_existing_lots")
    private String useExistingLots;

    /**
     * 创建人
     */
    @JSONField(name = "create_uid_text")
    @JsonProperty("create_uid_text")
    private String createUidText;

    /**
     * 默认源位置
     */
    @JSONField(name = "default_location_src_id_text")
    @JsonProperty("default_location_src_id_text")
    private String defaultLocationSrcIdText;

    /**
     * 退回的作业类型
     */
    @JSONField(name = "return_picking_type_id_text")
    @JsonProperty("return_picking_type_id_text")
    private String returnPickingTypeIdText;

    /**
     * 仓库
     */
    @JSONField(name = "warehouse_id_text")
    @JsonProperty("warehouse_id_text")
    private String warehouseIdText;

    /**
     * 默认目的位置
     */
    @JSONField(name = "default_location_dest_id_text")
    @JsonProperty("default_location_dest_id_text")
    private String defaultLocationDestIdText;

    /**
     * 最后更新者
     */
    @JSONField(name = "write_uid_text")
    @JsonProperty("write_uid_text")
    private String writeUidText;

    /**
     * 最后更新者
     */
    @DEField(name = "write_uid" , preType = DEPredefinedFieldType.UPDATEMAN)
    @JSONField(name = "write_uid")
    @JsonProperty("write_uid")
    private Integer writeUid;

    /**
     * 默认目的位置
     */
    @DEField(name = "default_location_dest_id")
    @JSONField(name = "default_location_dest_id")
    @JsonProperty("default_location_dest_id")
    private Integer defaultLocationDestId;

    /**
     * 创建人
     */
    @DEField(name = "create_uid" , preType = DEPredefinedFieldType.CREATEMAN)
    @JSONField(name = "create_uid")
    @JsonProperty("create_uid")
    private Integer createUid;

    /**
     * 仓库
     */
    @DEField(name = "warehouse_id")
    @JSONField(name = "warehouse_id")
    @JsonProperty("warehouse_id")
    private Integer warehouseId;

    /**
     * 默认源位置
     */
    @DEField(name = "default_location_src_id")
    @JSONField(name = "default_location_src_id")
    @JsonProperty("default_location_src_id")
    private Integer defaultLocationSrcId;

    /**
     * 退回的作业类型
     */
    @DEField(name = "return_picking_type_id")
    @JSONField(name = "return_picking_type_id")
    @JsonProperty("return_picking_type_id")
    private Integer returnPickingTypeId;


    /**
     * 
     */
    @JSONField(name = "odoocreate")
    @JsonProperty("odoocreate")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_users odooCreate;

    /**
     * 
     */
    @JSONField(name = "odoowrite")
    @JsonProperty("odoowrite")
    private cn.ibizlab.odoo.core.odoo_base.domain.Res_users odooWrite;

    /**
     * 
     */
    @JSONField(name = "odoodefaultlocationdest")
    @JsonProperty("odoodefaultlocationdest")
    private cn.ibizlab.odoo.core.odoo_stock.domain.Stock_location odooDefaultLocationDest;

    /**
     * 
     */
    @JSONField(name = "odoodefaultlocationsrc")
    @JsonProperty("odoodefaultlocationsrc")
    private cn.ibizlab.odoo.core.odoo_stock.domain.Stock_location odooDefaultLocationSrc;

    /**
     * 
     */
    @JSONField(name = "odooreturnpickingtype")
    @JsonProperty("odooreturnpickingtype")
    private cn.ibizlab.odoo.core.odoo_stock.domain.Stock_picking_type odooReturnPickingType;

    /**
     * 
     */
    @JSONField(name = "odoowarehouse")
    @JsonProperty("odoowarehouse")
    private cn.ibizlab.odoo.core.odoo_stock.domain.Stock_warehouse odooWarehouse;




    /**
     * 设置 [显示详细作业]
     */
    public void setShowOperations(String showOperations){
        this.showOperations = showOperations ;
        this.modify("show_operations",showOperations);
    }
    /**
     * 设置 [参考序列]
     */
    public void setSequenceId(Integer sequenceId){
        this.sequenceId = sequenceId ;
        this.modify("sequence_id",sequenceId);
    }
    /**
     * 设置 [创建新批次/序列号码]
     */
    public void setUseCreateLots(String useCreateLots){
        this.useCreateLots = useCreateLots ;
        this.modify("use_create_lots",useCreateLots);
    }
    /**
     * 设置 [条码]
     */
    public void setBarcode(String barcode){
        this.barcode = barcode ;
        this.modify("barcode",barcode);
    }
    /**
     * 设置 [显示预留]
     */
    public void setShowReserved(String showReserved){
        this.showReserved = showReserved ;
        this.modify("show_reserved",showReserved);
    }
    /**
     * 设置 [序号]
     */
    public void setSequence(Integer sequence){
        this.sequence = sequence ;
        this.modify("sequence",sequence);
    }
    /**
     * 设置 [颜色]
     */
    public void setColor(Integer color){
        this.color = color ;
        this.modify("color",color);
    }
    /**
     * 设置 [移动整个包裹]
     */
    public void setShowEntirePacks(String showEntirePacks){
        this.showEntirePacks = showEntirePacks ;
        this.modify("show_entire_packs",showEntirePacks);
    }
    /**
     * 设置 [有效]
     */
    public void setActive(String active){
        this.active = active ;
        this.modify("active",active);
    }
    /**
     * 设置 [作业的类型]
     */
    public void setCode(String code){
        this.code = code ;
        this.modify("code",code);
    }
    /**
     * 设置 [作业类型]
     */
    public void setName(String name){
        this.name = name ;
        this.modify("name",name);
    }
    /**
     * 设置 [使用已有批次/序列号码]
     */
    public void setUseExistingLots(String useExistingLots){
        this.useExistingLots = useExistingLots ;
        this.modify("use_existing_lots",useExistingLots);
    }
    /**
     * 设置 [默认目的位置]
     */
    public void setDefaultLocationDestId(Integer defaultLocationDestId){
        this.defaultLocationDestId = defaultLocationDestId ;
        this.modify("default_location_dest_id",defaultLocationDestId);
    }
    /**
     * 设置 [仓库]
     */
    public void setWarehouseId(Integer warehouseId){
        this.warehouseId = warehouseId ;
        this.modify("warehouse_id",warehouseId);
    }
    /**
     * 设置 [默认源位置]
     */
    public void setDefaultLocationSrcId(Integer defaultLocationSrcId){
        this.defaultLocationSrcId = defaultLocationSrcId ;
        this.modify("default_location_src_id",defaultLocationSrcId);
    }
    /**
     * 设置 [退回的作业类型]
     */
    public void setReturnPickingTypeId(Integer returnPickingTypeId){
        this.returnPickingTypeId = returnPickingTypeId ;
        this.modify("return_picking_type_id",returnPickingTypeId);
    }

}


