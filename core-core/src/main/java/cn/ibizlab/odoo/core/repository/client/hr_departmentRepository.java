package cn.ibizlab.odoo.core.repository.client;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import cn.ibizlab.odoo.core.repository.client.po.hr_department;

/**
 * 实体[hr_department] 服务对象接口
 */
public interface hr_departmentRepository{


    public hr_department createPO() ;
        public void remove(String id);

        public List<hr_department> search();

        public void get(String id);

        public void update(hr_department hr_department);

        public void removeBatch(String id);

        public void createBatch(hr_department hr_department);

        public void create(hr_department hr_department);

        public void updateBatch(hr_department hr_department);


}
